#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <errnoh>
#include <sys/types.h>
#include <sys/stats.h>

#include "gni_pub.h"
#include "pmi.h"

int cookie;
int modes        = 0;

#define NTRANS             20

typedef struct {
        gni_mem_handle_t mdh;
        uint64_t addr;
} mdh_addr_t;

static unsigned int
get_gni_nic_address(int device_id)
{
        unsigned int address, cpu_id;
        gni_return_t status;
        int i,alps_dev_id=-1,alps_address=-1;
        char *token,*p_ptr;

        p_ptr = getenv("PMI_GNI_DEV_ID");
        if (!p_ptr) {
                status = GNI_CdmGetNicAddress(device_id, &address, &cpu_id);
                if(status != GNI_RC_SUCCESS) {
                      fprintf(stderr,"FAILED:GNI_CdmGetNicAddress returned error %d\n",status);
                      abort();
                }
        } else {
                while ((token = strtok(p_ptr,":")) != NULL) {
                        alps_dev_id = atoi(token);
                        if (alps_dev_id == device_id) {
                                break;
                        }
                        p_ptr = NULL;
                }
                assert(alps_dev_id != -1);
                p_ptr = getenv("PMI_GNI_LOC_ADDR");
                assert(p_ptr != NULL);
                i = 0;
                while ((token = strtok(p_ptr,":")) != NULL) {
                        if (i == alps_dev_id) {
                                alps_address = atoi(token);
                                break;
                        }
                        p_ptr = NULL;
                        ++i;
                }
                assert(alps_address != -1);
                address = alps_address;
                address = alps_address;
        }

        return address;
}

static uint8_t get_ptag(void)
{
        char *p_ptr,*token;
        uint8_t ptag;

        p_ptr = getenv("PMI_GNI_PTAG");
        assert(p_ptr != NULL);  /* something wrong like we haven't called PMI_Init */
        token = strtok(p_ptr,":");
        ptag = (uint8_t)atoi(token);
        return ptag;
}

static uint32_t get_cookie(void)
{
        uint32_t cookie;
        char *p_ptr,*token;

        p_ptr = getenv("PMI_GNI_COOKIE");
        assert(p_ptr != NULL);
        token = strtok(p_ptr,":");
        cookie = (uint32_t)atoi(token);

        return cookie;
}



int main(int argc,char **argv)
{
        register int          i;
        int                   rc;
        int                   inst_id, nranks;
        int                   n_entries = 1024;
        int                   device_id = 0;
        int                   send_to, recv_from;
        int                   events_returned;
        int                   test_id;
        unsigned int          local_addr;
        unsigned int          remote_addr;
        gni_cdm_handle_t      cdm_hndl;
        gni_nic_handle_t      nic_hndl;
        gni_return_t          status = GNI_RC_SUCCESS;
        uint32_t              vmdh_index = -1;
        uint64_t              *recv_buffer;
        gni_mem_handle_t      my_remote_mdh;
        uint8_t                  ptag;
        gni_post_descriptor_t fma_data_desc[NTRANS];
        gni_post_descriptor_t *post_desc_ptr;
        mdh_addr_t            my_mdh_addr;
        mdh_addr_t            *remote_mdh_addr_vec;
        gni_cq_handle_t       dst_cq_hndl = NULL;
        gni_cq_handle_t       cq_hndl;
        gni_ep_handle_t       *ep_hndl_array;
        gni_cq_entry_t        event_data;
        int first_spawned;

        if (argc > 1) {
                device_id = atoi(argv[1]);
                fprintf(stderr, "going to try to attach to device %d\n", device_id);
        }

        rc = PMI_Init(&first_spawned);
        assert(rc == PMI_SUCCESS);

        rc = PMI_Get_size(&nranks);
        assert(rc == PMI_SUCCESS);

        rc = PMI_Get_rank(&inst_id);
        assert(rc == PMI_SUCCESS);

        ptag = get_ptag();
        cookie = get_cookie();

        /* Create and attach to the communication domain. */

        status = GNI_CdmCreate(inst_id, ptag, cookie, modes, &cdm_hndl);
        if (status != GNI_RC_SUCCESS) {
                fprintf(stderr, "FAIL: GNI_CdmCreate returned error %s\n", gni_err_str[status]);
                PMI_Abort(-1,"pmi abort called");
        } else {
                fprintf(stderr, "Inst_id %d created CDM, now attaching...\n", inst_id);
        }

        status = GNI_CdmAttach(cdm_hndl, device_id, &local_addr, &nic_hndl);
        if (status != GNI_RC_SUCCESS) {
                fprintf(stderr, "FAIL: GNI_CdmAttach returned error %s\n", gni_err_str[status]);
                PMI_Abort(-1,"pmi abort called");
        } else {
                fprintf(stderr, "Inst_id %d attached CDM to NIC\n", inst_id);
        }

        // Iterate allocate 512 MB, 1 GB, 2 GB, and 4 GB with different page sizes
        int count = gethugepagesizes(NULL,0);
        long *sizes = (long*)malloc((count+1)*sizeof(long));
        for (int i = 0; i < count; i++)
        {
          fprintf(stdout,"Testing page size... %ld\n", sizes[i]);
          int fd = hugetlbfs_unlinked_fd_for_size(sizes[i]);
          const int mmap_flags = MAP_SHARED;
          for (int j = 0; j < 4; j++)
          {
            const long alloc_size = (long(1) << (29+j));
            void *ptr = mmap(NULL, alloc_size, (PROT_READ|PROT_WRITE), mmap_flags, fd, 0);
            assert(ptr != NULL);
            // Now try to register it with the NIC
            status = GNI_MemRegister(nic_hndl, (uint64_t)ptr, (uint64_t)alloc_size,
                                     NULL, GNI_MEM_READWRITE, -1, &my_remote_mdh);
            if (status == GNI_RC_SUCCESS)
            {
              fprintf(stdout,"  Allocation for %ld bytes SUCCEEDED!\n", alloc_size):
              status = GNI_MemDeregister(nic_hndl, my_remote_mdh);
              assert(status == GNI_RC_SUCCESS);
            }
            else
            {
              fprintf(stdout,"  Allocation for %ld bytes FAILED with error code %s!\n",
                                alloc_size, gni_return_string(status));
            }
            // Now we can unmap the memory
            assert(munmap(ptr, alloc_size) == 0);
          }
          close(fd);
        }

        free(sizes);

        PMI_Finalize();
        return 0;
}


