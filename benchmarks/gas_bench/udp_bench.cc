#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <mpi.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <errno.h>
#include <netdb.h>

#include <vector>
#include <deque>

#define CHECK_PTHREAD(cmd) do { \
  int ret = (cmd); \
  if(ret != 0) { \
    fprintf(stderr, "PTHREAD: %s = %d (%s)\n", #cmd, ret, strerror(ret)); \
    exit(1); \
  } \
} while(0)

#define CHECK_MPI(cmd) do { \
  int ret = (cmd); \
  if(ret != MPI_SUCCESS) { \
    char buffer[256]; int len = 256; \
    MPI_Error_string(ret, buffer, &len); buffer[len] = 0; \
    fprintf(stderr, "MPI: %s = %d (%s)\n", #cmd, ret, buffer); \
    exit(1); \
  } \
} while(0)

#define CHECK_LIBC(cmd) do { \
  int ret = (cmd); \
  if(ret < 0) { \
    fprintf(stderr, "LIBC: %s = %d (%s)\n", #cmd, errno, strerror(errno));	\
    exit(1); \
  } \
} while(0)

typedef unsigned long long time64_t;
#define UNPACK_TIME64(t) ((gasnet_handlerarg_t)((t) >> 32)), ((gasnet_handlerarg_t)(t))
#define PACK_TIME64(a,b) ((((time64_t)(a)) << 32) | ((unsigned)(b)))

static inline time64_t now(void)
{
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return ((ts.tv_sec * 1000000000ULL) + ts.tv_nsec);
}

static int myrank = -1;
static int num_nodes = -1;
static int fd_outbound = -1;
static int fd_inbound = -1;
static sockaddr_in myaddr;
static sockaddr_in *all_addrs = 0;

template <typename T>
class Stat {
public:
  Stat(void) : count(0), sum(T()), sum2(T()), max(T())
  {
    pthread_mutex_init(&lock, 0);
  }

  void add_data_point(T val)
  {
    pthread_mutex_lock(&lock);
    count++;
    sum += val;
    sum2 += val * val;
    if(val > max) max = val;
    pthread_mutex_unlock(&lock);
  }

  void clear(void)
  {
    count = 0;
    sum = T();
    sum2 = T();
    max = T();
  }

  unsigned get_count(void) const { return count; }
  T get_max(void) const { return max; }
  double get_average(void) const { return ((double)sum) / count; }
  double get_stddev(void) const { return (sqrt(count * ((double)sum2) - (((double)sum) * ((double)sum))) / count); }

protected:
  unsigned count;
  T sum, sum2, max;
  pthread_mutex_t lock;
};

Stat<double> ping_latency_in_us;
static volatile int pings_in_flight = 0;

void init_sockets()
{
  CHECK_MPI( MPI_Comm_rank(MPI_COMM_WORLD, &myrank) );
  CHECK_MPI( MPI_Comm_size(MPI_COMM_WORLD, &num_nodes) );

  CHECK_LIBC( fd_outbound = socket(AF_INET, SOCK_DGRAM, 0) );
  CHECK_LIBC( fd_inbound = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0) );

  char hostname[80];
  gethostname(hostname, 80);
  printf("hostname = %s\n", hostname);
  struct hostent *hostent = gethostbyname(hostname);
  if(!hostent) {
    // try again with a . at the end
    strcat(hostname, ".");
    hostent = gethostbyname(hostname);
  }
  assert(hostent);
  assert(hostent->h_addrtype == AF_INET);
  assert(hostent->h_addr_list[0]);
  //printf("IP = %08x\n", *(unsigned *)(hostent->h_addr_list[0]));

  myaddr.sin_family = AF_INET;
  myaddr.sin_port = htons(44544 + myrank);
  memcpy(&myaddr.sin_addr.s_addr, hostent->h_addr_list[0], 4);
  CHECK_LIBC( bind(fd_inbound, (const sockaddr *)&myaddr, sizeof(myaddr)) );
  printf("rank %d bound to: %08x:%d\n", myrank, ntohl(myaddr.sin_addr.s_addr), ntohs(myaddr.sin_port));
  
  all_addrs = new sockaddr_in[num_nodes];
  memcpy(all_addrs+myrank, &myaddr, sizeof(myaddr));
  for(int i = 0; i < num_nodes; i++)
    CHECK_MPI( MPI_Bcast(all_addrs+i, sizeof(all_addrs[i]), MPI_BYTE, i, MPI_COMM_WORLD) );

  // for(int i = 0; i < num_nodes; i++)
  //   printf("%d: %d = %08x:%d (%d)\n", myrank, i, ntohl(all_addrs[i].sin_addr.s_addr), ntohs(all_addrs[i].sin_port), all_addrs[i].sin_family);
}

enum { MSGID_REQUEST = 88,
       MSGID_RESPONSE = 99 };

struct pinghdr {
  int sender;
  int msgtype;
  time64_t send_time;
};

void socket_poll(void)
{
  while(1) {
    sockaddr_in peer;
    socklen_t peerlen = sizeof(peer);
    char buffer[2048];
    int len = 2048;
    int ret = recvfrom(fd_inbound, buffer, len, 0, (sockaddr *)&peer, &peerlen);
    if((ret < 0) && ((errno == EAGAIN) || (errno == EWOULDBLOCK))) return;

    if(ret < 0) {
      printf("error? %d (%s)\n", errno, strerror(errno));
      exit(1);
    }
    assert(ret >= 8);
    pinghdr *hdr = (pinghdr *)buffer;

    //printf("recevied packet: %08x:%04x, %d bytes (%d, %d)\n", ntohl(peer.sin_addr.s_addr), ntohs(peer.sin_port), ret, hdr->sender, hdr->msgtype);

    if(hdr->msgtype == MSGID_REQUEST) {
      // got a request, so send a response - just the hdr, back to the requesting node
      time64_t mytime = now();
      //printf("[%d] request: %llu, %llu\n", myrank, hdr->send_time, mytime);
      hdr->msgtype = MSGID_RESPONSE;
      CHECK_LIBC( sendto(fd_outbound, hdr, sizeof(pinghdr), 0, (const sockaddr *)(all_addrs+hdr->sender), sizeof(sockaddr_in)) );
      continue;
    }

    if(hdr->msgtype == MSGID_RESPONSE) {
      // record the response
      time64_t mytime = now();
      time64_t origtime = hdr->send_time;
      //printf("[%d] response: %llu - %llu = %llu\n", myrank, mytime, origtime, mytime - origtime);
      ping_latency_in_us.add_data_point((mytime - origtime) * 1e-3);
      __sync_fetch_and_add(&pings_in_flight, -1);
      continue;
    }

    assert(0);
  }
}

void send_ping_request(int target, int size)
{
  char buffer[2048];
  pinghdr *hdr = (pinghdr *)buffer;
  hdr->sender = myrank;
  hdr->msgtype = MSGID_REQUEST;
  hdr->send_time = now();
  CHECK_LIBC( sendto(fd_outbound, buffer, size, 0, (const sockaddr *)(all_addrs+target), sizeof(sockaddr_in)) );
}

// polling thread(s)

static volatile int poller_count = 0;
static volatile bool poller_run = true;
static pthread_mutex_t poller_count_mutex = PTHREAD_MUTEX_INITIALIZER;

static void *polling_thread(void *data)
{
  pthread_mutex_lock(&poller_count_mutex);
  poller_count++;
  pthread_mutex_unlock(&poller_count_mutex);

  while(poller_run) {
    socket_poll();
  }

  pthread_mutex_lock(&poller_count_mutex);
  poller_count--;
  pthread_mutex_unlock(&poller_count_mutex);

  return data;
}

static void start_polling_threads(int target_count)
{
  int i;
  for(i = 0; i < target_count; i++) {
    pthread_t t;
    CHECK_PTHREAD( pthread_create(&t, 0, polling_thread, 0) );
  }
  while(poller_count < target_count) {
    socket_poll();
  }
}

static void stop_polling_threads(void)
{
  poller_run = false;
  while(poller_count > 0) {
    socket_poll();
  }
}

static void barrier(int id = 0)
{
  CHECK_MPI( MPI_Barrier(MPI_COMM_WORLD) );
}

class Config {
public:
  Config(void)
    : num_polling_threads(1), app_polls(false),
      quick_mode(false), base_count(100)
  {
    *outfile = 0;
  }

  void parse_args(int argc, char *argv[])
  {
    for(int i = 1; i < argc; i++) {
      if(!strcmp(argv[i], "-q")) {
	quick_mode = (atoi(argv[++i]) != 0);
	continue;
      }

      if(!strcmp(argv[i], "-p")) {
	num_polling_threads = atoi(argv[++i]);
	continue;
      }

      if(!strcmp(argv[i], "-c")) {
	base_count = atoi(argv[++i]);
	continue;
      }

      if(!strcmp(argv[i], "-ap")) {
	app_polls = (atoi(argv[++i]) != 0);
	continue;
      }

      if(!strcmp(argv[i], "-o")) {
	strcpy(outfile, argv[++i]);
	continue;
      }
    }

    // if there are no polling threads, app must poll
    if(!num_polling_threads)
      app_polls = true;
  }
  
  int num_polling_threads;
  bool app_polls;
  bool quick_mode;
  int base_count;
  char outfile[80];
};

static Config config;

#define APP_WAIT(cond) do { while(!(cond)) { if(config.app_polls) socket_poll(); } } while(0)
#define APP_WAIT_TIMEOUT(cond,timeout) do { while(!(cond) && (now() < (timeout))) { if(config.app_polls) socket_poll(); } } while(0)

class TestPattern {
public:
  TestPattern(void)
  {
    name[0] = 0;
  }

  ~TestPattern(void) {}

  struct TestResults {
    double latency_avg;
    double latency_stddev;
    double packets_per_sec;
    double bytes_per_sec;
  };

  virtual void prepare(int src_node, int dst_node) = 0;
  virtual void run_one(int src_node, int dst_node, TestResults& results) = 0;

  void run(FILE *f) {
    time64_t t_start = now();

    // unidirectional, try all pairs
    for(int src_node = 0; src_node < num_nodes; src_node++) {
      for(int dst_node = 0; dst_node < num_nodes; dst_node++) {
	prepare(src_node, dst_node);

	barrier();

	if(myrank == src_node) {
	  TestResults results;
	  run_one(src_node, dst_node, results);

	  fprintf((f ? f : stdout),
		  "%s,%d->%d,%g,%g,%g,%g\n", 
		  name, src_node, dst_node,
		  results.latency_avg, results.latency_stddev,
		  results.packets_per_sec, results.bytes_per_sec);
	}

	barrier();
      }
    }

    // bidirectional, try all pairs
    for(int node1 = 0; node1 < num_nodes; node1++) {
      for(int node2 = node1 + 1; node2 < num_nodes; node2++) {
	if(myrank == node1) prepare(node1, node2);
	if(myrank == node2) prepare(node2, node1);

	barrier();

	if(myrank == node1) {
	  TestResults results;
	  run_one(node1, node2, results);

	  fprintf((f ? f : stdout),
		  "%s,%d<->%d,%g,%g,%g,%g\n", 
		  name, node1, node2,
		  results.latency_avg, results.latency_stddev,
		  results.packets_per_sec, results.bytes_per_sec);
	}
	if(myrank == node2) {
	  TestResults results;
	  run_one(node2, node1, results);

	  fprintf((f ? f : stdout),
		  "%s,%d<->%d,%g,%g,%g,%g\n", 
		  name, node2, node1,
		  results.latency_avg, results.latency_stddev,
		  results.packets_per_sec, results.bytes_per_sec);
	}

	barrier();
      }
    }

    // sender/receiver bottlenecks
    for(int node = 0; node < num_nodes; node++) {
      // let one source node spray randomly to eliminate receiver bottlenecks
      {
	prepare(node, -1);

	barrier();

	if(myrank == node) {
	  TestResults results;
	  run_one(node, -1, results);

	  fprintf((f ? f : stdout),
		  "%s,%d->R,%g,%g,%g,%g\n", 
		  name, node,
		  results.latency_avg, results.latency_stddev,
		  results.packets_per_sec, results.bytes_per_sec);
	}

	barrier();
      }

      // now have all nodes poke at a single one to force receiver bottlneck
      {
	int src_node = myrank;
	int dst_node = node;

	prepare(src_node, dst_node);

	barrier();

	TestResults results;
	run_one(src_node, dst_node, results);

	fprintf((f ? f : stdout),
		"%s,%d*->%d,%g,%g,%g,%g\n", 
		name, src_node, dst_node,
		results.latency_avg, results.latency_stddev,
		results.packets_per_sec, results.bytes_per_sec);

	barrier();
      }
    }

    // now a cycle where everybody sends to the next guy
    {
      int src_node = myrank;
      int dst_node = (src_node + 1) % num_nodes;

      prepare(src_node, dst_node);

      barrier();

      TestResults results;
      run_one(src_node, dst_node, results);

      fprintf((f ? f : stdout),
	      "%s,%d->%d*,%g,%g,%g,%g\n", 
	      name, src_node, dst_node,
	      results.latency_avg, results.latency_stddev,
	      results.packets_per_sec, results.bytes_per_sec);

      barrier();
    }

    // now random traffic
    {
      int src_node = myrank;

      prepare(src_node, -1);

      barrier();

      TestResults results;
      run_one(src_node, -1, results);

      fprintf((f ? f : stdout),
	      "%s,%d->R*,%g,%g,%g,%g\n", 
	      name, src_node,
	      results.latency_avg, results.latency_stddev,
	      results.packets_per_sec, results.bytes_per_sec);
      
      barrier();
    }

    fflush(f);

    time64_t t_finish = now();
    if(myrank == 0) {
      printf("%s = %5.3f\n", name, (t_finish - t_start) * 1e-9);
      fflush(stdout);
    }
  }

protected:
  char name[80];
};

class TestPings : public TestPattern {
public:
  TestPings(int _num_pings, int _max_in_flight, int _ping_size)
    : num_pings(_num_pings), max_in_flight(_max_in_flight), ping_size(_ping_size)
  {
  }

  virtual void prepare(int src_node, int dst_node)
  {
    ping_latency_in_us.clear();
  }

  virtual void send_ping(int src_node, int dst_node) = 0;

  virtual void run_one(int src_node, int dst_node, TestResults& results)
  {
    pings_in_flight = 0;

    time64_t t1 = now();
    for(int i = 0; i < num_pings; i++) {
      // wait until enough have finished
      APP_WAIT(pings_in_flight < max_in_flight);
      
      __sync_fetch_and_add(&pings_in_flight, 1);
      if(dst_node >= 0)
	send_ping(src_node, dst_node);
      else
	send_ping(src_node, random() % num_nodes);
    }
    time64_t t2 = now();

    // wait until all pings finish
    time64_t timeout = t2 + 2000000000ULL; // wait up to 2 seconds to finish
    APP_WAIT_TIMEOUT(pings_in_flight == 0, timeout);
    if(pings_in_flight)
      printf("lost %d packets\n", pings_in_flight);

    time64_t t3 = now();
    
    results.latency_avg = ping_latency_in_us.get_average();
    results.latency_stddev = ping_latency_in_us.get_stddev();
    results.packets_per_sec = (num_pings - pings_in_flight) * (1e9 / (t3 - t1));
    results.bytes_per_sec = results.packets_per_sec * ping_size;
  }

protected:
  int num_pings, max_in_flight, ping_size;
};

class TestPingShort : public TestPings {
public:
  TestPingShort(int _num_pings, int _max_in_flight, int _ping_size)
    : TestPings(_num_pings, _max_in_flight, _ping_size)
  {
    sprintf(name, "pingshort,%d,%d,%d",
	    num_pings, max_in_flight, ping_size);
  }

  virtual void send_ping(int src_node, int dst_node)
  {
    send_ping_request(dst_node, ping_size);
  }
};

int main(int argc, char *argv[])
{
  CHECK_MPI( MPI_Init(&argc, &argv) );

  config.parse_args(argc, argv);

  init_sockets();
  printf("init done: node %d out of %d\n", myrank, num_nodes);

  char hostname[80];
  gethostname(hostname, 80);
  printf("(%d) attached (%s)\n", myrank, hostname);

  start_polling_threads(config.num_polling_threads);

  FILE *outf = 0;
  if(config.outfile[0]) {
    char filename[512];
    sprintf(filename, "%s.%d", config.outfile, myrank);
    outf = fopen(filename, "w");
    assert(outf);
  }

  std::vector<int> short_sizes;
  short_sizes.push_back(16);
  short_sizes.push_back(32);
  short_sizes.push_back(64);
  short_sizes.push_back(128);
  short_sizes.push_back(256);
  short_sizes.push_back(512);
  short_sizes.push_back(1024);
  short_sizes.push_back(1500);

  for(std::vector<int>::const_iterator it = short_sizes.begin(); it != short_sizes.end(); it++) {
    int short_size = *it;
    TestPingShort(config.base_count * 100, 1, short_size).run(outf);
    TestPingShort(config.base_count * 100, 10, short_size).run(outf);
    TestPingShort(config.base_count * 100, 100, short_size).run(outf);
    // if we don't have a plan for losing packets, this is a really bad idea
    //TestPingShort(config.base_count * 1000, config.base_count * 1000, short_size).run(outf);
    if(config.quick_mode) break;
  }

  if(outf)
    fclose(outf);

  stop_polling_threads();
  
  CHECK_MPI( MPI_Finalize() );
}
