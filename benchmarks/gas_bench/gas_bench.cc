#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <math.h>
#include <assert.h>

#include <vector>
#include <deque>

#define GASNET_PAR
#include <gasnet.h>

#define CHECK_PTHREAD(cmd) do { \
  int ret = (cmd); \
  if(ret != 0) { \
    fprintf(stderr, "PTHREAD: %s = %d (%s)\n", #cmd, ret, strerror(ret)); \
    exit(1); \
  } \
} while(0)

#define CHECK_GASNET(cmd) do { \
  int ret = (cmd); \
  if(ret != GASNET_OK) { \
    fprintf(stderr, "GASNET: %s = %d (%s, %s)\n", #cmd, ret, gasnet_ErrorName(ret), gasnet_ErrorDesc(ret)); \
    exit(1); \
  } \
} while(0)

typedef unsigned long long time64_t;
#define UNPACK_TIME64(t) ((gasnet_handlerarg_t)((t) >> 32)), ((gasnet_handlerarg_t)(t))
#define PACK_TIME64(a,b) ((((time64_t)(a)) << 32) | ((unsigned)(b)))

static inline time64_t now(void)
{
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return ((ts.tv_sec * 1000000000ULL) + ts.tv_nsec);
}

// polling thread(s)

static volatile int poller_count = 0;
static volatile bool poller_run = true;
static gasnet_hsl_t poller_count_mutex = GASNET_HSL_INITIALIZER;

static void *polling_thread(void *data)
{
  gasnet_hsl_lock(&poller_count_mutex);
  poller_count++;
  gasnet_hsl_unlock(&poller_count_mutex);

  while(poller_run) {
    gasnet_AMPoll();
  }

  gasnet_hsl_lock(&poller_count_mutex);
  poller_count--;
  gasnet_hsl_unlock(&poller_count_mutex);

  return data;
}

static void start_polling_threads(int target_count)
{
  int i;
  for(i = 0; i < target_count; i++) {
    pthread_t t;
    CHECK_PTHREAD( pthread_create(&t, 0, polling_thread, 0) );
  }
  while(poller_count < target_count) {
    gasnet_AMPoll();
  }
}

static void stop_polling_threads(void)
{
  poller_run = false;
  while(poller_count > 0) {
    gasnet_AMPoll();
  }
}

template <typename T>
class Stat {
public:
  Stat(void) : count(0), sum(T()), sum2(T()), max(T())
  {
    gasnet_hsl_init(&lock);
  }

  void add_data_point(T val)
  {
    gasnet_hsl_lock(&lock);
    count++;
    sum += val;
    sum2 += val * val;
    if(val > max) max = val;
    gasnet_hsl_unlock(&lock);
  }

  void clear(void)
  {
    count = 0;
    sum = T();
    sum2 = T();
    max = T();
  }

  unsigned get_count(void) const { return count; }
  T get_max(void) const { return max; }
  double get_average(void) const { return ((double)sum) / count; }
  double get_stddev(void) const { return (sqrt(count * ((double)sum2) - (((double)sum) * ((double)sum))) / count); }

protected:
  unsigned count;
  T sum, sum2, max;
  gasnet_hsl_t lock;
};

#define _HANDLERS_(f) \
  f(MSGID_PINGSH_REQUEST, handle_ping_short_request) \
  f(MSGID_PINGMED_REQUEST, handle_ping_medium_request) \
  f(MSGID_PINGLONG_REQUEST, handle_ping_long_request) \
  f(MSGID_PING_RESPONSE, handle_ping_response)

#define HANDLER_ENUM(id, fn) id,
enum {
  FIRST_MSGID = 128,
  _HANDLERS_(HANDLER_ENUM)
};

Stat<double> *ping_latency_in_us = 0;
static volatile int pings_in_flight = 0;

// expletive deleted...
#define EXPAND_ARGS(f,x) f x

static void handle_ping_short_request(gasnet_token_t token, gasnet_handlerarg_t arg0, gasnet_handlerarg_t arg1)
{
  time64_t mytime = now();
  //printf("[%d] request: %llu, %llu\n", gasnet_mynode(), PACK_TIME64(arg0, arg1), mytime);
  CHECK_GASNET( EXPAND_ARGS(gasnet_AMReplyShort4, (token, MSGID_PING_RESPONSE, arg0, arg1, UNPACK_TIME64(mytime))) );
}

static void handle_ping_medium_request(gasnet_token_t token, void *buf, size_t nbytes, gasnet_handlerarg_t arg0, gasnet_handlerarg_t arg1)
{
  time64_t mytime = now();
  //printf("[%d] request: %llu, %llu\n", gasnet_mynode(), PACK_TIME64(arg0, arg1), mytime);
  CHECK_GASNET( EXPAND_ARGS(gasnet_AMReplyShort4, (token, MSGID_PING_RESPONSE, arg0, arg1, UNPACK_TIME64(mytime))) );
}

static void handle_ping_long_request(gasnet_token_t token, void *buf, size_t nbytes, gasnet_handlerarg_t arg0, gasnet_handlerarg_t arg1)
{
  time64_t mytime = now();
  //printf("[%d] request: %llu, %llu\n", gasnet_mynode(), PACK_TIME64(arg0, arg1), mytime);
  CHECK_GASNET( EXPAND_ARGS(gasnet_AMReplyShort4, (token, MSGID_PING_RESPONSE, arg0, arg1, UNPACK_TIME64(mytime))) );
}

static void handle_ping_response(gasnet_token_t token, gasnet_handlerarg_t arg0, gasnet_handlerarg_t arg1, gasnet_handlerarg_t arg2, gasnet_handlerarg_t arg3)
{
  time64_t mytime = now();
  time64_t origtime = PACK_TIME64(arg0, arg1);
  //printf("[%d] response: %llu - %llu = %llu\n", gasnet_mynode(), mytime, origtime, mytime - origtime);
  ping_latency_in_us->add_data_point((mytime - origtime) * 1e-3);
  __sync_fetch_and_add(&pings_in_flight, -1);
}

#define HANDLER_ENTRY(id, fn) { id, ((void(*)()) fn) },
static gasnet_handlerentry_t handlers[] = {
  _HANDLERS_(HANDLER_ENTRY)
};

static gasnet_seginfo_t *segments = 0;

static void barrier(int id = 0)
{
  gasnet_barrier_notify(id, (id == 0) ? GASNET_BARRIERFLAG_ANONYMOUS : 0);
  CHECK_GASNET( gasnet_barrier_wait(id, (id == 0) ? GASNET_BARRIERFLAG_ANONYMOUS : 0) );
}

class Config {
public:
  Config(void)
    : num_polling_threads(0), app_polls_gasnet(false),
      segsize_in_mb(512), quick_mode(false), base_count(100)
  {
    *outfile = 0;
  }

  void parse_args(int argc, char *argv[])
  {
    for(int i = 1; i < argc; i++) {
      if(!strcmp(argv[i], "-q")) {
	quick_mode = (atoi(argv[++i]) != 0);
	continue;
      }

      if(!strcmp(argv[i], "-p")) {
	num_polling_threads = atoi(argv[++i]);
	continue;
      }

      if(!strcmp(argv[i], "-c")) {
	base_count = atoi(argv[++i]);
	continue;
      }

      if(!strcmp(argv[i], "-ap")) {
	app_polls_gasnet = (atoi(argv[++i]) != 0);
	continue;
      }

      if(!strcmp(argv[i], "-ss")) {
	segsize_in_mb = atoi(argv[++i]);
	continue;
      }

      if(!strcmp(argv[i], "-o")) {
	strcpy(outfile, argv[++i]);
	continue;
      }
    }

    // if there are no polling threads, app must poll
    if(!num_polling_threads)
      app_polls_gasnet = true;
  }
  
  int num_polling_threads;
  bool app_polls_gasnet;
  int segsize_in_mb;
  bool quick_mode;
  int base_count;
  char outfile[80];
};

static Config config;

#define APP_WAIT(cond) do { while(!(cond)) { if(config.app_polls_gasnet) gasnet_AMPoll(); } } while(0)

class TestPattern {
public:
  TestPattern(void)
  {
    name[0] = 0;
  }

  ~TestPattern(void) {}

  struct TestResults {
    double latency_avg;
    double latency_stddev;
    double packets_per_sec;
    double bytes_per_sec;
  };

  virtual void prepare(int src_node, int dst_node) = 0;
  virtual void run_one(int src_node, int dst_node, TestResults& results) = 0;

  void run(FILE *f) {
    time64_t t_start = now();

    // unidirectional, try all pairs
    for(int src_node = 0; src_node < gasnet_nodes(); src_node++) {
      for(int dst_node = 0; dst_node < gasnet_nodes(); dst_node++) {
	prepare(src_node, dst_node);

	barrier();

	if(gasnet_mynode() == src_node) {
	  TestResults results;
	  run_one(src_node, dst_node, results);

	  fprintf((f ? f : stdout),
		  "%s,%d->%d,%g,%g,%g,%g\n", 
		  name, src_node, dst_node,
		  results.latency_avg, results.latency_stddev,
		  results.packets_per_sec, results.bytes_per_sec);
	}

	barrier();
      }
    }

    // bidirectional, try all pairs
    for(int node1 = 0; node1 < gasnet_nodes(); node1++) {
      for(int node2 = node1 + 1; node2 < gasnet_nodes(); node2++) {
	if(gasnet_mynode() == node1) prepare(node1, node2);
	if(gasnet_mynode() == node2) prepare(node2, node1);

	barrier();

	if(gasnet_mynode() == node1) {
	  TestResults results;
	  run_one(node1, node2, results);

	  fprintf((f ? f : stdout),
		  "%s,%d<->%d,%g,%g,%g,%g\n", 
		  name, node1, node2,
		  results.latency_avg, results.latency_stddev,
		  results.packets_per_sec, results.bytes_per_sec);
	}
	if(gasnet_mynode() == node2) {
	  TestResults results;
	  run_one(node2, node1, results);

	  fprintf((f ? f : stdout),
		  "%s,%d<->%d,%g,%g,%g,%g\n", 
		  name, node2, node1,
		  results.latency_avg, results.latency_stddev,
		  results.packets_per_sec, results.bytes_per_sec);
	}

	barrier();
      }
    }

    // sender/receiver bottlenecks
    for(int node = 0; node < gasnet_nodes(); node++) {
      // let one source node spray randomly to eliminate receiver bottlenecks
      {
	prepare(node, -1);

	barrier();

	if(gasnet_mynode() == node) {
	  TestResults results;
	  run_one(node, -1, results);

	  fprintf((f ? f : stdout),
		  "%s,%d->R,%g,%g,%g,%g\n", 
		  name, node,
		  results.latency_avg, results.latency_stddev,
		  results.packets_per_sec, results.bytes_per_sec);
	}

	barrier();
      }

      // now have all nodes poke at a single one to force receiver bottlneck
      {
	int src_node = gasnet_mynode();
	int dst_node = node;

	prepare(src_node, dst_node);

	barrier();

	TestResults results;
	run_one(src_node, dst_node, results);

	fprintf((f ? f : stdout),
		"%s,%d*->%d,%g,%g,%g,%g\n", 
		name, src_node, dst_node,
		results.latency_avg, results.latency_stddev,
		results.packets_per_sec, results.bytes_per_sec);

	barrier();
      }
    }

    // now a cycle where everybody sends to the next guy
    {
      int src_node = gasnet_mynode();
      int dst_node = (src_node + 1) % gasnet_nodes();

      prepare(src_node, dst_node);

      barrier();

      TestResults results;
      run_one(src_node, dst_node, results);

      fprintf((f ? f : stdout),
	      "%s,%d->%d*,%g,%g,%g,%g\n", 
	      name, src_node, dst_node,
	      results.latency_avg, results.latency_stddev,
	      results.packets_per_sec, results.bytes_per_sec);

      barrier();
    }

    // now random traffic
    {
      int src_node = gasnet_mynode();

      prepare(src_node, -1);

      barrier();

      TestResults results;
      run_one(src_node, -1, results);

      fprintf((f ? f : stdout),
	      "%s,%d->R*,%g,%g,%g,%g\n", 
	      name, src_node,
	      results.latency_avg, results.latency_stddev,
	      results.packets_per_sec, results.bytes_per_sec);
      
      barrier();
    }

    fflush(f);

    time64_t t_finish = now();
    if(gasnet_mynode() == 0) {
      printf("%s = %5.3f\n", name, (t_finish - t_start) * 1e-9);
      fflush(stdout);
    }
  }

protected:
  char name[80];
};

class TestPings : public TestPattern {
public:
  TestPings(int _num_pings, int _max_in_flight, int _ping_size)
    : num_pings(_num_pings), max_in_flight(_max_in_flight), ping_size(_ping_size)
  {
  }

  virtual void prepare(int src_node, int dst_node)
  {
    ping_latency_in_us->clear();
  }

  virtual void send_ping(int src_node, int dst_node) = 0;

  virtual void run_one(int src_node, int dst_node, TestResults& results)
  {
    time64_t t1 = now();
    for(int i = 0; i < num_pings; i++) {
      // wait until enough have finished
      APP_WAIT(pings_in_flight < max_in_flight);
      
      __sync_fetch_and_add(&pings_in_flight, 1);
      if(dst_node >= 0)
	send_ping(src_node, dst_node);
      else
	send_ping(src_node, random() % gasnet_nodes());
    }
    time64_t t2 = now();

    // wait until all pings finish
    APP_WAIT(pings_in_flight == 0);

    time64_t t3 = now();
    
    results.latency_avg = ping_latency_in_us->get_average();
    results.latency_stddev = ping_latency_in_us->get_stddev();
    results.packets_per_sec = num_pings * (1e9 / (t3 - t1));
    results.bytes_per_sec = results.packets_per_sec * ping_size;
  }

protected:
  int num_pings, max_in_flight, ping_size;
};

class TestPingLong : public TestPings {
public:
  static const size_t LOCAL_BUFFER_SIZE = 256 << 20;

  TestPingLong(int _num_pings, int _max_in_flight, int _ping_size, bool _async_msg, bool _use_registered_mem)
    : TestPings(_num_pings, _max_in_flight, _ping_size), async_msg(_async_msg), use_registered_mem(_use_registered_mem)
  {
    sprintf(name, "pinglong,%d,%d,%d,%c,%c",
	    num_pings, max_in_flight, ping_size, async_msg ? 'y' : 'n', use_registered_mem ? 'y' : 'n');
    local_buffer = new char[LOCAL_BUFFER_SIZE];
  }

  virtual ~TestPingLong(void)
  {
    delete[] local_buffer;
  }

  static inline size_t random_offset(size_t block_size, size_t total_size, size_t align)
  {
    size_t last_block = (total_size - block_size + 1) / align;
    return (random() % (last_block+1)) * align;
  }
  
  static inline void *random_pointer(size_t block_size, int node_id, size_t align = 4096)
  {
    char *base = (char *)(segments[node_id].addr);
    return base + random_offset(block_size, segments[node_id].size, align);
  }

  virtual void send_ping(int src_node, int dst_node)
  {
    void *local_ptr = (use_registered_mem ?
		         random_pointer(ping_size, gasnet_mynode()) :
		         (local_buffer + random_offset(ping_size, LOCAL_BUFFER_SIZE, 4096)));
    void *remote_ptr = random_pointer(ping_size, dst_node);
    time64_t mytime = now();
    if(async_msg)
      CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestLongAsync2, (dst_node, MSGID_PINGLONG_REQUEST, local_ptr, ping_size, remote_ptr, 
							     UNPACK_TIME64(mytime))) );
    else
      CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestLong2, (dst_node, MSGID_PINGLONG_REQUEST, local_ptr, ping_size, remote_ptr, 
							UNPACK_TIME64(mytime))) );
  }

protected:
  bool async_msg, use_registered_mem;
  char *local_buffer;
};

class TestPingShort : public TestPings {
public:
  TestPingShort(int _num_pings, int _max_in_flight, int _ping_size)
    : TestPings(_num_pings, _max_in_flight, _ping_size)
  {
    sprintf(name, "pingshort,%d,%d,%d",
	    num_pings, max_in_flight, ping_size);
  }

  virtual void send_ping(int src_node, int dst_node)
  {
    time64_t mytime = now();
    switch(ping_size) {
    case 8:
      {
	CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestShort2, (dst_node, MSGID_PINGSH_REQUEST, UNPACK_TIME64(mytime))) );
	break;
      }
      
    case 16:
      {
	CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestShort4, (dst_node, MSGID_PINGSH_REQUEST, UNPACK_TIME64(mytime), 0, 0)) );
	break;
      }
      
    case 32:
      {
	CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestShort8, (dst_node, MSGID_PINGSH_REQUEST, UNPACK_TIME64(mytime), 0, 0, 0, 0, 0, 0)) );
	break;
      }
      
    case 48:
      {
	CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestShort12, (dst_node, MSGID_PINGSH_REQUEST, UNPACK_TIME64(mytime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)) );
	break;
      }
      
    case 52:
      {
	CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestShort13, (dst_node, MSGID_PINGSH_REQUEST, UNPACK_TIME64(mytime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)) );
	break;
      }
      
    case 56:
      {
	CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestShort14, (dst_node, MSGID_PINGSH_REQUEST, UNPACK_TIME64(mytime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)) );
	break;
      }
      
    case 60:
      {
	CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestShort15, (dst_node, MSGID_PINGSH_REQUEST, UNPACK_TIME64(mytime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)) );
	break;
      }
      
    case 64:
      {
	CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestShort16, (dst_node, MSGID_PINGSH_REQUEST, UNPACK_TIME64(mytime), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)) );
	break;
      }
      
    default:
      assert(0);
    }
  }
};

class TestPingMedium : public TestPings {
public:
  TestPingMedium(int _num_pings, int _max_in_flight, int _ping_size)
    : TestPings(_num_pings, _max_in_flight, _ping_size)
  {
    sprintf(name, "pingmedium,%d,%d,%d",
	    num_pings, max_in_flight, ping_size);
  }

  virtual void send_ping(int src_node, int dst_node)
  {
    time64_t mytime = now();
    CHECK_GASNET( EXPAND_ARGS(gasnet_AMRequestMedium2, (dst_node, MSGID_PINGMED_REQUEST, local_buffer, ping_size, UNPACK_TIME64(mytime))) );
  }

protected:
  char local_buffer[4096];
};

class TestRDMA : public TestPattern {
public:
  static const size_t LOCAL_BUFFER_SIZE = 256 << 20;

  TestRDMA(int _num_rdmas, int _max_in_flight, int _rdma_size)
    : num_rdmas(_num_rdmas), max_in_flight(_max_in_flight), rdma_size(_rdma_size)
  {
    local_buffer = new char[LOCAL_BUFFER_SIZE];
  }

  virtual ~TestRDMA(void)
  {
    delete[] local_buffer;
  }

  static inline size_t random_offset(size_t block_size, size_t total_size, size_t align)
  {
    size_t last_block = (total_size - block_size + 1) / align;
    return (random() % (last_block+1)) * align;
  }
  
  static inline void *random_pointer(size_t block_size, int node_id, size_t align = 4096)
  {
    char *base = (char *)(segments[node_id].addr);
    return base + random_offset(block_size, segments[node_id].size, align);
  }

  virtual std::vector<gasnet_handle_t> *do_one_rdma(int src_node, int dst_node) = 0;

  static void wait_for_dmas(std::deque<std::vector<gasnet_handle_t> *>& in_flight, size_t max_left)
  { 
    while(in_flight.size() > max_left) {
      std::vector<gasnet_handle_t> *handles = in_flight.front();
      if(config.app_polls_gasnet) {
	// block until all handles for this dma are done
	gasnet_wait_syncnb_all(&(handles->front()), handles->size());
      } else {
	int ret = gasnet_try_syncnb_all(&(handles->front()), handles->size());
	if(ret == GASNET_ERR_NOT_READY) continue;
	assert(ret == GASNET_OK);
      }
      // if we get here, this dma is done and we can pop it from the list
      delete handles;
      in_flight.pop_front();
    }
  }

  virtual void prepare(int src_node, int dst_node) {}

  virtual void run_one(int src_node, int dst_node, TestResults& results)
  {
    std::deque<std::vector<gasnet_handle_t> *> in_flight;

    time64_t t1 = now();
    for(int i = 0; i < num_rdmas; i++) {
      // wait until enough have finished
      wait_for_dmas(in_flight, max_in_flight - 1);

      std::vector<gasnet_handle_t> *handles;
      if(dst_node >= 0)
	handles = do_one_rdma(src_node, dst_node);
      else
	handles = do_one_rdma(src_node, random() % gasnet_nodes());
      if(handles)
	in_flight.push_back(handles);
    }
    time64_t t2 = now();

    // wait until all dmas finish
    wait_for_dmas(in_flight, 0);

    time64_t t3 = now();
    
    results.latency_avg = 0; // TODO - what makes sense here?
    results.latency_stddev = 0;
    results.packets_per_sec = num_rdmas * (1e9 / (t3 - t1));
    results.bytes_per_sec = results.packets_per_sec * rdma_size;
  }

protected:
  int num_rdmas, max_in_flight, rdma_size;
  char *local_buffer;
};

class TestRDMAPut : public TestRDMA {
public:
  TestRDMAPut(int _num_rdmas, int _max_in_flight, int _rdma_size, int _num_pieces, bool _use_registered_mem)
    : TestRDMA(_num_rdmas, _max_in_flight, _rdma_size), 
      num_pieces(_num_pieces), use_registered_mem(_use_registered_mem)
  {
    sprintf(name, "put,%d,%d,%d,%d,%c",
	    num_rdmas, max_in_flight, rdma_size, num_pieces, use_registered_mem ? 'y' : 'n');
  }

  virtual std::vector<gasnet_handle_t> *do_one_rdma(int src_node, int dst_node)
  {
    int piece_size = rdma_size / num_pieces;
    for(int i = 0; i < num_pieces; i++) {
      void *local_ptr = (use_registered_mem ?
		           random_pointer(piece_size, gasnet_mynode()) :
		           (local_buffer + random_offset(piece_size, LOCAL_BUFFER_SIZE, 4096)));
      void *remote_ptr = random_pointer(piece_size, dst_node);
      gasnet_put(dst_node, remote_ptr, local_ptr, piece_size);
    }
    return 0;  // calls are blocking, so nothing for outer loop to synchronize
  }

protected:
  int num_pieces;
  bool use_registered_mem;
};

class TestRDMAPutNB : public TestRDMA {
public:
  TestRDMAPutNB(int _num_rdmas, int _max_in_flight, int _rdma_size, int _num_pieces, bool _use_registered_mem)
    : TestRDMA(_num_rdmas, _max_in_flight, _rdma_size), 
      num_pieces(_num_pieces), use_registered_mem(_use_registered_mem)
  {
    sprintf(name, "putnb,%d,%d,%d,%d,%c",
	    num_rdmas, max_in_flight, rdma_size, num_pieces, use_registered_mem ? 'y' : 'n');
  }

  virtual std::vector<gasnet_handle_t> *do_one_rdma(int src_node, int dst_node)
  {
    std::vector<gasnet_handle_t> *handles = new std::vector<gasnet_handle_t>(num_pieces);
    int piece_size = rdma_size / num_pieces;
    for(int i = 0; i < num_pieces; i++) {
      void *local_ptr = (use_registered_mem ?
		           random_pointer(piece_size, gasnet_mynode()) :
		           (local_buffer + random_offset(piece_size, LOCAL_BUFFER_SIZE, 4096)));
      void *remote_ptr = random_pointer(piece_size, dst_node);
      (*handles)[i] = gasnet_put_nb(dst_node, remote_ptr, local_ptr, piece_size);
    }
    return handles;
  }

protected:
  int num_pieces;
  bool use_registered_mem;
};

class TestRDMAPutNBI : public TestRDMA {
public:
  TestRDMAPutNBI(int _num_rdmas, int _max_in_flight, int _rdma_size, int _num_pieces, bool _use_registered_mem)
    : TestRDMA(_num_rdmas, _max_in_flight, _rdma_size), 
      num_pieces(_num_pieces), use_registered_mem(_use_registered_mem)
  {
    sprintf(name, "putnbi,%d,%d,%d,%d,%c",
	    num_rdmas, max_in_flight, rdma_size, num_pieces, use_registered_mem ? 'y' : 'n');
  }

  virtual std::vector<gasnet_handle_t> *do_one_rdma(int src_node, int dst_node)
  {
    int piece_size = rdma_size / num_pieces;
    for(int i = 0; i < num_pieces; i++) {
      void *local_ptr = (use_registered_mem ?
		           random_pointer(piece_size, gasnet_mynode()) :
		           (local_buffer + random_offset(piece_size, LOCAL_BUFFER_SIZE, 4096)));
      void *remote_ptr = random_pointer(piece_size, dst_node);
      gasnet_put_nbi(dst_node, remote_ptr, local_ptr, piece_size);
    }
    // externally we appear to be blocking
    gasnet_wait_syncnbi_puts();
    return 0;
  }

protected:
  int num_pieces;
  bool use_registered_mem;
};

class TestRDMAPutNBA : public TestRDMA {
public:
  TestRDMAPutNBA(int _num_rdmas, int _max_in_flight, int _rdma_size, int _num_pieces, bool _use_registered_mem)
    : TestRDMA(_num_rdmas, _max_in_flight, _rdma_size), 
      num_pieces(_num_pieces), use_registered_mem(_use_registered_mem)
  {
    sprintf(name, "putnba,%d,%d,%d,%d,%c",
	    num_rdmas, max_in_flight, rdma_size, num_pieces, use_registered_mem ? 'y' : 'n');
  }

  virtual std::vector<gasnet_handle_t> *do_one_rdma(int src_node, int dst_node)
  {
    int piece_size = rdma_size / num_pieces;
    gasnet_begin_nbi_accessregion();
    for(int i = 0; i < num_pieces; i++) {
      void *local_ptr = (use_registered_mem ?
		           random_pointer(piece_size, gasnet_mynode()) :
		           (local_buffer + random_offset(piece_size, LOCAL_BUFFER_SIZE, 4096)));
      void *remote_ptr = random_pointer(piece_size, dst_node);
      gasnet_put_nbi(dst_node, remote_ptr, local_ptr, piece_size);
    }
    std::vector<gasnet_handle_t> *handles = new std::vector<gasnet_handle_t>(1);
    (*handles)[0] = gasnet_end_nbi_accessregion();
    return handles;
  }

protected:
  int num_pieces;
  bool use_registered_mem;
};

class TestRDMAGet : public TestRDMA {
public:
  TestRDMAGet(int _num_rdmas, int _max_in_flight, int _rdma_size, int _num_pieces, bool _use_registered_mem)
    : TestRDMA(_num_rdmas, _max_in_flight, _rdma_size), 
      num_pieces(_num_pieces), use_registered_mem(_use_registered_mem)
  {
    sprintf(name, "get,%d,%d,%d,%d,%c",
	    num_rdmas, max_in_flight, rdma_size, num_pieces, use_registered_mem ? 'y' : 'n');
  }

  virtual std::vector<gasnet_handle_t> *do_one_rdma(int src_node, int dst_node)
  {
    int piece_size = rdma_size / num_pieces;
    for(int i = 0; i < num_pieces; i++) {
      void *local_ptr = (use_registered_mem ?
		           random_pointer(piece_size, gasnet_mynode()) :
		           (local_buffer + random_offset(piece_size, LOCAL_BUFFER_SIZE, 4096)));
      void *remote_ptr = random_pointer(piece_size, dst_node);
      gasnet_get(local_ptr, dst_node, remote_ptr, piece_size);
    }
    return 0;  // calls are blocking, so nothing for outer loop to synchronize
  }

protected:
  int num_pieces;
  bool use_registered_mem;
};

class TestRDMAGetNB : public TestRDMA {
public:
  TestRDMAGetNB(int _num_rdmas, int _max_in_flight, int _rdma_size, int _num_pieces, bool _use_registered_mem)
    : TestRDMA(_num_rdmas, _max_in_flight, _rdma_size), 
      num_pieces(_num_pieces), use_registered_mem(_use_registered_mem)
  {
    sprintf(name, "getnb,%d,%d,%d,%d,%c",
	    num_rdmas, max_in_flight, rdma_size, num_pieces, use_registered_mem ? 'y' : 'n');
  }

  virtual std::vector<gasnet_handle_t> *do_one_rdma(int src_node, int dst_node)
  {
    std::vector<gasnet_handle_t> *handles = new std::vector<gasnet_handle_t>(num_pieces);
    int piece_size = rdma_size / num_pieces;
    for(int i = 0; i < num_pieces; i++) {
      void *local_ptr = (use_registered_mem ?
		           random_pointer(piece_size, gasnet_mynode()) :
		           (local_buffer + random_offset(piece_size, LOCAL_BUFFER_SIZE, 4096)));
      void *remote_ptr = random_pointer(piece_size, dst_node);
      (*handles)[i] = gasnet_get_nb(local_ptr, dst_node, remote_ptr, piece_size);
    }
    return handles;
  }

protected:
  int num_pieces;
  bool use_registered_mem;
};

class TestRDMAGetNBI : public TestRDMA {
public:
  TestRDMAGetNBI(int _num_rdmas, int _max_in_flight, int _rdma_size, int _num_pieces, bool _use_registered_mem)
    : TestRDMA(_num_rdmas, _max_in_flight, _rdma_size), 
      num_pieces(_num_pieces), use_registered_mem(_use_registered_mem)
  {
    sprintf(name, "getnbi,%d,%d,%d,%d,%c",
	    num_rdmas, max_in_flight, rdma_size, num_pieces, use_registered_mem ? 'y' : 'n');
  }

  virtual std::vector<gasnet_handle_t> *do_one_rdma(int src_node, int dst_node)
  {
    int piece_size = rdma_size / num_pieces;
    for(int i = 0; i < num_pieces; i++) {
      void *local_ptr = (use_registered_mem ?
		           random_pointer(piece_size, gasnet_mynode()) :
		           (local_buffer + random_offset(piece_size, LOCAL_BUFFER_SIZE, 4096)));
      void *remote_ptr = random_pointer(piece_size, dst_node);
      gasnet_get_nbi(local_ptr, dst_node, remote_ptr, piece_size);
    }
    // externally we appear to be blocking
    gasnet_wait_syncnbi_gets();
    return 0;
  }

protected:
  int num_pieces;
  bool use_registered_mem;
};

class TestRDMAGetNBA : public TestRDMA {
public:
  TestRDMAGetNBA(int _num_rdmas, int _max_in_flight, int _rdma_size, int _num_pieces, bool _use_registered_mem)
    : TestRDMA(_num_rdmas, _max_in_flight, _rdma_size), 
      num_pieces(_num_pieces), use_registered_mem(_use_registered_mem)
  {
    sprintf(name, "getnba,%d,%d,%d,%d,%c",
	    num_rdmas, max_in_flight, rdma_size, num_pieces, use_registered_mem ? 'y' : 'n');
  }

  virtual std::vector<gasnet_handle_t> *do_one_rdma(int src_node, int dst_node)
  {
    int piece_size = rdma_size / num_pieces;
    gasnet_begin_nbi_accessregion();
    for(int i = 0; i < num_pieces; i++) {
      void *local_ptr = (use_registered_mem ?
		           random_pointer(piece_size, gasnet_mynode()) :
		           (local_buffer + random_offset(piece_size, LOCAL_BUFFER_SIZE, 4096)));
      void *remote_ptr = random_pointer(piece_size, dst_node);
      gasnet_get_nbi(local_ptr, dst_node, remote_ptr, piece_size);
    }
    std::vector<gasnet_handle_t> *handles = new std::vector<gasnet_handle_t>(1);
    (*handles)[0] = gasnet_end_nbi_accessregion();
    return handles;
  }

protected:
  int num_pieces;
  bool use_registered_mem;
};

int main(int argc, char *argv[])
{
  CHECK_GASNET( gasnet_init(&argc, &argv) );

  config.parse_args(argc, argv);

  printf("init done: node %d out of %d (%ld local, %ld global)\n", gasnet_mynode(), gasnet_nodes(), gasnet_getMaxLocalSegmentSize(), gasnet_getMaxGlobalSegmentSize());

  CHECK_GASNET( gasnet_attach(handlers, sizeof(handlers)/sizeof(handlers[0]), (config.segsize_in_mb << 20), 0) );

  char hostname[80];
  gethostname(hostname, 80);
  printf("(%d) attached (%s)\n", gasnet_mynode(), hostname);
  printf("(%d) gasnet_AMMaxArgs = %zd\n", gasnet_mynode(), gasnet_AMMaxArgs());
  printf("(%d) gasnet_AMMaxMedium = %zd\n", gasnet_mynode(), gasnet_AMMaxMedium());
  printf("(%d) gasnet_AMMaxLongRequest = %zd\n", gasnet_mynode(), gasnet_AMMaxLongRequest());

  gasnet_seginfo_t segs[4];

  segments = new gasnet_seginfo_t[gasnet_nodes()];
  CHECK_GASNET( gasnet_getSegmentInfo(segments, gasnet_nodes()) );

  ping_latency_in_us = new Stat<double>;

  start_polling_threads(config.num_polling_threads);

  FILE *outf = 0;
  if(config.outfile[0]) {
    char filename[512];
    sprintf(filename, "%s.%d", config.outfile, gasnet_mynode());
    outf = fopen(filename, "w");
    assert(outf);
  }

  std::vector<int> short_sizes;
  short_sizes.push_back(8);
  short_sizes.push_back(16);
  short_sizes.push_back(32);
  short_sizes.push_back(48);
  short_sizes.push_back(52);
  short_sizes.push_back(56);
  short_sizes.push_back(60);
  short_sizes.push_back(64);

  for(std::vector<int>::const_iterator it = short_sizes.begin(); it != short_sizes.end(); it++) {
    int short_size = *it;
    TestPingShort(config.base_count * 100, 1, short_size).run(outf);
    TestPingShort(config.base_count * 1000, config.base_count * 1000, short_size).run(outf);
    if(config.quick_mode) break;
  }

  std::vector<int> medium_sizes;
  for(int i = 256; i <= 4096; i *= 2)
    if(i <= gasnet_AMMaxMedium())
      medium_sizes.push_back(i);
  if(gasnet_AMMaxMedium() < 4096)
    medium_sizes.push_back(gasnet_AMMaxMedium());

  for(std::vector<int>::const_iterator it = medium_sizes.begin(); it != medium_sizes.end(); it++) {
    int medium_size = *it;
    TestPingMedium(config.base_count * 1000, 1, medium_size).run(outf);
    TestPingMedium(config.base_count * 1000, config.base_count * 1000, medium_size).run(outf);
    if(config.quick_mode) break;
  }

  std::vector<int> long_sizes;
  if(gasnet_AMMaxMedium() < 1024) long_sizes.push_back(1024);
  if(gasnet_AMMaxMedium() < 2048) long_sizes.push_back(2048);
  for(int i = 4 << 10; i <= (16 << 20); i *= 16)
    if(i <= gasnet_AMMaxLongRequest())
      long_sizes.push_back(i);
  if(gasnet_AMMaxLongRequest() < (16 << 20))
    long_sizes.push_back(gasnet_AMMaxLongRequest());

  for(std::vector<int>::const_iterator it = long_sizes.begin(); it != long_sizes.end(); it++) {
    int long_size = *it;
  
    for(int async_msg = 0; async_msg < 2; async_msg++)
      for(int use_reg = 0; use_reg < 2; use_reg++) {
	int num_pings = config.base_count * ((long_size > (1 << 20)) ? 1 : 10);
	TestPingLong(num_pings, 1, long_size, async_msg, use_reg).run(outf);
	TestPingLong(num_pings, num_pings, long_size, async_msg, use_reg).run(outf);
      }

    if(config.quick_mode) break;
  }

  std::vector<int> rdma_sizes;
  rdma_sizes.push_back(4 << 10);
  rdma_sizes.push_back(64 << 10);
  rdma_sizes.push_back(1 << 20);
  rdma_sizes.push_back(16 << 20);
  for(std::vector<int>::const_iterator it = rdma_sizes.begin(); it != rdma_sizes.end(); it++) {
    int rdma_size = *it;
    for(int num_pieces = 1; num_pieces <= 64; num_pieces *= 64)
      for(int allow_multiple = 0; allow_multiple < 2; allow_multiple++)
        for(int use_registered = 0; use_registered < 2; use_registered++) {
          int num_rdmas = config.base_count * ((rdma_size > (1 << 20)) ? 1 : 10);
          TestRDMAPut(num_rdmas, (allow_multiple ? num_rdmas : 1), rdma_size, num_pieces, use_registered).run(outf);
          TestRDMAPutNB(num_rdmas, (allow_multiple ? num_rdmas : 1), rdma_size, num_pieces, use_registered).run(outf);
          TestRDMAPutNBI(num_rdmas, (allow_multiple ? num_rdmas : 1), rdma_size, num_pieces, use_registered).run(outf);
          TestRDMAPutNBA(num_rdmas, (allow_multiple ? num_rdmas : 1), rdma_size, num_pieces, use_registered).run(outf);
          TestRDMAGet(num_rdmas, (allow_multiple ? num_rdmas : 1), rdma_size, num_pieces, use_registered).run(outf);
          TestRDMAGetNB(num_rdmas, (allow_multiple ? num_rdmas : 1), rdma_size, num_pieces, use_registered).run(outf);
          TestRDMAGetNBI(num_rdmas, (allow_multiple ? num_rdmas : 1), rdma_size, num_pieces, use_registered).run(outf);
          TestRDMAGetNBA(num_rdmas, (allow_multiple ? num_rdmas : 1), rdma_size, num_pieces, use_registered).run(outf);
	}
    if(config.quick_mode) break;
  }

  if(outf)
    fclose(outf);

  stop_polling_threads();

  gasnet_exit(0);
}
