#include <stdio.h>
#include <stdlib.h>

#include "aff_gpu_kernels.h"

#define CHECK_CUDA(x) do { \
  cudaError_t res = (x); \
  if(res != cudaSuccess) { \
  fprintf(stderr, "CUDA error: %s = %d (%s)\n", #x, res, cudaGetErrorString(res)); \
  exit(1); \
  } \
  } while(0)

__global__ void gpu_chain_walk_kernel(const void *buffer, size_t *result)
{
  size_t count = 0;
  while(buffer) {
    //printf("read %p\n", buffer);
    buffer = *(const void **)buffer;
    count++;
  }
  *result = count;
}

#define CHECK_CUDA(x) do { \
  cudaError_t res = (x); \
  if(res != cudaSuccess) { \
  fprintf(stderr, "CUDA error: %s = %d (%s)\n", #x, res, cudaGetErrorString(res)); \
  exit(1); \
  } \
  } while(0)

double gpu_chain_walk(const void *buffer)
{
  cudaStream_t stream;
  cudaEvent_t ev_start, ev_end;
  size_t *result;
  size_t count;

  CHECK_CUDA( cudaStreamCreate(&stream) );
  CHECK_CUDA( cudaEventCreate(&ev_start) );
  CHECK_CUDA( cudaEventCreate(&ev_end) );
  CHECK_CUDA( cudaMalloc((void **)&result, sizeof(size_t)) );

  CHECK_CUDA( cudaEventRecord(ev_start, stream) );
  gpu_chain_walk_kernel<<< 1, 1 >>>(buffer, result);
  CHECK_CUDA( cudaEventRecord(ev_end, stream) );

  CHECK_CUDA( cudaStreamSynchronize(stream) );
  CHECK_CUDA( cudaMemcpy(&count, result, sizeof(size_t), cudaMemcpyDeviceToHost) );
  CHECK_CUDA( cudaFree(result) );

  float ms;
  CHECK_CUDA( cudaEventElapsedTime(&ms, ev_start, ev_end) );

  CHECK_CUDA( cudaEventDestroy(ev_start) );
  CHECK_CUDA( cudaEventDestroy(ev_end) );
  CHECK_CUDA( cudaStreamDestroy(stream) );

  //printf("gpu chain walk: %zd, %g, %g\n", count, ms, ms / count);
  return (1e6 * ms / count);
}

template <typename T>
__global__ void gpu_read_mem_kernel(const void *buffer, size_t buffer_size, void *target)
{
  const T* buffer_start = (const T *)buffer;
  const T* buffer_end = (const T *)(((const char *)buffer) + buffer_size);

  // threads within a CTA will access sequential locations per access
  buffer_start += threadIdx.x;

  // CTAs are finely interleaved after that
  buffer_start += blockIdx.x * blockDim.x;

  size_t fetch_stride = gridDim.x * blockDim.x;
  size_t fetch_stride_x4 = 4 * fetch_stride;
  size_t fetch_stride_x4_in_bytes = fetch_stride_x4 * sizeof(T);

  const T *p1 = buffer_start;
  const T *p2 = p1 + fetch_stride;
  const T *p3 = p2 + fetch_stride;
  const T *p4 = p3 + fetch_stride;

  T accum;
  while(p4 < buffer_end) {
    // 4 fetches in parallel
    T v1 = *p1;
    T v2 = *p2;
    T v3 = *p3;
    T v4 = *p4;

    p1 = (const T *)(((const char *) p1) + fetch_stride_x4_in_bytes);
    p2 = (const T *)(((const char *) p2) + fetch_stride_x4_in_bytes);
    p3 = (const T *)(((const char *) p3) + fetch_stride_x4_in_bytes);
    p4 = (const T *)(((const char *) p4) + fetch_stride_x4_in_bytes);

    accum += v1;
    accum += v2;
    accum += v3;
    accum += v4;
  }

  while(p1 < buffer_end) {
    accum += *p1;
    p1 += fetch_stride;
  }

  if(target)
    *(T *)target = accum;
}

double gpu_read_mem(int gpu_id, const void *buffer, size_t len)
{
  cudaStream_t stream;
  cudaEvent_t ev_start, ev_end;
  cudaDeviceProp dev_props;

  CHECK_CUDA( cudaGetDeviceProperties(&dev_props, gpu_id) );

  // printf("num SMs = %d\n", dev_props.multiProcessorCount);
  // printf("core clock = %d\n", dev_props.clockRate);
  // printf("memory clock = %d\n", dev_props.memoryClockRate);
  // printf("memory bus width = %d\n", dev_props.memoryBusWidth);
  // printf("shared mem/block = %d\n", dev_props.sharedMemPerBlock);

  CHECK_CUDA( cudaStreamCreate(&stream) );
  CHECK_CUDA( cudaEventCreate(&ev_start) );
  CHECK_CUDA( cudaEventCreate(&ev_end) );

  CHECK_CUDA( cudaEventRecord(ev_start, stream) );
  dim3 grid_size(2 * dev_props.multiProcessorCount, 1, 1);
  dim3 blk_size(256, 1, 1);
  gpu_read_mem_kernel<float><<< grid_size, blk_size, dev_props.sharedMemPerBlock / 2, stream >>>(buffer, len, (void *)0);
  CHECK_CUDA( cudaEventRecord(ev_end, stream) );

  CHECK_CUDA( cudaStreamSynchronize(stream) );

  float ms;
  CHECK_CUDA( cudaEventElapsedTime(&ms, ev_start, ev_end) );

  CHECK_CUDA( cudaEventDestroy(ev_start) );
  CHECK_CUDA( cudaEventDestroy(ev_end) );
  CHECK_CUDA( cudaStreamDestroy(stream) );

  //printf("gpu read: %zd, %g, %g\n", len, ms, 1e3 * len / ms);
  return (1e-6 * len / ms);
}

template <typename T>
__global__ void gpu_write_mem_kernel(void *buffer, size_t buffer_size, const void *target)
{
  T* buffer_start = (T *)buffer;
  T* buffer_end = (T *)(((char *)buffer) + buffer_size);

  // threads within a CTA will access sequential locations per access
  buffer_start += threadIdx.x;

  // CTAs are finely interleaved after that
  buffer_start += blockIdx.x * blockDim.x;

  size_t fetch_stride = gridDim.x * blockDim.x;

  T *p1 = buffer_start;
  
  T val;
  if(target)
    val = *(const T *)target;

  while(p1 < buffer_end) {
    *p1 = val;
    p1 += fetch_stride;
  }
}

double gpu_write_mem(int gpu_id, void *buffer, size_t len)
{
  cudaStream_t stream;
  cudaEvent_t ev_start, ev_end;
  cudaDeviceProp dev_props;

  CHECK_CUDA( cudaGetDeviceProperties(&dev_props, gpu_id) );

  // printf("num SMs = %d\n", dev_props.multiProcessorCount);
  // printf("core clock = %d\n", dev_props.clockRate);
  // printf("memory clock = %d\n", dev_props.memoryClockRate);
  // printf("memory bus width = %d\n", dev_props.memoryBusWidth);

  CHECK_CUDA( cudaStreamCreate(&stream) );
  CHECK_CUDA( cudaEventCreate(&ev_start) );
  CHECK_CUDA( cudaEventCreate(&ev_end) );

  CHECK_CUDA( cudaEventRecord(ev_start, stream) );
  dim3 grid_size(2 * dev_props.multiProcessorCount, 1, 1);
  dim3 blk_size(256, 1, 1);
  gpu_write_mem_kernel<float><<< grid_size, blk_size, dev_props.sharedMemPerBlock / 2, stream >>>(buffer, len, (void *)0);
  CHECK_CUDA( cudaEventRecord(ev_end, stream) );

  CHECK_CUDA( cudaStreamSynchronize(stream) );
  CHECK_CUDA( cudaDeviceSynchronize() );

  float ms;
  CHECK_CUDA( cudaEventElapsedTime(&ms, ev_start, ev_end) );

  CHECK_CUDA( cudaEventDestroy(ev_start) );
  CHECK_CUDA( cudaEventDestroy(ev_end) );
  CHECK_CUDA( cudaStreamDestroy(stream) );

  //printf("gpu write: %zd, %g, %g\n", len, ms, 1e3 * len / ms);
  return (1e-6 * len / ms);
}
