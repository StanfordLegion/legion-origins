#ifndef AFF_GPU_KERNELS_H
#define AFF_GPU_KERNELS_H

#ifdef __cplusplus
extern "C" {
#endif

double gpu_chain_walk(const void *buffer);
double gpu_read_mem(int gpu_id, const void *buffer, size_t len);
double gpu_write_mem(int gpu_id, void *buffer, size_t len);

#ifdef __cplusplus
}
#endif

#endif
