// subset of libnuma used by Legion

#ifndef NUMALITE_H
#define NUMALITE_H

#ifndef KEEP_ORIGINAL_NUMANAMES

#define get_mempolicy get_mempolicy_lite
#define mbind         mbind_lite
#define set_mempolicy set_mempolicy_lite

#define numa_available numa_available_lite
#define numa_bitmask_isbitset numa_bitmask_isbitset_lite
#define numa_bitmask_nbytes numa_bitmask_nbytes_lite
#define numa_node_of_cpu numa_node_of_cpu_lite
#define numa_all_cpus_ptr numa_all_cpus_ptr_lite

#endif

#ifdef __cplusplus
extern "C" {
#endif

struct bitmask {
        unsigned long size; /* number of bits in the map */
        unsigned long *maskp;
};

extern int numa_available(void);

extern int numa_bitmask_isbitset(const struct bitmask *, unsigned int);
extern unsigned int numa_bitmask_nbytes(struct bitmask *);

extern int numa_node_of_cpu(int cpu);

extern struct bitmask *numa_all_cpus_ptr;

/* System calls */
extern long get_mempolicy(int *policy, const unsigned long *nmask,
			  unsigned long maxnode, void *addr, int flags);

extern long mbind(void *start, unsigned long len, int mode,
		  const unsigned long *nmask, unsigned long maxnode, unsigned flags);

extern long set_mempolicy(int mode, const unsigned long *nmask,
			  unsigned long maxnode);

#ifndef NO_LINUX_MEMPOLICY
#include <linux/mempolicy.h>
#else
/* Policies */
#define MPOL_DEFAULT     0
#define MPOL_PREFERRED    1
#define MPOL_BIND        2
#define MPOL_INTERLEAVE  3

#define MPOL_MAX MPOL_INTERLEAVE

/* Flags for get_mem_policy */
#define MPOL_F_NODE    (1<<0)   /* return next il node or node of address */
                                /* Warning: MPOL_F_NODE is unsupported and
                                   subject to change. Don't use. */
#define MPOL_F_ADDR     (1<<1)  /* look up vma using address */
#define MPOL_F_MEMS_ALLOWED (1<<2) /* query nodes allowed in cpuset */

/* Flags for mbind */
#define MPOL_MF_STRICT  (1<<0)  /* Verify existing pages in the mapping */
#define MPOL_MF_MOVE    (1<<1)  /* Move pages owned by this process to conform to mapping */
#define MPOL_MF_MOVE_ALL (1<<2) /* Move every page to conform to mapping */
#endif

#ifdef __cplusplus
}
#endif

#endif

