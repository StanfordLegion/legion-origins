// subset of libnuma used by Legion

#include "numalite.h"

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sched.h>

long get_mempolicy(int *policy, const unsigned long *nmask,
		   unsigned long maxnode, void *addr, int flags)
{
  return syscall(__NR_get_mempolicy, policy, nmask,
		 maxnode, addr, flags);
}

long mbind(void *start, unsigned long len, int mode,
	   const unsigned long *nmask, unsigned long maxnode, unsigned flags)
{
  return syscall(__NR_mbind, (long)start, len, mode, (long)nmask,
		 maxnode, flags);
}

long set_mempolicy(int mode, const unsigned long *nmask,
		   unsigned long maxnode)
{
  long i;
  i = syscall(__NR_set_mempolicy, mode, nmask, maxnode);
  return i;
}

struct bitmask *numa_all_cpus_ptr = 0;

int numa_available(void)
{
  FILE *f;
  char line[256];
  char *pos;
  int i;

  // try to get allowed CPUs from sched_getaffinity()
  if(!numa_all_cpus_ptr) {
    cpu_set_t all_cpus;
    int ret = sched_getaffinity(0, sizeof(all_cpus), &all_cpus);
    printf("result = %d\n", ret);
    if(ret == 0) {
      numa_all_cpus_ptr = (struct bitmask *)malloc(sizeof(struct bitmask));
      numa_all_cpus_ptr->size = 8 * sizeof(all_cpus);
      numa_all_cpus_ptr->maskp = (unsigned long *)malloc(sizeof(all_cpus));
      memcpy(numa_all_cpus_ptr->maskp, &all_cpus, sizeof(all_cpus));
    }
  }

  // plan B: init all cpus from /proc/self/status
  if(!numa_all_cpus_ptr) {
    f = fopen("/proc/self/status", "r");
    if(f) {
      // read until we get a line that starts with "Cpus_allowed:"
      while(fgets(line, 255, f)) {
        if(strncmp(line, "Cpus_allowed:", 13)) continue;

        numa_all_cpus_ptr = (struct bitmask *)malloc(sizeof(struct bitmask));
        numa_all_cpus_ptr->size = 32;
        pos = line+13;
        while(*pos) {
  	  if(*pos++ == ',')
  	    numa_all_cpus_ptr->size += 32; // every comma is another long
        }

        i = numa_all_cpus_ptr->size / 32;
        numa_all_cpus_ptr->maskp = (unsigned long *)malloc(i * sizeof(long));
        pos = strtok(line + 13, ",");
        while(pos) {
	  numa_all_cpus_ptr->maskp[--i] = strtoul(pos, 0, 16);
	  pos = strtok(0, ",");
        }
        if(i != 0) {
  	  printf("oops: i = %d\n", i);
        }
        break;
      }

      fclose(f);
    }
  }

  // if we still don't have data, we're toast
  if(!numa_all_cpus_ptr)
    return -1;

  return 0;
}

#define BITS_PER_LONG (8 * sizeof(long))

int numa_bitmask_isbitset(const struct bitmask *bmp, unsigned int i)
{
  if(i < bmp->size)
    return ((bmp->maskp[i / BITS_PER_LONG] & (1UL << (i % BITS_PER_LONG))) != 0);
  else
    return 0;
}

unsigned int numa_bitmask_nbytes(struct bitmask *bmp)
{
  unsigned num_longs = 1 + ((bmp->size - 1) / (8 * sizeof(long)));
  return num_longs * sizeof(long);
}

extern int numa_node_of_cpu(int cpu)
{
  // get this information from /sys
  DIR *d = opendir("/sys/devices/system/node");
  if(!d) {
    printf("no /sys/devices/system/node ?\n");
    return -1;
  }
  for(struct dirent *e = readdir(d); e; e = readdir(d)) {
    if(!strncmp(e->d_name, "node", 4)) {
      int n = atoi(e->d_name+4);
      char cpuname[80];
      sprintf(cpuname, "/sys/devices/system/node/%s/cpu%d", e->d_name, cpu);
      if(access(cpuname, F_OK) == 0) {
	//printf("found %s\n", cpuname);
        return n;
      }
    }
  }
  printf("couldn't find cpu %d\n", cpu);
  return -1;
}

