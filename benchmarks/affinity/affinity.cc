#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <time.h>
#include <pthread.h>

#include <assert.h>
#include <time.h>
#include <errno.h>
#include <vector>

#ifdef USE_HUGETLBFS
extern "C" {
#include <hugetlbfs.h>
};
#endif

#ifdef USE_NUMA
extern "C" {
extern int pthread_attr_setaffinity_np (pthread_attr_t *__attr,
                                        size_t __cpusetsize,
                                        __const cpu_set_t *__cpuset);
};

#ifdef HAVE_NUMA_H
#include <numa.h>
#include <numaif.h>
#else
#include "numalite.h"
#endif

#include <map>
#endif

#ifdef USE_CUDA
#ifndef __GNUC__
// cuda header files get confused when not using gcc
#define __align__(x)
#define CUDARTAPI
#define __location__(x)
#endif
#include <cuda_runtime.h>
#include "aff_gpu_kernels.h"
#endif

#ifdef USE_GASNET
#ifdef GASNET_ON_MPI
#include <mpi.h>
#endif

#define GASNET_PAR
#include <gasnet.h>
#endif

#define CHECK_LIBC(x) do { \
  int ret = (x); \
  if(ret < 0) { \
  fprintf(stderr, "LIBC: %s = %d (%s)\n", #x, errno, strerror(errno)); \
  exit(1); \
  } \
  } while(0)

static double cpu_time(void)
{
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return((1.0 * ts.tv_sec) + (1e-9 * ts.tv_nsec));
}

void init_random_chain(void *buffer, const void *target_base, size_t block_size, size_t num_blocks, size_t num_subchains = 0)
{
  void **buffer_typed = (void **)buffer;
  const void **target_buffer_typed = (const void **)target_base;
  size_t size_typed = block_size / sizeof(void *);

  if(num_subchains == 0)
    num_subchains = size_typed;

  size_t *block_idxs = (size_t *)malloc(num_blocks * sizeof(size_t));

  for(size_t ofs = 0; ofs < num_subchains; ofs++) {
    for(size_t i = 0; i < num_blocks; i++) 
      block_idxs[i] = i;

    // randomize list, except for first element
    for(size_t i = 1; i < num_blocks - 1; i++) {
      size_t rand_idx = i + 1 + (random() % (num_blocks - i - 1));
      size_t x = block_idxs[i];
      block_idxs[i] = block_idxs[rand_idx];
      block_idxs[rand_idx] = x;
    }

    // now hook up chain
    void **p = buffer_typed + ofs;
    for(size_t i = 0; i < num_blocks - 1; i++) {
      const void **target_nxt = target_buffer_typed + ofs + (size_typed * block_idxs[i + 1]);
      void **nxt = buffer_typed + ofs + (size_typed * block_idxs[i + 1]);
      //printf("%zd, %zd, %zd - %p, %p\n", ofs, i, block_idxs[i + 1], p, nxt);
      *p = (void *)target_nxt;
      p = nxt;
    }

    if(ofs < (num_subchains - 1)) {
      // link last pointer to beginning of next subchain
      *p = (void *)(target_buffer_typed + ofs + 1);
    } else {
      // end of list
      *p = 0;
    }
  }

  free(block_idxs);
}

double cpu_chain_walk(const void *buffer)
{
  double t1 = cpu_time();
  size_t count = 0;
  //const void *orig_buffer = buffer;
  while(buffer) {
    //printf("%p, %d\n", buffer, ((char *)buffer)-((char *)orig_buffer));
    count++;
    buffer = *(const void **)buffer;
  }
  double t2 = cpu_time();
  //printf("cpu chain walk: %zd, %g, %g\n", count, t2 - t1, (t2 - t1) / count);
  return (1e9 * (t2 - t1) / count);
}

double cpu_read_mem(const void *buffer, size_t len, bool nontemp = false, size_t prefetch = 0)
{
  size_t num_cache_lines = len / 64;

  long long result;

  double t1 = cpu_time();
  asm volatile (
"xorpd %%xmm0, %%xmm0;"
"xorpd %%xmm1, %%xmm1;"
"xorpd %%xmm2, %%xmm2;"
"xorpd %%xmm3, %%xmm3;"
"movq %1, %%rax;"
"movq %2, %%rcx;"
"1:"
"paddq 0(%%rax), %%xmm0;"
"paddq 16(%%rax), %%xmm1;"
"paddq 32(%%rax), %%xmm2;"
"paddq 48(%%rax), %%xmm3;"
"addq $64, %%rax;"
"loopq 1b;"
"paddq %%xmm0, %%xmm1;"
"paddq %%xmm2, %%xmm3;"
"paddq %%xmm1, %%xmm3;"
//"phaddd %%xmm3, %%xmm3;"
"movq %%xmm3, %0;"
: "=r" (result)
: "rm" (buffer), "rm" (num_cache_lines)
: "rax", "rcx", "xmm0", "xmm1", "xmm2", "xmm3");
  double t2 = cpu_time();

  if(!buffer)
    printf("result = %llx\n", result);

  //printf("read: %zd, %g, %g\n", num_cache_lines, (t2 - t1), 64 * num_cache_lines / (t2 - t1));
  return (1e-9 * 64 * num_cache_lines / (t2 - t1));
}

double cpu_write_mem(void *buffer, size_t len, bool nontemp = false)
{
  size_t num_cache_lines = len / 64;

  double t1 = cpu_time();
  asm volatile (
"movq $55, %%rax;"
"movq %%rax, %%xmm0;"
"movq %0, %%rax;"
"movq %1, %%rcx;"
"1:"
"movapd %%xmm0, 0(%%rax);"
"movapd %%xmm0, 16(%%rax);"
"movapd %%xmm0, 32(%%rax);"
"movapd %%xmm0, 48(%%rax);"
"addq $64, %%rax;"
"loopq 1b;"
: // none
: "rm" (buffer), "rm" (num_cache_lines)
: "memory", "rax", "rcx", "xmm0");
  double t2 = cpu_time();

  //printf("write: %zd, %g, %g\n", num_cache_lines, (t2 - t1), 64 * num_cache_lines / (t2 - t1));
  return (1e-9 * 64 * num_cache_lines / (t2 - t1));
}

#ifdef USE_CUDA
#if 0
__global__ void gpu_chain_walk_kernel(const void *buffer, size_t *result)
{
  size_t count = 0;
  while(buffer) {
    //printf("read %p\n", buffer);
    buffer = *(const void **)buffer;
    count++;
  }
  *result = count;
}
#endif
#define CHECK_CUDA(x) do { \
  cudaError_t res = (x); \
  if(res != cudaSuccess) { \
  fprintf(stderr, "CUDA error: %s = %d (%s)\n", #x, res, cudaGetErrorString(res)); \
  exit(1); \
  } \
  } while(0)

#if 0
double gpu_chain_walk(const void *buffer)
{
  cudaStream_t stream;
  cudaEvent_t ev_start, ev_end;
  size_t *result;
  size_t count;

  CHECK_CUDA( cudaStreamCreate(&stream) );
  CHECK_CUDA( cudaEventCreate(&ev_start) );
  CHECK_CUDA( cudaEventCreate(&ev_end) );
  CHECK_CUDA( cudaMalloc((void **)&result, sizeof(size_t)) );

  CHECK_CUDA( cudaEventRecord(ev_start, stream) );
  gpu_chain_walk_kernel<<< 1, 1 >>>(buffer, result);
  CHECK_CUDA( cudaEventRecord(ev_end, stream) );

  CHECK_CUDA( cudaStreamSynchronize(stream) );
  CHECK_CUDA( cudaMemcpy(&count, result, sizeof(size_t), cudaMemcpyDeviceToHost) );
  CHECK_CUDA( cudaFree(result) );

  float ms;
  CHECK_CUDA( cudaEventElapsedTime(&ms, ev_start, ev_end) );

  CHECK_CUDA( cudaEventDestroy(ev_start) );
  CHECK_CUDA( cudaEventDestroy(ev_end) );
  CHECK_CUDA( cudaStreamDestroy(stream) );

  //printf("gpu chain walk: %zd, %g, %g\n", count, ms, ms / count);
  return (1e6 * ms / count);
}

#if 0
static __device__ float2& operator+=(float2& lhs, const float2& rhs)
{
  lhs.x += rhs.x;
  lhs.y += rhs.y;
  return lhs;
}

static __device__ float4& operator+=(float4& lhs, const float4& rhs)
{
  lhs.x += rhs.x;
  lhs.y += rhs.y;
  lhs.z += rhs.z;
  lhs.w += rhs.w;
  return lhs;
}
#endif

template <typename T>
__global__ void gpu_read_mem_kernel(const void *buffer, size_t buffer_size, void *target)
{
  const T* buffer_start = (const T *)buffer;
  const T* buffer_end = (const T *)(((const char *)buffer) + buffer_size);

  // threads within a CTA will access sequential locations per access
  buffer_start += threadIdx.x;

  // CTAs are finely interleaved after that
  buffer_start += blockIdx.x * blockDim.x;

  size_t fetch_stride = gridDim.x * blockDim.x;
  size_t fetch_stride_x4 = 4 * fetch_stride;
  size_t fetch_stride_x4_in_bytes = fetch_stride_x4 * sizeof(T);

  const T *p1 = buffer_start;
  const T *p2 = p1 + fetch_stride;
  const T *p3 = p2 + fetch_stride;
  const T *p4 = p3 + fetch_stride;

  T accum;
  while(p4 < buffer_end) {
    // 4 fetches in parallel
    T v1 = *p1;
    T v2 = *p2;
    T v3 = *p3;
    T v4 = *p4;

    p1 = (const T *)(((const char *) p1) + fetch_stride_x4_in_bytes);
    p2 = (const T *)(((const char *) p2) + fetch_stride_x4_in_bytes);
    p3 = (const T *)(((const char *) p3) + fetch_stride_x4_in_bytes);
    p4 = (const T *)(((const char *) p4) + fetch_stride_x4_in_bytes);

    accum += v1;
    accum += v2;
    accum += v3;
    accum += v4;
  }

  while(p1 < buffer_end) {
    accum += *p1;
    p1 += fetch_stride;
  }

  if(target)
    *(T *)target = accum;
}
#endif

#if 0
double gpu_read_mem(int gpu_id, const void *buffer, size_t len, bool nontemp = false, size_t prefetch = 0)
{
  cudaStream_t stream;
  cudaEvent_t ev_start, ev_end;
  cudaDeviceProp dev_props;

  CHECK_CUDA( cudaGetDeviceProperties(&dev_props, gpu_id) );

  // printf("num SMs = %d\n", dev_props.multiProcessorCount);
  // printf("core clock = %d\n", dev_props.clockRate);
  // printf("memory clock = %d\n", dev_props.memoryClockRate);
  // printf("memory bus width = %d\n", dev_props.memoryBusWidth);
  // printf("shared mem/block = %d\n", dev_props.sharedMemPerBlock);

  CHECK_CUDA( cudaStreamCreate(&stream) );
  CHECK_CUDA( cudaEventCreate(&ev_start) );
  CHECK_CUDA( cudaEventCreate(&ev_end) );

  CHECK_CUDA( cudaEventRecord(ev_start, stream) );
  dim3 grid_size(2 * dev_props.multiProcessorCount, 1, 1);
  dim3 blk_size(256, 1, 1);
  gpu_read_mem_kernel<float><<< grid_size, blk_size, dev_props.sharedMemPerBlock / 2, stream >>>(buffer, len, (void *)0);
  CHECK_CUDA( cudaEventRecord(ev_end, stream) );

  CHECK_CUDA( cudaStreamSynchronize(stream) );

  float ms;
  CHECK_CUDA( cudaEventElapsedTime(&ms, ev_start, ev_end) );

  CHECK_CUDA( cudaEventDestroy(ev_start) );
  CHECK_CUDA( cudaEventDestroy(ev_end) );
  CHECK_CUDA( cudaStreamDestroy(stream) );

  //printf("gpu read: %zd, %g, %g\n", len, ms, 1e3 * len / ms);
  return (1e-6 * len / ms);
}

template <typename T>
__global__ void gpu_write_mem_kernel(void *buffer, size_t buffer_size, const void *target)
{
  T* buffer_start = (T *)buffer;
  T* buffer_end = (T *)(((char *)buffer) + buffer_size);

  // threads within a CTA will access sequential locations per access
  buffer_start += threadIdx.x;

  // CTAs are finely interleaved after that
  buffer_start += blockIdx.x * blockDim.x;

  size_t fetch_stride = gridDim.x * blockDim.x;

  T *p1 = buffer_start;
  
  T val;
  if(target)
    val = *(const T *)target;

  while(p1 < buffer_end) {
    *p1 = val;
    p1 += fetch_stride;
  }
}
#endif

#if 0
double gpu_write_mem(int gpu_id, void *buffer, size_t len, bool nontemp = false, size_t prefetch = 0)
{
  cudaStream_t stream;
  cudaEvent_t ev_start, ev_end;
  cudaDeviceProp dev_props;

  CHECK_CUDA( cudaGetDeviceProperties(&dev_props, gpu_id) );

  // printf("num SMs = %d\n", dev_props.multiProcessorCount);
  // printf("core clock = %d\n", dev_props.clockRate);
  // printf("memory clock = %d\n", dev_props.memoryClockRate);
  // printf("memory bus width = %d\n", dev_props.memoryBusWidth);

  CHECK_CUDA( cudaStreamCreate(&stream) );
  CHECK_CUDA( cudaEventCreate(&ev_start) );
  CHECK_CUDA( cudaEventCreate(&ev_end) );

  CHECK_CUDA( cudaEventRecord(ev_start, stream) );
  dim3 grid_size(2 * dev_props.multiProcessorCount, 1, 1);
  dim3 blk_size(256, 1, 1);
  gpu_write_mem_kernel<float><<< grid_size, blk_size, dev_props.sharedMemPerBlock / 2, stream >>>(buffer, len, (void *)0);
  CHECK_CUDA( cudaEventRecord(ev_end, stream) );

  CHECK_CUDA( cudaStreamSynchronize(stream) );
  CHECK_CUDA( cudaDeviceSynchronize() );

  float ms;
  CHECK_CUDA( cudaEventElapsedTime(&ms, ev_start, ev_end) );

  CHECK_CUDA( cudaEventDestroy(ev_start) );
  CHECK_CUDA( cudaEventDestroy(ev_end) );
  CHECK_CUDA( cudaStreamDestroy(stream) );

  //printf("gpu write: %zd, %g, %g\n", len, ms, 1e3 * len / ms);
  return (1e-6 * len / ms);
}
#endif
#endif

namespace Config {
  int num_cpu_domains;
  int num_mem_domains;
  size_t buffer_size;
  int num_tries;
  std::vector<void *> sysmem_buffers;
#ifdef USE_CUDA
  int num_gpu_domains;
  std::vector<void *> zc_buffers;
  std::vector<void *> fbmem_buffers;
#endif
#ifdef USE_NUMA
  std::map<int, int> cpu_ids;
  std::vector<int> mem_ids;
  unsigned long allowed_mems[8];
#endif
#ifdef USE_GASNET
  int num_gasnet_nodes;
  gasnet_seginfo_t *seginfo;
#endif
}

void pin_memory(void *buffer, size_t size)
{
#ifdef USE_NUMA
  size_t size_per_domain = Config::buffer_size;
  assert((size_per_domain * Config::num_mem_domains) <= size);
  for(int i = 0; i < Config::num_mem_domains; i++) {
    int mode = MPOL_BIND;
    unsigned long nmask = (1UL << Config::mem_ids[i]);
    CHECK_LIBC( mbind(((char *)buffer) + i * size_per_domain, size_per_domain,
		      mode, &nmask, 8 * sizeof(nmask),
		      MPOL_MF_STRICT | MPOL_MF_MOVE) );
  }
#endif

  // now actually pin it - tolerate failures
  int ret = mlock(buffer, size);
  if(ret < 0)
    fprintf(stderr, "warning: mlock(%p, %zd) failed: %d\n", buffer, size, ret);
}

struct MemtestData {
  void *buffer;
  int id;
  double read_bw, write_bw, latency;
};

void *cpu_memtest_thread(void *data_untyped)
{
  MemtestData *data = (MemtestData *)data_untyped;

  data->write_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
   double bw = cpu_write_mem(data->buffer, Config::buffer_size);
   if(bw > data->write_bw) data->write_bw = bw;
  }
  data->read_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    double bw = cpu_read_mem(data->buffer, Config::buffer_size);
    if(bw > data->read_bw) data->read_bw = bw;
  }

  init_random_chain(data->buffer, data->buffer, 4 << 10, Config::buffer_size / (4 << 10));
  data->latency = cpu_chain_walk(data->buffer);

  return data;
}

struct MemcpyData {
  void *src_buffer, *dst_buffer;
  int src_id, dst_id;
  size_t bytes;
  double push_bw, pull_bw;
};

void *cpu_memcpy_thread(void *data_untyped)
{
  MemcpyData *data = (MemcpyData *)data_untyped;

  data->push_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    double t1 = cpu_time();
    memcpy(data->dst_buffer, data->src_buffer, data->bytes);
    double t2 = cpu_time();
    double bw = 1e-9 * data->bytes / (t2 - t1);
    //printf("%zd, %g, %g\n", data->bytes, t2 - t1, bw);
    if(bw > data->push_bw) data->push_bw = bw;
  }

  data->pull_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    double t1 = cpu_time();
    memcpy(data->src_buffer, data->dst_buffer, data->bytes);
    double t2 = cpu_time();
    data->pull_bw = 1e-9 * data->bytes / (t2 - t1);
    double bw = 1e-9 * data->bytes / (t2 - t1);
    //printf("%zd, %g, %g\n", data->bytes, t2 - t1, bw);
    if(bw > data->pull_bw) data->pull_bw = bw;
  }

  return data;
}

#ifdef USE_CUDA
void gpu_memtest_thread(void *cpu_base, void *gpu_base, int gpu_id,
			MemtestData *data, bool memcpy_needed)
{
  data->write_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    double bw = gpu_write_mem(gpu_id, gpu_base, Config::buffer_size);
    if(bw > data->write_bw) data->write_bw = bw;
  }
  data->read_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    double bw = gpu_read_mem(gpu_id, gpu_base, Config::buffer_size);
    if(bw > data->read_bw) data->read_bw = bw;
  }

  init_random_chain(cpu_base, gpu_base, 4 << 10, Config::buffer_size / (4 << 10), 128);
  if(memcpy_needed)
    CHECK_CUDA( cudaMemcpy(gpu_base, cpu_base, 
			   Config::buffer_size, cudaMemcpyHostToDevice) );
  data->latency = gpu_chain_walk(gpu_base);
}

void *gpu_memcpy_thread(void *data_untyped)
{
  MemcpyData *data = (MemcpyData *)data_untyped;

  cudaStream_t stream;
  cudaEvent_t ev_start, ev_end;

  CHECK_CUDA( cudaStreamCreate(&stream) );
  CHECK_CUDA( cudaEventCreate(&ev_start) );
  CHECK_CUDA( cudaEventCreate(&ev_end) );

  data->push_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    CHECK_CUDA( cudaEventRecord(ev_start, stream) );
    if(data->dst_id == data->src_id) {
      // intra-FB copy
      CHECK_CUDA( cudaMemcpyAsync(data->dst_buffer, data->src_buffer,
				  data->bytes, cudaMemcpyDeviceToDevice) );
    } else if(data->dst_id == -1) {
      // FB<->sysmem copy
      CHECK_CUDA( cudaMemcpyAsync(data->dst_buffer, data->src_buffer,
				  data->bytes, cudaMemcpyDeviceToHost) );
    } else {
      // peer copy
      CHECK_CUDA( cudaMemcpyPeerAsync(data->dst_buffer, data->dst_id,
				      data->src_buffer, data->src_id,
				      data->bytes, stream) );
    }
    CHECK_CUDA( cudaEventRecord(ev_end, stream) );

    CHECK_CUDA( cudaStreamSynchronize(stream) );
    CHECK_CUDA( cudaDeviceSynchronize() );

    float ms;
    CHECK_CUDA( cudaEventElapsedTime(&ms, ev_start, ev_end) );
    double bw = 1e-6 * data->bytes / ms;
    //printf("%zd, %g, %g\n", data->bytes, ms, bw);
    if(bw > data->push_bw) data->push_bw = bw;
  }

  data->pull_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    CHECK_CUDA( cudaEventRecord(ev_start, stream) );
    if(data->dst_id == data->src_id) {
      // intra-FB copy
      CHECK_CUDA( cudaMemcpyAsync(data->src_buffer, data->dst_buffer,
				  data->bytes, cudaMemcpyDeviceToDevice) );
    } else if(data->dst_id == -1) {
      // FB<->sysmem copy
      CHECK_CUDA( cudaMemcpyAsync(data->src_buffer, data->dst_buffer,
				  data->bytes, cudaMemcpyHostToDevice) );
    } else {
      // peer copy
      CHECK_CUDA( cudaMemcpyPeerAsync(data->src_buffer, data->src_id,
				      data->dst_buffer, data->dst_id,
				      data->bytes, stream) );
    }
    CHECK_CUDA( cudaEventRecord(ev_end, stream) );

    CHECK_CUDA( cudaStreamSynchronize(stream) );
    CHECK_CUDA( cudaDeviceSynchronize() );

    float ms;
    CHECK_CUDA( cudaEventElapsedTime(&ms, ev_start, ev_end) );
    double bw = 1e-6 * data->bytes / ms;
    //printf("%zd, %g, %g\n", data->bytes, ms, bw);
    if(bw > data->pull_bw) data->pull_bw = bw;
  }

  CHECK_CUDA( cudaEventDestroy(ev_start) );
  CHECK_CUDA( cudaEventDestroy(ev_end) );
  CHECK_CUDA( cudaStreamDestroy(stream) );

  return data;
}
#endif

#ifdef USE_GASNET

#define CHECK_GASNET(x) do { \
  int ret = (x); \
  if(ret != GASNET_OK) { \
  fprintf(stderr, "GASNET: %s = %d (%s)\n", #x, ret, gasnet_ErrorName(ret)); \
  exit(1); \
  } \
  } while(0)

static void gasnet_attach_hook(void *segbase, uintptr_t segsize)
{
  //printf("gasnet_attach: %p, %zx\n", segbase, (size_t) segsize);
  pin_memory(segbase, segsize);
}

double gasnet_chain_walk(int node, const void *buffer)
{
  double t1 = cpu_time();
  size_t count = 0;
  //const void *orig_buffer = buffer;
  while(buffer) {
    //printf("%p, %d\n", buffer, ((char *)buffer)-((char *)orig_buffer));
    count++;
    buffer = (const void *)(gasnet_get_val(node, (void *)buffer, 
					   sizeof(const void *)));
  }
  double t2 = cpu_time();
  //printf("gasnet chain walk: %zd, %g, %g\n", count, t2 - t1, (t2 - t1) / count);
  return (1e9 * (t2 - t1) / count);
}

double gasnet_read_mem(int node, const void *buffer, size_t len)
{
  gasnet_register_value_t *buffer_typed = (gasnet_register_value_t *)buffer;
  gasnet_register_value_t val;
  size_t num_fetches = len / sizeof(val);

  double t1 = cpu_time();
  for(size_t i = 0; i < num_fetches; i++)
    val += gasnet_get_val(node, buffer_typed++, sizeof(val));
  double t2 = cpu_time();

  //printf("gasnet read: %zd, %g, %g\n", num_fetches, (t2 - t1), sizeof(val) * num_fetches / (t2 - t1));
  return (1e-9 * sizeof(val) * num_fetches / (t2 - t1));
}

double gasnet_write_mem(int node, void *buffer, size_t len)
{
  gasnet_register_value_t *buffer_typed = (gasnet_register_value_t *)buffer;
  gasnet_register_value_t val = 44;
  size_t num_fetches = len / sizeof(val);

  double t1 = cpu_time();
  gasnet_begin_nbi_accessregion();
  for(size_t i = 0; i < num_fetches; i++)
    gasnet_put_nbi_val(node, buffer_typed++, val, sizeof(val));

  gasnet_handle_t handle = gasnet_end_nbi_accessregion();

  gasnet_wait_syncnb(handle);
  double t2 = cpu_time();

  //printf("gasnet write: %zd, %g, %g\n", num_fetches, (t2 - t1), sizeof(val) * num_fetches / (t2 - t1));
  return (1e-9 * sizeof(val) * num_fetches / (t2 - t1));
}

void *gasnet_memtest_thread(void *data_untyped)
{
  MemtestData *data = (MemtestData *)data_untyped;

  data->write_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    double bw = gasnet_write_mem(data->id, data->buffer,
				 Config::buffer_size / 4);
    if(bw > data->write_bw) data->write_bw = bw;
  }
  data->read_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    double bw = gasnet_read_mem(data->id, data->buffer,
				Config::buffer_size / 32);
    if(bw > data->read_bw) data->read_bw = bw;
  }

  init_random_chain(Config::sysmem_buffers[0], 
		    data->buffer, 4 << 10, Config::buffer_size / (4 << 10),
		    64);
  gasnet_put_bulk(data->id, data->buffer, 
		  Config::sysmem_buffers[0], Config::buffer_size);
  data->latency = gasnet_chain_walk(data->id, data->buffer);

  return data;
}

void *gasnet_memcpy_thread(void *data_untyped)
{
  MemcpyData *data = (MemcpyData *)data_untyped;

  data->push_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    double t1 = cpu_time();
    gasnet_put_bulk(data->dst_id, data->dst_buffer,
		    data->src_buffer, data->bytes);
    double t2 = cpu_time();
    double bw = 1e-9 * data->bytes / (t2 - t1);
    //printf("%zd, %g, %g\n", data->bytes, t2 - t1, bw);
    if(bw > data->push_bw) data->push_bw = bw;
  }

  data->pull_bw = 0;
  for(int x = 0; x < Config::num_tries; x++) {
    double t1 = cpu_time();
    gasnet_get_bulk(data->src_buffer, 
		    data->dst_id, data->dst_buffer,
		    data->bytes);
    double t2 = cpu_time();
    data->pull_bw = 1e-9 * data->bytes / (t2 - t1);
    double bw = 1e-9 * data->bytes / (t2 - t1);
    //printf("%zd, %g, %g\n", data->bytes, t2 - t1, bw);
    if(bw > data->pull_bw) data->pull_bw = bw;
  }

  return data;
}

#endif

int main(int argc, const char *argv[])
{
#ifdef USE_NUMA
  //numa_init();
  assert(numa_available() != -1);

  //numa_exit_on_error = 1;
  //numa_exit_on_warn = 1;

  // get the list of allowed cpus and see which nodes they live on
  {
    bitmask *all_cpus = numa_all_cpus_ptr;
    //printf("all cpus = %lx\n", all_cpus->maskp[0]);
    for(int i = 0; i < 8*numa_bitmask_nbytes(all_cpus); i++) {
      //printf("cpu: %d, %d, %d\n", i, numa_bitmask_isbitset(all_cpus, i), numa_node_of_cpu(i));
      if(!numa_bitmask_isbitset(all_cpus, i))
	continue;

      int node = numa_node_of_cpu(i);
      //printf("%d -> %d\n", i, node);
      Config::cpu_ids[node] = i;
    }
    Config::num_cpu_domains = Config::cpu_ids.size();
  }
  
  // get the list of memory domains
  {
    int mode, ret;

    CHECK_LIBC( get_mempolicy(&mode, Config::allowed_mems, 8*sizeof(Config::allowed_mems), 0, MPOL_F_MEMS_ALLOWED) );
    Config::allowed_mems[0] = 0;

    // if the mask that came from the above request is 0, there's no constraints, so take all allowed mems
    if(Config::allowed_mems[0] == 0) {
      ret = get_mempolicy(&mode, Config::allowed_mems, 8*sizeof(Config::allowed_mems), 0, MPOL_F_MEMS_ALLOWED);
      assert(ret >= 0);
    }

    Config::num_mem_domains = 0;
    for(int i = 0; i < 32; i++)
      if((Config::allowed_mems[0] >> i) & 1) {
	Config::mem_ids.push_back(i);
	Config::num_mem_domains++;
      }
  }

  //printf("NUMA: %d, %d, %lx\n", Config::num_cpu_domains, Config::num_mem_domains, Config::allowed_mems);
#else
  Config::num_cpu_domains = 1;
  Config::num_mem_domains = 1;
#endif

#ifdef USE_CUDA  
  CHECK_CUDA( cudaGetDeviceCount(&Config::num_gpu_domains) );
  // have to do this before any other GPU stuff
  for(int i = 0; i < Config::num_gpu_domains; i++) {
    CHECK_CUDA( cudaSetDevice(i) );
    CHECK_CUDA( cudaSetDeviceFlags(cudaDeviceMapHost) );
  }
#endif

  Config::buffer_size = 64 << 20;
  Config::num_tries = 3;

#ifdef USE_GASNET
#ifdef GASNET_ON_MPI
  {
    int ret = MPI_Init(&argc, (char ***)&argv);
    printf("ret = %d\n", ret);
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    printf("MPI: rank = %d, size = %d\n", rank, size);
  }
#endif
    
  {
    CHECK_GASNET( gasnet_init(&argc, (char ***)&argv) );

    gasnet_client_attach_hook = gasnet_attach_hook;

    CHECK_GASNET( gasnet_attach(0, 0,
				Config::num_mem_domains * Config::buffer_size,
				0) );

    int mynode = gasnet_mynode();
    int nodes = gasnet_nodes();

    Config::num_gasnet_nodes = nodes;
    Config::seginfo = new gasnet_seginfo_t[nodes];
    CHECK_GASNET( gasnet_getSegmentInfo(Config::seginfo, nodes) );

    // now zero out our memory - pulling in TLBs, splitting COW pages
    char *local_base = (char *)(Config::seginfo[mynode].addr);
    bzero(local_base, Config::seginfo[mynode].size);

    for(int i = 0; i < Config::num_mem_domains; i++)
      Config::sysmem_buffers.push_back(local_base + (i * Config::buffer_size));

    // if we're not the first node, we're passive for rest of test
    if(mynode != 0) {
      gasnet_barrier_notify(0, GASNET_BARRIERFLAG_ANONYMOUS);
      CHECK_GASNET( gasnet_barrier_wait(0, GASNET_BARRIERFLAG_ANONYMOUS) );
      gasnet_exit(0);
      return 0;
    }
  }
#else
  // cpu mem comes from mmap
  {
#ifdef USE_HUGETLBFS
    int numsizes = gethugepagesizes(0, 0);
    long *sizes = (long *)alloca(numsizes * sizeof(long));
    CHECK_LIBC( gethugepagesizes(sizes, numsizes) );
    printf("%d sizes:", numsizes);
    for(int i = 0; i < numsizes; i++) printf(" %ld(%lx)", sizes[i], sizes[i]);
    printf("\n");
    int fd = hugetlbfs_unlinked_fd_for_size(sizes[0]);
    void *mmap_base = mmap(0, Config::buffer_size * Config::num_mem_domains,
                           PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS,
                           fd, 0);
    printf("%p\n", mmap_base);
    assert(mmap_base != ((void *)-1));

    pin_memory(mmap_base, Config::buffer_size * Config::num_mem_domains);
#else
    void *mmap_base = mmap(0, Config::buffer_size * Config::num_mem_domains,
			   PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS,
			   -1, 0);
    assert(mmap_base != ((void *)-1));

    pin_memory(mmap_base, Config::buffer_size * Config::num_mem_domains);
#endif

    // now zero it out - pulling in TLBs, splitting COW pages
    bzero(mmap_base, Config::buffer_size * Config::num_mem_domains);

    for(int i = 0; i < Config::num_mem_domains; i++) {
      void *chunk_base = ((char *)mmap_base) + (i * Config::buffer_size);
      Config::sysmem_buffers.push_back(chunk_base);
    }
  }
#endif

#ifdef USE_CUDA  
  {
    // register system memory regions with the GPU(s) as well
    for(int i = 0; i < Config::num_mem_domains; i++) {
      void *chunk_base = Config::sysmem_buffers[i];

      CHECK_CUDA( cudaHostRegister(chunk_base, Config::buffer_size, cudaHostRegisterPortable | cudaHostRegisterMapped) );

      for(int j = 0; j < Config::num_gpu_domains; j++) {
	CHECK_CUDA( cudaSetDevice(j) );
	void *gpu_chunk_base;
	CHECK_CUDA( cudaHostGetDevicePointer(&gpu_chunk_base, chunk_base, 0) );
	//printf("cpu = %p, gpu = %p\n", chunk_base, gpu_chunk_base);
	if(j == 0) {
	  Config::zc_buffers.push_back(gpu_chunk_base);
	} else {
	  assert(Config::zc_buffers[i] == gpu_chunk_base);
	}
      }
    }

    // allocate buffers in each GPU's framebuffer
    for(int g = 0; g < Config::num_gpu_domains; g++) {
      CHECK_CUDA( cudaSetDevice(g) );

      void *gpu_buffer;
      CHECK_CUDA( cudaMalloc(&gpu_buffer, Config::buffer_size) );
      //printf("gpu buffer at %p\n", gpu_buffer);
      Config::fbmem_buffers.push_back(gpu_buffer);
    }
  }
#endif

  printf("Domains: CPU = %d, MEM = %d",
	 Config::num_cpu_domains, Config::num_mem_domains);
#ifdef USE_CUDA
  printf(", GPU = %d", Config::num_gpu_domains);
#endif
#ifdef USE_GASNET
  printf(", GASNET = %d", Config::num_gasnet_nodes);
#endif
  printf("\n");

  printf("Buffer Size (MB): %zd\n", Config::buffer_size >> 20);

  printf("\n");

  printf("Processor Affinities:\n");
  // CPUs
  for(int c = 0; c < Config::num_cpu_domains; c++) {
    // cpu affinity to sysmem regions
    for(int m = 0; m < Config::num_mem_domains; m++) {
      MemtestData data;
      data.buffer = Config::sysmem_buffers[m];

      pthread_attr_t attr;
      pthread_attr_init(&attr);
#ifdef USE_NUMA
      std::map<int, int>::const_iterator it = Config::cpu_ids.begin();
      for(int z = 0; z < c; z++) it++;
      int cpu_domain = it->first;
      cpu_set_t cpuset;
      CPU_ZERO(&cpuset);
      CPU_SET(it->second, &cpuset);
      int ret = pthread_attr_setaffinity_np(&attr, sizeof(cpuset), &cpuset);
      //printf("ret = %d (%d)\n", ret, it->second);
      int mem_domain = Config::mem_ids[m];
#else
      int cpu_domain = c;
      int mem_domain = m;
#endif
      pthread_t thread;
      pthread_create(&thread, &attr, cpu_memtest_thread, &data);
      void *dummy;
      pthread_join(thread, &dummy);

      printf("CPU %d -> SYS %d: r = %6.2f, w = %6.2f, l = %6.1f\n",
             cpu_domain, mem_domain,
             data.read_bw, data.write_bw, data.latency);
    }

#ifdef USE_GASNET
    // cpu affinity to gasnet regions
    for(int n = 1; n < Config::num_gasnet_nodes; n++) {
      for(int m = 0; m < Config::num_mem_domains; m++) {
	MemtestData data;
	data.id = n;
	data.buffer = ((char *)(Config::seginfo[n].addr)) + (m * Config::buffer_size);

	pthread_attr_t attr;
	pthread_attr_init(&attr);
#ifdef USE_NUMA
	std::map<int, int>::const_iterator it = Config::cpu_ids.begin();
	for(int z = 0; z < c; z++) it++;
	int cpu_domain = it->first;
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(it->second, &cpuset);
	CHECK_LIBC( pthread_attr_setaffinity_np(&attr, sizeof(cpuset), &cpuset) );
	int mem_domain = Config::mem_ids[m];
#else
	int cpu_domain = c;
	int mem_domain = m;
#endif
	pthread_t thread;
	pthread_create(&thread, &attr, gasnet_memtest_thread, &data);
	void *dummy;
	pthread_join(thread, &dummy);

	printf("CPU %d -> G %d %d: r = %6.2f, w = %6.2f, l = %6.1f\n",
	       cpu_domain, n, mem_domain,
	       data.read_bw, data.write_bw, data.latency);
      }
    }
#endif

    printf("\n");
  }

#ifdef USE_CUDA
  // GPUs
  for(int g = 0; g < Config::num_gpu_domains; g++) {
    CHECK_CUDA( cudaSetDevice(g) );

    // affinity to system memory
    for(int m = 0; m < Config::num_mem_domains; m++) {
      void *cpu_base = Config::sysmem_buffers[m];
      void *gpu_base = Config::zc_buffers[m];

#ifdef USE_NUMA
      int mem_domain = Config::mem_ids[m];
#else
      int mem_domain = m;
#endif

      MemtestData data;
      gpu_memtest_thread(cpu_base, gpu_base, g, &data, false);

      printf("GPU %d -> SYS %d: r = %6.2f, w = %6.2f, l = %6.1f\n",
             g, mem_domain,
             data.read_bw, data.write_bw, data.latency);
    }

    // affinity to local FB
    {
      void *cpu_base = Config::sysmem_buffers[0]; // any cpu buffer will do
      void *gpu_base = Config::fbmem_buffers[g];

      MemtestData data;
      gpu_memtest_thread(cpu_base, gpu_base, g, &data, true);

      printf("GPU %d -> FB  %d: r = %6.2f, w = %6.2f, l = %6.1f\n",
             g, g,
             data.read_bw, data.write_bw, data.latency);
    }

    printf("\n");
  }
#endif

  printf("Memory Affinities:\n");

  // system memories
  for(int m1 = 0; m1 < Config::num_mem_domains; m1++) {
    for(int m2 = 0; m2 < Config::num_mem_domains; m2++) {
      MemcpyData data;
      data.src_buffer = Config::sysmem_buffers[m1];
      data.dst_buffer = Config::sysmem_buffers[m2];
      data.bytes = Config::buffer_size;
      if(m1 == m2) {
	data.bytes >>= 1;
	data.dst_buffer = ((char *)(data.dst_buffer)) + data.bytes;
      }

      pthread_attr_t attr;
      pthread_attr_init(&attr);
#ifdef USE_NUMA
      int m1_domain = Config::mem_ids[m1];
      int m2_domain = Config::mem_ids[m2];
      // do we have a cpu available in the source domain?
      std::map<int, int>::const_iterator it = Config::cpu_ids.find(m1_domain);
      if(it == Config::cpu_ids.end()) {
	printf("no cpu in mem domain %d\n", m1_domain);
	continue;
      } else {
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(it->second, &cpuset);
	int ret = pthread_attr_setaffinity_np(&attr, sizeof(cpuset), &cpuset);
	assert(ret >= 0);
      }
#else
      int m1_domain = m1;
      int m2_domain = m2;
#endif

      pthread_t thread;
      pthread_create(&thread, &attr, cpu_memcpy_thread, &data);
      void *dummy;
      pthread_join(thread, &dummy);

      printf("SYS %d -> SYS %d: push = %6.2f, pull = %6.2f\n",
             m1_domain, m2_domain, data.push_bw, data.pull_bw);
    }

#ifdef USE_GASNET
    // gasnet remote memorys
    for(int n = 1; n < Config::num_gasnet_nodes; n++) {
      for(int m2 = 0; m2 < Config::num_mem_domains; m2++) {
	MemcpyData data;

	data.src_buffer = Config::sysmem_buffers[m1];
	data.src_id = 0;
	data.dst_buffer = ((char *)(Config::seginfo[n].addr)) + (m2 * Config::buffer_size);
	data.dst_id = n;

	pthread_attr_t attr;
	pthread_attr_init(&attr);
#ifdef USE_NUMA
	int m1_domain = Config::mem_ids[m1];
	int m2_domain = Config::mem_ids[m2];
	// do we have a cpu available in the source domain?
	std::map<int, int>::const_iterator it = Config::cpu_ids.find(m1_domain);
	if(it == Config::cpu_ids.end()) {
	  printf("no cpu in mem domain %d\n", m1_domain);
	  continue;
	} else {
	  cpu_set_t cpuset;
	  CPU_ZERO(&cpuset);
	  CPU_SET(it->second, &cpuset);
	  CHECK_LIBC( pthread_attr_setaffinity_np(&attr, sizeof(cpuset), &cpuset) );
	}
#else
	int m1_domain = m1;
	int m2_domain = m2;
#endif

	pthread_t thread;
	pthread_create(&thread, &attr, gasnet_memcpy_thread, &data);
	void *dummy;
	pthread_join(thread, &dummy);

	printf("SYS %d -> G %d %d: push = %6.2f, pull = %6.2f\n",
	       m1_domain, n, m2_domain, data.push_bw, data.pull_bw);
      }
    }
#endif

    printf("\n");
  }

#ifdef USE_CUDA
  // GPU framebuffers
  for(int g = 0; g < Config::num_gpu_domains; g++) {
    // to/from system
    for(int m = 0; m < Config::num_mem_domains; m++) {
      MemcpyData data;
      data.src_buffer = Config::fbmem_buffers[g];
      data.src_id = g;
      data.dst_buffer = Config::sysmem_buffers[m];
      data.dst_id = -1;
      data.bytes = Config::buffer_size;

#ifdef USE_NUMA
      int mem_domain = Config::mem_ids[m];
#else
      int mem_domain = m;
#endif

      gpu_memcpy_thread(&data);

      printf("FB  %d -> SYS %d: push = %6.2f, pull = %6.2f\n",
             g, mem_domain, data.push_bw, data.pull_bw);
    }

    // to/from other fbs
    for(int g2 = 0; g2 < Config::num_gpu_domains; g2++) {
      MemcpyData data;
      data.src_buffer = Config::fbmem_buffers[g];
      data.src_id = g;
      data.dst_buffer = Config::fbmem_buffers[g2];
      data.dst_id = g2;
      data.bytes = Config::buffer_size;
      if(g == g2) {
	data.bytes >>= 1;
	data.dst_buffer = ((char *)(data.dst_buffer)) + data.bytes;
      }

      gpu_memcpy_thread(&data);

      printf("FB  %d -> FB  %d: push = %6.2f, pull = %6.2f\n",
             g, g2, data.push_bw, data.pull_bw);
    }

    printf("\n");
  }
#endif

#ifdef USE_GASNET
  gasnet_barrier_notify(0, GASNET_BARRIERFLAG_ANONYMOUS);
  CHECK_GASNET( gasnet_barrier_wait(0, GASNET_BARRIERFLAG_ANONYMOUS) );
  gasnet_exit(0);
#endif

#if 0
  return 1;
  printf("cpu_lat = %g\n", cpu_lat);
  size_t buffer_size = 64 << 20;
  void *b = malloc(buffer_size);
  bzero(b, buffer_size);

  cpu_write_mem(b, buffer_size);
  cpu_read_mem(b, buffer_size);
  cpu_write_mem(b, buffer_size);
  cpu_read_mem(b, buffer_size);
  cpu_write_mem(b, buffer_size);
  cpu_read_mem(b, buffer_size);
  cpu_read_mem(b, buffer_size);

  void *gpu_buffer;
  CHECK_CUDA( cudaMalloc(&gpu_buffer, buffer_size) );
  printf("gpu buffer at %p\n", gpu_buffer);

  init_random_chain(b, b, 4 << 10, buffer_size / (4 << 10));
  double cpu_lat = cpu_chain_walk(b);
  printf("cpu_lat = %g\n", cpu_lat);

  init_random_chain(b, gpu_buffer, 4 << 10, buffer_size / (4 << 10), 64);
  CHECK_CUDA( cudaMemcpy(gpu_buffer, b, buffer_size, cudaMemcpyHostToDevice) );
  double gpu_lat = gpu_chain_walk(gpu_buffer);
  printf("gpu_lat = %g\n", gpu_lat);

  gpu_write_mem(gpu_buffer, buffer_size);
  gpu_read_mem(gpu_buffer, buffer_size);

  // zero copy
  void *zc_buffer_cpu, *zc_buffer_gpu;
  CHECK_CUDA( cudaHostAlloc(&zc_buffer_cpu, buffer_size, cudaHostAllocPortable | cudaHostAllocMapped) );
  CHECK_CUDA( cudaHostGetDevicePointer(&zc_buffer_gpu, zc_buffer_cpu, 0) );

  cpu_write_mem(zc_buffer_cpu, buffer_size);
  cpu_read_mem(zc_buffer_cpu, buffer_size);
  gpu_write_mem(zc_buffer_gpu, buffer_size);
  gpu_read_mem(zc_buffer_gpu, buffer_size);

  init_random_chain(zc_buffer_cpu, zc_buffer_cpu, 4 << 10, buffer_size / (4 << 10));
  double cpu_lat2 = cpu_chain_walk(zc_buffer_cpu);
  printf("cpu_lat = %g\n", cpu_lat2);

  init_random_chain(zc_buffer_cpu, zc_buffer_gpu, 4 << 10, buffer_size / (4 << 10));
  double gpu_lat2 = gpu_chain_walk(zc_buffer_gpu);
  printf("gpu_lat = %g\n", gpu_lat2);

  return 4;
  init_random_chain(b, b, 4 << 10, 16 << 10);
  cpu_chain_walk(b);
  init_random_chain(b, b, 4 << 10, 16 << 10);
  cpu_chain_walk(b);


  init_random_chain(b, b, 8, 8 << 20);
  cpu_chain_walk(b);
  init_random_chain(b, b, 8, 8 << 20);
  cpu_chain_walk(b);
  init_random_chain(b, b, 8, 8 << 20);
  cpu_chain_walk(b);

  init_random_chain(b, b, 256, 256 << 10);
  cpu_chain_walk(b);
  init_random_chain(b, b, 256, 256 << 10);
  cpu_chain_walk(b);
  init_random_chain(b, b, 256, 256 << 10);
  cpu_chain_walk(b);

  init_random_chain(b, b, 64 << 20, 1);
  cpu_chain_walk(b);
  cpu_chain_walk(b);
  cpu_chain_walk(b);
#endif
}
