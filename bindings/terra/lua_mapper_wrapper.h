#ifndef __LUA_MAPPER_WRAPPER_H__
#define __LUA_MAPPER_WRAPPER_H__

#include <vector>
#include <string>

#include "legion.h"
#include "default_mapper.h"

extern "C"
{
#include "lua.h"
}

using namespace LegionRuntime::HighLevel;

class LuaMapperWrapper : public DefaultMapper
{
  public:
    LuaMapperWrapper(const char*, Machine, HighLevelRuntime*, Processor);
    ~LuaMapperWrapper();

    virtual void slice_domain(const Task *task, const Domain &domain,
                              std::vector<DomainSplit> &slices);
    virtual bool map_task(Task *task);
    virtual bool map_inline(Inline *inline_operation);

    virtual void notify_mapping_failed(const Mappable *mappable);

  private:
    const std::string qualified_mapper_name;
    std::string mapper_name;
    lua_State* L;
};

#endif // __LUA_MAPPER_WRAPPER_H__
