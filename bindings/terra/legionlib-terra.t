local legion_c = terralib.includec("legion_c.h")

-- imports opaque types in legion_c.h to the global table
function legion:import_types()
  for k, v in pairs(legion_c)
    do
      if string.match(k, "legion_.*_t") then
        k = string.gsub(k, "legion_", "")
        rawset(_G, k, v)
      end
    end
end

function legion:register_terra_task_void(task_fn, task_id, proc, single, index, vid, opt, name)
  vid = vid or -1
  opt = opt or { leaf = false, inner = false, idempotent = false }
  name = name or task_fn.name
  local ptr = task_fn:getdefinitions()[1]:getpointer()
  legion_c.legion_runtime_register_task_void(task_id, proc, single, index,
                                             vid, opt, name, ptr)
end

function legion:set_terra_registration_callback(func)
  legion_c.legion_runtime_set_registration_callback(
    func:getdefinitions()[1]:getpointer())
end

