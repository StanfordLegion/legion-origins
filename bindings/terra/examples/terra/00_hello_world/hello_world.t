require("legionlib")
local std = terralib.includec("stdio.h")

legion:import_types()

HELLO_WORLD_ID = 1

terra hello_world_task(task : task_t,
                       regions : &physical_region_t,
                       num_regions : uint32,
                       ctx : context_t,
                       runtime : runtime_t)
  std.printf("Hello World!\n")
end

function legion_main(arg)
  legion:set_top_level_task_id(HELLO_WORLD_ID)
  legion:register_terra_task_void(
    hello_world_task,
    HELLO_WORLD_ID, legion.LOC_PROC,
    true,  -- single
    false  -- index
  )
  legion:start(arg)
end

if rawget(_G, "arg") then
  legion_main(arg)
end
