WIRE_SEGMENTS = 10
STEPS = 10000
DELTAT = 1e-6

INDEX_DIM = 1

-- PointerLocation
PointerLocation = uint
PRIVATE_PTR = 1
SHARED_PTR = 2
GHOST_PTR = 3

TOP_LEVEL_TASK_ID = 0
CALC_NEW_CURRENTS_TASK_ID = 1
DISTRIBUTE_CHARGE_TASK_ID = 2
UPDATE_VOLTAGES_TASK_ID = 3
CHECK_FIELD_TASK_ID = 4

REDUCE_ID = 1

-- NodeFields
FID_NODE_CAP = 1
FID_LEAKAGE = 2
FID_CHARGE = 3
FID_NODE_VOLTAGE = 4

-- WireFields
FID_IN_PTR = 1
FID_OUT_PTR = 2
FID_IN_LOC = 3
FID_OUT_LOC = 4
FID_INDUCTANCE = 5
FID_RESISTANCE = 6
FID_WIRE_CAP = 7
FID_CURRENT = 8
FID_WIRE_VOLTAGE = FID_CURRENT + WIRE_SEGMENTS
FID_LAST = FID_WIRE_VOLTAGE + WIRE_SEGMENTS - 1

-- LocatorFields
FID_LOCATOR = 1

-- CircuitVariants
CIRCUIT_CPU_LEAF_VARIANT = 0

struct Circuit {
  all_nodes : logical_region_t;
  all_wires : logical_region_t;
  node_locator : logical_region_t;
}

struct CircuitPiece {
  pvt_nodes : logical_region_t;
  shr_nodes : logical_region_t;
  ghost_nodes : logical_region_t;
  pvt_wires : logical_region_t;
  num_wires : uint;
  first_wire : ptr_t;
  num_nodes : uint;
  first_node : ptr_t;

  dt : float;
  steps : int;
}

CalcNewCurrentsTask = {}
CalcNewCurrentsTask.__index = CalcNewCurrentsTask

DistributeChargeTask = {}
DistributeChargeTask.__index = DistributeChargeTask

UpdateVoltagesTask = {}
UpdateVoltagesTask.__index = UpdateVoltagesTask

CheckTask = {}
CheckTask.__index = CheckTask

TaskHelper = {}
function TaskHelper:dispatch_task(T, launcher, ctx, runtime, perform_checks,
                                  simulation_success, wait)
  wait = wait or false
  local fm = runtime:execute_index_space(ctx, launcher)
  if wait then
    fm:wait_all_results()
  end
  -- See if we need to perform any checks
  if simulation_success and perform_checks then
    simulation_success =
      launcher:launch_check_fields(ctx, runtime) and simulation_success
    if not simulation_success then
      printf("WARNING: First NaN values found in %s", T.TASK_NAME)
    end
  end
  return simulation_success
end

function TaskHelper:register_cpu_variants(T)
  legion:register_lua_task_void(T.TASK_NAME, T.TASK_ID, legion.LOC_PROC,
                                false, true,
                                CIRCUIT_CPU_LEAF_VARIANT,
                                TaskConfigOptions:new(T.CPU_BASE_LEAF))
end

