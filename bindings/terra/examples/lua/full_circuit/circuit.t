require "legionlib"
legion:import_types()
require "circuit_defs"
require "circuit_cpu"
require "circuit_mapper"

local std = terralib.includec("stdlib.h")
rawset(_G, "drand48", std.drand48)
rawset(_G, "srand48", std.srand48)

local parse_input_args
local load_circuit
local allocate_node_fields
local allocate_wire_fields
local allocate_locator_fields

function top_level_task(task, regions, ctx, runtime)
  local num_loops = 2
  local num_pieces = 4
  local nodes_per_piece = 2
  local wires_per_piece = 4
  local pct_wire_in_piece = 95
  local random_seed = 12345
  local steps = STEPS
  local sync = 0
  local perform_checks = false
  local dump_values = false
  do
    local arg = legion:get_input_args()
    num_loops, num_pieces, nodes_per_piece, wires_per_piece,
    pct_wire_in_piece, random_seed, steps, sync, perform_checks,
    dump_values =
      parse_input_args(arg,
        num_loops, num_pieces, nodes_per_piece, wires_per_piece,
        pct_wire_in_piece, random_seed, steps, sync, perform_checks,
        dump_values)
    printf("circuit settings: loops=%d pieces=%d nodes/piece=%d "..
      "wires/piece=%d pct_in_piece=%d seed=%d",
      num_loops, num_pieces, nodes_per_piece, wires_per_piece,
      pct_wire_in_piece, random_seed)
  end

  local circuit = {}
  do
    local num_circuit_nodes = num_pieces * nodes_per_piece
    local num_circuit_wires = num_pieces * wires_per_piece
    -- Make index spaces
    local node_index_space = runtime:create_index_space(ctx, num_circuit_nodes)
    local wire_index_space = runtime:create_index_space(ctx, num_circuit_wires)
    -- Make field spaces
    local node_field_space = runtime:create_field_space(ctx)
    local wire_field_space = runtime:create_field_space(ctx)
    local locator_field_space = runtime:create_field_space(ctx)
    -- Allocate fields
    allocate_node_fields(ctx, runtime, node_field_space)
    allocate_wire_fields(ctx, runtime, wire_field_space)
    allocate_locator_fields(ctx, runtime, locator_field_space)
    -- Make logical regions
    circuit.all_nodes = runtime:create_logical_region(ctx,node_index_space,node_field_space)
    circuit.all_wires = runtime:create_logical_region(ctx,wire_index_space,wire_field_space)
    circuit.node_locator = runtime:create_logical_region(ctx,node_index_space,locator_field_space)
  end

  -- Load the circuit
  local parts, pieces =
    load_circuit(circuit, ctx, runtime, num_pieces, nodes_per_piece,
      wires_per_piece, pct_wire_in_piece, random_seed, steps)

  -- Arguments for each point
  local local_args = ArgumentMap:new()
  for idx = 0, num_pieces - 1 do
    local point = DomainPoint:from_point(Point:new{idx})
    local_args:set_point(point, TaskArgument:new(pieces[idx], CircuitPiece))
  end

  -- Make the launchers
  local launch_rect = Rect:new(Point:new{0}, Point:new{num_pieces-1})
  local launch_domain = Domain:from_rect(launch_rect)
  local cnc_launcher = CalcNewCurrentsTask:new(parts.pvt_wires, parts.pvt_nodes,
                                               parts.shr_nodes, parts.ghost_nodes,
                                               circuit.all_wires, circuit.all_nodes,
                                               launch_domain, local_args)

  local dsc_launcher = DistributeChargeTask:new(parts.pvt_wires, parts.pvt_nodes,
                                                parts.shr_nodes, parts.ghost_nodes,
                                                circuit.all_wires, circuit.all_nodes,
                                                launch_domain, local_args)

  local upv_launcher = UpdateVoltagesTask:new(parts.pvt_nodes, parts.shr_nodes,
                                              parts.node_locations, circuit.all_nodes,
                                              circuit.node_locator, launch_domain,
                                              local_args)

  printf("Starting main simulation loop")
  local ts_start, ts_end
  ts_start = legion:get_current_time_in_micros()
  -- Run the main loop
  local simulation_success = true
  for i = 1, num_loops
  do
    simulation_success =
      TaskHelper:dispatch_task(CalcNewCurrentsTask, cnc_launcher, ctx, runtime,
        perform_checks, simulation_success)
    simulation_success =
      TaskHelper:dispatch_task(DistributeChargeTask, dsc_launcher, ctx, runtime,
        perform_checks, simulation_success)
    simulation_success =
      TaskHelper:dispatch_task(UpdateVoltagesTask, upv_launcher, ctx, runtime,
        perform_checks, simulation_success, i == num_loops)
  end
  ts_end = legion:get_current_time_in_micros()
  if simulation_success then
    printf("SUCCESS!")
  else
    printf("FAILURE!")
  end
  do
    local sim_time = 1e-6 * (ts_end - ts_start)
    printf("ELAPSED TIME = %7.3f s", sim_time)

    -- Compute the floating point operations per second
    local num_circuit_nodes = num_pieces * nodes_per_piece
    local num_circuit_wires = num_pieces * wires_per_piece
    -- calculate currents
    local operations = num_circuit_wires * (WIRE_SEGMENTS*6 + (WIRE_SEGMENTS-1)*4) * steps
    -- distribute charge
    operations = operations + (num_circuit_wires * 4)
    -- update voltages
    operations = operations + (num_circuit_nodes * 4)
    -- multiply by the number of loops
    operations = operations * num_loops

    -- Compute the number of gflops
    local gflops = (1e-9*operations)/sim_time
    printf("GFLOPS = %7.3f GFLOPS", gflops)
  end
  print("simulation complete - destroying regions")

  if dump_values then
    local io = terralib.includec("stdio.h")
    local wires_req = RegionRequirement:new(circuit.all_wires, legion.READ_ONLY,
                                            legion.EXCLUSIVE, circuit.all_wires)
    for i = 0, WIRE_SEGMENTS - 1 do
      wires_req:add_field(FID_CURRENT+i)
    end
    for i = 0, WIRE_SEGMENTS - 2 do
      wires_req:add_field(FID_WIRE_VOLTAGE+i)
    end
    local wires = runtime:map_region(ctx, wires_req)
    wires:wait_until_valid()
    local fa_wire_currents = {}
    for i = 0, WIRE_SEGMENTS - 1 do
      fa_wire_currents[i] = wires:get_field_accessor(FID_CURRENT+i):typeify(float)
    end
    local fa_wire_voltages = {}
    for i = 0, WIRE_SEGMENTS - 2 do
      fa_wire_voltages[i] = wires:get_field_accessor(FID_WIRE_VOLTAGE+i):typeify(float)
    end
    local itr = IndexIterator:new(circuit.all_wires:get_index_space())
    while itr:has_next() do
      local wire_ptr = itr:next()
      for i = 0, WIRE_SEGMENTS - 1 do
        io.printf(" %.5g", fa_wire_currents[i]:read(wire_ptr))
      end
      for i = 0, WIRE_SEGMENTS - 2 do
        io.printf(" %.5g", fa_wire_voltages[i]:read(wire_ptr))
      end
      io.printf("\n")
    end
    runtime:unmap_region(ctx, wires)
  end
  -- Now we can destroy all the things that we made
  do
    runtime:destroy_logical_region(ctx, circuit.all_nodes)
    runtime:destroy_logical_region(ctx, circuit.all_wires)
    runtime:destroy_logical_region(ctx, circuit.node_locator)
    runtime:destroy_field_space(ctx, circuit.all_nodes:get_field_space())
    runtime:destroy_field_space(ctx, circuit.all_wires:get_field_space())
    runtime:destroy_field_space(ctx, circuit.node_locator:get_field_space())
    runtime:destroy_index_space(ctx, circuit.all_nodes:get_index_space())
    runtime:destroy_index_space(ctx, circuit.all_wires:get_index_space())
  end
end

function update_mappers(machine, runtime, local_procs)
  local_procs:itr(function(local_proc)
    local mapper =
      legion:create_mapper("CircuitMapper", machine, runtime, local_proc)
    runtime:replace_default_mapper(mapper, local_proc)
  end)
end

function legion_main(arg)
  legion:set_top_level_task_id(TOP_LEVEL_TASK_ID)
  legion:register_lua_task_void("top_level_task", TOP_LEVEL_TASK_ID,
    legion.LOC_PROC, true, false)
  TaskHelper:register_cpu_variants(CalcNewCurrentsTask)
  TaskHelper:register_cpu_variants(DistributeChargeTask)
  TaskHelper:register_cpu_variants(UpdateVoltagesTask)
  CheckTask:register_task()
  legion:register_reduction_op(legion.PLUS_OP, float, REDUCE_ID)
  legion:set_registration_callback("update_mappers")
  legion:start(arg)
end

function parse_input_args(arg,
  num_loops, num_pieces, nodes_per_piece, wires_per_piece,
  pct_wire_in_piece, random_seed, steps, sync, perform_checks,
  dump_values)

  for i = 1, #arg do
    if arg[i] == "-l" then
      i = i + 1
      num_loops = tonumber(arg[i])
    elseif arg[i] == "-i" then
      i = i + 1
      steps = tonumber(arg[i])
    elseif arg[i] == "-p" then
      i = i + 1
      num_pieces = tonumber(arg[i])
    elseif arg[i] == "-npp" then
      i = i + 1
      nodes_per_piece = tonumber(arg[i])
    elseif arg[i] == "-wpp" then
      i = i + 1
      wires_per_piece = tonumber(arg[i])
    elseif arg[i] == "-pct" then
      i = i + 1
      pct_wire_in_piece = tonumber(arg[i])
    elseif arg[i] == "-s" then
      i = i + 1
      random_seed = tonumber(arg[i])
    elseif arg[i] == "-sync" then
      i = i + 1
      sync = tonumber(arg[i])
    elseif arg[i] == "-checks" then
      i = i + 1
      perform_checks = true
    elseif arg[i] == "-dump" then
      i = i + 1
      dump_values = true
    end
  end

  return
    num_loops, num_pieces, nodes_per_piece, wires_per_piece,
    pct_wire_in_piece, random_seed, steps, sync, perform_checks,
    dump_values
end

function allocate_node_fields(ctx, runtime, node_space)
  local allocator = runtime:create_field_allocator(ctx, node_space)
  allocator:allocate_field(sizeof(float), FID_NODE_CAP)
  allocator:allocate_field(sizeof(float), FID_LEAKAGE)
  allocator:allocate_field(sizeof(float), FID_CHARGE)
  allocator:allocate_field(sizeof(float), FID_NODE_VOLTAGE)
end

function allocate_wire_fields(ctx, runtime, wire_space)
  local allocator = runtime:create_field_allocator(ctx, wire_space)
  allocator:allocate_field(sizeof(ptr_t), FID_IN_PTR)
  allocator:allocate_field(sizeof(ptr_t), FID_OUT_PTR)
  allocator:allocate_field(sizeof(PointerLocation), FID_IN_LOC)
  allocator:allocate_field(sizeof(PointerLocation), FID_OUT_LOC)
  allocator:allocate_field(sizeof(float), FID_INDUCTANCE)
  allocator:allocate_field(sizeof(float), FID_RESISTANCE)
  allocator:allocate_field(sizeof(float), FID_WIRE_CAP)
  for i = 0, WIRE_SEGMENTS - 1 do
    allocator:allocate_field(sizeof(float), FID_CURRENT + i)
  end
  for i = 0, WIRE_SEGMENTS - 2 do
    allocator:allocate_field(sizeof(float), FID_WIRE_VOLTAGE + i)
  end
end

function allocate_locator_fields(ctx, runtime, locator_space)
  local allocator = runtime:create_field_allocator(ctx, locator_space)
  allocator:allocate_field(sizeof(PointerLocation), FID_LOCATOR)
end

function find_location(ptr, private_nodes, shared_nodes, ghost_nodes)
  if private_nodes:elem(ptr) then
    return PRIVATE_PTR
  end
  if shared_nodes:elem(ptr) then
    return SHARED_PTR
  end
  if ghost_nodes:elem(ptr) then
    return GHOST_PTR
  end
  -- Should never make it here, if we do something bad happened
  assert(false)
end

function random_element(array)
  local index = math.floor(drand48() * array.size)
  return array[index]
end

function load_circuit(ckt, ctx, runtime, num_pieces, nodes_per_piece,
                      wires_per_piece, pct_wire_in_piece, random_seed, steps)
  local pieces = {}
  print("Initializing circuit simulation...")
  -- inline map physical instances for the nodes and wire regions
  local wires_req = RegionRequirement:new(ckt.all_wires,
                                          legion.READ_WRITE,
                                          legion.EXCLUSIVE,
                                          ckt.all_wires)
  wires_req:add_field(FID_IN_PTR)
  wires_req:add_field(FID_OUT_PTR)
  wires_req:add_field(FID_IN_LOC)
  wires_req:add_field(FID_OUT_LOC)
  wires_req:add_field(FID_INDUCTANCE)
  wires_req:add_field(FID_RESISTANCE)
  wires_req:add_field(FID_WIRE_CAP)
  for i = 0, WIRE_SEGMENTS - 1 do
    wires_req:add_field(FID_CURRENT + i)
  end
  for i = 0, WIRE_SEGMENTS - 2 do
    wires_req:add_field(FID_WIRE_VOLTAGE + i)
  end
  local nodes_req = RegionRequirement:new(ckt.all_nodes,
                                          legion.READ_WRITE,
                                          legion.EXCLUSIVE,
                                          ckt.all_nodes)
  nodes_req:add_field(FID_NODE_CAP)
  nodes_req:add_field(FID_LEAKAGE)
  nodes_req:add_field(FID_CHARGE)
  nodes_req:add_field(FID_NODE_VOLTAGE)
  local locator_req = RegionRequirement:new(ckt.node_locator,
                                            legion.READ_WRITE,
                                            legion.EXCLUSIVE,
                                            ckt.node_locator)
  locator_req:add_field(FID_LOCATOR)
  local wires = runtime:map_region(ctx, wires_req)
  local nodes = runtime:map_region(ctx, nodes_req)
  local locator = runtime:map_region(ctx, locator_req)

  local wire_owner_map = Coloring:new()
  local private_node_map = Coloring:new()
  local shared_node_map = Coloring:new()
  local ghost_node_map = Coloring:new()
  local locator_node_map = Coloring:new()

  local privacy_map = Coloring:new()
  privacy_map:ensure_color(0)
  privacy_map:ensure_color(1)

  -- keep a O(1) indexable list of nodes in each piece for connecting wires
  local piece_shared_nodes = Array:init(num_pieces, 0)

  srand48(random_seed)

  nodes:wait_until_valid()
  local fa_node_cap =
    nodes:get_field_accessor(FID_NODE_CAP):typeify(float)
  local fa_node_leakage =
    nodes:get_field_accessor(FID_LEAKAGE):typeify(float)
  local fa_node_charge =
    nodes:get_field_accessor(FID_CHARGE):typeify(float)
  local fa_node_voltage =
    nodes:get_field_accessor(FID_NODE_VOLTAGE):typeify(float)
  locator:wait_until_valid()
  local locator_acc =
    locator:get_field_accessor(FID_LOCATOR):typeify(PointerLocation)
  local first_nodes = {}
  do
    local node_allocator = runtime:create_index_allocator(ctx, ckt.all_nodes:get_index_space())
    node_allocator:alloc(num_pieces * nodes_per_piece)
  end
  local piece_node_ptrs = {}
  do
    local itr = IndexIterator:new(ckt.all_nodes:get_index_space())
    for n = 0, num_pieces - 1 do
      piece_node_ptrs[n] = Array:new()
      for i = 0, nodes_per_piece - 1 do
        assert(itr:has_next())
        local node_ptr = itr:next()
        if i == 0 then
          first_nodes[n] = node_ptr
        end
        local capacitance = drand48() + 1.0
        fa_node_cap:write(node_ptr, capacitance)
        local leakage = 0.1 * drand48()
        fa_node_leakage:write(node_ptr, leakage)
        fa_node_charge:write(node_ptr, 0.0)
        local init_voltage = 2 * drand48() - 1.0
        fa_node_voltage:write(node_ptr, init_voltage)
        -- Just put everything in everyones private map at the moment
        -- We'll pull pointers out of here later as nodes get tied to
        -- wires that are non-local
        private_node_map[n].points:insert(node_ptr)
        privacy_map[0].points:insert(node_ptr)
        locator_node_map[n].points:insert(node_ptr)
	      piece_node_ptrs[n]:insert(node_ptr)
      end
    end
  end

  wires:wait_until_valid()
  local fa_wire_currents = {}
  for i = 0, WIRE_SEGMENTS - 1 do
    fa_wire_currents[i] = wires:get_field_accessor(FID_CURRENT+i):typeify(float)
  end
  local fa_wire_voltages = {}
  for i = 0, WIRE_SEGMENTS - 2 do
    fa_wire_voltages[i] = wires:get_field_accessor(FID_WIRE_VOLTAGE+i):typeify(float)
  end
  local fa_wire_in_ptr =
    wires:get_field_accessor(FID_IN_PTR):typeify(ptr_t)
  local fa_wire_out_ptr =
    wires:get_field_accessor(FID_OUT_PTR):typeify(ptr_t)
  local fa_wire_in_loc =
    wires:get_field_accessor(FID_IN_LOC):typeify(PointerLocation)
  local fa_wire_out_loc =
    wires:get_field_accessor(FID_OUT_LOC):typeify(PointerLocation)
  local fa_wire_inductance =
    wires:get_field_accessor(FID_INDUCTANCE):typeify(float)
  local fa_wire_resistance =
    wires:get_field_accessor(FID_RESISTANCE):typeify(float)
  local fa_wire_cap =
    wires:get_field_accessor(FID_WIRE_CAP):typeify(float)
  local first_wires = {}
  -- Allocate all the wires
  do
    local wire_allocator = runtime:create_index_allocator(ctx, ckt.all_wires:get_index_space())
    wire_allocator:alloc(num_pieces * wires_per_piece)
  end
  do
    local itr = IndexIterator:new(ckt.all_wires:get_index_space())
    for n = 0, num_pieces - 1 do
      for i = 0, wires_per_piece - 1 do
        assert(itr:has_next())
        local wire_ptr = itr:next()
        -- Record the first wire pointer for this piece
        if i == 0 then
          first_wires[n] = wire_ptr
        end
        for j = 0, WIRE_SEGMENTS - 1 do
          fa_wire_currents[j]:write(wire_ptr, 0.0)
        end
        for j = 0, WIRE_SEGMENTS - 2 do
          fa_wire_voltages[j]:write(wire_ptr, 0.0)
        end

        local resistance = drand48() * 10.0 + 1.0
        fa_wire_resistance:write(wire_ptr, resistance)
        -- Keep inductance on the order of 1e-3 * dt to avoid resonance problems
        local inductance = (drand48() + 0.1) * DELTAT * 1e-3
        fa_wire_inductance:write(wire_ptr, inductance)
        local capacitance = drand48() * 0.1
        fa_wire_cap:write(wire_ptr, capacitance)

        fa_wire_in_ptr:write(wire_ptr, random_element(piece_node_ptrs[n]))

        if ((100 * drand48()) < pct_wire_in_piece) then
          fa_wire_out_ptr:write(wire_ptr, random_element(piece_node_ptrs[n]))
        else
          -- pick a random other piece and a node from there
          local nn = math.floor(drand48() * (num_pieces - 1))
          if nn >= n then nn = nn + 1 end

          -- pick an arbitrary node, except that if it's one that didn't used to be shared, make the
          --  sequentially next pointer shared instead so that each node's shared pointers stay compact
          local idx = math.floor(drand48() * piece_node_ptrs[nn].size)
          if idx > piece_shared_nodes[nn] then
            idx = piece_shared_nodes[nn]
            piece_shared_nodes[nn] = piece_shared_nodes[nn] + 1
          end
          local out_ptr = piece_node_ptrs[nn][idx]

          fa_wire_out_ptr:write(wire_ptr, out_ptr)
          -- This node is no longer private
          privacy_map[0].points:erase(out_ptr)
          privacy_map[1].points:insert(out_ptr)
          ghost_node_map[n].points:insert(out_ptr)
        end
        wire_owner_map[n].points:insert(wire_ptr)
      end
    end
  end

  -- Second pass: make some random fraction of the private nodes shared
  do
    local itr = IndexIterator:new(ckt.all_nodes:get_index_space())
    for n = 0, num_pieces - 1 do
      for i = 0, nodes_per_piece - 1 do
        assert(itr:has_next())
        local node_ptr = itr:next()
        if not privacy_map[0].points:elem(node_ptr) then
          private_node_map[n].points:erase(node_ptr)
          -- node is now shared
          shared_node_map[n].points:insert(node_ptr)
          locator_acc:write(node_ptr,SHARED_PTR) -- node is shared
        else
          locator_acc:write(node_ptr,PRIVATE_PTR) -- node is private
        end
      end
    end
  end
  -- Second pass (part 2): go through the wires and update the locations
  do
    local itr = IndexIterator:new(ckt.all_wires:get_index_space())
    for n = 0, num_pieces - 1 do
      for i = 0, wires_per_piece - 1 do
        assert(itr:has_next())
        local wire_ptr = itr:next()
        local in_ptr = fa_wire_in_ptr:read(wire_ptr)
        local out_ptr = fa_wire_out_ptr:read(wire_ptr)

        fa_wire_in_loc:write(wire_ptr,
            find_location(in_ptr, private_node_map[n].points,
              shared_node_map[n].points, ghost_node_map[n].points))
        fa_wire_out_loc:write(wire_ptr,
            find_location(out_ptr, private_node_map[n].points,
              shared_node_map[n].points, ghost_node_map[n].points))
      end
    end
  end

  runtime:unmap_region(ctx, wires)
  runtime:unmap_region(ctx, nodes)
  runtime:unmap_region(ctx, locator)

  -- Now we can create our partitions and update the circuit pieces

  -- first create the privacy partition that splits all the nodes into either shared or private
  local privacy_part = runtime:create_index_partition(ctx, ckt.all_nodes:get_index_space(), privacy_map, true)

  local all_private = runtime:get_index_subspace(ctx, privacy_part, 0)
  local all_shared  = runtime:get_index_subspace(ctx, privacy_part, 1)

  -- Now create partitions for each of the subregions
  local result = {}
  local priv = runtime:create_index_partition(ctx, all_private, private_node_map, true)
  result.pvt_nodes = runtime:get_logical_partition_by_tree(ctx, priv, ckt.all_nodes:get_field_space(), ckt.all_nodes:get_tree_id())
  local shared = runtime:create_index_partition(ctx, all_shared, shared_node_map, true)
  result.shr_nodes = runtime:get_logical_partition_by_tree(ctx, shared, ckt.all_nodes:get_field_space(), ckt.all_nodes:get_tree_id())
  local ghost = runtime:create_index_partition(ctx, all_shared, ghost_node_map, false)
  result.ghost_nodes = runtime:get_logical_partition_by_tree(ctx, ghost, ckt.all_nodes:get_field_space(), ckt.all_nodes:get_tree_id())

  local pvt_wires = runtime:create_index_partition(ctx, ckt.all_wires:get_index_space(), wire_owner_map, true)
  result.pvt_wires = runtime:get_logical_partition_by_tree(ctx, pvt_wires, ckt.all_wires:get_field_space(), ckt.all_wires:get_tree_id())

  local locs = runtime:create_index_partition(ctx, ckt.node_locator:get_index_space(), locator_node_map, true)
  result.node_locations = runtime:get_logical_partition_by_tree(ctx, locs, ckt.node_locator:get_field_space(), ckt.node_locator:get_tree_id())

  -- Build the pieces
  for n = 0, num_pieces - 1 do
    pieces[n] = {}
    pieces[n].pvt_nodes = runtime:get_logical_subregion_by_color(ctx, result.pvt_nodes, n)
    pieces[n].shr_nodes = runtime:get_logical_subregion_by_color(ctx, result.shr_nodes, n)
    pieces[n].ghost_nodes = runtime:get_logical_subregion_by_color(ctx, result.ghost_nodes, n)
    pieces[n].pvt_wires = runtime:get_logical_subregion_by_color(ctx, result.pvt_wires, n)
    pieces[n].num_wires = wires_per_piece
    pieces[n].first_wire = first_wires[n]
    pieces[n].num_nodes = nodes_per_piece
    pieces[n].first_node = first_nodes[n]

    pieces[n].dt = DELTAT
    pieces[n].steps = steps
  end

  print("Finished initializing simulation...")

  return result, pieces
end

if rawget(_G, "arg") then
  legion_main(arg)
end
