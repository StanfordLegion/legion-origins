CircuitMapper = {}
CircuitMapper.__index = CircuitMapper

function CircuitMapper:new(machine, runtime, local_proc)
  local all_procs = machine:get_all_processors()
  local all_cpus = Array:new{}
  all_procs:itr(function(proc)
    if proc:kind() == legion.LOC_PROC then
      all_cpus:insert(proc)
    end
  end)
  local mapper = { all_cpus = all_cpus }
  setmetatable(mapper, self)
  return mapper
end

function CircuitMapper:slice_domain(task, domain, slices)
  self:decompose_index_space(domain, self.all_cpus, 1, slices)
end

function CircuitMapper:map_task(task)
  local sys_mem =
    self.machine_interface:find_memory_kind(task.target_proc,
                                            legion.SYSTEM_MEM)
  for idx = 0, task.regions.size - 1 do
    task.regions[idx].target_ranking:insert(sys_mem)
    task.regions[idx].virtual_map = false
    task.regions[idx].enable_WAR_optimization = self.war_enabled
    task.regions[idx].reduction_list = false
    task.regions[idx].blocking_factor =
      task.regions[idx].max_blocking_factor
  end
  return false
end

function CircuitMapper:map_inline(inline)
  local ret = DefaultMapper:map_inline(self, inline)
  local req = inline.requirement
  req.blocking_factor = req.max_blocking_factor
  return ret
end
