require "circuit_defs"

--
-- CalcNewCurrentsTask
--

CalcNewCurrentsTask.TASK_NAME = "calc_new_currents"
CalcNewCurrentsTask.TASK_ID = CALC_NEW_CURRENTS_TASK_ID
CalcNewCurrentsTask.CPU_BASE_LEAF = true
CalcNewCurrentsTask.MAPPER_ID = 0

function calc_new_currents(task, regions, ctx, runtime)
  local p = task:get_local_args(CircuitPiece)
  CalcNewCurrentsTask:cpu_base_impl(p, regions)
end

function CalcNewCurrentsTask:new(lp_pvt_wires,
                                 lp_pvt_nodes,
                                 lp_shr_nodes,
                                 lp_ghost_nodes,
                                 lr_all_wires,
                                 lr_all_nodes,
                                 launch_domain,
                                 arg_map)
  local launcher =
    IndexLauncher:new(CalcNewCurrentsTask.TASK_ID, launch_domain,
                      nil, arg_map, Predicate.TRUE_PRED,
                      false, CalcNewCurrentsTask.MAPPER_ID)

  local rr_out = RegionRequirement:new(lp_pvt_wires, 0, legion.READ_WRITE,
                                       legion.EXCLUSIVE, lr_all_wires)
  for i = 0, WIRE_SEGMENTS - 1 do
    rr_out:add_field(FID_CURRENT + i)
  end
  for i = 0, WIRE_SEGMENTS - 2 do
    rr_out:add_field(FID_WIRE_VOLTAGE + i)
  end
  launcher:add_region_requirement(rr_out)

  local rr_wires = RegionRequirement:new(lp_pvt_wires, 0, legion.READ_ONLY,
                                         legion.EXCLUSIVE, lr_all_wires)
  rr_wires:add_field(FID_IN_PTR)
  rr_wires:add_field(FID_OUT_PTR)
  rr_wires:add_field(FID_IN_LOC)
  rr_wires:add_field(FID_OUT_LOC)
  rr_wires:add_field(FID_INDUCTANCE)
  rr_wires:add_field(FID_RESISTANCE)
  rr_wires:add_field(FID_WIRE_CAP)
  launcher:add_region_requirement(rr_wires)

  local rr_private = RegionRequirement:new(lp_pvt_nodes, 0, legion.READ_ONLY,
                                           legion.EXCLUSIVE, lr_all_nodes)
  rr_private:add_field(FID_NODE_VOLTAGE)
  launcher:add_region_requirement(rr_private)

  local rr_shared = RegionRequirement:new(lp_shr_nodes, 0, legion.READ_ONLY,
                                          legion.EXCLUSIVE, lr_all_nodes)
  rr_shared:add_field(FID_NODE_VOLTAGE)
  launcher:add_region_requirement(rr_shared)

  local rr_ghost = RegionRequirement:new(lp_ghost_nodes, 0, legion.READ_ONLY,
                                         legion.EXCLUSIVE, lr_all_nodes)
  rr_ghost:add_field(FID_NODE_VOLTAGE)
  launcher:add_region_requirement(rr_ghost)

  launcher = inherit(launcher, self)
  return launcher
end

function CalcNewCurrentsTask:launch_check_fields(ctx, runtime)
  local req = self.region_requirements[0]
  local success = true
  for i = 0, WIRE_SEGMENTS - 1 do
    local launcher = CheckTask:new(req.partition, req.parent, FID_CURRENT+i,
                                   self.launch_domain, self.argument_map)
    success = launcher:dispatch(ctx, runtime, success)
  end
  for i = 0, WIRE_SEGMENTS - 2 do
    local launcher = CheckTask:new(req.partition, req.parent, FID_WIRE_VOLTAGE+i,
                                   self.launch_domain, self.argument_map)
    success = launcher:dispatch(ctx, runtime, success)
  end
  return success
end

local function get_node_voltage(priv, shr, ghost, loc, ptr)
  if loc == PRIVATE_PTR then
    return priv:read(ptr)
  elseif loc == SHARED_PTR then
    return shr:read(ptr)
  elseif loc == GHOST_PTR then
    return ghost:read(ptr)
  else
    assert(false)
  end
end

function CalcNewCurrentsTask:cpu_base_impl(p, regions)
  local fa_current = {}
  for i = 0, WIRE_SEGMENTS - 1 do
    fa_current[i] = regions[0]:get_field_accessor(FID_CURRENT+i):typeify(float)
  end
  local fa_voltage = {}
  for i = 0, WIRE_SEGMENTS - 2 do
    fa_voltage[i] = regions[0]:get_field_accessor(FID_WIRE_VOLTAGE+i):typeify(float)
  end
  local fa_in_ptr =
    regions[1]:get_field_accessor(FID_IN_PTR):typeify(ptr_t)
  local fa_out_ptr =
    regions[1]:get_field_accessor(FID_OUT_PTR):typeify(ptr_t)
  local fa_in_loc =
    regions[1]:get_field_accessor(FID_IN_LOC):typeify(PointerLocation)
  local fa_out_loc =
    regions[1]:get_field_accessor(FID_OUT_LOC):typeify(PointerLocation)
  local fa_inductance =
    regions[1]:get_field_accessor(FID_INDUCTANCE):typeify(float)
  local fa_resistance =
    regions[1]:get_field_accessor(FID_RESISTANCE):typeify(float)
  local fa_wire_cap =
    regions[1]:get_field_accessor(FID_WIRE_CAP):typeify(float)
  local fa_pvt_voltage =
    regions[2]:get_field_accessor(FID_NODE_VOLTAGE):typeify(float)
  local fa_shr_voltage =
    regions[3]:get_field_accessor(FID_NODE_VOLTAGE):typeify(float)
  local fa_ghost_voltage =
    regions[4]:get_field_accessor(FID_NODE_VOLTAGE):typeify(float)

  -- See if we can do the dense version with vector instructions
  local itr = IndexIterator:new(p.pvt_wires)
  local temp_v = {}
  local temp_i = {}
  local old_i = {}
  local old_v = {}

  local dt = p.dt
  local recip_dt = 1.0 / dt
  local steps = p.steps
  while itr:has_next() do
    local wire_ptr = itr:next()

    for i = 0, WIRE_SEGMENTS - 1 do
      temp_i[i] = fa_current[i]:read(wire_ptr)
      old_i[i] = temp_i[i]
    end
    for i = 0, WIRE_SEGMENTS - 2 do
      temp_v[i+1] = fa_voltage[i]:read(wire_ptr)
      old_v[i] = temp_v[i+1]
    end

    -- Pin the outer voltages to the node voltages
    local in_ptr = fa_in_ptr:read(wire_ptr)
    local in_loc = fa_in_loc:read(wire_ptr)
    temp_v[0] =
      get_node_voltage(fa_pvt_voltage, fa_shr_voltage, fa_ghost_voltage, in_loc, in_ptr)
    local out_ptr = fa_out_ptr:read(wire_ptr)
    local out_loc = fa_out_loc:read(wire_ptr)
    temp_v[WIRE_SEGMENTS] =
      get_node_voltage(fa_pvt_voltage, fa_shr_voltage, fa_ghost_voltage, out_loc, out_ptr)

    -- Solve the RLC model iteratively
    local inductance = fa_inductance:read(wire_ptr)
    local recip_resistance = 1.0 / fa_resistance:read(wire_ptr)
    local recip_capacitance = 1.0 / fa_wire_cap:read(wire_ptr)
    for j = 0, steps - 1 do
      -- first, figure out the new current from the voltage differential
      -- and our inductance:
      -- dV = R*I + L*I' ==> I = (dV - L*I')/R
      for i = 0, WIRE_SEGMENTS - 1 do
        temp_i[i] = ((temp_v[i+1] - temp_v[i]) -
                     (inductance * (temp_i[i] - old_i[i]) * recip_dt)) * recip_resistance
      end
      -- Now update the inter-node voltages
      for i = 0, WIRE_SEGMENTS - 2 do
        temp_v[i+1] = old_v[i] + dt * (temp_i[i] - temp_i[i+1]) * recip_capacitance
      end
    end

    -- Write out the results
    for i = 0, WIRE_SEGMENTS - 1 do
      fa_current[i]:write(wire_ptr, temp_i[i])
    end
    for i = 0, WIRE_SEGMENTS - 2 do
      fa_voltage[i]:write(wire_ptr, temp_v[i+1])
    end
  end
end

--
-- DistributeChargeTask
--

DistributeChargeTask.TASK_NAME = "distribute_charge"
DistributeChargeTask.TASK_ID = DISTRIBUTE_CHARGE_TASK_ID
DistributeChargeTask.CPU_BASE_LEAF = true
DistributeChargeTask.MAPPER_ID = 0

function distribute_charge(task, regions, ctx, runtime)
  local p = task:get_local_args(CircuitPiece)
  DistributeChargeTask:cpu_base_impl(p, regions)
end

function DistributeChargeTask:new(lp_pvt_wires,
                                 lp_pvt_nodes,
                                 lp_shr_nodes,
                                 lp_ghost_nodes,
                                 lr_all_wires,
                                 lr_all_nodes,
                                 launch_domain,
                                 arg_map)
  local launcher =
    IndexLauncher:new(DistributeChargeTask.TASK_ID, launch_domain,
                      nil, arg_map, Predicate.TRUE_PRED,
                      false, DistributeChargeTask.MAPPER_ID)

  local rr_wires = RegionRequirement:new(lp_pvt_wires, 0, legion.READ_ONLY,
                                         legion.EXCLUSIVE, lr_all_wires)
  rr_wires:add_field(FID_IN_PTR)
  rr_wires:add_field(FID_OUT_PTR)
  rr_wires:add_field(FID_IN_LOC)
  rr_wires:add_field(FID_OUT_LOC)
  rr_wires:add_field(FID_CURRENT)
  rr_wires:add_field(FID_CURRENT+WIRE_SEGMENTS-1)
  launcher:add_region_requirement(rr_wires)

  local rr_private = RegionRequirement:new(lp_pvt_nodes, 0, legion.READ_WRITE,
                                           legion.EXCLUSIVE, lr_all_nodes)
  rr_private:add_field(FID_CHARGE)
  launcher:add_region_requirement(rr_private)

  local rr_shared = RegionRequirement:new(lp_shr_nodes, 0, REDUCE_ID,
                                          legion.SIMULTANEOUS, lr_all_nodes)
  rr_shared:set_reduction()
  rr_shared:add_field(FID_CHARGE)
  launcher:add_region_requirement(rr_shared)

  local rr_ghost = RegionRequirement:new(lp_ghost_nodes, 0, REDUCE_ID,
                                         legion.SIMULTANEOUS, lr_all_nodes)
  rr_ghost:set_reduction()
  rr_ghost:add_field(FID_CHARGE)
  launcher:add_region_requirement(rr_ghost)

  launcher = inherit(launcher, self)
  return launcher
end

function DistributeChargeTask:launch_check_fields(ctx, runtime)
  local success = true
  for idx = 1, 3 do
    local req = self.region_requirements[idx]
    local launcher = CheckTask:new(req.partition, req.parent, FID_CHARGE,
                                   self.launch_domain, self.argument_map)
    success = launcher:dispatch(ctx, runtime, success)
  end
  return success
end

local function reduce_node(priv, shr, ghost, loc, ptr, value)
  if loc == PRIVATE_PTR then
    priv:reduce(legion.PLUS_OP, ptr, value)
  elseif loc == SHARED_PTR then
    shr:reduce(ptr, value)
  elseif loc == GHOST_PTR then
    ghost:reduce(ptr, value)
  else
    assert(false)
  end
end

function DistributeChargeTask:cpu_base_impl(p, regions)
  local fa_in_ptr =
    regions[0]:get_field_accessor(FID_IN_PTR):typeify(ptr_t)
  local fa_out_ptr =
    regions[0]:get_field_accessor(FID_OUT_PTR):typeify(ptr_t)
  local fa_in_loc =
    regions[0]:get_field_accessor(FID_IN_LOC):typeify(PointerLocation)
  local fa_out_loc =
    regions[0]:get_field_accessor(FID_OUT_LOC):typeify(PointerLocation)
  local fa_in_current =
    regions[0]:get_field_accessor(FID_CURRENT):typeify(float)
  local fa_out_current =
    regions[0]:get_field_accessor(FID_CURRENT+WIRE_SEGMENTS-1):typeify(float)
  local fa_pvt_charge =
    regions[1]:get_field_accessor(FID_CHARGE):typeify(float)
  local fa_shr_temp = regions[2]:get_accessor():typeify(float)
  local fa_ghost_temp = regions[3]:get_accessor():typeify(float)
  local fa_shr_charge = fa_shr_temp:convert(legion.PLUS_OP)
  local fa_ghost_charge = fa_ghost_temp:convert(legion.PLUS_OP)

  local itr = IndexIterator:new(p.pvt_wires)
  local dt = p.dt
  while itr:has_next() do
    local wire_ptr = itr:next()
    local in_current = -dt * fa_in_current:read(wire_ptr)
    local out_current = dt * fa_out_current:read(wire_ptr)
    local in_ptr = fa_in_ptr:read(wire_ptr)
    local out_ptr = fa_out_ptr:read(wire_ptr)
    local in_loc = fa_in_loc:read(wire_ptr)
    local out_loc = fa_out_loc:read(wire_ptr)

    reduce_node(fa_pvt_charge, fa_shr_charge, fa_ghost_charge,
                in_loc, in_ptr, in_current)
    reduce_node(fa_pvt_charge, fa_shr_charge, fa_ghost_charge,
                out_loc, out_ptr, out_current)
  end
end

--
-- UpdateVoltagesTask
--

UpdateVoltagesTask.TASK_NAME = "update_voltages"
UpdateVoltagesTask.TASK_ID = UPDATE_VOLTAGES_TASK_ID
UpdateVoltagesTask.CPU_BASE_LEAF = true
UpdateVoltagesTask.MAPPER_ID = 0

function update_voltages(task, regions, ctx, runtime)
  local p = task:get_local_args(CircuitPiece)
  UpdateVoltagesTask:cpu_base_impl(p, regions)
end

function UpdateVoltagesTask:new(lp_pvt_nodes,
                                lp_shr_nodes,
                                lp_node_locations,
                                lr_all_nodes,
                                lr_node_locator,
                                launch_domain,
                                arg_map)
  local launcher =
    IndexLauncher:new(UpdateVoltagesTask.TASK_ID, launch_domain,
                      nil, arg_map, Predicate.TRUE_PRED,
                      false, UpdateVoltagesTask.MAPPER_ID)

  local rr_private_out = RegionRequirement:new(lp_pvt_nodes, 0,
                                               legion.READ_WRITE,
                                               legion.EXCLUSIVE,
                                               lr_all_nodes)
  rr_private_out:add_field(FID_NODE_VOLTAGE)
  rr_private_out:add_field(FID_CHARGE)
  launcher:add_region_requirement(rr_private_out)

  local rr_shared_out = RegionRequirement:new(lp_shr_nodes, 0,
                                              legion.READ_WRITE,
                                              legion.EXCLUSIVE,
                                              lr_all_nodes)
  rr_shared_out:add_field(FID_NODE_VOLTAGE)
  rr_shared_out:add_field(FID_CHARGE)
  launcher:add_region_requirement(rr_shared_out)

  local rr_private_in = RegionRequirement:new(lp_pvt_nodes, 0,
                                              legion.READ_ONLY,
                                              legion.EXCLUSIVE,
                                              lr_all_nodes)
  rr_private_in:add_field(FID_NODE_CAP)
  rr_private_in:add_field(FID_LEAKAGE)
  launcher:add_region_requirement(rr_private_in)

  local rr_shared_in = RegionRequirement:new(lp_shr_nodes, 0,
                                             legion.READ_ONLY,
                                             legion.EXCLUSIVE,
                                             lr_all_nodes)
  rr_shared_in:add_field(FID_NODE_CAP)
  rr_shared_in:add_field(FID_LEAKAGE)
  launcher:add_region_requirement(rr_shared_in)

  local rr_locator = RegionRequirement:new(lp_node_locations, 0,
                                           legion.READ_ONLY,
                                           legion.EXCLUSIVE,
                                           lr_node_locator)
  rr_locator:add_field(FID_LOCATOR)
  launcher:add_region_requirement(rr_locator)

  launcher = inherit(launcher, self)
  return launcher
end

function UpdateVoltagesTask:launch_check_fields(ctx, runtime)
  local success = true
  local req = self.region_requirements[0]
  do
    local launcher = CheckTask:new(req.partition, req.parent, FID_NODE_VOLTAGE,
                                   self.launch_domain, self.argument_map)
    success = launcher:dispatch(ctx, runtime, success)
  end
  do
    local launcher = CheckTask:new(req.partition, req.parent, FID_CHARGE,
                                   self.launch_domain, self.argument_map)
    success = launcher:dispatch(ctx, runtime, success)
  end
  return success
end

local function update_voltage(lr, fa_voltage, fa_charge, fa_cap, fa_leakage)
  local itr = IndexIterator:new(lr)
  while itr:has_next() do
    local node_ptr = itr:next()
    local voltage = fa_voltage:read(node_ptr)
    local charge = fa_charge:read(node_ptr)
    local capacitance = fa_cap:read(node_ptr)
    local leakage = fa_leakage:read(node_ptr)
    voltage = voltage + charge / capacitance
    voltage = voltage * (1.0 - leakage)
    fa_voltage:write(node_ptr, voltage)
    -- Reset the charge for the next iteration
    fa_charge:write(node_ptr, 0.0)
  end
end

function UpdateVoltagesTask:cpu_base_impl(p, regions)
  local fa_pvt_voltage =
    regions[0]:get_field_accessor(FID_NODE_VOLTAGE):typeify(float)
  local fa_pvt_charge =
    regions[0]:get_field_accessor(FID_CHARGE):typeify(float)
  local fa_shr_voltage =
    regions[1]:get_field_accessor(FID_NODE_VOLTAGE):typeify(float)
  local fa_shr_charge =
    regions[1]:get_field_accessor(FID_CHARGE):typeify(float)
  local fa_pvt_cap =
    regions[2]:get_field_accessor(FID_NODE_CAP):typeify(float)
  local fa_pvt_leakage =
    regions[2]:get_field_accessor(FID_LEAKAGE):typeify(float)
  local fa_shr_cap =
    regions[3]:get_field_accessor(FID_NODE_CAP):typeify(float)
  local fa_shr_leakage =
    regions[3]:get_field_accessor(FID_LEAKAGE):typeify(float)
  -- Don't need this for the CPU version
  --local fa_location =
  --  regions[4]:get_field_accessor(FID_LOCATOR):typeify(PointerLocation)

  update_voltage(p.pvt_nodes, fa_pvt_voltage, fa_pvt_charge,
                 fa_pvt_cap, fa_pvt_leakage)
  update_voltage(p.shr_nodes, fa_shr_voltage, fa_shr_charge,
                 fa_shr_cap, fa_shr_leakage)
end

--
-- CheckTask
--

CheckTask.TASK_NAME = "check_task"
CheckTask.TASK_ID = CHECK_FIELD_TASK_ID
CheckTask.LEAF = true
CheckTask.MAPPER_ID = 0

function CheckTask:new(lp, lr, fid, launch_domain, arg_map)
  local launcher = IndexLauncher:new(CheckTask.TASK_ID, launch_domain, nil,
                                     arg_map, Predicate.TRUE_PRED,
                                     false, CheckTask.MAPPER_ID)
  local rr_check = RegionRequirement:new(lp, 0, legion.READ_ONLY,
                                         legion.EXCLUSIVE, lr)
  rr_check:add_field(fid)
  launcher:add_region_requirement(rr_check)
  launcher = inherit(launcher, self)
  return launcher
end

function CheckTask:dispatch(ctx, runtime, success)
  local fm = runtime:execute_index_space(ctx, self)
  fm:wait_all_results()
  local launch_array = self.launch_domain:get_rect()
  local pir = GenericPointInRectIterator:new(launch_array)
  while(pir:has_next()) do
    success = fm:get_result(bool, DomainPoint:from_point(pir.p)) and success
    pir:next()
  end
  return success
end

function check_task(task, regions, ctx, runtime)
  local fa_check =
    regions[0]:get_field_accessor(task.regions[0].instance_fields[0]):typeify(float)
  local lr = task.regions[0].region
  local itr = IndexIterator:new(lr)
  local success = true
  while itr:has_next() and success do
    local ptr = itr:next()
    local value = fa_check:read(ptr)
    if not value then
      success = false
    end
  end
  return success
end

function CheckTask:register_task()
  legion:register_lua_task(bool, CheckTask.TASK_NAME, CheckTask.TASK_ID,
                           legion.LOC_PROC, false, true,
                           CIRCUIT_CPU_LEAF_VARIANT,
                           TaskConfigOptions:new(CheckTask.LEAF))
end

