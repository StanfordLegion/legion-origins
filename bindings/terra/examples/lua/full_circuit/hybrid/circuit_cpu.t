require "circuit_defs"
require "legionlib-terra"

local c = terralib.includec("legion_c.h")
local std = terralib.includec("stdio.h")
local v = terralib.includec("x86intrin.h")

--
-- CalcNewCurrentsTask
--

local vec4 = vector(float, 4)

local terra get_vec_node_voltage(current_wire : ptr_t,
                                 priv  : accessor_array_t,
                                 shr   : accessor_array_t,
                                 ghost : accessor_array_t,
                                 ptrs  : accessor_array_t,
                                 locs  : accessor_array_t) : vec4
  var voltages : float[4]
  for i = 0, 4 do
    var ptr = ptr_t { value = current_wire.value + i }
    var node_ptr =
      @[&ptr_t](c.legion_accessor_array_ref(ptrs, ptr))
    var loc =
      @[&PointerLocation](c.legion_accessor_array_ref(locs, ptr))
    if loc == PRIVATE_PTR then
      voltages[i] = @[&float](c.legion_accessor_array_ref(priv, node_ptr))
    elseif loc == SHARED_PTR then
      voltages[i] = @[&float](c.legion_accessor_array_ref(shr, node_ptr))
    elseif loc == GHOST_PTR then
      voltages[i] = @[&float](c.legion_accessor_array_ref(ghost, node_ptr))
    else
      assert(false)
    end
  end
  return vector(voltages[0], voltages[1], voltages[2], voltages[3])
end

local terra get_node_voltage(priv  : accessor_array_t,
                             shr   : accessor_array_t,
                             ghost : accessor_array_t,
                             loc : PointerLocation,
                             ptr : ptr_t)
  var tmp : float
  if loc == PRIVATE_PTR then
    c.legion_accessor_array_read(priv, ptr, &tmp, sizeof(float))
    return tmp
  elseif loc == SHARED_PTR then
    c.legion_accessor_array_read(shr, ptr, &tmp, sizeof(float))
    return tmp
  elseif loc == GHOST_PTR then
    c.legion_accessor_array_read(ghost, ptr, &tmp, sizeof(float))
    return tmp
  else
    assert(false)
  end
end

terra dense_calc_new_currents(piece            : &CircuitPiece,
                              fa_in_ptr        : accessor_array_t,
                              fa_out_ptr       : accessor_array_t,
                              fa_in_loc        : accessor_array_t,
                              fa_out_loc       : accessor_array_t,
                              fa_inductance    : accessor_array_t,
                              fa_resistance    : accessor_array_t,
                              fa_wire_cap      : accessor_array_t,
                              fa_pvt_voltage   : accessor_array_t,
                              fa_shr_voltage   : accessor_array_t,
                              fa_ghost_voltage : accessor_array_t,
                              fa_current       : &accessor_array_t,
                              fa_voltage       : &accessor_array_t)

  var steps = piece.steps
  var index      : uint = 0
  var temp_v     : vec4[WIRE_SEGMENTS+1]
  var temp_i     : vec4[WIRE_SEGMENTS+1]
  var old_i      : vec4[WIRE_SEGMENTS]
  var old_v      : vec4[WIRE_SEGMENTS-1]
  var dt         : vec4  = piece.dt
  var recip_dt   : vec4  = 1.0 / piece.dt
  var first_wire : ptr_t = piece.first_wire

  while index + 3 < piece.num_wires do
    var current_wire : ptr_t = ptr_t { value = first_wire.value + index }
    for i = 0, WIRE_SEGMENTS do
      temp_i[i] = @[&vec4](c.legion_accessor_array_ref(fa_current[i], current_wire))
      old_i[i] = temp_i[i]
    end
    for i = 0, WIRE_SEGMENTS - 1 do
      temp_v[i + 1] = @[&vec4](c.legion_accessor_array_ref(fa_voltage[i], current_wire))
      old_v[i] = temp_v[i + 1]
    end
    temp_v[0] = get_vec_node_voltage(current_wire, fa_pvt_voltage,
                                     fa_shr_voltage, fa_ghost_voltage,
                                     fa_in_ptr, fa_in_loc)
    temp_v[WIRE_SEGMENTS] = get_vec_node_voltage(current_wire, fa_pvt_voltage,
                                                 fa_shr_voltage, fa_ghost_voltage,
                                                 fa_out_ptr, fa_out_loc)
    var inductance : vec4 = @[&vec4](c.legion_accessor_array_ref(fa_inductance, current_wire))
    var recip_resistance  : vec4 = 1.0 / @[&vec4](c.legion_accessor_array_ref(fa_resistance, current_wire))
    var recip_capacitance : vec4 = 1.0 / @[&vec4](c.legion_accessor_array_ref(fa_wire_cap, current_wire))
    for j = 0, steps do
      for i = 0, WIRE_SEGMENTS do
        var dv  : vec4 = temp_v[i + 1] - temp_v[i]
        var di  : vec4 = temp_i[i] - old_i[i]
        var vol : vec4 = dv - inductance * di * recip_dt
        temp_i[i] = vol * recip_resistance
      end
      for i = 0, WIRE_SEGMENTS - 1 do
        var dq  : vec4 = dt * (temp_i[i] - temp_i[i + 1])
        temp_v[i + 1] = old_v[i] + dq * recip_capacitance
      end
    end

    for i = 0, WIRE_SEGMENTS do
      @[&vec4](c.legion_accessor_array_ref(fa_current[i], current_wire)) = temp_i[i]
    end
    for i = 0, WIRE_SEGMENTS - 1 do
      @[&vec4](c.legion_accessor_array_ref(fa_voltage[i], current_wire)) = temp_v[i + 1]
    end
    index = index + 4
  end

  while index < piece.num_wires do
    var temp_v : float[WIRE_SEGMENTS + 1]
    var temp_i : float[WIRE_SEGMENTS]
    var old_i  : float[WIRE_SEGMENTS]
    var old_v  : float[WIRE_SEGMENTS - 1]
    var wire_ptr : ptr_t = ptr_t { value = first_wire.value + index }

    for i = 0, WIRE_SEGMENTS do
      temp_i[i] = @[&float](c.legion_accessor_array_ref(fa_current[i], wire_ptr))
      old_i[i] = temp_i[i]
    end
    for i = 0, WIRE_SEGMENTS - 1 do
      temp_v[i + 1] = @[&float](c.legion_accessor_array_ref(fa_voltage[i], wire_ptr))
      old_v[i] = temp_v[i+1]
    end

    -- Pin the outer voltages to the node voltages
    var in_ptr = @[&ptr_t](c.legion_accessor_array_ref(fa_in_ptr, wire_ptr))
    var in_loc = @[&PointerLocation](c.legion_accessor_array_ref(fa_in_loc, wire_ptr))
    temp_v[0] =
      get_node_voltage(fa_pvt_voltage, fa_shr_voltage, fa_ghost_voltage, in_loc, in_ptr)
    var out_ptr = @[&ptr_t](c.legion_accessor_array_ref(fa_out_ptr, wire_ptr))
    var out_loc = @[&PointerLocation](c.legion_accessor_array_ref(fa_out_loc, wire_ptr))
    temp_v[WIRE_SEGMENTS] =
      get_node_voltage(fa_pvt_voltage, fa_shr_voltage, fa_ghost_voltage, out_loc, out_ptr)

    -- Solve the RLC model iteratively
    var inductance : float = @[&float](c.legion_accessor_array_ref(fa_inductance, wire_ptr))
    var recip_resistance : float = 1.0 / @[&float](c.legion_accessor_array_ref(fa_resistance, wire_ptr))
    var recip_capacitance : float = 1.0 / @[&float](c.legion_accessor_array_ref(fa_wire_cap, wire_ptr))
    var dt : float = piece.dt
    var recip_dt : float = 1.0 / dt
    for j = 0, steps do
      -- first, figure out the new current from the voltage differential
      -- and our inductance:
      -- dV = R*I + L*I' ==> I = (dV - L*I')/R
      for i = 0, WIRE_SEGMENTS do
        temp_i[i] = ((temp_v[i+1] - temp_v[i]) -
                     (inductance * (temp_i[i] - old_i[i]) * recip_dt)) * recip_resistance
      end
      -- Now update the inter-node voltages
      for i = 0, WIRE_SEGMENTS - 1 do
        temp_v[i+1] = old_v[i] + dt * (temp_i[i] - temp_i[i+1]) * recip_capacitance
      end
    end

    -- Write out the results
    for i = 0, WIRE_SEGMENTS do
      @[&float](c.legion_accessor_array_ref(fa_current[i], wire_ptr)) = temp_i[i]
    end
    for i = 0, WIRE_SEGMENTS - 1 do
      @[&float](c.legion_accessor_array_ref(fa_voltage[i], wire_ptr)) = temp_v[i + 1]
    end
    -- Update the index
    index = index + 1
  end
  return true
end

terra CalcNewCurrentsTask.cpu_base_impl(p : &CircuitPiece,
                                        regions : &physical_region_t,
                                        num_regions : uint32)
  var fa_current : accessor_array_t[WIRE_SEGMENTS]
  for i = 0, WIRE_SEGMENTS do
    fa_current[i] =
      c.legion_physical_region_get_field_accessor_array(regions[0], FID_CURRENT+i)
  end
  var fa_voltage : accessor_array_t[WIRE_SEGMENTS - 1]
  for i = 0, WIRE_SEGMENTS - 1 do
    fa_voltage[i] =
      c.legion_physical_region_get_field_accessor_array(regions[0], FID_WIRE_VOLTAGE+i)
  end
  var fa_in_ptr =
    c.legion_physical_region_get_field_accessor_array(regions[1], FID_IN_PTR)
  var fa_out_ptr =
    c.legion_physical_region_get_field_accessor_array(regions[1], FID_OUT_PTR)
  var fa_in_loc =
    c.legion_physical_region_get_field_accessor_array(regions[1], FID_IN_LOC)
  var fa_out_loc =
    c.legion_physical_region_get_field_accessor_array(regions[1], FID_OUT_LOC)
  var fa_inductance =
    c.legion_physical_region_get_field_accessor_array(regions[1], FID_INDUCTANCE)
  var fa_resistance =
    c.legion_physical_region_get_field_accessor_array(regions[1], FID_RESISTANCE)
  var fa_wire_cap =
    c.legion_physical_region_get_field_accessor_array(regions[1], FID_WIRE_CAP)
  var fa_pvt_voltage =
    c.legion_physical_region_get_field_accessor_array(regions[2], FID_NODE_VOLTAGE)
  var fa_shr_voltage =
    c.legion_physical_region_get_field_accessor_array(regions[3], FID_NODE_VOLTAGE)
  var fa_ghost_voltage =
    c.legion_physical_region_get_field_accessor_array(regions[4], FID_NODE_VOLTAGE)

  if not dense_calc_new_currents(p,
                                 fa_in_ptr,
                                 fa_out_ptr,
                                 fa_in_loc,
                                 fa_out_loc,
                                 fa_inductance,
                                 fa_resistance,
                                 fa_wire_cap,
                                 fa_pvt_voltage,
                                 fa_shr_voltage,
                                 fa_ghost_voltage,
                                 fa_current,
                                 fa_voltage) then

    var itr : index_iterator_t =
      c.legion_index_iterator_create(p.pvt_wires.index_space)
    var temp_v : float[WIRE_SEGMENTS + 1]
    var temp_i : float[WIRE_SEGMENTS]
    var old_i : float[WIRE_SEGMENTS]
    var old_v : float[WIRE_SEGMENTS - 1]

    var dt : float = p.dt
    var recip_dt : float = 1.0 / dt
    var steps = p.steps
    while c.legion_index_iterator_has_next(itr) do
      var wire_ptr = c.legion_index_iterator_next(itr)

      for i = 0, WIRE_SEGMENTS do
        temp_i[i] = @[&float](c.legion_accessor_array_ref(fa_current[i], wire_ptr))
        old_i[i] = temp_i[i]
      end
      for i = 0, WIRE_SEGMENTS - 1 do
        temp_v[i+1] = @[&float](c.legion_accessor_array_ref(fa_voltage[i], wire_ptr))
        old_v[i] = temp_v[i+1]
      end

      -- Pin the outer voltages to the node voltages
      var in_ptr = @[&ptr_t](c.legion_accessor_array_ref(fa_in_ptr, wire_ptr))
      var in_loc = @[&PointerLocation](c.legion_accessor_array_ref(fa_in_loc, wire_ptr))
      temp_v[0] =
        get_node_voltage(fa_pvt_voltage, fa_shr_voltage, fa_ghost_voltage, in_loc, in_ptr)
      var out_ptr = @[&ptr_t](c.legion_accessor_array_ref(fa_out_ptr, wire_ptr))
      var out_loc = @[&PointerLocation](c.legion_accessor_array_ref(fa_out_loc, wire_ptr))
      temp_v[WIRE_SEGMENTS] =
        get_node_voltage(fa_pvt_voltage, fa_shr_voltage, fa_ghost_voltage, out_loc, out_ptr)

      -- Solve the RLC model iteratively
      var inductance : float = @[&float](c.legion_accessor_array_ref(fa_inductance, wire_ptr))
      var recip_resistance : float = 1.0 / @[&float](c.legion_accessor_array_ref(fa_resistance, wire_ptr))
      var recip_capacitance : float = 1.0 / @[&float](c.legion_accessor_array_ref(fa_wire_cap, wire_ptr))
      for j = 0, steps do
        -- first, figure out the new current from the voltage differential
        -- and our inductance:
        -- dV = R*I + L*I' ==> I = (dV - L*I')/R
        for i = 0, WIRE_SEGMENTS do
          temp_i[i] = ((temp_v[i + 1] - temp_v[i]) -
                       (inductance * (temp_i[i] - old_i[i]) * recip_dt)) * recip_resistance
        end
        -- Now update the inter-node voltages
        for i = 0, WIRE_SEGMENTS - 1 do
          temp_v[i + 1] = old_v[i] + dt * (temp_i[i] - temp_i[i + 1]) * recip_capacitance
        end
      end

      -- Write out the results
      for i = 0, WIRE_SEGMENTS do
        @[&float](c.legion_accessor_array_ref(fa_current[i], wire_ptr)) = temp_i[i]
      end
      for i = 0, WIRE_SEGMENTS - 1 do
        @[&float](c.legion_accessor_array_ref(fa_voltage[i], wire_ptr)) = temp_v[i+1]
      end
    end
    c.legion_index_iterator_destroy(itr)
  end

  for i = 0, WIRE_SEGMENTS do
    c.legion_accessor_array_destroy(fa_current[i])
  end
  for i = 0, WIRE_SEGMENTS - 1 do
    c.legion_accessor_array_destroy(fa_voltage[i])
  end
  c.legion_accessor_array_destroy(fa_in_ptr)
  c.legion_accessor_array_destroy(fa_out_ptr)
  c.legion_accessor_array_destroy(fa_in_loc)
  c.legion_accessor_array_destroy(fa_out_loc)
  c.legion_accessor_array_destroy(fa_inductance)
  c.legion_accessor_array_destroy(fa_resistance)
  c.legion_accessor_array_destroy(fa_wire_cap)
  c.legion_accessor_array_destroy(fa_pvt_voltage)
  c.legion_accessor_array_destroy(fa_shr_voltage)
  c.legion_accessor_array_destroy(fa_ghost_voltage)
end

terra calc_new_currents(task : task_t,
                        regions : &physical_region_t,
                        num_regions : uint32,
                        ctx : context_t,
                        runtime : runtime_t)
  var p : &CircuitPiece =
    [&CircuitPiece](c.legion_task_get_local_args(task))
  CalcNewCurrentsTask.cpu_base_impl(p, regions, num_regions)
end

CalcNewCurrentsTask.TASK_NAME = "calc_new_currents"
CalcNewCurrentsTask.TASK_FN = calc_new_currents
CalcNewCurrentsTask.TASK_ID = CALC_NEW_CURRENTS_TASK_ID
CalcNewCurrentsTask.CPU_BASE_LEAF = true
CalcNewCurrentsTask.MAPPER_ID = 0

function CalcNewCurrentsTask:new(lp_pvt_wires,
                                 lp_pvt_nodes,
                                 lp_shr_nodes,
                                 lp_ghost_nodes,
                                 lr_all_wires,
                                 lr_all_nodes,
                                 launch_domain,
                                 arg_map)
  local launcher =
    IndexLauncher:new(CalcNewCurrentsTask.TASK_ID, launch_domain,
                      nil, arg_map, Predicate.TRUE_PRED,
                      false, CalcNewCurrentsTask.MAPPER_ID)

  local rr_out = RegionRequirement:new(lp_pvt_wires, 0, legion.READ_WRITE,
                                       legion.EXCLUSIVE, lr_all_wires)
  for i = 0, WIRE_SEGMENTS - 1 do
    rr_out:add_field(FID_CURRENT + i)
  end
  for i = 0, WIRE_SEGMENTS - 2 do
    rr_out:add_field(FID_WIRE_VOLTAGE + i)
  end
  launcher:add_region_requirement(rr_out)

  local rr_wires = RegionRequirement:new(lp_pvt_wires, 0, legion.READ_ONLY,
                                         legion.EXCLUSIVE, lr_all_wires)
  rr_wires:add_field(FID_IN_PTR)
  rr_wires:add_field(FID_OUT_PTR)
  rr_wires:add_field(FID_IN_LOC)
  rr_wires:add_field(FID_OUT_LOC)
  rr_wires:add_field(FID_INDUCTANCE)
  rr_wires:add_field(FID_RESISTANCE)
  rr_wires:add_field(FID_WIRE_CAP)
  launcher:add_region_requirement(rr_wires)

  local rr_private = RegionRequirement:new(lp_pvt_nodes, 0, legion.READ_ONLY,
                                           legion.EXCLUSIVE, lr_all_nodes)
  rr_private:add_field(FID_NODE_VOLTAGE)
  launcher:add_region_requirement(rr_private)

  local rr_shared = RegionRequirement:new(lp_shr_nodes, 0, legion.READ_ONLY,
                                          legion.EXCLUSIVE, lr_all_nodes)
  rr_shared:add_field(FID_NODE_VOLTAGE)
  launcher:add_region_requirement(rr_shared)

  local rr_ghost = RegionRequirement:new(lp_ghost_nodes, 0, legion.READ_ONLY,
                                         legion.EXCLUSIVE, lr_all_nodes)
  rr_ghost:add_field(FID_NODE_VOLTAGE)
  launcher:add_region_requirement(rr_ghost)

  launcher = inherit(launcher, self)
  return launcher
end

function CalcNewCurrentsTask:launch_check_fields(ctx, runtime)
  local req = self.region_requirements[0]
  local success = true
  for i = 0, WIRE_SEGMENTS - 1 do
    local launcher = CheckTask:new(req.partition, req.parent, FID_CURRENT+i,
                                   self.launch_domain, self.argument_map)
    success = launcher:dispatch(ctx, runtime, success)
  end
  for i = 0, WIRE_SEGMENTS - 2 do
    local launcher = CheckTask:new(req.partition, req.parent, FID_WIRE_VOLTAGE+i,
                                   self.launch_domain, self.argument_map)
    success = launcher:dispatch(ctx, runtime, success)
  end
  return success
end

--
-- DistributeChargeTask
--

local terra reduce_node(priv  : accessor_generic_t,
                        shr   : accessor_generic_t,
                        ghost : accessor_generic_t,
                        loc : PointerLocation,
                        ptr : ptr_t,
                        value : float)
  if loc == PRIVATE_PTR then
    [ReductionOp:get_reduce_func(legion.PLUS_OP, float)](priv, ptr, value)
  elseif loc == SHARED_PTR then
    [ReductionOp:get_safe_reduce_func(legion.PLUS_OP, float)](shr, ptr, value)
  elseif loc == GHOST_PTR then
    [ReductionOp:get_safe_reduce_func(legion.PLUS_OP, float)](ghost, ptr, value)
  else
    assert(false)
  end
end

terra DistributeChargeTask.cpu_base_impl(p : &CircuitPiece,
                                         regions : &physical_region_t,
                                         num_regions : uint32)
  var fa_in_ptr =
    c.legion_physical_region_get_field_accessor_array(regions[0], FID_IN_PTR)
  var fa_out_ptr =
    c.legion_physical_region_get_field_accessor_array(regions[0], FID_OUT_PTR)
  var fa_in_loc =
    c.legion_physical_region_get_field_accessor_array(regions[0], FID_IN_LOC)
  var fa_out_loc =
    c.legion_physical_region_get_field_accessor_array(regions[0], FID_OUT_LOC)
  var fa_in_current =
    c.legion_physical_region_get_field_accessor_array(regions[0], FID_CURRENT)
  var fa_out_current =
    c.legion_physical_region_get_field_accessor_array(regions[0], FID_CURRENT+WIRE_SEGMENTS-1)
  var fa_pvt_charge =
    c.legion_physical_region_get_field_accessor_generic(regions[1], FID_CHARGE)
  var fa_shr_charge =
    c.legion_physical_region_get_accessor_generic(regions[2])
  var fa_ghost_charge =
    c.legion_physical_region_get_accessor_generic(regions[3])

  var itr : index_iterator_t =
    c.legion_index_iterator_create(p.pvt_wires.index_space)
  var dt = p.dt
  var tmp : float
  var tmp_ptr : ptr_t
  var tmp_uint : uint
  while c.legion_index_iterator_has_next(itr) do
    var wire_ptr = c.legion_index_iterator_next(itr)
    c.legion_accessor_array_read(fa_in_current, wire_ptr, &tmp, sizeof(float))
    var in_current = -dt * tmp
    c.legion_accessor_array_read(fa_out_current, wire_ptr, &tmp, sizeof(float))
    var out_current = dt * tmp

    c.legion_accessor_array_read(fa_in_ptr, wire_ptr, &tmp_ptr, sizeof(ptr_t))
    var in_ptr = tmp_ptr
    c.legion_accessor_array_read(fa_out_ptr, wire_ptr, &tmp_ptr, sizeof(ptr_t))
    var out_ptr = tmp_ptr
    c.legion_accessor_array_read(fa_in_loc, wire_ptr, &tmp_uint, sizeof(PointerLocation))
    var in_loc = tmp_uint
    c.legion_accessor_array_read(fa_out_loc, wire_ptr, &tmp_uint, sizeof(PointerLocation))
    var out_loc = tmp_uint

    reduce_node(fa_pvt_charge, fa_shr_charge, fa_ghost_charge,
                in_loc, in_ptr, in_current)
    reduce_node(fa_pvt_charge, fa_shr_charge, fa_ghost_charge,
                out_loc, out_ptr, out_current)
  end
  c.legion_index_iterator_destroy(itr)

  c.legion_accessor_array_destroy(fa_in_ptr)
  c.legion_accessor_array_destroy(fa_out_ptr)
  c.legion_accessor_array_destroy(fa_in_loc)
  c.legion_accessor_array_destroy(fa_out_loc)
  c.legion_accessor_array_destroy(fa_in_current)
  c.legion_accessor_array_destroy(fa_out_current)
  c.legion_accessor_generic_destroy(fa_pvt_charge)
  c.legion_accessor_generic_destroy(fa_shr_charge)
  c.legion_accessor_generic_destroy(fa_ghost_charge)
end

terra distribute_charge(task : task_t,
                        regions : &physical_region_t,
                        num_regions : uint32,
                        ctx : context_t,
                        runtime : runtime_t)
  var p : &CircuitPiece =
    [&CircuitPiece](c.legion_task_get_local_args(task))
  DistributeChargeTask.cpu_base_impl(p, regions, num_regions)
end

DistributeChargeTask.TASK_NAME = "distribute_charge"
DistributeChargeTask.TASK_FN = distribute_charge
DistributeChargeTask.TASK_ID = DISTRIBUTE_CHARGE_TASK_ID
DistributeChargeTask.CPU_BASE_LEAF = true
DistributeChargeTask.MAPPER_ID = 0

function DistributeChargeTask:new(lp_pvt_wires,
                                  lp_pvt_nodes,
                                  lp_shr_nodes,
                                  lp_ghost_nodes,
                                  lr_all_wires,
                                  lr_all_nodes,
                                  launch_domain,
                                  arg_map)
  local launcher =
    IndexLauncher:new(DistributeChargeTask.TASK_ID, launch_domain,
                      nil, arg_map, Predicate.TRUE_PRED,
                      false, DistributeChargeTask.MAPPER_ID)

  local rr_wires = RegionRequirement:new(lp_pvt_wires, 0, legion.READ_ONLY,
                                         legion.EXCLUSIVE, lr_all_wires)
  rr_wires:add_field(FID_IN_PTR)
  rr_wires:add_field(FID_OUT_PTR)
  rr_wires:add_field(FID_IN_LOC)
  rr_wires:add_field(FID_OUT_LOC)
  rr_wires:add_field(FID_CURRENT)
  rr_wires:add_field(FID_CURRENT+WIRE_SEGMENTS-1)
  launcher:add_region_requirement(rr_wires)

  local rr_private = RegionRequirement:new(lp_pvt_nodes, 0, legion.READ_WRITE,
                                           legion.EXCLUSIVE, lr_all_nodes)
  rr_private:add_field(FID_CHARGE)
  launcher:add_region_requirement(rr_private)

  local rr_shared = RegionRequirement:new(lp_shr_nodes, 0, REDUCE_ID,
                                          legion.SIMULTANEOUS, lr_all_nodes)
  rr_shared:set_reduction()
  rr_shared:add_field(FID_CHARGE)
  launcher:add_region_requirement(rr_shared)

  local rr_ghost = RegionRequirement:new(lp_ghost_nodes, 0, REDUCE_ID,
                                         legion.SIMULTANEOUS, lr_all_nodes)
  rr_ghost:set_reduction()
  rr_ghost:add_field(FID_CHARGE)
  launcher:add_region_requirement(rr_ghost)

  launcher = inherit(launcher, self)
  return launcher
end

function DistributeChargeTask:launch_check_fields(ctx, runtime)
  local success = true
  for idx = 1, 3 do
    local req = self.region_requirements[idx]
    local launcher = CheckTask:new(req.partition, req.parent, FID_CHARGE,
                                   self.launch_domain, self.argument_map)
    success = launcher:dispatch(ctx, runtime, success)
  end
  return success
end

--
-- UpdateVoltagesTask
--

local terra update_voltage(lr : logical_region_t,
                           fa_voltage : accessor_array_t,
                           fa_charge  : accessor_array_t,
                           fa_cap     : accessor_array_t,
                           fa_leakage : accessor_array_t)
  var itr : index_iterator_t =
    c.legion_index_iterator_create(lr.index_space)
  var tmp : float
  while c.legion_index_iterator_has_next(itr) do
    var node_ptr = c.legion_index_iterator_next(itr)

    c.legion_accessor_array_read(fa_voltage, node_ptr, &tmp, sizeof(float))
    var voltage = tmp
    c.legion_accessor_array_read(fa_charge, node_ptr, &tmp, sizeof(float))
    var charge = tmp
    c.legion_accessor_array_read(fa_cap, node_ptr, &tmp, sizeof(float))
    var capacitance = tmp
    c.legion_accessor_array_read(fa_leakage, node_ptr, &tmp, sizeof(float))
    var leakage = tmp

    voltage = voltage + charge / capacitance
    voltage = voltage * (1.0 - leakage)

    c.legion_accessor_array_write(fa_voltage, node_ptr, &voltage, sizeof(float))
    -- Reset the charge for the next iteration
    tmp = 0.0
    c.legion_accessor_array_write(fa_charge, node_ptr, &tmp, sizeof(float))
  end
  c.legion_index_iterator_destroy(itr)
end

terra UpdateVoltagesTask.cpu_base_impl(p : &CircuitPiece,
                                       regions : &physical_region_t,
                                       num_regions : uint32)
  var fa_pvt_voltage =
    c.legion_physical_region_get_field_accessor_array(regions[0], FID_NODE_VOLTAGE)
  var fa_pvt_charge =
    c.legion_physical_region_get_field_accessor_array(regions[0], FID_CHARGE)
  var fa_shr_voltage =
    c.legion_physical_region_get_field_accessor_array(regions[1], FID_NODE_VOLTAGE)
  var fa_shr_charge =
    c.legion_physical_region_get_field_accessor_array(regions[1], FID_CHARGE)
  var fa_pvt_cap =
    c.legion_physical_region_get_field_accessor_array(regions[2], FID_NODE_CAP)
  var fa_pvt_leakage =
    c.legion_physical_region_get_field_accessor_array(regions[2], FID_LEAKAGE)
  var fa_shr_cap =
    c.legion_physical_region_get_field_accessor_array(regions[3], FID_NODE_CAP)
  var fa_shr_leakage =
    c.legion_physical_region_get_field_accessor_array(regions[3], FID_LEAKAGE)

  update_voltage(p.pvt_nodes, fa_pvt_voltage, fa_pvt_charge,
                 fa_pvt_cap, fa_pvt_leakage)
  update_voltage(p.shr_nodes, fa_shr_voltage, fa_shr_charge,
                 fa_shr_cap, fa_shr_leakage)

  c.legion_accessor_array_destroy(fa_pvt_voltage)
  c.legion_accessor_array_destroy(fa_pvt_charge)
  c.legion_accessor_array_destroy(fa_shr_voltage)
  c.legion_accessor_array_destroy(fa_shr_charge)
  c.legion_accessor_array_destroy(fa_pvt_cap)
  c.legion_accessor_array_destroy(fa_pvt_leakage)
  c.legion_accessor_array_destroy(fa_shr_cap)
  c.legion_accessor_array_destroy(fa_shr_leakage)
end

terra update_voltages(task : task_t,
                      regions : &physical_region_t,
                      num_regions : uint32,
                      ctx : context_t,
                      runtime : runtime_t)
  var p : &CircuitPiece =
    [&CircuitPiece](c.legion_task_get_local_args(task))
  UpdateVoltagesTask.cpu_base_impl(p, regions, num_regions)
end

UpdateVoltagesTask.TASK_NAME = "update_voltages"
UpdateVoltagesTask.TASK_FN = update_voltages
UpdateVoltagesTask.TASK_ID = UPDATE_VOLTAGES_TASK_ID
UpdateVoltagesTask.CPU_BASE_LEAF = true
UpdateVoltagesTask.MAPPER_ID = 0

function UpdateVoltagesTask:new(lp_pvt_nodes,
                                lp_shr_nodes,
                                lp_node_locations,
                                lr_all_nodes,
                                lr_node_locator,
                                launch_domain,
                                arg_map)
  local launcher =
    IndexLauncher:new(UpdateVoltagesTask.TASK_ID, launch_domain,
                      nil, arg_map, Predicate.TRUE_PRED,
                      false, UpdateVoltagesTask.MAPPER_ID)

  local rr_private_out = RegionRequirement:new(lp_pvt_nodes, 0,
                                               legion.READ_WRITE,
                                               legion.EXCLUSIVE,
                                               lr_all_nodes)
  rr_private_out:add_field(FID_NODE_VOLTAGE)
  rr_private_out:add_field(FID_CHARGE)
  launcher:add_region_requirement(rr_private_out)

  local rr_shared_out = RegionRequirement:new(lp_shr_nodes, 0,
                                              legion.READ_WRITE,
                                              legion.EXCLUSIVE,
                                              lr_all_nodes)
  rr_shared_out:add_field(FID_NODE_VOLTAGE)
  rr_shared_out:add_field(FID_CHARGE)
  launcher:add_region_requirement(rr_shared_out)

  local rr_private_in = RegionRequirement:new(lp_pvt_nodes, 0,
                                              legion.READ_ONLY,
                                              legion.EXCLUSIVE,
                                              lr_all_nodes)
  rr_private_in:add_field(FID_NODE_CAP)
  rr_private_in:add_field(FID_LEAKAGE)
  launcher:add_region_requirement(rr_private_in)

  local rr_shared_in = RegionRequirement:new(lp_shr_nodes, 0,
                                             legion.READ_ONLY,
                                             legion.EXCLUSIVE,
                                             lr_all_nodes)
  rr_shared_in:add_field(FID_NODE_CAP)
  rr_shared_in:add_field(FID_LEAKAGE)
  launcher:add_region_requirement(rr_shared_in)

  local rr_locator = RegionRequirement:new(lp_node_locations, 0,
                                           legion.READ_ONLY,
                                           legion.EXCLUSIVE,
                                           lr_node_locator)
  rr_locator:add_field(FID_LOCATOR)
  launcher:add_region_requirement(rr_locator)

  launcher = inherit(launcher, self)
  return launcher
end

function UpdateVoltagesTask:launch_check_fields(ctx, runtime)
  local success = true
  local req = self.region_requirements[0]
  do
    local launcher = CheckTask:new(req.partition, req.parent, FID_NODE_VOLTAGE,
                                   self.launch_domain, self.argument_map)
    success = launcher:dispatch(ctx, runtime, success)
  end
  do
    local launcher = CheckTask:new(req.partition, req.parent, FID_CHARGE,
                                   self.launch_domain, self.argument_map)
    success = launcher:dispatch(ctx, runtime, success)
  end
  return success
end

--
-- CheckTask
--

CheckTask.TASK_NAME = "check_task"
CheckTask.TASK_ID = CHECK_FIELD_TASK_ID
CheckTask.LEAF = true
CheckTask.MAPPER_ID = 0

function CheckTask:new(lp, lr, fid, launch_domain, arg_map)
  local launcher = IndexLauncher:new(CheckTask.TASK_ID, launch_domain, nil,
                                     arg_map, Predicate.TRUE_PRED,
                                     false, CheckTask.MAPPER_ID)
  local rr_check = RegionRequirement:new(lp, 0, legion.READ_ONLY,
                                         legion.EXCLUSIVE, lr)
  rr_check:add_field(fid)
  launcher:add_region_requirement(rr_check)
  launcher = inherit(launcher, self)
  return launcher
end

function CheckTask:dispatch(ctx, runtime, success)
  local fm = runtime:execute_index_space(ctx, self)
  fm:wait_all_results()
  local launch_array = self.launch_domain:get_rect()
  local pir = GenericPointInRectIterator:new(launch_array)
  while(pir:has_next()) do
    success = fm:get_result(bool, DomainPoint:from_point(pir.p)) and success
    pir:next()
  end
  return success
end

function check_task(task, regions, ctx, runtime)
  local fa_check =
    regions[0]:get_field_accessor(task.regions[0].instance_fields[0]):typeify(float)
  local lr = task.regions[0].region
  local itr = IndexIterator:new(lr)
  local success = true
  while itr:has_next() and success do
    local ptr = itr:next()
    local value = fa_check:read(ptr)
    if not value then
      success = false
    end
  end
  return success
end

function CheckTask:register_task()
  legion:register_lua_task(bool, CheckTask.TASK_NAME, CheckTask.TASK_ID,
                           legion.LOC_PROC, false, true,
                           CIRCUIT_CPU_LEAF_VARIANT,
                           TaskConfigOptions:new(CheckTask.LEAF))
end

