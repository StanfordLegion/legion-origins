#ifndef __LEGION_TERRA_UTIL_H__
#define __LEGION_TERRA_UTIL_H__

#include "legion_c_util.h"

template<typename T, typename W = LegionRuntime::HighLevel::CObjectWrapper>
void lua_push_opaque_object(lua_State* L, T obj)
{
  void* ptr = W::unwrap(obj);

  lua_newtable(L);
  lua_pushstring(L, "impl");
  lua_pushlightuserdata(L, ptr);
  lua_settable(L, -3);
}

template<typename T>
void lua_push_opaque_object_array(lua_State* L, T* objs, unsigned num_objs)
{
  lua_newtable(L);
  for(unsigned i = 0; i < num_objs; ++i)
  {
    lua_push_opaque_object(L, objs[i]);
    lua_pushinteger(L, i + 1);
    lua_insert(L, -2);
    lua_settable(L, -3);
  }
}

#define CHECK_LUA(EXP)                              \
  if ((EXP) != 0)                                   \
  {                                                 \
    fprintf(stderr,                                 \
        "error calling lua function : %s (%s:%d)\n", \
        lua_tostring(L, -1),                        \
        __FILE__, __LINE__);                        \
    exit(-1);                                       \
  }                                                 \

struct ObjectWrapper
{
  typedef LegionRuntime::HighLevel::Mapper::DomainSplit DomainSplit;

  static vector_legion_domain_split_t
  wrap(std::vector<DomainSplit>* slices)
  {
    vector_legion_domain_split_t slices_;
    slices_.impl = slices;
    return slices_;
  }

  static std::vector<DomainSplit>*
  unwrap(vector_legion_domain_split_t slices_)
  {
    return reinterpret_cast<std::vector<DomainSplit>*>(slices_.impl);
  }
};

#endif // __LEGION_TERRA_UTIL_H__
