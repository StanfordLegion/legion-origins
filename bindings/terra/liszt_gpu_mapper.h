#ifndef __LISZT_GPU_MAPPER_H__
#define __LISZT_GPU_MAPPER_H__

//#include <stdbool.h>
//#include <stddef.h>
//#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void register_liszt_gpu_mapper();

#ifdef __cplusplus
}
#endif

#endif // __LISZT_GPU_MAPPER_H__
