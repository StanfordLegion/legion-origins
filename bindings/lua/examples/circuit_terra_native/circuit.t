require('legionlib')
terralib.require('circuit_tasks')
terralib.require('circuit_defs')

binding = LegionLib:init_binding("circuit_tasks.t")

binding:set_top_level_task_id(CIRCUIT_MAIN)
binding:register_single_task(CIRCUIT_MAIN, "circuit_main",
                             Processor.LOC_PROC, false)
binding:register_terra_task(CALC_NEW_CURRENTS, Processor.LOC_PROC, false, true)
binding:register_terra_task(DISTRIBUTE_CHARGE, Processor.LOC_PROC, false, true)
binding:register_terra_task(UPDATE_VOLTAGES, Processor.LOC_PROC, false, true)
binding:register_reduction_op(REDUCE_ID, ReductionType.PLUS, PrimType.float)
binding:start(arg)
