require('legionlib')

CIRCUIT_MAIN = 100
CALC_NEW_CURRENTS = 200
DISTRIBUTE_CHARGE = 300
UPDATE_VOLTAGES = 400

PRIVATE_PTR = 0
SHARED_PTR = 1
GHOST_PTR = 2

WIRE_SEGMENTS = 10
STEPS = 10000
DELTAT = 1e-6

REDUCE_ID = 1

-- CircuitNode = {
--    charge      = PrimType.float,
--    voltage     = PrimType.float,
--    capacitance = PrimType.float,
--    leakage     = PrimType.float
-- }

-- CircuitWire = {
--    in_ptr = PrimType.int,
--    out_ptr = PrimType.int,
--    in_loc = PrimType.int,
--    out_loc = PrimType.int,
--    inductance = PrimType.float,
--    resistance = PrimType.float,
--    capacitance = PrimType.float,
--    current = ArrayType:new(PrimType.float, WIRE_SEGMENTS),
--    voltage = ArrayType:new(PrimType.float, WIRE_SEGMENTS - 1)
-- }

-- PointerLocation = {
--    loc = PrimType.int
-- }

struct CircuitNode
{
   charge : float;
   voltage : float;
   capacitance : float;
   leakage : float;
}

struct CircuitWire
{
   in_ptr : int;
   out_ptr : int;
   in_loc : int;
   out_loc : int;
   inductance : float;
   resistance : float;
   capacitance : float;
   current : float[WIRE_SEGMENTS];
   voltage : float[WIRE_SEGMENTS - 1];
}

struct PointerLocation
{
   loc : int
}
