require('legionlib')
terralib.require('circuit_defs')

local function get_node(priv, shr, ghost, loc, ptr)
   if loc == PRIVATE_PTR
   then return priv:read(ptr)
   else if loc == SHARED_PTR
        then return shr:read(ptr)
        else if loc == GHOST_PTR
             then return ghost:read(ptr)
             else assert(false)
             end
        end
   end
end   

terra calculate_new_currents_kernel_terra(wire : CircuitWire,
                                          in_node_v : float,
                                          out_node_v : float) : CircuitWire
   -- Solve RLC model iteratively
   var new_i : float[WIRE_SEGMENTS]
   var new_v : float[WIRE_SEGMENTS + 1]

   var dt = DELTAT
   var steps = STEPS

   for i = 0, WIRE_SEGMENTS do new_i[i] = wire.current[i] end
   for i = 0, WIRE_SEGMENTS - 1 do new_v[i + 1] = wire.voltage[i] end
   new_v[0] = in_node_v
   new_v[WIRE_SEGMENTS] = out_node_v

   for j = 0, steps
   do
      -- first, figure out the new current from the voltage differential
      -- and our inductance:
      -- dV = R*I + L*I' ==> I = (dV - L*I')/R
      for i = 0, WIRE_SEGMENTS
      do
         new_i[i] =
            ((new_v[i + 1] - new_v[i]) - 
                (wire.inductance * (new_i[i] - wire.current[i]) / dt))
            / wire.resistance
      end
      -- Now update the inter-node voltages
      for i = 0, WIRE_SEGMENTS - 1
      do
         new_v[i + 1] =
            wire.voltage[i] +
            dt * (new_i[i] - new_i[i + 1]) / wire.capacitance
      end
   end

   for i = 0, WIRE_SEGMENTS do wire.current[i] = new_i[i] end
   for i = 0, WIRE_SEGMENTS - 1 do wire.voltage[i] = new_v[i + 1] end

   return wire

end
calculate_new_currents_kernel_terra:compile()

function gen_calculate_new_currents_kernel(WS, NS)
   local itype = vector(float, WS)
   local vtype = vector(float, WS - 1)
   local ptype = &float

   return
      terra(wire : CircuitWire,
            in_node_v : float,
            out_node_v : float) : CircuitWire

         -- Solve RLC model iteratively
         var wire_current : ptype = wire.current
         var wire_voltage : ptype = wire.voltage
         
         var new_i : itype = @[&itype](wire_current)
         var new_i_ptr: ptype = [ptype](&new_i)
         var new_v : vector(float, WS + 1)
         var new_v_ptr: ptype = [ptype](&new_v)
         @[&vtype](&new_v_ptr[1]) = @[&vtype](wire_voltage)
         new_v_ptr[0] = in_node_v
         new_v_ptr[WS] = out_node_v

         var dt = DELTAT

         var new_i_ptr_0: ptype = new_i_ptr
         var new_i_ptr_1: ptype = &new_i_ptr[1]
         var new_v_ptr_0: ptype = new_v_ptr
         var new_v_ptr_1: ptype = &new_v_ptr[1]

         for j = 0, NS
         do
            -- first, figure out the new current from the voltage differential
            -- and our inductance:
            -- dV = R*I + L*I' ==> I = (dV - L*I')/R
            var new_i_next : itype =
               ((@[&itype](new_v_ptr_1) - @[&itype](new_v_ptr_0)) -
                   (wire.inductance * (new_i - @[&itype](wire_current)) / dt))
               / wire.resistance
            new_i = new_i_next

            -- Now update the inter-node voltages
            var new_v_next : vtype =
               @[&vtype](wire_voltage) +
               dt * (@[&vtype](new_i_ptr_0) - @[&vtype](new_v_ptr_1)) / wire.capacitance
            @[&vtype](new_v_ptr_1) = new_v_next
         end

         @[&itype](wire_current) = new_i
         @[&vtype](wire_voltage) = @[&vtype](&new_v_ptr[1])

         return wire
      end
end

calculate_new_currents_kernel_terra_vec =
   gen_calculate_new_currents_kernel(WIRE_SEGMENTS, STEPS)
calculate_new_currents_kernel_terra_vec:compile()

function calculate_new_currents_kernel_lua(wire,
                                           in_node_v,
                                           out_node_v)
   -- Solve RLC model iteratively
   local new_i = {}
   local new_v = {}

   local dt = DELTAT
   local steps = STEPS

   for i = 0, WIRE_SEGMENTS do new_i[i] = wire.current[i] end
   for i = 0, WIRE_SEGMENTS - 1 do new_v[i + 1] = wire.voltage[i] end
   new_v[0] = in_node_v
   new_v[WIRE_SEGMENTS] = out_node_v

   for j = 1, steps
   do
      -- first, figure out the new current from the voltage differential
      -- and our inductance:
      -- dV = R*I + L*I' ==> I = (dV - L*I')/R
      for i = 0, WIRE_SEGMENTS - 1
      do
         new_i[i] =
            ((new_v[i + 1] - new_v[i]) - 
                (wire.inductance * (new_i[i] - wire.current[i]) / dt)) / wire.resistance
      end
      -- Now update the inter-node voltages
      for i = 0, WIRE_SEGMENTS - 2
      do
         new_v[i + 1] =
            wire.voltage[i] +
            dt * (new_i[i] - new_i[i + 1]) / wire.capacitance
      end
   end

   for i = 0, WIRE_SEGMENTS - 1 do wire.current[i] = new_i[i] end
   for i = 0, WIRE_SEGMENTS - 2 do wire.voltage[i] = new_v[i + 1] end

   return wire
end

function calc_new_currents_cpu(reg, global_args, piece)
   local pvt_wires = reg[0]:get_lua_accessor(global_args.wire_field)
   local pvt_nodes = reg[1]:get_lua_accessor(global_args.node_field)
   local shr_nodes = reg[2]:get_lua_accessor(global_args.node_field)
   local ghost_nodes = reg[3]:get_lua_accessor(global_args.node_field)

   local itr = IndexIterator:new(piece.pvt_wires)

   while itr:has_next()
   do
      local wire_ptr = itr:next()

      local wire = pvt_wires:read(wire_ptr)
      local in_node  = get_node(pvt_nodes, shr_nodes, ghost_nodes,
                                wire.in_loc, wire.in_ptr)
      local out_node = get_node(pvt_nodes, shr_nodes, ghost_nodes,
                                wire.out_loc, wire.out_ptr)

      wire =
         calculate_new_currents_kernel_terra(wire,
                                             in_node.voltage,
                                             out_node.voltage)
      
      -- wire =
      --    calculate_new_currents_kernel_terra_vec(wire,
      --                                            in_node.voltage,
      --                                            out_node.voltage)

      -- wire =
      --    calculate_new_currents_kernel_lua(wire,
      --                                      in_node.voltage,
      --                                      out_node.voltage)
      
      pvt_wires:write(wire_ptr, wire)
   end
end

local function reduce_node(pvt, shr, ghost, loc, ptr, value)
   if loc == PRIVATE_PTR
   then
      pvt.charge:reduce(ReductionType.PLUS, ptr, value)
   else if loc == SHARED_PTR
        then
           shr.charge:reduce(ReductionType.PLUS, ptr, value)
        else if loc == GHOST_PTR
             then
                ghost.charge:reduce(ReductionType.PLUS, ptr, value)
             else
                assert(false)
             end
        end
   end
end

function distribute_charge_cpu(reg, global_args, piece)
   local pvt_wires = reg[0]:get_lua_accessor(global_args.wire_field)
   local pvt_nodes = reg[1]:get_lua_accessor(global_args.node_field)
   local shr_nodes = reg[2]:get_lua_accessor(global_args.node_field)
   local ghost_nodes = reg[3]:get_lua_accessor(global_args.node_field)

   local itr = IndexIterator:new(piece.pvt_wires)
   while itr:has_next()
   do
      local wire_ptr = itr:next()
      local wire = pvt_wires:read(wire_ptr)

      local dt = DELTAT; 

      reduce_node(pvt_nodes, shr_nodes, ghost_nodes,
                  wire.in_loc, wire.in_ptr,
                     -dt * wire.current[0])
      reduce_node(pvt_nodes, shr_nodes, ghost_nodes,
                  wire.out_loc, wire.out_ptr,
                     -dt * wire.current[WIRE_SEGMENTS - 1])
   end

end

terra update_region_voltages_kernel_terra(node : CircuitNode) : float
   var voltage : float = node.voltage

   voltage = voltage + node.charge / node.capacitance
   voltage = voltage * (1.0 - node.leakage)

   return voltage

end
update_region_voltages_kernel_terra:compile()

function update_region_voltages_kernel_lua(node)
   local voltage = node.voltage

   voltage = voltage + node.charge / node.capacitance
   voltage = voltage * (1.0 - node.leakage)

   return voltage

end

local function update_region_voltages(piece, nodes, itr)
   while itr:has_next()
   do
      local node_ptr = itr:next()
      local node = nodes:read(node_ptr)

      -- charge adds in, and then some leaks away
      node.voltage = update_region_voltages_kernel_terra(node)
      --node.voltage = update_region_voltages_kernel_lua(node)
      node.charge = 0

      nodes:write(node_ptr, node)
   end
end

function update_voltages_cpu(reg, global_args, piece)
   local pvt_nodes = reg[0]:get_lua_accessor(global_args.node_field)
   local shr_nodes = reg[1]:get_lua_accessor(global_args.node_field)

   local itr = IndexIterator:new(piece.pvt_nodes)
   update_region_voltages(piece, pvt_nodes, itr)
   itr = IndexIterator:new(piece.shr_nodes)
   update_region_voltages(piece, shr_nodes, itr)
end
