require('legionlib')
require('circuit_tasks')
terralib.require('circuit_defs')

binding = LegionLib:init_binding("circuit_tasks.lua")

binding:set_top_level_task_id(CIRCUIT_MAIN)
binding:register_single_task(CIRCUIT_MAIN, "circuit_main",
                             Processor.LOC_PROC, false)
binding:register_index_task(CALC_NEW_CURRENTS, "calculate_currents_task",
                            Processor.LOC_PROC, true)
binding:register_index_task(DISTRIBUTE_CHARGE, "distribute_charge_task",
                            Processor.LOC_PROC, true)
binding:register_index_task(UPDATE_VOLTAGES, "update_voltages_task",
                            Processor.LOC_PROC, true)
binding:register_reduction_op(REDUCE_ID, ReductionType.PLUS, PrimType.float)
binding:start(arg)
