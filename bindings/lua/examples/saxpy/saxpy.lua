require('legionlib')
require('saxpy_task')

local binding = LegionLib:init_binding("saxpy_task.lua")
binding:set_top_level_task_id(TOP_LEVEL_TASK_ID)
binding:register_single_task(TOP_LEVEL_TASK_ID, "top_level_task",
                             Processor.LOC_PROC, false)
binding:register_single_task(TASKID_MAIN, "main_task",
                             Processor.LOC_PROC, false)
binding:register_index_task(TASKID_INIT_VECTORS, "init_vectors_task",
                            Processor.LOC_PROC, true)
binding:register_index_task(TASKID_ADD_VECTORS, "add_vectors_task",
                            Processor.LOC_PROC, true)
binding:start(arg)
