#ifndef __BINDING_H__
#define __BINDING_H__

struct TIndexSpace
{
  unsigned id;
};

struct TFieldSpace
{
  unsigned id;
};

struct TLogicalRegion
{
  unsigned int tree_id;
  struct TIndexSpace index_space;
  struct TFieldSpace field_space;
};

struct TLogicalPartition
{
  unsigned int tree_id;
  unsigned int index_partition;
  struct TFieldSpace field_space;
};

struct TPhysicalRegion
{
  void* rawptr;
  unsigned int redop;
};

struct TTask
{
  void* rawptr;
};

#endif // __BINDING_H_
