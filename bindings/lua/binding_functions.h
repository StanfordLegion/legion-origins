#ifndef __BINDING_FUNCTIONS_H__
#define __BINDING_FUNCTIONS_H__

#include "binding.h"

// simple IndexIterator wrapper in C

void* create_index_iterator(struct TLogicalRegion region);
void destroy_index_iterator(void* _iterator);
unsigned next(void* _iterator);
int has_next(void* _iterator);

// simple PhysicalRegion wrapper in C

void* create_terra_accessor(struct TPhysicalRegion region);
void* create_terra_field_accessor(struct TPhysicalRegion region,
                                  unsigned field);
void destroy_terra_accessor(void* _accessor);

// simple GenericAccessor wrapper in C
void read_from_accessor(void* _accessor, unsigned ptr,
                        void* dst, unsigned long long bytes);
void write_to_accessor(void* _accessor, unsigned ptr,
                       void* src, unsigned long long bytes);

void* create_terra_reducer(struct TPhysicalRegion region,
                           unsigned long long offset,
                           unsigned int redop,
                           unsigned int elem_type,
                           unsigned int red_type);
void reduce_terra_reducer_float(void* _reducer,
                                unsigned int redop,
                                unsigned int red_type,
                                unsigned int ptr,
                                float value);
void reduce_terra_reducer_double(void* _reducer,
                                 unsigned int redop,
                                 unsigned int red_type,
                                 unsigned int ptr,
                                 double value);
void reduce_terra_reducer_int(void* _reducer,
                              unsigned int redop,
                              unsigned int red_type,
                              unsigned int ptr,
                              int value);
void destroy_terra_reducer(void* _reducer,
                           unsigned int redop,
                           unsigned int elem_type,
                           unsigned int red_type);

// simple Task wrapper in C
int get_index(void* _task);

#endif // __BINDING_FUNCTIONS_H__
