
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include "legion.h"
#include "../legion_storage.h"
using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;
using namespace LegionStorage;

/*
 * In this example we illustrate how the Legion
 * programming model supports multiple partitions
 * of the same logical region and the benefits it
 * provides by allowing multiple views onto the
 * same logical region.  We compute a simple 5-point
 * 1D stencil using the standard forumala:
 * f'(x) = (-f(x+2h) + 8f(x+h) - 8f(x-h) + f(x-2h))/12h
 * For simplicity we'll assume h=1.
 */

enum TaskIDs {
  TOP_LEVEL_TASK_ID,
  INIT_FIELD_TASK_ID,
  STENCIL_TASK_ID,
  CHECK_TASK_ID,
};

enum FieldIDs {
  FID_VAL,
  FID_DERIV,
};

RegionFileAccessor<double, 1> *rfa;

void top_level_task(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, HighLevelRuntime *runtime)
{
  int num_elements = 1024;
  int num_subregions = 4;
  // Check for any command line arguments
  {
      const InputArgs &command_args = HighLevelRuntime::get_input_args();
    for (int i = 1; i < command_args.argc; i++)
    {
      if (!strcmp(command_args.argv[i],"-n"))
        num_elements = atoi(command_args.argv[++i]);
      if (!strcmp(command_args.argv[i],"-b"))
        num_subregions = atoi(command_args.argv[++i]);
    }
  }
  printf("Running stencil computation for %d elements...\n", num_elements);
  printf("Partitioning data into %d sub-regions...\n", num_subregions);

  // For this example we'll create a single logical region with two
  // fields.  We'll initialize the field identified by 'FID_VAL' with
  // our input data and then compute the derivatives stencil values 
  // and write them into the field identified by 'FID_DERIV'.
  FieldSpace fs = runtime->create_field_space(ctx);
  {
    FieldAllocator allocator = 
      runtime->create_field_allocator(ctx, fs);
    allocator.allocate_field(sizeof(double),FID_VAL);
    allocator.allocate_field(sizeof(double),FID_DERIV);
  }

  IndexSpace is = LegionStorageManager::load_index_space_tree_from_file("IndexSpace.dat", runtime, ctx);

  IndexPartition disjoint_ip = runtime->get_index_partition(ctx, is, 0);
  IndexPartition ghost_ip = runtime->get_index_partition(ctx, is, 1);
  Rect<1> color_bounds(Point<1>(0),Point<1>(num_subregions-1));
  Domain color_domain = Domain::from_rect<1>(color_bounds);

  LogicalRegion stencil_lr = runtime->create_logical_region(ctx, is, fs);
  LogicalPartition disjoint_lp = 
    runtime->get_logical_partition(ctx, stencil_lr, disjoint_ip);
  LogicalPartition ghost_lp = 
    runtime->get_logical_partition(ctx, stencil_lr, ghost_ip);

  // Our launch domain will again be isomorphic to our coloring domain.
  Domain launch_domain = color_domain;
  ArgumentMap arg_map;

  // First initialize the 'FID_VAL' field with some data
  std::set<LogicalRegion> region_set = LegionStorageManager::helper_get_region_set(stencil_lr, runtime, ctx, LegionStorageManager::REGIONSET_LEAVES);
  std::map<FieldID, char*> field_set;
  field_set[FID_VAL] = "VAL";
  LegionStorageManager::load_logical_region_data_from_file(region_set, field_set, "input/stencil", runtime, ctx, 0, true);
  /*
  IndexLauncher init_launcher(INIT_FIELD_TASK_ID, launch_domain,
                              TaskArgument(NULL, 0), arg_map);
  init_launcher.add_region_requirement(
      RegionRequirement(disjoint_lp, 0,
                        WRITE_DISCARD, EXCLUSIVE, stencil_lr));
  init_launcher.add_field(0, FID_VAL);
  runtime->execute_index_space(ctx, init_launcher);
  */
  // Now we're going to launch our stencil computation.  We
  // specify two region requirements for the stencil task.
  // Each region requirement is upper bounded by one of our
  // two partitions.  The first region requirement requests
  // read-only privileges on the ghost partition.  Note that
  // because we are only requesting read-only privileges, all
  // of our sub-tasks in the index space launch will be 
  // non-interfering.  The second region requirement asks for
  // read-write privileges on the disjoint partition for
  // the 'FID_DERIV' field.  Again this meets with the 
  // mandate that all points in our index space task
  // launch be non-interfering.
  IndexLauncher stencil_launcher(STENCIL_TASK_ID, launch_domain,
       TaskArgument(&num_elements, sizeof(num_elements)), arg_map);
  stencil_launcher.add_region_requirement(
      RegionRequirement(ghost_lp, 0/*projection ID*/,
                        READ_ONLY, EXCLUSIVE, stencil_lr));
  stencil_launcher.add_field(0, FID_VAL);
  stencil_launcher.add_region_requirement(
      RegionRequirement(disjoint_lp, 0/*projection ID*/,
                        READ_WRITE, EXCLUSIVE, stencil_lr));
  stencil_launcher.add_field(1, FID_DERIV);
  runtime->execute_index_space(ctx, stencil_launcher);

  // Finally, we launch a single task to check the results.
  TaskLauncher check_launcher(CHECK_TASK_ID, 
      TaskArgument(&num_elements, sizeof(num_elements)));
  check_launcher.add_region_requirement(
      RegionRequirement(stencil_lr, READ_ONLY, EXCLUSIVE, stencil_lr));
  check_launcher.add_field(0, FID_VAL);
  check_launcher.add_region_requirement(
      RegionRequirement(stencil_lr, READ_ONLY, EXCLUSIVE, stencil_lr));
  check_launcher.add_field(1, FID_DERIV);
  runtime->execute_task(ctx, check_launcher);

  // Clean up our region, index space, and field space
  runtime->destroy_logical_region(ctx, stencil_lr);
  runtime->destroy_field_space(ctx, fs);
  runtime->destroy_index_space(ctx, is);
}

// The standard initialize field task from earlier examples
void init_field_task(const Task *task,
                     const std::vector<PhysicalRegion> &regions,
                     Context ctx, HighLevelRuntime *runtime)
{
  assert(regions.size() == 1); 
  assert(task->regions.size() == 1);
  assert(task->regions[0].privilege_fields.size() == 1);

  FieldID fid = *(task->regions[0].privilege_fields.begin());
  const int point = task->index_point.point_data[0];
  printf("Initializing field %d for block %d...\n", fid, point);

  RegionAccessor<AccessorType::Generic, double> acc = 
    regions[0].get_field_accessor(fid).typeify<double>();

  Domain dom = runtime->get_index_space_domain(ctx, 
      task->regions[0].region.get_index_space());
  Rect<1> rect = dom.get_rect<1>();
  rfa->load_from_file(acc, rect);
}

// Our stencil tasks is interesting because it
// has both slow and fast versions depending
// on whether or not its bounds have been clamped.
void stencil_task(const Task *task,
                  const std::vector<PhysicalRegion> &regions,
                  Context ctx, HighLevelRuntime *runtime)
{
  assert(regions.size() == 2);
  assert(task->regions.size() == 2);
  assert(task->regions[0].privilege_fields.size() == 1);
  assert(task->regions[1].privilege_fields.size() == 1);
  assert(task->arglen == sizeof(int));
  const int max_elements = *((const int*)task->args);
  const int point = task->index_point.point_data[0];
  
  FieldID read_fid = *(task->regions[0].privilege_fields.begin());
  FieldID write_fid = *(task->regions[1].privilege_fields.begin());

  RegionAccessor<AccessorType::Generic, double> read_acc = 
    regions[0].get_field_accessor(read_fid).typeify<double>();
  RegionAccessor<AccessorType::Generic, double> write_acc = 
    regions[1].get_field_accessor(write_fid).typeify<double>();

  Domain dom = runtime->get_index_space_domain(ctx,
      task->regions[1].region.get_index_space());
  Rect<1> rect = dom.get_rect<1>();
  const DomainPoint zero = DomainPoint::from_point<1>(Point<1>(0));
  const DomainPoint max = DomainPoint::from_point<1>(Point<1>(max_elements-1));
  const Point<1> one(1);
  const Point<1> two(2);
  // If we are on the edges of the entire space we are 
  // operating over, then we're going to do the slow
  // path which checks for clamping when necessary.
  // If not, then we can do the fast path without
  // any checks.
  if ((rect.lo[0] == 0) || (rect.hi[0] == (max_elements-1)))
  {
    printf("Running slow stencil path for point %d...\n", point);
    // Note in the slow path that there are checks which
    // perform clamps when necessary before reading values.
    for (GenericPointInRectIterator<1> pir(rect); pir; pir++)
    {
      double l2, l1, r1, r2;
      if (pir.p[0] < 2)
        l2 = read_acc.read(zero);
      else
        l2 = read_acc.read(DomainPoint::from_point<1>(pir.p-two));
      if (pir.p[0] < 1)
        l1 = read_acc.read(zero);
      else
        l1 = read_acc.read(DomainPoint::from_point<1>(pir.p-one));
      if (pir.p[0] > (max_elements-2))
        r1 = read_acc.read(max);
      else
        r1 = read_acc.read(DomainPoint::from_point<1>(pir.p+one));
      if (pir.p[0] > (max_elements-3))
        r2 = read_acc.read(max);
      else
        r2 = read_acc.read(DomainPoint::from_point<1>(pir.p+two));
      
      double result = (-l2 + 8.0*l1 - 8.0*r1 + r2) / 12.0;
      write_acc.write(DomainPoint::from_point<1>(pir.p), result);
      printf("stencil[%d]: %lf\n", pir.p.x[0], result);
    }
  }
  else
  {
    printf("Running fast stencil path for point %d...\n", point);
    // In the fast path, we don't need any checks
    for (GenericPointInRectIterator<1> pir(rect); pir; pir++)
    {
      double l2 = read_acc.read(DomainPoint::from_point<1>(pir.p-two));
      double l1 = read_acc.read(DomainPoint::from_point<1>(pir.p-one));
      double r1 = read_acc.read(DomainPoint::from_point<1>(pir.p+one));
      double r2 = read_acc.read(DomainPoint::from_point<1>(pir.p+two));

      double result = (-l2 + 8.0*l1 - 8.0*r1 + r2) / 12.0;
      write_acc.write(DomainPoint::from_point<1>(pir.p), result);
      printf("stencil[%d]: %lf\n", pir.p.x[0], result);
    }
  }
}

void check_task(const Task *task,
                const std::vector<PhysicalRegion> &regions,
                Context ctx, HighLevelRuntime *runtime)
{
  assert(regions.size() == 2);
  assert(task->regions.size() == 2);
  assert(task->regions[0].privilege_fields.size() == 1);
  assert(task->regions[1].privilege_fields.size() == 1);
  assert(task->arglen == sizeof(int));
  const int max_elements = *((const int*)task->args);

  FieldID src_fid = *(task->regions[0].privilege_fields.begin());
  FieldID dst_fid = *(task->regions[1].privilege_fields.begin());

  RegionAccessor<AccessorType::Generic, double> src_acc = 
    regions[0].get_field_accessor(src_fid).typeify<double>();
  RegionAccessor<AccessorType::Generic, double> dst_acc = 
    regions[1].get_field_accessor(dst_fid).typeify<double>();

  Domain dom = runtime->get_index_space_domain(ctx,
      task->regions[1].region.get_index_space());
  Rect<1> rect = dom.get_rect<1>();
  const DomainPoint zero = DomainPoint::from_point<1>(Point<1>(0));
  const DomainPoint max = DomainPoint::from_point<1>(Point<1>(max_elements-1));
  const Point<1> one(1);
  const Point<1> two(2);

  // This is the checking task so we can just do the slow path
  bool all_passed = true;
  for (GenericPointInRectIterator<1> pir(rect); pir; pir++)
  {
    double l2, l1, r1, r2;
    if (pir.p[0] < 2)
      l2 = src_acc.read(zero);
    else
      l2 = src_acc.read(DomainPoint::from_point<1>(pir.p-two));
    if (pir.p[0] < 1)
      l1 = src_acc.read(zero);
    else
      l1 = src_acc.read(DomainPoint::from_point<1>(pir.p-one));
    if (pir.p[0] > (max_elements-2))
      r1 = src_acc.read(max);
    else
      r1 = src_acc.read(DomainPoint::from_point<1>(pir.p+one));
    if (pir.p[0] > (max_elements-3))
      r2 = src_acc.read(max);
    else
      r2 = src_acc.read(DomainPoint::from_point<1>(pir.p+two));
    
    double expected = (-l2 + 8.0*l1 - 8.0*r1 + r2) / 12.0;
    double received = dst_acc.read(DomainPoint::from_point<1>(pir.p));
    // Probably shouldn't bitwise compare floating point
    // numbers but the order of operations are the same so they
    // should be bitwise equal.
    if (expected != received)
      all_passed = false;
  }
  if (all_passed)
    printf("SUCCESS!\n");
  else
    printf("FAILURE!\n");
}

int main(int argc, char **argv)
{
  HighLevelRuntime::set_top_level_task_id(TOP_LEVEL_TASK_ID);
  HighLevelRuntime::register_legion_task<top_level_task>(TOP_LEVEL_TASK_ID,
      Processor::LOC_PROC, true/*single*/, false/*index*/);
  HighLevelRuntime::register_legion_task<init_field_task>(INIT_FIELD_TASK_ID,
      Processor::LOC_PROC, true/*single*/, true/*index*/);
  HighLevelRuntime::register_legion_task<stencil_task>(STENCIL_TASK_ID,
      Processor::LOC_PROC, true/*single*/, true/*index*/);
  HighLevelRuntime::register_legion_task<check_task>(CHECK_TASK_ID,
      Processor::LOC_PROC, true/*single*/, true/*index*/);

  LegionStorageManager::initialize();
  return HighLevelRuntime::start(argc, argv);
}
