
#include "circuit_mapper.h"

using namespace LegionRuntime::HighLevel;

CircuitMapper::CircuitMapper(Machine *m, HighLevelRuntime *rt, Processor p)
  : DefaultMapper(m, rt, p)
{
  const std::set<Processor> &all_procs = machine->get_all_processors();
  for (std::set<Processor>::const_iterator it = all_procs.begin();
        it != all_procs.end(); it++)
  {
    Processor::Kind k = machine->get_processor_kind(*it);
    switch (k)
    {
      case Processor::LOC_PROC:
        all_cpus.push_back(*it);
        break;
      case Processor::TOC_PROC:
        all_gpus.push_back(*it);
        break;
      default:
        break;
    }
  }
  map_to_gpus = !all_gpus.empty();
}

void CircuitMapper::slice_domain(const Task *task, const Domain &domain,
                                 std::vector<DomainSplit> &slices)
{
  if (map_to_gpus && (task->task_id != CHECK_FIELD_TASK_ID))
  {
    decompose_index_space(domain, all_gpus, 1/*splitting factor*/, slices);
  }
  else
  {
    decompose_index_space(domain, all_cpus, 1/*splitting factor*/, slices);
  }
}

bool CircuitMapper::map_task(Task *task)
{
  if (map_to_gpus && (task->task_id != TOP_LEVEL_TASK_ID) &&
      (task->task_id != CHECK_FIELD_TASK_ID))
  {
    // Otherwise do custom mappings for GPU memories
    Memory zc_mem = machine_interface.find_memory_kind(task->target_proc,
                                                       Memory::Z_COPY_MEM);
    assert(zc_mem.exists());
    Memory fb_mem = machine_interface.find_memory_kind(task->target_proc,
                                                       Memory::GPU_FB_MEM);
    assert(zc_mem.exists());
    switch (task->task_id)
    {
      case CALC_NEW_CURRENTS_TASK_ID:
        {
          for (unsigned idx = 0; idx < task->regions.size(); idx++)
          {
            // Wires and pvt nodes in framebuffer, 
            // shared and ghost in zero copy memory
            if (idx < 3)
              task->regions[idx].target_ranking.push_back(fb_mem);
            else
              task->regions[idx].target_ranking.push_back(zc_mem);
            task->regions[idx].virtual_map = false;
            task->regions[idx].enable_WAR_optimization = war_enabled;
            task->regions[idx].reduction_list = false;
            // Make everything SOA
            task->regions[idx].blocking_factor = 
              task->regions[idx].max_blocking_factor;
          }
          break;
        }
      case DISTRIBUTE_CHARGE_TASK_ID:
        {
          for (unsigned idx = 0; idx < task->regions.size(); idx++)
          {
            // Wires and pvt nodes in framebuffer, 
            // shared and ghost in zero copy memory
            if (idx < 2)
              task->regions[idx].target_ranking.push_back(fb_mem);
            else
              task->regions[idx].target_ranking.push_back(zc_mem);
            task->regions[idx].virtual_map = false;
            task->regions[idx].enable_WAR_optimization = war_enabled;
            task->regions[idx].reduction_list = false;
            // Make everything SOA
            task->regions[idx].blocking_factor = 
              task->regions[idx].max_blocking_factor;
          }
          break;
        }
      case UPDATE_VOLTAGES_TASK_ID:
        {
          for (unsigned idx = 0; idx < task->regions.size(); idx++)
          {
            // Only shared write stuff needs to go in zc_mem
            if (idx != 1)
              task->regions[idx].target_ranking.push_back(fb_mem);
            else
              task->regions[idx].target_ranking.push_back(zc_mem);
            task->regions[idx].virtual_map = false;
            task->regions[idx].enable_WAR_optimization = war_enabled;
            task->regions[idx].reduction_list = false;
            // Make everything SOA
            task->regions[idx].blocking_factor = 
              task->regions[idx].max_blocking_factor;
          }
          break;
        }
      default:
        assert(false); // should never get here
    }
  }
  else
  {
    // Put everything in the system memory
    Memory sys_mem = 
      machine_interface.find_memory_kind(task->target_proc,
                                         Memory::SYSTEM_MEM);
    assert(sys_mem.exists());
    for (unsigned idx = 0; idx < task->regions.size(); idx++)
    {
      task->regions[idx].target_ranking.push_back(sys_mem);
      task->regions[idx].virtual_map = false;
      task->regions[idx].enable_WAR_optimization = war_enabled;
      task->regions[idx].reduction_list = false;
      // Make everything SOA
      task->regions[idx].blocking_factor = 
        task->regions[idx].max_blocking_factor;
    }
  }
  // We don't care about the result
  return false;
}

