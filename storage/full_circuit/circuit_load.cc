
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <time.h>

#include "circuit.h"
#include "circuit_mapper.h"
#include "legion.h"
#include "../legion_storage.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionStorage;

LegionRuntime::Logger::Category log_circuit("circuit");

// Utility functions (forward declarations)
void parse_input_args(char **argv, int argc, int &num_loops, int &num_pieces,
                      int &nodes_per_piece, int &wires_per_piece,
                      int &pct_wire_in_piece, int &random_seed,
		      int &sync, bool &perform_checks);

Partitions load_circuit(Circuit &ckt, std::vector<CircuitPiece> &pieces, Context ctx,
                        HighLevelRuntime *runtime, int num_pieces, int nodes_per_piece,
                        int wires_per_piece, int pct_wire_in_piece, int random_seed);

void allocate_node_fields(Context ctx, HighLevelRuntime *runtime, FieldSpace node_space);
void allocate_wire_fields(Context ctx, HighLevelRuntime *runtime, FieldSpace wire_space);
void allocate_locator_fields(Context ctx, HighLevelRuntime *runtime, FieldSpace locator_space);

void top_level_task(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, HighLevelRuntime *runtime)
{
  int num_loops = 10;
  int num_pieces = 4;
  int nodes_per_piece = 2;
  int wires_per_piece = 4;
  int pct_wire_in_piece = 95;
  int random_seed = 12345;
  int sync = 0;
  bool perform_checks = true;
  {
    const InputArgs &command_args = HighLevelRuntime::get_input_args();
    char **argv = command_args.argv;
    int argc = command_args.argc;

    parse_input_args(argv, argc, num_loops, num_pieces, nodes_per_piece, 
		     wires_per_piece, pct_wire_in_piece, random_seed,
		     sync, perform_checks);

    log_circuit.print("circuit settings: loops=%d pieces=%d nodes/piece=%d "
                            "wires/piece=%d pct_in_piece=%d seed=%d",
       num_loops, num_pieces, nodes_per_piece, wires_per_piece,
       pct_wire_in_piece, random_seed);
  }

  Circuit circuit;
  {
    int num_circuit_nodes = num_pieces * nodes_per_piece;
    int num_circuit_wires = num_pieces * wires_per_piece;
    // Make index spaces
    //IndexSpace node_index_space = runtime->create_index_space(ctx,num_circuit_nodes);
    //IndexSpace wire_index_space = runtime->create_index_space(ctx,num_circuit_wires);
    IndexSpace node_index_space = LegionStorageManager::load_index_space_tree_from_file("NodeSpace.dat", runtime, ctx);
    IndexSpace wire_index_space = LegionStorageManager::load_index_space_tree_from_file("WireSpace.dat", runtime, ctx);
    // Make field spaces
    FieldSpace node_field_space = runtime->create_field_space(ctx);
    FieldSpace wire_field_space = runtime->create_field_space(ctx);
    FieldSpace locator_field_space = runtime->create_field_space(ctx);
    // Allocate fields
    allocate_node_fields(ctx, runtime, node_field_space);
    allocate_wire_fields(ctx, runtime, wire_field_space);
    allocate_locator_fields(ctx, runtime, locator_field_space);
    // Make logical regions
    circuit.all_nodes = runtime->create_logical_region(ctx,node_index_space,node_field_space);
    circuit.all_wires = runtime->create_logical_region(ctx,wire_index_space,wire_field_space);
    circuit.node_locator = runtime->create_logical_region(ctx,node_index_space,locator_field_space);
  }

  // Load the circuit
  std::vector<CircuitPiece> pieces(num_pieces);
  Partitions parts = load_circuit(circuit, pieces, ctx, runtime, num_pieces, nodes_per_piece,
                                  wires_per_piece, pct_wire_in_piece, random_seed);

  std::set<LogicalRegion> nodes_region_set = LegionStorageManager::helper_get_region_set(circuit.all_nodes, runtime, ctx, LegionStorageManager::REGIONSET_LEAVES);
  std::map<FieldID, char*> nodes_field_set;
  nodes_field_set[FID_NODE_CAP] = "FID_NODE_CAP";
  nodes_field_set[FID_LEAKAGE] = "FID_LEAKAGE";
  nodes_field_set[FID_CHARGE] = "FID_CHARGE";
  nodes_field_set[FID_NODE_VOLTAGE] = "FID_NODE_VOLTAGE";
  LegionStorageManager::load_logical_region_data_from_file(nodes_region_set, nodes_field_set, "input/nodes", runtime, ctx, 0, true);

  // check nodes value
  for (int n = 0; n < num_pieces; n++) {
    LogicalRegion child_nodes = runtime->get_logical_subregion_by_color(ctx, parts.pvt_nodes, n);
    RegionRequirement nodes_req(child_nodes, READ_ONLY, EXCLUSIVE, circuit.all_nodes);
    nodes_req.add_field(FID_NODE_CAP);
    nodes_req.add_field(FID_LEAKAGE);
    nodes_req.add_field(FID_CHARGE);
    nodes_req.add_field(FID_NODE_VOLTAGE);
    PhysicalRegion nodes = runtime->map_region(ctx, nodes_req);
    nodes.wait_until_valid();
    RegionAccessor<AccessorType::Generic, float> fa_node_cap =
      nodes.get_field_accessor(FID_NODE_CAP).typeify<float>();
    RegionAccessor<AccessorType::Generic, float> fa_node_leakage =
      nodes.get_field_accessor(FID_LEAKAGE).typeify<float>();
    RegionAccessor<AccessorType::Generic, float> fa_node_charge =
      nodes.get_field_accessor(FID_CHARGE).typeify<float>();
    RegionAccessor<AccessorType::Generic, float> fa_node_voltage =
      nodes.get_field_accessor(FID_NODE_VOLTAGE).typeify<float>();
    IndexIterator itr(child_nodes.get_index_space());
    while(itr.has_next())
    {
      assert(itr.has_next());
      ptr_t node_ptr = itr.next();
      printf("CAP[%d, %u]: %f\n", n, node_ptr.value, fa_node_cap.read(node_ptr));
      printf("LEAK[%d, %u]: %f\n", n, node_ptr.value, fa_node_leakage.read(node_ptr));
      printf("CHARGE[%d, %u]: %f\n", n, node_ptr.value, fa_node_charge.read(node_ptr));
      printf("VOLTAGE[%d, %u]: %f\n", n, node_ptr.value, fa_node_voltage.read(node_ptr));
    }
    runtime->unmap_region(ctx, nodes);
  }

  std::set<LogicalRegion> wires_region_set = LegionStorageManager::helper_get_region_set(circuit.all_wires, runtime, ctx, LegionStorageManager::REGIONSET_LEAVES);
  std::map<FieldID, char*> wires_field_set;
  wires_field_set[FID_IN_PTR] = "FID_IN_PTR";
  wires_field_set[FID_OUT_PTR] = "FID_OUT_PTR";
  wires_field_set[FID_IN_LOC] = "FID_IN_LOC";
  wires_field_set[FID_OUT_LOC] = "FID_OUT_LOC";
  wires_field_set[FID_INDUCTANCE] = "FID_INDUCTANCE";
  wires_field_set[FID_RESISTANCE] = "FID_RESISTANCE";
  wires_field_set[FID_WIRE_CAP] = "FID_WIRE_CAP";
  for (int i = 0; i < WIRE_SEGMENTS; i++) {
    char* buf = new char[32];
    sprintf(buf, "FID_CURRENT_%d", i);
    wires_field_set[FID_CURRENT+i] = buf;
  }
  for (int i = 0; i < (WIRE_SEGMENTS-1); i++) {
    char* buf = new char[32];
    sprintf(buf, "FID_WIRE_VOLTAGE_%d", i);
    wires_field_set[FID_WIRE_VOLTAGE+i] = buf;
  }
  LegionStorageManager::load_logical_region_data_from_file(wires_region_set, wires_field_set, "input/wires", runtime, ctx, 0, true);

  // check wires value
  for (int n = 0; n < num_pieces; n++) {
    LogicalRegion child_wires = runtime->get_logical_subregion_by_color(ctx, parts.pvt_wires, n);
    RegionRequirement wires_req(child_wires, READ_ONLY, EXCLUSIVE, circuit.all_wires);
    wires_req.add_field(FID_IN_PTR);
    wires_req.add_field(FID_OUT_PTR);
    wires_req.add_field(FID_IN_LOC);
    wires_req.add_field(FID_OUT_LOC);
    wires_req.add_field(FID_INDUCTANCE);
    wires_req.add_field(FID_RESISTANCE);
    wires_req.add_field(FID_WIRE_CAP);
    for (int i = 0; i < WIRE_SEGMENTS; i++)
      wires_req.add_field(FID_CURRENT+i);
    for (int i = 0; i < (WIRE_SEGMENTS-1); i++)
      wires_req.add_field(FID_WIRE_VOLTAGE+i);
    PhysicalRegion wires = runtime->map_region(ctx, wires_req);
    wires.wait_until_valid();
    RegionAccessor<AccessorType::Generic, ptr_t> fa_wire_in_ptr =
      wires.get_field_accessor(FID_IN_PTR).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, ptr_t> fa_wire_out_ptr =
      wires.get_field_accessor(FID_OUT_PTR).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, float> fa_wire_inductance =
      wires.get_field_accessor(FID_INDUCTANCE).typeify<float>();
    RegionAccessor<AccessorType::Generic, float> fa_wire_resistance =
      wires.get_field_accessor(FID_RESISTANCE).typeify<float>();
    RegionAccessor<AccessorType::Generic, float> fa_wire_cap =
      wires.get_field_accessor(FID_WIRE_CAP).typeify<float>();
    IndexIterator itr(child_wires);
    while(itr.has_next())
    {
      assert(itr.has_next());
      ptr_t wire_ptr = itr.next();
      printf("RES[%d, %u]: %f\n", n, wire_ptr.value, fa_wire_resistance.read(wire_ptr));
      printf("IND[%d, %u]: %f\n", n, wire_ptr.value, fa_wire_inductance.read(wire_ptr));
      printf("CAP[%d, %u]: %f\n", n, wire_ptr.value, fa_wire_cap.read(wire_ptr));
      ptr_t in_ptr = fa_wire_in_ptr.read(wire_ptr);
      printf("IN[%d, %u]: %u\n", n, wire_ptr.value, in_ptr.value);
      ptr_t out_ptr = fa_wire_out_ptr.read(wire_ptr);
      printf("OUT[%d, %u]: %d\n", n, wire_ptr.value, out_ptr.value);
    }
    runtime->unmap_region(ctx, wires);
  }

  std::set<LogicalRegion> locator_region_set = LegionStorageManager::helper_get_region_set(circuit.node_locator, runtime, ctx, LegionStorageManager::REGIONSET_LEAVES);
  std::map<FieldID, char*> locator_field_set;
  locator_field_set[FID_LOCATOR] = "FID_LOCATOR";
  LegionStorageManager::load_logical_region_data_from_file(locator_region_set, locator_field_set, "input/locator", runtime, ctx, 0, true);

  // Arguments for each point
  ArgumentMap local_args;
  for (int idx = 0; idx < num_pieces; idx++)
  {
    DomainPoint point = DomainPoint::from_point<1>(Point<1>(idx));
    local_args.set_point(point, TaskArgument(&(pieces[idx]),sizeof(CircuitPiece)));
  }

  // Make the launchers
  Rect<1> launch_rect(Point<1>(0), Point<1>(num_pieces-1)); 
  Domain launch_domain = Domain::from_rect<1>(launch_rect);
  CalcNewCurrentsTask cnc_launcher(parts.pvt_wires, parts.pvt_nodes, parts.shr_nodes, parts.ghost_nodes,
                                   circuit.all_wires, circuit.all_nodes, launch_domain, local_args);

  DistributeChargeTask dsc_launcher(parts.pvt_wires, parts.pvt_nodes, parts.shr_nodes, parts.ghost_nodes,
                                    circuit.all_wires, circuit.all_nodes, launch_domain, local_args);

  UpdateVoltagesTask upv_launcher(parts.pvt_nodes, parts.shr_nodes, parts.node_locations,
                                 circuit.all_nodes, circuit.node_locator, launch_domain, local_args);

  printf("Starting main simulation loop\n");
  struct timespec ts_start, ts_end;
  clock_gettime(CLOCK_MONOTONIC, &ts_start);
  // Run the main loop
  bool simulation_success = true;
  for (int i = 0; i < num_loops; i++)
  {
    TaskHelper::dispatch_task<CalcNewCurrentsTask>(cnc_launcher, ctx, runtime, 
                                                   perform_checks, simulation_success);
    TaskHelper::dispatch_task<DistributeChargeTask>(dsc_launcher, ctx, runtime, 
                                                    perform_checks, simulation_success);
    TaskHelper::dispatch_task<UpdateVoltagesTask>(upv_launcher, ctx, runtime, 
                                                  perform_checks, simulation_success,
                                                  ((i+1)==num_loops));
  }
  clock_gettime(CLOCK_MONOTONIC, &ts_end);
  if (simulation_success)
    printf("SUCCESS!\n");
  else
    printf("FAILURE!\n");
  {
    double sim_time = ((1.0 * (ts_end.tv_sec - ts_start.tv_sec)) +
                       (1e-9 * (ts_end.tv_nsec - ts_start.tv_nsec)));
    printf("ELAPSED TIME = %7.3f s\n", sim_time);

    // Compute the floating point operations per second
    long num_circuit_nodes = num_pieces * nodes_per_piece;
    long num_circuit_wires = num_pieces * wires_per_piece;
    // calculate currents
    long operations = num_circuit_wires * (WIRE_SEGMENTS*6 + (WIRE_SEGMENTS-1)*4) * STEPS;
    // distribute charge
    operations += (num_circuit_wires * 4);
    // update voltages
    operations += (num_circuit_nodes * 4);
    // multiply by the number of loops
    operations *= num_loops;

    // Compute the number of gflops
    double gflops = (1e-9*operations)/sim_time;
    printf("GFLOPS = %7.3f GFLOPS\n", gflops);
  }
  log_circuit.print("simulation complete - destroying regions");

  // Now we can destroy all the things that we made
  {
    runtime->destroy_logical_region(ctx,circuit.all_nodes);
    runtime->destroy_logical_region(ctx,circuit.all_wires);
    runtime->destroy_logical_region(ctx,circuit.node_locator);
    runtime->destroy_field_space(ctx,circuit.all_nodes.get_field_space());
    runtime->destroy_field_space(ctx,circuit.all_wires.get_field_space());
    runtime->destroy_field_space(ctx,circuit.node_locator.get_field_space());
    runtime->destroy_index_space(ctx,circuit.all_nodes.get_index_space());
    runtime->destroy_index_space(ctx,circuit.all_wires.get_index_space());
  }
}

static void update_mappers(Machine *machine, HighLevelRuntime *rt,
                           const std::set<Processor> &local_procs)
{
  for (std::set<Processor>::const_iterator it = local_procs.begin();
        it != local_procs.end(); it++)
  {
    rt->replace_default_mapper(new CircuitMapper(machine, rt, *it), *it);
  }
}

int main(int argc, char **argv)
{
  HighLevelRuntime::set_top_level_task_id(TOP_LEVEL_TASK_ID);
  HighLevelRuntime::register_legion_task<top_level_task>(TOP_LEVEL_TASK_ID,
      Processor::LOC_PROC, true/*single*/, false/*index*/);
  // If we're running on the shared low-level then only register cpu tasks
#ifdef SHARED_LOWLEVEL
  TaskHelper::register_cpu_variants<CalcNewCurrentsTask>();
  TaskHelper::register_cpu_variants<DistributeChargeTask>();
  TaskHelper::register_cpu_variants<UpdateVoltagesTask>();
#else
  TaskHelper::register_hybrid_variants<CalcNewCurrentsTask>();
  TaskHelper::register_hybrid_variants<DistributeChargeTask>();
  TaskHelper::register_hybrid_variants<UpdateVoltagesTask>();
#endif
  CheckTask::register_task();
  HighLevelRuntime::register_reduction_op<AccumulateCharge>(REDUCE_ID);
  HighLevelRuntime::set_registration_callback(update_mappers);

  LegionStorageManager::initialize();

  return HighLevelRuntime::start(argc, argv);
}

void parse_input_args(char **argv, int argc, int &num_loops, int &num_pieces,
                      int &nodes_per_piece, int &wires_per_piece,
                      int &pct_wire_in_piece, int &random_seed,
		      int &sync, bool &perform_checks)
{
  for (int i = 1; i < argc; i++) 
  {
    if (!strcmp(argv[i], "-l")) 
    {
      num_loops = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-p")) 
    {
      num_pieces = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-npp")) 
    {
      nodes_per_piece = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-wpp")) 
    {
      wires_per_piece = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-pct")) 
    {
      pct_wire_in_piece = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-s")) 
    {
      random_seed = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-sync")) 
    {
      sync = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-checks"))
    {
      perform_checks = true;
      continue;
    }
  }
}

void allocate_node_fields(Context ctx, HighLevelRuntime *runtime, FieldSpace node_space)
{
  FieldAllocator allocator = runtime->create_field_allocator(ctx, node_space);
  allocator.allocate_field(sizeof(float), FID_NODE_CAP);
  allocator.allocate_field(sizeof(float), FID_LEAKAGE);
  allocator.allocate_field(sizeof(float), FID_CHARGE);
  allocator.allocate_field(sizeof(float), FID_NODE_VOLTAGE);
}

void allocate_wire_fields(Context ctx, HighLevelRuntime *runtime, FieldSpace wire_space)
{
  FieldAllocator allocator = runtime->create_field_allocator(ctx, wire_space);
  allocator.allocate_field(sizeof(ptr_t), FID_IN_PTR);
  allocator.allocate_field(sizeof(ptr_t), FID_OUT_PTR);
  allocator.allocate_field(sizeof(PointerLocation), FID_IN_LOC);
  allocator.allocate_field(sizeof(PointerLocation), FID_OUT_LOC);
  allocator.allocate_field(sizeof(float), FID_INDUCTANCE);
  allocator.allocate_field(sizeof(float), FID_RESISTANCE);
  allocator.allocate_field(sizeof(float), FID_WIRE_CAP);
  for (int i = 0; i < WIRE_SEGMENTS; i++)
    allocator.allocate_field(sizeof(float), FID_CURRENT+i);
  for (int i = 0; i < (WIRE_SEGMENTS-1); i++)
    allocator.allocate_field(sizeof(float), FID_WIRE_VOLTAGE+i);
}

void allocate_locator_fields(Context ctx, HighLevelRuntime *runtime, FieldSpace locator_space)
{
  FieldAllocator allocator = runtime->create_field_allocator(ctx, locator_space);
  allocator.allocate_field(sizeof(float), FID_LOCATOR);
}

PointerLocation find_location(ptr_t ptr, const std::set<ptr_t> &private_nodes,
                              const std::set<ptr_t> &shared_nodes, const std::set<ptr_t> &ghost_nodes)
{
  if (private_nodes.find(ptr) != private_nodes.end())
  {
    return PRIVATE_PTR;
  }
  else if (shared_nodes.find(ptr) != shared_nodes.end())
  {
    return SHARED_PTR;
  }
  else if (ghost_nodes.find(ptr) != ghost_nodes.end())
  {
    return GHOST_PTR;
  }
  // Should never make it here, if we do something bad happened
  assert(false);
  return PRIVATE_PTR;
}

template<typename T>
static T random_element(const std::set<T> &set)
{
  int index = int(drand48() * set.size());
  typename std::set<T>::const_iterator it = set.begin();
  while (index-- > 0) it++;
  return *it;
}

Partitions load_circuit(Circuit &ckt, std::vector<CircuitPiece> &pieces, Context ctx,
                        HighLevelRuntime *runtime, int num_pieces, int nodes_per_piece,
                        int wires_per_piece, int pct_wire_in_piece, int random_seed)
{
  log_circuit.print("Initializing circuit simulation...");

  // Now we can create our partitions and update the circuit pieces

  // first create the privacy partition that splits all the nodes into either shared or private
  IndexPartition privacy_part = runtime->get_index_partition(ctx, ckt.all_nodes.get_index_space(), 0);
  
  IndexSpace all_private = runtime->get_index_subspace(ctx, privacy_part, 0);
  IndexSpace all_shared  = runtime->get_index_subspace(ctx, privacy_part, 1);
  

  // Now create partitions for each of the subregions
  Partitions result;
  IndexPartition priv = runtime->get_index_partition(ctx, all_private, 0);
  result.pvt_nodes = runtime->get_logical_partition_by_tree(ctx, priv, ckt.all_nodes.get_field_space(), ckt.all_nodes.get_tree_id());
  IndexPartition shared = runtime->get_index_partition(ctx, all_shared, 0);
  result.shr_nodes = runtime->get_logical_partition_by_tree(ctx, shared, ckt.all_nodes.get_field_space(), ckt.all_nodes.get_tree_id());
  IndexPartition ghost = runtime->get_index_partition(ctx, all_shared, 1);
  result.ghost_nodes = runtime->get_logical_partition_by_tree(ctx, ghost, ckt.all_nodes.get_field_space(), ckt.all_nodes.get_tree_id());

  IndexPartition pvt_wires = runtime->get_index_partition(ctx, ckt.all_wires.get_index_space(), 0);
  result.pvt_wires = runtime->get_logical_partition_by_tree(ctx, pvt_wires, ckt.all_wires.get_field_space(), ckt.all_wires.get_tree_id()); 

  IndexPartition locs = runtime->get_index_partition(ctx, ckt.node_locator.get_index_space(), 1);
  result.node_locations = runtime->get_logical_partition_by_tree(ctx, locs, ckt.node_locator.get_field_space(), ckt.node_locator.get_tree_id());

  // Build the pieces
  for (int n = 0; n < num_pieces; n++)
  {
    pieces[n].pvt_nodes = runtime->get_logical_subregion_by_color(ctx, result.pvt_nodes, n);
    pieces[n].shr_nodes = runtime->get_logical_subregion_by_color(ctx, result.shr_nodes, n);
    pieces[n].ghost_nodes = runtime->get_logical_subregion_by_color(ctx, result.ghost_nodes, n);
    pieces[n].pvt_wires = runtime->get_logical_subregion_by_color(ctx, result.pvt_wires, n);
    pieces[n].num_wires = wires_per_piece;
    //pieces[n].first_wire = first_wires[n];
    pieces[n].first_wire = ptr_t(n * wires_per_piece);
    pieces[n].num_nodes = nodes_per_piece;
    //pieces[n].first_node = first_nodes[n];
    pieces[n].first_node = ptr_t(n * nodes_per_piece);
  }

  log_circuit.print("Finished initializing simulation...");

  return result;
}

