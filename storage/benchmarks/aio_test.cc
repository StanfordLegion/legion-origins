#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h>
#include <sys/syscall.h>
#include <linux/aio_abi.h>
#include <queue>
#include <algorithm>

inline int io_setup(unsigned nr, aio_context_t *ctxp)
{
  return syscall(__NR_io_setup, nr, ctxp);
}

inline int io_destroy(aio_context_t ctx)
{
  return syscall(__NR_io_destroy, ctx);
}

inline int io_submit(aio_context_t ctx, long nr, struct iocb **iocbpp)
{
  return syscall(__NR_io_submit, ctx, nr, iocbpp);
}

inline int io_getevents(aio_context_t ctx, long min_nr, long max_nr,
                        struct io_event *events, struct timespec *timeout)
{
  return syscall(__NR_io_getevents, ctx, min_nr, max_nr, events, timeout);
}

time_t start_sec;

void start_timer()
{
  timespec time;
  clock_gettime(CLOCK_REALTIME, &time);
  start_sec = time.tv_sec;
}

double get_current_time()
{
  timespec time;
  clock_gettime(CLOCK_REALTIME, &time);
  double ret = time.tv_sec - start_sec;
  return (double) time.tv_nsec / 1000 + ret * 1000000;
}

void print_latency_stats(int buf_size, int fly_request, double* starttime, double* midtime, int cnt, double avg_latency)
{
  double* latency = (double*) calloc(cnt, sizeof(double));
  for (int i = 0; i < cnt; i++)
    latency[i] = midtime[i] - starttime[i];
  std::sort(latency, latency + cnt);
  printf("buf = %d, fly = %d, lat_avg = %.2lf, lat_5 = %.2lf, lat_95 = %.2lf\n", buf_size, fly_request, latency[cnt / 2], latency[cnt / 20], latency[cnt * 19 / 20]);
  free(latency);
}

int main()
{
  const size_t TOTAL_SIZE = 1024 * 1024 * 1024;
  const int BUF_SIZE_CNT = 4;
  const size_t BLOCK_SIZE = 512;
  const int FLY_REQUEST_CNT = 37;
  const int MAX_SAMPLE_CNT = 1024 * 1024;
  const int MIN_RECEIVED = 0;
  const int MAX_RECEIVED = 1000;
  const int MAX_SUBMITTED = 1;
  //const int BUF_SIZE_ARRAY[BUF_SIZE_CNT] = {64, 128, 256, 512, 1024, 2 * 1024, 4 * 1024, 8 * 1024, 16 * 1024, 32 * 1024, 64 * 1024, 128 * 1024, 256 * 1024, 512 * 1024, 1024 * 1024};
  const int BUF_SIZE_ARRAY[BUF_SIZE_CNT] = {1024, 8 * 1024, 64 * 1024, 512 * 1024};
  const int FLY_REQUEST_ARRAY[FLY_REQUEST_CNT] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000};
  //const int BUF_SIZE_CNT = 1;
  //const int FLY_REQUEST_CNT = 1;
  //const int BUF_SIZE_ARRAY[BUF_SIZE_CNT] = {4 * 1024};
  //const int FLY_REQUEST_ARRAY[1] = {700}; 
  double latency[BUF_SIZE_CNT][FLY_REQUEST_CNT], throughput[BUF_SIZE_CNT][FLY_REQUEST_CNT];
  char* buf;
  posix_memalign((void**)&buf, BLOCK_SIZE, TOTAL_SIZE);
  double* starttime = (double*) calloc(MAX_SAMPLE_CNT, sizeof(double));
  double* midtime = (double*) calloc(MAX_SAMPLE_CNT, sizeof(double));
  double* finishtime = (double*) calloc(MAX_SAMPLE_CNT, sizeof(double));
  for (int it1 = 0; it1 < BUF_SIZE_CNT; it1++)
    for (int it2 = 0; it2 < FLY_REQUEST_CNT; it2++) {
      start_timer();
      int fd = open("/scratch/sdb2_raw", O_CREAT | O_WRONLY | O_DIRECT, 00777);
      // int fd = open("/scratch/sdb1_ext4/test.tmp", O_RDONLY | O_DIRECT, 00777);
      int BUF_SIZE = BUF_SIZE_ARRAY[it1];
      int FLY_REQUEST = FLY_REQUEST_ARRAY[it2];
      int NUM_BUF = TOTAL_SIZE / BUF_SIZE;
      if (NUM_BUF > MAX_SAMPLE_CNT)
        NUM_BUF = MAX_SAMPLE_CNT;
      for (int i = 0; i < NUM_BUF * BUF_SIZE; i++) {
        buf[i] = (char)((it1 + it2) % 200);
      }
      
      aio_context_t ctx = 0;
      int ret = io_setup(FLY_REQUEST * 10, &ctx);
      if (ret < 0) {
        perror("io setup");
        return -1;
      }

      struct iocb cb[FLY_REQUEST];
      struct iocb *cbs[FLY_REQUEST];
      struct io_event events[FLY_REQUEST];
      std::queue<struct iocb *> available_cb;
      assert(available_cb.empty());
      double measure_start, measure_stop;
      timespec time_start, time_stop;
      double time_spent;
      measure_start = get_current_time();
      clock_gettime(CLOCK_REALTIME, &time_start);
      //printf("measure start: %.2lf\n", measure_start);
      //printf("time start: %ld:%ld\n", time_start.tv_sec, time_start.tv_nsec);
      for (int i = 0; i < FLY_REQUEST; i++) {
        memset(&cb[i], 0, sizeof(cb[i]));
        cb[i].aio_fildes = fd;
        cb[i].aio_lio_opcode = IOCB_CMD_PWRITE;
        cb[i].aio_nbytes = BUF_SIZE;
        cb[i].aio_buf = (uint64_t) (buf + i * BUF_SIZE);
        cb[i].aio_offset = i * BUF_SIZE;
        cbs[i] = &cb[i];
        starttime[i] = get_current_time();
        //printf("submit time: %.2lf\n", midtime[i] - starttime[i]);
        finishtime[i] = 0.0;
      }
      ret = io_submit(ctx, FLY_REQUEST, cbs);
      if (ret != FLY_REQUEST)
        perror("io_submit error");
      for (int i = 0; i < FLY_REQUEST; i++)
        midtime[i] = get_current_time();
      
      int pos = FLY_REQUEST;
      while (pos < NUM_BUF) {
        double before_time = get_current_time();
        ret = io_getevents(ctx, MIN_RECEIVED, MAX_RECEIVED, events, NULL);
        //printf("[WAIT] nr = %d, t1 = %.2lf\n", ret, get_current_time() - before_time);
        if (ret < 0)
          perror("io_getevents error");
        int nr = ret;
        //if (nr > NUM_BUF - pos)
          //nr = NUM_BUF - pos;
        for (int j = 0; j < nr; j++) {
          struct iocb* ret_cb = (struct iocb*) events[j].obj;
          int idx = ret_cb->aio_offset / BUF_SIZE;
          if (events[j].res != BUF_SIZE) {
            printf("%lld\n", events[j].res);
          }
          else {
            //printf("success\n");
          }
          finishtime[idx] = get_current_time();
          available_cb.push(ret_cb);
          //starttime[pos + j] = get_current_time();
          //cbs[j] = ret_cb;
          //cbs[j]->aio_buf = (uint64_t) (buf + (pos + j) * BUF_SIZE);
          //cbs[j]->aio_offset = (pos + j) * BUF_SIZE;
        }
        //printf("t1 = %.2lf, t2 = %.2lf\n", midtime[idx] - starttime[idx], finishtime[idx] - starttime[idx]);
        
        int ns = 0;
        while (pos + ns < NUM_BUF && ns < MAX_SUBMITTED && !available_cb.empty()) {
          starttime[pos + ns] = get_current_time();
          cbs[ns] = available_cb.front();
          available_cb.pop();
          cbs[ns]->aio_buf = (uint64_t) (buf + (pos + ns) * BUF_SIZE);
          cbs[ns]->aio_offset = (pos + ns) * BUF_SIZE;
          ns++;
        }
        if (ns > 0) {
          ret = io_submit(ctx, ns, cbs);
          if (ret != ns)
            perror("io_submit error");
          //printf("[SUBMIT] nr = %d, t1 = %.2lf", nr, get_current_time() - starttime[pos]);
          for (int j = 0; j < ns; j++) {
            midtime[pos + j] = get_current_time();
            finishtime[pos + j] = 0.0;
          }
          pos = pos + ns;
        }
      }
      close(fd);
      measure_stop = get_current_time();
      clock_gettime(CLOCK_REALTIME, &time_stop);
      io_destroy(ctx);
      time_spent = time_stop.tv_sec - time_start.tv_sec;
      time_spent = time_spent * 1000000 + ((double) time_stop.tv_nsec - time_start.tv_nsec) / 1000;
      //printf("measure stop: %ld\n", measure_stop);
      //printf("time stop: %ld:%ld\n", time_stop.tv_sec, time_stop.tv_nsec);
      //printf("measure time = %ld\n", measure_stop - measure_start);
      long long count = 0, err_cnt = 0;
      double sum_1 = 0, sum_2 = 0;
      //for (int i = 0; i < NUM_BUF * BUF_SIZE; i++)
        //if (buf[i] != 50) printf("read error!\n");
      for (int i = 0; i < NUM_BUF; i++) {
        if (finishtime[i] != 0) {
          count ++;
          sum_1 = sum_1 + midtime[i] - starttime[i];
          sum_2 = sum_2 + finishtime[i] - starttime[i];
        }
      }
      latency[it1][it2] = sum_1 / count;
      throughput[it1][it2] = ((double)BUF_SIZE * count / (time_spent * 1.024 * 1.024));
      print_latency_stats(BUF_SIZE, FLY_REQUEST, starttime, midtime, count, latency[it1][it2]);
      printf("buf = %d, fly = %d, latency = %.2lf, complete = %.2lf, throughput = %.2lf, count = %lld, err_cnt = %lld\n", BUF_SIZE, FLY_REQUEST, latency[it1][it2], sum_2 / count, throughput[it1][it2], count, err_cnt);
    }
}
