#include "channel.h"

  inline int io_setup(unsigned nr, aio_context_t *ctxp)
  {
    return syscall(__NR_io_setup, nr, ctxp);
  }

  inline int io_destroy(aio_context_t ctx)
  {
    return syscall(__NR_io_destroy, ctx);
  }

  inline int io_submit(aio_context_t ctx, long nr, struct iocb **iocbpp)
  {
    return syscall(__NR_io_submit, ctx, nr, iocbpp);
  }

  inline int io_getevents(aio_context_t ctx, long min_nr, long max_nr,
                          struct io_event *events, struct timespec *timeout)
  {
    return syscall(__NR_io_getevents, ctx, min_nr, max_nr, events, timeout);
  }

  DiskWriteXferDes::DiskWriteXferDes(
                   uint64_t _channel_id, uint64_t _req_nbytes,
                   bool has_pre_XferDes, bool has_next_XferDes,
                   uint64_t _src_bufsize, uint64_t _dst_bufsize,
                   uint64_t _bytes_total, void* _mem_addr,
                   long max_nr)
  {
    kind = XferDes::XFER_DISK_WRITE;
    channel_id = _channel_id;
    req_nbytes = _req_nbytes;
    bytes_submit = bytes_read = bytes_write = 0;
    pre_XferDes = NULL;
    pre_bytes_write = (!has_pre_XferDes) ? _bytes_total : 0;
    next_XferDes = NULL;
    next_bytes_read = 0;
    src_bufsize = _src_bufsize;
    dst_bufsize = _dst_bufsize;
    bytes_total = _bytes_total;
    mem_addr = _mem_addr;
    // make sure bytes_total is a multiple of req_nbytes
    assert(bytes_total % req_nbytes == 0);
    assert(!has_next_XferDes);
    //allocate memory for DiskWRiteRequests, and push them into
    // available_queue
    requests = (DiskWriteRequest*) calloc(max_nr, sizeof(DiskWriteRequest));
    for (int i = 0; i < max_nr; i++) {
      requests[i].xd = this;
      available_queue.push_back(&requests[i]);
    }
  }

  long DiskWriteXferDes::get_requests(Request** requests, long nr)
  {
    DiskWriteRequest** disk_write_reqs = (DiskWriteRequest**) requests;
    long cnt = num_available_reqs();
    if (cnt > nr)
      cnt = nr;
    for (int i = 0; i < cnt; i++) {
      disk_write_reqs[i] = (DiskWriteRequest*) available_queue.back();
      available_queue.pop_back();
      disk_write_reqs[i]->uid = i + bytes_submit / req_nbytes;
      flying_reqs.insert(std::pair<uint64_t, Request*>(disk_write_reqs[i]->uid, disk_write_reqs[i]));
      disk_write_reqs[i]->is_done = false;
      disk_write_reqs[i]->src_buf = (uint64_t)mem_addr + (req_nbytes * i + bytes_submit) % src_bufsize;
      disk_write_reqs[i]->dst_offset = req_nbytes * i + bytes_submit;
      disk_write_reqs[i]->nbytes = req_nbytes;
    }
    bytes_submit += req_nbytes * cnt;
    return cnt;
  }

  DiskReadXferDes::DiskReadXferDes(
                  uint64_t _channel_id, uint64_t _req_nbytes,
                  bool has_pre_XferDes, bool has_next_XferDes,
                  uint64_t _src_bufsize, uint64_t _dst_bufsize,
                  uint64_t _bytes_total, void* _mem_addr,
                  long max_nr)
  {
    kind = XferDes::XFER_DISK_READ;
    channel_id = _channel_id;
    req_nbytes = _req_nbytes;
    bytes_submit = bytes_read = bytes_write = 0;
    pre_XferDes = NULL;
    assert(!has_pre_XferDes);
    pre_bytes_write = _bytes_total;
    next_XferDes = NULL;
    next_bytes_read = 0;
    src_bufsize = _src_bufsize;
    dst_bufsize = _dst_bufsize;
    bytes_total = _bytes_total;
    mem_addr = _mem_addr;
    // make sure bytes_total is a multiple of req_nbytes
    assert(bytes_total % req_nbytes == 0);
    // allocate memory for DiskReadRequests, and push them into
    // available_queue
    requests = (DiskReadRequest*) calloc(max_nr, sizeof(DiskReadRequest));
    for (int i = 0; i < max_nr; i++) {
      requests[i].xd = this;
      available_queue.push_back(&requests[i]);
    }
  }

  long DiskReadXferDes::get_requests(Request** requests, long nr)
  {
    DiskReadRequest** disk_read_reqs = (DiskReadRequest**) requests;
    long cnt = num_available_reqs();
    if (cnt > nr)
      cnt = nr;
    for (int i = 0; i < cnt; i++) {
      disk_read_reqs[i] = (DiskReadRequest*) available_queue.back();
      available_queue.pop_back();
      disk_read_reqs[i]->uid = i + bytes_submit / req_nbytes;
      flying_reqs.insert(std::pair<uint64_t, Request*>(disk_read_reqs[i]->uid, disk_read_reqs[i]));
      disk_read_reqs[i]->is_done = false;
      disk_read_reqs[i]->dst_buf = (uint64_t)mem_addr + (req_nbytes * i + bytes_submit) % dst_bufsize;
      disk_read_reqs[i]->src_offset = req_nbytes * i + bytes_submit;
      disk_read_reqs[i]->nbytes = req_nbytes;
    }
    bytes_submit += req_nbytes * cnt;
    return cnt;
  }
  
  DiskReadChannel::DiskReadChannel(uint64_t id, int fd, long max_nr)
  {
    kind = XferDes::XFER_DISK_READ;
    uid = id;
    ctx = 0;
    capacity = max_nr;
    int ret = io_setup(max_nr, &ctx);
    assert(ret >= 0);
    assert(available_cb.empty());
    cb = (struct iocb*) calloc(max_nr, sizeof(struct iocb));
    cbs = (struct iocb**) calloc(max_nr, sizeof(struct iocb*));
    events = (struct io_event*) calloc(max_nr, sizeof(struct io_event));
    for (int i = 0; i < max_nr; i++) {
      memset(&cb[i], 0, sizeof(cb[i]));
      cb[i].aio_fildes = fd;
      cb[i].aio_lio_opcode = IOCB_CMD_PREAD;
      available_cb.push_back(&cb[i]);
    }
  }

  DiskReadChannel::~DiskReadChannel()
  {
    io_destroy(ctx);
    free(cb);
    free(cbs);
    free(events);
  }

  long DiskReadChannel::submit(Request** requests, long nr)
  {
    DiskReadRequest** disk_read_reqs = (DiskReadRequest**) requests;
    int ns = 0;
    while (ns < nr && !available_cb.empty()) {
      cbs[ns] = available_cb.back();
      available_cb.pop_back();
      cbs[ns]->aio_data = (uint64_t) (disk_read_reqs[ns]);
      cbs[ns]->aio_buf = disk_read_reqs[ns]->dst_buf;
      cbs[ns]->aio_offset = disk_read_reqs[ns]->src_offset;
      cbs[ns]->aio_nbytes = disk_read_reqs[ns]->nbytes;
      ns++;
    }
    int ret = io_submit(ctx, ns, cbs);
    if (ret < 0) {
      perror("io_submit error");
    }
    return ret;
  }

  void DiskReadChannel::poll()
  {
    int nr = io_getevents(ctx, 0, capacity, events, NULL);
    if (nr > 0)
      printf("Read::poll nr = %d\n", nr);
    if (nr < 0)
      perror("io_getevents error");
    for (int i = 0; i < nr; i++) {
      DiskReadRequest* req = (DiskReadRequest*) events[i].data;
      struct iocb* ret_cb = (struct iocb*) events[i].obj;
      available_cb.push_back(ret_cb);
      assert(events[i].res = req->nbytes);
      req->xd->notify_request_read_done(req->uid);
      req->xd->notify_request_write_done(req->uid);
    }
  }

  long DiskReadChannel::available()
  {
    return available_cb.size();
  }

  DiskWriteChannel::DiskWriteChannel(uint64_t id, int fd, long max_nr)
  {
    kind = XferDes::XFER_DISK_WRITE;
    uid = id;
    ctx = 0;
    capacity = max_nr;
    int ret = io_setup(max_nr, &ctx);
    assert(ret >= 0);
    assert(available_cb.empty());
    cb = (struct iocb*) calloc(max_nr, sizeof(struct iocb));
    cbs = (struct iocb**) calloc(max_nr, sizeof(struct iocb*));
    events = (struct io_event*) calloc(max_nr, sizeof(struct io_event));
    for (int i = 0; i < max_nr; i++) {
      memset(&cb[i], 0, sizeof(cb[i]));
      cb[i].aio_fildes = fd;
      cb[i].aio_lio_opcode = IOCB_CMD_PWRITE;
      available_cb.push_back(&cb[i]);
    }
  }

  DiskWriteChannel::~DiskWriteChannel()
  {
    io_destroy(ctx);
    free(cb);
    free(cbs);
    free(events);
  }

  long DiskWriteChannel::submit(Request** requests, long nr)
  {
    DiskWriteRequest** disk_write_reqs = (DiskWriteRequest**) requests;
    int ns = 0;
    while (ns < nr && !available_cb.empty()) {
      cbs[ns] = available_cb.back();
      available_cb.pop_back();
      cbs[ns]->aio_data = (uint64_t) (disk_write_reqs[ns]);
      cbs[ns]->aio_buf = disk_write_reqs[ns]->src_buf;
      printf("write_buf = %llu\n", cbs[ns]->aio_buf);
      cbs[ns]->aio_offset = disk_write_reqs[ns]->dst_offset;
      printf("write_offset = %lld\n", cbs[ns]->aio_offset);
      cbs[ns]->aio_nbytes = disk_write_reqs[ns]->nbytes;
      ns++;
    }
    int ret = io_submit(ctx, ns, cbs);
    if (ret < 0) {
      perror("io_submit error");
    }
    return ret;
  }

  void DiskWriteChannel::poll()
  {
    int nr = io_getevents(ctx, 0, capacity, events, NULL);
    if (nr > 0) 
      printf("Write::poll nr = %d\n", nr);
    if (nr < 0)
      perror("io_getevents error");
    for (int i = 0; i < nr; i++) {
      DiskWriteRequest* req = (DiskWriteRequest*) events[i].data;
      struct iocb* ret_cb = (struct iocb*) events[i].obj;
      available_cb.push_back(ret_cb);
      assert(events[i].res = req->nbytes);
      req->xd->notify_request_read_done(req->uid);
      req->xd->notify_request_write_done(req->uid);
    }
  }

  long DiskWriteChannel::available()
  {
    return available_cb.size();
  }

  void* DMAThread::start(void* arg)
  {
    DMAThread* dma = (DMAThread*) arg;
    while (true) {
      printf("CP#1\n");
      pthread_mutex_lock(&dma->channel_lock);
      std::vector<Channel*>::iterator it;
      for (it = dma->channel_queue.begin(); it != dma->channel_queue.end(); it++) {
        printf("CP#2\n");
        (*it)->poll();
        printf("CP#3\n");
        long nr = (*it)->available();
        printf("[%lu] available = %ld\n", (*it)->uid, nr);
        if (nr > dma->max_nr)
          nr = dma->max_nr;
        if (nr == 0)
          continue;
        pthread_mutex_lock(&dma->xferDes_lock);
        std::vector<XferDes*>::iterator it2;
        for (it2 = dma->xferDes_queue.begin(); it2 != dma->xferDes_queue.end(); it2++) {
          if ((*it2)->channel_id == (*it)->uid) {
            assert((*it2)->kind == (*it)->kind);
            long nr_got = (*it2)->get_requests(dma->requests, nr);
            printf("[%lu] nr_got = %ld\n", (*it2)->channel_id, nr_got);
            long nr_submitted = (*it)->submit(dma->requests, nr_got);
            printf("nr_submitted = %ld\n", nr_submitted);
            assert(nr_got == nr_submitted);
          }
        }
        pthread_mutex_unlock(&dma->xferDes_lock);
      }
      pthread_mutex_unlock(&dma->channel_lock);
    }
  }

