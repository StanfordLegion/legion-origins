#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <linux/aio_abi.h>
#include <map>
#include <vector>
#include <assert.h>
#include <pthread.h>
#include <string.h>

class XferDes;

class Request {
public:
  // a pointer to the owning xfer descriptor
  // this should set at Request creation
  XferDes* xd;
  // an unique id for identifying the request
  uint64_t uid;
  // a flag indicating whether this request is done
  bool is_done;
  /*
   * Virtual call back function that is invoked when read is done
   */
  // virtual void read_done();
  
  /*
   * Virtual call back function that is invoked when write is done
   */
  // virtual void write_done();
};

class DiskReadRequest : public Request {
public:
  uint64_t dst_buf;
  int64_t src_offset;
  uint64_t nbytes;
};

class DiskWriteRequest : public Request {
public:
  uint64_t src_buf;
  int64_t dst_offset;
  uint64_t nbytes;
};

class XferDes {
public:
  enum XferKind {
    XFER_DISK_READ,
    XFER_DISK_WRITE,
    XFER_SSD_READ,
    XFER_SSD_WRITE,
    XFER_GPU_READ,
    XFER_GPU_WRITE
  };
protected:
  uint64_t bytes_submit, bytes_read, bytes_write, bytes_total;
  uint64_t pre_bytes_write;
  uint64_t next_bytes_read;
  // number of bytes for each single request
  uint64_t req_nbytes;
  // buffer size for both end. If this num is smaller than
  // bytes_total, it means we have to reuse the buffer.
  uint64_t src_bufsize, dst_bufsize;
  // starting address of in-memory buffer
  void* mem_addr;
  // map from unique id to request class, this map only keeps 
  std::map<uint64_t, Request*> flying_reqs;
  // queue that contains all available free requests
  std::vector<Request*> available_queue;
  // previous XferDes in the chain, NULL is this XferDes is
  // the first one.
  XferDes* pre_XferDes;
  // next XferDes in the chain, NULL if this XferDes is
  // the last one.
  XferDes* next_XferDes;
public:
  // XferKind of the Xfer Descriptor
  XferKind kind;
  // uid of the channel this XferDes describtes
  uint64_t channel_id;
public:

  virtual long get_requests(Request** requests, long nr) = 0;

  bool is_done() {
    return bytes_write == bytes_total;
  }

  void update_pre_XferDes(XferDes* _pre_XferDes) {
    pre_XferDes = _pre_XferDes;
  }

  void update_next_XferDes(XferDes* _next_XferDes) {
    next_XferDes = _next_XferDes;
  }
  
  void update_pre_bytes_write(size_t new_val) {
    pre_bytes_write = new_val;
  }

  void update_next_bytes_read(size_t new_val) {
    next_bytes_read = new_val;
  }

  void notify_request_read_done(uint64_t uid) {
    // notify previous XferDes that there are more bytes read
  }

  void notify_request_write_done(uint64_t uid) {
    std::map<uint64_t, Request*>::iterator it = flying_reqs.find(uid);
    it->second->is_done = true;
    // clear countinous finished requests and updates bytes
    while (bytes_write < bytes_submit) {
      it = flying_reqs.find(bytes_write / req_nbytes);
      if (!it->second->is_done)
        break;
      printf("it->first = %lu\n", it->first);
      flying_reqs.erase(it);
      available_queue.push_back(it->second);
      bytes_read += req_nbytes;
      bytes_write += req_nbytes;
    }
    // notify next XferDes that there are more bytes written
    if (next_XferDes != NULL) {
      next_XferDes->update_pre_bytes_write(bytes_write);
    }
    if (pre_XferDes != NULL) {
      pre_XferDes->update_next_bytes_read(bytes_read);
    }
  }
};

class DiskWriteXferDes : public XferDes{
public:
  DiskWriteXferDes(uint64_t _channel_id, uint64_t _req_nbytes,
                   bool has_pre_XferDes, bool has_next_XferDes,
                   uint64_t _src_bufsize, uint64_t _dst_bufsize,
                   uint64_t _bytes_total, void* _mem_addr,
                   long max_nr);

  ~DiskWriteXferDes()
  {
    free(requests);
  }

  long get_requests(Request** requests, long nr);

private:
  DiskWriteRequest* requests;
  long min(long a, long b)
  {
    return (a<b) ? a : b;
  }

  long num_available_reqs()
  {
    assert(bytes_submit <= pre_bytes_write);
    long ret = min((pre_bytes_write - bytes_submit) / req_nbytes, (dst_bufsize + next_bytes_read - bytes_submit) / req_nbytes);
    //ret = min(ret, (bytes_total - bytes_submit) / req_nbytes);
    ret = min(ret, available_queue.size());
    return ret;
  }
};

class DiskReadXferDes : public XferDes{
private:
  DiskReadRequest* requests;
public:
  DiskReadXferDes(uint64_t _channel_id, uint64_t _req_nbytes,
                  bool has_pre_XferDes, bool has_next_XferDes,
                  uint64_t _src_bufsize, uint64_t _dst_bufsize,
                  uint64_t _bytes_total, void* _mem_addr,
                  long max_nr);
  
  ~DiskReadXferDes()
  {
    free(requests);
  }

  long get_requests(Request** requests, long nr);

private:
  long min(long a, long b)
  {
    return (a<b) ? a : b;
  }

  long num_available_reqs()
  {
    assert(bytes_submit <= pre_bytes_write);
    long ret = min((pre_bytes_write - bytes_submit) / req_nbytes, (dst_bufsize + next_bytes_read - bytes_submit) / req_nbytes);
    //ret = min(ret, (bytes_total - bytes_submit) / req_nbytes);
    ret = min(ret, available_queue.size());
    return ret;
  }
};

class Channel {
public:
  // the kind of XferDes this channel can accept
  XferDes::XferKind kind;
  // an unique id for identifying the channel
  uint64_t uid;
  /*
   * Submit nr asynchronous requests into the channel instance.
   * This is supposed to be a non-blocking function call, and
   * should immediately return the number of requests that are
   * successfully submitted.
   */
  virtual long submit(Request** requests, long nr) = 0;
  
  /*
   * 
   */
  virtual void poll() = 0;

  /*
   * Return the number of slots that are available for
   * submitting requests
   */
  virtual long available() = 0;
};

class DiskReadChannel : public Channel {
public:
  DiskReadChannel(uint64_t id, int fd, long max_nr);
  ~DiskReadChannel();
  long submit(Request** requests, long nr);
  void poll();
  long available();
private:
  aio_context_t ctx;
  long capacity;
  std::vector<struct iocb*> available_cb;
  struct iocb* cb;
  struct iocb** cbs;
  struct io_event* events;
};

class DiskWriteChannel : public Channel {
public:
  DiskWriteChannel(uint64_t id, int fd, long max_nr);
  ~DiskWriteChannel();
  long submit(Request** requests, long nr);
  void poll();
  long available();
private:
  aio_context_t ctx;
  long capacity;
  std::vector<struct iocb*> available_cb;
  struct iocb* cb;
  struct iocb** cbs;
  struct io_event* events;
};

class DMAThread {
public:
  DMAThread(long _max_nr) {
    max_nr = _max_nr;
    requests = (Request**) calloc(max_nr, sizeof(Request*));
    pthread_mutex_init(&channel_lock, NULL);
    pthread_mutex_init(&xferDes_lock, NULL);
    channel_queue.clear();
    xferDes_queue.clear();
  }
  ~DMAThread() {
    free(requests);
    pthread_mutex_destroy(&channel_lock);
    pthread_mutex_destroy(&xferDes_lock);
  }
  // Add a channel into the DMAThread instance.
  // Invocation of this function indicates the DMAThread
  // instance keeps charge of this channel
  void add_channel(Channel* channel) {
    pthread_mutex_lock(&channel_lock);
    channel_queue.push_back(channel);
    pthread_mutex_unlock(&channel_lock);
  }
  // Add a XferDes into the DMAThread instance.
  // The DMAThread will start to polling requests from
  // this XferDes and perform IO
  void add_xferDes(XferDes* xferDes) {
    pthread_mutex_lock(&xferDes_lock);
    xferDes_queue.push_back(xferDes);
    pthread_mutex_unlock(&xferDes_lock);
  }
  // Thread start function that takes an input of DMAThread
  // instance, and start to execute the requests from XferDes
  // by using its channels.
  static void* start(void* arg);

private:
  // maximum allowed num of requests for a single 
  long max_nr;
  Request** requests;
  pthread_mutex_t channel_lock, xferDes_lock;
  std::vector<Channel*> channel_queue;
  std::vector<XferDes*> xferDes_queue;
};

