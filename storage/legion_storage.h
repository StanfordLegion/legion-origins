#ifndef __LEGION_STORAGE_H__
#define __LEGION_STORAGE_H__

#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "legion.h"
using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::LowLevel;
using namespace LegionRuntime::Accessor;

namespace LegionStorage {

  //==========================================================================
  //                       Class Definitions
  //==========================================================================

  /**
   * \class LegionStorageManager
   * A class that helps user save a index space tree into disk, given root of
   * tree, or load a index space tree from disk, given a file name.
   * LegionStorageManager(LSM) also provide saving/load for region data. LSM
   * provides a sequential way and a simultaneous way for user to save/load
   * region data. If user wants to save/load region data in simultaneous way,
   * user MUST call 'LegionStorageManager::initialize()' in main function,
   * otherwise it will cause crash.
   */
  class LegionStorageManager {
  public:
    ~LegionStorageManager(void);
    typedef std::set<LogicalRegion> RegionSet;
    enum RegionSetType {
      REGIONSET_ALL,
      REGIONSET_LEAVES,
      REGIONSET_INTERNALS
    };

    enum RegionTaskIDs {
      SAVE_REGION_TASK_ID = 666,
      LOAD_REGION_TASK_ID = 667
    };

    /**
     * Initializing LegionStorageManager. This will register 'task_save_lr_into_file'
     * and 'task_load_lr_from_file' in Legion runtime. User MUST call this function if
     * wants to save/load region data in parallel.
     */
    static void initialize(void);

    /**
     * Saving an index space tree into disk. This function supports saving for
     * both unstructed and structed index spaces.
     * @param rt the high-level runtime pointer for the specified processor
     * @param ctx the enclosing task context
     * @param is the root of the index space tree being saved
     */
    static void save_index_space_tree_into_file(char* filename, HighLevelRuntime* rt, Context ctx, IndexSpace is);

    /**
     * Loading an index space tree from disk. This function supports loading for
     * both unstructed and structed index spaces.
     * @param rt the high-level runtime pointer for the specified processor
     * @param ctx the enclosing task context
     * @return the root of the loaded index space tree
     */
    static IndexSpace load_index_space_tree_from_file(char* filename, HighLevelRuntime* rt, Context ctx);

    /**
     * Saving a set of logical regions into disk. Given a set of logical regions
     * and a set of fields, this function saves all corresponding region data
     * (i.e., region data that both stays in logical region set and field set)
     * into disk. LSM provides a sequential way and a simultaneous way for user
     * to save region data. In sequential way, LSM saves every field in every
     * logical region one by one. In simultaneous way, LSM creates an individual
     * task for every field in every logical region, and Legion scheduler will
     * schedule some tasks to run in parallel if there is no conflict between them.
     * @param region_set the set of logical regions to save
     * @param field_set the mapping from FieldID to field name
     * @param prefix the prefix for the files
     * @param runtime the high-level runtime pointer for the specified processor
     * @param ctx the enclosing task context
     * @param mapper_id id of the mapper to associate with the task
     * @param in_parallel whether user wants to save region data in parallel
     */
    static void save_logical_region_data_into_file(std::set<LogicalRegion> region_set, std::map<FieldID, char*> field_set, char* prefix, HighLevelRuntime* runtime, Context ctx, MapperID mapper_id, bool in_parallel);

    /**
     * Loading a set of logical regions from disk. Given a set of logical regions
     * and a set of fields, this function loads all corresponding region data
     * (i.e., region data that both stays in logical region set and field set)
     * from disk. LSM provides a sequential way and a simultaneous way for user
     * to load region data. In sequential way, LSM loads every field in every
     * logical region one by one. In simultaneous way, LSM creates an individual
     * task for every field in every logical region, and Legion scheduler will
     * schedule some tasks to run in parallel if there is no conflict between them.
     * @param region_set the set of logical regions to load
     * @param field_set the mapping from FieldID to field name
     * @param prefix the prefix for the files
     * @param runtime the high-level runtime pointer for the specified processor
     * @param ctx the enclosing task context
     * @param mapper_id id of the mapper to associate with the task
     * @param in_parallel whether user wants to load region data in parallel
     */
    static void load_logical_region_data_from_file(std::set<LogicalRegion> region_set, std::map<FieldID, char*> field_set, char* prefix, HighLevelRuntime* runtime, Context ctx, MapperID mapper_id, bool in_parallel);

    /**
     * Helper function that helps user get particular set of logical regions
     * @param root the root of the region tree
     * @param runtime the high-level runtime pointer for the specified processor
     * @param ctx the enclosing task context
     * @param type what kind of region set user wants to get
     */
    static std::set<LogicalRegion> helper_get_region_set(LogicalRegion root, HighLevelRuntime* runtime, Context ctx, RegionSetType type);

    static void fill_in_region_file_name(char* filename, char* prefix, LogicalRegion lr, char* field_name, HighLevelRuntime* runtime, Context ctx);
    static void task_save_lr_into_file(const Task *task, const std::vector<PhysicalRegion> &regions, Context ctx, HighLevelRuntime *runtime);
    static void task_load_lr_from_file(const Task *task, const std::vector<PhysicalRegion> &regions, Context ctx, HighLevelRuntime *runtime);
    static int get_num_of_elemts(HighLevelRuntime* runtime, Context ctx, IndexSpace is);
  };

  /**
   * \class IndexSpaceTreeSerializer
   * A class for accessing field file. Given a filename and whether
   * you are going to write into it, a RegionFileAccessor could be
   * created. By using it, users could save a Field data into a
   * file, or load a Field data from file.
   */
  class IndexSpaceTreeSerializer {
  public:
    typedef IndexSpace::id_t id_type;
    IndexSpaceTreeSerializer(HighLevelRuntime* _runtime, Context _ctx);
    IndexSpaceTreeSerializer(IndexSpace _root, HighLevelRuntime* _runtime, Context _ctx);
  public:
    size_t legion_buffer_size(void);
    size_t legion_serialize(void *buffer);
    size_t legion_deserialize(void *buffer);
  public:
    IndexSpace& get_root(void) { return root; }
    std::map<id_type, IndexSpace>& get_mapper(void) {return mapper;}
  private:
    size_t get_size_of_IS_node(IndexSpace is);
    int get_num_of_IP_nodes(IndexSpace root);
    template<typename TN> TN fetch_data();
    template<typename TN> void save_data(TN source);
  private:
    friend class Elementmask;
    static const size_t IP_size = sizeof(id_type) + sizeof(Color) + sizeof(bool) + sizeof(Domain);
    IndexSpace root;
    HighLevelRuntime* runtime;
    Context ctx;
    char* cursor;
    std::map<id_type, IndexSpace> mapper; // mapper from id to IndexSpace
  };

  /**
   * \class DomainFileManager
   * A class for accessing domain file. Given a filename and whether
   * you are going to write into it, a DomainFileManager could be
   * created. By using it, users could save a Domain object (e.g.,
   * Domain, Coloring, DomainColoring) into a file, or load a Domain
   * object from file.
   * @TN TN could be one of Domain, Coloring, and DomainColoring
   */
  template<typename TN>
  class DomainFileManager {
  public:
    DomainFileManager(char* filename, bool isw);
    ~DomainFileManager(void);

    TN load_from_file(void);
    void save_into_file(TN id);
  private:
    int fd; // file descriptor
  };


  //==========================================================================
  //                       Class member function implementations
  //==========================================================================
  LegionStorageManager::~LegionStorageManager(void) {
  }

  void LegionStorageManager::initialize(void) {
    HighLevelRuntime::register_legion_task<task_save_lr_into_file>(SAVE_REGION_TASK_ID,
      Processor::LOC_PROC, true/*single*/, true/*index*/);
    HighLevelRuntime::register_legion_task<task_load_lr_from_file>(LOAD_REGION_TASK_ID,
      Processor::LOC_PROC, true/*single*/, true/*index*/);
  }

  std::set<LogicalRegion> LegionStorageManager::helper_get_region_set(LogicalRegion root, HighLevelRuntime* runtime, Context ctx, RegionSetType type) {
    std::set<LogicalRegion> region_set;
    // use Breadth First Search Traversal to explore the whole IndexSpace tree
    std::queue<LogicalRegion> queue;
    queue.push(root);
    while (!queue.empty()) {
      LogicalRegion cur_lr = queue.front();
      IndexSpace cur_is = cur_lr.get_index_space();
      queue.pop();
      std::set<Color> cur_colors;
      runtime->get_index_space_partition_colors(ctx, cur_is, cur_colors);
      if ((type==REGIONSET_ALL)
      || (type==REGIONSET_LEAVES && cur_colors.empty())
      || (type==REGIONSET_INTERNALS && !cur_colors.empty())) {
        region_set.insert(cur_lr);
      }

      for (std::set<Color>::const_iterator it = cur_colors.begin(); it != cur_colors.end(); it++) {
        IndexPartition child_ip = runtime->get_index_partition(ctx, cur_is, *it);
        LogicalPartition child_lp = runtime->get_logical_partition_by_color(ctx, cur_lr, *it);
        Domain child_colors = runtime->get_index_partition_color_space(ctx, child_ip);
        assert(*it == runtime->get_index_partition_color(ctx, child_ip));
        for (Domain::DomainPointIterator iter(child_colors); iter; iter++) {
          LogicalRegion grand_lr = runtime->get_logical_subregion_by_color(ctx, child_lp, iter.p.point_data[0]);
          queue.push(grand_lr);
        }
      }
    }

    return region_set;
  }

  void LegionStorageManager::fill_in_region_file_name(char* filename, char* prefix, LogicalRegion lr, char* field_name, HighLevelRuntime* runtime, Context ctx) {
    char num[64];
    sprintf(filename, "%s_%s_%d", prefix, field_name, runtime->get_logical_region_color(ctx, lr));
    while(runtime->has_parent_logical_partition(ctx, lr)) {
      LogicalPartition lp = runtime->get_parent_logical_partition(ctx, lr);
      lr = runtime->get_parent_logical_region(ctx, lp);
      sprintf(num, "_%d_%d", runtime->get_logical_partition_color(ctx, lp), runtime->get_logical_region_color(ctx, lr));
      strcat(filename, num);
    }
  }

  void LegionStorageManager::save_index_space_tree_into_file(char* filename, HighLevelRuntime* runtime, Context ctx, IndexSpace is) {
    int fd = open(filename, O_CREAT | O_RDWR | O_TRUNC, 00777);
    IndexSpaceTreeSerializer ser(is, runtime, ctx);
    size_t length = ser.legion_buffer_size();
    write(fd, "", length + sizeof(size_t));
    char* ptr_len = (char*) mmap(NULL, length + sizeof(size_t), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    *((size_t*) ptr_len) = length;
    char* addr = ptr_len + sizeof(size_t);
    size_t save_len = ser.legion_serialize(addr);
    assert(length == save_len);
    munmap(ptr_len, length + sizeof(size_t));
    close(fd);
  }

  IndexSpace LegionStorageManager::load_index_space_tree_from_file(char* filename, HighLevelRuntime* runtime, Context ctx) {
    int fd = open(filename, O_RDONLY, 00777);
    size_t* ptr_len = (size_t*) mmap(NULL, sizeof(size_t), PROT_READ, MAP_SHARED, fd, 0);
    size_t length = *ptr_len;
    munmap(ptr_len, sizeof(size_t));

    char* addr = (char*) mmap(NULL, length + sizeof(size_t), PROT_READ, MAP_SHARED, fd, 0) + sizeof(size_t);
    IndexSpaceTreeSerializer ser(runtime, ctx);
    size_t load_len = ser.legion_deserialize(addr);
    assert(length == load_len);
    munmap(addr - sizeof(size_t), length + sizeof(size_t));
    close(fd);
    return ser.get_root();
  }

  void LegionStorageManager::save_logical_region_data_into_file(std::set<LogicalRegion> region_set, std::map<FieldID, char*> field_set, char* prefix, HighLevelRuntime* runtime, Context ctx, MapperID mapper_id, bool in_parallel) {
    if (!in_parallel) {
      for(std::map<FieldID, char*>::iterator field_iter = field_set.begin(); field_iter != field_set.end(); field_iter++) {
        FieldID fid = field_iter->first;
        char* field_name = field_iter->second;

        for(std::set<LogicalRegion>::iterator region_iter = region_set.begin(); region_iter != region_set.end(); region_iter++) {
          LogicalRegion lr = *region_iter;
          LogicalRegion parent = lr;
          while(runtime->has_parent_logical_partition(ctx, parent)) {
            LogicalPartition lp = runtime->get_parent_logical_partition(ctx, parent);
            parent = runtime->get_parent_logical_region(ctx, lp);
          }

          RegionRequirement req(lr, READ_ONLY, EXCLUSIVE, parent);
          req.add_field(fid);
          InlineLauncher reader_launcher(req);
          PhysicalRegion pr = runtime->map_region(ctx, reader_launcher);
          pr.wait_until_valid();
          RegionAccessor<AccessorType::Generic> acc = pr.get_field_accessor(fid);
          IndexSpace is = lr.get_index_space();
          Domain domain = runtime->get_index_space_domain(ctx, is);
          size_t field_size = runtime->get_field_size(ctx, lr.get_field_space(), fid);

          char fn[256];
          fill_in_region_file_name(fn, prefix, lr, field_name, runtime, ctx);
          int filedes = open(fn, O_CREAT | O_RDWR | O_TRUNC, 00777);
          size_t length = field_size * get_num_of_elemts(runtime, ctx, is);
          write(filedes, "", length);
          char* addr = (char*) mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, filedes, 0);

          switch (domain.dim) {
          case 0:
            {
              IndexIterator itr(is);
              char* cursor = addr;
              int num_of_elemts = get_num_of_elemts(runtime, ctx, is);
              for(int i = 0; i < num_of_elemts; i++) {
                assert(itr.has_next());
                ptr_t ptr = itr.next();
                acc.read_untyped(ptr, cursor, field_size);
                cursor += field_size;
              }
              assert(!itr.has_next());
            }
            break;
          case 1:
            {
              Rect<1> rect = domain.get_rect<1>();
              char* cursor = addr;
              for (GenericPointInRectIterator<1> pir(rect); pir; pir++) {
                acc.read_untyped(DomainPoint::from_point<1>(pir.p), cursor, field_size);
                cursor += field_size;
              }
            }
            break;
          case 2:
            {
              Rect<2> rect = domain.get_rect<2>();
              char* cursor = addr;
              for (GenericPointInRectIterator<2> pir(rect); pir; pir++) {
                acc.read_untyped(DomainPoint::from_point<2>(pir.p), cursor, field_size);
                cursor += field_size;
              }
            }
            break;
          case 3:
            {
              Rect<3> rect = domain.get_rect<3>();
              char* cursor = addr;
              for (GenericPointInRectIterator<3> pir(rect); pir; pir++) {
                acc.read_untyped(DomainPoint::from_point<3>(pir.p), cursor, field_size);
                cursor += field_size;
              }
            }
            break;
          default:
            assert(false);
            break;
          }
          runtime->unmap_region(ctx, pr);
          munmap(addr, length);
          close(filedes);
        }
      }
    }
    else {
      for (std::map<FieldID, char*>::iterator field_iter = field_set.begin(); field_iter != field_set.end(); field_iter++) {
        FieldID fid = field_iter->first;
        char* field_name = field_iter->second;
        for (std::set<LogicalRegion>::iterator region_iter = region_set.begin(); region_iter != region_set.end(); region_iter++) {
          LogicalRegion lr = *region_iter;
          LogicalRegion parent = lr;
          while(runtime->has_parent_logical_partition(ctx, parent)) {
            LogicalPartition lp = runtime->get_parent_logical_partition(ctx, parent);
            parent = runtime->get_parent_logical_region(ctx, lp);
          }

          char fn[256];
          fill_in_region_file_name(fn, prefix, lr, field_name, runtime, ctx);
          TaskLauncher save_launcher(SAVE_REGION_TASK_ID, TaskArgument(fn, strlen(fn)));
          RegionRequirement req(lr, READ_ONLY, EXCLUSIVE, parent);
          req.add_field(fid);
          save_launcher.add_region_requirement(req);
          runtime->execute_task(ctx, save_launcher);
        }
      }
    }
  }

  void LegionStorageManager::load_logical_region_data_from_file(std::set<LogicalRegion> region_set, std::map<FieldID, char*> field_set, char* prefix, HighLevelRuntime* runtime, Context ctx, MapperID mapper_id, bool in_parallel) {
    if (!in_parallel) {
      for (std::map<FieldID, char*>::iterator field_iter = field_set.begin(); field_iter != field_set.end(); field_iter++) {
        FieldID fid = field_iter->first;
        char* field_name = field_iter->second;
        for(std::set<LogicalRegion>::iterator it = region_set.begin(); it != region_set.end(); it++) {
          LogicalRegion lr = *it;
          LogicalRegion parent = lr;
          while(runtime->has_parent_logical_partition(ctx, parent)) {
            LogicalPartition lp = runtime->get_parent_logical_partition(ctx, parent);
            parent = runtime->get_parent_logical_region(ctx, lp);
          }

          RegionRequirement req(lr, WRITE_ONLY, EXCLUSIVE, parent);
          req.add_field(fid);
          InlineLauncher writer_launcher(req);
          PhysicalRegion pr = runtime->map_region(ctx, writer_launcher);
          pr.wait_until_valid();
          RegionAccessor<AccessorType::Generic> acc = pr.get_field_accessor(fid);
          IndexSpace is = lr.get_index_space();
          Domain domain = runtime->get_index_space_domain(ctx, is);
          size_t field_size = runtime->get_field_size(ctx, lr.get_field_space(), fid);;

          char fn[256];
          fill_in_region_file_name(fn, prefix, lr, field_name, runtime, ctx);
          int filedes = open(fn, O_RDONLY, 00777);
          assert(filedes != -1);
          size_t length = field_size * get_num_of_elemts(runtime, ctx, is);
          char* addr = (char*)mmap(NULL, length, PROT_READ, MAP_SHARED, filedes, 0);

          switch (domain.dim) {
          case 0:
            {
              IndexIterator itr(is);
              char* cursor = addr;
              int num_of_elemts = get_num_of_elemts(runtime, ctx, is);
              for(int i = 0; i < num_of_elemts; i++) {
                assert(itr.has_next());
                ptr_t ptr = itr.next();
                acc.write_untyped(ptr, cursor, field_size);
                cursor += field_size;
              }
              assert(!itr.has_next());
            }
            break;
          case 1:
            {
              Rect<1> rect = domain.get_rect<1>();
              char* cursor = addr;
              for (GenericPointInRectIterator<1> pir(rect); pir; pir++) {
                acc.write_untyped(DomainPoint::from_point<1>(pir.p), cursor, field_size);
                cursor += field_size;
              }
            }
            break;
          case 2:
            {
              Rect<2> rect = domain.get_rect<2>();
              char* cursor = addr;
              for (GenericPointInRectIterator<2> pir(rect); pir; pir++) {
                acc.write_untyped(DomainPoint::from_point<2>(pir.p), cursor, field_size);
                cursor += field_size;
              }
            }
            break;
          case 3:
            {
              Rect<3> rect = domain.get_rect<3>();
              char* cursor = addr;
              for (GenericPointInRectIterator<3> pir(rect); pir; pir++) {
                acc.write_untyped(DomainPoint::from_point<3>(pir.p), cursor, field_size);
                cursor += field_size;
              }
            }
            break;
          default:
            assert(false);
            break;
          }
          runtime->unmap_region(ctx, pr);
          munmap(addr, length);
          close(filedes);
        }
      }
    }
    else {
      for (std::map<FieldID, char*>::iterator field_iter = field_set.begin(); field_iter != field_set.end(); field_iter++) {
        FieldID fid = field_iter->first;
        char* field_name = field_iter->second;
        for (std::set<LogicalRegion>::iterator region_iter = region_set.begin(); region_iter != region_set.end(); region_iter++) {
          LogicalRegion lr = *region_iter;
          LogicalRegion parent = lr;
          while(runtime->has_parent_logical_partition(ctx, parent)) {
            LogicalPartition lp = runtime->get_parent_logical_partition(ctx, parent);
            parent = runtime->get_parent_logical_region(ctx, lp);
          }

          char fn[256];
          fill_in_region_file_name(fn, prefix, lr, field_name, runtime, ctx);
          TaskLauncher load_launcher(LOAD_REGION_TASK_ID, TaskArgument(fn, strlen(fn)));
          RegionRequirement req(lr, WRITE_ONLY, EXCLUSIVE, parent);
          req.add_field(fid);
          load_launcher.add_region_requirement(req);
          runtime->execute_task(ctx, load_launcher);
        }
      }
    }
  }

  void LegionStorageManager::task_save_lr_into_file(const Task *task, const std::vector<PhysicalRegion> &regions, Context ctx, HighLevelRuntime *runtime) {
    FieldID fid = *(task->regions[0].privilege_fields.begin());
    RegionAccessor<AccessorType::Generic> acc = regions[0].get_field_accessor(fid);
    IndexSpace is = regions[0].get_logical_region().get_index_space();
    Domain domain = runtime->get_index_space_domain(ctx, is);
    size_t field_size = runtime->get_field_size(ctx, regions[0].get_logical_region().get_field_space(), fid);

    char fn[256];
    strncpy(fn, (char*)task->args, task->arglen);
    fn[task->arglen] = '\0';

    int filedes = open(fn, O_CREAT | O_RDWR | O_TRUNC, 00777);
    assert(filedes != -1);
    size_t length = field_size * get_num_of_elemts(runtime, ctx, is);
    write(filedes, "", length);
    char* addr = (char*) mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, filedes, 0);

    switch (domain.dim) {
    case 0:
      {
        IndexIterator itr(is);
        char* cursor = addr;
        int num_of_elemts = get_num_of_elemts(runtime, ctx, is);
        for(int i = 0; i < num_of_elemts; i++) {
          assert(itr.has_next());
          ptr_t ptr = itr.next();
          acc.read_untyped(ptr, cursor, field_size);
          cursor += field_size;;
        }
        assert(!itr.has_next());
      }
      break;
    case 1:
      {
        Rect<1> rect = domain.get_rect<1>();
        char* cursor = addr;
        for (GenericPointInRectIterator<1> pir(rect); pir; pir++) {
          acc.read_untyped(DomainPoint::from_point<1>(pir.p), cursor, field_size);
          cursor += field_size;
        }
      }
      break;
    case 2:
      {
        Rect<2> rect = domain.get_rect<2>();
        char* cursor = addr;
        for (GenericPointInRectIterator<2> pir(rect); pir; pir++) {
          acc.read_untyped(DomainPoint::from_point<2>(pir.p), cursor, field_size);
          cursor += field_size;
        }
      }
      break;
    case 3:
      {
        Rect<3> rect = domain.get_rect<3>();
        char* cursor = addr;
        for (GenericPointInRectIterator<3> pir(rect); pir; pir++) {
          acc.read_untyped(DomainPoint::from_point<3>(pir.p), cursor, field_size);
          cursor += field_size;
        }
      }
      break;
    default:
      assert(false);
      break;
    }
    munmap(addr, length);
    close(filedes);
  }

  void LegionStorageManager::task_load_lr_from_file(const Task *task, const std::vector<PhysicalRegion> &regions, Context ctx, HighLevelRuntime *runtime) {
    FieldID fid = *(task->regions[0].privilege_fields.begin());
    RegionAccessor<AccessorType::Generic> acc = regions[0].get_field_accessor(fid);
    IndexSpace is = regions[0].get_logical_region().get_index_space();
    Domain domain = runtime->get_index_space_domain(ctx, is);
    size_t field_size = runtime->get_field_size(ctx, regions[0].get_logical_region().get_field_space(), fid);

    char fn[256];
    strncpy(fn, (char*)task->args, task->arglen);
    fn[task->arglen] = '\0';

    int filedes = open(fn, O_RDONLY, 00777);
    assert(filedes != -1);
    size_t length = field_size * get_num_of_elemts(runtime, ctx, is);
    char* addr = (char*)mmap(NULL, length, PROT_READ, MAP_SHARED, filedes, 0);

    switch (domain.dim) {
    case 0:
      {
        IndexIterator itr(is);
        char* cursor = addr;
        int num_of_elemts = get_num_of_elemts(runtime, ctx, is);
        for(int i = 0; i < num_of_elemts; i++) {
          assert(itr.has_next());
          ptr_t ptr = itr.next();
          acc.write_untyped(ptr, cursor, field_size);
          cursor += field_size;
        }
        assert(!itr.has_next());
      }
      break;
    case 1:
      {
        Rect<1> rect = domain.get_rect<1>();
        char* cursor = addr;
        for (GenericPointInRectIterator<1> pir(rect); pir; pir++) {
          acc.write_untyped(DomainPoint::from_point<1>(pir.p), cursor, field_size);
          cursor += field_size;
        }
      }
      break;
    case 2:
      {
        Rect<2> rect = domain.get_rect<2>();
        char* cursor = addr;
        for (GenericPointInRectIterator<2> pir(rect); pir; pir++) {
          acc.write_untyped(DomainPoint::from_point<2>(pir.p), cursor, field_size);
          cursor += field_size;
        }
      }
      break;
    case 3:
      {
        Rect<3> rect = domain.get_rect<3>();
        char* cursor = addr;
        for (GenericPointInRectIterator<3> pir(rect); pir; pir++) {
          acc.write_untyped(DomainPoint::from_point<3>(pir.p), cursor, field_size);
          cursor += field_size;
        }
      }
      break;
    default:
      assert(false);
      break;
    }
    munmap(addr, length);
    close(filedes);
  }

  int LegionStorageManager::get_num_of_elemts(HighLevelRuntime* runtime, Context ctx, IndexSpace is) {
    Domain dm = runtime->get_index_space_domain(ctx, is);
    if (dm.dim == 0) {
      ElementMask mask = is.get_valid_mask();
      ElementMask::Enumerator* elem = mask.enumerate_enabled();
      int total = 0;
      int pos, len;
      while (elem->get_next(pos, len)) {
        total += len;
      }
      return total;
    }
    else {
      return dm.get_volume();
    }
  }

  IndexSpaceTreeSerializer::IndexSpaceTreeSerializer(HighLevelRuntime* _runtime, Context _ctx)
  : runtime(_runtime), ctx(_ctx), cursor(0) {}

  IndexSpaceTreeSerializer::IndexSpaceTreeSerializer(IndexSpace _root, HighLevelRuntime* _runtime, Context _ctx)
  : root(_root), runtime(_runtime), ctx(_ctx), cursor(0) {}

  size_t IndexSpaceTreeSerializer::legion_buffer_size(void){
    size_t result = get_size_of_IS_node(root) + sizeof(int);

    // use Breadth First Search Traversal to explore the whole IndexSpace tree
    std::queue<IndexSpace> queue;
    queue.push(root);
    while (!queue.empty()) {
      IndexSpace cur = queue.front();
      queue.pop();
      std::set<Color> cur_colors;
      runtime->get_index_space_partition_colors(ctx, cur, cur_colors);
      for (std::set<Color>::const_iterator it = cur_colors.begin(); it != cur_colors.end(); it++) {
        IndexPartition child = runtime->get_index_partition(ctx, cur, *it);
        Domain child_colors = runtime->get_index_partition_color_space(ctx, child);
        result +=  IP_size;
        for (Domain::DomainPointIterator iter(child_colors); iter; iter++) {
          IndexSpace grand_child = runtime->get_index_subspace(ctx, child, iter.p.point_data[0]);
          result += get_size_of_IS_node(grand_child);
          queue.push(grand_child);
        }
      }
    }
    return result;
  }

  size_t IndexSpaceTreeSerializer::get_size_of_IS_node(IndexSpace is) {
    Domain dm = runtime->get_index_space_domain(ctx, is);
    if (dm.dim == 0) {
      ElementMask mask = is.get_valid_mask();
      return (sizeof(id_type) + sizeof(Color) + sizeof(bool) + sizeof(int) * 4 + mask.raw_size());
    }
    else {
      return (sizeof(id_type) + sizeof(Color) + sizeof(bool) + sizeof(Domain));
    }
  }

  int IndexSpaceTreeSerializer::get_num_of_IP_nodes(IndexSpace root) {
    // use Breadth First Search Traversal to explore the whole IndexSpace tree
    int result = 0;
    std::queue<IndexSpace> queue;
    queue.push(root);
    while (!queue.empty()) {
      IndexSpace cur = queue.front();
      queue.pop();
      std::set<Color> cur_colors;
      runtime->get_index_space_partition_colors(ctx, cur, cur_colors);
      for (std::set<Color>::const_iterator it = cur_colors.begin(); it != cur_colors.end(); it++) {
        result ++;
        IndexPartition child = runtime->get_index_partition(ctx, cur, *it);
        Domain child_colors = runtime->get_index_partition_color_space(ctx, child);
        for (Domain::DomainPointIterator iter(child_colors); iter; iter++) {
          IndexSpace grand_child = runtime->get_index_subspace(ctx, child, iter.p.point_data[0]);
          queue.push(grand_child);
        }
      }
    }
    return result;
  }

  template<typename TN>
  TN IndexSpaceTreeSerializer::fetch_data () {
    TN result = *((TN*)cursor);
    cursor += sizeof(TN);
    return result;
  }

  template<>
  ElementMask IndexSpaceTreeSerializer::fetch_data() {
    int first_elmt = fetch_data<int>();
    int num_elmts = fetch_data<int>();
    ElementMask result(num_elmts, first_elmt);
    result.first_enabled_elmt = fetch_data<int>();
    result.last_enabled_elmt = fetch_data<int>();
    memcpy(result.raw_data, cursor, result.raw_size());
    cursor += result.raw_size();
    return result;
  }

  template<typename TN>
  void IndexSpaceTreeSerializer::save_data (TN source) {
    *((TN*)cursor) = source;
    cursor += sizeof(TN);
  }

  /* ElementMask protocol:
   * (int) first_element
   * (int) num_elements
   * (int) first_enabled_elmt
   * (int) last_enabled_elmt
   * raw_data
   *
   * Assumption: size of raw_data = ElementMask::raw_size() = ElementMaskImpl::bytes_needed(num_elements);
   */
  template<>
  void IndexSpaceTreeSerializer::save_data(ElementMask mask) {
    save_data<int>(mask.first_element);
    save_data<int>(mask.num_elements);
    save_data<int>(mask.first_enabled_elmt);
    save_data<int>(mask.last_enabled_elmt);
    memcpy(cursor, mask.raw_data, mask.raw_size());
    cursor += mask.raw_size();
  }

  size_t IndexSpaceTreeSerializer::legion_serialize(void* buffer) {
    cursor = (char*)buffer;
    save_data<id_type>(root.id);
    save_data<Color>(runtime->get_index_space_color(ctx, root));
    Domain root_domain = runtime->get_index_space_domain(ctx, root);
    mapper[root.id] = root;
    if (root_domain.dim != 0) {
      save_data<bool>(true);
      save_data<Domain>(root_domain);
    }
    else {
      save_data<bool>(false);
      save_data<ElementMask>(root.get_valid_mask());
    }
    save_data<int>(get_num_of_IP_nodes(root));

    // use Breadth First Search Traversal to explore the whole IndexSpace tree
    std::queue<IndexSpace> queue;
    queue.push(root);
    while (!queue.empty()) {
      IndexSpace cur = queue.front();
      queue.pop();
      Domain cur_domain = runtime->get_index_space_domain(ctx, cur);
      std::set<Color> cur_colors;
      runtime->get_index_space_partition_colors(ctx, cur, cur_colors);
      for (std::set<Color>::const_iterator it = cur_colors.begin(); it != cur_colors.end(); it++) {
        IndexPartition child = runtime->get_index_partition(ctx, cur, *it);
        Domain child_colors = runtime->get_index_partition_color_space(ctx, child);
        save_data<id_type>(cur.id);
        save_data<Color>(*it);
        assert(*it == runtime->get_index_partition_color(ctx, child));
        save_data<bool>(runtime->is_index_partition_disjoint(ctx, child));
        save_data<Domain>(child_colors);
        for (Domain::DomainPointIterator iter(child_colors); iter; iter++) {
          IndexSpace grand_child = runtime->get_index_subspace(ctx, child, iter.p.point_data[0]);
          save_data<id_type>(grand_child.id);
          save_data<Color>(iter.p.point_data[0]);
          assert(iter.p.point_data[0] == runtime->get_index_space_color(ctx, grand_child));
          Domain grand_child_domain = runtime->get_index_space_domain(ctx, grand_child);
          mapper[grand_child.id] = grand_child;
          if (grand_child_domain.dim != 0) {
            save_data<bool>(true);
            save_data<Domain>(grand_child_domain);
          } else {
            save_data<bool>(false);
            save_data<ElementMask>(grand_child.get_valid_mask());
          }
          queue.push(grand_child);
        }
      }
    }
    return (size_t(cursor) - size_t(buffer));
  }

  size_t IndexSpaceTreeSerializer::legion_deserialize(void* buffer) {
    cursor = (char*) buffer;
    id_type root_id = fetch_data<id_type>();
    Color root_color = fetch_data<Color>();
    bool root_is_domain = fetch_data<bool>();
    if (root_is_domain) {
      Domain root_domain = fetch_data<Domain>();
      switch (root_domain.dim) {
        case 1:
          root = runtime->create_index_space(ctx, Domain::from_rect<1>(root_domain.get_rect<1>()));
          break;
        case 2:
          root = runtime->create_index_space(ctx, Domain::from_rect<2>(root_domain.get_rect<2>()));
          break;
        case 3:
          root = runtime->create_index_space(ctx, Domain::from_rect<3>(root_domain.get_rect<3>()));
          break;
        defalut:
          assert(false);
      }
    }
    else {
      ElementMask root_mask = fetch_data<ElementMask>();
      root = runtime->create_index_space(ctx, root_mask.get_num_elmts());
      IndexAllocator node_allocator = runtime->create_index_allocator(ctx, root);
      node_allocator.alloc(root_mask.get_num_elmts());
    }

    int num_IP_nodes = fetch_data<int>();

    mapper[root_id] = root;
    while (num_IP_nodes > 0) {
      id_type parent_id = fetch_data<id_type>();
      Color part_color = fetch_data<Color>();
      bool dis_joint = fetch_data<bool>();
      Domain set_grand_child_colors = fetch_data<Domain>();
      DomainColoring coloring;
      std::map<Color, id_type> ip_mapper;
      for (Domain::DomainPointIterator iter(set_grand_child_colors); iter; iter++) {
        id_type grand_child_id = fetch_data<id_type>();
        Color grand_child_color = fetch_data<Color>();
        ip_mapper[grand_child_color] = grand_child_id;
        bool grand_child_is_domain = fetch_data<bool>();
        Domain grand_child_domain;
        if (grand_child_is_domain) {
          grand_child_domain = fetch_data<Domain>();
        }
        else {
          ElementMask grand_child_mask = fetch_data<ElementMask>();
          IndexSpace grand_child_index_space = IndexSpace::create_index_space(mapper[parent_id], grand_child_mask);
          grand_child_domain = Domain(grand_child_index_space);
        }
        coloring[grand_child_color] = grand_child_domain;
      }

      assert(mapper.find(parent_id) != mapper.end());
      IndexPartition new_ip = runtime->create_index_partition(ctx, mapper[parent_id], set_grand_child_colors, coloring, dis_joint, part_color);
      assert(new_ip == runtime->get_index_partition(ctx, mapper[parent_id], part_color));
      for (std::map<Color, id_type>::const_iterator it = ip_mapper.begin(); it != ip_mapper.end(); it++) {
        mapper[it->second] = runtime->get_index_subspace(ctx, new_ip, it->first);
      }
      num_IP_nodes --;
    }

    return (size_t(cursor) - size_t(buffer));
  }

  template<typename TN>
  DomainFileManager<TN>::DomainFileManager(char* filename, bool isw) {
    if (isw) {
      printf("DFA: create WRITE file.\n");
      fd = open(filename, O_CREAT | O_RDWR | O_TRUNC, 00777);
    } else {
      printf("DFA: create READ file.\n");
      fd = open(filename, O_RDONLY, 00777);
    }
  }

  template<typename TN>
  DomainFileManager<TN>::~DomainFileManager(void) {
    assert(fd);
    close(fd);
  }

  template<>
  Domain DomainFileManager<Domain>::load_from_file(void) {
    size_t* ptr_len = (size_t*) mmap(NULL, sizeof(size_t), PROT_READ, MAP_SHARED, fd, 0);
    size_t length = *ptr_len;
    munmap(ptr_len, sizeof(size_t));

    Domain result;
    char* addr = (char*) mmap(NULL, length + sizeof(size_t), PROT_READ, MAP_SHARED, fd, 0) + sizeof(size_t);
    result = *((Domain*) addr);
    assert(length == sizeof(Domain));
    munmap(addr - sizeof(size_t), length + sizeof(size_t));
    return result;
  }

  template<>
  Coloring DomainFileManager<Coloring>::load_from_file(void) {
    size_t* ptr_len = (size_t*) mmap(NULL, sizeof(size_t), PROT_READ, MAP_SHARED, fd, 0);
    size_t length = *ptr_len;
    munmap(ptr_len, sizeof(size_t));

    char* addr = (char*) mmap(NULL, length + sizeof(size_t), PROT_READ, MAP_SHARED, fd, 0) + sizeof(size_t);
    ColoringSerializer ser;
    size_t load_len = ser.legion_deserialize(addr);
    assert(length == load_len);
    munmap(addr - sizeof(size_t), length + sizeof(size_t));
    return ser.ref();
  }

  template<>
  DomainColoring DomainFileManager<DomainColoring>::load_from_file(void) {
    size_t* ptr_len = (size_t*) mmap(NULL, sizeof(size_t), PROT_READ, MAP_SHARED, fd, 0);
    size_t length = *ptr_len;
    munmap(ptr_len, sizeof(size_t));

    char* addr = (char*) mmap(NULL, length + sizeof(size_t), PROT_READ, MAP_SHARED, fd, 0) + sizeof(size_t);
    DomainColoringSerializer ser;
    size_t load_len = ser.legion_deserialize(addr);
    assert(length == load_len);
    munmap(addr - sizeof(size_t), length + sizeof(size_t));
    return ser.ref();
  }

  template<>
  void DomainFileManager<Domain>::save_into_file(Domain id) {
    size_t length = sizeof(Domain);
    write(fd, "", length + sizeof(size_t));
    char* ptr_len = (char*) mmap(NULL, length + sizeof(size_t), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    *((size_t*) ptr_len) = length;
    Domain* addr = (Domain*) (ptr_len + sizeof(size_t));
    *addr = id;
    assert(*((size_t*) ptr_len) == 32);
    munmap(ptr_len, length + sizeof(size_t));
  }

  template<>
  void DomainFileManager<Coloring>::save_into_file(Coloring id) {
    ColoringSerializer ser(id);
    size_t length = ser.legion_buffer_size();
    write(fd, "", length + sizeof(size_t));
    char* ptr_len = (char*) mmap(NULL, length + sizeof(size_t), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    *((size_t*) ptr_len) = length;
    void* addr = ptr_len + sizeof(size_t);
    size_t save_len = ser.legion_serialize(addr);
    assert(length == save_len);
    munmap(ptr_len, length + sizeof(size_t));
  }

  template<>
  void DomainFileManager<DomainColoring>::save_into_file(DomainColoring id) {
    DomainColoringSerializer ser(id);
    size_t length = ser.legion_buffer_size();
    write(fd, "", length + sizeof(size_t));
    char* ptr_len = (char*) mmap(NULL, length + sizeof(size_t), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    *((size_t*) ptr_len) = length;
    void* addr = ptr_len + sizeof(size_t);
    size_t save_len = ser.legion_serialize(addr);
    assert(length == save_len);
    munmap(ptr_len, length + sizeof(size_t));
  }

  /**
   * \class RegionFileAccessor
   * A class for accessing region file. Given a filename and whether
   * you are going to write into it, a RegionFileAccessor could be
   * created. By using it, users could save region data into a
   * file, or load it from file.
   */
  template<typename TN, unsigned DIM>
  class RegionFileAccessor {
  public:
    RegionFileAccessor(char* filename, bool isw);
    RegionFileAccessor(char* filename, bool isw, HighLevelRuntime *runtime, Context ctx, IndexSpace is);
    ~RegionFileAccessor(void);

    void init_layout(HighLevelRuntime *runtime, Context ctx, IndexSpace is);
    void load_layout();
    void save_layout();
    int get_rank(Rect<DIM> rect, Point<DIM> point);

    /**
     * Load field data for a particular Rect in a particular PhysicalRegion.
     * Here we make PhysicalRegion, Rect and FieldID parameters for
     * functions instead of parameters for classes, since different fields,
     * physicalregions, rects may read the same data. But element data type
     * and dimension should be fixed for a data source.
     */
    void load_from_file(PhysicalRegion region, FieldID fid, Rect<DIM> rect);

    void load_from_file(RegionAccessor<AccessorType::Generic, TN> acc, Rect<DIM> rect);

    void load_from_file(RegionAccessor<AccessorType::Generic, TN> acc, IndexSpace is);

    /**
     * Save field data for a particular Rect in a particular PhysicalRegion.
     */
    void save_into_file(PhysicalRegion region, FieldID fid, Rect<DIM> rect);

    void save_into_file(RegionAccessor<AccessorType::Generic, TN> acc, Rect<DIM> rect);

    void save_into_file(RegionAccessor<AccessorType::Generic, TN> acc, IndexSpace is);
  private:
    int fd; // file descriptor
    char fn[256];
    std::vector<std::pair<char*, Domain> > rect_list;
    std::vector<std::pair<char*, ElementMask> > mask_list;
  };

  template<typename TN, unsigned DIM>
  RegionFileAccessor<TN, DIM>::RegionFileAccessor(char* filename, bool isw) {
    assert(!isw);
    fd = open(filename, O_RDONLY, 00777);

    strcpy(fn, filename);
    load_layout();
    close(fd);
  }

  template<typename TN, unsigned DIM>
  RegionFileAccessor<TN, DIM>::RegionFileAccessor(char* filename, bool isw, HighLevelRuntime *runtime, Context ctx, IndexSpace is) {
    assert(isw);
    fd = open(filename, O_CREAT | O_RDWR | O_TRUNC, 00777);

    strcpy(fn, filename);
    init_layout(runtime, ctx, is);
    save_layout();
    close(fd);
  }

  template<typename TN, unsigned DIM>
  RegionFileAccessor<TN, DIM>::~RegionFileAccessor(void) {
    for (std::vector<std::pair<char*, Domain> >::const_iterator it = rect_list.begin(); it != rect_list.end(); it++) {
      delete it->first;
    }
  }

  template<typename TN, unsigned DIM>
  void RegionFileAccessor<TN, DIM>::init_layout(HighLevelRuntime *runtime, Context ctx, IndexSpace is) {
    rect_list.clear();
    mask_list.clear();

    // use Breadth First Search Traversal to explore the whole IndexSpace tree
    std::queue<IndexSpace> queue;
    queue.push(is);
    while (!queue.empty()) {
      IndexSpace cur = queue.front();
      queue.pop();
      Domain cur_domain = runtime->get_index_space_domain(ctx, cur);
      std::set<Color> cur_colors;
      runtime->get_index_space_partition_colors(ctx, cur, cur_colors);
      if (cur_colors.empty()) {
        char* filename = new char[256];
        sprintf(filename, "input/%s_%lu.dat", fn, rect_list.size());
        rect_list.push_back(std::make_pair(filename, cur_domain));
        int filedes = open(filename, O_CREAT | O_RDWR | O_TRUNC, 00777);
        write(filedes, "", sizeof(TN) * cur_domain.get_volume());
        close(filedes);
      }

      for (std::set<Color>::const_iterator it = cur_colors.begin(); it != cur_colors.end(); it++) {
        IndexPartition child = runtime->get_index_partition(ctx, cur, *it);
        Domain child_colors = runtime->get_index_partition_color_space(ctx, child);
        assert(*it == runtime->get_index_partition_color(ctx, child));
        for (Domain::DomainPointIterator iter(child_colors); iter; iter++) {
          IndexSpace grand_child = runtime->get_index_subspace(ctx, child, iter.p.point_data[0]);
          assert(iter.p.point_data[0] == runtime->get_index_space_color(ctx, grand_child));
          Domain grand_child_domain = runtime->get_index_space_domain(ctx, grand_child);
          queue.push(grand_child);
        }
      }
    }
  }

  template<typename TN, unsigned DIM>
  void RegionFileAccessor<TN, DIM>::load_layout() {
    size_t* ptr_len = (size_t*) mmap(NULL, sizeof(size_t), PROT_READ, MAP_SHARED, fd, 0);
    size_t length = *ptr_len;
    munmap(ptr_len, sizeof(size_t));

    char* addr = (char*) mmap(NULL, length + sizeof(size_t), PROT_READ, MAP_SHARED, fd, 0);
    char* cursor = addr + sizeof(size_t);
    size_t list_size = *((size_t*)cursor);
    cursor += sizeof(size_t);
    for (size_t i = 0; i < list_size; i++) {
      size_t len = *((size_t*)cursor);
      cursor += sizeof(size_t);
      char* filename = new char[256];
      strncpy(filename, cursor, len);
      cursor += len;
      Domain cur_domain = *((Domain*)cursor);
      cursor += sizeof(Domain);
      rect_list.push_back(std::make_pair(filename, cur_domain));
    }
    munmap(addr, length + sizeof(size_t));
  }

  template<typename TN, unsigned DIM>
  void RegionFileAccessor<TN, DIM>::save_layout() {
    size_t length = sizeof(size_t);
    for (std::vector<std::pair<char*, Domain> >::const_iterator it = rect_list.begin(); it != rect_list.end(); it++) {
      length += sizeof(size_t) + strlen(it->first) + sizeof(Domain);
    }
    write(fd, "", length + sizeof(size_t));
    char* ptr_len = (char*) mmap(NULL, length + sizeof(size_t), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    *((size_t*)ptr_len) = length;
    char* addr = ptr_len + sizeof(size_t);
    *((size_t*)addr) = rect_list.size();
    addr += sizeof(size_t);
    for (std::vector<std::pair<char*, Domain> >::const_iterator it = rect_list.begin(); it != rect_list.end(); it++) {
      *((size_t*)addr) = strlen(it->first);
      addr += sizeof(size_t);
      strcpy(addr, it->first);
      addr += strlen(it->first);
      *((Domain*)addr) = it->second;
      addr += sizeof(Domain);
    }
    munmap(ptr_len, length + sizeof(size_t));
  }

  template<typename TN, unsigned DIM>
  int RegionFileAccessor<TN, DIM>::get_rank(Rect<DIM> rect, Point<DIM> point) {
    assert(rect.contains(point));
    int dim_multi[DIM+1];
    dim_multi[DIM] = 1;
    for (int i = DIM-1; i >=0; i--) {
      dim_multi[i] = dim_multi[i+1] * rect.dim_size(i);
    }
    int rank = 0;
    for (int i = 0; i < DIM; i++) {
      rank += dim_multi[i+1] * (point[i] - rect.lo[i]);
    }
    return rank;
  }

  template<typename TN, unsigned DIM>
  void RegionFileAccessor<TN, DIM>::load_from_file(PhysicalRegion region, FieldID fid, Rect<DIM> rect) {
    RegionAccessor<AccessorType::Generic, TN> acc = region.get_field_accessor(fid).typeify<TN> ();
    load_from_file(acc, rect);
  }

  template<typename TN, unsigned DIM>
  void RegionFileAccessor<TN, DIM>::load_from_file(RegionAccessor<AccessorType::Generic, TN> acc, Rect<DIM> rect) {
    for (std::vector<std::pair<char*, Domain> >::const_iterator it = rect_list.begin(); it != rect_list.end(); it++)
      if (rect.overlaps(it->second.get_rect<DIM>())) {
        int filedes = open(it->first, O_RDONLY, 00777);
        size_t length = sizeof(TN) * it->second.get_volume();
        TN* addr = (TN*)mmap(NULL, length, PROT_READ, MAP_SHARED, filedes, 0);
        Rect<DIM> intersect = rect.intersection(it->second.get_rect<DIM>());
        for (GenericPointInRectIterator < DIM > pir(intersect); pir; pir++) {
          TN entry = *(addr + get_rank(it->second.get_rect<DIM>(), pir.p));
          acc.write(DomainPoint::from_point<DIM>(pir.p), entry);
        }
        munmap(addr, length);
        close(filedes);
      }
  }

  template<typename TN, unsigned DIM>
  void RegionFileAccessor<TN, DIM>::save_into_file(PhysicalRegion region, FieldID fid, Rect<DIM> rect) {
    RegionAccessor<AccessorType::Generic, TN> acc = region.get_field_accessor(fid).typeify<TN> ();
    save_into_file(acc, rect);
  }

  template<typename TN, unsigned DIM>
  void RegionFileAccessor<TN, DIM>::save_into_file(RegionAccessor<AccessorType::Generic, TN> acc, Rect<DIM> rect) {
    for (std::vector<std::pair<char*, Domain> >::const_iterator it = rect_list.begin(); it != rect_list.end(); it++)
      if (rect.overlaps(it->second.get_rect<DIM>())) {
        int filedes = open(it->first, O_CREAT | O_RDWR | O_TRUNC, 00777);
        write(filedes, "", sizeof(TN) * it->second.get_volume());
        size_t length = sizeof(TN) * it->second.get_volume();
        TN* addr = (TN*) mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, filedes, 0);
        Rect<DIM> intersect = rect.intersection(it->second.get_rect<DIM>());
        for (GenericPointInRectIterator < DIM > pir(intersect); pir; pir++) {
          int rank = get_rank(it->second.get_rect<DIM>(), pir.p);
          *(addr + rank) = acc.read(DomainPoint::from_point<DIM>(pir.p));
        }
        munmap(addr, length);
        close(filedes);
      }
  }

}
#endif //__LEGION_STORAGE_H__

