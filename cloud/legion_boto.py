#!/usr/bin/python

import boto.ec2
import re
import readline

AWS_USER_ID='931181932734'
AWS_ACCESS_KEY='AKIAJLJEKGMEMXAO6T2Q'
AWS_SECRET_KEY='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'
AWS_KEY_NAME="legion"
AWS_SECURITY_GROUP="sapling-cloud"
# Current target region is oregon
TARGET_REGION ='us-west-2'
TARGET_ZONE='us-west-2a'

# Make sure we don't burn money
MAX_INSTANCES=32

# Ubuntu 12.04 LTS 64-bit server
BASE_UBUNTU='ami-70f96e40'
LEGION_AMI='ami-29019019'
HVM_AMI='ami-a11d8a91'
LEGION_HVM_AMI='ami-d10293e1'

EBS_ATTACH_DIR='/dev/sdh'

option_pat = re.compile("print options")
quit_pat   = re.compile("quit")
status_pat = re.compile("print status")
start_ubuntu_pat = re.compile("start ubuntu (?P<num>[0-9]+)")
start_legion_pat = re.compile("start legion (?P<num>[0-9]+)")
start_hvm_pat = re.compile("start hvm (?P<num>[0-9]+)")
start_leg_hvm_pat = re.compile("start leghvm (?P<num>[0-9]+)")
create_ami_pat = re.compile("create AMI")
create_hvm_pat = re.compile("create HVM AMI")
term_inst_pat = re.compile("terminate inst")
term_all_pat = re.compile("terminate all")
attach_vol_pat = re.compile("attach inst volume")
detach_vol_pat = re.compile("detach inst volume")
print_console_pat = re.compile("print console")
create_place_pat = re.compile("create placement group")
delete_place_pat = re.compile("delete placement group")
print_place_pat = re.compile("print placement groups")

def check_max_instances(num):
    if num > MAX_INSTANCES:
        print "THIS IS THE CHECK TO MAKE SURE WE DON'T BURN MONEY!"
        print "YOU\"RE ABOUT TO BURN MONEY!"
        print "NOT LETTING YOU DO IT! Asserting..."
        assert False

def select_instance_kind():
    while True:
        print "    Instance kinds:"
        print "      0) Micro              ($.020)"
        print "      1) Small              ($.060)"
        print "      2) Medium             ($.120)"
        print "      3) Large              ($.240)"
        print "      4) X-large            ($.480)"
        print "      5) High-CPU Medium    ($.145)"
        print "      6) High-CPU X-Large   ($.580)"
        print "      7) High-Mem X-Large   ($.410)"
        print "      8) High-Mem 2X-Large  ($.820)"
        print "      9) Compute Cluster 4X   N/A  "
        print "      10) Compute Cluster 8X ($2.40)"
        print "      11) GPU Cluser 4X        N/A  "
        kind = int(raw_input("Select instance kind: "))
        if kind == 0:
            return "t1.micro"
        elif kind == 1:
            return "m1.small"
        elif kind == 2:
            return "m1.medium"
        elif kind == 3:
            return "m1.large"
        elif kind == 4:
            return "m1.xlarge"
        elif kind == 5:
            return "c1.medium"
        elif kind == 6:
            return "c1.xlarge"
        elif kind == 7:
            return "m3.xlarge"
        elif kind == 8:
            return "m3.2xlarge"
        elif kind == 9:
            assert False
            return "cc1.4xlarge"
        elif kind == 10:
            return "cc2.8xlarge"
        elif kind == 11:
            assert False
            return "cg1.4xlarge"
        print "    Invalid kind!  Try again..."

def select_placement_group(conn):
    groups = conn.get_all_placement_groups()
    if len(groups) == 0:
        return None
    print "-1) NO PLACEMENT GROUP"
    for idx in range(len(groups)):
        print str(idx)+") "+groups[idx].name
    index = int(raw_input("Select a placement group for the launch: "))
    if index not in range(len(groups)):
        if index == -1:
            return None
        print "Invalid index selection!  Instance will not be in a placement group."
        return None
    return groups[index].name

def print_options():
    print "Options:"
    print "  'print options' - print this list"
    print "  'print status' - print all the instances and their states"
    print "  'start ubuntu <num>' - start the given number of ubuntu instances"
    print "  'start legion <num>' - start the given number of legion instances"
    print "  'start hvm <num>' - start the given number of hvm instances"
    print "  'start leghvm <num>' - start the given number of legion hvm instances"
    print "  'terminate inst' - terminate a selected instance"
    print "  'terminate all' - terminate all running instances"
    print "  'create AMI' - create an AMI from one of the instances"
    print "  'create HVM AMI' - create an HVM AMI from an HVM instance"
    print "  'attach inst volume' - attach a volume to a specific instance"
    print "  'detach inst volume' - detach a volume from an instance"
    print "  'print console' - print the console for an instance"
    print "  'create placement group' - create a cluster placement group"
    print "  'delete placement group' - delete a cluster placement group"
    print "  'print placement groups' - list the current placement groups"
    print "  'quit'"

def print_reservations(conn):
    reservations = conn.get_all_instances()
    print "Total reservations: "+str(len(reservations))
    for r in reservations:
        print "  Reservation "+str(r.id)+" with "+str(len(r.instances))+" instances"
        for inst in r.instances:
            print "    Instance "+str(inst.id)
            print "      Name: "+str(inst.public_dns_name)
            print "      State: "+str(inst.state)
            print "      AMI ID: "+str(inst.image_id)
            print "      Start Time: "+str(inst.launch_time)

def print_volumes(conn):
    volumes = conn.get_all_volumes()
    print "Total volumes: "+str(len(volumes))
    for v in volumes:
        attachment_set = v.attach_data
        print "  Volume "+str(v.id)
        print "    Status: "+str(v.status)
        print "    Size: "+str(v.size)+" GB"
        print "    Region: "+str(v.zone)
        print "    Instances: "+str(attachment_set.instance_id)

def start_ubuntu(conn, num):
    if num <= 0:
        return
    check_max_instances(num)
    ubuntu = conn.get_image(BASE_UBUNTU)
    assert ubuntu <> None
    kind = select_instance_kind()
    if kind == "cc1.4xlarge" or kind == "cc2.8xlarge" or kind == "cg1.4xlarge":
        place_group = select_placement_group(conn)
    else:
        place_group = None
    security = list()
    security.append(AWS_SECURITY_GROUP)
    print "Starting "+str(num)+" Ubuntu instances of kind: "+kind+"..."
    ubuntu.run(min_count=num,max_count=num,key_name=AWS_KEY_NAME,
              security_groups=security,instance_type=kind,
              placement=TARGET_ZONE,placement_group=place_group) 
    print "Done"

def start_legion(conn, num):
    if num <= 0:
        return
    check_max_instances(num)
    legion = conn.get_image(LEGION_AMI)
    assert legion <> None
    kind = select_instance_kind()
    if kind == "cc1.4xlarge" or kind == "cc2.8xlarge" or kind == "cg1.4xlarge":
        place_group = select_placement_group(conn)
    else:
        place_group = None
    security = list()
    security.append(AWS_SECURITY_GROUP)
    print "Starting "+str(num)+" Legion instances of kind: "+kind+"..."
    legion.run(min_count=num,max_count=num,key_name=AWS_KEY_NAME,
               security_groups=security,instance_type=kind,
               placement=TARGET_ZONE,placement_group=place_group)
    print "Done"

def start_hvm(conn, num):
    if num <= 0:
        return
    check_max_instances(num)
    hvm = conn.get_image(HVM_AMI)
    assert hvm <> None
    kind = select_instance_kind()
    if kind == "cc1.4xlarge" or kind == "cc2.8xlarge" or kind == "cg1.4xlarge":
        place_group = select_placement_group(conn)
    else:
        place_group = None
    security = list()
    security.append(AWS_SECURITY_GROUP)
    print "Starting "+str(num)+" HVM instances of kind: "+kind+"..."
    hvm.run(min_count=num,max_count=num,key_name=AWS_KEY_NAME,
               security_groups=security,instance_type=kind,
               placement=TARGET_ZONE,placement_group=place_group)
    print "Done"

def start_legion_hvm(conn, num):
    if num <= 0:
        return
    check_max_instances(num)
    legion = conn.get_image(LEGION_HVM_AMI)
    assert legion <> None
    kind = select_instance_kind()
    if kind == "cc1.4xlarge" or kind == "cc2.8xlarge" or kind == "cg1.4xlarge":
        place_group = select_placement_group(conn)
    else:
        place_group = None
    security = list()
    security.append(AWS_SECURITY_GROUP)
    print "Starting "+str(num)+" Legion HVM instances of kind: "+kind+"..."
    legion.run(min_count=num,max_count=num,key_name=AWS_KEY_NAME,
               security_groups=security,instance_type=kind,
               placement=TARGET_ZONE,placement_group=place_group)
    print "Done"

def term_inst(conn):
    reservations = conn.get_all_instances()
    instances = dict()
    idx = 0
    for r in reservations:
        for inst in r.instances:
            if inst.state <> "running":
                continue
            instances[idx] = inst
            idx = idx + 1
    if len(instances) == 0:
        print "No instances to terminate!"
        return
    for idx,inst in instances.iteritems():
        print str(idx)+") "+str(inst.id)+" "+str(inst.public_dns_name)
    target = int(raw_input("Select target instance: "))
    if target not in instances:
        print "Invalid index selection"
        return
    term = raw_input("Terminate target instance "+str(target)+"? [y/n] ")
    if term <> "y":
        print "Not terminating instance"
        return
    instances[target].terminate()

def term_all(conn):
    print "Terminating all instances"
    reservations = conn.get_all_instances()
    for r in reservations:
        for inst in r.instances:
            inst.terminate()
    print "Done"
            
def create_ami(conn):
    instances = dict()
    idx = 0
    reservations = conn.get_all_instances()
    for r in reservations:
        for inst in r.instances:
            # Only use instances which are still running
            if inst.state <> "running":
                continue
            instances[idx] = inst
            idx = idx + 1
    for idx,inst in instances.iteritems():
        print str(idx)+") "+str(inst.id)+" "+str(inst.public_dns_name)
    index = int(raw_input("Select an instance from which to make an AMI: "))
    if index not in instances:
        print "Invalid index selection!  Not creating AMI."
        return
    deregister = raw_input("Deregister old image [y/n]: ")
    if deregister <> "y":
        print "Not deregistering old image"
        return
    deregister = conn.deregister_image(image_id=LEGION_AMI,delete_snapshot=True)
    if not deregister:
        print "WARNING: Deregistering old image failed! Unable to register new image."
        return
    print "Creating image..."
    ami_id = instances[index].create_image(name='ami-legion',description='AMI for running Legion')
    print "Done"
    print "The new AMI id is "+str(ami_id)

def create_hvm_ami(conn):
    instances = dict()
    idx = 0
    reservations = conn.get_all_instances()
    for r in reservations:
        for inst in r.instances:
            # Only use instances which are still running
            if inst.state <> "running":
                continue
            instances[idx] = inst
            idx = idx + 1
    for idx,inst in instances.iteritems():
        print str(idx)+") "+str(inst.id)+" "+str(inst.public_dns_name)
    index = int(raw_input("Select an instance from which to make an HVM AMI: "))
    if index not in instances:
        print "Invalid index selection!  Not creating AMI."
        return
    deregister = raw_input("Deregister old image [y/n]: ")
    if deregister <> "y":
        print "Not deregistering old image"
        return
    deregister = conn.deregister_image(image_id=LEGION_HVMAMI,delete_snapshot=True)
    if not deregister:
        print "WARNING: Deregistering old image failed! Unable to register new image."
        return

    print "Creating image..."
    ami_id = instances[index].create_image(name='ami-legion-hvm',description='HVM AMI for running Legion')
    print "Done"
    print "The new AMI id is "+str(ami_id)

def attach_inst_volume(conn):
    instances = dict()
    idx = 0
    reservations = conn.get_all_instances()
    for r in reservations:
        for inst in r.instances:
            # Only use instances which are still running
            if inst.state <> "running":
                continue
            instances[idx] = inst
            idx = idx + 1
    for idx,inst in instances.iteritems():
        print str(idx)+") "+str(inst.id)+" "+str(inst.public_dns_name)
    index = int(raw_input("Select an instance to attach a volume to: "))
    if index not in instances:
        print "Invalid index selection!  Not attaching volume."
        return
    volumes = conn.get_all_volumes()
    for idx in range(len(volumes)):
        vol = volumes[idx]
        print str(idx)+") "+str(vol.id)
    vol_idx = int(raw_input("Select the volume to attach: "))
    if vol_idx >= len(volumes) or (vol_idx < 0):
        print "Invalid volume selection!  Not attaching volume."
        return
    result = volumes[vol_idx].attach(instances[index].id,EBS_ATTACH_DIR)
    if not result:
        print "WARNING: attaching volume "+str(volumes[vol_idx])+" to instance "+str(instances[index].id)+" failed!"

def detach_inst_volume(conn):
    volumes = conn.get_all_volumes()
    for idx in range(len(volumes)):
        vol = volumes[idx]
        print str(idx)+") "+str(vol.id)
    vol_idx = int(raw_input("Select the volume to detach: "))
    if vol_idx >= len(volumes) or (vol_idx < 0):
        print "Invalid volume selection!  Not detaching volume."
        return
    result = volumes[vol_idx].detach()
    if not result:
        print "WARNING: detaching volume "+str(volumes[vol_idx])+" did not succeed"

def print_console(conn):
    instances = dict()
    idx = 0
    reservations = conn.get_all_instances()
    for r in reservations:
        for inst in r.instances:
            # Only use instances which are still running
            if inst.state <> "running":
                continue
            instances[idx] = inst
            idx = idx + 1
    for idx,inst in instances.iteritems():
        print str(idx)+") "+str(inst.id)+" "+str(inst.public_dns_name)
    index = int(raw_input("Select an instance to print the console for: "))
    if index not in instances:
        print "Invalid index selection!  Not printing console."
        return
    result = conn.get_console_output(instances[index].id)
    print "CONSOLE OUTPUT:"
    print str(result.output)

def create_placement_group(conn):
    group_name = raw_input("Input a name for the cluster placement group: ")
    result = conn.create_placement_group(group_name)
    if not result:
        print "WARNING: failed to create cluster placement group"

def delete_placement_group(conn):
    groups = conn.get_all_placement_groups()
    if len(groups) == 0:
        print "No placement groups to delete."
        return
    for idx in range(len(groups)):
        print str(idx)+") "+str(groups[idx].name)
    index = int(raw_input("Selelct a placement group to delete: "))
    if index not in range(len(groups)):
        print "Invalid index selection!  Not deleting any placement groups."
        return
    result = groups[index].delete()
    if not result:
        print "WARNING: failed to delete placement group "+str(groups[index].name)

def print_placement_groups(conn):
    groups = conn.get_all_placement_groups()
    for g in groups:
        print "- "+str(g.name)

def main():
    #regions = boto.ec2.regions()
    #for r in regions:
    #    print "Region "+str(r.name)
    print "Connecting to region "+str(TARGET_REGION)+'...'
    conn = boto.ec2.connect_to_region(TARGET_REGION,
            aws_access_key_id=AWS_ACCESS_KEY,
            aws_secret_access_key=AWS_SECRET_KEY)
    if conn <> None:
        print 'SUCCESS!'
    else:
        print 'FAILURE!'
        return
    #zones = conn.get_all_zones()
    #for z in zones:
    #    print "Zone "+str(z)
    print_options()
    while True:
        try:
            cmd = raw_input('Enter command: ')
        except EOFError:
            break
        m = option_pat.match(cmd)
        if m <> None:
            print_options()
            continue
        m = quit_pat.match(cmd)
        if m <> None:
            break
        m = status_pat.match(cmd)
        if m <> None:
            print_reservations(conn)
            print_volumes(conn)
            continue
        m = start_ubuntu_pat.match(cmd)
        if m <> None:
            start_ubuntu(conn, int(m.group('num')))
            continue
        m = start_legion_pat.match(cmd)
        if m <> None:
            start_legion(conn, int(m.group('num')))
            continue
        m = start_hvm_pat.match(cmd)
        if m <> None:
            start_hvm(conn, int(m.group('num')))
            continue
        m = start_leg_hvm_pat.match(cmd)
        if m <> None:
            start_legion_hvm(conn, int(m.group('num')))
            continue
        m = term_inst_pat.match(cmd)
        if m <> None:
            term_inst(conn)
            continue
        m = term_all_pat.match(cmd)
        if m <> None:
            term_all(conn)
            continue
        m = create_ami_pat.match(cmd)
        if m <> None:
            create_ami(conn)
            continue
        m = create_hvm_pat.match(cmd)
        if m <> None:
            create_hvm_ami(conn)
            continue
        m = attach_vol_pat.match(cmd)
        if m <> None:
            attach_inst_volume(conn)
            continue
        m = detach_vol_pat.match(cmd)
        if m <> None:
            detach_inst_volume(conn)
            continue
        m = print_console_pat.match(cmd)
        if m <> None:
            print_console(conn)
            continue
        m = create_place_pat.match(cmd)
        if m <> None:
            create_placement_group(conn)
            continue
        m = delete_place_pat.match(cmd)
        if m <> None:
            delete_placement_group(conn)
            continue
        m = print_place_pat.match(cmd)
        if m <> None:
            print_placement_groups(conn)
            continue
        print "Invalid command!  Here are your options."
        print_options()

if __name__ == "__main__":
    main()

