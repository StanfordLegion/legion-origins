
#include "replay_mapper.h"

namespace LegionRuntime {
  namespace HighLevel {

    Logger::Category log_replay("replay_mapper");

    ReplayTestMapper::ReplayTestMapper(Machine *machine, HighLevelRuntime *runtime,
                                       Processor local, const char *file)
      : DefaultMapper(machine, runtime, local), replay_file(file)
    {
    }

    ReplayTestMapper::ReplayTestMapper(const ReplayTestMapper &rhs)
      : DefaultMapper(NULL, NULL, Processor::NO_PROC), replay_file(NULL)
    {
      // should never be called
      assert(false);
    }

    ReplayTestMapper::~ReplayTestMapper(void)
    {
    }

    ReplayTestMapper& ReplayTestMapper::operator=(const ReplayTestMapper &rhs)
    {
      // should never be called
      assert(false);
      return *this;
    }

  }; // namespace HighLevel 
}; // namespace LegionRuntime

