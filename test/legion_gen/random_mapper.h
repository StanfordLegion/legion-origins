
#ifndef __LEGION_TEST_RANDOM_MAPPER__
#define __LEGION_TEST_RANDOM_MAPPER__

#include <cstdio>
#include <cstdlib>
#include <cassert>

#include "legion.h"
#include "default_mapper.h"

namespace LegionRuntime {
  namespace HighLevel {
    
    class RandomTestMapper : public DefaultMapper {
    public:
      RandomTestMapper(Machine *machine, HighLevelRuntime *runtime,
                       Processor local, const char *replay_file);
      RandomTestMapper(const RandomTestMapper &rhs);
      virtual ~RandomTestMapper(void);
    public:
      RandomTestMapper& operator=(const RandomTestMapper &rhs);
    public:
      const char *const replay_file;
    };

  }; // namespace HighLevel
}; // namespace LegionRuntime

#endif // __LEGION_TEST_RANDOM_MAPPER__

