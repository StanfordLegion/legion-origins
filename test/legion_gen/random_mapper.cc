
#include "random_mapper.h"

namespace LegionRuntime {
  namespace HighLevel {

    Logger::Category log_random("random_mapper");

    RandomTestMapper::RandomTestMapper(Machine *machine, HighLevelRuntime *runtime,
                                       Processor local, const char *file)
      : DefaultMapper(machine, runtime, local), replay_file(file)
    {
    }

    RandomTestMapper::RandomTestMapper(const RandomTestMapper &rhs)
      : DefaultMapper(NULL, NULL, Processor::NO_PROC), replay_file(NULL)
    {
      // should never be called
      assert(false);
    }

    RandomTestMapper::~RandomTestMapper(void)
    {
    }

    RandomTestMapper& RandomTestMapper::operator=(const RandomTestMapper &rhs)
    {
      // should never be called
      assert(false);
      return *this;
    }

  }; // namespace HighLevel 
}; // namespace LegionRuntime

