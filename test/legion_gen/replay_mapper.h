
#ifndef __LEGION_TEST_REPLAY_MAPPER__
#define __LEGION_TEST_REPLAY_MAPPER__

#include <cstdio>
#include <cstdlib>
#include <cassert>

#include "legion.h"
#include "default_mapper.h"

namespace LegionRuntime {
  namespace HighLevel {

    class ReplayTestMapper : public DefaultMapper {
    public:
      ReplayTestMapper(Machine *machine, HighLevelRuntime *runtime,
                       Processor local, const char *replay_file);
      ReplayTestMapper(const ReplayTestMapper &rhs);
      virtual ~ReplayTestMapper(void);
    public:
      ReplayTestMapper& operator=(const ReplayTestMapper &rhs);
    public:
      const char *const replay_file;
    };
 
  }; // namespace HighLevel
}; // namespace LegionRuntime

#endif // __LEGION_TEST_REPLAY_MAPPER__

