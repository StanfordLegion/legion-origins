#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>

#include <time.h>

#include "lowlevel.h"

#define LO_X 10
#define LO_Y 10
#define HI_X 100
#define HI_Y 100
#define DIM_X (HI_X - LO_X)
#define DIM_Y (HI_Y - LO_Y)
#define FILE_NAME "file.h5"
#define DATA_SET1 "/dset1"
#define DATA_SET2 "/dest2"

using namespace LegionRuntime::LowLevel;
using namespace LegionRuntime::Accessor;

int data1[DIM_X][DIM_Y];
float data2[DIM_X][DIM_Y];

enum {
  TOP_LEVEL_TASK = Processor::TASK_ID_FIRST_AVAILABLE+0,
};

void top_level_task(const void *args, size_t arglen, Processor p)
{
  printf("top level task - map HDF file\n");

  // create Domain that is to be mapped to HDF file
  Point<2> lo = make_point(LO_X, LO_Y), hi = make_point(HI_X - 1, HI_Y - 1);
  Rect<2> rect(lo, hi);
  Domain domain = Domain::from_rect<2>(rect);

  // get HDF memory instance
  Machine machine = Machine::get_machine();
  std::set<Memory> mem;
  Memory hdf_mem;
  machine.get_all_memories(mem);
  for(std::set<Memory>::iterator it = mem.begin(); it != mem.end(); it++) {
    if (it->kind() == Memory::HDF_MEM) {
      printf("find HDF mem\n");
      hdf_mem = *it;
    }
  }

  std::vector<size_t> field_sizes;
  std::vector<std::string> field_paths;
  std::string file_name;
  file_name = FILE_NAME;
  field_sizes.push_back(sizeof(int));
  field_sizes.push_back(sizeof(float));
  field_paths.push_back(DATA_SET1);
  field_paths.push_back(DATA_SET2);
  // create instance: the map_hdf function should invoke following
  // function to create instance
  RegionInstance inst = domain.mmap_instance(hdf_mem, field_sizes, field_paths, file_name);

  // create an accessor and read HDF file
  int read_data1[DIM_X][DIM_Y];
  float read_data2[DIM_X][DIM_Y];
  RegionAccessor<AccessorType::Generic> acc = inst.get_accessor();
  for (GenericPointInRectIterator<2> pir(rect); pir; pir++) {
    int i = pir.p.x[0] - LO_X;
    int j = pir.p.x[1] - LO_Y;
    acc.read_untyped(DomainPoint::from_point<2>(pir.p), &read_data1[i][j], sizeof(int), 0);
    acc.read_untyped(DomainPoint::from_point<2>(pir.p), &read_data2[i][j], sizeof(float), sizeof(int));
  }
  
  for (int i = 0; i < DIM_X; i++)
    for (int j = 0; j < DIM_Y; j++) {
      assert(read_data1[i][j] == data1[i][j]);
      assert(read_data2[i][j] == data2[i][j]);
    }
  printf("all check passed......\n");
  printf("finish top level task......\n");
  return;
}

void create_hdf_file()
{
  for (int i = 0; i < DIM_X; i++)
    for (int j = 0; j < DIM_Y; j++) {
      data1[i][j] = rand();
      data2[i][j] = rand() / (float) RAND_MAX;
    }
  hid_t file_id = H5Fcreate(FILE_NAME, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  hsize_t dims[2];
  dims[0] = DIM_X; dims[1] = DIM_Y;
  hid_t dataspace_id = H5Screate_simple(2, dims, NULL);
  hid_t dataset1 = H5Dcreate2(file_id, DATA_SET1, H5T_STD_I32BE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  hid_t dataset2 = H5Dcreate2(file_id, DATA_SET2, H5T_IEEE_F32BE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  H5Dwrite(dataset1, H5T_STD_I32BE, H5S_ALL, H5S_ALL, H5P_DEFAULT, data1);
  H5Dwrite(dataset2, H5T_IEEE_F32BE, H5S_ALL, H5S_ALL, H5P_DEFAULT, data2);
  H5Dclose(dataset1);
  H5Dclose(dataset2);
  H5Sclose(dataspace_id);
  H5Fclose(file_id);
}

int main(int argc, char **argv)
{
  // create HDF file that contains initial data
  create_hdf_file();

  Runtime rt;
  
  rt.init(&argc, &argv);
  rt.register_task(TOP_LEVEL_TASK, top_level_task);

  rt.run(TOP_LEVEL_TASK, Runtime::ONE_TASK_ONLY);

  rt.shutdown();
  
  return -1;

}
