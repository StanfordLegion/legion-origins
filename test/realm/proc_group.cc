#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>

#include <time.h>

#include "lowlevel.h"

using namespace LegionRuntime::LowLevel;

// Task IDs, some IDs are reserved so start at first available number
enum {
  TOP_LEVEL_TASK = Processor::TASK_ID_FIRST_AVAILABLE+0,
  DELAY_TASK     = Processor::TASK_ID_FIRST_AVAILABLE+1,
};

void delay_task(const void *args, size_t arglent, Processor p)
{
  printf("starting task on processor " IDFMT "\n", p.id);
  sleep(5);
  printf("ending task on processor " IDFMT "\n", p.id);
}

void top_level_task(const void *args, size_t arglen, Processor p)
{
  printf("top level task - getting machine and list of CPUs\n");

  Machine *machine = Machine::get_machine();
  std::vector<Processor> all_cpus;
  {
    const std::set<Processor> &all_processors = machine->get_all_processors();
    for(std::set<Processor>::const_iterator it = all_processors.begin();
	it != all_processors.end();
	it++)
      if(machine->get_processor_kind(*it) == Processor::LOC_PROC)
	all_cpus.push_back(*it);
  }

  printf("creating processor group for all CPUs...\n");
  Processor pgrp = Processor::create_group(all_cpus);
  printf("group ID is " IDFMT "\n", pgrp.id);

  // see if the member list is what we expect
  std::vector<Processor> members;
  pgrp.get_group_members(members);
  if(members == all_cpus)
    printf("member list matches\n");
  else {
    printf("member list MISMATCHES\n");
    printf("expected:");
    for(std::vector<Processor>::const_iterator it = all_cpus.begin();
	it != all_cpus.end();
	it++)
      printf(" " IDFMT, (*it).id);
    printf("\n");
    printf("  actual:");
    for(std::vector<Processor>::const_iterator it = members.begin();
	it != members.end();
	it++)
      printf(" " IDFMT, (*it).id);
    printf("\n");
  }

  std::set<Event> task_events;
  int count = 0;
  for(size_t i = 0; i < all_cpus.size(); i++) {
    for(int j = 0; j < 2; j++) {  // two tasks per actual cpu
      count++;
      Event e = pgrp.spawn(DELAY_TASK, &count, sizeof(count));
      task_events.insert(e);
    }
  }
  printf("%d tasks launched\n", count);

  // merge events
  Event merged = Event::merge_events(task_events);
  printf("merged event ID is " IDFMT "/%d - waiting on it...\n",
	 merged.id, merged.gen);

  merged.wait();
  printf("done!\n");

  // shutdown the runtime
  {
    Machine *machine = Machine::get_machine();
    const std::set<Processor> &all_procs = machine->get_all_processors();
    for (std::set<Processor>::const_iterator it = all_procs.begin();
          it != all_procs.end(); it++)
    {
      // Damn you C++ and your broken const qualifiers
      Processor handle_copy = *it;
      // Send the kill pill
      handle_copy.spawn(0,NULL,0);
    }
  }
}

int main(int argc, char **argv)
{
  // Build the task table that the processors will use when running
  Processor::TaskIDTable task_table;
  ReductionOpTable redop_table;
  task_table[TOP_LEVEL_TASK] = top_level_task;
  task_table[DELAY_TASK] = delay_task;

  // Initialize the machine
  Machine m(&argc,&argv,task_table,redop_table,false/*cps style*/);

  // Start the machine running
  // Control never returns from this call
  // Note we only run the top level task on one processor
  // You can also run the top level task on all processors or one processor per node
  m.run(TOP_LEVEL_TASK, Machine::ONE_TASK_ONLY);

  return -1;
}
