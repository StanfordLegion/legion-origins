#!/usr/bin/env python

import os, platform, subprocess, sys

os_name = platform.system()

root_dir = os.path.realpath(os.path.dirname(__file__))
legion_dir = os.path.dirname(root_dir)

terra_dir = os.path.join(root_dir, 'terra')
luabind_dir = os.path.join(root_dir, 'luabind', 'install_dir')
luajit_dir = os.path.join(root_dir, 'luabind', 'fake_luajit_dir')

runtime_dir = os.path.join(legion_dir, 'runtime')
bindings_dir = os.path.join(legion_dir, 'bindings', 'lua')

terra_path = [
    '?.lua',
    '?.t',
    os.path.join(root_dir, 'src', '?.t'),
    os.path.join(terra_dir, 'tests', 'lib', '?.t'),
    os.path.join(bindings_dir, '?.lua'),
    os.path.join(bindings_dir, '?.t'),
]

include_path = [
    bindings_dir,
    runtime_dir,
]

lib_path = [
    os.path.join(luabind_dir, 'lib'),
    os.path.join(luajit_dir, 'lib'),
    os.path.join(terra_dir, 'build'),
    bindings_dir,
]

LD_LIBRARY_PATH = 'LD_LIBRARY_PATH'
if os_name == 'Darwin':
    LD_LIBRARY_PATH = 'DYLD_LIBRARY_PATH'

terra_exe = os.path.join(terra_dir, 'terra')
terra_env = dict(os.environ.items() + [
    ('LUA_PATH', ';'.join(terra_path)),
    (LD_LIBRARY_PATH, ':'.join(lib_path)),
    ('INCLUDE_PATH', ';'.join(include_path)),
])

def legion(args, **kwargs):
    return subprocess.Popen([terra_exe] + args, env = terra_env, **kwargs)

if __name__ == '__main__':
    sys.exit(legion(sys.argv[1:]).wait())
