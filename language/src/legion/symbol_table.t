-- Legion Symbol Table

local log = require("legion/log")

local symbol_table = {}
symbol_table.__index = symbol_table

symbol_table.new_global_scope = function(lua_env)
  local st = setmetatable({}, symbol_table)
  st.lua_env = lua_env
  st.local_env = {}
  st.combined_env = setmetatable({}, {
      __index = function(_, index)
        return st.local_env[index] or st.lua_env[index]
      end,
      __newindex = function()
        log.error("cannot create global variables")
      end,
  })
  return st
end

function symbol_table:new_local_scope()
  local st = setmetatable({}, symbol_table)
  st.lua_env = self.lua_env
  st.local_env = setmetatable({}, { __index = self.local_env })
  st.combined_env = setmetatable({}, {
      __index = function(_, index)
        return st.local_env[index] or st.lua_env[index]
      end,
      __newindex = function()
        log.error("cannot create global variables")
      end,
  })
  return st
end

function symbol_table:safe_lookup(index)
  return self.combined_env[index]
end

function symbol_table:lookup(node, index)
  local value = self.combined_env[index]
  if value == nil then
    log.error(node, "name '" .. tostring(index) .. "' is undefined or nil")
  end
  return value
end

function symbol_table:insert(node, index, value)
  if rawget(self.local_env, index) then
    log.error(node, "name '" .. tostring(index) .. "' is already defined")
  end
  rawset(self.local_env, index, value)
  return value
end

function symbol_table:env()
  return self.combined_env
end

return symbol_table
