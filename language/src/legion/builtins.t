-- Legion Builtins

local std = require("legion/std")

local builtins = {}

-- Builtins consists of a list of which will be stuffed into the
-- global scope of any legion program (i.e. they need not be accessed
-- via std).

builtins.region = std.region
builtins.disjoint = std.disjoint
builtins.aliased = std.aliased
builtins.partition = std.partition
builtins.cross_product = std.cross_product
builtins.ptr = std.ptr
builtins.wild = std.wild

return builtins
