import "legion"

-- This tests a crash in conflicting reduction privileges.

local c = legionlib.c

task g2b(s : region(int)) : int
where reads(s) do
  return 5
end

task h2(s : region(int)) : int
where reduces +(s) do
  return 5
end

task h2b(s : region(int)) : int
where reduces *(s) do
  return 5
end

task with_partitions(r1 : region(int), p1_disjoint : partition(disjoint, r1),
                     n : int)
where reads(r1), writes(r1) do

  for i = 0, n do
    g2b(p1_disjoint[i])
  end

  for i = 0, n do
    h2b(p1_disjoint[i])
  end

  __demand(__parallel)
  for i = 0, n do
    h2(p1_disjoint[i])
  end
end
with_partitions:printpretty()

task main()
  var n = 1
  var r = region(int, n)
  var rc = c.legion_coloring_create()
  for i = 0, n do
    c.legion_coloring_ensure_color(rc, i)
  end
  var p_disjoint = partition(disjoint, r, rc)
  var r1 = p_disjoint[0]
  var p1_disjoint = partition(disjoint, r1, rc)
  c.legion_coloring_destroy(rc)

  with_partitions(r1, p1_disjoint, n)
end
legionlib.start(main)
