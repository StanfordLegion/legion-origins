#ifndef __CIRCUIT_H__
#define __CIRCUIT_H__

#ifdef __cplusplus
extern "C" {
#endif

void register_mappers();

#ifdef __cplusplus
}
#endif

#endif // __CIRCUIT_H__
