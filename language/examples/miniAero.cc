#include "miniAero.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstring>
#include <map>
#include <vector>

#include "default_mapper.h"

using namespace LegionRuntime::HighLevel;

///
/// Mapper
///

LegionRuntime::Logger::Category log_mapper("mapper");

class MiniAeroMapper : public DefaultMapper
{
public:
  MiniAeroMapper(Machine machine, HighLevelRuntime *rt, Processor local);
  virtual void select_task_options(Task *task);
  virtual void select_task_variant(Task *task);
  virtual bool map_task(Task *task);
  virtual bool map_inline(Inline *inline_operation);
  virtual void notify_mapping_failed(const Mappable *mappable);
  //virtual bool rank_copy_targets(const Mappable *mappable,
  //                               LogicalRegion rebuild_region,
  //                               const std::set<Memory> &current_instances,
  //                               bool complete,
  //                               size_t max_blocking_factor,
  //                               std::set<Memory> &to_reuse,
  //                               std::vector<Memory> &to_create,
  //                               bool &create_one,
  //                               size_t &blocking_factor);
  //virtual void rank_copy_sources(const Mappable *mappable,
  //                               const std::set<Memory> &current_instances,
  //                               Memory dst_mem,
  //                               std::vector<Memory> &chosen_order);
private:
  Color get_task_color_by_region(Task *task, const RegionRequirement &requirement);
  LogicalRegion get_root_region(LogicalRegion handle);
  LogicalRegion get_root_region(LogicalPartition handle);
private:
  std::map<Processor, Memory> all_sysmems;
};

MiniAeroMapper::MiniAeroMapper(Machine machine, HighLevelRuntime *rt, Processor local)
  : DefaultMapper(machine, rt, local)
{
  std::set<Processor> all_procs;
	machine.get_all_processors(all_procs);
  machine_interface.filter_processors(machine, Processor::LOC_PROC, all_procs);

  for (std::set<Processor>::iterator itr = all_procs.begin();
       itr != all_procs.end(); ++itr) {
    Memory sysmem = machine_interface.find_memory_kind(*itr, Memory::SYSTEM_MEM);
    all_sysmems[*itr] = sysmem;
  }
}

void MiniAeroMapper::select_task_options(Task *task)
{
  // Task options:
  task->inline_task = false;
  task->spawn_task = false;
  task->map_locally = true;
  task->profile_task = false;
}

void MiniAeroMapper::select_task_variant(Task *task)
{
  // Use the SOA variant for all tasks.
  // task->selected_variant = VARIANT_SOA;
  DefaultMapper::select_task_variant(task);

  std::vector<RegionRequirement> &regions = task->regions;
  for (std::vector<RegionRequirement>::iterator it = regions.begin();
        it != regions.end(); it++) {
    RegionRequirement &req = *it;

    // Select SOA layout for all regions.
    req.blocking_factor = req.max_blocking_factor;
  }
}

bool MiniAeroMapper::map_task(Task *task)
{
  Memory sysmem = all_sysmems[task->target_proc];
  std::vector<RegionRequirement> &regions = task->regions;
  for (std::vector<RegionRequirement>::iterator it = regions.begin();
        it != regions.end(); it++) {
    RegionRequirement &req = *it;

    // Region options:
    req.virtual_map = false;
    req.enable_WAR_optimization = false;
    req.reduction_list = false;

    // Place all regions in local system memory.
    req.target_ranking.push_back(sysmem);
    {
      LogicalRegion root;

      if (req.handle_type == SINGULAR || req.handle_type == REG_PROJECTION) {
        root = get_root_region(req.region);
      } else {
        assert(req.handle_type == PART_PROJECTION);
        root = get_root_region(req.partition);
      }

      const char *name_;
      runtime->retrieve_name(root, name_);
      assert(name_);
      std::string name(name_);

      int num_fields = 0;
      if (name == "cells") {
        num_fields = 67;
      } else if (name == "faces") {
        num_fields = 20;
      } else if (name == "blocks") {
        num_fields = 6;
      } else {
        assert(false);
      }

      const int base = 101;
      for (int i = base; i < base + num_fields; i++) {
        req.additional_fields.insert(i);
      }
    }
  }

  return false;
}

bool MiniAeroMapper::map_inline(Inline *inline_operation)
{
  Memory sysmem = all_sysmems[local_proc];
  RegionRequirement &req = inline_operation->requirement;

  // Region options:
  req.virtual_map = false;
  req.enable_WAR_optimization = false;
  req.reduction_list = false;
  req.blocking_factor = req.max_blocking_factor;

  // Place all regions in global memory.
  req.target_ranking.push_back(sysmem);

  log_mapper.debug(
    "inline mapping region (%d,%d,%d) target ranking front %d (size %lu)",
    req.region.get_index_space().get_id(),
    req.region.get_field_space().get_id(),
    req.region.get_tree_id(),
    req.target_ranking[0].id,
    req.target_ranking.size());

  return false;
}

void MiniAeroMapper::notify_mapping_failed(const Mappable *mappable)
{
  switch (mappable->get_mappable_kind()) {
  case Mappable::TASK_MAPPABLE:
    {
      log_mapper.warning("mapping failed on task");
      break;
    }
  case Mappable::COPY_MAPPABLE:
    {
      log_mapper.warning("mapping failed on copy");
      break;
    }
  case Mappable::INLINE_MAPPABLE:
    {
      Inline *_inline = mappable->as_mappable_inline();
      RegionRequirement &req = _inline->requirement;
      LogicalRegion region = req.region;
      log_mapper.warning(
        "mapping %s on inline region (%d,%d,%d) memory %d",
        (req.mapping_failed ? "failed" : "succeeded"),
        region.get_index_space().get_id(),
        region.get_field_space().get_id(),
        region.get_tree_id(),
        req.selected_memory.id);
      break;
    }
  case Mappable::ACQUIRE_MAPPABLE:
    {
      log_mapper.warning("mapping failed on acquire");
      break;
    }
  case Mappable::RELEASE_MAPPABLE:
    {
      log_mapper.warning("mapping failed on release");
      break;
    }
  }
  assert(0 && "mapping failed");
}

//bool MiniAeroMapper::rank_copy_targets(const Mappable *mappable,
//                                       LogicalRegion rebuild_region,
//                                       const std::set<Memory> &current_instances,
//                                       bool complete,
//                                       size_t max_blocking_factor,
//                                       std::set<Memory> &to_reuse,
//                                       std::vector<Memory> &to_create,
//                                       bool &create_one,
//                                       size_t &blocking_factor)
//{
//  DefaultMapper::rank_copy_targets(mappable, rebuild_region, current_instances,
//                                   complete, max_blocking_factor, to_reuse,
//                                   to_create, create_one, blocking_factor);
//  if (create_one) {
//    blocking_factor = max_blocking_factor;
//  }
//  return true;
//}
//
//void MiniAeroMapper::rank_copy_sources(const Mappable *mappable,
//                                       const std::set<Memory> &current_instances,
//                                       Memory dst_mem,
//                                       std::vector<Memory> &chosen_order)
//{
//  // Elliott: This is to fix a bug in the default mapper which throws
//  // an error with composite instances.
//
//  // Handle the simple case of having the destination
//  // memory in the set of instances
//  if (current_instances.find(dst_mem) != current_instances.end())
//  {
//    chosen_order.push_back(dst_mem);
//    return;
//  }
//
//  machine_interface.find_memory_stack(dst_mem,
//                                      chosen_order, true/*latency*/);
//  if (chosen_order.empty())
//  {
//    // This is the multi-hop copy because none
//    // of the memories had an affinity
//    // SJT: just send the first one
//    if(current_instances.size() > 0) {
//      chosen_order.push_back(*(current_instances.begin()));
//    } else {
//      // Elliott: This is a composite instance.
//      //assert(false);
//    }
//  }
//}

Color MiniAeroMapper::get_task_color_by_region(Task *task, const RegionRequirement &requirement)
{
  if (requirement.handle_type == SINGULAR) {
    return get_logical_region_color(requirement.region);
  }
  return 0;
}

LogicalRegion MiniAeroMapper::get_root_region(LogicalRegion handle)
{
  if (has_parent_logical_partition(handle)) {
    return get_root_region(get_parent_logical_partition(handle));
  }
  return handle;
}

LogicalRegion MiniAeroMapper::get_root_region(LogicalPartition handle)
{
  return get_root_region(get_parent_logical_region(handle));
}

static void create_mappers(Machine machine, HighLevelRuntime *runtime, const std::set<Processor> &local_procs)
{
  for (std::set<Processor>::const_iterator it = local_procs.begin();
        it != local_procs.end(); it++)
  {
    runtime->replace_default_mapper(new MiniAeroMapper(machine, runtime, *it), *it);
  }
}

void register_mappers()
{
  HighLevelRuntime::set_registration_callback(create_mappers);
}
