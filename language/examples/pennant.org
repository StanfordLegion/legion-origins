* Brodak
** Intel(R) Core(TM) i7-4770 CPU @ 3.40GHz
4 physical cores
8 logical cores
AVX2
GCC 4.8.2
** PENNANT v0.6 sedovbig
For the reference, mean across application run.

For the comparison, mean across first 50 timesteps.

|   | configuration                                          | time/step | speedup |
|---+--------------------------------------------------------+-----------+---------|
| # | *baseline* reference v0.6 -O3 -march=native            | 1.389e-01 |     1.0 |
| # | reference v0.6 -O3 -march=native -fopenmp nt=4         | 6.683e-02 |     2.1 |
| # | reference v0.6 -O3 -march=native -fopenmp nt=8         | 4.368e-02 |     3.2 |
| # | reference v0.6 -O3 -march=native -fopenmp MPI n=1 nt=8 | 4.460e-02 |     3.1 |
| # | reference v0.6 clang -O3 -march=native                 | 1.399e-01 |     1.0 |
| # | 2015-01-29 24b169d serial with_mapper                  | 6.930e-01 |   1/5.0 |
| # | 2015-01-29 24b169d serial with_mapper with_unmap       | 6.164e-01 |   1/4.4 |
| # | 2015-02-26 d32ee68 task_parallel with_mapper_pgroup    | 4.822e-01 |   1/3.5 |
| # | 2015-02-26 fe20d8d parallel(4) with_mapper_pgroup      | 2.800e-01 |   1/2.0 |
  #+TBLFM: $4='(if (<= $3 (* @2$3 1.05)) (format "%.1f" (/ @2$3 $3)) (format "1/%.1f" (/ $3 @2$3)));N
** PENNANT v0.7 sedovbig
For the reference, mean across application run.

For the comparison, mean across first 50 timesteps.

./legion.py examples/pennant.lg examples/pennant.tests/sedovbig50/sedovbig.pnt -npieces 1 -ll:cpu 1 -hl:sched 256 -level 5

|   | configuration                                                                            | time/step | speedup |
|---+------------------------------------------------------------------------------------------+-----------+---------|
| # | *baseline* reference v0.7 -O3 -march=native                                              | 1.377e-01 |    1.00 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=2                                           | 7.371e-02 |    1.87 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=3                                           | 5.378e-02 |    2.56 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=4                                           | 4.445e-02 |    3.10 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=8                                           | 4.301e-02 |    3.20 |
| # | 2015-01-29 24b169d serial with_mapper                                                    | 6.930e-01 |  1/5.03 |
| # | 2015-01-29 24b169d serial manual_unmap                                                   | 6.164e-01 |  1/4.48 |
| # | 2015-03-10 c5ac164 serial auto_unmap                                                     | 6.178e-01 |  1/4.49 |
| # | 2015-03-11 346ab07 shared -npieces 1 -ll:cpu 1 raw_pointers                              | 2.272e-01 |  1/1.65 |
| # | 2015-03-11 346ab07 shared -npieces 2 -ll:cpu 2 raw_pointers                              | 1.605e-01 |  1/1.17 |
| # | 2015-03-11 346ab07 shared -npieces 4 -ll:cpu 4 raw_pointers                              | 1.898e-01 |  1/1.38 |
| # | 2015-03-12 6035c44 general -npieces 1 -ll:cpu 1                                          | 2.030e-01 |  1/1.47 |
| # | 2015-03-12 122c962 general -npieces 1 -ll:cpu 1 fixed_auto_unmap                         | 1.758e-01 |  1/1.28 |
| # | 2015-03-12 122c962 general -npieces 2 -ll:cpu 2 fixed_auto_unmap                         | 1.165e-01 |    1.18 |
| # | 2015-03-12 122c962 general -npieces 4 -ll:cpu 4 fixed_auto_unmap                         | 1.212e-01 |    1.14 |
| # | 2015-03-16 04758ae general -npieces 1 -ll:cpu 1 leaf                                     | 1.620e-01 |  1/1.18 |
| # | 2015-03-16 04758ae general -npieces 2 -ll:cpu 2 leaf                                     | 1.031e-01 |    1.34 |
| # | 2015-03-16 04758ae general -npieces 4 -ll:cpu 4 leaf                                     | 1.042e-01 |    1.32 |
| # | 2015-03-16 9bd0f6d general -npieces 1 -ll:cpu 1 flat_qcs                                 | 1.612e-01 |  1/1.17 |
| # | 2015-03-16 9bd0f6d general -npieces 2 -ll:cpu 2 flat_qcs                                 | 1.014e-01 |    1.36 |
| # | 2015-03-16 9bd0f6d general -npieces 4 -ll:cpu 4 flat_qcs                                 | 1.027e-01 |    1.34 |
| # | 2015-03-16 9bd0f6d general -npieces 4 -ll:cpu 4 -hl:sched 128 flat_qcs                   | 9.752e-02 |    1.41 |
| # | 2015-03-17 d6f9329 general -npieces 1 -ll:cpu 1 -hl:sched 256 fuse_dt                    | 1.608e-01 |  1/1.17 |
| # | 2015-03-17 d6f9329 general -npieces 4 -ll:cpu 4 -hl:sched 256 fuse_dt                    | 1.131e-01 |    1.22 |
| # | 2015-03-17 56eae13 general -npieces 1 -ll:cpu 1 no_empty_virtual                         | 1.585e-01 |  1/1.15 |
| # | 2015-03-17 56eae13 general -npieces 1 -ll:cpu 1 -hl:sched 256 no_empty_virtual           | 1.574e-01 |  1/1.14 |
| # | 2015-03-17 56eae13 general -npieces 2 -ll:cpu 2 -hl:sched 256 no_empty_virtual           | 9.794e-02 |    1.41 |
| # | 2015-03-17 56eae13 general -npieces 4 -ll:cpu 4 -hl:sched 256 no_empty_virtual           | 9.284e-02 |    1.48 |
| # | 2015-03-17 56eae13 general -npieces 8 -ll:cpu 8 -hl:sched 256 no_empty_virtual           | 9.600e-02 |    1.43 |
| # | 2015-03-18 b5f6b0e general -npieces 1 -ll:cpu 1 -hl:sched 256 compress_bitmask           | 1.549e-01 |  1/1.12 |
| # | 2015-03-18 b5f6b0e general -npieces 2 -ll:cpu 2 -hl:sched 256 compress_bitmask           | 9.454e-02 |    1.46 |
| # | 2015-03-18 b5f6b0e general -npieces 3 -ll:cpu 3 -hl:sched 256 compress_bitmask           | 8.066e-02 |    1.71 |
| # | 2015-03-18 b5f6b0e general -npieces 4 -ll:cpu 4 -hl:sched 256 compress_bitmask           | 8.680e-02 |    1.59 |
| # | 2015-03-18 b5f6b0e general -npieces 8 -ll:cpu 8 -hl:sched 256 compress_bitmask           | 9.224e-02 |    1.49 |
| # | 2015-03-18 66dabb1 general -npieces 1 -ll:cpu 1 -hl:sched 256 compaction                 | 1.553e-01 |  1/1.13 |
| # | 2015-03-18 66dabb1 general -npieces 2 -ll:cpu 2 -hl:sched 256 compaction                 | 9.548e-02 |    1.44 |
| # | 2015-03-18 66dabb1 general -npieces 3 -ll:cpu 3 -hl:sched 256 compaction                 | 8.070e-02 |    1.71 |
| # | 2015-03-18 66dabb1 general -npieces 4 -ll:cpu 4 -hl:sched 256 compaction                 | 7.802e-02 |    1.76 |
| # | 2015-03-18 66dabb1 general -npieces 8 -ll:cpu 8 -hl:sched 256 compaction                 | 8.476e-02 |    1.62 |
| # | 2015-03-18 a5144f4 general -npieces 1 -ll:cpu 1 -hl:sched 256 opt_mapper                 | 1.581e-01 |  1/1.15 |
| # | 2015-03-18 a5144f4 general -npieces 2 -ll:cpu 2 -hl:sched 256 opt_mapper                 | 9.590e-02 |    1.44 |
| # | 2015-03-18 a5144f4 general -npieces 3 -ll:cpu 3 -hl:sched 256 opt_mapper                 | 8.082e-02 |    1.70 |
| # | 2015-03-18 a5144f4 general -npieces 4 -ll:cpu 4 -hl:sched 256 opt_mapper                 | 8.142e-02 |    1.69 |
| # | 2015-03-18 a5144f4 general -npieces 8 -ll:cpu 8 -hl:sched 256 opt_mapper                 | 8.460e-02 |    1.63 |
| # | 2015-04-02 general -npieces 1 -ll:cpu 1 -hl:sched 256 span=2048 strip=128                | 1.187e-01 |    1.16 |
| # | 2015-04-02 general -npieces 3 -ll:cpu 3 -hl:sched 256 span=2048 strip=128                | 4.943e-02 |    2.79 |
| # | 2015-04-10 a15a6ab general -npieces 1 -ll:cpu 1 -hl:sched 256 span=2048 strip=128 global | 1.179e-01 |    1.17 |
| # | 2015-04-10 a15a6ab general -npieces 1 -ll:cpu 1 -hl:sched 256 span=4096 strip=256 global | 1.158e-01 |    1.19 |
| # | 2015-04-10 a15a6ab general -npieces 1 -ll:cpu 1 -hl:sched 256 span=2048 strip=128 tls    | 1.158e-01 |    1.19 |
| # | 2015-04-10 a15a6ab general -npieces 1 -ll:cpu 1 -hl:sched 256 span=4096 strip=256 tls    | 1.142e-01 |    1.21 |
| # | 2015-04-10 a15a6ab general -npieces 3 -ll:cpu 3 -hl:sched 256 span=1024 strip=128 tls    | 4.998e-02 |    2.76 |
| # | 2015-04-10 a15a6ab general -npieces 3 -ll:cpu 3 -hl:sched 256 span=2048 strip=128 tls    | 4.936e-02 |    2.79 |
| # | 2015-04-10 a15a6ab general -npieces 3 -ll:cpu 3 -hl:sched 256 span=4096 strip=256 tls    | 4.829e-02 |    2.85 |
| # | 2015-04-10 a15a6ab general -npieces 8 -ll:cpu 8 -hl:sched 256 span=4096 strip=256 tls    | 4.679e-02 |    2.94 |
  #+TBLFM: $4='(if (<= $3 (* @2$3 1.05)) (format "%.2f" (/ @2$3 $3)) (format "1/%.2f" (/ $3 @2$3)));N
** PENNANT v0.7 sedovhuge10
(This problem is 4x bigger than sedovbig in each dimension.)

Mean accross application run (10 timesteps).

./legion.py examples/pennant.lg examples/pennant.tests/sedovhuge10/sedovhuge.pnt -npieces 1 -ll:cpu 1 -hl:sched 256 -ll:csize 8192 -level 5

|   | configuration                                                                                   |    time/step | speedup |
|---+-------------------------------------------------------------------------------------------------+--------------+---------|
| # | *baseline* reference v0.7 -O3 -march=native                                                     | 2.273543e+00 |    1.00 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=4                                                  | 1.088576e+00 |    2.09 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=8                                                  | 7.131058e-01 |    3.19 |
| # | reference v0.7 -O3 -march=native -fopenmp MPI n=1 nt=8                                          | 7.133575e-01 |    3.19 |
| # | 2015-03-16 fcb96f7 general -npieces 1 -ll:cpu 1                                                 | 2.643675e+00 |  1/1.16 |
| # | 2015-03-16 90f849a general -npieces 1 -ll:cpu 1 some_manual_fision_PRE                          | 2.588373e+00 |  1/1.14 |
| # | 2015-03-16 04758ae general -npieces 1 -ll:cpu 1 leaf                                            | 2.558665e+00 |  1/1.13 |
| # | 2015-03-17 56eae13 general -npieces 1 -ll:cpu 1 -hl:sched 256 flat_qcs fuse_dt no_empty_virtual | 2.553100e+00 |  1/1.12 |
| # | 2015-03-17 56eae13 general -npieces 2 -ll:cpu 2 -hl:sched 256 flat_qcs fuse_dt no_empty_virtual | 1.525523e+00 |    1.49 |
| # | 2015-03-17 56eae13 general -npieces 4 -ll:cpu 4 -hl:sched 256 flat_qcs fuse_dt no_empty_virtual | 1.304799e+00 |    1.74 |
| # | 2015-03-18 b5f6b0e general -npieces 1 -ll:cpu 1 -hl:sched 256 compress_bitmask                  | 2.536144e+00 |  1/1.12 |
| # | 2015-03-18 b5f6b0e general -npieces 2 -ll:cpu 2 -hl:sched 256 compress_bitmask                  | 1.501368e+00 |    1.51 |
| # | 2015-03-18 b5f6b0e general -npieces 3 -ll:cpu 3 -hl:sched 256 compress_bitmask                  | 1.268469e+00 |    1.79 |
| # | 2015-03-18 b5f6b0e general -npieces 4 -ll:cpu 4 -hl:sched 256 compress_bitmask                  | 1.257930e+00 |    1.81 |
| # | 2015-03-18 b5f6b0e general -npieces 8 -ll:cpu 8 -hl:sched 256 compress_bitmask                  | 1.297506e+00 |    1.75 |
| # | 2015-03-18 66dabb1 general -npieces 1 -ll:cpu 1 -hl:sched 256 compaction                        | 2.522558e+00 |  1/1.11 |
| # | 2015-03-18 66dabb1 general -npieces 2 -ll:cpu 2 -hl:sched 256 compaction                        | 1.492500e+00 |    1.52 |
| # | 2015-03-18 66dabb1 general -npieces 3 -ll:cpu 3 -hl:sched 256 compaction                        | 1.260823e+00 |    1.80 |
| # | 2015-03-18 66dabb1 general -npieces 4 -ll:cpu 4 -hl:sched 256 compaction                        | 1.187907e+00 |    1.91 |
| # | 2015-03-18 66dabb1 general -npieces 8 -ll:cpu 8 -hl:sched 256 compaction                        | 1.238999e+00 |    1.83 |
| # | 2015-03-24 35941dc general -npieces 1 -ll:cpu 1 -hl:sched 256 some_vectorization                | 2.474103e+00 |  1/1.09 |
| # | 2015-03-24 4191aca general -npieces 1 -ll:cpu 1 -hl:sched 256 some_deduplication                | 2.382405e+00 |  1/1.05 |
| # | 2015-03-25 6168c96 general -npieces 1 -ll:cpu 1 -hl:sched 256 non-composite less_branch         | 2.343684e+00 |  1/1.03 |
| # | 2015-03-26 8cfa20b general -npieces 1 -ll:cpu 1 -hl:sched 256 no_branch                         | 2.006002e+00 |    1.13 |
| # | 2015-03-26 8cfa20b general -npieces 2 -ll:cpu 2 -hl:sched 256 no_branch                         | 1.264393e+00 |    1.80 |
| # | 2015-03-26 8cfa20b general -npieces 3 -ll:cpu 3 -hl:sched 256 no_branch                         | 1.146561e+00 |    1.98 |
| # | 2015-03-26 8cfa20b general -npieces 4 -ll:cpu 4 -hl:sched 256 no_branch                         | 1.121394e+00 |    2.03 |
| # | 2015-03-27 65f2932 general -npieces 1 -ll:cpu 1 -hl:sched 256 manual_fusion                     | 1.769643e+00 |    1.28 |
| # | 2015-03-27 65f2932 general -npieces 2 -ll:cpu 2 -hl:sched 256 manual_fusion                     | 1.117740e+00 |    2.03 |
| # | 2015-03-27 65f2932 general -npieces 3 -ll:cpu 3 -hl:sched 256 manual_fusion                     | 9.900541e-01 |    2.30 |
| # | 2015-03-27 65f2932 general -npieces 4 -ll:cpu 4 -hl:sched 256 manual_fusion                     | 9.590668e-01 |    2.37 |
#+TBLFM: $4='(if (<= $3 (* @2$3 1.005)) (format "%.2f" (/ @2$3 $3)) (format "1/%.2f" (/ $3 @2$3)));N
* Sapling (Compute Nodes)
** Intel(R) Xeon(R) CPU X5680 @ 3.33GHz
** PENNANT v0.7 sedovbig50
When using MPI, use `--bind-to none` or you won't get all the cores.

./legion.py examples/pennant.lg examples/pennant.tests/sedovbig50/sedovbig.pnt -npieces 1 -ll:cpu 1 -ll:util 1 -hl:sched 256 -level 5
./legion.py examples/pennant.lg examples/pennant.tests/sedovbig50/sedovbig.pnt -npieces 10 -ll:cpu 10 -ll:util 4 -hl:sched 500 -level 5

Full command:
# compile on n0000
# then from head:
cd pennant
make -f Makefile.mpi+openmp
cd test/sedovbig
mpirun -n 4 -npernode 1 --bind-to none -H n0002,n0003,n0000,n0001 ../../build/pennant sedovbig.pnt

Reference results are somewhat noisy and are an average of three. For
some of these, it may be worth trying to run with numactl to lock down
the NUMA effects. Something like:

numactl -m 0 -N 0 -- ../../build/pennant sedovbig.pnt

|   | configuration                                                                               | time/step | speedup |
|---+---------------------------------------------------------------------------------------------+-----------+---------|
| # | *baseline* reference v0.7 -O3 -march=native                                                 | 2.383e-01 |    1.00 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=2                                              | 1.308e-01 |    1.82 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=3                                              | 8.965e-02 |    2.66 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=4                                              | 6.968e-02 |    3.42 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=6                                              | 5.020e-02 |    4.75 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=8                                              | 4.701e-02 |    5.07 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=12                                             | 4.486e-02 |    5.31 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=24                                             | 2.843e-02 |    8.38 |
| # | reference v0.7 -O3 -march=native -fopenmp MPI n=1 nt=24                                     | 3.116e-02 |    7.65 |
| # | reference v0.7 -O3 -march=native -fopenmp MPI n=2 nt=24                                     | 2.108e-02 |   11.30 |
| # | reference v0.7 -O3 -march=native -fopenmp MPI n=4 nt=24                                     | 1.379e-02 |   17.28 |
| # | 2015-03-18 62e0ba0 general -npieces 1 -ll:cpu 1                                             | 3.021e-01 |  1/1.27 |
| # | 2015-03-18 62e0ba0 general -npieces 2 -ll:cpu 2                                             | 1.851e-01 |    1.29 |
| # | 2015-03-18 62e0ba0 general -npieces 3 -ll:cpu 3                                             | 1.297e-01 |    1.84 |
| # | 2015-03-18 62e0ba0 general -npieces 4 -ll:cpu 4                                             | 1.012e-01 |    2.35 |
| # | 2015-03-18 62e0ba0 general -npieces 6 -ll:cpu 6                                             | 7.736e-02 |    3.08 |
| # | 2015-03-18 62e0ba0 general -npieces 8 -ll:cpu 8                                             | 7.580e-02 |    3.14 |
| # | 2015-03-18 62e0ba0 general -npieces 10 -ll:cpu 10                                           | 9.106e-02 |    2.62 |
| # | 2015-03-18 62e0ba0 general -npieces 12 -ll:cpu 12                                           | 1.221e-01 |    1.95 |
| # | 2015-03-18 62e0ba0 general -npieces 8 -ll:cpu 8 max_fields 64                               | 7.498e-02 |    3.18 |
| # | 2015-03-18 62e0ba0 general -npieces 8 -ll:cpu 8 -ll:util 2 max_fields 64                    | 6.820e-02 |    3.49 |
| # | 2015-03-18 62e0ba0 general -npieces 8 -ll:cpu 8 -ll:util 4 max_fields 64                    | 6.898e-02 |    3.45 |
| # | 2015-03-18 62e0ba0 general -npieces 10 -ll:cpu 10 max_fields 64                             | 9.202e-02 |    2.59 |
| # | 2015-03-18 62e0ba0 general -npieces 10 -ll:cpu 10 -ll:util 2 max_fields 64                  | 7.934e-02 |    3.00 |
| # | 2015-03-18 62e0ba0 general -npieces 10 -ll:cpu 10 -ll:util 3 max_fields 64                  | 7.198e-02 |    3.31 |
| # | 2015-03-18 62e0ba0 general -npieces 10 -ll:cpu 10 -ll:util 4 max_fields 64                  | 6.946e-02 |    3.43 |
| # | 2015-03-18 62e0ba0 general -npieces 8 -ll:cpu 8 -ll:util 2                                  | 6.736e-02 |    3.54 |
| # | 2015-03-18 62e0ba0 general -npieces 8 -ll:cpu 8 -ll:util 4                                  | 6.814e-02 |    3.50 |
| # | 2015-03-18 62e0ba0 general -npieces 10 -ll:cpu 10 -ll:util 4                                | 7.086e-02 |    3.36 |
| # | 2015-03-20 f2d8c34 general -npieces 1 -ll:cpu 1 futures                                     | 3.047e-01 |  1/1.28 |
| # | 2015-03-20 f2d8c34 general -npieces 8 -ll:cpu 8 -ll:util 2 futures                          | 6.516e-02 |    3.66 |
| # | 2015-03-20 f2d8c34 general -npieces 10 -ll:cpu 10 -ll:util 4 futures                        | 6.631e-02 |    3.59 |
| # | 2015-03-28 6878658 general -npieces 1 -ll:cpu 1                                             | 2.323e-01 |    1.03 |
| # | 2015-03-28 6878658 general -npieces 10 -ll:cpu 10 -ll:util 2                                | 5.383e-02 |    4.43 |
| # | 2015-04-01 7145ff4 general -npieces 1 -ll:cpu 1 no_autovector                               | 2.360e-01 |    1.01 |
| # | 2015-04-01 7145ff4 general -npieces 10 -ll:cpu 10 -ll:util 4 no_autovector                  | 6.163e-02 |    3.87 |
| # | 2015-04-01 7145ff4 general -npieces 1 -ll:cpu 1 no_autovector stripmine                     | 1.181e+00 |  1/4.96 |
| # | 2015-04-01 7145ff4 general -npieces 10 -ll:cpu 10 -ll:util 2 no_autovector stripmine        | 1.457e-01 |    1.64 |
| # | 2015-04-01 3311c6d general -npieces 1 -ll:cpu 1 tight_enum span=2048 strip=128              | 2.167e-01 |    1.10 |
| # | 2015-04-01 3311c6d general -npieces 10 -ll:cpu 10 -ll:util 2 tight_enum span=2048 strip=128 | 3.673e-02 |    6.49 |
  #+TBLFM: $4='(if (<= $3 (* @2$3 1.005)) (format "%.2f" (/ @2$3 $3)) (format "1/%.2f" (/ $3 @2$3)));N
** PENNANT v0.7 sedovlarge100
./legion.py examples/pennant.lg examples/pennant.tests/sedovlarge100/sedovlarge.pnt -npieces 1 -ll:cpu 1 -ll:util 1 -hl:sched 256 -ll:csize 4096 -level 5
./legion.py examples/pennant.lg examples/pennant.tests/sedovlarge100/sedovlarge.pnt -npieces 4 -ll:cpu 4 -ll:util 1 -hl:sched 256 -ll:csize 4096 -level 5
./legion.py examples/pennant.lg examples/pennant.tests/sedovlarge100/sedovlarge.pnt -npieces 10 -ll:cpu 10 -ll:util 2 -hl:sched 500 -ll:csize 4096 -level 5

./legion.py examples/pennant_stripmine.lg -faligned-instances 1 -fno-dynamic-branches 1 -fcached-iterators 1 examples/pennant.tests/sedovlarge100/sedovlarge.pnt -spansize 4096 -stripsize 256 -npieces 1 -ll:cpu 1 -ll:util 1 -hl:sched 256 -ll:csize 4096 -level 5
./legion.py examples/pennant_stripmine.lg -faligned-instances 1 -fno-dynamic-branches 1 -fcached-iterators 1 examples/pennant.tests/sedovlarge100/sedovlarge.pnt -spansize 4096 -stripsize 256 -npieces 4 -ll:cpu 4 -ll:util 1 -hl:sched 256 -ll:csize 4096 -level 5
./legion.py examples/pennant_stripmine.lg -faligned-instances 1 -fno-dynamic-branches 1 -fcached-iterators 1 examples/pennant.tests/sedovlarge100/sedovlarge.pnt -spansize 4096 -stripsize 256 -npieces 10 -ll:cpu 10 -ll:util 2 -hl:sched 500 -ll:csize 4096 -level 5

mpirun -n 1 -npernode 1 --bind-to none -H n0002,n0003,n0000,n0001 ./legion.py examples/pennant_stripmine.lg -faligned-instances 1 -fno-dynamic-branches 0 -fcached-iterators 1 examples/pennant.tests/sedovlarge100/sedovlarge.pnt -spansize 4096 -stripsize 256 -npieces 20 -ll:cpu 10 -ll:util 2 -hl:sched 500 -ll:csize 4096 -level 5

|   | configuration                                                                    |    time/step | speedup |
|---+----------------------------------------------------------------------------------+--------------+---------|
| # | *baseline* reference v0.7 -O3 -march=native                                      | 1.092378e+00 |    1.00 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=2                                   | 6.418938e-01 |    1.70 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=4                                   | 3.578309e-01 |    3.05 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=6                                   | 2.469529e-01 |    4.42 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=8                                   | 2.038611e-01 |    5.36 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=10                                  | 1.754607e-01 |    6.23 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=12                                  | 1.505885e-01 |    7.25 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=24                                  | 1.307251e-01 |    8.36 |
| # | 2015-04-02 69906a7 general -npieces 1 -ll:cpu 1 span=2048 strip=128              | 1.028674e+00 |    1.06 |
| # | 2015-04-02 69906a7 general -npieces 10 -ll:cpu 10 -ll:util 2 span=2048 strip=128 | 1.663044e-01 |    6.57 |
| # | 2015-04-02 a65433f general -npieces 10 -ll:cpu 10 -ll:util 2 span=4096 strip=256 | 1.598176e-01 |    6.84 |
| # | 2015-04-09 general -npieces 1 -ll:cpu 1 span=2048 strip=128 global_cache         | 9.794570e-01 |    1.12 |
| # | 2015-04-09 general -npieces 10 -ll:cpu 10 span=2048 strip=128 global_cache       | 1.563320e-01 |    6.99 |
  #+TBLFM: $4='(if (<= $3 (* @2$3 1.005)) (format "%.2f" (/ @2$3 $3)) (format "1/%.2f" (/ $3 @2$3)));N
** PENNANT v0.7 sedovhuge10
./legion.py examples/pennant.lg examples/pennant.tests/sedovhuge10/sedovhuge.pnt -npieces 1 -ll:cpu 1 -ll:util 1 -hl:sched 256 -ll:csize 8192 -level 5
./legion.py examples/pennant.lg examples/pennant.tests/sedovhuge10/sedovhuge.pnt -npieces 4 -ll:cpu 4 -ll:util 1 -hl:sched 256 -ll:csize 8192 -level 5
./legion.py examples/pennant.lg examples/pennant.tests/sedovhuge10/sedovhuge.pnt -npieces 10 -ll:cpu 10 -ll:util 2 -hl:sched 500 -ll:csize 8192 -level 5

|   | configuration                                                                             |    time/step | speedup |
|---+-------------------------------------------------------------------------------------------+--------------+---------|
| # | *baseline* reference v0.7 -O3 -march=native                                               | 3.637528e+00 |    1.00 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=2                                            | 1.968021e+00 |    1.85 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=3                                            | 1.374763e+00 |    2.65 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=4                                            | 1.053039e+00 |    3.45 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=6                                            | 7.262428e-01 |    5.01 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=8                                            | 5.872395e-01 |    6.19 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=12                                           | 5.150905e-01 |    7.06 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=24                                           | 4.259367e-01 |    8.54 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=4 disable_stripmining                        | 1.114323e+00 |    3.26 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=8 disable_stripmining                        | 9.468208e-01 |    3.84 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=12 disable_stripmining                       | 8.371834e-01 |    4.34 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=24 disable_stripmining                       | 5.761895e-01 |    6.31 |
| # | reference v0.7 -O3 -march=native -fopenmp MPI n=1 nt=8                                    |              |     inf |
| # | reference v0.7 -O3 -march=native -fopenmp MPI n=2 nt=8                                    |              |     inf |
| # | reference v0.7 -O3 -march=native -fopenmp MPI n=4 nt=8                                    |              |     inf |
| # | 2015-03-18 62e0ba0 general -npieces 1 -ll:cpu 1                                           | 4.625415e+00 |  1/1.27 |
| # | 2015-03-18 62e0ba0 general -npieces 8 -ll:cpu 8                                           | 9.955978e-01 |    3.65 |
| # | 2015-03-18 62e0ba0 general -npieces 8 -ll:cpu 8 -ll:util 2                                | 9.985524e-01 |    3.64 |
| # | 2015-03-18 62e0ba0 general -npieces 10 -ll:cpu 10 -ll:util 2                              | 9.156356e-01 |    3.97 |
| # | 2015-03-20 general -npieces 1 -ll:cpu 1                                                   | 4.632059e+00 |  1/1.27 |
| # | 2015-03-20 general -npieces 8 -ll:cpu 8 -ll:util 2 futures                                | 9.852121e-01 |    3.69 |
| # | 2015-03-20 general -npieces 10 -ll:cpu 10 -ll:util 2 futures                              | 9.090359e-01 |    4.00 |
| # | 2015-03-23 3e1b409 general -npieces 10 -ll:cpu 10 -ll:util 4 composite                    | 9.444751e-01 |    3.85 |
| # | 2015-03-23 3e1b409 general -npieces 16 -ll:cpu 10 -ll:util 4 composite                    | 9.694286e-01 |    3.75 |
| # | 2015-03-23 3e1b409 general -npieces 10 -ll:cpu 10 -ll:util 4 non-composite                | 9.084806e-01 |    4.00 |
| # | 2015-03-23 3e1b409 general -npieces 16 -ll:cpu 10 -ll:util 4 non-composite                | 9.681609e-01 |    3.76 |
| # | 2015-03-24 f407b77 general -npieces 1 -ll:cpu 1 vector unaligned                          | 4.316547e+00 |  1/1.19 |
| # | 2015-03-24 f407b77 general -npieces 1 -ll:cpu 1 vector aligned                            | 4.326962e+00 |  1/1.19 |
| # | 2015-03-24 f407b77 general -npieces 4 -ll:cpu 4 -ll:util 2 vector aligned non-composite   | 1.452134e+00 |    2.50 |
| # | 2015-03-24 f407b77 general -npieces 8 -ll:cpu 4 -ll:util 2 vector aligned non-composite   | 1.427462e+00 |    2.55 |
| # | 2015-03-24 f407b77 general -npieces 8 -ll:cpu 8 -ll:util 2 vector aligned non-composite   | 9.722994e-01 |    3.74 |
| # | 2015-03-24 f407b77 general -npieces 10 -ll:cpu 10 -ll:util 2 vector aligned non-composite | 8.974667e-01 |    4.05 |
| # | 2015-03-24 4e07548 general -npieces 1 -ll:cpu 1 non-composite less_branch                 | 4.249722e+00 |  1/1.17 |
| # | 2015-03-24 4e07548 general -npieces 10 -ll:cpu 10 -ll:util 2 non-composite less_branch    | 8.812257e-01 |    4.13 |
| # | 2015-03-25 ab011e1 general -npieces 1 -ll:cpu 1 composite warmup                          | 4.149753e+00 |  1/1.14 |
| # | 2015-03-25 ab011e1 general -npieces 10 -ll:cpu 10 -ll:util 2 composite warmup             | 8.668764e-01 |    4.20 |
| # | 2015-03-25 8ce8d79 general -npieces 1 -ll:cpu 1 compact_ghost                             | 4.132273e+00 |  1/1.14 |
| # | 2015-03-25 8ce8d79 general -npieces 10 -ll:cpu 10 -ll:util 2 compact_ghost                | 8.691091e-01 |    4.19 |
| # | 2015-03-26 8cfa20b general -npieces 1 -ll:cpu 1 no_branch                                 | 3.580052e+00 |    1.02 |
| # | 2015-03-26 8cfa20b general -npieces 4 -ll:cpu 4 no_branch                                 | 1.250384e+00 |    2.91 |
| # | 2015-03-26 8cfa20b general -npieces 10 -ll:cpu 10 -ll:util 2 no_branch                    | 8.328494e-01 |    4.37 |
| # | 2015-03-27 2a4d716 general -npieces 1 -ll:cpu 1 manual_fuse_init                          | 3.508154e+00 |    1.04 |
| # | 2015-03-27 2a4d716 general -npieces 10 -ll:cpu 10 -ll:util 2 manual_fuse_init             | 7.970551e-01 |    4.56 |
| # | 2015-03-27 9ce560c general -npieces 1 -ll:cpu 1 more_fuse                                 | 3.511326e+00 |    1.04 |
| # | 2015-03-27 9ce560c general -npieces 10 -ll:cpu 10 -ll:util 2 more_fuse                    | 7.786575e-01 |    4.67 |
| # | 2015-03-27 a90a4e9 general -npieces 1 -ll:cpu 1 fuse_pgas_tts                             | 3.432788e+00 |    1.06 |
| # | 2015-03-27 a90a4e9 general -npieces 10 -ll:cpu 10 -ll:util 2 fuse_pgas_tts                | 7.242919e-01 |    5.02 |
| # | 2015-03-27 867aa17 general -npieces 1 -ll:cpu 1 fuse_zwrate_ze_zr                         | 3.421164e+00 |    1.06 |
| # | 2015-03-27 867aa17 general -npieces 10 -ll:cpu 10 -ll:util 2 fuse_zwrate_ze_zr            | 7.139537e-01 |    5.09 |
| # | 2015-03-28 02d13ec general -npieces 1 -ll:cpu 1 stripsize=128                             | 3.176993e+00 |    1.14 |
| # | 2015-03-28 02d13ec general -npieces 10 -ll:cpu 10 -ll:util 2 stripsize=128                | 7.202340e-01 |    5.05 |
#+TBLFM: $4='(if (<= $3 (* @2$3 1.005)) (format "%.2f" (/ @2$3 $3)) (format "1/%.2f" (/ $3 @2$3)));N

*** Cumulative time for kernels
|   |                       | cum time (1) | cum time (10) | relative |
|---+-----------------------+--------------+---------------+----------|
| # | qcs_corner_divergence |      5124865 |       7259922 |     1.42 |
| # | qcs_qcn_force         |      1919239 |       3869678 |     2.02 |
| # | qcs_force             |      1179671 |       2690274 |     2.28 |
| # | calc_work             |      1034550 |       2445805 |     2.36 |
| # | calc_volumes          |       983964 |       2397670 |     2.44 |
| # | calc_volumes_full     |       980815 |       2219595 |     2.26 |
| # | calc_centers_full     |       637094 |       2011053 |     3.16 |
| # | sum_point_force       |       602378 |       1951535 |     3.24 |
| # | calc_force_tts        |       695737 |       1962560 |     2.82 |
| # | calc_centers          |       579655 |       1794943 |     3.10 |
| # | calc_surface_vecs     |       385844 |       1853461 |     4.80 |
| # | init_step_zones       |       326474 |       1489206 |     4.56 |
#+TBLFM: $5='(format "%.2f" (/ (* 1.0 $4) $3));N

*** NUMA comparison
numactl -m 0 -N 0 -- ./legion.py examples/pennant.lg examples/pennant.tests/sedovhuge10/sedovhuge.pnt -npieces 2 -ll:cpu 2 -ll:util 1 -hl:sched 256 -ll:csize 8192 -level 5
numactl -m 0 -N 0 -- ./legion.py examples/pennant.lg examples/pennant.tests/sedovhuge10/sedovhuge.pnt -npieces 4 -ll:cpu 4 -ll:util 1 -hl:sched 256 -ll:csize 8192 -level 5

OMP_NUM_THREADS=2 numactl -m 0 -N 0 -- ../../build/pennant sedovhuge.pnt
OMP_NUM_THREADS=4 numactl -m 0 -N 0 -- ../../build/pennant sedovhuge.pnt

|   | configuration                                           |    time/step | speedup |
|---+---------------------------------------------------------+--------------+---------|
| # | *baseline* reference v0.7 -O3 -march=native             | 3.637528e+00 |    1.00 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=2          | 1.968021e+00 |    1.85 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=3          | 1.367014e+00 |    2.66 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=4          | 1.053039e+00 |    3.45 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=6          | 7.504991e-01 |    4.85 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=2 numactl  | 1.922032e+00 |    1.89 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=3 numactl  | 1.352326e+00 |    2.69 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=4 numactl  | 1.074079e+00 |    3.39 |
| # | reference v0.7 -O3 -march=native -fopenmp nt=6 numactl  | 8.176867e-01 |    4.45 |
| # | 2015-03-30 b6363eb general -npieces 1 -ll:cpu 1         | 3.219461e+00 |    1.13 |
| # | 2015-03-30 b6363eb general -npieces 2 -ll:cpu 2         | 1.813508e+00 |    2.01 |
| # | 2015-03-30 b6363eb general -npieces 3 -ll:cpu 3         | 1.278919e+00 |    2.84 |
| # | 2015-03-30 b6363eb general -npieces 4 -ll:cpu 4         | 1.077440e+00 |    3.38 |
| # | 2015-03-30 b6363eb general -npieces 6 -ll:cpu 6         | 8.726286e-01 |    4.17 |
| # | 2015-03-30 b6363eb general -npieces 2 -ll:cpu 2 numactl | 1.786579e+01 |    2.01 |
| # | 2015-03-30 b6363eb general -npieces 3 -ll:cpu 3 numactl | 1.372710e+00 |    2.65 |
| # | 2015-03-30 b6363eb general -npieces 4 -ll:cpu 4 numactl | 1.216359e+00 |    2.99 |
#+TBLFM: $4='(if (<= $3 (* @2$3 1.005)) (format "%.2f" (/ @2$3 $3)) (format "1/%.2f" (/ $3 @2$3)));N

* Certainty
** GCC 4.9
** Intel 15
** PENNANT v0.7 sedovhuge10
qsub -I
./legion.py examples/pennant.lg examples/pennant.tests/sedovhuge10/sedovhuge.pnt -npieces 1 -ll:cpu 1 -ll:util 1 -hl:sched 256 -ll:csize 8192 -level 5
./legion.py examples/pennant.lg examples/pennant.tests/sedovhuge10/sedovhuge.pnt -npieces 4 -ll:cpu 4 -ll:util 1 -hl:sched 256 -ll:csize 8192 -level 5
./legion.py examples/pennant.lg examples/pennant.tests/sedovhuge10/sedovhuge.pnt -npieces 8 -ll:cpu 8 -ll:util 2 -hl:sched 500 -ll:csize 8192 -level 5
./legion.py examples/pennant.lg examples/pennant.tests/sedovhuge10/sedovhuge.pnt -npieces 10 -ll:cpu 10 -ll:util 2 -hl:sched 500 -ll:csize 8192 -level 5

|   | configuration                                                |    time/step | speedup |
|---+--------------------------------------------------------------+--------------+---------|
| # | *baseline* reference v0.7 icpc -O3 -march=native             | 3.311145e+00 |    1.00 |
| # | reference v0.7 icpc -O3 -march=native -fopenmp nt=4          | 8.200583e-01 |    4.04 |
| # | reference v0.7 icpc -O3 -march=native -fopenmp nt=8          | 5.382958e-01 |    6.15 |
| # | reference v0.7 icpc -O3 -march=native -fopenmp nt=12         | 5.076317e-01 |    6.52 |
| # | reference v0.7 icpc -O3 -march=native -fopenmp nt=24         | 5.129245e-01 |    6.46 |
| # | reference v0.7 g++ -O3 -march=native                         | 4.222926e+00 |  1/1.28 |
| # | reference v0.7 g++ -O3 -march=native -fopenmp nt=4           | 1.134901e+00 |    2.92 |
| # | reference v0.7 g++ -O3 -march=native -fopenmp nt=8           | 6.462710e-01 |    5.12 |
| # | reference v0.7 g++ -O3 -march=native -fopenmp nt=12          | 5.042937e-01 |    6.57 |
| # | reference v0.7 g++ -O3 -march=native -fopenmp nt=24          | 4.873324e-01 |    6.79 |
| # | 2015-03-25 8cfa20b general -npieces 1 -ll:cpu 1              | 3.986338e+00 |  1/1.20 |
| # | 2015-03-25 8cfa20b general -npieces 4 -ll:cpu 4              | 1.352805e+00 |    2.45 |
| # | 2015-03-25 8cfa20b general -npieces 10 -ll:cpu 8 -ll:util 2  | 9.668833e-01 |    3.42 |
| # | 2015-03-25 8cfa20b general -npieces 10 -ll:cpu 10 -ll:util 2 | 9.237593e-01 |    3.58 |
#+TBLFM: $4='(if (<= $3 (* @2$3 1.005)) (format "%.2f" (/ @2$3 $3)) (format "1/%.2f" (/ $3 @2$3)));N
* Darwin cn62
** Intel(R) Xeon(R) CPU E5-2698 v3 @ 2.30GHz
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                32
On-line CPU(s) list:   0-31
Thread(s) per core:    1
Core(s) per socket:    16
Socket(s):             2
NUMA node(s):          4
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 63
Stepping:              2
CPU MHz:               2299.873
BogoMIPS:              4599.34
Virtualization:        VT-x
L1d cache:             32K
L1i cache:             32K
L2 cache:              256K
L3 cache:              20480K
NUMA node0 CPU(s):     0-7
NUMA node1 CPU(s):     8-15
NUMA node2 CPU(s):     16-23
NUMA node3 CPU(s):     24-31
** GCC 4.9.1
** PENNANT v0.7 sedovlarge10
./legion.py examples/pennant_stripmine.lg -faligned-instances 1 -fno-dynamic-branches 1 -fcached-iterators 1 examples/pennant.tests/sedovlarge10/sedovlarge.pnt -spansize 4096 -stripsize 256 -npieces 1 -ll:cpu 1 -ll:util 1 -hl:sched 256 -ll:csize 4096 -level 5
./legion.py examples/pennant_stripmine.lg -faligned-instances 1 -fno-dynamic-branches 1 -fcached-iterators 1 examples/pennant.tests/sedovlarge10/sedovlarge.pnt -spansize 4096 -stripsize 256 -npieces 4 -ll:cpu 4 -ll:util 1 -hl:sched 256 -ll:csize 4096 -level 5
./legion.py examples/pennant_stripmine.lg -faligned-instances 1 -fno-dynamic-branches 1 -fcached-iterators 1 examples/pennant.tests/sedovlarge10/sedovlarge.pnt -spansize 4096 -stripsize 256 -npieces 12 -ll:cpu 12 -ll:util 2 -hl:sched 500 -ll:csize 4096 -level 5

GASNET_SPAWNFN='C' GASNET_CSPAWN_CMD='srun -n %N -N %N ./legion.py %C' ./legion.py examples/circuit.lg 1

|   | configuration                                                            |    time/step | speedup |
|---+--------------------------------------------------------------------------+--------------+---------|
| # | *baseline* reference v0.7 g++ -O3                                        | 8.911549e-01 |    1.00 |
| # | reference v0.7 g++ -O3 -fopenmp nt=2                                     | 4.514373e-01 |    1.97 |
| # | reference v0.7 g++ -O3 -fopenmp nt=4                                     | 2.289532e-01 |    3.89 |
| # | reference v0.7 g++ -O3 -fopenmp nt=8                                     | 1.198897e-01 |    7.43 |
| # | reference v0.7 g++ -O3 -fopenmp nt=12                                    | 8.293881e-02 |   10.74 |
| # | reference v0.7 g++ -O3 -fopenmp nt=16                                    | 6.577320e-02 |   13.55 |
| # | reference v0.7 g++ -O3 -fopenmp nt=32                                    | 4.572060e-02 |   19.49 |
| # | 2015-04-09 general -npieces 1 -ll:cpu 1 span=4096 strip=256              | 7.558481e-01 |    1.18 |
| # | 2015-04-09 general -npieces 12 -ll:cpu 12 -ll:util 2 span=4096 strip=256 | 9.439370e-02 |    9.44 |
| # | 2015-04-09 general -npieces 16 -ll:cpu 16 -ll:util 2 span=4096 strip=256 | 6.925080e-02 |   12.87 |
  #+TBLFM: $4='(if (<= $3 (* @2$3 1.005)) (format "%.2f" (/ @2$3 $3)) (format "1/%.2f" (/ $3 @2$3)));N
