#!/bin/bash
echo "Running Ramp test."
../../../legion.py ../../miniAero_fused.lg -problem_type 2 -length_x 2 -length_y 2 -length_z 1 -ramp 30 -mesh 64x32x2 -time_steps 400 -dt 1e-5 -output_frequency 401 -level 3 -second_order $@ 2>&1 &> run.out
#sort -o results -n -k1,1 -k2,2 -k3,3 results
#../tools/numeric_text_diff results results.gold > diff.txt
#ESTATUS=$?
#rm -f results gradients limiters
#exit $ESTATUS
