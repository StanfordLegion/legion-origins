#!/bin/bash
echo "Running 3D sod test."
../../../legion.py ../../miniAero_fused.lg -problem_type 0 -length_x .3048 -length_y 1 -length_z 1 -ramp 0 -mesh 128x4x4 -time_steps 100 -dt 2e-6 -output_frequency 101 -level 3 $@ &> run.out
#sort -o results -n -k1,1 -k2,2 -k3,3 results
#../tools/numeric_text_diff results results.gold > diff.txt
#ESTATUS=$?
#rm -f results gradients limiters
#exit $ESTATUS
