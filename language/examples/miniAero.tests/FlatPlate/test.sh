#!/bin/bash
echo "Running Flat Plate test."
../../../legion.py ../../miniAero_fused.lg -problem_type 1 -length_x 2 -length_y 0.002 -length_z 1 -ramp 0 -mesh 16x32x2 -time_steps 400 -dt 3e-8 -output_frequency 401 -level 3 -second_order -viscous $@ &> run.out
#sort -o results -n -k1,1 -k2,2 -k3,3 results
#../tools/numeric_text_diff results results.gold > diff.txt
#ESTATUS=$?
#rm -f results gradients limiters
#exit $ESTATUS
