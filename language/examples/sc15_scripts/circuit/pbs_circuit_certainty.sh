#!/bin/bash -l
#PBS -l nodes=16:ppn=24
#PBS -l walltime=4:00:00

export REGENT_HOME="$HOME/legion/language"
CONDUIT=ibv "$REGENT_HOME/install.py" --general --gasnet
python -u "$REGENT_HOME/examples/sc15_scripts/circuit/run_circuit_torque.py"
