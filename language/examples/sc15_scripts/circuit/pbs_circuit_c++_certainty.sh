#!/bin/bash -l
#PBS -l nodes=16:ppn=24
#PBS -l walltime=4:00:00

export LG_RT_DIR="$HOME/legion/runtime"
export REGENT_HOME="$HOME/legion/language"
export NODE_FILE="$PBS_NODEFILE"

pushd "$LG_RT_DIR/../examples/full_circuit" &&
make CONDUIT=ibv USE_CUDA=0 USE_GASNET=1 DEBUG=0 SHARED_LOWLEVEL=0 clean &&
make CONDUIT=ibv USE_CUDA=0 USE_GASNET=1 DEBUG=0 SHARED_LOWLEVEL=0 &&
popd

"$REGENT_HOME/examples/sc15_scripts/circuit/run_circuit_multinode_c++.sh"
