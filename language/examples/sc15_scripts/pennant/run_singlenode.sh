#!/bin/bash

if [ ! -d "$REGENT_HOME" ]; then
  echo "REGENT_HOME variable must be set properly"
  exit -1
fi

if [ -z "$PBS_NODEFILE" ]; then
  echo "PBS_NODEFILE variable must be set properly"
  exit -1
fi

LEGION_OPTS="examples/pennant.tests/sedovlarge10/sedovlarge.pnt -faligned-instances 1 -fno-dynamic-branches 1 -fcached-iterators 1 -spansize 4096 -stripsize 256 -hl:sched 256 -ll:csize 4096 -level 5"

function run_pennant {
  local n=$1
  local cpu=$2
  local vectorize=$3
  local future=$4
  local inline=$5
  local leaf=$6
  local index=$7
  local nodes=$8

  echo "##### num nodes: $n (@ $nodes), num cpus: $cpu, optimizations: I$index F$future N$inline L$leaf V$vectorize #####"
  local LOG_FILE="node_$n""_cpu_$cpu""_I$index""_F$future""_N$inline""_L$leaf""_V$vectorize"
  date >> "$LOG_FILE"
  USE_MPIRUN=1 MPIRUN_FLAGS="-n $n -npernode 1 --bind-to none -H $nodes" "$REGENT_HOME/legion.py" "$REGENT_HOME/examples/pennant_stripmine.lg" $LEGION_OPTS \
    -npieces "$cpu" -ll:cpu "$cpu" -ll:util 2 -findex-launches "$index" -ffutures "$future" -finlines "$inline" -fleaf "$leaf" -fvectorize "$vectorize" &>> $LOG_FILE &
}

NODES="`uniq $PBS_NODEFILE`"
NUM_NODES="`echo "$NODES" | wc -l`"

node_list=""

function get_node_list {
  local n=$1
  local offset=$2

  node_list=""

  for i in `seq 1 $n`
  do
    pos=$(((offset + i - 1) % NUM_NODES + 1))
    node_list="$node_list,`echo "$NODES" | head -n $pos | tail -n 1`"
  done

  node_list="`echo $node_list | sed s/"^,"//g`"
}

for n in 1
do

  if [ $n -eq 1 ]; then
    cpus="1 2 4 6 8 10 12"
  else
    cpus="10"
  fi

  for cpu in $cpus
  do
    for i in $(seq 1 5)
    do
      remaining_nodes=$NUM_NODES
      offset=0
      #vectorize future inline leaf index
      for config in 2#11111 2#01111 2#10111 2#11011 2#11101 2#11110 2#11010 2#11100 2#00000
      do
        get_node_list "$n" "$offset"
        remaining_nodes=$((remaining_nodes - n))
        offset=$(((offset + n) % NUM_NODES))

        index=$((config >> 0 & 1))
        leaf=$((config >> 1 & 1))
        inline=$((config >> 2 & 1))
        future=$((config >> 3 & 1))
        vectorize=$((config >> 4 & 1))

        waited=false
        run_pennant $n $cpu $vectorize $future $inline $leaf $index $node_list
        if [ $remaining_nodes -eq 0 ]; then
          waited=true
          wait
          remaining_nodes=$NUM_NODES
          offset=0
        fi
      done
      if [ ! $waited ]; then
        wait
        waited=true
      fi
    done
  done

done
