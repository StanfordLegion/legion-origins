#!/usr/bin/env bash

if [ ! -d "$REGENT_HOME" ]; then
  echo "REGENT_HOME variable must be set properly"
  exit -1
fi

if [ -z "$NODE_FILE" ]; then
  echo "NODE_FILE variable must be set properly"
  exit -1
fi

LEGION_OPTS="$REGENT_HOME/examples/pennant.tests/sedovbig3x30/sedovbig.pnt"

function run_circuit {
  local n=$1
  local cpu=$2
  local nodes=$3

  echo "##### num nodes: $n (@ $nodes), num cpus: $cpu #####"
  local LOG_FILE="node_$n""_cpu_$cpu""_cpp"
  date >> "$LOG_FILE"
  mpirun -n "$n" -npernode 1 --bind-to none -H "$nodes" -x "OMP_NUM_THREADS=$cpu" $PENNANT_DIR/build/pennant $LEGION_OPTS &>> $LOG_FILE
}

NODES="`uniq $NODE_FILE`"
NUM_NODES="`echo "$NODES" | wc -l`"

node_list=""

function get_node_list {
  local n=$1
  local offset=$2

  node_list=""

  for i in `seq 1 $n`
  do
    pos=$(((offset + i - 1) % NUM_NODES + 1))
    node_list="$node_list,`echo "$NODES" | head -n $pos | tail -n 1`"
  done

  node_list="`echo $node_list | sed s/"^,"//g`"
}

n=1
while [ "$n" -le "1" ];
do

  if [ $n -eq 1 ]; then
    cpus="2 4 6 8 10 12 24"
  else
    cpus="8 10 12 24"
  fi

  for cpu in $cpus
  do
    for i in $(seq 1 5)
    do
      get_node_list "$n" 0
      run_circuit "$n" "$cpu" "$node_list"
    done
  done
  n=$((n * 2))
done
