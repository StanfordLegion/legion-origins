#!/bin/bash -l
#PBS -l nodes=16:ppn=24
#PBS -l walltime=24:00:00

export REGENT_HOME="$HOME/legion/language"
CONDUIT=ibv "$REGENT_HOME/install.py" --general --gasnet
python -u "$REGENT_HOME/examples/sc15_scripts/pennant/run_multinode.py"
