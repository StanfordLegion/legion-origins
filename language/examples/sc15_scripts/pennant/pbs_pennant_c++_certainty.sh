#!/bin/bash -l
#PBS -l nodes=16:ppn=24
#PBS -l walltime=4:00:00

export PENNANT_DIR="$HOME/pennant"
export REGENT_HOME="$HOME/legion/language"
export NODE_FILE="$PBS_NODEFILE"

pushd "$PENNANT_DIR" &&
make -f Makefile.serial-gcc clean &&
make -f Makefile.serial-gcc -j &&
popd

"$REGENT_HOME/examples/sc15_scripts/pennant/run_pennant_serial_c++.sh"

pushd "$PENNANT_DIR" &&
make -f Makefile.openmp-gcc clean &&
make -f Makefile.openmp-gcc -j &&
popd

"$REGENT_HOME/examples/sc15_scripts/pennant/run_pennant_singlenode_c++.sh"

pushd "$PENNANT_DIR" &&
make -f Makefile.mpi+openmp-gcc clean &&
make -f Makefile.mpi+openmp-gcc -j &&
popd

"$REGENT_HOME/examples/sc15_scripts/pennant/run_pennant_multinode_c++.sh"
