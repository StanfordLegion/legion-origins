#!/usr/bin/env python

from __future__ import print_function
import itertools, os, subprocess, sys, time

def get_regent_home():
    if 'REGENT_HOME' not in os.environ:
        raise Exception('REGENT_HOME is not set')
    return os.environ['REGENT_HOME']

def get_nodefile():
    if 'PBS_NODEFILE' not in os.environ:
        raise Exception('PBS_NODEFILE is not set')
    return os.environ['PBS_NODEFILE']

def get_run_params(**options):
    regent_home = get_regent_home()
    return [
        # application flags
        os.path.join(regent_home, 'legion.py'),
        os.path.join(regent_home, 'examples', 'pennant_stripmine.lg'),
        os.path.join(regent_home, 'examples', 'pennant.tests', 'sedovbig3x30', 'sedovbig.pnt'),
        '-npieces', str(options['nodecount'] * options['cores']),
        '-spansize', '4096', '-stripsize', '256',

        # runtime flags
        '-hl:sched', '256',
        '-ll:cpu', str(options['cores']), '-ll:util', '2', '-ll:dma', '2',
        '-ll:gsize', '0', '-ll:csize', '16384', '-ll:rsize', '0',
        '-level', '5',

        # optimization flags
        '-findex-launches', str(options['index']),
        '-ffutures', str(options['futures']),
        '-finlines', str(options['inline']),
        '-fleaf', str(options['leaf']),
        '-fvectorize', str(options['vectorize']),

        # other optimizations
        '-faligned-instances', '0',
        '-fno-dynamic-branches', ('1' if options['nodecount'] <= 1 else '0'),
        '-fcached-iterators', '1',
    ]

def get_optimizations():
    return ['index', 'futures', 'inline', 'leaf', 'vectorize']

def get_reps():
    return 5

def get_node_list():
    with open(get_nodefile(), 'rb') as f:
        return frozenset(f.read().strip().split())

def get_max_cores():
    with open(get_nodefile(), 'rb') as f:
        cores = f.read().strip().split()
        if len(cores) > 0:
            return sum(1 for core in cores if core == cores[0])
        return 0

def get_options_product(nodelist, cores, optimizations):
    single_nodes = [1]
    multi_nodes = itertools.takewhile(
        lambda x: x <= len(nodelist),
        (2**x for x in itertools.count(1)))
    single_node_cores = [1] + range(2, cores/2 + 2, 2)
    multi_node_cores = [max(1, cores/2 - 4), max(1, cores/2 - 2), max(1, cores/2)]
    optimization_flags = (
        [[0 for optimization in optimizations],
         [1 for optimization in optimizations]] +
        [[0 if optimization in knockout else 1
          for optimization in optimizations]
         for knockout in itertools.combinations(optimizations, 1)] +
        [[0 if optimization in knockout else 1
          for optimization in optimizations]
         for knockout in itertools.combinations(optimizations, 2)])
    names = ['nodecount', 'cores'] + optimizations
    options = itertools.chain(
            itertools.product(
                single_nodes,
                single_node_cores,
                optimization_flags),
            itertools.product(
                multi_nodes,
                multi_node_cores,
                optimization_flags))
    def comb(x):
        return dict(itertools.izip(names, x[:2] + tuple(x[2])))
    return itertools.imap(comb, options)

def get_log_filename(**options):
    return 'node_%s_cpu_%s_I%s_F%s_N%s_L%s_V%s_rep_%s' % (
        options['nodecount'], options['cores'], options['index'],
        options['futures'], options['inline'], options['leaf'],
        options['vectorize'], options['rep'])

def wait_for_job(nodelist_free, jobs):
    while True:
        completed = set()
        for i in xrange(len(jobs)):
            job = jobs[i]
            if job['process'].poll() is not None:
                nodelist_free.update(job['nodes'])
                completed.add(i)
        if len(completed) > 0:
            for i in reversed(sorted(completed)):
                jobs.pop(i)
            return
        time.sleep(10)

def get_free_nodes(nodelist, nodelist_free, count, jobs):
    nodes = set()
    for i in xrange(count):
        if len(nodelist_free) <= 0:
            wait_for_job(nodelist_free, jobs)
        nodes.add(nodelist_free.pop())
    return nodes

def run(**options):
    cmd = get_run_params(**options)
    env = dict(os.environ.items() + [
        ('USE_MPIRUN', '1'),
        ('MPIRUN_FLAGS', '-n %s -npernode 1 --bind-to none -H %s' % (
            options['nodecount'],
            ','.join(options['nodes']))),
    ])
    log_filename = get_log_filename(**options)
    print('##### num nodes: %s (@ %s), num cpus: %s, optimizations: I%s F%s N%s L%s V%s #####' % (
        options['nodecount'], options['nodes'], options['cores'],
        options['index'], options['futures'], options['inline'],
        options['leaf'], options['vectorize']))
    with open(log_filename, 'wb') as f:
        return subprocess.Popen(cmd, env = env, stdout = f, stderr = subprocess.STDOUT)

def run_experiment(nodelist, cores, options, reps):
    jobs = []
    nodelist_free = set(nodelist)
    for option in options:
        for rep in xrange(reps):
            nodes = get_free_nodes(
                nodelist, nodelist_free, option['nodecount'], jobs)
            jobs.append({
                'process': run(nodes = nodes, rep = rep, **option),
                'nodes': nodes,
            })

if __name__ == '__main__':
    nodelist = get_node_list()
    cores = get_max_cores()
    optimizations = get_optimizations()
    options = get_options_product(nodelist, cores, optimizations)
    reps = get_reps()
    run_experiment(nodelist, cores, options, reps)
