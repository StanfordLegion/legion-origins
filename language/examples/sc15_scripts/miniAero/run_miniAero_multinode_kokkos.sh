#!/usr/bin/env bash

if [ -z "$MINIAERO_BIN" ]; then
  echo "MINIAERO_BIN variable must be set properly"
  exit -1
fi

if [ -z "$NODE_FILE" ]; then
  echo "NODE_FILE variable must be set properly"
  exit -1
fi

function run_miniAero {
  local n=$1
  local cpu=$2
  local nodes=$3

  rm -rf miniaero.inp
  cat miniaero.inp.template | sed s/"<<threads>>"/"$cpu"/g > miniaero.inp

  echo "##### num nodes: $n (@ $nodes), num cpus: $cpu #####"
  local LOG_FILE="miniAero_node_$n""_cpu_$cpu""_kokkos"
  date >> "$LOG_FILE"
  mpirun --bind-to none -n "$n" -H "$nodes" "$MINIAERO_BIN" &>> "$LOG_FILE"
}

NODES="`uniq $NODE_FILE`"
NUM_NODES="`echo "$NODES" | wc -l`"

node_list=""

function get_node_list {
  local n=$1
  local offset=$2

  node_list=""

  for i in `seq 1 $n`
  do
    pos=$(((offset + i - 1) % NUM_NODES + 1))
    node_list="$node_list,`echo "$NODES" | head -n $pos | tail -n 1`"
  done

  node_list="`echo $node_list | sed s/"^,"//g`"
}

n=1
while [ "$n" -le "$NUM_NODES" ];
do

  if [ $n -eq 1 ]; then
    cpus="1 2 4 6 8 10 12"
  else
    cpus="8 10 12"
  fi

  for cpu in $cpus
  do
    for i in $(seq 1 5)
    do
      get_node_list "$n" 0
      run_miniAero "$n" "$cpu" "$node_list"
    done
  done
  n=$((n * 2))
done
