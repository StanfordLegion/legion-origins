#!/bin/bash -l
#PBS -l nodes=32:ppn=24
#PBS -l walltime=24:00:00

export REGENT_HOME="$HOME/legion/language"
CC_FLAGS="-DMAX_FIELDS=128" CONDUIT=ibv "$REGENT_HOME/install.py" --general --gasnet
python -u "$REGENT_HOME/examples/sc15_scripts/miniAero/run_multinode.py"
