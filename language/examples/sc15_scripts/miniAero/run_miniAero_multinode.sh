#!/usr/bin/env bash

if [ ! -d "$REGENT_HOME" ]; then
  echo "REGENT_HOME variable must be set properly"
  exit -1
fi

if [ -z "$NODE_FILE" ]; then
  echo "NODE_FILE variable must be set properly"
  exit -1
fi

LEGION_OPTS="-mesh 512x1024x8 -problem_type 1 -ramp 0 -length_x 2.0 -length_y 0.002 -length_z 1.0 -viscous -second_order -dt 1e-08 -time_steps 3 -output_frequency 11 -ll:rsize 4096 -ll:gsize 0 -ll:csize 16384 -ll:util 2 -level 5"

function run_miniAero {
  local n=$1
  local cpu=$2
  local index=$3
  local future=$4
  local inline=$5
  local leaf=$6
  local vectorize=$7
  local nodes=$8

  local num_cpus=$((n * cpu))
  local num_blocks=1
  while (( $num_blocks < $num_cpus ));
  do
    num_blocks=$((num_blocks * 2))
  done

  echo "##### num nodes: $n (@ $nodes), num cpus: $cpu, optimizations: I$index F$future N$inline L$leaf V$vectorize #####"
  local LOG_FILE="miniAero_node_$n""_cpu_$cpu""_I$index""_F$future""_N$inline""_L$leaf""_V$vectorize"
  date >> "$LOG_FILE"
  USE_MPIRUN=1 MPIRUN_FLAGS="-n $n -npernode 1 --bind-to none -H $nodes" "$REGENT_HOME/legion.py" "$REGENT_HOME/examples/miniAero_fused.lg" $LEGION_OPTS \
    -blocks "$num_blocks" -ll:cpu "$cpu" -findex-launches "$index" -ffutures "$future" -finlines "$inline" -fleaf "$leaf" -fvectorize "$vectorize" &>> $LOG_FILE &
}

NODES="`uniq $NODE_FILE`"
NUM_NODES="`echo "$NODES" | wc -l`"

node_list=""

function get_node_list {
  local n=$1
  local offset=$2

  node_list=""

  for i in `seq 1 $n`
  do
    pos=$(((offset + i - 1) % NUM_NODES + 1))
    node_list="$node_list,`echo "$NODES" | head -n $pos | tail -n 1`"
  done

  node_list="`echo $node_list | sed s/"^,"//g`"
}

n=1
while [ "$n" -le "$NUM_NODES" ];
do

  if [ $n -eq 1 ]; then
    cpus="1 2 4 6 8 10 12"
  else
    cpus="8 10 12"
  fi

  for cpu in $cpus
  do
    remaining_nodes=$NUM_NODES
    offset=0
    for i in $(seq 1 5)
    do
      #index future inline leaf vectorize
      #for config in 2#11111
      for config in 2#01111 2#01011 2#00000 2#01101 2#11111 2#11011 2#11101
      do
        get_node_list "$n" "$offset"
        remaining_nodes=$((remaining_nodes - n))
        offset=$(((offset + n) % NUM_NODES))

        vectorize=$((config >> 0 & 1))
        leaf=$((config >> 1 & 1))
        inline=$((config >> 2 & 1))
        future=$((config >> 3 & 1))
        index=$((config >> 4 & 1))

        waited=false
        run_miniAero $n $cpu $index $future $inline $leaf $vectorize $node_list
        if [ $remaining_nodes -eq 0 ]; then
          wait
          waited=true
          remaining_nodes=$NUM_NODES
          offset=0
        fi
      done
      if [ $waited == false ]; then
        wait
        waited=true
        remaining_nodes=$NUM_NODES
        offset=0
      fi
    done
  done
  n=$((n * 2))
done
