#!/bin/bash -l
#PBS -l nodes=16:ppn=24
#PBS -l walltime=4:00:00

export NODE_FILE="$PBS_NODEFILE"
export MINIAERO_BIN="$HOME/miniaero/build/src/miniaero.exe"
export REGENT_HOME="$HOME/legion/language"

cp "$REGENT_HOME/examples/sc15_scripts/miniAero/miniaero.inp.template" .
"$REGENT_HOME/examples/sc15_scripts/miniAero/run_miniAero_multinode_kokkos.sh"
