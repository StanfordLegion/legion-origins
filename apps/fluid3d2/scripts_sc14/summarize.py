#!/usr/bin/env python

import csv, numpy, sys

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: %s <input> <output>' % sys.argv[0]
    input_filename = sys.argv[1]
    output_filename = sys.argv[2]
    with open(input_filename, 'rb') as f: t = list(csv.reader(f))
    m = [(k, '%.3f' % numpy.mean([float(v[1]) for v in vs]))
         for k, vs in itertools.groupby(t, lambda (k, v): k)]
    with open(output_filename, 'wb') as f: csv.writer(f).writerows(m)
