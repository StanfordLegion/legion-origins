#!/bin/bash
# To run this script:
# qsub -A UT-NTNL0048 -l walltime=01:00:00,nodes=4:ppn=16:gpus=3:shared keeneland_run4.sh

root_dir="/lustre/medusa/sequoia/elliott_legion"
source_dir="$root_dir/apps/fluid3d2"
results="$source_dir/results_sc14/3x300_n4"

export LG_RT_DIR="$root_dir/runtime"
cd "$source_dir"
# make clean
# make -j

common="-MPI -ll:csize 16384 -ll:sdpsize 1536 -ll:rsize 1536 -ll:gsize 0 -ll:cpu 8 -ll:util 2 -i in_3x300K.fluid -nbx 8 -nby 1 -nbz 4 -s 10"
reps=4

rm -rf "$results"
mkdir "$results"

for i in $(seq $reps); do mpirun -n 1 --pernode ./fluid3d $common | tee -a "$results/base1.log"; done
for i in $(seq $reps); do mpirun -n 2 --pernode ./fluid3d $common | tee -a "$results/base2.log"; done
for i in $(seq $reps); do mpirun -n 4 --pernode ./fluid3d $common | tee -a "$results/base4.log"; done

for i in $(seq $reps); do mpirun -n 1 --pernode ./fluidfields $common -aos | tee -a "$results/aos1.log"; done
for i in $(seq $reps); do mpirun -n 2 --pernode ./fluidfields $common -aos | tee -a "$results/aos2.log"; done
for i in $(seq $reps); do mpirun -n 4 --pernode ./fluidfields $common -aos | tee -a "$results/aos4.log"; done

for i in $(seq $reps); do mpirun -n 1 --pernode ./fluidfields $common -soa | tee -a "$results/soa1.log"; done
for i in $(seq $reps); do mpirun -n 2 --pernode ./fluidfields $common -soa | tee -a "$results/soa2.log"; done
for i in $(seq $reps); do mpirun -n 4 --pernode ./fluidfields $common -soa | tee -a "$results/soa4.log"; done

for i in $(seq $reps); do mpirun -n 1 --pernode ./fluidfields $common -mixed | tee -a "$results/mixed1.log"; done
for i in $(seq $reps); do mpirun -n 2 --pernode ./fluidfields $common -mixed | tee -a "$results/mixed2.log"; done
for i in $(seq $reps); do mpirun -n 4 --pernode ./fluidfields $common -mixed | tee -a "$results/mixed4.log"; done
