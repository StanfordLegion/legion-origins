
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <cassert>

// cstdint complains about C++11 support?
#include <stdint.h>

#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

#include "default_mapper.h"
#include "shim_mapper.h"
#include "legion.h"
#include "utilities.h"

using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::HighLevel;

LegionRuntime::Logger::Category log_mapper("mapper");

enum {
  TOP_LEVEL_TASK_ID,
  TASKID_INIT_CELLS,
  TASKID_REBUILD_REDUCE,
  TASKID_SCATTER_DENSITIES,
  TASKID_GATHER_FORCES,
  TASKID_INIT_CELLS_NOP,
  TASKID_REBUILD_REDUCE_NOP,
  TASKID_SCATTER_DENSITIES_NOP,
  TASKID_GATHER_FORCES_NOP,
  TASKID_MAIN_TASK,
  TASKID_LOAD_FILE,
  TASKID_SAVE_FILE,
  TASKID_DUMMY_TASK,
};

enum {
  FIELD,
};

std::vector<FieldID> fields;
std::set<FieldID> fieldset;

typedef AccessorType::Generic Generic;
typedef AccessorType::SOA<0> SOA;

const ptr_t base_ptr(0);

const unsigned MAX_PARTICLES = 16;

// Number of ghost cells needed for each block
const unsigned GHOST_CELLS = 26;

enum { // don't change the order of these!  needs to be symmetric
  TOP_FRONT_LEFT = 0,
  TOP_FRONT,
  TOP_FRONT_RIGHT,
  FRONT_LEFT,
  FRONT,
  FRONT_RIGHT,
  BOTTOM_FRONT_LEFT,
  BOTTOM_FRONT,
  BOTTOM_FRONT_RIGHT,
  BOTTOM_LEFT,
  BOTTOM,
  BOTTOM_RIGHT,
  LEFT,
  RIGHT,
  TOP_LEFT,
  TOP,
  TOP_RIGHT,
  TOP_BACK_LEFT,
  TOP_BACK,
  TOP_BACK_RIGHT,
  BACK_LEFT,
  BACK,
  BACK_RIGHT,
  BOTTOM_BACK_LEFT,
  BOTTOM_BACK,
  BOTTOM_BACK_RIGHT,
  CENTER,
};

enum {
  SIDE_TOP    = 0x01,
  SIDE_BOTTOM = 0x02,
  SIDE_FRONT  = 0x04,
  SIDE_BACK   = 0x08,
  SIDE_RIGHT  = 0x10,
  SIDE_LEFT   = 0x20,
};

// order corresponds to order of elements in enum above
const unsigned char DIR2SIDES[] = {
  SIDE_TOP | SIDE_FRONT | SIDE_LEFT,
  SIDE_TOP | SIDE_FRONT,
  SIDE_TOP | SIDE_FRONT | SIDE_RIGHT,
  SIDE_FRONT | SIDE_LEFT,
  SIDE_FRONT,
  SIDE_FRONT | SIDE_RIGHT,
  SIDE_BOTTOM | SIDE_FRONT | SIDE_LEFT,
  SIDE_BOTTOM | SIDE_FRONT,
  SIDE_BOTTOM | SIDE_FRONT | SIDE_RIGHT,
  SIDE_BOTTOM | SIDE_LEFT,
  SIDE_BOTTOM,
  SIDE_BOTTOM | SIDE_RIGHT,
  SIDE_LEFT,
  SIDE_RIGHT,
  SIDE_TOP | SIDE_LEFT,
  SIDE_TOP,
  SIDE_TOP | SIDE_RIGHT,
  SIDE_TOP | SIDE_BACK | SIDE_LEFT,
  SIDE_TOP | SIDE_BACK,
  SIDE_TOP | SIDE_BACK | SIDE_RIGHT,
  SIDE_BACK | SIDE_LEFT,
  SIDE_BACK,
  SIDE_BACK | SIDE_RIGHT,
  SIDE_BOTTOM | SIDE_BACK | SIDE_LEFT,
  SIDE_BOTTOM | SIDE_BACK,
  SIDE_BOTTOM | SIDE_BACK | SIDE_RIGHT,
  0,
};

// order corresponds to zyx [3][3][3] lookup array
const unsigned char SIDES2DIR[] = {
  BOTTOM_FRONT_LEFT,
  BOTTOM_FRONT,
  BOTTOM_FRONT_RIGHT,
  BOTTOM_LEFT,
  BOTTOM,
  BOTTOM_RIGHT,
  BOTTOM_BACK_LEFT,
  BOTTOM_BACK,
  BOTTOM_BACK_RIGHT,
  FRONT_LEFT,
  FRONT,
  FRONT_RIGHT,
  LEFT,
  CENTER,
  RIGHT,
  BACK_LEFT,
  BACK,
  BACK_RIGHT,
  TOP_FRONT_LEFT,
  TOP_FRONT,
  TOP_FRONT_RIGHT,
  TOP_LEFT,
  TOP,
  TOP_RIGHT,
  TOP_BACK_LEFT,
  TOP_BACK,
  TOP_BACK_RIGHT,
};

static inline int CLAMP(int x, int min, int max)
{
  return x < min ? min : (x > max ? max : x);
}

static inline int MOVE_TOP(int z)    { return z+1; }
static inline int MOVE_BOTTOM(int z) { return z-1; }
static inline int MOVE_LEFT(int x)   { return x-1; }
static inline int MOVE_RIGHT(int x)  { return x+1; }
static inline int MOVE_FRONT(int y)  { return y-1; }
static inline int MOVE_BACK(int y)   { return y+1; }

static inline int MOVE_X(int x, int dir, int min, int max)
{
  return CLAMP((DIR2SIDES[dir] & SIDE_RIGHT) ? MOVE_RIGHT(x) :
               ((DIR2SIDES[dir] & SIDE_LEFT) ? MOVE_LEFT(x) : x),
               min, max);
}

static inline int MOVE_Y(int y, int dir, int min, int max)
{
  return CLAMP((DIR2SIDES[dir] & SIDE_BACK) ? MOVE_BACK(y) :
               ((DIR2SIDES[dir] & SIDE_FRONT) ? MOVE_FRONT(y) : y),
               min, max);
}

static inline int MOVE_Z(int z, int dir, int min, int max)
{
  return CLAMP((DIR2SIDES[dir] & SIDE_TOP) ? MOVE_TOP(z) :
               ((DIR2SIDES[dir] & SIDE_BOTTOM) ? MOVE_BOTTOM(z) : z),
               min, max);
}

static inline int REVERSE(int dir) { return 25 - dir; }

// maps {-1, 0, 1}^3 to directions
static inline int LOOKUP_DIR(int x, int y, int z)
{
  return SIDES2DIR[((z+1)*3 + y+1)*3 + x+1];
}

static inline int REVERSE_SIDES(int dir, int flipx, int flipy, int flipz)
{
  int dirx = (DIR2SIDES[dir] & SIDE_RIGHT) ? -1 :
    ((DIR2SIDES[dir] & SIDE_LEFT) ? 1 : 0);
  int diry = (DIR2SIDES[dir] & SIDE_BACK) ? -1 :
    ((DIR2SIDES[dir] & SIDE_FRONT) ? 1 : 0);
  int dirz = (DIR2SIDES[dir] & SIDE_TOP) ? -1 :
    ((DIR2SIDES[dir] & SIDE_BOTTOM) ? 1 : 0);
  if (flipx) dirx = -dirx;
  if (flipy) diry = -diry;
  if (flipz) dirz = -dirz;
  return LOOKUP_DIR(dirx, diry, dirz);
}

class Vec3
{
public:
    float x, y, z;

    Vec3() {}
    Vec3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}

    float   GetLengthSq() const         { return x*x + y*y + z*z; }
    float   GetLength() const           { return sqrtf(GetLengthSq()); }
    Vec3 &  Normalize()                 { return *this /= GetLength(); }

    Vec3 &  operator += (Vec3 const &v) { x += v.x;  y += v.y; z += v.z; return *this; }
    Vec3 &  operator -= (Vec3 const &v) { x -= v.x;  y -= v.y; z -= v.z; return *this; }
    Vec3 &  operator *= (float s)       { x *= s;  y *= s; z *= s; return *this; }
    Vec3 &  operator /= (float s)       { x /= s;  y /= s; z /= s; return *this; }

    Vec3    operator + (Vec3 const &v) const    { return Vec3(x+v.x, y+v.y, z+v.z); }
    Vec3    operator - () const                 { return Vec3(-x, -y, -z); }
    Vec3    operator - (Vec3 const &v) const    { return Vec3(x-v.x, y-v.y, z-v.z); }
    Vec3    operator * (float s) const          { return Vec3(x*s, y*s, z*s); }
    Vec3    operator / (float s) const          { return Vec3(x/s, y/s, z/s); }

    float   operator * (Vec3 const &v) const    { return x*v.x + y*v.y + z*v.z; }
};

#define NO_TRACK_PARTICLE_IDS

struct Cell
{
public:
  Vec3 p[MAX_PARTICLES];
  Vec3 hv[MAX_PARTICLES];
  Vec3 v[MAX_PARTICLES];
  Vec3 a[MAX_PARTICLES];
  float density[MAX_PARTICLES];
#ifdef TRACK_PARTICLE_IDS
  int id[MAX_PARTICLES];
#endif
  unsigned num_particles;
};

// two kinds of double-buffering going on here
// * for the CELLS_X x CELLS_Y x CELLS_Z grid of "real" cells, we have two copies
//     for double-buffering the simulation
// * for the ring of edge/ghost cells around the "real" cells, we have
//     two copies for bidirectional exchanges
//
// in addition to the requisite 2*1 + 2*26 = 54 regions, we track 
//  2 sets of (CELLS_X+2)*(CELLS_Y+2)*(CELLS_Z+2) pointers
// have to pay attention though, because the phase of the "real" cells changes
//  only once per simulation iteration, while the phase of the "edge" cells
//  changes every task
struct Block {
  int id;
  unsigned x, y, z; // position in block grid
  unsigned CELLS_X, CELLS_Y, CELLS_Z;
};

const float timeStep = 0.005f;
const float doubleRestDensity = 2000.f;
const float kernelRadiusMultiplier = 1.695f;
const float stiffness = 1.5f;
const float viscosity = 0.4f;
const Vec3 externalAcceleration(0.f, -9.8f, 0.f);
const Vec3 domainMin(-0.065f, -0.08f, -0.065f);
const Vec3 domainMax(0.065f, 0.1f, 0.065f);

const unsigned MAX_SPLITS = 16;

const size_t MAX_FILENAME = 256;
struct Config {
  // Parameters from command line arguments.
  unsigned numSteps;
  unsigned nbx, nby, nbz, numBlocks;
  int balance;
  int splitx[MAX_SPLITS + 1];
  int splity[MAX_SPLITS + 1];
  int splitz[MAX_SPLITS + 1];
  char inFilename[MAX_FILENAME], outFilename[MAX_FILENAME];

  // Parameters of simulation from fluid file header.
  float restParticlesPerMeter;
  int origNumParticles;

  // Variables dependent on fluid file headers.
  float h, hSq;
  float densityCoeff, pressureCoeff, viscosityCoeff;
  unsigned nx, ny, nz, numCells;
  Vec3 delta;                           // cell dimensions

  // should app be run in bulk-synchronous mode?
  int sync;

  // PARSEC does not double buffer. That is, PARSEC performs the first
  // step of the computation over and over. However, double buffering
  // has a potential performance impact, so we need both.
  bool repeat_first_timestep;
};

static inline int MOVE_BX(int x, int dir, const Config &c) { return MOVE_X(x, dir, 0, c.nbx-1); }
static inline int MOVE_BY(int y, int dir, const Config &c) { return MOVE_Y(y, dir, 0, c.nby-1); }
static inline int MOVE_BZ(int z, int dir, const Config &c) { return MOVE_Z(z, dir, 0, c.nbz-1); }
static inline int MOVE_CX(const Block &b, int x, int dir) {
return MOVE_X(x, dir, 0, b.CELLS_X+1);
}
static inline int MOVE_CY(const Block &b, int y, int dir) {
return MOVE_Y(y, dir, 0, b.CELLS_Y+1);
}
static inline int MOVE_CZ(const Block &b, int z, int dir) {
return MOVE_Z(z, dir, 0, b.CELLS_Z+1);
}

LegionRuntime::Logger::Category log_app("application");

static unsigned parse_args(int argc, char **argv, Config &conf)
{
  unsigned &nbx = conf.nbx, &nby = conf.nby, &nbz = conf.nbz, &numBlocks = conf.numBlocks;
  unsigned &numSteps = conf.numSteps;
  char (&inFilename)[256] = conf.inFilename, (&outFilename)[256] = conf.outFilename;

  // Default parameter values.
  numSteps = 4;
  nbx = nby = nbz = 1;
  conf.balance = 0;
  conf.sync = 0; // default is to not be bulk-synchronous
  conf.repeat_first_timestep = true;  // match PARSEC by default
  strcpy(inFilename, "init.fluid");
  strcpy(outFilename, "");

  for (int i = 1; i < argc; i++) {
    if (!strcmp(argv[i], "-s")) {
      numSteps = atoi(argv[++i]);
      continue;
    }
    
    if (!strcmp(argv[i], "-nbx")) {
      nbx = atoi(argv[++i]);
      continue;
    }
    
    if (!strcmp(argv[i], "-nby")) {
      nby = atoi(argv[++i]);
      continue;
    }

    if (!strcmp(argv[i], "-nbz")) {
      nbz = atoi(argv[++i]);
      continue;
    }

    if (!strcmp(argv[i], "-i") || !strcmp(argv[i], "-input")) {
      strncpy(inFilename, argv[++i], MAX_FILENAME);
      inFilename[MAX_FILENAME-1] = '\0';
      continue;
    }

    if (!strcmp(argv[i], "-o") || !strcmp(argv[i], "-output")) {
      strncpy(outFilename, argv[++i], MAX_FILENAME);
      outFilename[MAX_FILENAME-1] = '\0';
      continue;
    }

    if (!strcmp(argv[i], "-balance")) {
      conf.balance = atoi(argv[++i]);
      continue;
    }

    if (!strcmp(argv[i], "-sync")) {
      conf.sync = atoi(argv[++i]);
      printf("sync set to %d\n", conf.sync);
      continue;
    }

    if (!strcmp(argv[i], "-repeat")) {
      conf.repeat_first_timestep = atoi(argv[++i]);
      printf("repeat set to %d\n", conf.repeat_first_timestep);
      continue;
    }
  }
  numBlocks = nbx * nby * nbz;
  return numSteps;
}

// Fills in restParticlesPerMeter and origNumParticles from fluid file header.
static void load_file_header(const char *fileName, Config &conf);

// Expects restParticlesPerMeter to be filled in from load_file_header.
static void init_config(Config &conf)
{
  const float &restParticlesPerMeter = conf.restParticlesPerMeter;

  float &h = conf.h, &hSq = conf.hSq;
  float &densityCoeff = conf.densityCoeff;
  float &pressureCoeff = conf.pressureCoeff;
  float &viscosityCoeff = conf.viscosityCoeff;
  unsigned &nx = conf.nx, &ny = conf.ny, &nz = conf.nz, &numCells = conf.numCells;
  Vec3 &delta = conf.delta;

  // Simulation parameters
  h = kernelRadiusMultiplier / restParticlesPerMeter;
  hSq = h*h;
  const float pi = 3.14159265358979f;
  float coeff1 = 315.f / (64.f*pi*pow(h,9.f));
  float coeff2 = 15.f / (pi*pow(h,6.f));
  float coeff3 = 45.f / (pi*pow(h,6.f));
  float particleMass = 0.5f*doubleRestDensity / (restParticlesPerMeter*restParticlesPerMeter*restParticlesPerMeter);
  densityCoeff = particleMass * coeff1;
  pressureCoeff = 3.f*coeff2 * 0.5f*stiffness * particleMass;
  viscosityCoeff = viscosity * coeff3 * particleMass;

  // Simulation grid size
  Vec3 range = domainMax - domainMin;
  nx = (int)(range.x / h);
  ny = (int)(range.y / h);
  nz = (int)(range.z / h);
  numCells = nx*ny*nz;
  delta.x = range.x / nx;
  delta.y = range.y / ny;
  delta.z = range.z / nz;
  assert(delta.x >= h && delta.y >= h && delta.z >= h);
  
  // default splits are the same way that original PARSEC does it
  int xstep = nx / conf.nbx;
  unsigned xextra = nx % conf.nbx;
  conf.splitx[0] = 0;
  for (unsigned i = 0; i < conf.nbx; i++)
    conf.splitx[i + 1] = conf.splitx[i] + xstep + ((i < xextra) ? 1 : 0);

  int ystep = ny / conf.nby;
  unsigned yextra = ny % conf.nby;
  conf.splity[0] = 0;
  for (unsigned i = 0; i < conf.nby; i++)
    conf.splity[i + 1] = conf.splity[i] + ystep + ((i < yextra) ? 1 : 0);

  int zstep = nz / conf.nbz;
  unsigned zextra = nz % conf.nbz;
  conf.splitz[0] = 0;
  for (unsigned i = 0; i < conf.nbz; i++)
    conf.splitz[i + 1] = conf.splitz[i] + zstep + ((i < zextra) ? 1 : 0);
}

#ifdef PARTITIONED_EDGE_CELLS
static void add_all_regions(TaskLauncher &launcher,
                            std::vector<LogicalRegion> &regions,
                            PrivilegeMode access, CoherenceProperty prop,
                            LogicalRegion parent)
{
  for (unsigned i = 0; i < regions.size(); i++) {
    launcher.add_region_requirement(
      RegionRequirement(regions[i], fieldset, fields, access, prop, parent));
  }
}
#else
static void add_all_regions(TaskLauncher &launcher,
                            std::vector<LogicalRegion> &regions,
                            PrivilegeMode access, CoherenceProperty prop,
                            std::vector<LogicalRegion> &parents)
{
  for (unsigned i = 0; i < regions.size(); i++) {
    launcher.add_region_requirement(
      RegionRequirement(regions[i], fieldset, fields, access, prop, parents[i]));
  }
}
#endif

void analyze_particle_distribution(Config& conf);

static LogicalRegion create_logical_region(HighLevelRuntime *runtime,
                                           Context ctx,
                                           unsigned num_elements,
                                           size_t field_size) {
  IndexSpace is = runtime->create_index_space(ctx, num_elements);
  IndexAllocator isa = runtime->create_index_allocator(ctx, is);
  assert(isa.alloc(num_elements) == base_ptr);
  FieldSpace fs = runtime->create_field_space(ctx);
  FieldAllocator fsa = runtime->create_field_allocator(ctx, fs);
  fsa.allocate_field(field_size, FIELD);
  LogicalRegion r = runtime->create_logical_region(ctx, is, fs);
  return r;
}

void top_level_task(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, HighLevelRuntime *runtime)
{
  log_app.info("In top_level_task...");

  InputArgs inputs = HighLevelRuntime::get_input_args();
  char **argv = inputs.argv;
  int argc = inputs.argc;

  Config conf;
  unsigned &nx = conf.nx, &ny = conf.ny, &nz = conf.nz;
  unsigned &nbx = conf.nbx, &nby = conf.nby, &nbz = conf.nbz;
  unsigned &numBlocks = conf.numBlocks;
  char (&inFilename)[256] = conf.inFilename;

  // parse command line arguments
  parse_args(argc, argv, conf);
  printf("fluid: %p %d ",argv,argc);
  for (int i = 1; i < argc; i++)
    printf("%s ",argv[i]);
  printf("\n");

  // read input file header to get problem size
  load_file_header(inFilename, conf);
  init_config(conf);

  if (conf.balance) {
    analyze_particle_distribution(conf);
  }

  fields.push_back(FIELD);
  fieldset.insert(FIELD);

  const unsigned n_config = 1;
  LogicalRegion config =
    create_logical_region(runtime, ctx, n_config, sizeof(Config));

  const unsigned n_blocks = nbx*nby*nbz;
  LogicalRegion blocks =
    create_logical_region(runtime, ctx, n_blocks, sizeof(Block));

  const unsigned n_real_cells = nx*ny*nz;
  LogicalRegion real_cells[2];
  real_cells[0] =
    create_logical_region(runtime, ctx, n_real_cells, sizeof(Cell));
  real_cells[1] =
    create_logical_region(runtime, ctx, n_real_cells, sizeof(Cell));

// #define PARTITIONED_EDGE_CELLS
#ifdef PARTITIONED_EDGE_CELLS
  const unsigned rnx = nx + 2*nbx, rny = ny + 2*nby, rnz = nz + 2*nbz;
  const unsigned n_edge_cells =
    2*nbx*rny*rnz + 2*nby*rnx*rnz + 2*nbz*rnx*rny
    - 4*nbx*nby*rnz - 4*nbx*nbz*rny - 4*nby*nbz*rnx
    + 8*nbx*nby*nbz;
  LogicalRegion edge_cells =
    create_logical_region(runtime, ctx, n_edge_cells, sizeof(Cell));
#endif

  std::vector<LogicalRegion> block_ptrs;
  for (unsigned idz = 0; idz < nbz; idz++)
    for (unsigned idy = 0; idy < nby; idy++)
      for (unsigned idx = 0; idx < nbx; idx++) {
        // unsigned id = (idz*nby+idy)*nbx+idx;

        unsigned CELLS_X = (conf.splitx[idx+1] - conf.splitx[idx]);
        unsigned CELLS_Y = (conf.splity[idy+1] - conf.splity[idy]);
        unsigned CELLS_Z = (conf.splitz[idz+1] - conf.splitz[idz]);
        //assert(CELLS_X == (nx/nbx) + (nx%nbx > idx ? 1 : 0));
        //assert(CELLS_Y == (ny/nby) + (ny%nby > idy ? 1 : 0));
        //assert(CELLS_Z == (nz/nbz) + (nz%nbz > idz ? 1 : 0));

        const unsigned n_block_ptrs = 2*(CELLS_X+2)*(CELLS_Y+2)*(CELLS_Z+2);
        block_ptrs.push_back(
          create_logical_region(runtime, ctx, n_block_ptrs, sizeof(ptr_t)));
      }

  // Initialize configuration.
  {
    InlineLauncher map(
      RegionRequirement(config, fieldset, fields, READ_WRITE, EXCLUSIVE, config));
    PhysicalRegion pr_config = runtime->map_region(ctx, map);
    pr_config.wait_until_valid();
    pr_config.get_field_accessor(FIELD).typeify<Config>().write(base_ptr, conf);
    runtime->unmap_region(ctx, pr_config);
  }

  // Call main.
  {
    TaskLauncher main(TASKID_MAIN_TASK, TaskArgument());
    main.add_region_requirement(
      RegionRequirement(config, fieldset, fields, READ_ONLY, EXCLUSIVE, config));
    main.add_region_requirement(
      RegionRequirement(blocks, fieldset, fields, READ_WRITE, EXCLUSIVE, blocks));
    main.add_region_requirement(
      RegionRequirement(real_cells[0], fieldset, fields, READ_WRITE, EXCLUSIVE, real_cells[0]));
    main.add_region_requirement(
      RegionRequirement(real_cells[1], fieldset, fields, READ_WRITE, EXCLUSIVE, real_cells[1]));
#ifdef PARTITIONED_EDGE_CELLS
    main.add_region_requirement(
      RegionRequirement(edge_cells, fieldset, fields, READ_WRITE, EXCLUSIVE, edge_cells));
#endif
    for (unsigned id = 0; id < numBlocks; id++) {
      main.add_region_requirement(
        RegionRequirement(block_ptrs[id], fieldset, fields, READ_WRITE, EXCLUSIVE, block_ptrs[id]));
    }

    Future f = runtime->execute_task(ctx, main);
    f.get_void_result();
  }

  log_app.info("Done with top_level_task...");
}

static inline int NEIGH_X(int idx, int dir, int cx, const Config &c)
{
  return MOVE_BX(idx, dir, c) == idx ? cx : 1-cx;
}

static inline int NEIGH_Y(int idy, int dir, int cy, const Config &c)
{
  return MOVE_BY(idy, dir, c) == idy ? cy : 1-cy;
}

static inline int NEIGH_Z(int idz, int dir, int cz, const Config &c)
{
  return MOVE_BZ(idz, dir, c) == idz ? cz : 1-cz;
}

static inline int OPPOSITE_DIR(int idz, int idy, int idx, int dir, const Config &c)
{
  int flipx = MOVE_BX(idx, dir, c) == idx;
  int flipy = MOVE_BY(idy, dir, c) == idy;
  int flipz = MOVE_BZ(idz, dir, c) == idz;
  return REVERSE_SIDES(dir, flipx, flipy, flipz);
}

static inline ptr_t get_cell_ptr_ptr(const Block &b, unsigned cb, unsigned cz, unsigned cy, unsigned cx)
{
  ptr_t result;
  result.value = ((cb*(b.CELLS_Z+2) + cz)*(b.CELLS_Y+2) + cy)*(b.CELLS_X+2) + cx;
  return result;
}

// Flag main_task passes to the mapper to request an instance for edge_cells.
const unsigned REQUEST_INSTANCE = 1;

void main_task(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime)
{
  log_app.info("In main_task...");

  // Retrieve configuration.
  PhysicalRegion pr_config = regions[0];
  LogicalRegion config = pr_config.get_logical_region();
  Config conf =
    pr_config.get_field_accessor(FIELD).typeify<Config>().read(base_ptr);
  unsigned &numBlocks = conf.numBlocks;
  unsigned &nx = conf.nx, &ny = conf.ny, &nz = conf.nz;
  unsigned &nbx = conf.nbx, &nby = conf.nby, &nbz = conf.nbz;
  char (&outFilename)[256] = conf.outFilename;

  // Retrieve physical regions.
  PhysicalRegion pr_blocks = regions[1];
  PhysicalRegion pr_real_cells[2];
  pr_real_cells[0] = regions[2];
  pr_real_cells[1] = regions[3];
#ifdef PARTITIONED_EDGE_CELLS
  PhysicalRegion pr_edge_cells = regions[4];
  std::vector<PhysicalRegion> pr_block_ptrs;
  for (unsigned id = 0; id < numBlocks; id++) {
    pr_block_ptrs.push_back(regions[5 + id]);
  }
#else
  std::vector<PhysicalRegion> pr_block_ptrs;
  for (unsigned id = 0; id < numBlocks; id++) {
    pr_block_ptrs.push_back(regions[4 + id]);
  }
#endif

  // Retrieve logical regions.
  LogicalRegion blocks = pr_blocks.get_logical_region();
  LogicalRegion real_cells[2];
  real_cells[0] = pr_real_cells[0].get_logical_region();
  real_cells[1] = pr_real_cells[1].get_logical_region();
#ifdef PARTITIONED_EDGE_CELLS
  LogicalRegion edge_cells = pr_edge_cells.get_logical_region();
#endif
  std::vector<LogicalRegion> block_ptrs;
  for (unsigned id = 0; id < numBlocks; id++) {
    block_ptrs.push_back(pr_block_ptrs[id].get_logical_region());
  }

  // Retrieve accessors.
  RegionAccessor<SOA, Block> a_blocks =
    pr_blocks.get_field_accessor(FIELD).typeify<Block>().convert<SOA>();
  std::vector<RegionAccessor<SOA, ptr_t> > a_block_ptrs(numBlocks);
  for (unsigned id = 0; id < numBlocks; id++) {
    a_block_ptrs[id] = pr_block_ptrs[id].get_field_accessor(FIELD).typeify<ptr_t>().convert<SOA>();
  }

  printf("fluid: cells     = %d (%d x %d x %d)\n", nx*ny*nz, nx, ny, nz);
  printf("fluid: divisions = %d x %d x %d\n", nbx, nby, nbz);
  printf("fluid: steps     = %d\n", conf.numSteps);

  // Initialize and partition blocks
  for (unsigned idz = 0; idz < nbz; idz++)
    for (unsigned idy = 0; idy < nby; idy++)
      for (unsigned idx = 0; idx < nbx; idx++) {
        ptr_t id = (idz*nby+idy)*nbx+idx;

        Block &block = a_blocks.ref(id);
        block.id = id.value;
        block.x = idx;
        block.y = idy;
        block.z = idz;

        block.CELLS_X = (conf.splitx[idx+1] - conf.splitx[idx]);
        block.CELLS_Y = (conf.splity[idy+1] - conf.splity[idy]);
        block.CELLS_Z = (conf.splitz[idz+1] - conf.splitz[idz]);
      }

  // first, do two passes of the "real" cells
  std::vector<LogicalRegion> block_bases[2];
  for (int b = 0; b < 2; b++) {
    block_bases[b].resize(numBlocks);

    Coloring coloring;
    ptr_t next(base_ptr);

    // Store pointers, set up colors
    for (unsigned idz = 0; idz < nbz; idz++)
      for (unsigned idy = 0; idy < nby; idy++)
        for (unsigned idx = 0; idx < nbx; idx++) {
          unsigned id = (idz*nby+idy)*nbx+idx;

          for (unsigned cz = 0; cz < a_blocks.ref(id).CELLS_Z; cz++)
            for (unsigned cy = 0; cy < a_blocks.ref(id).CELLS_Y; cy++)
              for (unsigned cx = 0; cx < a_blocks.ref(id).CELLS_X; cx++) {
                ptr_t cell(next.value++);
                coloring[id].points.insert(cell);
                a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), b, cz+1, cy+1, cx+1), cell);
              }
        }

    // Create the partitions
    IndexPartition isp_block_ptrs =
      runtime->create_index_partition(ctx, real_cells[b].get_index_space(),
                                      coloring, true/*disjoint*/);
    LogicalPartition lp_block_ptrs =
      runtime->get_logical_partition(ctx, real_cells[b], isp_block_ptrs);

    for (unsigned idz = 0; idz < nbz; idz++)
      for (unsigned idy = 0; idy < nby; idy++)
        for (unsigned idx = 0; idx < nbx; idx++) {
          unsigned id = (idz*nby+idy)*nbx+idx;
          block_bases[b][id] =
            runtime->get_logical_subregion_by_color(ctx, lp_block_ptrs, id);
        }
  }

  std::vector<std::vector<LogicalRegion> > block_edges[2];
  {
#ifdef PARTITIONED_EDGE_CELLS
    // the edge cells work a bit different - we'll create one region, partition
    //  it once, and use each subregion in two places
    Coloring coloring;

    // allocate cells, set up coloring
    ptr_t next(base_ptr);
#else
    std::map<Color, LogicalRegion> coloring;
#endif
    int color = 0;
    for (unsigned idz = 0; idz < nbz; idz++)
      for (unsigned idy = 0; idy < nby; idy++)
        for (unsigned idx = 0; idx < nbx; idx++) {
          unsigned id = (idz*nby+idy)*nbx+idx;

#define CX (a_blocks.ref(id).CELLS_X+1)
#define CY (a_blocks.ref(id).CELLS_Y+1)
#define CZ (a_blocks.ref(id).CELLS_Z+1)
#define C2X (a_blocks.ref(id2).CELLS_X+1)
#define C2Y (a_blocks.ref(id2).CELLS_Y+1)
#define C2Z (a_blocks.ref(id2).CELLS_Z+1)

          // eight corners
#ifdef PARTITIONED_EDGE_CELLS
#define CORNER(dir,ix,iy,iz) do {                                       \
            unsigned id2 = (MOVE_BZ(idz,dir,conf)*nby + MOVE_BY(idy,dir,conf))*nbx + MOVE_BX(idx,dir,conf); \
            ptr_t cell(next.value++);                                   \
            coloring[color + dir].points.insert(cell);                         \
            a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, iz*CZ, iy*CY, ix*CX), cell); \
            a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, NEIGH_Z(idz, dir, iz, conf)*C2Z, NEIGH_Y(idy, dir, iy, conf)*C2Y, NEIGH_X(idx, dir, ix, conf)*C2X), cell); \
          } while (0)
#else
#define CORNER(dir,ix,iy,iz) do {                                       \
            unsigned id2 = (MOVE_BZ(idz,dir,conf)*nby + MOVE_BY(idy,dir,conf))*nbx + MOVE_BX(idx,dir,conf); \
            ptr_t next(base_ptr);                                       \
            ptr_t cell(next.value++);                                     \
            coloring[color + dir] = create_logical_region(runtime, ctx, 1, sizeof(Cell)); \
            a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, iz*CZ, iy*CY, ix*CX), cell); \
            a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, NEIGH_Z(idz, dir, iz, conf)*C2Z, NEIGH_Y(idy, dir, iy, conf)*C2Y, NEIGH_X(idx, dir, ix, conf)*C2X), cell); \
          } while (0)
#endif
          CORNER(TOP_FRONT_LEFT,     0, 0, 1);
          CORNER(TOP_FRONT_RIGHT,    1, 0, 1);
          CORNER(TOP_BACK_LEFT,      0, 1, 1);
          CORNER(TOP_BACK_RIGHT,     1, 1, 1);
          CORNER(BOTTOM_FRONT_LEFT,  0, 0, 0);
          CORNER(BOTTOM_FRONT_RIGHT, 1, 0, 0);
          CORNER(BOTTOM_BACK_LEFT,   0, 1, 0);
          CORNER(BOTTOM_BACK_RIGHT,  1, 1, 0);
#undef CORNER

          // x-axis edges
#ifdef PARTITIONED_EDGE_CELLS
#define XAXIS(dir,iy,iz) do {                                           \
            unsigned id2 = (MOVE_BZ(idz,dir,conf)*nby + MOVE_BY(idy,dir,conf))*nbx + idx; \
            assert(a_blocks.ref(id).CELLS_X == a_blocks.ref(id2).CELLS_X); \
            for (unsigned cx = 1; cx <= a_blocks.ref(id).CELLS_X; cx++) { \
              ptr_t cell(next.value++);                                 \
              coloring[color + dir].points.insert(cell);                       \
              a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, iz*CZ, iy*CY, cx), cell); \
              a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, NEIGH_Z(idz, dir, iz, conf)*C2Z, NEIGH_Y(idy, dir, iy, conf)*C2Y, cx), cell); \
            }                                                           \
          } while (0)
#else
#define XAXIS(dir,iy,iz) do {                                           \
            unsigned id2 = (MOVE_BZ(idz,dir,conf)*nby + MOVE_BY(idy,dir,conf))*nbx + idx; \
            assert(a_blocks.ref(id).CELLS_X == a_blocks.ref(id2).CELLS_X); \
            ptr_t next(base_ptr);                                       \
            coloring[color + dir] = create_logical_region(runtime, ctx, a_blocks.ref(id).CELLS_X, sizeof(Cell)); \
            for (unsigned cx = 1; cx <= a_blocks.ref(id).CELLS_X; cx++) { \
              ptr_t cell(next.value++);                                 \
              a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, iz*CZ, iy*CY, cx), cell); \
              a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, NEIGH_Z(idz, dir, iz, conf)*C2Z, NEIGH_Y(idy, dir, iy, conf)*C2Y, cx), cell); \
            }                                                           \
          } while (0)
#endif
          XAXIS(TOP_FRONT,    0, 1);
          XAXIS(TOP_BACK,     1, 1);
          XAXIS(BOTTOM_FRONT, 0, 0);
          XAXIS(BOTTOM_BACK,  1, 0);
#undef XAXIS

          // y-axis edges
#ifdef PARTITIONED_EDGE_CELLS
#define YAXIS(dir,ix,iz) do {                                           \
            unsigned id2 = (MOVE_BZ(idz,dir,conf)*nby + idy)*nbx + MOVE_BX(idx,dir,conf); \
            assert(a_blocks.ref(id).CELLS_Y == a_blocks.ref(id2).CELLS_Y); \
            for (unsigned cy = 1; cy <= a_blocks.ref(id).CELLS_Y; cy++) { \
              ptr_t cell(next.value++);                                 \
              coloring[color + dir].points.insert(cell);                       \
              a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, iz*CZ, cy, ix*CX), cell); \
              a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, NEIGH_Z(idz, dir, iz, conf)*C2Z, cy, NEIGH_X(idx, dir, ix, conf)*C2X), cell); \
            }                                                           \
          } while (0)
#else
#define YAXIS(dir,ix,iz) do {                                           \
            unsigned id2 = (MOVE_BZ(idz,dir,conf)*nby + idy)*nbx + MOVE_BX(idx,dir,conf); \
            assert(a_blocks.ref(id).CELLS_Y == a_blocks.ref(id2).CELLS_Y); \
            ptr_t next(base_ptr);                                       \
            coloring[color + dir] = create_logical_region(runtime, ctx, a_blocks.ref(id).CELLS_Y, sizeof(Cell)); \
            for (unsigned cy = 1; cy <= a_blocks.ref(id).CELLS_Y; cy++) { \
              ptr_t cell(next.value++);                                 \
              a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, iz*CZ, cy, ix*CX), cell); \
              a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, NEIGH_Z(idz, dir, iz, conf)*C2Z, cy, NEIGH_X(idx, dir, ix, conf)*C2X), cell); \
            }                                                           \
          } while (0)
#endif
          YAXIS(TOP_LEFT,     0, 1);
          YAXIS(TOP_RIGHT,    1, 1);
          YAXIS(BOTTOM_LEFT,  0, 0);
          YAXIS(BOTTOM_RIGHT, 1, 0);
#undef YAXIS

          // z-axis edges
#ifdef PARTITIONED_EDGE_CELLS
#define ZAXIS(dir,ix,iy) do {                                           \
            unsigned id2 = (idz*nby + MOVE_BY(idy,dir,conf))*nbx + MOVE_BX(idx,dir,conf); \
            assert(a_blocks.ref(id).CELLS_Z == a_blocks.ref(id2).CELLS_Z); \
            for (unsigned cz = 1; cz <= a_blocks.ref(id).CELLS_Z; cz++) { \
              ptr_t cell(next.value++);                                 \
              coloring[color + dir].points.insert(cell);                       \
              a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, cz, iy*CY, ix*CX), cell); \
              a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, cz, NEIGH_Y(idy, dir, iy, conf)*C2Y, NEIGH_X(idx, dir, ix, conf)*C2X), cell); \
            }                                                           \
          } while (0)
#else
#define ZAXIS(dir,ix,iy) do {                                           \
            unsigned id2 = (idz*nby + MOVE_BY(idy,dir,conf))*nbx + MOVE_BX(idx,dir,conf); \
            assert(a_blocks.ref(id).CELLS_Z == a_blocks.ref(id2).CELLS_Z); \
            ptr_t next(base_ptr);                                       \
            coloring[color + dir] = create_logical_region(runtime, ctx, a_blocks.ref(id).CELLS_Z, sizeof(Cell)); \
            for (unsigned cz = 1; cz <= a_blocks.ref(id).CELLS_Z; cz++) { \
              ptr_t cell(next.value++);                                 \
              a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, cz, iy*CY, ix*CX), cell); \
              a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, cz, NEIGH_Y(idy, dir, iy, conf)*C2Y, NEIGH_X(idx, dir, ix, conf)*C2X), cell); \
            }                                                           \
          } while (0)
#endif
          ZAXIS(FRONT_LEFT,  0, 0);
          ZAXIS(FRONT_RIGHT, 1, 0);
          ZAXIS(BACK_LEFT,   0, 1);
          ZAXIS(BACK_RIGHT,  1, 1);
#undef ZAXIS

          // xy-plane edges
#ifdef PARTITIONED_EDGE_CELLS
#define XYPLANE(dir,iz) do {                                            \
            unsigned id2 = (MOVE_BZ(idz,dir,conf)*nby + idy)*nbx + idx; \
            assert(a_blocks.ref(id).CELLS_X == a_blocks.ref(id2).CELLS_X); \
            assert(a_blocks.ref(id).CELLS_Y == a_blocks.ref(id2).CELLS_Y); \
            for (unsigned cy = 1; cy <= a_blocks.ref(id).CELLS_Y; cy++) { \
              for (unsigned cx = 1; cx <= a_blocks.ref(id).CELLS_X; cx++) { \
                ptr_t cell(next.value++);                               \
                coloring[color + dir].points.insert(cell);                     \
                a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, iz*CZ, cy, cx), cell); \
                a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, NEIGH_Z(idz, dir, iz, conf)*C2Z, cy, cx), cell); \
              }                                                         \
            }                                                           \
          } while (0)
#else
#define XYPLANE(dir,iz) do {                                            \
            unsigned id2 = (MOVE_BZ(idz,dir,conf)*nby + idy)*nbx + idx; \
            assert(a_blocks.ref(id).CELLS_X == a_blocks.ref(id2).CELLS_X); \
            assert(a_blocks.ref(id).CELLS_Y == a_blocks.ref(id2).CELLS_Y); \
            ptr_t next(base_ptr);                                       \
            coloring[color + dir] = create_logical_region(runtime, ctx, a_blocks.ref(id).CELLS_Y * a_blocks.ref(id).CELLS_X, sizeof(Cell)); \
            for (unsigned cy = 1; cy <= a_blocks.ref(id).CELLS_Y; cy++) { \
              for (unsigned cx = 1; cx <= a_blocks.ref(id).CELLS_X; cx++) { \
                ptr_t cell(next.value++);                               \
                a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, iz*CZ, cy, cx), cell); \
                a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, NEIGH_Z(idz, dir, iz, conf)*C2Z, cy, cx), cell); \
              }                                                         \
            }                                                           \
          } while (0)
#endif
          XYPLANE(TOP,    1);
          XYPLANE(BOTTOM, 0);
#undef XYPLANE

          // xz-plane edges
#ifdef PARTITIONED_EDGE_CELLS
#define XZPLANE(dir,iy) do {                                            \
            unsigned id2 = (idz*nby + MOVE_BY(idy,dir,conf))*nbx + idx; \
            assert(a_blocks.ref(id).CELLS_X == a_blocks.ref(id2).CELLS_X); \
            assert(a_blocks.ref(id).CELLS_Z == a_blocks.ref(id2).CELLS_Z); \
            for (unsigned cz = 1; cz <= a_blocks.ref(id).CELLS_Z; cz++) { \
              for (unsigned cx = 1; cx <= a_blocks.ref(id).CELLS_X; cx++) { \
                ptr_t cell(next.value++);                               \
                coloring[color + dir].points.insert(cell);                     \
                a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, cz, iy*CY, cx), cell); \
                a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, cz, NEIGH_Y(idy, dir, iy, conf)*C2Y, cx), cell); \
              }                                                         \
            }                                                           \
          } while (0)
#else
#define XZPLANE(dir,iy) do {                                            \
            unsigned id2 = (idz*nby + MOVE_BY(idy,dir,conf))*nbx + idx; \
            assert(a_blocks.ref(id).CELLS_X == a_blocks.ref(id2).CELLS_X); \
            assert(a_blocks.ref(id).CELLS_Z == a_blocks.ref(id2).CELLS_Z); \
            ptr_t next(base_ptr);                                       \
            coloring[color + dir] = create_logical_region(runtime, ctx, a_blocks.ref(id).CELLS_Z * a_blocks.ref(id).CELLS_X, sizeof(Cell)); \
            for (unsigned cz = 1; cz <= a_blocks.ref(id).CELLS_Z; cz++) { \
              for (unsigned cx = 1; cx <= a_blocks.ref(id).CELLS_X; cx++) { \
                ptr_t cell(next.value++);                               \
                a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, cz, iy*CY, cx), cell); \
                a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, cz, NEIGH_Y(idy, dir, iy, conf)*C2Y, cx), cell); \
              }                                                         \
            }                                                           \
          } while (0)
#endif
          XZPLANE(FRONT, 0);
          XZPLANE(BACK,  1);
#undef XZPLANE

          // yz-plane edges
#ifdef PARTITIONED_EDGE_CELLS
#define YZPLANE(dir,ix) do {                                            \
          unsigned id2 = (idz*nby + idy)*nbx + MOVE_BX(idx,dir,conf);   \
          assert(a_blocks.ref(id).CELLS_Y == a_blocks.ref(id2).CELLS_Y); \
          assert(a_blocks.ref(id).CELLS_Z == a_blocks.ref(id2).CELLS_Z); \
          for (unsigned cz = 1; cz <= a_blocks.ref(id).CELLS_Z; cz++) {  \
            for (unsigned cy = 1; cy <= a_blocks.ref(id).CELLS_Y; cy++) { \
              ptr_t cell(next.value++);                                 \
              coloring[color + dir].points.insert(cell);                       \
              a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, cz, cy, ix*CX), cell); \
              a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, cz, cy, NEIGH_X(idx, dir, ix, conf)*C2X), cell); \
            }                                                           \
          }                                                             \
        } while (0)
#else
#define YZPLANE(dir,ix) do {                                            \
          unsigned id2 = (idz*nby + idy)*nbx + MOVE_BX(idx,dir,conf);   \
          assert(a_blocks.ref(id).CELLS_Y == a_blocks.ref(id2).CELLS_Y); \
          assert(a_blocks.ref(id).CELLS_Z == a_blocks.ref(id2).CELLS_Z); \
          ptr_t next(base_ptr);                                         \
          coloring[color + dir] = create_logical_region(runtime, ctx, a_blocks.ref(id).CELLS_Z * a_blocks.ref(id).CELLS_Y, sizeof(Cell)); \
          for (unsigned cz = 1; cz <= a_blocks.ref(id).CELLS_Z; cz++) {  \
            for (unsigned cy = 1; cy <= a_blocks.ref(id).CELLS_Y; cy++) { \
              ptr_t cell(next.value++);                                 \
              a_block_ptrs[id].write(get_cell_ptr_ptr(a_blocks.ref(id), 0, cz, cy, ix*CX), cell); \
              a_block_ptrs[id2].write(get_cell_ptr_ptr(a_blocks.ref(id2), 1, cz, cy, NEIGH_X(idx, dir, ix, conf)*C2X), cell); \
            }                                                           \
          }                                                             \
        } while (0)
#endif
          YZPLANE(LEFT,  0);
          YZPLANE(RIGHT, 1);
#undef YZPLANE

#undef CX
#undef CY
#undef CZ
#undef C2X
#undef C2Y
#undef C2Z

          color += GHOST_CELLS;
        }

#ifdef PARTITIONED_EDGE_CELLS
    // now partition the edge cells
    IndexPartition isp_edge_cells =
      runtime->create_index_partition(ctx, edge_cells.get_index_space(),
                                      coloring, true/*disjoint*/);
    LogicalPartition lp_edge_cells =
      runtime->get_logical_partition(ctx, edge_cells, isp_edge_cells);
#endif

    // now go back through and store subregion handles in the right places
    for (unsigned b = 0; b < 2; b++) {
      block_edges[b].resize(numBlocks);
      for (unsigned id = 0; id < numBlocks; id++) {
        block_edges[b][id].resize(GHOST_CELLS);
      }
    }

    color = 0;
    for (unsigned idz = 0; idz < nbz; idz++)
      for (unsigned idy = 0; idy < nby; idy++)
        for (unsigned idx = 0; idx < nbx; idx++) {
          unsigned id = (idz*nby+idy)*nbx+idx;

          for (unsigned dir = 0; dir < GHOST_CELLS; dir++) {
            unsigned id2 = 
              (MOVE_BZ(idz, dir, conf)*nby +
               MOVE_BY(idy, dir, conf))*nbx +
              MOVE_BX(idx, dir, conf);
#ifdef PARTITIONED_EDGE_CELLS
            LogicalRegion subr =
              runtime->get_logical_subregion_by_color(ctx, lp_edge_cells,
                                                      color + dir);
#else
            LogicalRegion subr = coloring[color + dir];
#endif
            block_edges[0][id][dir] = subr;
            block_edges[1][id2][OPPOSITE_DIR(idz, idy, idx, dir, conf)] = subr;
          }

          color += GHOST_CELLS;
        }
  }

  // Unmap the physical regions we intend to pass to children
  runtime->unmap_region(ctx, pr_config);
  runtime->unmap_region(ctx, pr_blocks);
  runtime->unmap_region(ctx, pr_real_cells[0]);
  runtime->unmap_region(ctx, pr_real_cells[1]);
#ifdef PARTITIONED_EDGE_CELLS
  runtime->unmap_region(ctx, pr_edge_cells);
#endif
  for (unsigned id = 0; id < numBlocks; id++) {
    runtime->unmap_region(ctx, pr_block_ptrs[id]);
  }

  // Initialize the simulation in buffer 1
  {
    std::list<Future> load_futures;
    for (unsigned id = 0; id < numBlocks; id++) {
      unsigned buffer = id;
      TaskLauncher load(TASKID_LOAD_FILE,
                        TaskArgument((void *)&buffer, sizeof(buffer)),
                        Predicate::TRUE_PRED, 0, id);

      load.add_region_requirement(
        RegionRequirement(config, fieldset, fields, READ_ONLY, EXCLUSIVE, config));
      load.add_region_requirement(
        RegionRequirement(blocks, fieldset, fields, READ_ONLY, EXCLUSIVE, blocks));
      load.add_region_requirement(
        RegionRequirement(block_ptrs[id], fieldset, fields, READ_ONLY, EXCLUSIVE,
                          block_ptrs[id]));
      load.add_region_requirement(
        RegionRequirement(block_bases[1][id], fieldset, fields, READ_WRITE, EXCLUSIVE,
                          real_cells[1]));

      Future f = runtime->execute_task(ctx, load);
      load_futures.push_back(f);
    }

    while (load_futures.size() > 0) {
      load_futures.front().get_void_result();
      load_futures.pop_front();
    }
  }

  printf("STARTING MAIN SIMULATION LOOP\n");
  double start_time = LegionRuntime::TimeStamp::get_current_time_in_micros();
  std::list<Future> futures;
  LegionRuntime::DetailedTimer::clear_timers();

  int cur_buffer = 0;  // buffer we're generating on this pass
  // Run the simulation
  for (unsigned step = 0; step < conf.numSteps; step++) {

    // Trace runtime calls to amortize cost of analysis
    runtime->begin_trace(ctx, cur_buffer);

    // Initialize cells
    //for (unsigned id = 0; id < numBlocks; id++)
    for (int spass = conf.sync ? 1 : 0; spass >= 0; spass--) {
      std::list<Future> phase_futures;

      for (int id = (numBlocks-1); id >= 0; id--) {
        unsigned buf[2];
        buf[0] = cur_buffer;
        buf[1] = id;
        TaskLauncher init(TASKID_INIT_CELLS,
                          TaskArgument((void *)&buf, sizeof(buf)),
                          Predicate::TRUE_PRED, 0, id);

        // init and rebuild reads the real cells from the previous pass and
        //  moves atoms into the real cells for this pass or the edge0 cells
        init.add_region_requirement(
          RegionRequirement(config, fieldset, fields, READ_ONLY, EXCLUSIVE, config));
        init.add_region_requirement(
          RegionRequirement(blocks, fieldset, fields, READ_ONLY, EXCLUSIVE, blocks));
        init.add_region_requirement(
          RegionRequirement(block_ptrs[id], fieldset, fields, READ_ONLY, EXCLUSIVE,
                            block_ptrs[id]));

        // read old
        init.add_region_requirement(
          RegionRequirement(block_bases[1 - cur_buffer][id],
                            fieldset, fields,
                            READ_ONLY, EXCLUSIVE,
                            real_cells[1 - cur_buffer]));
        // write new
        init.add_region_requirement(
          RegionRequirement(block_bases[cur_buffer][id],
                            fieldset, fields,
                            READ_WRITE, EXCLUSIVE,
                            real_cells[cur_buffer]));

        // write edge0
#ifdef PARTITIONED_EDGE_CELLS
        add_all_regions(init, block_edges[0][id],
                        READ_WRITE, EXCLUSIVE, edge_cells);
#else
        add_all_regions(init, block_edges[0][id],
                        READ_WRITE, EXCLUSIVE, block_edges[0][id]);
#endif

        if (spass > 0) {
          // run a dummy task to move data in a separate phase
          init.task_id = TASKID_INIT_CELLS_NOP;
          Future f = runtime->execute_task(ctx, init);
          phase_futures.push_back(f);
        } else {
          Future f = runtime->execute_task(ctx, init);

          if (conf.sync) {
            phase_futures.push_back(f);
          }
        }
      }

      while (phase_futures.size() > 0) {
        Future f = phase_futures.front();
        phase_futures.pop_front();
        if (conf.sync <= 1) {
          f.get_void_result();
        }
      }
    }

    // Rebuild reduce (reduction)
    //for (unsigned id = 0; id < numBlocks; id++)
    for (int spass = conf.sync ? 1 : 0; spass >= 0; spass--) {
      std::list<Future> phase_futures;

      for (int id = (numBlocks-1); id >= 0; id--) {
        unsigned buf[2];
        buf[0] = cur_buffer;
        buf[1] = id;
        TaskLauncher rebuild(TASKID_REBUILD_REDUCE,
                             TaskArgument((void *)&buf, sizeof(buf)),
                             Predicate::TRUE_PRED, 0, id);

        // rebuild reduce reads the cells provided by neighbors, incorporates
        //  them into its own cells, and puts copies of those boundary cells into
        //  the ghosts to exchange back
        //
        // edge phase here is _1_

        rebuild.add_region_requirement(
          RegionRequirement(blocks, fieldset, fields, READ_ONLY, EXCLUSIVE, blocks));
        rebuild.add_region_requirement(
          RegionRequirement(block_ptrs[id], fieldset, fields, READ_ONLY, EXCLUSIVE,
                            block_ptrs[id]));
        rebuild.add_region_requirement(
          RegionRequirement(block_bases[cur_buffer][id],
                            fieldset, fields,
                            READ_WRITE, EXCLUSIVE,
                            real_cells[cur_buffer]));

        // write edge1
#ifdef PARTITIONED_EDGE_CELLS
        add_all_regions(rebuild, block_edges[1][id],
                        READ_WRITE, EXCLUSIVE, edge_cells);
#else
        add_all_regions(rebuild, block_edges[1][id],
                        READ_WRITE, EXCLUSIVE, block_edges[1][id]);
#endif

        if (spass > 0) {
          // run a dummy task to move data in a separate phase
          rebuild.task_id = TASKID_REBUILD_REDUCE_NOP;
          Future f = runtime->execute_task(ctx, rebuild);
          phase_futures.push_back(f);
        } else {
          Future f = runtime->execute_task(ctx, rebuild);

          if (conf.sync) {
            phase_futures.push_back(f);
          }
        }
      }

      while (phase_futures.size() > 0) {
        Future f = phase_futures.front();
        phase_futures.pop_front();
        if (conf.sync <= 1) {
          f.get_void_result();
        }
      }
    }

    // init forces and scatter densities
    //for (unsigned id = 0; id < numBlocks; id++)
    for (int spass = conf.sync ? 1 : 0; spass >= 0; spass--) {
      std::list<Future> phase_futures;

      for (int id = (numBlocks-1); id >= 0; id--) {
        unsigned buf[2];
        buf[0] = cur_buffer;
        buf[1] = id;
        TaskLauncher density(TASKID_SCATTER_DENSITIES,
                             TaskArgument((void *)&buf, sizeof(buf)),
                             Predicate::TRUE_PRED, 0, id);

        // this step looks at positions in real and edge cells and updates
        // densities for all owned particles - boundary real cells are copied to
        // the edge cells for exchange
        //
        // edge phase here is _0_

        density.add_region_requirement(
          RegionRequirement(config, fieldset, fields, READ_ONLY, EXCLUSIVE, config));
        density.add_region_requirement(
          RegionRequirement(blocks, fieldset, fields, READ_ONLY, EXCLUSIVE, blocks));
        density.add_region_requirement(
          RegionRequirement(block_ptrs[id], fieldset, fields, READ_ONLY, EXCLUSIVE,
                            block_ptrs[id]));
        density.add_region_requirement(
          RegionRequirement(block_bases[cur_buffer][id],
                            fieldset, fields,
                            READ_WRITE, EXCLUSIVE,
                            real_cells[cur_buffer]));

        // write edge1
#ifdef PARTITIONED_EDGE_CELLS
        add_all_regions(density, block_edges[0][id],
                        READ_WRITE, EXCLUSIVE, edge_cells);
#else
        add_all_regions(density, block_edges[0][id],
                        READ_WRITE, EXCLUSIVE, block_edges[0][id]);
#endif

        if (spass > 0) {
          // run a dummy task to move data in a separate phase
          density.task_id = TASKID_SCATTER_DENSITIES_NOP;
          Future f = runtime->execute_task(ctx, density);
          phase_futures.push_back(f);
        } else {
          Future f = runtime->execute_task(ctx, density);

          if (conf.sync) {
            phase_futures.push_back(f);
          }
        }
      }

      while (phase_futures.size() > 0) {
        Future f = phase_futures.front();
        phase_futures.pop_front();
        if (conf.sync <= 1) {
          f.get_void_result();
        }
      }
    }

    // Gather forces and advance
    //for (unsigned id = 0; id < numBlocks; id++)
    for (int spass = conf.sync ? 1 : 0; spass >= 0; spass--) {
      std::list<Future> phase_futures;

      for (int id = (numBlocks-1); id >= 0; id--) {
        unsigned buf[2];
        buf[0] = cur_buffer;
        buf[1] = id;
        TaskLauncher force(TASKID_GATHER_FORCES,
                           TaskArgument((void *)&buf, sizeof(buf)),
                           Predicate::TRUE_PRED, 0, id);

        // this is very similar to scattering of density - basically just
        //  different math, and a different edge phase
        // actually, since this fully calculates the accelerations, we just
        //  advance positions in this task as well and we're done with an
        //  iteration
        //
        // edge phase here is _1_

        force.add_region_requirement(
          RegionRequirement(config, fieldset, fields, READ_ONLY, EXCLUSIVE, config));
        force.add_region_requirement(
         RegionRequirement(blocks, fieldset, fields, READ_ONLY, EXCLUSIVE, blocks));
        force.add_region_requirement(
          RegionRequirement(block_ptrs[id], fieldset, fields, READ_ONLY, EXCLUSIVE,
                            block_ptrs[id]));
        force.add_region_requirement(
          RegionRequirement(block_bases[cur_buffer][id],
                            fieldset, fields,
                            READ_WRITE, EXCLUSIVE,
                            real_cells[cur_buffer]));

        // write edge1
#ifdef PARTITIONED_EDGE_CELLS
        add_all_regions(force, block_edges[1][id],
                        READ_WRITE, EXCLUSIVE, edge_cells);
#else
        add_all_regions(force, block_edges[1][id],
                        READ_WRITE, EXCLUSIVE, block_edges[1][id]);
#endif

        if (spass > 0) {
          // run a dummy task to move data in a separate phase
          force.task_id = TASKID_GATHER_FORCES_NOP;
          Future f = runtime->execute_task(ctx, force);
          phase_futures.push_back(f);
        } else {
          Future f = runtime->execute_task(ctx, force);

          // remember the futures for the last pass so we can wait on them
          if (step == conf.numSteps - 1) {
            futures.push_back(f);
          } else if (conf.sync) {
              phase_futures.push_back(f);
          }
        }
      }

      while (phase_futures.size() > 0) {
        Future f = phase_futures.front();
        phase_futures.pop_front();
        if (conf.sync <= 1) {
          f.get_void_result();
        }
      }
    }

    // Trace runtime calls to amortize cost of analysis
    runtime->end_trace(ctx, cur_buffer);

    // flip the phase
    if(!conf.repeat_first_timestep)
      cur_buffer = 1 - cur_buffer;
  }

  log_app.info("waiting for all simulation tasks to complete");

  while (futures.size() > 0) {
    futures.front().get_void_result();
    futures.pop_front();
  }
  double end_time = LegionRuntime::TimeStamp::get_current_time_in_micros();

  double sim_time = 1e-6 * (end_time - start_time);
  printf("ELAPSED TIME = %7.3f s\n", sim_time);
  LegionRuntime::DetailedTimer::report_timers();

  if (outFilename[0] != '\0') {
    int target_buffer = (conf.repeat_first_timestep ? cur_buffer : (1 - cur_buffer));

    TaskLauncher save(TASKID_SAVE_FILE,
                      TaskArgument((void *)&target_buffer,
                                   sizeof(target_buffer)));

    save.add_region_requirement(
      RegionRequirement(config, fieldset, fields, READ_ONLY, EXCLUSIVE, config));
    save.add_region_requirement(
      RegionRequirement(blocks, fieldset, fields, READ_ONLY, EXCLUSIVE, blocks));
    save.add_region_requirement(
      RegionRequirement(real_cells[target_buffer], fieldset, fields, READ_ONLY, EXCLUSIVE,
                        real_cells[target_buffer]));
    for (unsigned id = 0; id < numBlocks; id++) {
      save.add_region_requirement(
        RegionRequirement(block_ptrs[id], fieldset, fields, READ_ONLY, EXCLUSIVE,
                          block_ptrs[id]));
    }

    Future f = runtime->execute_task(ctx, save);
    f.get_void_result();
  }

  log_app.info("all done!");
}

static inline int GET_DIR(Block &b, int idz, int idy, int idx)
{
  return LOOKUP_DIR(idx == 0 ? -1 : (idx == (int)(b.CELLS_X+1) ? 1 : 0),
                    idy == 0 ? -1 : (idy == (int)(b.CELLS_Y+1) ? 1 : 0),
                    idz == 0 ? -1 : (idz == (int)(b.CELLS_Z+1) ? 1 : 0));
}

template<class AT>
static inline ptr_t& get_cell_ptr(Block &b, int cb, int cz, int cy, int cx,
                                  RegionAccessor<AT, ptr_t> &block_ptrs)
{
  return block_ptrs.ref(get_cell_ptr_ptr(b, cb, cz, cy, cx));
}

template<class AT>
static inline Cell& REF_CELL(Block &b, int cb, int eb, int cz, int cy, int cx,
                             RegionAccessor<AT, Cell> &base,
                             std::vector<RegionAccessor<AT, Cell> > &edge,
                             RegionAccessor<AT, ptr_t> &block_ptrs)
{
  int dir = GET_DIR(b, cz, cy,cx);
  if (dir == CENTER) {
    return base.ref(get_cell_ptr(b, cb, cz, cy, cx, block_ptrs));
  } else {
    return edge[dir].ref(get_cell_ptr(b, eb, cz, cy, cx, block_ptrs));
  }
}

template<class AT>
static inline void WRITE_CELL(Block &b, int cb, int eb, int cz, int cy, int cx,
                              RegionAccessor<AT, Cell> &base,
                              std::vector<RegionAccessor<AT, Cell> > &edge,
                              RegionAccessor<AT, ptr_t> &block_ptrs,
                              Cell &cell)
{
  int dir = GET_DIR(b, cz, cy,cx);
  if (dir == CENTER) {
    base.write(get_cell_ptr(b, cb, cz, cy, cx, block_ptrs), cell);
  } else {
    edge[dir].write(get_cell_ptr(b, eb, cz, cy, cx, block_ptrs), cell);
  }
}

template<class AT, class T>
static T& get_singleton_ref(PhysicalRegion physical)
{
  RegionAccessor<AT, T> accessor =
    physical.get_field_accessor(FIELD).typeify<T>().template convert<AT>();
  return accessor.ref(base_ptr);
}

template<class AT, class T>
static T& get_array_ref(PhysicalRegion physical, unsigned index)
{
  RegionAccessor<AT, T> accessor =
    physical.get_field_accessor(FIELD).typeify<T>().template convert<AT>();
  return accessor.ref(ptr_t(index));
}

void init_and_rebuild(const Task *task,
                      const std::vector<PhysicalRegion> &regions,
                      Context ctx, HighLevelRuntime *runtime)
{
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp total_stamp("Init and Rebuild total: ",true);
  LegionRuntime::TimeStamp start_stamp("Init and Rebuild start: ",false);
#endif
  assert(task->arglen == sizeof(unsigned)*2);
  int cb = ((const unsigned *)task->args)[0];
  unsigned bid = ((const unsigned *)task->args)[1];
  int eb = 0; // edge phase for this task is 0

  // Initialize all the cells and update all our cells
  PhysicalRegion config_region = regions[0];
  PhysicalRegion block_region = regions[1];
  PhysicalRegion block_ptrs_region = regions[2];
  PhysicalRegion src_block = regions[3];
  PhysicalRegion dst_block = regions[4];
  PhysicalRegion edge_blocks[GHOST_CELLS];
  for (unsigned i = 0; i < GHOST_CELLS; i++) {
    edge_blocks[i] = regions[i + 5];
  }

  RegionAccessor<SOA, ptr_t> block_ptrs =
    block_ptrs_region.get_field_accessor(FIELD).typeify<ptr_t>().convert<SOA>();
  RegionAccessor<SOA, Cell> src =
    src_block.get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();
  RegionAccessor<SOA, Cell> dst =
    dst_block.get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();
  std::vector<RegionAccessor<SOA, Cell> > edges(GHOST_CELLS);
  for (unsigned i = 0; i < GHOST_CELLS; i++) {
    edges[i] = edge_blocks[i].get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();
  }

  Config &conf = get_singleton_ref<SOA, Config>(config_region);
  unsigned &nx = conf.nx, &ny = conf.ny, &nz = conf.nz;
  //unsigned &nbx = conf.nbx, &nby = conf.nby, &nbz = conf.nbz;
  Vec3 &delta = conf.delta;

  Block &b = get_array_ref<SOA, Block>(block_region, bid);

  log_app.info("In init_and_rebuild() for block %d", b.id);

  // start by clearing the particle count on all the destination cells
  for (int cz = 0; cz <= (int)b.CELLS_Z + 1; cz++)
    for (int cy = 0; cy <= (int)b.CELLS_Y + 1; cy++)
      for (int cx = 0; cx <= (int)b.CELLS_X + 1; cx++) {
        REF_CELL(b, cb, eb, cz, cy, cx, dst, edges, block_ptrs).num_particles = 0;
      }

#if 0
  // Minimum block sizes
  unsigned mbsx = nx / nbx;
  unsigned mbsy = ny / nby;
  unsigned mbsz = nz / nbz;

  // Number of oversized blocks
  unsigned ovbx = nx % nbx;
  unsigned ovby = ny % nby;
  unsigned ovbz = nz % nbz;
#endif

  // now go through each source cell and move particles that have wandered too
  //  far
  for (int cz = 1; cz < (int)b.CELLS_Z + 1; cz++)
    for (int cy = 1; cy < (int)b.CELLS_Y + 1; cy++)
      for (int cx = 1; cx < (int)b.CELLS_X + 1; cx++) {
        // don't need to macro-ize this because it's known to be a real cell
        Cell &c_src = src.ref(get_cell_ptr(b, 1-cb, cz, cy, cx, block_ptrs));
        assert(c_src.num_particles <= MAX_PARTICLES);
        for (unsigned p = 0; p < c_src.num_particles; p++) {
          Vec3 pos = c_src.p[p];

          // Global dst coordinates
          int di = (int)((pos.x - domainMin.x) / delta.x);
          int dj = (int)((pos.y - domainMin.y) / delta.y);
          int dk = (int)((pos.z - domainMin.z) / delta.z);

          if (di < 0) di = 0; else if (di > (int)(nx-1)) di = nx-1;
          if (dj < 0) dj = 0; else if (dj > (int)(ny-1)) dj = ny-1;
          if (dk < 0) dk = 0; else if (dk > (int)(nz-1)) dk = nz-1;

          // Global src coordinates
#if 0
          int ci = cx + (b.x*mbsx + (b.x < ovbx ? b.x : ovbx)) - 1;
          int cj = cy + (b.y*mbsy + (b.y < ovby ? b.y : ovby)) - 1;
          int ck = cz + (b.z*mbsz + (b.z < ovbz ? b.z : ovbz)) - 1;
#endif
          int ci = cx + conf.splitx[b.x] - 1;
          int cj = cy + conf.splity[b.y] - 1;
          int ck = cz + conf.splitz[b.z] - 1;

          // Assume particles move no more than one block per timestep
          if (di - ci < -1) di = ci - 1; else if (di - ci > 1) di = ci + 1;
          if (dj - cj < -1) dj = cj - 1; else if (dj - cj > 1) dj = cj + 1;
          if (dk - ck < -1) dk = ck - 1; else if (dk - ck > 1) dk = ck + 1;
          int dx = cx + (di - ci);
          int dy = cy + (dj - cj);
          int dz = cz + (dk - ck);

          Cell &c_dst = REF_CELL(b, cb, eb, dz, dy, dx, dst, edges, block_ptrs);
          if (c_dst.num_particles < MAX_PARTICLES) {
            int dp = c_dst.num_particles++;

            // just have to copy p, hv, v
            c_dst.p[dp] = pos;
            c_dst.hv[dp] = c_src.hv[p];
            c_dst.v[dp] = c_src.v[p];
#ifdef TRACK_PARTICLE_IDS
	    c_dst.id[dp] = c_src.id[p];
#endif
          }
        }
      }

  log_app.info("Done with init_and_rebuild() for block %d", b.id);
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp finish_stamp("Init and Rebuild stop: ",false);
#endif
}

void rebuild_reduce(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, HighLevelRuntime *runtime)
{
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp total_stamp("Rebuild Reduce total: ",true);
  LegionRuntime::TimeStamp start_stamp("Rebuild Reduce start: ",false);
#endif
  assert(task->arglen == sizeof(unsigned)*2);
  int cb = ((const unsigned *)task->args)[0];
  unsigned bid = ((const unsigned *)task->args)[1];
  int eb = 1; // edge phase for this task is 1

  // Initialize all the cells and update all our cells
  PhysicalRegion block_region = regions[0];
  PhysicalRegion block_ptrs_region = regions[1];
  PhysicalRegion base_block = regions[2];
  PhysicalRegion edge_blocks[GHOST_CELLS];
  for (unsigned i = 0; i < GHOST_CELLS; i++) {
    edge_blocks[i] = regions[i + 3];
  }

  RegionAccessor<SOA, ptr_t> block_ptrs =
    block_ptrs_region.get_field_accessor(FIELD).typeify<ptr_t>().convert<SOA>();
  RegionAccessor<SOA, Cell> base =
    base_block.get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();
  std::vector<RegionAccessor<SOA, Cell> > edges(GHOST_CELLS);
  for (unsigned i = 0; i < GHOST_CELLS; i++) {
    edges[i] = edge_blocks[i].get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();
  }

  Block &b = get_array_ref<SOA, Block>(block_region, bid);

  log_app.info("In rebuild_reduce() for block %d", b.id);

  // for each edge cell, copy inward
  for (int cz = 0; cz <= (int)b.CELLS_Z+1; cz++)
    for (int cy = 0; cy <= (int)b.CELLS_Y+1; cy++)
      for (int cx = 0; cx <= (int)b.CELLS_X+1; cx++) {
        int dir = GET_DIR(b, cz, cy, cx);
        if (dir == CENTER) continue;
        int dz = MOVE_CZ(b, cz, REVERSE(dir));
        int dy = MOVE_CY(b, cy, REVERSE(dir));
        int dx = MOVE_CX(b, cx, REVERSE(dir));

        Cell &c_src = REF_CELL(b, cb, eb, cz, cy, cx, base, edges, block_ptrs);
        Cell &c_dst = base.ref(get_cell_ptr(b, cb, dz, dy, dx, block_ptrs));

        for (unsigned p = 0; p < c_src.num_particles; p++) {
          if (c_dst.num_particles == MAX_PARTICLES) break;
          int dp = c_dst.num_particles++;
          // just have to copy p, hv, v
          c_dst.p[dp] = c_src.p[p];
          c_dst.hv[dp] = c_src.hv[p];
          c_dst.v[dp] = c_src.v[p];
        }
      }

  // now turn around and have each edge grab a copy of the boundary real cell
  //  to share for the next step
  for (int cz = 0; cz <= (int)b.CELLS_Z+1; cz++)
    for (int cy = 0; cy <= (int)b.CELLS_Y+1; cy++)
      for (int cx = 0; cx <= (int)b.CELLS_X+1; cx++) {
        int dir = GET_DIR(b, cz, cy, cx);
        if (dir == CENTER) continue;
        int dz = MOVE_CZ(b, cz, REVERSE(dir));
        int dy = MOVE_CY(b, cy, REVERSE(dir));
        int dx = MOVE_CX(b, cx, REVERSE(dir));

        Cell &cell = base.ref(get_cell_ptr(b, cb, dz, dy, dx, block_ptrs));
        WRITE_CELL(b, cb, eb, cz, cy, cx, base, edges, block_ptrs, cell);
      }

  log_app.info("Done with rebuild_reduce() for block %d", b.id);
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp stop_stamp("Rebuild Reduce stop ",false);
#endif
}

void scatter_densities(const Task *task,
                       const std::vector<PhysicalRegion> &regions,
                       Context ctx, HighLevelRuntime *runtime)
{
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp total_stamp("Scatter Densities total: ",true);
  LegionRuntime::TimeStamp start_stamp("Scatter Densities start: ",false);
#endif
  assert(task->arglen == sizeof(unsigned)*2);
  int cb = ((const unsigned *)task->args)[0];
  unsigned bid = ((const unsigned *)task->args)[1];
  int eb = 0; // edge phase for this task is 0

  // Initialize all the cells and update all our cells
  PhysicalRegion config_region = regions[0];
  PhysicalRegion block_region = regions[1];
  PhysicalRegion block_ptrs_region = regions[2];
  PhysicalRegion base_block = regions[3];
  PhysicalRegion edge_blocks[GHOST_CELLS];
  for (unsigned i = 0; i < GHOST_CELLS; i++) {
    edge_blocks[i] = regions[i + 4];
  }

  RegionAccessor<SOA, ptr_t> block_ptrs =
    block_ptrs_region.get_field_accessor(FIELD).typeify<ptr_t>().convert<SOA>();
  RegionAccessor<SOA, Cell> base =
    base_block.get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();
  std::vector<RegionAccessor<SOA, Cell> > edges(GHOST_CELLS);
  for (unsigned i = 0; i < GHOST_CELLS; i++) {
    edges[i] = edge_blocks[i].get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();
  }

  Config &conf = get_singleton_ref<SOA, Config>(config_region);
  unsigned &nbx = conf.nbx, &nby = conf.nby, &nbz = conf.nbz;
  float &hSq = conf.hSq;
  float &densityCoeff = conf.densityCoeff;

  Block &b = get_array_ref<SOA, Block>(block_region, bid);

  log_app.info("In scatter_densities() for block %d", b.id);

  // first, clear our density (and acceleration, while we're at it) values
  for (int cz = 1; cz < (int)b.CELLS_Z+1; cz++)
    for (int cy = 1; cy < (int)b.CELLS_Y+1; cy++)
      for (int cx = 1; cx < (int)b.CELLS_X+1; cx++) {
        Cell &cell = base.ref(get_cell_ptr(b, cb, cz, cy, cx, block_ptrs));
        for (unsigned p = 0; p < cell.num_particles; p++) {
          cell.density[p] = 0;
          cell.a[p] = externalAcceleration;
        }
      }

  // now for each cell, look at neighbors and calculate density contributions
  // two things to watch out for:
  //  * for pairs of real cells, we can do the calculation once instead of twice
  //  * edge cells on outer blocks may not contain valid data
  int minx = b.x == 0 ? 1 : 0;
  int miny = b.y == 0 ? 1 : 0;
  int minz = b.z == 0 ? 1 : 0;
  int maxx = b.x == nbx-1 ? b.CELLS_X : b.CELLS_X+1;
  int maxy = b.y == nby-1 ? b.CELLS_Y : b.CELLS_Y+1;
  int maxz = b.z == nbz-1 ? b.CELLS_Z : b.CELLS_Z+1;

  for (int cz = minz; cz <= maxz; cz++)
    for (int cy = miny; cy <= maxy; cy++)
      for (int cx = minx; cx <= maxx; cx++) {
        Cell &cell = REF_CELL(b, cb, eb, cz, cy, cx, base, edges, block_ptrs);
        assert(cell.num_particles <= MAX_PARTICLES);
	if(cell.num_particles == 0) continue;

        for (int dz = cz - 1; dz <= cz + 1; dz++)
          for (int dy = cy - 1; dy <= cy + 1; dy++)
            for (int dx = cx - 1; dx <= cx + 1; dx++) {
              // did we already get updated by this neighbor's bidirectional update?
              if (//dz < 1 || dy < 1 || dx < 1 ||
                  dz < minz || dy < miny || dx < minx ||
                  dz > maxz || dy > maxy || dx > maxx ||
                  (dz < cz || (dz == cz && (dy < cy || (dy == cy && dx < cx)))))
                continue;

              Cell &c2 = REF_CELL(b, cb, eb, dz, dy, dx, base, edges, block_ptrs);
              assert(c2.num_particles <= MAX_PARTICLES);
	      if(c2.num_particles == 0) continue;

              // do bidirectional update if other cell is a real cell and it is
              //  either below or to the right (but not up-right) of us
              bool update_other =
                true;//dz < (int)b.CELLS_Z+1 && dy < (int)b.CELLS_Y+1 && dx < (int)b.CELLS_X+1;

              // pairwise across particles - watch out for identical particle case!

              for (unsigned p = 0; p < cell.num_particles; p++)
                for (unsigned p2 = 0; p2 < c2.num_particles; p2++) {
                  if ((dx == cx) && (dy == cy) && (dz == cz) && (p <= p2)) continue;

                  Vec3 pdiff = cell.p[p] - c2.p[p2];
                  float distSq = pdiff.GetLengthSq();
#ifdef TRACK_PARTICLE_IDS
		  printf("DIST: %d->%d: (%f,%f,%f) = %f\n",
			 cell.id[p], c2.id[p2], pdiff.x, pdiff.y, pdiff.z, distSq);
#endif
                  if (distSq >= hSq) continue;

                  float t = hSq - distSq;
                  float tc = t*t*t;

                  cell.density[p] += tc;
                  if (update_other)
                    c2.density[p2] += tc;
                }
            }

        // a little offset for every particle once we're done
        const float tc = hSq*hSq*hSq;
        for (unsigned p = 0; p < cell.num_particles; p++) {
          cell.density[p] += tc;
          cell.density[p] *= densityCoeff;
        }

      }

  // now turn around and have each edge grab a copy of the boundary real cell
  //  to share for the next step
  for (int cz = 0; cz <= (int)b.CELLS_Z+1; cz++)
    for (int cy = 0; cy <= (int)b.CELLS_Y+1; cy++)
      for (int cx = 0; cx <= (int)b.CELLS_X+1; cx++) {
        int dir = GET_DIR(b, cz, cy, cx);
        if (dir == CENTER) continue;
        int dz = MOVE_CZ(b, cz, REVERSE(dir));
        int dy = MOVE_CY(b, cy, REVERSE(dir));
        int dx = MOVE_CX(b, cx, REVERSE(dir));

        Cell &cell = base.ref(get_cell_ptr(b, cb, dz, dy, dx, block_ptrs));
        WRITE_CELL(b, cb, eb, cz, cy, cx, base, edges, block_ptrs, cell);
      }

  log_app.info("Done with scatter_densities() for block %d", b.id);
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp stop_stamp("Scatter Densities stop ",false);
#endif
}

void calc_accelerations(Cell& cell, Cell& c2, bool same_cell,
			float hSq, float h, float pressureCoeff, float doubleRestDensity,
			float viscosityCoeff)
{
  // pairwise across particles - watch out for identical particle case!
  for (unsigned p = 0; p < cell.num_particles; p++)
    for (unsigned p2 = 0; p2 < c2.num_particles; p2++) {
      if (same_cell && (p <= p2)) continue;
      
      Vec3 disp = cell.p[p] - c2.p[p2];
      float distSq = disp.GetLengthSq();
      if (distSq >= hSq) continue;

      float dist = sqrtf(std::max(distSq, 1e-12f));
      float hmr = h - dist;

      Vec3 acc = (disp * pressureCoeff * (hmr*hmr/dist) * 
		  (cell.density[p] + c2.density[p2] - doubleRestDensity));
      acc += (c2.v[p2] - cell.v[p]) * viscosityCoeff * hmr;
      acc /= cell.density[p] * c2.density[p2];

#ifdef TRACK_PARTICLE_IDS
      printf("ACC: %d->%d = (%f,%f,%f)\n",
	     cell.id[p], c2.id[p2], acc.x, acc.y, acc.z);
#endif

      cell.a[p] += acc;
      c2.a[p2] -= acc;
    }
}

void gather_forces_and_advance(const Task *task,
                               const std::vector<PhysicalRegion> &regions,
                               Context ctx, HighLevelRuntime *runtime)
{
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp total_stamp("Gather Forces total: ",true);
  LegionRuntime::TimeStamp start_stamp("Gather Forces start: ",false);
#endif
  assert(task->arglen == sizeof(unsigned)*2);
  int cb = ((const unsigned *)task->args)[0];
  unsigned bid = ((const unsigned *)task->args)[1];
  int eb = 1; // edge phase for this task is 1

  // Initialize all the cells and update all our cells
  PhysicalRegion config_region = regions[0];
  PhysicalRegion block_region = regions[1];
  PhysicalRegion block_ptrs_region = regions[2];
  PhysicalRegion base_block = regions[3];
  PhysicalRegion edge_blocks[GHOST_CELLS];
  for (unsigned i = 0; i < GHOST_CELLS; i++) {
    edge_blocks[i] = regions[i + 4];
  }

  RegionAccessor<SOA, ptr_t> block_ptrs =
    block_ptrs_region.get_field_accessor(FIELD).typeify<ptr_t>().convert<SOA>();
  RegionAccessor<SOA, Cell> base =
    base_block.get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();
  std::vector<RegionAccessor<SOA, Cell> > edges(GHOST_CELLS);
  for (unsigned i = 0; i < GHOST_CELLS; i++) {
    edges[i] = edge_blocks[i].get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();
  }

  Config &conf = get_singleton_ref<SOA, Config>(config_region);
  unsigned &nbx = conf.nbx, &nby = conf.nby, &nbz = conf.nbz;
  float &h = conf.h, &hSq = conf.hSq;
  float &pressureCoeff = conf.pressureCoeff;
  float &viscosityCoeff = conf.viscosityCoeff;

  Block &b = get_array_ref<SOA, Block>(block_region, bid);

  log_app.info("In gather_forces_and_advance() for block %d", b.id);

  // acceleration was cleared out for us in the previous step

  // now for each cell, look at neighbors and calculate acceleration
  // one thing to watch out for:
  //  * for pairs of real cells, we can do the calculation once instead of twice
  int minx = b.x == 0 ? 1 : 0;
  int miny = b.y == 0 ? 1 : 0;
  int minz = b.z == 0 ? 1 : 0;
  int maxx = b.x == nbx-1 ? b.CELLS_X : b.CELLS_X+1;
  int maxy = b.y == nby-1 ? b.CELLS_Y : b.CELLS_Y+1;
  int maxz = b.z == nbz-1 ? b.CELLS_Z : b.CELLS_Z+1;
  for (int cz = minz; cz <= maxz; cz++)
    for (int cy = miny; cy <= maxy; cy++)
      for (int cx = minx; cx <= maxx; cx++) {
        Cell &cell = REF_CELL(b, cb, eb, cz, cy, cx, base, edges, block_ptrs);
        assert(cell.num_particles <= MAX_PARTICLES);
	if(cell.num_particles == 0) continue;

#ifdef TRACK_PARTICLE_IDS
	for(int p = 0; p < cell.num_particles; p++)
	  printf("DENS: %d = %f\n", cell.id[p], cell.density[p]);
#endif

        for (int dz = cz - 1; dz <= cz + 1; dz++)
          for (int dy = cy - 1; dy <= cy + 1; dy++)
            for (int dx = cx - 1; dx <= cx + 1; dx++) {
              // did we already get updated by this neighbor's bidirectional update?
              if (//dz < 1 || dy < 1 || dx < 1 ||
                  dz < minz || dy < miny || dx < minx ||
                  dz > maxz || dy > maxy || dx > maxx ||
                  (dz < cz || (dz == cz && (dy < cy || (dy == cy && dx < cx)))))
                continue;

              Cell &c2 = REF_CELL(b, cb, eb, dz, dy, dx, base, edges, block_ptrs);
              assert(c2.num_particles <= MAX_PARTICLES);
	      if(c2.num_particles == 0) continue;

              // do bidirectional update if other cell is a real cell and it is
              //  either below or to the right (but not up-right) of us
              // bool update_other =
              //   true;//dz < (int)b.CELLS_Z+1 && dy < (int)b.CELLS_Y+1 && dx < (int)b.CELLS_X+1;

              // pairwise across particles - watch out for identical particle case!
	      calc_accelerations(cell, c2, ((dx == cx) && (dy == cy) && (dz == cz)),
				 hSq, h, pressureCoeff, doubleRestDensity,
				 viscosityCoeff);
            }

        // compute collisions for particles near edge of box
        const float parSize = 0.0002f;
        const float epsilon = 1e-10f;
        const float stiffness = 30000.f;
        const float damping = 128.f;
        for (unsigned p = 0; p < cell.num_particles; p++) {
          Vec3 pos = cell.p[p] + cell.hv[p] * timeStep;
          float diff = parSize - (pos.x - domainMin.x);
          if (diff > epsilon)
            cell.a[p].x += stiffness*diff - damping*cell.v[p].x;

          diff = parSize - (domainMax.x - pos.x);
          if (diff > epsilon)
            cell.a[p].x -= stiffness*diff + damping*cell.v[p].x;

          diff = parSize - (pos.y - domainMin.y);
          if (diff > epsilon)
            cell.a[p].y += stiffness*diff - damping*cell.v[p].y;

          diff = parSize - (domainMax.y - pos.y);
          if (diff > epsilon)
            cell.a[p].y -= stiffness*diff + damping*cell.v[p].y;

          diff = parSize - (pos.z - domainMin.z);
          if (diff > epsilon)
            cell.a[p].z += stiffness*diff - damping*cell.v[p].z;

          diff = parSize - (domainMax.z - pos.z);
          if (diff > epsilon)
            cell.a[p].z -= stiffness*diff + damping*cell.v[p].z;
        }

        // we have everything we need to go ahead and update positions, so
        //  do that here instead of in a different task
        for (unsigned p = 0; p < cell.num_particles; p++) {
          Vec3 v_half = cell.hv[p] + cell.a[p]*timeStep;
          cell.p[p] += v_half * timeStep;
          cell.v[p] = cell.hv[p] + v_half;
          cell.v[p] *= 0.5f;
          cell.hv[p] = v_half;
#ifdef TRACK_PARTICLE_IDS
	  printf("ADV: %d: a=(%f,%f,%f) -> p=(%f,%f,%f) hv=(%f,%f,%f) v=(%f,%f,%f)\n",
		 cell.id[p],
		 cell.a[p].x, cell.a[p].y, cell.a[p].z, 
		 cell.p[p].x, cell.p[p].y, cell.p[p].z, 
		 cell.hv[p].x, cell.hv[p].y, cell.hv[p].z, 
		 cell.v[p].x, cell.v[p].y, cell.v[p].z);
#endif
        }

      }

  log_app.info("Done with gather_forces_and_advance() for block %d", b.id);
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp stop_stamp("Gather Forces stop ",false);
#endif
}

static inline int isLittleEndian() {
  union {
    uint16_t word;
    uint8_t byte;
  } endian_test;

  endian_test.word = 0x00FF;
  return (endian_test.byte == 0xFF);
}

union __float_and_int {
  uint32_t i;
  float    f;
};

static inline float bswap_float(float x) {
  union __float_and_int __x;

   __x.f = x;
   __x.i = ((__x.i & 0xff000000) >> 24) | ((__x.i & 0x00ff0000) >>  8) |
           ((__x.i & 0x0000ff00) <<  8) | ((__x.i & 0x000000ff) << 24);

  return __x.f;
}

static inline int bswap_int32(int x) {
  return ( (((x) & 0xff000000) >> 24) | (((x) & 0x00ff0000) >>  8) |
           (((x) & 0x0000ff00) <<  8) | (((x) & 0x000000ff) << 24) );
}

static void load_file_header(const char *fileName, Config &conf)
{
  std::ifstream file(fileName, std::ios::binary);
  assert(file);

  float &restParticlesPerMeter = conf.restParticlesPerMeter;
  int &origNumParticles = conf.origNumParticles;
  file.read((char *)&restParticlesPerMeter, 4);
  file.read((char *)&origNumParticles, 4);
  if (!isLittleEndian()) {
    restParticlesPerMeter = bswap_float(restParticlesPerMeter);
    origNumParticles      = bswap_int32(origNumParticles);
  }
}

static int read_int(std::ifstream& i)
{
  int val;
  i.read((char *)&val, 4);
  if (!isLittleEndian())
    val = bswap_int32(val);
  return val;
}

static float read_float(std::ifstream& i)
{
  float val;
  i.read((char *)&val, 4);
  if (!isLittleEndian())
    val = bswap_float(val);
  return val;
}

static void split_evenly(int pieces, int total,
                         const std::vector<int>& buckets,
                         int *splits)
{
  splits[0] = 0;
  splits[pieces] = buckets.size();

  int cum = 0;
  int pos = 1;
  for (size_t i = 0; (i < buckets.size()) && (pos < pieces); i++) {
    cum += buckets[i];
    if ((cum * pieces) >= (pos * total))
      splits[pos++] = i + 1;
  }
}

void analyze_particle_distribution(Config& conf)
{
  std::ifstream file(conf.inFilename, std::ios::binary);
  assert(file);

  /*float restParticlesPerMeter =*/ read_float(file);
  int origNumParticles = read_int(file);
  
  std::vector<int> hist_x(conf.nx, 0);
  std::vector<int> hist_y(conf.ny, 0);
  std::vector<int> hist_z(conf.nz, 0);

  for (int i = 0; i < origNumParticles; ++i) {
    float px = read_float(file);
    float py = read_float(file);
    float pz = read_float(file);
    char junk[24];
    file.read(junk, 24);

    // Global cell coordinates
    int ci = (int)((px - domainMin.x) / conf.delta.x);
    int cj = (int)((py - domainMin.y) / conf.delta.y);
    int ck = (int)((pz - domainMin.z) / conf.delta.z);

    if (ci < 0) ci = 0; else if (ci > (int)(conf.nx-1)) ci = conf.nx-1;
    if (cj < 0) cj = 0; else if (cj > (int)(conf.ny-1)) cj = conf.ny-1;
    if (ck < 0) ck = 0; else if (ck > (int)(conf.nz-1)) ck = conf.nz-1;

    hist_x[ci]++;
    hist_y[cj]++;
    hist_z[ck]++;
  }

  split_evenly(conf.nbx, origNumParticles, hist_x, conf.splitx);
  split_evenly(conf.nby, origNumParticles, hist_y, conf.splity);
  split_evenly(conf.nbz, origNumParticles, hist_z, conf.splitz);

  printf("X splits:");
  for (unsigned i = 0; i <= conf.nbx; i++) printf(" %d", conf.splitx[i]);
  printf("\n");

  printf("Y splits:");
  for (unsigned i = 0; i <= conf.nby; i++) printf(" %d", conf.splity[i]);
  printf("\n");

  printf("Z splits:");
  for (unsigned i = 0; i <= conf.nbz; i++) printf(" %d", conf.splitz[i]);
  printf("\n");

#if 0
  printf("X histogram\n");
  for (unsigned i = 0; i < conf.nx; i++)
    printf("%3d: %6d  %8.4f\n", i, hist_x[i], 100.0 * hist_x[i] * conf.nx / numParticles);
  printf("\n");

  printf("Y histogram\n");
  for (unsigned i = 0; i < conf.ny; i++)
    printf("%3d: %6d  %8.4f\n", i, hist_y[i], 100.0 * hist_y[i] * conf.ny / numParticles);
  printf("\n");

  printf("Z histogram\n");
  for (unsigned i = 0; i < conf.nz; i++)
    printf("%3d: %6d  %8.4f\n", i, hist_z[i], 100.0 * hist_z[i] * conf.nz / numParticles);
  printf("\n");
#endif
}

/*
 * Expects load_file_header and init_config to have been called on the
 * incoming Config already.
 */
void load_file(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(unsigned));
  unsigned id = ((const unsigned *)task->args)[0];

  PhysicalRegion config_region = regions[0];
  PhysicalRegion block_region = regions[1];
  PhysicalRegion block_ptrs_region = regions[2];
  PhysicalRegion real_cells_region = regions[3];
  RegionAccessor<SOA, ptr_t> block_ptrs =
    block_ptrs_region.get_field_accessor(FIELD).typeify<ptr_t>().convert<SOA>();
  RegionAccessor<SOA, Cell> real_cells =
    real_cells_region.get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();

  Config conf = get_singleton_ref<SOA, Config>(config_region);
  float &restParticlesPerMeter = conf.restParticlesPerMeter;
  int &origNumParticles = conf.origNumParticles, numParticles;
  unsigned &nx = conf.nx, &ny = conf.ny, &nz = conf.nz;
  unsigned &nbx = conf.nbx, &nby = conf.nby;//, &nbz = conf.nbz;
  Vec3 &delta = conf.delta;
  char (&inFilename)[256] = conf.inFilename;

  Block &b = get_array_ref<SOA, Block>(block_region, id);

  log_app.info("Loading file \"%s\"...", inFilename);

  const int cb = 1;

  // Clear all cells
  for (unsigned cz = 0; cz < b.CELLS_Z; cz++)
    for (unsigned cy = 0; cy < b.CELLS_Y; cy++)
      for (unsigned cx = 0; cx < b.CELLS_X; cx++) {
        Cell &cell = real_cells.ref(get_cell_ptr(b, cb, cz+1, cy+1, cx+1, block_ptrs));
        cell.num_particles = 0;
      }

  std::ifstream file(inFilename, std::ios::binary);
  assert(file);

  file.read((char *)&restParticlesPerMeter, 4);
  file.read((char *)&origNumParticles, 4);
  if (!isLittleEndian()) {
    restParticlesPerMeter = bswap_float(restParticlesPerMeter);
    origNumParticles      = bswap_int32(origNumParticles);
  }
  numParticles = origNumParticles;

#if 0
  // Minimum block sizes
  int mbsx = nx / nbx;
  int mbsy = ny / nby;
  int mbsz = nz / nbz;

  // Number of oversized blocks
  int ovbx = nx % nbx;
  int ovby = ny % nby;
  int ovbz = nz % nbz;
#endif

  float px, py, pz, hvx, hvy, hvz, vx, vy, vz;
  for (int i = 0; i < origNumParticles; ++i) {
    file.read((char *)&px, 4);
    file.read((char *)&py, 4);
    file.read((char *)&pz, 4);
    file.read((char *)&hvx, 4);
    file.read((char *)&hvy, 4);
    file.read((char *)&hvz, 4);
    file.read((char *)&vx, 4);
    file.read((char *)&vy, 4);
    file.read((char *)&vz, 4);
    if (!isLittleEndian()) {
      px  = bswap_float(px);
      py  = bswap_float(py);
      pz  = bswap_float(pz);
      hvx = bswap_float(hvx);
      hvy = bswap_float(hvy);
      hvz = bswap_float(hvz);
      vx  = bswap_float(vx);
      vy  = bswap_float(vy);
      vz  = bswap_float(vz);
    }

    // Global cell coordinates
    int ci = (int)((px - domainMin.x) / delta.x);
    int cj = (int)((py - domainMin.y) / delta.y);
    int ck = (int)((pz - domainMin.z) / delta.z);

    if (ci < 0) ci = 0; else if (ci > (int)(nx-1)) ci = nx-1;
    if (cj < 0) cj = 0; else if (cj > (int)(ny-1)) cj = ny-1;
    if (ck < 0) ck = 0; else if (ck > (int)(nz-1)) ck = nz-1;

    // Block coordinates and id
    int idx = 0; while (ci >= conf.splitx[idx+1]) idx++;
    int idy = 0; while (cj >= conf.splity[idy+1]) idy++;
    int idz = 0; while (ck >= conf.splitz[idz+1]) idz++;

    unsigned target_id = (idz*nby+idy)*nbx+idx;

    if (target_id != id) continue;

    // Local cell coordinates
    int cx = ci - conf.splitx[idx];
    int cy = cj - conf.splity[idy];
    int cz = ck - conf.splitz[idz];

    Cell &cell = real_cells.ref(get_cell_ptr(b, cb, cz+1, cy+1, cx+1, block_ptrs));

    unsigned np = cell.num_particles;
    if (np < MAX_PARTICLES) {
      cell.p[np].x = px;
      cell.p[np].y = py;
      cell.p[np].z = pz;
      cell.hv[np].x = hvx;
      cell.hv[np].y = hvy;
      cell.hv[np].z = hvz;
      cell.v[np].x = vx;
      cell.v[np].y = vy;
      cell.v[np].z = vz;
#ifdef TRACK_PARTICLE_IDS
      cell.id[np] = i;
#endif
      ++cell.num_particles;
    } else {
      --numParticles;
    }

  }

  log_app.info("Number of particles: %d (%d skipped)",
               numParticles, origNumParticles - numParticles);

  log_app.info("Done loading file.");
}

void save_file(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(unsigned));
  unsigned b = ((const unsigned *)task->args)[0];

  PhysicalRegion config_region = regions[0];
  Config conf = get_singleton_ref<SOA, Config>(config_region);
  unsigned &numBlocks = conf.numBlocks;

  PhysicalRegion blocks_region = regions[1];
  PhysicalRegion real_cells_region = regions[2];
  std::vector<PhysicalRegion> block_ptrs_region;
  for (unsigned id = 0; id < numBlocks; id++) {
    block_ptrs_region.push_back(regions[3 + id]);
  }

  RegionAccessor<SOA, Block> blocks =
    blocks_region.get_field_accessor(FIELD).typeify<Block>().convert<SOA>();
  RegionAccessor<SOA, Cell> real_cells =
    real_cells_region.get_field_accessor(FIELD).typeify<Cell>().convert<SOA>();
  std::vector<RegionAccessor<SOA, ptr_t> > block_ptrs(numBlocks);
  for (unsigned id = 0; id < numBlocks; id++) {
    block_ptrs[id] = block_ptrs_region[id].get_field_accessor(FIELD).typeify<ptr_t>().convert<SOA>();
  }

  float &restParticlesPerMeter = conf.restParticlesPerMeter;
  int &origNumParticles = conf.origNumParticles;
  unsigned &nx = conf.nx, &ny = conf.ny, &nz = conf.nz;
  unsigned &nbx = conf.nbx, &nby = conf.nby;//, &nbz = conf.nbz;
  char (&outFilename)[256] = conf.outFilename;

  log_app.info("Saving file \"%s\"...", outFilename);

  std::ofstream file(outFilename, std::ios::binary);
  assert(file);

  if (!isLittleEndian()) {
    float restParticlesPerMeter_le;
    int   origNumParticles_le;

    restParticlesPerMeter_le = bswap_float(restParticlesPerMeter);
    origNumParticles_le      = bswap_int32(origNumParticles);
    file.write((char *)&restParticlesPerMeter_le, 4);
    file.write((char *)&origNumParticles_le,      4);
  } else {
    file.write((char *)&restParticlesPerMeter, 4);
    file.write((char *)&origNumParticles,      4);
  }

#if 0
  // Minimum block sizes
  int mbsx = nx / nbx;
  int mbsy = ny / nby;
  int mbsz = nz / nbz;

  // Number of oversized blocks
  int ovbx = nx % nbx;
  int ovby = ny % nby;
  int ovbz = nz % nbz;
#endif

  int numParticles = 0;
  for (int ck = 0; ck < (int)nz; ck++)
    for (int cj = 0; cj < (int)ny; cj++)
      for (int ci = 0; ci < (int)nx; ci++) {

#if 0
        // Block coordinates and id
        int midx = ci / (mbsx + 1) + 1;
        int eci = ci + (midx > ovbx ? midx - ovbx : 0);
        int idx = eci / (mbsx + 1);
        int midy = cj / (mbsy + 1) + 1;
        int ecj = cj + (midy > ovby ? midy - ovby : 0);
        int idy = ecj / (mbsy + 1);
        int midz = ck / (mbsz + 1) + 1;
        int eck = ck + (midz > ovbz ? midz - ovbz : 0);
        int idz = eck / (mbsz + 1);

        int id = (idz*nby+idy)*nbx+idx;

        // Local cell coordinates
        int cx = ci - (idx*mbsx + (idx < ovbx ? idx : ovbx));
        int cy = cj - (idy*mbsy + (idy < ovby ? idy : ovby));
        int cz = ck - (idz*mbsz + (idz < ovbz ? idz : ovbz));
#endif
        // Block coordinates and id
        int idx = 0; while (ci >= conf.splitx[idx+1]) idx++;
        int idy = 0; while (cj >= conf.splity[idy+1]) idy++;
        int idz = 0; while (ck >= conf.splitz[idz+1]) idz++;

        int id = (idz*nby+idy)*nbx+idx;

        int cx = ci - conf.splitx[idx];
        int cy = cj - conf.splity[idy];
        int cz = ck - conf.splitz[idz];

        ptr_t cell_ptr = block_ptrs[id].read(get_cell_ptr_ptr(blocks.ref(id), b, cz+1, cy+1, cx+1));
        Cell cell = real_cells.read(cell_ptr);

#ifdef TRACK_PARTICLE_IDS
	for(int p = 0; p < cell.num_particles; p++) {
	  printf("SAVE: %d\n", cell.id[p]);
	  printf("DATA: %d (%.3f,%.3f,%.3f) (%.3f,%.3f,%.3f) (%.3f,%.3f,%.3f)\n",
		 cell.id[p],
		 cell.p[p].x, cell.p[p].y, cell.p[p].z,
		 cell.hv[p].x, cell.hv[p].y, cell.hv[p].z,
		 cell.v[p].x, cell.v[p].y, cell.v[p].z);
	}
#endif

        unsigned np = cell.num_particles;
        for (unsigned p = 0; p < np; ++p) {
          if (!isLittleEndian()) {
            float px, py, pz, hvx, hvy, hvz, vx,vy, vz;

            px  = bswap_float(cell.p[p].x);
            py  = bswap_float(cell.p[p].y);
            pz  = bswap_float(cell.p[p].z);
            hvx = bswap_float(cell.hv[p].x);
            hvy = bswap_float(cell.hv[p].y);
            hvz = bswap_float(cell.hv[p].z);
            vx  = bswap_float(cell.v[p].x);
            vy  = bswap_float(cell.v[p].y);
            vz  = bswap_float(cell.v[p].z);

            file.write((char *)&px,  4);
            file.write((char *)&py,  4);
            file.write((char *)&pz,  4);
            file.write((char *)&hvx, 4);
            file.write((char *)&hvy, 4);
            file.write((char *)&hvz, 4);
            file.write((char *)&vx,  4);
            file.write((char *)&vy,  4);
            file.write((char *)&vz,  4);
          } else {
            file.write((char *)&cell.p[p].x,  4);
            file.write((char *)&cell.p[p].y,  4);
            file.write((char *)&cell.p[p].z,  4);
            file.write((char *)&cell.hv[p].x, 4);
            file.write((char *)&cell.hv[p].y, 4);
            file.write((char *)&cell.hv[p].z, 4);
            file.write((char *)&cell.v[p].x,  4);
            file.write((char *)&cell.v[p].y,  4);
            file.write((char *)&cell.v[p].z,  4);
          }
          ++numParticles;
        }
      }

  int numSkipped = origNumParticles - numParticles;
  float zero = 0.f;
  if (!isLittleEndian()) {
    zero = bswap_float(zero);
  }

  for (int i = 0; i < numSkipped; ++i) {
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
  }

  log_app.info("Done saving file.");
}

/*
 * Elliott: A hack to work around deadlocks in the GASNET runtime
 * caused by copying directly between local memories.
 */
void dummy_task(const Task *task,
                const std::vector<PhysicalRegion> &regions,
                Context ctx, HighLevelRuntime *runtime)
{
  //printf("dummy task run\n");
}

#if 1
#if 0
static bool sort_by_proc_id(const std::pair<Processor,Memory> &a,
                            const std::pair<Processor,Memory> &b)
{
  return (a.first.id < b.first.id);
}
#endif

// Run main in a separate thread? This will cannibalize 1 thread from
// the rest of the computation.
#ifndef MAP_MAIN_SEPARATELY
#define MAP_MAIN_SEPARATELY 0
#endif
static inline int get_proc_id_for_task(int task_id, int tag, int num_procs)
{
  switch (task_id) {
  case TOP_LEVEL_TASK_ID:
  case TASKID_MAIN_TASK:
  case TASKID_SAVE_FILE:
  case TASKID_DUMMY_TASK:
    return 0;
  case TASKID_LOAD_FILE:
  case TASKID_INIT_CELLS:
  case TASKID_INIT_CELLS_NOP:
  case TASKID_REBUILD_REDUCE:
  case TASKID_REBUILD_REDUCE_NOP:
  case TASKID_SCATTER_DENSITIES:
  case TASKID_SCATTER_DENSITIES_NOP:
  //case TASKID_GATHER_DENSITIES:
  //case TASKID_SCATTER_FORCES:
  case TASKID_GATHER_FORCES:
  case TASKID_GATHER_FORCES_NOP:
#if MAP_MAIN_SEPARATELY
    if (num_procs == 1)
      return 0;
    return (tag % (num_procs - 1)) + 1;
#else
    return tag % num_procs;
#endif
  default:
    assert(false);
  }
}

static inline int get_proc_id_for_neighbor(int task_id, int tag, int num_procs, int dir, Config &c)
{
  // for (unsigned idz = 0; idz < nbz; idz++)
  //   for (unsigned idy = 0; idy < nby; idy++)
  //     for (unsigned idx = 0; idx < nbx; idx++) {
  //       unsigned id = (idz*nby+idy)*nbx+idx;

  int id = tag;
  int idx = id % c.nbx;
  int idy = id/c.nbx % c.nby;
  int idz = id/c.nbx/c.nby;
  int idx2 = MOVE_BX(idx, dir, c);
  int idy2 = MOVE_BY(idy, dir, c);
  int idz2 = MOVE_BZ(idz, dir, c);
  int id2 = (idz2*c.nby + idy2)*c.nbx + idx2;

  int p = get_proc_id_for_task(task_id, id2, num_procs);

  log_mapper.info("edge cell for %d (%d %d %d) in direction %d is piece %d (%d %d %d) which lives on proc %d",
         id, idx, idy, idz, dir, id2, idx2, idy2, idz2, p);

  return p;
}

// No need to put all ghost cells in regmem when the edges shared
// within a node aren't going to be send across the network anyway.
#ifndef AVOID_OVERUSING_REGMEM
#define AVOID_OVERUSING_REGMEM 1
#endif

class FluidMapper : public ShimMapper {
public:
  struct CpuMemPair {
    Processor cpu;
    Memory sysmem;
    Memory regmem;
  };
  std::vector<CpuMemPair> cpu_mem_pairs;
  Config conf;

  FluidMapper(Machine *m, HighLevelRuntime *r, Processor p)
    : ShimMapper(m, r, p)
  {
    // for each CPU processor, we want to know:
    //  1) the system memory it's closest to, and
    //  2) the registered DMA memory it uses (if any)
    const std::set<Processor> &all_procs = m->get_all_processors();
    for(std::set<Processor>::const_iterator it = all_procs.begin(); it != all_procs.end(); it++) {
      // we only want CPUs
      if(m->get_processor_kind(*it) != Processor::LOC_PROC) continue;

      CpuMemPair cmp;
      cmp.cpu = *it;
      cmp.sysmem = machine_interface.find_memory_kind(cmp.cpu, Memory::SYSTEM_MEM);
      cmp.regmem = machine_interface.find_memory_kind(cmp.cpu, Memory::REGDMA_MEM);
      if(!cmp.regmem.exists())
	cmp.regmem = cmp.sysmem;

      cpu_mem_pairs.push_back(cmp);
    }

    // Parse command line arguments to determine problem size.
    InputArgs inputs = HighLevelRuntime::get_input_args();
    char **argv = inputs.argv;
    int argc = inputs.argc;
    parse_args(argc, argv, conf);
  }

  virtual bool spawn_child_task(const Task *task)
  {
    return true;
  }

  virtual Processor select_target_processor(const Task *task)
  {
    int proc_index = get_proc_id_for_task(task->task_id, task->tag, cpu_mem_pairs.size());
    const CpuMemPair &cmp = cpu_mem_pairs[proc_index];

    log_mapper.info("selected proc="IDFMT" (mem="IDFMT","IDFMT") for task %p",
	       cmp.cpu.id, cmp.sysmem.id, cmp.regmem.id, task);
    return cmp.cpu;
  }

  virtual Processor target_task_steal(const std::set<Processor> &blacklisted)
  {
    return Processor::NO_PROC;
  }

  virtual void permit_task_steal(Processor thief,
                                 const std::vector<const Task*> &tasks,
                                 std::set<const Task*> &to_steal)
  {
    return;
  }

  // virtual void split_index_space(const Task *task, const std::vector<Constraint> &index_space,
  //                                std::vector<ConstraintSplit> &chunks)
  // {
  //   return;
  // }

  virtual bool map_region_virtually(const Task *task, Processor target,
				    const RegionRequirement &req, 
				    unsigned index)
  {
    switch (task->task_id) {
    case TASKID_MAIN_TASK:
      {
        switch (index) {
        case 2: // real cell [0]
        case 3: // real cells [1]
#ifdef PARTITIONED_EDGE_CELLS
        case 4: // edge cells
#endif
          {
	    return true;
          }
          break;
	}
      }
    }

    return false; // map_task_region will say where we want it to be
  }

  // Elliott: try mapping everything locally?
  virtual bool map_task_locally(const Task *task)
  {
    return false;
  }

  virtual bool map_task_region(const Task *task, Processor target, MappingTagID tag, bool inline_mapping, bool pre_mapping,
                               const RegionRequirement &req, unsigned index,
                               const std::map<Memory,bool> &current_instances,
                               std::vector<Memory> &target_ranking,
                               std::set<FieldID> &additional_fields,
                               bool &enable_WAR_optimization)
  {
    log_mapper.info("mapper: mapping region for task (%s) region ("IDFMT" %x %x)",
                    task->variants->name,
                    req.region.get_index_space().id,
                    req.region.get_field_space().get_id(),
                    req.region.get_tree_id());
    int idx = index; 
    log_mapper.info("func_id=%d map_tag=%ld region_index=%d", task->task_id, task->tag, idx);

    int proc_id = get_proc_id_for_task(task->task_id, task->tag, cpu_mem_pairs.size());
    const CpuMemPair &cmp = cpu_mem_pairs[proc_id];

    switch (task->task_id) {
    case TOP_LEVEL_TASK_ID:
    case TASKID_SAVE_FILE:
    case TASKID_DUMMY_TASK:
      {
        // Don't care, put it in global memory
        //target_ranking.push_back(global_memory);
	// actually, we do care - put it in local memory always
	target_ranking.push_back(cmp.sysmem);
      }
      break;
    case TASKID_MAIN_TASK:
      {
        switch (idx) {
        case 2: // real cell [0]
        case 3: // real cells [1]
#ifdef PARTITIONED_EDGE_CELLS
        case 4: // edge cells
#endif
          {
            // Don't create an instance until explicitly requested
            if (1 || task->tag == REQUEST_INSTANCE) {
              target_ranking.push_back(cmp.sysmem);
            } else {
              target_ranking.push_back(Memory::NO_MEMORY);
            }
          }
          break;
        default:
          {
            // Copy anything else into global memory
            //target_ranking.push_back(global_memory);
            // Keep cell pointers in local memory
            target_ranking.push_back(cmp.sysmem);
          }
        }
      }
      break;
    case TASKID_LOAD_FILE:
      {
        switch (idx) {
        case 0: // config
        case 1: // block
        case 2: // block cell ptrs
        case 3: // real cells
          {
            // Put the owned cells in the local memory
            target_ranking.push_back(cmp.sysmem);
          }
          break;
        default:
          {
            // Copy anything else into local memory
            target_ranking.push_back(cmp.sysmem);
          }
        }
      }
      break;
    case TASKID_INIT_CELLS:
    case TASKID_INIT_CELLS_NOP:
      {
        switch (idx) { // First four regions should be local to us
        case 0: // config
        case 1: // blocks
        case 2: // block cell ptrs
        case 3: // source cells
        case 4: // dest cells
          {
            // These should go in the local memory
            target_ranking.push_back(cmp.sysmem);
          }
          break;
        default:
          {
#if AVOID_OVERUSING_REGMEM
            int neighbor_id = get_proc_id_for_neighbor(task->task_id, task->tag, cpu_mem_pairs.size(), idx - 5, conf);

	    // ghost cells in registered memory (if available)
            if (cpu_mem_pairs[neighbor_id].sysmem != cmp.sysmem) {
              target_ranking.push_back(cmp.regmem);
            } else {
              target_ranking.push_back(cmp.sysmem);
            }
#else
	    // ghost cells in registered memory (if available)
            target_ranking.push_back(cmp.regmem);
#endif
          }
        }
      }
      break;
    case TASKID_SCATTER_DENSITIES: // Operations which write ghost cells
    case TASKID_SCATTER_DENSITIES_NOP:
    //case TASKID_SCATTER_FORCES:
      {
        switch (idx) {
        case 0: // config
        case 1: // block
        case 2: // block cell ptrs
        case 3: // real cells
          {
            // Put the owned cells in the local memory
            target_ranking.push_back(cmp.sysmem);
          }
          break;
        default:
          {
#if AVOID_OVERUSING_REGMEM
            int neighbor_id = get_proc_id_for_neighbor(task->task_id, task->tag, cpu_mem_pairs.size(), idx - 4, conf);

	    // ghost cells in registered memory (if available)
            if (cpu_mem_pairs[neighbor_id].sysmem != cmp.sysmem) {
              target_ranking.push_back(cmp.regmem);
            } else {
              target_ranking.push_back(cmp.sysmem);
            }
#else
	    // ghost cells in registered memory (if available)
            target_ranking.push_back(cmp.regmem);
#endif
          }
        }
      }
      break;
    case TASKID_REBUILD_REDUCE:
    case TASKID_REBUILD_REDUCE_NOP:
      {
        switch (idx) {
        case 0: // block
        case 1: // block cell ptrs
        case 2: // real cells
          {
            // These are the owned cells, keep them in local memory
            target_ranking.push_back(cmp.sysmem);
          }
          break;
        default:
          {
#if AVOID_OVERUSING_REGMEM
            int neighbor_id = get_proc_id_for_neighbor(task->task_id, task->tag, cpu_mem_pairs.size(), idx - 3, conf);

	    // ghost cells in registered memory (if available)
            if (cpu_mem_pairs[neighbor_id].sysmem != cmp.sysmem) {
              target_ranking.push_back(cmp.regmem);
            } else {
              target_ranking.push_back(cmp.sysmem);
            }
#else
	    // ghost cells in registered memory (if available)
            target_ranking.push_back(cmp.regmem);
#endif
          }
        }
      }
      break;
    //case TASKID_GATHER_DENSITIES:
    case TASKID_GATHER_FORCES:
    case TASKID_GATHER_FORCES_NOP:
      {
        switch (idx) {
	case 0: // config
        case 1: // block
        case 2: // block cell ptrs
        case 3: // real cells
          {
            // These are the owned cells, keep them in local memory
            target_ranking.push_back(cmp.sysmem);
          }
          break;
        default:
          {
#if AVOID_OVERUSING_REGMEM
            int neighbor_id = get_proc_id_for_neighbor(task->task_id, task->tag, cpu_mem_pairs.size(), idx - 4, conf);

	    // ghost cells in registered memory (if available)
            if (cpu_mem_pairs[neighbor_id].sysmem != cmp.sysmem) {
              target_ranking.push_back(cmp.regmem);
            } else {
              target_ranking.push_back(cmp.sysmem);
            }
#else
	    // ghost cells in registered memory (if available)
            target_ranking.push_back(cmp.regmem);
#endif
          }
        }
      }
      break;
    default:
      assert(false);
    }
    
    char buffer[256];
    sprintf(buffer, "mapper: chose dst=[");
    for(unsigned i = 0; i < target_ranking.size(); i++) {
      if(i) strcat(buffer, ", ");
      sprintf(buffer+strlen(buffer), ""IDFMT"", target_ranking[i].id);
    }
    strcat(buffer, "]");
    log_mapper.info("%s", buffer);
    return true;
  }

  virtual void rank_copy_targets(const Task *task, const RegionRequirement &req,
                                 const std::set<Memory> &current_instances,
                                 std::vector<Memory> &future_ranking)
  {
    log_mapper.info("mapper: ranking copy targets (%p)\n", task);
    //Mapper::rank_copy_targets(task, req, current_instances, future_ranking);

    // Always copy back to GASNET
    assert(0);
    //future_ranking.push_back(global_memory);
  }

  // virtual void select_copy_source(const std::set<Memory> &current_instances,
  //                                 const Memory &dst, Memory &chosen_src)
  // {
  //   if(current_instances.size() == 1) {
  //     chosen_src = *(current_instances.begin());
  //     return;
  //   }
  //   log_mapper.info("mapper: selecting copy source\n");
  //   for(std::set<Memory>::const_iterator it = current_instances.begin();
  // 	it != current_instances.end();
  // 	it++)
  //     log_mapper.info("  choice = "IDFMT"", (*it).id);
  //   Mapper::select_copy_source(current_instances, dst, chosen_src);
  // }

  // virtual bool compact_partition(const Partition &partition,
  //                                MappingTagID tag)
  // {
  //   return false;
  // }
};
#else
class FluidMapper : public DefaultMapper
{
public:
  FluidMapper(Machine *m, HighLevelRuntime *rt, Processor local)
    : DefaultMapper(m, rt, local)
  {
    std::set<Processor> procset;
    machine_interface.filter_processors(m, Processor::LOC_PROC, procset);
    procs.assign(procset.begin(), procset.end());
  }

  virtual void select_task_options(Task *task);
  virtual bool map_task(Task *task);
  virtual bool map_inline(Inline *inline_operation);
private:
  std::vector<Processor> procs;
};

void FluidMapper::select_task_options(Task *task)
{
  task->target_proc = procs[task->tag % procs.size()];
}


bool FluidMapper::map_task(Task *task)
{
  Memory global_memory = machine_interface.find_global_memory();

  std::vector<RegionRequirement> &regions = task->regions;
  for (std::vector<RegionRequirement>::iterator it = regions.begin();
        it != regions.end(); it++) {
    RegionRequirement &req = *it;

    // Region options:
    req.virtual_map = false;
    req.enable_WAR_optimization = false;
    req.reduction_list = false;

    // Place all regions in global memory.
    req.target_ranking.push_back(global_memory);
  }

  return false;
}

bool FluidMapper::map_inline(Inline *inline_operation)
{
  Memory global_memory = machine_interface.find_global_memory();

  RegionRequirement &req = inline_operation->requirement;

  // Region options:
  req.virtual_map = false;
  req.enable_WAR_optimization = false;
  req.reduction_list = false;

  // Place all regions in global memory.
  req.target_ranking.push_back(global_memory);

  return false;
}
#endif

void create_mappers(Machine *machine, HighLevelRuntime *runtime, const std::set<Processor> &local_procs)
{
  for (std::set<Processor>::const_iterator it = local_procs.begin();
        it != local_procs.end(); it++) {
    runtime->replace_default_mapper(
      new FluidMapper(machine, runtime, *it), *it);
  }
}

int main(int argc, char **argv)
{
  TaskConfigOptions default_options(false/*leaf*/, false/*inner*/, false);
  TaskConfigOptions leaf_options(true/*leaf*/, false/*inner*/, false);

  HighLevelRuntime::set_top_level_task_id(TOP_LEVEL_TASK_ID);
  HighLevelRuntime::set_registration_callback(create_mappers);
  HighLevelRuntime::register_legion_task<top_level_task>(
    TOP_LEVEL_TASK_ID, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, default_options, "top_level_task");
  HighLevelRuntime::register_legion_task<main_task>(
    TASKID_MAIN_TASK, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, default_options, "main_task");
  HighLevelRuntime::register_legion_task<init_and_rebuild>(
    TASKID_INIT_CELLS, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "init_cells");
  HighLevelRuntime::register_legion_task<dummy_task>(
    TASKID_INIT_CELLS_NOP, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "init_cells_nop");
  HighLevelRuntime::register_legion_task<rebuild_reduce>(
    TASKID_REBUILD_REDUCE, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "rebuild_reduce");
  HighLevelRuntime::register_legion_task<dummy_task>(
    TASKID_REBUILD_REDUCE_NOP, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "rebuild_reduce_nop");
  HighLevelRuntime::register_legion_task<scatter_densities>(
    TASKID_SCATTER_DENSITIES, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "scatter_densities");
  HighLevelRuntime::register_legion_task<dummy_task>(
    TASKID_SCATTER_DENSITIES_NOP, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "scatter_densities_nop");
  HighLevelRuntime::register_legion_task<gather_forces_and_advance>(
    TASKID_GATHER_FORCES, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "gather_forces");
  HighLevelRuntime::register_legion_task<dummy_task>(
    TASKID_GATHER_FORCES_NOP, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "gather_forces_nop");
  HighLevelRuntime::register_legion_task<load_file>(
    TASKID_LOAD_FILE, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, default_options, "load_file");
  HighLevelRuntime::register_legion_task<save_file>(
    TASKID_SAVE_FILE, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, default_options, "save_file");
  HighLevelRuntime::register_legion_task<dummy_task>(
    TASKID_DUMMY_TASK, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, default_options, "dummy_task");

  return HighLevelRuntime::start(argc, argv);
}

// EOF

