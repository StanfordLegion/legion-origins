
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <cassert>

// cstdint complains about C++11 support?
#include <stdint.h>

#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

#include "default_mapper.h"
#include "shim_mapper.h"
#include "legion.h"
#include "utilities.h"

using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::HighLevel;

LegionRuntime::Logger::Category log_mapper("mapper");

enum {
  TOP_LEVEL_TASK_ID,
  TASKID_INIT_CELLS,
  TASKID_REBUILD_REDUCE,
  TASKID_CLEAR_DENSITIES,
  TASKID_SCATTER_DENSITIES,
  TASKID_CLEAR_FORCES,
  TASKID_GATHER_FORCES,
  TASKID_INIT_CELLS_NOP,
  TASKID_REBUILD_REDUCE_NOP,
  TASKID_SCATTER_DENSITIES_NOP,
  TASKID_GATHER_FORCES_NOP,
  TASKID_MAIN_TASK,
  TASKID_LOAD_FILE,
  TASKID_SAVE_FILE,
  TASKID_DUMMY_TASK,
};

enum {
  LAYOUT_ALL_AOS,
  LAYOUT_MIXED,
  LAYOUT_ALL_SOA,
};

static const MappingTagID TAG_EDGE_CELLS = 1234;

static int layout_policy = LAYOUT_ALL_SOA;

typedef AccessorType::Generic Generic;
typedef AccessorType::SOA<0> SOA;

const ptr_t base_ptr(0);

const unsigned MAX_PARTICLES = 16;

// Number of ghost cells needed for each block
const unsigned GHOST_CELLS = 26;

// a pointer that understands about edge cell regions
struct multiptr_t {
  int dir;
  ptr_t ptr;

  multiptr_t(int _dir, ptr_t _ptr) : dir(_dir), ptr(_ptr) {}
};

enum { // don't change the order of these!  needs to be symmetric
  TOP_FRONT_LEFT = 0,
  TOP_FRONT,
  TOP_FRONT_RIGHT,
  FRONT_LEFT,
  FRONT,
  FRONT_RIGHT,
  BOTTOM_FRONT_LEFT,
  BOTTOM_FRONT,
  BOTTOM_FRONT_RIGHT,
  BOTTOM_LEFT,
  BOTTOM,
  BOTTOM_RIGHT,
  LEFT,
  RIGHT,
  TOP_LEFT,
  TOP,
  TOP_RIGHT,
  TOP_BACK_LEFT,
  TOP_BACK,
  TOP_BACK_RIGHT,
  BACK_LEFT,
  BACK,
  BACK_RIGHT,
  BOTTOM_BACK_LEFT,
  BOTTOM_BACK,
  BOTTOM_BACK_RIGHT,
  CENTER,
  BOUNDARY,
};

enum {
  SIDE_TOP    = 0x01,
  SIDE_BOTTOM = 0x02,
  SIDE_FRONT  = 0x04,
  SIDE_BACK   = 0x08,
  SIDE_RIGHT  = 0x10,
  SIDE_LEFT   = 0x20,
};

// order corresponds to order of elements in enum above
const unsigned char DIR2SIDES[] = {
  SIDE_TOP | SIDE_FRONT | SIDE_LEFT,
  SIDE_TOP | SIDE_FRONT,
  SIDE_TOP | SIDE_FRONT | SIDE_RIGHT,
  SIDE_FRONT | SIDE_LEFT,
  SIDE_FRONT,
  SIDE_FRONT | SIDE_RIGHT,
  SIDE_BOTTOM | SIDE_FRONT | SIDE_LEFT,
  SIDE_BOTTOM | SIDE_FRONT,
  SIDE_BOTTOM | SIDE_FRONT | SIDE_RIGHT,
  SIDE_BOTTOM | SIDE_LEFT,
  SIDE_BOTTOM,
  SIDE_BOTTOM | SIDE_RIGHT,
  SIDE_LEFT,
  SIDE_RIGHT,
  SIDE_TOP | SIDE_LEFT,
  SIDE_TOP,
  SIDE_TOP | SIDE_RIGHT,
  SIDE_TOP | SIDE_BACK | SIDE_LEFT,
  SIDE_TOP | SIDE_BACK,
  SIDE_TOP | SIDE_BACK | SIDE_RIGHT,
  SIDE_BACK | SIDE_LEFT,
  SIDE_BACK,
  SIDE_BACK | SIDE_RIGHT,
  SIDE_BOTTOM | SIDE_BACK | SIDE_LEFT,
  SIDE_BOTTOM | SIDE_BACK,
  SIDE_BOTTOM | SIDE_BACK | SIDE_RIGHT,
  0,
};

// order corresponds to zyx [3][3][3] lookup array
const unsigned char SIDES2DIR[] = {
  BOTTOM_FRONT_LEFT,
  BOTTOM_FRONT,
  BOTTOM_FRONT_RIGHT,
  BOTTOM_LEFT,
  BOTTOM,
  BOTTOM_RIGHT,
  BOTTOM_BACK_LEFT,
  BOTTOM_BACK,
  BOTTOM_BACK_RIGHT,
  FRONT_LEFT,
  FRONT,
  FRONT_RIGHT,
  LEFT,
  CENTER,
  RIGHT,
  BACK_LEFT,
  BACK,
  BACK_RIGHT,
  TOP_FRONT_LEFT,
  TOP_FRONT,
  TOP_FRONT_RIGHT,
  TOP_LEFT,
  TOP,
  TOP_RIGHT,
  TOP_BACK_LEFT,
  TOP_BACK,
  TOP_BACK_RIGHT,
};

static inline int CLAMP(int x, int min, int max)
{
  return x < min ? min : (x > max ? max : x);
}

static inline int MOVE_TOP(int z)    { return z+1; }
static inline int MOVE_BOTTOM(int z) { return z-1; }
static inline int MOVE_LEFT(int x)   { return x-1; }
static inline int MOVE_RIGHT(int x)  { return x+1; }
static inline int MOVE_FRONT(int y)  { return y-1; }
static inline int MOVE_BACK(int y)   { return y+1; }

static inline int MOVE_X(int x, int dir, int min, int max)
{
  return CLAMP((DIR2SIDES[dir] & SIDE_RIGHT) ? MOVE_RIGHT(x) :
               ((DIR2SIDES[dir] & SIDE_LEFT) ? MOVE_LEFT(x) : x),
               min, max);
}

static inline int MOVE_Y(int y, int dir, int min, int max)
{
  return CLAMP((DIR2SIDES[dir] & SIDE_BACK) ? MOVE_BACK(y) :
               ((DIR2SIDES[dir] & SIDE_FRONT) ? MOVE_FRONT(y) : y),
               min, max);
}

static inline int MOVE_Z(int z, int dir, int min, int max)
{
  return CLAMP((DIR2SIDES[dir] & SIDE_TOP) ? MOVE_TOP(z) :
               ((DIR2SIDES[dir] & SIDE_BOTTOM) ? MOVE_BOTTOM(z) : z),
               min, max);
}

static inline int REVERSE(int dir) { return 25 - dir; }

// maps {-1, 0, 1}^3 to directions
static inline int LOOKUP_DIR(int x, int y, int z)
{
  return SIDES2DIR[((z+1)*3 + y+1)*3 + x+1];
}

#if 0
static inline int REVERSE_SIDES(int dir, int flipx, int flipy, int flipz)
{
  int dirx = (DIR2SIDES[dir] & SIDE_RIGHT) ? -1 :
    ((DIR2SIDES[dir] & SIDE_LEFT) ? 1 : 0);
  int diry = (DIR2SIDES[dir] & SIDE_BACK) ? -1 :
    ((DIR2SIDES[dir] & SIDE_FRONT) ? 1 : 0);
  int dirz = (DIR2SIDES[dir] & SIDE_TOP) ? -1 :
    ((DIR2SIDES[dir] & SIDE_BOTTOM) ? 1 : 0);
  if (flipx) dirx = -dirx;
  if (flipy) diry = -diry;
  if (flipz) dirz = -dirz;
  return LOOKUP_DIR(dirx, diry, dirz);
}
#endif

class Vec3
{
public:
    float x, y, z;

    Vec3() {}
    Vec3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}

    float   GetLengthSq() const         { return x*x + y*y + z*z; }
    float   GetLength() const           { return sqrtf(GetLengthSq()); }
    Vec3 &  Normalize()                 { return *this /= GetLength(); }

    Vec3 &  operator += (Vec3 const &v) { x += v.x;  y += v.y; z += v.z; return *this; }
    Vec3 &  operator -= (Vec3 const &v) { x -= v.x;  y -= v.y; z -= v.z; return *this; }
    Vec3 &  operator *= (float s)       { x *= s;  y *= s; z *= s; return *this; }
    Vec3 &  operator /= (float s)       { x /= s;  y /= s; z /= s; return *this; }

    Vec3    operator + (Vec3 const &v) const    { return Vec3(x+v.x, y+v.y, z+v.z); }
    Vec3    operator - () const                 { return Vec3(-x, -y, -z); }
    Vec3    operator - (Vec3 const &v) const    { return Vec3(x-v.x, y-v.y, z-v.z); }
    Vec3    operator * (float s) const          { return Vec3(x*s, y*s, z*s); }
    Vec3    operator / (float s) const          { return Vec3(x/s, y/s, z/s); }

    float   operator * (Vec3 const &v) const    { return x*v.x + y*v.y + z*v.z; }
};

// cell structure is split into separate Legion fields - structure shown here for
//  posterity:
// struct Cell
// {
// public:
//   Vec3 p[MAX_PARTICLES];
//   Vec3 hv[MAX_PARTICLES];
//   Vec3 v[MAX_PARTICLES];
//   Vec3 a[MAX_PARTICLES];
//   float density[MAX_PARTICLES];
//   unsigned num_particles;
// };

template <typename T, unsigned N>
struct Vec {
  T v[N];
  T& operator[](int i) { return v[i]; }
  const T& operator[](int i) const { return v[i]; }
};

typedef Vec<float, MAX_PARTICLES> Floats __attribute__((aligned (64)));

#define NO_TRACK_PARTICLE_IDS

#ifdef TRACK_PARTICLE_IDS
typedef Vec<int, MAX_PARTICLES> Ints;
#endif

enum {
  FID_CELL_NUM_PARTICLES,  // int
  FID_CELL_P_X,            // vec<float, MAX_PARTICLES>
  FID_CELL_P_Y,            // vec<float, MAX_PARTICLES>
  FID_CELL_P_Z,            // vec<float, MAX_PARTICLES>
  FID_CELL_HV_X,           // vec<float, MAX_PARTICLES>
  FID_CELL_HV_Y,           // vec<float, MAX_PARTICLES>
  FID_CELL_HV_Z,           // vec<float, MAX_PARTICLES>
  FID_CELL_V_X,            // vec<float, MAX_PARTICLES>
  FID_CELL_V_Y,            // vec<float, MAX_PARTICLES>
  FID_CELL_V_Z,            // vec<float, MAX_PARTICLES>
  FID_CELL_A_X,            // vec<float, MAX_PARTICLES>
  FID_CELL_A_Y,            // vec<float, MAX_PARTICLES>
  FID_CELL_A_Z,            // vec<float, MAX_PARTICLES>
  FID_CELL_DENSITY,        // vec<float, MAX_PARTICLES>
#ifdef TRACK_PARTICLE_IDS
  FID_CELL_ID,
#endif

  FID_CONFIG = 100,
  FID_BLOCK = 200,
  FID_PTR = 300,
};

// two kinds of double-buffering going on here
// * for the CELLS_X x CELLS_Y x CELLS_Z grid of "real" cells, we have two copies
//     for double-buffering the simulation
// * for the ring of edge/ghost cells around the "real" cells, we have
//     two copies for bidirectional exchanges
//
// in addition to the requisite 2*1 + 2*26 = 54 regions, we track 
//  2 sets of (CELLS_X+2)*(CELLS_Y+2)*(CELLS_Z+2) pointers
// have to pay attention though, because the phase of the "real" cells changes
//  only once per simulation iteration, while the phase of the "edge" cells
//  changes every task
struct Block {
  int id;
  unsigned x, y, z; // position in block grid
  unsigned CELLS_X, CELLS_Y, CELLS_Z;
};

const float timeStep = 0.005f;
const float doubleRestDensity = 2000.f;
const float kernelRadiusMultiplier = 1.695f;
const float stiffness = 1.5f;
const float viscosity = 0.4f;
const Vec3 externalAcceleration(0.f, -9.8f, 0.f);
const Vec3 domainMin(-0.065f, -0.08f, -0.065f);
const Vec3 domainMax(0.065f, 0.1f, 0.065f);

const unsigned MAX_SPLITS = 16;

const size_t MAX_FILENAME = 256;
struct Config {
  // Parameters from command line arguments.
  int numSteps;
  int nbx, nby, nbz, numBlocks;
  int balance;
  int splitx[MAX_SPLITS + 1];
  int splity[MAX_SPLITS + 1];
  int splitz[MAX_SPLITS + 1];
  char inFilename[MAX_FILENAME], outFilename[MAX_FILENAME];

  // Parameters of simulation from fluid file header.
  float restParticlesPerMeter;
  int origNumParticles;

  // Variables dependent on fluid file headers.
  float h, hSq;
  float densityCoeff, pressureCoeff, viscosityCoeff;
  int nx, ny, nz, numCells;
  Vec3 delta;                           // cell dimensions

  // should app be run in bulk-synchronous mode?
  int sync;

// PARSEC does not double buffer. That is, PARSEC performs the first
// step of the computation over and over. However, double buffering
// has a potential performance impact, so we need both.
  bool repeat_first_timestep;
};

static inline int MOVE_BX(int x, int dir, const Config &c) { return MOVE_X(x, dir, 0, c.nbx-1); }
static inline int MOVE_BY(int y, int dir, const Config &c) { return MOVE_Y(y, dir, 0, c.nby-1); }
static inline int MOVE_BZ(int z, int dir, const Config &c) { return MOVE_Z(z, dir, 0, c.nbz-1); }
static inline int MOVE_CX(const Block &b, int x, int dir) {
return MOVE_X(x, dir, 0, b.CELLS_X+1);
}
static inline int MOVE_CY(const Block &b, int y, int dir) {
return MOVE_Y(y, dir, 0, b.CELLS_Y+1);
}
static inline int MOVE_CZ(const Block &b, int z, int dir) {
return MOVE_Z(z, dir, 0, b.CELLS_Z+1);
}

LegionRuntime::Logger::Category log_app("application");

static int parse_args(int argc, char **argv, Config &conf)
{
  int &nbx = conf.nbx, &nby = conf.nby, &nbz = conf.nbz, &numBlocks = conf.numBlocks;
  int &numSteps = conf.numSteps;
  char (&inFilename)[256] = conf.inFilename, (&outFilename)[256] = conf.outFilename;

  // Default parameter values.
  numSteps = 4;
  nbx = nby = nbz = 1;
  conf.balance = 0;
  conf.sync = 0; // default is to not be bulk-synchronous
  conf.repeat_first_timestep = true;  // match PARSEC by default
  strcpy(inFilename, "init.fluid");
  strcpy(outFilename, "");

  for (int i = 1; i < argc; i++) {
    if (!strcmp(argv[i], "-s")) {
      numSteps = atoi(argv[++i]);
      continue;
    }
    
    if (!strcmp(argv[i], "-nbx")) {
      nbx = atoi(argv[++i]);
      continue;
    }
    
    if (!strcmp(argv[i], "-nby")) {
      nby = atoi(argv[++i]);
      continue;
    }

    if (!strcmp(argv[i], "-nbz")) {
      nbz = atoi(argv[++i]);
      continue;
    }

    if (!strcmp(argv[i], "-i") || !strcmp(argv[i], "-input")) {
      strncpy(inFilename, argv[++i], MAX_FILENAME);
      inFilename[MAX_FILENAME-1] = '\0';
      continue;
    }

    if (!strcmp(argv[i], "-o") || !strcmp(argv[i], "-output")) {
      strncpy(outFilename, argv[++i], MAX_FILENAME);
      outFilename[MAX_FILENAME-1] = '\0';
      continue;
    }

    if (!strcmp(argv[i], "-balance")) {
      conf.balance = atoi(argv[++i]);
      continue;
    }

    if (!strcmp(argv[i], "-sync")) {
      conf.sync = atoi(argv[++i]);
      printf("sync set to %d\n", conf.sync);
      continue;
    }

    if (!strcmp(argv[i], "-repeat")) {
      conf.repeat_first_timestep = atoi(argv[++i]);
      printf("repeat set to %d\n", conf.repeat_first_timestep);
      continue;
    }
  }
  numBlocks = nbx * nby * nbz;
  return numSteps;
}

// Fills in restParticlesPerMeter and origNumParticles from fluid file header.
static void load_file_header(const char *fileName, Config &conf);

// Expects restParticlesPerMeter to be filled in from load_file_header.
static void init_config(Config &conf)
{
  const float &restParticlesPerMeter = conf.restParticlesPerMeter;

  float &h = conf.h, &hSq = conf.hSq;
  float &densityCoeff = conf.densityCoeff;
  float &pressureCoeff = conf.pressureCoeff;
  float &viscosityCoeff = conf.viscosityCoeff;
  int &nx = conf.nx, &ny = conf.ny, &nz = conf.nz, &numCells = conf.numCells;
  Vec3 &delta = conf.delta;

  // Simulation parameters
  h = kernelRadiusMultiplier / restParticlesPerMeter;
  hSq = h*h;
  const float pi = 3.14159265358979f;
  float coeff1 = 315.f / (64.f*pi*pow(h,9.f));
  float coeff2 = 15.f / (pi*pow(h,6.f));
  float coeff3 = 45.f / (pi*pow(h,6.f));
  float particleMass = 0.5f*doubleRestDensity / (restParticlesPerMeter*restParticlesPerMeter*restParticlesPerMeter);
  densityCoeff = particleMass * coeff1;
  pressureCoeff = 3.f*coeff2 * 0.5f*stiffness * particleMass;
  viscosityCoeff = viscosity * coeff3 * particleMass;

  // Simulation grid size
  Vec3 range = domainMax - domainMin;
  nx = (int)(range.x / h);
  ny = (int)(range.y / h);
  nz = (int)(range.z / h);
  numCells = nx*ny*nz;
  delta.x = range.x / nx;
  delta.y = range.y / ny;
  delta.z = range.z / nz;
  assert(delta.x >= h && delta.y >= h && delta.z >= h);
  
  // default splits are the same way that original PARSEC does it
  int xstep = nx / conf.nbx;
  int xextra = nx % conf.nbx;
  conf.splitx[0] = 0;
  for (int i = 0; i < conf.nbx; i++)
    conf.splitx[i + 1] = conf.splitx[i] + xstep + ((i < xextra) ? 1 : 0);

  int ystep = ny / conf.nby;
  int yextra = ny % conf.nby;
  conf.splity[0] = 0;
  for (int i = 0; i < conf.nby; i++)
    conf.splity[i + 1] = conf.splity[i] + ystep + ((i < yextra) ? 1 : 0);

  int zstep = nz / conf.nbz;
  int zextra = nz % conf.nbz;
  conf.splitz[0] = 0;
  for (int i = 0; i < conf.nbz; i++)
    conf.splitz[i + 1] = conf.splitz[i] + zstep + ((i < zextra) ? 1 : 0);
}

static void add_edge_regions(TaskLauncher &launcher,
			     std::vector<LogicalRegion> &regions,
			     std::vector<LogicalRegion> &parent_regions,
			     PrivilegeMode access, CoherenceProperty prop,
			     const std::vector<FieldID>& fields)
{
  for (int i = 0; i < regions.size(); i++) {
    RegionRequirement rr(regions[i], access, prop, parent_regions[i], TAG_EDGE_CELLS);
    for(std::vector<FieldID>::const_iterator it = fields.begin(); it != fields.end(); it++)
      rr.add_field(*it);
    launcher.add_region_requirement(rr);
  }
}

void analyze_particle_distribution(Config& conf);

static LogicalRegion create_logical_region_simple(HighLevelRuntime *runtime,
						  Context ctx,
						  unsigned num_elements,
						  int field_id, size_t field_size) {
  IndexSpace is = runtime->create_index_space(ctx, num_elements);
  IndexAllocator isa = runtime->create_index_allocator(ctx, is);
  assert(isa.alloc(num_elements) == base_ptr);
  FieldSpace fs = runtime->create_field_space(ctx);
  FieldAllocator fsa = runtime->create_field_allocator(ctx, fs);
  fsa.allocate_field(field_size, field_id);
  LogicalRegion r = runtime->create_logical_region(ctx, is, fs);
  return r;
}

#if 0
static LogicalRegion create_cells_region(HighLevelRuntime *runtime,
					 Context ctx,
					 unsigned num_elements,
					 FieldSpace fs_cells)
{
  IndexSpace is = runtime->create_index_space(ctx, num_elements);
  IndexAllocator isa = runtime->create_index_allocator(ctx, is);
  assert(isa.alloc(num_elements) == base_ptr);
  LogicalRegion r = runtime->create_logical_region(ctx, is, fs_cells);
  return r;
}
#endif

void top_level_task(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, HighLevelRuntime *runtime)
{
  log_app.info("In top_level_task...");

  InputArgs inputs = HighLevelRuntime::get_input_args();
  char **argv = inputs.argv;
  int argc = inputs.argc;

  Config conf;
  int &nx = conf.nx, &ny = conf.ny, &nz = conf.nz;
  int &nbx = conf.nbx, &nby = conf.nby, &nbz = conf.nbz;
  int &numBlocks = conf.numBlocks;
  char (&inFilename)[256] = conf.inFilename;

  // parse command line arguments
  parse_args(argc, argv, conf);
  printf("fluid: %p %d ",argv,argc);
  for (int i = 1; i < argc; i++)
    printf("%s ",argv[i]);
  printf("\n");

  // read input file header to get problem size
  load_file_header(inFilename, conf);
  init_config(conf);

  if (conf.balance) {
    analyze_particle_distribution(conf);
  }

  const unsigned n_config = 1;
  LogicalRegion config =
    create_logical_region_simple(runtime, ctx, n_config, FID_CONFIG, sizeof(Config));

  const unsigned n_blocks = nbx*nby*nbz;
  LogicalRegion blocks =
    create_logical_region_simple(runtime, ctx, n_blocks, FID_BLOCK, sizeof(Block));

  FieldSpace fs_cells = runtime->create_field_space(ctx);
  {
    FieldAllocator fsa = runtime->create_field_allocator(ctx, fs_cells);
    fsa.allocate_field(sizeof(int), FID_CELL_NUM_PARTICLES);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_P_X);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_P_Y);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_P_Z);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_HV_X);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_HV_Y);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_HV_Z);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_V_X);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_V_Y);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_V_Z);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_A_X);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_A_Y);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_A_Z);
    fsa.allocate_field(MAX_PARTICLES * sizeof(float), FID_CELL_DENSITY);
#ifdef TRACK_PARTICLE_IDS
    fsa.allocate_field(MAX_PARTICLES * sizeof(int), FID_CELL_ID);
#endif
  }

  // real cells are double-buffered in time - we will have a single
  //  index space, and two regions using it
  const unsigned n_real_cells = nx*ny*nz;
  IndexSpace is_real_cells = runtime->create_index_space(ctx, n_real_cells);

  LogicalRegion real_cells[2];
  real_cells[0] = runtime->create_logical_region(ctx, is_real_cells, fs_cells);
  real_cells[1] = runtime->create_logical_region(ctx, is_real_cells, fs_cells);

//define PARTITIONED_EDGE_CELLS
#ifdef PARTITIONED_EDGE_CELLS
  // we also use explicit edge cells - since the simulation is bounded (i.e. no periodic)
  //  we need an extra edge
  const unsigned n_edge_cells = ((1 * nx * ny * (nbz + 1)) +
				 (1 * nx * (nby + 1) * nz) +
				 (1 * (nbx + 1) * ny * nz) +
				 (2 * nx * (nby + 1) * (nbz + 1)) +
				 (2 * (nbx + 1) * ny * (nbz + 1)) +
				 (2 * (nbx + 1) * (nby + 1) * nz) +
				 (4 * (nbx + 1) * (nby + 1) * (nbz + 1)));
  IndexSpace is_edge_cells = runtime->create_index_space(ctx, n_edge_cells);

  LogicalRegion edge_cells[2];
  edge_cells[0] = runtime->create_logical_region(ctx, is_edge_cells, fs_cells);
  edge_cells[1] = runtime->create_logical_region(ctx, is_edge_cells, fs_cells);
#endif

  std::vector<LogicalRegion> block_ptrs;
  for (int idz = 0; idz < nbz; idz++)
    for (int idy = 0; idy < nby; idy++)
      for (int idx = 0; idx < nbx; idx++) {
        // int id = (idz*nby+idy)*nbx+idx;

        int CELLS_X = (conf.splitx[idx+1] - conf.splitx[idx]);
        int CELLS_Y = (conf.splity[idy+1] - conf.splity[idy]);
        int CELLS_Z = (conf.splitz[idz+1] - conf.splitz[idz]);
        //assert(CELLS_X == (nx/nbx) + (nx%nbx > idx ? 1 : 0));
        //assert(CELLS_Y == (ny/nby) + (ny%nby > idy ? 1 : 0));
        //assert(CELLS_Z == (nz/nbz) + (nz%nbz > idz ? 1 : 0));

        const unsigned n_block_ptrs = (CELLS_X+2)*(CELLS_Y+2)*(CELLS_Z+2);
        block_ptrs.push_back(create_logical_region_simple(runtime, ctx, n_block_ptrs, 
							  FID_PTR, sizeof(multiptr_t)));
      }

  // Initialize configuration.
  {
    InlineLauncher map(RegionRequirement(config, READ_WRITE, EXCLUSIVE, config)
		       .add_field(FID_CONFIG));
    PhysicalRegion pr_config = runtime->map_region(ctx, map);
    pr_config.wait_until_valid();
    pr_config.get_field_accessor(FID_CONFIG).typeify<Config>().write(base_ptr, conf);
    runtime->unmap_region(ctx, pr_config);
  }

  // Call main.
  {
    TaskLauncher main(TASKID_MAIN_TASK, TaskArgument());
    main.add_region_requirement(RegionRequirement(config, READ_ONLY, EXCLUSIVE, config)
				.add_field(FID_CONFIG));
    main.add_region_requirement(RegionRequirement(blocks, READ_WRITE, EXCLUSIVE, blocks)
				.add_field(FID_BLOCK));
    std::vector<FieldID> cell_fields;
    cell_fields.push_back(FID_CELL_NUM_PARTICLES);
    cell_fields.push_back(FID_CELL_P_X);
    cell_fields.push_back(FID_CELL_P_Y);
    cell_fields.push_back(FID_CELL_P_Z);
    cell_fields.push_back(FID_CELL_HV_X);
    cell_fields.push_back(FID_CELL_HV_Y);
    cell_fields.push_back(FID_CELL_HV_Z);
    cell_fields.push_back(FID_CELL_V_X);
    cell_fields.push_back(FID_CELL_V_Y);
    cell_fields.push_back(FID_CELL_V_Z);
    cell_fields.push_back(FID_CELL_A_X);
    cell_fields.push_back(FID_CELL_A_Y);
    cell_fields.push_back(FID_CELL_A_Z);
    cell_fields.push_back(FID_CELL_DENSITY);
#ifdef TRACK_PARTICLE_IDS
    cell_fields.push_back(FID_CELL_ID);
#endif

    main.add_region_requirement(RegionRequirement(real_cells[0], READ_WRITE, EXCLUSIVE, real_cells[0])
				.add_fields(cell_fields));
    main.add_region_requirement(RegionRequirement(real_cells[1], READ_WRITE, EXCLUSIVE, real_cells[1])
				.add_fields(cell_fields));
#ifdef PARTITIONED_EDGE_CELLS
    main.add_region_requirement(RegionRequirement(edge_cells[0], READ_WRITE, EXCLUSIVE, edge_cells[0],
						  TAG_EDGE_CELLS)
				.add_fields(cell_fields));
    main.add_region_requirement(RegionRequirement(edge_cells[1], READ_WRITE, EXCLUSIVE, edge_cells[1],
						  TAG_EDGE_CELLS)
				.add_fields(cell_fields));
#endif
    for (int id = 0; id < numBlocks; id++) {
      main.add_region_requirement(RegionRequirement(block_ptrs[id], READ_WRITE, EXCLUSIVE, block_ptrs[id])
				  .add_field(FID_PTR));
    }

    Future f = runtime->execute_task(ctx, main);
    f.get_void_result();
  }

  log_app.info("Done with top_level_task...");
}

#if 0
static inline int NEIGH_X(int idx, int dir, int cx, const Config &c)
{
  return MOVE_BX(idx, dir, c) == idx ? cx : 1-cx;
}

static inline int NEIGH_Y(int idy, int dir, int cy, const Config &c)
{
  return MOVE_BY(idy, dir, c) == idy ? cy : 1-cy;
}

static inline int NEIGH_Z(int idz, int dir, int cz, const Config &c)
{
  return MOVE_BZ(idz, dir, c) == idz ? cz : 1-cz;
}

static inline int OPPOSITE_DIR(int idz, int idy, int idx, int dir, const Config &c)
{
  int flipx = MOVE_BX(idx, dir, c) == idx;
  int flipy = MOVE_BY(idy, dir, c) == idy;
  int flipz = MOVE_BZ(idz, dir, c) == idz;
  return REVERSE_SIDES(dir, flipx, flipy, flipz);
}
#endif

static inline ptr_t get_cell_ptr_ptr(const Block &b, int cz, int cy, int cx)
{
  ptr_t result;
  result.value = (cz*(b.CELLS_Y+2) + cy)*(b.CELLS_X+2) + cx;
  return result;
}

// Flag main_task passes to the mapper to request an instance for edge_cells.
const unsigned REQUEST_INSTANCE = 1;

void main_task(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime)
{
  log_app.info("In main_task...");

  // Retrieve configuration.
  PhysicalRegion pr_config = regions[0];
  LogicalRegion config = pr_config.get_logical_region();
  const Config &conf = pr_config.get_field_accessor(FID_CONFIG).typeify<Config>().convert<SOA>().ref(base_ptr);
  int numBlocks = conf.numBlocks;
  int nx = conf.nx, ny = conf.ny, nz = conf.nz;
  int nbx = conf.nbx, nby = conf.nby, nbz = conf.nbz;
  const char *outFilename = conf.outFilename;

  // Retrieve physical regions.
  PhysicalRegion pr_blocks = regions[1];
  PhysicalRegion pr_real_cells[2];
  pr_real_cells[0] = regions[2];
  pr_real_cells[1] = regions[3];
#ifdef PARTITIONED_EDGE_CELLS
  PhysicalRegion pr_edge_cells[2];
  pr_edge_cells[0] = regions[4];
  pr_edge_cells[1] = regions[5];
  std::vector<PhysicalRegion> pr_block_ptrs;
  for (int id = 0; id < numBlocks; id++) {
    pr_block_ptrs.push_back(regions[6 + id]);
  }
#else
  std::vector<PhysicalRegion> pr_block_ptrs;
  for (int id = 0; id < numBlocks; id++) {
    pr_block_ptrs.push_back(regions[4 + id]);
  }
#endif

  // Retrieve logical regions and index spaces.
  LogicalRegion blocks = pr_blocks.get_logical_region();
  LogicalRegion real_cells[2];
  real_cells[0] = pr_real_cells[0].get_logical_region();
  real_cells[1] = pr_real_cells[1].get_logical_region();
  IndexSpace is_real_cells = real_cells[0].get_index_space();

#ifdef PARTITIONED_EDGE_CELLS
  LogicalRegion edge_cells[2];
  edge_cells[0] = pr_edge_cells[0].get_logical_region();
  edge_cells[1] = pr_edge_cells[1].get_logical_region();
  IndexSpace is_edge_cells = edge_cells[0].get_index_space();
#else
  // we're going to need a field space for the edge regions we create - same
  //  as the real cell field space
  FieldSpace fs_edge_cells = real_cells[0].get_field_space();
#endif

  std::vector<LogicalRegion> block_ptrs;
  for (int id = 0; id < numBlocks; id++) {
    block_ptrs.push_back(pr_block_ptrs[id].get_logical_region());
  }

  // Retrieve accessors.
  RegionAccessor<SOA, Block> a_blocks =
    pr_blocks.get_field_accessor(FID_BLOCK).typeify<Block>().convert<SOA>();
  std::vector<RegionAccessor<SOA, multiptr_t> > a_block_ptrs(numBlocks);
  for (int id = 0; id < numBlocks; id++) {
    a_block_ptrs[id] = pr_block_ptrs[id].get_field_accessor(FID_PTR).typeify<multiptr_t>().convert<SOA>();
  }

  printf("fluid: cells     = %d (%d x %d x %d)\n", nx*ny*nz, nx, ny, nz);
  printf("fluid: divisions = %d x %d x %d\n", nbx, nby, nbz);
  printf("fluid: steps     = %d\n", conf.numSteps);

  // Initialize and partition blocks
  for (int idz = 0; idz < nbz; idz++)
    for (int idy = 0; idy < nby; idy++)
      for (int idx = 0; idx < nbx; idx++) {
        ptr_t id = (idz*nby+idy)*nbx+idx;

        Block &block = a_blocks.ref(id);
        block.id = id.value;
        block.x = idx;
        block.y = idy;
        block.z = idz;

        block.CELLS_X = (conf.splitx[idx+1] - conf.splitx[idx]);
        block.CELLS_Y = (conf.splity[idy+1] - conf.splity[idy]);
        block.CELLS_Z = (conf.splitz[idz+1] - conf.splitz[idz]);
      }

  // first, partition the real cells
  IndexPartition isp_real_cells;
  {
    IndexAllocator isa = runtime->create_index_allocator(ctx, is_real_cells);

    Coloring coloring;

    // Store pointers, set up colors
    for (int idz = 0; idz < nbz; idz++)
      for (int idy = 0; idy < nby; idy++)
        for (int idx = 0; idx < nbx; idx++) {
          int id = (idz*nby+idy)*nbx+idx;
	  const Block& b = a_blocks.ref(id);

          int n_cells = b.CELLS_X * b.CELLS_Y * b.CELLS_Z;
          ptr_t p = isa.alloc(n_cells);

          for (int cz = 0; cz < a_blocks.ref(id).CELLS_Z; cz++)
            for (int cy = 0; cy < a_blocks.ref(id).CELLS_Y; cy++)
              for (int cx = 0; cx < a_blocks.ref(id).CELLS_X; cx++) {
                coloring[id].points.insert(p);
                a_block_ptrs[id].write(get_cell_ptr_ptr(b, cz+1, cy+1, cx+1),
				       multiptr_t(CENTER, p));
                p.value++;
              }
        }

    // Create the partition
    isp_real_cells = runtime->create_index_partition(ctx, is_real_cells,
						     coloring,
						     true /*disjoint*/);
  }
  // now remember all the logical subregions we just made
  std::vector<std::vector<LogicalRegion> > real_cells_by_block(2);

  for(int buffer = 0; buffer < 2; buffer++) {
    LogicalPartition lp = runtime->get_logical_partition(ctx, real_cells[buffer], isp_real_cells);

    real_cells_by_block[buffer].resize(numBlocks);

    for(int id = 0; id < numBlocks; id++)
      real_cells_by_block[buffer][id] = runtime->get_logical_subregion_by_color(ctx, lp, id);
  }

  // setting up edge cells is pretty horrible no matter how you do it...
  std::vector<std::vector<LogicalRegion> > mirror_edges(numBlocks);
  std::vector<std::vector<LogicalRegion> > ghost_edges(numBlocks);
  std::vector<std::vector<LogicalRegion> > mirror_parents(numBlocks);
  std::vector<std::vector<LogicalRegion> > ghost_parents(numBlocks);
  for(int i = 0; i < numBlocks; i++) {
    mirror_edges[i].resize(GHOST_CELLS);
    ghost_edges[i].resize(GHOST_CELLS);
    mirror_parents[i].resize(GHOST_CELLS);
    ghost_parents[i].resize(GHOST_CELLS);
  }
#ifdef PARTITIONED_EDGE_CELLS
  std::vector<std::vector<int> > edge_colors(numBlocks);
  for(int i = 0; i < numBlocks; i++)
    edge_colors[i].resize(GHOST_CELLS);
  IndexPartition isp_edge_cells;
#endif
  {
#ifdef PARTITIONED_EDGE_CELLS
    IndexAllocator isa = runtime->create_index_allocator(ctx, is_edge_cells);

    Coloring coloring;

    int color = 0;
#endif

    // try to handle all directions programmatically
    for(int dir = 0; dir < GHOST_CELLS; dir++) {
      int revdir = REVERSE(dir);
      if(revdir < dir) continue;

      int sides = DIR2SIDES[dir];
      int dx = ((sides & SIDE_LEFT) ? -1 : (sides & SIDE_RIGHT ? 1 : 0));
      int dy = ((sides & SIDE_FRONT) ? -1 : (sides & SIDE_BACK ? 1 : 0));
      int dz = ((sides & SIDE_BOTTOM) ? -1 : (sides & SIDE_TOP ? 1 : 0));

      // iterate over possible "from" blocks
      for(int fbz = -1; fbz <= nbz; fbz++)
	for(int fby = -1; fby <= nby; fby++)
	  for(int fbx = -1; fbx <= nbx; fbx++) {
	    int fid = (fbz * nby + fby) * nbx + fbx;
	    bool fvalid = ((fbx >= 0) && (fbx < nbx) && (fby >= 0) && (fby < nby) &&
			   (fbz >= 0) && (fbz < nbz));

	    // figure out the coordinates, id, validity of "to" block
	    int tbx = fbx + dx;
	    int tby = fby + dy;
	    int tbz = fbz + dz;
	    int tid = (tbz * nby + tby) * nbx + tbx;
	    bool tvalid = ((tbx >= 0) && (tbx < nbx) && (tby >= 0) && (tby < nby) &&
			   (tbz >= 0) && (tbz < nbz));

	    // if neither "from" nor "to" is a valid block, skip this side instance
	    if(!fvalid && !tvalid)
	      continue;

	    const Block *fb = fvalid ? &(a_blocks.ref(fid)) : 0;
	    const Block *tb = tvalid ? &(a_blocks.ref(tid)) : 0;
	    const Block *ab = fb ? fb : tb;  // when either will do
	    if(fvalid && tvalid) {
	      // check that they agree on dimensions if both are valid?
	      
	      assert((dx != 0) || (fb->CELLS_X == tb->CELLS_X));
	      assert((dy != 0) || (fb->CELLS_Y == tb->CELLS_Y));
	      assert((dz != 0) || (fb->CELLS_Z == tb->CELLS_Z));
	    }

	    // now we iterate over cells in the plane/axis/corner
	    int xmin, xmax, ymin, ymax, zmin, zmax;
	    int cells = 1;
	    if(dx != 0) {
	      xmin = xmax = 0;
	    } else {
	      xmin = 1;
	      xmax = ab->CELLS_X;
	      cells *= ab->CELLS_X;
	    }
	    if(dy != 0) {
	      ymin = ymax = 0;
	    } else {
	      ymin = 1;
	      ymax = ab->CELLS_Y;
	      cells *= ab->CELLS_Y;
	    }
	    if(dz != 0) {
	      zmin = zmax = 0;
	    } else {
	      zmin = 1;
	      zmax = ab->CELLS_Z;
	      cells *= ab->CELLS_Z;
	    }

#ifdef PARTITIONED_EDGE_CELLS
	    ptr_t p = isa.alloc(cells);

	    if(fvalid) edge_colors[fid][dir] = color;
	    if(tvalid) edge_colors[tid][revdir] = color;
#else
	    ptr_t p;
	    {
	      IndexSpace is = runtime->create_index_space(ctx, cells);
	      IndexAllocator isa = runtime->create_index_allocator(ctx, is);
	      p = isa.alloc(cells);

	      // two regions based on this index space
	      LogicalRegion lr0 = runtime->create_logical_region(ctx, is, fs_edge_cells);
	      LogicalRegion lr1 = runtime->create_logical_region(ctx, is, fs_edge_cells);

	      // always have dir < REVERSE(dir) here
	      assert(dir < REVERSE(dir));
	      if(fvalid) {
		mirror_edges[fid][dir] = mirror_parents[fid][dir] = lr0;
		ghost_edges[fid][dir] = ghost_parents[fid][dir] = lr1;
	      }
	      if(tvalid) {
		mirror_edges[tid][revdir] = mirror_parents[tid][revdir] = lr1;
		ghost_edges[tid][revdir] = ghost_parents[tid][revdir] = lr0;
	      }
	    }
#endif

	    for(int z = zmin; z <= zmax; z++)
	      for(int y = ymin; y <= ymax; y++)
		for(int x = xmin; x <= xmax; x++) {
#ifdef PARTITIONED_EDGE_CELLS
		  coloring[color].points.insert(p);
#endif
		  if(fvalid) {
		    ptr_t fpp = get_cell_ptr_ptr(*fb,
						 ((dz == 1) ? fb->CELLS_Z+1 :
						  (dz == -1) ? 0 :
						  z),
						 ((dy == 1) ? fb->CELLS_Y+1 :
						  (dy == -1) ? 0 :
						  y),
						 ((dx == 1) ? fb->CELLS_X+1 :
						  (dx == -1) ? 0 :
						  x));
		    a_block_ptrs[fid].write(fpp, multiptr_t(tvalid ? dir : BOUNDARY, p));
		  }

		  if(tvalid) {
		    ptr_t tpp = get_cell_ptr_ptr(*tb,
						 ((dz == -1) ? tb->CELLS_Z+1 :
						  (dz == 1) ? 0 :
						  z),
						 ((dy == -1) ? tb->CELLS_Y+1 :
						  (dy == 1) ? 0 :
						  y),
						 ((dx == -1) ? tb->CELLS_X+1 :
						  (dx == 1) ? 0 :
						  x));
		    a_block_ptrs[tid].write(tpp, multiptr_t(fvalid ? revdir : BOUNDARY, p));
		  }

		  p.value++;
		}

#ifdef PARTITIONED_EDGE_CELLS
	    color++;
#endif
	  }
    }

#ifdef PARTITIONED_EDGE_CELLS
    // now partition the edge cells
    isp_edge_cells = runtime->create_index_partition(ctx, is_edge_cells,
						     coloring, true/*disjoint*/);
#endif
  }

#ifdef PARTITIONED_EDGE_CELLS
  // now use the colors we remembered to get the sets of "mirror" and "ghost" edge regions for
  //  each block
  {
    LogicalPartition lp0 = runtime->get_logical_partition(ctx, edge_cells[0], isp_edge_cells);
    LogicalPartition lp1 = runtime->get_logical_partition(ctx, edge_cells[1], isp_edge_cells);

    for(int i = 0; i < numBlocks; i++) {
      for(int dir = 0; dir < GHOST_CELLS; dir++) {
	if(dir < REVERSE(dir)) {
	  mirror_edges[i][dir] = runtime->get_logical_subregion_by_color(ctx, lp0, edge_colors[i][dir]);
	  ghost_edges[i][dir] = runtime->get_logical_subregion_by_color(ctx, lp1,  edge_colors[i][dir]);
	  mirror_parents[i][dir] = edge_cells[0];
	  ghost_parents[i][dir] = edge_cells[1];
	} else {
	  mirror_edges[i][dir] = runtime->get_logical_subregion_by_color(ctx, lp1, edge_colors[i][dir]);
	  ghost_edges[i][dir] = runtime->get_logical_subregion_by_color(ctx, lp0,  edge_colors[i][dir]);
	  mirror_parents[i][dir] = edge_cells[1];
	  ghost_parents[i][dir] = edge_cells[0];
	}
      }
    }
  }
#endif

  // Unmap the physical regions we intend to pass to children
  runtime->unmap_region(ctx, pr_config);
  runtime->unmap_region(ctx, pr_blocks);
  runtime->unmap_region(ctx, pr_real_cells[0]);
  runtime->unmap_region(ctx, pr_real_cells[1]);
#ifdef PARTITIONED_EDGE_CELLS
  runtime->unmap_region(ctx, pr_edge_cells[0]);
  runtime->unmap_region(ctx, pr_edge_cells[1]);
#endif
  for (int id = 0; id < numBlocks; id++) {
    runtime->unmap_region(ctx, pr_block_ptrs[id]);
  }

  // Initialize the simulation in buffer 1
  {
    std::list<Future> load_futures;

    std::vector<FieldID> cell_fields;
    cell_fields.push_back(FID_CELL_NUM_PARTICLES);
    cell_fields.push_back(FID_CELL_P_X);
    cell_fields.push_back(FID_CELL_P_Y);
    cell_fields.push_back(FID_CELL_P_Z);
    cell_fields.push_back(FID_CELL_HV_X);
    cell_fields.push_back(FID_CELL_HV_Y);
    cell_fields.push_back(FID_CELL_HV_Z);
    cell_fields.push_back(FID_CELL_V_X);
    cell_fields.push_back(FID_CELL_V_Y);
    cell_fields.push_back(FID_CELL_V_Z);
#ifdef TRACK_PARTICLE_IDS
    cell_fields.push_back(FID_CELL_ID);
#endif

    for (int id = 0; id < numBlocks; id++) {
      int buffer = id;
      TaskLauncher load(TASKID_LOAD_FILE,
                        TaskArgument((void *)&buffer, sizeof(buffer)),
                        Predicate::TRUE_PRED, 0, id);

      load.add_region_requirement(RegionRequirement(config, READ_ONLY, EXCLUSIVE, config)
				  .add_field(FID_CONFIG));
      load.add_region_requirement(RegionRequirement(blocks, READ_ONLY, EXCLUSIVE, blocks)
				  .add_field(FID_BLOCK));
      load.add_region_requirement(RegionRequirement(block_ptrs[id], READ_ONLY, EXCLUSIVE, block_ptrs[id])
				  .add_field(FID_PTR));

      load.add_region_requirement(RegionRequirement(real_cells_by_block[1][id], WRITE_DISCARD, EXCLUSIVE,
						    real_cells[1])
				  .add_fields(cell_fields));

      Future f = runtime->execute_task(ctx, load);
      load_futures.push_back(f);
    }

    while (load_futures.size() > 0) {
      load_futures.front().get_void_result();
      load_futures.pop_front();
    }
  }

  printf("STARTING MAIN SIMULATION LOOP\n");
  double start_time = LegionRuntime::TimeStamp::get_current_time_in_micros();
  std::list<Future> futures;
  LegionRuntime::DetailedTimer::clear_timers();

  int cur_buffer = 0;  // buffer we're generating on this pass

  // Run the simulation
  for (int step = 0; step < conf.numSteps; step++) {

    // Trace runtime calls to amortize cost of analysis
    runtime->begin_trace(ctx, cur_buffer);

    // Initialize cells
    //for (int id = 0; id < numBlocks; id++)
    for (int spass = conf.sync ? 1 : 0; spass >= 0; spass--) {
      std::list<Future> phase_futures;

      std::vector<FieldID> cell_fields;
      cell_fields.push_back(FID_CELL_NUM_PARTICLES);
      cell_fields.push_back(FID_CELL_P_X);
      cell_fields.push_back(FID_CELL_P_Y);
      cell_fields.push_back(FID_CELL_P_Z);
      cell_fields.push_back(FID_CELL_HV_X);
      cell_fields.push_back(FID_CELL_HV_Y);
      cell_fields.push_back(FID_CELL_HV_Z);
      cell_fields.push_back(FID_CELL_V_X);
      cell_fields.push_back(FID_CELL_V_Y);
      cell_fields.push_back(FID_CELL_V_Z);
#ifdef TRACK_PARTICLE_IDS
      cell_fields.push_back(FID_CELL_ID);
#endif

      for (int id = (numBlocks-1); id >= 0; id--) {
        int buf[2];
        buf[0] = cur_buffer;
        buf[1] = id;
        TaskLauncher init(TASKID_INIT_CELLS,
                          TaskArgument((void *)&buf, sizeof(buf)),
                          Predicate::TRUE_PRED, 0, id);

        // init and rebuild reads the real cells from the previous pass and
        //  moves atoms into the real cells for this pass or the edge0 cells
        init.add_region_requirement(RegionRequirement(config, READ_ONLY, EXCLUSIVE, config)
				    .add_field(FID_CONFIG));
        init.add_region_requirement(RegionRequirement(blocks, READ_ONLY, EXCLUSIVE, blocks)
				    .add_field(FID_BLOCK));
        init.add_region_requirement(RegionRequirement(block_ptrs[id], READ_ONLY, EXCLUSIVE, block_ptrs[id])
				    .add_field(FID_PTR));

        // read old
        init.add_region_requirement(RegionRequirement(real_cells_by_block[1 - cur_buffer][id],
						      READ_ONLY, EXCLUSIVE,
						      real_cells[1 - cur_buffer])
				    .add_fields(cell_fields));
        // write new
        init.add_region_requirement(RegionRequirement(real_cells_by_block[cur_buffer][id],
						      WRITE_DISCARD, EXCLUSIVE,
						      real_cells[cur_buffer])
				    .add_fields(cell_fields));

        // write into ghost cells
        add_edge_regions(init, ghost_edges[id], ghost_parents[id],
			 WRITE_DISCARD, EXCLUSIVE, cell_fields);

        if (spass > 0) {
          // run a dummy task to move data in a separate phase
          init.task_id = TASKID_INIT_CELLS_NOP;
          Future f = runtime->execute_task(ctx, init);
          phase_futures.push_back(f);
        } else {
          Future f = runtime->execute_task(ctx, init);

          if (conf.sync) {
            phase_futures.push_back(f);
          }
        }
      }

      while (phase_futures.size() > 0) {
        Future f = phase_futures.front();
        phase_futures.pop_front();
        if (conf.sync <= 1) {
          f.get_void_result();
        }
      }
    }

    // Rebuild reduce (reduction)
    //for (int id = 0; id < numBlocks; id++)
    for (int spass = conf.sync ? 1 : 0; spass >= 0; spass--) {
      std::list<Future> phase_futures;

      std::vector<FieldID> cell_fields;
      cell_fields.push_back(FID_CELL_NUM_PARTICLES);
      cell_fields.push_back(FID_CELL_P_X);
      cell_fields.push_back(FID_CELL_P_Y);
      cell_fields.push_back(FID_CELL_P_Z);
      cell_fields.push_back(FID_CELL_HV_X);
      cell_fields.push_back(FID_CELL_HV_Y);
      cell_fields.push_back(FID_CELL_HV_Z);
      cell_fields.push_back(FID_CELL_V_X);
      cell_fields.push_back(FID_CELL_V_Y);
      cell_fields.push_back(FID_CELL_V_Z);
#ifdef TRACK_PARTICLE_IDS
      cell_fields.push_back(FID_CELL_ID);
#endif

      for (int id = (numBlocks-1); id >= 0; id--) {
        int buf[2];
        buf[0] = cur_buffer;
        buf[1] = id;
        TaskLauncher rebuild(TASKID_REBUILD_REDUCE,
                             TaskArgument((void *)&buf, sizeof(buf)),
                             Predicate::TRUE_PRED, 0, id);

        // rebuild reduce reads the cells provided by neighbors, incorporates
        //  them into its own cells, and puts copies of those boundary cells into
        //  the ghosts to exchange back
        //
        // edge phase here is _1_

        rebuild.add_region_requirement(RegionRequirement(blocks, READ_ONLY, EXCLUSIVE, blocks)
				       .add_field(FID_BLOCK));
        rebuild.add_region_requirement(RegionRequirement(block_ptrs[id], READ_ONLY, EXCLUSIVE,
							 block_ptrs[id])
				       .add_field(FID_PTR));
        rebuild.add_region_requirement(RegionRequirement(real_cells_by_block[cur_buffer][id],
							 READ_WRITE, EXCLUSIVE,
							 real_cells[cur_buffer])
				       .add_fields(cell_fields));

        // read and write just our own mirror cells
        add_edge_regions(rebuild, mirror_edges[id], mirror_parents[id],
			 READ_WRITE, EXCLUSIVE, cell_fields);

        if (spass > 0) {
          // run a dummy task to move data in a separate phase
          rebuild.task_id = TASKID_REBUILD_REDUCE_NOP;
          Future f = runtime->execute_task(ctx, rebuild);
          phase_futures.push_back(f);
        } else {
          Future f = runtime->execute_task(ctx, rebuild);

          if (conf.sync) {
            phase_futures.push_back(f);
          }
        }
      }

      while (phase_futures.size() > 0) {
        Future f = phase_futures.front();
        phase_futures.pop_front();
        if (conf.sync <= 1) {
          f.get_void_result();
        }
      }
    }

    // separate task to clear densities
    for (int spass = conf.sync ? 1 : 0; spass >= 0; spass--) {
      std::list<Future> phase_futures;

      for (int id = (numBlocks-1); id >= 0; id--) {
        int buf[2];
        buf[0] = cur_buffer;
        buf[1] = id;
        TaskLauncher density(TASKID_CLEAR_DENSITIES,
                             TaskArgument((void *)&buf, sizeof(buf)),
                             Predicate::TRUE_PRED, 0, id);

        density.add_region_requirement(RegionRequirement(config, READ_ONLY, EXCLUSIVE, config)
				       .add_field(FID_CONFIG));
        density.add_region_requirement(RegionRequirement(blocks, READ_ONLY, EXCLUSIVE, blocks)
				       .add_field(FID_BLOCK));
        density.add_region_requirement(RegionRequirement(block_ptrs[id], READ_ONLY, EXCLUSIVE,
							 block_ptrs[id])
				       .add_field(FID_PTR));
	// we write the density field (don't bother to read the num_particles
	//  field - just clear them all
        density.add_region_requirement(RegionRequirement(real_cells_by_block[cur_buffer][id],
							 WRITE_DISCARD, EXCLUSIVE,
							 real_cells[cur_buffer])
				       .add_field(FID_CELL_DENSITY));

        if (spass > 0) {
          // run a dummy task to move data in a separate phase
          density.task_id = TASKID_SCATTER_DENSITIES_NOP;
          Future f = runtime->execute_task(ctx, density);
          phase_futures.push_back(f);
        } else {
          Future f = runtime->execute_task(ctx, density);

          if (conf.sync) {
            phase_futures.push_back(f);
          }
        }
      }

      while (phase_futures.size() > 0) {
        Future f = phase_futures.front();
        phase_futures.pop_front();
        if (conf.sync <= 1) {
          f.get_void_result();
        }
      }
    }

    // init forces and scatter densities
    //for (int id = 0; id < numBlocks; id++)
    for (int spass = conf.sync ? 1 : 0; spass >= 0; spass--) {
      std::list<Future> phase_futures;

      // we're going to read position and particle count from our real cells and ghosts
      // and we're going to write our densities to real cells and mirrors
      std::vector<FieldID> real_cell_fields, ghost_cell_fields, mirror_cell_fields;
      real_cell_fields.push_back(FID_CELL_NUM_PARTICLES);
      real_cell_fields.push_back(FID_CELL_P_X);
      real_cell_fields.push_back(FID_CELL_P_Y);
      real_cell_fields.push_back(FID_CELL_P_Z);
      real_cell_fields.push_back(FID_CELL_DENSITY);

      ghost_cell_fields.push_back(FID_CELL_NUM_PARTICLES);
      ghost_cell_fields.push_back(FID_CELL_P_X);
      ghost_cell_fields.push_back(FID_CELL_P_Y);
      ghost_cell_fields.push_back(FID_CELL_P_Z);

      mirror_cell_fields.push_back(FID_CELL_DENSITY);

#ifdef TRACK_PARTICLE_IDS
      real_cell_fields.push_back(FID_CELL_ID);
      ghost_cell_fields.push_back(FID_CELL_ID);
#endif

      for (int id = (numBlocks-1); id >= 0; id--) {
        int buf[2];
        buf[0] = cur_buffer;
        buf[1] = id;
        TaskLauncher density(TASKID_SCATTER_DENSITIES,
                             TaskArgument((void *)&buf, sizeof(buf)),
                             Predicate::TRUE_PRED, 0, id);

        // this step looks at positions in real and edge cells and updates
        // densities for all owned particles - boundary real cells are copied to
        // the edge cells for exchange
        //
        // edge phase here is _0_

        density.add_region_requirement(RegionRequirement(config, READ_ONLY, EXCLUSIVE, config)
				       .add_field(FID_CONFIG));
        density.add_region_requirement(RegionRequirement(blocks, READ_ONLY, EXCLUSIVE, blocks)
				       .add_field(FID_BLOCK));
        density.add_region_requirement(RegionRequirement(block_ptrs[id], READ_ONLY, EXCLUSIVE,
							 block_ptrs[id])
				       .add_field(FID_PTR));
        density.add_region_requirement(RegionRequirement(real_cells_by_block[cur_buffer][id],
							 READ_WRITE, EXCLUSIVE,
							 real_cells[cur_buffer])
				       .add_fields(real_cell_fields));

        // read ghost
        add_edge_regions(density, ghost_edges[id], ghost_parents[id],
			 READ_ONLY, EXCLUSIVE, ghost_cell_fields);

        // write mirror
        add_edge_regions(density, mirror_edges[id], mirror_parents[id],
			 WRITE_DISCARD, EXCLUSIVE, mirror_cell_fields);

        if (spass > 0) {
          // run a dummy task to move data in a separate phase
          density.task_id = TASKID_SCATTER_DENSITIES_NOP;
          Future f = runtime->execute_task(ctx, density);
          phase_futures.push_back(f);
        } else {
          Future f = runtime->execute_task(ctx, density);

          if (conf.sync) {
            phase_futures.push_back(f);
          }
        }
      }

      while (phase_futures.size() > 0) {
        Future f = phase_futures.front();
        phase_futures.pop_front();
        if (conf.sync <= 1) {
          f.get_void_result();
        }
      }
    }

    // separate task to clear accelerations before accumulation

    // Gather forces and advance
    //for (int id = 0; id < numBlocks; id++)
    for (int spass = conf.sync ? 1 : 0; spass >= 0; spass--) {
      std::list<Future> phase_futures;

      for (int id = (numBlocks-1); id >= 0; id--) {
        int buf[2];
        buf[0] = cur_buffer;
        buf[1] = id;
        TaskLauncher force(TASKID_CLEAR_FORCES,
                           TaskArgument((void *)&buf, sizeof(buf)),
                           Predicate::TRUE_PRED, 0, id);

        force.add_region_requirement(RegionRequirement(config, READ_ONLY, EXCLUSIVE, config)
				     .add_field(FID_CONFIG));
        force.add_region_requirement(RegionRequirement(blocks, READ_ONLY, EXCLUSIVE, blocks)
				     .add_field(FID_BLOCK));
        force.add_region_requirement(RegionRequirement(block_ptrs[id], READ_ONLY, EXCLUSIVE,
						       block_ptrs[id])
				     .add_field(FID_PTR));
        force.add_region_requirement(RegionRequirement(real_cells_by_block[cur_buffer][id],
						       WRITE_DISCARD, EXCLUSIVE,
						       real_cells[cur_buffer])
				     .add_field(FID_CELL_A_X)
				     .add_field(FID_CELL_A_Y)
				     .add_field(FID_CELL_A_Z));

        if (spass > 0) {
          // run a dummy task to move data in a separate phase
          force.task_id = TASKID_GATHER_FORCES_NOP;
          Future f = runtime->execute_task(ctx, force);
          phase_futures.push_back(f);
        } else {
          Future f = runtime->execute_task(ctx, force);

          // remember the futures for the last pass so we can wait on them
          if (step == conf.numSteps - 1) {
            futures.push_back(f);
          } else if (conf.sync) {
              phase_futures.push_back(f);
          }
        }
      }

      while (phase_futures.size() > 0) {
        Future f = phase_futures.front();
        phase_futures.pop_front();
        if (conf.sync <= 1) {
          f.get_void_result();
        }
      }
    }

    for (int spass = conf.sync ? 1 : 0; spass >= 0; spass--) {
      std::list<Future> phase_futures;

      // we'll touch nearly all of the real cell fields, but just need
      //  p, v, density from ghosts
      std::vector<FieldID> real_cell_fields, ghost_cell_fields;
      real_cell_fields.push_back(FID_CELL_NUM_PARTICLES);
      real_cell_fields.push_back(FID_CELL_P_X);
      real_cell_fields.push_back(FID_CELL_P_Y);
      real_cell_fields.push_back(FID_CELL_P_Z);
      real_cell_fields.push_back(FID_CELL_HV_X);
      real_cell_fields.push_back(FID_CELL_HV_Y);
      real_cell_fields.push_back(FID_CELL_HV_Z);
      real_cell_fields.push_back(FID_CELL_V_X);
      real_cell_fields.push_back(FID_CELL_V_Y);
      real_cell_fields.push_back(FID_CELL_V_Z);
      real_cell_fields.push_back(FID_CELL_A_X);
      real_cell_fields.push_back(FID_CELL_A_Y);
      real_cell_fields.push_back(FID_CELL_A_Z);
      real_cell_fields.push_back(FID_CELL_DENSITY);

      ghost_cell_fields.push_back(FID_CELL_NUM_PARTICLES);
      ghost_cell_fields.push_back(FID_CELL_P_X);
      ghost_cell_fields.push_back(FID_CELL_P_Y);
      ghost_cell_fields.push_back(FID_CELL_P_Z);
      ghost_cell_fields.push_back(FID_CELL_V_X);
      ghost_cell_fields.push_back(FID_CELL_V_Y);
      ghost_cell_fields.push_back(FID_CELL_V_Z);
      ghost_cell_fields.push_back(FID_CELL_DENSITY);

#ifdef TRACK_PARTICLE_IDS
      real_cell_fields.push_back(FID_CELL_ID);
      ghost_cell_fields.push_back(FID_CELL_ID);
#endif

      for (int id = (numBlocks-1); id >= 0; id--) {
        int buf[2];
        buf[0] = cur_buffer;
        buf[1] = id;
        TaskLauncher force(TASKID_GATHER_FORCES,
                           TaskArgument((void *)&buf, sizeof(buf)),
                           Predicate::TRUE_PRED, 0, id);

        // this is very similar to scattering of density - basically just
        //  different math, and a different edge phase
        // actually, since this fully calculates the accelerations, we just
        //  advance positions in this task as well and we're done with an
        //  iteration
        //
        // edge phase here is _1_

        force.add_region_requirement(RegionRequirement(config, READ_ONLY, EXCLUSIVE, config)
				     .add_field(FID_CONFIG));
        force.add_region_requirement(RegionRequirement(blocks, READ_ONLY, EXCLUSIVE, blocks)
				     .add_field(FID_BLOCK));
        force.add_region_requirement(RegionRequirement(block_ptrs[id], READ_ONLY, EXCLUSIVE,
						       block_ptrs[id])
				     .add_field(FID_PTR));
        force.add_region_requirement(RegionRequirement(real_cells_by_block[cur_buffer][id],
						       READ_WRITE, EXCLUSIVE,
						       real_cells[cur_buffer])
				     .add_fields(real_cell_fields));

        // read ghosts
        add_edge_regions(force, ghost_edges[id], ghost_parents[id],
			 READ_ONLY, EXCLUSIVE, ghost_cell_fields);

        if (spass > 0) {
          // run a dummy task to move data in a separate phase
          force.task_id = TASKID_GATHER_FORCES_NOP;
          Future f = runtime->execute_task(ctx, force);
          phase_futures.push_back(f);
        } else {
          Future f = runtime->execute_task(ctx, force);

          // remember the futures for the last pass so we can wait on them
          if (step == conf.numSteps - 1) {
            futures.push_back(f);
          } else if (conf.sync) {
              phase_futures.push_back(f);
          }
        }
      }

      while (phase_futures.size() > 0) {
        Future f = phase_futures.front();
        phase_futures.pop_front();
        if (conf.sync <= 1) {
          f.get_void_result();
        }
      }
    }

    // Trace runtime calls to amortize cost of analysis
    runtime->end_trace(ctx, cur_buffer);

    // flip the phase
    if(!conf.repeat_first_timestep)
      cur_buffer = 1 - cur_buffer;
  }

  log_app.info("waiting for all simulation tasks to complete");

  while (futures.size() > 0) {
    futures.front().get_void_result();
    futures.pop_front();
  }
  double end_time = LegionRuntime::TimeStamp::get_current_time_in_micros();

  double sim_time = 1e-6 * (end_time - start_time);
  printf("ELAPSED TIME = %7.3f s\n", sim_time);
  LegionRuntime::DetailedTimer::report_timers();

  if (outFilename[0] != '\0') {
    int target_buffer = (conf.repeat_first_timestep ? cur_buffer : (1 - cur_buffer));

    std::vector<FieldID> cell_fields;
    cell_fields.push_back(FID_CELL_NUM_PARTICLES);
    cell_fields.push_back(FID_CELL_P_X);
    cell_fields.push_back(FID_CELL_P_Y);
    cell_fields.push_back(FID_CELL_P_Z);
    cell_fields.push_back(FID_CELL_HV_X);
    cell_fields.push_back(FID_CELL_HV_Y);
    cell_fields.push_back(FID_CELL_HV_Z);
    cell_fields.push_back(FID_CELL_V_X);
    cell_fields.push_back(FID_CELL_V_Y);
    cell_fields.push_back(FID_CELL_V_Z);
#ifdef TRACK_PARTICLE_IDS
      cell_fields.push_back(FID_CELL_ID);
#endif

    TaskLauncher save(TASKID_SAVE_FILE,
                      TaskArgument((void *)&target_buffer,
                                   sizeof(target_buffer)));

    save.add_region_requirement(RegionRequirement(config, READ_ONLY, EXCLUSIVE, config)
				.add_field(FID_CONFIG));
    save.add_region_requirement(RegionRequirement(blocks, READ_ONLY, EXCLUSIVE, blocks)
				.add_field(FID_BLOCK));
    save.add_region_requirement(RegionRequirement(real_cells[target_buffer], READ_ONLY, EXCLUSIVE,
						  real_cells[target_buffer])
				.add_fields(cell_fields));
    for (int id = 0; id < numBlocks; id++) {
      save.add_region_requirement(RegionRequirement(block_ptrs[id], READ_ONLY, EXCLUSIVE,
						    block_ptrs[id])
				  .add_field(FID_PTR));
    }

    Future f = runtime->execute_task(ctx, save);
    f.get_void_result();
  }

  log_app.info("all done!");
}

static inline int GET_DIR(const Block &b, int idz, int idy, int idx)
{
  return LOOKUP_DIR(idx == 0 ? -1 : (idx == (int)(b.CELLS_X+1) ? 1 : 0),
                    idy == 0 ? -1 : (idy == (int)(b.CELLS_Y+1) ? 1 : 0),
                    idz == 0 ? -1 : (idz == (int)(b.CELLS_Z+1) ? 1 : 0));
}

template<class AT>
static inline multiptr_t& get_cell_ptr(const Block &b, int cz, int cy, int cx,
				       RegionAccessor<AT, multiptr_t> &block_ptrs)
{
  return block_ptrs.ref(get_cell_ptr_ptr(b, cz, cy, cx));
}

template<class AT>
static inline multiptr_t& get_cell_ptr(const Block &b, int cb, int eb, int cz, int cy, int cx,
				       RegionAccessor<AT, multiptr_t> &block_ptrs)
{
  assert(0);
  //return block_ptrs.ref(get_cell_ptr_ptr(b, cb, eb, cz, cy, cx));
}

template<typename AT, typename T>
class AccessorCube {
public:
  AccessorCube(const std::vector<PhysicalRegion> &regions, int center_idx, int edge_start,
	       FieldID fid)
  {
    for(int i = 0; i < GHOST_CELLS; i++)
      accs[i] = regions[edge_start + i].get_field_accessor(fid).template typeify<T>().template convert<AT>();

    accs[CENTER] = regions[center_idx].get_field_accessor(fid).template typeify<T>().template convert<AT>();
  }

  T &ref(const multiptr_t &mp)
  {
    return accs[mp.dir].ref(mp.ptr);
  }

  void write(const multiptr_t &mp, const T& newval)
  {
    accs[mp.dir].write(mp.ptr, newval);
  }

  RegionAccessor<AT, T> accs[GHOST_CELLS + 1];
};

#if 0
template<class AT>
static inline Cell& REF_CELL(Block &b, int cb, int eb, int cz, int cy, int cx,
                             RegionAccessor<AT, Cell> &base,
                             std::vector<RegionAccessor<AT, Cell> > &edge,
                             RegionAccessor<AT, ptr_t> &block_ptrs)
{
  int dir = GET_DIR(b, cz, cy,cx);
  if (dir == CENTER) {
    return base.ref(get_cell_ptr(b, cb, cz, cy, cx, block_ptrs));
  } else {
    return edge[dir].ref(get_cell_ptr(b, eb, cz, cy, cx, block_ptrs));
  }
}

template<class AT>
static inline void WRITE_CELL(Block &b, int cb, int eb, int cz, int cy, int cx,
                              RegionAccessor<AT, Cell> &base,
                              std::vector<RegionAccessor<AT, Cell> > &edge,
                              RegionAccessor<AT, ptr_t> &block_ptrs,
                              Cell &cell)
{
  int dir = GET_DIR(b, cz, cy,cx);
  if (dir == CENTER) {
    base.write(get_cell_ptr(b, cb, cz, cy, cx, block_ptrs), cell);
  } else {
    edge[dir].write(get_cell_ptr(b, eb, cz, cy, cx, block_ptrs), cell);
  }
}
#endif

template <typename T>
static RegionAccessor<SOA, T> soa(const PhysicalRegion& p, FieldID fid)
{
  return p.get_field_accessor(fid).template typeify<T>().template convert<SOA>();
}

template<class AT, class T>
static T& get_singleton_ref(PhysicalRegion physical, FieldID fid)
{
  RegionAccessor<AT, T> accessor =
    physical.get_field_accessor(fid).typeify<T>().template convert<AT>();
  return accessor.ref(base_ptr);
}

template<class AT, class T>
static T& get_array_ref(PhysicalRegion physical, FieldID fid, int index)
{
  RegionAccessor<AT, T> accessor =
    physical.get_field_accessor(fid).typeify<T>().template convert<AT>();
  return accessor.ref(ptr_t(index));
}

void init_and_rebuild(const Task *task,
                      const std::vector<PhysicalRegion> &regions,
                      Context ctx, HighLevelRuntime *runtime)
{
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp total_stamp("Init and Rebuild total: ",true);
  LegionRuntime::TimeStamp start_stamp("Init and Rebuild start: ",false);
#endif
  assert(task->arglen == sizeof(int)*2);
  // int cb = ((const int *)task->args)[0];
  int bid = ((const int *)task->args)[1];
  // int eb = 0; // edge phase for this task is 0

  // Initialize all the cells and update all our cells
  PhysicalRegion config_region = regions[0];
  PhysicalRegion block_region = regions[1];
  PhysicalRegion block_ptrs_region = regions[2];
  PhysicalRegion src_block = regions[3];
  PhysicalRegion dst_block = regions[4];
  PhysicalRegion edge_blocks[GHOST_CELLS];
  for (int i = 0; i < GHOST_CELLS; i++) {
    edge_blocks[i] = regions[i + 5];
  }

  RegionAccessor<SOA, multiptr_t> block_ptrs =
    block_ptrs_region.get_field_accessor(FID_PTR).typeify<multiptr_t>().convert<SOA>();

  AccessorCube<SOA, int> dst_num_particles(regions, 4, 5, FID_CELL_NUM_PARTICLES);
  AccessorCube<SOA, Floats> dst_p_x(regions, 4, 5, FID_CELL_P_X);
  AccessorCube<SOA, Floats> dst_p_y(regions, 4, 5, FID_CELL_P_Y);
  AccessorCube<SOA, Floats> dst_p_z(regions, 4, 5, FID_CELL_P_Z);
  AccessorCube<SOA, Floats> dst_hv_x(regions, 4, 5, FID_CELL_HV_X);
  AccessorCube<SOA, Floats> dst_hv_y(regions, 4, 5, FID_CELL_HV_Y);
  AccessorCube<SOA, Floats> dst_hv_z(regions, 4, 5, FID_CELL_HV_Z);
  AccessorCube<SOA, Floats> dst_v_x(regions, 4, 5, FID_CELL_V_X);
  AccessorCube<SOA, Floats> dst_v_y(regions, 4, 5, FID_CELL_V_Y);
  AccessorCube<SOA, Floats> dst_v_z(regions, 4, 5, FID_CELL_V_Z);
#ifdef TRACK_PARTICLE_IDS
  AccessorCube<SOA, Ints> dst_id(regions, 4, 5, FID_CELL_ID);
#endif
  
  const Config &conf = get_singleton_ref<SOA, Config>(config_region, FID_CONFIG);
  int nx = conf.nx, ny = conf.ny, nz = conf.nz;
  //int &nbx = conf.nbx, &nby = conf.nby, &nbz = conf.nbz;
  Vec3 delta = conf.delta;

  const Block &b = get_array_ref<SOA, Block>(block_region, FID_BLOCK, bid);

  log_app.info("In init_and_rebuild() for block %d", b.id);

  // start by clearing the particle count on all the destination cells
  for (int cz = 0; cz <= (int)b.CELLS_Z + 1; cz++)
    for (int cy = 0; cy <= (int)b.CELLS_Y + 1; cy++)
      for (int cx = 0; cx <= (int)b.CELLS_X + 1; cx++) {
	multiptr_t cptr = get_cell_ptr(b, cz, cy, cx, block_ptrs);
	//printf("(%d,%d,%d) -> (%d,%d)\n", cx, cy, cz, cptr.dir, cptr.ptr.value);
	if(cptr.dir == BOUNDARY) continue;
	dst_num_particles.write(cptr, 0);
        //REF_CELL(b, cb, eb, cz, cy, cx, dst, edges, block_ptrs).num_particles = 0;
      }

#if 0
  // Minimum block sizes
  unsigned mbsx = nx / nbx;
  unsigned mbsy = ny / nby;
  unsigned mbsz = nz / nbz;

  // Number of oversized blocks
  unsigned ovbx = nx % nbx;
  unsigned ovby = ny % nby;
  unsigned ovbz = nz % nbz;
#endif

  RegionAccessor<SOA, int> src_num_particles = soa<int>(src_block, FID_CELL_NUM_PARTICLES);
  RegionAccessor<SOA, Floats> src_p_x = soa<Floats>(src_block, FID_CELL_P_X);
  RegionAccessor<SOA, Floats> src_p_y = soa<Floats>(src_block, FID_CELL_P_Y);
  RegionAccessor<SOA, Floats> src_p_z = soa<Floats>(src_block, FID_CELL_P_Z);
  RegionAccessor<SOA, Floats> src_hv_x = soa<Floats>(src_block, FID_CELL_HV_X);
  RegionAccessor<SOA, Floats> src_hv_y = soa<Floats>(src_block, FID_CELL_HV_Y);
  RegionAccessor<SOA, Floats> src_hv_z = soa<Floats>(src_block, FID_CELL_HV_Z);
  RegionAccessor<SOA, Floats> src_v_x = soa<Floats>(src_block, FID_CELL_V_X);
  RegionAccessor<SOA, Floats> src_v_y = soa<Floats>(src_block, FID_CELL_V_Y);
  RegionAccessor<SOA, Floats> src_v_z = soa<Floats>(src_block, FID_CELL_V_Z);
#ifdef TRACK_PARTICLE_IDS
  RegionAccessor<SOA, Ints> src_id = soa<Ints>(src_block, FID_CELL_ID);
#endif

  // now go through each source cell and move particles that have wandered too
  //  far
  for (int cz = 1; cz < (int)b.CELLS_Z + 1; cz++)
    for (int cy = 1; cy < (int)b.CELLS_Y + 1; cy++)
      for (int cx = 1; cx < (int)b.CELLS_X + 1; cx++) {
	multiptr_t sptr = get_cell_ptr(b, cz, cy, cx, block_ptrs);
	assert(sptr.dir == CENTER);

	int snp = src_num_particles.read(sptr.ptr);
	assert(snp <= MAX_PARTICLES);

	const Floats& spx = src_p_x.ref(sptr.ptr);
	const Floats& spy = src_p_y.ref(sptr.ptr);
	const Floats& spz = src_p_z.ref(sptr.ptr);
	const Floats& shvx = src_hv_x.ref(sptr.ptr);
	const Floats& shvy = src_hv_y.ref(sptr.ptr);
	const Floats& shvz = src_hv_z.ref(sptr.ptr);
	const Floats& svx = src_v_x.ref(sptr.ptr);
	const Floats& svy = src_v_y.ref(sptr.ptr);
	const Floats& svz = src_v_z.ref(sptr.ptr);
#ifdef TRACK_PARTICLE_IDS
	const Ints& sid = src_id.ref(sptr.ptr);
#endif

        // don't need to macro-ize this because it's known to be a real cell
        for (unsigned p = 0; p < snp; p++) {
          // Global dst coordinates
          int di = (int)((spx[p] - domainMin.x) / delta.x);
          int dj = (int)((spy[p] - domainMin.y) / delta.y);
          int dk = (int)((spz[p] - domainMin.z) / delta.z);

          if (di < 0) di = 0; else if (di > (int)(nx-1)) di = nx-1;
          if (dj < 0) dj = 0; else if (dj > (int)(ny-1)) dj = ny-1;
          if (dk < 0) dk = 0; else if (dk > (int)(nz-1)) dk = nz-1;

          // Global src coordinates
#if 0
          int ci = cx + (b.x*mbsx + (b.x < ovbx ? b.x : ovbx)) - 1;
          int cj = cy + (b.y*mbsy + (b.y < ovby ? b.y : ovby)) - 1;
          int ck = cz + (b.z*mbsz + (b.z < ovbz ? b.z : ovbz)) - 1;
#endif
          int ci = cx + conf.splitx[b.x] - 1;
          int cj = cy + conf.splity[b.y] - 1;
          int ck = cz + conf.splitz[b.z] - 1;

          // Assume particles move no more than one block per timestep
          if (di - ci < -1) di = ci - 1; else if (di - ci > 1) di = ci + 1;
          if (dj - cj < -1) dj = cj - 1; else if (dj - cj > 1) dj = cj + 1;
          if (dk - ck < -1) dk = ck - 1; else if (dk - ck > 1) dk = ck + 1;
          int dx = cx + (di - ci);
          int dy = cy + (dj - cj);
          int dz = cz + (dk - ck);

	  multiptr_t dptr = get_cell_ptr(b, dz, dy, dx, block_ptrs);
	  assert(dptr.dir != BOUNDARY);
	  int &dnp = dst_num_particles.ref(dptr);
	  Floats &dpx = dst_p_x.ref(dptr);
	  Floats &dpy = dst_p_y.ref(dptr);
	  Floats &dpz = dst_p_z.ref(dptr);
	  Floats &dhvx = dst_hv_x.ref(dptr);
	  Floats &dhvy = dst_hv_y.ref(dptr);
	  Floats &dhvz = dst_hv_z.ref(dptr);
	  Floats &dvx = dst_v_x.ref(dptr);
	  Floats &dvy = dst_v_y.ref(dptr);
	  Floats &dvz = dst_v_z.ref(dptr);
#ifdef TRACK_PARTICLE_IDS
	  Ints &did = dst_id.ref(dptr);
#endif

          if (dnp < MAX_PARTICLES) {
            int dp = dnp++;

            // just have to copy p, hv, v
	    dpx[dp] = spx[p];
	    dpy[dp] = spy[p];
	    dpz[dp] = spz[p];
	    dhvx[dp] = shvx[p];
	    dhvy[dp] = shvy[p];
	    dhvz[dp] = shvz[p];
	    dvx[dp] = svx[p];
	    dvy[dp] = svy[p];
	    dvz[dp] = svz[p];
#ifdef TRACK_PARTICLE_IDS
	    did[dp] = sid[p];
#endif
          }
        }
      }

  log_app.info("Done with init_and_rebuild() for block %d", b.id);
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp finish_stamp("Init and Rebuild stop: ",false);
#endif
}

void rebuild_reduce(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, HighLevelRuntime *runtime)
{
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp total_stamp("Rebuild Reduce total: ",true);
  LegionRuntime::TimeStamp start_stamp("Rebuild Reduce start: ",false);
#endif
  assert(task->arglen == sizeof(int)*2);
  // int cb = ((const int *)task->args)[0];
  int bid = ((const int *)task->args)[1];
  // int eb = 1; // edge phase for this task is 1

  // Initialize all the cells and update all our cells
  PhysicalRegion block_region = regions[0];
  PhysicalRegion block_ptrs_region = regions[1];
  PhysicalRegion base_block = regions[2];
  PhysicalRegion edge_blocks[GHOST_CELLS];
  for (int i = 0; i < GHOST_CELLS; i++) {
    edge_blocks[i] = regions[i + 3];
  }

  RegionAccessor<SOA, multiptr_t> block_ptrs = soa<multiptr_t>(block_ptrs_region, FID_PTR);

  AccessorCube<SOA, int> cell_num_particles(regions, 2, 3, FID_CELL_NUM_PARTICLES);
  AccessorCube<SOA, Floats> cell_p_x(regions, 2, 3, FID_CELL_P_X);
  AccessorCube<SOA, Floats> cell_p_y(regions, 2, 3, FID_CELL_P_Y);
  AccessorCube<SOA, Floats> cell_p_z(regions, 2, 3, FID_CELL_P_Z);
  AccessorCube<SOA, Floats> cell_hv_x(regions, 2, 3, FID_CELL_HV_X);
  AccessorCube<SOA, Floats> cell_hv_y(regions, 2, 3, FID_CELL_HV_Y);
  AccessorCube<SOA, Floats> cell_hv_z(regions, 2, 3, FID_CELL_HV_Z);
  AccessorCube<SOA, Floats> cell_v_x(regions, 2, 3, FID_CELL_V_X);
  AccessorCube<SOA, Floats> cell_v_y(regions, 2, 3, FID_CELL_V_Y);
  AccessorCube<SOA, Floats> cell_v_z(regions, 2, 3, FID_CELL_V_Z);
#ifdef TRACK_PARTICLE_IDS
  AccessorCube<SOA, Ints> cell_id(regions, 2, 3, FID_CELL_ID);
#endif

  const Block &b = get_array_ref<SOA, Block>(block_region, FID_BLOCK, bid);

  log_app.info("In rebuild_reduce() for block %d", b.id);

  // for each edge cell, copy inward
  for (int cz = 0; cz <= (int)b.CELLS_Z+1; cz++)
    for (int cy = 0; cy <= (int)b.CELLS_Y+1; cy++)
      for (int cx = 0; cx <= (int)b.CELLS_X+1; cx++) {
	multiptr_t sptr = get_cell_ptr(b, cz, cy, cx, block_ptrs);
        int dir = GET_DIR(b, cz, cy, cx);
	assert((sptr.dir == BOUNDARY) || (sptr.dir == dir));
        if((sptr.dir == BOUNDARY) || (sptr.dir == CENTER)) continue;
        int dz = MOVE_CZ(b, cz, REVERSE(dir));
        int dy = MOVE_CY(b, cy, REVERSE(dir));
        int dx = MOVE_CX(b, cx, REVERSE(dir));

	multiptr_t dptr = get_cell_ptr(b, dz, dy, dx, block_ptrs);
	assert(dptr.dir == CENTER);

	int snp = cell_num_particles.ref(sptr);
	int& dnp = cell_num_particles.ref(dptr);

	int to_copy = (((snp + dnp) > MAX_PARTICLES) ? (MAX_PARTICLES - dnp) : snp);

	if(to_copy == 0) continue;

	// just have to copy p, hv, v
	memcpy(&cell_p_x.ref(dptr)[dnp], &cell_p_x.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_p_y.ref(dptr)[dnp], &cell_p_y.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_p_z.ref(dptr)[dnp], &cell_p_z.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_hv_x.ref(dptr)[dnp], &cell_hv_x.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_hv_y.ref(dptr)[dnp], &cell_hv_y.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_hv_z.ref(dptr)[dnp], &cell_hv_z.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_v_x.ref(dptr)[dnp], &cell_v_x.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_v_y.ref(dptr)[dnp], &cell_v_y.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_v_z.ref(dptr)[dnp], &cell_v_z.ref(sptr)[0], to_copy * sizeof(float));
      }

  // now turn around and have each edge grab a copy of the boundary real cell
  //  to share for the next step
  for (int cz = 0; cz <= (int)b.CELLS_Z+1; cz++)
    for (int cy = 0; cy <= (int)b.CELLS_Y+1; cy++)
      for (int cx = 0; cx <= (int)b.CELLS_X+1; cx++) {
	multiptr_t dptr = get_cell_ptr(b, cz, cy, cx, block_ptrs);
        int dir = GET_DIR(b, cz, cy, cx);
	assert((dptr.dir == BOUNDARY) || (dptr.dir == dir));
        if((dptr.dir == BOUNDARY) || (dptr.dir == CENTER)) continue;
        int dz = MOVE_CZ(b, cz, REVERSE(dir));
        int dy = MOVE_CY(b, cy, REVERSE(dir));
        int dx = MOVE_CX(b, cx, REVERSE(dir));

	multiptr_t sptr = get_cell_ptr(b, dz, dy, dx, block_ptrs);
	assert(sptr.dir == CENTER);

	int snp = cell_num_particles.ref(sptr);
	
	cell_num_particles.write(dptr, snp);

	int to_copy = snp;
	if(to_copy == 0) continue;

	memcpy(&cell_p_x.ref(dptr)[0], &cell_p_x.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_p_y.ref(dptr)[0], &cell_p_y.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_p_z.ref(dptr)[0], &cell_p_z.ref(sptr)[0], to_copy * sizeof(float));
	// neighbors don't actually need our HV
	//memcpy(&cell_hv_x.ref(dptr)[0], &cell_hv_x.ref(sptr)[0], to_copy * sizeof(float));
	//memcpy(&cell_hv_y.ref(dptr)[0], &cell_hv_y.ref(sptr)[0], to_copy * sizeof(float));
	//memcpy(&cell_hv_z.ref(dptr)[0], &cell_hv_z.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_v_x.ref(dptr)[0], &cell_v_x.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_v_y.ref(dptr)[0], &cell_v_y.ref(sptr)[0], to_copy * sizeof(float));
	memcpy(&cell_v_z.ref(dptr)[0], &cell_v_z.ref(sptr)[0], to_copy * sizeof(float));
#ifdef TRACK_PARTICLE_IDS
	memcpy(&cell_id.ref(dptr)[0], &cell_id.ref(sptr)[0], to_copy * sizeof(int));
#endif
      }

  log_app.info("Done with rebuild_reduce() for block %d", b.id);
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp stop_stamp("Rebuild Reduce stop ",false);
#endif
}

void clear_densities(const Task *task,
                       const std::vector<PhysicalRegion> &regions,
                       Context ctx, HighLevelRuntime *runtime)
{
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp total_stamp("Clear Densities total: ",true);
  LegionRuntime::TimeStamp start_stamp("Clear Densities start: ",false);
#endif
  assert(task->arglen == sizeof(int)*2);
  int bid = ((const int *)task->args)[1];

  // Initialize all the cells and update all our cells
  PhysicalRegion config_region = regions[0];
  PhysicalRegion block_region = regions[1];
  PhysicalRegion block_ptrs_region = regions[2];
  PhysicalRegion base_block = regions[3];

  RegionAccessor<SOA, multiptr_t> block_ptrs = soa<multiptr_t>(block_ptrs_region, FID_PTR);

  RegionAccessor<SOA, Floats> cell_density = soa<Floats>(base_block, FID_CELL_DENSITY);

  const Block &b = get_array_ref<SOA, Block>(block_region, FID_BLOCK, bid);

  log_app.info("In clear_densities() for block %d", b.id);

  // first, clear our density (and acceleration, while we're at it) values
  for (int cz = 1; cz < b.CELLS_Z + 1; cz++)
    for (int cy = 1; cy < b.CELLS_Y + 1; cy++)
      for (int cx = 1; cx < b.CELLS_X + 1; cx++) {
	multiptr_t sptr = get_cell_ptr(b, cz, cy, cx, block_ptrs);
	assert(sptr.dir == CENTER);

	Floats &density = cell_density.ref(sptr.ptr);
        for (unsigned p = 0; p < MAX_PARTICLES; p++) {
	  density[p] = 0;
        }
      }

  log_app.info("Done with clear_densities() for block %d", b.id);
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp stop_stamp("Clear Densities stop ",false);
#endif
}

void scatter_densities(const Task *task,
                       const std::vector<PhysicalRegion> &regions,
                       Context ctx, HighLevelRuntime *runtime)
{
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp total_stamp("Scatter Densities total: ",true);
  LegionRuntime::TimeStamp start_stamp("Scatter Densities start: ",false);
#endif
  assert(task->arglen == sizeof(int)*2);
  // int cb = ((const int *)task->args)[0];
  int bid = ((const int *)task->args)[1];
  // int eb = 0; // edge phase for this task is 0

  // Initialize all the cells and update all our cells
  PhysicalRegion config_region = regions[0];
  PhysicalRegion block_region = regions[1];
  PhysicalRegion block_ptrs_region = regions[2];
  PhysicalRegion base_block = regions[3];
  int ghost_base = 4;
  int mirror_base = ghost_base + GHOST_CELLS;

  RegionAccessor<SOA, multiptr_t> block_ptrs = soa<multiptr_t>(block_ptrs_region, FID_PTR);

  AccessorCube<SOA, int> cell_num_particles(regions, 3, ghost_base, FID_CELL_NUM_PARTICLES);
  AccessorCube<SOA, Floats> cell_p_x(regions, 3, ghost_base, FID_CELL_P_X);
  AccessorCube<SOA, Floats> cell_p_y(regions, 3, ghost_base, FID_CELL_P_Y);
  AccessorCube<SOA, Floats> cell_p_z(regions, 3, ghost_base, FID_CELL_P_Z);

  AccessorCube<SOA, Floats> cell_density(regions, 3, mirror_base, FID_CELL_DENSITY);
#ifdef TRACK_PARTICLE_IDS
  AccessorCube<SOA, Ints> cell_id(regions, 3, ghost_base, FID_CELL_ID);
#endif

  const Config &conf = get_singleton_ref<SOA, Config>(config_region, FID_CONFIG);
  float hSq = conf.hSq;
  float densityCoeff = conf.densityCoeff;

  const Block &b = get_array_ref<SOA, Block>(block_region, FID_BLOCK, bid);

  log_app.info("In scatter_densities() for block %d", b.id);

  // densities cleared in separate task

  // now for each cell, look at neighbors and calculate density contributions
  // two things to watch out for:
  //  * for pairs of real cells, we can do the calculation once instead of twice
  //  * edge cells on outer blocks may not contain valid data
  for (int cz = 1; cz <= b.CELLS_Z; cz++)
    for (int cy = 1; cy <= b.CELLS_Y; cy++)
      for (int cx = 1; cx <= b.CELLS_X; cx++) {
	multiptr_t cptr = get_cell_ptr(b, cz, cy, cx, block_ptrs);
	assert(cptr.dir == CENTER);

	int cnp = cell_num_particles.ref(cptr);
        assert(cnp <= MAX_PARTICLES);
	if(cnp == 0) continue;

	const Floats& cpx = cell_p_x.ref(cptr);
	const Floats& cpy = cell_p_y.ref(cptr);
	const Floats& cpz = cell_p_z.ref(cptr);
	Floats& cdensity = cell_density.ref(cptr);
#ifdef TRACK_PARTICLE_IDS
	const Ints& cid = cell_id.ref(cptr);
#endif

        for (int dz = cz - 1; dz <= cz + 1; dz++)
          for (int dy = cy - 1; dy <= cy + 1; dy++)
            for (int dx = cx - 1; dx <= cx + 1; dx++) {
	      multiptr_t dptr = get_cell_ptr(b, dz, dy, dx, block_ptrs);
	      if(dptr.dir == BOUNDARY) continue;

              // did we already get updated by this neighbor's bidirectional update?
	      // (can only happen if it's a real cell)
              if ((dptr.dir == CENTER) && 
                  (dz < cz || (dz == cz && (dy < cy || (dy == cy && dx < cx)))))
                continue;

	      int dnp = cell_num_particles.ref(dptr);
              assert(dnp <= MAX_PARTICLES);
	      if(dnp == 0) continue;

	      const Floats& dpx = cell_p_x.ref(dptr);
	      const Floats& dpy = cell_p_y.ref(dptr);
	      const Floats& dpz = cell_p_z.ref(dptr);
	      Floats& ddensity = cell_density.ref(dptr);
#ifdef TRACK_PARTICLE_IDS
	      const Ints& did = cell_id.ref(dptr);
#endif

              // do bidirectional update if other cell is a real cell and it is
              //  either below or to the right (but not up-right) of us
              bool update_other =
                true;//dz < (int)b.CELLS_Z+1 && dy < (int)b.CELLS_Y+1 && dx < (int)b.CELLS_X+1;

              // pairwise across particles - watch out for identical particle case!

              for (unsigned p = 0; p < cnp; p++)
                for (unsigned p2 = 0; p2 < dnp; p2++) {
                  if ((dx == cx) && (dy == cy) && (dz == cz) && (p <= p2)) continue;

		  float dx = cpx[p] - dpx[p2];
		  float dy = cpy[p] - dpy[p2];
		  float dz = cpz[p] - dpz[p2];
		  float distSq = (dx * dx) + (dy * dy) + (dz * dz);
#ifdef TRACK_PARTICLE_IDS
		  printf("DIST: %d->%d: (%f,%f,%f) = %f\n",
			 cid[p], did[p2], dx, dy, dz, distSq);
#endif
                  if (distSq >= hSq) continue;

                  float t = hSq - distSq;
                  float tc = t*t*t;

		  cdensity[p] += tc;
                  if (update_other)
                    ddensity[p2] += tc;
                }
            }

        // a little offset for every particle once we're done
        const float tc = hSq*hSq*hSq;
        for (unsigned p = 0; p < cnp; p++) {
          cdensity[p] += tc;
          cdensity[p] *= densityCoeff;
        }

      }

  // now turn around and have each edge grab a copy of the boundary real cell
  //  to share for the next step
  for (int cz = 0; cz <= (int)b.CELLS_Z+1; cz++)
    for (int cy = 0; cy <= (int)b.CELLS_Y+1; cy++)
      for (int cx = 0; cx <= (int)b.CELLS_X+1; cx++) {
	multiptr_t dptr = get_cell_ptr(b, cz, cy, cx, block_ptrs);
        int dir = GET_DIR(b, cz, cy, cx);
	assert((dptr.dir == BOUNDARY) || (dptr.dir == dir));
        if((dptr.dir == BOUNDARY) || (dptr.dir == CENTER)) continue;
        int dz = MOVE_CZ(b, cz, REVERSE(dir));
        int dy = MOVE_CY(b, cy, REVERSE(dir));
        int dx = MOVE_CX(b, cx, REVERSE(dir));

	multiptr_t sptr = get_cell_ptr(b, dz, dy, dx, block_ptrs);
	assert(sptr.dir == CENTER);

	int num_particles = cell_num_particles.ref(sptr);

	if(num_particles == 0) continue;

	memcpy(&cell_density.ref(dptr)[0], &cell_density.ref(sptr)[0], sizeof(float) * num_particles);
      }

  log_app.info("Done with scatter_densities() for block %d", b.id);
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp stop_stamp("Scatter Densities stop ",false);
#endif
}

void clear_forces(const Task *task,
                       const std::vector<PhysicalRegion> &regions,
                       Context ctx, HighLevelRuntime *runtime)
{
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp total_stamp("Clear Forces total: ",true);
  LegionRuntime::TimeStamp start_stamp("Clear Forces start: ",false);
#endif
  assert(task->arglen == sizeof(int)*2);
  int bid = ((const int *)task->args)[1];

  // Initialize all the cells and update all our cells
  PhysicalRegion config_region = regions[0];
  PhysicalRegion block_region = regions[1];
  PhysicalRegion block_ptrs_region = regions[2];
  PhysicalRegion base_block = regions[3];

  RegionAccessor<SOA, multiptr_t> block_ptrs = soa<multiptr_t>(block_ptrs_region, FID_PTR);

  RegionAccessor<SOA, Floats> cell_a_x = soa<Floats>(base_block, FID_CELL_A_X);
  RegionAccessor<SOA, Floats> cell_a_y = soa<Floats>(base_block, FID_CELL_A_Y);
  RegionAccessor<SOA, Floats> cell_a_z = soa<Floats>(base_block, FID_CELL_A_Z);

  const Block &b = get_array_ref<SOA, Block>(block_region, FID_BLOCK, bid);

  log_app.info("In clear_forces() for block %d", b.id);

  // first, clear our density (and acceleration, while we're at it) values
  for (int cz = 1; cz < b.CELLS_Z + 1; cz++)
    for (int cy = 1; cy < b.CELLS_Y + 1; cy++)
      for (int cx = 1; cx < b.CELLS_X + 1; cx++) {
	multiptr_t sptr = get_cell_ptr(b, cz, cy, cx, block_ptrs);
	assert(sptr.dir == CENTER);

	Floats &cax = cell_a_x.ref(sptr.ptr);
	Floats &cay = cell_a_y.ref(sptr.ptr);
	Floats &caz = cell_a_z.ref(sptr.ptr);

        for (unsigned p = 0; p < MAX_PARTICLES; p++) {
	  cax[p] = externalAcceleration.x;
	  cay[p] = externalAcceleration.y;
	  caz[p] = externalAcceleration.z;
        }
      }

  log_app.info("Done with clear_forces() for block %d", b.id);
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp stop_stamp("Clear Forces stop ",false);
#endif
}

void calc_accelerations(int cnp, int dnp,
#ifdef TRACK_PARTICLE_IDS
			const Ints& __restrict cid,
			const Ints& __restrict did,
#endif
			const Floats& __restrict cpx,
			const Floats& __restrict cpy,
			const Floats& __restrict cpz,
			const Floats& __restrict cvx,
			const Floats& __restrict cvy,
			const Floats& __restrict cvz,
			const Floats& __restrict cdensity,
			Floats& __restrict cax,
			Floats& __restrict cay,
			Floats& __restrict caz,
			const Floats& __restrict dpx,
			const Floats& __restrict dpy,
			const Floats& __restrict dpz,
			const Floats& __restrict dvx,
			const Floats& __restrict dvy,
			const Floats& __restrict dvz,
			const Floats& __restrict ddensity,
			Floats& __restrict dax,
			Floats& __restrict day,
			Floats& __restrict daz,
			float hSq, float h, float pressureCoeff, float doubleRestDensity,
			float viscosityCoeff, bool update_other)
{
  for (unsigned p = 0; p < cnp; p++) {
    for (unsigned p2 = 0; p2 < dnp; p2++) {
      float dx = cpx[p] - dpx[p2];
      float dy = cpy[p] - dpy[p2];
      float dz = cpz[p] - dpz[p2];
      float distSq = (dx * dx) + (dy * dy) + (dz * dz);
      if (distSq >= hSq) continue;

      float dist = (distSq > 1e-12f) ? sqrtf(distSq) : 1e-6f;
      float hmr = h - dist;

      // factor by which the distance vector is multiplied to get accel
      float oodensity = 1.0f / (cdensity[p] * ddensity[p2]);
      float accpf = (pressureCoeff
		     * (hmr * hmr / dist)
		     * (cdensity[p] + ddensity[p2] - doubleRestDensity) 
		     * oodensity);/// (cdensity[p] * ddensity[p2]));
      float accx = accpf * dx;
      float accy = accpf * dy;
      float accz = accpf * dz;

      // now a viscosity term
      float accvf = (viscosityCoeff * hmr * oodensity);/// (cdensity[p] * ddensity[p2]));
      accx += accvf * (dvx[p2] - cvx[p]);
      accy += accvf * (dvy[p2] - cvy[p]);
      accz += accvf * (dvz[p2] - cvz[p]);

#ifdef TRACK_PARTICLE_IDS
      printf("ACC: %d->%d = (%f,%f,%f)\n",
	     cid[p], did[p2], accx, accy, accz);
#endif

      cax[p] += accx;
      cay[p] += accy;
      caz[p] += accz;
      if (update_other) {
	dax[p2] -= accx;
	day[p2] -= accy;
	daz[p2] -= accz;
      }
    }
  }
}

void calc_accelerations_self(int cnp,
#ifdef TRACK_PARTICLE_IDS
			     const Ints& __restrict cid,
#endif
			     const Floats& __restrict cpx,
			     const Floats& __restrict cpy,
			     const Floats& __restrict cpz,
			     const Floats& __restrict cvx,
			     const Floats& __restrict cvy,
			     const Floats& __restrict cvz,
			     const Floats& __restrict cdensity,
			     Floats& __restrict cax,
			     Floats& __restrict cay,
			     Floats& __restrict caz,
			     float hSq, float h, float pressureCoeff, float doubleRestDensity,
			     float viscosityCoeff)
{
  for (unsigned p = 0; p < cnp; p++) {
    for (unsigned p2 = 0; p2 < p; p2++) {
      float dx = cpx[p] - cpx[p2];
      float dy = cpy[p] - cpy[p2];
      float dz = cpz[p] - cpz[p2];
      float distSq = (dx * dx) + (dy * dy) + (dz * dz);
      if (distSq >= hSq) continue;

      float dist = (distSq > 1e-12f) ? sqrtf(distSq) : 1e-6f;
      float hmr = h - dist;

      // factor by which the distance vector is multiplied to get accel
      float oodensity = 1.0f / (cdensity[p] * cdensity[p2]);
      float accpf = (pressureCoeff
		     * (hmr * hmr / dist)
		     * (cdensity[p] + cdensity[p2] - doubleRestDensity) 
		     * oodensity);
      float accx = accpf * dx;
      float accy = accpf * dy;
      float accz = accpf * dz;

      // now a viscosity term
      float accvf = (viscosityCoeff * hmr * oodensity);
      accx += accvf * (cvx[p2] - cvx[p]);
      accy += accvf * (cvy[p2] - cvy[p]);
      accz += accvf * (cvz[p2] - cvz[p]);

#ifdef TRACK_PARTICLE_IDS
      printf("ACC: %d->%d = (%f,%f,%f)\n",
	     cid[p], cid[p2], accx, accy, accz);
#endif

      cax[p] += accx;
      cay[p] += accy;
      caz[p] += accz;
      cax[p2] -= accx;
      cay[p2] -= accy;
      caz[p2] -= accz;
    }
  }
}

void gather_forces_and_advance(const Task *task,
                               const std::vector<PhysicalRegion> &regions,
                               Context ctx, HighLevelRuntime *runtime)
{
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp total_stamp("Gather Forces total: ",true);
  LegionRuntime::TimeStamp start_stamp("Gather Forces start: ",false);
#endif
  assert(task->arglen == sizeof(int)*2);
  // int cb = ((const int *)task->args)[0];
  int bid = ((const int *)task->args)[1];
  // int eb = 1; // edge phase for this task is 1

  // Initialize all the cells and update all our cells
  PhysicalRegion config_region = regions[0];
  PhysicalRegion block_region = regions[1];
  PhysicalRegion block_ptrs_region = regions[2];
  PhysicalRegion base_block = regions[3];
  PhysicalRegion edge_blocks[GHOST_CELLS];
  for (int i = 0; i < GHOST_CELLS; i++) {
    edge_blocks[i] = regions[i + 4];
  }

  RegionAccessor<SOA, multiptr_t> block_ptrs = soa<multiptr_t>(block_ptrs_region, FID_PTR);

  AccessorCube<SOA, int> cell_num_particles(regions, 3, 4, FID_CELL_NUM_PARTICLES);
  AccessorCube<SOA, Floats> cell_p_x(regions, 3, 4, FID_CELL_P_X);
  AccessorCube<SOA, Floats> cell_p_y(regions, 3, 4, FID_CELL_P_Y);
  AccessorCube<SOA, Floats> cell_p_z(regions, 3, 4, FID_CELL_P_Z);
  //AccessorCube<SOA, Floats> cell_hv_x(regions, 3, 4, FID_CELL_HV_X);
  //AccessorCube<SOA, Floats> cell_hv_y(regions, 3, 4, FID_CELL_HV_Y);
  //AccessorCube<SOA, Floats> cell_hv_z(regions, 3, 4, FID_CELL_HV_Z);
  AccessorCube<SOA, Floats> cell_v_x(regions, 3, 4, FID_CELL_V_X);
  AccessorCube<SOA, Floats> cell_v_y(regions, 3, 4, FID_CELL_V_Y);
  AccessorCube<SOA, Floats> cell_v_z(regions, 3, 4, FID_CELL_V_Z);
  //AccessorCube<SOA, Floats> cell_a_x(regions, 3, 4, FID_CELL_A_X);
  //AccessorCube<SOA, Floats> cell_a_y(regions, 3, 4, FID_CELL_A_Y);
  //AccessorCube<SOA, Floats> cell_a_z(regions, 3, 4, FID_CELL_A_Z);
  AccessorCube<SOA, Floats> cell_density(regions, 3, 4, FID_CELL_DENSITY);
#ifdef TRACK_PARTICLE_IDS
  AccessorCube<SOA, Ints> cell_id(regions, 3, 4, FID_CELL_ID);
#endif

  RegionAccessor<SOA, Floats> cell_hv_x = soa<Floats>(base_block, FID_CELL_HV_X);
  RegionAccessor<SOA, Floats> cell_hv_y = soa<Floats>(base_block, FID_CELL_HV_Y);
  RegionAccessor<SOA, Floats> cell_hv_z = soa<Floats>(base_block, FID_CELL_HV_Z);
  RegionAccessor<SOA, Floats> cell_a_x = soa<Floats>(base_block, FID_CELL_A_X);
  RegionAccessor<SOA, Floats> cell_a_y = soa<Floats>(base_block, FID_CELL_A_Y);
  RegionAccessor<SOA, Floats> cell_a_z = soa<Floats>(base_block, FID_CELL_A_Z);

  const Config &conf = get_singleton_ref<SOA, Config>(config_region, FID_CONFIG);
  float h = conf.h, hSq = conf.hSq;
  float pressureCoeff = conf.pressureCoeff;
  float viscosityCoeff = conf.viscosityCoeff;

  const Block &b = get_array_ref<SOA, Block>(block_region, FID_BLOCK, bid);

  log_app.info("In gather_forces_and_advance() for block %d", b.id);

  // clear of acceleration values done in another task

  // now for each cell, look at neighbors and calculate acceleration
  // one thing to watch out for:
  //  * for pairs of real cells, we can do the calculation once instead of twice
  for (int cz = 1; cz <= b.CELLS_Z; cz++)
    for (int cy = 1; cy <= b.CELLS_Y; cy++)
      for (int cx = 1; cx <= b.CELLS_X; cx++) {
	multiptr_t cptr = get_cell_ptr(b, cz, cy, cx, block_ptrs);
	assert(cptr.dir == CENTER);

	int cnp = cell_num_particles.ref(cptr);
        assert(cnp <= MAX_PARTICLES);

#ifdef TRACK_PARTICLE_IDS
	printf("GF: (%d,%d,%d) %d\n", conf.splitx[b.x] + cx - 1, conf.splity[b.y] + cy - 1, conf.splitz[b.z] + cz - 1, cnp);
	printf("GFPTR: %d -> %p\n", cptr.ptr.value, &cell_num_particles.ref(cptr));
#endif
	if(cnp == 0) continue;

	Floats& cpx = cell_p_x.ref(cptr);
	Floats& cpy = cell_p_y.ref(cptr);
	Floats& cpz = cell_p_z.ref(cptr);
	Floats& chvx = cell_hv_x.ref(cptr.ptr);
	Floats& chvy = cell_hv_y.ref(cptr.ptr);
	Floats& chvz = cell_hv_z.ref(cptr.ptr);
	Floats& cvx = cell_v_x.ref(cptr);
	Floats& cvy = cell_v_y.ref(cptr);
	Floats& cvz = cell_v_z.ref(cptr);
	Floats& cax = cell_a_x.ref(cptr.ptr);
	Floats& cay = cell_a_y.ref(cptr.ptr);
	Floats& caz = cell_a_z.ref(cptr.ptr);
	const Floats& cdensity = cell_density.ref(cptr);
#ifdef TRACK_PARTICLE_IDS
	const Ints& cid = cell_id.ref(cptr);
#endif

#ifdef TRACK_PARTICLE_IDS
	for(int p = 0; p < cnp; p++)
	  printf("DENS: %d = %f\n", cid[p], cdensity[p]);
#endif

        for (int dz = cz - 1; dz <= cz + 1; dz++)
          for (int dy = cy - 1; dy <= cy + 1; dy++)
            for (int dx = cx - 1; dx <= cx + 1; dx++) {
	      // skip self case in this loop
	      if((dx == cx) && (dy == cy) && (dz == cz)) continue;

	      multiptr_t dptr = get_cell_ptr(b, dz, dy, dx, block_ptrs);
	      if(dptr.dir == BOUNDARY) continue;

              // did we already get updated by this neighbor's bidirectional update?
	      // (can only happen if it's a real cell)
              if ((dptr.dir == CENTER) && 
                  (dz < cz || (dz == cz && (dy < cy || (dy == cy && dx < cx)))))
                continue;

	      int dnp = cell_num_particles.ref(dptr);
              assert(dnp <= MAX_PARTICLES);
	      if(dnp == 0) continue;

	      const Floats& dpx = cell_p_x.ref(dptr);
	      const Floats& dpy = cell_p_y.ref(dptr);
	      const Floats& dpz = cell_p_z.ref(dptr);
	      const Floats& dvx = cell_v_x.ref(dptr);
	      const Floats& dvy = cell_v_y.ref(dptr);
	      const Floats& dvz = cell_v_z.ref(dptr);
	      const Floats& ddensity = cell_density.ref(dptr);
#ifdef TRACK_PARTICLE_IDS
	      const Ints& did = cell_id.ref(dptr);
#endif

	      bool update_other = (dptr.dir == CENTER);

	      Floats *dax = (update_other ? &cell_a_x.ref(dptr.ptr) : 0);
	      Floats *day = (update_other ? &cell_a_y.ref(dptr.ptr) : 0);
	      Floats *daz = (update_other ? &cell_a_z.ref(dptr.ptr) : 0);

	      calc_accelerations(cnp, dnp,
#ifdef TRACK_PARTICLE_IDS
				 cid, did,
#endif
				 cpx, cpy, cpz, cvx, cvy, cvz, cdensity, cax, cay, caz,
				 dpx, dpy, dpz, dvx, dvy, dvz, ddensity, *dax, *day, *daz,
				 hSq, h, pressureCoeff, doubleRestDensity, viscosityCoeff,
				 update_other);
            }

	// finish up by calculating acceleration contributions from within our own cell
	calc_accelerations_self(cnp,
#ifdef TRACK_PARTICLE_IDS
				cid,
#endif
				cpx, cpy, cpz, cvx, cvy, cvz, cdensity, cax, cay, caz,
				hSq, h, pressureCoeff, doubleRestDensity, viscosityCoeff);

        // compute collisions for particles near edge of box
        const float parSize = 0.0002f;
        const float epsilon = 1e-10f;
        const float stiffness = 30000.f;
        const float damping = 128.f;
        for (unsigned p = 0; p < cnp; p++) {
	  float px = cpx[p] + chvx[p] * timeStep;
	  float py = cpy[p] + chvy[p] * timeStep;
	  float pz = cpz[p] + chvz[p] * timeStep;

	  {
	    float lo_diff = parSize - (px - domainMin.x);
	    if (lo_diff > epsilon)
	      cax[p] += (stiffness * lo_diff) - (damping * cvx[p]);

	    float hi_diff = parSize - (domainMax.x - px);
	    if (hi_diff > epsilon)
	      cax[p] -= (stiffness * hi_diff) + (damping * cvx[p]);
	  }

	  {
	    float lo_diff = parSize - (py - domainMin.y);
	    if (lo_diff > epsilon)
	      cay[p] += (stiffness * lo_diff) - (damping * cvy[p]);

	    float hi_diff = parSize - (domainMax.y - py);
	    if (hi_diff > epsilon)
	      cay[p] -= (stiffness * hi_diff) + (damping * cvy[p]);
	  }

	  {
	    float lo_diff = parSize - (pz - domainMin.z);
	    if (lo_diff > epsilon)
	      caz[p] += (stiffness * lo_diff) - (damping * cvz[p]);

	    float hi_diff = parSize - (domainMax.z - pz);
	    if (hi_diff > epsilon)
	      caz[p] -= (stiffness * hi_diff) + (damping * cvz[p]);
	  }
        }

        // we have everything we need to go ahead and update positions, so
        //  do that here instead of in a different task
        for (unsigned p = 0; p < cnp; p++) {
	  float hvx = chvx[p] + cax[p] * timeStep;
	  float hvy = chvy[p] + cay[p] * timeStep;
	  float hvz = chvz[p] + caz[p] * timeStep;

	  cpx[p] += hvx * timeStep;
	  cpy[p] += hvy * timeStep;
	  cpz[p] += hvz * timeStep;

	  cvx[p] = 0.5 * (chvx[p] + hvx);
	  cvy[p] = 0.5 * (chvy[p] + hvy);
	  cvz[p] = 0.5 * (chvz[p] + hvz);

	  chvx[p] = hvx;
	  chvy[p] = hvy;
	  chvz[p] = hvz;

#ifdef TRACK_PARTICLE_IDS
	  printf("ADV: %d: a=(%f,%f,%f) -> p=(%f,%f,%f) hv=(%f,%f,%f) v=(%f,%f,%f)\n",
		 cid[p],
		 cax[p], cay[p], caz[p],
		 cpx[p], cpy[p], cpz[p],
		 chvx[p], chvy[p], chvz[p],
		 cvx[p], cvy[p], cvz[p]);
#endif
        }

      }

  log_app.info("Done with gather_forces_and_advance() for block %d", b.id);
#ifdef TIME_STAMPS
  LegionRuntime::TimeStamp stop_stamp("Gather Forces stop ",false);
#endif
}

static inline int isLittleEndian() {
  union {
    uint16_t word;
    uint8_t byte;
  } endian_test;

  endian_test.word = 0x00FF;
  return (endian_test.byte == 0xFF);
}

union __float_and_int {
  uint32_t i;
  float    f;
};

static inline float bswap_float(float x) {
  union __float_and_int __x;

   __x.f = x;
   __x.i = ((__x.i & 0xff000000) >> 24) | ((__x.i & 0x00ff0000) >>  8) |
           ((__x.i & 0x0000ff00) <<  8) | ((__x.i & 0x000000ff) << 24);

  return __x.f;
}

static inline int bswap_int32(int x) {
  return ( (((x) & 0xff000000) >> 24) | (((x) & 0x00ff0000) >>  8) |
           (((x) & 0x0000ff00) <<  8) | (((x) & 0x000000ff) << 24) );
}

static void load_file_header(const char *fileName, Config &conf)
{
  std::ifstream file(fileName, std::ios::binary);
  assert(file);

  float &restParticlesPerMeter = conf.restParticlesPerMeter;
  int &origNumParticles = conf.origNumParticles;
  file.read((char *)&restParticlesPerMeter, 4);
  file.read((char *)&origNumParticles, 4);
  if (!isLittleEndian()) {
    restParticlesPerMeter = bswap_float(restParticlesPerMeter);
    origNumParticles      = bswap_int32(origNumParticles);
  }
}

static int read_int(std::ifstream& i)
{
  int val;
  i.read((char *)&val, 4);
  if (!isLittleEndian())
    val = bswap_int32(val);
  return val;
}

static float read_float(std::ifstream& i)
{
  float val;
  i.read((char *)&val, 4);
  if (!isLittleEndian())
    val = bswap_float(val);
  return val;
}

static void split_evenly(int pieces, int total,
                         const std::vector<int>& buckets,
                         int *splits)
{
  splits[0] = 0;
  splits[pieces] = buckets.size();

  int cum = 0;
  int pos = 1;
  for (size_t i = 0; (i < buckets.size()) && (pos < pieces); i++) {
    cum += buckets[i];
    if ((cum * pieces) >= (pos * total))
      splits[pos++] = i + 1;
  }
}

void analyze_particle_distribution(Config& conf)
{
  std::ifstream file(conf.inFilename, std::ios::binary);
  assert(file);

  /*float restParticlesPerMeter =*/ read_float(file);
  int origNumParticles = read_int(file);
  
  std::vector<int> hist_x(conf.nx, 0);
  std::vector<int> hist_y(conf.ny, 0);
  std::vector<int> hist_z(conf.nz, 0);

  for (int i = 0; i < origNumParticles; ++i) {
    float px = read_float(file);
    float py = read_float(file);
    float pz = read_float(file);
    char junk[24];
    file.read(junk, 24);

    // Global cell coordinates
    int ci = (int)((px - domainMin.x) / conf.delta.x);
    int cj = (int)((py - domainMin.y) / conf.delta.y);
    int ck = (int)((pz - domainMin.z) / conf.delta.z);

    if (ci < 0) ci = 0; else if (ci > (int)(conf.nx-1)) ci = conf.nx-1;
    if (cj < 0) cj = 0; else if (cj > (int)(conf.ny-1)) cj = conf.ny-1;
    if (ck < 0) ck = 0; else if (ck > (int)(conf.nz-1)) ck = conf.nz-1;

    hist_x[ci]++;
    hist_y[cj]++;
    hist_z[ck]++;
  }

  split_evenly(conf.nbx, origNumParticles, hist_x, conf.splitx);
  split_evenly(conf.nby, origNumParticles, hist_y, conf.splity);
  split_evenly(conf.nbz, origNumParticles, hist_z, conf.splitz);

  printf("X splits:");
  for (int i = 0; i <= conf.nbx; i++) printf(" %d", conf.splitx[i]);
  printf("\n");

  printf("Y splits:");
  for (int i = 0; i <= conf.nby; i++) printf(" %d", conf.splity[i]);
  printf("\n");

  printf("Z splits:");
  for (int i = 0; i <= conf.nbz; i++) printf(" %d", conf.splitz[i]);
  printf("\n");

#if 0
  printf("X histogram\n");
  for (int i = 0; i < conf.nx; i++)
    printf("%3d: %6d  %8.4f\n", i, hist_x[i], 100.0 * hist_x[i] * conf.nx / numParticles);
  printf("\n");

  printf("Y histogram\n");
  for (int i = 0; i < conf.ny; i++)
    printf("%3d: %6d  %8.4f\n", i, hist_y[i], 100.0 * hist_y[i] * conf.ny / numParticles);
  printf("\n");

  printf("Z histogram\n");
  for (int i = 0; i < conf.nz; i++)
    printf("%3d: %6d  %8.4f\n", i, hist_z[i], 100.0 * hist_z[i] * conf.nz / numParticles);
  printf("\n");
#endif
}

/*
 * Expects load_file_header and init_config to have been called on the
 * incoming Config already.
 */
void load_file(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(int));
  int id = ((const int *)task->args)[0];

  PhysicalRegion config_region = regions[0];
  PhysicalRegion block_region = regions[1];
  PhysicalRegion block_ptrs_region = regions[2];
  PhysicalRegion real_cells_region = regions[3];
  RegionAccessor<SOA, multiptr_t> block_ptrs = soa<multiptr_t>(block_ptrs_region, FID_PTR);

  RegionAccessor<SOA, int> cell_num_particles = soa<int>(real_cells_region, FID_CELL_NUM_PARTICLES);
  RegionAccessor<SOA, Floats> cell_p_x = soa<Floats>(real_cells_region, FID_CELL_P_X);
  RegionAccessor<SOA, Floats> cell_p_y = soa<Floats>(real_cells_region, FID_CELL_P_Y);
  RegionAccessor<SOA, Floats> cell_p_z = soa<Floats>(real_cells_region, FID_CELL_P_Z);
  RegionAccessor<SOA, Floats> cell_hv_x = soa<Floats>(real_cells_region, FID_CELL_HV_X);
  RegionAccessor<SOA, Floats> cell_hv_y = soa<Floats>(real_cells_region, FID_CELL_HV_Y);
  RegionAccessor<SOA, Floats> cell_hv_z = soa<Floats>(real_cells_region, FID_CELL_HV_Z);
  RegionAccessor<SOA, Floats> cell_v_x = soa<Floats>(real_cells_region, FID_CELL_V_X);
  RegionAccessor<SOA, Floats> cell_v_y = soa<Floats>(real_cells_region, FID_CELL_V_Y);
  RegionAccessor<SOA, Floats> cell_v_z = soa<Floats>(real_cells_region, FID_CELL_V_Z);
#ifdef TRACK_PARTICLE_IDS
  RegionAccessor<SOA, Ints> cell_id = soa<Ints>(real_cells_region, FID_CELL_ID);
#endif

  const Config &conf = get_singleton_ref<SOA, Config>(config_region, FID_CONFIG);
  float restParticlesPerMeter = conf.restParticlesPerMeter;
  int origNumParticles = conf.origNumParticles, numParticles;
  int nx = conf.nx, ny = conf.ny, nz = conf.nz;
  int nbx = conf.nbx, nby = conf.nby;//, &nbz = conf.nbz;
  Vec3 delta = conf.delta;
  const char *inFilename = conf.inFilename;

  const Block &b = get_array_ref<SOA, Block>(block_region, FID_BLOCK, id);

  log_app.info("Loading file \"%s\"...", inFilename);

  // const int cb = 1;
  // const int eb = 0;

  // Clear all cells
  for (int cz = 0; cz < b.CELLS_Z; cz++)
    for (int cy = 0; cy < b.CELLS_Y; cy++)
      for (int cx = 0; cx < b.CELLS_X; cx++) {
	cell_num_particles.write(get_cell_ptr(b, cz+1, cy+1, cx+1, block_ptrs).ptr, 0);
      }

  std::ifstream file(inFilename, std::ios::binary);
  assert(file);

  file.read((char *)&restParticlesPerMeter, 4);
  file.read((char *)&origNumParticles, 4);
  if (!isLittleEndian()) {
    restParticlesPerMeter = bswap_float(restParticlesPerMeter);
    origNumParticles      = bswap_int32(origNumParticles);
  }
  numParticles = origNumParticles;

  float px, py, pz, hvx, hvy, hvz, vx, vy, vz;
  for (int i = 0; i < origNumParticles; ++i) {
    file.read((char *)&px, 4);
    file.read((char *)&py, 4);
    file.read((char *)&pz, 4);
    file.read((char *)&hvx, 4);
    file.read((char *)&hvy, 4);
    file.read((char *)&hvz, 4);
    file.read((char *)&vx, 4);
    file.read((char *)&vy, 4);
    file.read((char *)&vz, 4);
    if (!isLittleEndian()) {
      px  = bswap_float(px);
      py  = bswap_float(py);
      pz  = bswap_float(pz);
      hvx = bswap_float(hvx);
      hvy = bswap_float(hvy);
      hvz = bswap_float(hvz);
      vx  = bswap_float(vx);
      vy  = bswap_float(vy);
      vz  = bswap_float(vz);
    }

    // Global cell coordinates
    int ci = (int)((px - domainMin.x) / delta.x);
    int cj = (int)((py - domainMin.y) / delta.y);
    int ck = (int)((pz - domainMin.z) / delta.z);

    if (ci < 0) ci = 0; else if (ci > (int)(nx-1)) ci = nx-1;
    if (cj < 0) cj = 0; else if (cj > (int)(ny-1)) cj = ny-1;
    if (ck < 0) ck = 0; else if (ck > (int)(nz-1)) ck = nz-1;

    // Block coordinates and id
    int idx = 0; while (ci >= conf.splitx[idx+1]) idx++;
    int idy = 0; while (cj >= conf.splity[idy+1]) idy++;
    int idz = 0; while (ck >= conf.splitz[idz+1]) idz++;

    int target_id = (idz*nby+idy)*nbx+idx;

    if (target_id != id) continue;

    // Local cell coordinates
    int cx = ci - conf.splitx[idx];
    int cy = cj - conf.splity[idy];
    int cz = ck - conf.splitz[idz];

    multiptr_t cptr = get_cell_ptr(b, cz+1, cy+1, cx+1, block_ptrs);
    assert(cptr.dir == CENTER);
    int &np = cell_num_particles.ref(cptr.ptr);

    if (np < MAX_PARTICLES) {
      cell_p_x.ref(cptr.ptr)[np] = px;
      cell_p_y.ref(cptr.ptr)[np] = py;
      cell_p_z.ref(cptr.ptr)[np] = pz;
      cell_hv_x.ref(cptr.ptr)[np] = hvx;
      cell_hv_y.ref(cptr.ptr)[np] = hvy;
      cell_hv_z.ref(cptr.ptr)[np] = hvz;
      cell_v_x.ref(cptr.ptr)[np] = vx;
      cell_v_y.ref(cptr.ptr)[np] = vy;
      cell_v_z.ref(cptr.ptr)[np] = vz;
#ifdef TRACK_PARTICLE_IDS
      cell_id.ref(cptr.ptr)[np] = i;
#endif

      ++np;
    } else {
      --numParticles;
    }

  }

  log_app.info("Number of particles: %d (%d skipped)",
               numParticles, origNumParticles - numParticles);

  log_app.info("Done loading file.");
}

void save_file(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(int));
  // int cb = ((const int *)task->args)[0];

  PhysicalRegion config_region = regions[0];
  const Config &conf = get_singleton_ref<SOA, Config>(config_region, FID_CONFIG);
  int numBlocks = conf.numBlocks;

  PhysicalRegion blocks_region = regions[1];
  PhysicalRegion real_cells_region = regions[2];
  std::vector<PhysicalRegion> block_ptrs_region;
  for (int id = 0; id < numBlocks; id++) {
    block_ptrs_region.push_back(regions[3 + id]);
  }

  RegionAccessor<SOA, Block> blocks = soa<Block>(blocks_region, FID_BLOCK);

  RegionAccessor<SOA, int> cell_num_particles = soa<int>(real_cells_region, FID_CELL_NUM_PARTICLES);
  RegionAccessor<SOA, Floats> cell_p_x = soa<Floats>(real_cells_region, FID_CELL_P_X);
  RegionAccessor<SOA, Floats> cell_p_y = soa<Floats>(real_cells_region, FID_CELL_P_Y);
  RegionAccessor<SOA, Floats> cell_p_z = soa<Floats>(real_cells_region, FID_CELL_P_Z);
  RegionAccessor<SOA, Floats> cell_hv_x = soa<Floats>(real_cells_region, FID_CELL_HV_X);
  RegionAccessor<SOA, Floats> cell_hv_y = soa<Floats>(real_cells_region, FID_CELL_HV_Y);
  RegionAccessor<SOA, Floats> cell_hv_z = soa<Floats>(real_cells_region, FID_CELL_HV_Z);
  RegionAccessor<SOA, Floats> cell_v_x = soa<Floats>(real_cells_region, FID_CELL_V_X);
  RegionAccessor<SOA, Floats> cell_v_y = soa<Floats>(real_cells_region, FID_CELL_V_Y);
  RegionAccessor<SOA, Floats> cell_v_z = soa<Floats>(real_cells_region, FID_CELL_V_Z);
#ifdef TRACK_PARTICLE_IDS
  RegionAccessor<SOA, Ints> cell_id = soa<Ints>(real_cells_region, FID_CELL_ID);
#endif

  float restParticlesPerMeter = conf.restParticlesPerMeter;
  int origNumParticles = conf.origNumParticles;
  int nx = conf.nx, ny = conf.ny, nz = conf.nz;
  int nbx = conf.nbx, nby = conf.nby;
  const char *outFilename = conf.outFilename;

  log_app.info("Saving file \"%s\"...", outFilename);

  std::ofstream file(outFilename, std::ios::binary);
  assert(file);

  if (!isLittleEndian()) {
    float restParticlesPerMeter_le;
    int   origNumParticles_le;

    restParticlesPerMeter_le = bswap_float(restParticlesPerMeter);
    origNumParticles_le      = bswap_int32(origNumParticles);
    file.write((char *)&restParticlesPerMeter_le, 4);
    file.write((char *)&origNumParticles_le,      4);
  } else {
    file.write((char *)&restParticlesPerMeter, 4);
    file.write((char *)&origNumParticles,      4);
  }

  int numParticles = 0;

  // have to iterate by cell (not blocks) to keep comparison scripts happy
  for (int ck = 0; ck < (int)nz; ck++)
    for (int cj = 0; cj < (int)ny; cj++)
      for (int ci = 0; ci < (int)nx; ci++) {
        // Block coordinates and id
        int idx = 0; while (ci >= conf.splitx[idx+1]) idx++;
        int idy = 0; while (cj >= conf.splity[idy+1]) idy++;
        int idz = 0; while (ck >= conf.splitz[idz+1]) idz++;

        int id = (idz*nby+idy)*nbx+idx;

        int cx = ci - conf.splitx[idx];
        int cy = cj - conf.splity[idy];
        int cz = ck - conf.splitz[idz];

	const Block& b = blocks.ref(id);

	RegionAccessor<SOA, multiptr_t> block_ptrs = soa<multiptr_t>(regions[3 + id], FID_PTR);

	multiptr_t cptr = get_cell_ptr(b, cz+1, cy+1, cx+1, block_ptrs);
	assert(cptr.dir == CENTER);

	int cnp = cell_num_particles.ref(cptr.ptr);
	assert(cnp <= MAX_PARTICLES);

#ifdef TRACK_PARTICLE_IDS
	printf("SF: (%d,%d,%d) %d\n", ci, cj, ck, cnp);
	printf("SFPTR: %d -> %p\n", cptr.ptr.value, &cell_num_particles.ref(cptr.ptr));
#endif

	if(cnp == 0) continue;

	const Floats& cpx = cell_p_x.ref(cptr.ptr);
	const Floats& cpy = cell_p_y.ref(cptr.ptr);
	const Floats& cpz = cell_p_z.ref(cptr.ptr);
	const Floats& chvx = cell_hv_x.ref(cptr.ptr);
	const Floats& chvy = cell_hv_y.ref(cptr.ptr);
	const Floats& chvz = cell_hv_z.ref(cptr.ptr);
	const Floats& cvx = cell_v_x.ref(cptr.ptr);
	const Floats& cvy = cell_v_y.ref(cptr.ptr);
	const Floats& cvz = cell_v_z.ref(cptr.ptr);
#ifdef TRACK_PARTICLE_IDS
	const Ints& cid = cell_id.ref(cptr.ptr);
	for(int p = 0; p < cnp; p++) {
	  printf("SAVE: %d\n", cid[p]);
	  printf("DATA: %d (%.3f,%.3f,%.3f) (%.3f,%.3f,%.3f) (%.3f,%.3f,%.3f)\n",
		 cid[p],
		 cpx[p], cpy[p], cpz[p], chvx[p], chvy[p], chvz[p], cvx[p], cvy[p], cvz[p]);
	}
#endif

	for (unsigned p = 0; p < cnp; ++p) {
	  if (!isLittleEndian()) {
	    float v[9];
	    
	    v[0] = bswap_float(cpx[p]);
	    v[1] = bswap_float(cpy[p]);
	    v[2] = bswap_float(cpz[p]);
	    v[3] = bswap_float(chvx[p]);
	    v[4] = bswap_float(chvy[p]);
	    v[5] = bswap_float(chvz[p]);
	    v[6] = bswap_float(cvx[p]);
	    v[7] = bswap_float(cvy[p]);
	    v[8] = bswap_float(cvz[p]);

	    file.write((char *)v, 9 * sizeof(float));
	  } else {
	    file.write((char *)&cpx[p],  4);
	    file.write((char *)&cpy[p],  4);
	    file.write((char *)&cpz[p],  4);
	    file.write((char *)&chvx[p], 4);
	    file.write((char *)&chvy[p], 4);
	    file.write((char *)&chvz[p], 4);
	    file.write((char *)&cvx[p],  4);
	    file.write((char *)&cvy[p],  4);
	    file.write((char *)&cvz[p],  4);
	  }
	  
	  ++numParticles;
	}
      }

  int numSkipped = origNumParticles - numParticles;
  float zero = 0.f;
  if (!isLittleEndian()) {
    zero = bswap_float(zero);
  }

  for (int i = 0; i < numSkipped; ++i) {
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
    file.write((char *)&zero, 4);
  }

  log_app.info("Done saving file.");
}

/*
 * Elliott: A hack to work around deadlocks in the GASNET runtime
 * caused by copying directly between local memories.
 */
void dummy_task(const Task *task,
                const std::vector<PhysicalRegion> &regions,
                Context ctx, HighLevelRuntime *runtime)
{
  //printf("dummy task run\n");
}

#if 1
#if 0
static bool sort_by_proc_id(const std::pair<Processor,Memory> &a,
                            const std::pair<Processor,Memory> &b)
{
  return (a.first.id < b.first.id);
}
#endif

// Run main in a separate thread? This will cannibalize 1 thread from
// the rest of the computation.
#ifndef MAP_MAIN_SEPARATELY
#define MAP_MAIN_SEPARATELY 0
#endif
static inline int get_proc_id_for_task(int task_id, int tag, int num_procs)
{
  switch (task_id) {
  case TOP_LEVEL_TASK_ID:
  case TASKID_MAIN_TASK:
  case TASKID_SAVE_FILE:
  case TASKID_DUMMY_TASK:
    return 0;
  case TASKID_LOAD_FILE:
  case TASKID_INIT_CELLS:
  case TASKID_INIT_CELLS_NOP:
  case TASKID_REBUILD_REDUCE:
  case TASKID_REBUILD_REDUCE_NOP:
  case TASKID_CLEAR_DENSITIES:
  case TASKID_SCATTER_DENSITIES:
  case TASKID_SCATTER_DENSITIES_NOP:
  //case TASKID_GATHER_DENSITIES:
  //case TASKID_SCATTER_FORCES:
  case TASKID_CLEAR_FORCES:
  case TASKID_GATHER_FORCES:
  case TASKID_GATHER_FORCES_NOP:
#if MAP_MAIN_SEPARATELY
    if (num_procs == 1)
      return 0;
    return (tag % (num_procs - 1)) + 1;
#else
    return tag % num_procs;
#endif
  default:
    assert(false);
  }
}

static inline int get_proc_id_for_neighbor(int task_id, int tag, int num_procs, int dir, Config &c)
{
  // for (unsigned idz = 0; idz < nbz; idz++)
  //   for (unsigned idy = 0; idy < nby; idy++)
  //     for (unsigned idx = 0; idx < nbx; idx++) {
  //       unsigned id = (idz*nby+idy)*nbx+idx;

  int id = tag;
  int idx = id % c.nbx;
  int idy = id/c.nbx % c.nby;
  int idz = id/c.nbx/c.nby;
  int idx2 = MOVE_BX(idx, dir, c);
  int idy2 = MOVE_BY(idy, dir, c);
  int idz2 = MOVE_BZ(idz, dir, c);
  int id2 = (idz2*c.nby + idy2)*c.nbx + idx2;

  int p = get_proc_id_for_task(task_id, id2, num_procs);

  log_mapper.info("edge cell for %d (%d %d %d) in direction %d is piece %d (%d %d %d) which lives on proc %d",
         id, idx, idy, idz, dir, id2, idx2, idy2, idz2, p);

  return p;
}

// No need to put all ghost cells in regmem when the edges shared
// within a node aren't going to be send across the network anyway.
#ifndef AVOID_OVERUSING_REGMEM
#define AVOID_OVERUSING_REGMEM 1
#endif

class FluidMapper : public ShimMapper {
public:
  struct CpuMemPair {
    Processor cpu;
    Memory sysmem;
    Memory regmem;
  };
  std::vector<CpuMemPair> cpu_mem_pairs;
  Config conf;

  FluidMapper(Machine *m, HighLevelRuntime *r, Processor p)
    : ShimMapper(m, r, p)
  {
    // for each CPU processor, we want to know:
    //  1) the system memory it's closest to, and
    //  2) the registered DMA memory it uses (if any)
    const std::set<Processor> &all_procs = m->get_all_processors();
    for(std::set<Processor>::const_iterator it = all_procs.begin(); it != all_procs.end(); it++) {
      // we only want CPUs
      if(m->get_processor_kind(*it) != Processor::LOC_PROC) continue;

      CpuMemPair cmp;
      cmp.cpu = *it;
      cmp.sysmem = machine_interface.find_memory_kind(cmp.cpu, Memory::SYSTEM_MEM);
      cmp.regmem = machine_interface.find_memory_kind(cmp.cpu, Memory::REGDMA_MEM);
      if(!cmp.regmem.exists())
	cmp.regmem = cmp.sysmem;

      cpu_mem_pairs.push_back(cmp);
    }

    // Parse command line arguments to determine problem size.
    InputArgs inputs = HighLevelRuntime::get_input_args();
    char **argv = inputs.argv;
    int argc = inputs.argc;
    parse_args(argc, argv, conf);
  }

  virtual bool spawn_child_task(const Task *task)
  {
    return true;
  }

  virtual Processor select_target_processor(const Task *task)
  {
    int proc_index = get_proc_id_for_task(task->task_id, task->tag, cpu_mem_pairs.size());
    const CpuMemPair &cmp = cpu_mem_pairs[proc_index];

    log_mapper.info("selected proc="IDFMT" (mem="IDFMT","IDFMT") for task %p",
	       cmp.cpu.id, cmp.sysmem.id, cmp.regmem.id, task);
    return cmp.cpu;
  }

  virtual size_t select_region_layout(const Task *task, Processor target,
				      const RegionRequirement &req, 
				      unsigned index, 
				      const Memory &chosen_mem, 
				      size_t max_blocking_factor)
  {
    switch(layout_policy) {
    case LAYOUT_ALL_AOS:
      return 1;

    case LAYOUT_ALL_SOA:
      return max_blocking_factor;

    case LAYOUT_MIXED:
      // all instances are AOS except those tagged as edge cells
      if(req.tag == TAG_EDGE_CELLS)
	return max_blocking_factor;
      else
	return 1;

    default:
      assert(0);
      return 0;
    }
  }

  virtual Processor target_task_steal(const std::set<Processor> &blacklisted)
  {
    return Processor::NO_PROC;
  }

  virtual void permit_task_steal(Processor thief,
                                 const std::vector<const Task*> &tasks,
                                 std::set<const Task*> &to_steal)
  {
    return;
  }

  // virtual void split_index_space(const Task *task, const std::vector<Constraint> &index_space,
  //                                std::vector<ConstraintSplit> &chunks)
  // {
  //   return;
  // }

  virtual bool map_region_virtually(const Task *task, Processor target,
				    const RegionRequirement &req, 
				    unsigned index)
  {
    switch (task->task_id) {
    case TASKID_MAIN_TASK:
      {
        switch (index) {
        case 2: // real cell [0]
        case 3: // real cells [1]
#ifdef PARTITIONED_EDGE_CELLS
        case 4: // edge cells [0]
        case 5: // edge cells [1]
#endif
          {
	    return true;
          }
          break;
	}
      }
    }

    return false; // map_task_region will say where we want it to be
  }

  // Elliott: try mapping everything locally?
  virtual bool map_task_locally(const Task *task)
  {
    return false;
  }

  virtual bool map_task_region(const Task *task, Processor target, MappingTagID tag, bool inline_mapping, bool pre_mapping,
                               const RegionRequirement &req, unsigned index,
                               const std::map<Memory,bool> &current_instances,
                               std::vector<Memory> &target_ranking,
                               std::set<FieldID> &additional_fields,
                               bool &enable_WAR_optimization)
  {
    log_mapper.info("mapper: mapping region for task (%s) region ("IDFMT" %x %x)",
                    task->variants->name,
                    req.region.get_index_space().id,
                    req.region.get_field_space().get_id(),
                    req.region.get_tree_id());
    int idx = index; 
    log_mapper.info("func_id=%d map_tag=%ld region_index=%d", task->task_id, task->tag, idx);

    int proc_id = get_proc_id_for_task(task->task_id, task->tag, cpu_mem_pairs.size());
    const CpuMemPair &cmp = cpu_mem_pairs[proc_id];

    switch (task->task_id) {
    case TOP_LEVEL_TASK_ID:
    case TASKID_DUMMY_TASK:
      {
        // Don't care, put it in global memory
        //target_ranking.push_back(global_memory);
	// actually, we do care - put it in local memory always
	target_ranking.push_back(cmp.sysmem);
      }
      break;
    case TASKID_MAIN_TASK:
      {
        switch (idx) {
        case 2: // real cell [0]
        case 3: // real cells [1]
#ifdef PARTITIONED_EDGE_CELLS
        case 4: // edge cells [0]
        case 5: // edge cells [1]
#endif
          {
            // Don't create an instance until explicitly requested
	    assert(task->tag == REQUEST_INSTANCE);
	    target_ranking.push_back(cmp.sysmem);
          }
          break;
        default:
          {
            // Copy anything else into global memory
            //target_ranking.push_back(global_memory);
            // Keep cell pointers in local memory
            target_ranking.push_back(cmp.sysmem);
          }
        }
      }
      break;
    case TASKID_LOAD_FILE:
      {
        switch (idx) {
        case 0: // config
        case 1: // block
        case 2: // block cell ptrs
          {
            // Put the owned cells in the local memory
            target_ranking.push_back(cmp.sysmem);
          }
          break;
        case 3: // real cells
          {
            // Put the owned cells in the local memory
            target_ranking.push_back(cmp.sysmem);

	    // make sure our instances have all the fields
	    additional_fields.insert(FID_CELL_NUM_PARTICLES);
	    additional_fields.insert(FID_CELL_P_X);
	    additional_fields.insert(FID_CELL_P_Y);
	    additional_fields.insert(FID_CELL_P_Z);
	    additional_fields.insert(FID_CELL_HV_X);
	    additional_fields.insert(FID_CELL_HV_Y);
	    additional_fields.insert(FID_CELL_HV_Z);
	    additional_fields.insert(FID_CELL_V_X);
	    additional_fields.insert(FID_CELL_V_Y);
	    additional_fields.insert(FID_CELL_V_Z);
	    additional_fields.insert(FID_CELL_A_X);
	    additional_fields.insert(FID_CELL_A_Y);
	    additional_fields.insert(FID_CELL_A_Z);
	    additional_fields.insert(FID_CELL_DENSITY);
          }
          break;
        default:
          {
            // Copy anything else into local memory
            target_ranking.push_back(cmp.sysmem);
          }
        }
      }
      break;
    case TASKID_SAVE_FILE:
      {
        switch (idx) {
        case 2: // real cells
          {
            // Put the owned cells in the local memory
            target_ranking.push_back(cmp.sysmem);
          }
          break;
        default:
          {
            // Copy anything else into local memory
            target_ranking.push_back(cmp.sysmem);
          }
        }
      }
      break;
    case TASKID_INIT_CELLS:
    case TASKID_INIT_CELLS_NOP:
      {
        switch (idx) { // First four regions should be local to us
        case 0: // config
        case 1: // blocks
        case 2: // block cell ptrs
          {
            // These should go in the local memory
            target_ranking.push_back(cmp.sysmem);

	  }
	  break;
        case 3: // source cells
        case 4: // dest cells
          {
            // These should go in the local memory
            target_ranking.push_back(cmp.sysmem);

	    // make sure our instances have all the fields
	    additional_fields.insert(FID_CELL_NUM_PARTICLES);
	    additional_fields.insert(FID_CELL_P_X);
	    additional_fields.insert(FID_CELL_P_Y);
	    additional_fields.insert(FID_CELL_P_Z);
	    additional_fields.insert(FID_CELL_HV_X);
	    additional_fields.insert(FID_CELL_HV_Y);
	    additional_fields.insert(FID_CELL_HV_Z);
	    additional_fields.insert(FID_CELL_V_X);
	    additional_fields.insert(FID_CELL_V_Y);
	    additional_fields.insert(FID_CELL_V_Z);
	    additional_fields.insert(FID_CELL_A_X);
	    additional_fields.insert(FID_CELL_A_Y);
	    additional_fields.insert(FID_CELL_A_Z);
	    additional_fields.insert(FID_CELL_DENSITY);
          }
          break;
        default:
          {
#if AVOID_OVERUSING_REGMEM
            int neighbor_id = get_proc_id_for_neighbor(task->task_id, task->tag, cpu_mem_pairs.size(), idx - 5, conf);

	    // ghost cells in registered memory (if available)
            if (cpu_mem_pairs[neighbor_id].sysmem != cmp.sysmem) {
              target_ranking.push_back(cmp.regmem);
            } else {
              target_ranking.push_back(cmp.sysmem);
            }
#else
	    // ghost cells in registered memory (if available)
            target_ranking.push_back(cmp.regmem);
#endif

	    // make sure our instances have all the fields
	    additional_fields.insert(FID_CELL_NUM_PARTICLES);
	    additional_fields.insert(FID_CELL_P_X);
	    additional_fields.insert(FID_CELL_P_Y);
	    additional_fields.insert(FID_CELL_P_Z);
	    additional_fields.insert(FID_CELL_HV_X);
	    additional_fields.insert(FID_CELL_HV_Y);
	    additional_fields.insert(FID_CELL_HV_Z);
	    additional_fields.insert(FID_CELL_V_X);
	    additional_fields.insert(FID_CELL_V_Y);
	    additional_fields.insert(FID_CELL_V_Z);
	    //additional_fields.insert(FID_CELL_A_X);
	    //additional_fields.insert(FID_CELL_A_Y);
	    //additional_fields.insert(FID_CELL_A_Z);
	    additional_fields.insert(FID_CELL_DENSITY);
          }
        }
      }
      break;
    case TASKID_CLEAR_DENSITIES:
    case TASKID_SCATTER_DENSITIES: // Operations which write ghost cells
    case TASKID_SCATTER_DENSITIES_NOP:
    //case TASKID_SCATTER_FORCES:
      {
        switch (idx) {
        case 0: // config
        case 1: // block
        case 2: // block cell ptrs
          {
            // Put the owned cells in the local memory
            target_ranking.push_back(cmp.sysmem);
	  }
	  break;
        case 3: // real cells
          {
            // Put the owned cells in the local memory
            target_ranking.push_back(cmp.sysmem);

	    // make sure our instances have all the fields
	    additional_fields.insert(FID_CELL_NUM_PARTICLES);
	    additional_fields.insert(FID_CELL_P_X);
	    additional_fields.insert(FID_CELL_P_Y);
	    additional_fields.insert(FID_CELL_P_Z);
	    additional_fields.insert(FID_CELL_HV_X);
	    additional_fields.insert(FID_CELL_HV_Y);
	    additional_fields.insert(FID_CELL_HV_Z);
	    additional_fields.insert(FID_CELL_V_X);
	    additional_fields.insert(FID_CELL_V_Y);
	    additional_fields.insert(FID_CELL_V_Z);
	    additional_fields.insert(FID_CELL_A_X);
	    additional_fields.insert(FID_CELL_A_Y);
	    additional_fields.insert(FID_CELL_A_Z);
	    additional_fields.insert(FID_CELL_DENSITY);
          }
          break;
        default:
          {
#if AVOID_OVERUSING_REGMEM
            int neighbor_id = get_proc_id_for_neighbor(task->task_id, task->tag, cpu_mem_pairs.size(), idx - 4, conf);

	    // ghost cells in registered memory (if available)
            if (cpu_mem_pairs[neighbor_id].sysmem != cmp.sysmem) {
              target_ranking.push_back(cmp.regmem);
            } else {
              target_ranking.push_back(cmp.sysmem);
            }
#else
	    // ghost cells in registered memory (if available)
            target_ranking.push_back(cmp.regmem);
#endif

	    // make sure our instances have all the fields
	    additional_fields.insert(FID_CELL_NUM_PARTICLES);
	    additional_fields.insert(FID_CELL_P_X);
	    additional_fields.insert(FID_CELL_P_Y);
	    additional_fields.insert(FID_CELL_P_Z);
	    additional_fields.insert(FID_CELL_HV_X);
	    additional_fields.insert(FID_CELL_HV_Y);
	    additional_fields.insert(FID_CELL_HV_Z);
	    additional_fields.insert(FID_CELL_V_X);
	    additional_fields.insert(FID_CELL_V_Y);
	    additional_fields.insert(FID_CELL_V_Z);
	    //additional_fields.insert(FID_CELL_A_X);
	    //additional_fields.insert(FID_CELL_A_Y);
	    //additional_fields.insert(FID_CELL_A_Z);
	    additional_fields.insert(FID_CELL_DENSITY);
          }
        }
      }
      break;
    case TASKID_REBUILD_REDUCE:
    case TASKID_REBUILD_REDUCE_NOP:
      {
        switch (idx) {
        case 0: // block
        case 1: // block cell ptrs
          {
            // Put the owned cells in the local memory
            target_ranking.push_back(cmp.sysmem);
	  }
	  break;
        case 2: // real cells
          {
            // These are the owned cells, keep them in local memory
            target_ranking.push_back(cmp.sysmem);

	    // make sure our instances have all the fields
	    additional_fields.insert(FID_CELL_NUM_PARTICLES);
	    additional_fields.insert(FID_CELL_P_X);
	    additional_fields.insert(FID_CELL_P_Y);
	    additional_fields.insert(FID_CELL_P_Z);
	    additional_fields.insert(FID_CELL_HV_X);
	    additional_fields.insert(FID_CELL_HV_Y);
	    additional_fields.insert(FID_CELL_HV_Z);
	    additional_fields.insert(FID_CELL_V_X);
	    additional_fields.insert(FID_CELL_V_Y);
	    additional_fields.insert(FID_CELL_V_Z);
	    additional_fields.insert(FID_CELL_A_X);
	    additional_fields.insert(FID_CELL_A_Y);
	    additional_fields.insert(FID_CELL_A_Z);
	    additional_fields.insert(FID_CELL_DENSITY);
          }
          break;
        default:
          {
#if AVOID_OVERUSING_REGMEM
            int neighbor_id = get_proc_id_for_neighbor(task->task_id, task->tag, cpu_mem_pairs.size(), idx - 3, conf);

	    // ghost cells in registered memory (if available)
            if (cpu_mem_pairs[neighbor_id].sysmem != cmp.sysmem) {
              target_ranking.push_back(cmp.regmem);
            } else {
              target_ranking.push_back(cmp.sysmem);
            }
#else
	    // ghost cells in registered memory (if available)
            target_ranking.push_back(cmp.regmem);
#endif

	    // make sure our instances have all the fields
	    additional_fields.insert(FID_CELL_NUM_PARTICLES);
	    additional_fields.insert(FID_CELL_P_X);
	    additional_fields.insert(FID_CELL_P_Y);
	    additional_fields.insert(FID_CELL_P_Z);
	    additional_fields.insert(FID_CELL_HV_X);
	    additional_fields.insert(FID_CELL_HV_Y);
	    additional_fields.insert(FID_CELL_HV_Z);
	    additional_fields.insert(FID_CELL_V_X);
	    additional_fields.insert(FID_CELL_V_Y);
	    additional_fields.insert(FID_CELL_V_Z);
	    //additional_fields.insert(FID_CELL_A_X);
	    //additional_fields.insert(FID_CELL_A_Y);
	    //additional_fields.insert(FID_CELL_A_Z);
	    additional_fields.insert(FID_CELL_DENSITY);
          }
        }
      }
      break;
    //case TASKID_GATHER_DENSITIES:
    case TASKID_CLEAR_FORCES:
    case TASKID_GATHER_FORCES:
    case TASKID_GATHER_FORCES_NOP:
      {
        switch (idx) {
	case 0: // config
        case 1: // block
        case 2: // block cell ptrs
          {
            // Put the owned cells in the local memory
            target_ranking.push_back(cmp.sysmem);
	  }
	  break;
        case 3: // real cells
          {
            // These are the owned cells, keep them in local memory
            target_ranking.push_back(cmp.sysmem);

	    // make sure our instances have all the fields
	    additional_fields.insert(FID_CELL_NUM_PARTICLES);
	    additional_fields.insert(FID_CELL_P_X);
	    additional_fields.insert(FID_CELL_P_Y);
	    additional_fields.insert(FID_CELL_P_Z);
	    additional_fields.insert(FID_CELL_HV_X);
	    additional_fields.insert(FID_CELL_HV_Y);
	    additional_fields.insert(FID_CELL_HV_Z);
	    additional_fields.insert(FID_CELL_V_X);
	    additional_fields.insert(FID_CELL_V_Y);
	    additional_fields.insert(FID_CELL_V_Z);
	    additional_fields.insert(FID_CELL_A_X);
	    additional_fields.insert(FID_CELL_A_Y);
	    additional_fields.insert(FID_CELL_A_Z);
	    additional_fields.insert(FID_CELL_DENSITY);
          }
          break;
        default:
          {
#if AVOID_OVERUSING_REGMEM
            int neighbor_id = get_proc_id_for_neighbor(task->task_id, task->tag, cpu_mem_pairs.size(), idx - 4, conf);

	    // ghost cells in registered memory (if available)
            if (cpu_mem_pairs[neighbor_id].sysmem != cmp.sysmem) {
              target_ranking.push_back(cmp.regmem);
            } else {
              target_ranking.push_back(cmp.sysmem);
            }
#else
	    // ghost cells in registered memory (if available)
            target_ranking.push_back(cmp.regmem);
#endif

	    // make sure our instances have all the fields
	    additional_fields.insert(FID_CELL_NUM_PARTICLES);
	    additional_fields.insert(FID_CELL_P_X);
	    additional_fields.insert(FID_CELL_P_Y);
	    additional_fields.insert(FID_CELL_P_Z);
	    additional_fields.insert(FID_CELL_HV_X);
	    additional_fields.insert(FID_CELL_HV_Y);
	    additional_fields.insert(FID_CELL_HV_Z);
	    additional_fields.insert(FID_CELL_V_X);
	    additional_fields.insert(FID_CELL_V_Y);
	    additional_fields.insert(FID_CELL_V_Z);
	    //additional_fields.insert(FID_CELL_A_X);
	    //additional_fields.insert(FID_CELL_A_Y);
	    //additional_fields.insert(FID_CELL_A_Z);
	    additional_fields.insert(FID_CELL_DENSITY);
          }
        }
      }
      break;
    default:
      assert(false);
    }
    
    char buffer[256];
    sprintf(buffer, "mapper: chose dst=[");
    for(unsigned i = 0; i < target_ranking.size(); i++) {
      if(i) strcat(buffer, ", ");
      sprintf(buffer+strlen(buffer), ""IDFMT"", target_ranking[i].id);
    }
    strcat(buffer, "]");
    log_mapper.info("%s", buffer);
    return true;
  }

  virtual void rank_copy_targets(const Task *task, const RegionRequirement &req,
                                 const std::set<Memory> &current_instances,
                                 std::vector<Memory> &future_ranking)
  {
    log_mapper.info("mapper: ranking copy targets (%p)\n", task);
    //Mapper::rank_copy_targets(task, req, current_instances, future_ranking);

    // Always copy back to GASNET
    assert(0);
    //future_ranking.push_back(global_memory);
  }

  // virtual void select_copy_source(const std::set<Memory> &current_instances,
  //                                 const Memory &dst, Memory &chosen_src)
  // {
  //   if(current_instances.size() == 1) {
  //     chosen_src = *(current_instances.begin());
  //     return;
  //   }
  //   log_mapper.info("mapper: selecting copy source\n");
  //   for(std::set<Memory>::const_iterator it = current_instances.begin();
  // 	it != current_instances.end();
  // 	it++)
  //     log_mapper.info("  choice = "IDFMT"", (*it).id);
  //   Mapper::select_copy_source(current_instances, dst, chosen_src);
  // }

  // virtual bool compact_partition(const Partition &partition,
  //                                MappingTagID tag)
  // {
  //   return false;
  // }
};
#else
class FluidMapper : public DefaultMapper
{
public:
  FluidMapper(Machine *m, HighLevelRuntime *rt, Processor local)
    : DefaultMapper(m, rt, local)
  {
    std::set<Processor> procset;
    machine_interface.filter_processors(m, Processor::LOC_PROC, procset);
    procs.assign(procset.begin(), procset.end());
  }

  virtual void select_task_options(Task *task);
  virtual bool map_task(Task *task);
  virtual bool map_inline(Inline *inline_operation);
private:
  std::vector<Processor> procs;
};

void FluidMapper::select_task_options(Task *task)
{
  task->target_proc = procs[task->tag % procs.size()];
}


bool FluidMapper::map_task(Task *task)
{
  Memory global_memory = machine_interface.find_global_memory();

  std::vector<RegionRequirement> &regions = task->regions;
  for (std::vector<RegionRequirement>::iterator it = regions.begin();
        it != regions.end(); it++) {
    RegionRequirement &req = *it;

    // Region options:
    req.virtual_map = false;
    req.enable_WAR_optimization = false;
    req.reduction_list = false;

    // Place all regions in global memory.
    req.target_ranking.push_back(global_memory);
  }

  return false;
}

bool FluidMapper::map_inline(Inline *inline_operation)
{
  Memory global_memory = machine_interface.find_global_memory();

  RegionRequirement &req = inline_operation->requirement;

  // Region options:
  req.virtual_map = false;
  req.enable_WAR_optimization = false;
  req.reduction_list = false;

  // Place all regions in global memory.
  req.target_ranking.push_back(global_memory);

  return false;
}
#endif

void create_mappers(Machine *machine, HighLevelRuntime *runtime, const std::set<Processor> &local_procs)
{
  for (std::set<Processor>::const_iterator it = local_procs.begin();
        it != local_procs.end(); it++) {
    runtime->replace_default_mapper(
      new FluidMapper(machine, runtime, *it), *it);
  }
}

int main(int argc, char **argv)
{
  // most args get parsed in the main task, but SOA vs AOS we have to know
  //  here if/when we want to swap task variants around
  for(int i = 1; i < argc; i++) {
    if(!strcmp(argv[i], "-soa"))   layout_policy = LAYOUT_ALL_SOA;
    if(!strcmp(argv[i], "-aos"))   layout_policy = LAYOUT_ALL_AOS;
    if(!strcmp(argv[i], "-mixed")) layout_policy = LAYOUT_MIXED;
  }

  TaskConfigOptions default_options(false/*leaf*/, false/*inner*/, false);
  TaskConfigOptions leaf_options(true/*leaf*/, false/*inner*/, false);

  HighLevelRuntime::set_top_level_task_id(TOP_LEVEL_TASK_ID);
  HighLevelRuntime::set_registration_callback(create_mappers);
  HighLevelRuntime::register_legion_task<top_level_task>(
    TOP_LEVEL_TASK_ID, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, default_options, "top_level_task");
  HighLevelRuntime::register_legion_task<main_task>(
    TASKID_MAIN_TASK, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, default_options, "main_task");
  HighLevelRuntime::register_legion_task<init_and_rebuild>(
    TASKID_INIT_CELLS, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "init_cells");
  HighLevelRuntime::register_legion_task<dummy_task>(
    TASKID_INIT_CELLS_NOP, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "init_cells_nop");
  HighLevelRuntime::register_legion_task<rebuild_reduce>(
    TASKID_REBUILD_REDUCE, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "rebuild_reduce");
  HighLevelRuntime::register_legion_task<dummy_task>(
    TASKID_REBUILD_REDUCE_NOP, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "rebuild_reduce_nop");
  HighLevelRuntime::register_legion_task<scatter_densities>(
    TASKID_SCATTER_DENSITIES, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "scatter_densities");
  HighLevelRuntime::register_legion_task<clear_densities>(
    TASKID_CLEAR_DENSITIES, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "clear_densities");
  HighLevelRuntime::register_legion_task<dummy_task>(
    TASKID_SCATTER_DENSITIES_NOP, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "scatter_densities_nop");
  HighLevelRuntime::register_legion_task<clear_forces>(
    TASKID_CLEAR_FORCES, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "clear_forces");
  HighLevelRuntime::register_legion_task<gather_forces_and_advance>(
    TASKID_GATHER_FORCES, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "gather_forces");
  HighLevelRuntime::register_legion_task<dummy_task>(
    TASKID_GATHER_FORCES_NOP, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, leaf_options, "gather_forces_nop");
  HighLevelRuntime::register_legion_task<load_file>(
    TASKID_LOAD_FILE, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, default_options, "load_file");
  HighLevelRuntime::register_legion_task<save_file>(
    TASKID_SAVE_FILE, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, default_options, "save_file");
  HighLevelRuntime::register_legion_task<dummy_task>(
    TASKID_DUMMY_TASK, Processor::LOC_PROC, true, false,
    AUTO_GENERATE_ID, default_options, "dummy_task");

  return HighLevelRuntime::start(argc, argv);
}

// EOF

