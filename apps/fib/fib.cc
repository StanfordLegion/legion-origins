#include <stdio.h>
#include "legion.h"

using namespace LegionRuntime::HighLevel;

enum {
  TASK_TOP = 100,
  TASK_FIB,
};

void task_top(const Task *task, const std::vector<PhysicalRegion> &regions,
              Context ctx, HighLevelRuntime *runtime)
{
  int maxarg = 8;
  std::vector<Future> results;

  for (int arg = 0; arg < maxarg; arg++) {
    TaskLauncher launch_fib(TASK_FIB, TaskArgument(&arg, sizeof(arg)));
    results.push_back(runtime->execute_task(ctx, launch_fib));
  }

  for (int arg = 0; arg < maxarg; arg++) {
    printf("fib(%d) => %d\n", arg, results[arg].get_result<int>());
  }
}

int task_fib(const Task *task, const std::vector<PhysicalRegion> &regions,
             Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(int));
  int arg = *static_cast<int *>(task->args);

  if (arg <= 1) {
    return 1;
  }

  int arg1 = arg - 1;
  TaskLauncher launch_fib1(TASK_FIB, TaskArgument(&arg1, sizeof(arg1)));
  Future future1 = runtime->execute_task(ctx, launch_fib1);

  int arg2 = arg - 2;
  TaskLauncher launch_fib2(TASK_FIB, TaskArgument(&arg2, sizeof(arg2)));
  Future future2 = runtime->execute_task(ctx, launch_fib2);

  return future1.get_result<int>() + future2.get_result<int>();
}

int main(int argc, char **argv)
{
  HighLevelRuntime::register_legion_task<task_top>(TASK_TOP,
      Processor::LOC_PROC, true /* single */, false/* index */,
      AUTO_GENERATE_ID, TaskConfigOptions(), "top");

  HighLevelRuntime::register_legion_task<int, task_fib>(TASK_FIB,
      Processor::LOC_PROC, true /* single */, false/* index */,
      AUTO_GENERATE_ID, TaskConfigOptions(), "fib");

  HighLevelRuntime::set_top_level_task_id(TASK_TOP);

  return HighLevelRuntime::start(argc, argv);
}
