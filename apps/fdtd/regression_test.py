#!/usr/bin/env python

import subprocess, sys, os
_root_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(_root_dir)
from validate import validate

tests = (
    {'sx': '1', 'sy': '1', 'sz': '1', 'a': '10', 'level': '3'},
    {'sx': '2', 'sy': '2', 'sz': '2', 'a': '10', 'level': '3'},
    {'sx': '3', 'sy': '3', 'sz': '3', 'a': '10', 'level': '3'},
    {'sx': '2', 'sy': '1', 'sz': '1', 'a': '10', 'level': '3'},
    {'sx': '2', 'sy': '2', 'sz': '2', 'a': '10', 'nbx': '2', 'level': '3'},
    {'sx': '2', 'sy': '2', 'sz': '2', 'a': '10', 'nby': '2', 'level': '3'},
    {'sx': '2', 'sy': '2', 'sz': '2', 'a': '10', 'nbz': '2', 'level': '3'},
    )

def solution_filename(test_spec):
    return 'solution_%s_%s_%s_%s.out' % (test_spec['sx'], test_spec['sy'], test_spec['sz'], test_spec['a'])

def run_test(test_spec):
    result_path = 'out.log'
    solution_path = solution_filename(test_spec)
    args = ['./fdtd']
    args.extend(x for k, v in test_spec.iteritems() for x in ['-%s' % k, v])
    print 'Checking %s' % ' '.join(args)
    with open(result_path, 'w') as out:
        proc1 = subprocess.Popen(args, stdout = subprocess.PIPE)
        proc2 = subprocess.Popen(['grep', 'XC:'], stdin = proc1.stdout, stdout = out)
        proc1.stdout.close()
        proc1.wait()
        proc2.wait()
        assert(proc1.returncode == 0 and proc2.returncode == 0)
    print 'Comparing %s and %s' % (result_path, solution_path)
    assert(validate(result_path, solution_path))

def run_all_tests():
    failed, passed = 0, 0
    for test_spec in tests:
        try:
            run_test(test_spec)
        except Exception as e:
            failed += 1
        else:
            passed += 1
    total = passed + failed
    if total > 0:
        print
        print 'Passed %d of %d tests (%.1f%%)' % (
            passed, total, float(100*passed)/total
            )

if __name__ == '__main__':
    run_all_tests()
