#!/usr/bin/env python

import sys

def parse(text):
    lines = iter(line.replace('XC: ', '', 1).split() for line in text.split('\n'))

    line = next(lines)
    assert(line[0] == 'Grid' and line[1] == 'size')
    nx, ny, nz = (int(ni) for ni in line[2:])

    line = next(lines)
    assert(line[0] == 'Time' and line[1] == 'step')
    t = float(line[2])

    result = {'nx': nx, 'ny': ny, 'nz': nz, 't': t}
    for component in ('ex', 'ey', 'ez', 'hx', 'hy', 'hz'):
        line = next(lines)
        assert(line[0] == 'Component')
        assert(line[1] == component)

        elts = []
        for _j in xrange(nx*ny):
            line = next(lines)
            assert(len(line) == nz)
            elts.extend(float(elt) for elt in line)
        result[component] = elts
    return result

def within(x, y, epsilon):
    return abs(x - y) < epsilon

def validate_result(result, solution, epsilon = 0.0000001):
    assert(result['nx'] == solution['nx'])
    assert(result['ny'] == solution['ny'])
    assert(result['nz'] == solution['nz'])
    assert(within(result['t'], solution['t'], epsilon))
    for component in ('ex', 'ey', 'ez', 'hx', 'hy', 'hz'):
        for result_elt, solution_elt in zip(result[component], solution[component]):
            assert(within(result_elt, solution_elt, epsilon))
    return True

def validate(result_filename, solution_filename):
    result = parse(open(result_filename).read())
    solution = parse(open(solution_filename).read())
    return validate_result(result, solution)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print 'Usage: %s <filename>', sys.argv[0]
        sys.exit()
    validate(sys.argv[1], sys.argv[2])
