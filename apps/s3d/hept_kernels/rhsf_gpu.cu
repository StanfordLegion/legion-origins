
#include "rhsf_gpu.h"

#include "cuda.h"
#include "cuda_runtime.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>

//#define DEBUG_CUDA

#ifdef DEBUG_CUDA
#define CUDA_SAFE_CALL(expr)                        \
  {                                                 \
    cudaError_t err = (expr);                       \
    if (err != cudaSuccess)                         \
    {                                               \
      printf("Cuda error: %s\n", cudaGetErrorString(err));  \
      exit(1);                                      \
    }                                               \
  }
#else
#define CUDA_SAFE_CALL(expr)  expr
#endif

extern __global__ void
gpu_conductivity(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *conductivity);

__host__
void run_gpu_conductivity(const double *temp_array, const double *mole_frac_array,
                          const double *mole_avg_array, const int nx, const int ny, 
                          const int nz, double *conductivity)
{
#ifdef K20_ARCH
  assert(((nx*ny*nz) % 128) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));
  gpu_conductivity<<<grid,block>>>(temp_array, mole_frac_array,
                                                 mole_avg_array,
                                                 nx*ny, nx, nz, nx*ny*nz,
                                                 conductivity);

#else
  assert(((nx*ny*nz) % 256) == 0);
  dim3 grid((nx*ny*nz)/256,1,1);
  dim3 block(256,1,1);
  gpu_conductivity<<<grid,block>>>(temp_array, mole_frac_array,
                                                 mole_avg_array,
                                                 nx*ny, nx, nz, nx*ny*nz,
                                                 conductivity);
#endif
}

extern __global__ void
gpu_viscosity(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *viscosity);

__host__
void run_gpu_viscosity(const double *temp_array, const double *mole_frac_array,
                       const double *mole_avg_array, const int nx, const int ny, 
                       const int nz, double *viscosity)
{
  assert(((nx*ny*nz) % 16) == 0);
  dim3 grid((nx*ny*nz)/16,1,1);
  dim3 block(13*32,1,1);
#ifdef K20_ARCH
  CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));
#endif
  gpu_viscosity<<<grid,block>>>(temp_array, mole_frac_array,
                                              mole_avg_array,
                                              nx*ny, nx, nz, nx*ny*nz,
                                              viscosity);
}

extern __global__ void
gpu_diffusion(const double *temperature_array, const double *pressure_array, 
  const double *mass_frac_array, const double *avmolwt_array, const int 
  slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX in number 
  of doubles*/, const int total_steps/*NZ in number of doubles*/, const int 
  spec_stride/*NX*NY*NZ in number of doubles*/, double *diffusion);

__host__
void run_gpu_diffusion(const double *temp_array, const double *pres_array,
                       const double *mole_frac_array, const double *mole_avg_array,
                       const int nx, const int ny, const int nz, double *diffusion)
{
  assert(((nx*ny*nz) % 16) == 0);
  dim3 grid((nx*ny*nz)/16,1,1);
  dim3 block(13*32,1,1);
#ifdef K20_ARCH
  CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));
#endif
  gpu_diffusion<<<grid,block>>>(temp_array, pres_array,
                                              mole_frac_array, mole_avg_array,
                                              nx*ny, nx, nz, nx*ny*nz,
                                              diffusion);
}

extern __global__ void
gpu_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *thermal_out); 

__host__
void run_gpu_thermal(const double *temp_array, const double *mole_frac_array,
                     const double *mole_avg_array, const int nx, const int ny, 
                     const int nz, double *thermal)
{
  assert(((nx*ny*nz) % 128) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  gpu_thermal<<<grid,block>>>(temp_array, mole_frac_array,
                                            mole_avg_array, 
                                            nx*ny, nx, nz, nx*ny*nz,
                                            thermal);
}

#ifndef FAST_GPU_CHEMISTRY
extern __global__ void
gpu_getrates(const double *temperature_array, const double *pressure_array, 
  const double *avmolwt_array, const double *mass_frac_array, const double 
  *diffusion_array, const double dt, const double recip_dt, const int 
  slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX in number 
  of doubles*/, const int total_steps/*NZ in number of doubles*/, const int 
  spec_stride/*NX*NY*NZ in number of doubles*/, const int step_stride/*always 
  zero*/, double *wdot_array);
#else
extern __global__ void
gpu_getrates(const double *temperature_array, const double *pressure_array, 
  const double *avmolwt_array, const double *mass_frac_array, const double 
  *diffusion_array, const double dt, const double recip_dt, const int 
  slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX in number 
  of doubles*/, const int total_steps/*NZ in number of doubles*/, const int 
  spec_stride/*NX*NY*NZ in number of doubles*/, const int step_stride/*always 
  zero*/, double *wdot_array);
#endif

#if 0
// used for capturing state for Singe debugging
__host__
static void capture_state(const double *temp_d, const double *pres_d,
                          const double *mass_frac_d, const double *avmolwt_d,
                          const double *diffusion_d, const int nx,
                          const int ny, const int nz)
{
  static int iteration = 0;
  if (iteration == 7)
  {
    const size_t dump_size = 32*32*32*sizeof(double);
    const int ns = 52;
    assert((nx*ny*nz*sizeof(double)) >= dump_size);
    FILE *target = fopen("chemistry.dat","w");
    double *buffer = (double*)malloc(dump_size);
    // Temperature
    cudaMemcpy(buffer,temp_d,dump_size,cudaMemcpyDeviceToHost);
    assert(fwrite(buffer,dump_size,1,target) > 0);
    // Pressure
    cudaMemcpy(buffer,pres_d,dump_size,cudaMemcpyDeviceToHost);
    assert(fwrite(buffer,dump_size,1,target) > 0);
    // Mass fractions 
    for (int s = 0; s < ns; s++)
    {
      cudaMemcpy(buffer,mass_frac_d+(s*nx*ny*nz),dump_size,cudaMemcpyDeviceToHost);
      assert(fwrite(buffer,dump_size,1,target) > 0);
    }
    // avmolwt
    cudaMemcpy(buffer,avmolwt_d,dump_size,cudaMemcpyDeviceToHost);
    assert(fwrite(buffer,dump_size,1,target) > 0);
    // Diffusion values
    for (int s = 0; s < ns; s++)
    {
      cudaMemcpy(buffer,diffusion_d+(s*nx*ny*nz),dump_size,cudaMemcpyDeviceToHost);
      assert(fwrite(buffer,dump_size,1,target) > 0);
    }
    free(buffer);
    assert(fclose(target) == 0);
  }
  iteration++;
}
#endif

__host__
void run_gpu_getrates(const double *temp_array, const double *pres_array,
                  const double *mass_frac_array, const double *avmolwt_array,
                  const double *diffusion_array, const int nx, const int ny, 
                  const int nz, const double dt, double *wdot)
{
#ifndef FAST_GPU_CHEMISTRY
  assert(((nx*ny*nz) % 128) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  gpu_getrates<<<grid,block>>>(temp_array, pres_array,
                                             avmolwt_array, mass_frac_array, diffusion_array,
                                             dt, (1.0/dt), nx*ny, nx, nz, nx*ny*nz, 0, wdot);
#else
  assert(((nx*ny) % 32) == 0);
  dim3 grid((nx*ny)/32,1,1);
#ifdef K20_ARCH
  dim3 block(16*32,1,1);
  CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));
#else
  dim3 block(20*32,1,1);
#endif
  gpu_getrates<<<grid,block>>>(temp_array, pres_array,
                                             avmolwt_array, mass_frac_array, diffusion_array,
                                             dt, (1.0/dt), nx*ny, nx, nz, nx*ny*nz, 0, wdot);
#endif
}

#define MACH_NO           (0.001)
#define RE_REAL           (100.0)
#define PARAM_PR          (0.708)
#define G_REF             (1.4)
#define A_REF             (347.2) // m/s
#define T_0               (300.0) // K
#define RHO_REF           (1.1766)  // Kg/m3
#define LAMBDA_REF        (26.14e-3)
#define PARAM_RE          (RE_REAL / MACH_NO)
#define T_REF             (T_0 * (G_REF - 1))
#define CP_REF            ((A_REF * A_REF) / T_REF)
#define L_REF             (PARAM_PR * LAMBDA_REF / CP_REF * PARAM_RE / RHO_REF / A_REF)
#define VIS_REF           (RHO_REF * A_REF * L_REF)
#define DIF_REF           (A_REF * L_REF)
#define P_REF             ((A_REF * A_REF) * RHO_REF * 10.0)

__constant__
double recip_molecular_masses[52] = {0.9920930186414277, 
  0.4960465093207139, 0.06250234383789392, 0.03125117191894696, 
  0.05879803873262004, 0.05550825019122593, 0.03570083414998991, 
  0.02272213442641948, 0.06651120780362699, 0.06233236489615739, 
  0.03029681486555637, 0.02939901936631002, 0.03330392596670473, 
  0.03325560390181349, 0.03564531203549703, 0.0344090165386938, 
  0.03995162657054838, 0.0384050534905585, 0.03120861932131863, 
  0.02378820504671527, 0.02437260645771706, 0.02269963076780593, 
  0.02495923532889907, 0.02495923532889907, 0.02376354135699802, 
  0.01848687856819865, 0.01814869186951308, 0.01782265601774851, 
  0.01750812771058645, 0.01752151203640269, 0.01721743223692151, 
  0.01446602711396394, 0.01425812481419881, 0.01405611369259448, 
  0.021261278576753, 0.02081519375927187, 0.01816307388956415, 
  0.01783652574443861, 0.02434671672351625, 0.02560336899370566, 
  0.01386813291662368, 0.01406473915042506, 0.01783652574443861, 
  0.01160970269292859, 0.01174717081138179, 9.979485172331238e-03, 
  0.01008088905376743, 7.622160364156333e-03, 6.127631204839358e-03, 
  8.757408658257278e-03, 6.840514699479429e-03, 0.03569720205330306}; 

__global__ void
__launch_bounds__(128,4)
gpu_dimensionalize(const double *nondim_temp, const double *nondim_pres,
                   const double *avmolwt, const double *nondim_spec,
                   const int slice_stride, const int row_stride, 
                   const int total_steps, const int spec_stride,
                   double *dim_temp, double *dim_pres, double *dim_spec)
{
  {
    const int offset = (blockIdx.y*blockDim.y+threadIdx.y)*row_stride +
                       (blockIdx.x*blockDim.x+threadIdx.x) +
                       blockIdx.z*slice_stride;
    nondim_temp += offset;
    nondim_pres += offset;
    avmolwt += offset;
    nondim_spec += offset;
    dim_temp += offset;
    dim_pres += offset;
    dim_spec += offset;
  }
  // Update temperature
  {
    double temp = *nondim_temp;
    temp *= T_REF;
    *dim_temp = temp;
  }
  // Update pressure
  {
    double pres = *nondim_pres;
    pres *= P_REF;
    *dim_pres = pres;
  }
  // Compute mole fractions
  const double avg = *avmolwt;
  #pragma unroll
  for (int i = 0; i < 52; i++)
  {
    double yspec = nondim_spec[i*spec_stride];
    double next = yspec * recip_molecular_masses[i] * 1e3 / avg;
    dim_spec[i*spec_stride] = next;
  }
}

__host__
void run_gpu_dimensionalize(const double *nondim_temp, const double *nondim_pres,
                            const double *avmolwt, const double *nondim_spec,
                            const int nx, const int ny, const int nz,
                            double *dim_temp, double *dim_pres, double *dim_spec)
{
  assert(nx >= 32);
  assert(ny >= 4);
  assert((nx % 32) == 0);
  assert((ny % 4) == 0);
  dim3 grid(nx/32,ny/4,nz);
  dim3 block(32,4,1);
  gpu_dimensionalize<<<grid,block>>>(nondim_temp, nondim_pres,
                                                   avmolwt, nondim_spec,
                                                   nx*ny, nx, nz, nx*ny*nz,
                                                   dim_temp, dim_pres, dim_spec);
}

__global__ void
__launch_bounds__(128,4)
gpu_nondimensionalize(const int slice_stride, const int row_stride,
                      const int total_steps, const int spec_stride,
                      double *lambda, double *viscosity, double *diffusion)
{
  {
    const int offset = (blockIdx.y*blockDim.y+threadIdx.y)*row_stride +
                       (blockIdx.x*blockDim.x+threadIdx.x) +
                       blockIdx.z*slice_stride;
    lambda += offset;
    viscosity += offset;
    diffusion += offset;
  } 
  // Update lambda
  {
    double conductivity = *lambda;
    double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
    conductivity *= (1.0/(lambda_ref * 1e5));
    *lambda = conductivity;
  }
  // Update viscosity
  {
    double visc = *viscosity;
    visc *= (1.0/(VIS_REF * 10.0));
    *viscosity = visc;
  }
  // Update diffusion
  #pragma unroll
  for (int i = 0; i < 52; i++)
  {
    double *diff_spec = diffusion + i*spec_stride;
    double diff = *diff_spec;
    diff *= (1.0 / (DIF_REF * 1e4));
    *diff_spec = diff; 
  }
}

__host__
void run_gpu_nondimensionalize(const int nx, const int ny, const int nz,
                               double *lambda, double *viscosity, double *diffusion)
{
  assert(nx >= 32);
  assert(ny >= 4);
  assert((nx % 32) == 0);
  assert((ny % 4) == 0);
  dim3 grid(nx/32,ny/4,nz);
  dim3 block(32,4,1);
  gpu_nondimensionalize<<<grid,block>>>(nx*ny, nx, nz, nx*ny*nz,
                                                      lambda, viscosity, diffusion);
}

