
/*
 * Fermi tuned thermal kernel for C7H16.
 * Gets 4/6 CTAs with 4 warps per CTA.
 * There are two versions of this kernel:
 * one which uses 64-byte loads and stores and one
 * which uses 128-byte loads and stores.
 * Should compile to 41 and 62 registers.
 * Completely memory bandwidth limited.
 *
 * Can tune number of threads for each CTA
 * Probably want at least 4 warps per CTA
 * since max number of CTAs/SM is 8 on Fermi.
 *
 * Launch with (64-byte version):
 *  dim3 grid((nx*ny*nz)/128,1,1);
 *  dim3 block(128,1,1);
 *
 * Launch with (128-byte version):
 *  dim3 grid((nx*ny*nz)/256,1,1);
 *  dim3 grid(128,1,1);
 *
 * gpu_thermal<<<grid,block>>>(temperature_ptr,  
 *                 mole_frac_input_ptr, nx*ny, nx, nz, nx*ny*nz, output_d);
 *
 * Performance numbers on C2070 with 14 SMs with 6 memory partitions:
 * 
 * 32x32x32 (threads=128) 64-byte loads and stores
 *   Latency: 0.255 ms
 *   Throughput: 128.665 Mpoints/s
 *   Perf: 108.1 GB/s (54.6 GB/s read, 53.5 GB/s write)
 *
 * 64x64x64 (threads=128) 128-byte loads and stores
 *   Latency: 1.978 ms
 *   Throughput: 132.507 Mpoints/s
 *   Perf: 111.3 GB/s (56.2 GB/s read, 55.1 GB/s write) 
 *
 * 128x128x128 (threads=128) 128 byte loads and stores
 *   Latency: 16.069 ms
 *   Throughput: 130.507 Mpoints/
 *   Perf: 109.6 GB/s (55.3 GB/s read, 54.3 GB/s write)
 *
 * Generation command:
 *   May need to disable diffusion kernel generation to avoid warnings
 *   Set THERMAL_FENCES to 7/8 to control register usage
 *   ./singe --dir inputs/nC7H16 --cuda
 *
 */

__constant__ double recip_molecular_masses[52] = {0.9920930186414277, 
  0.4960465093207139, 0.06250234383789392, 0.03125117191894696, 
  0.05879803873262004, 0.05550825019122593, 0.03570083414998991, 
  0.02272213442641948, 0.06651120780362699, 0.06233236489615739, 
  0.03029681486555637, 0.02939901936631002, 0.03330392596670473, 
  0.03325560390181349, 0.03564531203549703, 0.0344090165386938, 
  0.03995162657054838, 0.0384050534905585, 0.03120861932131863, 
  0.02378820504671527, 0.02437260645771706, 0.02269963076780593, 
  0.02495923532889907, 0.02495923532889907, 0.02376354135699802, 
  0.01848687856819865, 0.01814869186951308, 0.01782265601774851, 
  0.01750812771058645, 0.01752151203640269, 0.01721743223692151, 
  0.01446602711396394, 0.01425812481419881, 0.01405611369259448, 
  0.021261278576753, 0.02081519375927187, 0.01816307388956415, 
  0.01783652574443861, 0.02434671672351625, 0.02560336899370566, 
  0.01386813291662368, 0.01406473915042506, 0.01783652574443861, 
  0.01160970269292859, 0.01174717081138179, 9.979485172331238e-03, 
  0.01008088905376743, 7.622160364156333e-03, 6.127631204839358e-03, 
  8.757408658257278e-03, 6.840514699479429e-03, 0.03569720205330306}; 

__global__ void
__launch_bounds__(128,4)
gpu_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *thermal_out) 
{
  {
    const int offset = (blockIdx.x*blockDim.x+threadIdx.x);
    temperature_array += offset;
    mass_frac_array += offset;
    avmolwt_array += offset;
    thermal_out += offset;
  }
  // Load the temperatures
  double temperature;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  // Load the average mole weight
  double avmolwt;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(avmolwt) : "l"(avmolwt_array) 
    : "memory"); 
  double thermal[2];
  thermal[0] = 0.0;
  double mass_frac_light_0;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac_light_0) : 
    "l"(mass_frac_array+0*spec_stride) : "memory"); 
  thermal[1] = 0.0;
  double mass_frac_light_1;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac_light_1) : 
    "l"(mass_frac_array+1*spec_stride) : "memory"); 
  temperature *= 120.0;
  double mole_frac_light_0 = mass_frac_light_0 * recip_molecular_masses[0] * 1e3 
    * avmolwt; 
  double mole_frac_light_1 = mass_frac_light_1 * recip_molecular_masses[1] * 1e3 
    * avmolwt; 
  {
    double mole_frac = mole_frac_light_0;
    // No interaction for species 0 with itself
    {
      double val = 
        __fma_rn(-2.636753465162816e-12,temperature,1.768015910550093e-08); 
      val = __fma_rn(val,temperature,-3.775348599026121e-05);
      val = __fma_rn(val,temperature,-0.1585930684619718);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mole_frac = mole_frac_light_1;
    {
      double val = 
        __fma_rn(2.636753465162816e-12,temperature,-1.768015910550093e-08); 
      val = __fma_rn(val,temperature,3.775348599026121e-05);
      val = __fma_rn(val,temperature,0.1585930684619718);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    // No interaction for species 1 with itself
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+2*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[2] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(1.757513014843561e-11,temperature,-1.285543870147497e-07); 
      val = __fma_rn(val,temperature,2.854588272324489e-04);
      val = __fma_rn(val,temperature,0.2976867248976821);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(-8.093601450240291e-13,temperature,6.103164319166216e-09); 
      val = __fma_rn(val,temperature,-3.287522678250429e-06);
      val = __fma_rn(val,temperature,0.4204726021699148);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+3*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[3] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(2.277133918512246e-11,temperature,-1.706164304353198e-07); 
      val = __fma_rn(val,temperature,3.891613712354512e-04);
      val = __fma_rn(val,temperature,0.2536889601459076);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.810981392477091e-12,temperature,-1.740692804329701e-08); 
      val = __fma_rn(val,temperature,4.085412256055141e-05);
      val = __fma_rn(val,temperature,0.4534799151152161);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+4*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[4] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(1.770736265618588e-11,temperature,-1.295216099504371e-07); 
      val = __fma_rn(val,temperature,2.876065744334992e-04);
      val = __fma_rn(val,temperature,0.2999264728725038);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(-8.217252067738418e-13,temperature,6.19640588058943e-09); 
      val = __fma_rn(val,temperature,-3.337748058348944e-06);
      val = __fma_rn(val,temperature,0.4268964046290616);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+5*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[5] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(2.969326092389167e-11,temperature,-2.506085990918214e-07); 
      val = __fma_rn(val,temperature,6.84139750691556e-04);
      val = __fma_rn(val,temperature,-0.1112922345203659);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.499719027252861e-11,temperature,-1.957520736623052e-07); 
      val = __fma_rn(val,temperature,4.742310930643889e-04);
      val = __fma_rn(val,temperature,0.09225446319738231);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+6*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[6] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(2.138048772731333e-11,temperature,-1.590656312136814e-07); 
      val = __fma_rn(val,temperature,3.597782376370609e-04);
      val = __fma_rn(val,temperature,0.2712782254042295);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.588830133987327e-12,temperature,-9.211783055858657e-09); 
      val = __fma_rn(val,temperature,2.51672947887423e-05);
      val = __fma_rn(val,temperature,0.4536271223894905);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+7*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[7] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.126315401830763e-11,temperature,-2.480822205530582e-07); 
      val = __fma_rn(val,temperature,6.132164599559319e-04);
      val = __fma_rn(val,temperature,0.06305126061316429);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.496814407970614e-11,temperature,-1.072253571601051e-07); 
      val = __fma_rn(val,temperature,2.336568835077498e-04);
      val = __fma_rn(val,temperature,0.3511176078507614);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+8*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[8] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(2.447219115855363e-11,temperature,-1.87350239923066e-07); 
      val = __fma_rn(val,temperature,4.393980243788655e-04);
      val = __fma_rn(val,temperature,0.1735855528169444);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.954421320119816e-12,temperature,-3.987829722071353e-08); 
      val = __fma_rn(val,temperature,8.518539304410531e-05);
      val = __fma_rn(val,temperature,0.3640489834414005);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+9*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[9] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(2.449287050458037e-11,temperature,-1.872670834275956e-07); 
      val = __fma_rn(val,temperature,4.384325498849513e-04);
      val = __fma_rn(val,temperature,0.179062640187455);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.829054410894387e-12,temperature,-3.891311305124087e-08); 
      val = __fma_rn(val,temperature,8.32178960638599e-05);
      val = __fma_rn(val,temperature,0.3723903181438474);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+10*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[10] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(2.281523401978496e-11,temperature,-1.709453166700634e-07); 
      val = __fma_rn(val,temperature,3.899115323879656e-04);
      val = __fma_rn(val,temperature,0.2541779799119729);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.821860427380867e-12,temperature,-1.747429617965584e-08); 
      val = __fma_rn(val,temperature,4.101223581825173e-05);
      val = __fma_rn(val,temperature,0.4552349690041921);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+11*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[11] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(2.285660222663966e-11,temperature,-1.712552719050051e-07); 
      val = __fma_rn(val,temperature,3.906185135617233e-04);
      val = __fma_rn(val,temperature,0.2546388512421464);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.83213077320014e-12,temperature,-1.753789502493254e-08); 
      val = __fma_rn(val,temperature,4.11615025362608e-05);
      val = __fma_rn(val,temperature,0.4568918264856361);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+12*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[12] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.162068743695279e-11,temperature,-2.638982110193224e-07); 
      val = __fma_rn(val,temperature,7.073407084606059e-04);
      val = __fma_rn(val,temperature,-0.09121151917142747);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.343646881471828e-11,temperature,-1.781654045987706e-07); 
      val = __fma_rn(val,temperature,4.139309999808266e-04);
      val = __fma_rn(val,temperature,0.1948729212751452);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+13*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[13] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.068609912428751e-11,temperature,-2.437355985544139e-07); 
      val = __fma_rn(val,temperature,6.03362935494119e-04);
      val = __fma_rn(val,temperature,0.05841809172467596);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.454408449375579e-11,temperature,-1.043349476396319e-07); 
      val = __fma_rn(val,temperature,2.276147817795946e-04);
      val = __fma_rn(val,temperature,0.3339639954619114);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+14*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[14] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.032335798336825e-11,temperature,-2.402507903023256e-07); 
      val = __fma_rn(val,temperature,5.924350932493196e-04);
      val = __fma_rn(val,temperature,0.06680574170940606);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.388135433710349e-11,temperature,-9.92082642450402e-08); 
      val = __fma_rn(val,temperature,2.157956205583607e-04);
      val = __fma_rn(val,temperature,0.3372446703225273);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+15*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[15] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.061474747914644e-11,temperature,-2.431688619397056e-07); 
      val = __fma_rn(val,temperature,6.019599895579059e-04);
      val = __fma_rn(val,temperature,0.05828225735440704);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.44762907380564e-11,temperature,-1.038486153473362e-07); 
      val = __fma_rn(val,temperature,2.265538101580283e-04);
      val = __fma_rn(val,temperature,0.3324073025308119);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+16*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[16] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.062833770415917e-11,temperature,-2.444134365186354e-07); 
      val = __fma_rn(val,temperature,6.094501095785496e-04);
      val = __fma_rn(val,temperature,0.04180903023643695);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.50987425728907e-11,temperature,-1.09042965559124e-07); 
      val = __fma_rn(val,temperature,2.392349771058175e-04);
      val = __fma_rn(val,temperature,0.3129007313029911);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+17*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[17] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.072412934963075e-11,temperature,-2.451778516653413e-07); 
      val = __fma_rn(val,temperature,6.113561950276935e-04);
      val = __fma_rn(val,temperature,0.04193979005241541);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.519378118153954e-11,temperature,-1.097293334258289e-07); 
      val = __fma_rn(val,temperature,2.407408349118242e-04);
      val = __fma_rn(val,temperature,0.3148702761180426);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+18*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[18] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.185130935783487e-11,temperature,-2.651424075793323e-07); 
      val = __fma_rn(val,temperature,7.077047347231746e-04);
      val = __fma_rn(val,temperature,-0.08530313446041707);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.327102184557304e-11,temperature,-1.764833502784834e-07); 
      val = __fma_rn(val,temperature,4.087327251764788e-04);
      val = __fma_rn(val,temperature,0.2037014810244143);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+19*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[19] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.25385022377053e-11,temperature,-2.688268803523174e-07); 
      val = __fma_rn(val,temperature,7.087521333192038e-04);
      val = __fma_rn(val,temperature,-0.0666682584411887);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.279801198506851e-11,temperature,-1.716221798512884e-07); 
      val = __fma_rn(val,temperature,3.937349326414702e-04);
      val = __fma_rn(val,temperature,0.2319811932466156);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+20*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[20] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(2.708995271314182e-11,temperature,-2.079866531812512e-07); 
      val = __fma_rn(val,temperature,4.897257316956756e-04);
      val = __fma_rn(val,temperature,0.1793027967505053);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(7.667061037097754e-12,temperature,-5.170172757195434e-08); 
      val = __fma_rn(val,temperature,1.102581228924998e-04);
      val = __fma_rn(val,temperature,0.4266508259851286);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+21*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[21] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.261002571364098e-11,temperature,-2.694177936269137e-07); 
      val = __fma_rn(val,temperature,7.1031005432557e-04);
      val = __fma_rn(val,temperature,-0.06681480315745897);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.289851358095159e-11,temperature,-1.723787503353852e-07); 
      val = __fma_rn(val,temperature,3.954706536820286e-04);
      val = __fma_rn(val,temperature,0.2330038473337971);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+22*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[22] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.230691790300545e-11,temperature,-2.613766930898042e-07); 
      val = __fma_rn(val,temperature,6.66106286262361e-04);
      val = __fma_rn(val,temperature,-3.320420756542652e-03);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.888310737681516e-11,temperature,-1.388603431919326e-07); 
      val = __fma_rn(val,temperature,3.100481316070349e-04);
      val = __fma_rn(val,temperature,0.2929305605177095);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+23*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[23] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.230691790300545e-11,temperature,-2.613766930898042e-07); 
      val = __fma_rn(val,temperature,6.66106286262361e-04);
      val = __fma_rn(val,temperature,-3.320420756542652e-03);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.888310737681516e-11,temperature,-1.388603431919326e-07); 
      val = __fma_rn(val,temperature,3.100481316070349e-04);
      val = __fma_rn(val,temperature,0.2929305605177095);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+24*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[24] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.223714578667719e-11,temperature,-2.598526009100887e-07); 
      val = __fma_rn(val,temperature,6.583319655886146e-04);
      val = __fma_rn(val,temperature,8.841530685384569e-03);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.823098471289975e-11,temperature,-1.334478883305589e-07); 
      val = __fma_rn(val,temperature,2.96541354688965e-04);
      val = __fma_rn(val,temperature,0.3051880222242602);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+25*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[25] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.290512752654892e-11,temperature,-2.679705609324293e-07); 
      val = __fma_rn(val,temperature,6.901653958129413e-04);
      val = __fma_rn(val,temperature,-0.02458194725485896);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.068795839855288e-11,temperature,-1.533286435933309e-07); 
      val = __fma_rn(val,temperature,3.452962832267535e-04);
      val = __fma_rn(val,temperature,0.2807711887572397);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+26*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[26] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.292045725824064e-11,temperature,-2.67989830078188e-07); 
      val = __fma_rn(val,temperature,6.897761918718049e-04);
      val = __fma_rn(val,temperature,-0.02335263835172963);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.063977735122053e-11,temperature,-1.529017779117991e-07); 
      val = __fma_rn(val,temperature,3.44158250624087e-04);
      val = __fma_rn(val,temperature,0.2823555583880165);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+27*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[27] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.294210907594261e-11,temperature,-2.681660872577683e-07); 
      val = __fma_rn(val,temperature,6.902298583638711e-04);
      val = __fma_rn(val,temperature,-0.02336799740535642);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.066696271536391e-11,temperature,-1.531031701283899e-07); 
      val = __fma_rn(val,temperature,3.446115533514836e-04);
      val = __fma_rn(val,temperature,0.282727458653318);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+28*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[28] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.295146592814352e-11,temperature,-2.680829524453684e-07); 
      val = __fma_rn(val,temperature,6.893546331607471e-04);
      val = __fma_rn(val,temperature,-0.02149408403266997);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.057714036645206e-11,temperature,-1.523324164542271e-07); 
      val = __fma_rn(val,temperature,3.426112497355691e-04);
      val = __fma_rn(val,temperature,0.2849016924115291);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+29*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[29] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.295397853943752e-11,temperature,-2.722362960411805e-07); 
      val = __fma_rn(val,temperature,7.176412891912808e-04);
      val = __fma_rn(val,temperature,-0.06727921335864356);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.336809597149917e-11,temperature,-1.75898558745162e-07); 
      val = __fma_rn(val,temperature,4.035022162205326e-04);
      val = __fma_rn(val,temperature,0.2381854910990132);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+30*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[30] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.297515624149514e-11,temperature,-2.723973288205335e-07); 
      val = __fma_rn(val,temperature,7.180059381934386e-04);
      val = __fma_rn(val,temperature,-0.06717800882994507);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.491575702759885e-11,temperature,-1.892981309676922e-07); 
      val = __fma_rn(val,temperature,4.394499275830982e-04);
      val = __fma_rn(val,temperature,0.2098605478757698);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+31*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[31] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.323050289531883e-11,temperature,-2.726604349389064e-07); 
      val = __fma_rn(val,temperature,7.108437800855215e-04);
      val = __fma_rn(val,temperature,-0.04798447273844533);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.245473495982841e-11,temperature,-1.678251545914919e-07); 
      val = __fma_rn(val,temperature,3.816451433092867e-04);
      val = __fma_rn(val,temperature,0.2621011851697239);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+32*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[32] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.324380578881488e-11,temperature,-2.722399878445388e-07); 
      val = __fma_rn(val,temperature,7.075106530904983e-04);
      val = __fma_rn(val,temperature,-0.04213655105859476);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.213786368334464e-11,temperature,-1.651309811589791e-07); 
      val = __fma_rn(val,temperature,3.746396724757235e-04);
      val = __fma_rn(val,temperature,0.2679372303381302);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+33*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[33] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.316720602534791e-11,temperature,-2.742411801307379e-07); 
      val = __fma_rn(val,temperature,7.239744038703685e-04);
      val = __fma_rn(val,temperature,-0.07023061728558644);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.384797899271548e-11,temperature,-1.796719054618293e-07); 
      val = __fma_rn(val,temperature,4.126220378373132e-04);
      val = __fma_rn(val,temperature,0.238815692951617);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+34*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[34] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.249694012524726e-11,temperature,-2.705168835280551e-07); 
      val = __fma_rn(val,temperature,7.220500147192796e-04);
      val = __fma_rn(val,temperature,-0.08703224165490056);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.42260678774566e-11,temperature,-1.837262519652026e-07); 
      val = __fma_rn(val,temperature,4.255071740971657e-04);
      val = __fma_rn(val,temperature,0.2120614186512262);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+35*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[35] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.252619028502869e-11,temperature,-2.707603729777164e-07); 
      val = __fma_rn(val,temperature,7.226999244713772e-04);
      val = __fma_rn(val,temperature,-0.0871105784756821);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.426975779352755e-11,temperature,-1.840575886298645e-07); 
      val = __fma_rn(val,temperature,4.262745447170254e-04);
      val = __fma_rn(val,temperature,0.2124438556868042);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+36*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[36] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.288473792618383e-11,temperature,-2.720186605455213e-07); 
      val = __fma_rn(val,temperature,7.1859445557792e-04);
      val = __fma_rn(val,temperature,-0.07079495433060273);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.352481466438435e-11,temperature,-1.773111948712001e-07); 
      val = __fma_rn(val,temperature,4.074147004361504e-04);
      val = __fma_rn(val,temperature,0.2336367604891859);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+37*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[37] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.295317172770128e-11,temperature,-2.719176331453455e-07); 
      val = __fma_rn(val,temperature,7.154619062778548e-04);
      val = __fma_rn(val,temperature,-0.06402505305270359);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.463169482033282e-11,temperature,-1.868815485794694e-07); 
      val = __fma_rn(val,temperature,4.330541015233772e-04);
      val = __fma_rn(val,temperature,0.2136884083029943);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+38*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[38] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.227591603495213e-11,temperature,-2.606335427331569e-07); 
      val = __fma_rn(val,temperature,6.622092313952259e-04);
      val = __fma_rn(val,temperature,2.866816986383802e-03);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.855233513522455e-11,temperature,-1.361089053218244e-07); 
      val = __fma_rn(val,temperature,3.031602478166766e-04);
      val = __fma_rn(val,temperature,0.299205069471473);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+39*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[39] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.22649662451741e-11,temperature,-2.610372863526306e-07); 
      val = __fma_rn(val,temperature,6.652413240556753e-04);
      val = __fma_rn(val,temperature,-3.316109074572554e-03);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.883400286801476e-11,temperature,-1.384992443108944e-07); 
      val = __fma_rn(val,temperature,3.092418680560616e-04);
      val = __fma_rn(val,temperature,0.292168810293035);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+40*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[40] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.307964045390942e-11,temperature,-2.745854911053823e-07); 
      val = __fma_rn(val,temperature,7.295120438281277e-04);
      val = __fma_rn(val,temperature,-0.08087631222093869);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.560446285481596e-11,temperature,-1.94951688353351e-07); 
      val = __fma_rn(val,temperature,4.538720510087629e-04);
      val = __fma_rn(val,temperature,0.2057271860455629);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+41*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[41] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.311391013659248e-11,temperature,-2.744088971340765e-07); 
      val = __fma_rn(val,temperature,7.270466687522299e-04);
      val = __fma_rn(val,temperature,-0.07632773335550136);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.563191071693732e-11,temperature,-1.952187591535617e-07); 
      val = __fma_rn(val,temperature,4.546740419576007e-04);
      val = __fma_rn(val,temperature,0.2045964797566218);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+42*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[42] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.29064002577697e-11,temperature,-2.721978487888793e-07); 
      val = __fma_rn(val,temperature,7.190678189785176e-04);
      val = __fma_rn(val,temperature,-0.0708415894529123);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.355584879386443e-11,temperature,-1.77545105261501e-07); 
      val = __fma_rn(val,temperature,4.079521652683204e-04);
      val = __fma_rn(val,temperature,0.2339449760300846);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+43*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[43] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.316305214484237e-11,temperature,-2.758495683296961e-07); 
      val = __fma_rn(val,temperature,7.353587689012826e-04);
      val = __fma_rn(val,temperature,-0.08673865957101029);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.573775681904159e-11,temperature,-1.958447266052752e-07); 
      val = __fma_rn(val,temperature,4.555743378994857e-04);
      val = __fma_rn(val,temperature,0.209647883107428);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+44*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[44] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.308270456081856e-11,temperature,-2.757122913444244e-07); 
      val = __fma_rn(val,temperature,7.373121603827887e-04);
      val = __fma_rn(val,temperature,-0.09170212086428919);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.535475864578331e-11,temperature,-1.924961889670894e-07); 
      val = __fma_rn(val,temperature,4.464556531291609e-04);
      val = __fma_rn(val,temperature,0.2168498660404349);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+45*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[45] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.336227471919228e-11,temperature,-2.767227825010256e-07); 
      val = __fma_rn(val,temperature,7.342852071174661e-04);
      val = __fma_rn(val,temperature,-0.07947743603312946);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.477325850498314e-11,temperature,-1.872262620347478e-07); 
      val = __fma_rn(val,temperature,4.316762139434951e-04);
      val = __fma_rn(val,temperature,0.2330125297269697);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+46*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[46] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.335545466484962e-11,temperature,-2.766662136839891e-07); 
      val = __fma_rn(val,temperature,7.341351014949413e-04);
      val = __fma_rn(val,temperature,-0.07946118892656001);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.476312792988248e-11,temperature,-1.871496992479841e-07); 
      val = __fma_rn(val,temperature,4.314996877790567e-04);
      val = __fma_rn(val,temperature,0.2329172434758164);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+47*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[47] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.280936558612366e-11,temperature,-2.764466684600639e-07); 
      val = __fma_rn(val,temperature,7.526250807223522e-04);
      val = __fma_rn(val,temperature,-0.1188862252915318);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.7570900537465e-11,temperature,-2.116487295622062e-07); 
      val = __fma_rn(val,temperature,4.982492158902414e-04);
      val = __fma_rn(val,temperature,0.183133307386705);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+48*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[48] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.253507397211482e-11,temperature,-2.75710681147811e-07); 
      val = __fma_rn(val,temperature,7.576610031646734e-04);
      val = __fma_rn(val,temperature,-0.1316343877874153);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.841286845917054e-11,temperature,-2.190737570648687e-07); 
      val = __fma_rn(val,temperature,5.189071513362518e-04);
      val = __fma_rn(val,temperature,0.1687672145898553);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+49*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[49] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.313264302131829e-11,temperature,-2.770977977503217e-07); 
      val = __fma_rn(val,temperature,7.452730610745273e-04);
      val = __fma_rn(val,temperature,-0.1010865704936297);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.619695016529612e-11,temperature,-1.995348344161635e-07); 
      val = __fma_rn(val,temperature,4.647655258162161e-04);
      val = __fma_rn(val,temperature,0.2088141607458208);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+50*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[50] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(3.267508505142998e-11,temperature,-2.761313237880533e-07); 
      val = __fma_rn(val,temperature,7.554028172414584e-04);
      val = __fma_rn(val,temperature,-0.1255892993729075);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.808741692316584e-11,temperature,-2.16215060143183e-07); 
      val = __fma_rn(val,temperature,5.109756715260993e-04);
      val = __fma_rn(val,temperature,0.173974029290639);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+51*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[51] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(2.130268425696491e-11,temperature,-1.584132795786654e-07); 
      val = __fma_rn(val,temperature,3.581104184828728e-04);
      val = __fma_rn(val,temperature,0.2725429566377711);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.514695193142395e-12,temperature,-8.719776099246706e-09); 
      val = __fma_rn(val,temperature,2.424900788336922e-05);
      val = __fma_rn(val,temperature,0.4541343543221107);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+0*spec_stride) , 
    "d"(thermal[0]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+1*spec_stride) , 
    "d"(thermal[1]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+2*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+3*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+4*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+5*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+6*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+7*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+8*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+9*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+10*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+11*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+12*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+13*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+14*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+15*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+16*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+17*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+18*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+19*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+20*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+21*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+22*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+23*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+24*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+25*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+26*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+27*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+28*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+29*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+30*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+31*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+32*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+33*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+34*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+35*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+36*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+37*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+38*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+39*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+40*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+41*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+42*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+43*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+44*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+45*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+46*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+47*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+48*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+49*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+50*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+51*spec_stride) 
    , "d"(0.0) : "memory"); 
}

