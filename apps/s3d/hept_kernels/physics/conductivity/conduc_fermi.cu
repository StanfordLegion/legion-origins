
/*
 * Fermi tuned conductivity kernel for C7H16.
 * Gets 4 CTAs with 8 warps per CTA.
 * Should compile to 32 registers.
 * Math throughput limited:
 * 23 DMFAs per 8-byte load
 *
 * Can tune number of threads for each CTA
 * Probably want at least 8 warps per CTA
 * since max number of CTAs/SM is 8 on Fermi.
 *
 * Launch with:
 *  dim3 grid((nx*ny*nz)/256,1,1);
 *  dim3 block(256,1,1);
 *
 * gpu_conductivity<<<grid,block>>>(temperature_ptr,  
 *                 mole_frac_input_ptr, nx*ny, nx, nz, nx*ny*nz, output_d);
 *
 * Performance numbers on C2070 with 14 SMs:
 * 
 * 32x32x32 (threads=256)
 *   Latency: 0.452 ms
 *   Throughput: 72.450 Mpoints/s
 *   Perf: 204.3 GFLOPS
 *
 * 64x64x64 (threads=256)
 *   Latency: 3.365 ms
 *   Throughput: 77.902 Mpoints/s
 *   Perf: 227.6 GFLOPS
 *
 * 128x128x128 (threads=256)
 *   Latency: 26.896 ms
 *   Throughput: 77.973 Mpoints/s
 *   Perf: 230.9 GFLOPS
 *
 * Generation command:
 *   ./singe --dir inputs/nC7H16 --cuda
 *
 */

__constant__ double recip_molecular_masses[52] = {0.9920930186414277, 
  0.4960465093207139, 0.06250234383789392, 0.03125117191894696, 
  0.05879803873262004, 0.05550825019122593, 0.03570083414998991, 
  0.02272213442641948, 0.06651120780362699, 0.06233236489615739, 
  0.03029681486555637, 0.02939901936631002, 0.03330392596670473, 
  0.03325560390181349, 0.03564531203549703, 0.0344090165386938, 
  0.03995162657054838, 0.0384050534905585, 0.03120861932131863, 
  0.02378820504671527, 0.02437260645771706, 0.02269963076780593, 
  0.02495923532889907, 0.02495923532889907, 0.02376354135699802, 
  0.01848687856819865, 0.01814869186951308, 0.01782265601774851, 
  0.01750812771058645, 0.01752151203640269, 0.01721743223692151, 
  0.01446602711396394, 0.01425812481419881, 0.01405611369259448, 
  0.021261278576753, 0.02081519375927187, 0.01816307388956415, 
  0.01783652574443861, 0.02434671672351625, 0.02560336899370566, 
  0.01386813291662368, 0.01406473915042506, 0.01783652574443861, 
  0.01160970269292859, 0.01174717081138179, 9.979485172331238e-03, 
  0.01008088905376743, 7.622160364156333e-03, 6.127631204839358e-03, 
  8.757408658257278e-03, 6.840514699479429e-03, 0.03569720205330306}; 

__global__ void
__launch_bounds__(256,4)
gpu_conductivity(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *conductivity) 
{
  {
    const int offset = (blockIdx.x*blockDim.x+threadIdx.x);
    temperature_array += offset;
    mass_frac_array += offset;
    avmolwt_array += offset;
    conductivity += offset;
  }
  // Load the temperatures
  double temperature;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  // Load the average mole weight
  double avmolwt;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(avmolwt) : "l"(avmolwt_array) 
    : "memory"); 
  temperature *= 120.0;
  // Compute log(t)
  double logt = log(temperature);
  double sum = 0.0;
  double sumr = 0.0;
  // Species H
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+0*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[0] * 1e3 * avmolwt;
    double val = __fma_rn(0.01473938862116963,logt,-0.3401776285106699);
    val = __fma_rn(val,logt,3.261058144879565);
    val = __fma_rn(val,logt,0.02274969941640074);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+1*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[1] * 1e3 * avmolwt;
    double val = __fma_rn(-0.01372974142396394,logt,0.3410718605465255);
    val = __fma_rn(val,logt,-1.977628113931892);
    val = __fma_rn(val,logt,12.58393663792542);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species O
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+2*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[2] * 1e3 * avmolwt;
    double val = __fma_rn(6.048679323976329e-03,logt,-0.1372174134188909);
    val = __fma_rn(val,logt,1.680892288317469);
    val = __fma_rn(val,logt,2.241323411952997);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species O2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+3*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[3] * 1e3 * avmolwt;
    double val = __fma_rn(0.01382413591952113,logt,-0.3174172865521667);
    val = __fma_rn(val,logt,3.200330657881275);
    val = __fma_rn(val,logt,-2.618427696162442);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species OH
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+4*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[4] * 1e3 * avmolwt;
    double val = __fma_rn(-0.02917655465034293,logt,0.6330047633553172);
    val = __fma_rn(val,logt,-3.771562312017947);
    val = __fma_rn(val,logt,15.00655081891857);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2O
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+5*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[5] * 1e3 * avmolwt;
    double val = __fma_rn(-0.09188748927997951,logt,1.865915523816867);
    val = __fma_rn(val,logt,-11.28690040940324);
    val = __fma_rn(val,logt,28.51814848417955);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+6*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[6] * 1e3 * avmolwt;
    double val = __fma_rn(-0.01902954473348815,logt,0.3704518381861377);
    val = __fma_rn(val,logt,-1.581395250434219);
    val = __fma_rn(val,logt,8.335359673386879);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CO2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+7*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[7] * 1e3 * avmolwt;
    double val = __fma_rn(0.02555669781704255,logt,-0.6736263901574183);
    val = __fma_rn(val,logt,6.583392839820579);
    val = __fma_rn(val,logt,-12.9174208914974);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+8*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[8] * 1e3 * avmolwt;
    double val = __fma_rn(-0.03523872382293242,logt,0.6538055500349718);
    val = __fma_rn(val,logt,-2.936246978303837);
    val = __fma_rn(val,logt,10.20724412614542);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH4
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+9*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[9] * 1e3 * avmolwt;
    double val = __fma_rn(-0.03243163117874146,logt,0.525960502259268);
    val = __fma_rn(val,logt,-1.441415112612421);
    val = __fma_rn(val,logt,5.27997109929273);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HO2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+10*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[10] * 1e3 * avmolwt;
    double val = __fma_rn(-0.01035466079073106,logt,0.1416797395812364);
    val = __fma_rn(val,logt,0.4305512057109371);
    val = __fma_rn(val,logt,2.838717758076594);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2O2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+11*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[11] * 1e3 * avmolwt;
    double val = __fma_rn(-6.460865164786525e-03,logt,0.05869050570094366);
    val = __fma_rn(val,logt,1.052528466024186);
    val = __fma_rn(val,logt,1.506805433923156);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH2O
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+12*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[12] * 1e3 * avmolwt;
    double val = __fma_rn(-0.01598547128586226,logt,0.1308683454894246);
    val = __fma_rn(val,logt,1.7163744906578);
    val = __fma_rn(val,logt,-3.791462650658649);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H6
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+13*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[13] * 1e3 * avmolwt;
    double val = __fma_rn(0.01965539597409133,logt,-0.6576554399563096);
    val = __fma_rn(val,logt,7.499753566779444);
    val = __fma_rn(val,logt,-17.37128480809956);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H4
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+14*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[14] * 1e3 * avmolwt;
    double val = __fma_rn(0.02033202000910156,logt,-0.6478950279161562);
    val = __fma_rn(val,logt,7.217979224583608);
    val = __fma_rn(val,logt,-15.90406287185341);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H5
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+15*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[15] * 1e3 * avmolwt;
    double val = __fma_rn(5.157266296143541e-03,logt,-0.332313961259657);
    val = __fma_rn(val,logt,5.083567500047072);
    val = __fma_rn(val,logt,-11.5710860164518);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+16*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[16] * 1e3 * avmolwt;
    double val = __fma_rn(0.02113810970578733,logt,-0.5302141624530307);
    val = __fma_rn(val,logt,5.26471543892773);
    val = __fma_rn(val,logt,-8.967530043630704);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+17*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[17] * 1e3 * avmolwt;
    double val = __fma_rn(0.01399545035506612,logt,-0.4178138824809322);
    val = __fma_rn(val,logt,4.794485714833743);
    val = __fma_rn(val,logt,-8.460850627534054);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3OH
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+18*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[18] * 1e3 * avmolwt;
    double val = __fma_rn(-9.256139406355293e-03,logt,-0.03549974985485047);
    val = __fma_rn(val,logt,3.121378739687247);
    val = __fma_rn(val,logt,-7.465154054656418);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH2CO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+19*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[19] * 1e3 * avmolwt;
    double val = __fma_rn(8.267061292353856e-03,logt,-0.3557015205333651);
    val = __fma_rn(val,logt,4.851459603792371);
    val = __fma_rn(val,logt,-10.23797797770669);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HCCO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+20*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[20] * 1e3 * avmolwt;
    double val = __fma_rn(2.110940246916986e-03,logt,-0.1289980496470799);
    val = __fma_rn(val,logt,2.365989847134272);
    val = __fma_rn(val,logt,-0.858096830636617);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3CHO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+21*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[21] * 1e3 * avmolwt;
    double val = __fma_rn(7.700006048688366e-03,logt,-0.3959794446319773);
    val = __fma_rn(val,logt,5.610243800483931);
    val = __fma_rn(val,logt,-13.12220498163495);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C3H4-A
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+22*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[22] * 1e3 * avmolwt;
    double val = __fma_rn(0.01830268033858104,logt,-0.5997479909300256);
    val = __fma_rn(val,logt,6.832562042987553);
    val = __fma_rn(val,logt,-15.31376741761309);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C3H4-P
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+23*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[23] * 1e3 * avmolwt;
    double val = __fma_rn(0.01235776574228979,logt,-0.4668515977218866);
    val = __fma_rn(val,logt,5.845030731116824);
    val = __fma_rn(val,logt,-12.88439384425461);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C3H6
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+24*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[24] * 1e3 * avmolwt;
    double val = __fma_rn(0.02177863176018352,logt,-0.699541259985777);
    val = __fma_rn(val,logt,7.766464844229805);
    val = __fma_rn(val,logt,-17.88683724042892);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C4H6
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+25*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[25] * 1e3 * avmolwt;
    double val = __fma_rn(0.03601464338482925,logt,-0.9982797264327079);
    val = __fma_rn(val,logt,9.812552574651852);
    val = __fma_rn(val,logt,-22.71705505787219);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C4H7
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+26*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[26] * 1e3 * avmolwt;
    double val = __fma_rn(0.02911546905560454,logt,-0.8594114237096099);
    val = __fma_rn(val,logt,8.917309480458737);
    val = __fma_rn(val,logt,-20.7894720156388);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C4H8-1
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+27*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[27] * 1e3 * avmolwt;
    double val = __fma_rn(0.02972599806531719,logt,-0.8816109789847654);
    val = __fma_rn(val,logt,9.157270473931229);
    val = __fma_rn(val,logt,-21.53293034037972);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species PC4H9
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+28*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[28] * 1e3 * avmolwt;
    double val = __fma_rn(0.02654443353877778,logt,-0.8073085659057427);
    val = __fma_rn(val,logt,8.580304848708984);
    val = __fma_rn(val,logt,-20.2178412015387);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3COCH2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+29*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[29] * 1e3 * avmolwt;
    double val = __fma_rn(0.01720596402832114,logt,-0.5919305814641354);
    val = __fma_rn(val,logt,6.915781505811204);
    val = __fma_rn(val,logt,-16.16919483899415);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H5CHO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+30*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[30] * 1e3 * avmolwt;
    double val = __fma_rn(8.962137724047204e-03,logt,-0.4270827569397477);
    val = __fma_rn(val,logt,5.885090062901472);
    val = __fma_rn(val,logt,-14.0749114789796);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C5H9
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+31*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[31] * 1e3 * avmolwt;
    double val = __fma_rn(0.03293454171541638,logt,-0.9567957219354717);
    val = __fma_rn(val,logt,9.745893291438435);
    val = __fma_rn(val,logt,-23.34943376455083);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C5H10-1
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+32*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[32] * 1e3 * avmolwt;
    double val = __fma_rn(0.02955496800399457,logt,-0.8826375963673944);
    val = __fma_rn(val,logt,9.210030562519322);
    val = __fma_rn(val,logt,-22.03231551299703);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C5H11-1
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+33*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[33] * 1e3 * avmolwt;
    double val = __fma_rn(0.02484318237728951,logt,-0.7826618065450812);
    val = __fma_rn(val,logt,8.520576215751699);
    val = __fma_rn(val,logt,-20.31093477870443);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3O2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+34*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[34] * 1e3 * avmolwt;
    double val = __fma_rn(-0.02157451842437974,logt,0.2785708721737499);
    val = __fma_rn(val,logt,0.4469235979068201);
    val = __fma_rn(val,logt,-0.0465065447484536);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3O2H
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+35*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[35] * 1e3 * avmolwt;
    double val = __fma_rn(4.411342415573788e-03,logt,-0.2961435468532054);
    val = __fma_rn(val,logt,4.638549267197505);
    val = __fma_rn(val,logt,-9.958455556067218);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H3CO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+36*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[36] * 1e3 * avmolwt;
    double val = __fma_rn(0.0227001574847945,logt,-0.6788254277995532);
    val = __fma_rn(val,logt,7.260494334228295);
    val = __fma_rn(val,logt,-16.06549787703782);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H3CHO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+37*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[37] * 1e3 * avmolwt;
    double val = __fma_rn(0.03091830794329992,logt,-0.8913998867556281);
    val = __fma_rn(val,logt,9.083687458454014);
    val = __fma_rn(val,logt,-21.50756921397017);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C3H5-A
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+38*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[38] * 1e3 * avmolwt;
    double val = __fma_rn(0.03303203895197247,logt,-0.9325434582935184);
    val = __fma_rn(val,logt,9.332072983485055);
    val = __fma_rn(val,logt,-21.39523507650279);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C3H3
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+39*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[39] * 1e3 * avmolwt;
    double val = __fma_rn(0.01288346169428678,logt,-0.431315571541328);
    val = __fma_rn(val,logt,5.170265065653203);
    val = __fma_rn(val,logt,-10.18071547045539);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NC3H7CHO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+40*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[40] * 1e3 * avmolwt;
    double val = __fma_rn(9.036569959721172e-03,logt,-0.430636287763054);
    val = __fma_rn(val,logt,5.920315486417818);
    val = __fma_rn(val,logt,-14.15409086170672);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H5COCH2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+41*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[41] * 1e3 * avmolwt;
    double val = __fma_rn(0.01513032666915388,logt,-0.5610458647658541);
    val = __fma_rn(val,logt,6.826517258043592);
    val = __fma_rn(val,logt,-16.40572619315682);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3CHCO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+42*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[42] * 1e3 * avmolwt;
    double val = __fma_rn(0.02091840426432253,logt,-0.6544986646467716);
    val = __fma_rn(val,logt,7.209562107700416);
    val = __fma_rn(val,logt,-16.18896861203852);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NC3H7COCH3
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+43*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[43] * 1e3 * avmolwt;
    double val = __fma_rn(0.01358245778332787,logt,-0.5393195349732909);
    val = __fma_rn(val,logt,6.784538524552068);
    val = __fma_rn(val,logt,-16.58999341197276);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NC3H7COCH2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+44*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[44] * 1e3 * avmolwt;
    double val = __fma_rn(0.01597377179741014,logt,-0.5810178297326815);
    val = __fma_rn(val,logt,6.99068957304659);
    val = __fma_rn(val,logt,-16.71573692561978);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NC7H16
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+45*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[45] * 1e3 * avmolwt;
    double val = __fma_rn(0.0249905828072101,logt,-0.7924835275324114);
    val = __fma_rn(val,logt,8.650516398154048);
    val = __fma_rn(val,logt,-21.02729812724021);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H15-1
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+46*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[46] * 1e3 * avmolwt;
    double val = __fma_rn(0.02359374238705252,logt,-0.7578006518259439);
    val = __fma_rn(val,logt,8.362019603607301);
    val = __fma_rn(val,logt,-20.26573252443184);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H15O2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+47*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[47] * 1e3 * avmolwt;
    double val = __fma_rn(0.01411278754301529,logt,-0.5497049578906817);
    val = __fma_rn(val,logt,6.854237445757703);
    val = __fma_rn(val,logt,-16.77297272809283);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14OOHO2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+48*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[48] * 1e3 * avmolwt;
    double val = __fma_rn(0.01530757369180861,logt,-0.5651461457509733);
    val = __fma_rn(val,logt,6.865547065599182);
    val = __fma_rn(val,logt,-16.80090337327195);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14O
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+49*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[49] * 1e3 * avmolwt;
    double val = __fma_rn(0.0363687733290179,logt,-1.048257707128793);
    val = __fma_rn(val,logt,10.56163157270997);
    val = __fma_rn(val,logt,-25.90041077795992);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NC7KET
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+50*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[50] * 1e3 * avmolwt;
    double val = __fma_rn(0.01233367082263954,logt,-0.5053488987593112);
    val = __fma_rn(val,logt,6.476783803141909);
    val = __fma_rn(val,logt,-15.80321444759775);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species N2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+51*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[51] * 1e3 * avmolwt;
    double val = __fma_rn(-0.0229087459444169,logt,0.4598621935094542);
    val = __fma_rn(val,logt,-2.26326390330196);
    val = __fma_rn(val,logt,10.07453974738464);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Write out the coefficients
  double result = 0.5 * (sum + (1.0/sumr));
  result *= 5.403325855130351e-09;
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(conductivity), "d"(result) : 
    "memory"); 
}

