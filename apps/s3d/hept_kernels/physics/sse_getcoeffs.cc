
#include "sse_getcoeffs.h"
#include <cmath>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <emmintrin.h>
#include <smmintrin.h>

void sse_conductivity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *lambda) 
{
  __m128d *sum = (__m128d*)malloc(358*sizeof(__m128d));
  __m128d *sumr = (__m128d*)malloc(358*sizeof(__m128d));
  __m128d *logt = (__m128d*)malloc(358*sizeof(__m128d));
  __m128d *mixmw = (__m128d*)malloc(358*sizeof(__m128d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 716)
  {
    for (unsigned idx = 0; idx < 358; idx++)
    {
      logt[idx] = _mm_load_pd(temperature_array);
      temperature_array += 2;
      mixmw[idx] = _mm_load_pd(mixmw_array);
      mixmw_array += 2;
      sum[idx] = _mm_set1_pd(0.0);
      sumr[idx] = _mm_set1_pd(0.0);
      logt[idx] = _mm_mul_pd(logt[idx],_mm_set1_pd(120.0));
      double t1 = _mm_cvtsd_f64(logt[idx]);
      double log1 = log(t1);
      double t0 = _mm_cvtsd_f64(_mm_shuffle_pd(logt[idx],logt[idx],1));
      double log0 = log(t0);
      logt[idx] = _mm_set_pd(log0,log1);
    }
    // Species H
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01473938862116963);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.3401776285106699)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(3.261058144879565)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.02274969941640074)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(0*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[0])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species H2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-0.01372974142396394);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.3410718605465255)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-1.977628113931892)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(12.58393663792542)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(1*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[1])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species O
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(6.048679323976329e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.1372174134188909)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(1.680892288317469)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(2.241323411952997)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(2*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[2])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species O2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01382413591952113);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.3174172865521667)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(3.200330657881275)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-2.618427696162442)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(3*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[3])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species OH
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-0.02917655465034293);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.6330047633553172)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-3.771562312017947)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(15.00655081891857)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(4*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[4])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species H2O
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-0.09188748927997951);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(1.865915523816867)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-11.28690040940324)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(28.51814848417955)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(5*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[5])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CO
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-0.01902954473348815);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.3704518381861377)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-1.581395250434219)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.335359673386879)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(6*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[6])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CO2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02555669781704255);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.6736263901574183)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.583392839820579)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-12.9174208914974)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(7*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[7])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-0.03523872382293242);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.6538055500349718)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-2.936246978303837)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(10.20724412614542)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(8*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[8])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH4
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-0.03243163117874146);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.525960502259268)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-1.441415112612421)); 
      val = _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.27997109929273));
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(9*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[9])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species HO2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-0.01035466079073106);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.1416797395812364)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.4305512057109371)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(2.838717758076594)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(10*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[10])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species H2O2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-6.460865164786525e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.05869050570094366)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(1.052528466024186)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(1.506805433923156)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(11*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[11])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH2O
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-0.01598547128586226);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.1308683454894246)); 
      val = _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(1.7163744906578));
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-3.791462650658649)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(12*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[12])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H6
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01965539597409133);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.6576554399563096)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(7.499753566779444)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-17.37128480809956)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(13*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[13])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H4
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02033202000910156);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.6478950279161562)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(7.217979224583608)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-15.90406287185341)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(14*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[14])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H5
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(5.157266296143541e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.332313961259657)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.083567500047072)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-11.5710860164518)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(15*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[15])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02113810970578733);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5302141624530307)); 
      val = _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.26471543892773));
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-8.967530043630704)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(16*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[16])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01399545035506612);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.4178138824809322)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(4.794485714833743)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-8.460850627534054)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(17*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[17])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3OH
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-9.256139406355293e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.03549974985485047)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(3.121378739687247)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-7.465154054656418)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(18*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[18])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH2CO
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(8.267061292353856e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.3557015205333651)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(4.851459603792371)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-10.23797797770669)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(19*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[19])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species HCCO
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(2.110940246916986e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.1289980496470799)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(2.365989847134272)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.858096830636617)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(20*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[20])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3CHO
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(7.700006048688366e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.3959794446319773)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.610243800483931)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-13.12220498163495)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(21*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[21])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C3H4-A
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01830268033858104);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5997479909300256)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.832562042987553)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-15.31376741761309)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(22*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[22])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C3H4-P
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01235776574228979);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.4668515977218866)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.845030731116824)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-12.88439384425461)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(23*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[23])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C3H6
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02177863176018352);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.699541259985777)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(7.766464844229805)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-17.88683724042892)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(24*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[24])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C4H6
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.03601464338482925);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.9982797264327079)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.812552574651852)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-22.71705505787219)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(25*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[25])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C4H7
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02911546905560454);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.8594114237096099)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.917309480458737)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-20.7894720156388)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(26*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[26])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C4H8-1
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02972599806531719);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.8816109789847654)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.157270473931229)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-21.53293034037972)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(27*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[27])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species PC4H9
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02654443353877778);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.8073085659057427)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.580304848708984)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-20.2178412015387)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(28*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[28])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3COCH2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01720596402832114);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5919305814641354)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.915781505811204)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.16919483899415)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(29*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[29])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H5CHO
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(8.962137724047204e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.4270827569397477)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.885090062901472)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-14.0749114789796)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(30*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[30])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C5H9
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.03293454171541638);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.9567957219354717)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.745893291438435)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-23.34943376455083)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(31*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[31])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C5H10-1
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02955496800399457);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.8826375963673944)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.210030562519322)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-22.03231551299703)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(32*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[32])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C5H11-1
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02484318237728951);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.7826618065450812)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.520576215751699)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-20.31093477870443)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(33*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[33])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3O2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-0.02157451842437974);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.2785708721737499)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.4469235979068201)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.0465065447484536)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(34*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[34])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3O2H
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(4.411342415573788e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.2961435468532054)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(4.638549267197505)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-9.958455556067218)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(35*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[35])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H3CO
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.0227001574847945);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.6788254277995532)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(7.260494334228295)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.06549787703782)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(36*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[36])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H3CHO
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.03091830794329992);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.8913998867556281)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.083687458454014)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-21.50756921397017)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(37*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[37])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C3H5-A
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.03303203895197247);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.9325434582935184)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.332072983485055)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-21.39523507650279)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(38*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[38])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C3H3
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01288346169428678);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.431315571541328)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.170265065653203)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-10.18071547045539)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(39*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[39])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species NC3H7CHO
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(9.036569959721172e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.430636287763054)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.920315486417818)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-14.15409086170672)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(40*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[40])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H5COCH2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01513032666915388);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5610458647658541)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.826517258043592)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.40572619315682)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(41*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[41])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3CHCO
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02091840426432253);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.6544986646467716)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(7.209562107700416)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.18896861203852)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(42*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[42])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species NC3H7COCH3
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01358245778332787);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5393195349732909)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.784538524552068)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.58999341197276)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(43*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[43])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species NC3H7COCH2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01597377179741014);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5810178297326815)); 
      val = _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.99068957304659));
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.71573692561978)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(44*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[44])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species NC7H16
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.0249905828072101);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.7924835275324114)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.650516398154048)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-21.02729812724021)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(45*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[45])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C7H15-1
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.02359374238705252);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.7578006518259439)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.362019603607301)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-20.26573252443184)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(46*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[46])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C7H15O2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01411278754301529);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5497049578906817)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.854237445757703)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.77297272809283)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(47*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[47])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C7H14OOHO2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01530757369180861);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5651461457509733)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.865547065599182)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.80090337327195)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(48*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[48])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C7H14O
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.0363687733290179);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-1.048257707128793)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(10.56163157270997)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-25.90041077795992)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(49*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[49])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species NC7KET
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(0.01233367082263954);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5053488987593112)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.476783803141909)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-15.80321444759775)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(50*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[50])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species N2
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d val = _mm_set1_pd(-0.0229087459444169);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.4598621935094542)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-2.26326390330196)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(10.07453974738464)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(51*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[51])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    for (unsigned idx = 0; idx < 358; idx++)
    {
      __m128d result = 
        _mm_add_pd(sum[idx],_mm_div_pd(_mm_set1_pd(1.0),sumr[idx])); 
      result = _mm_mul_pd(result,_mm_set1_pd(0.5));
      result = _mm_mul_pd(result,_mm_set1_pd(5.403325855130351e-09));
      _mm_store_pd(lambda,result);
      lambda += 2;
    }
    remaining_elmts -= 716;
    mass_frac_array += 716;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 2) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      logt[idx] = _mm_load_pd(temperature_array);
      temperature_array += 2;
      mixmw[idx] = _mm_load_pd(mixmw_array);
      mixmw_array += 2;
      sum[idx] = _mm_set1_pd(0.0);
      sumr[idx] = _mm_set1_pd(0.0);
      logt[idx] = _mm_mul_pd(logt[idx],_mm_set1_pd(120.0));
      double t1 = _mm_cvtsd_f64(logt[idx]);
      double log1 = log(t1);
      double t0 = _mm_cvtsd_f64(_mm_shuffle_pd(logt[idx],logt[idx],1));
      double log0 = log(t0);
      logt[idx] = _mm_set_pd(log0,log1);
    }
    // Species H
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01473938862116963);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.3401776285106699)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(3.261058144879565)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.02274969941640074)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(0*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[0])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species H2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-0.01372974142396394);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.3410718605465255)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-1.977628113931892)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(12.58393663792542)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(1*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[1])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species O
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(6.048679323976329e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.1372174134188909)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(1.680892288317469)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(2.241323411952997)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(2*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[2])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species O2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01382413591952113);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.3174172865521667)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(3.200330657881275)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-2.618427696162442)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(3*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[3])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species OH
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-0.02917655465034293);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.6330047633553172)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-3.771562312017947)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(15.00655081891857)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(4*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[4])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species H2O
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-0.09188748927997951);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(1.865915523816867)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-11.28690040940324)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(28.51814848417955)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(5*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[5])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CO
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-0.01902954473348815);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.3704518381861377)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-1.581395250434219)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.335359673386879)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(6*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[6])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CO2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02555669781704255);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.6736263901574183)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.583392839820579)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-12.9174208914974)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(7*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[7])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-0.03523872382293242);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.6538055500349718)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-2.936246978303837)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(10.20724412614542)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(8*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[8])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH4
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-0.03243163117874146);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.525960502259268)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-1.441415112612421)); 
      val = _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.27997109929273));
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(9*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[9])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species HO2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-0.01035466079073106);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.1416797395812364)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.4305512057109371)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(2.838717758076594)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(10*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[10])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species H2O2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-6.460865164786525e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.05869050570094366)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(1.052528466024186)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(1.506805433923156)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(11*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[11])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH2O
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-0.01598547128586226);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.1308683454894246)); 
      val = _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(1.7163744906578));
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-3.791462650658649)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(12*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[12])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H6
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01965539597409133);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.6576554399563096)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(7.499753566779444)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-17.37128480809956)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(13*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[13])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H4
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02033202000910156);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.6478950279161562)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(7.217979224583608)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-15.90406287185341)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(14*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[14])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H5
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(5.157266296143541e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.332313961259657)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.083567500047072)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-11.5710860164518)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(15*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[15])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02113810970578733);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5302141624530307)); 
      val = _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.26471543892773));
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-8.967530043630704)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(16*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[16])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01399545035506612);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.4178138824809322)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(4.794485714833743)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-8.460850627534054)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(17*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[17])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3OH
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-9.256139406355293e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.03549974985485047)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(3.121378739687247)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-7.465154054656418)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(18*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[18])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH2CO
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(8.267061292353856e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.3557015205333651)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(4.851459603792371)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-10.23797797770669)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(19*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[19])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species HCCO
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(2.110940246916986e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.1289980496470799)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(2.365989847134272)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.858096830636617)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(20*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[20])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3CHO
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(7.700006048688366e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.3959794446319773)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.610243800483931)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-13.12220498163495)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(21*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[21])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C3H4-A
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01830268033858104);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5997479909300256)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.832562042987553)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-15.31376741761309)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(22*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[22])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C3H4-P
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01235776574228979);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.4668515977218866)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.845030731116824)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-12.88439384425461)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(23*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[23])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C3H6
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02177863176018352);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.699541259985777)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(7.766464844229805)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-17.88683724042892)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(24*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[24])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C4H6
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.03601464338482925);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.9982797264327079)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.812552574651852)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-22.71705505787219)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(25*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[25])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C4H7
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02911546905560454);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.8594114237096099)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.917309480458737)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-20.7894720156388)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(26*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[26])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C4H8-1
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02972599806531719);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.8816109789847654)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.157270473931229)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-21.53293034037972)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(27*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[27])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species PC4H9
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02654443353877778);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.8073085659057427)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.580304848708984)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-20.2178412015387)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(28*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[28])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3COCH2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01720596402832114);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5919305814641354)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.915781505811204)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.16919483899415)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(29*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[29])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H5CHO
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(8.962137724047204e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.4270827569397477)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.885090062901472)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-14.0749114789796)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(30*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[30])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C5H9
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.03293454171541638);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.9567957219354717)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.745893291438435)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-23.34943376455083)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(31*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[31])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C5H10-1
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02955496800399457);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.8826375963673944)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.210030562519322)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-22.03231551299703)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(32*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[32])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C5H11-1
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02484318237728951);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.7826618065450812)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.520576215751699)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-20.31093477870443)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(33*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[33])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3O2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-0.02157451842437974);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.2785708721737499)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.4469235979068201)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.0465065447484536)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(34*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[34])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3O2H
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(4.411342415573788e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.2961435468532054)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(4.638549267197505)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-9.958455556067218)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(35*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[35])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H3CO
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.0227001574847945);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.6788254277995532)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(7.260494334228295)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.06549787703782)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(36*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[36])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H3CHO
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.03091830794329992);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.8913998867556281)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.083687458454014)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-21.50756921397017)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(37*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[37])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C3H5-A
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.03303203895197247);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.9325434582935184)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(9.332072983485055)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-21.39523507650279)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(38*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[38])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C3H3
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01288346169428678);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.431315571541328)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.170265065653203)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-10.18071547045539)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(39*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[39])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species NC3H7CHO
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(9.036569959721172e-03);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.430636287763054)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(5.920315486417818)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-14.15409086170672)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(40*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[40])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C2H5COCH2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01513032666915388);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5610458647658541)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.826517258043592)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.40572619315682)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(41*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[41])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species CH3CHCO
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02091840426432253);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.6544986646467716)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(7.209562107700416)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.18896861203852)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(42*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[42])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species NC3H7COCH3
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01358245778332787);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5393195349732909)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.784538524552068)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.58999341197276)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(43*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[43])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species NC3H7COCH2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01597377179741014);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5810178297326815)); 
      val = _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.99068957304659));
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.71573692561978)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(44*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[44])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species NC7H16
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.0249905828072101);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.7924835275324114)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.650516398154048)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-21.02729812724021)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(45*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[45])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C7H15-1
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.02359374238705252);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.7578006518259439)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(8.362019603607301)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-20.26573252443184)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(46*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[46])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C7H15O2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01411278754301529);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5497049578906817)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.854237445757703)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.77297272809283)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(47*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[47])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C7H14OOHO2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01530757369180861);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5651461457509733)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.865547065599182)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-16.80090337327195)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(48*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[48])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species C7H14O
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.0363687733290179);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-1.048257707128793)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(10.56163157270997)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-25.90041077795992)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(49*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[49])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species NC7KET
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(0.01233367082263954);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-0.5053488987593112)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(6.476783803141909)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-15.80321444759775)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(50*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[50])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    // Species N2
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d val = _mm_set1_pd(-0.0229087459444169);
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(0.4598621935094542)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(-2.26326390330196)); 
      val = 
        _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(10.07453974738464)); 
      val = 
        _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
      __m128d frac = _mm_load_pd(mass_frac_array+(51*spec_stride)+(idx<<1));
      __m128d mole = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,_mm_set1_pd(recip_molecular_masses[51])),_mm_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);
    }
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d result = 
        _mm_add_pd(sum[idx],_mm_div_pd(_mm_set1_pd(1.0),sumr[idx])); 
      result = _mm_mul_pd(result,_mm_set1_pd(0.5));
      result = _mm_mul_pd(result,_mm_set1_pd(5.403325855130351e-09));
      _mm_store_pd(lambda,result);
      lambda += 2;
    }
  }
  free(sum);
  free(sumr);
  free(logt);
  free(mixmw);
}

const double visc_constants[5304] = {1.414213562373095, 
  0.8164965809277261,3.98407994930469, 0.9699140894422974,5.634339897885405, 
  0.9846124717471159,4.107662722577361, 0.971622019642798,4.227634449955632, 
  0.9731464190357054,5.271534037005418, 0.9824788020819667,6.607720041418092, 
  0.9887414136306164,3.862146820209582, 0.9680758473154983,3.989508498656819, 
  0.9699922639292418,5.722393387814518, 0.9850719113086823,5.809112331923296, 
  0.9855047090477966,5.457936524301712, 0.9836264244359701,5.461900413016515, 
  0.9836496020649622,5.275637982434918, 0.9825053402396818,5.369576903417063, 
  0.9830968156469072,4.983207413073428, 0.9804533942177576,5.082554094322063, 
  0.9811888513529012,5.638179768622729, 0.9846329434151064,6.457960139561225, 
  0.9882224824658319,6.380066548568411, 0.9879383598985628,6.61099456694389, 
  0.9887523765479622,6.304643858503425, 0.9876534007151484,6.304643858503425, 
  0.9876534007151484,6.461310562305836, 0.9882344787779606,7.325620263392032, 
  0.9908111137125415,7.393558834784501, 0.9909769409092173,7.460878784930093, 
  0.9911368888864747,7.527596710997472, 0.9912912648660567,7.524721072904737, 
  0.9912846941359229,7.590877895541241, 0.9914340074373406,8.281357998799166, 
  0.9927881133107064,8.341516067494862, 0.9928906581700099,8.40124337846934, 
  0.9929903277330832,6.830956312681139, 0.9894538438686602,6.903764490896131, 
  0.9896716976419159,7.390631043761867, 0.990969887103225,7.457977421862888, 
  0.9911300830429209,6.383457854686984, 0.9879509417063109,6.22483206059127, 
  0.9873407920092888,8.457990617509079, 0.9930831018787741,8.398666875514923, 
  0.9929860714350582,7.457977421862888, 0.9911300830429209,9.244121556250036, 
  0.9941997416100037,9.189873957064187, 0.9941316653123009,9.970619159610145, 
  0.9950081178426623,9.920345075953, 0.9949577771598376,11.40872615636331, 
  0.9961805398952744,12.72418243329573, 0.9969259983361046,10.6435961718038, 
  0.9956154039351396,12.04292014228027, 0.9965702094339812,5.271802212565431, 
  0.9824805380660118,0.7071067811865476, 0.5773502691896258,2.817169948942702, 
  0.9423898864639306,3.98407994930469, 0.9699140894422974,2.904556165961648, 
  0.9455304890716288,2.989388987941487, 0.9483461395154609,3.727537464822228, 
  0.9658474431744803,4.672363649468988, 0.9778547280897629,2.730950206508258, 
  0.9390260827927792,2.821008513001599, 0.9425333143890049,4.046343169140707, 
  0.9707930073454198,4.107662722577361, 0.971622019642798,3.859343927619476, 
  0.9680316606213769,3.862146820209582, 0.9680758473154983,3.730439392465047, 
  0.9658978713457121,3.796864240508869, 0.9670227915024118,3.523659753843294, 
  0.9620099396775748,3.593908465842583, 0.9634008457828757,3.986795147941931, 
  0.9699532275455067,4.566467407316165, 0.9768515272803485,4.511388320914175, 
  0.9763028985421802,4.674679088673448, 0.9778759379539206,4.458056425313893, 
  0.9757531073159714,4.458056425313893, 0.9757531073159714,4.568836513958721, 
  0.9768747015890779,5.179995764642088, 0.9818709664857089,5.228035589177829, 
  0.9821938021709975,5.275637982434918, 0.9825053402396818,5.322814680383864, 
  0.9828061635773339,5.320781297188253, 0.9827933568184763,5.36756123509628, 
  0.9830844383977753,5.855804398384347, 0.9857300432484298,5.898342576702159, 
  0.985930820059355,5.940576163314251, 0.9861260251680428,4.830215530685888, 
  0.979234465399816,4.881698687227547, 0.9796569453538764,5.225965328291828, 
  0.9821800664976531,5.273586408935413, 0.9824920812181137,4.513786336467698, 
  0.9763271839353315,4.401620961791517, 0.9751504908927621,5.980702520852864, 
  0.9863077768169746,5.938754300603436, 0.9861176879431457,5.273586408935413, 
  0.9824920812181137,6.536581038537141, 0.9884992553568341,6.498222193289738, 
  0.9883654447798123,7.050292420388849, 0.9900902536739494,7.014743274916941, 
  0.989991065184729,8.067187629864829, 0.9924045129394649,8.997355683638153, 
  0.9938801719532785,7.526159029293646, 0.9912879807399481,8.515630497894442, 
  0.9931754688976696,3.727727093579261, 0.9658507417986416,0.2509989791180074, 
  0.2434474462821885,0.3549661604104874, 0.3345165196077766,1.414213562373095, 
  0.8164965809277261,1.031019149928013, 0.7178224991180043,1.061131931022983, 
  0.7277591150584508,1.323149661674189, 0.7977841076744662,1.658530984693539, 
  0.8563789353178104,0.9693949090764638, 0.6960340506883298,1.001362560345476, 
  0.7075880270542277,1.43631489845308, 0.8206843133241595,1.458081264894575, 
  0.8246832525414386,1.369936495690615, 0.8077020519103046,1.370931427711368, 
  0.8079058204185576,1.324179747787349, 0.7980097289466876,1.347758321053314, 
  0.8030836919542321,1.250779973414717, 0.7810588455682465,1.275715888986887, 
  0.7870215957491786,1.415177366008108, 0.8166819386810913,1.620941402214652, 
  0.8510724575791483,1.60139019039562, 0.8482050763144294,1.65935288725761, 
  0.8564920218216583,1.582459172186975, 0.8453557180290607,1.582459172186975, 
  0.8453557180290607,1.621782354903164, 0.8511941110396607,1.838723207517589, 
  0.8784853779507377,1.855775719579835, 0.88032528785483,1.872672958340653, 
  0.8821100805533123,1.889419089672436, 0.8838422066790383,1.888697307446847, 
  0.8837682904285943,1.9053026023903, 0.8854521907191043,2.078612403409336, 
  0.9011394538695873,2.093712017237666, 0.9023589766466329,2.108703511317724, 
  0.9035486979755,1.714563060882675, 0.8638143742617245,1.732837839286079, 
  0.8661237324794798,1.855040847022082, 0.8802468067616825,1.871944719172734, 
  0.8820339333913941,1.602241404769259, 0.8483314920034378,1.562426492389452, 
  0.8422598860276483,2.122947010384464, 0.9046600266104295,2.108056811706471, 
  0.9034978031236266,1.871944719172734, 0.8820339333913941,2.320265073461525, 
  0.9183405235519709,2.306648981446275, 0.9174900060026253,2.502615230236592, 
  0.9286104784433915,2.489996486562555, 0.9279615846508632,2.863578618284099, 
  0.9440896793050675,3.193756800868511, 0.9543138600162182,2.671531773267087, 
  0.9365392798441519,3.022760661312037, 0.9493957461410379,1.323216973465976, 
  0.7977988613181562,0.1774830802052437, 0.1747520542941766,0.2509989791180074, 
  0.2434474462821885,0.7071067811865476, 0.5773502691896258,0.7290406324472875, 
  0.5891056647772915,0.7503335841599268, 0.6001707540730823,0.9356080982945049, 
  0.6832055047437288,1.172758506084803, 0.7609281352078161,0.6854657138556842, 
  0.565388891894947,0.7080702568466096, 0.5778743617221752,1.01562800461544, 
  0.7125678571310604,1.031019149928013, 0.7178224991180043,0.9686913858977697, 
  0.6957734972999603,0.9693949090764638, 0.6960340506883298,0.9363364791703267, 
  0.683488966178902,0.9530090482173947, 0.6898935610111014,0.8844350009738763, 
  0.6624980777112744,0.9020673559700526, 0.6698124036621164,1.00068151208605, 
  0.7073476089790444,1.146178657412012, 0.7535228030710742,1.13235386295436, 
  0.749553973920673,1.173339678961333, 0.761086815717388,1.118967611604261, 
  0.7456324290880788,1.118967611604261, 0.7456324290880788,1.146773300760715, 
  0.753691690347268,1.300173648760767, 0.7926633430792437,1.312231595676246, 
  0.795371963611013,1.324179747787349, 0.7980097289466876,1.336021050810693, 
  0.8005794251847408,1.335510673704439, 0.8004695691113756,1.347252390362558, 
  0.8029766152212672,1.469800925909209, 0.8267864429400101,1.48047796524052, 
  0.8286728150454894,1.491078552364646, 0.8305181577974947,1.212379167122102, 
  0.7714397472242659,1.225301386855831, 0.77473740019655,1.311711962307351, 
  0.7952562086521223,1.323664804933388, 0.7978969821760902,1.132955762410203, 
  0.7497284724046169,1.104802367874093, 0.7413965751392962,1.501150227142562, 
  0.8322464554054902,1.490621266684139, 0.830439113372568,1.323664804933388, 
  0.7978969821760902,1.640675167594947, 0.8538913999619056,1.631047136597704, 
  0.8525253378439477,1.769616200001027, 0.870608746443673,1.76069340077906, 
  0.8695401169235576,2.02485585944949, 0.8966176189449917,2.258327091354778, 
  0.9143668204706097,1.889058233032479, 0.8838052603567698,2.137414561517674, 
  0.9057702509699829,0.9356556949189316, 0.6832240371937915,0.2434474462821884, 
  0.2365388994335815,0.3442866802573664, 0.3255335531645952,0.9699140894422975, 
  0.6962261556132335,1.371665659626048, 0.8080560102661852,1.029206810656303, 
  0.7172100349425605,1.283341499298605, 0.7888021863940915,1.608632569830871, 
  0.8492760427787966,0.9402297805468971, 0.6849988788408234,0.9712356559190902, 
  0.6967144818036923,1.393102056885525, 0.8123723078052557,1.414213562373095, 
  0.8164965809277261,1.328720708811535, 0.7990003558022288,1.329685707396501, 
  0.7992100394194569,1.284340594333098, 0.7890340160044326,1.307209784752705, 
  0.7942496393960862,1.213149119007197, 0.7716379948749582,1.237334814853787, 
  0.7777524351054738,1.372600466351103, 0.8082470006880066,1.572173904168345, 
  0.8437767669571917,1.553210908359396, 0.8408071278955829,1.609429744707912, 
  0.8493932884565886,1.534849447071342, 0.837857483010951,1.534849447071342, 
  0.837857483010951,1.572989556029487, 0.8439027886151153,1.783403545555843, 
  0.8722360788887579,1.7999430172654, 0.8741507491719046,1.816331887212188, 
  0.8760086149944187,1.832574195934535, 0.8778121793406822,1.831874129184427, 
  0.8777352040489701,1.847979838709428, 0.8794890257165072,2.016075456556231, 
  0.8958512659539903,2.030720784753467, 0.8971250614215497,2.045261246083506, 
  0.8983679815497063,1.662978869987418, 0.8569894563676924,1.680703835042316, 
  0.8593870224591206,1.799230254017691, 0.8740690673940833,1.81562555778274, 
  0.8759293390831928,1.554036513173523, 0.8409380235764988,1.515419468686438, 
  0.8346541841683938,2.059076216511295, 0.8995292291483684,2.044634003018915, 
  0.8983148058082256,1.81562555778274, 0.8759293390831928,2.250457785991201, 
  0.9138422087650162,2.237251346502466, 0.9129513940024911,2.427321772259349, 
  0.9246090750765665,2.41508267497884, 0.9239282174793702,2.777425248099454, 
  0.9408737096986002,3.097669719414527, 0.9516411216349832,2.591156307284513, 
  0.9329346699997051,2.931818154418462, 0.9464592483967553,1.283406785953845, 
  0.7888173457727758,0.2365388994335815, 0.2301869829464367,0.3345165196077766, 
  0.3172374499741829,0.9423898864639306, 0.6858328298130242,1.332740558480532, 
  0.7998719059670416,0.9716220196427982, 0.6968570626589725,1.246922859439927, 
  0.7801169616843492,1.562982826362255, 0.8423470045732541,0.9135479583032811, 
  0.6744724623090879,0.9436739495532042, 0.6863272906138056,1.35356863407965, 
  0.8043078475514793,1.374081037679183, 0.8085489752128718,1.291014298636674, 
  0.7905739733009424,1.291951912510751, 0.7907891339056414,1.247893602175156, 
  0.7803545121900962,1.270113811158251, 0.7857008447863071,1.178722397137654, 
  0.762550014860288,1.202221751782585, 0.7688032478102023,1.333648837278706, 
  0.8000681333739995,1.527558783997751, 0.8366655396199205,1.509133919711381, 
  0.8335994352681999,1.563757379026295, 0.8424681849823727,1.491293519611089, 
  0.8305552987760048,1.491293519611089, 0.8305552987760048,1.528351289306398, 
  0.8367956846189182,1.732794154771095, 0.8661182773396673,1.748864269677359, 
  0.8681043374972893,1.764788056594721, 0.8700320324583799,1.78056944139919, 
  0.871903911392149,1.779889241129565, 0.8718240098532887,1.795537903146027, 
  0.873644738972245,1.958863306851442, 0.8906550971324519,1.973093030212772, 
  0.8919811764797512,1.987220862616802, 0.8932753699567257,1.615786888280473, 
  0.8503237927430193,1.633008854625211, 0.8528051004758449,1.748171733211094, 
  0.868019598261674,1.764101771367948, 0.8699497656787508,1.509936095528311, 
  0.8337345547208851,1.472414924771133, 0.8272508708669416,2.000643792085156, 
  0.8944847512790048,1.986611419443577, 0.8932199953930448,1.764101771367948, 
  0.8699497656787508,2.186594339145631, 0.9094093553322429,2.173762671737296, 
  0.9084794768322371,2.358439282685565, 0.9206589563045536,2.346547506267272, 
  0.9199469900799941,2.698607528965291, 0.9376903826329444,3.009764108963882, 
  0.9489907147397737,2.517624524493953, 0.9293713632473257,2.848619076421487, 
  0.9435498310923747,1.246986293391747, 0.7801324949225127,0.1896981017252554, 
  0.186374363740252,0.2682736282162872, 0.2591113978838421,0.7557724035047513, 
  0.6029432125350246,1.06882358310373, 0.7302261555763849,0.7792158210005197, 
  0.6146471432780769,0.8019742299448775, 0.6256336996137434,1.253471948588938, 
  0.7817130668172513,0.7326419203779889, 0.5910009098591849,0.756802189011972, 
  0.6034657052367048,1.085527162993567, 0.7354869124545533,1.101977582074621, 
  0.7405411157484487,1.035360197996973, 0.7192827689648849,1.036112140161621, 
  0.7195347479143021,1.00077851065756, 0.7073818656207655,1.018598545645992, 
  0.713591487497392,0.9453049867632499, 0.6869539932097527,0.96415086360882, 
  0.694085265401827,1.069551999293471, 0.7304583206841164,1.22506277949213, 
  0.7746770755510691,1.210286513144229, 0.7708997923317239,1.254093119865233, 
  0.7818636624477916,1.195978972011889, 0.7671631435394782,1.195978972011889, 
  0.7671631435394782,1.225698348326759, 0.7748377130260997,1.389656257925534, 
  0.8116873393483847,1.402544075952611, 0.8142321363703868,1.415314542703469, 
  0.8167082998371638,1.427970806629496, 0.8191186022971582,1.427425303542056, 
  0.8190156000773022,1.439975127212375, 0.8213653497239776,1.570957892079462, 
  0.8435886284686901,1.582369763514492, 0.8453420870549838,1.593699921027505, 
  0.8470563755439713,1.295819445483762, 0.7916735536405574,1.30963101868122, 
  0.7947917790027593,1.401988679553369, 0.8141234264278278,1.414764159637204, 
  0.8166025021786087,1.210929837477292, 0.7710659626384883,1.180838825452674, 
  0.7631221439795041,1.604464764551492, 0.848661004883292,1.593211163307962, 
  0.8469829657028576,1.414764159637204, 0.8166025021786087,1.753592311338145, 
  0.8686808421476614,1.743301644749437, 0.8674215421983185,1.891407527603505, 
  0.8840454966752354,1.881870629367768, 0.8830658989942288,2.164213694965388, 
  0.9077786155669408,2.41375325360204, 0.9238537292702547,2.019069989321376, 
  0.8961135309741901,2.28451909021941, 0.9160803209721865,1.000050872394664, 
  0.7071247666079385,0.1513381308124229, 0.1496342773956902,0.2140244370991221, 
  0.2092848077441266,0.6029432125350246, 0.5163478664078446,0.8526904685078353, 
  0.6488361681111461,0.6216459984427261, 0.5279490535667124,0.6398022954144912, 
  0.5389355470614888,0.7977841076744662, 0.6236382614682711,0.5844900806936612, 
  0.5046159603103175,0.6037647590469988, 0.5168635619782184,0.8660163190852177, 
  0.6546497464603095,0.8791402019926672, 0.6602638986159685,0.8259939116806735, 
  0.6368388066661637,0.8265937991895203, 0.6371136188392371,0.7984051911047227, 
  0.6239348093432265,0.8126217318166965, 0.6306495289699677,0.7541492953451423, 
  0.6021181018474171,0.7691842363877284, 0.6096875881905095,0.8532715873677831, 
  0.649092090923767,0.9773356163823298, 0.6989559523737043,0.96554734591921, 
  0.6946057219064908,1.000495560572372, 0.7072819232039818,0.9541330169839304, 
  0.6903195945735253,0.9541330169839304, 0.6903195945735253,0.9778426630979308, 
  0.6991413486970811,1.10864567770336, 0.7425547080936934,1.118927374107962, 
  0.7456205230905057,1.129115449529382, 0.7486124273800557,1.1392124157521, 
  0.7515331313384815,1.138777222058253, 0.7514081476248194,1.14878927193655, 
  0.7542631459051303,1.253285240126773, 0.781667774811308,1.262389449796465, 
  0.7838619557528989,1.271428469397795, 0.7860117589142108,1.033784160022485, 
  0.7187537096085959,1.044802813621399, 0.7224265120939624,1.118484287687187, 
  0.7454893696371657,1.128676362665983, 0.7484844160313333,0.9660605798482077, 
  0.6947967259876687,0.942054448671126, 0.6857035043555409,1.280016490482835, 
  0.7880282215663148,1.271038546256641, 0.7859196050006031,1.128676362665983, 
  0.7484844160313333,1.398988077325707, 0.8135346230313567,1.390778347063859, 
  0.8119107608836386,1.508934866657931, 0.8335658833427739,1.501326480808951, 
  0.8322764863213721,1.726575291454821, 0.8653385230228594,1.925653985571243, 
  0.8874694617875741,1.610781949763048, 0.8495918788288783,1.822553023855975, 
  0.8767037980361789,0.7978246928624479, 0.6236576478512198,0.2589233518434015, 
  0.2506574432255698,0.3661729157920391, 0.3438459187410087,1.031571334486059, 
  0.7180087745162979,1.458862171785498, 0.8248244667332583,1.063569800371923, 
  0.7285441208237253,1.094633282151147, 0.7383000051379629,1.364923262218021, 
  0.8066708898588171,1.710893021166792, 0.8633439248643011,1.03297691267996, 
  0.7184822406842919,1.481661276539453, 0.8288801559439831,1.504114836216418, 
  0.8327506059785745,1.413187219020724, 0.8162989176295664,1.414213562373095, 
  0.8164965809277261,1.365985869524409, 0.8068900730770983,1.390308849813658, 
  0.8118173216724673,1.290268766323858, 0.7904026817236154,1.315991942027273, 
  0.7962070417039337,1.459856403987451, 0.8250040341995759,1.672116685406273, 
  0.8582324948648609,1.651948215739295, 0.8554687400722604,1.711740872291629, 
  0.8634528083374381,1.632419520022623, 0.8527211319184945,1.632419520022623, 
  0.8527211319184945,1.6729841880934, 0.8583497244035616,1.896774152929407, 
  0.8845916653348738,1.914365035553797, 0.8863567216031207,1.931795742691425, 
  0.8880684023570596,1.94907057173683, 0.8897291017960881,1.948326001883171, 
  0.8896582430970829,1.965455548147524, 0.8912722781793775,2.144236970864244, 
  0.9062874074721147,2.159813299651359, 0.9074530611737743,2.175278095205464, 
  0.9085900038054472,1.768694104775243, 0.8704988800344307,1.787545842320281, 
  0.8727194094832724,1.91360696208872, 0.8862814440735566,1.931044512041149, 
  0.8879953845538896,1.652826304086643, 0.8555906132104245,1.61175438179056, 
  0.8497344711816858,2.189971280545492, 0.9096518295761703,2.174610978424472, 
  0.9085413713363067,1.931044512041149, 0.8879953845538896,2.3935189381921, 
  0.9227069016437633,2.379472967981443, 0.9218961352078825,2.581626132760298, 
  0.9324877161666871,2.568608998508934, 0.9318702295340456,2.953985616669073, 
  0.9471975173771988,3.294587965095858, 0.9568921022118918,2.755875596471038, 
  0.9400272657643077,3.118193249221622, 0.9522308749427114,1.364992699132902, 
  0.8066852227333896,0.2506574432255699, 0.2431357808250859,0.354483155719365, 
  0.3341121836552468,0.9986392936989714, 0.7066252075672828,1.412289233067774, 
  0.8161257391261402,1.029616235674224, 0.7173485420944357,1.059688042118217, 
  0.7272928228428442,1.321349243592345, 0.7973889531484409,1.656274211132215, 
  0.8560677883784604,0.9680758473154985, 0.6955453039315838,1.434360495720494, 
  0.8203192310747778,1.456097244530021, 0.8243237801367711,1.36807241446892, 
  0.8073194954177303,1.369065992679404, 0.8075235295471489,1.32237792806084, 
  0.7976148538422746,1.345924417813594, 0.8026951886966347,1.249078029223692, 
  0.7806438965387017,1.27398001433842, 0.7866134966445973,1.413251725249108, 
  0.8163113493259505,1.618735777035061, 0.8507527361696235,1.599211168673144, 
  0.8478808033884424,1.657094995328289, 0.8561810597575654,1.58030591002026, 
  0.8450269787572346,1.58030591002026, 0.8450269787572346,1.61957558543395, 
  0.8508745840014376,1.836221245263272, 0.8782120904781852,1.853250553864906, 
  0.8800553086859348,1.870124800446473, 0.8818433339059323,1.886848145211835, 
  0.8835786191729667,1.886127345119868, 0.8835045676668715,1.902710045133862, 
  0.8851915590614996,2.07578402241462, 0.9009086125207364,2.090863090103272, 
  0.9021305268850823,2.105834185162873, 0.9033225919532178,1.712230044122221, 
  0.863515574332011,1.730479955919502, 0.8658288468194811,1.852516681252874, 
  0.8799766859795527,1.869397552198179, 0.8817670483606308,1.600061224794021, 
  0.8480074185378512,1.560300488816363, 0.8419263577514636,2.120058303010684, 
  0.9044361193595573,2.105188365519856, 0.9032715966245546,1.869397552198179, 
  0.8817670483606308,2.31710787415601, 0.9181444367123647,2.303510309642959, 
  0.917292148726268,2.499209905923759, 0.9284362007428457,2.48660833265375, 
  0.9277859053060608,2.859682128814709, 0.943949792575802,3.189411035865616, 
  0.9541977038596511,2.667896603149804, 0.9363823882475032,3.018647571833689, 
  0.9492680733857662,1.321416463792553, 0.7974037250536872,0.1747520542941766, 
  0.1721433401286832,0.247136725235384, 0.2399186047167572,0.6962261556132336, 
  0.5713818844377662,0.984612471747116, 0.7016031990973591,0.7178224991180044, 
  0.5831391202030293,0.7387878049345782, 0.5942128291842127,0.9212114022483711, 
  0.6775389299575841,1.154712651438613, 0.7559323444988001,0.6749180908173465, 
  0.5594262123660976,0.6971748057643555, 0.5719059005893237,1.015154313629231, 
  0.7124042040882749,0.9537856198289424, 0.6901879923821975,0.9544783175248478, 
  0.6904503346529252,0.9219285751428873, 0.6778241078903778,0.9383445945626954, 
  0.6842688588135148,0.8708257324085513, 0.6567198446596768,0.888186769044059, 
  0.6640705375867318,0.9852834970466874, 0.701845859579285,1.128541800938232, 
  0.7484451691579944,1.114929735895887, 0.744434082212205,1.155284881501085, 
  0.7560928216566766,1.101749465866638, 0.7404718753833931,1.101749465866638, 
  0.7404718753833931,1.129127294195606, 0.7486158794138996,1.280167190006805, 
  0.7880633810811588,1.29203959492345, 0.790809240007382,1.303803894506374, 
  0.7934838007841263,1.315462989144896, 0.7960898487906132,1.314960465480784, 
  0.7959784284179124,1.326521506142088, 0.798521401934506,1.447184322635666, 
  0.8226965206008835,1.457697068722609, 0.8246137203176986,1.468134539012866, 
  0.8264894915447631,1.193723648434803, 0.7665668986132887,1.206447027147289, 
  0.7699047735463662,1.291527957427701, 0.790691882347044,1.303296875350127, 
  0.7933694705066887,1.115522373606848, 0.7446104152348048,1.087802190224577, 
  0.7361933130836336,1.478051235610583, 0.8282465316209139,1.467684289828687, 
  0.8264091382188503,1.303296875350127, 0.7933694705066887,1.615429232099775, 
  0.8502716527752076,1.605949352701521, 0.8488804794399814,1.74238618072675, 
  0.8673086999532241,1.733600681329906, 0.8662189431375097,1.993698332704193, 
  0.8938614148924769,2.223577019432303, 0.9120150573778179,1.859990296100349, 
  0.8807739934126004,2.104525034564196, 0.9032191786855563,0.9212582664783947, 
  0.6775575744738503,0.1721433401286832, 0.1696480723280338,0.2434474462821884, 
  0.2365388994335815,0.6858328298130242, 0.5655948487899036,0.9699140894422975, 
  0.6962261556132335,0.7071067811865476, 0.5773502691896258,0.7277591150584509, 
  0.5884288866823369,0.9074594767321543, 0.672011053395733,1.137474998564951, 
  0.7510336771306878,0.6648428536982504, 0.5536482892977428,0.6867673184305531, 
  0.5661186320030672,0.9850719113086823, 0.7017693709455775,0.9395474235036325, 
  0.6847348805565449,0.9402297805468971, 0.6849988788408234,0.9081659436060942, 
  0.6722978079439913,0.924336903232045, 0.6787795253596322,0.8578259686404748, 
  0.6510902125769223,0.8749278381813145, 0.6584738481000175,0.9705750976166828, 
  0.6964705227737358,1.111694828841966, 0.7434688183264676,1.098285965913846, 
  0.7394176928919515,1.138038686326299, 0.751195861570777,1.085302452124569, 
  0.7354170088145736,1.085302452124569, 0.7354170088145736,1.112271581804067, 
  0.7436412569539566,1.261056740654668, 0.7835425838258827,1.272751913257739, 
  0.7863241495620722,1.284340594333098, 0.7890340160044326,1.295825640972795, 
  0.7916749664336856,1.29533061902651, 0.7915620436776353,1.306719075447459, 
  0.7941395326870839,1.425580626714676, 0.8186666996043305,1.435936437595644, 
  0.8206136952304767,1.446218096403695, 0.8225188888094886,1.175903635938045, 
  0.7617852375982823,1.188437078924658, 0.7651614674561222,1.272247913531904, 
  0.7862052546191586,1.283841144003783, 0.7889181658582193,1.098869756676496, 
  0.7395957630432602,1.071563382650295, 0.7310980879016427,1.455986755675077, 
  0.8243037319819111,1.44577456857927, 0.8224372696139641,1.283841144003783, 
  0.7889181658582193,1.591313961248442, 0.8466975523364076,1.581975598330628, 
  0.8452819741412596,1.716375685286335, 0.8640458977267527,1.707721336603685, 
  0.8629355362168485,1.963936227169853, 0.8911304732933875,2.190383264474242, 
  0.9096813477312079,1.832224195995172, 0.8777737038690617,2.073108498195123, 
  0.9006895407260915,0.9075056413687956, 0.6720298008362261,0.1832194265263904, 
  0.1802194694013624,0.2591113978838421, 0.2508280367794221,0.7299608435468958, 
  0.5895908711470045,1.032320524945325, 0.7182612619757396,0.7526036083944555, 
  0.6013305508852113,0.7745847594840828, 0.6123665509636861,0.9658474431744805, 
  0.6947174233241945,1.210662676635559, 0.7709969742638532,0.7076203255595218, 
  0.5776297058469191,0.7309554592460631, 0.590114592531367,1.048453634873784, 
  0.7236301093593548,1.064342230082369, 0.7287922497867374,1.000726261417141, 
  0.7073634135610164,0.9665993657025689, 0.694997081369698,0.9838108009334254, 
  0.7013129733946517,0.913020404485371, 0.6742600711044225,0.931222646451046, 
  0.6814922876305141,1.033024063859753, 0.7184981061740208,1.183223753300696, 
  0.763764712787132,1.168952134228911, 0.7598854783915429,1.211262633324542, 
  0.7711518618381339,1.155133232208127, 0.7560503061508176,1.155133232208127, 
  0.7560503061508176,1.183837615834584, 0.763929738693506,1.342195943608793, 
  0.8019021676980161,1.354643609698343, 0.8045332144192389,1.366977932357804, 
  0.8070944047556065,1.379201952510899, 0.8095885216868489,1.378675079748651, 
  0.8094819165060249,1.390796294852919, 0.811914331609802,1.517305663399719, 
  0.834968917698817,1.52832779024708, 0.8367918276492478,1.539270993911787, 
  0.8385745687713838,1.251563898236265, 0.78124962602354,1.264903770895247, 
  0.7844628009152728,1.354107181506187, 0.80442079654654,1.366446346280486, 
  0.8069849518907546,1.169573487391132, 0.7600560838169467,1.140510160364621, 
  0.7519053519776219,1.549668190505604, 0.8402437683902361,1.538798928518035, 
  0.8384982164099136,1.366446346280486, 0.8069849518907546,1.693702650276375, 
  0.8611100229100805,1.68376343626311, 0.8597952312277823,1.826811124536811, 
  0.8771764759090637,1.817599935760009, 0.8761507583208231,2.090300263765515, 
  0.9020853050179428,2.331317408445614, 0.9190216707478598,1.950113586776377, 
  0.8898282546818596,2.206496922171707, 0.9108251428988418,0.9658965781467943, 
  0.6947357074285243,0.1830864578960196, 0.1800929214529031,0.2589233518434015, 
  0.2506574432255698,0.7294310858927489, 0.5893116198869808,1.031571334486059, 
  0.7180087745162979,0.7520574181082091, 0.6010518387719567,0.7740226167215636, 
  0.6120886747006234,0.9651464945136267, 0.6944564396302347,1.209784057151778, 
  0.7707698986659839,0.7071067811865476, 0.5773502691896258,0.730424979765144, 
  0.5898353577293538,1.047692736062563, 0.7233798002278359,1.063569800371923, 
  0.7285441208237253,0.9992742656557126, 0.7068500556379029,0.9658978713457123, 
  0.694736188635475,0.9830968156469072, 0.7010541981839915,0.9123577942208009, 
  0.6739930694307749,0.9305468261943384, 0.6812272710539267,1.032274362818135, 
  0.7182457129032661,1.182365047185949, 0.7635336135502983,1.168103785518273, 
  0.7596522928142984,1.210383578431587, 0.7709248743281107,1.154294912349286, 
  0.7558151069289791,1.154294912349286, 0.7558151069289791,1.182978464218714, 
  0.7636987293205006,1.341221865915753, 0.8016942870477831,1.353660498306516, 
  0.8043271203107029,1.365985869524409, 0.8068900730770983,1.378201018286254, 
  0.8093859284666561,1.377674527893664, 0.8092792485307828,1.389786946215837, 
  0.8117133793176844,1.516204502569009, 0.8347852756526836,1.527218630280369, 
  0.8366096361683738,1.53815389208634, 0.8383938076194417,1.250655595331245, 
  0.7810285560832686,1.263985786786489, 0.7842436866014556,1.353124459418722, 
  0.8042146256076498,1.365454669237364, 0.806780544422902,1.168724687743164, 
  0.7598229889454786,1.139682452971236, 0.7516680309550557,1.548543543077504, 
  0.8400643570054427,1.537682169286657, 0.8383173937608996,1.365454669237364, 
  0.806780544422902,1.692473472094059, 0.8609483527273606,1.682541471309759, 
  0.8596323953095708,1.825485344963209, 0.8770295781475284,1.816280841062454, 
  0.8760028881346472,2.088783261074227, 0.9019632690111711,2.32962549133487, 
  0.9189179294797981,1.948698322371192, 0.8896936838673354,2.204895591574723, 
  0.9107124054010286,0.9651955938270036, 0.6944747296742978,0.1895505346139122, 
  0.1862344125034554,0.2680649368060655, 0.2589233518434015,0.7551844843352723, 
  0.6026445656491346,1.067992139840674, 0.7299608435468957,0.778609665078177, 
  0.6143495109365001,0.8013503701412827, 0.6253373771849537,0.9992220949498121, 
  0.7068315896950879,1.252496866429861, 0.7814763935589044,0.7320719945281474, 
  0.5907016251625139,0.7562134687671463, 0.6031670953642669,1.084682725931358, 
  0.7352241010485261,1.101120348148331, 0.7402807963426474,1.034554786070187, 
  0.7190125568358325,1.035306143295228, 0.71926464406382,1.017806172693219, 
  0.7133188864345454,0.9445696292400789, 0.6866716321445391,0.9634008457828757, 
  0.6938052904162757,1.068719989391782, 0.7301931170145588,1.224109796969165, 
  0.7744359317044821,1.20934502515348, 0.7706563224753595,1.253117554493884, 
  0.7816270868471196,1.195048613929643, 0.7669174232663951,1.195048613929643, 
  0.7669174232663951,1.224744871391589, 0.7745966692414834,1.388575237304468, 
  0.8114717575582852,1.401453029832816, 0.8140185004319814,1.414213562373095, 
  0.8164965809277261,1.426859980927498, 0.8189087719071878,1.426314902189664, 
  0.8188056885285723,1.438854963288771, 0.8211572997337945,1.56973583600158, 
  0.8433992413775286,1.58113883008419, 0.8451542547285166,1.592460173810454, 
  0.8468700749970912,1.294811420992989, 0.7914435187635961,1.308612250097905, 
  0.7945638975588809,1.400898065479238, 0.813909706862152,1.413663607452597, 
  0.816390700883189,1.209987849041295, 0.7708225940253536,1.179920244966896, 
  0.7628740463179675,1.603216643308299, 0.8484761486758321,1.59197179629801, 
  0.8467965993232586,1.413663607452597, 0.816390700883189,1.752228183023185, 
  0.8685148710112521,1.741945521595986, 0.8672543355568072,1.889936192135819, 
  0.8838951214182464,1.880406712701383, 0.8829145004510592,2.162530142202388, 
  0.9076542546568698,2.411875582756156, 0.9237483432100854,2.01749934458, 
  0.8959760888007205,2.282741951281877, 0.9159656210592916,0.9992729277705827, 
  0.7068495821084,0.1862344125034554, 0.1830864578960195,0.2633752319429721, 
  0.2546898520060881,0.7419727887255553, 0.5958662464992963,1.049307980727467, 
  0.7239108194214406,0.764988153901539, 0.6075915653785749,0.7873310180668559, 
  0.618606646021592,0.9817410443736727, 0.7005620521918522,1.230584859900806, 
  0.7760677622540173,0.71926464406382, 0.5839114969175909,0.7429837714248951, 
  0.5963894986024435,1.065706570693294, 0.7292298189583651,1.081856622302313, 
  0.7343421245935389,1.016455602084481, 0.712853500621518,1.017193814570511, 
  0.7131079940714454,0.9825053402396817, 0.700839615215764,0.9280447049565937, 
  0.6802437519148747,0.9465464757731015, 0.6874299924970035,1.050023096798322, 
  0.7241455070396345,1.202694412561917, 0.7689268112762583,1.188187945405586, 
  0.7650949642146415,1.231194689238331, 0.7762206519186073,1.174141645031904, 
  0.7613055563372126,1.174141645031904, 0.7613055563372126,1.203318376573399, 
  0.7690897967394739,1.364282585976223, 0.8065385769956245,1.376935085905824, 
  0.809129284030593,1.38947237727095, 0.8116506924699338,1.401897551035557, 
  0.8141055813023218,1.401362008264784, 0.8140006627040949,1.413682685261589, 
  0.8163943752165067,1.542273841637154, 0.8390590980084401,1.553477344218039, 
  0.8408493861483476,1.564600624887782, 0.8425999673622909,1.272159135728942, 
  0.7861843026388989,1.285718524024258, 0.7893531981537708,1.376389830464791, 
  0.8090186006863364,1.38893204362467, 0.8115429500673399,1.188819523308198, 
  0.7652635088196953,1.159277941736889, 0.7572088560935655,1.575168913611541, 
  0.8442388245681711,1.564120791373753, 0.842524997579963,1.38893204362467, 
  0.8115429500673399,1.721573547138753, 0.8647067841255434,1.711470777374654, 
  0.8634181350669143,1.856872401485691, 0.8804422742794636,1.847509637051653, 
  0.8794383252487232,2.124697413143125, 0.9047953389157737,2.369680640051617, 
  0.9213236394530113,1.982203879979908, 0.892818309223563,2.242806157523596, 
  0.9133276827680242,0.9817909878915394, 0.7005801994846593,0.2006739670069729, 
  0.1967514720831289,0.2837958457564721, 0.2730144244569301,0.7995011282999116, 
  0.6244574283004393,1.130665338774327, 0.7490636134727585,0.8243009736662618, 
  0.63606195049331,0.8483761761279386, 0.6469292657134738,1.05785964741816, 
  0.7267009090493661,1.325997393582848, 0.7984070336787267,0.775032323574823, 
  0.6125876269760249,0.8005904968334965, 0.6249760849799558,1.14833538190721, 
  0.7541346336233139,1.165737616436175, 0.7590003524943438,1.095265774003874, 
  0.7384939786580924,1.096061223277048, 0.7387376681605473,1.058683202427878, 
  0.7269677225351583,1.077534298357719, 0.7329859739318171,1.019936292635141, 
  0.7140509797858983,1.13143590086798, 0.7492875435509326,1.295944479978655, 
  0.7917020635200928,1.280313264069709, 0.7880974540763626,1.326654505610175, 
  0.798550410419897,1.265177893652031, 0.7845281745780398,1.265177893652031, 
  0.7845281745780398,1.296616822601967, 0.7918552798218103,1.470061279041545, 
  0.8268327757266962,1.483694781675658, 0.8292356723887719,1.497204143130086, 
  0.8315721808641646,1.510592694024504, 0.8338450415985194,1.510015628320759, 
  0.8337479431641558,1.523291580363803, 0.8359623374787739,1.661852961823955, 
  0.8568352532304413,1.673925120116598, 0.8584767249724512,1.685910836548506, 
  0.8600807541411571,1.370795101717048, 0.8078779170926925,1.385405807670001, 
  0.8108377815638603,1.483107250236579, 0.8291330578874452,1.496621915093662, 
  0.8314723821118092,1.280993810921858, 0.7882561018664712,1.24916174355104, 
  0.7806643310089401,1.697298530123303, 0.8615814828510189,1.685393799479638, 
  0.8600120815605904,1.496621915093662, 0.8314723821118092,1.855054544187367, 
  0.8802482702185039,1.844168463258138, 0.8790771924941126,2.000843700274698, 
  0.8945026156618847,1.990755000469578, 0.8935956850137121,2.289434336293639, 
  0.9163963932612245,2.553412165809891, 0.9311389532731706,2.135892667016099, 
  0.905654332384115,2.416700559299561, 0.9240187268882579,1.057913463271642, 
  0.7267183542387443,0.1967514720831289, 0.1930503508952376,0.2782486002368323, 
  0.2680649368060655,0.7838735949225752, 0.6169254475416105,1.10856466912566, 
  0.7425303656419568,0.8081886874880889, 0.6285707197185578,0.8317933014581195, 
  0.6394853916677729,1.037182081917135, 0.7198928005974746,1.300078645262203, 
  0.7926418136878975,0.7598830722774107, 0.6050242530188936,0.7849416699988826, 
  0.6174457116998708,1.12588932289127, 0.7476699279155669,1.142951402802166, 
  0.7526036083944555,1.073857045692638, 0.7318252946572212,1.074636946632449, 
  0.7320719945281474,1.037989539221738, 0.7201626337087944,1.056472160210876, 
  0.7262506491670556,0.9804533942177577, 0.7000937067755993,1.109320169345837, 
  0.7427572734287433,1.27061316411284, 0.7858190109876364,1.255287485419162, 
  0.7821528381623483,1.300722912979778, 0.7927877574810048,1.240447960120407, 
  0.7785238196428866,1.240447960120407, 0.7785238196428866,1.271272364719943, 
  0.7859748716311795,1.441326570744381, 0.8216159200896873,1.454693584677074, 
  0.8240688559379761,1.467938883968782, 0.8264545803433695,1.481065734136872, 
  0.8287758471217302,1.480499948108948, 0.8286766700167976,1.493516400351023, 
  0.8309387083008458,1.62936937711113, 0.8522854881065539,1.641205565684686, 
  0.853966146058572,1.652957002042482, 0.8556087411094255,1.344000710255556, 
  0.8022865597222986,1.358325826499047, 0.8053025587809007,1.454117537483419, 
  0.823964092630789,1.467368036514262, 0.8263526681451542,1.255954729890276, 
  0.7823141693950717,1.224744871391589, 0.7745966692414834,1.664122104860204, 
  0.8571457973638914,1.652450071293374, 0.8555384118774253,1.467368036514262, 
  0.8263526681451542,1.818794524307579, 0.8762844583153514,1.808121229310788, 
  0.8750826787605936,1.961733997233545, 0.8909244649794129,1.951842497266372, 
  0.8899923341426094,2.244683665857777, 0.9134543405209785,2.503501624805223, 
  0.9286557417882323,2.094143215060754, 0.9023934874061736,2.369462266173208, 
  0.9213108037745444,1.037234845853345, 0.719910443030877,0.1773622057184381, 
  0.1746366706671413,0.250828036779422, 0.2432914638331035,0.7066252075672828, 
  0.5770880444369769,0.9993189520563546, 0.7068658713445067,0.7285441208237253, 
  0.5888436005246567,0.7498225710153865, 0.5999091447539736,0.9349709043231034, 
  0.6829572766603637,1.171959801315842, 0.7607098379147021,0.6849988788408234, 
  0.5651268384658659,0.7075880270542277, 0.5776121371315238,1.014936313251388, 
  0.7123288491921512,1.030316976456095, 0.7175854032150314,0.968031660621377, 
  0.6955289148729517,0.968734704667057, 0.6957895485677253,0.9356987891366275, 
  0.6832408154264226,0.9523600033648307, 0.6896472175208935,0.883832658335175, 
  0.662244801474054,0.9014530048522396, 0.6695607760081972,1.145398054794331, 
  0.7533008740016652,1.131582675684516, 0.7493301680978268,1.172540578385779, 
  0.7608685981073617,1.118205541013372, 0.7454068163012665,1.118205541013372, 
  0.7454068163012665,1.145992293162404, 0.7534698422151642,1.299288168170896, 
  0.7924625632425952,1.311337903046424, 0.7951728283096031,1.323277917893102, 
  0.7978122181035842,1.335111156421371, 0.8003835187222503,1.334601126906396, 
  0.8002735936186106,1.34633484689253, 0.802782224932542,1.46879992101105, 
  0.8266081459420496,1.47946968876668, 0.8284958743255685,1.490063056382745, 
  0.8303425555303563,1.211553478783415, 0.7712268985935011,1.224466897865967, 
  0.7745263325170877,1.310818623572767, 0.7950570025825351,1.322763325739912, 
  0.7976994014341281,1.132184165217972, 0.7495047480806893,1.104049944493318, 
  0.7411690632401259,1.500127871867264, 0.8320721171904764,1.489606082135709, 
  0.8302634535363675,1.322763325739912, 0.7976994014341281,1.639557789145867, 
  0.8537337608582012,1.629936315299335, 0.8523665968573053,1.768411006526974, 
  0.8704651231260861,1.75949428415907, 0.8693955680808895,2.023476835530234, 
  0.8964977882619501,2.256789062333132, 0.9142646474074376,1.887771693807446, 
  0.8836734032988369,2.135958879725835, 0.9056593798562586,0.935018468531947, 
  0.6829758141593908,0.1548476575248641, 0.1530239365232401,0.218987657373367, 
  0.2139184275607192,0.6169254475416105, 0.5250482567823542,0.8724643348864369, 
  0.6574217712031684,0.6360619504933099, 0.5366942961717305,0.6546392914470468, 
  0.5477141360349459,0.8162846971928793, 0.6323570420384698,1.023189969993689, 
  0.7151647199361613,0.5980443881265562, 0.5132611272617835,0.6177660456925458, 
  0.5255661536856221,0.8860992115388492, 0.6631966742709595,0.8995274368971231, 
  0.6687706005621403,0.8451486857075184, 0.6454947432018249,0.8457624845896949, 
  0.6457680860640481,0.8169201835292471, 0.6326523434596689,0.8314664053937455, 
  0.6393368117827454,0.7716379948749582, 0.6109073928330112,0.7870215957491786, 
  0.618456531997531,0.8730589298753098, 0.6576760549300295,0.9879383598985629, 
  0.7028035371542816,1.023697022600865, 0.7153377961316837,0.976259333017776, 
  0.698561953125077,0.976259333017776, 0.698561953125077,1.000518805113721, 
  0.7072901351405821,1.134355137702934, 0.750133565817368,1.144875266338643, 
  0.7531520987068586,1.155299602923379, 0.7560969483585953,1.16563071748983, 
  0.7589708450674552,1.165185431667281, 0.758847882036623,1.175429660681831, 
  0.7616563264376881,1.282348887238847, 0.788571527229057,1.291664223257595, 
  0.7907231470567894,1.300912857452252, 0.792830758951242,1.057757583673357, 
  0.7266678197441441,1.069031759518602, 0.7302925326253433,1.144421904757066, 
  0.7530229873021438,1.154850333648793, 0.7559709687840789,0.9884634957069738, 
  0.7029925158637903,0.9639006630682311, 0.693991902750083,1.309700034488559, 
  0.7948072045339161,1.300513892005155, 0.7927404236016774,1.154850333648793, 
  0.7559709687840789,1.431430568860419, 0.8197700361985394,1.423030455200143, 
  0.8181828651029873,1.54392702093816, 0.8393250000610506,1.536142196849642, 
  0.8380675909698627,1.766614520655504, 0.8702506218268502,1.970309843714868, 
  0.8917236047426668,1.64813593484443, 0.8549379667360802,1.864817973791103, 
  0.8812850738678667,0.8163262235501526, 0.6323763471669942,0.1567381770060666, 
  0.1548476575248641,0.2216612556636142, 0.2164085263988858,0.6244574283004393, 
  0.5296679606266864,0.8831161642271058, 0.6619432303300089,0.6438275668925518, 
  0.5413348073789278,0.6626317167340908, 0.5523694248603359,0.8262506349856602, 
  0.6369564429244005,1.035681993458323, 0.7193906387316717,0.6053458519294268, 
  0.5178544532580359,0.6253082892296795, 0.5301868946375132,0.896917507717617, 
  0.6676959616778225,0.9105096769291179, 0.6732469646707245,0.8554670210338776, 
  0.6500569665265161,0.8560883137248908, 0.6503294503726396,0.8268938799108122, 
  0.6372509965694481,0.8416176951354706, 0.6439174603420817,0.7810588455682462, 
  0.6155504876761577,0.7966302632787602, 0.623086621389501,0.883718018566413, 
  0.6621965714034487,1.01220889945267, 0.7113839948743789,1.036195236619796, 
  0.7195625764588234,0.9881783850543207, 0.7028899326014622,0.9881783850543207, 
  0.7028899326014622,1.01273403860586, 0.7115662187128817,1.148204365522768, 
  0.7540975221410426,1.158852933351221, 0.7570903832584583,1.16940453961318, 
  0.7600097112409546,1.179861785718606, 0.7628582457545873,1.179411063446222, 
  0.7627363771091806,1.189780363222781, 0.7655196196669727,1.298004955866389, 
  0.7921711411533547,1.307434021885958, 0.7942999283093379,1.316795571725572, 
  0.7963849196790369,1.070671639657724, 0.7308146859344564,1.082083460782275, 
  0.7344130544816013,1.158394036723678, 0.7569623800205417,1.168949785255194, 
  0.75988483313389,1.000531547138695, 0.7072946365792457,0.9756688293459929, 
  0.6983455148380353,1.325690030522789, 0.7983399227906722,1.316391735349446, 
  0.7962955590605725,1.168949785255194, 0.75988483313389,1.448906760749114, 
  0.8230125783650194,1.440404090945768, 0.8214449352015132,1.562776670699054, 
  0.842314729537743,1.554896802475982, 0.8410742614490566,1.788182939709813, 
  0.8727935202576655,1.994365158487389, 0.8939214866797913,1.668257860757277, 
  0.8577094025248405,1.88758534893065, 0.8836542874047543,0.8262926683340541, 
  0.6369756992902303,0.1512631707489478, 0.1495618195622024,0.2139184275607192, 
  0.2091856829965662,0.6026445656491346, 0.5161602624726624,0.8522681180314492, 
  0.6486500280900068,0.6213380877843072, 0.5277604016264412,0.6394853916677729, 
  0.5387460972410903,0.7973889531484408, 0.6234494473040505,0.9995046848862693, 
  0.7069315957777504,0.5842005739228679, 0.5044296261860434,0.6034657052367048, 
  0.5166759070369086,0.8655873681136373, 0.6544643955474164,0.878704750563532, 
  0.6600793721659071,0.8255847844123686, 0.6366512436048324,0.8261843747878657, 
  0.636926085305184,0.7980097289466875, 0.6237460197122588,0.8122192279911816, 
  0.630461338652143,0.7537757538011486, 0.6019279375624744,0.7688032478102022, 
  0.6094978027755634,0.852848949054443, 0.6489059842643909,0.9768515272803484, 
  0.6987788186725937,0.9650690957257537, 0.6944276049812104,0.9536604204801088, 
  0.6901405471373564,0.9536604204801088, 0.6901405471373564,0.9773583228480477, 
  0.6989642577706352,1.108096548743421, 0.7423896414417492,1.1183731524684, 
  0.7454564600537388,1.128556181582083, 0.7484493638560024,1.138648146624829, 
  0.7513710627628175,1.138213168488995, 0.7512460360830635,1.148220259247668, 
  0.7541020245560554,1.252664469005523, 0.7815170983096932,1.261764169222567, 
  0.7837121902080957,1.270798711660875, 0.7858628967876962,1.03327211110369, 
  0.7185815509638864,1.044285306996944, 0.7222553678280478,1.117930285515026, 
  0.7453252632403338,1.128117312205044, 0.7483213093347072,0.9655820754422295, 
  0.6946186514103458,0.9415878348647421, 0.6855234969385129,1.279382478969274, 
  0.787880216643431,1.270408981654546, 0.7857707039265361,1.128117312205044, 
  0.7483213093347072,1.398295137387078, 0.8133982918308145,1.390089473528709, 
  0.8117736400758927,1.50818746841284, 0.833439821218661,1.500582851112362, 
  0.8321497334791894,1.72572009261797, 0.8652307995586594,1.924700180048374, 
  0.8873760418632671,1.609984105118406, 0.8494747477696031,1.821650285797684, 
  0.87660326056629,0.7974295182339657, 0.6234688352858434,0.1586132416744277, 
  0.1566549075382022,0.224312997547937, 0.2188741043687594,0.6319278358493063, 
  0.5342038094169408,0.893680915899168, 0.6663574721515556,0.6515297001332006, 
  0.545889034660483,0.6705588047219487, 0.5569361684027188,0.8361351022065119, 
  0.6414520334324507,1.048071895846407, 0.7235045662245978,0.6125876269760249, 
  0.5223664146742605,0.6327888756596372, 0.5347236717898507,0.9076473653775711, 
  0.6720873468279258,0.9214021382172479, 0.6776148044030808,0.8657010049727532, 
  0.6545135098447205,0.8663297302114448, 0.6547850976755167,0.8367860422947397, 
  0.6417457953819665,0.8516859990711156, 0.6483932833474505,0.7904026817236153, 
  0.6200931730742153,0.8061603808860573, 0.6276150589721776,0.8942899702444256, 
  0.6666098395700516,1.024317992340045, 0.7155495773502196,1.011963037367013, 
  0.7112986311300705,1.048591278955, 0.7236753589814637,1.0, 
  0.7071067811865475,1.024849413752548, 0.7157306616513655,1.161940377252485, 
  0.7579493235727444,1.172716334295774, 0.7609166155191657,1.183394169817727, 
  0.7638105412281924,1.193976516349069, 0.7666338487445955,1.193520402069298, 
  0.7665130683182169,1.204013750166554, 0.7692712584708246,1.313533037655987, 
  0.7956614949312494,1.323074903944684, 0.7977677203232911,1.332548446354843, 
  0.799830368563556,1.083480124490751, 0.7348492461028211,1.09502846565784, 
  0.7384212220710148,1.172251947870729, 0.7607897156518549,1.182933975216363, 
  0.7636867590863875,1.01250094342399, 0.7114853521543005,0.9873407920092888, 
  0.7025883114378096,1.341549309895009, 0.8017642002651092,1.332139778869059, 
  0.7997419705407316,1.182933975216363, 0.7636867590863875,1.466240086469273, 
  0.8261510586297738,1.457635698909351, 0.8246026100818169,1.581472226406923, 
  0.845205159750559,1.573498091025852, 0.8439812887896,1.809575039036617, 
  0.8752473323545059,2.018223823401842, 0.8960395176715702,1.68821529188333, 
  0.8603862629856615,1.910166602993334, 0.8859389278710642,0.8361776384014232, 
  0.6414712381618227,0.1586132416744277, 0.1566549075382022,0.224312997547937, 
  0.2188741043687594,0.6319278358493063, 0.5342038094169408,0.893680915899168, 
  0.6663574721515556,0.6515297001332006, 0.545889034660483,0.6705588047219487, 
  0.5569361684027188,0.8361351022065119, 0.6414520334324507,1.048071895846407, 
  0.7235045662245978,0.6125876269760249, 0.5223664146742605,0.6327888756596372, 
  0.5347236717898507,0.9076473653775711, 0.6720873468279258,0.9214021382172479, 
  0.6776148044030808,0.8657010049727532, 0.6545135098447205,0.8663297302114448, 
  0.6547850976755167,0.8367860422947397, 0.6417457953819665,0.8516859990711156, 
  0.6483932833474505,0.7904026817236153, 0.6200931730742153,0.8061603808860573, 
  0.6276150589721776,0.8942899702444256, 0.6666098395700516,1.024317992340045, 
  0.7155495773502196,1.011963037367013, 0.7112986311300705,1.048591278955, 
  0.7236753589814637,1.0, 0.7071067811865475,1.024849413752548, 
  0.7157306616513655,1.161940377252485, 0.7579493235727444,1.172716334295774, 
  0.7609166155191657,1.183394169817727, 0.7638105412281924,1.193976516349069, 
  0.7666338487445955,1.193520402069298, 0.7665130683182169,1.204013750166554, 
  0.7692712584708246,1.313533037655987, 0.7956614949312494,1.323074903944684, 
  0.7977677203232911,1.332548446354843, 0.799830368563556,1.083480124490751, 
  0.7348492461028211,1.09502846565784, 0.7384212220710148,1.172251947870729, 
  0.7607897156518549,1.182933975216363, 0.7636867590863875,1.01250094342399, 
  0.7114853521543005,0.9873407920092888, 0.7025883114378096,1.341549309895009, 
  0.8017642002651092,1.332139778869059, 0.7997419705407316,1.182933975216363, 
  0.7636867590863875,1.466240086469273, 0.8261510586297738,1.457635698909351, 
  0.8246026100818169,1.581472226406923, 0.845205159750559,1.573498091025852, 
  0.8439812887896,1.809575039036617, 0.8752473323545059,2.018223823401842, 
  0.8960395176715702,1.68821529188333, 0.8603862629856615,1.910166602993334, 
  0.8859389278710642,0.8361776384014232, 0.6414712381618227,0.1547673634252819, 
  0.1529464447264227,0.2188741043687594, 0.2138125753908085,0.6166055494294177, 
  0.5248510125086947,0.8720119306375963, 0.6572281460052346,0.6357321294136135, 
  0.5364961168243737,0.6542998373455251, 0.5475152803375956,0.8158614241139611, 
  0.6321602000066784,1.022659409062673, 0.7149834785098369,0.5977342805251733, 
  0.5130650549553437,0.6174457116998707, 0.5253688630860991,0.8856397371143196, 
  0.6630039706437313,0.8990609994530597, 0.6685788517115536,0.844710445608723, 
  0.6452994299855468,0.8453239262140243, 0.645572808313867,0.816496580927726, 
  0.6324555320336759,0.8310352600511485, 0.6391407392360736,0.7712378727226943, 
  0.6107087815140066,0.7866134966445975, 0.6182584420485908,0.8726062173075058, 
  0.6574824688706578,0.9994814639054564, 0.7069233796761972,0.9874260781997712, 
  0.7026190407231014,1.023166198744769, 0.7151566026816399,0.9757531073159713, 
  0.6983764171076359,0.9757531073159713, 0.6983764171076359,1.133766933620004, 
  0.7499633943276977,1.144281607189297, 0.7529830147714396,1.154700538379251, 
  0.7559289460184544,1.165026295889902, 0.758803917944793,1.164581240964125, 
  0.7586809085103915,1.17482015797617, 0.7614904216782106,1.281683943055016, 
  0.7884168237955614,1.290994448735806, 0.7905694150420948,1.300238287179808, 
  0.7926779895376144,1.057209098186946, 0.7264899128135799,1.068477427965078, 
  0.7301157382161665,1.143828480692064, 0.7528538564121064,1.154251502067001, 
  0.7558029197562779,0.987950941706311, 0.7028080666642191,0.9634008457828757, 
  0.6938052904162757,1.309020907747652, 0.7946553477775995,1.299839528610695, 
  0.7925876127048154,1.154251502067001, 0.7558029197562779,1.430688320443632, 
  0.8196305466791945,1.422292562545487, 0.8180425424121595,1.543126439050462, 
  0.839196307353421,1.535345651674224, 0.8379381741914053,1.765698467261399, 
  0.8701410536839675,1.969288166943468, 0.8916288350684193,1.647281316873498, 
  0.8548186108910535,1.863850998361939, 0.8811829522353593,0.8159029289383194, 
  0.6321795071307924,0.136507212228465, 0.1352528629778797,0.1930503508952376, 
  0.1895505346139122,0.5438556471749074, 0.4777692337590916,0.769128032207951, 
  0.6096595972659147,0.5607255870338224, 0.4890850873669781,0.5771025930844634, 
  0.4998391037705704,0.7196024155590757, 0.5840921700738166,0.9020014420401412, 
  0.6697854174942075,0.5272108956438408, 0.4663663641602755,0.5445966833134113, 
  0.4782713917201571,0.7811479686451615, 0.615594109295185,0.7929857299528468, 
  0.6213380877843072,0.7450476994523445, 0.5974553652292551,0.7455887988503862, 
  0.5977342805251733,0.7201626337087944, 0.5843916381034789,0.7329859739318171, 
  0.5911814643727197,0.6802437519148747, 0.562447829566518,0.6938052904162758, 
  0.5700414720484612,0.7696522022576207, 0.6099205570063824,0.8815581353340517, 
  0.6612863475334422,0.8709250983771584, 0.6567624586566591,0.9024484383910388, 
  0.6699683725967898,0.8606293572176157, 0.6523134391499377,0.8606293572176157, 
  0.6523134391499377,0.8820154922027055, 0.661479332381956,1.009274104983571, 
  0.7103629714111876,1.018463763705304, 0.7135451411591677,1.027571241798427, 
  0.7166558782459502,1.02717869645901, 0.7165226753019953,1.036209579887012, 
  0.7195673795889246,1.130465093881974, 0.7490053790128469,1.138677104132672, 
  0.751379383208592,1.146830312847701, 0.7537078747621879,0.9324748030985373, 
  0.6819826014597117,0.9424136445340989, 0.6858419870909768,1.008874440393083, 
  0.710223578911547,1.018067706721338, 0.7134088981335868,0.8713880361212183, 
  0.6569609107924586,0.8497344711816858, 0.647530941653952,1.154576720250678, 
  0.7558942036857416,1.146478601612095, 0.753608010198407,1.018067706721338, 
  0.7134088981335868,1.261889263144752, 0.7837421633159669,1.254484074609805, 
  0.7819583746823641,1.361061425670102, 0.8058715000595025,1.354198650662724, 
  0.80443997161597,1.557373402683126, 0.8414655711244431,1.736942671855606, 
  0.8666349489383306,1.4529276414985, 0.823747423362419,1.64394545571271, 
  0.8543514234247904,0.7196390234571608, 0.5841117463373665,0.1352528629778797, 
  0.1340324684030327,0.1912764331731074, 0.187870527164001,0.5388582194762251, 
  0.474370517173349,0.7620606021794954, 0.6061216375460963,0.5555731433761001, 
  0.4856546795020079,0.571799662980413, 0.4963817676127846,0.7129900707903229, 
  0.580539428550478,0.8937130533581107, 0.6663707943376875,0.5223664146742604, 
  0.4630029827862538,0.5395924463179173, 0.4748711969089134,0.7739700887876018, 
  0.6120626977025694,0.7856990742527328, 0.6178141563734875,0.7382015408633443, 
  0.5939076585600216,0.7387376681605472, 0.5941867412966165,0.7135451411591677, 
  0.5808389457969122,0.7262506491670556, 0.587630667787293,0.6739930694307747, 
  0.5588990961148008,0.6874299924970035, 0.5664896474544572,0.7625799556901834, 
  0.6063828601783748,0.8734575978726833, 0.6578464229692629,0.8629222666832771, 
  0.6533101496054999,0.8941559423103692, 0.6665543234907028,0.8527211319184946, 
  0.648849677681093,0.8527211319184946, 0.648849677681093,0.8739107521410781, 
  0.6580399527883652,0.9908111137125415, 0.703835526844069,1.009105215992719, 
  0.7103040775668953,1.018129006505279, 0.713429990463749,1.017740068220348, 
  0.7132961297859948,1.026687967887455, 0.716356045398207,1.120077378682352, 
  0.7459605202362203,1.128213929704664, 0.7483495078405482,1.136292219511933, 
  0.7506929251139199,0.9239063981669445, 0.6786089904136869,0.9337539127187258, 
  0.6824825217587568,0.9996040078819878, 0.7069667352418766,1.008712798331342, 
  0.7101671786742024,0.8633809505450486, 0.6535091293460153,0.8419263577514636, 
  0.6440556676486023,1.143967446058147, 0.7528934760989554,1.135943740110877, 
  0.7505924157384363,1.008712798331342, 0.7101671786742024,1.25029390619835, 
  0.7809404434991201,1.242956763098788, 0.7791429019927676,1.348554786999373, 
  0.803252100962569,1.341755073251155, 0.8018081163756667,1.543062875578758, 
  0.839186083545935,1.720982103156091, 0.8646318100991593,1.439576854616865, 
  0.8212914153924608,1.628839427857381, 0.8522096155284694,0.7130263423025953, 
  0.5805590079429339,0.1340324684030327, 0.1328445237427566,0.1895505346139122, 
  0.1862344125034554,0.5339960699203371, 0.4710433162525807,0.7551844843352723, 
  0.6026445656491346,0.5505601740741656, 0.4822954555617959,0.5666402808332509, 
  0.4929951952061405,0.7065567192504404, 0.5770507369175515,0.8856490276674566, 
  0.6630078684089207,0.5176530716476142, 0.4597113363133213,0.5347236717898507, 
  0.4715425054495834,0.7669865109419728, 0.6085913718523924,0.778609665078177, 
  0.6143495109365001,0.7315407047392275, 0.5904224096460036,0.7320719945281474, 
  0.5907016251625139,0.7071067811865476, 0.5773502691896258,0.7196976466449019, 
  0.5841430932683164,0.6679115901385229, 0.5554166976359435,0.6812272710539267, 
  0.5630033984173318,0.7556991516885443, 0.6029060164276344,0.8655763383537847, 
  0.6544596280007038,0.8551360680802417, 0.6499117161733898,0.8860879204065281, 
  0.6631919403487539,0.8450269787572348, 0.6454405139969878,0.8450269787572348, 
  0.6454405139969878,0.8660254037844386, 0.6546536707079772,0.9818709664857089, 
  0.7006092573811336,0.9909769409092172, 0.7038949619025853,1.008942368317542, 
  0.7102472762800239,1.008556939445739, 0.710112783354981,1.017424101685411, 
  0.7131873262582445,1.109970854308252, 0.7429524959860135,1.118033988749895, 
  0.7453559924999299,1.12603938767088, 0.7477138692130079,0.9155699361419318, 
  0.6752849545273784,0.925328595988015, 0.6791719447681462,0.9905845218514853, 
  0.7037542888751901,0.9996111231463691, 0.706969252348987,0.8555906132104244, 
  0.6501111910376419,0.8343296064753846, 0.640635754656622,1.133645360194433, 
  0.7499282037187606,1.125694052620052, 0.7476127350405867,0.9996111231463691, 
  0.706969252348987,1.239012430401877, 0.778168557193924,1.231741490778059, 
  0.7763576236055071,1.336386697469119, 0.8006580791672294,1.329648337939852, 
  0.7992019248726872,1.529139728071618, 0.8369250212126407,1.705453579945134, 
  0.8626424975134195,1.426587467591933, 0.8188572450449083,1.614142313450427, 
  0.8500838354400496,0.7065926634827142, 0.5770703172059444,0.1328445237427566, 
  0.1316876159714861,0.187870527164001, 0.1846403120513031,0.5292632034184473, 
  0.4677851576233773,0.7484912003393985, 0.5992266549235523,0.5456804980766644, 
  0.4790049872403856,0.5616180850628266, 0.4896770050748471,0.7002944285397158, 
  0.5736241935019392,0.8777994219276543, 0.659695348248399,0.5130650549553437, 
  0.4564889105083785,0.529984356471745, 0.4682828458745777,0.760188624272919, 
  0.6051784469497724,0.7717087611025244, 0.6109425075424222,0.7250569781890571, 
  0.5869978071108125,0.7255835590975445, 0.5872771226603068,0.700839615215764, 
  0.5739237086002472,0.7133188864345454, 0.5807168866947204,0.6619918155011137, 
  0.5519985929344056,0.6751894780570121, 0.559580731644379,0.7490013061387319, 
  0.5994883009348798,0.8579046390897168, 0.6511246089172125,0.8475569020916633, 
  0.6465657715068388,0.8782344247116125, 0.6598799330504559,0.8375374107505829, 
  0.6420845286713025,0.8375374107505829, 0.6420845286713025,0.8583497244035616, 
  0.6513191338442559,0.9731685350106016, 0.6974269511393474,0.9821938021709974, 
  0.7007265149164079,0.9911368888864748, 0.7039522757522754,0.9996179872271143, 
  0.7069716805715205,1.008406558822595, 0.7100602879140243,1.100133059293743, 
  0.7399805695388345,1.108124729278907, 0.7423981157257481,1.116059175459747, 
  0.7447700004587026,0.9074551380657027, 0.6720092913985956,0.9171273058252509, 
  0.6759090817684271,0.9818048611669773, 0.700585240185727,0.9907514586916072, 
  0.703814141936462,0.8480074185378512, 0.6467657104674392,0.8269348504678894, 
  0.6372697482846683,1.123597735403697, 0.7469976937963622,1.115716901151846, 
  0.7446682608690424,0.9907514586916072, 0.703814141936462,1.228030925560186, 
  0.775425978671787,1.220824429082154, 0.773602003656866,1.324542153678814, 
  0.7980890275344913,1.317863516978777, 0.7966209827132918,1.515586792753613, 
  0.834682137233941,1.69033795536712, 0.8606668528547363,1.413943464353503, 
  0.8164445906105072,1.599835990773273, 0.8479738842499582,0.7003300541942651, 
  0.5736437725834522,0.1328952914415436, 0.1317370683287349,0.1879423235321561, 
  0.1847084670324319,0.5294654659892571, 0.4679247897183048,0.7487772428101982, 
  0.5993733969126831,0.545889034660483, 0.4791460232258145,0.5618327123351637, 
  0.4898192481347918,0.7005620521918522, 0.5737712495672962,0.8781348806683861, 
  0.6598377040477739,0.5132611272617834, 0.4566269927297465,0.5301868946375132, 
  0.4684225431293573,0.7604791370167726, 0.6053249883272209,0.7720036763676116, 
  0.6110888077921944,0.7253340650665216, 0.5871448090971537,0.7258608472125173, 
  0.5874241209700634,0.7011074472157657, 0.5740707660500147,0.713591487497392, 
  0.5808639437228779,0.6622448014740541, 0.5521452411001472,0.6754475076323415, 
  0.5597275913959142,0.7492875435509326, 0.5996350351311661,0.8582324948648608, 
  0.6512679110232066,0.8478808033884424, 0.6467095321969221,0.8785700496924699, 
  0.660022267252768,0.8378574830109509, 0.6422287101161023,0.8378574830109509, 
  0.6422287101161023,0.8586777502719583, 0.6514624156939883,0.9735404398935618, 
  0.6975638005072162,0.982569156138886, 0.7008627763209583,0.9915156605333147, 
  0.7040879454413644,1.000382158762414, 0.7072418559940298,1.008791930225125, 
  0.7101947911267917,1.100553484782174, 0.7401084717427121,1.108548208854049, 
  0.7425254190978361,1.116485687253022, 0.7448967066453337,0.9078019299982123, 
  0.672150092776162,0.9174777940614215, 0.6760493423668907,0.9821800664976531, 
  0.7007215271223577,0.9911300830429208, 0.7039498373075849,0.8483314920034377, 
  0.6469094516926612,0.8272508708669415, 0.6374143488654536,1.124027128123711, 
  0.7471238303573542,1.116143282141994, 0.7447949928682349,0.9911300830429208, 
  0.7039498373075849,1.228500228338904, 0.7755440913656263,1.221290977835097, 
  0.7737206737219627,1.325048339069029, 0.7981997173638623,1.318367150069456, 
  0.7967321815394369,1.516165987526663, 0.8347788474004991,1.690983932828205, 
  0.8607520877788497,1.414483815237965, 0.8165485811782732,1.600447382115573, 
  0.8480648884292254,0.7005976914610575, 0.5737908287429122,0.1317370683287349, 
  0.1306086095812044,0.186304348697768, 0.1831529060106086,0.5248510125086947, 
  0.4647304788269637,0.742251420115047, 0.5960105329671591,0.5411314447555711, 
  0.4759191671326432,0.5569361684027188, 0.4865643534683955,0.6944564396302346, 
  0.5704024564049561,0.8704816665934463, 0.6565722402975137,0.5087878995495561, 
  0.4534685503416331,0.525566153685622, 0.4652267229709314,0.7538513287344221, 
  0.6019664198712009,0.7652754281998759, 0.6077354709275574,0.7190125568358325, 
  0.5837765995024197,0.7195347479143021, 0.5840559817660163,0.6949970813696981, 
  0.5707019266604094,0.7073723194218505, 0.5774947827598529,0.6564731354723126, 
  0.5487868168214542,0.6695607760081971, 0.5563639663451633,0.7427572734287433, 
  0.5962723365479549,0.8507527361696235, 0.6479812037377672,0.8404912628506327, 
  0.6434125518708299,0.8709130429863826, 0.656757288928306,0.8305552987760046, 
  0.6389223199190289,0.8305552987760046, 0.6389223199190289,0.8511941110396606, 
  0.6481761625456007,0.9650557371888412, 0.6944226279662324,0.9740057654105275, 
  0.6977349183045392,0.9828742982827445, 0.7009734928402188,0.9916635222678342, 
  0.7041408861353337,0.9912846941359229, 0.7040052262990473,1.090961824542521, 
  0.7371704099113822,1.098886872148811, 0.7396009813297582,1.106755172995747, 
  0.7419858584079139,0.8998901585142783, 0.6689196204497998,0.9094816944626773, 
  0.6728310649564008,0.9736200668045265, 0.6975930908030309,0.9824920812181137, 
  0.7008348027576352,0.8409380235764988, 0.6436129070711442,0.8200411265010119, 
  0.6340986032971363,1.114230887902592, 0.7442259471319371,1.106415752049992, 
  0.7418835571820621,0.9824920812181137, 0.7008348027576352,1.217793473094842, 
  0.7728290718371827,1.210647053414226, 0.7709929390724254,1.313500137509355, 
  0.7956541823587304,1.306877177115449, 0.7941750165179591,1.502952137204658, 
  0.8325531280850049,1.676246490642368, 0.8587893735671605,1.402156156148379, 
  0.8141562163043108,1.586498993661074, 0.8459698979752348,0.6944917682923081, 
  0.5704220324763907,0.1207531421953989, 0.1198822842165095,0.1707707313919, 
  0.1683338404405036,0.4810901726362269, 0.4335293354314351,0.6803642468665658, 
  0.5625159355703669,0.4960131808301235, 0.4443540359765317,0.5105001438856596, 
  0.4546795552386128,0.6365542991584007, 0.5369899681728835,0.7979029577487461, 
  0.6236950294988235,0.4663663641602755, 0.4226619631070122,0.4817456870280594, 
  0.434008838488298,0.6909969824567771, 0.5684808132129001,0.7014685674457792, 
  0.5742689569870139,0.659062985212466, 0.550297107458204,0.6595416372300913, 
  0.550575647439544,0.6370498634644112, 0.5372873715655425,0.6483932833474504, 
  0.5440402834802427,0.6017379533400218, 0.5155902916284649,0.6137343772674791, 
  0.5230769032971854,0.6808279233237218, 0.5627779074041975,0.7798189790246548, 
  0.614943043251676,0.7704130831553894, 0.6102990112426792,0.7982983669951853, 
  0.6238838233594437,0.7613055563372126, 0.6057415170547331,0.7613055563372126, 
  0.6057415170547331,0.7802235530987496, 0.6151413755846038,0.8845916653348738, 
  0.6625639155657526,0.8927954613067808, 0.6659901667809425,0.9009245568192987, 
  0.669344148183991,0.9089809560326949, 0.6726282455450278,0.9086337136971805, 
  0.6724875092183253,0.916622357908202, 0.6757068793131269,1.007264275823412, 
  0.7096611485994264,1.014476536298462, 0.7121698400202608,0.8248594389557435, 
  0.6363184334084834,0.8336512552527263, 0.6403285041886827,0.8924419213411062, 
  0.6658433754300892,0.9005742081122841, 0.669200434819026,0.7708225940253536, 
  0.6105025248776912,0.7516680309550557, 0.600853006780535,1.021328943723424, 
  0.7145283522892566,1.01416541547084, 0.7120621798331724,0.9005742081122841, 
  0.669200434819026,1.116256724753413, 0.7448286975230092,1.109706156695165, 
  0.7428731026242714,1.203983593156573, 0.7692633926850282,1.197912839583978, 
  0.7676728258371118,1.377639531827705, 0.809272154625404,1.536485010687956, 
  0.8381232453211838,1.285247682004228, 0.7892442047320855,1.454220448388604, 
  0.8239828148109214,0.6365866821999322, 0.5370094083506091,0.1198822842165096, 
  0.1190300000786544,0.1695391522272538, 0.1671538753875842,0.4776206048238418, 
  0.4309852401941877,0.6754575370107175, 0.5597332986383642,0.4924359899735806, 
  0.4417766677512301,0.5068184746930881, 0.4520725393184137,0.6319635416812875, 
  0.5342253792675189,0.7921485720284099, 0.6209351289170555,0.4630029827862537, 
  0.4201534740619743,0.4782713917201571, 0.4314632226065669,0.686013590516655, 
  0.5656962190644411,0.6964096556212591, 0.5714833008935452,0.6543098976420061, 
  0.5475211750968465,0.6547850976755167, 0.5477995223347871,0.6324555320336759, 
  0.5345224838248488,0.6437171444578496, 0.54126916577055,0.597398287403886, 
  0.5128525252746392,0.6093081944813019, 0.5203285706031043,0.675917869485804, 
  0.5599951662519167,0.7741950129097684, 0.6121739170436838,0.7648569512957234, 
  0.6075258215810918,0.7925411296281679, 0.6211241445308897,0.7558151069289791, 
  0.6029648948406361,0.7558151069289791, 0.6029648948406361,0.7745966692414834, 
  0.6123724356957945,0.8782120904781852, 0.6598704588698269,0.8863567216031207, 
  0.6633046163828572,0.8944271909999159, 0.6666666666666666,0.9024254883750613, 
  0.6699589821525335,0.9020807503119244, 0.669817887185452,0.9100117813260949, 
  0.6730456064904211,0.9927881133107064, 0.7045431528079333,1.00716024646973, 
  0.7096247644204009,0.8189106461474005, 0.6335754998081615,0.8276390568614563, 
  0.6375918779331807,0.8860057313276187, 0.6631574793420393,0.8940793689680779, 
  0.6665226008568857,0.7652635088196954, 0.6077295012913249,0.7462470862878435, 
  0.5980733092017282,1.013963235108794, 0.71199219044635,1.006851369410264, 
  0.7095167014686743,0.8940793689680779, 0.6665226008568857,1.108206407738329, 
  0.742422675576061,1.101703081634668, 0.7404577934409412,1.195300599906959, 
  0.7669840095355137,1.189273627921248, 0.7653845952363312,1.367704151625472, 
  0.8072437979651622,1.525404054891076, 0.8363109728351636,1.275978621353936, 
  0.7870832735010654,1.443732775293571, 0.8220608771522107,0.6319956911799929, 
  0.5342447998487028,0.1190300000786544, 0.1181956387881719,0.1683338404405036, 
  0.1659983809748653,0.4742250366791025, 0.4284854144387868,0.6706554784884652, 
  0.5569915525110374,0.4889350941914689, 0.4392436336776055,0.5032153289107408, 
  0.4495098591006373,0.6274706968393842, 0.5315030542248188,0.7865169170497215, 
  0.6182115453860341,0.4597113363133213, 0.4176891248103278,0.4748711969089134, 
  0.4289618804356866,0.6811364854016533, 0.5629521474921994,0.691458641325742, 
  0.568737793320968,0.6496581849169233, 0.5447868322655088,0.6501300065909581, 
  0.545064971673446,0.6279591894641803, 0.5317998458766429,0.6391407392360736, 
  0.5385399660202261,0.5931511787700812, 0.5101579131562878,0.6049764142469203, 
  0.5176231081947018,0.6711125383026308, 0.5572533001026707,0.7686909959199193, 
  0.6094418656941757,0.7594193217779179, 0.6047900955768158,0.7869066838233147, 
  0.6184007660509898,0.7504417589735505, 0.6002261086652981,0.7504417589735505, 
  0.6002261086652981,0.769089796739474, 0.6096405538533388,0.8719685805277456, 
  0.6572095856889688,0.8800553086859347, 0.6606512939394781,0.8880684023570596, 
  0.6640210612522114,0.8960098371021074, 0.6673212467895385,0.895667549899703, 
  0.6671798081693837,0.9035421965033296, 0.6704155322802949,0.9857300432484297, 
  0.702007207203399,0.99289065817001, 0.7045797993991102,0.8130887304357205, 
  0.6308677344404231,0.8217550878943776, 0.6348900416682502,0.8797068137202804, 
  0.6605038265228959,0.8877230531109421, 0.6638766581497509,0.7598229889454786, 
  0.6049939245206476,0.7409417606617891, 0.5953318368257511,1.006754623867363, 
  0.709482844018259,0.9996933188531331, 0.7069983280837631,0.8877230531109421, 
  0.6638766581497509,1.100327789567532, 0.7400398205325169,1.093870697832174, 
  0.7380658908360437,1.186802799352628, 0.7647247689794697,1.180818675170964, 
  0.7631167052445945,1.35798067528927, 0.8052306168452265,1.514559436036003, 
  0.8345104057583127,1.266907253166972, 0.7849399953543283,1.433468785482849, 
  0.8201523258015028,0.6275026177763134, 0.5315224542911514,0.1463923869844662, 
  0.1448485100148885,0.2070300991016027, 0.2027310084154288,0.5832389737156648, 
  0.5038102091252473,0.8248244667332583, 0.6363023781210947,0.6013305508852113, 
  0.5153339419004022,0.618893498426766, 0.5262598668862435,0.7717124507470828, 
  0.6109443382716068,0.9673199093882906, 0.6952647732510847,0.5653888918949469, 
  0.492170397178459,0.584033672013186, 0.5043221717176997,0.8377148273062938, 
  0.6421644570905524,0.8504098205311503, 0.647829647189235,0.7990003558022288, 
  0.6242187291631667,0.7995806389329296, 0.6244953118979234,0.7723132371145613, 
  0.6112423059696518,0.7860651799878826, 0.6179921053574942,0.7295036280385063, 
  0.5893498715313108,0.7440472258454789, 0.5969390890944893,0.8253865945762069, 
  0.6365603434756595,0.9453961998809045, 0.686988995361856,0.9339931711646747, 
  0.6825759260496387,0.9677992749962538, 0.6954427040485329,0.922951863533272, 
  0.6782304811066187,0.922951863533272, 0.6782304811066187,0.9458866762638952, 
  0.687177128970484,1.072415036499734, 0.7313683964366002,1.082360726134192, 
  0.7344997196253492,1.092215854327681, 0.7375569335237494,1.101982850779337, 
  0.7405427146856267,1.101561879254838, 0.7404149193398231,1.111246734435904, 
  0.7433347438249444,1.212327764917112, 0.7714265041496653,1.221134448189851, 
  0.7736808683448483,1.229878071811568, 0.7758903928017202,1.010658563000883, 
  0.710845188388645,1.081932119817797, 0.7343657345043453,1.091791116862764, 
  0.7374260998278647,0.9344896325623677, 0.6827696333373809,0.9112680239273897, 
  0.6735533666565483,1.238185435589374, 0.7779635542161755,1.229500891393999, 
  0.7757956634936686,1.091791116862764, 0.7374260998278647,1.353269020194002, 
  0.8042449720738165,1.345327584661009, 0.8025685317394653,1.459622738488382, 
  0.824961854511411,1.452262995378356, 0.8236262400098729,1.670150654482139, 
  0.8579663182724941,1.862723438835975, 0.8810637154534557,1.558141449689086, 
  0.8415866563484976,1.762991825891717, 0.8698165597563497,0.771751709607444, 
  0.6109638171403224,0.1448485100148885, 0.1433524708073368,0.2048467273525904, 
  0.2006795191839771,0.5770880444369769, 0.4998296510170384,0.8161257391261402, 
  0.6322831333640736,0.5949888250090312, 0.511325674720962,0.6123665509636861, 
  0.5222293180226328,0.7635738507530051, 0.6068822192399684,0.9571184025949281, 
  0.6914477092475989,0.5594262123660976, 0.4882221137056044,0.5778743617221752, 
  0.5003402922164547,0.8288801559439831, 0.638158788759129,0.8414412657882041, 
  0.6438384337086391,0.7905739733009424, 0.6201758734263734,0.7911481366751465, 
  0.620452931353989,0.7641683011336505, 0.607180543739702,0.7777752138703206, 
  0.6139393525132706,0.7218101688788333, 0.5852709560439485,0.7362003876326222, 
  0.5928640559360415,0.8166819386810913, 0.6325416667996108,0.935425903950985, 
  0.683134552479716,0.9241431333559668, 0.6787027813461534,0.9575927127383458, 
  0.691626476968292,0.9132182690787389, 0.6743397502706993,0.9132182690787389, 
  0.6743397502706993,0.9359112076934619, 0.6833235023098958,1.061105180087211, 
  0.7277504852235417,1.07094598092595, 0.7309019137297481,1.080697175398816, 
  0.7339792023210564,1.090361167580959, 0.7369850155755961,1.089944635687885, 
  0.7368563541731147,1.099527352874101, 0.739796159782974,1.199542367025938, 
  0.7681011695886709,1.208256173641883, 0.7703743227766876,1.216907585643732, 
  0.7726025077557589,0.9894538438686601, 0.7033485040466866,1.070521894758687, 
  0.7307670588933904,1.080276917281519, 0.7338475004914964,0.9246343589942465, 
  0.6788972916854288,0.9016576490695538, 0.6696446203806499,1.225127338666097, 
  0.7746933994704316,1.216534383029736, 0.7725069734207752,1.080276917281519, 
  0.7338475004914964,1.33899723381933, 0.8012184289654555,1.331139549905375, 
  0.7995253934067883,1.44422932919543, 0.8221525132302889,1.436947203085328, 
  0.820802213006331,1.652536984917111, 0.8555504732374188,1.843078866620505, 
  0.8789590944998514,1.541709046685952, 0.8389681167470764,1.744399038837587, 
  0.8675566334190138,0.7636126955832954, 0.6069017213284036,0.1353064432629281, 
  0.1340846108046027,0.1913522071388986, 0.1879423235321561,0.5390716876155645, 
  0.474516131639232,0.7623624917172837, 0.6062735047816721,0.5557932331156584, 
  0.4858016729333459,0.5720261808393219, 0.4965299356873079,0.7132725210866679, 
  0.5806918688438931,0.8940670968814498, 0.6665175164674829,0.5225733496017859, 
  0.4631470629194264,0.5398062053204783, 0.4750168756291125,0.7742766962564799, 
  0.6122142984204865,0.7860103281473554, 0.6179654501743799,0.7384939786580924, 
  0.5940599145569663,0.7390303183415825, 0.5943389907777781,0.7138278113460786, 
  0.5809913846827385,0.7265383526281299, 0.5877830413881657,0.6742600711044225, 
  0.5590513145662162,0.6877023171941512, 0.5666420157869698,0.7628820509693403, 
  0.6065347167677005,0.873803617217792, 0.6579942101527607,0.8632641124675771, 
  0.6534584571597777,0.8945101612837193, 0.6667010214299415,0.8530589365335617, 
  0.6489984657596424,0.8530589365335617, 0.6489984657596424,0.8742569510027925, 
  0.6581877170575424,0.9912036225344103, 0.703976184226479,1.00039614898989, 
  0.7072467993999283,1.009504972004728, 0.7104434536891557,1.018532337282783, 
  0.7135687221521886,1.018143244920344, 0.7134348894098962,1.027094689288866, 
  0.7164941588483986,1.120521096204515, 0.7460915489374379,1.128660870513297, 
  0.7484798979215893,1.136742360526912, 0.7508226788986946,0.9242724027533311, 
  0.6787539819300461,0.9341238183880549, 0.6826269154057024,1.009112398887489, 
  0.7103065826419068,0.8637229780364971, 0.6536574147462029,0.8422598860276485, 
  0.6442049379856379,1.14442062760637, 0.7530226234635427,1.136393743076093, 
  0.7507221970148142,1.009112398887489, 0.7103065826419068,1.250789208866356, 
  0.7810610944349405,1.243449159164966, 0.7792641406772454,1.349089015616054, 
  0.8033649532798846,1.342286608168102, 0.80192150187345,1.543674158378255, 
  0.8392843679049156,1.721663868477874, 0.8647182285687802,1.44014714153369, 
  0.8213972692379263,1.62948469095142, 0.8523019904103896,0.7133088069678633, 
  0.5807114481481171,0.1340846108046027, 0.1328952914415436,0.1896242751053872, 
  0.186304348697768,0.5342038094169408, 0.471185887252691,0.7554782723488097, 
  0.6027938336067216,0.5507743574733603, 0.4824394189256108,0.5668607198464317, 
  0.4931403504028913,0.7068315896950879, 0.5772004447638925,0.8859935700593183, 
  0.6631523798933651,0.5178544532580359, 0.4598523643638138,0.5349316943440546, 
  0.4716851411963081,0.7672848902759418, 0.6087404071260067,0.7789125661461535, 
  0.6144982730479422,0.7318252946572212, 0.590572000201395,0.7323567911328184, 
  0.5908512106619448,0.7073818656207657, 0.5774999770661947,0.7199776292800608, 
  0.5842927692484301,0.6681714265405619, 0.5555660876847268,0.6814922876305141, 
  0.5631529702038202,0.7559931399221637, 0.603055275204217,0.8659130719047049, 
  0.6546051438505974,0.8554687400722605, 0.6500577208010687,0.8864326335400148, 
  0.6633364289676765,0.8453557180290607, 0.6455869685767596,0.8453557180290607, 
  0.6455869685767596,0.8663623120344469, 0.6547991650024344,0.9822529419192321, 
  0.7007479889830733,0.991362458823012, 0.7040330804258587,1.000389028137669, 
  0.7072442832806175,1.009334875288104, 0.7103841591774424,1.008949296473624, 
  0.7102496931042103,1.017819908288909, 0.7133236146684521,1.11040266420257, 
  0.7430819457085648,1.118468935430389, 0.745484823820695,1.126477448676807, 
  0.7478420841086164,0.9159261186090948, 0.675427825376379,0.925688574848444, 
  0.6793142468860661,0.9909698871032249, 0.7038924340093279,0.8559234620332887, 
  0.650257174886526,0.8346541841683938, 0.6407826591959385,1.134086380137686, 
  0.7500558308857858,1.126131979280927, 0.7477409765747846,1.0, 
  0.7071067811865475,1.239494441100225, 0.7782879277654863,1.232220672876306, 
  0.7764775665212558,1.336906589497242, 0.8007698415240829,1.330165608556515, 
  0.7993142081634708,1.529734606452265, 0.8370225131801732,1.70611704937522, 
  0.8627283208100529,1.427142450257673, 0.8189621603578516,1.614770260228561, 
  0.8501755207016208,0.7068675479106794, 0.5772200250579534,0.1566549075382022, 
  0.1547673634252819,0.2215434948528287, 0.2162989364488539,0.6241256760831314, 
  0.5294654659892571,0.8826469957420416, 0.6617455837902052,0.6434855240034764, 
  0.5411314447555711,0.6622796838631178, 0.552165457326305,0.8258116771515696, 
  0.6367552758009795,1.035131772126677, 0.7192061662394285,0.6050242530188936, 
  0.5176530716476142,0.6249760849799558, 0.529984356471745,0.8964410070653028, 
  0.6674993105044016,0.910025955236474, 0.6730513407522917,0.8550125415638612, 
  0.6498574839554028,0.8556335041839704, 0.6501300065909581,0.8264545803433696, 
  0.6370498634644112,0.841170573324066, 0.6437171444578497,0.7806438965387016, 
  0.6153473148314498,0.7962070417039337, 0.6228840504971201,0.8832485303373557, 
  0.6619989672231389,1.011671148548359, 0.7111972459448213,0.9994687352535611, 
  0.7069188758734858,1.035644742620152, 0.7193781544590248,0.9876534007151483, 
  0.7027009276142099,0.9876534007151483, 0.7027009276142099,1.01219600871363, 
  0.7113795199692653,1.147594365021659, 0.7539246392649224,1.158237275641424, 
  0.7569186335805276,1.168783276206958, 0.7598390877597826,1.179234966746184, 
  0.7626887410756426,1.178784483926653, 0.7625668241608077,1.189148274859796, 
  0.7653511781211491,1.297315371592634, 0.7920143099199434,1.306739428281835, 
  0.7941441010674307,1.316096004660048, 0.7962300869052267,1.070102829560562, 
  0.730633716572359,1.081508587986857, 0.7342332513188093,1.157778622809482, 
  0.7567905814340652,1.16832876344393, 0.7597141610557183,0.975150490892762, 
  0.6981553693718647,1.324985738144866, 0.7981860325184339,1.315692382827952, 
  0.7961406834385222,1.16832876344393, 0.7597141610557183,1.448137007666251, 
  0.8228714273528104,1.439638855031623, 0.8213029277151372,1.561946422547354, 
  0.8421846569040813,1.554070740620476, 0.8409434469894418,1.787232941153762, 
  0.8726829889434468,1.993305622587158, 0.8938260145994205,1.667371574167887, 
  0.8575888823363158,1.886582541378869, 0.8835513400325815,0.8258536881691273, 
  0.6367745343853681,0.1606469042483717, 0.1586132416744277,0.2271890307412992, 
  0.2215434948528287,0.6400301101338081, 0.5390716876155645,0.905139262078377, 
  0.6710671489290188,0.6598833000784911, 0.5507743574733603,0.6791563866791399, 
  0.5618327123351637,0.8468556236848418, 0.6462542791874766,1.061509768793739, 
  0.7278809683695067,0.6204419304193619, 0.5272108956438407,0.640902189781787, 
  0.5395924463179174,0.9192847826437541, 0.6767713098018733,0.933215912554517, 
  0.682272369267994,0.8768006062131877, 0.6592710684289191,0.8774373926640058, 
  0.6595416372300913,0.847514909813295, 0.6465471285640757,0.862605906657509, 
  0.6531728318396853,0.800536844137783, 0.6249505598768306,0.816496580927726, 
  0.6324555320336759,0.9057561254250419, 0.6713184190052842,1.037451304179893, 
  0.7199828045983593,1.024937939926109, 0.7157608133347336,1.06203581118234, 
  0.7280505031556482,1.012821518257084, 0.7115965603001236,1.012821518257084, 
  0.7115965603001236,1.037989539221738, 0.7201626337087944,1.176838217013071, 
  0.7620391588368318,1.187752338186328, 0.7649786251717843,1.198567079771372, 
  0.7678449256559169,1.209285108051968, 0.7706408164126758,1.208823145694632, 
  0.7705212183064331,1.219451034446095, 0.773252197731517,1.330374525479577, 
  0.7993595337786311,1.340038732981101, 0.8014413994924982,1.349633740588231, 
  0.8034799338264007,1.097371984688097, 0.7391385947612459,1.109068393122299, 
  0.7426816830885594,1.187281997622256, 0.7648529256497096,1.198100984776525, 
  0.7677223350103746,1.025482742755416, 0.7159462830528804,1.358750008864607, 
  0.8053909249552199,1.349219833364817, 0.8033925744918017,1.198100984776525, 
  0.7677223350103746,1.485039510507208, 0.829470218940036,1.476324801635095, 
  0.8279423853141071,1.601749101430871, 0.8482583966515559,1.593672725527427, 
  0.8470522921402638,1.832776538437189, 0.8778344150915847,2.044100517000472, 
  0.8982695474123863,1.709860775070101, 0.8632111978834364,1.934657838967686, 
  0.8883459765788706,0.8468987052583529, 0.6462734243864373,0.1182313914997585, 
  0.1174135970100239,0.1672044373572015, 0.1649150368837146,0.4710433162525808, 
  0.4261340590157247,0.6661558463095987, 0.554405841838808,0.4856546795020079, 
  0.436860579484739,0.4998391037705705, 0.4470984564157395,0.6232608045334901, 
  0.5289371406797607,0.7812399351377028, 0.6156391167031471,0.4566269927297465, 
  0.4153715793704786,0.4716851411963082, 0.4266090786631537,0.6765665329503276, 
  0.5603638843268957,0.6868194343816983, 0.5661478229585393,0.6452994299855469, 
  0.5422088247911272,0.6457680860640482, 0.54248675199403,0.6237460197122588, 
  0.529233620557337,0.6348525490559637, 0.535967169789114,0.5891715465795833, 
  0.5076192947556656,0.6009174429444925, 0.5150738607825277,0.6666098395700517, 
  0.5546674605510566,0.7635336135502983, 0.6068620169535919,0.7543241458983048, 
  0.6022070803955923,0.7816270868471195, 0.6158285185194824,0.7454068163012666, 
  0.5976404999439461,0.7454068163012666, 0.5976404999439461,0.763929738693506, 
  0.6070608521791387,0.8661182773396673, 0.654693785547334,0.8741507491719046, 
  0.6581423961785412,0.8821100805533124, 0.6615192281915568,0.8899982337902365, 
  0.6648266281241422,0.8896582430970827, 0.6646848742916868,0.8974800562945994, 
  0.6679279449278726,0.9791164797056808, 0.6996064849433574,0.9862290518775109, 
  0.702187382928096,0.9932906949645623, 0.7047227072003306,0.8076334701223505, 
  0.6283094049203273,0.8162416823452713, 0.6323370436855225,0.8738045923652781, 
  0.6579946265373932,0.8817670483606308, 0.6613745161058396,0.7547251047397053, 
  0.6024110369942449,0.7359705563760149, 0.5927440071394864,0.9929860714350583, 
  0.7046138923010963,0.8817670483606308, 0.6613745161058396,1.092945354788355, 
  0.7377814509864328,1.086531585651091, 0.7357990839528452,1.17884017735486, 
  0.7625819012166508,1.172896202487701, 0.7609657434869379,1.348869568706525, 
  0.8033186070204815,1.504397794785337, 0.8327986168685125,1.258407185953866, 
  0.7829057919212717,1.423851206142266, 0.8183387731263766,0.6232925113031166, 
  0.5289565203091295,0.1190665155341918, 0.1182313914997585,0.1683854810929609, 
  0.1660479012985849,0.4743705171733491, 0.4285927201327394,0.6708612189764894, 
  0.5571093958828761,0.4890850873669781, 0.439352375281766,0.5033697029085268, 
  0.4496198837129526,0.6276631893061266, 0.5316200295410173,0.786758200957098, 
  0.6183286945271875,0.4598523643638138, 0.4177948977313425,0.4750168756291125, 
  0.4290692516731159,0.6813454412029739, 0.5630700988938919,0.6916707637088105, 
  0.5688558143764795,0.6498574839554028, 0.5449043412172393,0.6503294503726397, 
  0.5451824899223496,0.6281518317883596, 0.5319168350170586,0.6393368117827454, 
  0.5386572458000388,0.5933331428588073, 0.51027367124889,0.6051620060249683, 
  0.5177393415631584,0.6713184190052843, 0.5573711489859016,0.7689268112762583, 
  0.6095593660898281,0.7596522928142984, 0.6049077471982074,0.7871480873014826, 
  0.6185179066533125,0.7506719759160453, 0.6003438852488027,0.7506719759160453, 
  0.6003438852488027,0.7693257344380257, 0.6097580472506136,0.8722360788887579, 
  0.6573240958346176,0.88032528785483, 0.6607654844465912,0.8883408397446012, 
  0.6641349248497128,0.8962847107251117, 0.6674347767791816,0.8959423185174725, 
  0.6672933527305709,0.9038193808676146, 0.6705287373281551,0.9860324408082247, 
  0.702116409188128,0.9931952524290877, 0.7046886194178335,1.000306775229046, 
  0.707215217657796,0.8133381659172346, 0.6309842220724846,0.8220071819996861, 
  0.6350062802967179,0.8799766859795527, 0.6606180310203851,0.8879953845538896, 
  0.6639905360402267,0.7600560838169468, 0.6051115700216306,0.7411690632401259, 
  0.5954497218501617,1.007063471247694, 0.7095909122700904,0.8879953845538896, 
  0.6639905360402267,1.100665342877203, 0.74014248778777,1.094206270266048, 
  0.7381689448371662,1.187166881053232, 0.7648221471861418,1.181180921090501, 
  0.7632144538055136,1.358397270121972, 0.8053174457611399,1.515024065353896, 
  0.834588102669638,1.267295908929742, 0.7850324049130957,1.433908538197846, 
  0.820234662406533,0.6276951200356088, 0.531639430513616,0.1340846108046027, 
  0.1328952914415436,0.1896242751053872, 0.186304348697768,0.5342038094169408, 
  0.471185887252691,0.7554782723488097, 0.6027938336067216,0.5507743574733603, 
  0.4824394189256108,0.5668607198464317, 0.4931403504028913,0.7068315896950879, 
  0.5772004447638925,0.8859935700593183, 0.6631523798933651,0.5178544532580359, 
  0.4598523643638138,0.5349316943440546, 0.4716851411963081,0.7672848902759418, 
  0.6087404071260067,0.7789125661461535, 0.6144982730479422,0.7318252946572212, 
  0.590572000201395,0.7323567911328184, 0.5908512106619448,0.7073818656207657, 
  0.5774999770661947,0.7199776292800608, 0.5842927692484301,0.6681714265405619, 
  0.5555660876847268,0.6814922876305141, 0.5631529702038202,0.7559931399221637, 
  0.603055275204217,0.8659130719047049, 0.6546051438505974,0.8554687400722605, 
  0.6500577208010687,0.8864326335400148, 0.6633364289676765,0.8453557180290607, 
  0.6455869685767596,0.8453557180290607, 0.6455869685767596,0.8663623120344469, 
  0.6547991650024344,0.9822529419192321, 0.7007479889830733,0.991362458823012, 
  0.7040330804258587,1.000389028137669, 0.7072442832806175,1.009334875288104, 
  0.7103841591774424,1.008949296473624, 0.7102496931042103,1.017819908288909, 
  0.7133236146684521,1.11040266420257, 0.7430819457085648,1.118468935430389, 
  0.745484823820695,1.126477448676807, 0.7478420841086164,0.9159261186090948, 
  0.675427825376379,0.925688574848444, 0.6793142468860661,0.9909698871032249, 
  0.7038924340093279,1.0, 0.7071067811865475,0.8559234620332887, 
  0.650257174886526,0.8346541841683938, 0.6407826591959385,1.134086380137686, 
  0.7500558308857858,1.126131979280927, 0.7477409765747846,1.239494441100225, 
  0.7782879277654863,1.232220672876306, 0.7764775665212558,1.336906589497242, 
  0.8007698415240829,1.330165608556515, 0.7993142081634708,1.529734606452265, 
  0.8370225131801732,1.70611704937522, 0.8627283208100529,1.427142450257673, 
  0.8189621603578516,1.614770260228561, 0.8501755207016208,0.7068675479106794, 
  0.5772200250579534,0.1081768553036703, 0.1075494015910918,0.1529851759053225, 
  0.1512257324630455,0.4309852401941877, 0.3957912111231022,0.6095051718652462, 
  0.5204512244880369,0.4443540359765317, 0.4060694737104433,0.4573322001696624, 
  0.4159021813289688,0.5702579747495083, 0.4953721777468228,0.7148023748076474, 
  0.5815164805310581,0.4177948977313425, 0.38550223560826,0.4315724835919618, 
  0.396245874848098,0.6190305215042909, 0.5263441046377522,0.6284115041732334, 
  0.5320744824435168,0.5904224096460037, 0.5084186546968952,0.5908512106619448, 
  0.5086923765263681,0.5707019266604094, 0.4956631102193385,0.5808639437228779, 
  0.5022769927910903,0.5390677072722216, 0.474513416857028,0.5498147188345551, 
  0.4817940930677453,0.6099205570063824, 0.5207097709577877,0.6986018195741853, 
  0.5726928389206953,0.6901755358522724, 0.5680231472862376,0.7151566026816399, 
  0.5817071590127744,0.6820165464224989, 0.5634486918299697,0.6820165464224989, 
  0.5634486918299697,0.6989642577706353, 0.5728924567057632,0.7924625632425952, 
  0.6210863236626678,0.7998119442496564, 0.6246054944582202,0.8070944047556065, 
  0.6280554884679591,0.8143117401899582, 0.6314384780807243,0.8140006627040948, 
  0.6312934043278648,0.8211572997337945, 0.6346142337855957,0.8958512659539903, 
  0.6672557315948495,0.9023589766466328, 0.6699317657720695,0.9088200893235964, 
  0.6725630557993801,0.738951372622601, 0.5942979260387723,0.7468275323822881, 
  0.5983719822034846,0.7994952250238415, 0.6244546154526307,0.806780544422902, 
  0.6279075580804113,0.6905423966835518, 0.5682276076066248,0.6733827571082299, 
  0.5585509429690085,0.9149588271900809, 0.6750396511171427,0.9085413713363069, 
  0.6724500708389662,0.806780544422902, 0.6279075580804113,0.9941316653123009, 
  0.7050228531524796,1.07859022611715, 0.7333180862347811,1.073151733843846, 
  0.7316019409361164,1.234160118616112, 0.7769621637466176,1.376462041944136, 
  0.8090332640980304,1.151390762987919, 0.7549980505154016,1.30276522966511, 
  0.7932494968305371,0.5702869851382598, 0.4953911940523847,0.1088154206109984, 
  0.1081768553036703,0.1538882436234068, 0.1520978223552326,0.4335293354314351, 
  0.3977588325673015,0.6131030658537303, 0.5226858983501114,0.4469770468853745, 
  0.4080683180411194,0.460031820861469, 0.4179294679424088,0.5736241935019392, 
  0.4975739825697188,0.7190218355866336, 0.5837815656230941,0.4202611307025358, 
  0.3874371121727624,0.4341200453124946, 0.398214909169843,0.622684643396633, 
  0.5285848386264476,0.6321210017747714, 0.5343204882763294,0.5939076585600216, 
  0.5106389726195645,0.5943389907777781, 0.5109130502681742,0.5740707660500147, 
  0.4978653607732928,0.5842927692484301, 0.5044889731575625,0.5422498106454304, 
  0.4766794411726491,0.5530602615518074, 0.4839734551947903,0.613520903003104, 
  0.5229447241935766,0.7027256488754168, 0.5749580847782444,0.6942496250086326, 
  0.5702878382288911,0.7193781544590248, 0.5839722230362803,0.6860424732655982, 
  0.5657124140817974,0.6860424732655982, 0.5657124140817974,0.7030902265355963, 
  0.5751577164603203,0.7971404501974569, 0.6233306508299714,0.8045332144192389, 
  0.6268463434321752,0.811858663109843, 0.6302926623955016,0.8191186022971582, 
  0.6336717919696933,0.8188056885285723, 0.6335268889757025,0.8260045710100508, 
  0.6368436918902969,0.9011394538695875, 0.6694322619932419,0.9076855794178548, 
  0.6721028612739068,0.9141848318835066, 0.6747286423328992,0.743313384339753, 
  0.5965599314918484,0.7512360368761378, 0.6006322879247505,0.8042146256076498, 
  0.6266956191442178,0.8115429500673398, 0.6301448949957691,0.6946186514103458, 
  0.5704923320488575,0.6773577189060671, 0.5608131655020115,0.9203598065681287, 
  0.6771999025798467,0.9139044686308163, 0.6746158972911807,0.8115429500673398, 
  0.6301448949957691,1.005902975322545, 0.7091845856564688,1.084957117605057, 
  0.7353095354704546,1.079486522046073, 0.7335995795599592,1.241445335340372, 
  0.7787702085944065,1.384587263410152, 0.8106735842010665,1.158187394248443, 
  0.7569047113242585,1.310455420666893, 0.7949759381377488,0.5736533751382995, 
  0.4975930280551744,0.1002946741814076, 0.09979401498688548,0.1418380884611374, 
  0.1404325089851156,0.399582000428193, 0.3710560325949917,0.5650942842857224, 
  0.4919760264644775,0.4119766944080103, 0.3809173903696917,0.4240092197163947, 
  0.390367885687604,0.528706788677659, 0.4674008555921098,0.6627191286359849, 
  0.5524200558696083,0.3873527569736796, 0.3612016877012563,0.4001264550167425, 
  0.3714918857124475,0.5739255803686986, 0.4977706489794756,0.5826230286134442, 
  0.5034130377945829,0.5474019654076446, 0.480168126921973,0.5477995223347872, 
  0.4804363839826959,0.5291183925473689, 0.467685165825284,0.5385399660202261, 
  0.4741533524732329,0.4997891638725748, 0.4470627143634845,0.5097531068994108, 
  0.4541515140359511,0.5654794028702209, 0.4922300980446926,0.6476990080738109, 
  0.5436299699910937,0.6398866957443669, 0.5389859890607164,0.6630475461066936, 
  0.5526102282866346,0.6323222016184138, 0.5344419874327166,0.6323222016184138, 
  0.5344419874327166,0.6480350376313517, 0.5438286106158655,0.7347206974936216, 
  0.5920904706139487,0.7415345743757795, 0.5956392048037292,0.7482864068415404, 
  0.5991215571686956,0.7549778594985272, 0.6025395456972513,0.7546894483129528, 
  0.6023929043408881,0.7613246252841581, 0.6057511222400337,0.8305761022691565, 
  0.6389317903146783,0.8366096361683739, 0.6416662131644668,0.8425999673622909, 
  0.6443570653832366,0.6851085377277847, 0.5651884098255143,0.6924108102395991, 
  0.5692672878263071,0.7412409325290932, 0.595486987130373,0.7479954155780331, 
  0.5989721703931621,0.6402268256865785, 0.5391892095316402,0.6243175033509818, 
  0.5295825643940062,0.8482914132124759, 0.6468916786733294,0.8423415578379503, 
  0.6442414789297407,0.7479954155780331, 0.5989721703931621,0.9271361595774243, 
  0.6798857142203814,0.9216954142919561, 0.6777314269282665,0.9949577771598377, 
  0.7053173276322738,1.144234472677361, 0.7529695836889484,1.276167731372186, 
  0.7871276534279429,1.06749601016954, 0.7298023721131245,1.207840751862711, 
  0.7702666141409265,0.5287336852580738, 0.4674194385001023,0.1008029450935138, 
  0.1002946741814077,0.1425568920783976, 0.1411300494381173,0.4016069923779299, 
  0.3726758610538909,0.5679580553647366, 0.4938623138695294,0.4140644998866391, 
  0.3825658753017493,0.4261580033343249, 0.3920427724659123,0.5313861560908463, 
  0.4692489936414508,0.6660776405283786, 0.5543607582761888,0.3893157738606754, 
  0.3627917795487722,0.40215420614021, 0.3731130042164349,0.5768341064753532, 
  0.4996646300767503,0.5855756314369179, 0.5053140215095364,0.5501760757830689, 
  0.4820371860073105,0.550575647439544, 0.4823058572936438,0.5317998458766429, 
  0.4695337952621265,0.54126916577055, 0.476012848654026,0.5023219832496317, 
  0.4488727567194011,0.5123364213047608, 0.4559754874632953,0.5683451256438372, 
  0.4941167333751283,0.6509814013642908, 0.5455664148075565,0.6431294979883055, 
  0.5409196675366167,0.666407722345163, 0.554551008538001,0.6355266687028797, 
  0.5363726169120175,0.6355266687028797, 0.5363726169120175,0.6513191338442559, 
  0.5457651658293833,0.7384440971866391, 0.5940339485808005,0.7452925052684458, 
  0.5975815797981942,0.75207855450667, 0.6010626284171989,0.7588039179447931, 
  0.6044791227998771,0.7585140451560224, 0.6043325499254607,0.7651828477158112, 
  0.6076891007239634,0.8347852756526836, 0.640841971527508,0.8408493861483476, 
  0.6435731670718704,0.8468700749970912, 0.6462607014020229,0.6885805141233883, 
  0.5671329797915116,0.6959197929143528, 0.5712125060990083,0.7449973753107459, 
  0.5974294141009717,0.7517860885647103, 0.6009133020894143,0.6434713516327715, 
  0.5411230164810179,0.6274814044201261, 0.5315095618894596,0.8525903638182224, 
  0.6487920600927325,0.8466103559112438, 0.6461452603728914,0.7517860885647103, 
  0.6009133020894143,0.9318346776724395, 0.6817320588167372,0.9263663599102536, 
  0.6795819721486519,1.005067775694518, 0.7088917176421711,1.150033196326825, 
  0.754614900930355,1.282635063183362, 0.7886380618067345,1.072905840503872, 
  0.7315240175503172,1.21396181786785, 0.7718470053684059,0.5314131889770977, 
  0.4692676087023754,0.08765220466285295, 
  0.08731742056405194,0.1239589366061089, 0.1230174081070795,0.3492133911096235, 
  0.3296887584217195,0.4938623138695295, 0.4428056519483617,0.3600456936453226, 
  0.3387575274410803,0.3705614800472388, 0.3474719360145255,0.4620615802987943, 
  0.4194496216703124,0.5791812294252137, 0.5011878296333783,0.338525683522997, 
  0.3206506870014021,0.3496892154284587, 0.3300890623696886,0.5015803963900746, 
  0.4483433627995614,0.5091815030272436, 0.4537471537849062,0.4784001692649539, 
  0.4315577626120113,0.4787476128498446, 0.4318127619173404,0.462421300163506, 
  0.4197186605373677,0.4706552536912406, 0.4258466797760554,0.4367891160481581, 
  0.4002719705622905,0.4454970716855387, 0.4069412338205409,0.4941988870052763, 
  0.4430482091617476,0.5660544438573668, 0.4926092317547253,0.5592268988777401, 
  0.4880896136942804,0.5794682488067747, 0.501373776233942,0.5526159338119411, 
  0.4836756218854957,0.5526159338119411, 0.4836756218854957,0.5663481157974846, 
  0.492802746231953,0.6421067666091811, 0.5403107370876641,0.6480617321733756, 
  0.5438443869185697,0.6539624742214302, 0.5473175576100414,0.659810447531769, 
  0.5507319945151001,0.6595583915131285, 0.5505853938606564,0.6653571828707073, 
  0.553945203892834,0.7258792861970988, 0.5874338939386764,0.7311522735465358, 
  0.5902181381885688,0.7363875040320327, 0.5929617641088303,0.5987483807621345, 
  0.5137059438141059,0.6051301781001643, 0.5177194102438392,0.6478051048354496, 
  0.5436926979373978,0.6537081633520623, 0.5471684497753383,0.5595241543357197, 
  0.4882872113917504,0.5456202537668347, 0.4789642363275312,0.7413615246423957, 
  0.5955495072743097,0.7361616678677577, 0.5928438340345232,0.6537081633520623, 
  0.5471684497753383,0.8102676345767188, 0.629547294574581,0.8055127129104124, 
  0.6273093034586882,0.8739467511935112, 0.6580553214124871,0.8695401169235577, 
  0.6561679291872398,1.115302642810715, 0.7445450548588899,0.932934669999705, 
  0.682162443535217,1.055588501049544, 0.7259634007512483,0.4620850864778661, 
  0.4194672056046497,0.07859051104008631, 0.0783489236783827,0.1111437665867225, 
  0.1104635858467468,0.3131108792404168, 0.298806051780994,0.4428056519483617, 
  0.4048867960584389,0.3228233125476671, 0.3072119392427826,0.3322519519126873, 
  0.3153040173193061,0.4142925539334651, 0.3827457209603299,0.5193040948648681, 
  0.4608665255738079,0.3035280923121154, 0.2904436343329053,0.3135375117082069, 
  0.2991767737458396,0.4497258207207538, 0.4101567201889244,0.4565411068551193, 
  0.4153069293786624,0.4289420206692239, 0.3942070124893932,0.4292535447090263, 
  0.3944487785258824,0.4146150851020501, 0.3829999979329273,0.4219977929085911, 
  0.3887965424036813,0.3916328172121872, 0.3646645714863789,0.399440523661654, 
  0.3709427358012932,0.4431074293519358, 0.4051174576600637,0.5075343876446238, 
  0.4525803936813259,0.5014126905217511, 0.4482235777513178,0.5195614414993543, 
  0.4610463754624705,0.4954851827655258, 0.4439743041786315,0.4954851827655258, 
  0.4439743041786315,0.5077976990803231, 0.4527670708814122,0.5757242401855914, 
  0.4989427474957993,0.581063567230659, 0.5024060439173192,0.5863542765157942, 
  0.5058141175212589,0.5915976724209642, 0.5091685068787384,0.5913716745536899, 
  0.5090244035253629,0.5965709730534811, 0.5123288122369503,0.650836157231533, 
  0.5454809122712606,0.6555640105935122, 0.5482553754551818,0.6602580104860467, 
  0.5509921802358871,0.5368483475061143, 0.4729975996887851,0.5425703794399281, 
  0.4768971694149601,0.5808334706379716, 0.5022572898235234,0.5861262569096298, 
  0.5056677214063265,0.5016792150027031, 0.4484139333532318,0.4892127327805814, 
  0.4394449000631895,0.6647178050022936, 0.5535760686137837,0.6600555218021628, 
  0.550874485597485,0.5861262569096298, 0.5056677214063265,0.7265002372223682, 
  0.587762858288006,0.7222368906796547, 0.5854983688095095,0.7835960551398371, 
  0.6167901241176129,0.7796449892131501, 0.6148577131903911,0.8966176189449918, 
  0.6675722142848463,0.8364856624463671, 0.6416102724104429,0.9464592483967555, 
  0.6873965775497565,0.4143136299877749, 0.3827623395476286,0.09395320753047012, 
  0.09354126066645101,0.1328699003180449, 0.1317123351873929,0.3743170902950084, 
  0.3505626581782456,0.5293643057232353, 0.4678549580433047,0.3859280882394868, 
  0.3600456936453226,0.3971998168396464, 0.3691461352578503,0.4952775313827071, 
  0.4438248974595379,0.6208164923546007, 0.5274406501474991,0.3628610817122961, 
  0.3410993104942889,0.3748271199188784, 0.3509815137295726,0.5376372135363274, 
  0.4735368755736141,0.5457847364890027, 0.4790754896631518,0.5127906429558515, 
  0.4562956028385939,0.5131630630149011, 0.456557935958373,0.4956631102193386, 
  0.4441022948571234,0.5044889731575625, 0.4504169920364663,0.468188320247865, 
  0.4240167806041205,0.4775222596086815, 0.4309129771623544,0.5297250738955093, 
  0.4681039588619725,0.6067460692156995, 0.5187302507403793,0.5994277164958581, 
  0.5141347885724921,0.6211241445308898, 0.5276292760089881,0.5923415128436762, 
  0.5096425006468438,0.5923415128436762, 0.5096425006468438,0.6070608521791389, 
  0.5189269143861106,0.6882655208958888, 0.5669569494271816,0.6946485675932491, 
  0.5705089052790049,0.7009734928402188, 0.5739972231966484,0.7072418559940298, 
  0.5774237875796608,0.7069716805715206, 0.5772767227038945,0.7131873262582445, 
  0.5806458950626004,0.7780601466952968, 0.6140794617122592,0.7837121902080957, 
  0.6168467561516776,0.7893237626513179, 0.6195717905885864,0.6417902560769061, 
  0.5401221156888094,0.6486308179546546, 0.5441805758035323,0.6943734922356937, 
  0.5703564903536013,0.7007009004738445, 0.5738475232167518,0.5997463405834159, 
  0.514335793906227,0.5848429384310556, 0.5048429734567402,0.7946553477775996, 
  0.6221402743562953,0.7890816919345387, 0.6194546982922653,0.7007009004738445, 
  0.5738475232167518,0.8685148710112521, 0.6557270344571309,0.8634181350669143, 
  0.6535252542749522,0.9367716511101335, 0.6836581731085036,0.9320482396948895, 
  0.6818156728523065,1.071886416227104, 0.7312006568856881,1.195477752810996, 
  0.7670308066416872,1.131470975400537, 0.7492977303145251,0.4953027273367515, 
  0.4438430281099736,0.08303633904282076, 
  0.08275154179053497,0.1174311168441677, 0.1166297045348832,0.3308234134441684, 
  0.3140823414477513,0.4678549580433047, 0.4237691027644349,0.34108527450549, 
  0.3228233125476671,0.3510472875356249, 0.3312306088596752,0.4377288875725516, 
  0.4009948198262612,0.5486808816592343, 0.48103061286049,0.320698532796075, 
  0.3053790444772503,0.3312741803086825, 0.314468002904072,0.4751665974869621, 
  0.4291795839209842,0.482367421131414, 0.4344632910000824,0.4532070677061117, 
  0.4127923922061843,0.4535362145133621, 0.4130410568559408,0.4380696641766488, 
  0.4012567520147994,0.4458700082686389, 0.4072254214677558,0.413787300272663, 
  0.3823472144004753,0.4220366849796037, 0.388826957460903,0.4681738068517297, 
  0.4240059995782812,0.536245367673628, 0.4725850384615545,0.5297773690426859, 
  0.4681400435245802,0.5489527862709989, 0.4812138023421074,0.5235145449789281, 
  0.4638019147035395,0.5235145449789281, 0.4638019147035395,0.5365235745125864, 
  0.4727754273328686,0.6082926878899787, 0.519695723757695,0.6139340581382087, 
  0.5232005076457957,0.6195240603428439, 0.5266473893636374,0.6250640726720085, 
  0.5300380096087812,0.6248252902123759, 0.529892390031717,0.6303187105668163, 
  0.5332306552700912,0.6876536505232633, 0.5666147905731642,0.6926489563116403, 
  0.5693996085841103,0.6976084935558331, 0.5721452284886991,0.5672176043664879, 
  0.4933752652632968,0.573263328937838, 0.4973384037158874,0.6136909450902067, 
  0.5230500139972163,0.6192831417755091, 0.5264993675207292,0.5300589706873458, 
  0.4683343138470952,0.5168872654678773, 0.4591747226232435,0.7023205765364808, 
  0.5747361589442732,0.6973945501829654, 0.5720271834334805,0.6192831417755091, 
  0.5264993675207292,0.7675980116978259, 0.6088967365474212,0.7630934896595707, 
  0.6066409628289259,0.8279237130042326, 0.6377219951827545,0.8237491371486108, 
  0.6358083046829633,0.9473388531664808, 0.6877333355085258,1.056569526573848, 
  0.7262822765102296,0.8838052603567698, 0.6622332756253655,0.4377511558892758, 
  0.4010119389969542,0.1896884518194713, 0.1863652122085029,0.2682599811886517, 
  0.2590991018259489,0.7557339575086043, 0.6029236907598284,1.06876921225456, 
  0.7302088160249892,0.7791761824422541, 0.614627688123463,0.801933433670746, 
  0.6256143303713364,0.9999491301932054, 0.7070887953076815,1.253408184713091, 
  0.7816976002756334,0.7326046510250497, 0.5909813460876593,0.7567636906308354, 
  0.6034461858944041,1.085471942436515, 0.7354697364767042,1.101921524687929, 
  0.7405241027731794,1.035307529423738, 0.7192651088602782,1.036059433337233, 
  0.7195170948933813,1.000727601248079, 0.707363886746546,1.018546729734773, 
  0.7135736711020348,0.9452568992812114, 0.6869355381784603,0.9641018174406674, 
  0.6940669665205846,1.069497591389911, 0.7304409882210317,1.225000460785226, 
  0.7746613166692456,1.21022494610314, 0.7708838813425288,1.254029324390523, 
  0.7818482022920522,1.195918132792653, 0.7671470853826782,1.195918132792653, 
  0.7671470853826782,1.225635997288592, 0.7748219606876592,1.389585566380182, 
  0.8116732518635268,1.402472728806446, 0.8142181761028898,1.415242545926128, 
  0.8166944649009388,1.427898166030455, 0.8191048908267026,1.427352690692651, 
  0.8190018833009617,1.439901875955864, 0.821351754649312,1.570877977755009, 
  0.8435762534252187,1.582289268670345, 0.8453298136435385,1.593618849820434, 
  0.8470442022611988,1.29575352739893, 0.7916585211726875,1.309564398004327, 
  0.794776887339221,1.401917360660112, 0.8141094606929198,1.414692190857743, 
  0.8165886618561977,1.210868237710418, 0.7710500582700826,1.180778756409767, 
  0.7631059303476994,1.604383145738911, 0.8486489260126695,1.593130116963902, 
  0.8469707881167792,1.414692190857743, 0.8165886618561977,1.753503106436071, 
  0.868669997671931,1.743212963331584, 0.8674106169691967,1.891311312068007, 
  0.8840356715157195,1.88177489897244, 0.8830560069569509,2.164103601832863, 
  0.9077704904997884,2.41363046644038, 0.9238468441381538,2.018967279621115, 
  0.8961045510419915,2.284402877174672, 0.9160728272260362}; 

void sse_viscosity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *viscosity) 
{
  __m128d *mole_frac = (__m128d*)malloc(14*52*sizeof(__m128d));
  __m128d *spec_visc = (__m128d*)malloc(14*52*sizeof(__m128d));
  __m128d *result_vec = (__m128d*)malloc(14*sizeof(__m128d));
  __m128d *spec_sum = (__m128d*)malloc(14*sizeof(__m128d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 28)
  {
    for (unsigned idx = 0; idx < 14; idx++)
    {
      __m128d temp = _mm_load_pd(temperature_array);
      temperature_array += 2;
      mole_frac[0+idx] = _mm_load_pd(mass_frac_array+(0*spec_stride)+(idx<<1));
      mole_frac[14+idx] = _mm_load_pd(mass_frac_array+(1*spec_stride)+(idx<<1));
      mole_frac[28+idx] = _mm_load_pd(mass_frac_array+(2*spec_stride)+(idx<<1));
      mole_frac[42+idx] = _mm_load_pd(mass_frac_array+(3*spec_stride)+(idx<<1));
      mole_frac[56+idx] = _mm_load_pd(mass_frac_array+(4*spec_stride)+(idx<<1));
      mole_frac[70+idx] = _mm_load_pd(mass_frac_array+(5*spec_stride)+(idx<<1));
      mole_frac[84+idx] = _mm_load_pd(mass_frac_array+(6*spec_stride)+(idx<<1));
      mole_frac[98+idx] = _mm_load_pd(mass_frac_array+(7*spec_stride)+(idx<<1));
      mole_frac[112+idx] = 
        _mm_load_pd(mass_frac_array+(8*spec_stride)+(idx<<1)); 
      mole_frac[126+idx] = 
        _mm_load_pd(mass_frac_array+(9*spec_stride)+(idx<<1)); 
      mole_frac[140+idx] = 
        _mm_load_pd(mass_frac_array+(10*spec_stride)+(idx<<1)); 
      mole_frac[154+idx] = 
        _mm_load_pd(mass_frac_array+(11*spec_stride)+(idx<<1)); 
      mole_frac[168+idx] = 
        _mm_load_pd(mass_frac_array+(12*spec_stride)+(idx<<1)); 
      mole_frac[182+idx] = 
        _mm_load_pd(mass_frac_array+(13*spec_stride)+(idx<<1)); 
      mole_frac[196+idx] = 
        _mm_load_pd(mass_frac_array+(14*spec_stride)+(idx<<1)); 
      mole_frac[210+idx] = 
        _mm_load_pd(mass_frac_array+(15*spec_stride)+(idx<<1)); 
      mole_frac[224+idx] = 
        _mm_load_pd(mass_frac_array+(16*spec_stride)+(idx<<1)); 
      mole_frac[238+idx] = 
        _mm_load_pd(mass_frac_array+(17*spec_stride)+(idx<<1)); 
      mole_frac[252+idx] = 
        _mm_load_pd(mass_frac_array+(18*spec_stride)+(idx<<1)); 
      mole_frac[266+idx] = 
        _mm_load_pd(mass_frac_array+(19*spec_stride)+(idx<<1)); 
      mole_frac[280+idx] = 
        _mm_load_pd(mass_frac_array+(20*spec_stride)+(idx<<1)); 
      mole_frac[294+idx] = 
        _mm_load_pd(mass_frac_array+(21*spec_stride)+(idx<<1)); 
      mole_frac[308+idx] = 
        _mm_load_pd(mass_frac_array+(22*spec_stride)+(idx<<1)); 
      mole_frac[322+idx] = 
        _mm_load_pd(mass_frac_array+(23*spec_stride)+(idx<<1)); 
      mole_frac[336+idx] = 
        _mm_load_pd(mass_frac_array+(24*spec_stride)+(idx<<1)); 
      mole_frac[350+idx] = 
        _mm_load_pd(mass_frac_array+(25*spec_stride)+(idx<<1)); 
      mole_frac[364+idx] = 
        _mm_load_pd(mass_frac_array+(26*spec_stride)+(idx<<1)); 
      mole_frac[378+idx] = 
        _mm_load_pd(mass_frac_array+(27*spec_stride)+(idx<<1)); 
      mole_frac[392+idx] = 
        _mm_load_pd(mass_frac_array+(28*spec_stride)+(idx<<1)); 
      mole_frac[406+idx] = 
        _mm_load_pd(mass_frac_array+(29*spec_stride)+(idx<<1)); 
      mole_frac[420+idx] = 
        _mm_load_pd(mass_frac_array+(30*spec_stride)+(idx<<1)); 
      mole_frac[434+idx] = 
        _mm_load_pd(mass_frac_array+(31*spec_stride)+(idx<<1)); 
      mole_frac[448+idx] = 
        _mm_load_pd(mass_frac_array+(32*spec_stride)+(idx<<1)); 
      mole_frac[462+idx] = 
        _mm_load_pd(mass_frac_array+(33*spec_stride)+(idx<<1)); 
      mole_frac[476+idx] = 
        _mm_load_pd(mass_frac_array+(34*spec_stride)+(idx<<1)); 
      mole_frac[490+idx] = 
        _mm_load_pd(mass_frac_array+(35*spec_stride)+(idx<<1)); 
      mole_frac[504+idx] = 
        _mm_load_pd(mass_frac_array+(36*spec_stride)+(idx<<1)); 
      mole_frac[518+idx] = 
        _mm_load_pd(mass_frac_array+(37*spec_stride)+(idx<<1)); 
      mole_frac[532+idx] = 
        _mm_load_pd(mass_frac_array+(38*spec_stride)+(idx<<1)); 
      mole_frac[546+idx] = 
        _mm_load_pd(mass_frac_array+(39*spec_stride)+(idx<<1)); 
      mole_frac[560+idx] = 
        _mm_load_pd(mass_frac_array+(40*spec_stride)+(idx<<1)); 
      mole_frac[574+idx] = 
        _mm_load_pd(mass_frac_array+(41*spec_stride)+(idx<<1)); 
      mole_frac[588+idx] = 
        _mm_load_pd(mass_frac_array+(42*spec_stride)+(idx<<1)); 
      mole_frac[602+idx] = 
        _mm_load_pd(mass_frac_array+(43*spec_stride)+(idx<<1)); 
      mole_frac[616+idx] = 
        _mm_load_pd(mass_frac_array+(44*spec_stride)+(idx<<1)); 
      mole_frac[630+idx] = 
        _mm_load_pd(mass_frac_array+(45*spec_stride)+(idx<<1)); 
      mole_frac[644+idx] = 
        _mm_load_pd(mass_frac_array+(46*spec_stride)+(idx<<1)); 
      mole_frac[658+idx] = 
        _mm_load_pd(mass_frac_array+(47*spec_stride)+(idx<<1)); 
      mole_frac[672+idx] = 
        _mm_load_pd(mass_frac_array+(48*spec_stride)+(idx<<1)); 
      mole_frac[686+idx] = 
        _mm_load_pd(mass_frac_array+(49*spec_stride)+(idx<<1)); 
      mole_frac[700+idx] = 
        _mm_load_pd(mass_frac_array+(50*spec_stride)+(idx<<1)); 
      mole_frac[714+idx] = 
        _mm_load_pd(mass_frac_array+(51*spec_stride)+(idx<<1)); 
      __m128d mixmw = _mm_load_pd(mixmw_array);
      mixmw_array += 2;
      result_vec[idx] = _mm_set1_pd(0.0);
      temp = _mm_mul_pd(temp,_mm_set1_pd(120.0));
      double t1 = _mm_cvtsd_f64(temp);
      double log1 = log(t1);
      double t0 = _mm_cvtsd_f64(_mm_shuffle_pd(temp,temp,1));
      double log0 = log(t0);
      __m128d logt = _mm_set_pd(log0,log1);
      // Species H
      {
        __m128d val = _mm_set1_pd(0.01473938862116986);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3401776285106742));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.261058144879592));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-19.52716556548602));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[0+idx] = val;
      }
      // Species H2
      {
        __m128d val = _mm_set1_pd(1.271284683469859e-03);
        val = 
          _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.02829817903088695)); 
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(0.8541054785785867));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-13.50231595822253));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[14+idx] = val;
      }
      // Species O
      {
        __m128d val = _mm_set1_pd(6.048679323975888e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.1372174134188812));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(1.680892288317397));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-14.54397903871408));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[28+idx] = val;
      }
      // Species O2
      {
        __m128d val = _mm_set1_pd(0.0100847718313627);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.2301633310424902));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.39404370945068));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-16.52174380450916));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[42+idx] = val;
      }
      // Species OH
      {
        __m128d val = _mm_set1_pd(6.048679323975597e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.137217413418875));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(1.680892288317354));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-14.51343125972167));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[56+idx] = val;
      }
      // Species H2O
      {
        __m128d val = _mm_set1_pd(-0.06210023880608071);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(1.238210189354115));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-7.126095134578899));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.553853497038316));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[70+idx] = val;
      }
      // Species CO
      {
        __m128d val = _mm_set1_pd(8.68673668813154e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.1978066221878029));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.144532361494317));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-16.0420622318806));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[84+idx] = val;
      }
      // Species CO2
      {
        __m128d val = _mm_set1_pd(0.02235862750234295);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5304157271437536));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.845156016570812));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.32881511990188));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[98+idx] = val;
      }
      // Species CH3
      {
        __m128d val = _mm_set1_pd(0.01464383830637072);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3378794809999195));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.242607583590146));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-19.35981201106423));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[112+idx] = val;
      }
      // Species CH4
      {
        __m128d val = _mm_set1_pd(0.0144008482711447);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3320119047555915));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.195329576946196));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-19.16897626263594));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[126+idx] = val;
      }
      // Species HO2
      {
        __m128d val = _mm_set1_pd(0.01008477183136259);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.2301633310424879));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.394043709450665));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-16.50623665958695));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[140+idx] = val;
      }
      // Species H2O2
      {
        __m128d val = _mm_set1_pd(0.01008477183136263);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.2301633310424887));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.394043709450671));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-16.49119602551682));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[154+idx] = val;
      }
      // Species CH2O
      {
        __m128d val = _mm_set1_pd(0.01025079016985108);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3105955692469765));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.67086047971521));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.01291420235239));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[168+idx] = val;
      }
      // Species C2H6
      {
        __m128d val = _mm_set1_pd(0.02241484422225297);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5324773728006829));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.867444393688489));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.8864340411546));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[182+idx] = val;
      }
      // Species C2H4
      {
        __m128d val = _mm_set1_pd(0.02219608533476465);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5255368807469865));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.798106645330391));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.25594298587761));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[196+idx] = val;
      }
      // Species C2H5
      {
        __m128d val = _mm_set1_pd(0.0224148442222536);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5324773728006966));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.867444393688588));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.90348171486313));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[210+idx] = val;
      }
      // Species C2H
      {
        __m128d val = _mm_set1_pd(0.02308471478059632);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5512760981276462));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.040498151655746));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.2004878222601));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[224+idx] = val;
      }
      // Species C2H2
      {
        __m128d val = _mm_set1_pd(0.02308471478059636);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.551276098127647));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.040498151655751));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.18074765511536));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[238+idx] = val;
      }
      // Species CH3OH
      {
        __m128d val = _mm_set1_pd(0.01194195005178133);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3457594494399707));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.907123245251647));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.5002104811602));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[252+idx] = val;
      }
      // Species CH2CO
      {
        __m128d val = _mm_set1_pd(0.01620162697675229);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4328513924391304));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.478564425409844));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.70763362444367));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[266+idx] = val;
      }
      // Species HCCO
      {
        __m128d val = _mm_set1_pd(0.01522307629912794);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.351796228499638));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.35420427277198));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-18.32506377331114));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[280+idx] = val;
      }
      // Species CH3CHO
      {
        __m128d val = _mm_set1_pd(0.01620162697675266);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4328513924391382));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.478564425409899));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.68421301797287));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[294+idx] = val;
      }
      // Species C3H4-A
      {
        __m128d val = _mm_set1_pd(0.02258284368884785);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.553160052068685));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.169968650910175));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.93873939706588));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[308+idx] = val;
      }
      // Species C3H4-P
      {
        __m128d val = _mm_set1_pd(0.02258284368884785);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.553160052068685));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.169968650910175));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.93873939706588));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[322+idx] = val;
      }
      // Species C3H6
      {
        __m128d val = _mm_set1_pd(0.02287503877385922);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5560497395691175));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.159086829883717));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.71239478064636));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[336+idx] = val;
      }
      // Species C4H6
      {
        __m128d val = _mm_set1_pd(0.02142240280637452);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5341371089626625));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.090413145400232));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.98120854465839));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[350+idx] = val;
      }
      // Species C4H7
      {
        __m128d val = _mm_set1_pd(0.02150485993261591);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5355860444732981));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.097610274266972));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.9482793621001));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[364+idx] = val;
      }
      // Species C4H8-1
      {
        __m128d val = _mm_set1_pd(0.02150485993261587);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5355860444732972));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.097610274266965));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.93921534866988));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[378+idx] = val;
      }
      // Species PC4H9
      {
        __m128d val = _mm_set1_pd(0.02162777198022059);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.53773838185872));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.108213569249132));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-25.17790518947539));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[392+idx] = val;
      }
      // Species CH3COCH2
      {
        __m128d val = _mm_set1_pd(0.01624205095928127);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4336631947942615));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.483746957928423));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.96931095870644));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[406+idx] = val;
      }
      // Species C2H5CHO
      {
        __m128d val = _mm_set1_pd(0.02000879758404488);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5168490196310973));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.151113874438199));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-25.82663397850021));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[420+idx] = val;
      }
      // Species C5H9
      {
        __m128d val = _mm_set1_pd(0.01915723389629229);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4915124228316154));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.846391809456143));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.78184868007241));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[434+idx] = val;
      }
      // Species C5H10-1
      {
        __m128d val = _mm_set1_pd(9.823559104095006e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.295979744803851));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.520120470878764));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-21.91911480147456));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[448+idx] = val;
      }
      // Species C5H11-1
      {
        __m128d val = _mm_set1_pd(0.01580442210718414);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4248519106427305));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.427246798629395));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.82208326587578));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[462+idx] = val;
      }
      // Species CH3O2
      {
        __m128d val = _mm_set1_pd(0.01194195005178086);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3457594494399605));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.907123245251574));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.30830707824042));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[476+idx] = val;
      }
      // Species CH3O2H
      {
        __m128d val = _mm_set1_pd(0.01194195005178048);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3457594494399524));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.907123245251516));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.29770491729938));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[490+idx] = val;
      }
      // Species C2H3CO
      {
        __m128d val = _mm_set1_pd(0.01558871094841275);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4204941660044574));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.399143408361484));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.49129347016926));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[504+idx] = val;
      }
      // Species C2H3CHO
      {
        __m128d val = _mm_set1_pd(0.02340061661233695);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5955476968700483));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.751833071084701));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-27.47741799780282));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[518+idx] = val;
      }
      // Species C3H5-A
      {
        __m128d val = _mm_set1_pd(0.02271080873650039);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5541553453533459));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.160772514327559));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.81744375381571));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[532+idx] = val;
      }
      // Species C3H3
      {
        __m128d val = _mm_set1_pd(0.02258284368884768);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5531600520686816));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.169968650910151));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.95147941555229));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[546+idx] = val;
      }
      // Species NC3H7CHO
      {
        __m128d val = _mm_set1_pd(-0.01572518516534323);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(0.248962293644394));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3165817798945547));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-12.81021987594491));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[560+idx] = val;
      }
      // Species C2H5COCH2
      {
        __m128d val = _mm_set1_pd(-7.89459267319049e-04);
        val = 
          _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.06762206191012728)); 
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(1.952157119077493));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-18.56615649146089));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[574+idx] = val;
      }
      // Species CH3CHCO
      {
        __m128d val = _mm_set1_pd(0.01558871094841311);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4204941660044652));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.399143408361541));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.48222233868143));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[588+idx] = val;
      }
      // Species NC3H7COCH3
      {
        __m128d val = _mm_set1_pd(1.820340891942987e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.1229676616478414));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.32561444740514));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-19.3765420216231));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[602+idx] = val;
      }
      // Species NC3H7COCH2
      {
        __m128d val = _mm_set1_pd(0.01119525229819807);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3302621812756928));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.803279455428719));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.57017928153328));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[616+idx] = val;
      }
      // Species NC7H16
      {
        __m128d val = _mm_set1_pd(0.01407613802539561);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3896780138463274));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.197919285611414));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.62032594105494));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[630+idx] = val;
      }
      // Species C7H15-1
      {
        __m128d val = _mm_set1_pd(0.01407613802539558);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3896780138463268));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.197919285611412));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.62538091879411));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[644+idx] = val;
      }
      // Species C7H15O2
      {
        __m128d val = _mm_set1_pd(9.014132110063371e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.2788560203411342));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.425986209058756));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-21.87023237436892));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[658+idx] = val;
      }
      // Species C7H14OOHO2
      {
        __m128d val = _mm_set1_pd(5.413054249775649e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.216168047330524));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.117418074055381));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-21.74516858824146));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[672+idx] = val;
      }
      // Species C7H14O
      {
        __m128d val = _mm_set1_pd(8.872569026068563e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.2819477382149219));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.478518292881485));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.06257892272827));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[686+idx] = val;
      }
      // Species NC7KET
      {
        __m128d val = _mm_set1_pd(-0.01928649818698724);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(0.3290478605967052));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.892645465051813));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-11.73853058625284));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[700+idx] = val;
      }
      // Species N2
      {
        __m128d val = _mm_set1_pd(8.617661554832269e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.1961905296416808));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.13193508629056));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-15.99249280222402));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[714+idx] = val;
      }
      mole_frac[0+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[0+idx],_mm_set1_pd(recip_molecular_masses[0])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[14+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[14+idx],_mm_set1_pd(recip_molecular_masses[1])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[28+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[28+idx],_mm_set1_pd(recip_molecular_masses[2])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[42+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[42+idx],_mm_set1_pd(recip_molecular_masses[3])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[56+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[56+idx],_mm_set1_pd(recip_molecular_masses[4])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[70+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[70+idx],_mm_set1_pd(recip_molecular_masses[5])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[84+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[84+idx],_mm_set1_pd(recip_molecular_masses[6])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[98+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[98+idx],_mm_set1_pd(recip_molecular_masses[7])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[112+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[112+idx],_mm_set1_pd(recip_molecular_masses[8])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[126+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[126+idx],_mm_set1_pd(recip_molecular_masses[9])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[140+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[140+idx],_mm_set1_pd(recip_molecular_masses[10])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[154+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[154+idx],_mm_set1_pd(recip_molecular_masses[11])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[168+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[168+idx],_mm_set1_pd(recip_molecular_masses[12])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[182+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[182+idx],_mm_set1_pd(recip_molecular_masses[13])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[196+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[196+idx],_mm_set1_pd(recip_molecular_masses[14])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[210+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[210+idx],_mm_set1_pd(recip_molecular_masses[15])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[224+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[224+idx],_mm_set1_pd(recip_molecular_masses[16])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[238+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[238+idx],_mm_set1_pd(recip_molecular_masses[17])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[252+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[252+idx],_mm_set1_pd(recip_molecular_masses[18])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[266+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[266+idx],_mm_set1_pd(recip_molecular_masses[19])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[280+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[280+idx],_mm_set1_pd(recip_molecular_masses[20])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[294+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[294+idx],_mm_set1_pd(recip_molecular_masses[21])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[308+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[308+idx],_mm_set1_pd(recip_molecular_masses[22])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[322+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[322+idx],_mm_set1_pd(recip_molecular_masses[23])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[336+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[336+idx],_mm_set1_pd(recip_molecular_masses[24])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[350+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[350+idx],_mm_set1_pd(recip_molecular_masses[25])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[364+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[364+idx],_mm_set1_pd(recip_molecular_masses[26])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[378+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[378+idx],_mm_set1_pd(recip_molecular_masses[27])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[392+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[392+idx],_mm_set1_pd(recip_molecular_masses[28])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[406+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[406+idx],_mm_set1_pd(recip_molecular_masses[29])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[420+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[420+idx],_mm_set1_pd(recip_molecular_masses[30])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[434+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[434+idx],_mm_set1_pd(recip_molecular_masses[31])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[448+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[448+idx],_mm_set1_pd(recip_molecular_masses[32])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[462+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[462+idx],_mm_set1_pd(recip_molecular_masses[33])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[476+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[476+idx],_mm_set1_pd(recip_molecular_masses[34])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[490+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[490+idx],_mm_set1_pd(recip_molecular_masses[35])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[504+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[504+idx],_mm_set1_pd(recip_molecular_masses[36])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[518+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[518+idx],_mm_set1_pd(recip_molecular_masses[37])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[532+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[532+idx],_mm_set1_pd(recip_molecular_masses[38])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[546+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[546+idx],_mm_set1_pd(recip_molecular_masses[39])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[560+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[560+idx],_mm_set1_pd(recip_molecular_masses[40])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[574+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[574+idx],_mm_set1_pd(recip_molecular_masses[41])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[588+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[588+idx],_mm_set1_pd(recip_molecular_masses[42])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[602+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[602+idx],_mm_set1_pd(recip_molecular_masses[43])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[616+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[616+idx],_mm_set1_pd(recip_molecular_masses[44])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[630+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[630+idx],_mm_set1_pd(recip_molecular_masses[45])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[644+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[644+idx],_mm_set1_pd(recip_molecular_masses[46])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[658+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[658+idx],_mm_set1_pd(recip_molecular_masses[47])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[672+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[672+idx],_mm_set1_pd(recip_molecular_masses[48])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[686+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[686+idx],_mm_set1_pd(recip_molecular_masses[49])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[700+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[700+idx],_mm_set1_pd(recip_molecular_masses[50])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[714+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[714+idx],_mm_set1_pd(recip_molecular_masses[51])),_mm_set1_pd(1e3)),mixmw); 
    }
    int visc_idx = 0;
    for (int spec_idx = 0; spec_idx < 52; spec_idx++)
    {
      spec_sum[0] = _mm_set1_pd(0.0);
      spec_sum[1] = _mm_set1_pd(0.0);
      spec_sum[2] = _mm_set1_pd(0.0);
      spec_sum[3] = _mm_set1_pd(0.0);
      spec_sum[4] = _mm_set1_pd(0.0);
      spec_sum[5] = _mm_set1_pd(0.0);
      spec_sum[6] = _mm_set1_pd(0.0);
      spec_sum[7] = _mm_set1_pd(0.0);
      spec_sum[8] = _mm_set1_pd(0.0);
      spec_sum[9] = _mm_set1_pd(0.0);
      spec_sum[10] = _mm_set1_pd(0.0);
      spec_sum[11] = _mm_set1_pd(0.0);
      spec_sum[12] = _mm_set1_pd(0.0);
      spec_sum[13] = _mm_set1_pd(0.0);
      for (int idx = 0; idx < 52; idx++)
      {
        if (idx == spec_idx)
        {
          spec_sum[0] = 
            _mm_add_pd(spec_sum[0],_mm_mul_pd(mole_frac[idx*14+0],_mm_set1_pd(2.82842712474619))); 
          spec_sum[1] = 
            _mm_add_pd(spec_sum[1],_mm_mul_pd(mole_frac[idx*14+1],_mm_set1_pd(2.82842712474619))); 
          spec_sum[2] = 
            _mm_add_pd(spec_sum[2],_mm_mul_pd(mole_frac[idx*14+2],_mm_set1_pd(2.82842712474619))); 
          spec_sum[3] = 
            _mm_add_pd(spec_sum[3],_mm_mul_pd(mole_frac[idx*14+3],_mm_set1_pd(2.82842712474619))); 
          spec_sum[4] = 
            _mm_add_pd(spec_sum[4],_mm_mul_pd(mole_frac[idx*14+4],_mm_set1_pd(2.82842712474619))); 
          spec_sum[5] = 
            _mm_add_pd(spec_sum[5],_mm_mul_pd(mole_frac[idx*14+5],_mm_set1_pd(2.82842712474619))); 
          spec_sum[6] = 
            _mm_add_pd(spec_sum[6],_mm_mul_pd(mole_frac[idx*14+6],_mm_set1_pd(2.82842712474619))); 
          spec_sum[7] = 
            _mm_add_pd(spec_sum[7],_mm_mul_pd(mole_frac[idx*14+7],_mm_set1_pd(2.82842712474619))); 
          spec_sum[8] = 
            _mm_add_pd(spec_sum[8],_mm_mul_pd(mole_frac[idx*14+8],_mm_set1_pd(2.82842712474619))); 
          spec_sum[9] = 
            _mm_add_pd(spec_sum[9],_mm_mul_pd(mole_frac[idx*14+9],_mm_set1_pd(2.82842712474619))); 
          spec_sum[10] = 
            _mm_add_pd(spec_sum[10],_mm_mul_pd(mole_frac[idx*14+10],_mm_set1_pd(2.82842712474619))); 
          spec_sum[11] = 
            _mm_add_pd(spec_sum[11],_mm_mul_pd(mole_frac[idx*14+11],_mm_set1_pd(2.82842712474619))); 
          spec_sum[12] = 
            _mm_add_pd(spec_sum[12],_mm_mul_pd(mole_frac[idx*14+12],_mm_set1_pd(2.82842712474619))); 
          spec_sum[13] = 
            _mm_add_pd(spec_sum[13],_mm_mul_pd(mole_frac[idx*14+13],_mm_set1_pd(2.82842712474619))); 
        }
        else
        {
          __m128d c1 = _mm_set1_pd(visc_constants[visc_idx++]);
          __m128d c2 = _mm_set1_pd(visc_constants[visc_idx++]);
          __m128d numer;
          numer = _mm_div_pd(spec_visc[spec_idx*14+0],spec_visc[idx*14+0]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[0] = 
            _mm_add_pd(spec_sum[0],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+0],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+1],spec_visc[idx*14+1]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[1] = 
            _mm_add_pd(spec_sum[1],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+1],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+2],spec_visc[idx*14+2]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[2] = 
            _mm_add_pd(spec_sum[2],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+2],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+3],spec_visc[idx*14+3]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[3] = 
            _mm_add_pd(spec_sum[3],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+3],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+4],spec_visc[idx*14+4]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[4] = 
            _mm_add_pd(spec_sum[4],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+4],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+5],spec_visc[idx*14+5]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[5] = 
            _mm_add_pd(spec_sum[5],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+5],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+6],spec_visc[idx*14+6]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[6] = 
            _mm_add_pd(spec_sum[6],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+6],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+7],spec_visc[idx*14+7]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[7] = 
            _mm_add_pd(spec_sum[7],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+7],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+8],spec_visc[idx*14+8]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[8] = 
            _mm_add_pd(spec_sum[8],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+8],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+9],spec_visc[idx*14+9]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[9] = 
            _mm_add_pd(spec_sum[9],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+9],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+10],spec_visc[idx*14+10]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[10] = 
            _mm_add_pd(spec_sum[10],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+10],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+11],spec_visc[idx*14+11]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[11] = 
            _mm_add_pd(spec_sum[11],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+11],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+12],spec_visc[idx*14+12]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[12] = 
            _mm_add_pd(spec_sum[12],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+12],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+13],spec_visc[idx*14+13]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[13] = 
            _mm_add_pd(spec_sum[13],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+13],numer),numer),c2)); 
        }
      }
      result_vec[0] = 
        _mm_add_pd(result_vec[0],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+0],spec_visc[spec_idx*14+0]),spec_sum[0])); 
      result_vec[1] = 
        _mm_add_pd(result_vec[1],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+1],spec_visc[spec_idx*14+1]),spec_sum[1])); 
      result_vec[2] = 
        _mm_add_pd(result_vec[2],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+2],spec_visc[spec_idx*14+2]),spec_sum[2])); 
      result_vec[3] = 
        _mm_add_pd(result_vec[3],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+3],spec_visc[spec_idx*14+3]),spec_sum[3])); 
      result_vec[4] = 
        _mm_add_pd(result_vec[4],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+4],spec_visc[spec_idx*14+4]),spec_sum[4])); 
      result_vec[5] = 
        _mm_add_pd(result_vec[5],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+5],spec_visc[spec_idx*14+5]),spec_sum[5])); 
      result_vec[6] = 
        _mm_add_pd(result_vec[6],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+6],spec_visc[spec_idx*14+6]),spec_sum[6])); 
      result_vec[7] = 
        _mm_add_pd(result_vec[7],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+7],spec_visc[spec_idx*14+7]),spec_sum[7])); 
      result_vec[8] = 
        _mm_add_pd(result_vec[8],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+8],spec_visc[spec_idx*14+8]),spec_sum[8])); 
      result_vec[9] = 
        _mm_add_pd(result_vec[9],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+9],spec_visc[spec_idx*14+9]),spec_sum[9])); 
      result_vec[10] = 
        _mm_add_pd(result_vec[10],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+10],spec_visc[spec_idx*14+10]),spec_sum[10])); 
      result_vec[11] = 
        _mm_add_pd(result_vec[11],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+11],spec_visc[spec_idx*14+11]),spec_sum[11])); 
      result_vec[12] = 
        _mm_add_pd(result_vec[12],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+12],spec_visc[spec_idx*14+12]),spec_sum[12])); 
      result_vec[13] = 
        _mm_add_pd(result_vec[13],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+13],spec_visc[spec_idx*14+13]),spec_sum[13])); 
    }
    for (unsigned idx = 0; idx < 14; idx++)
    {
      result_vec[idx] = 
        _mm_mul_pd(_mm_set1_pd(0.1535268500652559),result_vec[idx]); 
      _mm_store_pd(viscosity,result_vec[idx]);
      viscosity += 2;
    }
    remaining_elmts -= 28;
    mass_frac_array += 28;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 2) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d temp = _mm_load_pd(temperature_array);
      temperature_array += 2;
      mole_frac[0+idx] = _mm_load_pd(mass_frac_array+(0*spec_stride)+(idx<<1));
      mole_frac[14+idx] = _mm_load_pd(mass_frac_array+(1*spec_stride)+(idx<<1));
      mole_frac[28+idx] = _mm_load_pd(mass_frac_array+(2*spec_stride)+(idx<<1));
      mole_frac[42+idx] = _mm_load_pd(mass_frac_array+(3*spec_stride)+(idx<<1));
      mole_frac[56+idx] = _mm_load_pd(mass_frac_array+(4*spec_stride)+(idx<<1));
      mole_frac[70+idx] = _mm_load_pd(mass_frac_array+(5*spec_stride)+(idx<<1));
      mole_frac[84+idx] = _mm_load_pd(mass_frac_array+(6*spec_stride)+(idx<<1));
      mole_frac[98+idx] = _mm_load_pd(mass_frac_array+(7*spec_stride)+(idx<<1));
      mole_frac[112+idx] = 
        _mm_load_pd(mass_frac_array+(8*spec_stride)+(idx<<1)); 
      mole_frac[126+idx] = 
        _mm_load_pd(mass_frac_array+(9*spec_stride)+(idx<<1)); 
      mole_frac[140+idx] = 
        _mm_load_pd(mass_frac_array+(10*spec_stride)+(idx<<1)); 
      mole_frac[154+idx] = 
        _mm_load_pd(mass_frac_array+(11*spec_stride)+(idx<<1)); 
      mole_frac[168+idx] = 
        _mm_load_pd(mass_frac_array+(12*spec_stride)+(idx<<1)); 
      mole_frac[182+idx] = 
        _mm_load_pd(mass_frac_array+(13*spec_stride)+(idx<<1)); 
      mole_frac[196+idx] = 
        _mm_load_pd(mass_frac_array+(14*spec_stride)+(idx<<1)); 
      mole_frac[210+idx] = 
        _mm_load_pd(mass_frac_array+(15*spec_stride)+(idx<<1)); 
      mole_frac[224+idx] = 
        _mm_load_pd(mass_frac_array+(16*spec_stride)+(idx<<1)); 
      mole_frac[238+idx] = 
        _mm_load_pd(mass_frac_array+(17*spec_stride)+(idx<<1)); 
      mole_frac[252+idx] = 
        _mm_load_pd(mass_frac_array+(18*spec_stride)+(idx<<1)); 
      mole_frac[266+idx] = 
        _mm_load_pd(mass_frac_array+(19*spec_stride)+(idx<<1)); 
      mole_frac[280+idx] = 
        _mm_load_pd(mass_frac_array+(20*spec_stride)+(idx<<1)); 
      mole_frac[294+idx] = 
        _mm_load_pd(mass_frac_array+(21*spec_stride)+(idx<<1)); 
      mole_frac[308+idx] = 
        _mm_load_pd(mass_frac_array+(22*spec_stride)+(idx<<1)); 
      mole_frac[322+idx] = 
        _mm_load_pd(mass_frac_array+(23*spec_stride)+(idx<<1)); 
      mole_frac[336+idx] = 
        _mm_load_pd(mass_frac_array+(24*spec_stride)+(idx<<1)); 
      mole_frac[350+idx] = 
        _mm_load_pd(mass_frac_array+(25*spec_stride)+(idx<<1)); 
      mole_frac[364+idx] = 
        _mm_load_pd(mass_frac_array+(26*spec_stride)+(idx<<1)); 
      mole_frac[378+idx] = 
        _mm_load_pd(mass_frac_array+(27*spec_stride)+(idx<<1)); 
      mole_frac[392+idx] = 
        _mm_load_pd(mass_frac_array+(28*spec_stride)+(idx<<1)); 
      mole_frac[406+idx] = 
        _mm_load_pd(mass_frac_array+(29*spec_stride)+(idx<<1)); 
      mole_frac[420+idx] = 
        _mm_load_pd(mass_frac_array+(30*spec_stride)+(idx<<1)); 
      mole_frac[434+idx] = 
        _mm_load_pd(mass_frac_array+(31*spec_stride)+(idx<<1)); 
      mole_frac[448+idx] = 
        _mm_load_pd(mass_frac_array+(32*spec_stride)+(idx<<1)); 
      mole_frac[462+idx] = 
        _mm_load_pd(mass_frac_array+(33*spec_stride)+(idx<<1)); 
      mole_frac[476+idx] = 
        _mm_load_pd(mass_frac_array+(34*spec_stride)+(idx<<1)); 
      mole_frac[490+idx] = 
        _mm_load_pd(mass_frac_array+(35*spec_stride)+(idx<<1)); 
      mole_frac[504+idx] = 
        _mm_load_pd(mass_frac_array+(36*spec_stride)+(idx<<1)); 
      mole_frac[518+idx] = 
        _mm_load_pd(mass_frac_array+(37*spec_stride)+(idx<<1)); 
      mole_frac[532+idx] = 
        _mm_load_pd(mass_frac_array+(38*spec_stride)+(idx<<1)); 
      mole_frac[546+idx] = 
        _mm_load_pd(mass_frac_array+(39*spec_stride)+(idx<<1)); 
      mole_frac[560+idx] = 
        _mm_load_pd(mass_frac_array+(40*spec_stride)+(idx<<1)); 
      mole_frac[574+idx] = 
        _mm_load_pd(mass_frac_array+(41*spec_stride)+(idx<<1)); 
      mole_frac[588+idx] = 
        _mm_load_pd(mass_frac_array+(42*spec_stride)+(idx<<1)); 
      mole_frac[602+idx] = 
        _mm_load_pd(mass_frac_array+(43*spec_stride)+(idx<<1)); 
      mole_frac[616+idx] = 
        _mm_load_pd(mass_frac_array+(44*spec_stride)+(idx<<1)); 
      mole_frac[630+idx] = 
        _mm_load_pd(mass_frac_array+(45*spec_stride)+(idx<<1)); 
      mole_frac[644+idx] = 
        _mm_load_pd(mass_frac_array+(46*spec_stride)+(idx<<1)); 
      mole_frac[658+idx] = 
        _mm_load_pd(mass_frac_array+(47*spec_stride)+(idx<<1)); 
      mole_frac[672+idx] = 
        _mm_load_pd(mass_frac_array+(48*spec_stride)+(idx<<1)); 
      mole_frac[686+idx] = 
        _mm_load_pd(mass_frac_array+(49*spec_stride)+(idx<<1)); 
      mole_frac[700+idx] = 
        _mm_load_pd(mass_frac_array+(50*spec_stride)+(idx<<1)); 
      mole_frac[714+idx] = 
        _mm_load_pd(mass_frac_array+(51*spec_stride)+(idx<<1)); 
      __m128d mixmw = _mm_load_pd(mixmw_array);
      mixmw_array += 2;
      result_vec[idx] = _mm_set1_pd(0.0);
       temp = _mm_mul_pd(temp,_mm_set1_pd(120.0));
      double t1 = _mm_cvtsd_f64(temp);
      double log1 = log(t1);
      double t0 = _mm_cvtsd_f64(_mm_shuffle_pd(temp,temp,1));
      double log0 = log(t0);
      __m128d logt = _mm_set_pd(log0,log1);
      // Species H
      {
        __m128d val = _mm_set1_pd(0.01473938862116986);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3401776285106742));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.261058144879592));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-19.52716556548602));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[0+idx] = val;
      }
      // Species H2
      {
        __m128d val = _mm_set1_pd(1.271284683469859e-03);
        val = 
          _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.02829817903088695)); 
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(0.8541054785785867));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-13.50231595822253));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[14+idx] = val;
      }
      // Species O
      {
        __m128d val = _mm_set1_pd(6.048679323975888e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.1372174134188812));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(1.680892288317397));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-14.54397903871408));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[28+idx] = val;
      }
      // Species O2
      {
        __m128d val = _mm_set1_pd(0.0100847718313627);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.2301633310424902));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.39404370945068));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-16.52174380450916));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[42+idx] = val;
      }
      // Species OH
      {
        __m128d val = _mm_set1_pd(6.048679323975597e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.137217413418875));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(1.680892288317354));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-14.51343125972167));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[56+idx] = val;
      }
      // Species H2O
      {
        __m128d val = _mm_set1_pd(-0.06210023880608071);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(1.238210189354115));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-7.126095134578899));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.553853497038316));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[70+idx] = val;
      }
      // Species CO
      {
        __m128d val = _mm_set1_pd(8.68673668813154e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.1978066221878029));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.144532361494317));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-16.0420622318806));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[84+idx] = val;
      }
      // Species CO2
      {
        __m128d val = _mm_set1_pd(0.02235862750234295);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5304157271437536));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.845156016570812));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.32881511990188));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[98+idx] = val;
      }
      // Species CH3
      {
        __m128d val = _mm_set1_pd(0.01464383830637072);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3378794809999195));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.242607583590146));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-19.35981201106423));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[112+idx] = val;
      }
      // Species CH4
      {
        __m128d val = _mm_set1_pd(0.0144008482711447);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3320119047555915));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.195329576946196));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-19.16897626263594));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[126+idx] = val;
      }
      // Species HO2
      {
        __m128d val = _mm_set1_pd(0.01008477183136259);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.2301633310424879));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.394043709450665));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-16.50623665958695));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[140+idx] = val;
      }
      // Species H2O2
      {
        __m128d val = _mm_set1_pd(0.01008477183136263);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.2301633310424887));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.394043709450671));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-16.49119602551682));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[154+idx] = val;
      }
      // Species CH2O
      {
        __m128d val = _mm_set1_pd(0.01025079016985108);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3105955692469765));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.67086047971521));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.01291420235239));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[168+idx] = val;
      }
      // Species C2H6
      {
        __m128d val = _mm_set1_pd(0.02241484422225297);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5324773728006829));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.867444393688489));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.8864340411546));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[182+idx] = val;
      }
      // Species C2H4
      {
        __m128d val = _mm_set1_pd(0.02219608533476465);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5255368807469865));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.798106645330391));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.25594298587761));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[196+idx] = val;
      }
      // Species C2H5
      {
        __m128d val = _mm_set1_pd(0.0224148442222536);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5324773728006966));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.867444393688588));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.90348171486313));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[210+idx] = val;
      }
      // Species C2H
      {
        __m128d val = _mm_set1_pd(0.02308471478059632);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5512760981276462));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.040498151655746));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.2004878222601));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[224+idx] = val;
      }
      // Species C2H2
      {
        __m128d val = _mm_set1_pd(0.02308471478059636);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.551276098127647));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.040498151655751));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.18074765511536));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[238+idx] = val;
      }
      // Species CH3OH
      {
        __m128d val = _mm_set1_pd(0.01194195005178133);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3457594494399707));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.907123245251647));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.5002104811602));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[252+idx] = val;
      }
      // Species CH2CO
      {
        __m128d val = _mm_set1_pd(0.01620162697675229);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4328513924391304));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.478564425409844));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.70763362444367));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[266+idx] = val;
      }
      // Species HCCO
      {
        __m128d val = _mm_set1_pd(0.01522307629912794);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.351796228499638));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.35420427277198));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-18.32506377331114));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[280+idx] = val;
      }
      // Species CH3CHO
      {
        __m128d val = _mm_set1_pd(0.01620162697675266);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4328513924391382));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.478564425409899));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.68421301797287));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[294+idx] = val;
      }
      // Species C3H4-A
      {
        __m128d val = _mm_set1_pd(0.02258284368884785);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.553160052068685));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.169968650910175));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.93873939706588));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[308+idx] = val;
      }
      // Species C3H4-P
      {
        __m128d val = _mm_set1_pd(0.02258284368884785);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.553160052068685));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.169968650910175));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.93873939706588));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[322+idx] = val;
      }
      // Species C3H6
      {
        __m128d val = _mm_set1_pd(0.02287503877385922);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5560497395691175));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.159086829883717));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.71239478064636));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[336+idx] = val;
      }
      // Species C4H6
      {
        __m128d val = _mm_set1_pd(0.02142240280637452);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5341371089626625));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.090413145400232));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.98120854465839));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[350+idx] = val;
      }
      // Species C4H7
      {
        __m128d val = _mm_set1_pd(0.02150485993261591);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5355860444732981));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.097610274266972));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.9482793621001));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[364+idx] = val;
      }
      // Species C4H8-1
      {
        __m128d val = _mm_set1_pd(0.02150485993261587);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5355860444732972));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.097610274266965));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.93921534866988));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[378+idx] = val;
      }
      // Species PC4H9
      {
        __m128d val = _mm_set1_pd(0.02162777198022059);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.53773838185872));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.108213569249132));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-25.17790518947539));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[392+idx] = val;
      }
      // Species CH3COCH2
      {
        __m128d val = _mm_set1_pd(0.01624205095928127);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4336631947942615));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.483746957928423));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.96931095870644));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[406+idx] = val;
      }
      // Species C2H5CHO
      {
        __m128d val = _mm_set1_pd(0.02000879758404488);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5168490196310973));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.151113874438199));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-25.82663397850021));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[420+idx] = val;
      }
      // Species C5H9
      {
        __m128d val = _mm_set1_pd(0.01915723389629229);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4915124228316154));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.846391809456143));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.78184868007241));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[434+idx] = val;
      }
      // Species C5H10-1
      {
        __m128d val = _mm_set1_pd(9.823559104095006e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.295979744803851));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.520120470878764));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-21.91911480147456));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[448+idx] = val;
      }
      // Species C5H11-1
      {
        __m128d val = _mm_set1_pd(0.01580442210718414);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4248519106427305));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.427246798629395));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.82208326587578));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[462+idx] = val;
      }
      // Species CH3O2
      {
        __m128d val = _mm_set1_pd(0.01194195005178086);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3457594494399605));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.907123245251574));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.30830707824042));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[476+idx] = val;
      }
      // Species CH3O2H
      {
        __m128d val = _mm_set1_pd(0.01194195005178048);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3457594494399524));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.907123245251516));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.29770491729938));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[490+idx] = val;
      }
      // Species C2H3CO
      {
        __m128d val = _mm_set1_pd(0.01558871094841275);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4204941660044574));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.399143408361484));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.49129347016926));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[504+idx] = val;
      }
      // Species C2H3CHO
      {
        __m128d val = _mm_set1_pd(0.02340061661233695);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5955476968700483));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.751833071084701));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-27.47741799780282));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[518+idx] = val;
      }
      // Species C3H5-A
      {
        __m128d val = _mm_set1_pd(0.02271080873650039);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5541553453533459));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.160772514327559));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.81744375381571));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[532+idx] = val;
      }
      // Species C3H3
      {
        __m128d val = _mm_set1_pd(0.02258284368884768);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.5531600520686816));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(5.169968650910151));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-24.95147941555229));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[546+idx] = val;
      }
      // Species NC3H7CHO
      {
        __m128d val = _mm_set1_pd(-0.01572518516534323);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(0.248962293644394));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3165817798945547));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-12.81021987594491));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[560+idx] = val;
      }
      // Species C2H5COCH2
      {
        __m128d val = _mm_set1_pd(-7.89459267319049e-04);
        val = 
          _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.06762206191012728)); 
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(1.952157119077493));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-18.56615649146089));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[574+idx] = val;
      }
      // Species CH3CHCO
      {
        __m128d val = _mm_set1_pd(0.01558871094841311);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.4204941660044652));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.399143408361541));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.48222233868143));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[588+idx] = val;
      }
      // Species NC3H7COCH3
      {
        __m128d val = _mm_set1_pd(1.820340891942987e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.1229676616478414));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.32561444740514));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-19.3765420216231));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[602+idx] = val;
      }
      // Species NC3H7COCH2
      {
        __m128d val = _mm_set1_pd(0.01119525229819807);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3302621812756928));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.803279455428719));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.57017928153328));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[616+idx] = val;
      }
      // Species NC7H16
      {
        __m128d val = _mm_set1_pd(0.01407613802539561);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3896780138463274));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.197919285611414));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.62032594105494));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[630+idx] = val;
      }
      // Species C7H15-1
      {
        __m128d val = _mm_set1_pd(0.01407613802539558);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.3896780138463268));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(4.197919285611412));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-23.62538091879411));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[644+idx] = val;
      }
      // Species C7H15O2
      {
        __m128d val = _mm_set1_pd(9.014132110063371e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.2788560203411342));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.425986209058756));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-21.87023237436892));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[658+idx] = val;
      }
      // Species C7H14OOHO2
      {
        __m128d val = _mm_set1_pd(5.413054249775649e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.216168047330524));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.117418074055381));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-21.74516858824146));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[672+idx] = val;
      }
      // Species C7H14O
      {
        __m128d val = _mm_set1_pd(8.872569026068563e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.2819477382149219));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(3.478518292881485));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-22.06257892272827));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[686+idx] = val;
      }
      // Species NC7KET
      {
        __m128d val = _mm_set1_pd(-0.01928649818698724);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(0.3290478605967052));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.892645465051813));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-11.73853058625284));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[700+idx] = val;
      }
      // Species N2
      {
        __m128d val = _mm_set1_pd(8.617661554832269e-03);
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-0.1961905296416808));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(2.13193508629056));
        val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(-15.99249280222402));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        spec_visc[714+idx] = val;
      }
      mole_frac[0+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[0+idx],_mm_set1_pd(recip_molecular_masses[0])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[14+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[14+idx],_mm_set1_pd(recip_molecular_masses[1])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[28+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[28+idx],_mm_set1_pd(recip_molecular_masses[2])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[42+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[42+idx],_mm_set1_pd(recip_molecular_masses[3])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[56+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[56+idx],_mm_set1_pd(recip_molecular_masses[4])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[70+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[70+idx],_mm_set1_pd(recip_molecular_masses[5])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[84+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[84+idx],_mm_set1_pd(recip_molecular_masses[6])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[98+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[98+idx],_mm_set1_pd(recip_molecular_masses[7])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[112+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[112+idx],_mm_set1_pd(recip_molecular_masses[8])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[126+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[126+idx],_mm_set1_pd(recip_molecular_masses[9])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[140+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[140+idx],_mm_set1_pd(recip_molecular_masses[10])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[154+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[154+idx],_mm_set1_pd(recip_molecular_masses[11])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[168+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[168+idx],_mm_set1_pd(recip_molecular_masses[12])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[182+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[182+idx],_mm_set1_pd(recip_molecular_masses[13])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[196+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[196+idx],_mm_set1_pd(recip_molecular_masses[14])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[210+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[210+idx],_mm_set1_pd(recip_molecular_masses[15])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[224+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[224+idx],_mm_set1_pd(recip_molecular_masses[16])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[238+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[238+idx],_mm_set1_pd(recip_molecular_masses[17])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[252+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[252+idx],_mm_set1_pd(recip_molecular_masses[18])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[266+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[266+idx],_mm_set1_pd(recip_molecular_masses[19])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[280+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[280+idx],_mm_set1_pd(recip_molecular_masses[20])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[294+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[294+idx],_mm_set1_pd(recip_molecular_masses[21])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[308+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[308+idx],_mm_set1_pd(recip_molecular_masses[22])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[322+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[322+idx],_mm_set1_pd(recip_molecular_masses[23])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[336+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[336+idx],_mm_set1_pd(recip_molecular_masses[24])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[350+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[350+idx],_mm_set1_pd(recip_molecular_masses[25])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[364+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[364+idx],_mm_set1_pd(recip_molecular_masses[26])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[378+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[378+idx],_mm_set1_pd(recip_molecular_masses[27])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[392+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[392+idx],_mm_set1_pd(recip_molecular_masses[28])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[406+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[406+idx],_mm_set1_pd(recip_molecular_masses[29])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[420+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[420+idx],_mm_set1_pd(recip_molecular_masses[30])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[434+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[434+idx],_mm_set1_pd(recip_molecular_masses[31])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[448+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[448+idx],_mm_set1_pd(recip_molecular_masses[32])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[462+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[462+idx],_mm_set1_pd(recip_molecular_masses[33])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[476+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[476+idx],_mm_set1_pd(recip_molecular_masses[34])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[490+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[490+idx],_mm_set1_pd(recip_molecular_masses[35])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[504+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[504+idx],_mm_set1_pd(recip_molecular_masses[36])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[518+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[518+idx],_mm_set1_pd(recip_molecular_masses[37])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[532+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[532+idx],_mm_set1_pd(recip_molecular_masses[38])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[546+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[546+idx],_mm_set1_pd(recip_molecular_masses[39])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[560+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[560+idx],_mm_set1_pd(recip_molecular_masses[40])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[574+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[574+idx],_mm_set1_pd(recip_molecular_masses[41])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[588+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[588+idx],_mm_set1_pd(recip_molecular_masses[42])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[602+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[602+idx],_mm_set1_pd(recip_molecular_masses[43])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[616+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[616+idx],_mm_set1_pd(recip_molecular_masses[44])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[630+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[630+idx],_mm_set1_pd(recip_molecular_masses[45])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[644+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[644+idx],_mm_set1_pd(recip_molecular_masses[46])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[658+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[658+idx],_mm_set1_pd(recip_molecular_masses[47])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[672+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[672+idx],_mm_set1_pd(recip_molecular_masses[48])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[686+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[686+idx],_mm_set1_pd(recip_molecular_masses[49])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[700+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[700+idx],_mm_set1_pd(recip_molecular_masses[50])),_mm_set1_pd(1e3)),mixmw); 
      mole_frac[714+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[714+idx],_mm_set1_pd(recip_molecular_masses[51])),_mm_set1_pd(1e3)),mixmw); 
    }
    int visc_idx = 0;
    for (int spec_idx = 0; spec_idx <52; spec_idx++)
    {
      spec_sum[0] = _mm_set1_pd(0.0);
      spec_sum[1] = _mm_set1_pd(0.0);
      spec_sum[2] = _mm_set1_pd(0.0);
      spec_sum[3] = _mm_set1_pd(0.0);
      spec_sum[4] = _mm_set1_pd(0.0);
      spec_sum[5] = _mm_set1_pd(0.0);
      spec_sum[6] = _mm_set1_pd(0.0);
      spec_sum[7] = _mm_set1_pd(0.0);
      spec_sum[8] = _mm_set1_pd(0.0);
      spec_sum[9] = _mm_set1_pd(0.0);
      spec_sum[10] = _mm_set1_pd(0.0);
      spec_sum[11] = _mm_set1_pd(0.0);
      spec_sum[12] = _mm_set1_pd(0.0);
      spec_sum[13] = _mm_set1_pd(0.0);
      for (int idx = 0; idx < 52; idx++)
      {
        if (idx == spec_idx)
        {
          spec_sum[0] = 
            _mm_add_pd(spec_sum[0],_mm_mul_pd(mole_frac[idx*14+0],_mm_set1_pd(2.82842712474619))); 
          spec_sum[1] = 
            _mm_add_pd(spec_sum[1],_mm_mul_pd(mole_frac[idx*14+1],_mm_set1_pd(2.82842712474619))); 
          spec_sum[2] = 
            _mm_add_pd(spec_sum[2],_mm_mul_pd(mole_frac[idx*14+2],_mm_set1_pd(2.82842712474619))); 
          spec_sum[3] = 
            _mm_add_pd(spec_sum[3],_mm_mul_pd(mole_frac[idx*14+3],_mm_set1_pd(2.82842712474619))); 
          spec_sum[4] = 
            _mm_add_pd(spec_sum[4],_mm_mul_pd(mole_frac[idx*14+4],_mm_set1_pd(2.82842712474619))); 
          spec_sum[5] = 
            _mm_add_pd(spec_sum[5],_mm_mul_pd(mole_frac[idx*14+5],_mm_set1_pd(2.82842712474619))); 
          spec_sum[6] = 
            _mm_add_pd(spec_sum[6],_mm_mul_pd(mole_frac[idx*14+6],_mm_set1_pd(2.82842712474619))); 
          spec_sum[7] = 
            _mm_add_pd(spec_sum[7],_mm_mul_pd(mole_frac[idx*14+7],_mm_set1_pd(2.82842712474619))); 
          spec_sum[8] = 
            _mm_add_pd(spec_sum[8],_mm_mul_pd(mole_frac[idx*14+8],_mm_set1_pd(2.82842712474619))); 
          spec_sum[9] = 
            _mm_add_pd(spec_sum[9],_mm_mul_pd(mole_frac[idx*14+9],_mm_set1_pd(2.82842712474619))); 
          spec_sum[10] = 
            _mm_add_pd(spec_sum[10],_mm_mul_pd(mole_frac[idx*14+10],_mm_set1_pd(2.82842712474619))); 
          spec_sum[11] = 
            _mm_add_pd(spec_sum[11],_mm_mul_pd(mole_frac[idx*14+11],_mm_set1_pd(2.82842712474619))); 
          spec_sum[12] = 
            _mm_add_pd(spec_sum[12],_mm_mul_pd(mole_frac[idx*14+12],_mm_set1_pd(2.82842712474619))); 
          spec_sum[13] = 
            _mm_add_pd(spec_sum[13],_mm_mul_pd(mole_frac[idx*14+13],_mm_set1_pd(2.82842712474619))); 
        }
        else
        {
          __m128d c1 = _mm_set1_pd(visc_constants[visc_idx++]);
          __m128d c2 = _mm_set1_pd(visc_constants[visc_idx++]);
          __m128d numer;
          numer = _mm_div_pd(spec_visc[spec_idx*14+0],spec_visc[idx*14+0]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[0] = 
            _mm_add_pd(spec_sum[0],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+0],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+1],spec_visc[idx*14+1]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[1] = 
            _mm_add_pd(spec_sum[1],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+1],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+2],spec_visc[idx*14+2]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[2] = 
            _mm_add_pd(spec_sum[2],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+2],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+3],spec_visc[idx*14+3]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[3] = 
            _mm_add_pd(spec_sum[3],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+3],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+4],spec_visc[idx*14+4]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[4] = 
            _mm_add_pd(spec_sum[4],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+4],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+5],spec_visc[idx*14+5]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[5] = 
            _mm_add_pd(spec_sum[5],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+5],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+6],spec_visc[idx*14+6]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[6] = 
            _mm_add_pd(spec_sum[6],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+6],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+7],spec_visc[idx*14+7]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[7] = 
            _mm_add_pd(spec_sum[7],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+7],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+8],spec_visc[idx*14+8]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[8] = 
            _mm_add_pd(spec_sum[8],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+8],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+9],spec_visc[idx*14+9]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[9] = 
            _mm_add_pd(spec_sum[9],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+9],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+10],spec_visc[idx*14+10]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[10] = 
            _mm_add_pd(spec_sum[10],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+10],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+11],spec_visc[idx*14+11]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[11] = 
            _mm_add_pd(spec_sum[11],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+11],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+12],spec_visc[idx*14+12]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[12] = 
            _mm_add_pd(spec_sum[12],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+12],numer),numer),c2)); 
          numer = _mm_div_pd(spec_visc[spec_idx*14+13],spec_visc[idx*14+13]);
          numer = _mm_mul_pd(c1,numer);
          numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));
          spec_sum[13] = 
            _mm_add_pd(spec_sum[13],_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*14+13],numer),numer),c2)); 
        }
      }
      result_vec[0] = 
        _mm_add_pd(result_vec[0],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+0],spec_visc[spec_idx*14+0]),spec_sum[0])); 
      result_vec[1] = 
        _mm_add_pd(result_vec[1],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+1],spec_visc[spec_idx*14+1]),spec_sum[1])); 
      result_vec[2] = 
        _mm_add_pd(result_vec[2],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+2],spec_visc[spec_idx*14+2]),spec_sum[2])); 
      result_vec[3] = 
        _mm_add_pd(result_vec[3],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+3],spec_visc[spec_idx*14+3]),spec_sum[3])); 
      result_vec[4] = 
        _mm_add_pd(result_vec[4],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+4],spec_visc[spec_idx*14+4]),spec_sum[4])); 
      result_vec[5] = 
        _mm_add_pd(result_vec[5],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+5],spec_visc[spec_idx*14+5]),spec_sum[5])); 
      result_vec[6] = 
        _mm_add_pd(result_vec[6],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+6],spec_visc[spec_idx*14+6]),spec_sum[6])); 
      result_vec[7] = 
        _mm_add_pd(result_vec[7],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+7],spec_visc[spec_idx*14+7]),spec_sum[7])); 
      result_vec[8] = 
        _mm_add_pd(result_vec[8],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+8],spec_visc[spec_idx*14+8]),spec_sum[8])); 
      result_vec[9] = 
        _mm_add_pd(result_vec[9],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+9],spec_visc[spec_idx*14+9]),spec_sum[9])); 
      result_vec[10] = 
        _mm_add_pd(result_vec[10],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+10],spec_visc[spec_idx*14+10]),spec_sum[10])); 
      result_vec[11] = 
        _mm_add_pd(result_vec[11],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+11],spec_visc[spec_idx*14+11]),spec_sum[11])); 
      result_vec[12] = 
        _mm_add_pd(result_vec[12],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+12],spec_visc[spec_idx*14+12]),spec_sum[12])); 
      result_vec[13] = 
        _mm_add_pd(result_vec[13],_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*14+13],spec_visc[spec_idx*14+13]),spec_sum[13])); 
    }
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      result_vec[idx] = 
        _mm_mul_pd(_mm_set1_pd(0.1535268500652559),result_vec[idx]); 
      _mm_store_pd(viscosity,result_vec[idx]);
      viscosity += 2;
    }
  }
  free(mole_frac);
  free(spec_visc);
  free(result_vec);
  free(spec_sum);
}

const double diff_constants[5304] = {6.07005636955586e-03, -0.1386145161448153, 
  2.707818945836592, -11.27823930531106, 0.01274259093587177, 
  -0.2926680862187829, 3.893331475844598, -14.46349099620983, 
  0.01613119190705508, -0.3728785793781316, 4.526060936787678, 
  -16.42636730227215, 0.01277302505120672, -0.2933568018640708, 
  3.898513752624181, -14.4779543967332, 0.01287219707469157, 
  -0.3376036723887701, 4.593171151733713, -17.34880569712517, 
  0.01675969348173545, -0.3846361249979026, 4.594529227576374, 
  -16.6004524022483, 0.01631111894482831, -0.3970760847516223, 
  4.875734188702281, -17.93437709809112, 0.01744492482303953, 
  -0.4076246855228118, 4.82905614638663, -17.42407339559932, 
  0.01728295034331914, -0.403698343166343, 4.797318264425541, 
  -17.32092542045278, 0.01614843886042517, -0.3732731334199464, 
  4.529063238336419, -16.43428980262436, 0.01616494282080296, 
  -0.3736506813214178, 4.531936050068589, -16.44186326872099, 
  0.01082257879805209, -0.2949278232297451, 4.298374979557995, 
  -17.03770565779608, 0.01649213606317376, -0.403713121748136, 
  4.946150262622447, -18.34446184390922, 0.01616002702773094, 
  -0.3914720819937988, 4.81585104903874, -17.64159342368206, 
  0.01648940668175259, -0.4035915459347964, 4.944756899275299, 
  -18.33939117197763, 0.01544995741722749, -0.3794651739720996, 
  4.759847854951454, -17.68071628552682, 0.01544541375242724, 
  -0.3794254511508813, 4.760056798991309, -17.68311509310161, 
  0.01093817248600197, -0.296719316524457, 4.304630717633641, 
  -17.04241262561012, 0.01066142594111403, -0.2906041415410128, 
  4.257182232927045, -17.01763919802604, 0.01605492906096749, 
  -0.3754995684506969, 4.582328486605241, -16.33019827892464, 
  0.01062659297128495, -0.2899162752672357, 4.252824759894023, 
  -17.00924773273659, 0.01354586702789, -0.3472357733317638, 4.607589052949854, 
  -17.74711708482682, 0.01354586702789, -0.3472357733317638, 4.607589052949854, 
  -17.74711708482682, 0.01392281557524954, -0.3529640979729814, 
  4.627618773587727, -17.6852140880109, 0.01128820425565955, 
  -0.3035682058471521, 4.341268568286155, -17.38520166176037, 
  0.01146803987495026, -0.3068819885136018, 4.360027053380294, 
  -17.39576967908494, 0.01145762838956716, -0.3066867586076895, 
  4.358885863023379, -17.39384700950846, 0.01140084767025297, 
  -0.3099794643511752, 4.419043643696589, -17.78874535853305, 
  7.78447959864281e-03, -0.2341087316803336, 3.902080167835877, 
  -16.57598756234416, 8.399950569711405e-03, -0.2461668816896619, 
  3.977601399290996, -16.67026693949255, 8.011470068852437e-03, 
  -0.2415430466201732, 3.973831801307318, -16.93372788262581, 
  8.351242551044056e-03, -0.2482424543444974, 4.015922239726652, 
  -17.02099331252863, 6.828010971768527e-03, -0.215411678548531, 
  3.785525838001176, -16.39864956948779, 0.01063936992401773, 
  -0.2907156303998116, 4.265672041668134, -16.96501865580841, 
  0.01062428305670706, -0.2904123917558677, 4.263703373407284, 
  -16.96107878552007, 9.905462382935677e-03, -0.275984191637148, 
  4.167632425945081, -16.89846009094116, 7.692843507506922e-03, 
  -0.2322071854018039, 3.888995025827544, -16.57057952777258, 
  0.01370400448537074, -0.3494404035063852, 4.612834197590581, 
  -17.70627871142884, 0.01355600649837461, -0.3474125134137284, 
  4.608493434190811, -17.74808151545601, 5.915882127232079e-03, 
  -0.1959411038650129, 3.65120646590983, -16.09911726340144, 
  4.748535657606066e-03, -0.1725813210855693, 3.500260128823176, 
  -15.8876905168324, 9.892129552972419e-03, -0.2757202520214445, 
  4.165954410812983, -16.8951678389679, 2.321086018580908e-03, 
  -0.1244245592384868, 3.193796951800068, -15.3540195687286, 
  3.485807582875432e-03, -0.1476653375085639, 3.343481765699011, 
  -15.57135576048758, 7.23452106126048e-04, -0.09271521852749207, 
  2.991258330877321, -15.05520178606029, 7.358523349177236e-04, 
  -0.09296292417746271, 2.992851816074919, -15.05847955211062, 
  -5.496134612509556e-03, 0.035836953366181, 2.129207306276658, 
  -13.22930823773059, -0.01357626943242583, 0.2030134951606286, 
  1.005259952080528, -10.9809204900768, -2.293211819447536e-03, 
  -0.02982623937276727, 2.566379561797968, -14.15360604257172, 
  -7.487768808030028e-03, 0.0782977395923834, 1.835309993911189, 
  -12.61717693319752, 0.0166363085769806, -0.3818063877452844, 
  4.572913371984209, -16.53529915105964, 2.795390607818213e-03, 
  -0.06501215286510499, 2.155935564682138, -10.59937186463389, 
  5.292788909391028e-03, -0.1209962823601938, 2.573522524746874, 
  -11.91485013159442, 2.795311878055015e-03, -0.06501021068765825, 
  2.155919498210914, -10.60237602288665, 0.01688049411673393, 
  -0.3967428195441058, 4.765096756884214, -17.55769760757197, 
  5.136360584657117e-03, -0.1173684712056846, 2.54522417288654, 
  -11.8878798132339, 0.01160270803702101, -0.2645840451207713, 3.66317526366357, 
  -14.8335087756257, 6.74522310723356e-03, -0.1533474644163446, 
  2.814310164589852, -12.60984961409305, 6.621447543525716e-03, 
  -0.1506071284541636, 2.794077954627578, -12.5461517244027, 
  5.299663414797426e-03, -0.1211474480990539, 2.574625651895275, 
  -11.91831496013485, 5.306182435286764e-03, -0.1212907981415695, 
  2.57567175619086, -11.92159536489396, 0.01523750046743601, 
  -0.3551264057012359, 4.413095443907515, -16.89431885382482, 
  0.01220102287802632, -0.277979305267465, 3.762845438041857, -15.2354754878375, 
  0.01094167310178062, -0.2497144509659547, 3.551936517323391, 
  -14.46511464784045, 0.01217861562298818, -0.2774786999185308, 
  3.759129586512917, -15.22541781847272, 0.0120251932076417, 
  -0.2744796606548115, 3.740525963010383, -15.01550606313474, 
  0.01204942751895602, -0.2750204227398973, 3.744534504652912, 
  -15.02662786270632, 0.01612104123767124, -0.373455618774532, 
  4.537529397721381, -17.17914855926564, 0.01585412104488423, 
  -0.3664509276272117, 4.476625925659231, -17.10113986289785, 
  7.121209254304696e-03, -0.1622028425828566, 2.88378954715211, 
  -12.40828498810663, 0.01587710797375491, -0.3669765045866935, 
  4.480623263191378, -17.11211874331906, 0.01431338113313719, 
  -0.3281436451379399, 4.15980880018118, -16.2935160545892, 0.01431338113313719, 
  -0.3281436451379399, 4.15980880018118, -16.2935160545892, 0.01354937539718556, 
  -0.310694987947511, 4.027076775059973, -15.91343195684986, 
  0.01609890159858668, -0.3689743538042209, 4.4706934941855, -17.20495324733379, 
  0.01593749003031078, -0.3653030197101193, 4.442908465618515, 
  -17.11720743910764, 0.01594843281577179, -0.3655500438269643, 
  4.444762525094158, -17.12206958158253, 0.01691921266417153, 
  -0.3872802452499391, 4.606299960979188, -17.66318071563752, 
  0.01737679963372699, -0.4010638546024475, 4.738167991958075, 
  -17.99576756941306, 0.01777935839526863, -0.4113771936936297, 
  4.826044616283593, -18.19512858023234, 0.01718110227794977, 
  -0.3963181186575399, 4.699435554499391, -18.02466946395959, 
  0.01679928949004035, -0.3876551538042579, 4.633957194530955, 
  -17.86476036538931, 0.01796178375858354, -0.4144728291438168, 
  4.840450370577194, -18.30237990106014, 0.01630393420851265, 
  -0.3776368555066107, 4.569324307544011, -17.26772561477097, 
  0.01631260176790126, -0.3778351256534287, 4.570832962789371, 
  -17.27191025959687, 0.01637076994078888, -0.3783328949748113, 
  4.567601518699476, -17.38028145810659, 0.01809424996511365, 
  -0.4183399167889328, 4.876997993958335, -18.38985465192316, 
  0.01393303820800295, -0.319447858690007, 4.09358631549345, -16.10507625034313, 
  0.01429839759079891, -0.3278054498990226, 4.157270990088332, 
  -16.2866867858833, 0.01891563877682642, -0.4375526894782177, 
  5.026748581440506, -18.80001171510612, 0.01959446800707452, 
  -0.4530087824047507, 5.143778225184334, -19.186710238212, 0.01637890165732786, 
  -0.3785190356820617, 4.569019033392058, -17.38412542907474, 
  0.02054203139904574, -0.474521516272946, 5.306010697734096, 
  -19.66974094149979, 0.01778921026141394, -0.4142228297423126, 
  4.868154768120996, -18.53238842203232, 0.02111538696917221, 
  -0.486607371204671, 5.389327267568694, -19.96297503606925, 
  0.02110851916373922, -0.48644907436745, 5.388112663511953, -19.95983165654846, 
  0.02109950326025132, -0.4921497123297016, 5.479172043416174, 
  -20.34739355273189, 0.02064485105681144, -0.4876873286068847, 
  5.49257715867006, -20.68493458988628, 0.02005243260587897, 
  -0.4669355899237864, 5.276743267160139, -19.79422095609425, 
  0.02182640074893506, -0.5095029489372571, 5.617120834987325, 
  -20.75280315931132, 5.112587018456543e-03, -0.1168323602167619, 
  2.541187758342387, -11.86867598382883, 8.921387876363899e-03, 
  -0.2043111094110479, 3.21271036899084, -14.43516047152531, 
  6.709989444111375e-03, -0.153421897482575, 2.822369844918028, 
  -13.04822501577877, 0.01668800933097615, -0.4000865075599804, 
  4.857886405450124, -18.76832767337158, 8.421269539825305e-03, 
  -0.192303679819131, 3.116694421364719, -14.21085374850775, 
  0.01414740597208021, -0.3300325147218622, 4.22120640174638, 
  -17.33506700696438, 0.01014800539478876, -0.2339033058673573, 
  3.450818212071303, -15.04882928157028, 0.01003752809593942, 
  -0.2313023905779877, 3.430385256165832, -14.99361155646193, 
  8.953335747600466e-03, -0.2050263467052026, 3.218028757404499, 
  -14.45311028018079, 8.984701805449817e-03, -0.2057285734515858, 
  3.223250560897271, -14.47057247332107, 0.01631101644945535, 
  -0.3901557704464421, 4.769971326135041, -18.93305497580333, 
  0.01401006289649292, -0.3269333682056579, 4.198071191061252, 
  -17.39540340510438, 0.01336944844525558, -0.3117550178418557, 
  4.078249616720181, -16.81258828816052, 0.01396088456434844, 
  -0.3257899228963624, 4.18922934013801, -17.36711512294028, 
  0.01387007441274128, -0.3242258348053997, 4.181897552723494, 
  -17.15793564741556, 0.01391408523932538, -0.3252485141178312, 4.1897996052336, 
  -17.18547199876029, 0.01613789593863038, -0.3858562626630013, 
  4.734161713269049, -18.85205143153762, 0.01592202139895919, 
  -0.3801589910299608, 4.683334650175035, -18.83329662111954, 
  0.01086730873862738, -0.2503916171527278, 3.576654621796225, -15.123529065566, 
  0.01593220619723479, -0.3805248359154658, 4.687164844818441, 
  -18.85149796892453, 0.01538436767296208, -0.3626136226938619, 
  4.505016324496773, -18.31687082564888, 0.01538436767296208, 
  -0.3626136226938619, 4.505016324496773, -18.31687082564888, 
  0.01501113438505228, -0.3533595555080306, 4.428560006606044, 
  -18.06616068494523, 0.01588413120937565, -0.3769989529195602, 
  4.638719029127194, -18.87907354144117, 0.01582248982557253, 
  -0.3754910363983166, 4.626423250939595, -18.82870581811314, 
  0.01583637065440803, -0.3758495454233906, 4.629473540512518, 
  -18.83903790739068, 0.01603961210296154, -0.3811189800349848, 
  4.674349892624078, -19.11487907721562, 0.0161278365874757, -0.38704058420555, 
  4.752778748434771, -19.33023413824901, 0.01609515151390448, 
  -0.3860159820447161, 4.7428006875827, -19.24959080088679, 0.01700835399822671, 
  -0.4059536173467931, 4.88557065007758, -19.78869407042372, 
  0.01694380314825956, -0.4040505586994958, 4.86743662785883, 
  -19.73866589458224, 0.01626143673540029, -0.391385744429972, 
  4.796132572935058, -19.53270038203646, 0.01607459310947566, 
  -0.385583228195326, 4.741382450019588, -18.946638085476, 0.01607085780667344, 
  -0.3855626972402351, 4.741740446564701, -18.95133324725129, 
  0.01608801104881464, -0.3851936535241047, 4.731582103509045, 
  -19.05689657669705, 0.01609332646403691, -0.3860489589904988, 
  4.743462893886107, -19.32346295706456, 0.01519590490766243, 
  -0.3579413118253633, 4.466412522374004, -18.19200606645746, 
  0.01535432888277651, -0.3618889515127398, 4.499210130735202, 
  -18.29809533185839, 0.01652960612588926, -0.3985101178183235, 
  4.858765545226605, -19.71220350741126, 0.01649772136697698, 
  -0.3981021293562491, 4.857903629692595, -19.8102087643, 0.01609147733499475, 
  -0.3853248141690443, 4.732989008802232, -19.06333388486743, 
  0.01675318746795728, -0.4065321356960222, 4.942987954057028, 
  -20.17338551118376, 0.01590087378216396, -0.3874375306948203, 
  4.801747634123167, -19.7407932963726, 0.01679963743593631, 
  -0.4086015774444273, 4.966217037256961, -20.36907137257742, 
  0.01679742199531968, -0.4085109786316845, 4.965212372047211, 
  -20.36517100268592, 0.01441760342060429, -0.3632958436444087, 
  4.697839445107325, -19.94259776861082, 0.01330221696384889, 
  -0.345731621209717, 4.627944912716204, -20.12749538130885, 
  0.01573668231032049, -0.388997665315901, 4.856113005575871, 
  -20.22226411401526, 0.01407018648719866, -0.3580060096579848, 
  4.679018948496533, -19.99806193550906, 8.372042302695153e-03, 
  -0.1911814097131465, 3.108166782030393, -14.17982639082345, 
  8.859840042651536e-03, -0.2029332432454998, 3.202465246193662, 
  -14.43059516365417, 0.01745455510555758, -0.430169976219648, 
  5.195749765783188, -20.35995016651012, 9.643409359213619e-03, 
  -0.2216960962204998, 3.352436079737641, -15.26983025986587, 
  0.01448007460870805, -0.3409100327767888, 4.332065595567145, 
  -18.16269156098646, 0.01204523332255568, -0.2793353695025515, 
  3.813687188677732, -16.38044168538186, 0.01183922015355889, 
  -0.2745516182771348, 3.776635898498599, -16.29053037133515, 
  0.01007996391839944, -0.2323734110936852, 3.439392352174992, 
  -15.50004291502803, 0.01008223653245205, -0.232424758905539, 
  3.439777844028267, -15.50833294214318, 0.01719363908271478, 
  -0.414715844742564, 4.995923831965781, -20.05884180086192, 0.0145021758224369, 
  -0.3414936378826617, 4.337260530248074, -18.24730298780436, 
  0.01421848106643789, -0.3345627430029221, 4.280747800790546, 
  -17.84097961116675, 0.01449598502506042, -0.3413479240870884, 
  4.336120870665544, -18.23553582794667, 0.01477501130932063, 
  -0.3487286728863743, 4.400897711371161, -18.22123914386417, 
  0.01477324265401361, -0.3486723145654258, 4.400342791355876, 
  -18.23055682387125, 0.01713838302443, -0.4127426782723577, 4.974739689476374, 
  -20.01135078489306, 0.01676326848404254, -0.4026062358063881, 
  4.883645529899316, -19.88811092341739, 0.01212663439193806, 
  -0.2813797553583489, 3.830927098692963, -16.32182119031236, 
  0.01674927834103349, -0.402377904070954, 4.88263290894642, -19.89717143000786, 
  0.01571641151604193, -0.3733284988525228, 4.614662871310468, 
  -19.12438592149105, 0.01571641151604193, -0.3733284988525228, 
  4.614662871310468, -19.12438592149105, 0.01548932415920301, 
  -0.3673303663669795, 4.562033025306415, -18.93833975652305, 
  0.01639164241250889, -0.3909174243131573, 4.766574596208835, 
  -19.73304685740921, 0.01635919138326361, -0.3900875084898815, 
  4.759496181751546, -19.69869261917766, 0.01636079016736964, 
  -0.3901696080829988, 4.760478508827247, -19.7052429333174, 
  0.01633448321971819, -0.3898964393316811, 4.760870546039426, 
  -19.85351236954241, 0.0165164669441436, -0.3984440263757634, 
  4.863709773959997, -20.15078757338976, 0.01708089940661849, 
  -0.4127340406270751, 4.984703551297108, -20.44816838913453, 
  0.01640651374098501, -0.3950405850590485, 4.829329979440616, 
  -20.19671640016324, 0.01633508951262163, -0.3929477038330859, 
  4.809459276241359, -20.14188954532398, 0.01637712675127979, 
  -0.3965964415546734, 4.860445281870467, -20.24919350581264, 
  0.01703899937708034, -0.410976225946881, 4.965306337410277, 
  -20.08338166519534, 0.01702848721507976, -0.4107832548186515, 
  4.964215518847868, -20.08575942442786, 0.01671536378310315, 
  -0.402584945115586, 4.892223110072403, -20.02652099805963, 
  0.01699906284095335, -0.410634707852065, 4.966673740301648, 
  -20.46001479185745, 0.01559758148692304, -0.3702061165393553, 
  4.587382172503484, -19.0302662456236, 0.01571302332897862, 
  -0.3732196570415779, 4.613589096193557, -19.11547202903997, 
  0.01681599066523259, -0.4090121678589246, 4.97553229512143, 
  -20.59838497027611, 0.0167927156394482, -0.4087671586189743, 
  4.975908490951712, -20.69301267260242, 0.01670602679187113, 
  -0.4024256210015395, 4.891440817692603, -20.02883440190177, 
  0.01606239574698725, -0.3940352955452908, 4.879930756259689, 
  -20.59310679081298, 0.01608175549449248, -0.393634843735802, 
  4.869873371213337, -20.46465061404162, 0.01583241398194664, 
  -0.3887694753435486, 4.838221900819122, -20.5998153482091, 
  0.01584316666608613, -0.3889584092129303, 4.839206400832731, 
  -20.60004091208728, 0.01431893390642452, -0.3622611239543703, 
  4.705352975232477, -20.51201846536094, 0.01268863720824975, 
  -0.3316895641928439, 4.527449878782691, -20.40363115908312, 
  0.01514531606187664, -0.3770737554343602, 4.783225269624663, 
  -20.58667639110196, 0.01380438343960207, -0.3530337685908498, 
  4.656053395048525, -20.49298415182425, 9.598235058336482e-03, 
  -0.2206511942570476, 3.344373881577649, -15.24046887285127, 
  0.01668591762661101, -0.4000143819292453, 4.857154913388838, 
  -18.78205560706603, 8.360700262427152e-03, -0.1909630266095029, 
  3.106846575391895, -14.20664048632639, 0.01407004845883094, 
  -0.328231571182103, 4.207261292181449, -17.32207575338645, 
  0.01013038374081819, -0.2335065688754916, 3.447849894456242, 
  -15.05616308019808, 0.01001503338196486, -0.2307950092941395, 
  3.426582051455392, -14.99939243741708, 8.890835104032832e-03, 
  -0.203627119671497, 3.207624486641297, -14.44837171154892, 
  8.921387876363755e-03, -0.2043111094110447, 3.212710368990817, 
  -14.46570825051756, 0.01632070830830463, -0.3902028447755962, 
  4.768951501861825, -18.9471923267252, 0.01392245192265643, 
  -0.3248964216081119, 4.182321022846665, -17.37540477179135, 
  0.01330827123327063, -0.3103322505256539, 4.067246084491885, 
  -16.80407965419224, 0.01387464400217716, -0.3237850863869338, 
  4.173729533158471, -17.34751317654655, 0.01380503004302218, 
  -0.3227149377196643, 4.170227503157337, -17.1469232064959, 0.0138466184181859, 
  -0.32368090214922, 4.177687887543875, -17.17362535103958, 0.01614642893563057, 
  -0.3858738857364965, 4.732892587358385, -18.86596442507136, 
  0.01590851868031888, -0.3796765476894065, 4.678297103306653, 
  -18.83966666105025, 0.01081043778498633, -0.2491088600361257, 
  3.567040524136709, -15.12182420038743, 0.01591889218855689, 
  -0.380046918320627, 4.682163022708305, -18.85824629236493, 
  0.01531231602001087, -0.3608755599586282, 4.491091880113276, 
  -18.30228299632415, 0.01531231602001087, -0.3608755599586282, 
  4.491091880113276, -18.30228299632415, 0.01494257373538834, 
  -0.3517095935412156, 4.41536905469408, -18.05386107251976, 0.0158354886623282, 
  -0.3757428156802451, 4.628033332232552, -18.87338446602391, 
  0.01577506891328646, -0.374266640907845, 4.616008643941134, 
  -18.82385909114426, 0.01578923910589939, -0.3746324503282895, 
  4.619119719296429, -18.83445176005826, 0.01598463842380939, 
  -0.3797028265003652, 4.662325971047792, -19.10588942956311, 
  0.01611130716582129, -0.3864545437919397, 4.746679790526477, 
  -19.33511501955221, 0.01607978232481002, -0.3854677277501663, 
  4.73707777882417, -19.25569672269431, 0.01695810061222789, 
  -0.4046275158932987, 4.874093647969457, -19.78168801790867, 
  0.01689186141983643, -0.4026909718821782, 4.855744292355989, 
  -19.73128523156167, 0.01624654780295389, -0.3908416368743579, 
  4.790386310712458, -19.53963127167953, 0.01608530534649958, 
  -0.385640175409824, 4.740326049603597, -18.96333837519587, 
  0.01608159837671767, -0.3856208097765868, 4.740696863142738, 
  -18.96819797494949, 0.01607611213382641, -0.3847448010956315, 
  4.726774331715202, -19.0653452278504, 0.01607513138022208, 
  -0.3854217937003934, 4.737026649610084, -19.32735754972278, 
  0.01512532717633623, -0.3562412988500165, 4.452810809890872, 
  -18.17845909874614, 0.01528227261378342, -0.3601509948325189, 
  4.485288323663527, -18.28335656034938, 0.01651927687718883, 
  -0.3980618202722723, 4.853682960734621, -19.72069906906984, 
  0.01648340852379357, -0.3975450380381269, 4.851853266497981, 
  -19.81592066376576, 0.01607968136083972, -0.3848792122124652, 
  4.72821291303906, -19.07197457439526, 0.01674435370878059, 
  -0.4060887216301592, 4.93771071660823, -20.18165645193199, 
  0.01591497560718322, -0.3875151010382588, 4.800394075475992, 
  -19.75872143133074, 0.01678598187439792, -0.4080452152428211, 
  4.960056810827773, -20.37568572413593, 0.01678366013817895, 
  -0.4079510925850759, 4.959016790576747, -20.37164077639627, 
  0.01446490755198233, -0.3640783226734743, 4.701403884474021, 
  -19.97334410453519, 0.01336536514531809, -0.3468236295448472, 
  4.63344379210652, -20.16273595321535, 0.01575675326185649, 
  -0.3891771313690958, 4.855259786243619, -20.24192799552621, 
  0.01412215819141029, -0.3588873971174609, 4.683274402868129, 
  -20.0307061769838, 8.312503253669523e-03, -0.1898635251425451, 
  3.09848596133249, -14.17601421718281, 0.01764084997621012, 
  -0.4322029071799307, 5.191769085446127, -20.32392645516661, 
  0.01164017599915519, -0.3241223645618151, 4.629821098889612, 
  -19.85728671201996, 0.01735457170668762, -0.429062142555344, 
  5.198757632092133, -20.37068017814066, 0.01701554653474726, 
  -0.427501766788619, 5.241148441060389, -20.63496923456497, 
  0.01660709881284158, -0.4064268235827433, 4.976348427553561, 
  -19.69598729761744, 0.01656592770094802, -0.4056065433966818, 
  4.971088916895506, -19.69030616806491, 6.910754370567942e-03, 
  -0.2297460843463133, 4.047886904675388, -18.77227536564263, 
  0.01354540636774852, -0.3607076275059714, 4.846168465029066, 
  -20.33778278761968, 0.01481622886271484, -0.3868332122718936, 
  5.019193243674478, -20.42575383698335, 0.01365880595701911, 
  -0.3631072570632005, 4.862763448932674, -20.36898849499574, 
  0.01413396552047578, -0.3745882846315332, 4.956732254764512, 
  -20.41939706835304, 0.01404560508611644, -0.3727109045646714, 
  4.943696635011682, -20.39786449913723, 7.202705727107518e-03, 
  -0.2355816523338849, 4.08415097410572, -18.85787293762389, 
  7.887252769827987e-03, -0.2484043778150136, 4.156437302528998, 
  -19.10426689200489, 0.01661362642068862, -0.4146647493928503, 
  5.110776266890758, -19.96770408049936, 7.785556715053307e-03, 
  -0.2461557948460157, 4.140293792446795, -19.07332453119833, 
  0.01057457113514126, -0.3020027266810234, 4.486538050200457, 
  -19.74944428937879, 0.01057457113514126, -0.3020027266810234, 
  4.486538050200457, -19.74944428937879, 0.01098999470116892, 
  -0.3101133151488093, 4.534438393181332, -19.7849102303083, 
  7.965249078703216e-03, -0.2474021974033382, 4.119380111177888, 
  -19.13550305456974, 8.104074443753873e-03, -0.2502862734192128, 
  4.13864985577035, -19.15893528693336, 8.0401452610993e-03, 
  -0.2489050963716939, 4.128912900404218, -19.1386582607522, 
  7.079572682538857e-03, -0.2279893320822776, 3.979939154900678, 
  -18.94690338451728, 6.215832111283265e-03, -0.2114205932335253, 
  3.890704073493253, -18.77543891309435, -6.151074762620318e-04, 
  -0.06907062101170626, 2.944920714088519, -16.78684517320129, 
  5.171727807838482e-03, -0.1877606466846136, 3.710932601008784, 
  -18.47544139195373, 5.095236466191807e-03, -0.1853293097130513, 
  3.686186853221475, -18.40018871539128, 5.175169857236185e-03, 
  -0.1886805942511676, 3.72971844126609, -18.47890803956224, 
  6.54601651270524e-03, -0.2208890213901238, 3.977659309808763, 
  -18.66710358133892, 6.508536648904388e-03, -0.2200504141295664, 
  3.971581073671179, -18.65565190976963, 6.873813174460339e-03, 
  -0.2264036885495892, 4.001864929378166, -18.83631383963951, 
  -1.29399514777586e-03, -0.05422945239460161, 2.835115964603641, 
  -16.58054215154629, 0.01096905924887359, -0.309934451908228, 
  4.537002079630077, -19.82848909064755, 0.01065988648758065, 
  -0.3038362401127417, 4.49940261377321, -19.77505054612377, 
  -7.650427735468065e-04, -0.06308880725590597, 2.878288100351326, 
  -16.67244050334723, -2.465557054542645e-03, -0.027335417938783, 
  2.635181940215198, -16.24319209083475, 6.833301354873819e-03, 
  -0.2255071240823108, 3.995422536679198, -18.82341623626277, 
  -9.083427810955231e-04, -0.05695457897238654, 2.807858658486866, 
  -16.61542510757615, 3.705439372920352e-03, -0.1569118824011367, 
  3.513208664832278, -18.12880544871659, 1.301621958078831e-03, 
  -0.1038038519167306, 3.127009328294562, -17.41775301978725, 
  1.333805167061397e-03, -0.1045170529547216, 3.132140788445743, 
  -17.42903669811651, -1.004823141565147e-03, -0.05139583338241942, 
  2.753405796286308, -16.65318985374902, -2.491583318120804e-03, 
  -0.01570541407832532, 2.485181557439895, -16.22976447605183, 
  7.797394036588313e-04, -0.09188543048844168, 3.046726256114427, 
  -17.29934035359863, -1.563015283630051e-03, -0.03829260657669534, 
  2.657280580690201, -16.48600545074611, 0.01758604727464278, 
  -0.4304295362550089, 5.17369957168572, -20.25516835535822, 0.0139487952631281, 
  -0.3277089847275479, 4.222644581040134, -17.86545906839283, 
  0.01153055589641775, -0.2666579691008834, 3.709667052589645, 
  -16.11964839645557, 0.0113828610988249, -0.2631798456135265, 
  3.682343133532276, -16.05325609599981, 9.649697855128255e-03, 
  -0.2218362356238396, 3.353472796328675, -15.27950845912205, 
  9.657417257316836e-03, -0.2220082981316167, 3.354745993131588, 
  -15.28942768227944, 0.01701480084051232, -0.4087604854475513, 
  4.934291267827721, -19.86846139605384, 0.01392456872661931, 
  -0.3272326798038054, 4.219762400388769, -17.93225889615509, 
  0.01366183130589335, -0.3207070756276397, 4.165763551405682, 
  -17.53811600365551, 0.01391468068254476, -0.3269982807618502, 
  4.217916017525624, -17.91923139805467, 0.01431741407301717, 
  -0.3370204436613317, 4.301113610456858, -17.95355868104626, 
  0.01431740170718458, -0.3370135329467013, 4.301007671460418, 
  -17.96350158666704, 0.01685181099078727, -0.4044610289117875, 
  4.896562991601122, -19.78089702808694, 0.01662405449852008, 
  -0.3976165094381263, 4.830034924453829, -19.71174914824316, 
  0.01134335000213215, -0.2630919253085229, 3.688369770475481, 
  -15.96842480355948, 0.01661697331268886, -0.3975477504830696, 
  4.830245432257528, -19.72312641025728, 0.01559412723601299, 
  -0.368999744237374, 4.568447795048298, -18.97240836667947, 
  0.01559412723601299, -0.368999744237374, 4.568447795048298, 
  -18.97240836667947, 0.01530632806449638, -0.3617460243743219, 
  4.507349031524173, -18.76802243017728, 0.01581483876285919, 
  -0.3765438608738398, 4.646747491105296, -19.3947225691539, 
  0.01579146967684098, -0.3759195746190456, 4.64122304484001, -19.3644110970297, 
  0.01579471325891119, -0.3760357595576774, 4.642437453840363, 
  -19.37120256597766, 0.01580068610169597, -0.3764635643267326, 
  4.647880977633112, -19.52811775496816, 0.01650950215247892, 
  -0.3963705620010037, 4.831372629906778, -20.01529747552541, 
  0.01698283866253878, -0.4088212743935661, 4.940311525980474, 
  -20.28809146923647, 0.01610947967208507, -0.3866122159977436, 
  4.751174545998796, -19.94712569816581, 0.01640315632127065, 
  -0.3925608242665184, 4.789779763477605, -20.03209430873462, 
  0.0164537475781556, -0.3962894708520976, 4.840341543110246, 
  -20.13723136987955, 0.01676462868195198, -0.4030671499446091, 
  4.890690315648831, -19.85715299854811, 0.01675667895603538, 
  -0.4029337042991554, 4.890057996706648, -19.86035208119481, 
  0.01661540099251793, -0.3984591955040059, 4.844868578275502, 
  -19.8595981250605, 0.0168907093712278, -0.4064903834146957, 4.920543736756825, 
  -20.29447368723757, 0.01546103058644638, -0.3655957934698482, 
  4.539390058089476, -18.87481833564339, 0.01558501103064054, 
  -0.3687620688619545, 4.56641236902988, -18.96155358449073, 
  0.01665263019550592, -0.4034804023217772, 4.917963419380706, 
  -20.39813213279084, 0.01663590309500846, -0.4033910148404958, 
  4.919615076872419, -20.49452405837917, 0.01661066610896151, 
  -0.3984003266668015, 4.844810714271246, -19.86335572932865, 
  0.01628089605271806, -0.3968896514405773, 4.882948570985148, 
  -20.53070536647049, 0.01617582008058177, -0.3937150283054604, 
  4.852371784612038, -20.35383600615533, 0.01571523980481356, 
  -0.383838968057119, 4.781687132191208, -20.38361567085153, 
  0.01572289017712602, -0.3839665408384457, 4.782280301718813, 
  -20.38315262554281, 0.01500394407529792, -0.3749865225445256, 
  4.776708960932312, -20.59692371525378, 0.01337344792117653, 
  -0.3435874940340646, 4.586563113698936, -20.43885584425859, 
  0.01569457104813041, -0.3868908548578843, 4.834237870869784, 
  -20.62679798800206, 0.01463516631487808, -0.3688154284629163, 
  4.748366778772635, -20.62304345467366, 8.983056151270411e-03, 
  -0.2063253891901465, 3.23310045184249, -14.96653390514944, 
  0.01580966944526371, -0.3766528327178619, 4.649413507342154, 
  -18.95394067620875, 0.0157324872446341, -0.3745019823118549, 
  4.629792472380002, -18.90371920840627, 0.01446571456849159, 
  -0.3405648633164163, 4.32931046965254, -18.16446555205686, 
  0.01445265690201578, -0.340251272783731, 4.326809510347545, 
  -18.16652247631266, 0.01595059576271076, -0.4084959206563935, 
  5.14524364965509, -21.23731204183873, 0.01721749445888508, 
  -0.4183315265273768, 5.049924377298788, -20.63354713205149, 
  0.01717060983974377, -0.4167983091243726, 5.033812436094497, 
  -20.33124391633331, 0.0172011961109357, -0.4180120240551465, 
  5.047920233921075, -20.61929443894634, 0.0170982062923231, 
  -0.4179905892999281, 5.067571354478464, -20.52166636541185, 
  0.01713580608278282, -0.4187351037255866, 5.07231030652994, 
  -20.54393201945869, 0.01609794402135782, -0.4109315049705172, 
  5.155016649927838, -21.2613573280374, 0.0167110191194619, -0.421456470012777, 
  5.204081026190214, -21.45141963711672, 0.01605422649898353, 
  -0.3818039668840406, 4.685671622332194, -18.989307666985, 0.0167059581814662, 
  -0.4213533488528203, 5.203397576208643, -21.46177892877124, 
  0.01756348801971916, -0.4325902579838207, 5.214511550538329, 
  -21.30286089973223, 0.01756348801971916, -0.4325902579838207, 
  5.214511550538329, -21.30286089973223, 0.01766425523036946, 
  -0.4333404744819977, 5.206729307960867, -21.21490633120592, 
  0.01702047959144326, -0.4234957421490746, 5.174136180136413, 
  -21.46745049557546, 0.01703706956334631, -0.4237113386280675, 
  5.174345649373926, -21.45089817650435, 0.0170154559906154, 
  -0.4232681438919013, 5.17139014024596, -21.44845383360435, 
  0.01682922956374606, -0.4192603373936929, 5.142808870185134, 
  -21.51866996235815, 0.01627033535421498, -0.412275665790533, 
  5.141612402658641, -21.60191308940338, 0.01607727755002233, 
  -0.4102507265696401, 5.147308677009384, -21.63579619558683, 
  0.01586622994882493, -0.4018245546403158, 5.050122755846782, 
  -21.49492335340391, 0.01597799041336903, -0.4036194564939151, 
  5.057087159030743, -21.50004665838649, 0.01568594779720081, 
  -0.400396935600054, 5.063723473465418, -21.52526332132286, 
  0.01628236229853582, -0.4148136700381341, 5.181714332518689, 
  -21.4235399028389, 0.01627712597076018, -0.4147052517260942, 
  5.180981947179995, -21.42701453356242, 0.0164712032162758, 
  -0.4168639175669602, 5.176778499453105, -21.50769819320384, 
  0.01611299313109767, -0.4105711629940699, 5.145342441341705, 
  -21.67774083294698, 0.01762938460689896, -0.4332900978432204, 
  5.212833055457246, -21.26515212328077, 0.01756313860765838, 
  -0.4325825370880521, 5.214455376294027, -21.29601737606882, 
  0.01528289478966295, -0.3944954706126736, 5.048620198515303, 
  -21.57550528631274, 0.01518116402349166, -0.3922864181057144, 
  5.032715571106514, -21.62228513702702, 0.01645140599981373, 
  -0.4164523792084441, 5.173987212901053, -21.50549978646529, 
  0.01417391545400967, -0.3713606264305079, 4.891257990725285, 
  -21.41862580626591, 0.014453157101266, -0.3768931859951553, 4.926052735828094, 
  -21.40403562090683, 0.01363001349261062, -0.3583199891399852, 
  4.785903821723429, -21.25537268234997, 0.01366634452967965, 
  -0.3590787379953567, 4.791074188167006, -21.26537763964078, 
  0.0113033813411879, -0.3131377087064334, 4.515088062529629, 
  -20.85227631350173, 8.786676220420096e-03, -0.2608983233666188, 
  4.165465174393151, -20.30628458408168, 0.0124598046157112, 
  -0.3358391368415484, 4.653242810720694, -21.06850291592727, 
  0.01020071799023698, -0.2905967205763067, 4.366984694565699, 
  -20.59620439350552, 0.01392008079713121, -0.3269784013591531, 
  4.216460263826973, -17.83973401206208, 0.01321880441713106, 
  -0.3087877565164844, 4.059922339153419, -17.03001828586137, 
  0.01207081207770948, -0.2799206847408843, 3.818140436800444, 
  -16.39640493836528, 0.01209605931678694, -0.2804984120532662, 
  3.822535976244451, -16.41197857009664, 0.01715818067660215, 
  -0.4220341222783548, 5.121814136477569, -20.47712385674381, 
  0.01589306961189147, -0.378321596211233, 4.660414426231291, -19.0745157959873, 
  0.01565526995273918, -0.3718479656468177, 4.602545952783058, 
  -18.67007042462373, 0.01588925588098818, -0.3781643944102411, 
  4.658667638470059, -19.06319895717979, 0.01633250070006451, 
  -0.3887713413805569, 4.743966957420243, -19.11400224100345, 
  0.01633427028282966, -0.3888737681631399, 4.745232531972796, 
  -19.12554850249077, 0.01704950364527847, -0.4189641476216954, 
  5.09316962577677, -20.40588650645331, 0.01643255045514843, 
  -0.4039294395812847, 4.969374097795065, -20.18068778907963, 
  0.01383501224392339, -0.3235770511912988, 4.178297354934524, 
  -17.16140303541574, 0.01637604328193411, -0.4028179506453952, 
  4.962367909860379, -20.17248524617889, 0.01642913503056386, 
  -0.3968195037438198, 4.854332194457681, -19.76399019948327, 
  0.01642913503056386, -0.3968195037438198, 4.854332194457681, 
  -19.76399019948327, 0.016566791771393, -0.3985682829111299, 4.855610088215969, 
  -19.69766732161566, 0.01616574155833733, -0.3945011715061152, 
  4.867800629536638, -20.02858673479397, 0.01640741337330543, -0.39978972985857, 
  4.90569210082873, -20.10213093758445, 0.01639404718792211, 
  -0.3995482093658101, 4.904364877695977, -20.10187911290602, 
  0.01619199393385088, -0.395670437644138, 4.880657919719122, 
  -20.19046470985488, 0.0156187731753363, -0.3879001160103509, 
  4.868111443019171, -20.22645445581366, 0.01570160430623603, 
  -0.3895010367429669, 4.877928324133196, -20.20108781771009, 
  0.01550804417950935, -0.3845835307300003, 4.834773401772323, 
  -20.25519575415504, 0.01551558257227986, -0.3841006350981409, 
  4.825477262810608, -20.22111168154699, 0.01522410891699613, 
  -0.3805013361848128, 4.825065510747307, -20.21389874197248, 
  0.01654946717893066, -0.4089530608460207, 5.028482240506469, 
  -20.32361999739216, 0.01652163227336141, -0.4083954244396528, 
  5.024876180879132, -20.31858219356591, 0.01607223949261417, 
  -0.3973644514487358, 4.933126212083768, -20.19520167619675, 
  0.01562173970042349, -0.387604903948404, 4.862724854697895, 
  -20.22294397856519, 0.01634037855186648, -0.3941968599810912, 
  4.829316163494584, -19.67004000885303, 0.01644370083598173, 
  -0.3970708855949487, 4.855594773290012, -19.76219395790609, 
  0.01513839402961841, -0.3797473240376246, 4.830405065213585, 
  -20.25911927979487, 0.01496019499222343, -0.3761369008196127, 
  4.805736011531879, -20.28634636775102, 0.01605019512092484, 
  -0.3969296253837283, 4.930374331195943, -20.19150088130727, 
  0.01424038542735933, -0.3625097236439974, 4.726305295467876, 
  -20.23888979025148, 0.01453112915269092, -0.3688973953365577, 
  4.773194615525952, -20.27463495166328, 0.01361172110460082, 
  -0.3490697572433711, 4.631074898418928, -20.12388302718789, 
  0.01362741736737682, -0.3493829230385579, 4.63308699085619, 
  -20.12741801168555, 0.01181308220086594, -0.3160694135019669, 
  4.449732265585032, -19.90539684441839, 9.272290998988112e-03, 
  -0.2646324154171417, 4.114805592006841, -19.39589607103863, 
  0.01288890700971349, -0.3369046091132352, 4.575390584078559, 
  -20.11052893967014, 0.01109466077827235, -0.301458935866332, 
  4.354995234812396, -19.75758348727961, 0.01149315551059416, 
  -0.2657670290364934, 3.702583500697727, -16.09265644181072, 
  0.01186365501719549, -0.2751110196872465, 3.780893962376286, 
  -16.30623071604027, 0.01188789947902094, -0.2756660590352941, 
  3.785118834013713, -16.3215933194294, 0.01720920108952341, 
  -0.4225656611083872, 5.120422993080004, -20.46370934997472, 
  0.01576282679035721, -0.3749876557311663, 4.632025977615272, 
  -19.00118334292851, 0.01557024243090788, -0.3695566869856171, 
  4.582158567721829, -18.61500124453871, 0.01575853936167338, 
  -0.3748225999616132, 4.630242868269995, -18.98957902935655, 
  0.01622116183455018, -0.3858711662773179, 4.718881412613837, 
  -19.04639891568322, 0.01622350028625094, -0.38598113103593, 4.720161255179485, 
  -19.05818601553772, 0.01707700726715722, -0.4189819968814562, 
  5.088071681356532, -20.38421250782186, 0.01649825032508801, 
  -0.4047583110746319, 4.970001580928516, -20.1749959910552, 
  0.01370759411515023, -0.3204798001375239, 4.153160465331435, 
  -17.09848012502269, 0.01644260645724562, -0.4036667447009868, 
  4.963148465676626, -20.16747478904044, 0.01637709396823283, 
  -0.3951036028436912, 4.836858847064875, -19.71634536735863, 
  0.01637709396823283, -0.3951036028436912, 4.836858847064875, 
  -19.71634536735863, 0.01653272530089169, -0.3972786540706439, 
  4.841524582948862, -19.65909949171371, 0.01641104770491699, 
  -0.3994649051521627, 4.899804448022491, -20.10396634986296, 
  0.01640334226005321, -0.3991319550995436, 4.895949654019199, 
  -20.07562122826956, 0.01639013750363669, -0.3988953985668193, 
  4.894670303283915, -20.07561137545045, 0.0161884437736153, 
  -0.3950229667388721, 4.87098240669127, -20.16513625122151, 
  0.01570963571595495, -0.3892803603975411, 4.872743565747826, 
  -20.23360515916189, 0.0162678699898062, -0.4038332568343881, 4.99953176186038, 
  -20.5580965345489, 0.01557036241785001, -0.3853705363402627, 
  4.835392218932953, -20.25532288145769, 0.01557481460804034, 
  -0.3848593478278757, 4.826251772699893, -20.22281553793163, 
  0.01526194939846748, -0.3807263106170647, 4.821356595300505, 
  -20.20239684545858, 0.01658997234800819, -0.4092505242587581, 
  5.025369374436823, -20.30912259559294, 0.01656229401346591, 
  -0.4086970651323669, 5.021799970187462, -20.30431485691243, 
  0.01608867446800262, -0.3971070037186536, 4.925795951472638, 
  -20.17196867484774, 0.01614004278988702, -0.4008015242580149, 
  4.975280061004331, -20.55594366880159, 0.01634895342747149, 
  -0.3938110600030056, 4.821539014291872, -19.64592014348902, 
  0.0163910800044819, -0.3953422436186508, 4.838028521422308, 
  -19.71415942482404, 0.01559038452240454, -0.3912034235144988, 
  4.927038876899984, -20.54346399030194, 0.01545372350729017, 
  -0.3886825375264298, 4.912706290388241, -20.60541031901631, 
  0.01606653047500309, -0.3966713187054917, 4.923048949350731, 
  -20.16840586539783, 0.01449580653578132, -0.3686375886354051, 
  4.775742719460637, -20.38775545761276, 0.01461710963839201, 
  -0.3702123375304376, 4.777644067762886, -20.28441816164614, 
  0.01378462317124669, -0.3527927054871092, 4.656439124298586, 
  -20.19293051470902, 0.01380035242199955, -0.3531035630332883, 
  4.658411499790721, -20.19628012102704, 0.01197758337830955, 
  -0.3195218445258918, 4.473439411181787, -19.97507119688035, 
  9.450992778462787e-03, -0.2683290201945103, 4.139651302891211, 
  -19.46764173415758, 0.01298646821255915, -0.3384971599232282, 
  4.582047546514962, -20.12818630058102, 0.01125769810763588, 
  -0.3049741401472766, 4.380048473932274, -19.8337055543305, 
  0.01134524048399774, -0.2622838919866578, 3.675223351377089, 
  -16.02612454164519, 0.01007991712380258, -0.2323723538097526, 
  3.439384414662892, -15.51530095312904, 0.01719034494033623, 
  -0.4146435052855548, 4.995401883323805, -20.06505368292892, 
  0.01449648942223847, -0.3413597517262509, 4.336213033087439, 
  -18.25208392961771, 0.0142212337471414, -0.3346334211163204, 
  4.281346560139111, -17.84979178214784, 0.01449151413682074, 
  -0.3412436281343895, 4.335312398253965, -18.24081104390627, 
  0.01477708216794929, -0.3487908434909765, 4.401487927020719, 
  -18.22971622933984, 0.01477454934610693, -0.3487144154783505, 
  4.400759989723843, -18.23870905144469, 0.01713809401768258, 
  -0.4127275834757476, 4.974561007195388, -20.0184662335405, 
  0.01677155513370917, -0.4027400483552252, 4.884223411777492, 
  -19.89732755516938, 0.01212642805755796, -0.2813759398720815, 
  3.830906019290594, -16.33044108170133, 0.01675873663474652, 
  -0.4025325596178483, 4.883321908702023, -19.90673822758287, 
  0.01571229757829891, -0.3731965095614887, 4.613361678000265, 
  -19.12905147212374, 0.01571229757829891, -0.3731965095614887, 
  4.613361678000265, -19.12905147212374, 0.01548475535313527, 
  -0.3671884356977242, 4.560659283526312, -18.94302112654486, 
  0.01638903905461283, -0.3907805565172365, 4.764925995552451, 
  -19.73728180315577, 0.0163564353164654, -0.3899480326545737, 
  4.757834617546608, -19.70297762417581, 0.01635804937423717, 
  -0.3900293986200199, 4.758802785701546, -19.70953907794862, 
  0.01633094448062505, -0.389724484285713, 4.758846828537162, 
  -19.85676975637258, 0.016535583992092, -0.3987673037287297, 4.865265150661589, 
  -20.16255063947253, 0.0165516217923302, -0.3989992074951558, 
  4.865950612918831, -20.11672189237333, 0.01641787835996447, 
  -0.3951747892706681, 4.829357259183674, -20.20513713814782, 
  0.01633846745483864, -0.3928799967382184, 4.807780655398406, 
  -20.14555329995413, 0.01640020063460258, -0.3969922621677952, 
  4.862409196846494, -20.26242147293754, 0.01705354348533593, 
  -0.4112426600276087, 4.966807181204015, -20.09513423428173, 
  0.01704370814761795, -0.4110625617806953, 4.965793395302903, 
  -20.09773712527875, 0.01673090969668389, -0.4028498029330532, 
  4.893519312026707, -20.03791750065399, 0.01646336132536909, 
  -0.396822471224794, 4.847934729925114, -20.1302262415562, 0.01559318932636509, 
  -0.3700679002736095, 4.586034401609016, -19.03491361653857, 
  0.01570900416403364, -0.3730922527179288, 4.61234185651608, 
  -19.12021632501049, 0.01656791950579097, -0.4019685004377104, 
  4.910029285242343, -20.40942171356294, 0.01641877421434524, 
  -0.3985329341822591, 4.883389819402076, -20.42771582998498, 
  0.0167218811022397, -0.4026960475909036, 4.892767522822812, 
  -20.04034620873857, 0.01596927771804751, -0.3909524721947351, 
  4.848235465195842, -20.50101163274719, 0.01611981860345594, 
  -0.3943463126490752, 4.874016093245075, -20.48339705877692, 
  0.01586546812746925, -0.3893502137144688, 4.841247539994202, 
  -20.61599108941098, 0.01587628500897176, -0.3895402255228874, 
  4.842237185394619, -20.61619369273426, 0.01436486474112124, 
  -0.3628099678950795, 4.705304448596118, -20.5129599555695, 
  0.01271035758600425, -0.3317696645002797, 4.524558344272822, 
  -20.39996717121787, 0.01519300807062988, -0.3779744589249024, 
  4.78856028531562, -20.60869712669417, 0.01377208766556857, 
  -0.3517728804072908, 4.642004640228986, -20.45794841091718, 
  9.604821200560667e-03, -0.2207979745355817, 3.345459808202456, 
  -15.25026606341815, 0.01718585478646967, -0.414548640277745, 
  4.994747059355336, -20.07068063413586, 0.01449203079261617, 
  -0.3412556176695198, 4.335404846175962, -18.25715078272178, 
  0.01422490421180513, -0.3347262495405725, 4.282122813529883, 
  -17.85873551167774, 0.01448822532630328, -0.3411679695783523, 
  4.334734214366366, -18.24635268323822, 0.01477964728190152, 
  -0.3488657576172812, 4.402186350194766, -18.23817188693664, 
  0.01477639231330114, -0.3487703671510942, 4.401294963551501, 
  -18.24686109446793, 0.01713657716012666, -0.4126895083150966, 
  4.974248208640644, -20.02499076044333, 0.0167788224067446, 
  -0.4028561467991169, 4.884711037156958, -19.90604117506926, 
  0.01212783796933372, -0.2814090240116193, 3.831164979909937, 
  -16.3393812735233, 0.01676717573148206, -0.402669491009408, 4.883920471476373, 
  -19.91579668013632, 0.01570841796473832, -0.3730738362381931, 
  4.612162515902694, -19.13366867135069, 0.01570841796473832, 
  -0.3730738362381931, 4.612162515902694, -19.13366867135069, 
  0.01548047004729858, -0.3670566777557606, 4.55939201590338, 
  -18.94765540310774, 0.01638648578225371, -0.3906491387841773, 
  4.763352784531854, -19.74138021666472, 0.01635373841746988, 
  -0.3898140922669797, 4.756248133000286, -19.70712166737091, 
  0.01635536637731798, -0.3898946417490822, 4.757201110104612, 
  -19.71368932867534, 0.01632749676980107, -0.3895590416144437, 
  4.756907599476815, -19.85991109932925, 0.01655366706615776, 
  -0.3990726342411552, 4.866729372095969, -20.17378281146414, 
  0.01656895661577248, -0.3992919947155104, 4.867355647701617, 
  -20.12794775600493, 0.0164287669615499, -0.3953027864656637, 
  4.829374438594646, -20.21319697694642, 0.0163479955441661, 
  -0.3929788334764971, 4.807590949541267, -20.15317923682083, 
  0.01642237751297191, -0.3973724119743953, 4.864292493183614, 
  -20.27511409761879, 0.01706666954460961, -0.4114824364402946, 
  4.968151372685199, -20.10621098760663, 0.01705751918372414, 
  -0.4113153608217047, 4.96721546926101, -20.10903853192736, 
  0.01674545604349204, -0.4030970998312357, 4.894724080394789, 
  -20.04877822325141, 0.01648089011479912, -0.3971152368681969, 
  4.849303999431109, -20.14116083019069, 0.01558906128892755, 
  -0.3699395455724268, 4.584791759316031, -19.03951536348421, 
  0.01570522008188808, -0.3729741643410602, 4.611196507352513, 
  -19.12491428746283, 0.01659334784933157, -0.4024171416859877, 
  4.91238782109542, -20.42324122085822, 0.01644489683954627, 
  -0.3989879151999446, 4.88572054421408, -20.44125224397603, 
  0.01673674765872607, -0.4029491180614063, 4.894003836088834, 
  -20.05132221952017, 0.01600734978010651, -0.3916592596416832, 
  4.852302761398287, -20.51908791165045, 0.01615674836341249, 
  -0.3950364519200986, 4.878033331581238, -20.50148187321522, 
  0.01589770836832696, -0.3899165038490723, 4.844196504465459, 
  -20.63161133315478, 0.01590857975314367, -0.3901074226936119, 
  4.845190398555389, -20.63179006992256, 0.01442189265450988, 
  -0.3639225820662973, 4.712230952957963, -20.53881205330692, 
  0.0127800129987926, -0.3331407127119589, 4.533208518513653, -20.4300802823541, 
  0.01523967397540808, -0.378855686293205, 4.793779133521404, 
  -20.63005595623214, 0.01383235707625898, -0.3529545311103585, 
  4.64941615249595, -20.48517683574475, 9.612818880292084e-03, 
  -0.2209762469007865, 3.346779029985822, -15.26029653688613, 
  0.0160824459711457, -0.4115948187104039, 5.169718553095167, -21.3717825801774, 
  0.01621049892140718, -0.4135367185300914, 5.175216431524073, 
  -21.10874358956544, 0.01609595632937072, -0.4118806697459564, 
  5.171694161400579, -21.3676714872385, 0.01581766120282784, 
  -0.4074088260250763, 5.155195237885478, -21.18097126925897, 
  0.01583927783790541, -0.4078626265861648, 5.158306119994752, 
  -21.19863611647373, 0.01007578454898659, -0.2959242537276966, 
  4.490130810468909, -20.14729660541763, 0.01108002350194283, 
  -0.3158456691166735, 4.613101244022373, -20.51393066233911, 
  0.01758007377647672, -0.4314581640128936, 5.192707474033726, 
  -20.55019664014986, 0.01102095671939833, -0.314563541772597, 
  4.604025425982943, -20.50251209112755, 0.01407695858430121, 
  -0.3745934916298091, 4.969198484773308, -21.18298787084928, 
  0.01407695858430121, -0.3745934916298091, 4.969198484773308, 
  -21.18298787084928, 0.01445427391298864, -0.381616227609422, 
  5.007645316565219, -21.20136472393608, 0.01238367616269781, 
  -0.3401618097277447, 4.746521588668378, -20.90877603152553, 
  0.01245196730502934, -0.3415109332155899, 4.754869450484036, 
  -20.91003249379277, 0.01240720049303275, -0.3405528247502091, 
  4.748170540123651, -20.89784788796089, 0.01205510174406379, 
  -0.3328467999350976, 4.692658193529471, -20.90554405029134, 
  0.01015499444139061, -0.2957387240387239, 4.470535263940921, 
  -20.46321658643152, 0.01024348003693838, -0.2976459238416575, 
  4.483909871026483, -20.44930672125932, 0.01035648224127195, 
  -0.298378831767386, 4.472361387612806, -20.56719845329079, 
  0.01068507683151024, -0.3048785175203855, 4.512671306138525, 
  -20.64823318269886, 9.636475282871289e-03, -0.2844819731743026, 
  4.392009191181106, -20.36608347616921, 9.821244844788121e-03, 
  -0.2903383775980356, 4.450248899064439, -20.13790663999501, 
  9.79788073015902e-03, -0.2898256760712687, 4.446588516130056, 
  -20.13346477576067, 0.01058919288075066, -0.3053698112106246, 
  4.541361092069004, -20.44993529894095, 0.01038884497034325, 
  -0.3004870415847439, 4.501201888366065, -20.54286273295784, 
  0.01436170865164514, -0.380092948868388, 5.001970257312586, 
  -21.22397392013534, 0.01411313797298908, -0.3753646887154276, 
  4.974569694664378, -21.18976311457301, 8.792630238951537e-03, 
  -0.267205585879255, 4.279233996589586, -20.13683559747809, 
  8.888104917644404e-03, -0.2688176331505522, 4.286513902495658, 
  -20.22727366180239, 0.01055856472176341, -0.3047024180333743, 
  4.536624454675118, -20.44211511763972, 7.692542558043028e-03, 
  -0.2433800233027589, 4.113028391211607, -19.95339826098176, 
  7.568544579570445e-03, -0.2412069605475337, 4.102083291890334, 
  -19.85915058458054, 6.951368154886307e-03, -0.2268098102312055, 
  3.990201565091951, -19.77034076174985, 6.981521070483141e-03, 
  -0.2274679064281359, 3.994879687446543, -19.78005442901442, 
  4.778602605078175e-03, -0.1811261054111203, 3.689606612568841, 
  -19.2342146473928, 2.875817787710058e-03, -0.1390915634903266, 
  3.392314788628434, -18.76545335446581, 5.686543672267622e-03, 
  -0.2002955426766123, 3.815480740260451, -19.45381082019608, 
  3.771791648915913e-03, -0.1593354256851688, 3.537658793660197, 
  -18.94627383632715, 0.01698607131441151, -0.4079991453702811, 
  4.927587197257994, -19.84036859977489, 0.01723054743894524, 
  -0.4180214691531642, 5.042350562216534, -20.4268276431636, 0.0172798268412014, 
  -0.4198908635740692, 5.063076146223465, -20.71749322218402, 
  0.01734399221390631, -0.4233251088389673, 5.106102353060868, 
  -20.6902202658523, 0.01736702591787965, -0.4237822997819469, 
  5.109022263447192, -20.70694347365182, 0.01616755212024519, 
  -0.4127274982932964, 5.170593568687386, -21.37213682815873, 
  0.01651054201838409, -0.4176322816496668, 5.181500479273592, 
  -21.45776611198869, 0.01614058977635213, -0.3838564269605939, 
  4.702083328551584, -19.11338635152571, 0.01646382219029345, 
  -0.4166613314766477, 5.174916629504257, -21.45276091111354, 
  0.01742266501452855, -0.4300540641789726, 5.200988727052906, 
  -21.32648242088629, 0.01742266501452855, -0.4300540641789726, 
  5.200988727052906, -21.32648242088629, 0.01754607076050203, 
  -0.4313165671135697, 5.197060342185416, -21.24830723121123, 
  0.01663133284332378, -0.4158251632190333, 5.12602408669236, 
  -21.39382706394728, 0.01664649537693854, -0.4160134101526945, 
  5.126070774760208, -21.37720758585069, 0.01661616898425517, 
  -0.415390775366544, 5.121911723562909, -21.37129627031387, 
  0.01638742324120003, -0.4105079422293876, 5.087470869715972, 
  -21.41929629592889, 0.01576855815969175, -0.4021694184874884, 
  5.076276199568571, -21.48402579728161, 0.01582386838511236, 
  -0.4033051257370713, 5.083835335195169, -21.45982227075635, 
  0.01530264599783095, -0.3904620431316593, 4.976451881644243, 
  -21.34203611201012, 0.01541881424146324, -0.3923197888255218, 
  4.98356028707164, -21.34551712351804, 0.01509157103214046, 
  -0.3883453527772476, 4.985010810271467, -21.36487838043697, 
  0.01598504391751971, -0.4089269305841637, 5.144753190042587, 
  -21.39830189664127, 0.01596146774785603, -0.4084342807351902, 
  5.141391336695744, -21.39488352976494, 0.01602460663312088, 
  -0.4079042850563019, 5.119196096457801, -21.42056567046791, 
  0.01584110434026938, -0.4033208697130823, 5.080575603338753, 
  -21.49862302916783, 0.0174966747450147, -0.4309350272689174, 
  5.200656712080669, -21.29206306609996, 0.01744095772987928, 
  -0.4304261469600382, 5.203445373654941, -21.3262799468548, 
  0.01489557567344176, -0.385318882505308, 4.975423977108989, 
  -21.38106756927525, 0.01482993116609763, -0.3834245805118208, 
  4.957415505800409, -21.40433526310643, 0.01599375709074838, 
  -0.4072620267891542, 5.114832057902915, -21.4140262080652, 
  0.01368937971845257, -0.3606808604004074, 4.813000310873017, 
  -21.21330424719859, 0.01407963936171151, -0.3692216943317795, 
  4.875887126685051, -21.29334071030039, 0.0131152340115706, 
  -0.3478820208759255, 4.717837657424998, -21.08918244908968, 
  0.01314525264881005, -0.3485094037748444, 4.722116818961537, 
  -21.0975826581042, 0.01102465829557074, -0.3072495547370273, 
  4.474515581921247, -20.72652951339458, 8.818749361725792e-03, 
  -0.2616284739536194, 4.171109152061294, -20.27141895401502, 
  0.01201467862479413, -0.3267437455246568, 4.593439646770259, 
  -20.91338182087432, 0.01055157534891192, -0.2976863948446647, 
  4.414307764577782, -20.66067506528996, 0.013893328873576, -0.3264461607063538, 
  4.213166425934452, -17.90611111720985, 0.01724379457290338, 
  -0.4182806980232594, 5.043971320570828, -20.42179100211819, 
  0.01731636506141159, -0.4215289000573458, 5.082470138596807, 
  -20.35987388255274, 0.01732003782592365, -0.4216017839735472, 
  5.082935498659231, -20.3711724775466, 0.01636802560960142, 
  -0.4160734149797383, 5.185082686019458, -21.12859293218487, 
  0.01634560841124899, -0.4133123637232479, 5.143462330075219, 
  -21.10389256882559, 0.01585683649879915, -0.3766919821142446, 
  4.641767970618347, -18.65526108882975, 0.01626714657134633, 
  -0.4116826117427828, 5.132422861498569, -21.08862290657069, 
  0.01724939124162139, -0.4256786016924421, 5.163735092674849, 
  -20.98693744838206, 0.01724939124162139, -0.4256786016924421, 
  5.163735092674849, -20.98693744838206, 0.01730496784843275, 
  -0.4256210397069541, 5.151382986730087, -20.88683410839904, 
  0.01616949894903597, -0.4055425481184389, 5.049465955863293, 
  -20.97659542411547, 0.01619781238035354, -0.4060047689159547, 
  5.051381824186695, -20.96240466572521, 0.01615813862961812, 
  -0.4051917210145566, 5.045965431652548, -20.95364098058331, 
  0.01577932632256064, -0.3972517396772596, 4.99134321400285, 
  -20.97087991675996, 0.01515619318563842, -0.3885682993380857, 
  4.975514609231078, -21.01079150082659, 0.01526085977205717, 
  -0.3907279108665164, 4.989996016190124, -20.99729229154727, 
  0.0144662996132148, -0.3724419088271281, 4.847814224959754, 
  -20.82348900079813, 0.01460671963014332, -0.3748436501397783, 
  4.858982603890324, -20.83747025500166, 0.01429295152706885, 
  -0.3708944780110698, 4.858355364984442, -20.83690379938356, 
  0.01589610395619445, -0.4062235836610379, 5.117980073548721, 
  -21.05921132710157, 0.01585878767475613, -0.4054441386049211, 
  5.112665757097407, -21.05126226871441, 0.01566581389125842, 
  -0.3995734102634464, 5.054147733768046, -21.00896335931184, 
  0.01520613766735047, -0.3892518241911986, 4.976666514170823, 
  -21.02080286670316, 0.01729442369655615, -0.4259283146666218, 
  5.158864904012535, -20.93978117956987, 0.01728218717695523, 
  -0.4263446255834947, 5.168121495191039, -20.99108415779216, 
  0.01413128275192716, -0.3684919524763171, 4.852323702891524, 
  -20.85840503963358, 0.01371968865245442, -0.3596131871866027, 
  4.788151436555748, -20.78992482260817, 0.01562466842968714, 
  -0.3987178024047485, 5.048343982818361, -20.99909999481888, 
  0.0125150529740246, -0.3353447980738503, 4.632359780775704, 
  -20.57638382956993, 0.01279282797926146, -0.3416761562392457, 
  4.680715159180449, -20.61727312477257, 0.0113001875813461, 
  -0.3094766847062928, 4.449923336150155, -20.26885359188287, 
  0.01133868397725761, -0.3102786598963351, 4.455375173687115, 
  -20.27989702799257, 9.253071444459936e-03, -0.2693080835062721, 
  4.207146824125942, -19.90179158811711, 6.421837587461112e-03, 
  -0.2102478456533477, 3.809473667798646, -19.24199919856372, 
  0.01036718328201663, -0.2914936768747446, 4.345119646504007, 
  -20.13323947403283, 8.044242436733639e-03, -0.2443474975681098, 
  4.040709815167565, -19.59628317892967, 0.01362821275818724, 
  -0.319864362585255, 4.158722432587901, -17.50994846052942, 
  0.01736406725919864, -0.423723568857678, 5.108647128748713, 
  -20.68767401192844, 0.01738439679991153, -0.4241271620002799, 
  5.111225350350467, -20.70352379815515, 0.01617226127465672, 
  -0.4128281394282001, 5.171296538896006, -21.36488053147545, 
  0.01647715038589955, -0.4169383561083498, 5.17679532864877, 
  -21.43729588657454, 0.01613918485742842, -0.3838100637518279, 
  4.70161831948448, -19.10207040827079, 0.01642599056229474, 
  -0.4158748941930296, 5.169582417239979, -21.43069517952392, 
  0.0173957413701029, -0.4295063240700419, 5.197371516423816, 
  -21.30886769068836, 0.0173957413701029, -0.4295063240700419, 
  5.197371516423816, -21.30886769068836, 0.01751969675184947, 
  -0.4307818665385664, 5.193546577481027, -21.23075710702035, 
  0.0165733065929936, -0.4146334511462736, 5.11806036109967, -21.36538443356199, 
  0.01658908511124615, -0.4148347003639605, 5.118197183998653, 
  -21.34889875911949, 0.01655793553343058, -0.4141951357519212, 
  5.113924838533432, -21.3426692396001, 0.01631985447134962, 
  -0.4091210298823165, 5.078210596400279, -21.38782855109919, 
  0.01569575841853615, -0.4006543591308457, 5.06598793537692, 
  -21.44988590897719, 0.01575389440829169, -0.4018489514184421, 
  5.073947532341984, -21.42650860897863, 0.01521826602937207, 
  -0.3887156083200314, 4.964670081638256, -21.3040489827912, 
  0.01533624986486522, -0.3906129977299633, 4.972065146289983, 
  -21.30816163566939, 0.01500806072121435, -0.3866061458494272, 
  4.973190172733561, -21.32648832970752, 0.0159465605601966, 
  -0.4081227453284618, 5.139265173811389, -21.37554675376485, 
  0.01592112538849797, -0.4075911407090417, 5.135636715952235, 
  -21.37144430155936, 0.01596616920441457, -0.4066876534577487, 
  5.110929148086637, -21.39105106220375, 0.0157685726517393, 
  -0.4018123153278907, 5.070340079048853, -21.46469109021806, 
  0.01746990066218321, -0.4303912805774286, 5.197074719566102, 
  -21.27444403347765, 0.01741612296370023, -0.4299209811497802, 
  5.200109940740351, -21.30938106406178, 0.01481217285226436, 
  -0.3835755766513421, 4.963529635367723, -21.34236002561557, 
  0.01474162594927425, -0.3815802158929216, 4.94484523480073, 
  -21.36420019687973, 0.0159342966810554, -0.4060240533620957, 
  5.106419784346437, -21.3841169433186, 0.0135909973891652, -0.3586221219925209, 
  4.798933616240061, -21.16908382597605, 0.01399109103153052, 
  -0.3673640602654371, 4.863165931422377, -21.25211534056185, 
  0.01301462868779806, -0.3457793701170769, 4.70349609644697, -21.043939324699, 
  0.0130443557661006, -0.3464006693606085, 4.70773381263267, -21.05227731897683, 
  0.01092638420275165, -0.305175042723519, 4.460208698676249, 
  -20.68024282030559, 8.715863119508825e-03, -0.2594484316713113, 
  4.156019078821227, -20.22271756691278, 0.01191298027393054, 
  -0.3246068174114812, 4.578775147572286, -20.8668253676645, 
  0.01045873478984065, -0.2957210035210127, 4.400720141984178, 
  -20.61568153469793, 0.01388319910867794, -0.3262059737670598, 
  4.211273971603807, -17.89296428511632, 0.01760474038249054, 
  -0.4304585017265122, 5.171194274545642, -20.69804346667955, 
  0.01582687921208601, -0.4070138228634493, 5.146095114550874, 
  -21.15940038673656, 0.0158102699778221, -0.4045862064931449, 
  5.108352940103648, -21.14050943165389, 0.01634049332145563, 
  -0.3895570101586471, 4.755379660841312, -19.0356487066321, 
  0.01572328673732381, -0.4027681783570604, 5.095948004522738, 
  -21.12135925381011, 0.0168211807272469, -0.4191635912463466, 
  5.142836629285284, -21.04858992269213, 0.0168211807272469, 
  -0.4191635912463466, 5.142836629285284, -21.04858992269213, 
  0.0169808724612109, -0.4213947811407495, 5.14707730946634, -20.98843290349598, 
  0.01565456280290651, -0.3970034291306931, 5.013236586454509, 
  -20.99392895895971, 0.01569174986475092, -0.3976622036380024, 
  5.016575283186727, -20.98330994383061, 0.01564976090742553, 
  -0.3967946666832905, 5.010736953194427, -20.97325370444261, 
  0.01525576474694937, -0.3884891495611182, 4.953205023519265, 
  -20.97904796238208, 0.01456753290178916, -0.3785770401753118, 
  4.93057723312747, -21.01231867581937, 0.01467681759465765, 
  -0.3808473668981134, 4.945926599047326, -21.00208489177706, 
  0.01395940050896511, -0.3639786191223962, 4.811697476773415, 
  -20.83401706990111, 0.01406058889854737, -0.3654845813086624, 
  4.816038801248066, -20.83028241359874, 0.01368009747204711, 
  -0.3602732410372249, 4.808133609033227, -20.82057578500958, 
  0.01527725433322163, -0.3954738923882106, 5.066945789041241, 
  -21.05656612612584, 0.01523717569973506, -0.3946319959267101, 
  5.061168635739968, -21.04721566777141, 0.01505393542264995, 
  -0.3891371665568658, 5.006489633109718, -21.01078211186212, 
  0.01465547586709522, -0.3800615630878587, 4.937250834746554, 
  -21.03429954309322, 0.01691885073874782, -0.4205662124036478, 
  5.146291331342784, -21.0213408330088, 0.01686025392528861, 
  -0.4199651741592044, 5.148184860977974, -21.0553290541375, 
  0.01351696755557952, -0.3577584626019509, 4.800793626799199, 
  -20.83815586310553, 0.01335093981280127, -0.3538222946995915, 
  4.769352004417569, -20.83891049907299, 0.01501148481825416, 
  -0.3882491022708718, 5.000423414530844, -21.00000645350501, 
  0.01192163869244814, -0.3247683663362911, 4.579715215874304, 
  -20.54232642915055, 0.01219705450399571, -0.3310117855651382, 
  4.627192384995978, -20.58271438071151, 0.01111758760890883, 
  -0.3072015968781788, 4.452428962078191, -20.35158906136677, 
  0.01115162660224842, -0.3079160310475401, 4.457323755136974, 
  -20.36157353159023, 8.724791075984277e-03, -0.2596400661443384, 
  4.1574278792721, -19.86083949287388, 5.954876865438947e-03, 
  -0.2016181344386257, 3.764962487889774, -19.20329047042764, 
  9.885174011656186e-03, -0.282819746694929, 4.30220931702429, 
  -20.10769915591695, 8.190506175282291e-03, -0.2486054589218684, 
  4.085935488065329, -19.767963882424, 0.01427856139328853, -0.3360629357351927, 
  4.293240034852092, -17.92364401396513, 0.01585911164179833, 
  -0.4076896898876325, 5.150724414473218, -21.18079088923495, 
  0.01588001992613184, -0.4060439402342784, 5.118298763550589, 
  -21.17504881352188, 0.0163434959606721, -0.3896034597849553, 
  4.755556919062554, -19.04786990093759, 0.0157968941721422, 
  -0.4043066488078761, 5.106445497008614, -21.15735114172486, 
  0.01688117230187869, -0.4203943044932184, 5.151047996217336, 
  -21.07857425097072, 0.01688117230187869, -0.4203943044932184, 
  5.151047996217336, -21.07857425097072, 0.01703852957402534, 
  -0.422570548362871, 5.154865308987364, -21.01758229234382, 
  0.01574685311615049, -0.3989106919562931, 5.026076352137777, 
  -21.03565478755915, 0.01578242764661091, -0.3995356694876744, 
  5.029183082069982, -21.02459183873777, 0.01574093261694455, 
  -0.3986783593208764, 5.023413707098751, -21.01476504801846, 
  0.01535973447690455, -0.390636584010403, 4.967649584306926, 
  -21.02451241383447, 0.01467676751629895, -0.3808610409600129, 
  4.946167018046522, -21.06085978281515, 0.01478140165266535, 
  -0.3830340588622503, 4.960851645822259, -21.04921005965081, 
  0.01407975500439294, -0.3664823719281961, 4.828686421838317, 
  -20.88622527715201, 0.0141780274063961, -0.3679250838987307, 4.83257404433265, 
  -20.88147385532833, 0.01379650064186098, -0.3627084915094139, 
  4.82476709064265, -20.87236939396131, 0.01534982009449911, 
  -0.3969981380158405, 5.077404738021501, -21.09290880641027, 
  0.01531119123256864, -0.3961867511443906, 5.071837317271702, 
  -21.08412653806314, 0.01514534927440128, -0.3910494908894213, 
  5.019552096486889, -21.05353812102275, 0.01476500762302711, 
  -0.3823505144034736, 4.952862569254614, -21.08278381962584, 
  0.01697718765687201, -0.4217612231640242, 5.154247965260819, 
  -21.05084241281229, 0.01691819955131443, -0.4211538855207925, 
  5.156115769262406, -21.08458066579193, 0.01363158174614722, 
  -0.3601645515837971, 4.817285477496539, -20.88981308520379, 
  0.01347306616112287, -0.356384259170933, 4.786894381898156, 
  -20.89282886582811, 0.0151035180086097, -0.3901744136623569, 
  5.013574782618472, -21.04303933870799, 0.01205339748581917, 
  -0.3275370265019095, 4.59871568950305, -20.60033155899706, 
  0.01232206832906087, -0.333640855418245, 4.645254856275972, 
  -20.63861804190601, 0.01125125445976685, -0.3100070769954386, 
  4.471650059189763, -20.41051188411544, 0.01128579960300451, 
  -0.3107321254418866, 4.476617538424279, -20.42062724867827, 
  8.851258365372844e-03, -0.2623200599668224, 4.175980933828171, 
  -19.91950208988448, 6.088493464966645e-03, -0.2044603035208216, 
  3.784708617752158, -19.26536000357481, 0.01001788272726114, 
  -0.285619335600125, 4.321498901772411, -20.16743453006136, 
  8.309404446941003e-03, -0.2511324362222627, 4.103471741888925, 
  -19.82468066957763, 0.01427881609618307, -0.3360625119648621, 
  4.293186403250545, -17.9337242423722, 0.01167485409292585, 
  -0.3280387074915069, 4.692948337662716, -20.70240657205738, 
  0.01754577869412256, -0.4299902710345507, 5.175219639602314, 
  -20.51417420477715, 0.01162512412216562, -0.3269606316481798, 
  4.685327786679427, -20.69475870099992, 0.0145283357739639, -0.383528029997166, 
  5.024756419629281, -21.3116407623462, 0.0145283357739639, -0.383528029997166, 
  5.024756419629281, -21.3116407623462, 0.0148789762879484, -0.3899000020775837, 
  5.057991325995754, -21.31692265403467, 0.01296578202919729, 
  -0.3519684323329953, 4.822758472674407, -21.08811432297851, 
  0.01303116053854547, -0.3532560987883239, 4.830685712373007, 
  -21.08865831789421, 0.01298980114827906, -0.3523718224578697, 
  4.824510793839785, -21.07783019050247, 0.01267535813358889, 
  -0.3454810059891196, 4.774774676148996, -21.09853823353603, 
  0.01085086014114835, -0.3101482909769073, 4.566254349936004, 
  -20.6902827671891, 0.01093039467912244, -0.3118590449750657, 
  4.578221163218529, -20.67339952271788, 0.01070169986921973, 
  -0.305351608642756, 4.515837542623975, -20.67364826882452, 0.0109728204651981, 
  -0.3106230993790305, 4.547474715383042, -20.73456396948392, 
  0.01002866036132408, -0.2925659511223377, 4.444178980310615, 
  -20.49543769849257, 0.01027879455076008, -0.2997518499276728, 
  4.511559731426104, -20.28688136201827, 0.01025644368832084, 
  -0.299264059504511, 4.508091344128196, -20.28307772471366, 
  0.01100090752940363, -0.3137723153123342, 4.595220274123271, 
  -20.58132631681568, 0.01107910334069417, -0.3147707871256225, 
  4.595976649847631, -20.76734114848529, 0.01462762596851975, 
  -0.3851532147099245, 5.030772397200687, -21.29179995918261, 
  0.01455769160330197, -0.3841532732198804, 5.029106354112182, 
  -21.31586626821404, 9.50186560395504e-03, -0.2819734170518634, 
  4.37792388091592, -20.37317159783034, 9.273445622443146e-03, 
  -0.2767821742507111, 4.337963513896946, -20.35471429261046, 
  0.01096978383658811, -0.3130972932658201, 4.590446260980737, 
  -20.57358489359421, 8.054482303666342e-03, -0.250978442038854, 
  4.162813938546162, -20.07997945486049, 8.363030026436163e-03, 
  -0.2579224185200268, 4.215383865964735, -20.13221262412638, 
  7.760071215352612e-03, -0.2437844222608636, 4.104879847432589, 
  -20.04564507554509, 7.789994157860272e-03, -0.244436602303829, 
  4.109508965970142, -20.05516874945112, 5.040098351524436e-03, 
  -0.1867869379904886, 3.727128348747764, -19.33703991525961, 
  3.057418617187775e-03, -0.1432166079817914, 3.420114566987183, 
  -18.8483010849165, 5.951207244256348e-03, -0.2059229524863919, 
  3.852000326714768, -19.55181014647983, 4.530131755618022e-03, 
  -0.1756140143108863, 3.650077957012465, -19.22386190836884, 
  0.01682255101027169, -0.403686762772786, 4.889756270037815, 
  -19.75257306082827, 0.01714930170005904, -0.4193740249298994, 
  5.079981660956779, -20.39992425079854, 0.01287736740632695, 
  -0.352011012267762, 4.842293260763322, -21.14272980716724, 
  0.01540244604316402, -0.4000299412317905, 5.118260038419124, 
  -21.59906128000502, 0.01540244604316402, -0.4000299412317905, 
  5.118260038419124, -21.59906128000502, 0.01575951047799802, 
  -0.4064818774999775, 5.151841132069089, -21.6087374796927, 
  0.01453420494055566, -0.3835476929425055, 5.023853461236381, 
  -21.6341430966513, 0.01459112627329808, -0.38464096305496, 5.030309264201109, 
  -21.63229909795308, 0.0145665411925878, -0.3841179145127167, 
  5.026674706188076, -21.62791739384924, 0.01422493312440917, 
  -0.3767850990589515, 4.974461106916764, -21.63906214666708, 
  0.01249057535953309, -0.3436746881662698, 4.783556787288178, 
  -21.2781914535823, 0.01253799477376633, -0.3446794387088047, 
  4.790468652689594, -21.25189050320611, 0.01278188646541126, 
  -0.3481599339070707, 4.797322036079237, -21.4089339850684, 
  0.01305070463532767, -0.3533797063743296, 4.828657146938574, 
  -21.46987197269553, 0.01209772389192219, -0.3353375310300863, 
  4.726860780458864, -21.24072472639138, 0.01184185558550073, 
  -0.3316618178275586, 4.718575824604034, -20.86153345522619, 
  0.0118379172582876, -0.3315767493527291, 4.717976382629526, -20.865125108712, 
  0.01271392721690721, -0.3487433982692472, 4.822288917213704, 
  -21.19790516113799, 0.01266369340933575, -0.3470601533100344, 
  4.804100088631679, -21.33135646701457, 0.01558235588266508, 
  -0.4032558157603654, 5.134782161806912, -21.60383216617035, 
  0.0154012147063823, -0.4000045817102713, 5.118089455014276, 
  -21.59212116629673, 0.01136131419855294, -0.3206175254850773, 
  4.63398425653482, -21.06277139215852, 0.01150905824124434, 
  -0.3233078451494045, 4.648361529864636, -21.16395965397216, 
  0.01269660725754752, -0.3483702320087758, 4.819665530074088, 
  -21.19576830137692, 0.01040371900366351, -0.300291083060665, 
  4.495046658062785, -20.94746565390849, 0.01026322918584201, 
  -0.2977575199781082, 4.481785589408757, -20.85169124554424, 
  9.822305308031673e-03, -0.2871848771455573, 4.396516654495533, 
  -20.82094984209725, 9.854099074491694e-03, -0.2878709133632135, 
  4.401347224985983, -20.8305978591071, 6.897375237980224e-03, 
  -0.2265950837405052, 3.998675380404284, -20.08243297609936, 
  4.845777609100976e-03, -0.1823936502216813, 3.692539953670058, 
  -19.60592030175957, 8.471396163139831e-03, -0.2594713103052415, 
  4.217757642082288, -20.5082576009742, 6.311364784064633e-03, 
  -0.2140450535876754, 3.913530634053647, -19.95495715465808, 
  0.01660121274794556, -0.3969993104630892, 4.82451491655896, 
  -19.68728662102517, 0.01710588765803445, -0.4185214377814492, 
  5.074615651936818, -20.40044715641476, 0.01686739719288575, 
  -0.4061568584469081, 4.920505268985612, -19.8861966544423, 
  0.01686739719288575, -0.4061568584469081, 4.920505268985612, 
  -19.8861966544423, 0.01662324867403157, -0.3995173557156041, 
  4.860863392113541, -19.67229729535078, 0.01659138764375247, 
  -0.403539725261698, 4.931595140355871, -20.19744085752341, 
  0.01659284492113042, -0.4034223932878803, 4.929417866008042, 
  -20.17234075304452, 0.01657079306136074, -0.4030087914366516, 
  4.926995261335493, -20.1717483179644, 0.01631123403463198, 
  -0.3979207653643668, 4.894939551539752, -20.26686970743446, 
  0.01628442815239653, -0.4022916588364721, 4.971587319298069, 
  -20.50443438481888, 0.01636790014970201, -0.4039177334391068, 
  4.981679937077699, -20.47349111745215, 0.01554858297714833, 
  -0.3854948338537349, 4.842650454831579, -20.35147085231986, 
  0.01575524131183993, -0.3896690154907934, 4.869129225584942, 
  -20.4097902937188, 0.01558246032173444, -0.3883350300160978, 
  4.882893944162076, -20.41691392518148, 0.01730793298234595, 
  -0.4252599178818025, 5.144900613348126, -20.5496630623534, 
  0.01728371295754675, -0.4247752336983296, 5.141768763445934, 
  -20.54799123632337, 0.01675466402413868, -0.4118340558255106, 
  5.035088300327921, -20.42777863760426, 0.01623980325106868, 
  -0.4009351302095472, 4.958321088168988, -20.48357492222192, 
  0.01674442659558196, -0.4028133172053705, 4.890464715530134, 
  -19.78021801932716, 0.01687735111337052, -0.4063280889074194, 
  4.921360513086508, -19.88085441665748, 0.01555955341676107, 
  -0.3892758425868065, 4.902684281980693, -20.50134173189092, 
  0.0152520021553395, -0.3825554702016423, 4.853720753846948, 
  -20.48063252587385, 0.01672510262904794, -0.4112458491188211, 
  5.03132077218102, -20.42380107096299, 0.01412873035827413, 
  -0.3612422517201538, 4.727628707948176, -20.3712230428031, 
  0.01456111259076064, -0.3705094439937351, 4.793550867394504, 
  -20.4337659417785, 0.01303078575248218, -0.3384934051127034, 
  4.572993382610303, -20.16216617962548, 0.01307156995060286, 
  -0.3393084509433931, 4.578245086437759, -20.17168933112144, 
  0.01001966916034459, -0.2796570331610596, 4.211741645433502, 
  -19.55444178812209, 5.515982434107229e-03, -0.1882290561832352, 
  3.611362840684803, -18.50596653097039, 0.01167311504275413, 
  -0.3125629623244416, 4.419700906296327, -19.93309715825567, 
  8.545982479386741e-03, -0.2499608799977354, 4.018948480084592, 
  -19.21078121022767, 0.01128695021094038, -0.2617830581131771, 
  3.678233532235371, -15.93235836546982, 0.01539827047329514, 
  -0.3999429272104598, 5.117667512798945, -21.60902896088037, 
  0.01539827047329514, -0.3999429272104598, 5.117667512798945, 
  -21.60902896088037, 0.0157592420682677, -0.4064775533001931, 
  5.151820812121546, -21.62029211527487, 0.01459247013390952, 
  -0.3847875730819502, 5.032471566947562, -21.66683561400893, 
  0.0146498767435244, -0.3858909558639092, 5.038995717425409, 
  -21.66524905865665, 0.0146279696885308, -0.3854248406565988, 5.0357564928607, 
  -21.66187221283715, 0.01431123303464709, -0.3786172144235912, 
  4.987173539155925, -21.68136447160526, 0.01256012699730483, 
  -0.3451689880615074, 4.794041245173406, -21.31566564209724, 
  0.01260442994668921, -0.3461066958271966, 4.80048215853795, 
  -21.28838033323995, 0.01289404370416223, -0.3505578600152829, 
  4.814068315235926, -21.46172692546076, 0.01316293216332727, 
  -0.3557773716644241, 4.845385431355327, -21.5226621873287, 
  0.01219101470514561, -0.3373476801276094, 4.740991778678532, 
  -21.2878713039107, 0.01184566982750602, -0.3317438797221426, 
  4.719152083471324, -20.8750902842482, 0.01184478066500536, 
  -0.3317248772333912, 4.719019426680997, -20.87988609857402, 
  0.01275429339310055, -0.3496131004217589, 4.828402962524238, 
  -21.2251369653744, 0.01273276591817184, -0.3485435214145448, 
  4.814502300721918, -21.36852509116998, 0.01558039119796211, 
  -0.4032156173073612, 5.134513808627252, -21.61468204774324, 
  0.0153924930241326, -0.3998215198265233, 5.116833691312079, 
  -21.60043690105063, 0.01145804780429591, -0.3227051083228849, 
  4.648687715119386, -21.11138125737872, 0.01161408988393865, 
  -0.3255730189017468, 4.664303032496103, -21.21531825477336, 
  0.01273941474605935, -0.3492925360377467, 4.826149384886981, 
  -21.22395665016185, 0.01052285674229961, -0.3028721636284493, 
  4.513277744933377, -21.00526020538463, 0.0103735484334881, 
  -0.3001494450079805, 4.498696781814106, -20.90641129770185, 
  9.969374643223004e-03, -0.2903582776239182, 4.418861395294818, 
  -20.88889774425756, 0.01000101893171803, -0.2910410792840855, 
  4.423669164297611, -20.89844327019742, 7.016860699674099e-03, 
  -0.2292195547785312, 4.017427735167947, -20.14388321351468, 
  4.966396814821454e-03, -0.1850649857499679, 3.711750851984551, 
  -19.66957544109386, 8.601348076402341e-03, -0.2623033626836221, 
  4.237859708636606, -20.57196759323801, 6.420644555834813e-03, 
  -0.21646074749644, 3.930871931204604, -20.01371222682342, 0.01659429885083433, 
  -0.3969350077739764, 4.824764368550273, -19.69877250540405, 
  0.01674230521149537, -0.4219070944211832, 5.205166733411623, 
  -21.61910092857582, 0.01681973730557413, -0.4222849865681105, 
  5.19532275976221, -21.52809913932727, 0.01610942967534678, 
  -0.4108849487404063, 5.151807431290605, -21.7478600053773, 
  0.01613739494658133, -0.4113401603103514, 5.153642317669504, 
  -21.73553349646459, 0.01611331174446244, -0.4108365605600366, 
  5.150204375122306, -21.7316311896997, 0.01595438138783511, -0.407309629605683, 
  5.124150817486837, -21.79751274073624, 0.01501045623303859, 
  -0.3917287418337963, 5.060726611401205, -21.74126592430943, 
  0.0150494174971965, -0.3925350734626798, 5.066127708508546, 
  -21.71272671341706, 0.01485336280738093, -0.3867117516328895, 
  5.008322884748583, -21.71615360307032, 0.01498465736108607, 
  -0.3889228021488847, 5.018001165417521, -21.72579060234563, 
  0.01458902686190255, -0.3829955521004599, 5.002696214981887, 
  -21.70113477463009, 0.01472721091215692, -0.3877716114610152, 
  5.054331926568067, -21.47670784013793, 0.01472273860025234, 
  -0.387677273045557, 5.053681715311791, -21.48008631565556, 
  0.01524857223102958, -0.3970813244362928, 5.101305553582665, 
  -21.6715552349007, 0.01510759249685759, -0.3934540511816754, 
  5.069368321858874, -21.76597825319663, 0.01683439898869156, 
  -0.4231536609302614, 5.207079905579347, -21.5892598633637, 
  0.01674117908630439, -0.4218836642042741, 5.205007691142644, 
  -21.61233637540469, 0.0140442852195719, -0.3726026686550747, 
  4.941942374516739, -21.60130503622473, 0.01410712824310414, 
  -0.3734489755923832, 4.943078414433292, -21.66837752813712, 
  0.01522961302839477, -0.396680318040887, 5.098534937182699, 
  -21.66907577991654, 0.01313823694090464, -0.3537994671355714, 
  4.81653638387822, -21.51705413650059, 0.01311528135320901, 
  -0.3538070121003306, 4.82141975124795, -21.466515534795, 0.01240191031860557, 
  -0.3375748832235205, 4.697249263527992, -21.3411033244789, 
  0.01243512690974501, -0.3382789266384888, 4.702125011261338, 
  -21.3507331738665, 0.01003675286032073, -0.2902090364432299, 
  4.401058741618202, -20.85357119910126, 7.97255872497352e-03, 
  -0.2468298997287708, 4.108015521583708, -20.41689040216084, 
  0.01135165419793411, -0.3168942949403173, 4.572118503038111, 
  -21.15990836147505, 9.029756744617277e-03, -0.2692782223505784, 
  4.260989081813688, -20.60561975194551, 0.01556807530462043, 
  -0.3683355160918706, 4.562786474273243, -18.94848949519248, 
  0.01681973730557413, -0.4222849865681105, 5.19532275976221, 
  -21.52809913932727, 0.01610942967534678, -0.4108849487404063, 
  5.151807431290605, -21.7478600053773, 0.01613739494658133, 
  -0.4113401603103514, 5.153642317669504, -21.73553349646459, 
  0.01611331174446244, -0.4108365605600366, 5.150204375122306, 
  -21.7316311896997, 0.01595438138783511, -0.407309629605683, 5.124150817486837, 
  -21.79751274073624, 0.01501045623303859, -0.3917287418337963, 
  5.060726611401205, -21.74126592430943, 0.0150494174971965, 
  -0.3925350734626798, 5.066127708508546, -21.71272671341706, 
  0.01485336280738093, -0.3867117516328895, 5.008322884748583, 
  -21.71615360307032, 0.01498465736108607, -0.3889228021488847, 
  5.018001165417521, -21.72579060234563, 0.01458902686190255, 
  -0.3829955521004599, 5.002696214981887, -21.70113477463009, 
  0.01472721091215692, -0.3877716114610152, 5.054331926568067, 
  -21.47670784013793, 0.01472273860025234, -0.387677273045557, 
  5.053681715311791, -21.48008631565556, 0.01524857223102958, 
  -0.3970813244362928, 5.101305553582665, -21.6715552349007, 
  0.01510759249685759, -0.3934540511816754, 5.069368321858874, 
  -21.76597825319663, 0.01683439898869156, -0.4231536609302614, 
  5.207079905579347, -21.5892598633637, 0.01674117908630439, 
  -0.4218836642042741, 5.205007691142644, -21.61233637540469, 
  0.0140442852195719, -0.3726026686550747, 4.941942374516739, 
  -21.60130503622473, 0.01410712824310414, -0.3734489755923832, 
  4.943078414433292, -21.66837752813712, 0.01522961302839477, 
  -0.396680318040887, 5.098534937182699, -21.66907577991654, 
  0.01313823694090464, -0.3537994671355714, 4.81653638387822, 
  -21.51705413650059, 0.01311528135320901, -0.3538070121003306, 
  4.82141975124795, -21.466515534795, 0.01240191031860557, -0.3375748832235205, 
  4.697249263527992, -21.3411033244789, 0.01243512690974501, 
  -0.3382789266384888, 4.702125011261338, -21.3507331738665, 
  0.01003675286032073, -0.2902090364432299, 4.401058741618202, 
  -20.85357119910126, 7.97255872497352e-03, -0.2468298997287708, 
  4.108015521583708, -20.41689040216084, 0.01135165419793411, 
  -0.3168942949403173, 4.572118503038111, -21.15990836147505, 
  9.029756744617277e-03, -0.2692782223505784, 4.260989081813688, 
  -20.60561975194551, 0.01556807530462043, -0.3683355160918706, 
  4.562786474273243, -18.94848949519248, 0.01631756467654404, 
  -0.414033116419924, 5.161158325871551, -21.70359641549617, 
  0.01634439380239921, -0.4144600398639275, 5.162770076804514, 
  -21.69056842672735, 0.01632122901919951, -0.4139775098518011, 
  5.159489526540377, -21.68715854279131, 0.01615779399213687, 
  -0.4103730644248922, 5.133037646578759, -21.75471192505852, 
  0.01540106062178805, -0.3989102203336593, 5.099530162534026, 
  -21.76871576744849, 0.01544177497404831, -0.3997557481996902, 
  5.105221117074619, -21.74025959665826, 0.01522300802222254, 
  -0.3933752327800575, 5.043016362675686, -21.73634912025057, 
  0.01517026220521717, -0.3917934378200086, 5.026879574889673, 
  -21.68808903256727, 0.01482176437486334, -0.3869204548599108, 
  5.019237299662366, -21.67992355256618, 0.01511227527460539, 
  -0.3948596910896339, 5.092442348564516, -21.49524070153327, 
  0.01510929217415399, -0.3947973003355271, 5.092015956680743, 
  -21.49926276280575, 0.0155152990099893, -0.4016538833502662, 
  5.121829562641465, -21.65310157627913, 0.01551113593798779, 
  -0.4009321073172232, 5.110440955093354, -21.79947384058671, 
  0.01690891997668572, -0.4234758005657188, 5.196921944129573, -21.497706800637, 
  0.01681675334754149, -0.4222230781250551, 5.194903792410755, 
  -21.52060271920353, 0.01460903781647786, -0.3833803093300482, 
  5.004968993160857, -21.68343596264339, 0.01440059776223151, 
  -0.3785970706772644, 4.967559040888842, -21.66471153733935, 
  0.01549689164441549, -0.4012659397347321, 5.119158390250018, 
  -21.65097622863757, 0.01344023163000191, -0.3592332714070029, 4.8438167244892, 
  -21.52381412114711, 0.01345856842270304, -0.3601588860426786, 
  4.855458951322219, -21.48846659952851, 0.01265571931595007, 
  -0.341997288317806, 4.717492101996705, -21.33380508992336, 
  0.01269113247081354, -0.342745340291868, 4.72265589274408, -21.34400213543302, 
  0.01029908369573462, -0.2951250366262684, 4.427181176322393, 
  -20.86714716670655, 8.009010968614325e-03, -0.2469891670842801, 
  4.101024392225131, -20.35704609093742, 0.01158984784943674, 
  -0.3211991484236988, 4.593248396975961, -21.15978720808426, 
  9.730999262788314e-03, -0.2833374182215119, 4.34995715420466, 
  -20.76256811250286, 0.01527264548032303, -0.3609178325200343, 
  4.500525856427368, -18.74125610327233, 0.01598311725515132, 
  -0.410113311975156, 5.165545178142181, -22.01470874349671, 0.0159820145827269, 
  -0.4100901396960503, 5.16538609242864, -22.01882001008513, 
  0.01599022135531306, -0.4100835325038785, 5.163554823872163, 
  -22.13438791576811, 0.01481442717482215, -0.3894913882275603, 
  5.064964159452342, -22.00227379099375, 0.01482600930577567, 
  -0.3897230548148696, 5.066431707867502, -21.96754488421646, 
  0.01514018478250725, -0.3944996165340959, 5.080168581097283, 
  -22.1264632334752, 0.01529510801982547, -0.3972086843619449, 
  5.093231666720938, -22.14371944891217, 0.01450309487981544, 
  -0.3831223739847637, 5.023159506929177, -22.00442185709249, 
  0.01365146169521955, -0.3666335453433235, 4.925205383444804, 
  -21.43451134590798, 0.01367591472477292, -0.3671567931771998, 
  4.928862344754884, -21.44853673239717, 0.01461446443193203, 
  -0.3855987779785506, 5.041437939892385, -21.79267474799168, 
  0.01481424371316865, -0.3892471673855556, 5.060432341838808, 
  -21.99612585570184, 0.01627292678082263, -0.4136427909913852, 
  5.164187682577004, -21.7433305620409, 0.0160754638907658, -0.4101745515240257, 
  5.146956418201033, -21.72963555221489, 0.0139313038604509, 
  -0.3719935821805596, 4.955952447786393, -21.88711348262638, 
  0.01410049908516376, -0.3751590572441671, 4.973689111033274, 
  -21.9891398573639, 0.01461888833228667, -0.3856932829830225, 
  5.042097353974134, -21.79865874723441, 0.01332270268817394, 
  -0.3594341874465966, 4.873196014252148, -21.89795311808853, 
  0.01318669019718115, -0.3570149303011156, 4.861043456587069, 
  -21.81105235619425, 0.01291897803348244, -0.3502421801740679, 
  4.802683069743455, -21.83435551341546, 0.0129460780028684, 
  -0.3508192170742427, 4.806698113002762, -21.84174358810207, 
  0.01044367711760782, -0.3006205007704236, 4.491265994844989, 
  -21.32155226306667, 8.499656693191956e-03, -0.2596712468945904, 
  4.213771971591663, -20.91650416846854, 0.0117418612611305, -0.326807713296333, 
  4.657611869806433, -21.60915765757235, 9.847774375093344e-03, 
  -0.2882451233449156, 4.409833844016154, -21.20789471031181, 
  0.01579373973517431, -0.375989969658885, 4.641900958313141, 
  -19.37312907723967, 0.01601572445798632, -0.4106803458273068, 
  5.168257243780658, -22.01047968243351, 0.01602294082172549, 
  -0.4106531075784483, 5.166284727944681, -22.1266610104423, 
  0.01487450056211075, -0.3906507310574714, 5.071869856917304, 
  -22.00364152738729, 0.01488685742758009, -0.3908988444460959, 
  5.073451139995778, -21.96889969464437, 0.01517398334326853, 
  -0.395079280720266, 5.082843098095297, -22.11894946367369, 
  0.01533908721742266, -0.3980271887185856, 5.097770432757379, 
  -22.14112156841914, 0.01456823654480568, -0.3843921719821169, 
  5.030854213460145, -22.00842143442668, 0.01371602987516439, 
  -0.367901395434188, 4.932972440771557, -21.43549166489872, 0.0137407854066876, 
  -0.3684310140396959, 4.93667306944942, -21.44966372522856, 
  0.01467472663982604, -0.3867651757985381, 5.048415547457741, 
  -21.79294856938521, 0.01486581255069661, -0.3902185318801157, 
  5.065965503671412, -21.99429569081382, 0.01629667987420861, 
  -0.4139997614868224, 5.165267261492297, -21.72902950422945, 
  0.01610327227697848, -0.410626625549297, 5.148771194442137, 
  -21.71721006198533, 0.01399960317223971, -0.3733372099889219, 
  4.964217334103466, -21.89254581719607, 0.01416677592604204, 
  -0.3764571464557723, 4.981617185135243, -21.99429895924063, 
  0.01467960457975287, -0.3868693194409295, 5.049141786333871, 
  -21.7991263972357, 0.01339009931735353, -0.3607634330627014, 
  4.881396395709729, -21.90472237083087, 0.01325891093640167, 
  -0.3584478081376865, 4.869971560034326, -21.81894191820759, 
  0.01298442175981649, -0.3515293548449158, 4.810591111134343, 
  -21.84142552799807, 0.01301163602221997, -0.3521087229163028, 
  4.814621485024672, -21.84882503112339, 0.01051698763768758, 
  -0.3020811481007097, 4.500433229974616, -21.33218794296026, 
  8.564371635675392e-03, -0.2609680002336507, 4.221966228213115, 
  -20.9267206586296, 0.01182269099306362, -0.3284337248173153, 4.66798430438143, 
  -21.62243507134106, 9.923778851540305e-03, -0.2897801120018683, 
  4.419657321807333, -21.22078683579381, 0.01577037582027467, 
  -0.375365638584603, 4.63637492846991, -19.34275325959047, 0.01603063756656273, 
  -0.4108148811619908, 5.167395236086185, -22.13375480043435, 
  0.01487873092306675, -0.3907403970605332, 5.072490608962176, 
  -22.00964240277068, 0.01489022840617458, -0.3909702157621007, 
  5.073944711222681, -21.97465025984429, 0.01519727839397875, 
  -0.3955712795145566, 5.086239027741122, -22.13166509216517, 
  0.01536301743667834, -0.3985322171656662, 5.101252725834335, 
  -22.15405601329995, 0.01458814433491049, -0.3848152644218703, 
  5.033791501411907, -22.02019857979824, 0.01369363567333147, 
  -0.3674223309751103, 4.929625285570383, -21.43197559767184, 
  0.01371971805441188, -0.3679802973278261, 4.933523737243133, 
  -21.44664819986167, 0.01466865159030437, -0.3866355715510447, 
  5.04751240562092, -21.7953939837, 0.01487006885229369, -0.3903085731871983, 
  5.066588024271849, -22.0002588820112, 0.01627372722725371, 
  -0.4135203135606669, 5.161998924667277, -21.72557405541916, 
  0.01607785935770985, -0.4100952132537057, 5.145143334427125, 
  -21.71282628434052, 0.01402089480336386, -0.373790349298819, 
  4.967369082481271, -21.90485649376252, 0.01419085007383706, 
  -0.376969277419386, 4.985176955297908, -22.00750334488797, 
  0.01467473093902715, -0.3867652675478422, 5.048416187012373, 
  -21.80201760547164, 0.01342600622734553, -0.3615300648847477, 
  4.886742189017027, -21.92245647621407, 0.01329130818835551, 
  -0.359139917381001, 4.874801655416318, -21.83548152077007, 
  0.01303309431441463, -0.3525655495248692, 4.8177993897223, -21.86370683908351, 
  0.01305998881166681, -0.35313810544562, 4.821782350893145, -21.87097702390193, 
  0.01056560801596857, -0.3031294705581848, 4.507809403915225, 
  -21.35556387998243, 8.617572272354755e-03, -0.2621211185979486, 
  4.230115911459685, -20.95234748846237, 0.01187091514064451, 
  -0.329467946427266, 4.675225198024763, -21.64515192176648, 
  9.970687146508497e-03, -0.2907956776460614, 4.42682539490418, 
  -21.24392411192571, 0.01577373806712713, -0.3754847007898923, 
  4.637612554285713, -19.34960549236411, 0.01480513747281931, 
  -0.3890821689861694, 5.059571275676121, -22.09876459070489, 
  0.01480022373824704, -0.3889625724314376, 5.058592664220538, 
  -22.06073440465922, 0.01539322722930236, -0.3995373638104168, 
  5.111878985332461, -22.30141679610993, 0.01556591007386917, 
  -0.4026499433540333, 5.127980881819353, -22.32609059284281, 
  0.01477914419562318, -0.3886984449573772, 5.059034474012889, 
  -22.1942119857203, 0.01354381967363177, -0.3640469566205299, 4.90439607869089, 
  -21.50952369838063, 0.01357915991601663, -0.3648026762509718, 
  4.909673744424057, -21.52739743473447, 0.01465456285450253, 
  -0.3861565348715275, 5.042449002377392, -21.91179299059758, 
  0.01495100202674928, -0.3918367685436288, 5.075345959789773, 
  -22.13684020987014, 0.01611270181827697, -0.4099432120284349, 
  5.135549922176895, -21.79145732389871, 0.01590945350709765, 
  -0.4063702781323444, 5.117739773652594, -21.77563483897733, 
  0.01422030867175356, -0.3778683653621226, 4.99409718973852, 
  -22.08300092483206, 0.0144245298294018, -0.3817679663948372, 
  5.016846367071417, -22.19208668483868, 0.01466787615119987, 
  -0.3864401213290241, 5.044422019608938, -21.92089374872754, 
  0.01376240157615373, -0.3685476698944766, 4.9340617927934, -22.14020743648329, 
  0.01359206798020799, -0.3654038356308804, 4.91693537445579, 
  -22.04568244000531, 0.01351244597979762, -0.3626099355150451, 
  4.886082233383708, -22.12432984900289, 0.01353501037040863, 
  -0.3630901339087307, 4.889421346397936, -22.13010648181022, 
  0.01113560983074353, -0.3152346008552353, 4.59127426477106, 
  -21.65248182778907, 9.391911450731562e-03, -0.2787390547961144, 
  4.34605284367189, -21.31826547927421, 0.01240411839661624, 
  -0.3407395029102468, 4.752596394045915, -21.92755926674136, 
  0.01025432553509689, -0.2969984594486486, 4.470026406549816, 
  -21.44840427809969, 0.01578124298332921, -0.3759521828791339, 
  4.643394853161255, -19.5079101685357, 0.0129130078136836, -0.3527124441972656, 
  4.846596507900302, -21.65929657789458, 0.01371121921951916, 
  -0.3680103083853024, 4.935755865379036, -21.9950850491248, 
  0.01399180271101166, -0.3734665958896157, 4.968603958304347, 
  -22.05910204882722, 0.01284082807371977, -0.3513265335157539, 
  4.839037590837046, -21.77715916882256, 0.01153213974837288, 
  -0.3249175943747208, 4.670649271083346, -21.04613200821413, 
  0.0115588480853983, -0.3254967104825044, 4.674743409773395, 
  -21.06139645935236, 0.01280709523775494, -0.3507230399853151, 
  4.835954511096944, -21.51808811108684, 0.0130815040159177, 
  -0.3560094704088376, 4.866626965863816, -21.74585837121248, 
  0.01520989712469294, -0.3953809748116318, 5.080312410130861, 
  -21.75589966229809, 0.01496753069381469, -0.3908223395111904, 
  5.054471498679658, -21.71957503465303, 0.01213853771731605, 
  -0.3373663741580835, 4.751723589380805, -21.61371921836, 0.01239475606913206, 
  -0.342385450424767, 4.782396889777488, -21.74395956639799, 
  0.01281592014276559, -0.3509133088489511, 4.837292876574488, 
  -21.52576961760474, 0.01156202239252628, -0.3253638412196633, 
  4.671943138379604, -21.62879731981003, 0.01129220267017111, 
  -0.3200455207830446, 4.639159780730475, -21.49477877571404, 
  0.01142122852685837, -0.3216634472865325, 4.639079390149321, 
  -21.64945257256942, 0.01144330883749053, -0.322139795100665, 
  4.642433072985832, -21.65535084076689, 8.40554679730182e-03, 
  -0.2596958491345852, 4.234979899286128, -20.91098301034217, 
  6.645815968508575e-03, -0.2222266611070862, 3.97875902115508, 
  -20.54779638557169, 9.990601930683726e-03, -0.2925553853609154, 
  4.452381320506224, -21.32570615297223, 7.791034824294957e-03, 
  -0.2467220070156517, 4.147858653395105, -20.78104645145498, 
  0.0164892110529967, -0.3958232691672659, 4.826476513692718, 
  -19.99339378045367, 0.01369350429996311, -0.3676177634932298, 
  4.932881804946607, -21.95400749216444, 0.01367794930032721, 
  -0.3664381471170276, 4.916564513513829, -21.89809013638882, 
  0.0128330866412655, -0.3511443656346689, 4.83761258399358, -21.73776270535346, 
  0.01158242799898865, -0.3259936560510459, 4.678125412109695, 
  -21.01964855584056, 0.01160782505435543, -0.3265443084780469, 
  4.682018123315377, -21.0344921543022, 0.01283381424260811, 
  -0.3512832102082237, 4.839748161282643, -21.48596495542361, 
  8.332316827630901e-03, -0.2575066145537243, 4.212199226524742, 
  -20.35381619687988, 0.01525018797396072, -0.3962181521306015, 
  5.085951228129542, -21.72768357510659, 0.01500830466324906, 
  -0.3916670072328622, 5.060137570076713, -21.69158301478047, 
  8.162266185775943e-03, -0.253447277885137, 4.179975185187246, 
  -20.34446590073874, 7.718203920074672e-03, -0.2446982514685981, 
  4.126859824421532, -20.33005164721471, 0.01284179283994838, 
  -0.351455213249158, 4.840957928893526, -21.49338949812164, 
  8.723380991920054e-03, -0.2645917252131913, 4.249179559742863, 
  -20.65333066926471, 0.01127154099825064, -0.3195825016579285, 
  4.635751786494398, -21.45241729506661, 0.01135962324224794, 
  -0.3203181412555162, 4.629461616359047, -21.59631532047541, 
  0.01138264172996685, -0.3208147270505905, 4.632957736969566, 
  -21.60252157234087, 7.077789071431219e-03, -0.2304172081286294, 
  4.023065485651308, -20.3806111491696, 5.520066878758906e-03, 
  -0.1972168834919087, 3.796092103987291, -20.08417241851153, 
  9.926562877129412e-03, -0.291143635798872, 4.442220722602772, 
  -21.27158139894618, 6.235417971653136e-03, -0.2124737753063577, 
  3.900502063691334, -20.16945082740605, 0.016910972066102, -0.4069712886139717, 
  4.924414496898271, -20.23500954912948, 0.01481782869268358, 
  -0.3893457045067561, 5.061337723691778, -22.36282197588607, 
  0.01371504868277477, -0.3683321252967543, 4.940325815389857, 
  -22.10672930075794, 0.01178484553344369, -0.328636921779143, 
  4.679308174148188, -21.17276423231505, 0.0118358854009464, 
  -0.3297342430432085, 4.687012351210739, -21.1967826119355, 
  0.01322991466982859, -0.3580649319840436, 4.86965618670177, 
  -21.69809382573076, 0.01388784107777989, -0.3714779847987392, 
  4.956983803257557, -22.04603866991803, 0.01504769863066053, -0.39024191051882, 
  5.026992186498119, -21.73019080167252, 0.01479052376916185, 
  -0.3853904318523127, 4.999245967588068, -21.68759031177982, 
  0.01322386305990654, -0.358761714259965, 4.882998727195615, 
  -22.01105791797579, 0.01352805822184964, -0.3648368612179175, 
  4.921287158304575, -22.15461914625114, 0.01325825453873147, 
  -0.3586712444714187, 4.873893818609107, -21.71283777405603, 
  0.01280818298186296, -0.35043500856869, 4.830362429995105, -22.08771706674409, 
  0.01273453951922564, -0.3492886535144273, 4.827050170956702, 
  -22.02643619918416, 0.01295852097271474, -0.3528710598484611, 
  4.839836544718841, -22.20286606978248, 0.01297388863833359, 
  -0.3532006762557837, 4.842144784979681, -22.20609612391675, 
  0.01034221433116855, -0.3002134861024592, 4.506672972664632, 
  -21.64614698539305, 8.73040538394605e-03, -0.2662799700141617, 
  4.277411740302305, -21.34568249879693, 0.01156900910393027, 
  -0.3251732140178017, 4.666716637948292, -21.92112997877132, 
  9.509356882950846e-03, -0.2828443746006495, 4.390268302748985, 
  -21.4520471673592, 0.01608185458721668, -0.3859197131421375, 
  4.745355588206677, -19.92377579172488, 0.01401859204880436, 
  -0.3742802750063978, 4.97662002801158, -22.17894371784944, 
  0.01205626033406976, -0.3338956987442194, 4.710694508986877, 
  -21.23333790140611, 0.012107752837183, -0.3350018471928192, 4.718453000589603, 
  -21.25749930132479, 0.01351582624015658, -0.3636415236479408, 
  4.903406512229714, -21.7646998519991, 0.01368613281363613, 
  -0.3664342518836665, 4.914180112547993, -21.92991973618313, 
  0.01517765614506637, -0.392403980528673, 5.036174650903287, 
  -21.73840631145334, 0.01492224015060327, -0.3876114715068723, 
  5.009003256893241, -21.69738953652923, 0.01334892926667927, 
  -0.3606115782895817, 4.888108481301932, -22.00488161615234, 
  0.01341268560366949, -0.3616014492874792, 4.890897915270699, -22.066530539386, 
  0.01354480122296711, -0.3642609586722738, 4.907731720211132, 
  -21.77966648081922, 0.01296287572413026, -0.352914921540839, 
  4.839761529894912, -22.09065900565638, 0.01289529823693713, 
  -0.3522615558205008, 4.842745818655217, -22.05125810717084, 
  0.01315540495519722, -0.3566722808073316, 4.86182327794692, 
  -22.24344217874679, 0.01317124322455468, -0.3570109397496218, 
  4.864189188655919, -22.24677662614406, 0.01048595810068052, 
  -0.3027587333604806, 4.518634711610763, -21.66016128461773, 
  8.904550810962072e-03, -0.2695986013781469, 4.295694636158657, 
  -21.37630003965605, 0.01198296959075339, -0.3334964182898756, 
  4.719730059830045, -22.0319389092738, 9.901939328207422e-03, 
  -0.2906243733241524, 4.438561283556709, -21.54932493543851, 
  0.01637731055685545, -0.3919061540207961, 4.784221377640135, 
  -20.00933876896288, 0.01091105929198816, -0.3117021589868738, 
  4.579496047209034, -20.93292619974365, 0.01095258947954664, 
  -0.3126028284758665, 4.585865184022668, -20.95403430231611, 
  0.0124070294519026, -0.3423715925513445, 4.779735611524035, 
  -21.48906758187726, 0.01283807007818213, -0.3510419433257984, 
  4.834289762499563, -21.76921354532639, 0.01461620652130035, 
  -0.3830831434405831, 4.997844481422537, -21.66107199599786, 
  0.01453319704706102, -0.3818139512320738, 4.994528144441701, 
  -21.67442465189245, 0.01207955776033184, -0.3363655476549194, 
  4.747167454880294, -21.7074970881843, 0.01238107613490557, 
  -0.3423642225655055, 4.784761334105927, -21.85196791842206, 
  0.01243029948702089, -0.3428732694205239, 4.783264742196461, 
  -21.50229720484486, 0.01169641846846679, -0.3285274827972837, 
  4.696584461820629, -21.79193330667232, 0.01137026959509606, 
  -0.3219926914540852, 4.655225230355685, -21.63904552364831, 
  0.01175645225394474, -0.3291630416065938, 4.694341764783904, 
  -21.88600618384186, 0.0117726889825582, -0.3295134563078876, 4.69680998000667, 
  -21.88959938825575, 8.707377441878454e-03, -0.2665965442956718, 
  4.286553272852778, -21.14638392689395, 7.349690083970815e-03, 
  -0.2377996450321783, 4.091800072581202, -20.92931129135824, 
  0.01030596825949897, -0.2996971291016758, 4.505443588667098, 
  -21.56091487132657, 8.096753510863173e-03, -0.253795204019746, 
  4.201224488681996, -21.02397325345216, 0.01643827481913559, 
  -0.3958538371484689, 4.836304218105864, -20.11762957067375, 
  0.01048477734169958, -0.3042470952766571, 4.543521985895611, 
  -20.47164786558099, 0.01149123471931724, -0.3244074648937079, 
  4.670441743898579, -20.86809362448194, 0.01176005422014436, 
  -0.3295261361379546, 4.70021351088653, -21.12185179245987, 
  0.01485321640283626, -0.3899528441625967, 5.064144712357756, 
  -21.46658446871364, 0.01472164618008409, -0.3876541963850187, 
  5.053522438978357, -21.46795659610138, 0.01032296648378136, 
  -0.2998730143028747, 4.505021844419787, -20.79321634796148, 
  0.01025417636681035, -0.298074043458799, 4.488705497109031, 
  -20.82833725964171, 0.0114767489668517, -0.3240931956091362, 
  4.668218468427501, -20.86710051525131, 9.101853779970141e-03, 
  -0.2738366836763602, 4.325315271908968, -20.59027975456669, 
  9.238057542432466e-03, -0.2771134616879677, 4.352294821858432, 
  -20.58331133628038, 8.918843367674885e-03, -0.269038841293583, 
  4.284130608394718, -20.60024814286594, 8.949167319116186e-03, 
  -0.2696996922252743, 4.288821143530779, -20.6095238167079, 
  5.899565751266047e-03, -0.2059715986527821, 3.865880056468744, 
  -19.81230120866359, 3.899338261254159e-03, -0.1622356464297488, 
  3.558899855903361, -19.33286721531426, 7.033015548495425e-03, 
  -0.2297008034748608, 4.02200503223035, -20.09236100116831, 
  5.296205504130753e-03, -0.1928721886612774, 3.775708954123712, 
  -19.67314228965767, 0.0167352578074848, -0.4022974182351688, 
  4.883974869814572, -19.82918613508407, 0.01150691920159197, 
  -0.3247477599071072, 4.672849253437133, -20.87936230867227, 
  0.01178655106484367, -0.3301004195157799, 4.704271287126566, 
  -21.13697958605649, 0.01484943108645787, -0.38987336179305, 5.06359915715669, 
  -21.4702697631475, 0.01471529207134773, -0.387519834816084, 5.052594158145451, 
  -21.47063434827727, 0.01036186917987102, -0.3007210708942723, 
  4.51104367469216, -20.8136015788913, 0.01030104843450563, -0.2990916632328953, 
  4.495910287089268, -20.85140006937702, 0.01149358301808147, 
  -0.3244584128639763, 4.67080218070347, -20.8788250751728, 
  9.156499299719789e-03, -0.2750292380130641, 4.333793176589303, 
  -20.61683841824804, 9.28320758911098e-03, -0.2781036243452712, 
  4.359358568309225, -20.60659326378028, 8.982371834048288e-03, 
  -0.2704233179870269, 4.293957236835906, -20.63026607947388, 
  9.012559593933843e-03, -0.2710811984210939, 4.298626688476297, 
  -20.63946936725151, 5.949281901567558e-03, -0.2070812279068192, 
  3.8739049963383, -19.83901262637547, 3.949229560185013e-03, 
  -0.1633626562306162, 3.567123872519661, -19.36061251689363, 
  7.094077853112561e-03, -0.2310428425681988, 4.03159996207211, 
  -20.12227835498591, 5.341178164710743e-03, -0.1938852311049277, 
  3.783083069845677, -19.69862006883372, 0.01672734530589432, 
  -0.4021651169584858, 4.883353629675837, -19.83241828540801, 
  0.01280934751822988, -0.3505424476933, 4.83192491957686, -21.51436409221593, 
  0.01531995328024282, -0.398047746973874, 5.102166890866969, 
  -21.64118251259229, 0.01522166615263354, -0.3965122260571911, 
  5.097373512099812, -21.65522890026581, 0.01186611091146031, 
  -0.3317424568390267, 4.715105817124138, -21.37605057346099, 
  0.01188685732999147, -0.3318255090083249, 4.711704463170424, 
  -21.43514472080844, 0.01264055850917541, -0.34753502076487, 4.817245322953193, 
  -21.31264748054528, 0.01089403135745277, -0.3112514305400017, 
  4.575540389021787, -21.26294301769499, 0.01067970033472444, 
  -0.3071314569638384, 4.551165637401398, -21.14254984506995, 
  0.01055966464327755, -0.3034479704989236, 4.514287554078465, 
  -21.22554184568816, 0.01058795975828025, -0.304058827557806, 
  4.518591545597623, -21.23368259823698, 7.80721787082933e-03, 
  -0.2466204921342596, 4.143356221689548, -20.55765176514059, 
  5.956466641929815e-03, -0.2068942593837328, 3.869733116221408, 
  -20.16168202237343, 9.099971994661294e-03, -0.273523378606105, 
  4.320621406538567, -20.88319334833936, 6.837365786910248e-03, 
  -0.2260816642988267, 4.003313555320942, -20.30795060303431, 
  0.01659615939703046, -0.3979260704489699, 4.839999869239998, 
  -19.83692930572109, 0.01531619329541724, -0.3973241910920178, 
  5.090678071916866, -21.78525489112996, 0.01506506643596584, 
  -0.3925564897196345, 5.06317793794419, -21.74449846768547, 
  8.394769760822743e-03, -0.2581873972374855, 4.210420771204531, 
  -20.45256208533953, 8.025581447205388e-03, -0.2509771253172272, 
  4.167646810339146, -20.45855743442047, 0.01281869180147589, 
  -0.3507433394279437, 4.833335256089236, -21.52216669351612, 
  8.799069373242242e-03, -0.2661078864049683, 4.257905575124576, 
  -20.70901926195877, 0.01153169391486101, -0.324910175227422, 
  4.670579521399336, -21.56975221604749, 0.01165027102296562, 
  -0.3262586429449588, 4.668244906330951, -21.71645006336385, 
  0.01167191319536227, -0.3267252854531928, 4.671528171851457, 
  -21.72220152637088, 7.498002023152292e-03, -0.2392744991250167, 
  4.083441258574346, -20.55188389328137, 5.923108405779112e-03, 
  -0.2058042559626938, 3.855000123171822, -20.24777138629545, 
  0.01024370645205536, -0.2977249588453019, 4.48601679925715, 
  -21.40368568515291, 6.663827059359411e-03, -0.2215137189593988, 
  3.962121697318176, -20.34201387571365, 0.0168274314365138, 
  -0.4048347232945987, 4.906118864990595, -20.2453608319957, 
  0.01683226989341609, -0.4231093713846306, 5.206779398160013, 
  -21.58210171554952, 0.01434097050443556, -0.3782903744258014, 
  4.975480145165544, -21.64800694495557, 0.01439771449178039, 
  -0.3789965098591886, 4.975538583321914, -21.71289506378402, 
  0.01530068840466548, -0.3976413474887236, 5.099365219154445, 
  -21.63869981423101, 0.01315802457345065, -0.3538110278735542, 
  4.81172474380614, -21.47987734380212, 0.01344571344358933, 
  -0.3602897976732755, 4.86114058198225, -21.53002864327192, 
  0.01271212918401676, -0.3435751015223251, 4.733179984052045, 
  -21.39648417001904, 0.01274521979626246, -0.3442758797002746, 
  4.738027676100936, -21.40601051182227, 9.983464531220961e-03, 
  -0.2888449878820689, 4.387914299160339, -20.80190276752982, 
  7.727064175620645e-03, -0.2414125201154272, 4.066620434569979, 
  -20.30146777859778, 0.01128679352072182, -0.3152374525037412, 
  4.556591824013735, -21.10174522622492, 9.353336746149965e-03, 
  -0.2756975544016197, 4.300829024274102, -20.67325278176483, 
  0.01543585897456768, -0.3649481063902126, 4.533828719618016, 
  -18.8510280804513, 0.01398570295678793, -0.3713609724902803, 
  4.933341675029074, -21.57353001825569, 0.01404427996975667, 
  -0.3721176435599579, 4.933864288935166, -21.63926872826413, 
  0.01520134149838065, -0.3960823020664198, 5.094402848126905, 
  -21.65223929697059, 0.01306699941370651, -0.3522852506155637, 
  4.806024423797285, -21.48438707590509, 0.01304830823767669, 
  -0.3523825183852983, 4.811521691334361, -21.43525520938164, 
  0.01231817861653588, -0.335800132008158, 4.684958430567551, 
  -21.30404129045254, 0.01235140166887913, -0.3365043197555684, 
  4.689835210721484, -21.31369973027573, 9.961890528636717e-03, 
  -0.2886032122279371, 4.389814398656311, -20.81796943305125, 
  7.894729585396507e-03, -0.2451527122315875, 4.09622423301302, 
  -20.37947074418929, 0.01127365239907741, -0.315229353321652, 
  4.560515317489623, -21.12392934694349, 8.953196078382342e-03, 
  -0.2676341675027712, 4.2494616228763, -20.56909133417538, 0.01555876789606573, 
  -0.3680932681679033, 4.560714680331717, -18.93754050181717, 
  7.947040792685692e-03, -0.249174845965495, 4.153775056636265, 
  -20.50521303549506, 0.01188840230000811, -0.3322251720249733, 
  4.718513832348246, -21.3890547933544, 8.850749819008212e-03, 
  -0.2678977547849458, 4.277962528880638, -20.87143548731222, 
  0.01075434671708761, -0.3096302240180674, 4.577383866284511, 
  -21.49456984470245, 0.0111926143095823, -0.3179134415412591, 
  4.624353130452604, -21.76055445571411, 0.01120758093543565, 
  -0.3182380043477042, 4.626648305551392, -21.76374931370119, 
  7.038038100984798e-03, -0.2307219726291547, 4.036206332039401, 
  -20.58961344061545, 5.671090866651304e-03, -0.2015814845078087, 
  3.837515581958132, -20.3587026327607, 9.701624849407632e-03, 
  -0.2874680274871246, 4.427709521673705, -21.41580339708614, 
  6.318736395658258e-03, -0.2154223908434917, 3.932208007258693, 
  -20.42258087723137, 0.01659012153961213, -0.4018937060421436, 
  4.904489669727914, -20.35261077129279, 0.01191419955647851, 
  -0.3324155303115428, 4.715859702376879, -21.44982789912049, 
  8.56580264068023e-03, -0.2618390529791907, 4.23578463291788, 
  -20.84465274597877, 0.01112821427497305, -0.3172629854009943, 
  4.627055609428234, -21.66563853671933, 0.01162793473918026, 
  -0.3268824803560909, 4.68350352982227, -21.94736934117836, 0.0116411639255453, 
  -0.3271690997050579, 4.685528310817607, -21.94995023233291, 
  7.325503614081772e-03, -0.2366039657828249, 4.07434588746484, 
  -20.72970947991592, 6.06856532117914e-03, -0.2099912704167403, 
  3.894425167247691, -20.53838357707792, 9.969840313968649e-03, 
  -0.293020597355009, 4.463793483361973, -21.55062535916841, 
  6.58368923545269e-03, -0.2209114513867377, 3.968258599233085, 
  -20.55841977892705, 0.01689500308348228, -0.4089179625067003, 
  4.95800170285616, -20.57358151125282, 0.01093135653377074, 
  -0.3120604813854875, 4.581258712798037, -21.28169589178179, 
  0.01071331477800307, -0.3078606710201294, 4.556324602156696, 
  -21.15999435563529, 0.01061032885165138, -0.3045417484389923, 
  4.521994120896152, -21.24917407752507, 0.01063833961301913, 
  -0.305146465215784, 4.526254848319621, -21.25719385752976, 
  7.847937055862104e-03, -0.2475195879211052, 4.149803572081505, 
  -20.57912657139357, 5.999306254876891e-03, -0.2078490635466058, 
  3.876629409730207, -20.18469663810859, 9.147190742403803e-03, 
  -0.2745529909758708, 4.327934656350236, -20.90629786378334, 
  6.879898429594197e-03, -0.2270226569810402, 4.01007456754116, 
  -20.33039076587325, 0.01659152805552844, -0.3978697462632563, 
  4.839962785378008, -19.84074198394356, 0.01055963722906132, 
  -0.3058628787768842, 4.555421724243629, -21.6448511882232, 0.0112894148730837, 
  -0.3204776114804723, 4.647249548545337, -22.0079528128921, 
  0.01129540720693965, -0.3206081492287249, 4.64817574341016, -22.0077606747627, 
  7.491880904401792e-03, -0.2411552763815807, 4.115479837052998, 
  -20.97301956235637, 6.200718696265493e-03, -0.2138365457531756, 
  3.930615584578552, -20.77171893222735, 9.607775144292651e-03, 
  -0.2860530177916924, 4.422953469166029, -21.60179910230358, 
  6.611873866275699e-03, -0.2225273266506937, 3.988414575014551, 
  -20.75265314121977, 0.0162336369641139, -0.3956969252264845, 
  4.872860436777739, -20.49530964758499, 0.01084981947443144, 
  -0.3114781362853984, 4.588469529677142, -21.82185131974854, 
  0.01085774417212958, -0.311650966057736, 4.589697308594812, 
  -21.82237790732158, 7.718357376209185e-03, -0.246995004264964, 
  4.165883695192275, -21.0509265017405, 6.148775002423566e-03, 
  -0.2135510484398964, 3.936999586246144, -20.75109527024901, 
  9.265037529079329e-03, -0.2789546324013732, 4.376427251194501, 
  -21.44174338711727, 7.076594660311765e-03, -0.2334401587740785, 
  4.074414989708207, -20.91342062215217, 0.01614998806189257, 
  -0.3930551384740824, 4.846722696063112, -20.33057500590423, 
  0.01176825948288279, -0.3301848447775028, 4.70921005087524, 
  -22.24568707573804, 8.896587752945847e-03, -0.2716525817776845, 
  4.330881061856481, -21.58883073936791, 7.562486377637551e-03, 
  -0.2436950220637823, 4.143374149221589, -21.38431041971199, 
  0.01041533317757053, -0.3028801345584655, 4.535674241127257, 
  -21.96333252188692, 8.36105586387474e-03, -0.2605785566296658, 
  4.258254076295645, -21.49786863525549, 0.01570797667641921, 
  -0.3835987255552361, 4.779204449693609, -20.36884302637304, 
  8.889696809399857e-03, -0.271500337303753, 4.329788260942029, 
  -21.58339569631723, 7.550468625967022e-03, -0.2434266477077077, 
  4.141431777685372, -21.37658699041365, 0.01041145413653204, 
  -0.3027952143565236, 4.535069291777852, -21.95922425687176, 
  8.35241733497516e-03, -0.2603862089326549, 4.256865578002083, 
  -21.49159309543425, 0.01571557786292587, -0.383724957832368, 
  4.779785650552974, -20.36834622678141, 3.997266692404934e-03, 
  -0.1689026654328078, 3.638518460273485, -20.41723811346249, 
  7.348180399038622e-03, -0.2398566348759379, 4.123302662924801, 
  -21.22420448435885, 4.606546879995174e-03, -0.1817349776737973, 
  3.72517518334388, -20.46348844777324, 0.01498336147029532, 
  -0.3744437815893104, 4.771905218285898, -20.57611320982896, 
  5.973024017428034e-03, -0.2108683573580779, 3.927142074653938, 
  -20.99908370214952, 3.280081805175671e-03, -0.1537883020443286, 
  3.535601650100264, -20.25514583076888, 0.01336386139190688, 
  -0.3433140535074222, 4.58396339405914, -20.42443115826144, 
  6.734982078984559e-03, -0.2270519133786738, 4.03782491505736, 
  -21.10335123913773, 0.01567424604726056, -0.3863792336051579, 
  4.829893746809508, -20.60781279042643, 0.01461872278337713, 
  -0.3683698420399465, 4.744310943256006, -20.60419159830051}; 

void sse_diffusion(const double *temperature_array, const double 
  *pressure_array, const double *mass_frac_array, const double *mixmw_array, 
  const int num_elmts, const int spec_stride, double *diffusion) 
{
  __m128d *mole_frac = (__m128d*)malloc(9*52*sizeof(__m128d));
  __m128d *clamped = (__m128d*)malloc(9*52*sizeof(__m128d));
  __m128d *logt = (__m128d*)malloc(9*sizeof(__m128d));
  __m128d *sumxod = (__m128d*)malloc(9*52*sizeof(__m128d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 18)
  {
    for (unsigned idx = 0; idx < 9; idx++)
    {
      logt[idx] = _mm_load_pd(temperature_array);
      temperature_array += 2;
      mole_frac[0+idx] = _mm_load_pd(mass_frac_array+0*spec_stride+(idx<<1));
      mole_frac[9+idx] = _mm_load_pd(mass_frac_array+1*spec_stride+(idx<<1));
      mole_frac[18+idx] = _mm_load_pd(mass_frac_array+2*spec_stride+(idx<<1));
      mole_frac[27+idx] = _mm_load_pd(mass_frac_array+3*spec_stride+(idx<<1));
      mole_frac[36+idx] = _mm_load_pd(mass_frac_array+4*spec_stride+(idx<<1));
      mole_frac[45+idx] = _mm_load_pd(mass_frac_array+5*spec_stride+(idx<<1));
      mole_frac[54+idx] = _mm_load_pd(mass_frac_array+6*spec_stride+(idx<<1));
      mole_frac[63+idx] = _mm_load_pd(mass_frac_array+7*spec_stride+(idx<<1));
      mole_frac[72+idx] = _mm_load_pd(mass_frac_array+8*spec_stride+(idx<<1));
      mole_frac[81+idx] = _mm_load_pd(mass_frac_array+9*spec_stride+(idx<<1));
      mole_frac[90+idx] = _mm_load_pd(mass_frac_array+10*spec_stride+(idx<<1));
      mole_frac[99+idx] = _mm_load_pd(mass_frac_array+11*spec_stride+(idx<<1));
      mole_frac[108+idx] = _mm_load_pd(mass_frac_array+12*spec_stride+(idx<<1));
      mole_frac[117+idx] = _mm_load_pd(mass_frac_array+13*spec_stride+(idx<<1));
      mole_frac[126+idx] = _mm_load_pd(mass_frac_array+14*spec_stride+(idx<<1));
      mole_frac[135+idx] = _mm_load_pd(mass_frac_array+15*spec_stride+(idx<<1));
      mole_frac[144+idx] = _mm_load_pd(mass_frac_array+16*spec_stride+(idx<<1));
      mole_frac[153+idx] = _mm_load_pd(mass_frac_array+17*spec_stride+(idx<<1));
      mole_frac[162+idx] = _mm_load_pd(mass_frac_array+18*spec_stride+(idx<<1));
      mole_frac[171+idx] = _mm_load_pd(mass_frac_array+19*spec_stride+(idx<<1));
      mole_frac[180+idx] = _mm_load_pd(mass_frac_array+20*spec_stride+(idx<<1));
      mole_frac[189+idx] = _mm_load_pd(mass_frac_array+21*spec_stride+(idx<<1));
      mole_frac[198+idx] = _mm_load_pd(mass_frac_array+22*spec_stride+(idx<<1));
      mole_frac[207+idx] = _mm_load_pd(mass_frac_array+23*spec_stride+(idx<<1));
      mole_frac[216+idx] = _mm_load_pd(mass_frac_array+24*spec_stride+(idx<<1));
      mole_frac[225+idx] = _mm_load_pd(mass_frac_array+25*spec_stride+(idx<<1));
      mole_frac[234+idx] = _mm_load_pd(mass_frac_array+26*spec_stride+(idx<<1));
      mole_frac[243+idx] = _mm_load_pd(mass_frac_array+27*spec_stride+(idx<<1));
      mole_frac[252+idx] = _mm_load_pd(mass_frac_array+28*spec_stride+(idx<<1));
      mole_frac[261+idx] = _mm_load_pd(mass_frac_array+29*spec_stride+(idx<<1));
      mole_frac[270+idx] = _mm_load_pd(mass_frac_array+30*spec_stride+(idx<<1));
      mole_frac[279+idx] = _mm_load_pd(mass_frac_array+31*spec_stride+(idx<<1));
      mole_frac[288+idx] = _mm_load_pd(mass_frac_array+32*spec_stride+(idx<<1));
      mole_frac[297+idx] = _mm_load_pd(mass_frac_array+33*spec_stride+(idx<<1));
      mole_frac[306+idx] = _mm_load_pd(mass_frac_array+34*spec_stride+(idx<<1));
      mole_frac[315+idx] = _mm_load_pd(mass_frac_array+35*spec_stride+(idx<<1));
      mole_frac[324+idx] = _mm_load_pd(mass_frac_array+36*spec_stride+(idx<<1));
      mole_frac[333+idx] = _mm_load_pd(mass_frac_array+37*spec_stride+(idx<<1));
      mole_frac[342+idx] = _mm_load_pd(mass_frac_array+38*spec_stride+(idx<<1));
      mole_frac[351+idx] = _mm_load_pd(mass_frac_array+39*spec_stride+(idx<<1));
      mole_frac[360+idx] = _mm_load_pd(mass_frac_array+40*spec_stride+(idx<<1));
      mole_frac[369+idx] = _mm_load_pd(mass_frac_array+41*spec_stride+(idx<<1));
      mole_frac[378+idx] = _mm_load_pd(mass_frac_array+42*spec_stride+(idx<<1));
      mole_frac[387+idx] = _mm_load_pd(mass_frac_array+43*spec_stride+(idx<<1));
      mole_frac[396+idx] = _mm_load_pd(mass_frac_array+44*spec_stride+(idx<<1));
      mole_frac[405+idx] = _mm_load_pd(mass_frac_array+45*spec_stride+(idx<<1));
      mole_frac[414+idx] = _mm_load_pd(mass_frac_array+46*spec_stride+(idx<<1));
      mole_frac[423+idx] = _mm_load_pd(mass_frac_array+47*spec_stride+(idx<<1));
      mole_frac[432+idx] = _mm_load_pd(mass_frac_array+48*spec_stride+(idx<<1));
      mole_frac[441+idx] = _mm_load_pd(mass_frac_array+49*spec_stride+(idx<<1));
      mole_frac[450+idx] = _mm_load_pd(mass_frac_array+50*spec_stride+(idx<<1));
      mole_frac[459+idx] = _mm_load_pd(mass_frac_array+51*spec_stride+(idx<<1));
      __m128d mixmw = _mm_load_pd(mixmw_array);
      mixmw_array += 2;
      sumxod[0+idx] = _mm_set1_pd(0.0);
      sumxod[9+idx] = _mm_set1_pd(0.0);
      sumxod[18+idx] = _mm_set1_pd(0.0);
      sumxod[27+idx] = _mm_set1_pd(0.0);
      sumxod[36+idx] = _mm_set1_pd(0.0);
      sumxod[45+idx] = _mm_set1_pd(0.0);
      sumxod[54+idx] = _mm_set1_pd(0.0);
      sumxod[63+idx] = _mm_set1_pd(0.0);
      sumxod[72+idx] = _mm_set1_pd(0.0);
      sumxod[81+idx] = _mm_set1_pd(0.0);
      sumxod[90+idx] = _mm_set1_pd(0.0);
      sumxod[99+idx] = _mm_set1_pd(0.0);
      sumxod[108+idx] = _mm_set1_pd(0.0);
      sumxod[117+idx] = _mm_set1_pd(0.0);
      sumxod[126+idx] = _mm_set1_pd(0.0);
      sumxod[135+idx] = _mm_set1_pd(0.0);
      sumxod[144+idx] = _mm_set1_pd(0.0);
      sumxod[153+idx] = _mm_set1_pd(0.0);
      sumxod[162+idx] = _mm_set1_pd(0.0);
      sumxod[171+idx] = _mm_set1_pd(0.0);
      sumxod[180+idx] = _mm_set1_pd(0.0);
      sumxod[189+idx] = _mm_set1_pd(0.0);
      sumxod[198+idx] = _mm_set1_pd(0.0);
      sumxod[207+idx] = _mm_set1_pd(0.0);
      sumxod[216+idx] = _mm_set1_pd(0.0);
      sumxod[225+idx] = _mm_set1_pd(0.0);
      sumxod[234+idx] = _mm_set1_pd(0.0);
      sumxod[243+idx] = _mm_set1_pd(0.0);
      sumxod[252+idx] = _mm_set1_pd(0.0);
      sumxod[261+idx] = _mm_set1_pd(0.0);
      sumxod[270+idx] = _mm_set1_pd(0.0);
      sumxod[279+idx] = _mm_set1_pd(0.0);
      sumxod[288+idx] = _mm_set1_pd(0.0);
      sumxod[297+idx] = _mm_set1_pd(0.0);
      sumxod[306+idx] = _mm_set1_pd(0.0);
      sumxod[315+idx] = _mm_set1_pd(0.0);
      sumxod[324+idx] = _mm_set1_pd(0.0);
      sumxod[333+idx] = _mm_set1_pd(0.0);
      sumxod[342+idx] = _mm_set1_pd(0.0);
      sumxod[351+idx] = _mm_set1_pd(0.0);
      sumxod[360+idx] = _mm_set1_pd(0.0);
      sumxod[369+idx] = _mm_set1_pd(0.0);
      sumxod[378+idx] = _mm_set1_pd(0.0);
      sumxod[387+idx] = _mm_set1_pd(0.0);
      sumxod[396+idx] = _mm_set1_pd(0.0);
      sumxod[405+idx] = _mm_set1_pd(0.0);
      sumxod[414+idx] = _mm_set1_pd(0.0);
      sumxod[423+idx] = _mm_set1_pd(0.0);
      sumxod[432+idx] = _mm_set1_pd(0.0);
      sumxod[441+idx] = _mm_set1_pd(0.0);
      sumxod[450+idx] = _mm_set1_pd(0.0);
      sumxod[459+idx] = _mm_set1_pd(0.0);
      logt[idx] = _mm_mul_pd(logt[idx],_mm_set1_pd(120.0));
      double t1 = _mm_cvtsd_f64(logt[idx]);
      double log1 = log(t1);
      double t0 = _mm_cvtsd_f64(_mm_shuffle_pd(logt[idx],logt[idx],1));
      double log0 = log(t0);
      logt[idx] = _mm_set_pd(log0,log1);
      {
        mole_frac[0+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[0+idx],_mm_set1_pd(recip_molecular_masses[0])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[0+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[0+idx],mole_frac[0+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[0+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[9+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[9+idx],_mm_set1_pd(recip_molecular_masses[1])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[9+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[9+idx],mole_frac[9+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[9+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[18+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[18+idx],_mm_set1_pd(recip_molecular_masses[2])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[18+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[18+idx],mole_frac[18+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[18+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[27+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[27+idx],_mm_set1_pd(recip_molecular_masses[3])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[27+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[27+idx],mole_frac[27+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[27+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[36+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[36+idx],_mm_set1_pd(recip_molecular_masses[4])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[36+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[36+idx],mole_frac[36+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[36+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[45+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[45+idx],_mm_set1_pd(recip_molecular_masses[5])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[45+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[45+idx],mole_frac[45+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[45+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[54+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[54+idx],_mm_set1_pd(recip_molecular_masses[6])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[54+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[54+idx],mole_frac[54+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[54+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[63+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[63+idx],_mm_set1_pd(recip_molecular_masses[7])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[63+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[63+idx],mole_frac[63+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[63+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[72+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[72+idx],_mm_set1_pd(recip_molecular_masses[8])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[72+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[72+idx],mole_frac[72+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[72+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[81+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[81+idx],_mm_set1_pd(recip_molecular_masses[9])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[81+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[81+idx],mole_frac[81+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[81+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[90+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[90+idx],_mm_set1_pd(recip_molecular_masses[10])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[90+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[90+idx],mole_frac[90+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[90+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[99+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[99+idx],_mm_set1_pd(recip_molecular_masses[11])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[99+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[99+idx],mole_frac[99+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[99+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[108+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[108+idx],_mm_set1_pd(recip_molecular_masses[12])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[108+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[108+idx],mole_frac[108+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[108+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[117+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[117+idx],_mm_set1_pd(recip_molecular_masses[13])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[117+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[117+idx],mole_frac[117+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[117+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[126+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[126+idx],_mm_set1_pd(recip_molecular_masses[14])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[126+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[126+idx],mole_frac[126+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[126+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[135+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[135+idx],_mm_set1_pd(recip_molecular_masses[15])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[135+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[135+idx],mole_frac[135+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[135+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[144+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[144+idx],_mm_set1_pd(recip_molecular_masses[16])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[144+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[144+idx],mole_frac[144+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[144+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[153+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[153+idx],_mm_set1_pd(recip_molecular_masses[17])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[153+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[153+idx],mole_frac[153+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[153+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[162+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[162+idx],_mm_set1_pd(recip_molecular_masses[18])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[162+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[162+idx],mole_frac[162+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[162+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[171+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[171+idx],_mm_set1_pd(recip_molecular_masses[19])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[171+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[171+idx],mole_frac[171+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[171+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[180+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[180+idx],_mm_set1_pd(recip_molecular_masses[20])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[180+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[180+idx],mole_frac[180+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[180+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[189+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[189+idx],_mm_set1_pd(recip_molecular_masses[21])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[189+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[189+idx],mole_frac[189+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[189+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[198+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[198+idx],_mm_set1_pd(recip_molecular_masses[22])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[198+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[198+idx],mole_frac[198+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[198+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[207+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[207+idx],_mm_set1_pd(recip_molecular_masses[23])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[207+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[207+idx],mole_frac[207+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[207+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[216+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[216+idx],_mm_set1_pd(recip_molecular_masses[24])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[216+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[216+idx],mole_frac[216+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[216+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[225+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[225+idx],_mm_set1_pd(recip_molecular_masses[25])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[225+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[225+idx],mole_frac[225+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[225+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[234+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[234+idx],_mm_set1_pd(recip_molecular_masses[26])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[234+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[234+idx],mole_frac[234+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[234+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[243+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[243+idx],_mm_set1_pd(recip_molecular_masses[27])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[243+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[243+idx],mole_frac[243+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[243+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[252+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[252+idx],_mm_set1_pd(recip_molecular_masses[28])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[252+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[252+idx],mole_frac[252+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[252+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[261+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[261+idx],_mm_set1_pd(recip_molecular_masses[29])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[261+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[261+idx],mole_frac[261+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[261+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[270+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[270+idx],_mm_set1_pd(recip_molecular_masses[30])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[270+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[270+idx],mole_frac[270+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[270+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[279+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[279+idx],_mm_set1_pd(recip_molecular_masses[31])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[279+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[279+idx],mole_frac[279+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[279+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[288+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[288+idx],_mm_set1_pd(recip_molecular_masses[32])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[288+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[288+idx],mole_frac[288+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[288+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[297+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[297+idx],_mm_set1_pd(recip_molecular_masses[33])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[297+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[297+idx],mole_frac[297+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[297+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[306+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[306+idx],_mm_set1_pd(recip_molecular_masses[34])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[306+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[306+idx],mole_frac[306+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[306+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[315+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[315+idx],_mm_set1_pd(recip_molecular_masses[35])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[315+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[315+idx],mole_frac[315+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[315+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[324+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[324+idx],_mm_set1_pd(recip_molecular_masses[36])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[324+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[324+idx],mole_frac[324+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[324+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[333+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[333+idx],_mm_set1_pd(recip_molecular_masses[37])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[333+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[333+idx],mole_frac[333+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[333+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[342+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[342+idx],_mm_set1_pd(recip_molecular_masses[38])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[342+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[342+idx],mole_frac[342+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[342+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[351+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[351+idx],_mm_set1_pd(recip_molecular_masses[39])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[351+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[351+idx],mole_frac[351+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[351+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[360+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[360+idx],_mm_set1_pd(recip_molecular_masses[40])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[360+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[360+idx],mole_frac[360+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[360+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[369+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[369+idx],_mm_set1_pd(recip_molecular_masses[41])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[369+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[369+idx],mole_frac[369+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[369+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[378+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[378+idx],_mm_set1_pd(recip_molecular_masses[42])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[378+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[378+idx],mole_frac[378+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[378+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[387+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[387+idx],_mm_set1_pd(recip_molecular_masses[43])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[387+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[387+idx],mole_frac[387+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[387+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[396+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[396+idx],_mm_set1_pd(recip_molecular_masses[44])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[396+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[396+idx],mole_frac[396+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[396+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[405+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[405+idx],_mm_set1_pd(recip_molecular_masses[45])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[405+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[405+idx],mole_frac[405+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[405+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[414+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[414+idx],_mm_set1_pd(recip_molecular_masses[46])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[414+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[414+idx],mole_frac[414+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[414+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[423+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[423+idx],_mm_set1_pd(recip_molecular_masses[47])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[423+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[423+idx],mole_frac[423+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[423+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[432+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[432+idx],_mm_set1_pd(recip_molecular_masses[48])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[432+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[432+idx],mole_frac[432+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[432+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[441+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[441+idx],_mm_set1_pd(recip_molecular_masses[49])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[441+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[441+idx],mole_frac[441+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[441+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[450+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[450+idx],_mm_set1_pd(recip_molecular_masses[50])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[450+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[450+idx],mole_frac[450+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[450+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[459+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[459+idx],_mm_set1_pd(recip_molecular_masses[51])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[459+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[459+idx],mole_frac[459+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[459+idx] = _mm_set_pd(c0, c1);
      }
    }
    int diff_off = 0;
    for (int k = 0; k < 52; k++)
    {
      for (int j = k+1; j < 52; j++)
      {
        __m128d c0 = _mm_set1_pd(diff_constants[diff_off+0]);
        __m128d c1 = _mm_set1_pd(diff_constants[diff_off+1]);
        __m128d c2 = _mm_set1_pd(diff_constants[diff_off+2]);
        __m128d c3 = _mm_set1_pd(diff_constants[diff_off+3]);
        diff_off += 4;
        __m128d val;
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[0]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[0]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[0]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+0] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+0],val),sumxod[k*9+0]); 
        sumxod[j*9+0] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+0],val),sumxod[j*9+0]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[1]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[1]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[1]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+1] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+1],val),sumxod[k*9+1]); 
        sumxod[j*9+1] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+1],val),sumxod[j*9+1]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[2]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[2]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[2]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+2] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+2],val),sumxod[k*9+2]); 
        sumxod[j*9+2] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+2],val),sumxod[j*9+2]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[3]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[3]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[3]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+3] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+3],val),sumxod[k*9+3]); 
        sumxod[j*9+3] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+3],val),sumxod[j*9+3]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[4]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[4]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[4]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+4] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+4],val),sumxod[k*9+4]); 
        sumxod[j*9+4] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+4],val),sumxod[j*9+4]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[5]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[5]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[5]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+5] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+5],val),sumxod[k*9+5]); 
        sumxod[j*9+5] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+5],val),sumxod[j*9+5]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[6]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[6]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[6]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+6] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+6],val),sumxod[k*9+6]); 
        sumxod[j*9+6] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+6],val),sumxod[j*9+6]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[7]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[7]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[7]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+7] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+7],val),sumxod[k*9+7]); 
        sumxod[j*9+7] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+7],val),sumxod[j*9+7]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[8]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[8]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[8]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+8] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+8],val),sumxod[k*9+8]); 
        sumxod[j*9+8] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+8],val),sumxod[j*9+8]); 
      }
    }
    for (unsigned idx = 0; idx < 9; idx++)
    {
      __m128d sumxw = _mm_set1_pd(0.0);
      __m128d wtm = _mm_set1_pd(0.0);
      __m128d pressure = _mm_load_pd(pressure_array);
      pressure_array += 2;
      sumxw = _mm_add_pd(_mm_mul_pd(clamped[0+idx],_mm_set1_pd(1.00797)),sumxw);
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],_mm_set1_pd(1.00797)),wtm);
      sumxw = _mm_add_pd(_mm_mul_pd(clamped[9+idx],_mm_set1_pd(2.01594)),sumxw);
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[9+idx],_mm_set1_pd(2.01594)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[18+idx],_mm_set1_pd(15.9994)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[18+idx],_mm_set1_pd(15.9994)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[27+idx],_mm_set1_pd(31.9988)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[27+idx],_mm_set1_pd(31.9988)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[36+idx],_mm_set1_pd(17.00737)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[36+idx],_mm_set1_pd(17.00737)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[45+idx],_mm_set1_pd(18.01534)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[45+idx],_mm_set1_pd(18.01534)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[54+idx],_mm_set1_pd(28.01055)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[54+idx],_mm_set1_pd(28.01055)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[63+idx],_mm_set1_pd(44.00995)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[63+idx],_mm_set1_pd(44.00995)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[72+idx],_mm_set1_pd(15.03506)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[72+idx],_mm_set1_pd(15.03506)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[81+idx],_mm_set1_pd(16.04303)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[81+idx],_mm_set1_pd(16.04303)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[90+idx],_mm_set1_pd(33.00677)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[90+idx],_mm_set1_pd(33.00677)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[99+idx],_mm_set1_pd(34.01474)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[99+idx],_mm_set1_pd(34.01474)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[108+idx],_mm_set1_pd(30.02649)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[108+idx],_mm_set1_pd(30.02649)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[117+idx],_mm_set1_pd(30.07012)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[117+idx],_mm_set1_pd(30.07012)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[126+idx],_mm_set1_pd(28.05418)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[126+idx],_mm_set1_pd(28.05418)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[135+idx],_mm_set1_pd(29.06215)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[135+idx],_mm_set1_pd(29.06215)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[144+idx],_mm_set1_pd(25.03027)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[144+idx],_mm_set1_pd(25.03027)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[153+idx],_mm_set1_pd(26.03824)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[153+idx],_mm_set1_pd(26.03824)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[162+idx],_mm_set1_pd(32.04243)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[162+idx],_mm_set1_pd(32.04243)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[171+idx],_mm_set1_pd(42.03764)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[171+idx],_mm_set1_pd(42.03764)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[180+idx],_mm_set1_pd(41.02967)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[180+idx],_mm_set1_pd(41.02967)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[189+idx],_mm_set1_pd(44.05358)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[189+idx],_mm_set1_pd(44.05358)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[198+idx],_mm_set1_pd(40.06533)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[198+idx],_mm_set1_pd(40.06533)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[207+idx],_mm_set1_pd(40.06533)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[207+idx],_mm_set1_pd(40.06533)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[216+idx],_mm_set1_pd(42.08127)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[216+idx],_mm_set1_pd(42.08127)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[225+idx],_mm_set1_pd(54.09242)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[225+idx],_mm_set1_pd(54.09242)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[234+idx],_mm_set1_pd(55.10039)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[234+idx],_mm_set1_pd(55.10039)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[243+idx],_mm_set1_pd(56.10836)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[243+idx],_mm_set1_pd(56.10836)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[252+idx],_mm_set1_pd(57.11633)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[252+idx],_mm_set1_pd(57.11633)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[261+idx],_mm_set1_pd(57.0727)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[261+idx],_mm_set1_pd(57.0727)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[270+idx],_mm_set1_pd(58.08067)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[270+idx],_mm_set1_pd(58.08067)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[279+idx],_mm_set1_pd(69.12748000000001)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[279+idx],_mm_set1_pd(69.12748000000001)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[288+idx],_mm_set1_pd(70.13545000000001)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[288+idx],_mm_set1_pd(70.13545000000001)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[297+idx],_mm_set1_pd(71.14342000000001)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[297+idx],_mm_set1_pd(71.14342000000001)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[306+idx],_mm_set1_pd(47.03386)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[306+idx],_mm_set1_pd(47.03386)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[315+idx],_mm_set1_pd(48.04183)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[315+idx],_mm_set1_pd(48.04183)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[324+idx],_mm_set1_pd(55.05676)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[324+idx],_mm_set1_pd(55.05676)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[333+idx],_mm_set1_pd(56.06473)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[333+idx],_mm_set1_pd(56.06473)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[342+idx],_mm_set1_pd(41.0733)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[342+idx],_mm_set1_pd(41.0733)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[351+idx],_mm_set1_pd(39.05736)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[351+idx],_mm_set1_pd(39.05736)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[360+idx],_mm_set1_pd(72.10776)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[360+idx],_mm_set1_pd(72.10776)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[369+idx],_mm_set1_pd(71.09979)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[369+idx],_mm_set1_pd(71.09979)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[378+idx],_mm_set1_pd(56.06473)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[378+idx],_mm_set1_pd(56.06473)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[387+idx],_mm_set1_pd(86.13485)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[387+idx],_mm_set1_pd(86.13485)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[396+idx],_mm_set1_pd(85.12688)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[396+idx],_mm_set1_pd(85.12688)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[405+idx],_mm_set1_pd(100.20557)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[405+idx],_mm_set1_pd(100.20557)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[414+idx],_mm_set1_pd(99.19760000000001)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[414+idx],_mm_set1_pd(99.19760000000001)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[423+idx],_mm_set1_pd(131.1964)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[423+idx],_mm_set1_pd(131.1964)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[432+idx],_mm_set1_pd(163.1952)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[432+idx],_mm_set1_pd(163.1952)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[441+idx],_mm_set1_pd(114.18903)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[441+idx],_mm_set1_pd(114.18903)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[450+idx],_mm_set1_pd(146.18783)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[450+idx],_mm_set1_pd(146.18783)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[459+idx],_mm_set1_pd(28.0134)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[459+idx],_mm_set1_pd(28.0134)),wtm);
      pressure = _mm_mul_pd(pressure,_mm_set1_pd(1.41836588544e+06));
      __m128d pfac = _mm_div_pd(_mm_set1_pd(1.01325e+06),pressure);
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(1.00797),clamped[0+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[0+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(0*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(2.01594),clamped[9+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[9+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(1*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(15.9994),clamped[18+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[18+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(2*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(31.9988),clamped[27+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[27+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(3*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(17.00737),clamped[36+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[36+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(4*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(18.01534),clamped[45+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[45+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(5*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(28.01055),clamped[54+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[54+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(6*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(44.00995),clamped[63+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[63+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(7*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(15.03506),clamped[72+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[72+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(8*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(16.04303),clamped[81+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[81+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(9*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(33.00677),clamped[90+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[90+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(10*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(34.01474),clamped[99+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[99+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(11*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(30.02649),clamped[108+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[108+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(12*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(30.07012),clamped[117+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[117+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(13*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(28.05418),clamped[126+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[126+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(14*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(29.06215),clamped[135+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[135+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(15*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(25.03027),clamped[144+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[144+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(16*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(26.03824),clamped[153+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[153+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(17*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(32.04243),clamped[162+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[162+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(18*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(42.03764),clamped[171+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[171+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(19*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(41.02967),clamped[180+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[180+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(20*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(44.05358),clamped[189+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[189+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(21*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(40.06533),clamped[198+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[198+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(22*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(40.06533),clamped[207+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[207+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(23*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(42.08127),clamped[216+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[216+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(24*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(54.09242),clamped[225+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[225+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(25*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(55.10039),clamped[234+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[234+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(26*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(56.10836),clamped[243+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[243+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(27*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(57.11633),clamped[252+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[252+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(28*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(57.0727),clamped[261+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[261+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(29*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(58.08067),clamped[270+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[270+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(30*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(69.12748000000001),clamped[279+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[279+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(31*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(70.13545000000001),clamped[288+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[288+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(32*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(71.14342000000001),clamped[297+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[297+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(33*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(47.03386),clamped[306+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[306+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(34*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(48.04183),clamped[315+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[315+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(35*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(55.05676),clamped[324+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[324+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(36*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(56.06473),clamped[333+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[333+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(37*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(41.0733),clamped[342+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[342+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(38*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(39.05736),clamped[351+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[351+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(39*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(72.10776),clamped[360+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[360+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(40*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(71.09979),clamped[369+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[369+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(41*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(56.06473),clamped[378+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[378+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(42*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(86.13485),clamped[387+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[387+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(43*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(85.12688),clamped[396+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[396+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(44*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(100.20557),clamped[405+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[405+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(45*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(99.19760000000001),clamped[414+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[414+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(46*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(131.1964),clamped[423+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[423+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(47*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(163.1952),clamped[432+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[432+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(48*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(114.18903),clamped[441+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[441+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(49*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(146.18783),clamped[450+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[450+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(50*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(28.0134),clamped[459+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[459+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(51*spec_stride)+(idx<<1),result);
      }
    }
    remaining_elmts -= 18;
    mass_frac_array += 18;
    diffusion += 18;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 2) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      logt[idx] = _mm_load_pd(temperature_array);
      temperature_array += 2;
      mole_frac[0+idx] = _mm_load_pd(mass_frac_array+0*spec_stride+(idx<<1));
      mole_frac[9+idx] = _mm_load_pd(mass_frac_array+1*spec_stride+(idx<<1));
      mole_frac[18+idx] = _mm_load_pd(mass_frac_array+2*spec_stride+(idx<<1));
      mole_frac[27+idx] = _mm_load_pd(mass_frac_array+3*spec_stride+(idx<<1));
      mole_frac[36+idx] = _mm_load_pd(mass_frac_array+4*spec_stride+(idx<<1));
      mole_frac[45+idx] = _mm_load_pd(mass_frac_array+5*spec_stride+(idx<<1));
      mole_frac[54+idx] = _mm_load_pd(mass_frac_array+6*spec_stride+(idx<<1));
      mole_frac[63+idx] = _mm_load_pd(mass_frac_array+7*spec_stride+(idx<<1));
      mole_frac[72+idx] = _mm_load_pd(mass_frac_array+8*spec_stride+(idx<<1));
      mole_frac[81+idx] = _mm_load_pd(mass_frac_array+9*spec_stride+(idx<<1));
      mole_frac[90+idx] = _mm_load_pd(mass_frac_array+10*spec_stride+(idx<<1));
      mole_frac[99+idx] = _mm_load_pd(mass_frac_array+11*spec_stride+(idx<<1));
      mole_frac[108+idx] = _mm_load_pd(mass_frac_array+12*spec_stride+(idx<<1));
      mole_frac[117+idx] = _mm_load_pd(mass_frac_array+13*spec_stride+(idx<<1));
      mole_frac[126+idx] = _mm_load_pd(mass_frac_array+14*spec_stride+(idx<<1));
      mole_frac[135+idx] = _mm_load_pd(mass_frac_array+15*spec_stride+(idx<<1));
      mole_frac[144+idx] = _mm_load_pd(mass_frac_array+16*spec_stride+(idx<<1));
      mole_frac[153+idx] = _mm_load_pd(mass_frac_array+17*spec_stride+(idx<<1));
      mole_frac[162+idx] = _mm_load_pd(mass_frac_array+18*spec_stride+(idx<<1));
      mole_frac[171+idx] = _mm_load_pd(mass_frac_array+19*spec_stride+(idx<<1));
      mole_frac[180+idx] = _mm_load_pd(mass_frac_array+20*spec_stride+(idx<<1));
      mole_frac[189+idx] = _mm_load_pd(mass_frac_array+21*spec_stride+(idx<<1));
      mole_frac[198+idx] = _mm_load_pd(mass_frac_array+22*spec_stride+(idx<<1));
      mole_frac[207+idx] = _mm_load_pd(mass_frac_array+23*spec_stride+(idx<<1));
      mole_frac[216+idx] = _mm_load_pd(mass_frac_array+24*spec_stride+(idx<<1));
      mole_frac[225+idx] = _mm_load_pd(mass_frac_array+25*spec_stride+(idx<<1));
      mole_frac[234+idx] = _mm_load_pd(mass_frac_array+26*spec_stride+(idx<<1));
      mole_frac[243+idx] = _mm_load_pd(mass_frac_array+27*spec_stride+(idx<<1));
      mole_frac[252+idx] = _mm_load_pd(mass_frac_array+28*spec_stride+(idx<<1));
      mole_frac[261+idx] = _mm_load_pd(mass_frac_array+29*spec_stride+(idx<<1));
      mole_frac[270+idx] = _mm_load_pd(mass_frac_array+30*spec_stride+(idx<<1));
      mole_frac[279+idx] = _mm_load_pd(mass_frac_array+31*spec_stride+(idx<<1));
      mole_frac[288+idx] = _mm_load_pd(mass_frac_array+32*spec_stride+(idx<<1));
      mole_frac[297+idx] = _mm_load_pd(mass_frac_array+33*spec_stride+(idx<<1));
      mole_frac[306+idx] = _mm_load_pd(mass_frac_array+34*spec_stride+(idx<<1));
      mole_frac[315+idx] = _mm_load_pd(mass_frac_array+35*spec_stride+(idx<<1));
      mole_frac[324+idx] = _mm_load_pd(mass_frac_array+36*spec_stride+(idx<<1));
      mole_frac[333+idx] = _mm_load_pd(mass_frac_array+37*spec_stride+(idx<<1));
      mole_frac[342+idx] = _mm_load_pd(mass_frac_array+38*spec_stride+(idx<<1));
      mole_frac[351+idx] = _mm_load_pd(mass_frac_array+39*spec_stride+(idx<<1));
      mole_frac[360+idx] = _mm_load_pd(mass_frac_array+40*spec_stride+(idx<<1));
      mole_frac[369+idx] = _mm_load_pd(mass_frac_array+41*spec_stride+(idx<<1));
      mole_frac[378+idx] = _mm_load_pd(mass_frac_array+42*spec_stride+(idx<<1));
      mole_frac[387+idx] = _mm_load_pd(mass_frac_array+43*spec_stride+(idx<<1));
      mole_frac[396+idx] = _mm_load_pd(mass_frac_array+44*spec_stride+(idx<<1));
      mole_frac[405+idx] = _mm_load_pd(mass_frac_array+45*spec_stride+(idx<<1));
      mole_frac[414+idx] = _mm_load_pd(mass_frac_array+46*spec_stride+(idx<<1));
      mole_frac[423+idx] = _mm_load_pd(mass_frac_array+47*spec_stride+(idx<<1));
      mole_frac[432+idx] = _mm_load_pd(mass_frac_array+48*spec_stride+(idx<<1));
      mole_frac[441+idx] = _mm_load_pd(mass_frac_array+49*spec_stride+(idx<<1));
      mole_frac[450+idx] = _mm_load_pd(mass_frac_array+50*spec_stride+(idx<<1));
      mole_frac[459+idx] = _mm_load_pd(mass_frac_array+51*spec_stride+(idx<<1));
      __m128d mixmw = _mm_load_pd(mixmw_array);
      mixmw_array += 2;
      sumxod[0+idx] = _mm_set1_pd(0.0);
      sumxod[9+idx] = _mm_set1_pd(0.0);
      sumxod[18+idx] = _mm_set1_pd(0.0);
      sumxod[27+idx] = _mm_set1_pd(0.0);
      sumxod[36+idx] = _mm_set1_pd(0.0);
      sumxod[45+idx] = _mm_set1_pd(0.0);
      sumxod[54+idx] = _mm_set1_pd(0.0);
      sumxod[63+idx] = _mm_set1_pd(0.0);
      sumxod[72+idx] = _mm_set1_pd(0.0);
      sumxod[81+idx] = _mm_set1_pd(0.0);
      sumxod[90+idx] = _mm_set1_pd(0.0);
      sumxod[99+idx] = _mm_set1_pd(0.0);
      sumxod[108+idx] = _mm_set1_pd(0.0);
      sumxod[117+idx] = _mm_set1_pd(0.0);
      sumxod[126+idx] = _mm_set1_pd(0.0);
      sumxod[135+idx] = _mm_set1_pd(0.0);
      sumxod[144+idx] = _mm_set1_pd(0.0);
      sumxod[153+idx] = _mm_set1_pd(0.0);
      sumxod[162+idx] = _mm_set1_pd(0.0);
      sumxod[171+idx] = _mm_set1_pd(0.0);
      sumxod[180+idx] = _mm_set1_pd(0.0);
      sumxod[189+idx] = _mm_set1_pd(0.0);
      sumxod[198+idx] = _mm_set1_pd(0.0);
      sumxod[207+idx] = _mm_set1_pd(0.0);
      sumxod[216+idx] = _mm_set1_pd(0.0);
      sumxod[225+idx] = _mm_set1_pd(0.0);
      sumxod[234+idx] = _mm_set1_pd(0.0);
      sumxod[243+idx] = _mm_set1_pd(0.0);
      sumxod[252+idx] = _mm_set1_pd(0.0);
      sumxod[261+idx] = _mm_set1_pd(0.0);
      sumxod[270+idx] = _mm_set1_pd(0.0);
      sumxod[279+idx] = _mm_set1_pd(0.0);
      sumxod[288+idx] = _mm_set1_pd(0.0);
      sumxod[297+idx] = _mm_set1_pd(0.0);
      sumxod[306+idx] = _mm_set1_pd(0.0);
      sumxod[315+idx] = _mm_set1_pd(0.0);
      sumxod[324+idx] = _mm_set1_pd(0.0);
      sumxod[333+idx] = _mm_set1_pd(0.0);
      sumxod[342+idx] = _mm_set1_pd(0.0);
      sumxod[351+idx] = _mm_set1_pd(0.0);
      sumxod[360+idx] = _mm_set1_pd(0.0);
      sumxod[369+idx] = _mm_set1_pd(0.0);
      sumxod[378+idx] = _mm_set1_pd(0.0);
      sumxod[387+idx] = _mm_set1_pd(0.0);
      sumxod[396+idx] = _mm_set1_pd(0.0);
      sumxod[405+idx] = _mm_set1_pd(0.0);
      sumxod[414+idx] = _mm_set1_pd(0.0);
      sumxod[423+idx] = _mm_set1_pd(0.0);
      sumxod[432+idx] = _mm_set1_pd(0.0);
      sumxod[441+idx] = _mm_set1_pd(0.0);
      sumxod[450+idx] = _mm_set1_pd(0.0);
      sumxod[459+idx] = _mm_set1_pd(0.0);
      logt[idx] = _mm_mul_pd(logt[idx],_mm_set1_pd(120.0));
      double t1 = _mm_cvtsd_f64(logt[idx]);
      double log1 = log(t1);
      double t0 = _mm_cvtsd_f64(_mm_shuffle_pd(logt[idx],logt[idx],1));
      double log0 = log(t0);
      logt[idx] = _mm_set_pd(log0,log1);
      {
        mole_frac[0+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[0+idx],_mm_set1_pd(recip_molecular_masses[0])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[0+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[0+idx],mole_frac[0+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[0+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[9+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[9+idx],_mm_set1_pd(recip_molecular_masses[1])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[9+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[9+idx],mole_frac[9+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[9+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[18+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[18+idx],_mm_set1_pd(recip_molecular_masses[2])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[18+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[18+idx],mole_frac[18+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[18+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[27+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[27+idx],_mm_set1_pd(recip_molecular_masses[3])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[27+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[27+idx],mole_frac[27+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[27+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[36+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[36+idx],_mm_set1_pd(recip_molecular_masses[4])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[36+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[36+idx],mole_frac[36+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[36+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[45+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[45+idx],_mm_set1_pd(recip_molecular_masses[5])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[45+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[45+idx],mole_frac[45+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[45+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[54+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[54+idx],_mm_set1_pd(recip_molecular_masses[6])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[54+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[54+idx],mole_frac[54+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[54+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[63+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[63+idx],_mm_set1_pd(recip_molecular_masses[7])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[63+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[63+idx],mole_frac[63+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[63+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[72+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[72+idx],_mm_set1_pd(recip_molecular_masses[8])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[72+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[72+idx],mole_frac[72+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[72+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[81+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[81+idx],_mm_set1_pd(recip_molecular_masses[9])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[81+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[81+idx],mole_frac[81+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[81+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[90+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[90+idx],_mm_set1_pd(recip_molecular_masses[10])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[90+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[90+idx],mole_frac[90+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[90+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[99+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[99+idx],_mm_set1_pd(recip_molecular_masses[11])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[99+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[99+idx],mole_frac[99+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[99+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[108+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[108+idx],_mm_set1_pd(recip_molecular_masses[12])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[108+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[108+idx],mole_frac[108+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[108+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[117+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[117+idx],_mm_set1_pd(recip_molecular_masses[13])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[117+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[117+idx],mole_frac[117+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[117+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[126+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[126+idx],_mm_set1_pd(recip_molecular_masses[14])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[126+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[126+idx],mole_frac[126+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[126+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[135+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[135+idx],_mm_set1_pd(recip_molecular_masses[15])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[135+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[135+idx],mole_frac[135+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[135+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[144+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[144+idx],_mm_set1_pd(recip_molecular_masses[16])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[144+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[144+idx],mole_frac[144+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[144+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[153+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[153+idx],_mm_set1_pd(recip_molecular_masses[17])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[153+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[153+idx],mole_frac[153+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[153+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[162+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[162+idx],_mm_set1_pd(recip_molecular_masses[18])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[162+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[162+idx],mole_frac[162+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[162+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[171+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[171+idx],_mm_set1_pd(recip_molecular_masses[19])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[171+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[171+idx],mole_frac[171+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[171+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[180+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[180+idx],_mm_set1_pd(recip_molecular_masses[20])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[180+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[180+idx],mole_frac[180+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[180+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[189+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[189+idx],_mm_set1_pd(recip_molecular_masses[21])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[189+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[189+idx],mole_frac[189+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[189+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[198+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[198+idx],_mm_set1_pd(recip_molecular_masses[22])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[198+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[198+idx],mole_frac[198+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[198+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[207+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[207+idx],_mm_set1_pd(recip_molecular_masses[23])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[207+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[207+idx],mole_frac[207+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[207+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[216+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[216+idx],_mm_set1_pd(recip_molecular_masses[24])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[216+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[216+idx],mole_frac[216+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[216+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[225+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[225+idx],_mm_set1_pd(recip_molecular_masses[25])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[225+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[225+idx],mole_frac[225+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[225+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[234+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[234+idx],_mm_set1_pd(recip_molecular_masses[26])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[234+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[234+idx],mole_frac[234+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[234+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[243+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[243+idx],_mm_set1_pd(recip_molecular_masses[27])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[243+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[243+idx],mole_frac[243+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[243+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[252+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[252+idx],_mm_set1_pd(recip_molecular_masses[28])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[252+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[252+idx],mole_frac[252+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[252+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[261+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[261+idx],_mm_set1_pd(recip_molecular_masses[29])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[261+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[261+idx],mole_frac[261+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[261+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[270+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[270+idx],_mm_set1_pd(recip_molecular_masses[30])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[270+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[270+idx],mole_frac[270+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[270+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[279+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[279+idx],_mm_set1_pd(recip_molecular_masses[31])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[279+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[279+idx],mole_frac[279+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[279+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[288+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[288+idx],_mm_set1_pd(recip_molecular_masses[32])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[288+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[288+idx],mole_frac[288+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[288+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[297+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[297+idx],_mm_set1_pd(recip_molecular_masses[33])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[297+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[297+idx],mole_frac[297+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[297+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[306+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[306+idx],_mm_set1_pd(recip_molecular_masses[34])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[306+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[306+idx],mole_frac[306+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[306+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[315+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[315+idx],_mm_set1_pd(recip_molecular_masses[35])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[315+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[315+idx],mole_frac[315+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[315+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[324+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[324+idx],_mm_set1_pd(recip_molecular_masses[36])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[324+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[324+idx],mole_frac[324+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[324+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[333+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[333+idx],_mm_set1_pd(recip_molecular_masses[37])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[333+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[333+idx],mole_frac[333+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[333+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[342+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[342+idx],_mm_set1_pd(recip_molecular_masses[38])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[342+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[342+idx],mole_frac[342+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[342+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[351+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[351+idx],_mm_set1_pd(recip_molecular_masses[39])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[351+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[351+idx],mole_frac[351+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[351+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[360+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[360+idx],_mm_set1_pd(recip_molecular_masses[40])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[360+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[360+idx],mole_frac[360+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[360+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[369+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[369+idx],_mm_set1_pd(recip_molecular_masses[41])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[369+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[369+idx],mole_frac[369+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[369+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[378+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[378+idx],_mm_set1_pd(recip_molecular_masses[42])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[378+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[378+idx],mole_frac[378+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[378+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[387+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[387+idx],_mm_set1_pd(recip_molecular_masses[43])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[387+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[387+idx],mole_frac[387+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[387+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[396+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[396+idx],_mm_set1_pd(recip_molecular_masses[44])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[396+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[396+idx],mole_frac[396+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[396+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[405+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[405+idx],_mm_set1_pd(recip_molecular_masses[45])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[405+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[405+idx],mole_frac[405+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[405+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[414+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[414+idx],_mm_set1_pd(recip_molecular_masses[46])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[414+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[414+idx],mole_frac[414+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[414+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[423+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[423+idx],_mm_set1_pd(recip_molecular_masses[47])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[423+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[423+idx],mole_frac[423+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[423+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[432+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[432+idx],_mm_set1_pd(recip_molecular_masses[48])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[432+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[432+idx],mole_frac[432+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[432+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[441+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[441+idx],_mm_set1_pd(recip_molecular_masses[49])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[441+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[441+idx],mole_frac[441+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[441+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[450+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[450+idx],_mm_set1_pd(recip_molecular_masses[50])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[450+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[450+idx],mole_frac[450+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[450+idx] = _mm_set_pd(c0, c1);
      }
      {
        mole_frac[459+idx] = 
          _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[459+idx],_mm_set1_pd(recip_molecular_masses[51])),_mm_set1_pd(1e3)),mixmw); 
        double m1 = _mm_cvtsd_f64(mole_frac[459+idx]);
        double c1 = (m1 > 9.999999999999999e-21) ? m1 : 9.999999999999999e-21;
        double m0 = 
          _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[459+idx],mole_frac[459+idx],1)); 
        double c0 = (m0 > 9.999999999999999e-21) ? m0 : 9.999999999999999e-21;
        clamped[459+idx] = _mm_set_pd(c0, c1);
      }
    }
    int diff_off = 0;
    for (int k = 0; k < 52; k++)
    {
      for (int j = k+1; j < 52; j++)
      {
        __m128d c0 = _mm_set1_pd(diff_constants[diff_off+0]);
        __m128d c1 = _mm_set1_pd(diff_constants[diff_off+1]);
        __m128d c2 = _mm_set1_pd(diff_constants[diff_off+2]);
        __m128d c3 = _mm_set1_pd(diff_constants[diff_off+3]);
        diff_off += 4;
        __m128d val;
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[0]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[0]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[0]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+0] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+0],val),sumxod[k*9+0]); 
        sumxod[j*9+0] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+0],val),sumxod[j*9+0]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[1]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[1]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[1]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+1] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+1],val),sumxod[k*9+1]); 
        sumxod[j*9+1] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+1],val),sumxod[j*9+1]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[2]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[2]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[2]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+2] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+2],val),sumxod[k*9+2]); 
        sumxod[j*9+2] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+2],val),sumxod[j*9+2]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[3]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[3]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[3]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+3] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+3],val),sumxod[k*9+3]); 
        sumxod[j*9+3] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+3],val),sumxod[j*9+3]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[4]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[4]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[4]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+4] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+4],val),sumxod[k*9+4]); 
        sumxod[j*9+4] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+4],val),sumxod[j*9+4]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[5]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[5]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[5]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+5] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+5],val),sumxod[k*9+5]); 
        sumxod[j*9+5] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+5],val),sumxod[j*9+5]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[6]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[6]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[6]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+6] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+6],val),sumxod[k*9+6]); 
        sumxod[j*9+6] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+6],val),sumxod[j*9+6]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[7]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[7]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[7]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+7] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+7],val),sumxod[k*9+7]); 
        sumxod[j*9+7] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+7],val),sumxod[j*9+7]); 
        val = c0;
        val = _mm_add_pd(_mm_mul_pd(val,logt[8]),c1);
        val = _mm_add_pd(_mm_mul_pd(val,logt[8]),c2);
        val = _mm_add_pd(_mm_mul_pd(val,logt[8]),c3);
        val = _mm_xor_pd(val,_mm_set1_pd(-0.0));
        val = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(val,val,1))),exp(_mm_cvtsd_f64(val))); 
        sumxod[k*9+8] = 
          _mm_add_pd(_mm_mul_pd(clamped[j*9+8],val),sumxod[k*9+8]); 
        sumxod[j*9+8] = 
          _mm_add_pd(_mm_mul_pd(clamped[k*9+8],val),sumxod[j*9+8]); 
      }
    }
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d sumxw = _mm_set1_pd(0.0);
      __m128d wtm = _mm_set1_pd(0.0);
      __m128d pressure = _mm_load_pd(pressure_array);
      pressure_array += 2;
      sumxw = _mm_add_pd(_mm_mul_pd(clamped[0+idx],_mm_set1_pd(1.00797)),sumxw);
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],_mm_set1_pd(1.00797)),wtm);
      sumxw = _mm_add_pd(_mm_mul_pd(clamped[9+idx],_mm_set1_pd(2.01594)),sumxw);
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[9+idx],_mm_set1_pd(2.01594)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[18+idx],_mm_set1_pd(15.9994)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[18+idx],_mm_set1_pd(15.9994)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[27+idx],_mm_set1_pd(31.9988)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[27+idx],_mm_set1_pd(31.9988)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[36+idx],_mm_set1_pd(17.00737)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[36+idx],_mm_set1_pd(17.00737)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[45+idx],_mm_set1_pd(18.01534)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[45+idx],_mm_set1_pd(18.01534)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[54+idx],_mm_set1_pd(28.01055)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[54+idx],_mm_set1_pd(28.01055)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[63+idx],_mm_set1_pd(44.00995)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[63+idx],_mm_set1_pd(44.00995)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[72+idx],_mm_set1_pd(15.03506)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[72+idx],_mm_set1_pd(15.03506)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[81+idx],_mm_set1_pd(16.04303)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[81+idx],_mm_set1_pd(16.04303)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[90+idx],_mm_set1_pd(33.00677)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[90+idx],_mm_set1_pd(33.00677)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[99+idx],_mm_set1_pd(34.01474)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[99+idx],_mm_set1_pd(34.01474)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[108+idx],_mm_set1_pd(30.02649)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[108+idx],_mm_set1_pd(30.02649)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[117+idx],_mm_set1_pd(30.07012)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[117+idx],_mm_set1_pd(30.07012)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[126+idx],_mm_set1_pd(28.05418)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[126+idx],_mm_set1_pd(28.05418)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[135+idx],_mm_set1_pd(29.06215)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[135+idx],_mm_set1_pd(29.06215)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[144+idx],_mm_set1_pd(25.03027)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[144+idx],_mm_set1_pd(25.03027)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[153+idx],_mm_set1_pd(26.03824)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[153+idx],_mm_set1_pd(26.03824)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[162+idx],_mm_set1_pd(32.04243)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[162+idx],_mm_set1_pd(32.04243)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[171+idx],_mm_set1_pd(42.03764)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[171+idx],_mm_set1_pd(42.03764)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[180+idx],_mm_set1_pd(41.02967)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[180+idx],_mm_set1_pd(41.02967)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[189+idx],_mm_set1_pd(44.05358)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[189+idx],_mm_set1_pd(44.05358)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[198+idx],_mm_set1_pd(40.06533)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[198+idx],_mm_set1_pd(40.06533)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[207+idx],_mm_set1_pd(40.06533)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[207+idx],_mm_set1_pd(40.06533)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[216+idx],_mm_set1_pd(42.08127)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[216+idx],_mm_set1_pd(42.08127)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[225+idx],_mm_set1_pd(54.09242)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[225+idx],_mm_set1_pd(54.09242)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[234+idx],_mm_set1_pd(55.10039)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[234+idx],_mm_set1_pd(55.10039)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[243+idx],_mm_set1_pd(56.10836)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[243+idx],_mm_set1_pd(56.10836)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[252+idx],_mm_set1_pd(57.11633)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[252+idx],_mm_set1_pd(57.11633)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[261+idx],_mm_set1_pd(57.0727)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[261+idx],_mm_set1_pd(57.0727)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[270+idx],_mm_set1_pd(58.08067)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[270+idx],_mm_set1_pd(58.08067)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[279+idx],_mm_set1_pd(69.12748000000001)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[279+idx],_mm_set1_pd(69.12748000000001)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[288+idx],_mm_set1_pd(70.13545000000001)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[288+idx],_mm_set1_pd(70.13545000000001)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[297+idx],_mm_set1_pd(71.14342000000001)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[297+idx],_mm_set1_pd(71.14342000000001)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[306+idx],_mm_set1_pd(47.03386)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[306+idx],_mm_set1_pd(47.03386)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[315+idx],_mm_set1_pd(48.04183)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[315+idx],_mm_set1_pd(48.04183)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[324+idx],_mm_set1_pd(55.05676)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[324+idx],_mm_set1_pd(55.05676)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[333+idx],_mm_set1_pd(56.06473)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[333+idx],_mm_set1_pd(56.06473)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[342+idx],_mm_set1_pd(41.0733)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[342+idx],_mm_set1_pd(41.0733)),wtm);
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[351+idx],_mm_set1_pd(39.05736)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[351+idx],_mm_set1_pd(39.05736)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[360+idx],_mm_set1_pd(72.10776)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[360+idx],_mm_set1_pd(72.10776)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[369+idx],_mm_set1_pd(71.09979)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[369+idx],_mm_set1_pd(71.09979)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[378+idx],_mm_set1_pd(56.06473)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[378+idx],_mm_set1_pd(56.06473)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[387+idx],_mm_set1_pd(86.13485)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[387+idx],_mm_set1_pd(86.13485)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[396+idx],_mm_set1_pd(85.12688)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[396+idx],_mm_set1_pd(85.12688)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[405+idx],_mm_set1_pd(100.20557)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[405+idx],_mm_set1_pd(100.20557)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[414+idx],_mm_set1_pd(99.19760000000001)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[414+idx],_mm_set1_pd(99.19760000000001)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[423+idx],_mm_set1_pd(131.1964)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[423+idx],_mm_set1_pd(131.1964)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[432+idx],_mm_set1_pd(163.1952)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[432+idx],_mm_set1_pd(163.1952)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[441+idx],_mm_set1_pd(114.18903)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[441+idx],_mm_set1_pd(114.18903)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[450+idx],_mm_set1_pd(146.18783)),sumxw); 
      wtm = 
        _mm_add_pd(_mm_mul_pd(mole_frac[450+idx],_mm_set1_pd(146.18783)),wtm); 
      sumxw = 
        _mm_add_pd(_mm_mul_pd(clamped[459+idx],_mm_set1_pd(28.0134)),sumxw); 
      wtm = _mm_add_pd(_mm_mul_pd(mole_frac[459+idx],_mm_set1_pd(28.0134)),wtm);
      pressure = _mm_mul_pd(pressure,_mm_set1_pd(1.41836588544e+06));
      __m128d pfac = _mm_div_pd(_mm_set1_pd(1.01325e+06),pressure);
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(1.00797),clamped[0+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[0+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(0*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(2.01594),clamped[9+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[9+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(1*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(15.9994),clamped[18+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[18+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(2*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(31.9988),clamped[27+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[27+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(3*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(17.00737),clamped[36+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[36+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(4*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(18.01534),clamped[45+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[45+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(5*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(28.01055),clamped[54+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[54+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(6*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(44.00995),clamped[63+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[63+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(7*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(15.03506),clamped[72+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[72+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(8*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(16.04303),clamped[81+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[81+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(9*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(33.00677),clamped[90+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[90+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(10*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(34.01474),clamped[99+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[99+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(11*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(30.02649),clamped[108+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[108+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(12*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(30.07012),clamped[117+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[117+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(13*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(28.05418),clamped[126+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[126+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(14*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(29.06215),clamped[135+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[135+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(15*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(25.03027),clamped[144+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[144+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(16*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(26.03824),clamped[153+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[153+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(17*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(32.04243),clamped[162+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[162+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(18*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(42.03764),clamped[171+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[171+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(19*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(41.02967),clamped[180+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[180+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(20*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(44.05358),clamped[189+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[189+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(21*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(40.06533),clamped[198+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[198+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(22*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(40.06533),clamped[207+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[207+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(23*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(42.08127),clamped[216+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[216+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(24*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(54.09242),clamped[225+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[225+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(25*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(55.10039),clamped[234+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[234+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(26*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(56.10836),clamped[243+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[243+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(27*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(57.11633),clamped[252+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[252+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(28*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(57.0727),clamped[261+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[261+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(29*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(58.08067),clamped[270+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[270+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(30*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(69.12748000000001),clamped[279+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[279+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(31*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(70.13545000000001),clamped[288+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[288+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(32*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(71.14342000000001),clamped[297+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[297+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(33*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(47.03386),clamped[306+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[306+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(34*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(48.04183),clamped[315+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[315+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(35*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(55.05676),clamped[324+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[324+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(36*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(56.06473),clamped[333+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[333+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(37*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(41.0733),clamped[342+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[342+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(38*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(39.05736),clamped[351+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[351+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(39*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(72.10776),clamped[360+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[360+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(40*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(71.09979),clamped[369+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[369+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(41*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(56.06473),clamped[378+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[378+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(42*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(86.13485),clamped[387+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[387+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(43*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(85.12688),clamped[396+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[396+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(44*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(100.20557),clamped[405+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[405+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(45*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(99.19760000000001),clamped[414+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[414+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(46*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(131.1964),clamped[423+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[423+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(47*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(163.1952),clamped[432+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[432+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(48*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(114.18903),clamped[441+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[441+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(49*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(146.18783),clamped[450+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[450+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(50*spec_stride)+(idx<<1),result);
      }
      {
        __m128d result = 
          _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd(_mm_set1_pd(28.0134),clamped[459+idx]))); 
        result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[459+idx]));
        result = _mm_mul_pd(result,_mm_set1_pd(6.386577550694006e-05));
        _mm_store_pd(diffusion+(51*spec_stride)+(idx<<1),result);
      }
    }
  }
  free(mole_frac);
  free(clamped);
  free(logt);
  free(sumxod);
}

void sse_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *mixmw_array, const int num_elmts, const int spec_stride, double 
  *thermal) 
{
  __m128d *mole_frac = (__m128d*)malloc(12*52*sizeof(__m128d));
  __m128d *temperature = (__m128d*)malloc(12*sizeof(__m128d));
  __m128d *thermal_vec = (__m128d*)malloc(12*52*sizeof(__m128d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 24)
  {
    for (unsigned idx = 0; idx < 12; idx++)
    {
      temperature[idx] = _mm_load_pd(temperature_array);
      temperature_array += 2;
      __m128d mixmw = _mm_load_pd(mixmw_array);
      mixmw_array += 2;
      mole_frac[0+idx] = _mm_load_pd(mass_frac_array+(0*spec_stride)+(idx<<1));
      mole_frac[12+idx] = _mm_load_pd(mass_frac_array+(1*spec_stride)+(idx<<1));
      mole_frac[24+idx] = _mm_load_pd(mass_frac_array+(2*spec_stride)+(idx<<1));
      mole_frac[36+idx] = _mm_load_pd(mass_frac_array+(3*spec_stride)+(idx<<1));
      mole_frac[48+idx] = _mm_load_pd(mass_frac_array+(4*spec_stride)+(idx<<1));
      mole_frac[60+idx] = _mm_load_pd(mass_frac_array+(5*spec_stride)+(idx<<1));
      mole_frac[72+idx] = _mm_load_pd(mass_frac_array+(6*spec_stride)+(idx<<1));
      mole_frac[84+idx] = _mm_load_pd(mass_frac_array+(7*spec_stride)+(idx<<1));
      mole_frac[96+idx] = _mm_load_pd(mass_frac_array+(8*spec_stride)+(idx<<1));
      mole_frac[108+idx] = 
        _mm_load_pd(mass_frac_array+(9*spec_stride)+(idx<<1)); 
      mole_frac[120+idx] = 
        _mm_load_pd(mass_frac_array+(10*spec_stride)+(idx<<1)); 
      mole_frac[132+idx] = 
        _mm_load_pd(mass_frac_array+(11*spec_stride)+(idx<<1)); 
      mole_frac[144+idx] = 
        _mm_load_pd(mass_frac_array+(12*spec_stride)+(idx<<1)); 
      mole_frac[156+idx] = 
        _mm_load_pd(mass_frac_array+(13*spec_stride)+(idx<<1)); 
      mole_frac[168+idx] = 
        _mm_load_pd(mass_frac_array+(14*spec_stride)+(idx<<1)); 
      mole_frac[180+idx] = 
        _mm_load_pd(mass_frac_array+(15*spec_stride)+(idx<<1)); 
      mole_frac[192+idx] = 
        _mm_load_pd(mass_frac_array+(16*spec_stride)+(idx<<1)); 
      mole_frac[204+idx] = 
        _mm_load_pd(mass_frac_array+(17*spec_stride)+(idx<<1)); 
      mole_frac[216+idx] = 
        _mm_load_pd(mass_frac_array+(18*spec_stride)+(idx<<1)); 
      mole_frac[228+idx] = 
        _mm_load_pd(mass_frac_array+(19*spec_stride)+(idx<<1)); 
      mole_frac[240+idx] = 
        _mm_load_pd(mass_frac_array+(20*spec_stride)+(idx<<1)); 
      mole_frac[252+idx] = 
        _mm_load_pd(mass_frac_array+(21*spec_stride)+(idx<<1)); 
      mole_frac[264+idx] = 
        _mm_load_pd(mass_frac_array+(22*spec_stride)+(idx<<1)); 
      mole_frac[276+idx] = 
        _mm_load_pd(mass_frac_array+(23*spec_stride)+(idx<<1)); 
      mole_frac[288+idx] = 
        _mm_load_pd(mass_frac_array+(24*spec_stride)+(idx<<1)); 
      mole_frac[300+idx] = 
        _mm_load_pd(mass_frac_array+(25*spec_stride)+(idx<<1)); 
      mole_frac[312+idx] = 
        _mm_load_pd(mass_frac_array+(26*spec_stride)+(idx<<1)); 
      mole_frac[324+idx] = 
        _mm_load_pd(mass_frac_array+(27*spec_stride)+(idx<<1)); 
      mole_frac[336+idx] = 
        _mm_load_pd(mass_frac_array+(28*spec_stride)+(idx<<1)); 
      mole_frac[348+idx] = 
        _mm_load_pd(mass_frac_array+(29*spec_stride)+(idx<<1)); 
      mole_frac[360+idx] = 
        _mm_load_pd(mass_frac_array+(30*spec_stride)+(idx<<1)); 
      mole_frac[372+idx] = 
        _mm_load_pd(mass_frac_array+(31*spec_stride)+(idx<<1)); 
      mole_frac[384+idx] = 
        _mm_load_pd(mass_frac_array+(32*spec_stride)+(idx<<1)); 
      mole_frac[396+idx] = 
        _mm_load_pd(mass_frac_array+(33*spec_stride)+(idx<<1)); 
      mole_frac[408+idx] = 
        _mm_load_pd(mass_frac_array+(34*spec_stride)+(idx<<1)); 
      mole_frac[420+idx] = 
        _mm_load_pd(mass_frac_array+(35*spec_stride)+(idx<<1)); 
      mole_frac[432+idx] = 
        _mm_load_pd(mass_frac_array+(36*spec_stride)+(idx<<1)); 
      mole_frac[444+idx] = 
        _mm_load_pd(mass_frac_array+(37*spec_stride)+(idx<<1)); 
      mole_frac[456+idx] = 
        _mm_load_pd(mass_frac_array+(38*spec_stride)+(idx<<1)); 
      mole_frac[468+idx] = 
        _mm_load_pd(mass_frac_array+(39*spec_stride)+(idx<<1)); 
      mole_frac[480+idx] = 
        _mm_load_pd(mass_frac_array+(40*spec_stride)+(idx<<1)); 
      mole_frac[492+idx] = 
        _mm_load_pd(mass_frac_array+(41*spec_stride)+(idx<<1)); 
      mole_frac[504+idx] = 
        _mm_load_pd(mass_frac_array+(42*spec_stride)+(idx<<1)); 
      mole_frac[516+idx] = 
        _mm_load_pd(mass_frac_array+(43*spec_stride)+(idx<<1)); 
      mole_frac[528+idx] = 
        _mm_load_pd(mass_frac_array+(44*spec_stride)+(idx<<1)); 
      mole_frac[540+idx] = 
        _mm_load_pd(mass_frac_array+(45*spec_stride)+(idx<<1)); 
      mole_frac[552+idx] = 
        _mm_load_pd(mass_frac_array+(46*spec_stride)+(idx<<1)); 
      mole_frac[564+idx] = 
        _mm_load_pd(mass_frac_array+(47*spec_stride)+(idx<<1)); 
      mole_frac[576+idx] = 
        _mm_load_pd(mass_frac_array+(48*spec_stride)+(idx<<1)); 
      mole_frac[588+idx] = 
        _mm_load_pd(mass_frac_array+(49*spec_stride)+(idx<<1)); 
      mole_frac[600+idx] = 
        _mm_load_pd(mass_frac_array+(50*spec_stride)+(idx<<1)); 
      mole_frac[612+idx] = 
        _mm_load_pd(mass_frac_array+(51*spec_stride)+(idx<<1)); 
      temperature[idx] = _mm_mul_pd(temperature[idx],_mm_set1_pd(120.0));
      mole_frac[0+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[0+idx],_mm_set1_pd(recip_molecular_masses[0])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[0+idx] = _mm_set1_pd(0.0);
      mole_frac[12+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[12+idx],_mm_set1_pd(recip_molecular_masses[1])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[12+idx] = _mm_set1_pd(0.0);
      mole_frac[24+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[24+idx],_mm_set1_pd(recip_molecular_masses[2])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[24+idx] = _mm_set1_pd(0.0);
      mole_frac[36+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[36+idx],_mm_set1_pd(recip_molecular_masses[3])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[36+idx] = _mm_set1_pd(0.0);
      mole_frac[48+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[48+idx],_mm_set1_pd(recip_molecular_masses[4])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[48+idx] = _mm_set1_pd(0.0);
      mole_frac[60+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[60+idx],_mm_set1_pd(recip_molecular_masses[5])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[60+idx] = _mm_set1_pd(0.0);
      mole_frac[72+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[72+idx],_mm_set1_pd(recip_molecular_masses[6])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[72+idx] = _mm_set1_pd(0.0);
      mole_frac[84+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[84+idx],_mm_set1_pd(recip_molecular_masses[7])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[84+idx] = _mm_set1_pd(0.0);
      mole_frac[96+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[96+idx],_mm_set1_pd(recip_molecular_masses[8])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[96+idx] = _mm_set1_pd(0.0);
      mole_frac[108+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[108+idx],_mm_set1_pd(recip_molecular_masses[9])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[108+idx] = _mm_set1_pd(0.0);
      mole_frac[120+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[120+idx],_mm_set1_pd(recip_molecular_masses[10])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[120+idx] = _mm_set1_pd(0.0);
      mole_frac[132+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[132+idx],_mm_set1_pd(recip_molecular_masses[11])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[132+idx] = _mm_set1_pd(0.0);
      mole_frac[144+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[144+idx],_mm_set1_pd(recip_molecular_masses[12])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[144+idx] = _mm_set1_pd(0.0);
      mole_frac[156+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[156+idx],_mm_set1_pd(recip_molecular_masses[13])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[156+idx] = _mm_set1_pd(0.0);
      mole_frac[168+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[168+idx],_mm_set1_pd(recip_molecular_masses[14])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[168+idx] = _mm_set1_pd(0.0);
      mole_frac[180+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[180+idx],_mm_set1_pd(recip_molecular_masses[15])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[180+idx] = _mm_set1_pd(0.0);
      mole_frac[192+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[192+idx],_mm_set1_pd(recip_molecular_masses[16])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[192+idx] = _mm_set1_pd(0.0);
      mole_frac[204+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[204+idx],_mm_set1_pd(recip_molecular_masses[17])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[204+idx] = _mm_set1_pd(0.0);
      mole_frac[216+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[216+idx],_mm_set1_pd(recip_molecular_masses[18])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[216+idx] = _mm_set1_pd(0.0);
      mole_frac[228+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[228+idx],_mm_set1_pd(recip_molecular_masses[19])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[228+idx] = _mm_set1_pd(0.0);
      mole_frac[240+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[240+idx],_mm_set1_pd(recip_molecular_masses[20])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[240+idx] = _mm_set1_pd(0.0);
      mole_frac[252+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[252+idx],_mm_set1_pd(recip_molecular_masses[21])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[252+idx] = _mm_set1_pd(0.0);
      mole_frac[264+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[264+idx],_mm_set1_pd(recip_molecular_masses[22])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[264+idx] = _mm_set1_pd(0.0);
      mole_frac[276+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[276+idx],_mm_set1_pd(recip_molecular_masses[23])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[276+idx] = _mm_set1_pd(0.0);
      mole_frac[288+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[288+idx],_mm_set1_pd(recip_molecular_masses[24])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[288+idx] = _mm_set1_pd(0.0);
      mole_frac[300+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[300+idx],_mm_set1_pd(recip_molecular_masses[25])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[300+idx] = _mm_set1_pd(0.0);
      mole_frac[312+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[312+idx],_mm_set1_pd(recip_molecular_masses[26])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[312+idx] = _mm_set1_pd(0.0);
      mole_frac[324+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[324+idx],_mm_set1_pd(recip_molecular_masses[27])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[324+idx] = _mm_set1_pd(0.0);
      mole_frac[336+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[336+idx],_mm_set1_pd(recip_molecular_masses[28])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[336+idx] = _mm_set1_pd(0.0);
      mole_frac[348+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[348+idx],_mm_set1_pd(recip_molecular_masses[29])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[348+idx] = _mm_set1_pd(0.0);
      mole_frac[360+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[360+idx],_mm_set1_pd(recip_molecular_masses[30])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[360+idx] = _mm_set1_pd(0.0);
      mole_frac[372+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[372+idx],_mm_set1_pd(recip_molecular_masses[31])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[372+idx] = _mm_set1_pd(0.0);
      mole_frac[384+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[384+idx],_mm_set1_pd(recip_molecular_masses[32])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[384+idx] = _mm_set1_pd(0.0);
      mole_frac[396+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[396+idx],_mm_set1_pd(recip_molecular_masses[33])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[396+idx] = _mm_set1_pd(0.0);
      mole_frac[408+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[408+idx],_mm_set1_pd(recip_molecular_masses[34])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[408+idx] = _mm_set1_pd(0.0);
      mole_frac[420+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[420+idx],_mm_set1_pd(recip_molecular_masses[35])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[420+idx] = _mm_set1_pd(0.0);
      mole_frac[432+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[432+idx],_mm_set1_pd(recip_molecular_masses[36])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[432+idx] = _mm_set1_pd(0.0);
      mole_frac[444+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[444+idx],_mm_set1_pd(recip_molecular_masses[37])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[444+idx] = _mm_set1_pd(0.0);
      mole_frac[456+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[456+idx],_mm_set1_pd(recip_molecular_masses[38])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[456+idx] = _mm_set1_pd(0.0);
      mole_frac[468+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[468+idx],_mm_set1_pd(recip_molecular_masses[39])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[468+idx] = _mm_set1_pd(0.0);
      mole_frac[480+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[480+idx],_mm_set1_pd(recip_molecular_masses[40])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[480+idx] = _mm_set1_pd(0.0);
      mole_frac[492+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[492+idx],_mm_set1_pd(recip_molecular_masses[41])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[492+idx] = _mm_set1_pd(0.0);
      mole_frac[504+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[504+idx],_mm_set1_pd(recip_molecular_masses[42])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[504+idx] = _mm_set1_pd(0.0);
      mole_frac[516+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[516+idx],_mm_set1_pd(recip_molecular_masses[43])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[516+idx] = _mm_set1_pd(0.0);
      mole_frac[528+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[528+idx],_mm_set1_pd(recip_molecular_masses[44])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[528+idx] = _mm_set1_pd(0.0);
      mole_frac[540+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[540+idx],_mm_set1_pd(recip_molecular_masses[45])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[540+idx] = _mm_set1_pd(0.0);
      mole_frac[552+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[552+idx],_mm_set1_pd(recip_molecular_masses[46])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[552+idx] = _mm_set1_pd(0.0);
      mole_frac[564+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[564+idx],_mm_set1_pd(recip_molecular_masses[47])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[564+idx] = _mm_set1_pd(0.0);
      mole_frac[576+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[576+idx],_mm_set1_pd(recip_molecular_masses[48])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[576+idx] = _mm_set1_pd(0.0);
      mole_frac[588+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[588+idx],_mm_set1_pd(recip_molecular_masses[49])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[588+idx] = _mm_set1_pd(0.0);
      mole_frac[600+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[600+idx],_mm_set1_pd(recip_molecular_masses[50])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[600+idx] = _mm_set1_pd(0.0);
      mole_frac[612+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[612+idx],_mm_set1_pd(recip_molecular_masses[51])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[612+idx] = _mm_set1_pd(0.0);
    }
    for (unsigned idx = 0; idx < 12; idx++)
    {
      // Light species H
      {
        // Interaction with H2
        {
          __m128d val = _mm_set1_pd(2.636753465162816e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.768015910550093e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.775348599026121e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.1585930684619718)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[12+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O
        {
          __m128d val = _mm_set1_pd(1.757513014843561e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.285543870147497e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.854588272324489e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2976867248976821)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[24+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O2
        {
          __m128d val = _mm_set1_pd(2.277133918512246e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.706164304353198e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.891613712354512e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2536889601459076)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[36+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with OH
        {
          __m128d val = _mm_set1_pd(1.770736265618588e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.295216099504371e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.876065744334992e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2999264728725038)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[48+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O
        {
          __m128d val = _mm_set1_pd(2.969326092389167e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.506085990918214e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.84139750691556e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1112922345203659)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[60+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CO
        {
          __m128d val = _mm_set1_pd(2.138048772731333e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.590656312136814e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.597782376370609e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2712782254042295)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[72+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CO2
        {
          __m128d val = _mm_set1_pd(3.126315401830763e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.480822205530582e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.132164599559319e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.06305126061316429)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[84+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3
        {
          __m128d val = _mm_set1_pd(2.447219115855363e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.87350239923066e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.393980243788655e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.1735855528169444)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[96+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH4
        {
          __m128d val = _mm_set1_pd(2.449287050458037e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.872670834275956e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.384325498849513e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.179062640187455)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[108+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HO2
        {
          __m128d val = _mm_set1_pd(2.281523401978496e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.709453166700634e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.899115323879656e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2541779799119729)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[120+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O2
        {
          __m128d val = _mm_set1_pd(2.285660222663966e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.712552719050051e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.906185135617233e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2546388512421464)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[132+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH2O
        {
          __m128d val = _mm_set1_pd(3.162068743695279e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.638982110193224e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.073407084606059e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.09121151917142747)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[144+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H6
        {
          __m128d val = _mm_set1_pd(3.068609912428751e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.437355985544139e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.03362935494119e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.05841809172467596)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[156+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H4
        {
          __m128d val = _mm_set1_pd(3.032335798336825e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.402507903023256e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(5.924350932493196e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.06680574170940606)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[168+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H5
        {
          __m128d val = _mm_set1_pd(3.061474747914644e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.431688619397056e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.019599895579059e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.05828225735440704)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[180+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H
        {
          __m128d val = _mm_set1_pd(3.062833770415917e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.444134365186354e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.094501095785496e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.04180903023643695)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[192+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H2
        {
          __m128d val = _mm_set1_pd(3.072412934963075e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.451778516653413e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.113561950276935e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.04193979005241541)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[204+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OH
        {
          __m128d val = _mm_set1_pd(3.185130935783487e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.651424075793323e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.077047347231746e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.08530313446041707)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[216+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH2CO
        {
          __m128d val = _mm_set1_pd(3.25385022377053e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.688268803523174e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.087521333192038e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.0666682584411887)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[228+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HCCO
        {
          __m128d val = _mm_set1_pd(2.708995271314182e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.079866531812512e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.897257316956756e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.1793027967505053)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[240+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3CHO
        {
          __m128d val = _mm_set1_pd(3.261002571364098e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.694177936269137e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.1031005432557e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.06681480315745897)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[252+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C3H4-A
        {
          __m128d val = _mm_set1_pd(3.230691790300545e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.613766930898042e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.66106286262361e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.320420756542652e-03)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[264+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C3H4-P
        {
          __m128d val = _mm_set1_pd(3.230691790300545e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.613766930898042e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.66106286262361e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.320420756542652e-03)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[276+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C3H6
        {
          __m128d val = _mm_set1_pd(3.223714578667719e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.598526009100887e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.583319655886146e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(8.841530685384569e-03)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[288+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C4H6
        {
          __m128d val = _mm_set1_pd(3.290512752654892e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.679705609324293e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.901653958129413e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.02458194725485896)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[300+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C4H7
        {
          __m128d val = _mm_set1_pd(3.292045725824064e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.67989830078188e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.897761918718049e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.02335263835172963)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[312+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C4H8-1
        {
          __m128d val = _mm_set1_pd(3.294210907594261e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.681660872577683e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.902298583638711e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.02336799740535642)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[324+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with PC4H9
        {
          __m128d val = _mm_set1_pd(3.295146592814352e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.680829524453684e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.893546331607471e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.02149408403266997)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[336+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3COCH2
        {
          __m128d val = _mm_set1_pd(3.295397853943752e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.722362960411805e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.176412891912808e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.06727921335864356)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[348+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H5CHO
        {
          __m128d val = _mm_set1_pd(3.297515624149514e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.723973288205335e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.180059381934386e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.06717800882994507)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[360+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C5H9
        {
          __m128d val = _mm_set1_pd(3.323050289531883e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.726604349389064e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.108437800855215e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.04798447273844533)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[372+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C5H10-1
        {
          __m128d val = _mm_set1_pd(3.324380578881488e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.722399878445388e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.075106530904983e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.04213655105859476)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[384+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C5H11-1
        {
          __m128d val = _mm_set1_pd(3.316720602534791e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.742411801307379e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.239744038703685e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.07023061728558644)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[396+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3O2
        {
          __m128d val = _mm_set1_pd(3.249694012524726e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.705168835280551e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.220500147192796e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.08703224165490056)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[408+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3O2H
        {
          __m128d val = _mm_set1_pd(3.252619028502869e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.707603729777164e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.226999244713772e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.0871105784756821)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[420+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H3CO
        {
          __m128d val = _mm_set1_pd(3.288473792618383e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.720186605455213e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.1859445557792e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.07079495433060273)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[432+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H3CHO
        {
          __m128d val = _mm_set1_pd(3.295317172770128e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.719176331453455e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.154619062778548e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.06402505305270359)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[444+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C3H5-A
        {
          __m128d val = _mm_set1_pd(3.227591603495213e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.606335427331569e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.622092313952259e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.866816986383802e-03)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[456+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C3H3
        {
          __m128d val = _mm_set1_pd(3.22649662451741e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.610372863526306e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.652413240556753e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.316109074572554e-03)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[468+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with NC3H7CHO
        {
          __m128d val = _mm_set1_pd(3.307964045390942e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.745854911053823e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.295120438281277e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.08087631222093869)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[480+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H5COCH2
        {
          __m128d val = _mm_set1_pd(3.311391013659248e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.744088971340765e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.270466687522299e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.07632773335550136)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[492+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3CHCO
        {
          __m128d val = _mm_set1_pd(3.29064002577697e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.721978487888793e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.190678189785176e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.0708415894529123)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[504+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with NC3H7COCH3
        {
          __m128d val = _mm_set1_pd(3.316305214484237e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.758495683296961e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.353587689012826e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.08673865957101029)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[516+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with NC3H7COCH2
        {
          __m128d val = _mm_set1_pd(3.308270456081856e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.757122913444244e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.373121603827887e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.09170212086428919)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[528+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with NC7H16
        {
          __m128d val = _mm_set1_pd(3.336227471919228e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.767227825010256e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.342852071174661e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.07947743603312946)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[540+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C7H15-1
        {
          __m128d val = _mm_set1_pd(3.335545466484962e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.766662136839891e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.341351014949413e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.07946118892656001)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[552+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C7H15O2
        {
          __m128d val = _mm_set1_pd(3.280936558612366e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.764466684600639e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.526250807223522e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1188862252915318)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[564+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C7H14OOHO2
        {
          __m128d val = _mm_set1_pd(3.253507397211482e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.75710681147811e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.576610031646734e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1316343877874153)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[576+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C7H14O
        {
          __m128d val = _mm_set1_pd(3.313264302131829e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.770977977503217e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.452730610745273e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1010865704936297)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[588+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with NC7KET
        {
          __m128d val = _mm_set1_pd(3.267508505142998e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.761313237880533e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.554028172414584e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1255892993729075)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[600+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with N2
        {
          __m128d val = _mm_set1_pd(2.130268425696491e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.584132795786654e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.581104184828728e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2725429566377711)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[612+idx]),thermal_vec[0+idx]); 
        }
      }
      // Light species H2
      {
        // Interaction with H
        {
          __m128d val = _mm_set1_pd(-2.636753465162816e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(1.768015910550093e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.775348599026121e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1585930684619718)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[0+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with O
        {
          __m128d val = _mm_set1_pd(-8.093601450240291e-13);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.103164319166216e-09)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.287522678250429e-06)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4204726021699148)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[24+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with O2
        {
          __m128d val = _mm_set1_pd(2.810981392477091e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.740692804329701e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.085412256055141e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4534799151152161)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[36+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with OH
        {
          __m128d val = _mm_set1_pd(-8.217252067738418e-13);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.19640588058943e-09)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.337748058348944e-06)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4268964046290616)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[48+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with H2O
        {
          __m128d val = _mm_set1_pd(2.499719027252861e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.957520736623052e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.742310930643889e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.09225446319738231)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[60+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CO
        {
          __m128d val = _mm_set1_pd(1.588830133987327e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-9.211783055858657e-09)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.51672947887423e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4536271223894905)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[72+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CO2
        {
          __m128d val = _mm_set1_pd(1.496814407970614e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.072253571601051e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.336568835077498e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3511176078507614)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[84+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3
        {
          __m128d val = _mm_set1_pd(5.954421320119816e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.987829722071353e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(8.518539304410531e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3640489834414005)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[96+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH4
        {
          __m128d val = _mm_set1_pd(5.829054410894387e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.891311305124087e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(8.32178960638599e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3723903181438474)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[108+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with HO2
        {
          __m128d val = _mm_set1_pd(2.821860427380867e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.747429617965584e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.101223581825173e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4552349690041921)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[120+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with H2O2
        {
          __m128d val = _mm_set1_pd(2.83213077320014e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.753789502493254e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.11615025362608e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4568918264856361)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[132+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH2O
        {
          __m128d val = _mm_set1_pd(2.343646881471828e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.781654045987706e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.139309999808266e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.1948729212751452)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[144+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H6
        {
          __m128d val = _mm_set1_pd(1.454408449375579e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.043349476396319e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.276147817795946e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3339639954619114)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[156+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H4
        {
          __m128d val = _mm_set1_pd(1.388135433710349e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-9.92082642450402e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.157956205583607e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3372446703225273)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[168+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H5
        {
          __m128d val = _mm_set1_pd(1.44762907380564e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.038486153473362e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.265538101580283e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3324073025308119)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[180+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H
        {
          __m128d val = _mm_set1_pd(1.50987425728907e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.09042965559124e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.392349771058175e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3129007313029911)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[192+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H2
        {
          __m128d val = _mm_set1_pd(1.519378118153954e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.097293334258289e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.407408349118242e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3148702761180426)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[204+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3OH
        {
          __m128d val = _mm_set1_pd(2.327102184557304e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.764833502784834e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.087327251764788e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2037014810244143)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[216+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH2CO
        {
          __m128d val = _mm_set1_pd(2.279801198506851e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.716221798512884e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.937349326414702e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2319811932466156)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[228+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with HCCO
        {
          __m128d val = _mm_set1_pd(7.667061037097754e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-5.170172757195434e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(1.102581228924998e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4266508259851286)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[240+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3CHO
        {
          __m128d val = _mm_set1_pd(2.289851358095159e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.723787503353852e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.954706536820286e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2330038473337971)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[252+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C3H4-A
        {
          __m128d val = _mm_set1_pd(1.888310737681516e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.388603431919326e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.100481316070349e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2929305605177095)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[264+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C3H4-P
        {
          __m128d val = _mm_set1_pd(1.888310737681516e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.388603431919326e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.100481316070349e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2929305605177095)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[276+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C3H6
        {
          __m128d val = _mm_set1_pd(1.823098471289975e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.334478883305589e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.96541354688965e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3051880222242602)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[288+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C4H6
        {
          __m128d val = _mm_set1_pd(2.068795839855288e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.533286435933309e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.452962832267535e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2807711887572397)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[300+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C4H7
        {
          __m128d val = _mm_set1_pd(2.063977735122053e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.529017779117991e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.44158250624087e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2823555583880165)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[312+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C4H8-1
        {
          __m128d val = _mm_set1_pd(2.066696271536391e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.531031701283899e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.446115533514836e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.282727458653318)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[324+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with PC4H9
        {
          __m128d val = _mm_set1_pd(2.057714036645206e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.523324164542271e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.426112497355691e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2849016924115291)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[336+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3COCH2
        {
          __m128d val = _mm_set1_pd(2.336809597149917e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.75898558745162e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.035022162205326e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2381854910990132)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[348+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H5CHO
        {
          __m128d val = _mm_set1_pd(2.491575702759885e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.892981309676922e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.394499275830982e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2098605478757698)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[360+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C5H9
        {
          __m128d val = _mm_set1_pd(2.245473495982841e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.678251545914919e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.816451433092867e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2621011851697239)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[372+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C5H10-1
        {
          __m128d val = _mm_set1_pd(2.213786368334464e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.651309811589791e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.746396724757235e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2679372303381302)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[384+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C5H11-1
        {
          __m128d val = _mm_set1_pd(2.384797899271548e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.796719054618293e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.126220378373132e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.238815692951617)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[396+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3O2
        {
          __m128d val = _mm_set1_pd(2.42260678774566e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.837262519652026e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.255071740971657e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2120614186512262)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[408+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3O2H
        {
          __m128d val = _mm_set1_pd(2.426975779352755e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.840575886298645e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.262745447170254e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2124438556868042)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[420+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H3CO
        {
          __m128d val = _mm_set1_pd(2.352481466438435e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.773111948712001e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.074147004361504e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2336367604891859)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[432+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H3CHO
        {
          __m128d val = _mm_set1_pd(2.463169482033282e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.868815485794694e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.330541015233772e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2136884083029943)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[444+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C3H5-A
        {
          __m128d val = _mm_set1_pd(1.855233513522455e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.361089053218244e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.031602478166766e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.299205069471473)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[456+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C3H3
        {
          __m128d val = _mm_set1_pd(1.883400286801476e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.384992443108944e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.092418680560616e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.292168810293035)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[468+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with NC3H7CHO
        {
          __m128d val = _mm_set1_pd(2.560446285481596e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.94951688353351e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.538720510087629e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2057271860455629)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[480+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H5COCH2
        {
          __m128d val = _mm_set1_pd(2.563191071693732e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.952187591535617e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.546740419576007e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2045964797566218)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[492+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3CHCO
        {
          __m128d val = _mm_set1_pd(2.355584879386443e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.77545105261501e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.079521652683204e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2339449760300846)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[504+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with NC3H7COCH3
        {
          __m128d val = _mm_set1_pd(2.573775681904159e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.958447266052752e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.555743378994857e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.209647883107428)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[516+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with NC3H7COCH2
        {
          __m128d val = _mm_set1_pd(2.535475864578331e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.924961889670894e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.464556531291609e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2168498660404349)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[528+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with NC7H16
        {
          __m128d val = _mm_set1_pd(2.477325850498314e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.872262620347478e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.316762139434951e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2330125297269697)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[540+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C7H15-1
        {
          __m128d val = _mm_set1_pd(2.476312792988248e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.871496992479841e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.314996877790567e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2329172434758164)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[552+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C7H15O2
        {
          __m128d val = _mm_set1_pd(2.7570900537465e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.116487295622062e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.982492158902414e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.183133307386705)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[564+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C7H14OOHO2
        {
          __m128d val = _mm_set1_pd(2.841286845917054e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.190737570648687e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(5.189071513362518e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.1687672145898553)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[576+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C7H14O
        {
          __m128d val = _mm_set1_pd(2.619695016529612e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.995348344161635e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.647655258162161e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2088141607458208)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[588+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with NC7KET
        {
          __m128d val = _mm_set1_pd(2.808741692316584e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.16215060143183e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(5.109756715260993e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.173974029290639)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[600+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with N2
        {
          __m128d val = _mm_set1_pd(1.514695193142395e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-8.719776099246706e-09)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.424900788336922e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4541343543221107)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[612+idx]),thermal_vec[12+idx]); 
        }
      }
    }
    for (unsigned idx = 0; idx < 12; idx++)
    {
      _mm_store_pd(thermal+(0*spec_stride)+(idx<<1),thermal_vec[0+idx]);
      _mm_store_pd(thermal+(1*spec_stride)+(idx<<1),thermal_vec[12+idx]);
      _mm_store_pd(thermal+(2*spec_stride)+(idx<<1),thermal_vec[24+idx]);
      _mm_store_pd(thermal+(3*spec_stride)+(idx<<1),thermal_vec[36+idx]);
      _mm_store_pd(thermal+(4*spec_stride)+(idx<<1),thermal_vec[48+idx]);
      _mm_store_pd(thermal+(5*spec_stride)+(idx<<1),thermal_vec[60+idx]);
      _mm_store_pd(thermal+(6*spec_stride)+(idx<<1),thermal_vec[72+idx]);
      _mm_store_pd(thermal+(7*spec_stride)+(idx<<1),thermal_vec[84+idx]);
      _mm_store_pd(thermal+(8*spec_stride)+(idx<<1),thermal_vec[96+idx]);
      _mm_store_pd(thermal+(9*spec_stride)+(idx<<1),thermal_vec[108+idx]);
      _mm_store_pd(thermal+(10*spec_stride)+(idx<<1),thermal_vec[120+idx]);
      _mm_store_pd(thermal+(11*spec_stride)+(idx<<1),thermal_vec[132+idx]);
      _mm_store_pd(thermal+(12*spec_stride)+(idx<<1),thermal_vec[144+idx]);
      _mm_store_pd(thermal+(13*spec_stride)+(idx<<1),thermal_vec[156+idx]);
      _mm_store_pd(thermal+(14*spec_stride)+(idx<<1),thermal_vec[168+idx]);
      _mm_store_pd(thermal+(15*spec_stride)+(idx<<1),thermal_vec[180+idx]);
      _mm_store_pd(thermal+(16*spec_stride)+(idx<<1),thermal_vec[192+idx]);
      _mm_store_pd(thermal+(17*spec_stride)+(idx<<1),thermal_vec[204+idx]);
      _mm_store_pd(thermal+(18*spec_stride)+(idx<<1),thermal_vec[216+idx]);
      _mm_store_pd(thermal+(19*spec_stride)+(idx<<1),thermal_vec[228+idx]);
      _mm_store_pd(thermal+(20*spec_stride)+(idx<<1),thermal_vec[240+idx]);
      _mm_store_pd(thermal+(21*spec_stride)+(idx<<1),thermal_vec[252+idx]);
      _mm_store_pd(thermal+(22*spec_stride)+(idx<<1),thermal_vec[264+idx]);
      _mm_store_pd(thermal+(23*spec_stride)+(idx<<1),thermal_vec[276+idx]);
      _mm_store_pd(thermal+(24*spec_stride)+(idx<<1),thermal_vec[288+idx]);
      _mm_store_pd(thermal+(25*spec_stride)+(idx<<1),thermal_vec[300+idx]);
      _mm_store_pd(thermal+(26*spec_stride)+(idx<<1),thermal_vec[312+idx]);
      _mm_store_pd(thermal+(27*spec_stride)+(idx<<1),thermal_vec[324+idx]);
      _mm_store_pd(thermal+(28*spec_stride)+(idx<<1),thermal_vec[336+idx]);
      _mm_store_pd(thermal+(29*spec_stride)+(idx<<1),thermal_vec[348+idx]);
      _mm_store_pd(thermal+(30*spec_stride)+(idx<<1),thermal_vec[360+idx]);
      _mm_store_pd(thermal+(31*spec_stride)+(idx<<1),thermal_vec[372+idx]);
      _mm_store_pd(thermal+(32*spec_stride)+(idx<<1),thermal_vec[384+idx]);
      _mm_store_pd(thermal+(33*spec_stride)+(idx<<1),thermal_vec[396+idx]);
      _mm_store_pd(thermal+(34*spec_stride)+(idx<<1),thermal_vec[408+idx]);
      _mm_store_pd(thermal+(35*spec_stride)+(idx<<1),thermal_vec[420+idx]);
      _mm_store_pd(thermal+(36*spec_stride)+(idx<<1),thermal_vec[432+idx]);
      _mm_store_pd(thermal+(37*spec_stride)+(idx<<1),thermal_vec[444+idx]);
      _mm_store_pd(thermal+(38*spec_stride)+(idx<<1),thermal_vec[456+idx]);
      _mm_store_pd(thermal+(39*spec_stride)+(idx<<1),thermal_vec[468+idx]);
      _mm_store_pd(thermal+(40*spec_stride)+(idx<<1),thermal_vec[480+idx]);
      _mm_store_pd(thermal+(41*spec_stride)+(idx<<1),thermal_vec[492+idx]);
      _mm_store_pd(thermal+(42*spec_stride)+(idx<<1),thermal_vec[504+idx]);
      _mm_store_pd(thermal+(43*spec_stride)+(idx<<1),thermal_vec[516+idx]);
      _mm_store_pd(thermal+(44*spec_stride)+(idx<<1),thermal_vec[528+idx]);
      _mm_store_pd(thermal+(45*spec_stride)+(idx<<1),thermal_vec[540+idx]);
      _mm_store_pd(thermal+(46*spec_stride)+(idx<<1),thermal_vec[552+idx]);
      _mm_store_pd(thermal+(47*spec_stride)+(idx<<1),thermal_vec[564+idx]);
      _mm_store_pd(thermal+(48*spec_stride)+(idx<<1),thermal_vec[576+idx]);
      _mm_store_pd(thermal+(49*spec_stride)+(idx<<1),thermal_vec[588+idx]);
      _mm_store_pd(thermal+(50*spec_stride)+(idx<<1),thermal_vec[600+idx]);
      _mm_store_pd(thermal+(51*spec_stride)+(idx<<1),thermal_vec[612+idx]);
    }
    remaining_elmts -= 24;
    mass_frac_array += 24;
    thermal += 24;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 2) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      temperature[idx] = _mm_load_pd(temperature_array);
      temperature_array += 2;
      __m128d mixmw = _mm_load_pd(mixmw_array);
      mixmw_array += 2;
      mole_frac[0+idx] = _mm_load_pd(mass_frac_array+(0*spec_stride)+(idx<<1));
      mole_frac[12+idx] = _mm_load_pd(mass_frac_array+(1*spec_stride)+(idx<<1));
      mole_frac[24+idx] = _mm_load_pd(mass_frac_array+(2*spec_stride)+(idx<<1));
      mole_frac[36+idx] = _mm_load_pd(mass_frac_array+(3*spec_stride)+(idx<<1));
      mole_frac[48+idx] = _mm_load_pd(mass_frac_array+(4*spec_stride)+(idx<<1));
      mole_frac[60+idx] = _mm_load_pd(mass_frac_array+(5*spec_stride)+(idx<<1));
      mole_frac[72+idx] = _mm_load_pd(mass_frac_array+(6*spec_stride)+(idx<<1));
      mole_frac[84+idx] = _mm_load_pd(mass_frac_array+(7*spec_stride)+(idx<<1));
      mole_frac[96+idx] = _mm_load_pd(mass_frac_array+(8*spec_stride)+(idx<<1));
      mole_frac[108+idx] = 
        _mm_load_pd(mass_frac_array+(9*spec_stride)+(idx<<1)); 
      mole_frac[120+idx] = 
        _mm_load_pd(mass_frac_array+(10*spec_stride)+(idx<<1)); 
      mole_frac[132+idx] = 
        _mm_load_pd(mass_frac_array+(11*spec_stride)+(idx<<1)); 
      mole_frac[144+idx] = 
        _mm_load_pd(mass_frac_array+(12*spec_stride)+(idx<<1)); 
      mole_frac[156+idx] = 
        _mm_load_pd(mass_frac_array+(13*spec_stride)+(idx<<1)); 
      mole_frac[168+idx] = 
        _mm_load_pd(mass_frac_array+(14*spec_stride)+(idx<<1)); 
      mole_frac[180+idx] = 
        _mm_load_pd(mass_frac_array+(15*spec_stride)+(idx<<1)); 
      mole_frac[192+idx] = 
        _mm_load_pd(mass_frac_array+(16*spec_stride)+(idx<<1)); 
      mole_frac[204+idx] = 
        _mm_load_pd(mass_frac_array+(17*spec_stride)+(idx<<1)); 
      mole_frac[216+idx] = 
        _mm_load_pd(mass_frac_array+(18*spec_stride)+(idx<<1)); 
      mole_frac[228+idx] = 
        _mm_load_pd(mass_frac_array+(19*spec_stride)+(idx<<1)); 
      mole_frac[240+idx] = 
        _mm_load_pd(mass_frac_array+(20*spec_stride)+(idx<<1)); 
      mole_frac[252+idx] = 
        _mm_load_pd(mass_frac_array+(21*spec_stride)+(idx<<1)); 
      mole_frac[264+idx] = 
        _mm_load_pd(mass_frac_array+(22*spec_stride)+(idx<<1)); 
      mole_frac[276+idx] = 
        _mm_load_pd(mass_frac_array+(23*spec_stride)+(idx<<1)); 
      mole_frac[288+idx] = 
        _mm_load_pd(mass_frac_array+(24*spec_stride)+(idx<<1)); 
      mole_frac[300+idx] = 
        _mm_load_pd(mass_frac_array+(25*spec_stride)+(idx<<1)); 
      mole_frac[312+idx] = 
        _mm_load_pd(mass_frac_array+(26*spec_stride)+(idx<<1)); 
      mole_frac[324+idx] = 
        _mm_load_pd(mass_frac_array+(27*spec_stride)+(idx<<1)); 
      mole_frac[336+idx] = 
        _mm_load_pd(mass_frac_array+(28*spec_stride)+(idx<<1)); 
      mole_frac[348+idx] = 
        _mm_load_pd(mass_frac_array+(29*spec_stride)+(idx<<1)); 
      mole_frac[360+idx] = 
        _mm_load_pd(mass_frac_array+(30*spec_stride)+(idx<<1)); 
      mole_frac[372+idx] = 
        _mm_load_pd(mass_frac_array+(31*spec_stride)+(idx<<1)); 
      mole_frac[384+idx] = 
        _mm_load_pd(mass_frac_array+(32*spec_stride)+(idx<<1)); 
      mole_frac[396+idx] = 
        _mm_load_pd(mass_frac_array+(33*spec_stride)+(idx<<1)); 
      mole_frac[408+idx] = 
        _mm_load_pd(mass_frac_array+(34*spec_stride)+(idx<<1)); 
      mole_frac[420+idx] = 
        _mm_load_pd(mass_frac_array+(35*spec_stride)+(idx<<1)); 
      mole_frac[432+idx] = 
        _mm_load_pd(mass_frac_array+(36*spec_stride)+(idx<<1)); 
      mole_frac[444+idx] = 
        _mm_load_pd(mass_frac_array+(37*spec_stride)+(idx<<1)); 
      mole_frac[456+idx] = 
        _mm_load_pd(mass_frac_array+(38*spec_stride)+(idx<<1)); 
      mole_frac[468+idx] = 
        _mm_load_pd(mass_frac_array+(39*spec_stride)+(idx<<1)); 
      mole_frac[480+idx] = 
        _mm_load_pd(mass_frac_array+(40*spec_stride)+(idx<<1)); 
      mole_frac[492+idx] = 
        _mm_load_pd(mass_frac_array+(41*spec_stride)+(idx<<1)); 
      mole_frac[504+idx] = 
        _mm_load_pd(mass_frac_array+(42*spec_stride)+(idx<<1)); 
      mole_frac[516+idx] = 
        _mm_load_pd(mass_frac_array+(43*spec_stride)+(idx<<1)); 
      mole_frac[528+idx] = 
        _mm_load_pd(mass_frac_array+(44*spec_stride)+(idx<<1)); 
      mole_frac[540+idx] = 
        _mm_load_pd(mass_frac_array+(45*spec_stride)+(idx<<1)); 
      mole_frac[552+idx] = 
        _mm_load_pd(mass_frac_array+(46*spec_stride)+(idx<<1)); 
      mole_frac[564+idx] = 
        _mm_load_pd(mass_frac_array+(47*spec_stride)+(idx<<1)); 
      mole_frac[576+idx] = 
        _mm_load_pd(mass_frac_array+(48*spec_stride)+(idx<<1)); 
      mole_frac[588+idx] = 
        _mm_load_pd(mass_frac_array+(49*spec_stride)+(idx<<1)); 
      mole_frac[600+idx] = 
        _mm_load_pd(mass_frac_array+(50*spec_stride)+(idx<<1)); 
      mole_frac[612+idx] = 
        _mm_load_pd(mass_frac_array+(51*spec_stride)+(idx<<1)); 
      temperature[idx] = _mm_mul_pd(temperature[idx],_mm_set1_pd(120.0));
      mole_frac[0+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[0+idx],_mm_set1_pd(recip_molecular_masses[0])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[0+idx] = _mm_set1_pd(0.0);
      mole_frac[12+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[12+idx],_mm_set1_pd(recip_molecular_masses[1])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[12+idx] = _mm_set1_pd(0.0);
      mole_frac[24+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[24+idx],_mm_set1_pd(recip_molecular_masses[2])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[24+idx] = _mm_set1_pd(0.0);
      mole_frac[36+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[36+idx],_mm_set1_pd(recip_molecular_masses[3])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[36+idx] = _mm_set1_pd(0.0);
      mole_frac[48+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[48+idx],_mm_set1_pd(recip_molecular_masses[4])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[48+idx] = _mm_set1_pd(0.0);
      mole_frac[60+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[60+idx],_mm_set1_pd(recip_molecular_masses[5])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[60+idx] = _mm_set1_pd(0.0);
      mole_frac[72+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[72+idx],_mm_set1_pd(recip_molecular_masses[6])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[72+idx] = _mm_set1_pd(0.0);
      mole_frac[84+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[84+idx],_mm_set1_pd(recip_molecular_masses[7])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[84+idx] = _mm_set1_pd(0.0);
      mole_frac[96+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[96+idx],_mm_set1_pd(recip_molecular_masses[8])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[96+idx] = _mm_set1_pd(0.0);
      mole_frac[108+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[108+idx],_mm_set1_pd(recip_molecular_masses[9])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[108+idx] = _mm_set1_pd(0.0);
      mole_frac[120+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[120+idx],_mm_set1_pd(recip_molecular_masses[10])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[120+idx] = _mm_set1_pd(0.0);
      mole_frac[132+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[132+idx],_mm_set1_pd(recip_molecular_masses[11])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[132+idx] = _mm_set1_pd(0.0);
      mole_frac[144+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[144+idx],_mm_set1_pd(recip_molecular_masses[12])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[144+idx] = _mm_set1_pd(0.0);
      mole_frac[156+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[156+idx],_mm_set1_pd(recip_molecular_masses[13])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[156+idx] = _mm_set1_pd(0.0);
      mole_frac[168+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[168+idx],_mm_set1_pd(recip_molecular_masses[14])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[168+idx] = _mm_set1_pd(0.0);
      mole_frac[180+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[180+idx],_mm_set1_pd(recip_molecular_masses[15])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[180+idx] = _mm_set1_pd(0.0);
      mole_frac[192+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[192+idx],_mm_set1_pd(recip_molecular_masses[16])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[192+idx] = _mm_set1_pd(0.0);
      mole_frac[204+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[204+idx],_mm_set1_pd(recip_molecular_masses[17])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[204+idx] = _mm_set1_pd(0.0);
      mole_frac[216+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[216+idx],_mm_set1_pd(recip_molecular_masses[18])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[216+idx] = _mm_set1_pd(0.0);
      mole_frac[228+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[228+idx],_mm_set1_pd(recip_molecular_masses[19])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[228+idx] = _mm_set1_pd(0.0);
      mole_frac[240+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[240+idx],_mm_set1_pd(recip_molecular_masses[20])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[240+idx] = _mm_set1_pd(0.0);
      mole_frac[252+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[252+idx],_mm_set1_pd(recip_molecular_masses[21])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[252+idx] = _mm_set1_pd(0.0);
      mole_frac[264+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[264+idx],_mm_set1_pd(recip_molecular_masses[22])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[264+idx] = _mm_set1_pd(0.0);
      mole_frac[276+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[276+idx],_mm_set1_pd(recip_molecular_masses[23])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[276+idx] = _mm_set1_pd(0.0);
      mole_frac[288+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[288+idx],_mm_set1_pd(recip_molecular_masses[24])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[288+idx] = _mm_set1_pd(0.0);
      mole_frac[300+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[300+idx],_mm_set1_pd(recip_molecular_masses[25])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[300+idx] = _mm_set1_pd(0.0);
      mole_frac[312+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[312+idx],_mm_set1_pd(recip_molecular_masses[26])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[312+idx] = _mm_set1_pd(0.0);
      mole_frac[324+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[324+idx],_mm_set1_pd(recip_molecular_masses[27])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[324+idx] = _mm_set1_pd(0.0);
      mole_frac[336+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[336+idx],_mm_set1_pd(recip_molecular_masses[28])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[336+idx] = _mm_set1_pd(0.0);
      mole_frac[348+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[348+idx],_mm_set1_pd(recip_molecular_masses[29])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[348+idx] = _mm_set1_pd(0.0);
      mole_frac[360+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[360+idx],_mm_set1_pd(recip_molecular_masses[30])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[360+idx] = _mm_set1_pd(0.0);
      mole_frac[372+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[372+idx],_mm_set1_pd(recip_molecular_masses[31])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[372+idx] = _mm_set1_pd(0.0);
      mole_frac[384+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[384+idx],_mm_set1_pd(recip_molecular_masses[32])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[384+idx] = _mm_set1_pd(0.0);
      mole_frac[396+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[396+idx],_mm_set1_pd(recip_molecular_masses[33])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[396+idx] = _mm_set1_pd(0.0);
      mole_frac[408+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[408+idx],_mm_set1_pd(recip_molecular_masses[34])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[408+idx] = _mm_set1_pd(0.0);
      mole_frac[420+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[420+idx],_mm_set1_pd(recip_molecular_masses[35])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[420+idx] = _mm_set1_pd(0.0);
      mole_frac[432+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[432+idx],_mm_set1_pd(recip_molecular_masses[36])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[432+idx] = _mm_set1_pd(0.0);
      mole_frac[444+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[444+idx],_mm_set1_pd(recip_molecular_masses[37])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[444+idx] = _mm_set1_pd(0.0);
      mole_frac[456+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[456+idx],_mm_set1_pd(recip_molecular_masses[38])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[456+idx] = _mm_set1_pd(0.0);
      mole_frac[468+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[468+idx],_mm_set1_pd(recip_molecular_masses[39])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[468+idx] = _mm_set1_pd(0.0);
      mole_frac[480+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[480+idx],_mm_set1_pd(recip_molecular_masses[40])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[480+idx] = _mm_set1_pd(0.0);
      mole_frac[492+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[492+idx],_mm_set1_pd(recip_molecular_masses[41])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[492+idx] = _mm_set1_pd(0.0);
      mole_frac[504+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[504+idx],_mm_set1_pd(recip_molecular_masses[42])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[504+idx] = _mm_set1_pd(0.0);
      mole_frac[516+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[516+idx],_mm_set1_pd(recip_molecular_masses[43])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[516+idx] = _mm_set1_pd(0.0);
      mole_frac[528+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[528+idx],_mm_set1_pd(recip_molecular_masses[44])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[528+idx] = _mm_set1_pd(0.0);
      mole_frac[540+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[540+idx],_mm_set1_pd(recip_molecular_masses[45])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[540+idx] = _mm_set1_pd(0.0);
      mole_frac[552+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[552+idx],_mm_set1_pd(recip_molecular_masses[46])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[552+idx] = _mm_set1_pd(0.0);
      mole_frac[564+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[564+idx],_mm_set1_pd(recip_molecular_masses[47])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[564+idx] = _mm_set1_pd(0.0);
      mole_frac[576+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[576+idx],_mm_set1_pd(recip_molecular_masses[48])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[576+idx] = _mm_set1_pd(0.0);
      mole_frac[588+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[588+idx],_mm_set1_pd(recip_molecular_masses[49])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[588+idx] = _mm_set1_pd(0.0);
      mole_frac[600+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[600+idx],_mm_set1_pd(recip_molecular_masses[50])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[600+idx] = _mm_set1_pd(0.0);
      mole_frac[612+idx] = 
        _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[612+idx],_mm_set1_pd(recip_molecular_masses[51])),_mm_set1_pd(1e3)),mixmw); 
      thermal_vec[612+idx] = _mm_set1_pd(0.0);
    }
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      // Light species H
      {
        // Interaction with H2
        {
          __m128d val = _mm_set1_pd(2.636753465162816e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.768015910550093e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.775348599026121e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.1585930684619718)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[12+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O
        {
          __m128d val = _mm_set1_pd(1.757513014843561e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.285543870147497e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.854588272324489e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2976867248976821)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[24+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O2
        {
          __m128d val = _mm_set1_pd(2.277133918512246e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.706164304353198e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.891613712354512e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2536889601459076)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[36+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with OH
        {
          __m128d val = _mm_set1_pd(1.770736265618588e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.295216099504371e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.876065744334992e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2999264728725038)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[48+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O
        {
          __m128d val = _mm_set1_pd(2.969326092389167e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.506085990918214e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.84139750691556e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1112922345203659)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[60+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CO
        {
          __m128d val = _mm_set1_pd(2.138048772731333e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.590656312136814e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.597782376370609e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2712782254042295)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[72+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CO2
        {
          __m128d val = _mm_set1_pd(3.126315401830763e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.480822205530582e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.132164599559319e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.06305126061316429)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[84+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3
        {
          __m128d val = _mm_set1_pd(2.447219115855363e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.87350239923066e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.393980243788655e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.1735855528169444)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[96+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH4
        {
          __m128d val = _mm_set1_pd(2.449287050458037e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.872670834275956e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.384325498849513e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.179062640187455)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[108+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HO2
        {
          __m128d val = _mm_set1_pd(2.281523401978496e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.709453166700634e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.899115323879656e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2541779799119729)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[120+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O2
        {
          __m128d val = _mm_set1_pd(2.285660222663966e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.712552719050051e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.906185135617233e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2546388512421464)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[132+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH2O
        {
          __m128d val = _mm_set1_pd(3.162068743695279e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.638982110193224e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.073407084606059e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.09121151917142747)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[144+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H6
        {
          __m128d val = _mm_set1_pd(3.068609912428751e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.437355985544139e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.03362935494119e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.05841809172467596)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[156+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H4
        {
          __m128d val = _mm_set1_pd(3.032335798336825e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.402507903023256e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(5.924350932493196e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.06680574170940606)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[168+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H5
        {
          __m128d val = _mm_set1_pd(3.061474747914644e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.431688619397056e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.019599895579059e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.05828225735440704)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[180+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H
        {
          __m128d val = _mm_set1_pd(3.062833770415917e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.444134365186354e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.094501095785496e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.04180903023643695)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[192+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H2
        {
          __m128d val = _mm_set1_pd(3.072412934963075e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.451778516653413e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.113561950276935e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.04193979005241541)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[204+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OH
        {
          __m128d val = _mm_set1_pd(3.185130935783487e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.651424075793323e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.077047347231746e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.08530313446041707)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[216+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH2CO
        {
          __m128d val = _mm_set1_pd(3.25385022377053e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.688268803523174e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.087521333192038e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.0666682584411887)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[228+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HCCO
        {
          __m128d val = _mm_set1_pd(2.708995271314182e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.079866531812512e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.897257316956756e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.1793027967505053)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[240+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3CHO
        {
          __m128d val = _mm_set1_pd(3.261002571364098e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.694177936269137e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.1031005432557e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.06681480315745897)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[252+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C3H4-A
        {
          __m128d val = _mm_set1_pd(3.230691790300545e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.613766930898042e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.66106286262361e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.320420756542652e-03)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[264+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C3H4-P
        {
          __m128d val = _mm_set1_pd(3.230691790300545e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.613766930898042e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.66106286262361e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.320420756542652e-03)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[276+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C3H6
        {
          __m128d val = _mm_set1_pd(3.223714578667719e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.598526009100887e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.583319655886146e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(8.841530685384569e-03)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[288+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C4H6
        {
          __m128d val = _mm_set1_pd(3.290512752654892e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.679705609324293e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.901653958129413e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.02458194725485896)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[300+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C4H7
        {
          __m128d val = _mm_set1_pd(3.292045725824064e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.67989830078188e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.897761918718049e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.02335263835172963)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[312+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C4H8-1
        {
          __m128d val = _mm_set1_pd(3.294210907594261e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.681660872577683e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.902298583638711e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.02336799740535642)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[324+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with PC4H9
        {
          __m128d val = _mm_set1_pd(3.295146592814352e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.680829524453684e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.893546331607471e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.02149408403266997)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[336+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3COCH2
        {
          __m128d val = _mm_set1_pd(3.295397853943752e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.722362960411805e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.176412891912808e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.06727921335864356)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[348+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H5CHO
        {
          __m128d val = _mm_set1_pd(3.297515624149514e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.723973288205335e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.180059381934386e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.06717800882994507)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[360+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C5H9
        {
          __m128d val = _mm_set1_pd(3.323050289531883e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.726604349389064e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.108437800855215e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.04798447273844533)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[372+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C5H10-1
        {
          __m128d val = _mm_set1_pd(3.324380578881488e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.722399878445388e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.075106530904983e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.04213655105859476)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[384+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C5H11-1
        {
          __m128d val = _mm_set1_pd(3.316720602534791e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.742411801307379e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.239744038703685e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.07023061728558644)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[396+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3O2
        {
          __m128d val = _mm_set1_pd(3.249694012524726e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.705168835280551e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.220500147192796e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.08703224165490056)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[408+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3O2H
        {
          __m128d val = _mm_set1_pd(3.252619028502869e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.707603729777164e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.226999244713772e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.0871105784756821)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[420+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H3CO
        {
          __m128d val = _mm_set1_pd(3.288473792618383e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.720186605455213e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.1859445557792e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.07079495433060273)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[432+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H3CHO
        {
          __m128d val = _mm_set1_pd(3.295317172770128e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.719176331453455e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.154619062778548e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.06402505305270359)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[444+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C3H5-A
        {
          __m128d val = _mm_set1_pd(3.227591603495213e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.606335427331569e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.622092313952259e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.866816986383802e-03)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[456+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C3H3
        {
          __m128d val = _mm_set1_pd(3.22649662451741e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.610372863526306e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.652413240556753e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.316109074572554e-03)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[468+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with NC3H7CHO
        {
          __m128d val = _mm_set1_pd(3.307964045390942e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.745854911053823e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.295120438281277e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.08087631222093869)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[480+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H5COCH2
        {
          __m128d val = _mm_set1_pd(3.311391013659248e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.744088971340765e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.270466687522299e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.07632773335550136)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[492+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3CHCO
        {
          __m128d val = _mm_set1_pd(3.29064002577697e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.721978487888793e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.190678189785176e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.0708415894529123)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[504+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with NC3H7COCH3
        {
          __m128d val = _mm_set1_pd(3.316305214484237e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.758495683296961e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.353587689012826e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.08673865957101029)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[516+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with NC3H7COCH2
        {
          __m128d val = _mm_set1_pd(3.308270456081856e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.757122913444244e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.373121603827887e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.09170212086428919)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[528+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with NC7H16
        {
          __m128d val = _mm_set1_pd(3.336227471919228e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.767227825010256e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.342852071174661e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.07947743603312946)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[540+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C7H15-1
        {
          __m128d val = _mm_set1_pd(3.335545466484962e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.766662136839891e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.341351014949413e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.07946118892656001)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[552+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C7H15O2
        {
          __m128d val = _mm_set1_pd(3.280936558612366e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.764466684600639e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.526250807223522e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1188862252915318)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[564+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C7H14OOHO2
        {
          __m128d val = _mm_set1_pd(3.253507397211482e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.75710681147811e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.576610031646734e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1316343877874153)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[576+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C7H14O
        {
          __m128d val = _mm_set1_pd(3.313264302131829e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.770977977503217e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.452730610745273e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1010865704936297)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[588+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with NC7KET
        {
          __m128d val = _mm_set1_pd(3.267508505142998e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.761313237880533e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(7.554028172414584e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1255892993729075)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[600+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with N2
        {
          __m128d val = _mm_set1_pd(2.130268425696491e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.584132795786654e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.581104184828728e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2725429566377711)); 
          thermal_vec[0+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[0+idx],mole_frac[612+idx]),thermal_vec[0+idx]); 
        }
      }
      // Light species H2
      {
        // Interaction with H
        {
          __m128d val = _mm_set1_pd(-2.636753465162816e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(1.768015910550093e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.775348599026121e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-0.1585930684619718)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[0+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with O
        {
          __m128d val = _mm_set1_pd(-8.093601450240291e-13);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.103164319166216e-09)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.287522678250429e-06)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4204726021699148)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[24+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with O2
        {
          __m128d val = _mm_set1_pd(2.810981392477091e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.740692804329701e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.085412256055141e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4534799151152161)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[36+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with OH
        {
          __m128d val = _mm_set1_pd(-8.217252067738418e-13);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(6.19640588058943e-09)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.337748058348944e-06)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4268964046290616)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[48+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with H2O
        {
          __m128d val = _mm_set1_pd(2.499719027252861e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.957520736623052e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.742310930643889e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.09225446319738231)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[60+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CO
        {
          __m128d val = _mm_set1_pd(1.588830133987327e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-9.211783055858657e-09)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.51672947887423e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4536271223894905)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[72+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CO2
        {
          __m128d val = _mm_set1_pd(1.496814407970614e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.072253571601051e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.336568835077498e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3511176078507614)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[84+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3
        {
          __m128d val = _mm_set1_pd(5.954421320119816e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.987829722071353e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(8.518539304410531e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3640489834414005)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[96+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH4
        {
          __m128d val = _mm_set1_pd(5.829054410894387e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-3.891311305124087e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(8.32178960638599e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3723903181438474)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[108+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with HO2
        {
          __m128d val = _mm_set1_pd(2.821860427380867e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.747429617965584e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.101223581825173e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4552349690041921)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[120+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with H2O2
        {
          __m128d val = _mm_set1_pd(2.83213077320014e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.753789502493254e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.11615025362608e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4568918264856361)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[132+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH2O
        {
          __m128d val = _mm_set1_pd(2.343646881471828e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.781654045987706e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.139309999808266e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.1948729212751452)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[144+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H6
        {
          __m128d val = _mm_set1_pd(1.454408449375579e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.043349476396319e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.276147817795946e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3339639954619114)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[156+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H4
        {
          __m128d val = _mm_set1_pd(1.388135433710349e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-9.92082642450402e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.157956205583607e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3372446703225273)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[168+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H5
        {
          __m128d val = _mm_set1_pd(1.44762907380564e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.038486153473362e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.265538101580283e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3324073025308119)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[180+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H
        {
          __m128d val = _mm_set1_pd(1.50987425728907e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.09042965559124e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.392349771058175e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3129007313029911)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[192+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H2
        {
          __m128d val = _mm_set1_pd(1.519378118153954e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.097293334258289e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.407408349118242e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3148702761180426)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[204+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3OH
        {
          __m128d val = _mm_set1_pd(2.327102184557304e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.764833502784834e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.087327251764788e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2037014810244143)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[216+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH2CO
        {
          __m128d val = _mm_set1_pd(2.279801198506851e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.716221798512884e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.937349326414702e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2319811932466156)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[228+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with HCCO
        {
          __m128d val = _mm_set1_pd(7.667061037097754e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-5.170172757195434e-08)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(1.102581228924998e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4266508259851286)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[240+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3CHO
        {
          __m128d val = _mm_set1_pd(2.289851358095159e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.723787503353852e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.954706536820286e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2330038473337971)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[252+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C3H4-A
        {
          __m128d val = _mm_set1_pd(1.888310737681516e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.388603431919326e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.100481316070349e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2929305605177095)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[264+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C3H4-P
        {
          __m128d val = _mm_set1_pd(1.888310737681516e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.388603431919326e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.100481316070349e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2929305605177095)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[276+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C3H6
        {
          __m128d val = _mm_set1_pd(1.823098471289975e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.334478883305589e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.96541354688965e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.3051880222242602)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[288+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C4H6
        {
          __m128d val = _mm_set1_pd(2.068795839855288e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.533286435933309e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.452962832267535e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2807711887572397)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[300+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C4H7
        {
          __m128d val = _mm_set1_pd(2.063977735122053e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.529017779117991e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.44158250624087e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2823555583880165)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[312+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C4H8-1
        {
          __m128d val = _mm_set1_pd(2.066696271536391e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.531031701283899e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.446115533514836e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.282727458653318)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[324+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with PC4H9
        {
          __m128d val = _mm_set1_pd(2.057714036645206e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.523324164542271e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.426112497355691e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2849016924115291)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[336+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3COCH2
        {
          __m128d val = _mm_set1_pd(2.336809597149917e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.75898558745162e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.035022162205326e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2381854910990132)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[348+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H5CHO
        {
          __m128d val = _mm_set1_pd(2.491575702759885e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.892981309676922e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.394499275830982e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2098605478757698)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[360+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C5H9
        {
          __m128d val = _mm_set1_pd(2.245473495982841e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.678251545914919e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.816451433092867e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2621011851697239)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[372+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C5H10-1
        {
          __m128d val = _mm_set1_pd(2.213786368334464e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.651309811589791e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.746396724757235e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2679372303381302)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[384+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C5H11-1
        {
          __m128d val = _mm_set1_pd(2.384797899271548e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.796719054618293e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.126220378373132e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.238815692951617)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[396+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3O2
        {
          __m128d val = _mm_set1_pd(2.42260678774566e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.837262519652026e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.255071740971657e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2120614186512262)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[408+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3O2H
        {
          __m128d val = _mm_set1_pd(2.426975779352755e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.840575886298645e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.262745447170254e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2124438556868042)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[420+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H3CO
        {
          __m128d val = _mm_set1_pd(2.352481466438435e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.773111948712001e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.074147004361504e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2336367604891859)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[432+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H3CHO
        {
          __m128d val = _mm_set1_pd(2.463169482033282e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.868815485794694e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.330541015233772e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2136884083029943)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[444+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C3H5-A
        {
          __m128d val = _mm_set1_pd(1.855233513522455e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.361089053218244e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.031602478166766e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.299205069471473)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[456+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C3H3
        {
          __m128d val = _mm_set1_pd(1.883400286801476e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.384992443108944e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(3.092418680560616e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.292168810293035)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[468+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with NC3H7CHO
        {
          __m128d val = _mm_set1_pd(2.560446285481596e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.94951688353351e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.538720510087629e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2057271860455629)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[480+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C2H5COCH2
        {
          __m128d val = _mm_set1_pd(2.563191071693732e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.952187591535617e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.546740419576007e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2045964797566218)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[492+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with CH3CHCO
        {
          __m128d val = _mm_set1_pd(2.355584879386443e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.77545105261501e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.079521652683204e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2339449760300846)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[504+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with NC3H7COCH3
        {
          __m128d val = _mm_set1_pd(2.573775681904159e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.958447266052752e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.555743378994857e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.209647883107428)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[516+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with NC3H7COCH2
        {
          __m128d val = _mm_set1_pd(2.535475864578331e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.924961889670894e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.464556531291609e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2168498660404349)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[528+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with NC7H16
        {
          __m128d val = _mm_set1_pd(2.477325850498314e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.872262620347478e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.316762139434951e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2330125297269697)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[540+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C7H15-1
        {
          __m128d val = _mm_set1_pd(2.476312792988248e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.871496992479841e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.314996877790567e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2329172434758164)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[552+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C7H15O2
        {
          __m128d val = _mm_set1_pd(2.7570900537465e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.116487295622062e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.982492158902414e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.183133307386705)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[564+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C7H14OOHO2
        {
          __m128d val = _mm_set1_pd(2.841286845917054e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.190737570648687e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(5.189071513362518e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.1687672145898553)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[576+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with C7H14O
        {
          __m128d val = _mm_set1_pd(2.619695016529612e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-1.995348344161635e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(4.647655258162161e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.2088141607458208)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[588+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with NC7KET
        {
          __m128d val = _mm_set1_pd(2.808741692316584e-11);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-2.16215060143183e-07)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(5.109756715260993e-04)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.173974029290639)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[600+idx]),thermal_vec[12+idx]); 
        }
        // Interaction with N2
        {
          __m128d val = _mm_set1_pd(1.514695193142395e-12);
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(-8.719776099246706e-09)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(2.424900788336922e-05)); 
          val = 
            _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd(0.4541343543221107)); 
          thermal_vec[12+idx] = 
            _mm_add_pd(_mm_mul_pd(mole_frac[12+idx],mole_frac[612+idx]),thermal_vec[12+idx]); 
        }
      }
    }
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      _mm_store_pd(thermal+(0*spec_stride)+(idx<<1),thermal_vec[0+idx]);
      _mm_store_pd(thermal+(1*spec_stride)+(idx<<1),thermal_vec[12+idx]);
      _mm_store_pd(thermal+(2*spec_stride)+(idx<<1),thermal_vec[24+idx]);
      _mm_store_pd(thermal+(3*spec_stride)+(idx<<1),thermal_vec[36+idx]);
      _mm_store_pd(thermal+(4*spec_stride)+(idx<<1),thermal_vec[48+idx]);
      _mm_store_pd(thermal+(5*spec_stride)+(idx<<1),thermal_vec[60+idx]);
      _mm_store_pd(thermal+(6*spec_stride)+(idx<<1),thermal_vec[72+idx]);
      _mm_store_pd(thermal+(7*spec_stride)+(idx<<1),thermal_vec[84+idx]);
      _mm_store_pd(thermal+(8*spec_stride)+(idx<<1),thermal_vec[96+idx]);
      _mm_store_pd(thermal+(9*spec_stride)+(idx<<1),thermal_vec[108+idx]);
      _mm_store_pd(thermal+(10*spec_stride)+(idx<<1),thermal_vec[120+idx]);
      _mm_store_pd(thermal+(11*spec_stride)+(idx<<1),thermal_vec[132+idx]);
      _mm_store_pd(thermal+(12*spec_stride)+(idx<<1),thermal_vec[144+idx]);
      _mm_store_pd(thermal+(13*spec_stride)+(idx<<1),thermal_vec[156+idx]);
      _mm_store_pd(thermal+(14*spec_stride)+(idx<<1),thermal_vec[168+idx]);
      _mm_store_pd(thermal+(15*spec_stride)+(idx<<1),thermal_vec[180+idx]);
      _mm_store_pd(thermal+(16*spec_stride)+(idx<<1),thermal_vec[192+idx]);
      _mm_store_pd(thermal+(17*spec_stride)+(idx<<1),thermal_vec[204+idx]);
      _mm_store_pd(thermal+(18*spec_stride)+(idx<<1),thermal_vec[216+idx]);
      _mm_store_pd(thermal+(19*spec_stride)+(idx<<1),thermal_vec[228+idx]);
      _mm_store_pd(thermal+(20*spec_stride)+(idx<<1),thermal_vec[240+idx]);
      _mm_store_pd(thermal+(21*spec_stride)+(idx<<1),thermal_vec[252+idx]);
      _mm_store_pd(thermal+(22*spec_stride)+(idx<<1),thermal_vec[264+idx]);
      _mm_store_pd(thermal+(23*spec_stride)+(idx<<1),thermal_vec[276+idx]);
      _mm_store_pd(thermal+(24*spec_stride)+(idx<<1),thermal_vec[288+idx]);
      _mm_store_pd(thermal+(25*spec_stride)+(idx<<1),thermal_vec[300+idx]);
      _mm_store_pd(thermal+(26*spec_stride)+(idx<<1),thermal_vec[312+idx]);
      _mm_store_pd(thermal+(27*spec_stride)+(idx<<1),thermal_vec[324+idx]);
      _mm_store_pd(thermal+(28*spec_stride)+(idx<<1),thermal_vec[336+idx]);
      _mm_store_pd(thermal+(29*spec_stride)+(idx<<1),thermal_vec[348+idx]);
      _mm_store_pd(thermal+(30*spec_stride)+(idx<<1),thermal_vec[360+idx]);
      _mm_store_pd(thermal+(31*spec_stride)+(idx<<1),thermal_vec[372+idx]);
      _mm_store_pd(thermal+(32*spec_stride)+(idx<<1),thermal_vec[384+idx]);
      _mm_store_pd(thermal+(33*spec_stride)+(idx<<1),thermal_vec[396+idx]);
      _mm_store_pd(thermal+(34*spec_stride)+(idx<<1),thermal_vec[408+idx]);
      _mm_store_pd(thermal+(35*spec_stride)+(idx<<1),thermal_vec[420+idx]);
      _mm_store_pd(thermal+(36*spec_stride)+(idx<<1),thermal_vec[432+idx]);
      _mm_store_pd(thermal+(37*spec_stride)+(idx<<1),thermal_vec[444+idx]);
      _mm_store_pd(thermal+(38*spec_stride)+(idx<<1),thermal_vec[456+idx]);
      _mm_store_pd(thermal+(39*spec_stride)+(idx<<1),thermal_vec[468+idx]);
      _mm_store_pd(thermal+(40*spec_stride)+(idx<<1),thermal_vec[480+idx]);
      _mm_store_pd(thermal+(41*spec_stride)+(idx<<1),thermal_vec[492+idx]);
      _mm_store_pd(thermal+(42*spec_stride)+(idx<<1),thermal_vec[504+idx]);
      _mm_store_pd(thermal+(43*spec_stride)+(idx<<1),thermal_vec[516+idx]);
      _mm_store_pd(thermal+(44*spec_stride)+(idx<<1),thermal_vec[528+idx]);
      _mm_store_pd(thermal+(45*spec_stride)+(idx<<1),thermal_vec[540+idx]);
      _mm_store_pd(thermal+(46*spec_stride)+(idx<<1),thermal_vec[552+idx]);
      _mm_store_pd(thermal+(47*spec_stride)+(idx<<1),thermal_vec[564+idx]);
      _mm_store_pd(thermal+(48*spec_stride)+(idx<<1),thermal_vec[576+idx]);
      _mm_store_pd(thermal+(49*spec_stride)+(idx<<1),thermal_vec[588+idx]);
      _mm_store_pd(thermal+(50*spec_stride)+(idx<<1),thermal_vec[600+idx]);
      _mm_store_pd(thermal+(51*spec_stride)+(idx<<1),thermal_vec[612+idx]);
    }
  }
  free(mole_frac);
  free(temperature);
  free(thermal_vec);
}

