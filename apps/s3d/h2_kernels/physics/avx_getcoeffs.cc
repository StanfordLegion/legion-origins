
#include "avx_getcoeffs.h"
#include <cmath>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <emmintrin.h>
#include <smmintrin.h>
#include <immintrin.h>

static const double molecular_masses[9] = {2.01594, 31.9988, 15.9994, 17.00737, 
  18.01534, 1.00797, 33.00677, 34.01474, 28.0134}; 

static const double recip_molecular_masses[9] = {0.4960465093207139, 
  0.03125117191894696, 0.06250234383789392, 0.05879803873262004, 
  0.05550825019122593, 0.9920930186414277, 0.03029681486555637, 
  0.02939901936631002, 0.03569720205330306}; 

void avx_conductivity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *lambda) 
{
  __m256d *sum = (__m256d*)malloc(1021*sizeof(__m256d));
  __m256d *sumr = (__m256d*)malloc(1021*sizeof(__m256d));
  __m256d *logt = (__m256d*)malloc(1021*sizeof(__m256d));
  __m256d *mixmw = (__m256d*)malloc(1021*sizeof(__m256d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 4084)
  {
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      logt[idx] = _mm256_load_pd(temperature_array);
      temperature_array += 4;
      mixmw[idx] = _mm256_load_pd(mixmw_array);
      mixmw_array += 4;
      sum[idx] = _mm256_set1_pd(0.0);
      sumr[idx] = _mm256_set1_pd(0.0);
      logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(120.0));
      __m128d lower = _mm256_extractf128_pd(logt[idx],0);
      __m128d upper = _mm256_extractf128_pd(logt[idx],1);
      logt[idx] = 
        _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
    }
    // Species H2
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      __m256d val = _mm256_set1_pd(-8.971812440098445e-03);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.2434840522735603)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.3149816638112)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(11.09465938678581)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species O2
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      __m256d val = _mm256_set1_pd(0.01344785152870045);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.3099605229855532)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.151649687562649)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-2.51386921584388)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species O
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      __m256d val = _mm256_set1_pd(6.908732177853375e-03);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1549096547430882)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.801392496839076)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.969675461941004)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species OH
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      __m256d val = _mm256_set1_pd(-0.02977135865933274);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.6631509208736623)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.103241187134545)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(16.05448695051902)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species H2O
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      __m256d val = _mm256_set1_pd(-0.07286086542855576);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.459337125398943)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.451500192314084)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(22.12505312748445)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species H
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      __m256d val = _mm256_set1_pd(0.01585988965532566);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.3631110078153958)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.416401604464596)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.3253871703970487)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species HO2
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      __m256d val = _mm256_set1_pd(4.072437253728964e-04);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.05282466276443485)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.591058710589892)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5546382791949179)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species H2O2
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      __m256d val = _mm256_set1_pd(-6.382550767548813e-03);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.05716835124727309)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.062275726777442)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.486257799258664)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species N2
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      __m256d val = _mm256_set1_pd(-0.02750102626098917);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5546581094878709)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-2.911524100094121)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(11.54289501420278)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    for (unsigned idx = 0; idx < 1021; idx++)
    {
      __m256d result = 
        _mm256_add_pd(sum[idx],_mm256_div_pd(_mm256_set1_pd(1.0),sumr[idx])); 
      result = _mm256_mul_pd(result,_mm256_set1_pd(0.5));
      result = _mm256_mul_pd(result,_mm256_set1_pd(5.403325855130351e-09));
      _mm256_store_pd(lambda,result);
      lambda += 4;
    }
    remaining_elmts -= 4084;
    mass_frac_array += 4084;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 4) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      logt[idx] = _mm256_load_pd(temperature_array);
      temperature_array += 4;
      mixmw[idx] = _mm256_load_pd(mixmw_array);
      mixmw_array += 4;
      sum[idx] = _mm256_set1_pd(0.0);
      sumr[idx] = _mm256_set1_pd(0.0);
      logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(120.0));
      __m128d lower = _mm256_extractf128_pd(logt[idx],0);
      __m128d upper = _mm256_extractf128_pd(logt[idx],1);
      logt[idx] = 
        _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
    }
    // Species H2
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d val = _mm256_set1_pd(-8.971812440098445e-03);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.2434840522735603)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.3149816638112)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(11.09465938678581)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species O2
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d val = _mm256_set1_pd(0.01344785152870045);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.3099605229855532)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.151649687562649)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-2.51386921584388)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species O
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d val = _mm256_set1_pd(6.908732177853375e-03);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1549096547430882)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.801392496839076)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.969675461941004)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species OH
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d val = _mm256_set1_pd(-0.02977135865933274);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.6631509208736623)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.103241187134545)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(16.05448695051902)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species H2O
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d val = _mm256_set1_pd(-0.07286086542855576);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.459337125398943)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.451500192314084)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(22.12505312748445)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species H
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d val = _mm256_set1_pd(0.01585988965532566);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.3631110078153958)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.416401604464596)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.3253871703970487)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species HO2
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d val = _mm256_set1_pd(4.072437253728964e-04);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.05282466276443485)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.591058710589892)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5546382791949179)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species H2O2
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d val = _mm256_set1_pd(-6.382550767548813e-03);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.05716835124727309)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.062275726777442)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.486257799258664)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    // Species N2
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d val = _mm256_set1_pd(-0.02750102626098917);
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5546581094878709)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-2.911524100094121)); 
      val = 
        _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(11.54289501420278)); 
      {
        __m128d lower = _mm256_extractf128_pd(val,0);
        __m128d upper = _mm256_extractf128_pd(val,1);
        val = 
          _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
      }
      __m256d frac = _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2));
      __m256d mole = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw[idx]); 
      sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
      sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
    }
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d result = 
        _mm256_add_pd(sum[idx],_mm256_div_pd(_mm256_set1_pd(1.0),sumr[idx])); 
      result = _mm256_mul_pd(result,_mm256_set1_pd(0.5));
      result = _mm256_mul_pd(result,_mm256_set1_pd(5.403325855130351e-09));
      _mm256_store_pd(lambda,result);
      lambda += 4;
    }
  }
  free(sum);
  free(sumr);
  free(logt);
  free(mixmw);
}

void avx_viscosity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *viscosity) 
{
  __m256d *mole_frac = (__m256d*)malloc(214*9*sizeof(__m256d));
  __m256d *spec_visc = (__m256d*)malloc(214*9*sizeof(__m256d));
  __m256d *result_vec = (__m256d*)malloc(214*sizeof(__m256d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 856)
  {
    for (unsigned idx = 0; idx < 214; idx++)
    {
      __m256d temp = _mm256_load_pd(temperature_array);
      temperature_array += 4;
      mole_frac[0+idx] = 
        _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
      mole_frac[214+idx] = 
        _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
      mole_frac[428+idx] = 
        _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
      mole_frac[642+idx] = 
        _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
      mole_frac[856+idx] = 
        _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
      mole_frac[1070+idx] = 
        _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
      mole_frac[1284+idx] = 
        _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
      mole_frac[1498+idx] = 
        _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
      mole_frac[1712+idx] = 
        _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
      __m256d mixmw = _mm256_load_pd(mixmw_array);
      mixmw_array += 4;
      result_vec[idx] = _mm256_set1_pd(0.0);
      temp = _mm256_mul_pd(temp,_mm256_set1_pd(120.0));
      __m128d lower = _mm256_extractf128_pd(temp,0);
      __m128d upper = _mm256_extractf128_pd(temp,1);
      __m256d logt = 
        _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      // Species H2
      {
        __m256d val = _mm256_set1_pd(2.095995943985418e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.04534890654515034)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(0.9708642724957206)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-13.76709568554653)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[0+idx] = val;
      }
      // Species O2
      {
        __m256d val = _mm256_set1_pd(0.01100612385033574);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.2490705573726834)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.522523673476987)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.8107894214942)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[214+idx] = val;
      }
      // Species O
      {
        __m256d val = _mm256_set1_pd(6.908732177852647e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.1549096547430725)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.801392496838963)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-14.81562698872598)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[428+idx] = val;
      }
      // Species OH
      {
        __m256d val = _mm256_set1_pd(6.90873217785305e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.154909654743081)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.801392496839023)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-14.78507920973381)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[642+idx] = val;
      }
      // Species H2O
      {
        __m256d val = _mm256_set1_pd(-0.01986368642206398);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(0.3341412968649872)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.7882553001460173)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-11.87799986949853)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[856+idx] = val;
      }
      // Species H
      {
        __m256d val = _mm256_set1_pd(0.01585988965532433);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.3631110078153673)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.416401604464392)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-19.87530243529893)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1070+idx] = val;
      }
      // Species HO2
      {
        __m256d val = _mm256_set1_pd(0.01100612385033564);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.2490705573726812)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.522523673476971)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.79528227657199)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1284+idx] = val;
      }
      // Species H2O2
      {
        __m256d val = _mm256_set1_pd(0.0110061238503359);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.249070557372687)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.522523673477013)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.78024164250196)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1498+idx] = val;
      }
      // Species N2
      {
        __m256d val = _mm256_set1_pd(9.477798464440641e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.2138335631679552)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.251736741733607)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.26171975836591)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1712+idx] = val;
      }
      mole_frac[0+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[214+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[428+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[642+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[856+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1070+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1284+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1498+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1712+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
    }
    for (unsigned idx = 0; idx < 214; idx++)
    {
      // Species H2
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(3.98407994930469),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.9699140894422974)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(2.817169948942702),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.9423898864639306)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(2.904556165961648),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.9455304890716288)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(2.989388987941487),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.9483461395154609)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[1070+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7071067811865476),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.5773502691896258)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[1284+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(4.046343169140707),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.9707930073454198)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[1498+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(4.107662722577361),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.971622019642798)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[1712+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(3.727727093579261),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.9658507417986416)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[0+idx],spec_visc[0+idx]),spec_sum),result_vec[idx]); 
      }
      // Species O2
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2509989791180074),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.2434474462821885)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[214+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7071067811865476),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.5773502691896258)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7290406324472875),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.5891056647772915)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7503335841599268),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.6001707540730823)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[1070+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.1774830802052437),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.1747520542941766)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[1284+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.01562800461544),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.7125678571310604)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[1498+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.031019149928013),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.7178224991180043)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[1712+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9356556949189316),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.6832240371937915)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[214+idx],spec_visc[214+idx]),spec_sum),result_vec[idx]); 
      }
      // Species O
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.3549661604104874),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.3345165196077766)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.414213562373095),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.8164965809277261)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[428+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.031019149928013),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.7178224991180043)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.061131931022983),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.7277591150584508)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[1070+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2509989791180074),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.2434474462821885)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[1284+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.43631489845308),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.8206843133241595)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[1498+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.458081264894575),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.8246832525414386)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[1712+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.323216973465976),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.7977988613181562)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[428+idx],spec_visc[428+idx]),spec_sum),result_vec[idx]); 
      }
      // Species OH
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.3442866802573664),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.3255335531645952)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.371665659626048),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.8080560102661852)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9699140894422975),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.6962261556132335)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[642+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.029206810656303),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.7172100349425605)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[1070+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2434474462821884),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.2365388994335815)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[1284+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.393102056885525),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.8123723078052557)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[1498+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.414213562373095),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.8164965809277261)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[1712+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.283406785953845),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.7888173457727758)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[642+idx],spec_visc[642+idx]),spec_sum),result_vec[idx]); 
      }
      // Species H2O
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.3345165196077766),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.3172374499741829)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.332740558480532),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.7998719059670416)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9423898864639306),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.6858328298130242)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9716220196427982),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.6968570626589725)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[856+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[1070+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2365388994335815),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.2301869829464367)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[1284+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.35356863407965),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.8043078475514793)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[1498+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.374081037679183),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.8085489752128718)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[1712+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.246986293391747),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.7801324949225127)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[856+idx],spec_visc[856+idx]),spec_sum),result_vec[idx]); 
      }
      // Species H
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[1070+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.414213562373095),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.8164965809277261)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1070+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(5.634339897885405),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.9846124717471159)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1070+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(3.98407994930469),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.9699140894422974)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1070+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(4.107662722577361),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.971622019642798)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1070+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(4.227634449955632),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.9731464190357054)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1070+idx],spec_visc[1284+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(5.722393387814518),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.9850719113086823)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1070+idx],spec_visc[1498+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(5.809112331923296),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.9855047090477966)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1070+idx],spec_visc[1712+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(5.271802212565431),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.9824805380660118)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[1070+idx],spec_visc[1070+idx]),spec_sum),result_vec[idx]); 
      }
      // Species HO2
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[1284+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.247136725235384),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.2399186047167572)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1284+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.984612471747116),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.7016031990973591)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1284+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.6962261556132336),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.5713818844377662)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1284+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7178224991180044),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.5831391202030293)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1284+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7387878049345782),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.5942128291842127)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1284+idx],spec_visc[1070+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.1747520542941766),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.1721433401286832)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1284+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1284+idx],spec_visc[1498+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(1.015154313629231),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.7124042040882749)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1284+idx],spec_visc[1712+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9212582664783947),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.6775575744738503)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[1284+idx],spec_visc[1284+idx]),spec_sum),result_vec[idx]); 
      }
      // Species H2O2
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[1498+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2434474462821884),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.2365388994335815)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1498+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9699140894422975),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.6962261556132335)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1498+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.6858328298130242),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.5655948487899036)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1498+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7071067811865476),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.5773502691896258)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1498+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7277591150584509),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.5884288866823369)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1498+idx],spec_visc[1070+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.1721433401286832),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.1696480723280338)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1498+idx],spec_visc[1284+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9850719113086823),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.7017693709455775)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1498+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1498+idx],spec_visc[1712+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9075056413687956),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.6720298008362261)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[1498+idx],spec_visc[1498+idx]),spec_sum),result_vec[idx]); 
      }
      // Species N2
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[1712+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2682599811886517),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.2590991018259489)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1712+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.06876921225456),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.7302088160249892)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1712+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7557339575086043),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.6029236907598284)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1712+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7791761824422541),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.614627688123463)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1712+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.801933433670746),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.6256143303713364)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1712+idx],spec_visc[1070+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.1896884518194713),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.1863652122085029)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1712+idx],spec_visc[1284+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(1.085471942436515),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.7354697364767042)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1712+idx],spec_visc[1498+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(1.101921524687929),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.7405241027731794)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1712+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[1712+idx],spec_visc[1712+idx]),spec_sum),result_vec[idx]); 
      }
    }
    for (unsigned idx = 0; idx < 214; idx++)
    {
      result_vec[idx] = 
        _mm256_mul_pd(_mm256_set1_pd(0.1535268500652559),result_vec[idx]); 
      _mm256_store_pd(viscosity,result_vec[idx]);
      viscosity += 4;
    }
    remaining_elmts -= 856;
    mass_frac_array += 856;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 4) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d temp = _mm256_load_pd(temperature_array);
      temperature_array += 4;
      mole_frac[0+idx] = 
        _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
      mole_frac[214+idx] = 
        _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
      mole_frac[428+idx] = 
        _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
      mole_frac[642+idx] = 
        _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
      mole_frac[856+idx] = 
        _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
      mole_frac[1070+idx] = 
        _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
      mole_frac[1284+idx] = 
        _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
      mole_frac[1498+idx] = 
        _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
      mole_frac[1712+idx] = 
        _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
      __m256d mixmw = _mm256_load_pd(mixmw_array);
      mixmw_array += 4;
      result_vec[idx] = _mm256_set1_pd(0.0);
      temp = _mm256_mul_pd(temp,_mm256_set1_pd(120.0));
      __m128d lower = _mm256_extractf128_pd(temp,0);
      __m128d upper = _mm256_extractf128_pd(temp,1);
      __m256d logt = 
        _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      // Species H2
      {
        __m256d val = _mm256_set1_pd(2.095995943985418e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.04534890654515034)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(0.9708642724957206)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-13.76709568554653)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[0+idx] = val;
      }
      // Species O2
      {
        __m256d val = _mm256_set1_pd(0.01100612385033574);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.2490705573726834)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.522523673476987)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.8107894214942)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[214+idx] = val;
      }
      // Species O
      {
        __m256d val = _mm256_set1_pd(6.908732177852647e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.1549096547430725)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.801392496838963)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-14.81562698872598)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[428+idx] = val;
      }
      // Species OH
      {
        __m256d val = _mm256_set1_pd(6.90873217785305e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.154909654743081)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.801392496839023)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-14.78507920973381)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[642+idx] = val;
      }
      // Species H2O
      {
        __m256d val = _mm256_set1_pd(-0.01986368642206398);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(0.3341412968649872)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.7882553001460173)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-11.87799986949853)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[856+idx] = val;
      }
      // Species H
      {
        __m256d val = _mm256_set1_pd(0.01585988965532433);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.3631110078153673)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.416401604464392)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-19.87530243529893)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1070+idx] = val;
      }
      // Species HO2
      {
        __m256d val = _mm256_set1_pd(0.01100612385033564);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.2490705573726812)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.522523673476971)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.79528227657199)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1284+idx] = val;
      }
      // Species H2O2
      {
        __m256d val = _mm256_set1_pd(0.0110061238503359);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.249070557372687)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.522523673477013)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.78024164250196)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1498+idx] = val;
      }
      // Species N2
      {
        __m256d val = _mm256_set1_pd(9.477798464440641e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.2138335631679552)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.251736741733607)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.26171975836591)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1712+idx] = val;
      }
      mole_frac[0+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[214+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[428+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[642+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[856+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1070+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1284+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1498+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1712+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
    }
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      // Species H2
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(3.98407994930469),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.9699140894422974)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(2.817169948942702),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.9423898864639306)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(2.904556165961648),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.9455304890716288)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(2.989388987941487),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.9483461395154609)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[1070+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7071067811865476),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.5773502691896258)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[1284+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(4.046343169140707),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.9707930073454198)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[1498+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(4.107662722577361),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.971622019642798)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[0+idx],spec_visc[1712+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(3.727727093579261),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.9658507417986416)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[0+idx],spec_visc[0+idx]),spec_sum),result_vec[idx]); 
      }
      // Species O2
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2509989791180074),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.2434474462821885)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[214+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7071067811865476),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.5773502691896258)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7290406324472875),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.5891056647772915)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7503335841599268),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.6001707540730823)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[1070+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.1774830802052437),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.1747520542941766)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[1284+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.01562800461544),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.7125678571310604)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[1498+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.031019149928013),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.7178224991180043)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[214+idx],spec_visc[1712+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9356556949189316),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.6832240371937915)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[214+idx],spec_visc[214+idx]),spec_sum),result_vec[idx]); 
      }
      // Species O
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.3549661604104874),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.3345165196077766)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.414213562373095),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.8164965809277261)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[428+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.031019149928013),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.7178224991180043)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.061131931022983),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.7277591150584508)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[1070+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2509989791180074),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.2434474462821885)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[1284+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.43631489845308),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.8206843133241595)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[1498+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.458081264894575),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.8246832525414386)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[428+idx],spec_visc[1712+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.323216973465976),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.7977988613181562)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[428+idx],spec_visc[428+idx]),spec_sum),result_vec[idx]); 
      }
      // Species OH
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.3442866802573664),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.3255335531645952)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.371665659626048),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.8080560102661852)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9699140894422975),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.6962261556132335)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[642+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.029206810656303),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.7172100349425605)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[1070+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2434474462821884),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.2365388994335815)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[1284+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.393102056885525),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.8123723078052557)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[1498+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.414213562373095),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.8164965809277261)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[642+idx],spec_visc[1712+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.283406785953845),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.7888173457727758)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[642+idx],spec_visc[642+idx]),spec_sum),result_vec[idx]); 
      }
      // Species H2O
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.3345165196077766),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.3172374499741829)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.332740558480532),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.7998719059670416)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9423898864639306),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.6858328298130242)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9716220196427982),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.6968570626589725)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[856+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[1070+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2365388994335815),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.2301869829464367)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[1284+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.35356863407965),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.8043078475514793)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[1498+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.374081037679183),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.8085489752128718)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[856+idx],spec_visc[1712+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.246986293391747),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.7801324949225127)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[856+idx],spec_visc[856+idx]),spec_sum),result_vec[idx]); 
      }
      // Species H
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[1070+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.414213562373095),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.8164965809277261)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1070+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(5.634339897885405),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.9846124717471159)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1070+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(3.98407994930469),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.9699140894422974)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1070+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(4.107662722577361),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.971622019642798)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1070+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(4.227634449955632),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.9731464190357054)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1070+idx],spec_visc[1284+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(5.722393387814518),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.9850719113086823)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1070+idx],spec_visc[1498+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(5.809112331923296),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.9855047090477966)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1070+idx],spec_visc[1712+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(5.271802212565431),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.9824805380660118)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[1070+idx],spec_visc[1070+idx]),spec_sum),result_vec[idx]); 
      }
      // Species HO2
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[1284+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.247136725235384),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.2399186047167572)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1284+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.984612471747116),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.7016031990973591)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1284+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.6962261556132336),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.5713818844377662)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1284+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7178224991180044),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.5831391202030293)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1284+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7387878049345782),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.5942128291842127)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1284+idx],spec_visc[1070+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.1747520542941766),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.1721433401286832)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1284+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1284+idx],spec_visc[1498+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(1.015154313629231),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.7124042040882749)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1284+idx],spec_visc[1712+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9212582664783947),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.6775575744738503)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[1284+idx],spec_visc[1284+idx]),spec_sum),result_vec[idx]); 
      }
      // Species H2O2
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[1498+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2434474462821884),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.2365388994335815)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1498+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9699140894422975),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.6962261556132335)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1498+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.6858328298130242),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.5655948487899036)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1498+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7071067811865476),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.5773502691896258)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1498+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7277591150584509),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.5884288866823369)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1498+idx],spec_visc[1070+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.1721433401286832),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.1696480723280338)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1498+idx],spec_visc[1284+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9850719113086823),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.7017693709455775)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1498+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1498+idx],spec_visc[1712+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.9075056413687956),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],numer),numer),_mm256_set1_pd(0.6720298008362261)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[1498+idx],spec_visc[1498+idx]),spec_sum),result_vec[idx]); 
      }
      // Species N2
      {
        __m256d spec_sum = _mm256_set1_pd(0.0);
        {
          __m256d numer = _mm256_div_pd(spec_visc[1712+idx],spec_visc[0+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.2682599811886517),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],numer),numer),_mm256_set1_pd(0.2590991018259489)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1712+idx],spec_visc[214+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(1.06876921225456),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],numer),numer),_mm256_set1_pd(0.7302088160249892)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1712+idx],spec_visc[428+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7557339575086043),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],numer),numer),_mm256_set1_pd(0.6029236907598284)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1712+idx],spec_visc[642+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.7791761824422541),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],numer),numer),_mm256_set1_pd(0.614627688123463)),spec_sum); 
        }
        {
          __m256d numer = _mm256_div_pd(spec_visc[1712+idx],spec_visc[856+idx]);
          numer = _mm256_mul_pd(_mm256_set1_pd(0.801933433670746),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],numer),numer),_mm256_set1_pd(0.6256143303713364)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1712+idx],spec_visc[1070+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(0.1896884518194713),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],numer),numer),_mm256_set1_pd(0.1863652122085029)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1712+idx],spec_visc[1284+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(1.085471942436515),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],numer),numer),_mm256_set1_pd(0.7354697364767042)),spec_sum); 
        }
        {
          __m256d numer = 
            _mm256_div_pd(spec_visc[1712+idx],spec_visc[1498+idx]); 
          numer = _mm256_mul_pd(_mm256_set1_pd(1.101921524687929),numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],numer),numer),_mm256_set1_pd(0.7405241027731794)),spec_sum); 
        }
        {
          spec_sum = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1712+idx],_mm256_set1_pd(2.82842712474619)),spec_sum); 
        }
        result_vec[idx] = 
          _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd(mole_frac[1712+idx],spec_visc[1712+idx]),spec_sum),result_vec[idx]); 
      }
    }
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      result_vec[idx] = 
        _mm256_mul_pd(_mm256_set1_pd(0.1535268500652559),result_vec[idx]); 
      _mm256_store_pd(viscosity,result_vec[idx]);
      viscosity += 4;
    }
  }
  free(mole_frac);
  free(spec_visc);
  free(result_vec);
}

void avx_diffusion(const double *temperature_array, const double 
  *pressure_array, const double *mass_frac_array, const double *mixmw_array, 
  const int num_elmts, const int spec_stride, double *diffusion) 
{
  __m256d *mole_frac = (__m256d*)malloc(143*9*sizeof(__m256d));
  __m256d *clamped = (__m256d*)malloc(143*9*sizeof(__m256d));
  __m256d *logt = (__m256d*)malloc(143*sizeof(__m256d));
  __m256d *sumxod = (__m256d*)malloc(143*9*sizeof(__m256d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 572)
  {
    for (unsigned idx = 0; idx < 143; idx++)
    {
      logt[idx] = _mm256_load_pd(temperature_array);
      temperature_array += 4;
      mole_frac[0+idx] = 
        _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
      mole_frac[143+idx] = 
        _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
      mole_frac[286+idx] = 
        _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
      mole_frac[429+idx] = 
        _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
      mole_frac[572+idx] = 
        _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
      mole_frac[715+idx] = 
        _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
      mole_frac[858+idx] = 
        _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
      mole_frac[1001+idx] = 
        _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
      mole_frac[1144+idx] = 
        _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
      __m256d mixmw = _mm256_load_pd(mixmw_array);
      mixmw_array += 4;
      sumxod[0+idx] = _mm256_set1_pd(0.0);
      sumxod[143+idx] = _mm256_set1_pd(0.0);
      sumxod[286+idx] = _mm256_set1_pd(0.0);
      sumxod[429+idx] = _mm256_set1_pd(0.0);
      sumxod[572+idx] = _mm256_set1_pd(0.0);
      sumxod[715+idx] = _mm256_set1_pd(0.0);
      sumxod[858+idx] = _mm256_set1_pd(0.0);
      sumxod[1001+idx] = _mm256_set1_pd(0.0);
      sumxod[1144+idx] = _mm256_set1_pd(0.0);
      logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(120.0));
      __m128d lower = _mm256_extractf128_pd(logt[idx],0);
      __m128d upper = _mm256_extractf128_pd(logt[idx],1);
      logt[idx] = 
        _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      {
        mole_frac[0+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[0+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[0+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[0+idx] = 
          _mm256_add_pd(clamped[0+idx],_mm256_andnot_pd(mask,mole_frac[0+idx])); 
      }
      {
        mole_frac[143+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[143+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[143+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[143+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[143+idx] = 
          _mm256_add_pd(clamped[143+idx],_mm256_andnot_pd(mask,mole_frac[143+idx])); 
      }
      {
        mole_frac[286+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[286+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[286+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[286+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[286+idx] = 
          _mm256_add_pd(clamped[286+idx],_mm256_andnot_pd(mask,mole_frac[286+idx])); 
      }
      {
        mole_frac[429+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[429+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[429+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[429+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[429+idx] = 
          _mm256_add_pd(clamped[429+idx],_mm256_andnot_pd(mask,mole_frac[429+idx])); 
      }
      {
        mole_frac[572+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[572+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[572+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[572+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[572+idx] = 
          _mm256_add_pd(clamped[572+idx],_mm256_andnot_pd(mask,mole_frac[572+idx])); 
      }
      {
        mole_frac[715+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[715+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[715+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[715+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[715+idx] = 
          _mm256_add_pd(clamped[715+idx],_mm256_andnot_pd(mask,mole_frac[715+idx])); 
      }
      {
        mole_frac[858+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[858+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[858+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[858+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[858+idx] = 
          _mm256_add_pd(clamped[858+idx],_mm256_andnot_pd(mask,mole_frac[858+idx])); 
      }
      {
        mole_frac[1001+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1001+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1001+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1001+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1001+idx] = 
          _mm256_add_pd(clamped[1001+idx],_mm256_andnot_pd(mask,mole_frac[1001+idx])); 
      }
      {
        mole_frac[1144+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1144+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1144+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1144+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1144+idx] = 
          _mm256_add_pd(clamped[1144+idx],_mm256_andnot_pd(mask,mole_frac[1144+idx])); 
      }
    }
    for (unsigned idx = 0; idx < 143; idx++)
    {
      // D_1_0 and D_0_1
      {
        __m256d val = _mm256_set1_pd(6.496713375703356e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1455910614138868)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.739819455057624)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-12.28690709681149)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[0+idx]); 
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[143+idx]); 
      }
      // D_2_0 and D_0_2
      {
        __m256d val = _mm256_set1_pd(2.809569800526466e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.06524669290450333)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.157124826702022)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-10.6010856877531)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[0+idx]); 
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[286+idx]); 
      }
      // D_3_0 and D_0_3
      {
        __m256d val = _mm256_set1_pd(2.809565777509212e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.06524630059088886)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.157119411146176)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-10.60411408885295)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[0+idx]); 
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[429+idx]); 
      }
      // D_4_0 and D_0_4
      {
        __m256d val = _mm256_set1_pd(0.01788843853924217);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4173868818898205)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.905110086058887)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-17.87212576262405)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[0+idx]); 
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[572+idx]); 
      }
      // D_5_0 and D_0_5
      {
        __m256d val = _mm256_set1_pd(7.265916105045835e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1637786688737705)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.883732506001666)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-11.68686806879253)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[0+idx]); 
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[715+idx]); 
      }
      // D_6_0 and D_0_6
      {
        __m256d val = _mm256_set1_pd(6.504631149451832e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1457630017711661)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.741058929835041)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-12.29066651878132)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[0+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[858+idx]); 
      }
      // D_7_0 and D_0_7
      {
        __m256d val = _mm256_set1_pd(6.512139301292456e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1459260483911689)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.742234306302837)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-12.29422622950706)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[0+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[1001+idx]); 
      }
      // D_8_0 and D_0_8
      {
        __m256d val = _mm256_set1_pd(6.125380882964287e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1373997450024724)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.679286520741738)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-12.17511398899321)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[0+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[1144+idx]); 
      }
      // D_2_1 and D_1_2
      {
        __m256d val = _mm256_set1_pd(9.906580065338089e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2245393002145299)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.350098295256954)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.74365510781629)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[143+idx]); 
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[286+idx]); 
      }
      // D_3_1 and D_1_3
      {
        __m256d val = _mm256_set1_pd(9.830281377761547e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2228586305481379)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.337801387077795)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.73450681298128)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[143+idx]); 
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[429+idx]); 
      }
      // D_4_1 and D_1_4
      {
        __m256d val = _mm256_set1_pd(0.01744937654854085);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4301218167849027)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(5.195865769831297)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-20.36133851212863)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[143+idx]); 
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[572+idx]); 
      }
      // D_5_1 and D_1_5
      {
        __m256d val = _mm256_set1_pd(0.01846372885086072);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4215908806029469)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.86260050714643)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-17.19486136688601)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[143+idx]); 
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[715+idx]); 
      }
      // D_6_1 and D_1_6
      {
        __m256d val = _mm256_set1_pd(0.01102637088401179);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2518700041268918)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.572313137949505)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.79979926721632)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[143+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[858+idx]); 
      }
      // D_7_1 and D_1_7
      {
        __m256d val = _mm256_set1_pd(0.01102928523676343);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2519347056276244)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.572790446851111)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.8082976536738)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[143+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[1001+idx]); 
      }
      // D_8_1 and D_1_8
      {
        __m256d val = _mm256_set1_pd(0.01025036970401586);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2339984755309502)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.434918804839066)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.44410441569896)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[143+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[1144+idx]); 
      }
      // D_3_2 and D_2_3
      {
        __m256d val = _mm256_set1_pd(7.55004835433738e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1706599961410447)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.939415691571989)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-13.31107769087447)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[286+idx]); 
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[429+idx]); 
      }
      // D_4_2 and D_2_4
      {
        __m256d val = _mm256_set1_pd(0.01723315096254202);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4114055748745884)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.935719617888984)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-18.94546370510591)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[286+idx]); 
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[572+idx]); 
      }
      // D_5_2 and D_2_5
      {
        __m256d val = _mm256_set1_pd(0.01442763234249989);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.3275342813724531)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.131924446659196)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.00302482332684)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[286+idx]); 
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[715+idx]); 
      }
      // D_6_2 and D_2_6
      {
        __m256d val = _mm256_set1_pd(9.946182695134111e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2254116731802506)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.356481426172766)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.76398317449067)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[286+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[858+idx]); 
      }
      // D_7_2 and D_2_7
      {
        __m256d val = _mm256_set1_pd(9.985062930487995e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2262681496617851)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.362748415172268)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.783779933709)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[286+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[1001+idx]); 
      }
      // D_8_2 and D_2_8
      {
        __m256d val = _mm256_set1_pd(9.135341978508068e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2069572347136649)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.216429729519678)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.42664211186539)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[286+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[1144+idx]); 
      }
      // D_4_3 and D_3_4
      {
        __m256d val = _mm256_set1_pd(0.01723401922443477);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4113924474910556)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.935376716697689)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-18.96003791122152)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[429+idx]); 
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[572+idx]); 
      }
      // D_5_3 and D_3_5
      {
        __m256d val = _mm256_set1_pd(0.01446662532221593);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.3284011275534711)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.138331707489685)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.02026843093467)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[429+idx]); 
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[715+idx]); 
      }
      // D_6_3 and D_3_6
      {
        __m256d val = _mm256_set1_pd(9.868705553439962e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2237050108610111)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.343993989202011)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.75459154149993)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[429+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[858+idx]); 
      }
      // D_7_3 and D_3_7
      {
        __m256d val = _mm256_set1_pd(9.90658006533804e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2245393002145287)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.350098295256946)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.77420288680858)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[429+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[1001+idx]); 
      }
      // D_8_3 and D_3_8
      {
        __m256d val = _mm256_set1_pd(9.070062339430059e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2055155196241321)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.205852266340754)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.42064795409786)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[429+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[1144+idx]); 
      }
      // D_5_4 and D_4_5
      {
        __m256d val = _mm256_set1_pd(0.01159664778110873);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.3114892309129107)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.416205130060692)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-16.95183706525973)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[572+idx]); 
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[715+idx]); 
      }
      // D_6_4 and D_4_6
      {
        __m256d val = _mm256_set1_pd(0.01672325032731412);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4088530131772313)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.993123969478275)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-19.73436418130672)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[572+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[858+idx]); 
      }
      // D_7_4 and D_4_7
      {
        __m256d val = _mm256_set1_pd(0.01666667781589108);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4077154431461505)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.98569942157695)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-19.72379103406141)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[572+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[1001+idx]); 
      }
      // D_8_4 and D_4_8
      {
        __m256d val = _mm256_set1_pd(0.01761066397930424);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4308686904885884)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(5.176212919443689)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-20.25975125772488)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[572+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[1144+idx]); 
      }
      // D_6_5 and D_5_6
      {
        __m256d val = _mm256_set1_pd(0.01848595495483889);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4220901559794718)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.866330904006294)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-17.20445538403923)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[715+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[858+idx]); 
      }
      // D_7_5 and D_5_7
      {
        __m256d val = _mm256_set1_pd(0.0185072230184242);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4225679016582141)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.869900355766876)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-17.21362814503232)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[715+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[1001+idx]); 
      }
      // D_8_5 and D_5_8
      {
        __m256d val = _mm256_set1_pd(0.01741112215299716);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.3974480768200861)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.677756383394393)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-16.76898168230458)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[715+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[1144+idx]); 
      }
      // D_7_6 and D_6_7
      {
        __m256d val = _mm256_set1_pd(0.01102631087568285);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2518686718836278)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.572303309877288)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.81505301509188)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[858+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[1001+idx]); 
      }
      // D_8_6 and D_6_8
      {
        __m256d val = _mm256_set1_pd(0.01025712117970497);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2341485365071557)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.43602697479444)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.45395375030161)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[858+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[1144+idx]); 
      }
      // D_8_7 and D_7_8
      {
        __m256d val = _mm256_set1_pd(0.01026531645003059);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2343307281563241)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.437372773195496)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.46404657753176)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[1001+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[1144+idx]); 
      }
    }
    for (unsigned idx = 0; idx < 143; idx++)
    {
      __m256d sumxw = _mm256_set1_pd(0.0);
      __m256d wtm = _mm256_set1_pd(0.0);
      __m256d pressure = _mm256_load_pd(pressure_array);
      pressure_array += 4;
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],_mm256_set1_pd(2.01594)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(2.01594)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],_mm256_set1_pd(31.9988)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[143+idx],_mm256_set1_pd(31.9988)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],_mm256_set1_pd(15.9994)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[286+idx],_mm256_set1_pd(15.9994)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],_mm256_set1_pd(17.00737)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[429+idx],_mm256_set1_pd(17.00737)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],_mm256_set1_pd(18.01534)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[572+idx],_mm256_set1_pd(18.01534)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],_mm256_set1_pd(1.00797)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[715+idx],_mm256_set1_pd(1.00797)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],_mm256_set1_pd(33.00677)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[858+idx],_mm256_set1_pd(33.00677)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],_mm256_set1_pd(34.01474)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1001+idx],_mm256_set1_pd(34.01474)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],_mm256_set1_pd(28.0134)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1144+idx],_mm256_set1_pd(28.0134)),wtm); 
      pressure = _mm256_mul_pd(pressure,_mm256_set1_pd(1.41836588544e+06));
      __m256d pfac = _mm256_div_pd(_mm256_set1_pd(1.01325e+06),pressure);
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(2.01594),clamped[0+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[0+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(0*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(31.9988),clamped[143+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[143+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(1*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(15.9994),clamped[286+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[286+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(2*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(17.00737),clamped[429+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[429+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(3*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(18.01534),clamped[572+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[572+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(4*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(1.00797),clamped[715+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[715+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(5*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(33.00677),clamped[858+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[858+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(6*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(34.01474),clamped[1001+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1001+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(7*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(28.0134),clamped[1144+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1144+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(8*spec_stride)+(idx<<2),result);
      }
    }
    remaining_elmts -= 572;
    mass_frac_array += 572;
    diffusion += 572;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 4) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      logt[idx] = _mm256_load_pd(temperature_array);
      temperature_array += 4;
      mole_frac[0+idx] = 
        _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
      mole_frac[143+idx] = 
        _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
      mole_frac[286+idx] = 
        _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
      mole_frac[429+idx] = 
        _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
      mole_frac[572+idx] = 
        _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
      mole_frac[715+idx] = 
        _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
      mole_frac[858+idx] = 
        _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
      mole_frac[1001+idx] = 
        _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
      mole_frac[1144+idx] = 
        _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
      __m256d mixmw = _mm256_load_pd(mixmw_array);
      mixmw_array += 4;
      sumxod[0+idx] = _mm256_set1_pd(0.0);
      sumxod[143+idx] = _mm256_set1_pd(0.0);
      sumxod[286+idx] = _mm256_set1_pd(0.0);
      sumxod[429+idx] = _mm256_set1_pd(0.0);
      sumxod[572+idx] = _mm256_set1_pd(0.0);
      sumxod[715+idx] = _mm256_set1_pd(0.0);
      sumxod[858+idx] = _mm256_set1_pd(0.0);
      sumxod[1001+idx] = _mm256_set1_pd(0.0);
      sumxod[1144+idx] = _mm256_set1_pd(0.0);
      logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(120.0));
      __m128d lower = _mm256_extractf128_pd(logt[idx],0);
      __m128d upper = _mm256_extractf128_pd(logt[idx],1);
      logt[idx] = 
        _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      {
        mole_frac[0+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[0+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[0+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[0+idx] = 
          _mm256_add_pd(clamped[0+idx],_mm256_andnot_pd(mask,mole_frac[0+idx])); 
      }
      {
        mole_frac[143+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[143+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[143+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[143+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[143+idx] = 
          _mm256_add_pd(clamped[143+idx],_mm256_andnot_pd(mask,mole_frac[143+idx])); 
      }
      {
        mole_frac[286+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[286+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[286+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[286+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[286+idx] = 
          _mm256_add_pd(clamped[286+idx],_mm256_andnot_pd(mask,mole_frac[286+idx])); 
      }
      {
        mole_frac[429+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[429+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[429+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[429+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[429+idx] = 
          _mm256_add_pd(clamped[429+idx],_mm256_andnot_pd(mask,mole_frac[429+idx])); 
      }
      {
        mole_frac[572+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[572+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[572+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[572+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[572+idx] = 
          _mm256_add_pd(clamped[572+idx],_mm256_andnot_pd(mask,mole_frac[572+idx])); 
      }
      {
        mole_frac[715+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[715+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[715+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[715+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[715+idx] = 
          _mm256_add_pd(clamped[715+idx],_mm256_andnot_pd(mask,mole_frac[715+idx])); 
      }
      {
        mole_frac[858+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[858+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[858+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[858+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[858+idx] = 
          _mm256_add_pd(clamped[858+idx],_mm256_andnot_pd(mask,mole_frac[858+idx])); 
      }
      {
        mole_frac[1001+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1001+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1001+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1001+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1001+idx] = 
          _mm256_add_pd(clamped[1001+idx],_mm256_andnot_pd(mask,mole_frac[1001+idx])); 
      }
      {
        mole_frac[1144+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1144+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1144+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1144+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1144+idx] = 
          _mm256_add_pd(clamped[1144+idx],_mm256_andnot_pd(mask,mole_frac[1144+idx])); 
      }
    }
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      // D_1_0 and D_0_1
      {
        __m256d val = _mm256_set1_pd(6.496713375703356e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1455910614138868)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.739819455057624)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-12.28690709681149)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[0+idx]); 
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[143+idx]); 
      }
      // D_2_0 and D_0_2
      {
        __m256d val = _mm256_set1_pd(2.809569800526466e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.06524669290450333)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.157124826702022)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-10.6010856877531)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[0+idx]); 
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[286+idx]); 
      }
      // D_3_0 and D_0_3
      {
        __m256d val = _mm256_set1_pd(2.809565777509212e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.06524630059088886)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.157119411146176)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-10.60411408885295)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[0+idx]); 
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[429+idx]); 
      }
      // D_4_0 and D_0_4
      {
        __m256d val = _mm256_set1_pd(0.01788843853924217);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4173868818898205)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.905110086058887)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-17.87212576262405)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[0+idx]); 
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[572+idx]); 
      }
      // D_5_0 and D_0_5
      {
        __m256d val = _mm256_set1_pd(7.265916105045835e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1637786688737705)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.883732506001666)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-11.68686806879253)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[0+idx]); 
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[715+idx]); 
      }
      // D_6_0 and D_0_6
      {
        __m256d val = _mm256_set1_pd(6.504631149451832e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1457630017711661)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.741058929835041)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-12.29066651878132)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[0+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[858+idx]); 
      }
      // D_7_0 and D_0_7
      {
        __m256d val = _mm256_set1_pd(6.512139301292456e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1459260483911689)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.742234306302837)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-12.29422622950706)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[0+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[1001+idx]); 
      }
      // D_8_0 and D_0_8
      {
        __m256d val = _mm256_set1_pd(6.125380882964287e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1373997450024724)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.679286520741738)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-12.17511398899321)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[0+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[0+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],val),sumxod[1144+idx]); 
      }
      // D_2_1 and D_1_2
      {
        __m256d val = _mm256_set1_pd(9.906580065338089e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2245393002145299)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.350098295256954)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.74365510781629)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[143+idx]); 
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[286+idx]); 
      }
      // D_3_1 and D_1_3
      {
        __m256d val = _mm256_set1_pd(9.830281377761547e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2228586305481379)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.337801387077795)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.73450681298128)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[143+idx]); 
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[429+idx]); 
      }
      // D_4_1 and D_1_4
      {
        __m256d val = _mm256_set1_pd(0.01744937654854085);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4301218167849027)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(5.195865769831297)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-20.36133851212863)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[143+idx]); 
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[572+idx]); 
      }
      // D_5_1 and D_1_5
      {
        __m256d val = _mm256_set1_pd(0.01846372885086072);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4215908806029469)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.86260050714643)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-17.19486136688601)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[143+idx]); 
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[715+idx]); 
      }
      // D_6_1 and D_1_6
      {
        __m256d val = _mm256_set1_pd(0.01102637088401179);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2518700041268918)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.572313137949505)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.79979926721632)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[143+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[858+idx]); 
      }
      // D_7_1 and D_1_7
      {
        __m256d val = _mm256_set1_pd(0.01102928523676343);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2519347056276244)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.572790446851111)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.8082976536738)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[143+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[1001+idx]); 
      }
      // D_8_1 and D_1_8
      {
        __m256d val = _mm256_set1_pd(0.01025036970401586);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2339984755309502)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.434918804839066)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.44410441569896)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[143+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[143+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],val),sumxod[1144+idx]); 
      }
      // D_3_2 and D_2_3
      {
        __m256d val = _mm256_set1_pd(7.55004835433738e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.1706599961410447)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(2.939415691571989)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-13.31107769087447)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[286+idx]); 
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[429+idx]); 
      }
      // D_4_2 and D_2_4
      {
        __m256d val = _mm256_set1_pd(0.01723315096254202);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4114055748745884)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.935719617888984)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-18.94546370510591)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[286+idx]); 
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[572+idx]); 
      }
      // D_5_2 and D_2_5
      {
        __m256d val = _mm256_set1_pd(0.01442763234249989);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.3275342813724531)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.131924446659196)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.00302482332684)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[286+idx]); 
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[715+idx]); 
      }
      // D_6_2 and D_2_6
      {
        __m256d val = _mm256_set1_pd(9.946182695134111e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2254116731802506)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.356481426172766)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.76398317449067)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[286+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[858+idx]); 
      }
      // D_7_2 and D_2_7
      {
        __m256d val = _mm256_set1_pd(9.985062930487995e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2262681496617851)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.362748415172268)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.783779933709)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[286+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[1001+idx]); 
      }
      // D_8_2 and D_2_8
      {
        __m256d val = _mm256_set1_pd(9.135341978508068e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2069572347136649)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.216429729519678)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.42664211186539)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[286+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[286+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],val),sumxod[1144+idx]); 
      }
      // D_4_3 and D_3_4
      {
        __m256d val = _mm256_set1_pd(0.01723401922443477);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4113924474910556)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.935376716697689)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-18.96003791122152)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[429+idx]); 
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[572+idx]); 
      }
      // D_5_3 and D_3_5
      {
        __m256d val = _mm256_set1_pd(0.01446662532221593);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.3284011275534711)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.138331707489685)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.02026843093467)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[429+idx]); 
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[715+idx]); 
      }
      // D_6_3 and D_3_6
      {
        __m256d val = _mm256_set1_pd(9.868705553439962e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2237050108610111)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.343993989202011)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.75459154149993)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[429+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[858+idx]); 
      }
      // D_7_3 and D_3_7
      {
        __m256d val = _mm256_set1_pd(9.90658006533804e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2245393002145287)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.350098295256946)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.77420288680858)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[429+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[1001+idx]); 
      }
      // D_8_3 and D_3_8
      {
        __m256d val = _mm256_set1_pd(9.070062339430059e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2055155196241321)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.205852266340754)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-14.42064795409786)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[429+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[429+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],val),sumxod[1144+idx]); 
      }
      // D_5_4 and D_4_5
      {
        __m256d val = _mm256_set1_pd(0.01159664778110873);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.3114892309129107)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.416205130060692)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-16.95183706525973)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[572+idx]); 
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[715+idx]); 
      }
      // D_6_4 and D_4_6
      {
        __m256d val = _mm256_set1_pd(0.01672325032731412);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4088530131772313)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.993123969478275)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-19.73436418130672)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[572+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[858+idx]); 
      }
      // D_7_4 and D_4_7
      {
        __m256d val = _mm256_set1_pd(0.01666667781589108);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4077154431461505)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.98569942157695)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-19.72379103406141)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[572+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[1001+idx]); 
      }
      // D_8_4 and D_4_8
      {
        __m256d val = _mm256_set1_pd(0.01761066397930424);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4308686904885884)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(5.176212919443689)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-20.25975125772488)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[572+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[572+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],val),sumxod[1144+idx]); 
      }
      // D_6_5 and D_5_6
      {
        __m256d val = _mm256_set1_pd(0.01848595495483889);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4220901559794718)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.866330904006294)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-17.20445538403923)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[715+idx]); 
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[858+idx]); 
      }
      // D_7_5 and D_5_7
      {
        __m256d val = _mm256_set1_pd(0.0185072230184242);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.4225679016582141)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.869900355766876)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-17.21362814503232)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[715+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[1001+idx]); 
      }
      // D_8_5 and D_5_8
      {
        __m256d val = _mm256_set1_pd(0.01741112215299716);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.3974480768200861)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(4.677756383394393)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-16.76898168230458)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[715+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[715+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],val),sumxod[1144+idx]); 
      }
      // D_7_6 and D_6_7
      {
        __m256d val = _mm256_set1_pd(0.01102631087568285);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2518686718836278)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.572303309877288)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.81505301509188)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[858+idx]); 
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[1001+idx]); 
      }
      // D_8_6 and D_6_8
      {
        __m256d val = _mm256_set1_pd(0.01025712117970497);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2341485365071557)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.43602697479444)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.45395375030161)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[858+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[858+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],val),sumxod[1144+idx]); 
      }
      // D_8_7 and D_7_8
      {
        __m256d val = _mm256_set1_pd(0.01026531645003059);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-0.2343307281563241)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(3.437372773195496)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd(-15.46404657753176)); 
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[1001+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],val),sumxod[1001+idx]); 
        sumxod[1144+idx] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],val),sumxod[1144+idx]); 
      }
    }
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d sumxw = _mm256_set1_pd(0.0);
      __m256d wtm = _mm256_set1_pd(0.0);
      __m256d pressure = _mm256_load_pd(pressure_array);
      pressure_array += 4;
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],_mm256_set1_pd(2.01594)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(2.01594)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[143+idx],_mm256_set1_pd(31.9988)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[143+idx],_mm256_set1_pd(31.9988)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[286+idx],_mm256_set1_pd(15.9994)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[286+idx],_mm256_set1_pd(15.9994)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[429+idx],_mm256_set1_pd(17.00737)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[429+idx],_mm256_set1_pd(17.00737)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[572+idx],_mm256_set1_pd(18.01534)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[572+idx],_mm256_set1_pd(18.01534)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[715+idx],_mm256_set1_pd(1.00797)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[715+idx],_mm256_set1_pd(1.00797)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[858+idx],_mm256_set1_pd(33.00677)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[858+idx],_mm256_set1_pd(33.00677)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1001+idx],_mm256_set1_pd(34.01474)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1001+idx],_mm256_set1_pd(34.01474)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1144+idx],_mm256_set1_pd(28.0134)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1144+idx],_mm256_set1_pd(28.0134)),wtm); 
      pressure = _mm256_mul_pd(pressure,_mm256_set1_pd(1.41836588544e+06));
      __m256d pfac = _mm256_div_pd(_mm256_set1_pd(1.01325e+06),pressure);
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(2.01594),clamped[0+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[0+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(0*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(31.9988),clamped[143+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[143+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(1*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(15.9994),clamped[286+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[286+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(2*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(17.00737),clamped[429+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[429+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(3*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(18.01534),clamped[572+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[572+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(4*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(1.00797),clamped[715+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[715+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(5*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(33.00677),clamped[858+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[858+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(6*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(34.01474),clamped[1001+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1001+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(7*spec_stride)+(idx<<2),result);
      }
      {
        __m256d result = 
          _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(28.0134),clamped[1144+idx]))); 
        result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1144+idx]));
        result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
        _mm256_store_pd(diffusion+(8*spec_stride)+(idx<<2),result);
      }
    }
  }
  free(mole_frac);
  free(clamped);
  free(logt);
  free(sumxod);
}

void avx_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *mixmw_array, const int num_elmts, const int spec_stride, double 
  *thermal) 
{
  __m256d *mole_frac = (__m256d*)malloc(214*9*sizeof(__m256d));
  __m256d *temperature = (__m256d*)malloc(214*sizeof(__m256d));
  __m256d *thermal_vec = (__m256d*)malloc(214*9*sizeof(__m256d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 856)
  {
    for (unsigned idx = 0; idx < 214; idx++)
    {
      temperature[idx] = _mm256_load_pd(temperature_array);
      temperature_array += 4;
      __m256d mixmw = _mm256_load_pd(mixmw_array);
      mixmw_array += 4;
      mole_frac[0+idx] = 
        _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
      mole_frac[214+idx] = 
        _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
      mole_frac[428+idx] = 
        _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
      mole_frac[642+idx] = 
        _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
      mole_frac[856+idx] = 
        _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
      mole_frac[1070+idx] = 
        _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
      mole_frac[1284+idx] = 
        _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
      mole_frac[1498+idx] = 
        _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
      mole_frac[1712+idx] = 
        _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
      temperature[idx] = _mm256_mul_pd(temperature[idx],_mm256_set1_pd(120.0));
      mole_frac[0+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[0+idx] = _mm256_set1_pd(0.0);
      mole_frac[214+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[214+idx] = _mm256_set1_pd(0.0);
      mole_frac[428+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[428+idx] = _mm256_set1_pd(0.0);
      mole_frac[642+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[642+idx] = _mm256_set1_pd(0.0);
      mole_frac[856+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[856+idx] = _mm256_set1_pd(0.0);
      mole_frac[1070+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1070+idx] = _mm256_set1_pd(0.0);
      mole_frac[1284+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1284+idx] = _mm256_set1_pd(0.0);
      mole_frac[1498+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1498+idx] = _mm256_set1_pd(0.0);
      mole_frac[1712+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1712+idx] = _mm256_set1_pd(0.0);
    }
    for (unsigned idx = 0; idx < 214; idx++)
    {
      // Light species H2
      {
        // Interaction with O2
        {
          __m256d val = _mm256_set1_pd(-6.863234368775381e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.847680615982582e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.117708177718642e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4427390838930854)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[214+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O
        {
          __m256d val = _mm256_set1_pd(-1.144144427154856e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.960219633369755e-09)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-1.09738398546134e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4155833371210523)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[428+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with OH
        {
          __m256d val = _mm256_set1_pd(-1.161624181476171e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.020722192842837e-09)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-1.114149353544903e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4219324434580686)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[642+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O
        {
          __m256d val = _mm256_set1_pd(-3.633898010835058e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.553727569929838e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-5.615615459953129e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.06020313090654318)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[856+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H
        {
          __m256d val = _mm256_set1_pd(4.870919137829873e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.93412470121339e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.464040220781074e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1525347421818075)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1070+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HO2
        {
          __m256d val = _mm256_set1_pd(-6.889796396702935e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.862571874897331e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.145255067892884e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4444525687136664)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1284+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O2
        {
          __m256d val = _mm256_set1_pd(-6.914872261877499e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.876629958182396e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.171260691630346e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4460701829431514)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1498+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with N2
        {
          __m256d val = _mm256_set1_pd(-4.903062174600689e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.630234415935386e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.946971739853057e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4452619662404715)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1712+idx]),thermal_vec[0+idx]); 
        }
      }
      // Light species H
      {
        // Interaction with H2
        {
          __m256d val = _mm256_set1_pd(-4.870919137829873e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.93412470121339e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-5.464040220781074e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1525347421818075)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[0+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with O2
        {
          __m256d val = _mm256_set1_pd(-3.464704359017016e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.329279443546141e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.801642880313133e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2204828425102966)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[214+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with O
        {
          __m256d val = _mm256_set1_pd(-2.753212482584995e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.80744751529337e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.615550927540219e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2700101503763489)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[428+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with OH
        {
          __m256d val = _mm256_set1_pd(-2.77392721913982e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.821046465375428e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.642753762569583e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2720416641688922)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[642+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with H2O
        {
          __m256d val = _mm256_set1_pd(-4.029595022301052e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.065500034524752e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.665588101246596e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.141883744068593)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[856+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with HO2
        {
          __m256d val = _mm256_set1_pd(-3.471383045051115e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.333769444561275e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.810898696083394e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2209078529955923)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[1284+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with H2O2
        {
          __m256d val = _mm256_set1_pd(-3.477677299659906e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.338000996911347e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.81962173842625e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2213083994791669)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[1498+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with N2
        {
          __m256d val = _mm256_set1_pd(-3.269585062280141e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.181738735658955e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.453434507868586e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2407444208104045)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[1712+idx]),thermal_vec[1070+idx]); 
        }
      }
    }
    for (unsigned idx = 0; idx < 214; idx++)
    {
      _mm256_store_pd(thermal+(0*spec_stride)+(idx<<2),thermal_vec[0+idx]);
      _mm256_store_pd(thermal+(1*spec_stride)+(idx<<2),thermal_vec[214+idx]);
      _mm256_store_pd(thermal+(2*spec_stride)+(idx<<2),thermal_vec[428+idx]);
      _mm256_store_pd(thermal+(3*spec_stride)+(idx<<2),thermal_vec[642+idx]);
      _mm256_store_pd(thermal+(4*spec_stride)+(idx<<2),thermal_vec[856+idx]);
      _mm256_store_pd(thermal+(5*spec_stride)+(idx<<2),thermal_vec[1070+idx]);
      _mm256_store_pd(thermal+(6*spec_stride)+(idx<<2),thermal_vec[1284+idx]);
      _mm256_store_pd(thermal+(7*spec_stride)+(idx<<2),thermal_vec[1498+idx]);
      _mm256_store_pd(thermal+(8*spec_stride)+(idx<<2),thermal_vec[1712+idx]);
    }
    remaining_elmts -= 856;
    mass_frac_array += 856;
    thermal += 856;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 4) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      temperature[idx] = _mm256_load_pd(temperature_array);
      temperature_array += 4;
      __m256d mixmw = _mm256_load_pd(mixmw_array);
      mixmw_array += 4;
      mole_frac[0+idx] = 
        _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
      mole_frac[214+idx] = 
        _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
      mole_frac[428+idx] = 
        _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
      mole_frac[642+idx] = 
        _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
      mole_frac[856+idx] = 
        _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
      mole_frac[1070+idx] = 
        _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
      mole_frac[1284+idx] = 
        _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
      mole_frac[1498+idx] = 
        _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
      mole_frac[1712+idx] = 
        _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
      temperature[idx] = _mm256_mul_pd(temperature[idx],_mm256_set1_pd(120.0));
      mole_frac[0+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[0+idx] = _mm256_set1_pd(0.0);
      mole_frac[214+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[214+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[214+idx] = _mm256_set1_pd(0.0);
      mole_frac[428+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[428+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[428+idx] = _mm256_set1_pd(0.0);
      mole_frac[642+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[642+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[642+idx] = _mm256_set1_pd(0.0);
      mole_frac[856+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[856+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[856+idx] = _mm256_set1_pd(0.0);
      mole_frac[1070+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1070+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1070+idx] = _mm256_set1_pd(0.0);
      mole_frac[1284+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1284+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1284+idx] = _mm256_set1_pd(0.0);
      mole_frac[1498+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1498+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1498+idx] = _mm256_set1_pd(0.0);
      mole_frac[1712+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1712+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1712+idx] = _mm256_set1_pd(0.0);
    }
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      // Light species H2
      {
        // Interaction with O2
        {
          __m256d val = _mm256_set1_pd(-6.863234368775381e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.847680615982582e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.117708177718642e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4427390838930854)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[214+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O
        {
          __m256d val = _mm256_set1_pd(-1.144144427154856e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.960219633369755e-09)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-1.09738398546134e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4155833371210523)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[428+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with OH
        {
          __m256d val = _mm256_set1_pd(-1.161624181476171e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.020722192842837e-09)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-1.114149353544903e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4219324434580686)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[642+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O
        {
          __m256d val = _mm256_set1_pd(-3.633898010835058e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.553727569929838e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-5.615615459953129e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.06020313090654318)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[856+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H
        {
          __m256d val = _mm256_set1_pd(4.870919137829873e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.93412470121339e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.464040220781074e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1525347421818075)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1070+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HO2
        {
          __m256d val = _mm256_set1_pd(-6.889796396702935e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.862571874897331e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.145255067892884e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4444525687136664)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1284+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O2
        {
          __m256d val = _mm256_set1_pd(-6.914872261877499e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.876629958182396e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.171260691630346e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4460701829431514)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1498+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with N2
        {
          __m256d val = _mm256_set1_pd(-4.903062174600689e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.630234415935386e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.946971739853057e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.4452619662404715)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1712+idx]),thermal_vec[0+idx]); 
        }
      }
      // Light species H
      {
        // Interaction with H2
        {
          __m256d val = _mm256_set1_pd(-4.870919137829873e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.93412470121339e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-5.464040220781074e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1525347421818075)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[0+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with O2
        {
          __m256d val = _mm256_set1_pd(-3.464704359017016e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.329279443546141e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.801642880313133e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2204828425102966)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[214+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with O
        {
          __m256d val = _mm256_set1_pd(-2.753212482584995e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.80744751529337e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.615550927540219e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2700101503763489)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[428+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with OH
        {
          __m256d val = _mm256_set1_pd(-2.77392721913982e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.821046465375428e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.642753762569583e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2720416641688922)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[642+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with H2O
        {
          __m256d val = _mm256_set1_pd(-4.029595022301052e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.065500034524752e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.665588101246596e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.141883744068593)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[856+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with HO2
        {
          __m256d val = _mm256_set1_pd(-3.471383045051115e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.333769444561275e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.810898696083394e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2209078529955923)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[1284+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with H2O2
        {
          __m256d val = _mm256_set1_pd(-3.477677299659906e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.338000996911347e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.81962173842625e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2213083994791669)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[1498+idx]),thermal_vec[1070+idx]); 
        }
        // Interaction with N2
        {
          __m256d val = _mm256_set1_pd(-3.269585062280141e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(2.181738735658955e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.453434507868586e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.2407444208104045)); 
          thermal_vec[1070+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[1070+idx],mole_frac[1712+idx]),thermal_vec[1070+idx]); 
        }
      }
    }
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      _mm256_store_pd(thermal+(0*spec_stride)+(idx<<2),thermal_vec[0+idx]);
      _mm256_store_pd(thermal+(1*spec_stride)+(idx<<2),thermal_vec[214+idx]);
      _mm256_store_pd(thermal+(2*spec_stride)+(idx<<2),thermal_vec[428+idx]);
      _mm256_store_pd(thermal+(3*spec_stride)+(idx<<2),thermal_vec[642+idx]);
      _mm256_store_pd(thermal+(4*spec_stride)+(idx<<2),thermal_vec[856+idx]);
      _mm256_store_pd(thermal+(5*spec_stride)+(idx<<2),thermal_vec[1070+idx]);
      _mm256_store_pd(thermal+(6*spec_stride)+(idx<<2),thermal_vec[1284+idx]);
      _mm256_store_pd(thermal+(7*spec_stride)+(idx<<2),thermal_vec[1498+idx]);
      _mm256_store_pd(thermal+(8*spec_stride)+(idx<<2),thermal_vec[1712+idx]);
    }
  }
  free(mole_frac);
  free(temperature);
  free(thermal_vec);
}

