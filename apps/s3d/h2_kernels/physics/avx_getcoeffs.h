
#ifndef __AVX_CPU_GET_COEFFS__
#define __AVX_CPU_GET_COEFFS__

void avx_conductivity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *lambda); 
void avx_viscosity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *viscosity); 
void avx_diffusion(const double *temperature_array, const double 
  *pressure_array, const double *mass_frac_array, const double *mixmw_array, 
  const int num_elmts, const int spec_stride, double *diffusion); 
void avx_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *mixmw_array, const int num_elmts, const int spec_stride, double 
  *thermal); 

#endif // __AVX_CPU_GET_COEFFS__

