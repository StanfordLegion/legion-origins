
#define THREADS_PER_BLOCK 128

__constant__ double recip_molecular_masses[9] = {0.4960465093207139, 
  0.03125117191894696, 0.06250234383789392, 0.05879803873262004, 
  0.05550825019122593, 0.9920930186414277, 0.03029681486555637, 
  0.02939901936631002, 0.03569720205330306}; 

__global__ void
__launch_bounds__(THREADS_PER_BLOCK,2)
gpu_conductivity(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *conductivity) 
{
  {
    const int offset = (blockIdx.x*blockDim.x+threadIdx.x);
    temperature_array += offset;
    mass_frac_array += offset;
    avmolwt_array += offset;
    conductivity += offset;
  }
  // Load the temperatures
  double temperature;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  // Load the average mole weight
  double avmolwt;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(avmolwt) : 
    "l"(avmolwt_array) : "memory"); 
  temperature *= 120.0;
  // Compute log(t)
  double logt = log(temperature);
  double sum = 0.0;
  double sumr = 0.0;
  // Species H2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+0*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[0] * 1e3 * avmolwt;
    double val = __fma_rn(-8.971812440098445e-03,logt,0.2434840522735603);
    val = __fma_rn(val,logt,-1.3149816638112);
    val = __fma_rn(val,logt,11.09465938678581);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+1*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[1] * 1e3 * avmolwt;
    double val = __fma_rn(0.01344785152870045,logt,-0.3099605229855532);
    val = __fma_rn(val,logt,3.151649687562649);
    val = __fma_rn(val,logt,-2.51386921584388);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species O
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+2*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[2] * 1e3 * avmolwt;
    double val = __fma_rn(6.908732177853375e-03,logt,-0.1549096547430882);
    val = __fma_rn(val,logt,1.801392496839076);
    val = __fma_rn(val,logt,1.969675461941004);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species OH
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+3*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[3] * 1e3 * avmolwt;
    double val = __fma_rn(-0.02977135865933274,logt,0.6631509208736623);
    val = __fma_rn(val,logt,-4.103241187134545);
    val = __fma_rn(val,logt,16.05448695051902);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2O
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+4*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[4] * 1e3 * avmolwt;
    double val = __fma_rn(-0.07286086542855576,logt,1.459337125398943);
    val = __fma_rn(val,logt,-8.451500192314084);
    val = __fma_rn(val,logt,22.12505312748445);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+5*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[5] * 1e3 * avmolwt;
    double val = __fma_rn(0.01585988965532566,logt,-0.3631110078153958);
    val = __fma_rn(val,logt,3.416401604464596);
    val = __fma_rn(val,logt,-0.3253871703970487);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HO2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+6*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[6] * 1e3 * avmolwt;
    double val = __fma_rn(4.072437253728964e-04,logt,-0.05282466276443485);
    val = __fma_rn(val,logt,1.591058710589892);
    val = __fma_rn(val,logt,0.5546382791949179);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+7*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[7] * 1e3 * avmolwt;
    double val = __fma_rn(-6.382550767548813e-03,logt,0.05716835124727309);
    val = __fma_rn(val,logt,1.062275726777442);
    val = __fma_rn(val,logt,1.486257799258664);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species N2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+8*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[8] * 1e3 * avmolwt;
    double val = __fma_rn(-0.02750102626098917,logt,0.5546581094878709);
    val = __fma_rn(val,logt,-2.911524100094121);
    val = __fma_rn(val,logt,11.54289501420278);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Write out the coefficients
  double result = 0.5 * (sum + (1.0/sumr));
  result *= 5.403325855130351e-09;
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(conductivity), "d"(result) : 
    "memory"); 
}

