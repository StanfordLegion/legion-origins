
#ifndef __SSE_CPU_GET_COEFFS__
#define __SSE_CPU_GET_COEFFS__

void sse_conductivity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *lambda); 
void sse_viscosity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *viscosity); 
void sse_diffusion(const double *temperature_array, const double 
  *pressure_array, const double *mass_frac_array, const double *mixmw_array, 
  const int num_elmts, const int spec_stride, double *diffusion); 
void sse_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *mixmw_array, const int num_elmts, const int spec_stride, double 
  *thermal); 

#endif // __SSE_CPU_GET_COEFFS__

