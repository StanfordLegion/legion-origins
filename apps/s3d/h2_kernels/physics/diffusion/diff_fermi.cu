
__constant__ double recip_molecular_masses[9] = {0.4960465093207139, 
  0.03125117191894696, 0.06250234383789392, 0.05879803873262004, 
  0.05550825019122593, 0.9920930186414277, 0.03029681486555637, 
  0.02939901936631002, 0.03569720205330306}; 

__global__ void
__launch_bounds__(128,8)
gpu_diffusion(const double *temperature_array, const double *pressure_array, 
  const double *mass_frac_array, const double *avmolwt_array, const int 
  slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX in number 
  of doubles*/, const int total_steps/*NZ in number of doubles*/, const int 
  spec_stride/*NX*NY*NZ in number of doubles*/, double *diffusion) 
{
  {
    const int offset = (blockIdx.x*blockDim.x+threadIdx.x);
    temperature_array += offset;
    pressure_array += offset;
    mass_frac_array += offset;
    avmolwt_array += offset;
    diffusion += offset;
  }
  double temperature;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  double pressure;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(pressure) : 
    "l"(pressure_array) : "memory"); 
  double avmolwt;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(avmolwt) : "l"(avmolwt_array) 
    : "memory"); 
  double mole_frac[9];
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac[0]) : 
    "l"(mass_frac_array+0*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac[1]) : 
    "l"(mass_frac_array+1*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac[2]) : 
    "l"(mass_frac_array+2*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac[3]) : 
    "l"(mass_frac_array+3*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac[4]) : 
    "l"(mass_frac_array+4*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac[5]) : 
    "l"(mass_frac_array+5*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac[6]) : 
    "l"(mass_frac_array+6*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac[7]) : 
    "l"(mass_frac_array+7*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac[8]) : 
    "l"(mass_frac_array+8*spec_stride) : "memory"); 
  for (int i = 0; i < 9; i++)
  {
    mole_frac[i] *= recip_molecular_masses[i] * 1e3 * avmolwt;
  }
  temperature *= 120.0;
  pressure *= 1.41836588544e+06;
  double clamped[9];
  for (int i = 0; i < 9; i++)
  clamped[i] = ((mole_frac[i] > 9.999999999999999e-21) ? mole_frac[i] : 
    9.999999999999999e-21); 
  double sumxod[9];
  for (int i = 0; i < 9; i++)
    sumxod[i] = 0.0;
  double logt = log(temperature);
  // D_1_0 and D_0_1
  {
    double val = 6.496713375703356e-03;
    val = val * logt + -0.1455910614138868;
    val = val * logt + 2.739819455057624;
    val = val * logt + -12.28690709681149;
    val = exp(-val);
    sumxod[0] += (clamped[1]*val);
    sumxod[1] += (clamped[0]*val);
  }
  // D_2_0 and D_0_2
  {
    double val = 2.809569800526466e-03;
    val = val * logt + -0.06524669290450333;
    val = val * logt + 2.157124826702022;
    val = val * logt + -10.6010856877531;
    val = exp(-val);
    sumxod[0] += (clamped[2]*val);
    sumxod[2] += (clamped[0]*val);
  }
  // D_3_0 and D_0_3
  {
    double val = 2.809565777509212e-03;
    val = val * logt + -0.06524630059088886;
    val = val * logt + 2.157119411146176;
    val = val * logt + -10.60411408885295;
    val = exp(-val);
    sumxod[0] += (clamped[3]*val);
    sumxod[3] += (clamped[0]*val);
  }
  // D_4_0 and D_0_4
  {
    double val = 0.01788843853924217;
    val = val * logt + -0.4173868818898205;
    val = val * logt + 4.905110086058887;
    val = val * logt + -17.87212576262405;
    val = exp(-val);
    sumxod[0] += (clamped[4]*val);
    sumxod[4] += (clamped[0]*val);
  }
  // D_5_0 and D_0_5
  {
    double val = 7.265916105045835e-03;
    val = val * logt + -0.1637786688737705;
    val = val * logt + 2.883732506001666;
    val = val * logt + -11.68686806879253;
    val = exp(-val);
    sumxod[0] += (clamped[5]*val);
    sumxod[5] += (clamped[0]*val);
  }
  // D_6_0 and D_0_6
  {
    double val = 6.504631149451832e-03;
    val = val * logt + -0.1457630017711661;
    val = val * logt + 2.741058929835041;
    val = val * logt + -12.29066651878132;
    val = exp(-val);
    sumxod[0] += (clamped[6]*val);
    sumxod[6] += (clamped[0]*val);
  }
  // D_7_0 and D_0_7
  {
    double val = 6.512139301292456e-03;
    val = val * logt + -0.1459260483911689;
    val = val * logt + 2.742234306302837;
    val = val * logt + -12.29422622950706;
    val = exp(-val);
    sumxod[0] += (clamped[7]*val);
    sumxod[7] += (clamped[0]*val);
  }
  // D_8_0 and D_0_8
  {
    double val = 6.125380882964287e-03;
    val = val * logt + -0.1373997450024724;
    val = val * logt + 2.679286520741738;
    val = val * logt + -12.17511398899321;
    val = exp(-val);
    sumxod[0] += (clamped[8]*val);
    sumxod[8] += (clamped[0]*val);
  }
  // D_2_1 and D_1_2
  {
    double val = 9.906580065338089e-03;
    val = val * logt + -0.2245393002145299;
    val = val * logt + 3.350098295256954;
    val = val * logt + -14.74365510781629;
    val = exp(-val);
    sumxod[1] += (clamped[2]*val);
    sumxod[2] += (clamped[1]*val);
  }
  // D_3_1 and D_1_3
  {
    double val = 9.830281377761547e-03;
    val = val * logt + -0.2228586305481379;
    val = val * logt + 3.337801387077795;
    val = val * logt + -14.73450681298128;
    val = exp(-val);
    sumxod[1] += (clamped[3]*val);
    sumxod[3] += (clamped[1]*val);
  }
  // D_4_1 and D_1_4
  {
    double val = 0.01744937654854085;
    val = val * logt + -0.4301218167849027;
    val = val * logt + 5.195865769831297;
    val = val * logt + -20.36133851212863;
    val = exp(-val);
    sumxod[1] += (clamped[4]*val);
    sumxod[4] += (clamped[1]*val);
  }
  // D_5_1 and D_1_5
  {
    double val = 0.01846372885086072;
    val = val * logt + -0.4215908806029469;
    val = val * logt + 4.86260050714643;
    val = val * logt + -17.19486136688601;
    val = exp(-val);
    sumxod[1] += (clamped[5]*val);
    sumxod[5] += (clamped[1]*val);
  }
  // D_6_1 and D_1_6
  {
    double val = 0.01102637088401179;
    val = val * logt + -0.2518700041268918;
    val = val * logt + 3.572313137949505;
    val = val * logt + -15.79979926721632;
    val = exp(-val);
    sumxod[1] += (clamped[6]*val);
    sumxod[6] += (clamped[1]*val);
  }
  // D_7_1 and D_1_7
  {
    double val = 0.01102928523676343;
    val = val * logt + -0.2519347056276244;
    val = val * logt + 3.572790446851111;
    val = val * logt + -15.8082976536738;
    val = exp(-val);
    sumxod[1] += (clamped[7]*val);
    sumxod[7] += (clamped[1]*val);
  }
  // D_8_1 and D_1_8
  {
    double val = 0.01025036970401586;
    val = val * logt + -0.2339984755309502;
    val = val * logt + 3.434918804839066;
    val = val * logt + -15.44410441569896;
    val = exp(-val);
    sumxod[1] += (clamped[8]*val);
    sumxod[8] += (clamped[1]*val);
  }
  // D_3_2 and D_2_3
  {
    double val = 7.55004835433738e-03;
    val = val * logt + -0.1706599961410447;
    val = val * logt + 2.939415691571989;
    val = val * logt + -13.31107769087447;
    val = exp(-val);
    sumxod[2] += (clamped[3]*val);
    sumxod[3] += (clamped[2]*val);
  }
  // D_4_2 and D_2_4
  {
    double val = 0.01723315096254202;
    val = val * logt + -0.4114055748745884;
    val = val * logt + 4.935719617888984;
    val = val * logt + -18.94546370510591;
    val = exp(-val);
    sumxod[2] += (clamped[4]*val);
    sumxod[4] += (clamped[2]*val);
  }
  // D_5_2 and D_2_5
  {
    double val = 0.01442763234249989;
    val = val * logt + -0.3275342813724531;
    val = val * logt + 4.131924446659196;
    val = val * logt + -15.00302482332684;
    val = exp(-val);
    sumxod[2] += (clamped[5]*val);
    sumxod[5] += (clamped[2]*val);
  }
  // D_6_2 and D_2_6
  {
    double val = 9.946182695134111e-03;
    val = val * logt + -0.2254116731802506;
    val = val * logt + 3.356481426172766;
    val = val * logt + -14.76398317449067;
    val = exp(-val);
    sumxod[2] += (clamped[6]*val);
    sumxod[6] += (clamped[2]*val);
  }
  // D_7_2 and D_2_7
  {
    double val = 9.985062930487995e-03;
    val = val * logt + -0.2262681496617851;
    val = val * logt + 3.362748415172268;
    val = val * logt + -14.783779933709;
    val = exp(-val);
    sumxod[2] += (clamped[7]*val);
    sumxod[7] += (clamped[2]*val);
  }
  // D_8_2 and D_2_8
  {
    double val = 9.135341978508068e-03;
    val = val * logt + -0.2069572347136649;
    val = val * logt + 3.216429729519678;
    val = val * logt + -14.42664211186539;
    val = exp(-val);
    sumxod[2] += (clamped[8]*val);
    sumxod[8] += (clamped[2]*val);
  }
  // D_4_3 and D_3_4
  {
    double val = 0.01723401922443477;
    val = val * logt + -0.4113924474910556;
    val = val * logt + 4.935376716697689;
    val = val * logt + -18.96003791122152;
    val = exp(-val);
    sumxod[3] += (clamped[4]*val);
    sumxod[4] += (clamped[3]*val);
  }
  // D_5_3 and D_3_5
  {
    double val = 0.01446662532221593;
    val = val * logt + -0.3284011275534711;
    val = val * logt + 4.138331707489685;
    val = val * logt + -15.02026843093467;
    val = exp(-val);
    sumxod[3] += (clamped[5]*val);
    sumxod[5] += (clamped[3]*val);
  }
  // D_6_3 and D_3_6
  {
    double val = 9.868705553439962e-03;
    val = val * logt + -0.2237050108610111;
    val = val * logt + 3.343993989202011;
    val = val * logt + -14.75459154149993;
    val = exp(-val);
    sumxod[3] += (clamped[6]*val);
    sumxod[6] += (clamped[3]*val);
  }
  // D_7_3 and D_3_7
  {
    double val = 9.90658006533804e-03;
    val = val * logt + -0.2245393002145287;
    val = val * logt + 3.350098295256946;
    val = val * logt + -14.77420288680858;
    val = exp(-val);
    sumxod[3] += (clamped[7]*val);
    sumxod[7] += (clamped[3]*val);
  }
  // D_8_3 and D_3_8
  {
    double val = 9.070062339430059e-03;
    val = val * logt + -0.2055155196241321;
    val = val * logt + 3.205852266340754;
    val = val * logt + -14.42064795409786;
    val = exp(-val);
    sumxod[3] += (clamped[8]*val);
    sumxod[8] += (clamped[3]*val);
  }
  // D_5_4 and D_4_5
  {
    double val = 0.01159664778110873;
    val = val * logt + -0.3114892309129107;
    val = val * logt + 4.416205130060692;
    val = val * logt + -16.95183706525973;
    val = exp(-val);
    sumxod[4] += (clamped[5]*val);
    sumxod[5] += (clamped[4]*val);
  }
  // D_6_4 and D_4_6
  {
    double val = 0.01672325032731412;
    val = val * logt + -0.4088530131772313;
    val = val * logt + 4.993123969478275;
    val = val * logt + -19.73436418130672;
    val = exp(-val);
    sumxod[4] += (clamped[6]*val);
    sumxod[6] += (clamped[4]*val);
  }
  // D_7_4 and D_4_7
  {
    double val = 0.01666667781589108;
    val = val * logt + -0.4077154431461505;
    val = val * logt + 4.98569942157695;
    val = val * logt + -19.72379103406141;
    val = exp(-val);
    sumxod[4] += (clamped[7]*val);
    sumxod[7] += (clamped[4]*val);
  }
  // D_8_4 and D_4_8
  {
    double val = 0.01761066397930424;
    val = val * logt + -0.4308686904885884;
    val = val * logt + 5.176212919443689;
    val = val * logt + -20.25975125772488;
    val = exp(-val);
    sumxod[4] += (clamped[8]*val);
    sumxod[8] += (clamped[4]*val);
  }
  // D_6_5 and D_5_6
  {
    double val = 0.01848595495483889;
    val = val * logt + -0.4220901559794718;
    val = val * logt + 4.866330904006294;
    val = val * logt + -17.20445538403923;
    val = exp(-val);
    sumxod[5] += (clamped[6]*val);
    sumxod[6] += (clamped[5]*val);
  }
  // D_7_5 and D_5_7
  {
    double val = 0.0185072230184242;
    val = val * logt + -0.4225679016582141;
    val = val * logt + 4.869900355766876;
    val = val * logt + -17.21362814503232;
    val = exp(-val);
    sumxod[5] += (clamped[7]*val);
    sumxod[7] += (clamped[5]*val);
  }
  // D_8_5 and D_5_8
  {
    double val = 0.01741112215299716;
    val = val * logt + -0.3974480768200861;
    val = val * logt + 4.677756383394393;
    val = val * logt + -16.76898168230458;
    val = exp(-val);
    sumxod[5] += (clamped[8]*val);
    sumxod[8] += (clamped[5]*val);
  }
  // D_7_6 and D_6_7
  {
    double val = 0.01102631087568285;
    val = val * logt + -0.2518686718836278;
    val = val * logt + 3.572303309877288;
    val = val * logt + -15.81505301509188;
    val = exp(-val);
    sumxod[6] += (clamped[7]*val);
    sumxod[7] += (clamped[6]*val);
  }
  // D_8_6 and D_6_8
  {
    double val = 0.01025712117970497;
    val = val * logt + -0.2341485365071557;
    val = val * logt + 3.43602697479444;
    val = val * logt + -15.45395375030161;
    val = exp(-val);
    sumxod[6] += (clamped[8]*val);
    sumxod[8] += (clamped[6]*val);
  }
  // D_8_7 and D_7_8
  {
    double val = 0.01026531645003059;
    val = val * logt + -0.2343307281563241;
    val = val * logt + 3.437372773195496;
    val = val * logt + -15.46404657753176;
    val = exp(-val);
    sumxod[7] += (clamped[8]*val);
    sumxod[8] += (clamped[7]*val);
  }
  double sumxw = 0.0;
  double wtm = 0.0;
  sumxw += (clamped[0]*2.01594);
  wtm += (mole_frac[0]*2.01594);
  sumxw += (clamped[1]*31.9988);
  wtm += (mole_frac[1]*31.9988);
  sumxw += (clamped[2]*15.9994);
  wtm += (mole_frac[2]*15.9994);
  sumxw += (clamped[3]*17.00737);
  wtm += (mole_frac[3]*17.00737);
  sumxw += (clamped[4]*18.01534);
  wtm += (mole_frac[4]*18.01534);
  sumxw += (clamped[5]*1.00797);
  wtm += (mole_frac[5]*1.00797);
  sumxw += (clamped[6]*33.00677);
  wtm += (mole_frac[6]*33.00677);
  sumxw += (clamped[7]*34.01474);
  wtm += (mole_frac[7]*34.01474);
  sumxw += (clamped[8]*28.0134);
  wtm += (mole_frac[8]*28.0134);
  double pfac = 1.01325e+06/pressure;
  {
    double result = pfac * (sumxw - (2.01594*clamped[0])) / (wtm * sumxod[0]);
    result *= 6.386577550694006e-05;
    asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(diffusion+0*spec_stride) , 
      "d"(result) : "memory"); 
  }
  {
    double result = pfac * (sumxw - (31.9988*clamped[1])) / (wtm * sumxod[1]);
    result *= 6.386577550694006e-05;
    asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(diffusion+1*spec_stride) , 
      "d"(result) : "memory"); 
  }
  {
    double result = pfac * (sumxw - (15.9994*clamped[2])) / (wtm * sumxod[2]);
    result *= 6.386577550694006e-05;
    asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(diffusion+2*spec_stride) , 
      "d"(result) : "memory"); 
  }
  {
    double result = pfac * (sumxw - (17.00737*clamped[3])) / (wtm * sumxod[3]);
    result *= 6.386577550694006e-05;
    asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(diffusion+3*spec_stride) , 
      "d"(result) : "memory"); 
  }
  {
    double result = pfac * (sumxw - (18.01534*clamped[4])) / (wtm * sumxod[4]);
    result *= 6.386577550694006e-05;
    asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(diffusion+4*spec_stride) , 
      "d"(result) : "memory"); 
  }
  {
    double result = pfac * (sumxw - (1.00797*clamped[5])) / (wtm * sumxod[5]);
    result *= 6.386577550694006e-05;
    asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(diffusion+5*spec_stride) , 
      "d"(result) : "memory"); 
  }
  {
    double result = pfac * (sumxw - (33.00677*clamped[6])) / (wtm * sumxod[6]);
    result *= 6.386577550694006e-05;
    asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(diffusion+6*spec_stride) , 
      "d"(result) : "memory"); 
  }
  {
    double result = pfac * (sumxw - (34.01474*clamped[7])) / (wtm * sumxod[7]);
    result *= 6.386577550694006e-05;
    asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(diffusion+7*spec_stride) , 
      "d"(result) : "memory"); 
  }
  {
    double result = pfac * (sumxw - (28.0134*clamped[8])) / (wtm * sumxod[8]);
    result *= 6.386577550694006e-05;
    asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(diffusion+8*spec_stride) , 
      "d"(result) : "memory"); 
  }
}

