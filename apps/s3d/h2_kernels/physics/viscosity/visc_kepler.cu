
#define THREADS_PER_BLOCK    128

__constant__ double recip_molecular_masses[9] = {0.4960465093207139, 
  0.03125117191894696, 0.06250234383789392, 0.05879803873262004, 
  0.05550825019122593, 0.9920930186414277, 0.03029681486555637, 
  0.02939901936631002, 0.03569720205330306}; 

__global__ void
__launch_bounds__(THREADS_PER_BLOCK,2)
gpu_viscosity(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *viscosity) 
{
  {
    const int offset = (blockIdx.x*blockDim.x+threadIdx.x);
    temperature_array += offset;
    mass_frac_array += offset;
    avmolwt_array += offset;
    viscosity += offset;
  }
  double temperature;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  double avmolwt;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(avmolwt) : 
    "l"(avmolwt_array) : "memory"); 
  double mole_frac[9];
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac[0]) : 
    "l"(mass_frac_array+0*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac[1]) : 
    "l"(mass_frac_array+1*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac[2]) : 
    "l"(mass_frac_array+2*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac[3]) : 
    "l"(mass_frac_array+3*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac[4]) : 
    "l"(mass_frac_array+4*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac[5]) : 
    "l"(mass_frac_array+5*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac[6]) : 
    "l"(mass_frac_array+6*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac[7]) : 
    "l"(mass_frac_array+7*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac[8]) : 
    "l"(mass_frac_array+8*spec_stride) : "memory"); 
  for (int i = 0; i < 9; i++)
  {
    mole_frac[i] *= recip_molecular_masses[i] * 1e3 * avmolwt;
  }
  temperature *= 120.0;
  double logt = log(temperature);
  double spec_visc[9];
  // Species H2
  {
    double val = 2.095995943985418e-03;
    val = val * logt + -0.04534890654515034;
    val = val * logt + 0.9708642724957206;
    val = val * logt + -13.76709568554653;
    spec_visc[0] = exp(val);
  }
  // Species O2
  {
    double val = 0.01100612385033574;
    val = val * logt + -0.2490705573726834;
    val = val * logt + 2.522523673476987;
    val = val * logt + -16.8107894214942;
    spec_visc[1] = exp(val);
  }
  // Species O
  {
    double val = 6.908732177852647e-03;
    val = val * logt + -0.1549096547430725;
    val = val * logt + 1.801392496838963;
    val = val * logt + -14.81562698872598;
    spec_visc[2] = exp(val);
  }
  // Species OH
  {
    double val = 6.90873217785305e-03;
    val = val * logt + -0.154909654743081;
    val = val * logt + 1.801392496839023;
    val = val * logt + -14.78507920973381;
    spec_visc[3] = exp(val);
  }
  // Species H2O
  {
    double val = -0.01986368642206398;
    val = val * logt + 0.3341412968649872;
    val = val * logt + -0.7882553001460173;
    val = val * logt + -11.87799986949853;
    spec_visc[4] = exp(val);
  }
  // Species H
  {
    double val = 0.01585988965532433;
    val = val * logt + -0.3631110078153673;
    val = val * logt + 3.416401604464392;
    val = val * logt + -19.87530243529893;
    spec_visc[5] = exp(val);
  }
  // Species HO2
  {
    double val = 0.01100612385033564;
    val = val * logt + -0.2490705573726812;
    val = val * logt + 2.522523673476971;
    val = val * logt + -16.79528227657199;
    spec_visc[6] = exp(val);
  }
  // Species H2O2
  {
    double val = 0.0110061238503359;
    val = val * logt + -0.249070557372687;
    val = val * logt + 2.522523673477013;
    val = val * logt + -16.78024164250196;
    spec_visc[7] = exp(val);
  }
  // Species N2
  {
    double val = 9.477798464440641e-03;
    val = val * logt + -0.2138335631679552;
    val = val * logt + 2.251736741733607;
    val = val * logt + -16.26171975836591;
    spec_visc[8] = exp(val);
  }
  double result = 0.0;
  // Species H2
  {
    double spec_sum = 0.0;
    {
      spec_sum += (mole_frac[0]*4.0/1.414213562373095);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[0]/spec_visc[1]*3.98407994930469);
      spec_sum += (mole_frac[1]*numer*numer/1.031019149928013);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[0]/spec_visc[2]*2.817169948942702);
      spec_sum += (mole_frac[2]*numer*numer/1.061131931022983);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[0]/spec_visc[3]*2.904556165961648);
      spec_sum += (mole_frac[3]*numer*numer/1.057607355403052);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[0]/spec_visc[4]*2.989388987941487);
      spec_sum += (mole_frac[4]*numer*numer/1.054467307170071);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[0]/spec_visc[5]*0.7071067811865476);
      spec_sum += (mole_frac[5]*numer*numer/1.732050807568877);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[0]/spec_visc[6]*4.046343169140707);
      spec_sum += (mole_frac[6]*numer*numer/1.030085705638162);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[0]/spec_visc[7]*4.107662722577361);
      spec_sum += (mole_frac[7]*numer*numer/1.029206810656303);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[0]/spec_visc[8]*3.727727093579261);
      spec_sum += (mole_frac[8]*numer*numer/1.035356661980467);
    }
    result += (mole_frac[0]*spec_visc[0]/spec_sum);
  }
  // Species O2
  {
    double spec_sum = 0.0;
    {
      double numer = 1.0 + sqrt(spec_visc[1]/spec_visc[0]*0.2509989791180074);
      spec_sum += (mole_frac[0]*numer*numer/4.107662722577361);
    }
    {
      spec_sum += (mole_frac[1]*4.0/1.414213562373095);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[1]/spec_visc[2]*0.7071067811865476);
      spec_sum += (mole_frac[2]*numer*numer/1.732050807568877);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[1]/spec_visc[3]*0.7290406324472875);
      spec_sum += (mole_frac[3]*numer*numer/1.697488345113852);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[1]/spec_visc[4]*0.7503335841599268);
      spec_sum += (mole_frac[4]*numer*numer/1.666192484744485);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[1]/spec_visc[5]*0.1774830802052437);
      spec_sum += (mole_frac[5]*numer*numer/5.722393387814518);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[1]/spec_visc[6]*1.01562800461544);
      spec_sum += (mole_frac[6]*numer*numer/1.403375117179995);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[1]/spec_visc[7]*1.031019149928013);
      spec_sum += (mole_frac[7]*numer*numer/1.393102056885525);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[1]/spec_visc[8]*0.9356556949189316);
      spec_sum += (mole_frac[8]*numer*numer/1.463648738278155);
    }
    result += (mole_frac[1]*spec_visc[1]/spec_sum);
  }
  // Species O
  {
    double spec_sum = 0.0;
    {
      double numer = 1.0 + sqrt(spec_visc[2]/spec_visc[0]*0.3549661604104874);
      spec_sum += (mole_frac[0]*numer*numer/2.989388987941487);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[2]/spec_visc[1]*1.414213562373095);
      spec_sum += (mole_frac[1]*numer*numer/1.224744871391589);
    }
    {
      spec_sum += (mole_frac[2]*4.0/1.414213562373095);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[2]/spec_visc[3]*1.031019149928013);
      spec_sum += (mole_frac[3]*numer*numer/1.393102056885525);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[2]/spec_visc[4]*1.061131931022983);
      spec_sum += (mole_frac[4]*numer*numer/1.374081037679183);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[2]/spec_visc[5]*0.2509989791180074);
      spec_sum += (mole_frac[5]*numer*numer/4.107662722577361);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[2]/spec_visc[6]*1.43631489845308);
      spec_sum += (mole_frac[6]*numer*numer/1.21849532611331);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[2]/spec_visc[7]*1.458081264894575);
      spec_sum += (mole_frac[7]*numer*numer/1.212586768214688);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[2]/spec_visc[8]*1.323216973465976);
      spec_sum += (mole_frac[8]*numer*numer/1.253448768211775);
    }
    result += (mole_frac[2]*spec_visc[2]/spec_sum);
  }
  // Species OH
  {
    double spec_sum = 0.0;
    {
      double numer = 1.0 + sqrt(spec_visc[3]/spec_visc[0]*0.3442866802573664);
      spec_sum += (mole_frac[0]*numer*numer/3.071879965302327);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[3]/spec_visc[1]*1.371665659626048);
      spec_sum += (mole_frac[1]*numer*numer/1.237537976693702);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[3]/spec_visc[2]*0.9699140894422975);
      spec_sum += (mole_frac[2]*numer*numer/1.43631489845308);
    }
    {
      spec_sum += (mole_frac[3]*4.0/1.414213562373095);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[3]/spec_visc[4]*1.029206810656303);
      spec_sum += (mole_frac[4]*numer*numer/1.394291701565619);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[3]/spec_visc[5]*0.2434474462821884);
      spec_sum += (mole_frac[5]*numer*numer/4.227634449955632);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[3]/spec_visc[6]*1.393102056885525);
      spec_sum += (mole_frac[6]*numer*numer/1.230962688402868);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[3]/spec_visc[7]*1.414213562373095);
      spec_sum += (mole_frac[7]*numer*numer/1.224744871391589);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[3]/spec_visc[8]*1.283406785953845);
      spec_sum += (mole_frac[8]*numer*numer/1.267720601428124);
    }
    result += (mole_frac[3]*spec_visc[3]/spec_sum);
  }
  // Species H2O
  {
    double spec_sum = 0.0;
    {
      double numer = 1.0 + sqrt(spec_visc[4]/spec_visc[0]*0.3345165196077766);
      spec_sum += (mole_frac[0]*numer*numer/3.152212956198523);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[4]/spec_visc[1]*1.332740558480532);
      spec_sum += (mole_frac[1]*numer*numer/1.250200178978663);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[4]/spec_visc[2]*0.9423898864639306);
      spec_sum += (mole_frac[2]*numer*numer/1.458081264894575);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[4]/spec_visc[3]*0.9716220196427982);
      spec_sum += (mole_frac[3]*numer*numer/1.435014515292901);
    }
    {
      spec_sum += (mole_frac[4]*4.0/1.414213562373095);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[4]/spec_visc[5]*0.2365388994335815);
      spec_sum += (mole_frac[5]*numer*numer/4.344294308912744);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[4]/spec_visc[6]*1.35356863407965);
      spec_sum += (mole_frac[6]*numer*numer/1.243305039288449);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[4]/spec_visc[7]*1.374081037679183);
      spec_sum += (mole_frac[7]*numer*numer/1.236783461059639);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[4]/spec_visc[8]*1.246986293391747);
      spec_sum += (mole_frac[8]*numer*numer/1.281833543030823);
    }
    result += (mole_frac[4]*spec_visc[4]/spec_sum);
  }
  // Species H
  {
    double spec_sum = 0.0;
    {
      double numer = 1.0 + sqrt(spec_visc[5]/spec_visc[0]*1.414213562373095);
      spec_sum += (mole_frac[0]*numer*numer/1.224744871391589);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[5]/spec_visc[1]*5.634339897885405);
      spec_sum += (mole_frac[1]*numer*numer/1.01562800461544);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[5]/spec_visc[2]*3.98407994930469);
      spec_sum += (mole_frac[2]*numer*numer/1.031019149928013);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[5]/spec_visc[3]*4.107662722577361);
      spec_sum += (mole_frac[3]*numer*numer/1.029206810656303);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[5]/spec_visc[4]*4.227634449955632);
      spec_sum += (mole_frac[4]*numer*numer/1.027594594645792);
    }
    {
      spec_sum += (mole_frac[5]*4.0/1.414213562373095);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[5]/spec_visc[6]*5.722393387814518);
      spec_sum += (mole_frac[6]*numer*numer/1.015154313629231);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[5]/spec_visc[7]*5.809112331923296);
      spec_sum += (mole_frac[7]*numer*numer/1.014708494864737);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[5]/spec_visc[8]*5.271802212565431);
      spec_sum += (mole_frac[8]*numer*numer/1.01783186664285);
    }
    result += (mole_frac[5]*spec_visc[5]/spec_sum);
  }
  // Species HO2
  {
    double spec_sum = 0.0;
    {
      double numer = 1.0 + sqrt(spec_visc[6]/spec_visc[0]*0.247136725235384);
      spec_sum += (mole_frac[0]*numer*numer/4.168080258638461);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[6]/spec_visc[1]*0.984612471747116);
      spec_sum += (mole_frac[1]*numer*numer/1.425307069988478);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[6]/spec_visc[2]*0.6962261556132336);
      spec_sum += (mole_frac[2]*numer*numer/1.750142990591992);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[6]/spec_visc[3]*0.7178224991180044);
      spec_sum += (mole_frac[3]*numer*numer/1.714856653163372);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[6]/spec_visc[4]*0.7387878049345782);
      spec_sum += (mole_frac[4]*numer*numer/1.682898703774012);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[6]/spec_visc[5]*0.1747520542941766);
      spec_sum += (mole_frac[5]*numer*numer/5.809112331923296);
    }
    {
      spec_sum += (mole_frac[6]*4.0/1.414213562373095);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[6]/spec_visc[7]*1.015154313629231);
      spec_sum += (mole_frac[7]*numer*numer/1.403697499623527);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[6]/spec_visc[8]*0.9212582664783947);
      spec_sum += (mole_frac[8]*numer*numer/1.47588933793049);
    }
    result += (mole_frac[6]*spec_visc[6]/spec_sum);
  }
  // Species H2O2
  {
    double spec_sum = 0.0;
    {
      double numer = 1.0 + sqrt(spec_visc[7]/spec_visc[0]*0.2434474462821884);
      spec_sum += (mole_frac[0]*numer*numer/4.227634449955632);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[7]/spec_visc[1]*0.9699140894422975);
      spec_sum += (mole_frac[1]*numer*numer/1.43631489845308);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[7]/spec_visc[2]*0.6858328298130242);
      spec_sum += (mole_frac[2]*numer*numer/1.768050048792897);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[7]/spec_visc[3]*0.7071067811865476);
      spec_sum += (mole_frac[3]*numer*numer/1.732050807568877);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[7]/spec_visc[4]*0.7277591150584509);
      spec_sum += (mole_frac[4]*numer*numer/1.69944070155728);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[7]/spec_visc[5]*0.1721433401286832);
      spec_sum += (mole_frac[5]*numer*numer/5.894555630826069);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[7]/spec_visc[6]*0.9850719113086823);
      spec_sum += (mole_frac[6]*numer*numer/1.424969571773389);
    }
    {
      spec_sum += (mole_frac[7]*4.0/1.414213562373095);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[7]/spec_visc[8]*0.9075056413687956);
      spec_sum += (mole_frac[8]*numer*numer/1.488029249232208);
    }
    result += (mole_frac[7]*spec_visc[7]/spec_sum);
  }
  // Species N2
  {
    double spec_sum = 0.0;
    {
      double numer = 1.0 + sqrt(spec_visc[8]/spec_visc[0]*0.2682599811886517);
      spec_sum += (mole_frac[0]*numer*numer/3.859527080382374);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[8]/spec_visc[1]*1.06876921225456);
      spec_sum += (mole_frac[1]*numer*numer/1.369471277330864);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[8]/spec_visc[2]*0.7557339575086043);
      spec_sum += (mole_frac[2]*numer*numer/1.658584685467841);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[8]/spec_visc[3]*0.7791761824422541);
      spec_sum += (mole_frac[3]*numer*numer/1.627001222566344);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[8]/spec_visc[4]*0.801933433670746);
      spec_sum += (mole_frac[4]*numer*numer/1.598428858569217);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[8]/spec_visc[5]*0.1896884518194713);
      spec_sum += (mole_frac[5]*numer*numer/5.365808286587378);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[8]/spec_visc[6]*1.085471942436515);
      spec_sum += (mole_frac[6]*numer*numer/1.359675252975789);
    }
    {
      double numer = 1.0 + sqrt(spec_visc[8]/spec_visc[7]*1.101921524687929);
      spec_sum += (mole_frac[7]*numer*numer/1.350394938200003);
    }
    {
      spec_sum += (mole_frac[8]*4.0/1.414213562373095);
    }
    result += (mole_frac[8]*spec_visc[8]/spec_sum);
  }
  result *= 0.1535268500652559;
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(viscosity), "d"(result) : 
    "memory"); 
}


