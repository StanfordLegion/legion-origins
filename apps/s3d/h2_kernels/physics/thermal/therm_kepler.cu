
__constant__ double recip_molecular_masses[9] = {0.4960465093207139, 
  0.03125117191894696, 0.06250234383789392, 0.05879803873262004, 
  0.05550825019122593, 0.9920930186414277, 0.03029681486555637, 
  0.02939901936631002, 0.03569720205330306}; 

__global__ void
__launch_bounds__(128,4)
gpu_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *thermal_out) 
{
  {
    const int offset = (blockIdx.x*blockDim.x+threadIdx.x);
    temperature_array += offset;
    mass_frac_array += offset;
    avmolwt_array += offset;
    thermal_out += offset;
  }
  // Load the temperatures
  double temperature;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  // Load the average mole weight
  double avmolwt;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(avmolwt) : 
    "l"(avmolwt_array) : "memory"); 
  double thermal[2];
  thermal[0] = 0.0;
  double mass_frac_light_0;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac_light_0) : 
    "l"(mass_frac_array+0*spec_stride) : "memory"); 
  thermal[1] = 0.0;
  double mass_frac_light_5;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac_light_5) : 
    "l"(mass_frac_array+5*spec_stride) : "memory"); 
  temperature *= 120.0;
  double mole_frac_light_0 = mass_frac_light_0 * recip_molecular_masses[0] * 1e3 
    * avmolwt; 
  double mole_frac_light_5 = mass_frac_light_5 * recip_molecular_masses[5] * 1e3 
    * avmolwt; 
  {
    double mole_frac = mole_frac_light_0;
    // No interaction for species 0 with itself
    {
      double val = 
        __fma_rn(-4.870919137829873e-12,temperature,2.93412470121339e-08); 
      val = __fma_rn(val,temperature,-5.464040220781074e-05);
      val = __fma_rn(val,temperature,-0.1525347421818075);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_5,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+1*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[1] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(-6.863234368775381e-12,temperature,3.847680615982582e-08); 
      val = __fma_rn(val,temperature,-7.117708177718642e-05);
      val = __fma_rn(val,temperature,-0.4427390838930854);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(-3.464704359017016e-11,temperature,2.329279443546141e-07); 
      val = __fma_rn(val,temperature,-4.801642880313133e-04);
      val = __fma_rn(val,temperature,-0.2204828425102966);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_5,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+2*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[2] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(-1.144144427154856e-12,temperature,3.960219633369755e-09); 
      val = __fma_rn(val,temperature,-1.09738398546134e-05);
      val = __fma_rn(val,temperature,-0.4155833371210523);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(-2.753212482584995e-11,temperature,1.80744751529337e-07); 
      val = __fma_rn(val,temperature,-3.615550927540219e-04);
      val = __fma_rn(val,temperature,-0.2700101503763489);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_5,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+3*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[3] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(-1.161624181476171e-12,temperature,4.020722192842837e-09); 
      val = __fma_rn(val,temperature,-1.114149353544903e-05);
      val = __fma_rn(val,temperature,-0.4219324434580686);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(-2.77392721913982e-11,temperature,1.821046465375428e-07); 
      val = __fma_rn(val,temperature,-3.642753762569583e-04);
      val = __fma_rn(val,temperature,-0.2720416641688922);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_5,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+4*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[4] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(-3.633898010835058e-11,temperature,2.553727569929838e-07); 
      val = __fma_rn(val,temperature,-5.615615459953129e-04);
      val = __fma_rn(val,temperature,-0.06020313090654318);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(-4.029595022301052e-11,temperature,3.065500034524752e-07); 
      val = __fma_rn(val,temperature,-7.665588101246596e-04);
      val = __fma_rn(val,temperature,0.141883744068593);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_5,val,thermal[1]);
    }
  }
  {
    double mole_frac = mole_frac_light_5;
    {
      double val = 
        __fma_rn(4.870919137829873e-12,temperature,-2.93412470121339e-08); 
      val = __fma_rn(val,temperature,5.464040220781074e-05);
      val = __fma_rn(val,temperature,0.1525347421818075);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    // No interaction for species 5 with itself
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+6*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[6] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(-6.889796396702935e-12,temperature,3.862571874897331e-08); 
      val = __fma_rn(val,temperature,-7.145255067892884e-05);
      val = __fma_rn(val,temperature,-0.4444525687136664);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(-3.471383045051115e-11,temperature,2.333769444561275e-07); 
      val = __fma_rn(val,temperature,-4.810898696083394e-04);
      val = __fma_rn(val,temperature,-0.2209078529955923);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_5,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+7*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[7] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(-6.914872261877499e-12,temperature,3.876629958182396e-08); 
      val = __fma_rn(val,temperature,-7.171260691630346e-05);
      val = __fma_rn(val,temperature,-0.4460701829431514);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(-3.477677299659906e-11,temperature,2.338000996911347e-07); 
      val = __fma_rn(val,temperature,-4.81962173842625e-04);
      val = __fma_rn(val,temperature,-0.2213083994791669);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_5,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+8*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[8] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(-4.903062174600689e-12,temperature,2.630234415935386e-08); 
      val = __fma_rn(val,temperature,-4.946971739853057e-05);
      val = __fma_rn(val,temperature,-0.4452619662404715);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(-3.269585062280141e-11,temperature,2.181738735658955e-07); 
      val = __fma_rn(val,temperature,-4.453434507868586e-04);
      val = __fma_rn(val,temperature,-0.2407444208104045);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_5,val,thermal[1]);
    }
  }
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+0*spec_stride) , 
    "d"(thermal[0]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+1*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+2*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+3*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+4*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+5*spec_stride) , 
    "d"(thermal[1]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+6*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+7*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+8*spec_stride) , 
    "d"(0.0) : "memory"); 
}

