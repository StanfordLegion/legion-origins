
#include "avx_getrates.h"
#include <cmath>
#include <cassert>
#include <cstdlib>
#include <emmintrin.h>
#include <smmintrin.h>
#include <immintrin.h>

#ifndef __SINGE_MOLE_MASSES__
#define __SINGE_MOLE_MASSES__
const double molecular_masses[9] = {2.01594, 31.9988, 15.9994, 17.00737, 
  18.01534, 1.00797, 33.00677, 34.01474, 28.0134}; 
#endif


#ifndef __SINGE_RECIP_MOLE_MASSES__
#define __SINGE_RECIP_MOLE_MASSES__
const double recip_molecular_masses[9] = {0.4960465093207139, 
  0.03125117191894696, 0.06250234383789392, 0.05879803873262004, 
  0.05550825019122593, 0.9920930186414277, 0.03029681486555637, 
  0.02939901936631002, 0.03569720205330306}; 
#endif


void avx_getrates(const double *pressure_array, const double *temperature_array, 
  const double *avmolwt_array, const double *mass_frac_array, const int 
  num_elmts, const int spec_stride, double *wdot_array) 
{
  
  const double PA = 1.013250e+06;
  // Scaled R0 for non-dimensionalization
  const double R0 = 9.977411999999998e+09;
  // Scaled R0c for non-dimensionalization
  const double R0c = 238.4658699839999;
  const double DLn10 = 2.3025850929940459e0;
  
  __m256d *cgspl = (__m256d*)malloc(9*73*sizeof(__m256d));
  __m256d *temperature = (__m256d*)malloc(73*sizeof(__m256d));
  __m256d *otc = (__m256d*)malloc(73*sizeof(__m256d));
  __m256d *ortc = (__m256d*)malloc(73*sizeof(__m256d));
  __m256d *vlntemp = (__m256d*)malloc(73*sizeof(__m256d));
  __m256d *prt = (__m256d*)malloc(73*sizeof(__m256d));
  __m256d *oprt = (__m256d*)malloc(73*sizeof(__m256d));
  __m256d *mole_frac = (__m256d*)malloc(9*73*sizeof(__m256d));
  __m256d *thbctemp = (__m256d*)malloc(2*73*sizeof(__m256d));
  __m256d *rr_f = (__m256d*)malloc(19*73*sizeof(__m256d));
  __m256d *rr_r = (__m256d*)malloc(19*73*sizeof(__m256d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 292)
  {
    // Temperature dependent
    for (unsigned idx = 0; idx < 73; idx++)
    {
      temperature[idx] = _mm256_load_pd(temperature_array);
      temperature_array += 4;
      otc[idx] = _mm256_div_pd(_mm256_set1_pd(1.0),temperature[idx]);
      ortc[idx] = 
        _mm256_div_pd(_mm256_set1_pd(1.0),_mm256_mul_pd(temperature[idx],_mm256_set1_pd(R0c))); 
      {
        __m128d lower = _mm256_extractf128_pd(temperature[idx],0);
        __m128d upper = _mm256_extractf128_pd(temperature[idx],1);
        vlntemp[idx] = 
          _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      }
      prt[idx] = 
        _mm256_div_pd(_mm256_set1_pd(PA),_mm256_mul_pd(temperature[idx],_mm256_set1_pd(R0))); 
      oprt[idx] = _mm256_div_pd(_mm256_set1_pd(1.0),prt[idx]);
    }
    // Gibbs computation
    for (unsigned idx = 0; idx < 73; idx++)
    {
      __m256d tk1 = temperature[idx];
      __m256d tklog = vlntemp[idx];
      __m256d tk2 = _mm256_mul_pd(tk1,tk1);
      __m256d tk3 = _mm256_mul_pd(tk1,tk2);
      __m256d tk4 = _mm256_mul_pd(tk1,tk3);
      __m256d tk5 = _mm256_mul_pd(tk1,tk4);
      __m128d lower = _mm256_extractf128_pd(tk1,0);
      __m128d upper = _mm256_extractf128_pd(tk1,1);
      // Species H2
      {
        __m256d cgspl_NC_H2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  0); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  1); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  2); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  3); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  4); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  5); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  6); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  7); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  8); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  9); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  10); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  11); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  12); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  13); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  14); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  15); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
          }
        }
        cgspl[0+idx] = cgspl_NC_H2;
      }
      // Species O2
      {
        __m256d cgspl_NC_O2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  0); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  1); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  2); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  3); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  4); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  5); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  6); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  7); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  8); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  9); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  10); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  11); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  12); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  13); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  14); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  15); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
          }
        }
        cgspl[73+idx] = cgspl_NC_O2;
      }
      // Species O
      {
        __m256d cgspl_NC_O, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  0); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  1); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  2); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  3); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  4); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  5); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  6); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  7); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  8); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  9); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  10); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  11); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  12); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  13); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  14); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  15); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
          }
        }
        cgspl[146+idx] = cgspl_NC_O;
      }
      // Species OH
      {
        __m256d cgspl_NC_OH, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  0); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  1); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  2); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  3); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  4); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  5); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  6); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  7); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  8); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  9); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  10); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  11); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  12); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  13); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  14); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  15); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
          }
        }
        cgspl[219+idx] = cgspl_NC_OH;
      }
      // Species H2O
      {
        __m256d cgspl_NC_H2O, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  0); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  1); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  2); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  3); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  4); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  5); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  6); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  7); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  8); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  9); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  10); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  11); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  12); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  13); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  14); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  15); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
          }
        }
        cgspl[292+idx] = cgspl_NC_H2O;
      }
      // Species H
      {
        __m256d cgspl_NC_H, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 0); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 1); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 2); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 3); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 4); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 5); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 6); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 7); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 8); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 9); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 10); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 11); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 12); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 13); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 14); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 15); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
          }
        }
        cgspl[365+idx] = cgspl_NC_H;
      }
      // Species HO2
      {
        __m256d cgspl_NC_HO2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  0); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  1); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  2); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  3); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  4); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  5); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  6); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  7); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  8); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  9); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  10); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  11); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  12); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  13); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  14); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  15); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
          }
        }
        cgspl[438+idx] = cgspl_NC_HO2;
      }
      // Species H2O2
      {
        __m256d cgspl_NC_H2O2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  0); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  1); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  2); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  3); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  4); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  5); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  6); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  7); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  8); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  9); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  10); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  11); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  12); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  13); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  14); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  15); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
          }
        }
        cgspl[511+idx] = cgspl_NC_H2O2;
      }
      // Species N2
      {
        __m256d cgspl_NC_N2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  0); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  1); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  2); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  3); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  4); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  5); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  6); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  7); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  8); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  9); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  10); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  11); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  12); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  13); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  14); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  15); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
          }
        }
        cgspl[584+idx] = cgspl_NC_N2;
      }
    }
    // Compute mole fractions
    for (unsigned idx = 0; idx < 73; idx++)
    {
      __m256d avmolwt = _mm256_load_pd(avmolwt_array);
      __m256d pressure = _mm256_load_pd(pressure_array);
      avmolwt_array += 4;
      pressure_array += 4;
      __m256d sumyow = 
        _mm256_mul_pd(_mm256_mul_pd(temperature[idx],avmolwt),_mm256_set1_pd(7.03444160806564)); 
      sumyow = _mm256_div_pd(pressure,sumyow);
      __m256d mask, mass_frac;
      __m256d small = _mm256_set1_pd(1e-200);
      mass_frac = _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[0])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[0+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[1])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[73+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[2])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[146+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[3])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[219+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[4])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[292+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[5])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[365+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[6])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[438+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[7])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[511+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[8])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[584+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
    }
    // Compute third body values
    for (unsigned idx = 0; idx < 73; idx++)
    {
      __m256d ctot = _mm256_set1_pd(0.0);
      ctot = _mm256_add_pd(ctot,mole_frac[0+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[73+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[146+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[219+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[292+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[365+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[438+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[511+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[584+idx]);
      thbctemp[0+idx] = ctot;
      thbctemp[0+idx] = 
        _mm256_add_pd(thbctemp[0+idx],_mm256_mul_pd(_mm256_set1_pd(1.5),mole_frac[0+idx])); 
      thbctemp[0+idx] = 
        _mm256_add_pd(thbctemp[0+idx],_mm256_mul_pd(_mm256_set1_pd(11.0),mole_frac[292+idx])); 
      thbctemp[73+idx] = ctot;
      thbctemp[73+idx] = _mm256_add_pd(thbctemp[73+idx],mole_frac[0+idx]);
      thbctemp[73+idx] = 
        _mm256_sub_pd(thbctemp[73+idx],_mm256_mul_pd(_mm256_set1_pd(0.22),mole_frac[73+idx])); 
      thbctemp[73+idx] = 
        _mm256_add_pd(thbctemp[73+idx],_mm256_mul_pd(_mm256_set1_pd(10.0),mole_frac[292+idx])); 
    }
    // Reaction rates
    for (unsigned idx = 0; idx < 73; idx++)
    {
      //   0)  O2 + H <=> O + OH
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(-0.406),vlntemp[idx]); 
            arrtemp = 
              _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(1.6599e+04),ortc[idx])); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(5.606052175734568e+15),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[73+idx]);
        xik = _mm256_add_pd(xik,cgspl[146+idx]);
        xik = _mm256_add_pd(xik,cgspl[219+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[0+idx] = forward;
        rr_f[0+idx] = _mm256_mul_pd(rr_f[0+idx],mole_frac[73+idx]);
        rr_f[0+idx] = _mm256_mul_pd(rr_f[0+idx],mole_frac[365+idx]);
        rr_r[0+idx] = reverse;
        rr_r[0+idx] = _mm256_mul_pd(rr_r[0+idx],mole_frac[146+idx]);
        rr_r[0+idx] = _mm256_mul_pd(rr_r[0+idx],mole_frac[219+idx]);
      }
      //   1)  H2 + O <=> OH + H
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(2.67),vlntemp[idx]);
            arrtemp = 
              _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(6.29e+03),ortc[idx])); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(1.996278696128123e+11),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[0+idx]);
        xik = _mm256_sub_pd(xik,cgspl[146+idx]);
        xik = _mm256_add_pd(xik,cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[365+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[73+idx] = forward;
        rr_f[73+idx] = _mm256_mul_pd(rr_f[73+idx],mole_frac[0+idx]);
        rr_f[73+idx] = _mm256_mul_pd(rr_f[73+idx],mole_frac[146+idx]);
        rr_r[73+idx] = reverse;
        rr_r[73+idx] = _mm256_mul_pd(rr_r[73+idx],mole_frac[219+idx]);
        rr_r[73+idx] = _mm256_mul_pd(rr_r[73+idx],mole_frac[365+idx]);
      }
      //   2)  H2 + OH <=> H2O + H
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(1.51),vlntemp[idx]);
            arrtemp = 
              _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(3.43e+03),ortc[idx])); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(3.288220749682686e+12),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[0+idx]);
        xik = _mm256_sub_pd(xik,cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[292+idx]);
        xik = _mm256_add_pd(xik,cgspl[365+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[146+idx] = forward;
        rr_f[146+idx] = _mm256_mul_pd(rr_f[146+idx],mole_frac[0+idx]);
        rr_f[146+idx] = _mm256_mul_pd(rr_f[146+idx],mole_frac[219+idx]);
        rr_r[146+idx] = reverse;
        rr_r[146+idx] = _mm256_mul_pd(rr_r[146+idx],mole_frac[292+idx]);
        rr_r[146+idx] = _mm256_mul_pd(rr_r[146+idx],mole_frac[365+idx]);
      }
      //   3)  O + H2O <=> 2 OH
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(2.02),vlntemp[idx]);
            arrtemp = 
              _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(1.34e+04),ortc[idx])); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(5.195724313044046e+11),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[146+idx]);
        xik = 
          _mm256_add_pd(xik,_mm256_mul_pd(_mm256_set1_pd(double(2)),cgspl[219+idx])); 
        xik = _mm256_sub_pd(xik,cgspl[292+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[219+idx] = forward;
        rr_f[219+idx] = _mm256_mul_pd(rr_f[219+idx],mole_frac[146+idx]);
        rr_f[219+idx] = _mm256_mul_pd(rr_f[219+idx],mole_frac[292+idx]);
        rr_r[219+idx] = reverse;
        rr_r[219+idx] = _mm256_mul_pd(rr_r[219+idx],mole_frac[219+idx]);
        rr_r[219+idx] = _mm256_mul_pd(rr_r[219+idx],mole_frac[219+idx]);
      }
      //   4)  H2 + M <=> 2 H + M
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-1.4),vlntemp[idx]);
            arrtemp = 
              _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(1.0438e+05),ortc[idx])); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(6.203984453113697e+17),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[0+idx]);
        xik = 
          _mm256_add_pd(xik,_mm256_mul_pd(_mm256_set1_pd(double(2)),cgspl[365+idx])); 
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,oprt[idx]);
        rr_f[292+idx] = forward;
        rr_f[292+idx] = _mm256_mul_pd(rr_f[292+idx],mole_frac[0+idx]);
        rr_r[292+idx] = reverse;
        rr_r[292+idx] = _mm256_mul_pd(rr_r[292+idx],mole_frac[365+idx]);
        rr_r[292+idx] = _mm256_mul_pd(rr_r[292+idx],mole_frac[365+idx]);
        rr_f[292+idx] = _mm256_mul_pd(rr_f[292+idx],thbctemp[0+idx]);
        rr_r[292+idx] = _mm256_mul_pd(rr_r[292+idx],thbctemp[0+idx]);
      }
      //   5)  2 O + M <=> O2 + M
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-0.5),vlntemp[idx]);
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = _mm256_mul_pd(_mm256_set1_pd(6.21278022363831e+15), 
              arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[73+idx];
        xik = 
          _mm256_add_pd(xik,_mm256_mul_pd(_mm256_set1_pd(double(-2)),cgspl[146+idx])); 
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,prt[idx]);
        rr_f[365+idx] = forward;
        rr_f[365+idx] = _mm256_mul_pd(rr_f[365+idx],mole_frac[146+idx]);
        rr_f[365+idx] = _mm256_mul_pd(rr_f[365+idx],mole_frac[146+idx]);
        rr_r[365+idx] = reverse;
        rr_r[365+idx] = _mm256_mul_pd(rr_r[365+idx],mole_frac[73+idx]);
        rr_f[365+idx] = _mm256_mul_pd(rr_f[365+idx],thbctemp[0+idx]);
        rr_r[365+idx] = _mm256_mul_pd(rr_r[365+idx],thbctemp[0+idx]);
      }
      //   6)  O + H + M <=> OH + M
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(4.336624958847576e+17),otc[idx]); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[146+idx]);
        xik = _mm256_add_pd(xik,cgspl[219+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,prt[idx]);
        rr_f[438+idx] = forward;
        rr_f[438+idx] = _mm256_mul_pd(rr_f[438+idx],mole_frac[146+idx]);
        rr_f[438+idx] = _mm256_mul_pd(rr_f[438+idx],mole_frac[365+idx]);
        rr_r[438+idx] = reverse;
        rr_r[438+idx] = _mm256_mul_pd(rr_r[438+idx],mole_frac[219+idx]);
        rr_f[438+idx] = _mm256_mul_pd(rr_f[438+idx],thbctemp[0+idx]);
        rr_r[438+idx] = _mm256_mul_pd(rr_r[438+idx],thbctemp[0+idx]);
      }
      //   7)  OH + H + M <=> H2O + M
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = 
              _mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(2.913162007428368e+19),otc[idx]),otc[idx]); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[292+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,prt[idx]);
        rr_f[511+idx] = forward;
        rr_f[511+idx] = _mm256_mul_pd(rr_f[511+idx],mole_frac[219+idx]);
        rr_f[511+idx] = _mm256_mul_pd(rr_f[511+idx],mole_frac[365+idx]);
        rr_r[511+idx] = reverse;
        rr_r[511+idx] = _mm256_mul_pd(rr_r[511+idx],mole_frac[292+idx]);
        rr_f[511+idx] = _mm256_mul_pd(rr_f[511+idx],thbctemp[0+idx]);
        rr_r[511+idx] = _mm256_mul_pd(rr_r[511+idx],thbctemp[0+idx]);
      }
      //   8)  O2 + H (+M) <=> HO2 (+M)
      {
        __m256d forward, reverse;
        __m256d rr_k0, rr_kinf;
        {
          __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-1.72),vlntemp[idx]);
          arrtemp = 
            _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(524.8),ortc[idx])); 
          {
            __m128d lower = _mm256_extractf128_pd(arrtemp,0);
            __m128d upper = _mm256_extractf128_pd(arrtemp,1);
            arrtemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          arrtemp = 
            _mm256_mul_pd(_mm256_set1_pd(1.864740588650248e+18),arrtemp); 
          rr_k0 = arrtemp;
        }
        {
          __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(0.6),vlntemp[idx]);
          {
            __m128d lower = _mm256_extractf128_pd(arrtemp,0);
            __m128d upper = _mm256_extractf128_pd(arrtemp,1);
            arrtemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          arrtemp = _mm256_mul_pd(_mm256_set1_pd(2.879017473981534e+14), 
            arrtemp); 
          rr_kinf = arrtemp;
        }
        __m256d pr = 
          _mm256_mul_pd(_mm256_div_pd(rr_k0,rr_kinf),thbctemp[73+idx]); 
        __m256d fcent;
        {
          fcent = _mm256_mul_pd(temperature[idx], _mm256_set1_pd(-1.2e+32));
          {
            __m128d lower = _mm256_extractf128_pd(fcent,0);
            __m128d upper = _mm256_extractf128_pd(fcent,1);
            fcent = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          fcent = _mm256_mul_pd(fcent, _mm256_set1_pd(0.2));
          {
            __m256d fctemp = _mm256_mul_pd(temperature[idx], 
              _mm256_set1_pd(-1.2e-28)); 
            {
              __m128d lower = _mm256_extractf128_pd(fctemp,0);
              __m128d upper = _mm256_extractf128_pd(fctemp,1);
              fctemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            fcent = _mm256_add_pd(fcent, _mm256_mul_pd(fctemp, 
              _mm256_set1_pd(0.8))); 
          }
          {
            __m128d lower = _mm256_extractf128_pd(fcent,0);
            __m128d upper = _mm256_extractf128_pd(fcent,1);
            fcent = 
              _mm256_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log10(_mm_cvtsd_f64(upper)),log10(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log10(_mm_cvtsd_f64(lower))); 
          }
          __m256d flogpr;
          __m128d lower = _mm256_extractf128_pd(pr,0);
          __m128d upper = _mm256_extractf128_pd(pr,1);
          flogpr = 
            _mm256_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log10(_mm_cvtsd_f64(upper)),log10(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log10(_mm_cvtsd_f64(lower))); 
          flogpr = _mm256_sub_pd(flogpr, 
            _mm256_add_pd(_mm256_set1_pd(0.4),_mm256_mul_pd(_mm256_set1_pd(0.67),fcent))); 
          __m256d fdenom = 
            _mm256_add_pd(_mm256_set1_pd(0.75),_mm256_mul_pd(_mm256_set1_pd(-1.27),fcent)); 
          fdenom = 
            _mm256_add_pd(fdenom,_mm256_mul_pd(_mm256_set1_pd(-0.14),flogpr)); 
          __m256d fquan = _mm256_div_pd(flogpr,fdenom);
          fquan = 
            _mm256_div_pd(fcent,_mm256_add_pd(_mm256_set1_pd(1.0),_mm256_mul_pd(fquan,fquan))); 
          __m256d ftemp = _mm256_mul_pd(fquan,_mm256_set1_pd(DLn10));
          {
            __m128d lower = _mm256_extractf128_pd(ftemp,0);
            __m128d upper = _mm256_extractf128_pd(ftemp,1);
            ftemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          forward = _mm256_mul_pd(rr_kinf,pr);
          forward = 
            _mm256_div_pd(forward,_mm256_add_pd(_mm256_set1_pd(1.0),pr)); 
          forward = _mm256_mul_pd(forward, ftemp);
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[73+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        xik = _mm256_add_pd(xik,cgspl[438+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,prt[idx]);
        rr_f[584+idx] = forward;
        rr_f[584+idx] = _mm256_mul_pd(rr_f[584+idx],mole_frac[73+idx]);
        rr_f[584+idx] = _mm256_mul_pd(rr_f[584+idx],mole_frac[365+idx]);
        rr_r[584+idx] = reverse;
        rr_r[584+idx] = _mm256_mul_pd(rr_r[584+idx],mole_frac[438+idx]);
      }
      //   9)  H + HO2 <=> H2 + O2
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-823.0), ortc[idx]);
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(1.832532226988623e+14),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[0+idx];
        xik = _mm256_add_pd(xik,cgspl[73+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        xik = _mm256_sub_pd(xik,cgspl[438+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[657+idx] = forward;
        rr_f[657+idx] = _mm256_mul_pd(rr_f[657+idx],mole_frac[365+idx]);
        rr_f[657+idx] = _mm256_mul_pd(rr_f[657+idx],mole_frac[438+idx]);
        rr_r[657+idx] = reverse;
        rr_r[657+idx] = _mm256_mul_pd(rr_r[657+idx],mole_frac[0+idx]);
        rr_r[657+idx] = _mm256_mul_pd(rr_r[657+idx],mole_frac[73+idx]);
      }
      //  10)  H + HO2 <=> 2 OH
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-295.0), ortc[idx]);
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(7.814756406537628e+14),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_mul_pd(_mm256_set1_pd(double(2)),cgspl[219+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        xik = _mm256_sub_pd(xik,cgspl[438+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[730+idx] = forward;
        rr_f[730+idx] = _mm256_mul_pd(rr_f[730+idx],mole_frac[365+idx]);
        rr_f[730+idx] = _mm256_mul_pd(rr_f[730+idx],mole_frac[438+idx]);
        rr_r[730+idx] = reverse;
        rr_r[730+idx] = _mm256_mul_pd(rr_r[730+idx],mole_frac[219+idx]);
        rr_r[730+idx] = _mm256_mul_pd(rr_r[730+idx],mole_frac[219+idx]);
      }
      //  11)  O + HO2 <=> O2 + OH
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_set1_pd(3.587788998622304e+14);
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[73+idx];
        xik = _mm256_sub_pd(xik,cgspl[146+idx]);
        xik = _mm256_add_pd(xik,cgspl[219+idx]);
        xik = _mm256_sub_pd(xik,cgspl[438+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[803+idx] = forward;
        rr_f[803+idx] = _mm256_mul_pd(rr_f[803+idx],mole_frac[146+idx]);
        rr_f[803+idx] = _mm256_mul_pd(rr_f[803+idx],mole_frac[438+idx]);
        rr_r[803+idx] = reverse;
        rr_r[803+idx] = _mm256_mul_pd(rr_r[803+idx],mole_frac[73+idx]);
        rr_r[803+idx] = _mm256_mul_pd(rr_r[803+idx],mole_frac[219+idx]);
      }
      //  12)  OH + HO2 <=> O2 + H2O
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(497.0), ortc[idx]);
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(3.190372371082602e+14),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[73+idx];
        xik = _mm256_sub_pd(xik,cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[292+idx]);
        xik = _mm256_sub_pd(xik,cgspl[438+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[876+idx] = forward;
        rr_f[876+idx] = _mm256_mul_pd(rr_f[876+idx],mole_frac[219+idx]);
        rr_f[876+idx] = _mm256_mul_pd(rr_f[876+idx],mole_frac[438+idx]);
        rr_r[876+idx] = reverse;
        rr_r[876+idx] = _mm256_mul_pd(rr_r[876+idx],mole_frac[73+idx]);
        rr_r[876+idx] = _mm256_mul_pd(rr_r[876+idx],mole_frac[292+idx]);
      }
      //  13, 14)  2 HO2 <=> O2 + H2O2
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-1.1982e+04), 
              ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(4.636527321296516e+15),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(1.6293e+03), 
              ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(1.435115599448922e+12),arrtemp); 
            next = arrtemp;
          }
          forward = _mm256_add_pd(forward,next);
        }
        __m256d xik = cgspl[73+idx];
        xik = 
          _mm256_add_pd(xik,_mm256_mul_pd(_mm256_set1_pd(double(-2)),cgspl[438+idx])); 
        xik = _mm256_add_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[949+idx] = forward;
        rr_f[949+idx] = _mm256_mul_pd(rr_f[949+idx],mole_frac[438+idx]);
        rr_f[949+idx] = _mm256_mul_pd(rr_f[949+idx],mole_frac[438+idx]);
        rr_r[949+idx] = reverse;
        rr_r[949+idx] = _mm256_mul_pd(rr_r[949+idx],mole_frac[73+idx]);
        rr_r[949+idx] = _mm256_mul_pd(rr_r[949+idx],mole_frac[511+idx]);
      }
      //  14)  H2O2 (+M) <=> 2 OH (+M)
      {
        __m256d forward, reverse;
        __m256d rr_k0, rr_kinf;
        {
          __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-4.55e+04), ortc[idx]);
          {
            __m128d lower = _mm256_extractf128_pd(arrtemp,0);
            __m128d upper = _mm256_extractf128_pd(arrtemp,1);
            arrtemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          arrtemp = 
            _mm256_mul_pd(_mm256_set1_pd(1.326929961952003e+18),arrtemp); 
          rr_k0 = arrtemp;
        }
        {
          __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-4.843e+04), 
            ortc[idx]); 
          {
            __m128d lower = _mm256_extractf128_pd(arrtemp,0);
            __m128d upper = _mm256_extractf128_pd(arrtemp,1);
            arrtemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          arrtemp = 
            _mm256_mul_pd(_mm256_set1_pd(3.257712410749052e+15),arrtemp); 
          rr_kinf = arrtemp;
        }
        __m256d pr = 
          _mm256_mul_pd(_mm256_div_pd(rr_k0,rr_kinf),thbctemp[0+idx]); 
        __m256d fcent;
        {
          fcent = _mm256_mul_pd(temperature[idx], _mm256_set1_pd(-1.2e+32));
          {
            __m128d lower = _mm256_extractf128_pd(fcent,0);
            __m128d upper = _mm256_extractf128_pd(fcent,1);
            fcent = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          fcent = _mm256_mul_pd(fcent, _mm256_set1_pd(0.5));
          {
            __m256d fctemp = _mm256_mul_pd(temperature[idx], 
              _mm256_set1_pd(-1.2e-28)); 
            {
              __m128d lower = _mm256_extractf128_pd(fctemp,0);
              __m128d upper = _mm256_extractf128_pd(fctemp,1);
              fctemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            fcent = _mm256_add_pd(fcent, _mm256_mul_pd(fctemp, 
              _mm256_set1_pd(0.5))); 
          }
          {
            __m128d lower = _mm256_extractf128_pd(fcent,0);
            __m128d upper = _mm256_extractf128_pd(fcent,1);
            fcent = 
              _mm256_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log10(_mm_cvtsd_f64(upper)),log10(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log10(_mm_cvtsd_f64(lower))); 
          }
          __m256d flogpr;
          __m128d lower = _mm256_extractf128_pd(pr,0);
          __m128d upper = _mm256_extractf128_pd(pr,1);
          flogpr = 
            _mm256_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log10(_mm_cvtsd_f64(upper)),log10(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log10(_mm_cvtsd_f64(lower))); 
          flogpr = _mm256_sub_pd(flogpr, 
            _mm256_add_pd(_mm256_set1_pd(0.4),_mm256_mul_pd(_mm256_set1_pd(0.67),fcent))); 
          __m256d fdenom = 
            _mm256_add_pd(_mm256_set1_pd(0.75),_mm256_mul_pd(_mm256_set1_pd(-1.27),fcent)); 
          fdenom = 
            _mm256_add_pd(fdenom,_mm256_mul_pd(_mm256_set1_pd(-0.14),flogpr)); 
          __m256d fquan = _mm256_div_pd(flogpr,fdenom);
          fquan = 
            _mm256_div_pd(fcent,_mm256_add_pd(_mm256_set1_pd(1.0),_mm256_mul_pd(fquan,fquan))); 
          __m256d ftemp = _mm256_mul_pd(fquan,_mm256_set1_pd(DLn10));
          {
            __m128d lower = _mm256_extractf128_pd(ftemp,0);
            __m128d upper = _mm256_extractf128_pd(ftemp,1);
            ftemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          forward = _mm256_mul_pd(rr_kinf,pr);
          forward = 
            _mm256_div_pd(forward,_mm256_add_pd(_mm256_set1_pd(1.0),pr)); 
          forward = _mm256_mul_pd(forward, ftemp);
        }
        __m256d xik = _mm256_mul_pd(_mm256_set1_pd(double(2)),cgspl[219+idx]);
        xik = _mm256_sub_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,oprt[idx]);
        rr_f[1022+idx] = forward;
        rr_f[1022+idx] = _mm256_mul_pd(rr_f[1022+idx],mole_frac[511+idx]);
        rr_r[1022+idx] = reverse;
        rr_r[1022+idx] = _mm256_mul_pd(rr_r[1022+idx],mole_frac[219+idx]);
        rr_r[1022+idx] = _mm256_mul_pd(rr_r[1022+idx],mole_frac[219+idx]);
      }
      //  15)  H + H2O2 <=> OH + H2O
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-3.97e+03), 
              ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(2.660483534363001e+14),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[219+idx];
        xik = _mm256_add_pd(xik,cgspl[292+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        xik = _mm256_sub_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[1095+idx] = forward;
        rr_f[1095+idx] = _mm256_mul_pd(rr_f[1095+idx],mole_frac[365+idx]);
        rr_f[1095+idx] = _mm256_mul_pd(rr_f[1095+idx],mole_frac[511+idx]);
        rr_r[1095+idx] = reverse;
        rr_r[1095+idx] = _mm256_mul_pd(rr_r[1095+idx],mole_frac[219+idx]);
        rr_r[1095+idx] = _mm256_mul_pd(rr_r[1095+idx],mole_frac[292+idx]);
      }
      //  16)  H + H2O2 <=> H2 + HO2
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-7.95e+03), 
              ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(5.320967068726001e+14),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[0+idx];
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        xik = _mm256_add_pd(xik,cgspl[438+idx]);
        xik = _mm256_sub_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[1168+idx] = forward;
        rr_f[1168+idx] = _mm256_mul_pd(rr_f[1168+idx],mole_frac[365+idx]);
        rr_f[1168+idx] = _mm256_mul_pd(rr_f[1168+idx],mole_frac[511+idx]);
        rr_r[1168+idx] = reverse;
        rr_r[1168+idx] = _mm256_mul_pd(rr_r[1168+idx],mole_frac[0+idx]);
        rr_r[1168+idx] = _mm256_mul_pd(rr_r[1168+idx],mole_frac[438+idx]);
      }
      //  17)  O + H2O2 <=> OH + HO2
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(-3.97e+03),ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(1.518131517201658e+12),temperature[idx]),temperature[idx]),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[146+idx]);
        xik = _mm256_add_pd(xik,cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[438+idx]);
        xik = _mm256_sub_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[1241+idx] = forward;
        rr_f[1241+idx] = _mm256_mul_pd(rr_f[1241+idx],mole_frac[146+idx]);
        rr_f[1241+idx] = _mm256_mul_pd(rr_f[1241+idx],mole_frac[511+idx]);
        rr_r[1241+idx] = reverse;
        rr_r[1241+idx] = _mm256_mul_pd(rr_r[1241+idx],mole_frac[219+idx]);
        rr_r[1241+idx] = _mm256_mul_pd(rr_r[1241+idx],mole_frac[438+idx]);
      }
      //  18, 20)  OH + H2O2 <=> H2O + HO2
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_set1_pd(1.103935076499171e+13);
            next = arrtemp;
          }
          forward = next;
        }
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-9.557e+03), 
              ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(6.402823443695189e+15),arrtemp); 
            next = arrtemp;
          }
          forward = _mm256_add_pd(forward,next);
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[292+idx]);
        xik = _mm256_add_pd(xik,cgspl[438+idx]);
        xik = _mm256_sub_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[1314+idx] = forward;
        rr_f[1314+idx] = _mm256_mul_pd(rr_f[1314+idx],mole_frac[219+idx]);
        rr_f[1314+idx] = _mm256_mul_pd(rr_f[1314+idx],mole_frac[511+idx]);
        rr_r[1314+idx] = reverse;
        rr_r[1314+idx] = _mm256_mul_pd(rr_r[1314+idx],mole_frac[292+idx]);
        rr_r[1314+idx] = _mm256_mul_pd(rr_r[1314+idx],mole_frac[438+idx]);
      }
    }
    // Output rates
    for (unsigned idx = 0; idx < 73; idx++)
    {
      __m256d ropl[19];
      ropl[0] = _mm256_sub_pd(rr_f[0+idx],rr_r[0+idx]);
      ropl[1] = _mm256_sub_pd(rr_f[73+idx],rr_r[73+idx]);
      ropl[2] = _mm256_sub_pd(rr_f[146+idx],rr_r[146+idx]);
      ropl[3] = _mm256_sub_pd(rr_f[219+idx],rr_r[219+idx]);
      ropl[4] = _mm256_sub_pd(rr_f[292+idx],rr_r[292+idx]);
      ropl[5] = _mm256_sub_pd(rr_f[365+idx],rr_r[365+idx]);
      ropl[6] = _mm256_sub_pd(rr_f[438+idx],rr_r[438+idx]);
      ropl[7] = _mm256_sub_pd(rr_f[511+idx],rr_r[511+idx]);
      ropl[8] = _mm256_sub_pd(rr_f[584+idx],rr_r[584+idx]);
      ropl[9] = _mm256_sub_pd(rr_f[657+idx],rr_r[657+idx]);
      ropl[10] = _mm256_sub_pd(rr_f[730+idx],rr_r[730+idx]);
      ropl[11] = _mm256_sub_pd(rr_f[803+idx],rr_r[803+idx]);
      ropl[12] = _mm256_sub_pd(rr_f[876+idx],rr_r[876+idx]);
      ropl[13] = _mm256_sub_pd(rr_f[949+idx],rr_r[949+idx]);
      ropl[14] = _mm256_sub_pd(rr_f[1022+idx],rr_r[1022+idx]);
      ropl[15] = _mm256_sub_pd(rr_f[1095+idx],rr_r[1095+idx]);
      ropl[16] = _mm256_sub_pd(rr_f[1168+idx],rr_r[1168+idx]);
      ropl[17] = _mm256_sub_pd(rr_f[1241+idx],rr_r[1241+idx]);
      ropl[18] = _mm256_sub_pd(rr_f[1314+idx],rr_r[1314+idx]);
      // 0. H2
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_sub_pd(result,ropl[1]);
        result = _mm256_sub_pd(result,ropl[2]);
        result = _mm256_sub_pd(result,ropl[4]);
        result = _mm256_add_pd(result,ropl[9]);
        result = _mm256_add_pd(result,ropl[16]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(2.01594e-03));
        _mm256_store_pd(wdot_array+(0*spec_stride)+(idx<<2),result);
      }
      // 1. O2
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_sub_pd(result,ropl[0]);
        result = _mm256_add_pd(result,ropl[5]);
        result = _mm256_sub_pd(result,ropl[8]);
        result = _mm256_add_pd(result,ropl[9]);
        result = _mm256_add_pd(result,ropl[11]);
        result = _mm256_add_pd(result,ropl[12]);
        result = _mm256_add_pd(result,ropl[13]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.0319988));
        _mm256_store_pd(wdot_array+(1*spec_stride)+(idx<<2),result);
      }
      // 2. O
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_add_pd(result,ropl[0]);
        result = _mm256_sub_pd(result,ropl[1]);
        result = _mm256_sub_pd(result,ropl[3]);
        result = 
          _mm256_sub_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[5])); 
        result = _mm256_sub_pd(result,ropl[6]);
        result = _mm256_sub_pd(result,ropl[11]);
        result = _mm256_sub_pd(result,ropl[17]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.0159994));
        _mm256_store_pd(wdot_array+(2*spec_stride)+(idx<<2),result);
      }
      // 3. OH
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_add_pd(result,ropl[0]);
        result = _mm256_add_pd(result,ropl[1]);
        result = _mm256_sub_pd(result,ropl[2]);
        result = 
          _mm256_add_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[3])); 
        result = _mm256_add_pd(result,ropl[6]);
        result = _mm256_sub_pd(result,ropl[7]);
        result = 
          _mm256_add_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[10])); 
        result = _mm256_add_pd(result,ropl[11]);
        result = _mm256_sub_pd(result,ropl[12]);
        result = 
          _mm256_add_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[14])); 
        result = _mm256_add_pd(result,ropl[15]);
        result = _mm256_add_pd(result,ropl[17]);
        result = _mm256_sub_pd(result,ropl[18]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.01700737));
        _mm256_store_pd(wdot_array+(3*spec_stride)+(idx<<2),result);
      }
      // 4. H2O
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_add_pd(result,ropl[2]);
        result = _mm256_sub_pd(result,ropl[3]);
        result = _mm256_add_pd(result,ropl[7]);
        result = _mm256_add_pd(result,ropl[12]);
        result = _mm256_add_pd(result,ropl[15]);
        result = _mm256_add_pd(result,ropl[18]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.01801534));
        _mm256_store_pd(wdot_array+(4*spec_stride)+(idx<<2),result);
      }
      // 5. H
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_sub_pd(result,ropl[0]);
        result = _mm256_add_pd(result,ropl[1]);
        result = _mm256_add_pd(result,ropl[2]);
        result = 
          _mm256_add_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[4])); 
        result = _mm256_sub_pd(result,ropl[6]);
        result = _mm256_sub_pd(result,ropl[7]);
        result = _mm256_sub_pd(result,ropl[8]);
        result = _mm256_sub_pd(result,ropl[9]);
        result = _mm256_sub_pd(result,ropl[10]);
        result = _mm256_sub_pd(result,ropl[15]);
        result = _mm256_sub_pd(result,ropl[16]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(1.00797e-03));
        _mm256_store_pd(wdot_array+(5*spec_stride)+(idx<<2),result);
      }
      // 6. HO2
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_add_pd(result,ropl[8]);
        result = _mm256_sub_pd(result,ropl[9]);
        result = _mm256_sub_pd(result,ropl[10]);
        result = _mm256_sub_pd(result,ropl[11]);
        result = _mm256_sub_pd(result,ropl[12]);
        result = 
          _mm256_sub_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[13])); 
        result = _mm256_add_pd(result,ropl[16]);
        result = _mm256_add_pd(result,ropl[17]);
        result = _mm256_add_pd(result,ropl[18]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.03300677));
        _mm256_store_pd(wdot_array+(6*spec_stride)+(idx<<2),result);
      }
      // 7. H2O2
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_add_pd(result,ropl[13]);
        result = _mm256_sub_pd(result,ropl[14]);
        result = _mm256_sub_pd(result,ropl[15]);
        result = _mm256_sub_pd(result,ropl[16]);
        result = _mm256_sub_pd(result,ropl[17]);
        result = _mm256_sub_pd(result,ropl[18]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.03401473999999999));
        _mm256_store_pd(wdot_array+(7*spec_stride)+(idx<<2),result);
      }
      // 8. N2
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.0280134));
        _mm256_store_pd(wdot_array+(8*spec_stride)+(idx<<2),result);
      }
    }
    remaining_elmts -= 292;
    mass_frac_array += 292;
    wdot_array += 292;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 4) == 0);
    // Temperature dependent
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      temperature[idx] = _mm256_load_pd(temperature_array);
      temperature_array += 4;
      otc[idx] = _mm256_div_pd(_mm256_set1_pd(1.0),temperature[idx]);
      ortc[idx] = 
        _mm256_div_pd(_mm256_set1_pd(1.0),_mm256_mul_pd(temperature[idx],_mm256_set1_pd(R0c))); 
      {
        __m128d lower = _mm256_extractf128_pd(temperature[idx],0);
        __m128d upper = _mm256_extractf128_pd(temperature[idx],1);
        vlntemp[idx] = 
          _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      }
      prt[idx] = 
        _mm256_div_pd(_mm256_set1_pd(PA),_mm256_mul_pd(temperature[idx],_mm256_set1_pd(R0))); 
      oprt[idx] = _mm256_div_pd(_mm256_set1_pd(1.0),prt[idx]);
    }
    // Gibbs computation
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d tk1 = temperature[idx];
      __m256d tklog = vlntemp[idx];
      __m256d tk2 = _mm256_mul_pd(tk1,tk1);
      __m256d tk3 = _mm256_mul_pd(tk1,tk2);
      __m256d tk4 = _mm256_mul_pd(tk1,tk3);
      __m256d tk5 = _mm256_mul_pd(tk1,tk4);
      __m128d lower = _mm256_extractf128_pd(tk1,0);
      __m128d upper = _mm256_extractf128_pd(tk1,1);
      // Species H2
      {
        __m256d cgspl_NC_H2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  0); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  1); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  2); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  3); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  4); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  5); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  6); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  7); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  8); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  9); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  10); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  11); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  12); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  13); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  14); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.991423),_mm256_set1_pd(3.298124), 
                  15); 
                cgspl_NC_H2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2 = _mm256_mul_pd(cgspl_NC_H2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.04200386399999999),_mm256_set1_pd(-0.04949665199999999), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.352118959999999e-04),_mm256_set1_pd(1.954323599999999e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.329347231999999e-06),_mm256_set1_pd(1.364462495999999e-05), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.640997273599998e-08),_mm256_set1_pd(-4.287035289599994e-06),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.35511),_mm256_set1_pd(-3.294094),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.958616666666668),_mm256_set1_pd(-8.437675),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2 = _mm256_add_pd(cgspl_NC_H2,temp);
              }
            }
          }
        }
        cgspl[0+idx] = cgspl_NC_H2;
      }
      // Species O2
      {
        __m256d cgspl_NC_O2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  0); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  1); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  2); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  3); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  4); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  5); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  6); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  7); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  8); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  9); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  10); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  11); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  12); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  13); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  14); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.697578),_mm256_set1_pd(3.212936), 
                  15); 
                cgspl_NC_O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O2 = _mm256_mul_pd(cgspl_NC_O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.03681118199999999),_mm256_set1_pd(-0.06764915999999999), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.021220799999999e-04),_mm256_set1_pd(1.381476e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-2.556404639999998e-06),_mm256_set1_pd(-1.891982879999998e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.178255807999999e-08),_mm256_set1_pd(9.091236787199989e-06),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.189166),_mm256_set1_pd(6.034738),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-10.28275),_mm256_set1_pd(-8.377075000000001),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O2 = _mm256_add_pd(cgspl_NC_O2,temp);
              }
            }
          }
        }
        cgspl[73+idx] = cgspl_NC_O2;
      }
      // Species O
      {
        __m256d cgspl_NC_O, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  0); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  1); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  2); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  3); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  4); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  5); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  6); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  7); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  8); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  9); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  10); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  11); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  12); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  13); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  14); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.54206),_mm256_set1_pd(2.946429), 
                  15); 
                cgspl_NC_O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_O = _mm256_mul_pd(cgspl_NC_O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.6530372e-03),_mm256_set1_pd(0.09828995999999997), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.446727199999996e-06),_mm256_set1_pd(-5.810476799999997e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-6.553536479999994e-07),_mm256_set1_pd(2.308093919999998e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.528796313599995e-09),_mm256_set1_pd(-4.033873612799995e-06),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.920308),_mm256_set1_pd(2.963995),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(243.59),_mm256_set1_pd(242.897),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_O = _mm256_add_pd(cgspl_NC_O,temp);
              }
            }
          }
        }
        cgspl[146+idx] = cgspl_NC_O;
      }
      // Species OH
      {
        __m256d cgspl_NC_OH, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  0); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  1); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  2); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  3); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  4); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  5); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  6); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  7); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  8); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  9); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  10); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  11); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  12); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  13); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  14); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.86472886),_mm256_set1_pd(4.12530561), 
                  15); 
                cgspl_NC_OH = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_OH = _mm256_mul_pd(cgspl_NC_OH,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.06339026879999998),_mm256_set1_pd(0.1935269634), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.217986191999998e-04),_mm256_set1_pd(-0.015666352584), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-4.395148905599995e-06),_mm256_set1_pd(8.349892459199992e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.380974842367998e-08),_mm256_set1_pd(-2.138269145471997e-05),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.70164073),_mm256_set1_pd(-0.69043296),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(30.69690625),_mm256_set1_pd(27.88590941666667),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_OH = _mm256_add_pd(cgspl_NC_OH,temp);
              }
            }
          }
        }
        cgspl[219+idx] = cgspl_NC_OH;
      }
      // Species H2O
      {
        __m256d cgspl_NC_H2O, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  0); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  1); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  2); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  3); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  4); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  5); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  6); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  7); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  8); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  9); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  10); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  11); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  12); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  13); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  14); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.672146),_mm256_set1_pd(3.386842), 
                  15); 
                cgspl_NC_H2O = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O = _mm256_mul_pd(cgspl_NC_H2O,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.18337758),_mm256_set1_pd(-0.2084989199999999), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.095262399999999e-03),_mm256_set1_pd(0.01525127039999999), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.729434239999998e-05),_mm256_set1_pd(-1.003475663999999e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.626829542399991e-08),_mm256_set1_pd(2.598830438399997e-05),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(6.862817),_mm256_set1_pd(2.590233),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-249.1600833333334),_mm256_set1_pd(-251.7342500000001),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O = _mm256_add_pd(cgspl_NC_H2O,temp);
              }
            }
          }
        }
        cgspl[292+idx] = cgspl_NC_H2O;
      }
      // Species H
      {
        __m256d cgspl_NC_H, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 0); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 1); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 2); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 3); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 4); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 5); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 6); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 7); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 8); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 9); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 10); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 11); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 12); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 13); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 14); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.5),_mm256_set1_pd(2.5), 15); 
                cgspl_NC_H = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H = _mm256_mul_pd(cgspl_NC_H,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.0),_mm256_set1_pd(-0.0),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.4601176),_mm256_set1_pd(-0.4601176),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(212.2635833333334),_mm256_set1_pd(212.2635833333334),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H = _mm256_add_pd(cgspl_NC_H,temp);
              }
            }
          }
        }
        cgspl[365+idx] = cgspl_NC_H;
      }
      // Species HO2
      {
        __m256d cgspl_NC_HO2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  0); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  1); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  2); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  3); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  4); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  5); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  6); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  7); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  8); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  9); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  10); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  11); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  12); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  13); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  14); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.0172109),_mm256_set1_pd(4.30179801), 
                  15); 
                cgspl_NC_HO2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_HO2 = _mm256_mul_pd(cgspl_NC_HO2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.1343892078),_mm256_set1_pd(0.2849472305999999), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.520779559999999e-03),_mm256_set1_pd(-0.05077989383999998), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.645147727999999e-05),_mm256_set1_pd(3.495800073599997e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.118795690879999e-07),_mm256_set1_pd(-9.634206085631988e-05),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.78510215),_mm256_set1_pd(3.71666245),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.9321392750000002),_mm256_set1_pd(2.456733666666667),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_HO2 = _mm256_add_pd(cgspl_NC_HO2,temp);
              }
            }
          }
        }
        cgspl[438+idx] = cgspl_NC_HO2;
      }
      // Species H2O2
      {
        __m256d cgspl_NC_H2O2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  0); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  1); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  2); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  3); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  4); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  5); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  6); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  7); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  8); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  9); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  10); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  11); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  12); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  13); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  14); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(4.573167),_mm256_set1_pd(3.388754), 
                  15); 
                cgspl_NC_H2O2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_H2O2 = _mm256_mul_pd(cgspl_NC_H2O2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.2601681599999999),_mm256_set1_pd(-0.3941535599999999), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(3.539253599999998e-03),_mm256_set1_pd(3.564031199999998e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-3.382421759999996e-05),_mm256_set1_pd(6.661160639999993e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.484338867199998e-07),_mm256_set1_pd(-2.562466751999997e-05),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(0.5011370000000001),_mm256_set1_pd(6.785363),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-150.058),_mm256_set1_pd(-147.1929166666667),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_H2O2 = _mm256_add_pd(cgspl_NC_H2O2,temp);
              }
            }
          }
        }
        cgspl[511+idx] = cgspl_NC_H2O2;
      }
      // Species N2
      {
        __m256d cgspl_NC_N2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  0); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  0); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  0); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  0); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),0); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),0); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),0); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  1); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  1); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  1); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  1); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),1); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),1); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),1); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  2); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  2); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  2); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  2); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),2); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),2); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),2); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  3); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  3); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  3); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  3); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),3); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),3); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),3); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  4); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  4); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  4); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  4); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),4); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),4); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),4); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  5); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  5); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  5); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  5); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),5); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),5); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),5); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  6); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  6); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  6); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  6); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),6); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),6); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),6); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  7); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  7); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  7); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  7); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),7); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),7); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),7); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
          }
        }
        else
        {
          if (_mm_cvtsd_f64(upper) > 8.333333333333336)
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  8); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  8); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  8); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  8); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),8); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),8); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),8); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  9); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  9); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  9); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  9); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),9); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),9); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),9); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  10); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  10); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  10); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  10); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),10); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),10); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),10); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  11); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  11); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  11); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  11); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),11); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),11); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),11); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
          }
          else
          {
            if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > 
              8.333333333333336) 
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  12); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  12); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  12); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  12); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),12); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),12); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),12); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  13); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  13); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  13); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  13); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),13); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),13); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),13); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
            else
            {
              if (_mm_cvtsd_f64(lower) > 8.333333333333336)
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  14); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  14); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  14); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  14); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),14); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),14); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),14); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
              else
              {
                __m256d coeff1 = 
                  _mm256_blend_pd(_mm256_set1_pd(2.92664),_mm256_set1_pd(3.298677), 
                  15); 
                cgspl_NC_N2 = _mm256_mul_pd(coeff1,tk1);
                __m256d coefflog = _mm256_set1_pd(-3.787491742782046);
                temp = _mm256_sub_pd(coefflog,tklog);
                cgspl_NC_N2 = _mm256_mul_pd(cgspl_NC_N2,temp);
                __m256d coeff2 = 
                  _mm256_blend_pd(_mm256_set1_pd(-0.08927861999999998),_mm256_set1_pd(-0.08449439999999998), 
                  15); 
                temp = _mm256_mul_pd(coeff2,tk2);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff3 = 
                  _mm256_blend_pd(_mm256_set1_pd(1.364342639999999e-03),_mm256_set1_pd(9.511732799999995e-03), 
                  15); 
                temp = _mm256_mul_pd(coeff3,tk3);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff4 = 
                  _mm256_blend_pd(_mm256_set1_pd(-1.453973759999998e-05),_mm256_set1_pd(-8.123781599999992e-04), 
                  15); 
                temp = _mm256_mul_pd(coeff4,tk4);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff5 = 
                  _mm256_blend_pd(_mm256_set1_pd(7.001874316799991e-08),_mm256_set1_pd(2.534825663999997e-05),15); 
                temp = _mm256_mul_pd(coeff5,tk5);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
                __m256d coeff7 = 
                  _mm256_blend_pd(_mm256_set1_pd(5.980528),_mm256_set1_pd(3.950372),15); 
                temp = _mm256_mul_pd(coeff7,tk1);
                __m256d coeff6 = 
                  _mm256_blend_pd(_mm256_set1_pd(-7.689980833333334),_mm256_set1_pd(-8.507500000000002),15); 
                temp = _mm256_sub_pd(coeff6,temp);
                cgspl_NC_N2 = _mm256_add_pd(cgspl_NC_N2,temp);
              }
            }
          }
        }
        cgspl[584+idx] = cgspl_NC_N2;
      }
    }
    // Compute mole fractions
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d avmolwt = _mm256_load_pd(avmolwt_array);
      __m256d pressure = _mm256_load_pd(pressure_array);
      avmolwt_array += 4;
      pressure_array += 4;
      __m256d sumyow = 
        _mm256_mul_pd(_mm256_mul_pd(temperature[idx],avmolwt),_mm256_set1_pd(7.03444160806564)); 
      sumyow = _mm256_div_pd(pressure,sumyow);
      __m256d mask, mass_frac;
      __m256d small = _mm256_set1_pd(1e-200);
      mass_frac = _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[0])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[0+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[1])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[73+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[2])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[146+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[3])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[219+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[4])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[292+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[5])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[365+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[6])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[438+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[7])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[511+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
      mass_frac = _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2));
      mass_frac = 
        _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[8])); 
      mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);
      mass_frac = _mm256_and_pd(mass_frac,mask);
      mole_frac[584+idx] = 
        _mm256_mul_pd(sumyow,_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small))); 
    }
    // Compute third body values
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d ctot = _mm256_set1_pd(0.0);
      ctot = _mm256_add_pd(ctot,mole_frac[0+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[73+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[146+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[219+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[292+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[365+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[438+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[511+idx]);
      ctot = _mm256_add_pd(ctot,mole_frac[584+idx]);
      thbctemp[0+idx] = ctot;
      thbctemp[0+idx] = 
        _mm256_add_pd(thbctemp[0+idx],_mm256_mul_pd(_mm256_set1_pd(1.5),mole_frac[0+idx])); 
      thbctemp[0+idx] = 
        _mm256_add_pd(thbctemp[0+idx],_mm256_mul_pd(_mm256_set1_pd(11.0),mole_frac[292+idx])); 
      thbctemp[73+idx] = ctot;
      thbctemp[73+idx] = _mm256_add_pd(thbctemp[73+idx],mole_frac[0+idx]);
      thbctemp[73+idx] = 
        _mm256_sub_pd(thbctemp[73+idx],_mm256_mul_pd(_mm256_set1_pd(0.22),mole_frac[73+idx])); 
      thbctemp[73+idx] = 
        _mm256_add_pd(thbctemp[73+idx],_mm256_mul_pd(_mm256_set1_pd(10.0),mole_frac[292+idx])); 
    }
    // Reaction rates
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      //   0)  O2 + H <=> O + OH
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(-0.406),vlntemp[idx]); 
            arrtemp = 
              _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(1.6599e+04),ortc[idx])); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(5.606052175734568e+15),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[73+idx]);
        xik = _mm256_add_pd(xik,cgspl[146+idx]);
        xik = _mm256_add_pd(xik,cgspl[219+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[0+idx] = forward;
        rr_f[0+idx] = _mm256_mul_pd(rr_f[0+idx],mole_frac[73+idx]);
        rr_f[0+idx] = _mm256_mul_pd(rr_f[0+idx],mole_frac[365+idx]);
        rr_r[0+idx] = reverse;
        rr_r[0+idx] = _mm256_mul_pd(rr_r[0+idx],mole_frac[146+idx]);
        rr_r[0+idx] = _mm256_mul_pd(rr_r[0+idx],mole_frac[219+idx]);
      }
      //   1)  H2 + O <=> OH + H
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(2.67),vlntemp[idx]);
            arrtemp = 
              _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(6.29e+03),ortc[idx])); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(1.996278696128123e+11),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[0+idx]);
        xik = _mm256_sub_pd(xik,cgspl[146+idx]);
        xik = _mm256_add_pd(xik,cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[365+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[73+idx] = forward;
        rr_f[73+idx] = _mm256_mul_pd(rr_f[73+idx],mole_frac[0+idx]);
        rr_f[73+idx] = _mm256_mul_pd(rr_f[73+idx],mole_frac[146+idx]);
        rr_r[73+idx] = reverse;
        rr_r[73+idx] = _mm256_mul_pd(rr_r[73+idx],mole_frac[219+idx]);
        rr_r[73+idx] = _mm256_mul_pd(rr_r[73+idx],mole_frac[365+idx]);
      }
      //   2)  H2 + OH <=> H2O + H
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(1.51),vlntemp[idx]);
            arrtemp = 
              _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(3.43e+03),ortc[idx])); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(3.288220749682686e+12),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[0+idx]);
        xik = _mm256_sub_pd(xik,cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[292+idx]);
        xik = _mm256_add_pd(xik,cgspl[365+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[146+idx] = forward;
        rr_f[146+idx] = _mm256_mul_pd(rr_f[146+idx],mole_frac[0+idx]);
        rr_f[146+idx] = _mm256_mul_pd(rr_f[146+idx],mole_frac[219+idx]);
        rr_r[146+idx] = reverse;
        rr_r[146+idx] = _mm256_mul_pd(rr_r[146+idx],mole_frac[292+idx]);
        rr_r[146+idx] = _mm256_mul_pd(rr_r[146+idx],mole_frac[365+idx]);
      }
      //   3)  O + H2O <=> 2 OH
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(2.02),vlntemp[idx]);
            arrtemp = 
              _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(1.34e+04),ortc[idx])); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(5.195724313044046e+11),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[146+idx]);
        xik = 
          _mm256_add_pd(xik,_mm256_mul_pd(_mm256_set1_pd(double(2)),cgspl[219+idx])); 
        xik = _mm256_sub_pd(xik,cgspl[292+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[219+idx] = forward;
        rr_f[219+idx] = _mm256_mul_pd(rr_f[219+idx],mole_frac[146+idx]);
        rr_f[219+idx] = _mm256_mul_pd(rr_f[219+idx],mole_frac[292+idx]);
        rr_r[219+idx] = reverse;
        rr_r[219+idx] = _mm256_mul_pd(rr_r[219+idx],mole_frac[219+idx]);
        rr_r[219+idx] = _mm256_mul_pd(rr_r[219+idx],mole_frac[219+idx]);
      }
      //   4)  H2 + M <=> 2 H + M
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-1.4),vlntemp[idx]);
            arrtemp = 
              _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(1.0438e+05),ortc[idx])); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(6.203984453113697e+17),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[0+idx]);
        xik = 
          _mm256_add_pd(xik,_mm256_mul_pd(_mm256_set1_pd(double(2)),cgspl[365+idx])); 
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,oprt[idx]);
        rr_f[292+idx] = forward;
        rr_f[292+idx] = _mm256_mul_pd(rr_f[292+idx],mole_frac[0+idx]);
        rr_r[292+idx] = reverse;
        rr_r[292+idx] = _mm256_mul_pd(rr_r[292+idx],mole_frac[365+idx]);
        rr_r[292+idx] = _mm256_mul_pd(rr_r[292+idx],mole_frac[365+idx]);
        rr_f[292+idx] = _mm256_mul_pd(rr_f[292+idx],thbctemp[0+idx]);
        rr_r[292+idx] = _mm256_mul_pd(rr_r[292+idx],thbctemp[0+idx]);
      }
      //   5)  2 O + M <=> O2 + M
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-0.5),vlntemp[idx]);
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = _mm256_mul_pd(_mm256_set1_pd(6.21278022363831e+15), 
              arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[73+idx];
        xik = 
          _mm256_add_pd(xik,_mm256_mul_pd(_mm256_set1_pd(double(-2)),cgspl[146+idx])); 
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,prt[idx]);
        rr_f[365+idx] = forward;
        rr_f[365+idx] = _mm256_mul_pd(rr_f[365+idx],mole_frac[146+idx]);
        rr_f[365+idx] = _mm256_mul_pd(rr_f[365+idx],mole_frac[146+idx]);
        rr_r[365+idx] = reverse;
        rr_r[365+idx] = _mm256_mul_pd(rr_r[365+idx],mole_frac[73+idx]);
        rr_f[365+idx] = _mm256_mul_pd(rr_f[365+idx],thbctemp[0+idx]);
        rr_r[365+idx] = _mm256_mul_pd(rr_r[365+idx],thbctemp[0+idx]);
      }
      //   6)  O + H + M <=> OH + M
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(4.336624958847576e+17),otc[idx]); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[146+idx]);
        xik = _mm256_add_pd(xik,cgspl[219+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,prt[idx]);
        rr_f[438+idx] = forward;
        rr_f[438+idx] = _mm256_mul_pd(rr_f[438+idx],mole_frac[146+idx]);
        rr_f[438+idx] = _mm256_mul_pd(rr_f[438+idx],mole_frac[365+idx]);
        rr_r[438+idx] = reverse;
        rr_r[438+idx] = _mm256_mul_pd(rr_r[438+idx],mole_frac[219+idx]);
        rr_f[438+idx] = _mm256_mul_pd(rr_f[438+idx],thbctemp[0+idx]);
        rr_r[438+idx] = _mm256_mul_pd(rr_r[438+idx],thbctemp[0+idx]);
      }
      //   7)  OH + H + M <=> H2O + M
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = 
              _mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(2.913162007428368e+19),otc[idx]),otc[idx]); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[292+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,prt[idx]);
        rr_f[511+idx] = forward;
        rr_f[511+idx] = _mm256_mul_pd(rr_f[511+idx],mole_frac[219+idx]);
        rr_f[511+idx] = _mm256_mul_pd(rr_f[511+idx],mole_frac[365+idx]);
        rr_r[511+idx] = reverse;
        rr_r[511+idx] = _mm256_mul_pd(rr_r[511+idx],mole_frac[292+idx]);
        rr_f[511+idx] = _mm256_mul_pd(rr_f[511+idx],thbctemp[0+idx]);
        rr_r[511+idx] = _mm256_mul_pd(rr_r[511+idx],thbctemp[0+idx]);
      }
      //   8)  O2 + H (+M) <=> HO2 (+M)
      {
        __m256d forward, reverse;
        __m256d rr_k0, rr_kinf;
        {
          __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-1.72),vlntemp[idx]);
          arrtemp = 
            _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(524.8),ortc[idx])); 
          {
            __m128d lower = _mm256_extractf128_pd(arrtemp,0);
            __m128d upper = _mm256_extractf128_pd(arrtemp,1);
            arrtemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          arrtemp = 
            _mm256_mul_pd(_mm256_set1_pd(1.864740588650248e+18),arrtemp); 
          rr_k0 = arrtemp;
        }
        {
          __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(0.6),vlntemp[idx]);
          {
            __m128d lower = _mm256_extractf128_pd(arrtemp,0);
            __m128d upper = _mm256_extractf128_pd(arrtemp,1);
            arrtemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          arrtemp = _mm256_mul_pd(_mm256_set1_pd(2.879017473981534e+14), 
            arrtemp); 
          rr_kinf = arrtemp;
        }
        __m256d pr = 
          _mm256_mul_pd(_mm256_div_pd(rr_k0,rr_kinf),thbctemp[73+idx]); 
        __m256d fcent;
        {
          fcent = _mm256_mul_pd(temperature[idx], _mm256_set1_pd(-1.2e+32));
          {
            __m128d lower = _mm256_extractf128_pd(fcent,0);
            __m128d upper = _mm256_extractf128_pd(fcent,1);
            fcent = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          fcent = _mm256_mul_pd(fcent, _mm256_set1_pd(0.2));
          {
            __m256d fctemp = _mm256_mul_pd(temperature[idx], 
              _mm256_set1_pd(-1.2e-28)); 
            {
              __m128d lower = _mm256_extractf128_pd(fctemp,0);
              __m128d upper = _mm256_extractf128_pd(fctemp,1);
              fctemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            fcent = _mm256_add_pd(fcent, _mm256_mul_pd(fctemp, 
              _mm256_set1_pd(0.8))); 
          }
          {
            __m128d lower = _mm256_extractf128_pd(fcent,0);
            __m128d upper = _mm256_extractf128_pd(fcent,1);
            fcent = 
              _mm256_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log10(_mm_cvtsd_f64(upper)),log10(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log10(_mm_cvtsd_f64(lower))); 
          }
          __m256d flogpr;
          __m128d lower = _mm256_extractf128_pd(pr,0);
          __m128d upper = _mm256_extractf128_pd(pr,1);
          flogpr = 
            _mm256_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log10(_mm_cvtsd_f64(upper)),log10(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log10(_mm_cvtsd_f64(lower))); 
          flogpr = _mm256_sub_pd(flogpr, 
            _mm256_add_pd(_mm256_set1_pd(0.4),_mm256_mul_pd(_mm256_set1_pd(0.67),fcent))); 
          __m256d fdenom = 
            _mm256_add_pd(_mm256_set1_pd(0.75),_mm256_mul_pd(_mm256_set1_pd(-1.27),fcent)); 
          fdenom = 
            _mm256_add_pd(fdenom,_mm256_mul_pd(_mm256_set1_pd(-0.14),flogpr)); 
          __m256d fquan = _mm256_div_pd(flogpr,fdenom);
          fquan = 
            _mm256_div_pd(fcent,_mm256_add_pd(_mm256_set1_pd(1.0),_mm256_mul_pd(fquan,fquan))); 
          __m256d ftemp = _mm256_mul_pd(fquan,_mm256_set1_pd(DLn10));
          {
            __m128d lower = _mm256_extractf128_pd(ftemp,0);
            __m128d upper = _mm256_extractf128_pd(ftemp,1);
            ftemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          forward = _mm256_mul_pd(rr_kinf,pr);
          forward = 
            _mm256_div_pd(forward,_mm256_add_pd(_mm256_set1_pd(1.0),pr)); 
          forward = _mm256_mul_pd(forward, ftemp);
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[73+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        xik = _mm256_add_pd(xik,cgspl[438+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,prt[idx]);
        rr_f[584+idx] = forward;
        rr_f[584+idx] = _mm256_mul_pd(rr_f[584+idx],mole_frac[73+idx]);
        rr_f[584+idx] = _mm256_mul_pd(rr_f[584+idx],mole_frac[365+idx]);
        rr_r[584+idx] = reverse;
        rr_r[584+idx] = _mm256_mul_pd(rr_r[584+idx],mole_frac[438+idx]);
      }
      //   9)  H + HO2 <=> H2 + O2
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-823.0), ortc[idx]);
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(1.832532226988623e+14),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[0+idx];
        xik = _mm256_add_pd(xik,cgspl[73+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        xik = _mm256_sub_pd(xik,cgspl[438+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[657+idx] = forward;
        rr_f[657+idx] = _mm256_mul_pd(rr_f[657+idx],mole_frac[365+idx]);
        rr_f[657+idx] = _mm256_mul_pd(rr_f[657+idx],mole_frac[438+idx]);
        rr_r[657+idx] = reverse;
        rr_r[657+idx] = _mm256_mul_pd(rr_r[657+idx],mole_frac[0+idx]);
        rr_r[657+idx] = _mm256_mul_pd(rr_r[657+idx],mole_frac[73+idx]);
      }
      //  10)  H + HO2 <=> 2 OH
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-295.0), ortc[idx]);
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(7.814756406537628e+14),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_mul_pd(_mm256_set1_pd(double(2)),cgspl[219+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        xik = _mm256_sub_pd(xik,cgspl[438+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[730+idx] = forward;
        rr_f[730+idx] = _mm256_mul_pd(rr_f[730+idx],mole_frac[365+idx]);
        rr_f[730+idx] = _mm256_mul_pd(rr_f[730+idx],mole_frac[438+idx]);
        rr_r[730+idx] = reverse;
        rr_r[730+idx] = _mm256_mul_pd(rr_r[730+idx],mole_frac[219+idx]);
        rr_r[730+idx] = _mm256_mul_pd(rr_r[730+idx],mole_frac[219+idx]);
      }
      //  11)  O + HO2 <=> O2 + OH
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_set1_pd(3.587788998622304e+14);
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[73+idx];
        xik = _mm256_sub_pd(xik,cgspl[146+idx]);
        xik = _mm256_add_pd(xik,cgspl[219+idx]);
        xik = _mm256_sub_pd(xik,cgspl[438+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[803+idx] = forward;
        rr_f[803+idx] = _mm256_mul_pd(rr_f[803+idx],mole_frac[146+idx]);
        rr_f[803+idx] = _mm256_mul_pd(rr_f[803+idx],mole_frac[438+idx]);
        rr_r[803+idx] = reverse;
        rr_r[803+idx] = _mm256_mul_pd(rr_r[803+idx],mole_frac[73+idx]);
        rr_r[803+idx] = _mm256_mul_pd(rr_r[803+idx],mole_frac[219+idx]);
      }
      //  12)  OH + HO2 <=> O2 + H2O
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(497.0), ortc[idx]);
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(3.190372371082602e+14),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[73+idx];
        xik = _mm256_sub_pd(xik,cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[292+idx]);
        xik = _mm256_sub_pd(xik,cgspl[438+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[876+idx] = forward;
        rr_f[876+idx] = _mm256_mul_pd(rr_f[876+idx],mole_frac[219+idx]);
        rr_f[876+idx] = _mm256_mul_pd(rr_f[876+idx],mole_frac[438+idx]);
        rr_r[876+idx] = reverse;
        rr_r[876+idx] = _mm256_mul_pd(rr_r[876+idx],mole_frac[73+idx]);
        rr_r[876+idx] = _mm256_mul_pd(rr_r[876+idx],mole_frac[292+idx]);
      }
      //  13, 14)  2 HO2 <=> O2 + H2O2
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-1.1982e+04), 
              ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(4.636527321296516e+15),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(1.6293e+03), 
              ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(1.435115599448922e+12),arrtemp); 
            next = arrtemp;
          }
          forward = _mm256_add_pd(forward,next);
        }
        __m256d xik = cgspl[73+idx];
        xik = 
          _mm256_add_pd(xik,_mm256_mul_pd(_mm256_set1_pd(double(-2)),cgspl[438+idx])); 
        xik = _mm256_add_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[949+idx] = forward;
        rr_f[949+idx] = _mm256_mul_pd(rr_f[949+idx],mole_frac[438+idx]);
        rr_f[949+idx] = _mm256_mul_pd(rr_f[949+idx],mole_frac[438+idx]);
        rr_r[949+idx] = reverse;
        rr_r[949+idx] = _mm256_mul_pd(rr_r[949+idx],mole_frac[73+idx]);
        rr_r[949+idx] = _mm256_mul_pd(rr_r[949+idx],mole_frac[511+idx]);
      }
      //  14)  H2O2 (+M) <=> 2 OH (+M)
      {
        __m256d forward, reverse;
        __m256d rr_k0, rr_kinf;
        {
          __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-4.55e+04), ortc[idx]);
          {
            __m128d lower = _mm256_extractf128_pd(arrtemp,0);
            __m128d upper = _mm256_extractf128_pd(arrtemp,1);
            arrtemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          arrtemp = 
            _mm256_mul_pd(_mm256_set1_pd(1.326929961952003e+18),arrtemp); 
          rr_k0 = arrtemp;
        }
        {
          __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-4.843e+04), 
            ortc[idx]); 
          {
            __m128d lower = _mm256_extractf128_pd(arrtemp,0);
            __m128d upper = _mm256_extractf128_pd(arrtemp,1);
            arrtemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          arrtemp = 
            _mm256_mul_pd(_mm256_set1_pd(3.257712410749052e+15),arrtemp); 
          rr_kinf = arrtemp;
        }
        __m256d pr = 
          _mm256_mul_pd(_mm256_div_pd(rr_k0,rr_kinf),thbctemp[0+idx]); 
        __m256d fcent;
        {
          fcent = _mm256_mul_pd(temperature[idx], _mm256_set1_pd(-1.2e+32));
          {
            __m128d lower = _mm256_extractf128_pd(fcent,0);
            __m128d upper = _mm256_extractf128_pd(fcent,1);
            fcent = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          fcent = _mm256_mul_pd(fcent, _mm256_set1_pd(0.5));
          {
            __m256d fctemp = _mm256_mul_pd(temperature[idx], 
              _mm256_set1_pd(-1.2e-28)); 
            {
              __m128d lower = _mm256_extractf128_pd(fctemp,0);
              __m128d upper = _mm256_extractf128_pd(fctemp,1);
              fctemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            fcent = _mm256_add_pd(fcent, _mm256_mul_pd(fctemp, 
              _mm256_set1_pd(0.5))); 
          }
          {
            __m128d lower = _mm256_extractf128_pd(fcent,0);
            __m128d upper = _mm256_extractf128_pd(fcent,1);
            fcent = 
              _mm256_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log10(_mm_cvtsd_f64(upper)),log10(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log10(_mm_cvtsd_f64(lower))); 
          }
          __m256d flogpr;
          __m128d lower = _mm256_extractf128_pd(pr,0);
          __m128d upper = _mm256_extractf128_pd(pr,1);
          flogpr = 
            _mm256_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log10(_mm_cvtsd_f64(upper)),log10(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log10(_mm_cvtsd_f64(lower))); 
          flogpr = _mm256_sub_pd(flogpr, 
            _mm256_add_pd(_mm256_set1_pd(0.4),_mm256_mul_pd(_mm256_set1_pd(0.67),fcent))); 
          __m256d fdenom = 
            _mm256_add_pd(_mm256_set1_pd(0.75),_mm256_mul_pd(_mm256_set1_pd(-1.27),fcent)); 
          fdenom = 
            _mm256_add_pd(fdenom,_mm256_mul_pd(_mm256_set1_pd(-0.14),flogpr)); 
          __m256d fquan = _mm256_div_pd(flogpr,fdenom);
          fquan = 
            _mm256_div_pd(fcent,_mm256_add_pd(_mm256_set1_pd(1.0),_mm256_mul_pd(fquan,fquan))); 
          __m256d ftemp = _mm256_mul_pd(fquan,_mm256_set1_pd(DLn10));
          {
            __m128d lower = _mm256_extractf128_pd(ftemp,0);
            __m128d upper = _mm256_extractf128_pd(ftemp,1);
            ftemp = 
              _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
          }
          forward = _mm256_mul_pd(rr_kinf,pr);
          forward = 
            _mm256_div_pd(forward,_mm256_add_pd(_mm256_set1_pd(1.0),pr)); 
          forward = _mm256_mul_pd(forward, ftemp);
        }
        __m256d xik = _mm256_mul_pd(_mm256_set1_pd(double(2)),cgspl[219+idx]);
        xik = _mm256_sub_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        reverse = _mm256_mul_pd(reverse,oprt[idx]);
        rr_f[1022+idx] = forward;
        rr_f[1022+idx] = _mm256_mul_pd(rr_f[1022+idx],mole_frac[511+idx]);
        rr_r[1022+idx] = reverse;
        rr_r[1022+idx] = _mm256_mul_pd(rr_r[1022+idx],mole_frac[219+idx]);
        rr_r[1022+idx] = _mm256_mul_pd(rr_r[1022+idx],mole_frac[219+idx]);
      }
      //  15)  H + H2O2 <=> OH + H2O
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-3.97e+03), 
              ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(2.660483534363001e+14),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[219+idx];
        xik = _mm256_add_pd(xik,cgspl[292+idx]);
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        xik = _mm256_sub_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[1095+idx] = forward;
        rr_f[1095+idx] = _mm256_mul_pd(rr_f[1095+idx],mole_frac[365+idx]);
        rr_f[1095+idx] = _mm256_mul_pd(rr_f[1095+idx],mole_frac[511+idx]);
        rr_r[1095+idx] = reverse;
        rr_r[1095+idx] = _mm256_mul_pd(rr_r[1095+idx],mole_frac[219+idx]);
        rr_r[1095+idx] = _mm256_mul_pd(rr_r[1095+idx],mole_frac[292+idx]);
      }
      //  16)  H + H2O2 <=> H2 + HO2
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-7.95e+03), 
              ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(5.320967068726001e+14),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = cgspl[0+idx];
        xik = _mm256_sub_pd(xik,cgspl[365+idx]);
        xik = _mm256_add_pd(xik,cgspl[438+idx]);
        xik = _mm256_sub_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[1168+idx] = forward;
        rr_f[1168+idx] = _mm256_mul_pd(rr_f[1168+idx],mole_frac[365+idx]);
        rr_f[1168+idx] = _mm256_mul_pd(rr_f[1168+idx],mole_frac[511+idx]);
        rr_r[1168+idx] = reverse;
        rr_r[1168+idx] = _mm256_mul_pd(rr_r[1168+idx],mole_frac[0+idx]);
        rr_r[1168+idx] = _mm256_mul_pd(rr_r[1168+idx],mole_frac[438+idx]);
      }
      //  17)  O + H2O2 <=> OH + HO2
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(-3.97e+03),ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(1.518131517201658e+12),temperature[idx]),temperature[idx]),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[146+idx]);
        xik = _mm256_add_pd(xik,cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[438+idx]);
        xik = _mm256_sub_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[1241+idx] = forward;
        rr_f[1241+idx] = _mm256_mul_pd(rr_f[1241+idx],mole_frac[146+idx]);
        rr_f[1241+idx] = _mm256_mul_pd(rr_f[1241+idx],mole_frac[511+idx]);
        rr_r[1241+idx] = reverse;
        rr_r[1241+idx] = _mm256_mul_pd(rr_r[1241+idx],mole_frac[219+idx]);
        rr_r[1241+idx] = _mm256_mul_pd(rr_r[1241+idx],mole_frac[438+idx]);
      }
      //  18, 20)  OH + H2O2 <=> H2O + HO2
      {
        __m256d forward, reverse;
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_set1_pd(1.103935076499171e+13);
            next = arrtemp;
          }
          forward = next;
        }
        {
          __m256d next;
          {
            __m256d arrtemp = _mm256_mul_pd(_mm256_set1_pd(-9.557e+03), 
              ortc[idx]); 
            {
              __m128d lower = _mm256_extractf128_pd(arrtemp,0);
              __m128d upper = _mm256_extractf128_pd(arrtemp,1);
              arrtemp = 
                _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
            }
            arrtemp = 
              _mm256_mul_pd(_mm256_set1_pd(6.402823443695189e+15),arrtemp); 
            next = arrtemp;
          }
          forward = _mm256_add_pd(forward,next);
        }
        __m256d xik = _mm256_xor_pd(_mm256_set1_pd(-0.0),cgspl[219+idx]);
        xik = _mm256_add_pd(xik,cgspl[292+idx]);
        xik = _mm256_add_pd(xik,cgspl[438+idx]);
        xik = _mm256_sub_pd(xik,cgspl[511+idx]);
        reverse = _mm256_mul_pd(xik,otc[idx]);
        {
          __m128d lower = _mm256_extractf128_pd(reverse,0);
          __m128d upper = _mm256_extractf128_pd(reverse,1);
          reverse = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        reverse = _mm256_mul_pd(forward,reverse);
        rr_f[1314+idx] = forward;
        rr_f[1314+idx] = _mm256_mul_pd(rr_f[1314+idx],mole_frac[219+idx]);
        rr_f[1314+idx] = _mm256_mul_pd(rr_f[1314+idx],mole_frac[511+idx]);
        rr_r[1314+idx] = reverse;
        rr_r[1314+idx] = _mm256_mul_pd(rr_r[1314+idx],mole_frac[292+idx]);
        rr_r[1314+idx] = _mm256_mul_pd(rr_r[1314+idx],mole_frac[438+idx]);
      }
    }
    // Output rates
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d ropl[19];
      ropl[0] = _mm256_sub_pd(rr_f[0+idx],rr_r[0+idx]);
      ropl[1] = _mm256_sub_pd(rr_f[73+idx],rr_r[73+idx]);
      ropl[2] = _mm256_sub_pd(rr_f[146+idx],rr_r[146+idx]);
      ropl[3] = _mm256_sub_pd(rr_f[219+idx],rr_r[219+idx]);
      ropl[4] = _mm256_sub_pd(rr_f[292+idx],rr_r[292+idx]);
      ropl[5] = _mm256_sub_pd(rr_f[365+idx],rr_r[365+idx]);
      ropl[6] = _mm256_sub_pd(rr_f[438+idx],rr_r[438+idx]);
      ropl[7] = _mm256_sub_pd(rr_f[511+idx],rr_r[511+idx]);
      ropl[8] = _mm256_sub_pd(rr_f[584+idx],rr_r[584+idx]);
      ropl[9] = _mm256_sub_pd(rr_f[657+idx],rr_r[657+idx]);
      ropl[10] = _mm256_sub_pd(rr_f[730+idx],rr_r[730+idx]);
      ropl[11] = _mm256_sub_pd(rr_f[803+idx],rr_r[803+idx]);
      ropl[12] = _mm256_sub_pd(rr_f[876+idx],rr_r[876+idx]);
      ropl[13] = _mm256_sub_pd(rr_f[949+idx],rr_r[949+idx]);
      ropl[14] = _mm256_sub_pd(rr_f[1022+idx],rr_r[1022+idx]);
      ropl[15] = _mm256_sub_pd(rr_f[1095+idx],rr_r[1095+idx]);
      ropl[16] = _mm256_sub_pd(rr_f[1168+idx],rr_r[1168+idx]);
      ropl[17] = _mm256_sub_pd(rr_f[1241+idx],rr_r[1241+idx]);
      ropl[18] = _mm256_sub_pd(rr_f[1314+idx],rr_r[1314+idx]);
      // 0. H2
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_sub_pd(result,ropl[1]);
        result = _mm256_sub_pd(result,ropl[2]);
        result = _mm256_sub_pd(result,ropl[4]);
        result = _mm256_add_pd(result,ropl[9]);
        result = _mm256_add_pd(result,ropl[16]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(2.01594e-03));
        _mm256_store_pd(wdot_array+(0*spec_stride)+(idx<<2),result);
      }
      // 1. O2
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_sub_pd(result,ropl[0]);
        result = _mm256_add_pd(result,ropl[5]);
        result = _mm256_sub_pd(result,ropl[8]);
        result = _mm256_add_pd(result,ropl[9]);
        result = _mm256_add_pd(result,ropl[11]);
        result = _mm256_add_pd(result,ropl[12]);
        result = _mm256_add_pd(result,ropl[13]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.0319988));
        _mm256_store_pd(wdot_array+(1*spec_stride)+(idx<<2),result);
      }
      // 2. O
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_add_pd(result,ropl[0]);
        result = _mm256_sub_pd(result,ropl[1]);
        result = _mm256_sub_pd(result,ropl[3]);
        result = 
          _mm256_sub_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[5])); 
        result = _mm256_sub_pd(result,ropl[6]);
        result = _mm256_sub_pd(result,ropl[11]);
        result = _mm256_sub_pd(result,ropl[17]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.0159994));
        _mm256_store_pd(wdot_array+(2*spec_stride)+(idx<<2),result);
      }
      // 3. OH
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_add_pd(result,ropl[0]);
        result = _mm256_add_pd(result,ropl[1]);
        result = _mm256_sub_pd(result,ropl[2]);
        result = 
          _mm256_add_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[3])); 
        result = _mm256_add_pd(result,ropl[6]);
        result = _mm256_sub_pd(result,ropl[7]);
        result = 
          _mm256_add_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[10])); 
        result = _mm256_add_pd(result,ropl[11]);
        result = _mm256_sub_pd(result,ropl[12]);
        result = 
          _mm256_add_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[14])); 
        result = _mm256_add_pd(result,ropl[15]);
        result = _mm256_add_pd(result,ropl[17]);
        result = _mm256_sub_pd(result,ropl[18]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.01700737));
        _mm256_store_pd(wdot_array+(3*spec_stride)+(idx<<2),result);
      }
      // 4. H2O
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_add_pd(result,ropl[2]);
        result = _mm256_sub_pd(result,ropl[3]);
        result = _mm256_add_pd(result,ropl[7]);
        result = _mm256_add_pd(result,ropl[12]);
        result = _mm256_add_pd(result,ropl[15]);
        result = _mm256_add_pd(result,ropl[18]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.01801534));
        _mm256_store_pd(wdot_array+(4*spec_stride)+(idx<<2),result);
      }
      // 5. H
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_sub_pd(result,ropl[0]);
        result = _mm256_add_pd(result,ropl[1]);
        result = _mm256_add_pd(result,ropl[2]);
        result = 
          _mm256_add_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[4])); 
        result = _mm256_sub_pd(result,ropl[6]);
        result = _mm256_sub_pd(result,ropl[7]);
        result = _mm256_sub_pd(result,ropl[8]);
        result = _mm256_sub_pd(result,ropl[9]);
        result = _mm256_sub_pd(result,ropl[10]);
        result = _mm256_sub_pd(result,ropl[15]);
        result = _mm256_sub_pd(result,ropl[16]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(1.00797e-03));
        _mm256_store_pd(wdot_array+(5*spec_stride)+(idx<<2),result);
      }
      // 6. HO2
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_add_pd(result,ropl[8]);
        result = _mm256_sub_pd(result,ropl[9]);
        result = _mm256_sub_pd(result,ropl[10]);
        result = _mm256_sub_pd(result,ropl[11]);
        result = _mm256_sub_pd(result,ropl[12]);
        result = 
          _mm256_sub_pd(result,_mm256_mul_pd(_mm256_set1_pd(2.0),ropl[13])); 
        result = _mm256_add_pd(result,ropl[16]);
        result = _mm256_add_pd(result,ropl[17]);
        result = _mm256_add_pd(result,ropl[18]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.03300677));
        _mm256_store_pd(wdot_array+(6*spec_stride)+(idx<<2),result);
      }
      // 7. H2O2
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_add_pd(result,ropl[13]);
        result = _mm256_sub_pd(result,ropl[14]);
        result = _mm256_sub_pd(result,ropl[15]);
        result = _mm256_sub_pd(result,ropl[16]);
        result = _mm256_sub_pd(result,ropl[17]);
        result = _mm256_sub_pd(result,ropl[18]);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.03401473999999999));
        _mm256_store_pd(wdot_array+(7*spec_stride)+(idx<<2),result);
      }
      // 8. N2
      {
        __m256d result = _mm256_set1_pd(0.0);
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.0280134));
        _mm256_store_pd(wdot_array+(8*spec_stride)+(idx<<2),result);
      }
    }
  }
  free(cgspl);
  free(temperature);
  free(mole_frac);
  free(thbctemp);
  free(rr_f);
  free(rr_r);
  free(otc);
  free(ortc);
  free(vlntemp);
  free(prt);
  free(oprt);
}

