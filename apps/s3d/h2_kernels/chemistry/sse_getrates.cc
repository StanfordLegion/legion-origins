
#include "sse_getrates.h"
#include <cmath>
#include <cassert>
#include <cstdlib>
#include <emmintrin.h>
#include <smmintrin.h>

#ifndef __SINGE_MOLE_MASSES__
#define __SINGE_MOLE_MASSES__
const double molecular_masses[9] = {2.01594, 31.9988, 15.9994, 17.00737, 
  18.01534, 1.00797, 33.00677, 34.01474, 28.0134}; 
#endif


#ifndef __SINGE_RECIP_MOLE_MASSES__
#define __SINGE_RECIP_MOLE_MASSES__
const double recip_molecular_masses[9] = {0.4960465093207139, 
  0.03125117191894696, 0.06250234383789392, 0.05879803873262004, 
  0.05550825019122593, 0.9920930186414277, 0.03029681486555637, 
  0.02939901936631002, 0.03569720205330306}; 
#endif


void sse_getrates(const double *pressure_array, const double *temperature_array, 
  const double *avmolwt_array, const double *mass_frac_array, const int 
  num_elmts, const int spec_stride, double *wdot_array) 
{
  
  const double PA = 1.013250e+06;
  // Scaled R0 for non-dimensionalization
  const double R0 = 9.977411999999998e+09;
  // Scaled R0c for non-dimensionalization
  const double R0c = 238.4658699839999;
  const double DLn10 = 2.3025850929940459e0;
  
  __m128d *cgspl = (__m128d*)malloc(9*147*sizeof(__m128d));
  __m128d *temperature = (__m128d*)malloc(147*sizeof(__m128d));
  __m128d *otc = (__m128d*)malloc(147*sizeof(__m128d));
  __m128d *ortc = (__m128d*)malloc(147*sizeof(__m128d));
  __m128d *vlntemp = (__m128d*)malloc(147*sizeof(__m128d));
  __m128d *prt = (__m128d*)malloc(147*sizeof(__m128d));
  __m128d *oprt = (__m128d*)malloc(147*sizeof(__m128d));
  __m128d *mole_frac = (__m128d*)malloc(9*147*sizeof(__m128d));
  __m128d *thbctemp = (__m128d*)malloc(2*147*sizeof(__m128d));
  __m128d *rr_f = (__m128d*)malloc(19*147*sizeof(__m128d));
  __m128d *rr_r = (__m128d*)malloc(19*147*sizeof(__m128d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 294)
  {
    // Temperature dependent
    for (unsigned idx = 0; idx < 147; idx++)
    {
      temperature[idx] = _mm_load_pd(temperature_array);
      temperature_array += 2;
      otc[idx] = _mm_div_pd(_mm_set1_pd(1.0),temperature[idx]);
      ortc[idx] = 
        _mm_div_pd(_mm_set1_pd(1.0),_mm_mul_pd(temperature[idx],_mm_set1_pd(R0c))); 
      double t1 = _mm_cvtsd_f64(temperature[idx]);
      double log1 = log(t1);
      double t0 = 
        _mm_cvtsd_f64(_mm_shuffle_pd(temperature[idx],temperature[idx],1)); 
      double log0 = log(t0);
      vlntemp[idx] = _mm_set_pd(log0,log1);
      prt[idx] = 
        _mm_div_pd(_mm_set1_pd(PA),_mm_mul_pd(temperature[idx],_mm_set1_pd(R0))); 
      oprt[idx] = _mm_div_pd(_mm_set1_pd(1.0),prt[idx]);
    }
    // Gibbs computation
    for (unsigned idx = 0; idx < 147; idx++)
    {
      __m128d tk1 = temperature[idx];
      __m128d tklog = vlntemp[idx];
      __m128d tk2 = _mm_mul_pd(tk1,tk1);
      __m128d tk3 = _mm_mul_pd(tk1,tk2);
      __m128d tk4 = _mm_mul_pd(tk1,tk3);
      __m128d tk5 = _mm_mul_pd(tk1,tk4);
      // Species H2
      {
        __m128d cgspl_NC_H2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.991423),_mm_set1_pd(3.298124), 0); 
            cgspl_NC_H2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2 = _mm_mul_pd(cgspl_NC_H2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.04200386399999999),_mm_set1_pd(-0.04949665199999999), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.352118959999999e-04),_mm_set1_pd(1.954323599999999e-03), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(1.329347231999999e-06),_mm_set1_pd(1.364462495999999e-05), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-1.640997273599998e-08),_mm_set1_pd(-4.287035289599994e-06),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-1.35511),_mm_set1_pd(-3.294094),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-6.958616666666668),_mm_set1_pd(-8.437675),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.991423),_mm_set1_pd(3.298124), 1); 
            cgspl_NC_H2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2 = _mm_mul_pd(cgspl_NC_H2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.04200386399999999),_mm_set1_pd(-0.04949665199999999), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.352118959999999e-04),_mm_set1_pd(1.954323599999999e-03), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(1.329347231999999e-06),_mm_set1_pd(1.364462495999999e-05), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-1.640997273599998e-08),_mm_set1_pd(-4.287035289599994e-06),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-1.35511),_mm_set1_pd(-3.294094),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-6.958616666666668),_mm_set1_pd(-8.437675),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.991423),_mm_set1_pd(3.298124), 2); 
            cgspl_NC_H2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2 = _mm_mul_pd(cgspl_NC_H2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.04200386399999999),_mm_set1_pd(-0.04949665199999999), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.352118959999999e-04),_mm_set1_pd(1.954323599999999e-03), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(1.329347231999999e-06),_mm_set1_pd(1.364462495999999e-05), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-1.640997273599998e-08),_mm_set1_pd(-4.287035289599994e-06),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-1.35511),_mm_set1_pd(-3.294094),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-6.958616666666668),_mm_set1_pd(-8.437675),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.991423),_mm_set1_pd(3.298124), 3); 
            cgspl_NC_H2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2 = _mm_mul_pd(cgspl_NC_H2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.04200386399999999),_mm_set1_pd(-0.04949665199999999), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.352118959999999e-04),_mm_set1_pd(1.954323599999999e-03), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(1.329347231999999e-06),_mm_set1_pd(1.364462495999999e-05), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-1.640997273599998e-08),_mm_set1_pd(-4.287035289599994e-06),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-1.35511),_mm_set1_pd(-3.294094),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-6.958616666666668),_mm_set1_pd(-8.437675),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
          }
        }
        cgspl[0+idx] = cgspl_NC_H2;
      }
      // Species O2
      {
        __m128d cgspl_NC_O2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(3.697578),_mm_set1_pd(3.212936), 0); 
            cgspl_NC_O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O2 = _mm_mul_pd(cgspl_NC_O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.03681118199999999),_mm_set1_pd(-0.06764915999999999), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.021220799999999e-04),_mm_set1_pd(1.381476e-03), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-2.556404639999998e-06),_mm_set1_pd(-1.891982879999998e-04), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.178255807999999e-08),_mm_set1_pd(9.091236787199989e-06),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.189166),_mm_set1_pd(6.034738),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-10.28275),_mm_set1_pd(-8.377075000000001),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(3.697578),_mm_set1_pd(3.212936), 1); 
            cgspl_NC_O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O2 = _mm_mul_pd(cgspl_NC_O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.03681118199999999),_mm_set1_pd(-0.06764915999999999), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.021220799999999e-04),_mm_set1_pd(1.381476e-03), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-2.556404639999998e-06),_mm_set1_pd(-1.891982879999998e-04), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.178255807999999e-08),_mm_set1_pd(9.091236787199989e-06),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.189166),_mm_set1_pd(6.034738),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-10.28275),_mm_set1_pd(-8.377075000000001),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(3.697578),_mm_set1_pd(3.212936), 2); 
            cgspl_NC_O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O2 = _mm_mul_pd(cgspl_NC_O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.03681118199999999),_mm_set1_pd(-0.06764915999999999), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.021220799999999e-04),_mm_set1_pd(1.381476e-03), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-2.556404639999998e-06),_mm_set1_pd(-1.891982879999998e-04), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.178255807999999e-08),_mm_set1_pd(9.091236787199989e-06),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.189166),_mm_set1_pd(6.034738),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-10.28275),_mm_set1_pd(-8.377075000000001),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(3.697578),_mm_set1_pd(3.212936), 3); 
            cgspl_NC_O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O2 = _mm_mul_pd(cgspl_NC_O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.03681118199999999),_mm_set1_pd(-0.06764915999999999), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.021220799999999e-04),_mm_set1_pd(1.381476e-03), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-2.556404639999998e-06),_mm_set1_pd(-1.891982879999998e-04), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.178255807999999e-08),_mm_set1_pd(9.091236787199989e-06),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.189166),_mm_set1_pd(6.034738),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-10.28275),_mm_set1_pd(-8.377075000000001),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
          }
        }
        cgspl[147+idx] = cgspl_NC_O2;
      }
      // Species O
      {
        __m128d cgspl_NC_O, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.54206),_mm_set1_pd(2.946429), 0); 
            cgspl_NC_O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O = _mm_mul_pd(cgspl_NC_O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(1.6530372e-03),_mm_set1_pd(0.09828995999999997), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(7.446727199999996e-06),_mm_set1_pd(-5.810476799999997e-03), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-6.553536479999994e-07),_mm_set1_pd(2.308093919999998e-04), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(4.528796313599995e-09),_mm_set1_pd(-4.033873612799995e-06),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(4.920308),_mm_set1_pd(2.963995),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(243.59),_mm_set1_pd(242.897),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.54206),_mm_set1_pd(2.946429), 1); 
            cgspl_NC_O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O = _mm_mul_pd(cgspl_NC_O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(1.6530372e-03),_mm_set1_pd(0.09828995999999997), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(7.446727199999996e-06),_mm_set1_pd(-5.810476799999997e-03), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-6.553536479999994e-07),_mm_set1_pd(2.308093919999998e-04), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(4.528796313599995e-09),_mm_set1_pd(-4.033873612799995e-06),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(4.920308),_mm_set1_pd(2.963995),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(243.59),_mm_set1_pd(242.897),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.54206),_mm_set1_pd(2.946429), 2); 
            cgspl_NC_O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O = _mm_mul_pd(cgspl_NC_O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(1.6530372e-03),_mm_set1_pd(0.09828995999999997), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(7.446727199999996e-06),_mm_set1_pd(-5.810476799999997e-03), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-6.553536479999994e-07),_mm_set1_pd(2.308093919999998e-04), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(4.528796313599995e-09),_mm_set1_pd(-4.033873612799995e-06),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(4.920308),_mm_set1_pd(2.963995),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(243.59),_mm_set1_pd(242.897),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.54206),_mm_set1_pd(2.946429), 3); 
            cgspl_NC_O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O = _mm_mul_pd(cgspl_NC_O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(1.6530372e-03),_mm_set1_pd(0.09828995999999997), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(7.446727199999996e-06),_mm_set1_pd(-5.810476799999997e-03), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-6.553536479999994e-07),_mm_set1_pd(2.308093919999998e-04), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(4.528796313599995e-09),_mm_set1_pd(-4.033873612799995e-06),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(4.920308),_mm_set1_pd(2.963995),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(243.59),_mm_set1_pd(242.897),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
          }
        }
        cgspl[294+idx] = cgspl_NC_O;
      }
      // Species OH
      {
        __m128d cgspl_NC_OH, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.86472886),_mm_set1_pd(4.12530561), 0); 
            cgspl_NC_OH = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_OH = _mm_mul_pd(cgspl_NC_OH,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.06339026879999998),_mm_set1_pd(0.1935269634), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(6.217986191999998e-04),_mm_set1_pd(-0.015666352584), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-4.395148905599995e-06),_mm_set1_pd(8.349892459199992e-04), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.380974842367998e-08),_mm_set1_pd(-2.138269145471997e-05),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.70164073),_mm_set1_pd(-0.69043296),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(30.69690625),_mm_set1_pd(27.88590941666667),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.86472886),_mm_set1_pd(4.12530561), 1); 
            cgspl_NC_OH = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_OH = _mm_mul_pd(cgspl_NC_OH,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.06339026879999998),_mm_set1_pd(0.1935269634), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(6.217986191999998e-04),_mm_set1_pd(-0.015666352584), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-4.395148905599995e-06),_mm_set1_pd(8.349892459199992e-04), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.380974842367998e-08),_mm_set1_pd(-2.138269145471997e-05),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.70164073),_mm_set1_pd(-0.69043296),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(30.69690625),_mm_set1_pd(27.88590941666667),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.86472886),_mm_set1_pd(4.12530561), 2); 
            cgspl_NC_OH = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_OH = _mm_mul_pd(cgspl_NC_OH,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.06339026879999998),_mm_set1_pd(0.1935269634), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(6.217986191999998e-04),_mm_set1_pd(-0.015666352584), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-4.395148905599995e-06),_mm_set1_pd(8.349892459199992e-04), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.380974842367998e-08),_mm_set1_pd(-2.138269145471997e-05),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.70164073),_mm_set1_pd(-0.69043296),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(30.69690625),_mm_set1_pd(27.88590941666667),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.86472886),_mm_set1_pd(4.12530561), 3); 
            cgspl_NC_OH = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_OH = _mm_mul_pd(cgspl_NC_OH,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.06339026879999998),_mm_set1_pd(0.1935269634), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(6.217986191999998e-04),_mm_set1_pd(-0.015666352584), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-4.395148905599995e-06),_mm_set1_pd(8.349892459199992e-04), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.380974842367998e-08),_mm_set1_pd(-2.138269145471997e-05),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.70164073),_mm_set1_pd(-0.69043296),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(30.69690625),_mm_set1_pd(27.88590941666667),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
          }
        }
        cgspl[441+idx] = cgspl_NC_OH;
      }
      // Species H2O
      {
        __m128d cgspl_NC_H2O, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.672146),_mm_set1_pd(3.386842), 0); 
            cgspl_NC_H2O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O = _mm_mul_pd(cgspl_NC_H2O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.18337758),_mm_set1_pd(-0.2084989199999999), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(2.095262399999999e-03),_mm_set1_pd(0.01525127039999999), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.729434239999998e-05),_mm_set1_pd(-1.003475663999999e-03), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(6.626829542399991e-08),_mm_set1_pd(2.598830438399997e-05),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(6.862817),_mm_set1_pd(2.590233),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-249.1600833333334),_mm_set1_pd(-251.7342500000001),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.672146),_mm_set1_pd(3.386842), 1); 
            cgspl_NC_H2O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O = _mm_mul_pd(cgspl_NC_H2O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.18337758),_mm_set1_pd(-0.2084989199999999), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(2.095262399999999e-03),_mm_set1_pd(0.01525127039999999), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.729434239999998e-05),_mm_set1_pd(-1.003475663999999e-03), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(6.626829542399991e-08),_mm_set1_pd(2.598830438399997e-05),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(6.862817),_mm_set1_pd(2.590233),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-249.1600833333334),_mm_set1_pd(-251.7342500000001),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.672146),_mm_set1_pd(3.386842), 2); 
            cgspl_NC_H2O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O = _mm_mul_pd(cgspl_NC_H2O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.18337758),_mm_set1_pd(-0.2084989199999999), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(2.095262399999999e-03),_mm_set1_pd(0.01525127039999999), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.729434239999998e-05),_mm_set1_pd(-1.003475663999999e-03), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(6.626829542399991e-08),_mm_set1_pd(2.598830438399997e-05),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(6.862817),_mm_set1_pd(2.590233),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-249.1600833333334),_mm_set1_pd(-251.7342500000001),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.672146),_mm_set1_pd(3.386842), 3); 
            cgspl_NC_H2O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O = _mm_mul_pd(cgspl_NC_H2O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.18337758),_mm_set1_pd(-0.2084989199999999), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(2.095262399999999e-03),_mm_set1_pd(0.01525127039999999), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.729434239999998e-05),_mm_set1_pd(-1.003475663999999e-03), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(6.626829542399991e-08),_mm_set1_pd(2.598830438399997e-05),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(6.862817),_mm_set1_pd(2.590233),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-249.1600833333334),_mm_set1_pd(-251.7342500000001),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
          }
        }
        cgspl[588+idx] = cgspl_NC_H2O;
      }
      // Species H
      {
        __m128d cgspl_NC_H, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = _mm_blend_pd(_mm_set1_pd(2.5),_mm_set1_pd(2.5), 0);
            cgspl_NC_H = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H = _mm_mul_pd(cgspl_NC_H,temp);
            __m128d coeff2 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff3 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff4 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-0.4601176),_mm_set1_pd(-0.4601176),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(212.2635833333334),_mm_set1_pd(212.2635833333334),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
          }
          else
          {
            __m128d coeff1 = _mm_blend_pd(_mm_set1_pd(2.5),_mm_set1_pd(2.5), 1);
            cgspl_NC_H = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H = _mm_mul_pd(cgspl_NC_H,temp);
            __m128d coeff2 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff3 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff4 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-0.4601176),_mm_set1_pd(-0.4601176),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(212.2635833333334),_mm_set1_pd(212.2635833333334),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = _mm_blend_pd(_mm_set1_pd(2.5),_mm_set1_pd(2.5), 2);
            cgspl_NC_H = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H = _mm_mul_pd(cgspl_NC_H,temp);
            __m128d coeff2 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff3 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff4 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-0.4601176),_mm_set1_pd(-0.4601176),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(212.2635833333334),_mm_set1_pd(212.2635833333334),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
          }
          else
          {
            __m128d coeff1 = _mm_blend_pd(_mm_set1_pd(2.5),_mm_set1_pd(2.5), 3);
            cgspl_NC_H = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H = _mm_mul_pd(cgspl_NC_H,temp);
            __m128d coeff2 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff3 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff4 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-0.4601176),_mm_set1_pd(-0.4601176),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(212.2635833333334),_mm_set1_pd(212.2635833333334),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
          }
        }
        cgspl[735+idx] = cgspl_NC_H;
      }
      // Species HO2
      {
        __m128d cgspl_NC_HO2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.0172109),_mm_set1_pd(4.30179801), 0); 
            cgspl_NC_HO2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_HO2 = _mm_mul_pd(cgspl_NC_HO2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.1343892078),_mm_set1_pd(0.2849472305999999), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.520779559999999e-03),_mm_set1_pd(-0.05077989383999998), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.645147727999999e-05),_mm_set1_pd(3.495800073599997e-03), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.118795690879999e-07),_mm_set1_pd(-9.634206085631988e-05),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.78510215),_mm_set1_pd(3.71666245),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(0.9321392750000002),_mm_set1_pd(2.456733666666667),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.0172109),_mm_set1_pd(4.30179801), 1); 
            cgspl_NC_HO2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_HO2 = _mm_mul_pd(cgspl_NC_HO2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.1343892078),_mm_set1_pd(0.2849472305999999), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.520779559999999e-03),_mm_set1_pd(-0.05077989383999998), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.645147727999999e-05),_mm_set1_pd(3.495800073599997e-03), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.118795690879999e-07),_mm_set1_pd(-9.634206085631988e-05),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.78510215),_mm_set1_pd(3.71666245),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(0.9321392750000002),_mm_set1_pd(2.456733666666667),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.0172109),_mm_set1_pd(4.30179801), 2); 
            cgspl_NC_HO2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_HO2 = _mm_mul_pd(cgspl_NC_HO2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.1343892078),_mm_set1_pd(0.2849472305999999), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.520779559999999e-03),_mm_set1_pd(-0.05077989383999998), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.645147727999999e-05),_mm_set1_pd(3.495800073599997e-03), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.118795690879999e-07),_mm_set1_pd(-9.634206085631988e-05),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.78510215),_mm_set1_pd(3.71666245),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(0.9321392750000002),_mm_set1_pd(2.456733666666667),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.0172109),_mm_set1_pd(4.30179801), 3); 
            cgspl_NC_HO2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_HO2 = _mm_mul_pd(cgspl_NC_HO2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.1343892078),_mm_set1_pd(0.2849472305999999), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.520779559999999e-03),_mm_set1_pd(-0.05077989383999998), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.645147727999999e-05),_mm_set1_pd(3.495800073599997e-03), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.118795690879999e-07),_mm_set1_pd(-9.634206085631988e-05),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.78510215),_mm_set1_pd(3.71666245),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(0.9321392750000002),_mm_set1_pd(2.456733666666667),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
          }
        }
        cgspl[882+idx] = cgspl_NC_HO2;
      }
      // Species H2O2
      {
        __m128d cgspl_NC_H2O2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.573167),_mm_set1_pd(3.388754), 0); 
            cgspl_NC_H2O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O2 = _mm_mul_pd(cgspl_NC_H2O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.2601681599999999),_mm_set1_pd(-0.3941535599999999), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.539253599999998e-03),_mm_set1_pd(3.564031199999998e-04), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-3.382421759999996e-05),_mm_set1_pd(6.661160639999993e-04), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.484338867199998e-07),_mm_set1_pd(-2.562466751999997e-05),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(0.5011370000000001),_mm_set1_pd(6.785363),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-150.058),_mm_set1_pd(-147.1929166666667),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.573167),_mm_set1_pd(3.388754), 1); 
            cgspl_NC_H2O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O2 = _mm_mul_pd(cgspl_NC_H2O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.2601681599999999),_mm_set1_pd(-0.3941535599999999), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.539253599999998e-03),_mm_set1_pd(3.564031199999998e-04), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-3.382421759999996e-05),_mm_set1_pd(6.661160639999993e-04), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.484338867199998e-07),_mm_set1_pd(-2.562466751999997e-05),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(0.5011370000000001),_mm_set1_pd(6.785363),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-150.058),_mm_set1_pd(-147.1929166666667),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.573167),_mm_set1_pd(3.388754), 2); 
            cgspl_NC_H2O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O2 = _mm_mul_pd(cgspl_NC_H2O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.2601681599999999),_mm_set1_pd(-0.3941535599999999), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.539253599999998e-03),_mm_set1_pd(3.564031199999998e-04), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-3.382421759999996e-05),_mm_set1_pd(6.661160639999993e-04), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.484338867199998e-07),_mm_set1_pd(-2.562466751999997e-05),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(0.5011370000000001),_mm_set1_pd(6.785363),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-150.058),_mm_set1_pd(-147.1929166666667),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.573167),_mm_set1_pd(3.388754), 3); 
            cgspl_NC_H2O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O2 = _mm_mul_pd(cgspl_NC_H2O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.2601681599999999),_mm_set1_pd(-0.3941535599999999), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.539253599999998e-03),_mm_set1_pd(3.564031199999998e-04), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-3.382421759999996e-05),_mm_set1_pd(6.661160639999993e-04), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.484338867199998e-07),_mm_set1_pd(-2.562466751999997e-05),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(0.5011370000000001),_mm_set1_pd(6.785363),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-150.058),_mm_set1_pd(-147.1929166666667),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
          }
        }
        cgspl[1029+idx] = cgspl_NC_H2O2;
      }
      // Species N2
      {
        __m128d cgspl_NC_N2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.92664),_mm_set1_pd(3.298677), 0); 
            cgspl_NC_N2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_N2 = _mm_mul_pd(cgspl_NC_N2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.08927861999999998),_mm_set1_pd(-0.08449439999999998), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.364342639999999e-03),_mm_set1_pd(9.511732799999995e-03), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.453973759999998e-05),_mm_set1_pd(-8.123781599999992e-04), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(7.001874316799991e-08),_mm_set1_pd(2.534825663999997e-05),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.980528),_mm_set1_pd(3.950372),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-7.689980833333334),_mm_set1_pd(-8.507500000000002),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.92664),_mm_set1_pd(3.298677), 1); 
            cgspl_NC_N2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_N2 = _mm_mul_pd(cgspl_NC_N2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.08927861999999998),_mm_set1_pd(-0.08449439999999998), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.364342639999999e-03),_mm_set1_pd(9.511732799999995e-03), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.453973759999998e-05),_mm_set1_pd(-8.123781599999992e-04), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(7.001874316799991e-08),_mm_set1_pd(2.534825663999997e-05),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.980528),_mm_set1_pd(3.950372),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-7.689980833333334),_mm_set1_pd(-8.507500000000002),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.92664),_mm_set1_pd(3.298677), 2); 
            cgspl_NC_N2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_N2 = _mm_mul_pd(cgspl_NC_N2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.08927861999999998),_mm_set1_pd(-0.08449439999999998), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.364342639999999e-03),_mm_set1_pd(9.511732799999995e-03), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.453973759999998e-05),_mm_set1_pd(-8.123781599999992e-04), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(7.001874316799991e-08),_mm_set1_pd(2.534825663999997e-05),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.980528),_mm_set1_pd(3.950372),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-7.689980833333334),_mm_set1_pd(-8.507500000000002),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.92664),_mm_set1_pd(3.298677), 3); 
            cgspl_NC_N2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_N2 = _mm_mul_pd(cgspl_NC_N2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.08927861999999998),_mm_set1_pd(-0.08449439999999998), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.364342639999999e-03),_mm_set1_pd(9.511732799999995e-03), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.453973759999998e-05),_mm_set1_pd(-8.123781599999992e-04), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(7.001874316799991e-08),_mm_set1_pd(2.534825663999997e-05),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.980528),_mm_set1_pd(3.950372),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-7.689980833333334),_mm_set1_pd(-8.507500000000002),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
          }
        }
        cgspl[1176+idx] = cgspl_NC_N2;
      }
    }
    // Compute mole fractions
    for (unsigned idx = 0; idx < 147; idx++)
    {
      __m128d avmolwt = _mm_load_pd(avmolwt_array);
      __m128d pressure = _mm_load_pd(pressure_array);
      avmolwt_array += 2;
      pressure_array += 2;
      __m128d sumyow = 
        _mm_mul_pd(_mm_mul_pd(temperature[idx],avmolwt),_mm_set1_pd(7.03444160806564)); 
      sumyow = _mm_div_pd(pressure,sumyow);
      __m128d mask, mass_frac;
      __m128d small = _mm_set1_pd(1e-200);
      mass_frac = _mm_load_pd(mass_frac_array+(0*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[0]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[0+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(1*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[1]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[147+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(2*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[2]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[294+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(3*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[3]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[441+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(4*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[4]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[588+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(5*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[5]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[735+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(6*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[6]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[882+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(7*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[7]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[1029+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(8*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[8]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[1176+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
    }
    // Compute third body values
    for (unsigned idx = 0; idx < 147; idx++)
    {
      __m128d ctot = _mm_set1_pd(0.0);
      ctot = _mm_add_pd(ctot,mole_frac[0+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[147+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[294+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[441+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[588+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[735+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[882+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[1029+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[1176+idx]);
      thbctemp[0+idx] = ctot;
      thbctemp[0+idx] = 
        _mm_add_pd(thbctemp[0+idx],_mm_mul_pd(_mm_set1_pd(1.5),mole_frac[0+idx])); 
      thbctemp[0+idx] = 
        _mm_add_pd(thbctemp[0+idx],_mm_mul_pd(_mm_set1_pd(11.0),mole_frac[588+idx])); 
      thbctemp[147+idx] = ctot;
      thbctemp[147+idx] = _mm_add_pd(thbctemp[147+idx],mole_frac[0+idx]);
      thbctemp[147+idx] = 
        _mm_sub_pd(thbctemp[147+idx],_mm_mul_pd(_mm_set1_pd(0.22),mole_frac[147+idx])); 
      thbctemp[147+idx] = 
        _mm_add_pd(thbctemp[147+idx],_mm_mul_pd(_mm_set1_pd(10.0),mole_frac[588+idx])); 
    }
    // Reaction rates
    for (unsigned idx = 0; idx < 147; idx++)
    {
      //   0)  O2 + H <=> O + OH
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-0.406),vlntemp[idx]);
            arrtemp = 
              _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(1.6599e+04),ortc[idx])); 
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(5.606052175734568e+15),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[147+idx]);
        xik = _mm_add_pd(xik,cgspl[294+idx]);
        xik = _mm_add_pd(xik,cgspl[441+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[0+idx] = forward;
        rr_f[0+idx] = _mm_mul_pd(rr_f[0+idx],mole_frac[147+idx]);
        rr_f[0+idx] = _mm_mul_pd(rr_f[0+idx],mole_frac[735+idx]);
        rr_r[0+idx] = reverse;
        rr_r[0+idx] = _mm_mul_pd(rr_r[0+idx],mole_frac[294+idx]);
        rr_r[0+idx] = _mm_mul_pd(rr_r[0+idx],mole_frac[441+idx]);
      }
      //   1)  H2 + O <=> OH + H
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(2.67),vlntemp[idx]);
            arrtemp = 
              _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(6.29e+03),ortc[idx])); 
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(1.996278696128123e+11),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[0+idx]);
        xik = _mm_sub_pd(xik,cgspl[294+idx]);
        xik = _mm_add_pd(xik,cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[735+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[147+idx] = forward;
        rr_f[147+idx] = _mm_mul_pd(rr_f[147+idx],mole_frac[0+idx]);
        rr_f[147+idx] = _mm_mul_pd(rr_f[147+idx],mole_frac[294+idx]);
        rr_r[147+idx] = reverse;
        rr_r[147+idx] = _mm_mul_pd(rr_r[147+idx],mole_frac[441+idx]);
        rr_r[147+idx] = _mm_mul_pd(rr_r[147+idx],mole_frac[735+idx]);
      }
      //   2)  H2 + OH <=> H2O + H
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(1.51),vlntemp[idx]);
            arrtemp = 
              _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(3.43e+03),ortc[idx])); 
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(3.288220749682686e+12),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[0+idx]);
        xik = _mm_sub_pd(xik,cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[588+idx]);
        xik = _mm_add_pd(xik,cgspl[735+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[294+idx] = forward;
        rr_f[294+idx] = _mm_mul_pd(rr_f[294+idx],mole_frac[0+idx]);
        rr_f[294+idx] = _mm_mul_pd(rr_f[294+idx],mole_frac[441+idx]);
        rr_r[294+idx] = reverse;
        rr_r[294+idx] = _mm_mul_pd(rr_r[294+idx],mole_frac[588+idx]);
        rr_r[294+idx] = _mm_mul_pd(rr_r[294+idx],mole_frac[735+idx]);
      }
      //   3)  O + H2O <=> 2 OH
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(2.02),vlntemp[idx]);
            arrtemp = 
              _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(1.34e+04),ortc[idx])); 
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(5.195724313044046e+11),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[294+idx]);
        xik = _mm_add_pd(xik,_mm_mul_pd(_mm_set1_pd(double(2)),cgspl[441+idx]));
        xik = _mm_sub_pd(xik,cgspl[588+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[441+idx] = forward;
        rr_f[441+idx] = _mm_mul_pd(rr_f[441+idx],mole_frac[294+idx]);
        rr_f[441+idx] = _mm_mul_pd(rr_f[441+idx],mole_frac[588+idx]);
        rr_r[441+idx] = reverse;
        rr_r[441+idx] = _mm_mul_pd(rr_r[441+idx],mole_frac[441+idx]);
        rr_r[441+idx] = _mm_mul_pd(rr_r[441+idx],mole_frac[441+idx]);
      }
      //   4)  H2 + M <=> 2 H + M
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-1.4),vlntemp[idx]);
            arrtemp = 
              _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(1.0438e+05),ortc[idx])); 
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(6.203984453113697e+17),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[0+idx]);
        xik = _mm_add_pd(xik,_mm_mul_pd(_mm_set1_pd(double(2)),cgspl[735+idx]));
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,oprt[idx]);
        rr_f[588+idx] = forward;
        rr_f[588+idx] = _mm_mul_pd(rr_f[588+idx],mole_frac[0+idx]);
        rr_r[588+idx] = reverse;
        rr_r[588+idx] = _mm_mul_pd(rr_r[588+idx],mole_frac[735+idx]);
        rr_r[588+idx] = _mm_mul_pd(rr_r[588+idx],mole_frac[735+idx]);
        rr_f[588+idx] = _mm_mul_pd(rr_f[588+idx],thbctemp[0+idx]);
        rr_r[588+idx] = _mm_mul_pd(rr_r[588+idx],thbctemp[0+idx]);
      }
      //   5)  2 O + M <=> O2 + M
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-0.5),vlntemp[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(6.21278022363831e+15), arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[147+idx];
        xik = 
          _mm_add_pd(xik,_mm_mul_pd(_mm_set1_pd(double(-2)),cgspl[294+idx])); 
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,prt[idx]);
        rr_f[735+idx] = forward;
        rr_f[735+idx] = _mm_mul_pd(rr_f[735+idx],mole_frac[294+idx]);
        rr_f[735+idx] = _mm_mul_pd(rr_f[735+idx],mole_frac[294+idx]);
        rr_r[735+idx] = reverse;
        rr_r[735+idx] = _mm_mul_pd(rr_r[735+idx],mole_frac[147+idx]);
        rr_f[735+idx] = _mm_mul_pd(rr_f[735+idx],thbctemp[0+idx]);
        rr_r[735+idx] = _mm_mul_pd(rr_r[735+idx],thbctemp[0+idx]);
      }
      //   6)  O + H + M <=> OH + M
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = 
              _mm_mul_pd(_mm_set1_pd(4.336624958847576e+17),otc[idx]); 
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[294+idx]);
        xik = _mm_add_pd(xik,cgspl[441+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,prt[idx]);
        rr_f[882+idx] = forward;
        rr_f[882+idx] = _mm_mul_pd(rr_f[882+idx],mole_frac[294+idx]);
        rr_f[882+idx] = _mm_mul_pd(rr_f[882+idx],mole_frac[735+idx]);
        rr_r[882+idx] = reverse;
        rr_r[882+idx] = _mm_mul_pd(rr_r[882+idx],mole_frac[441+idx]);
        rr_f[882+idx] = _mm_mul_pd(rr_f[882+idx],thbctemp[0+idx]);
        rr_r[882+idx] = _mm_mul_pd(rr_r[882+idx],thbctemp[0+idx]);
      }
      //   7)  OH + H + M <=> H2O + M
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = 
              _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(2.913162007428368e+19),otc[idx]),otc[idx]); 
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[588+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,prt[idx]);
        rr_f[1029+idx] = forward;
        rr_f[1029+idx] = _mm_mul_pd(rr_f[1029+idx],mole_frac[441+idx]);
        rr_f[1029+idx] = _mm_mul_pd(rr_f[1029+idx],mole_frac[735+idx]);
        rr_r[1029+idx] = reverse;
        rr_r[1029+idx] = _mm_mul_pd(rr_r[1029+idx],mole_frac[588+idx]);
        rr_f[1029+idx] = _mm_mul_pd(rr_f[1029+idx],thbctemp[0+idx]);
        rr_r[1029+idx] = _mm_mul_pd(rr_r[1029+idx],thbctemp[0+idx]);
      }
      //   8)  O2 + H (+M) <=> HO2 (+M)
      {
        __m128d forward, reverse;
        __m128d rr_k0, rr_kinf;
        {
          __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-1.72),vlntemp[idx]);
          arrtemp = 
            _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(524.8),ortc[idx])); 
          arrtemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
          arrtemp = _mm_mul_pd(_mm_set1_pd(1.864740588650248e+18),arrtemp);
          rr_k0 = arrtemp;
        }
        {
          __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(0.6),vlntemp[idx]);
          arrtemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
          arrtemp = _mm_mul_pd(_mm_set1_pd(2.879017473981534e+14), arrtemp);
          rr_kinf = arrtemp;
        }
        __m128d pr = _mm_mul_pd(_mm_div_pd(rr_k0,rr_kinf),thbctemp[147+idx]);
        __m128d fcent;
        {
          fcent = _mm_mul_pd(temperature[idx], _mm_set1_pd(-1.2e+32));
          fcent = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(fcent,fcent,1))),exp(_mm_cvtsd_f64(fcent))); 
          fcent = _mm_mul_pd(fcent, _mm_set1_pd(0.2));
          {
            __m128d fctemp = _mm_mul_pd(temperature[idx], 
              _mm_set1_pd(-1.2e-28)); 
            fctemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(fctemp,fctemp,1))),exp(_mm_cvtsd_f64(fctemp))); 
            fcent = _mm_add_pd(fcent, _mm_mul_pd(fctemp, _mm_set1_pd(0.8)));
          }
          fcent = 
            _mm_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(fcent,fcent,1))), 
            log10(_mm_cvtsd_f64(fcent))); 
          __m128d flogpr = 
            _mm_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(pr,pr,1))),log10(_mm_cvtsd_f64(pr))); 
          flogpr = _mm_sub_pd(flogpr, 
            _mm_add_pd(_mm_set1_pd(0.4),_mm_mul_pd(_mm_set1_pd(0.67),fcent))); 
          __m128d fdenom = 
            _mm_add_pd(_mm_set1_pd(0.75),_mm_mul_pd(_mm_set1_pd(-1.27),fcent)); 
          fdenom = _mm_add_pd(fdenom,_mm_mul_pd(_mm_set1_pd(-0.14),flogpr));
          __m128d fquan = _mm_div_pd(flogpr,fdenom);
          fquan = 
            _mm_div_pd(fcent,_mm_add_pd(_mm_set1_pd(1.0),_mm_mul_pd(fquan,fquan))); 
          __m128d ftemp = _mm_mul_pd(fquan,_mm_set1_pd(DLn10));
          ftemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(ftemp,ftemp,1))),exp(_mm_cvtsd_f64(ftemp))); 
          forward = _mm_mul_pd(rr_kinf,pr);
          forward = _mm_div_pd(forward,_mm_add_pd(_mm_set1_pd(1.0),pr));
          forward = _mm_mul_pd(forward, ftemp);
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[147+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        xik = _mm_add_pd(xik,cgspl[882+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,prt[idx]);
        rr_f[1176+idx] = forward;
        rr_f[1176+idx] = _mm_mul_pd(rr_f[1176+idx],mole_frac[147+idx]);
        rr_f[1176+idx] = _mm_mul_pd(rr_f[1176+idx],mole_frac[735+idx]);
        rr_r[1176+idx] = reverse;
        rr_r[1176+idx] = _mm_mul_pd(rr_r[1176+idx],mole_frac[882+idx]);
      }
      //   9)  H + HO2 <=> H2 + O2
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-823.0), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(1.832532226988623e+14),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[0+idx];
        xik = _mm_add_pd(xik,cgspl[147+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        xik = _mm_sub_pd(xik,cgspl[882+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[1323+idx] = forward;
        rr_f[1323+idx] = _mm_mul_pd(rr_f[1323+idx],mole_frac[735+idx]);
        rr_f[1323+idx] = _mm_mul_pd(rr_f[1323+idx],mole_frac[882+idx]);
        rr_r[1323+idx] = reverse;
        rr_r[1323+idx] = _mm_mul_pd(rr_r[1323+idx],mole_frac[0+idx]);
        rr_r[1323+idx] = _mm_mul_pd(rr_r[1323+idx],mole_frac[147+idx]);
      }
      //  10)  H + HO2 <=> 2 OH
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-295.0), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(7.814756406537628e+14),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_mul_pd(_mm_set1_pd(double(2)),cgspl[441+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        xik = _mm_sub_pd(xik,cgspl[882+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[1470+idx] = forward;
        rr_f[1470+idx] = _mm_mul_pd(rr_f[1470+idx],mole_frac[735+idx]);
        rr_f[1470+idx] = _mm_mul_pd(rr_f[1470+idx],mole_frac[882+idx]);
        rr_r[1470+idx] = reverse;
        rr_r[1470+idx] = _mm_mul_pd(rr_r[1470+idx],mole_frac[441+idx]);
        rr_r[1470+idx] = _mm_mul_pd(rr_r[1470+idx],mole_frac[441+idx]);
      }
      //  11)  O + HO2 <=> O2 + OH
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_set1_pd(3.587788998622304e+14);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[147+idx];
        xik = _mm_sub_pd(xik,cgspl[294+idx]);
        xik = _mm_add_pd(xik,cgspl[441+idx]);
        xik = _mm_sub_pd(xik,cgspl[882+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[1617+idx] = forward;
        rr_f[1617+idx] = _mm_mul_pd(rr_f[1617+idx],mole_frac[294+idx]);
        rr_f[1617+idx] = _mm_mul_pd(rr_f[1617+idx],mole_frac[882+idx]);
        rr_r[1617+idx] = reverse;
        rr_r[1617+idx] = _mm_mul_pd(rr_r[1617+idx],mole_frac[147+idx]);
        rr_r[1617+idx] = _mm_mul_pd(rr_r[1617+idx],mole_frac[441+idx]);
      }
      //  12)  OH + HO2 <=> O2 + H2O
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(497.0), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(3.190372371082602e+14),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[147+idx];
        xik = _mm_sub_pd(xik,cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[588+idx]);
        xik = _mm_sub_pd(xik,cgspl[882+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[1764+idx] = forward;
        rr_f[1764+idx] = _mm_mul_pd(rr_f[1764+idx],mole_frac[441+idx]);
        rr_f[1764+idx] = _mm_mul_pd(rr_f[1764+idx],mole_frac[882+idx]);
        rr_r[1764+idx] = reverse;
        rr_r[1764+idx] = _mm_mul_pd(rr_r[1764+idx],mole_frac[147+idx]);
        rr_r[1764+idx] = _mm_mul_pd(rr_r[1764+idx],mole_frac[588+idx]);
      }
      //  13, 14)  2 HO2 <=> O2 + H2O2
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-1.1982e+04), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(4.636527321296516e+15),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(1.6293e+03), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(1.435115599448922e+12),arrtemp);
            next = arrtemp;
          }
          forward = _mm_add_pd(forward,next);
        }
        __m128d xik = cgspl[147+idx];
        xik = 
          _mm_add_pd(xik,_mm_mul_pd(_mm_set1_pd(double(-2)),cgspl[882+idx])); 
        xik = _mm_add_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[1911+idx] = forward;
        rr_f[1911+idx] = _mm_mul_pd(rr_f[1911+idx],mole_frac[882+idx]);
        rr_f[1911+idx] = _mm_mul_pd(rr_f[1911+idx],mole_frac[882+idx]);
        rr_r[1911+idx] = reverse;
        rr_r[1911+idx] = _mm_mul_pd(rr_r[1911+idx],mole_frac[147+idx]);
        rr_r[1911+idx] = _mm_mul_pd(rr_r[1911+idx],mole_frac[1029+idx]);
      }
      //  14)  H2O2 (+M) <=> 2 OH (+M)
      {
        __m128d forward, reverse;
        __m128d rr_k0, rr_kinf;
        {
          __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-4.55e+04), ortc[idx]);
          arrtemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
          arrtemp = _mm_mul_pd(_mm_set1_pd(1.326929961952003e+18),arrtemp);
          rr_k0 = arrtemp;
        }
        {
          __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-4.843e+04), ortc[idx]);
          arrtemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
          arrtemp = _mm_mul_pd(_mm_set1_pd(3.257712410749052e+15),arrtemp);
          rr_kinf = arrtemp;
        }
        __m128d pr = _mm_mul_pd(_mm_div_pd(rr_k0,rr_kinf),thbctemp[0+idx]);
        __m128d fcent;
        {
          fcent = _mm_mul_pd(temperature[idx], _mm_set1_pd(-1.2e+32));
          fcent = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(fcent,fcent,1))),exp(_mm_cvtsd_f64(fcent))); 
          fcent = _mm_mul_pd(fcent, _mm_set1_pd(0.5));
          {
            __m128d fctemp = _mm_mul_pd(temperature[idx], 
              _mm_set1_pd(-1.2e-28)); 
            fctemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(fctemp,fctemp,1))),exp(_mm_cvtsd_f64(fctemp))); 
            fcent = _mm_add_pd(fcent, _mm_mul_pd(fctemp, _mm_set1_pd(0.5)));
          }
          fcent = 
            _mm_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(fcent,fcent,1))), 
            log10(_mm_cvtsd_f64(fcent))); 
          __m128d flogpr = 
            _mm_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(pr,pr,1))),log10(_mm_cvtsd_f64(pr))); 
          flogpr = _mm_sub_pd(flogpr, 
            _mm_add_pd(_mm_set1_pd(0.4),_mm_mul_pd(_mm_set1_pd(0.67),fcent))); 
          __m128d fdenom = 
            _mm_add_pd(_mm_set1_pd(0.75),_mm_mul_pd(_mm_set1_pd(-1.27),fcent)); 
          fdenom = _mm_add_pd(fdenom,_mm_mul_pd(_mm_set1_pd(-0.14),flogpr));
          __m128d fquan = _mm_div_pd(flogpr,fdenom);
          fquan = 
            _mm_div_pd(fcent,_mm_add_pd(_mm_set1_pd(1.0),_mm_mul_pd(fquan,fquan))); 
          __m128d ftemp = _mm_mul_pd(fquan,_mm_set1_pd(DLn10));
          ftemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(ftemp,ftemp,1))),exp(_mm_cvtsd_f64(ftemp))); 
          forward = _mm_mul_pd(rr_kinf,pr);
          forward = _mm_div_pd(forward,_mm_add_pd(_mm_set1_pd(1.0),pr));
          forward = _mm_mul_pd(forward, ftemp);
        }
        __m128d xik = _mm_mul_pd(_mm_set1_pd(double(2)),cgspl[441+idx]);
        xik = _mm_sub_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,oprt[idx]);
        rr_f[2058+idx] = forward;
        rr_f[2058+idx] = _mm_mul_pd(rr_f[2058+idx],mole_frac[1029+idx]);
        rr_r[2058+idx] = reverse;
        rr_r[2058+idx] = _mm_mul_pd(rr_r[2058+idx],mole_frac[441+idx]);
        rr_r[2058+idx] = _mm_mul_pd(rr_r[2058+idx],mole_frac[441+idx]);
      }
      //  15)  H + H2O2 <=> OH + H2O
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-3.97e+03), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(2.660483534363001e+14),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[441+idx];
        xik = _mm_add_pd(xik,cgspl[588+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        xik = _mm_sub_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[2205+idx] = forward;
        rr_f[2205+idx] = _mm_mul_pd(rr_f[2205+idx],mole_frac[735+idx]);
        rr_f[2205+idx] = _mm_mul_pd(rr_f[2205+idx],mole_frac[1029+idx]);
        rr_r[2205+idx] = reverse;
        rr_r[2205+idx] = _mm_mul_pd(rr_r[2205+idx],mole_frac[441+idx]);
        rr_r[2205+idx] = _mm_mul_pd(rr_r[2205+idx],mole_frac[588+idx]);
      }
      //  16)  H + H2O2 <=> H2 + HO2
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-7.95e+03), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(5.320967068726001e+14),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[0+idx];
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        xik = _mm_add_pd(xik,cgspl[882+idx]);
        xik = _mm_sub_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[2352+idx] = forward;
        rr_f[2352+idx] = _mm_mul_pd(rr_f[2352+idx],mole_frac[735+idx]);
        rr_f[2352+idx] = _mm_mul_pd(rr_f[2352+idx],mole_frac[1029+idx]);
        rr_r[2352+idx] = reverse;
        rr_r[2352+idx] = _mm_mul_pd(rr_r[2352+idx],mole_frac[0+idx]);
        rr_r[2352+idx] = _mm_mul_pd(rr_r[2352+idx],mole_frac[882+idx]);
      }
      //  17)  O + H2O2 <=> OH + HO2
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-3.97e+03),ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = 
              _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(_mm_set1_pd(1.518131517201658e+12),temperature[idx]),temperature[idx]),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[294+idx]);
        xik = _mm_add_pd(xik,cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[882+idx]);
        xik = _mm_sub_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[2499+idx] = forward;
        rr_f[2499+idx] = _mm_mul_pd(rr_f[2499+idx],mole_frac[294+idx]);
        rr_f[2499+idx] = _mm_mul_pd(rr_f[2499+idx],mole_frac[1029+idx]);
        rr_r[2499+idx] = reverse;
        rr_r[2499+idx] = _mm_mul_pd(rr_r[2499+idx],mole_frac[441+idx]);
        rr_r[2499+idx] = _mm_mul_pd(rr_r[2499+idx],mole_frac[882+idx]);
      }
      //  18, 20)  OH + H2O2 <=> H2O + HO2
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_set1_pd(1.103935076499171e+13);
            next = arrtemp;
          }
          forward = next;
        }
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-9.557e+03), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(6.402823443695189e+15),arrtemp);
            next = arrtemp;
          }
          forward = _mm_add_pd(forward,next);
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[588+idx]);
        xik = _mm_add_pd(xik,cgspl[882+idx]);
        xik = _mm_sub_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[2646+idx] = forward;
        rr_f[2646+idx] = _mm_mul_pd(rr_f[2646+idx],mole_frac[441+idx]);
        rr_f[2646+idx] = _mm_mul_pd(rr_f[2646+idx],mole_frac[1029+idx]);
        rr_r[2646+idx] = reverse;
        rr_r[2646+idx] = _mm_mul_pd(rr_r[2646+idx],mole_frac[588+idx]);
        rr_r[2646+idx] = _mm_mul_pd(rr_r[2646+idx],mole_frac[882+idx]);
      }
    }
    // Output rates
    for (unsigned idx = 0; idx < 147; idx++)
    {
      __m128d ropl[19];
      ropl[0] = _mm_sub_pd(rr_f[0+idx],rr_r[0+idx]);
      ropl[1] = _mm_sub_pd(rr_f[147+idx],rr_r[147+idx]);
      ropl[2] = _mm_sub_pd(rr_f[294+idx],rr_r[294+idx]);
      ropl[3] = _mm_sub_pd(rr_f[441+idx],rr_r[441+idx]);
      ropl[4] = _mm_sub_pd(rr_f[588+idx],rr_r[588+idx]);
      ropl[5] = _mm_sub_pd(rr_f[735+idx],rr_r[735+idx]);
      ropl[6] = _mm_sub_pd(rr_f[882+idx],rr_r[882+idx]);
      ropl[7] = _mm_sub_pd(rr_f[1029+idx],rr_r[1029+idx]);
      ropl[8] = _mm_sub_pd(rr_f[1176+idx],rr_r[1176+idx]);
      ropl[9] = _mm_sub_pd(rr_f[1323+idx],rr_r[1323+idx]);
      ropl[10] = _mm_sub_pd(rr_f[1470+idx],rr_r[1470+idx]);
      ropl[11] = _mm_sub_pd(rr_f[1617+idx],rr_r[1617+idx]);
      ropl[12] = _mm_sub_pd(rr_f[1764+idx],rr_r[1764+idx]);
      ropl[13] = _mm_sub_pd(rr_f[1911+idx],rr_r[1911+idx]);
      ropl[14] = _mm_sub_pd(rr_f[2058+idx],rr_r[2058+idx]);
      ropl[15] = _mm_sub_pd(rr_f[2205+idx],rr_r[2205+idx]);
      ropl[16] = _mm_sub_pd(rr_f[2352+idx],rr_r[2352+idx]);
      ropl[17] = _mm_sub_pd(rr_f[2499+idx],rr_r[2499+idx]);
      ropl[18] = _mm_sub_pd(rr_f[2646+idx],rr_r[2646+idx]);
      // 0. H2
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_sub_pd(result,ropl[1]);
        result = _mm_sub_pd(result,ropl[2]);
        result = _mm_sub_pd(result,ropl[4]);
        result = _mm_add_pd(result,ropl[9]);
        result = _mm_add_pd(result,ropl[16]);
        result = _mm_mul_pd(result,_mm_set1_pd(2.01594e-03));
        _mm_store_pd(wdot_array+(0*spec_stride)+(idx<<1),result);
      }
      // 1. O2
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_sub_pd(result,ropl[0]);
        result = _mm_add_pd(result,ropl[5]);
        result = _mm_sub_pd(result,ropl[8]);
        result = _mm_add_pd(result,ropl[9]);
        result = _mm_add_pd(result,ropl[11]);
        result = _mm_add_pd(result,ropl[12]);
        result = _mm_add_pd(result,ropl[13]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.0319988));
        _mm_store_pd(wdot_array+(1*spec_stride)+(idx<<1),result);
      }
      // 2. O
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_add_pd(result,ropl[0]);
        result = _mm_sub_pd(result,ropl[1]);
        result = _mm_sub_pd(result,ropl[3]);
        result = _mm_sub_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[5]));
        result = _mm_sub_pd(result,ropl[6]);
        result = _mm_sub_pd(result,ropl[11]);
        result = _mm_sub_pd(result,ropl[17]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.0159994));
        _mm_store_pd(wdot_array+(2*spec_stride)+(idx<<1),result);
      }
      // 3. OH
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_add_pd(result,ropl[0]);
        result = _mm_add_pd(result,ropl[1]);
        result = _mm_sub_pd(result,ropl[2]);
        result = _mm_add_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[3]));
        result = _mm_add_pd(result,ropl[6]);
        result = _mm_sub_pd(result,ropl[7]);
        result = _mm_add_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[10]));
        result = _mm_add_pd(result,ropl[11]);
        result = _mm_sub_pd(result,ropl[12]);
        result = _mm_add_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[14]));
        result = _mm_add_pd(result,ropl[15]);
        result = _mm_add_pd(result,ropl[17]);
        result = _mm_sub_pd(result,ropl[18]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.01700737));
        _mm_store_pd(wdot_array+(3*spec_stride)+(idx<<1),result);
      }
      // 4. H2O
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_add_pd(result,ropl[2]);
        result = _mm_sub_pd(result,ropl[3]);
        result = _mm_add_pd(result,ropl[7]);
        result = _mm_add_pd(result,ropl[12]);
        result = _mm_add_pd(result,ropl[15]);
        result = _mm_add_pd(result,ropl[18]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.01801534));
        _mm_store_pd(wdot_array+(4*spec_stride)+(idx<<1),result);
      }
      // 5. H
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_sub_pd(result,ropl[0]);
        result = _mm_add_pd(result,ropl[1]);
        result = _mm_add_pd(result,ropl[2]);
        result = _mm_add_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[4]));
        result = _mm_sub_pd(result,ropl[6]);
        result = _mm_sub_pd(result,ropl[7]);
        result = _mm_sub_pd(result,ropl[8]);
        result = _mm_sub_pd(result,ropl[9]);
        result = _mm_sub_pd(result,ropl[10]);
        result = _mm_sub_pd(result,ropl[15]);
        result = _mm_sub_pd(result,ropl[16]);
        result = _mm_mul_pd(result,_mm_set1_pd(1.00797e-03));
        _mm_store_pd(wdot_array+(5*spec_stride)+(idx<<1),result);
      }
      // 6. HO2
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_add_pd(result,ropl[8]);
        result = _mm_sub_pd(result,ropl[9]);
        result = _mm_sub_pd(result,ropl[10]);
        result = _mm_sub_pd(result,ropl[11]);
        result = _mm_sub_pd(result,ropl[12]);
        result = _mm_sub_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[13]));
        result = _mm_add_pd(result,ropl[16]);
        result = _mm_add_pd(result,ropl[17]);
        result = _mm_add_pd(result,ropl[18]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.03300677));
        _mm_store_pd(wdot_array+(6*spec_stride)+(idx<<1),result);
      }
      // 7. H2O2
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_add_pd(result,ropl[13]);
        result = _mm_sub_pd(result,ropl[14]);
        result = _mm_sub_pd(result,ropl[15]);
        result = _mm_sub_pd(result,ropl[16]);
        result = _mm_sub_pd(result,ropl[17]);
        result = _mm_sub_pd(result,ropl[18]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.03401473999999999));
        _mm_store_pd(wdot_array+(7*spec_stride)+(idx<<1),result);
      }
      // 8. N2
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_mul_pd(result,_mm_set1_pd(0.0280134));
        _mm_store_pd(wdot_array+(8*spec_stride)+(idx<<1),result);
      }
    }
    remaining_elmts -= 294;
    mass_frac_array += 294;
    wdot_array += 294;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 2) == 0);
    // Temperature dependent
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      temperature[idx] = _mm_load_pd(temperature_array);
      temperature_array += 2;
      otc[idx] = _mm_div_pd(_mm_set1_pd(1.0),temperature[idx]);
      ortc[idx] = 
        _mm_div_pd(_mm_set1_pd(1.0),_mm_mul_pd(temperature[idx],_mm_set1_pd(R0c))); 
      double t1 = _mm_cvtsd_f64(temperature[idx]);
      double log1 = log(t1);
      double t0 = 
        _mm_cvtsd_f64(_mm_shuffle_pd(temperature[idx],temperature[idx],1)); 
      double log0 = log(t0);
      vlntemp[idx] = _mm_set_pd(log0,log1);
      prt[idx] = 
        _mm_div_pd(_mm_set1_pd(PA),_mm_mul_pd(temperature[idx],_mm_set1_pd(R0))); 
      oprt[idx] = _mm_div_pd(_mm_set1_pd(1.0),prt[idx]);
    }
    // Gibbs computation
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d tk1 = temperature[idx];
      __m128d tklog = vlntemp[idx];
      __m128d tk2 = _mm_mul_pd(tk1,tk1);
      __m128d tk3 = _mm_mul_pd(tk1,tk2);
      __m128d tk4 = _mm_mul_pd(tk1,tk3);
      __m128d tk5 = _mm_mul_pd(tk1,tk4);
      // Species H2
      {
        __m128d cgspl_NC_H2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.991423),_mm_set1_pd(3.298124), 0); 
            cgspl_NC_H2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2 = _mm_mul_pd(cgspl_NC_H2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.04200386399999999),_mm_set1_pd(-0.04949665199999999), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.352118959999999e-04),_mm_set1_pd(1.954323599999999e-03), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(1.329347231999999e-06),_mm_set1_pd(1.364462495999999e-05), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-1.640997273599998e-08),_mm_set1_pd(-4.287035289599994e-06),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-1.35511),_mm_set1_pd(-3.294094),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-6.958616666666668),_mm_set1_pd(-8.437675),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.991423),_mm_set1_pd(3.298124), 1); 
            cgspl_NC_H2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2 = _mm_mul_pd(cgspl_NC_H2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.04200386399999999),_mm_set1_pd(-0.04949665199999999), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.352118959999999e-04),_mm_set1_pd(1.954323599999999e-03), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(1.329347231999999e-06),_mm_set1_pd(1.364462495999999e-05), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-1.640997273599998e-08),_mm_set1_pd(-4.287035289599994e-06),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-1.35511),_mm_set1_pd(-3.294094),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-6.958616666666668),_mm_set1_pd(-8.437675),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.991423),_mm_set1_pd(3.298124), 2); 
            cgspl_NC_H2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2 = _mm_mul_pd(cgspl_NC_H2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.04200386399999999),_mm_set1_pd(-0.04949665199999999), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.352118959999999e-04),_mm_set1_pd(1.954323599999999e-03), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(1.329347231999999e-06),_mm_set1_pd(1.364462495999999e-05), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-1.640997273599998e-08),_mm_set1_pd(-4.287035289599994e-06),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-1.35511),_mm_set1_pd(-3.294094),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-6.958616666666668),_mm_set1_pd(-8.437675),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.991423),_mm_set1_pd(3.298124), 3); 
            cgspl_NC_H2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2 = _mm_mul_pd(cgspl_NC_H2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.04200386399999999),_mm_set1_pd(-0.04949665199999999), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.352118959999999e-04),_mm_set1_pd(1.954323599999999e-03), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(1.329347231999999e-06),_mm_set1_pd(1.364462495999999e-05), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-1.640997273599998e-08),_mm_set1_pd(-4.287035289599994e-06),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-1.35511),_mm_set1_pd(-3.294094),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-6.958616666666668),_mm_set1_pd(-8.437675),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2 = _mm_add_pd(cgspl_NC_H2,temp);
          }
        }
        cgspl[0+idx] = cgspl_NC_H2;
      }
      // Species O2
      {
        __m128d cgspl_NC_O2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(3.697578),_mm_set1_pd(3.212936), 0); 
            cgspl_NC_O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O2 = _mm_mul_pd(cgspl_NC_O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.03681118199999999),_mm_set1_pd(-0.06764915999999999), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.021220799999999e-04),_mm_set1_pd(1.381476e-03), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-2.556404639999998e-06),_mm_set1_pd(-1.891982879999998e-04), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.178255807999999e-08),_mm_set1_pd(9.091236787199989e-06),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.189166),_mm_set1_pd(6.034738),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-10.28275),_mm_set1_pd(-8.377075000000001),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(3.697578),_mm_set1_pd(3.212936), 1); 
            cgspl_NC_O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O2 = _mm_mul_pd(cgspl_NC_O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.03681118199999999),_mm_set1_pd(-0.06764915999999999), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.021220799999999e-04),_mm_set1_pd(1.381476e-03), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-2.556404639999998e-06),_mm_set1_pd(-1.891982879999998e-04), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.178255807999999e-08),_mm_set1_pd(9.091236787199989e-06),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.189166),_mm_set1_pd(6.034738),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-10.28275),_mm_set1_pd(-8.377075000000001),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(3.697578),_mm_set1_pd(3.212936), 2); 
            cgspl_NC_O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O2 = _mm_mul_pd(cgspl_NC_O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.03681118199999999),_mm_set1_pd(-0.06764915999999999), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.021220799999999e-04),_mm_set1_pd(1.381476e-03), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-2.556404639999998e-06),_mm_set1_pd(-1.891982879999998e-04), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.178255807999999e-08),_mm_set1_pd(9.091236787199989e-06),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.189166),_mm_set1_pd(6.034738),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-10.28275),_mm_set1_pd(-8.377075000000001),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(3.697578),_mm_set1_pd(3.212936), 3); 
            cgspl_NC_O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O2 = _mm_mul_pd(cgspl_NC_O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.03681118199999999),_mm_set1_pd(-0.06764915999999999), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.021220799999999e-04),_mm_set1_pd(1.381476e-03), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-2.556404639999998e-06),_mm_set1_pd(-1.891982879999998e-04), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.178255807999999e-08),_mm_set1_pd(9.091236787199989e-06),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.189166),_mm_set1_pd(6.034738),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-10.28275),_mm_set1_pd(-8.377075000000001),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O2 = _mm_add_pd(cgspl_NC_O2,temp);
          }
        }
        cgspl[147+idx] = cgspl_NC_O2;
      }
      // Species O
      {
        __m128d cgspl_NC_O, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.54206),_mm_set1_pd(2.946429), 0); 
            cgspl_NC_O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O = _mm_mul_pd(cgspl_NC_O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(1.6530372e-03),_mm_set1_pd(0.09828995999999997), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(7.446727199999996e-06),_mm_set1_pd(-5.810476799999997e-03), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-6.553536479999994e-07),_mm_set1_pd(2.308093919999998e-04), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(4.528796313599995e-09),_mm_set1_pd(-4.033873612799995e-06),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(4.920308),_mm_set1_pd(2.963995),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(243.59),_mm_set1_pd(242.897),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.54206),_mm_set1_pd(2.946429), 1); 
            cgspl_NC_O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O = _mm_mul_pd(cgspl_NC_O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(1.6530372e-03),_mm_set1_pd(0.09828995999999997), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(7.446727199999996e-06),_mm_set1_pd(-5.810476799999997e-03), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-6.553536479999994e-07),_mm_set1_pd(2.308093919999998e-04), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(4.528796313599995e-09),_mm_set1_pd(-4.033873612799995e-06),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(4.920308),_mm_set1_pd(2.963995),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(243.59),_mm_set1_pd(242.897),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.54206),_mm_set1_pd(2.946429), 2); 
            cgspl_NC_O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O = _mm_mul_pd(cgspl_NC_O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(1.6530372e-03),_mm_set1_pd(0.09828995999999997), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(7.446727199999996e-06),_mm_set1_pd(-5.810476799999997e-03), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-6.553536479999994e-07),_mm_set1_pd(2.308093919999998e-04), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(4.528796313599995e-09),_mm_set1_pd(-4.033873612799995e-06),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(4.920308),_mm_set1_pd(2.963995),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(243.59),_mm_set1_pd(242.897),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.54206),_mm_set1_pd(2.946429), 3); 
            cgspl_NC_O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_O = _mm_mul_pd(cgspl_NC_O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(1.6530372e-03),_mm_set1_pd(0.09828995999999997), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(7.446727199999996e-06),_mm_set1_pd(-5.810476799999997e-03), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-6.553536479999994e-07),_mm_set1_pd(2.308093919999998e-04), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(4.528796313599995e-09),_mm_set1_pd(-4.033873612799995e-06),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(4.920308),_mm_set1_pd(2.963995),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(243.59),_mm_set1_pd(242.897),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_O = _mm_add_pd(cgspl_NC_O,temp);
          }
        }
        cgspl[294+idx] = cgspl_NC_O;
      }
      // Species OH
      {
        __m128d cgspl_NC_OH, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.86472886),_mm_set1_pd(4.12530561), 0); 
            cgspl_NC_OH = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_OH = _mm_mul_pd(cgspl_NC_OH,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.06339026879999998),_mm_set1_pd(0.1935269634), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(6.217986191999998e-04),_mm_set1_pd(-0.015666352584), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-4.395148905599995e-06),_mm_set1_pd(8.349892459199992e-04), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.380974842367998e-08),_mm_set1_pd(-2.138269145471997e-05),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.70164073),_mm_set1_pd(-0.69043296),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(30.69690625),_mm_set1_pd(27.88590941666667),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.86472886),_mm_set1_pd(4.12530561), 1); 
            cgspl_NC_OH = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_OH = _mm_mul_pd(cgspl_NC_OH,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.06339026879999998),_mm_set1_pd(0.1935269634), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(6.217986191999998e-04),_mm_set1_pd(-0.015666352584), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-4.395148905599995e-06),_mm_set1_pd(8.349892459199992e-04), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.380974842367998e-08),_mm_set1_pd(-2.138269145471997e-05),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.70164073),_mm_set1_pd(-0.69043296),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(30.69690625),_mm_set1_pd(27.88590941666667),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.86472886),_mm_set1_pd(4.12530561), 2); 
            cgspl_NC_OH = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_OH = _mm_mul_pd(cgspl_NC_OH,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.06339026879999998),_mm_set1_pd(0.1935269634), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(6.217986191999998e-04),_mm_set1_pd(-0.015666352584), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-4.395148905599995e-06),_mm_set1_pd(8.349892459199992e-04), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.380974842367998e-08),_mm_set1_pd(-2.138269145471997e-05),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.70164073),_mm_set1_pd(-0.69043296),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(30.69690625),_mm_set1_pd(27.88590941666667),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.86472886),_mm_set1_pd(4.12530561), 3); 
            cgspl_NC_OH = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_OH = _mm_mul_pd(cgspl_NC_OH,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.06339026879999998),_mm_set1_pd(0.1935269634), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(6.217986191999998e-04),_mm_set1_pd(-0.015666352584), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-4.395148905599995e-06),_mm_set1_pd(8.349892459199992e-04), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.380974842367998e-08),_mm_set1_pd(-2.138269145471997e-05),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.70164073),_mm_set1_pd(-0.69043296),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(30.69690625),_mm_set1_pd(27.88590941666667),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_OH = _mm_add_pd(cgspl_NC_OH,temp);
          }
        }
        cgspl[441+idx] = cgspl_NC_OH;
      }
      // Species H2O
      {
        __m128d cgspl_NC_H2O, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.672146),_mm_set1_pd(3.386842), 0); 
            cgspl_NC_H2O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O = _mm_mul_pd(cgspl_NC_H2O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.18337758),_mm_set1_pd(-0.2084989199999999), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(2.095262399999999e-03),_mm_set1_pd(0.01525127039999999), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.729434239999998e-05),_mm_set1_pd(-1.003475663999999e-03), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(6.626829542399991e-08),_mm_set1_pd(2.598830438399997e-05),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(6.862817),_mm_set1_pd(2.590233),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-249.1600833333334),_mm_set1_pd(-251.7342500000001),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.672146),_mm_set1_pd(3.386842), 1); 
            cgspl_NC_H2O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O = _mm_mul_pd(cgspl_NC_H2O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.18337758),_mm_set1_pd(-0.2084989199999999), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(2.095262399999999e-03),_mm_set1_pd(0.01525127039999999), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.729434239999998e-05),_mm_set1_pd(-1.003475663999999e-03), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(6.626829542399991e-08),_mm_set1_pd(2.598830438399997e-05),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(6.862817),_mm_set1_pd(2.590233),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-249.1600833333334),_mm_set1_pd(-251.7342500000001),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.672146),_mm_set1_pd(3.386842), 2); 
            cgspl_NC_H2O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O = _mm_mul_pd(cgspl_NC_H2O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.18337758),_mm_set1_pd(-0.2084989199999999), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(2.095262399999999e-03),_mm_set1_pd(0.01525127039999999), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.729434239999998e-05),_mm_set1_pd(-1.003475663999999e-03), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(6.626829542399991e-08),_mm_set1_pd(2.598830438399997e-05),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(6.862817),_mm_set1_pd(2.590233),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-249.1600833333334),_mm_set1_pd(-251.7342500000001),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.672146),_mm_set1_pd(3.386842), 3); 
            cgspl_NC_H2O = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O = _mm_mul_pd(cgspl_NC_H2O,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.18337758),_mm_set1_pd(-0.2084989199999999), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(2.095262399999999e-03),_mm_set1_pd(0.01525127039999999), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.729434239999998e-05),_mm_set1_pd(-1.003475663999999e-03), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(6.626829542399991e-08),_mm_set1_pd(2.598830438399997e-05),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(6.862817),_mm_set1_pd(2.590233),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-249.1600833333334),_mm_set1_pd(-251.7342500000001),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O = _mm_add_pd(cgspl_NC_H2O,temp);
          }
        }
        cgspl[588+idx] = cgspl_NC_H2O;
      }
      // Species H
      {
        __m128d cgspl_NC_H, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = _mm_blend_pd(_mm_set1_pd(2.5),_mm_set1_pd(2.5), 0);
            cgspl_NC_H = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H = _mm_mul_pd(cgspl_NC_H,temp);
            __m128d coeff2 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff3 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff4 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-0.4601176),_mm_set1_pd(-0.4601176),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(212.2635833333334),_mm_set1_pd(212.2635833333334),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
          }
          else
          {
            __m128d coeff1 = _mm_blend_pd(_mm_set1_pd(2.5),_mm_set1_pd(2.5), 1);
            cgspl_NC_H = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H = _mm_mul_pd(cgspl_NC_H,temp);
            __m128d coeff2 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff3 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff4 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-0.4601176),_mm_set1_pd(-0.4601176),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(212.2635833333334),_mm_set1_pd(212.2635833333334),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = _mm_blend_pd(_mm_set1_pd(2.5),_mm_set1_pd(2.5), 2);
            cgspl_NC_H = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H = _mm_mul_pd(cgspl_NC_H,temp);
            __m128d coeff2 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff3 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff4 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-0.4601176),_mm_set1_pd(-0.4601176),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(212.2635833333334),_mm_set1_pd(212.2635833333334),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
          }
          else
          {
            __m128d coeff1 = _mm_blend_pd(_mm_set1_pd(2.5),_mm_set1_pd(2.5), 3);
            cgspl_NC_H = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H = _mm_mul_pd(cgspl_NC_H,temp);
            __m128d coeff2 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff3 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff4 = _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(-0.0),_mm_set1_pd(-0.0),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(-0.4601176),_mm_set1_pd(-0.4601176),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(212.2635833333334),_mm_set1_pd(212.2635833333334),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H = _mm_add_pd(cgspl_NC_H,temp);
          }
        }
        cgspl[735+idx] = cgspl_NC_H;
      }
      // Species HO2
      {
        __m128d cgspl_NC_HO2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.0172109),_mm_set1_pd(4.30179801), 0); 
            cgspl_NC_HO2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_HO2 = _mm_mul_pd(cgspl_NC_HO2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.1343892078),_mm_set1_pd(0.2849472305999999), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.520779559999999e-03),_mm_set1_pd(-0.05077989383999998), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.645147727999999e-05),_mm_set1_pd(3.495800073599997e-03), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.118795690879999e-07),_mm_set1_pd(-9.634206085631988e-05),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.78510215),_mm_set1_pd(3.71666245),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(0.9321392750000002),_mm_set1_pd(2.456733666666667),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.0172109),_mm_set1_pd(4.30179801), 1); 
            cgspl_NC_HO2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_HO2 = _mm_mul_pd(cgspl_NC_HO2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.1343892078),_mm_set1_pd(0.2849472305999999), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.520779559999999e-03),_mm_set1_pd(-0.05077989383999998), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.645147727999999e-05),_mm_set1_pd(3.495800073599997e-03), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.118795690879999e-07),_mm_set1_pd(-9.634206085631988e-05),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.78510215),_mm_set1_pd(3.71666245),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(0.9321392750000002),_mm_set1_pd(2.456733666666667),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.0172109),_mm_set1_pd(4.30179801), 2); 
            cgspl_NC_HO2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_HO2 = _mm_mul_pd(cgspl_NC_HO2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.1343892078),_mm_set1_pd(0.2849472305999999), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.520779559999999e-03),_mm_set1_pd(-0.05077989383999998), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.645147727999999e-05),_mm_set1_pd(3.495800073599997e-03), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.118795690879999e-07),_mm_set1_pd(-9.634206085631988e-05),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.78510215),_mm_set1_pd(3.71666245),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(0.9321392750000002),_mm_set1_pd(2.456733666666667),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.0172109),_mm_set1_pd(4.30179801), 3); 
            cgspl_NC_HO2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_HO2 = _mm_mul_pd(cgspl_NC_HO2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.1343892078),_mm_set1_pd(0.2849472305999999), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.520779559999999e-03),_mm_set1_pd(-0.05077989383999998), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.645147727999999e-05),_mm_set1_pd(3.495800073599997e-03), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.118795690879999e-07),_mm_set1_pd(-9.634206085631988e-05),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(3.78510215),_mm_set1_pd(3.71666245),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(0.9321392750000002),_mm_set1_pd(2.456733666666667),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_HO2 = _mm_add_pd(cgspl_NC_HO2,temp);
          }
        }
        cgspl[882+idx] = cgspl_NC_HO2;
      }
      // Species H2O2
      {
        __m128d cgspl_NC_H2O2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.573167),_mm_set1_pd(3.388754), 0); 
            cgspl_NC_H2O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O2 = _mm_mul_pd(cgspl_NC_H2O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.2601681599999999),_mm_set1_pd(-0.3941535599999999), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.539253599999998e-03),_mm_set1_pd(3.564031199999998e-04), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-3.382421759999996e-05),_mm_set1_pd(6.661160639999993e-04), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.484338867199998e-07),_mm_set1_pd(-2.562466751999997e-05),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(0.5011370000000001),_mm_set1_pd(6.785363),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-150.058),_mm_set1_pd(-147.1929166666667),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.573167),_mm_set1_pd(3.388754), 1); 
            cgspl_NC_H2O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O2 = _mm_mul_pd(cgspl_NC_H2O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.2601681599999999),_mm_set1_pd(-0.3941535599999999), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.539253599999998e-03),_mm_set1_pd(3.564031199999998e-04), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-3.382421759999996e-05),_mm_set1_pd(6.661160639999993e-04), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.484338867199998e-07),_mm_set1_pd(-2.562466751999997e-05),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(0.5011370000000001),_mm_set1_pd(6.785363),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-150.058),_mm_set1_pd(-147.1929166666667),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.573167),_mm_set1_pd(3.388754), 2); 
            cgspl_NC_H2O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O2 = _mm_mul_pd(cgspl_NC_H2O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.2601681599999999),_mm_set1_pd(-0.3941535599999999), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.539253599999998e-03),_mm_set1_pd(3.564031199999998e-04), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-3.382421759999996e-05),_mm_set1_pd(6.661160639999993e-04), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.484338867199998e-07),_mm_set1_pd(-2.562466751999997e-05),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(0.5011370000000001),_mm_set1_pd(6.785363),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-150.058),_mm_set1_pd(-147.1929166666667),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(4.573167),_mm_set1_pd(3.388754), 3); 
            cgspl_NC_H2O2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_H2O2 = _mm_mul_pd(cgspl_NC_H2O2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.2601681599999999),_mm_set1_pd(-0.3941535599999999), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(3.539253599999998e-03),_mm_set1_pd(3.564031199999998e-04), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-3.382421759999996e-05),_mm_set1_pd(6.661160639999993e-04), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(1.484338867199998e-07),_mm_set1_pd(-2.562466751999997e-05),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(0.5011370000000001),_mm_set1_pd(6.785363),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-150.058),_mm_set1_pd(-147.1929166666667),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_H2O2 = _mm_add_pd(cgspl_NC_H2O2,temp);
          }
        }
        cgspl[1029+idx] = cgspl_NC_H2O2;
      }
      // Species N2
      {
        __m128d cgspl_NC_N2, temp;
        if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > 8.333333333333336)
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.92664),_mm_set1_pd(3.298677), 0); 
            cgspl_NC_N2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_N2 = _mm_mul_pd(cgspl_NC_N2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.08927861999999998),_mm_set1_pd(-0.08449439999999998), 
              0); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.364342639999999e-03),_mm_set1_pd(9.511732799999995e-03), 
              0); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.453973759999998e-05),_mm_set1_pd(-8.123781599999992e-04), 
              0); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(7.001874316799991e-08),_mm_set1_pd(2.534825663999997e-05),0); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.980528),_mm_set1_pd(3.950372),0); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-7.689980833333334),_mm_set1_pd(-8.507500000000002),0); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.92664),_mm_set1_pd(3.298677), 1); 
            cgspl_NC_N2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_N2 = _mm_mul_pd(cgspl_NC_N2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.08927861999999998),_mm_set1_pd(-0.08449439999999998), 
              1); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.364342639999999e-03),_mm_set1_pd(9.511732799999995e-03), 
              1); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.453973759999998e-05),_mm_set1_pd(-8.123781599999992e-04), 
              1); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(7.001874316799991e-08),_mm_set1_pd(2.534825663999997e-05),1); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.980528),_mm_set1_pd(3.950372),1); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-7.689980833333334),_mm_set1_pd(-8.507500000000002),1); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
          }
        }
        else
        {
          if (_mm_cvtsd_f64(tk1) > 8.333333333333336)
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.92664),_mm_set1_pd(3.298677), 2); 
            cgspl_NC_N2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_N2 = _mm_mul_pd(cgspl_NC_N2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.08927861999999998),_mm_set1_pd(-0.08449439999999998), 
              2); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.364342639999999e-03),_mm_set1_pd(9.511732799999995e-03), 
              2); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.453973759999998e-05),_mm_set1_pd(-8.123781599999992e-04), 
              2); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(7.001874316799991e-08),_mm_set1_pd(2.534825663999997e-05),2); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.980528),_mm_set1_pd(3.950372),2); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-7.689980833333334),_mm_set1_pd(-8.507500000000002),2); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
          }
          else
          {
            __m128d coeff1 = 
              _mm_blend_pd(_mm_set1_pd(2.92664),_mm_set1_pd(3.298677), 3); 
            cgspl_NC_N2 = _mm_mul_pd(coeff1,tk1);
            __m128d coefflog = _mm_set1_pd(-3.787491742782046);
            temp = _mm_sub_pd(coefflog,tklog);
            cgspl_NC_N2 = _mm_mul_pd(cgspl_NC_N2,temp);
            __m128d coeff2 = 
              _mm_blend_pd(_mm_set1_pd(-0.08927861999999998),_mm_set1_pd(-0.08449439999999998), 
              3); 
            temp = _mm_mul_pd(coeff2,tk2);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff3 = 
              _mm_blend_pd(_mm_set1_pd(1.364342639999999e-03),_mm_set1_pd(9.511732799999995e-03), 
              3); 
            temp = _mm_mul_pd(coeff3,tk3);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff4 = 
              _mm_blend_pd(_mm_set1_pd(-1.453973759999998e-05),_mm_set1_pd(-8.123781599999992e-04), 
              3); 
            temp = _mm_mul_pd(coeff4,tk4);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff5 = 
              _mm_blend_pd(_mm_set1_pd(7.001874316799991e-08),_mm_set1_pd(2.534825663999997e-05),3); 
            temp = _mm_mul_pd(coeff5,tk5);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
            __m128d coeff7 = 
              _mm_blend_pd(_mm_set1_pd(5.980528),_mm_set1_pd(3.950372),3); 
            temp = _mm_mul_pd(coeff7,tk1);
            __m128d coeff6 = 
              _mm_blend_pd(_mm_set1_pd(-7.689980833333334),_mm_set1_pd(-8.507500000000002),3); 
            temp = _mm_sub_pd(coeff6,temp);
            cgspl_NC_N2 = _mm_add_pd(cgspl_NC_N2,temp);
          }
        }
        cgspl[1176+idx] = cgspl_NC_N2;
      }
    }
    // Compute mole fractions
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d avmolwt = _mm_load_pd(avmolwt_array);
      __m128d pressure = _mm_load_pd(pressure_array);
      avmolwt_array += 2;
      pressure_array += 2;
      __m128d sumyow = 
        _mm_mul_pd(_mm_mul_pd(temperature[idx],avmolwt),_mm_set1_pd(7.03444160806564)); 
      sumyow = _mm_div_pd(pressure,sumyow);
      __m128d mask, mass_frac;
      __m128d small = _mm_set1_pd(1e-200);
      mass_frac = _mm_load_pd(mass_frac_array+(0*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[0]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[0+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(1*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[1]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[147+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(2*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[2]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[294+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(3*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[3]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[441+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(4*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[4]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[588+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(5*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[5]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[735+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(6*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[6]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[882+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(7*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[7]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[1029+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
      mass_frac = _mm_load_pd(mass_frac_array+(8*spec_stride)+(idx<<1));
      mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses[8]));
      mask = _mm_cmpgt_pd(mass_frac,small);
      mass_frac = _mm_and_pd(mass_frac,mask);
      mole_frac[1176+idx] = 
        _mm_mul_pd(sumyow,_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small))); 
    }
    // Compute third body values
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d ctot = _mm_set1_pd(0.0);
      ctot = _mm_add_pd(ctot,mole_frac[0+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[147+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[294+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[441+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[588+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[735+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[882+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[1029+idx]);
      ctot = _mm_add_pd(ctot,mole_frac[1176+idx]);
      thbctemp[0+idx] = ctot;
      thbctemp[0+idx] = 
        _mm_add_pd(thbctemp[0+idx],_mm_mul_pd(_mm_set1_pd(1.5),mole_frac[0+idx])); 
      thbctemp[0+idx] = 
        _mm_add_pd(thbctemp[0+idx],_mm_mul_pd(_mm_set1_pd(11.0),mole_frac[588+idx])); 
      thbctemp[147+idx] = ctot;
      thbctemp[147+idx] = _mm_add_pd(thbctemp[147+idx],mole_frac[0+idx]);
      thbctemp[147+idx] = 
        _mm_sub_pd(thbctemp[147+idx],_mm_mul_pd(_mm_set1_pd(0.22),mole_frac[147+idx])); 
      thbctemp[147+idx] = 
        _mm_add_pd(thbctemp[147+idx],_mm_mul_pd(_mm_set1_pd(10.0),mole_frac[588+idx])); 
    }
    // Reaction rates
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      //   0)  O2 + H <=> O + OH
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-0.406),vlntemp[idx]);
            arrtemp = 
              _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(1.6599e+04),ortc[idx])); 
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(5.606052175734568e+15),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[147+idx]);
        xik = _mm_add_pd(xik,cgspl[294+idx]);
        xik = _mm_add_pd(xik,cgspl[441+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[0+idx] = forward;
        rr_f[0+idx] = _mm_mul_pd(rr_f[0+idx],mole_frac[147+idx]);
        rr_f[0+idx] = _mm_mul_pd(rr_f[0+idx],mole_frac[735+idx]);
        rr_r[0+idx] = reverse;
        rr_r[0+idx] = _mm_mul_pd(rr_r[0+idx],mole_frac[294+idx]);
        rr_r[0+idx] = _mm_mul_pd(rr_r[0+idx],mole_frac[441+idx]);
      }
      //   1)  H2 + O <=> OH + H
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(2.67),vlntemp[idx]);
            arrtemp = 
              _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(6.29e+03),ortc[idx])); 
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(1.996278696128123e+11),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[0+idx]);
        xik = _mm_sub_pd(xik,cgspl[294+idx]);
        xik = _mm_add_pd(xik,cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[735+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[147+idx] = forward;
        rr_f[147+idx] = _mm_mul_pd(rr_f[147+idx],mole_frac[0+idx]);
        rr_f[147+idx] = _mm_mul_pd(rr_f[147+idx],mole_frac[294+idx]);
        rr_r[147+idx] = reverse;
        rr_r[147+idx] = _mm_mul_pd(rr_r[147+idx],mole_frac[441+idx]);
        rr_r[147+idx] = _mm_mul_pd(rr_r[147+idx],mole_frac[735+idx]);
      }
      //   2)  H2 + OH <=> H2O + H
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(1.51),vlntemp[idx]);
            arrtemp = 
              _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(3.43e+03),ortc[idx])); 
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(3.288220749682686e+12),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[0+idx]);
        xik = _mm_sub_pd(xik,cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[588+idx]);
        xik = _mm_add_pd(xik,cgspl[735+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[294+idx] = forward;
        rr_f[294+idx] = _mm_mul_pd(rr_f[294+idx],mole_frac[0+idx]);
        rr_f[294+idx] = _mm_mul_pd(rr_f[294+idx],mole_frac[441+idx]);
        rr_r[294+idx] = reverse;
        rr_r[294+idx] = _mm_mul_pd(rr_r[294+idx],mole_frac[588+idx]);
        rr_r[294+idx] = _mm_mul_pd(rr_r[294+idx],mole_frac[735+idx]);
      }
      //   3)  O + H2O <=> 2 OH
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(2.02),vlntemp[idx]);
            arrtemp = 
              _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(1.34e+04),ortc[idx])); 
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(5.195724313044046e+11),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[294+idx]);
        xik = _mm_add_pd(xik,_mm_mul_pd(_mm_set1_pd(double(2)),cgspl[441+idx]));
        xik = _mm_sub_pd(xik,cgspl[588+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[441+idx] = forward;
        rr_f[441+idx] = _mm_mul_pd(rr_f[441+idx],mole_frac[294+idx]);
        rr_f[441+idx] = _mm_mul_pd(rr_f[441+idx],mole_frac[588+idx]);
        rr_r[441+idx] = reverse;
        rr_r[441+idx] = _mm_mul_pd(rr_r[441+idx],mole_frac[441+idx]);
        rr_r[441+idx] = _mm_mul_pd(rr_r[441+idx],mole_frac[441+idx]);
      }
      //   4)  H2 + M <=> 2 H + M
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-1.4),vlntemp[idx]);
            arrtemp = 
              _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(1.0438e+05),ortc[idx])); 
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(6.203984453113697e+17),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[0+idx]);
        xik = _mm_add_pd(xik,_mm_mul_pd(_mm_set1_pd(double(2)),cgspl[735+idx]));
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,oprt[idx]);
        rr_f[588+idx] = forward;
        rr_f[588+idx] = _mm_mul_pd(rr_f[588+idx],mole_frac[0+idx]);
        rr_r[588+idx] = reverse;
        rr_r[588+idx] = _mm_mul_pd(rr_r[588+idx],mole_frac[735+idx]);
        rr_r[588+idx] = _mm_mul_pd(rr_r[588+idx],mole_frac[735+idx]);
        rr_f[588+idx] = _mm_mul_pd(rr_f[588+idx],thbctemp[0+idx]);
        rr_r[588+idx] = _mm_mul_pd(rr_r[588+idx],thbctemp[0+idx]);
      }
      //   5)  2 O + M <=> O2 + M
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-0.5),vlntemp[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(6.21278022363831e+15), arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[147+idx];
        xik = 
          _mm_add_pd(xik,_mm_mul_pd(_mm_set1_pd(double(-2)),cgspl[294+idx])); 
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,prt[idx]);
        rr_f[735+idx] = forward;
        rr_f[735+idx] = _mm_mul_pd(rr_f[735+idx],mole_frac[294+idx]);
        rr_f[735+idx] = _mm_mul_pd(rr_f[735+idx],mole_frac[294+idx]);
        rr_r[735+idx] = reverse;
        rr_r[735+idx] = _mm_mul_pd(rr_r[735+idx],mole_frac[147+idx]);
        rr_f[735+idx] = _mm_mul_pd(rr_f[735+idx],thbctemp[0+idx]);
        rr_r[735+idx] = _mm_mul_pd(rr_r[735+idx],thbctemp[0+idx]);
      }
      //   6)  O + H + M <=> OH + M
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = 
              _mm_mul_pd(_mm_set1_pd(4.336624958847576e+17),otc[idx]); 
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[294+idx]);
        xik = _mm_add_pd(xik,cgspl[441+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,prt[idx]);
        rr_f[882+idx] = forward;
        rr_f[882+idx] = _mm_mul_pd(rr_f[882+idx],mole_frac[294+idx]);
        rr_f[882+idx] = _mm_mul_pd(rr_f[882+idx],mole_frac[735+idx]);
        rr_r[882+idx] = reverse;
        rr_r[882+idx] = _mm_mul_pd(rr_r[882+idx],mole_frac[441+idx]);
        rr_f[882+idx] = _mm_mul_pd(rr_f[882+idx],thbctemp[0+idx]);
        rr_r[882+idx] = _mm_mul_pd(rr_r[882+idx],thbctemp[0+idx]);
      }
      //   7)  OH + H + M <=> H2O + M
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = 
              _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(2.913162007428368e+19),otc[idx]),otc[idx]); 
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[588+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,prt[idx]);
        rr_f[1029+idx] = forward;
        rr_f[1029+idx] = _mm_mul_pd(rr_f[1029+idx],mole_frac[441+idx]);
        rr_f[1029+idx] = _mm_mul_pd(rr_f[1029+idx],mole_frac[735+idx]);
        rr_r[1029+idx] = reverse;
        rr_r[1029+idx] = _mm_mul_pd(rr_r[1029+idx],mole_frac[588+idx]);
        rr_f[1029+idx] = _mm_mul_pd(rr_f[1029+idx],thbctemp[0+idx]);
        rr_r[1029+idx] = _mm_mul_pd(rr_r[1029+idx],thbctemp[0+idx]);
      }
      //   8)  O2 + H (+M) <=> HO2 (+M)
      {
        __m128d forward, reverse;
        __m128d rr_k0, rr_kinf;
        {
          __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-1.72),vlntemp[idx]);
          arrtemp = 
            _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(524.8),ortc[idx])); 
          arrtemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
          arrtemp = _mm_mul_pd(_mm_set1_pd(1.864740588650248e+18),arrtemp);
          rr_k0 = arrtemp;
        }
        {
          __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(0.6),vlntemp[idx]);
          arrtemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
          arrtemp = _mm_mul_pd(_mm_set1_pd(2.879017473981534e+14), arrtemp);
          rr_kinf = arrtemp;
        }
        __m128d pr = _mm_mul_pd(_mm_div_pd(rr_k0,rr_kinf),thbctemp[147+idx]);
        __m128d fcent;
        {
          fcent = _mm_mul_pd(temperature[idx], _mm_set1_pd(-1.2e+32));
          fcent = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(fcent,fcent,1))),exp(_mm_cvtsd_f64(fcent))); 
          fcent = _mm_mul_pd(fcent, _mm_set1_pd(0.2));
          {
            __m128d fctemp = _mm_mul_pd(temperature[idx], 
              _mm_set1_pd(-1.2e-28)); 
            fctemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(fctemp,fctemp,1))),exp(_mm_cvtsd_f64(fctemp))); 
            fcent = _mm_add_pd(fcent, _mm_mul_pd(fctemp, _mm_set1_pd(0.8)));
          }
          fcent = 
            _mm_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(fcent,fcent,1))), 
            log10(_mm_cvtsd_f64(fcent))); 
          __m128d flogpr = 
            _mm_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(pr,pr,1))),log10(_mm_cvtsd_f64(pr))); 
          flogpr = _mm_sub_pd(flogpr, 
            _mm_add_pd(_mm_set1_pd(0.4),_mm_mul_pd(_mm_set1_pd(0.67),fcent))); 
          __m128d fdenom = 
            _mm_add_pd(_mm_set1_pd(0.75),_mm_mul_pd(_mm_set1_pd(-1.27),fcent)); 
          fdenom = _mm_add_pd(fdenom,_mm_mul_pd(_mm_set1_pd(-0.14),flogpr));
          __m128d fquan = _mm_div_pd(flogpr,fdenom);
          fquan = 
            _mm_div_pd(fcent,_mm_add_pd(_mm_set1_pd(1.0),_mm_mul_pd(fquan,fquan))); 
          __m128d ftemp = _mm_mul_pd(fquan,_mm_set1_pd(DLn10));
          ftemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(ftemp,ftemp,1))),exp(_mm_cvtsd_f64(ftemp))); 
          forward = _mm_mul_pd(rr_kinf,pr);
          forward = _mm_div_pd(forward,_mm_add_pd(_mm_set1_pd(1.0),pr));
          forward = _mm_mul_pd(forward, ftemp);
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[147+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        xik = _mm_add_pd(xik,cgspl[882+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,prt[idx]);
        rr_f[1176+idx] = forward;
        rr_f[1176+idx] = _mm_mul_pd(rr_f[1176+idx],mole_frac[147+idx]);
        rr_f[1176+idx] = _mm_mul_pd(rr_f[1176+idx],mole_frac[735+idx]);
        rr_r[1176+idx] = reverse;
        rr_r[1176+idx] = _mm_mul_pd(rr_r[1176+idx],mole_frac[882+idx]);
      }
      //   9)  H + HO2 <=> H2 + O2
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-823.0), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(1.832532226988623e+14),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[0+idx];
        xik = _mm_add_pd(xik,cgspl[147+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        xik = _mm_sub_pd(xik,cgspl[882+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[1323+idx] = forward;
        rr_f[1323+idx] = _mm_mul_pd(rr_f[1323+idx],mole_frac[735+idx]);
        rr_f[1323+idx] = _mm_mul_pd(rr_f[1323+idx],mole_frac[882+idx]);
        rr_r[1323+idx] = reverse;
        rr_r[1323+idx] = _mm_mul_pd(rr_r[1323+idx],mole_frac[0+idx]);
        rr_r[1323+idx] = _mm_mul_pd(rr_r[1323+idx],mole_frac[147+idx]);
      }
      //  10)  H + HO2 <=> 2 OH
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-295.0), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(7.814756406537628e+14),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_mul_pd(_mm_set1_pd(double(2)),cgspl[441+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        xik = _mm_sub_pd(xik,cgspl[882+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[1470+idx] = forward;
        rr_f[1470+idx] = _mm_mul_pd(rr_f[1470+idx],mole_frac[735+idx]);
        rr_f[1470+idx] = _mm_mul_pd(rr_f[1470+idx],mole_frac[882+idx]);
        rr_r[1470+idx] = reverse;
        rr_r[1470+idx] = _mm_mul_pd(rr_r[1470+idx],mole_frac[441+idx]);
        rr_r[1470+idx] = _mm_mul_pd(rr_r[1470+idx],mole_frac[441+idx]);
      }
      //  11)  O + HO2 <=> O2 + OH
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_set1_pd(3.587788998622304e+14);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[147+idx];
        xik = _mm_sub_pd(xik,cgspl[294+idx]);
        xik = _mm_add_pd(xik,cgspl[441+idx]);
        xik = _mm_sub_pd(xik,cgspl[882+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[1617+idx] = forward;
        rr_f[1617+idx] = _mm_mul_pd(rr_f[1617+idx],mole_frac[294+idx]);
        rr_f[1617+idx] = _mm_mul_pd(rr_f[1617+idx],mole_frac[882+idx]);
        rr_r[1617+idx] = reverse;
        rr_r[1617+idx] = _mm_mul_pd(rr_r[1617+idx],mole_frac[147+idx]);
        rr_r[1617+idx] = _mm_mul_pd(rr_r[1617+idx],mole_frac[441+idx]);
      }
      //  12)  OH + HO2 <=> O2 + H2O
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(497.0), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(3.190372371082602e+14),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[147+idx];
        xik = _mm_sub_pd(xik,cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[588+idx]);
        xik = _mm_sub_pd(xik,cgspl[882+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[1764+idx] = forward;
        rr_f[1764+idx] = _mm_mul_pd(rr_f[1764+idx],mole_frac[441+idx]);
        rr_f[1764+idx] = _mm_mul_pd(rr_f[1764+idx],mole_frac[882+idx]);
        rr_r[1764+idx] = reverse;
        rr_r[1764+idx] = _mm_mul_pd(rr_r[1764+idx],mole_frac[147+idx]);
        rr_r[1764+idx] = _mm_mul_pd(rr_r[1764+idx],mole_frac[588+idx]);
      }
      //  13, 14)  2 HO2 <=> O2 + H2O2
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-1.1982e+04), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(4.636527321296516e+15),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(1.6293e+03), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(1.435115599448922e+12),arrtemp);
            next = arrtemp;
          }
          forward = _mm_add_pd(forward,next);
        }
        __m128d xik = cgspl[147+idx];
        xik = 
          _mm_add_pd(xik,_mm_mul_pd(_mm_set1_pd(double(-2)),cgspl[882+idx])); 
        xik = _mm_add_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[1911+idx] = forward;
        rr_f[1911+idx] = _mm_mul_pd(rr_f[1911+idx],mole_frac[882+idx]);
        rr_f[1911+idx] = _mm_mul_pd(rr_f[1911+idx],mole_frac[882+idx]);
        rr_r[1911+idx] = reverse;
        rr_r[1911+idx] = _mm_mul_pd(rr_r[1911+idx],mole_frac[147+idx]);
        rr_r[1911+idx] = _mm_mul_pd(rr_r[1911+idx],mole_frac[1029+idx]);
      }
      //  14)  H2O2 (+M) <=> 2 OH (+M)
      {
        __m128d forward, reverse;
        __m128d rr_k0, rr_kinf;
        {
          __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-4.55e+04), ortc[idx]);
          arrtemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
          arrtemp = _mm_mul_pd(_mm_set1_pd(1.326929961952003e+18),arrtemp);
          rr_k0 = arrtemp;
        }
        {
          __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-4.843e+04), ortc[idx]);
          arrtemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
          arrtemp = _mm_mul_pd(_mm_set1_pd(3.257712410749052e+15),arrtemp);
          rr_kinf = arrtemp;
        }
        __m128d pr = _mm_mul_pd(_mm_div_pd(rr_k0,rr_kinf),thbctemp[0+idx]);
        __m128d fcent;
        {
          fcent = _mm_mul_pd(temperature[idx], _mm_set1_pd(-1.2e+32));
          fcent = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(fcent,fcent,1))),exp(_mm_cvtsd_f64(fcent))); 
          fcent = _mm_mul_pd(fcent, _mm_set1_pd(0.5));
          {
            __m128d fctemp = _mm_mul_pd(temperature[idx], 
              _mm_set1_pd(-1.2e-28)); 
            fctemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(fctemp,fctemp,1))),exp(_mm_cvtsd_f64(fctemp))); 
            fcent = _mm_add_pd(fcent, _mm_mul_pd(fctemp, _mm_set1_pd(0.5)));
          }
          fcent = 
            _mm_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(fcent,fcent,1))), 
            log10(_mm_cvtsd_f64(fcent))); 
          __m128d flogpr = 
            _mm_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(pr,pr,1))),log10(_mm_cvtsd_f64(pr))); 
          flogpr = _mm_sub_pd(flogpr, 
            _mm_add_pd(_mm_set1_pd(0.4),_mm_mul_pd(_mm_set1_pd(0.67),fcent))); 
          __m128d fdenom = 
            _mm_add_pd(_mm_set1_pd(0.75),_mm_mul_pd(_mm_set1_pd(-1.27),fcent)); 
          fdenom = _mm_add_pd(fdenom,_mm_mul_pd(_mm_set1_pd(-0.14),flogpr));
          __m128d fquan = _mm_div_pd(flogpr,fdenom);
          fquan = 
            _mm_div_pd(fcent,_mm_add_pd(_mm_set1_pd(1.0),_mm_mul_pd(fquan,fquan))); 
          __m128d ftemp = _mm_mul_pd(fquan,_mm_set1_pd(DLn10));
          ftemp = 
            _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(ftemp,ftemp,1))),exp(_mm_cvtsd_f64(ftemp))); 
          forward = _mm_mul_pd(rr_kinf,pr);
          forward = _mm_div_pd(forward,_mm_add_pd(_mm_set1_pd(1.0),pr));
          forward = _mm_mul_pd(forward, ftemp);
        }
        __m128d xik = _mm_mul_pd(_mm_set1_pd(double(2)),cgspl[441+idx]);
        xik = _mm_sub_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        reverse = _mm_mul_pd(reverse,oprt[idx]);
        rr_f[2058+idx] = forward;
        rr_f[2058+idx] = _mm_mul_pd(rr_f[2058+idx],mole_frac[1029+idx]);
        rr_r[2058+idx] = reverse;
        rr_r[2058+idx] = _mm_mul_pd(rr_r[2058+idx],mole_frac[441+idx]);
        rr_r[2058+idx] = _mm_mul_pd(rr_r[2058+idx],mole_frac[441+idx]);
      }
      //  15)  H + H2O2 <=> OH + H2O
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-3.97e+03), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(2.660483534363001e+14),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[441+idx];
        xik = _mm_add_pd(xik,cgspl[588+idx]);
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        xik = _mm_sub_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[2205+idx] = forward;
        rr_f[2205+idx] = _mm_mul_pd(rr_f[2205+idx],mole_frac[735+idx]);
        rr_f[2205+idx] = _mm_mul_pd(rr_f[2205+idx],mole_frac[1029+idx]);
        rr_r[2205+idx] = reverse;
        rr_r[2205+idx] = _mm_mul_pd(rr_r[2205+idx],mole_frac[441+idx]);
        rr_r[2205+idx] = _mm_mul_pd(rr_r[2205+idx],mole_frac[588+idx]);
      }
      //  16)  H + H2O2 <=> H2 + HO2
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-7.95e+03), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(5.320967068726001e+14),arrtemp);
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = cgspl[0+idx];
        xik = _mm_sub_pd(xik,cgspl[735+idx]);
        xik = _mm_add_pd(xik,cgspl[882+idx]);
        xik = _mm_sub_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[2352+idx] = forward;
        rr_f[2352+idx] = _mm_mul_pd(rr_f[2352+idx],mole_frac[735+idx]);
        rr_f[2352+idx] = _mm_mul_pd(rr_f[2352+idx],mole_frac[1029+idx]);
        rr_r[2352+idx] = reverse;
        rr_r[2352+idx] = _mm_mul_pd(rr_r[2352+idx],mole_frac[0+idx]);
        rr_r[2352+idx] = _mm_mul_pd(rr_r[2352+idx],mole_frac[882+idx]);
      }
      //  17)  O + H2O2 <=> OH + HO2
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-3.97e+03),ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = 
              _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(_mm_set1_pd(1.518131517201658e+12),temperature[idx]),temperature[idx]),arrtemp); 
            next = arrtemp;
          }
          forward = next;
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[294+idx]);
        xik = _mm_add_pd(xik,cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[882+idx]);
        xik = _mm_sub_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[2499+idx] = forward;
        rr_f[2499+idx] = _mm_mul_pd(rr_f[2499+idx],mole_frac[294+idx]);
        rr_f[2499+idx] = _mm_mul_pd(rr_f[2499+idx],mole_frac[1029+idx]);
        rr_r[2499+idx] = reverse;
        rr_r[2499+idx] = _mm_mul_pd(rr_r[2499+idx],mole_frac[441+idx]);
        rr_r[2499+idx] = _mm_mul_pd(rr_r[2499+idx],mole_frac[882+idx]);
      }
      //  18, 20)  OH + H2O2 <=> H2O + HO2
      {
        __m128d forward, reverse;
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_set1_pd(1.103935076499171e+13);
            next = arrtemp;
          }
          forward = next;
        }
        {
          __m128d next;
          {
            __m128d arrtemp = _mm_mul_pd(_mm_set1_pd(-9.557e+03), ortc[idx]);
            arrtemp = 
              _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(arrtemp,arrtemp,1))),exp(_mm_cvtsd_f64(arrtemp))); 
            arrtemp = _mm_mul_pd(_mm_set1_pd(6.402823443695189e+15),arrtemp);
            next = arrtemp;
          }
          forward = _mm_add_pd(forward,next);
        }
        __m128d xik = _mm_xor_pd(_mm_set1_pd(-0.0),cgspl[441+idx]);
        xik = _mm_add_pd(xik,cgspl[588+idx]);
        xik = _mm_add_pd(xik,cgspl[882+idx]);
        xik = _mm_sub_pd(xik,cgspl[1029+idx]);
        reverse = _mm_mul_pd(xik,otc[idx]);
        reverse = 
          _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(reverse,reverse,1))),exp(_mm_cvtsd_f64(reverse))); 
        reverse = _mm_mul_pd(forward,reverse);
        rr_f[2646+idx] = forward;
        rr_f[2646+idx] = _mm_mul_pd(rr_f[2646+idx],mole_frac[441+idx]);
        rr_f[2646+idx] = _mm_mul_pd(rr_f[2646+idx],mole_frac[1029+idx]);
        rr_r[2646+idx] = reverse;
        rr_r[2646+idx] = _mm_mul_pd(rr_r[2646+idx],mole_frac[588+idx]);
        rr_r[2646+idx] = _mm_mul_pd(rr_r[2646+idx],mole_frac[882+idx]);
      }
    }
    // Output rates
    for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)
    {
      __m128d ropl[19];
      ropl[0] = _mm_sub_pd(rr_f[0+idx],rr_r[0+idx]);
      ropl[1] = _mm_sub_pd(rr_f[147+idx],rr_r[147+idx]);
      ropl[2] = _mm_sub_pd(rr_f[294+idx],rr_r[294+idx]);
      ropl[3] = _mm_sub_pd(rr_f[441+idx],rr_r[441+idx]);
      ropl[4] = _mm_sub_pd(rr_f[588+idx],rr_r[588+idx]);
      ropl[5] = _mm_sub_pd(rr_f[735+idx],rr_r[735+idx]);
      ropl[6] = _mm_sub_pd(rr_f[882+idx],rr_r[882+idx]);
      ropl[7] = _mm_sub_pd(rr_f[1029+idx],rr_r[1029+idx]);
      ropl[8] = _mm_sub_pd(rr_f[1176+idx],rr_r[1176+idx]);
      ropl[9] = _mm_sub_pd(rr_f[1323+idx],rr_r[1323+idx]);
      ropl[10] = _mm_sub_pd(rr_f[1470+idx],rr_r[1470+idx]);
      ropl[11] = _mm_sub_pd(rr_f[1617+idx],rr_r[1617+idx]);
      ropl[12] = _mm_sub_pd(rr_f[1764+idx],rr_r[1764+idx]);
      ropl[13] = _mm_sub_pd(rr_f[1911+idx],rr_r[1911+idx]);
      ropl[14] = _mm_sub_pd(rr_f[2058+idx],rr_r[2058+idx]);
      ropl[15] = _mm_sub_pd(rr_f[2205+idx],rr_r[2205+idx]);
      ropl[16] = _mm_sub_pd(rr_f[2352+idx],rr_r[2352+idx]);
      ropl[17] = _mm_sub_pd(rr_f[2499+idx],rr_r[2499+idx]);
      ropl[18] = _mm_sub_pd(rr_f[2646+idx],rr_r[2646+idx]);
      // 0. H2
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_sub_pd(result,ropl[1]);
        result = _mm_sub_pd(result,ropl[2]);
        result = _mm_sub_pd(result,ropl[4]);
        result = _mm_add_pd(result,ropl[9]);
        result = _mm_add_pd(result,ropl[16]);
        result = _mm_mul_pd(result,_mm_set1_pd(2.01594e-03));
        _mm_store_pd(wdot_array+(0*spec_stride)+(idx<<1),result);
      }
      // 1. O2
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_sub_pd(result,ropl[0]);
        result = _mm_add_pd(result,ropl[5]);
        result = _mm_sub_pd(result,ropl[8]);
        result = _mm_add_pd(result,ropl[9]);
        result = _mm_add_pd(result,ropl[11]);
        result = _mm_add_pd(result,ropl[12]);
        result = _mm_add_pd(result,ropl[13]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.0319988));
        _mm_store_pd(wdot_array+(1*spec_stride)+(idx<<1),result);
      }
      // 2. O
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_add_pd(result,ropl[0]);
        result = _mm_sub_pd(result,ropl[1]);
        result = _mm_sub_pd(result,ropl[3]);
        result = _mm_sub_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[5]));
        result = _mm_sub_pd(result,ropl[6]);
        result = _mm_sub_pd(result,ropl[11]);
        result = _mm_sub_pd(result,ropl[17]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.0159994));
        _mm_store_pd(wdot_array+(2*spec_stride)+(idx<<1),result);
      }
      // 3. OH
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_add_pd(result,ropl[0]);
        result = _mm_add_pd(result,ropl[1]);
        result = _mm_sub_pd(result,ropl[2]);
        result = _mm_add_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[3]));
        result = _mm_add_pd(result,ropl[6]);
        result = _mm_sub_pd(result,ropl[7]);
        result = _mm_add_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[10]));
        result = _mm_add_pd(result,ropl[11]);
        result = _mm_sub_pd(result,ropl[12]);
        result = _mm_add_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[14]));
        result = _mm_add_pd(result,ropl[15]);
        result = _mm_add_pd(result,ropl[17]);
        result = _mm_sub_pd(result,ropl[18]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.01700737));
        _mm_store_pd(wdot_array+(3*spec_stride)+(idx<<1),result);
      }
      // 4. H2O
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_add_pd(result,ropl[2]);
        result = _mm_sub_pd(result,ropl[3]);
        result = _mm_add_pd(result,ropl[7]);
        result = _mm_add_pd(result,ropl[12]);
        result = _mm_add_pd(result,ropl[15]);
        result = _mm_add_pd(result,ropl[18]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.01801534));
        _mm_store_pd(wdot_array+(4*spec_stride)+(idx<<1),result);
      }
      // 5. H
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_sub_pd(result,ropl[0]);
        result = _mm_add_pd(result,ropl[1]);
        result = _mm_add_pd(result,ropl[2]);
        result = _mm_add_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[4]));
        result = _mm_sub_pd(result,ropl[6]);
        result = _mm_sub_pd(result,ropl[7]);
        result = _mm_sub_pd(result,ropl[8]);
        result = _mm_sub_pd(result,ropl[9]);
        result = _mm_sub_pd(result,ropl[10]);
        result = _mm_sub_pd(result,ropl[15]);
        result = _mm_sub_pd(result,ropl[16]);
        result = _mm_mul_pd(result,_mm_set1_pd(1.00797e-03));
        _mm_store_pd(wdot_array+(5*spec_stride)+(idx<<1),result);
      }
      // 6. HO2
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_add_pd(result,ropl[8]);
        result = _mm_sub_pd(result,ropl[9]);
        result = _mm_sub_pd(result,ropl[10]);
        result = _mm_sub_pd(result,ropl[11]);
        result = _mm_sub_pd(result,ropl[12]);
        result = _mm_sub_pd(result,_mm_mul_pd(_mm_set1_pd(2.0),ropl[13]));
        result = _mm_add_pd(result,ropl[16]);
        result = _mm_add_pd(result,ropl[17]);
        result = _mm_add_pd(result,ropl[18]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.03300677));
        _mm_store_pd(wdot_array+(6*spec_stride)+(idx<<1),result);
      }
      // 7. H2O2
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_add_pd(result,ropl[13]);
        result = _mm_sub_pd(result,ropl[14]);
        result = _mm_sub_pd(result,ropl[15]);
        result = _mm_sub_pd(result,ropl[16]);
        result = _mm_sub_pd(result,ropl[17]);
        result = _mm_sub_pd(result,ropl[18]);
        result = _mm_mul_pd(result,_mm_set1_pd(0.03401473999999999));
        _mm_store_pd(wdot_array+(7*spec_stride)+(idx<<1),result);
      }
      // 8. N2
      {
        __m128d result = _mm_set1_pd(0.0);
        result = _mm_mul_pd(result,_mm_set1_pd(0.0280134));
        _mm_store_pd(wdot_array+(8*spec_stride)+(idx<<1),result);
      }
    }
  }
  free(cgspl);
  free(temperature);
  free(mole_frac);
  free(thbctemp);
  free(rr_f);
  free(rr_r);
  free(otc);
  free(ortc);
  free(vlntemp);
  free(prt);
  free(oprt);
}

