
__constant__ double molecular_masses[9] = {2.01594, 31.9988, 15.9994, 17.00737, 
  18.01534, 1.00797, 33.00677, 34.01474, 28.0134}; 


__constant__ double recip_molecular_masses[9] = {0.4960465093207139, 
  0.03125117191894696, 0.06250234383789392, 0.05879803873262004, 
  0.05550825019122593, 0.9920930186414277, 0.03029681486555637, 
  0.02939901936631002, 0.03569720205330306}; 


__global__ void
__launch_bounds__(128,4)
gpu_getrates(const double *temperature_array, const double *pressure_array, 
  const double *avmolwt_array, const double *mass_frac_array, const int 
  slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX in number 
  of doubles*/, const int total_steps/*NZ in number of doubles*/, const int 
  spec_stride/*NX*NY*NZ in number of doubles*/, const int step_stride/*always 
  zero*/, double *wdot_array) 
{
  
  const double PA = 1.013250e+06;
  // Scaled R0 for non-dimensionalization
  const double R0 = 9.977411999999998e+09;
  // Scaled R0c for non-dimensionalization
  const double R0c = 238.4658699839999;
  const double DLn10 = 2.3025850929940459e0;
  
  {
    const int offset = (blockIdx.x*blockDim.x + threadIdx.x);
    temperature_array += offset;
    pressure_array += offset;
    avmolwt_array += offset;
    mass_frac_array += offset;
    wdot_array += offset;
  }
  double temperature;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  const double otc     = 1.0 / temperature;
  const double ortc    = 1.0 / (temperature * R0c);
  const double vlntemp = log(temperature);
  const double prt     = PA / (R0 * temperature);
  const double oprt    = 1.0 / prt;
  
  double mass_frac[9];
  double avmolwt;
  double pressure;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac[0]) : 
    "l"(mass_frac_array+0*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac[1]) : 
    "l"(mass_frac_array+1*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac[2]) : 
    "l"(mass_frac_array+2*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac[3]) : 
    "l"(mass_frac_array+3*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac[4]) : 
    "l"(mass_frac_array+4*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac[5]) : 
    "l"(mass_frac_array+5*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac[6]) : 
    "l"(mass_frac_array+6*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac[7]) : 
    "l"(mass_frac_array+7*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac[8]) : 
    "l"(mass_frac_array+8*spec_stride) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(avmolwt) : 
    "l"(avmolwt_array) : "memory"); 
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(pressure) : 
    "l"(pressure_array) : "memory"); 
  double cgspl[9];
  // Gibbs computation
  {
    const double &tk1 = temperature;
    double tklog = log(tk1);
    double tk2 = tk1 * tk1;
    double tk3 = tk1 * tk2;
    double tk4 = tk1 * tk3;
    double tk5 = tk1 * tk4;
    
    // Species H2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[0] = 2.991423*tk1*(-3.787491742782046-tklog) + 
          -0.04200386399999999*tk2 + 1.352118959999999e-04*tk3 + 
          1.329347231999999e-06*tk4 + -1.640997273599998e-08*tk5 + 
          (-6.958616666666668 - tk1*-1.35511); 
      }
      else
      {
        cgspl[0] = 3.298124*tk1*(-3.787491742782046-tklog) + 
          -0.04949665199999999*tk2 + 1.954323599999999e-03*tk3 + 
          1.364462495999999e-05*tk4 + -4.287035289599994e-06*tk5 + (-8.437675 - 
          tk1*-3.294094); 
      }
    }
    // Species O2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[1] = 3.697578*tk1*(-3.787491742782046-tklog) + 
          -0.03681118199999999*tk2 + 3.021220799999999e-04*tk3 + 
          -2.556404639999998e-06*tk4 + 1.178255807999999e-08*tk5 + (-10.28275 - 
          tk1*3.189166); 
      }
      else
      {
        cgspl[1] = 3.212936*tk1*(-3.787491742782046-tklog) + 
          -0.06764915999999999*tk2 + 1.381476e-03*tk3 + 
          -1.891982879999998e-04*tk4 + 9.091236787199989e-06*tk5 + 
          (-8.377075000000001 - tk1*6.034738); 
      }
    }
    // Species O
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[2] = 2.54206*tk1*(-3.787491742782046-tklog) + 1.6530372e-03*tk2 + 
          7.446727199999996e-06*tk3 + -6.553536479999994e-07*tk4 + 
          4.528796313599995e-09*tk5 + (243.59 - tk1*4.920308); 
      }
      else
      {
        cgspl[2] = 2.946429*tk1*(-3.787491742782046-tklog) + 
          0.09828995999999997*tk2 + -5.810476799999997e-03*tk3 + 
          2.308093919999998e-04*tk4 + -4.033873612799995e-06*tk5 + (242.897 - 
          tk1*2.963995); 
      }
    }
    // Species OH
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[3] = 2.86472886*tk1*(-3.787491742782046-tklog) + 
          -0.06339026879999998*tk2 + 6.217986191999998e-04*tk3 + 
          -4.395148905599995e-06*tk4 + 1.380974842367998e-08*tk5 + (30.69690625 
          - tk1*5.70164073); 
      }
      else
      {
        cgspl[3] = 4.12530561*tk1*(-3.787491742782046-tklog) + 0.1935269634*tk2 
          + -0.015666352584*tk3 + 8.349892459199992e-04*tk4 + 
          -2.138269145471997e-05*tk5 + (27.88590941666667 - tk1*-0.69043296); 
      }
    }
    // Species H2O
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[4] = 2.672146*tk1*(-3.787491742782046-tklog) + -0.18337758*tk2 + 
          2.095262399999999e-03*tk3 + -1.729434239999998e-05*tk4 + 
          6.626829542399991e-08*tk5 + (-249.1600833333334 - tk1*6.862817); 
      }
      else
      {
        cgspl[4] = 3.386842*tk1*(-3.787491742782046-tklog) + 
          -0.2084989199999999*tk2 + 0.01525127039999999*tk3 + 
          -1.003475663999999e-03*tk4 + 2.598830438399997e-05*tk5 + 
          (-251.7342500000001 - tk1*2.590233); 
      }
    }
    // Species H
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[5] = 2.5*tk1*(-3.787491742782046-tklog) + -0.0*tk2 + -0.0*tk3 + 
          -0.0*tk4 + -0.0*tk5 + (212.2635833333334 - tk1*-0.4601176); 
      }
      else
      {
        cgspl[5] = 2.5*tk1*(-3.787491742782046-tklog) + -0.0*tk2 + -0.0*tk3 + 
          -0.0*tk4 + -0.0*tk5 + (212.2635833333334 - tk1*-0.4601176); 
      }
    }
    // Species HO2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[6] = 4.0172109*tk1*(-3.787491742782046-tklog) + -0.1343892078*tk2 
          + 1.520779559999999e-03*tk3 + -1.645147727999999e-05*tk4 + 
          1.118795690879999e-07*tk5 + (0.9321392750000002 - tk1*3.78510215); 
      }
      else
      {
        cgspl[6] = 4.30179801*tk1*(-3.787491742782046-tklog) + 
          0.2849472305999999*tk2 + -0.05077989383999998*tk3 + 
          3.495800073599997e-03*tk4 + -9.634206085631988e-05*tk5 + 
          (2.456733666666667 - tk1*3.71666245); 
      }
    }
    // Species H2O2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[7] = 4.573167*tk1*(-3.787491742782046-tklog) + 
          -0.2601681599999999*tk2 + 3.539253599999998e-03*tk3 + 
          -3.382421759999996e-05*tk4 + 1.484338867199998e-07*tk5 + (-150.058 - 
          tk1*0.5011370000000001); 
      }
      else
      {
        cgspl[7] = 3.388754*tk1*(-3.787491742782046-tklog) + 
          -0.3941535599999999*tk2 + 3.564031199999998e-04*tk3 + 
          6.661160639999993e-04*tk4 + -2.562466751999997e-05*tk5 + 
          (-147.1929166666667 - tk1*6.785363); 
      }
    }
    // Species N2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[8] = 2.92664*tk1*(-3.787491742782046-tklog) + 
          -0.08927861999999998*tk2 + 1.364342639999999e-03*tk3 + 
          -1.453973759999998e-05*tk4 + 7.001874316799991e-08*tk5 + 
          (-7.689980833333334 - tk1*5.980528); 
      }
      else
      {
        cgspl[8] = 3.298677*tk1*(-3.787491742782046-tklog) + 
          -0.08449439999999998*tk2 + 9.511732799999995e-03*tk3 + 
          -8.123781599999992e-04*tk4 + 2.534825663999997e-05*tk5 + 
          (-8.507500000000002 - tk1*3.950372); 
      }
    }
  }
  
  double mole_frac[9];
  // Compute mole fractions
  {
    double sumyow = temperature * avmolwt * 7.03444160806564;
    sumyow = pressure/sumyow;
    mole_frac[0] = mass_frac[0] * recip_molecular_masses[0];
    mole_frac[0] = (mole_frac[0] > 1e-200) ? mole_frac[0] : 1e-200;
    mole_frac[0] *= sumyow;
    mole_frac[1] = mass_frac[1] * recip_molecular_masses[1];
    mole_frac[1] = (mole_frac[1] > 1e-200) ? mole_frac[1] : 1e-200;
    mole_frac[1] *= sumyow;
    mole_frac[2] = mass_frac[2] * recip_molecular_masses[2];
    mole_frac[2] = (mole_frac[2] > 1e-200) ? mole_frac[2] : 1e-200;
    mole_frac[2] *= sumyow;
    mole_frac[3] = mass_frac[3] * recip_molecular_masses[3];
    mole_frac[3] = (mole_frac[3] > 1e-200) ? mole_frac[3] : 1e-200;
    mole_frac[3] *= sumyow;
    mole_frac[4] = mass_frac[4] * recip_molecular_masses[4];
    mole_frac[4] = (mole_frac[4] > 1e-200) ? mole_frac[4] : 1e-200;
    mole_frac[4] *= sumyow;
    mole_frac[5] = mass_frac[5] * recip_molecular_masses[5];
    mole_frac[5] = (mole_frac[5] > 1e-200) ? mole_frac[5] : 1e-200;
    mole_frac[5] *= sumyow;
    mole_frac[6] = mass_frac[6] * recip_molecular_masses[6];
    mole_frac[6] = (mole_frac[6] > 1e-200) ? mole_frac[6] : 1e-200;
    mole_frac[6] *= sumyow;
    mole_frac[7] = mass_frac[7] * recip_molecular_masses[7];
    mole_frac[7] = (mole_frac[7] > 1e-200) ? mole_frac[7] : 1e-200;
    mole_frac[7] *= sumyow;
    mole_frac[8] = mass_frac[8] * recip_molecular_masses[8];
    mole_frac[8] = (mole_frac[8] > 1e-200) ? mole_frac[8] : 1e-200;
    mole_frac[8] *= sumyow;
  }
  
  double thbctemp[2];
  // Computing third body values
  {
    double ctot = 0.0;
    ctot += mole_frac[0];
    ctot += mole_frac[1];
    ctot += mole_frac[2];
    ctot += mole_frac[3];
    ctot += mole_frac[4];
    ctot += mole_frac[5];
    ctot += mole_frac[6];
    ctot += mole_frac[7];
    ctot += mole_frac[8];
    thbctemp[0] = ctot + 1.5*mole_frac[0] + 11.0*mole_frac[4];
    thbctemp[1] = ctot + mole_frac[0] - 0.22*mole_frac[1] + 10.0*mole_frac[4];
  }
  
  double rr_f[19];
  double rr_r[19];
  //   0)  O2 + H <=> O + OH
  {
    double forward = 5.606052175734568e+15 * exp(-0.406*vlntemp - 
      1.6599e+04*ortc); 
    double xik = -cgspl[1] + cgspl[2] + cgspl[3] - cgspl[5];
    double reverse = forward * exp(xik*otc);
    rr_f[0] = forward * mole_frac[1] * mole_frac[5];
    rr_r[0] = reverse * mole_frac[2] * mole_frac[3];
  }
  //   1)  H2 + O <=> OH + H
  {
    double forward = 1.996278696128123e+11 * exp(2.67*vlntemp - 6.29e+03*ortc);
    double xik = -cgspl[0] - cgspl[2] + cgspl[3] + cgspl[5];
    double reverse = forward * exp(xik*otc);
    rr_f[1] = forward * mole_frac[0] * mole_frac[2];
    rr_r[1] = reverse * mole_frac[3] * mole_frac[5];
  }
  //   2)  H2 + OH <=> H2O + H
  {
    double forward = 3.288220749682686e+12 * exp(1.51*vlntemp - 3.43e+03*ortc);
    double xik = -cgspl[0] - cgspl[3] + cgspl[4] + cgspl[5];
    double reverse = forward * exp(xik*otc);
    rr_f[2] = forward * mole_frac[0] * mole_frac[3];
    rr_r[2] = reverse * mole_frac[4] * mole_frac[5];
  }
  //   3)  O + H2O <=> 2 OH
  {
    double forward = 5.195724313044046e+11 * exp(2.02*vlntemp - 1.34e+04*ortc);
    double xik = -cgspl[2] + 2.0 * cgspl[3] - cgspl[4];
    double reverse = forward * exp(xik*otc);
    rr_f[3] = forward * mole_frac[2] * mole_frac[4];
    rr_r[3] = reverse * mole_frac[3] * mole_frac[3];
  }
  //   4)  H2 + M <=> 2 H + M
  {
    double forward = 6.203984453113697e+17 * exp(-1.4*vlntemp - 
      1.0438e+05*ortc); 
    double xik = -cgspl[0] + 2.0 * cgspl[5];
    double reverse = forward * exp(xik*otc) * oprt;
    rr_f[4] = forward * mole_frac[0];
    rr_r[4] = reverse * mole_frac[5] * mole_frac[5];
    rr_f[4] *= thbctemp[0];
    rr_r[4] *= thbctemp[0];
  }
  //   5)  2 O + M <=> O2 + M
  {
    double forward = 6.21278022363831e+15 * exp(-0.5 * vlntemp);
    double xik = cgspl[1] - 2.0 * cgspl[2];
    double reverse = forward * exp(xik*otc) * prt;
    rr_f[5] = forward * mole_frac[2] * mole_frac[2];
    rr_r[5] = reverse * mole_frac[1];
    rr_f[5] *= thbctemp[0];
    rr_r[5] *= thbctemp[0];
  }
  //   6)  O + H + M <=> OH + M
  {
    double forward = 4.336624958847576e+17 * otc;
    double xik = -cgspl[2] + cgspl[3] - cgspl[5];
    double reverse = forward * exp(xik*otc) * prt;
    rr_f[6] = forward * mole_frac[2] * mole_frac[5];
    rr_r[6] = reverse * mole_frac[3];
    rr_f[6] *= thbctemp[0];
    rr_r[6] *= thbctemp[0];
  }
  //   7)  OH + H + M <=> H2O + M
  {
    double forward = 2.913162007428368e+19 * otc * otc;
    double xik = -cgspl[3] + cgspl[4] - cgspl[5];
    double reverse = forward * exp(xik*otc) * prt;
    rr_f[7] = forward * mole_frac[3] * mole_frac[5];
    rr_r[7] = reverse * mole_frac[4];
    rr_f[7] *= thbctemp[0];
    rr_r[7] *= thbctemp[0];
  }
  //   8)  O2 + H (+M) <=> HO2 (+M)
  {
    double rr_k0 = 1.864740588650248e+18 * exp(-1.72*vlntemp - 524.8*ortc);
    double rr_kinf = 2.879017473981534e+14 * exp(0.6 * vlntemp);
    double pr = rr_k0 / rr_kinf * thbctemp[1];
    double fcent = log10(0.2 * exp(-1.2e+32 * temperature) + 0.8 * exp(-1.2e-28 
      * temperature)); 
    double flogpr = log10(pr) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = -cgspl[1] - cgspl[5] + cgspl[6];
    double reverse = forward * exp(xik*otc) * prt;
    rr_f[8] = forward * mole_frac[1] * mole_frac[5];
    rr_r[8] = reverse * mole_frac[6];
  }
  //   9)  H + HO2 <=> H2 + O2
  {
    double forward = 1.832532226988623e+14 * exp(-823.0*ortc);
    double xik = cgspl[0] + cgspl[1] - cgspl[5] - cgspl[6];
    double reverse = forward * exp(xik*otc);
    rr_f[9] = forward * mole_frac[5] * mole_frac[6];
    rr_r[9] = reverse * mole_frac[0] * mole_frac[1];
  }
  //  10)  H + HO2 <=> 2 OH
  {
    double forward = 7.814756406537628e+14 * exp(-295.0*ortc);
    double xik = 2.0 * cgspl[3] - cgspl[5] - cgspl[6];
    double reverse = forward * exp(xik*otc);
    rr_f[10] = forward * mole_frac[5] * mole_frac[6];
    rr_r[10] = reverse * mole_frac[3] * mole_frac[3];
  }
  //  11)  O + HO2 <=> O2 + OH
  {
    double forward = 3.587788998622304e+14;
    double xik = cgspl[1] - cgspl[2] + cgspl[3] - cgspl[6];
    double reverse = forward * exp(xik*otc);
    rr_f[11] = forward * mole_frac[2] * mole_frac[6];
    rr_r[11] = reverse * mole_frac[1] * mole_frac[3];
  }
  //  12)  OH + HO2 <=> O2 + H2O
  {
    double forward = 3.190372371082602e+14 * exp(497.0*ortc);
    double xik = cgspl[1] - cgspl[3] + cgspl[4] - cgspl[6];
    double reverse = forward * exp(xik*otc);
    rr_f[12] = forward * mole_frac[3] * mole_frac[6];
    rr_r[12] = reverse * mole_frac[1] * mole_frac[4];
  }
  //  13, 14)  2 HO2 <=> O2 + H2O2
  {
    double forward = 4.636527321296516e+15 * exp(-1.1982e+04*ortc);
    forward = forward + 1.435115599448922e+12 * exp(1.6293e+03*ortc);
    double xik = cgspl[1] - 2.0 * cgspl[6] + cgspl[7];
    double reverse = forward * exp(xik*otc);
    rr_f[13] = forward * mole_frac[6] * mole_frac[6];
    rr_r[13] = reverse * mole_frac[1] * mole_frac[7];
  }
  //  14)  H2O2 (+M) <=> 2 OH (+M)
  {
    double rr_k0 = 1.326929961952003e+18 * exp(-4.55e+04*ortc);
    double rr_kinf = 3.257712410749052e+15 * exp(-4.843e+04*ortc);
    double pr = rr_k0 / rr_kinf * thbctemp[0];
    double fcent = log10(0.5 * exp(-1.2e+32 * temperature) + 0.5 * exp(-1.2e-28 
      * temperature)); 
    double flogpr = log10(pr) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = 2.0 * cgspl[3] - cgspl[7];
    double reverse = forward * exp(xik*otc) * oprt;
    rr_f[14] = forward * mole_frac[7];
    rr_r[14] = reverse * mole_frac[3] * mole_frac[3];
  }
  //  15)  H + H2O2 <=> OH + H2O
  {
    double forward = 2.660483534363001e+14 * exp(-3.97e+03*ortc);
    double xik = cgspl[3] + cgspl[4] - cgspl[5] - cgspl[7];
    double reverse = forward * exp(xik*otc);
    rr_f[15] = forward * mole_frac[5] * mole_frac[7];
    rr_r[15] = reverse * mole_frac[3] * mole_frac[4];
  }
  //  16)  H + H2O2 <=> H2 + HO2
  {
    double forward = 5.320967068726001e+14 * exp(-7.95e+03*ortc);
    double xik = cgspl[0] - cgspl[5] + cgspl[6] - cgspl[7];
    double reverse = forward * exp(xik*otc);
    rr_f[16] = forward * mole_frac[5] * mole_frac[7];
    rr_r[16] = reverse * mole_frac[0] * mole_frac[6];
  }
  //  17)  O + H2O2 <=> OH + HO2
  {
    double forward = 1.518131517201658e+12 * temperature * temperature * 
      exp(-3.97e+03*ortc); 
    double xik = -cgspl[2] + cgspl[3] + cgspl[6] - cgspl[7];
    double reverse = forward * exp(xik*otc);
    rr_f[17] = forward * mole_frac[2] * mole_frac[7];
    rr_r[17] = reverse * mole_frac[3] * mole_frac[6];
  }
  //  18, 20)  OH + H2O2 <=> H2O + HO2
  {
    double forward = 1.103935076499171e+13;
    forward = forward + 6.402823443695189e+15 * exp(-9.557e+03*ortc);
    double xik = -cgspl[3] + cgspl[4] + cgspl[6] - cgspl[7];
    double reverse = forward * exp(xik*otc);
    rr_f[18] = forward * mole_frac[3] * mole_frac[7];
    rr_r[18] = reverse * mole_frac[4] * mole_frac[6];
  }
  double wdot[9];
  double ropl[19];
  for (int i = 0; i < 19; i++)
  {
    ropl[i] = rr_f[i] - rr_r[i];
  }
  // 0. H2
  wdot[0] = 2.01594e-03 * (-ropl[1] - ropl[2] - ropl[4] + ropl[9] + ropl[16]);
  // 1. O2
  wdot[1] = 0.0319988 * (-ropl[0] + ropl[5] - ropl[8] + ropl[9] + ropl[11] + 
    ropl[12] + ropl[13]); 
  // 2. O
  wdot[2] = 0.0159994 * (ropl[0] - ropl[1] - ropl[3] - 2.0*ropl[5] - ropl[6] - 
    ropl[11] - ropl[17]); 
  // 3. OH
  wdot[3] = 0.01700737 * (ropl[0] + ropl[1] - ropl[2] + 2.0*ropl[3] + ropl[6] - 
    ropl[7] + 2.0*ropl[10] + ropl[11] - ropl[12] + 2.0*ropl[14] + ropl[15] + 
    ropl[17] - ropl[18]); 
  // 4. H2O
  wdot[4] = 0.01801534 * (ropl[2] - ropl[3] + ropl[7] + ropl[12] + ropl[15] + 
    ropl[18]); 
  // 5. H
  wdot[5] = 1.00797e-03 * (-ropl[0] + ropl[1] + ropl[2] + 2.0*ropl[4] - ropl[6] 
    - ropl[7] - ropl[8] - ropl[9] - ropl[10] - ropl[15] - ropl[16]); 
  // 6. HO2
  wdot[6] = 0.03300677 * (ropl[8] - ropl[9] - ropl[10] - ropl[11] - ropl[12] - 
    2.0*ropl[13] + ropl[16] + ropl[17] + ropl[18]); 
  // 7. H2O2
  wdot[7] = 0.03401473999999999 * (ropl[13] - ropl[14] - ropl[15] - ropl[16] - 
    ropl[17] - ropl[18]); 
  // 8. N2
  wdot[8] = 0.0;
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+0*spec_stride) , 
    "d"(wdot[0]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+1*spec_stride) , 
    "d"(wdot[1]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+2*spec_stride) , 
    "d"(wdot[2]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+3*spec_stride) , 
    "d"(wdot[3]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+4*spec_stride) , 
    "d"(wdot[4]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+5*spec_stride) , 
    "d"(wdot[5]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+6*spec_stride) , 
    "d"(wdot[6]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+7*spec_stride) , 
    "d"(wdot[7]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+8*spec_stride) , 
    "d"(wdot[8]) : "memory"); 
}

