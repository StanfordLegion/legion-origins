
#include "rhsf_gpu.h"

#include "cuda.h"
#include "cuda_runtime.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>

#define DEBUG_CUDA

#ifdef DEBUG_CUDA
#define CUDA_SAFE_CALL(expr)                        \
  {                                                 \
    cudaError_t err = (expr);                       \
    if (err != cudaSuccess)                         \
    {                                               \
      printf("Cuda error: %s\n", cudaGetErrorString(err));  \
      assert(false);                                \
    }                                               \
  }
#else
#define CUDA_SAFE_CALL(expr)  expr
#endif

extern __global__ void
gpu_conductivity(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *conductivity);

__host__
void run_gpu_conductivity(const double *temp_array, const double *mole_frac_array,
                          const double *mole_avg_array, const int nx, const int ny, 
                          const int nz, double *conductivity)
{
  assert(((nx*ny*nz) % 128) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  gpu_conductivity<<<grid,block>>>(temp_array, mole_frac_array,
                                                 mole_avg_array,
                                                 nx*ny, nx, nz, nx*ny*nz,
                                                 conductivity);
}

extern __global__ void
gpu_viscosity(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *viscosity); 

__host__
void run_gpu_viscosity(const double *temp_array, const double *mole_frac_array,
                       const double *mole_avg_array, const int nx, const int ny, 
                       const int nz, double *viscosity)
{
  assert(((nx*ny*nz) % 128) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  gpu_viscosity<<<grid,block>>>(temp_array, mole_frac_array,
                                              mole_avg_array,
                                              nx*ny, nx, nz, nx*ny*nz,
                                              viscosity);
}

extern __global__ void
gpu_diffusion(const double *temperature_array, const double *pressure_array, 
  const double *mass_frac_array, const double *avmolwt_array, const int 
  slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX in number 
  of doubles*/, const int total_steps/*NZ in number of doubles*/, const int 
  spec_stride/*NX*NY*NZ in number of doubles*/, double *diffusion);

__host__
void run_gpu_diffusion(const double *temp_array, const double *pres_array,
                       const double *mole_frac_array, const double *mole_avg_array,
                       const int nx, const int ny, const int nz, double *diffusion)
{
  assert(((nx*ny*nz) % 128) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  gpu_diffusion<<<grid,block>>>(temp_array, pres_array,
                                              mole_frac_array, mole_avg_array,
                                              nx*ny, nx, nz, nx*ny*nz,
                                              diffusion);
}

extern __global__ void
gpu_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *thermal_out);

__host__
void run_gpu_thermal(const double *temp_array, const double *mole_frac_array,
                     const double *mole_avg_array, const int nx, const int ny, 
                     const int nz, double *thermal)
{
  assert(((nx*ny*nz) % 128) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  gpu_thermal<<<grid,block>>>(temp_array, mole_frac_array,
                                            mole_avg_array,
                                            nx*ny, nx, nz, nx*ny*nz,
                                            thermal);
}

extern __global__ void
gpu_getrates(const double *temperature_array, const double *pressure_array, 
  const double *avmolwt_array, const double *mass_frac_array,
  const int slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX 
  in number of doubles*/, const int total_steps/*NZ in number of doubles*/, 
  const int spec_stride/*NX*NY*NZ in number of doubles*/, 
  const int step_stride/*always zero*/, double *wdot);

__host__
void run_gpu_getrates(const double *temp_array, const double *pres_array,
                  const double *mass_frac_array, const double *avmolwt_array,
                  const int nx, const int ny, const int nz, double *wdot)
{
  assert(((nx*ny*nz) % 32) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  gpu_getrates<<<grid,block>>>(temp_array, pres_array,
                                             avmolwt_array, mass_frac_array, 
                                             nx*ny, nx, nz, nx*ny*nz, 0,
                                             wdot);
}

#define MACH_NO           (0.001)
#define RE_REAL           (100.0)
#define PARAM_PR          (0.708)
#define G_REF             (1.4)
#define A_REF             (347.2) // m/s
#define T_0               (300.0) // K
#define RHO_REF           (1.1766)  // Kg/m3
#define LAMBDA_REF        (26.14e-3)
#define PARAM_RE          (RE_REAL / MACH_NO)
#define T_REF             (T_0 * (G_REF - 1))
#define CP_REF            ((A_REF * A_REF) / T_REF)
#define L_REF             (PARAM_PR * LAMBDA_REF / CP_REF * PARAM_RE / RHO_REF / A_REF)
#define VIS_REF           (RHO_REF * A_REF * L_REF)
#define DIF_REF           (A_REF * L_REF)
#define P_REF             ((A_REF * A_REF) * RHO_REF * 10.0)

__constant__
double one_over_molecular_masses[9] = {0.4960465093207139, 
  0.03125117191894696, 0.06250234383789392, 0.05879803873262004, 
  0.05550825019122593, 0.9920930186414277, 0.03029681486555637, 
  0.02939901936631002, 0.03569720205330306}; 

__global__ void
__launch_bounds__(128,4)
gpu_dimensionalize(const double *nondim_temp, const double *nondim_pres,
                   const double *avmolwt, const double *nondim_spec,
                   const int slice_stride, const int row_stride, 
                   const int total_steps, const int spec_stride,
                   double *dim_temp, double *dim_pres, double *dim_spec)
{
  {
    const int offset = (blockIdx.y*blockDim.y+threadIdx.y)*row_stride +
                       (blockIdx.x*blockDim.x+threadIdx.x) +
                       blockIdx.z*slice_stride;
    nondim_temp += offset;
    nondim_pres += offset;
    avmolwt += offset;
    nondim_spec += offset;
    dim_temp += offset;
    dim_pres += offset;
    dim_spec += offset;
  }
  // Update temperature
  {
    double temp = *nondim_temp;
    temp *= T_REF;
    *dim_temp = temp; 
  }
  // Update pressure
  {
    double pres = *nondim_pres;
    pres *= P_REF;
    *dim_pres = pres;
  }
  // Compute mole fractions
  const double avg = *avmolwt;
  #pragma unroll 
  for (int i = 0; i < 9; i++)
  {
    double yspec = nondim_spec[i*spec_stride];
    double next = yspec * (one_over_molecular_masses[i] * 1e3 / avg);
    dim_spec[i*spec_stride] = next;
  }
}

__host__
void run_gpu_dimensionalize(const double *nondim_temp, const double *nondim_pres,
                            const double *avmolwt, const double *nondim_spec,
                            const int nx, const int ny, const int nz,
                            double *dim_temp, double *dim_pres, double *dim_spec)
{
  assert(nx >= 32);
  assert(ny >= 4);
  assert((nx % 32) == 0);
  assert((ny % 4) == 0);
  dim3 grid(nx/32,ny/4,nz);
  dim3 block(32,4,1);
  gpu_dimensionalize<<<grid,block>>>(nondim_temp, nondim_pres,
                                                   avmolwt, nondim_spec,
                                                   nx*ny, nx, nz, nx*ny*nz,
                                                   dim_temp, dim_pres, dim_spec);
}

__global__ void
__launch_bounds__(128,4)
gpu_nondimensionalize(const int slice_stride, const int row_stride,
                      const int total_steps, const int spec_stride,
                      double *lambda, double *viscosity, double *diffusion)
{
  {
    const int offset = (blockIdx.y*blockDim.y+threadIdx.y)*row_stride +
                       (blockIdx.x*blockDim.x+threadIdx.x) +
                       blockIdx.z*slice_stride;
    lambda += offset;
    viscosity += offset;
    diffusion += offset;
  } 
  // Update lambda
  {
    double conductivity = *lambda;
    double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
    conductivity *= (1.0/(lambda_ref * 1e5));
    *lambda = conductivity;
  }
  // Update viscosity
  {
    double visc = *viscosity;
    visc *= (1.0/(VIS_REF * 10.0));
    *viscosity = visc;
  }
  // Update diffusion
  #pragma unroll
  for (int i = 0; i < 9; i++)
  {
    double *diff_spec = diffusion + i*spec_stride;
    double diff = *diff_spec;
    diff *= (1.0 / (DIF_REF * 1e4));
    *diff_spec = diff; 
  }
}

__host__
void run_gpu_nondimensionalize(const int nx, const int ny, const int nz,
                               double *lambda, double *viscosity, double *diffusion)
{
  assert(nx >= 32);
  assert(ny >= 4);
  assert((nx % 32) == 0);
  assert((ny % 4) == 0);
  dim3 grid(nx/32,ny/4,nz);
  dim3 block(32,4,1);
  gpu_nondimensionalize<<<grid,block>>>(nx*ny, nx, nz, nx*ny*nz,
                                                      lambda, viscosity, diffusion);
}

