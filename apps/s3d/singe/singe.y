
/**
 *
 * singe.y
 *
 * Parser definition for s3d generator
 *
 */

%{
  #include <cstring>
  #include <cstdio>
  #include <cassert>
  #include <cstdlib>
  #include <cmath>
  #include <iostream>

  extern char *curr_filename;
  void yyerror(const char *s);
  extern int yylex();

  // The translation unit with the result of the parse
  TranslationUnit *unit = NULL;
  // Number of erros discovered
  int num_errors = 0;
%}

%union  {
  TranslationUnit *unit;
  bool forward;
  char *str;
  double data; 
  Reactant *reac; 
  ReactantVec *reactants;
}

%token T_ELEMENTS T_SPECIES T_REACTIONS T_THERMO T_END T_EOL T_PLUS T_ALL 
%token T_EQUALS T_LRARROW T_RARROW T_SLASH
%token T_DUPLICATE T_LOW T_TROE T_SRI T_HV T_LT T_RLT T_REV
%token <str> T_NAME
%token <data> T_FLOAT

%type <reac>      Reactant
%type <reactants> Reactants
%type <forward>   Connector

%start Chemkin

%locations
%error_verbose

%%

Chemkin         : Elements Species OptThermo Reactions
                ;

Elements        : T_ELEMENTS Elems T_END
                ;

Elems           : Elems Elem
                | /* empty */
                ;

Elem            : T_NAME
                { assert(unit != NULL); unit->add_element($1); }
                ;

Species         : T_SPECIES Specs T_END
                ;

Specs           : Specs Spec
                | /* empty */
                ;

Spec            : T_NAME
                { assert(unit != NULL); unit->add_species($1); }
                ;

OptThermo       : T_THERMO Thermos T_END { assert(unit != NULL); unit->has_thermos = true; }
                | /* empty */
                ;

Thermos         : Thermos Thermitem
                | /* empty */
                ;

Thermitem       : T_NAME
                | T_FLOAT
                | T_SLASH
                ;

Reactions       : T_REACTIONS { lex_eol(true); } Reacs OptEnd { lex_eol(false); }
                ;

Reacs           : Reacs Reac
                | /* empty */
                ;

Reac            : GenuineReac
                | ReacTHB
                | ReacDuplicate
                | ReacLow
                | ReacTroe3
                | ReacTroe4
                | ReacSRI3
                | ReacSRI5
                | ReacHV
                | ReacRev
                | ReacLT
                | ReacRLT
                | T_EOL
                ;

GenuineReac     : Reactants Connector Reactants T_FLOAT T_FLOAT T_FLOAT T_EOL
                { assert(unit != NULL); unit->add_reaction($1, $2, $3, $4, $5, $6); }
                ;

ReacTHB         : T_NAME T_SLASH T_FLOAT T_SLASH
                { assert(unit != NULL); unit->add_thb(unit->find_or_add_species($1), $3); }
                ;

ReacDuplicate   : T_DUPLICATE
                { assert(unit != NULL); unit->add_duplicate(); }
                ;

ReacLow         : T_LOW T_SLASH T_FLOAT T_FLOAT T_FLOAT T_SLASH
                { assert(unit != NULL); unit->add_low($3, $4, $5); }
                ;

ReacTroe3       : T_TROE T_SLASH T_FLOAT T_FLOAT T_FLOAT T_SLASH
                { assert(unit != NULL); unit->add_troe3($3, $4, $5); }
                ;

ReacTroe4       : T_TROE T_SLASH T_FLOAT T_FLOAT T_FLOAT T_FLOAT T_SLASH
                { assert(unit != NULL); unit->add_troe4($3, $4, $5, $6); }
                ;

ReacSRI3        : T_SRI T_SLASH T_FLOAT T_FLOAT T_FLOAT T_SLASH
                { assert(unit != NULL); unit->add_sri3($3, $4, $5); }
                ;

ReacSRI5        : T_SRI T_SLASH T_FLOAT T_FLOAT T_FLOAT T_FLOAT T_FLOAT T_SLASH
                { assert(unit != NULL); unit->add_sri5($3, $4, $5, $6, $7); }
                ;

ReacHV          : T_HV T_SLASH T_FLOAT T_SLASH
                { assert(unit != NULL); unit->add_hv($3); }
                ;

ReacRev         : T_REV T_SLASH T_FLOAT T_FLOAT T_FLOAT T_SLASH
                { assert(unit != NULL); unit->add_rev($3, $4, $5); }
                ;

ReacLT          : T_LT T_SLASH T_FLOAT T_FLOAT T_SLASH
                { assert(unit != NULL); unit->add_lt($3, $4); }
                ;

ReacRLT         : T_RLT T_SLASH T_FLOAT T_FLOAT T_SLASH
                { assert(unit != NULL); unit->add_rlt($3, $4); }
                ;

Reactants       : Reactants T_PLUS Reactant
                { assert($1 != NULL); $1->push_back($3); $$ = $1; }
                | Reactant
                { $$ = new ReactantVec(); $$->push_back($1); }
                ;

Reactant        : T_NAME
                { assert(unit != NULL); $$ = unit->add_reactant(unit->find_or_add_species($1), 1); } 
                | T_FLOAT T_NAME
                { assert(unit != NULL); 
                  Species *spec = unit->find_or_add_species($2);
                  if ($1 != floor($1))
                  {
                    fprintf(stderr, "stoichometric coefficients on \"%s\" not integer, line %d",
                            spec->name, yylineno);
                    exit(1);
                  }
                  $$ = unit->add_reactant(spec, int(floor($1)));
                }
                ;

Connector       : T_EQUALS
                { $$ = true; }
                | T_LRARROW
                { $$ = true; }
                | T_RARROW
                { $$ = false; }
                ;

OptEnd          : T_END
                | /* empty */
                ;

%%

void yyerror(const char *s)
{
  std::cerr << "line " << yylloc.first_line << " column " << yylloc.first_column << ": " << s << " at or near ";
  std::cerr << std::endl;
  num_errors++;
  exit(1);
  //if (num_errors > 50)
  //{
  //  fprintf(stderr,"More than 50 errors, exiting.\n");
  //  exit(1);
  //}
}

// EOF

