
#ifndef __S3DGEN__
#define __S3DGEN__

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>

#include <set>
#include <map>
#include <list>
#include <vector>

#include "codegen.h"

class TranslationUnit;
class Element;
class Species;
class Reactant;
class Reaction;
class ThirdBody;
class Slicing;
class TranLib;
class Profiler;

class ChemistryUnit;
class Operation;
class ConnectedComponent;
class QSS;
class Statement;
class Zero;
class Den;
class Multiply;
class Add;
class Divide;
class Xstat;
class Stif;
class Warp;
class BankAnalyzer;
class MultiAnalyzer;
class QSSASharedMemoryStore;

typedef std::set<Element*> ElementSet;
typedef std::set<Species*> SpeciesSet;
typedef std::vector<Species*> SpeciesVec;
typedef std::vector<Reactant*> ReactantVec;
typedef std::vector<Reaction*> ReactionVec;
typedef std::map<Species*,int> SpeciesCoeffMap;
typedef std::vector<ThirdBody*> ThirdBodyVec;

class TranslationUnit {
public:
  TranslationUnit(void);
  ~TranslationUnit(void);
public:
  static TranslationUnit* parse_source_file(const char *file_name);
public:
  void parse_therm_file(const char *file_name);
  void parse_tran_asc_file(const char *file_name);
  bool parse_tran_dat_file(const char *file_name);
  void parse_qss_stif_file(const char *file_name);
public:
  Element* add_element(const char *name);
  Species* add_species(const char *name);
  Species* find_or_add_species(const char *name);
  Species* find_species(const char *name, bool must = true);
  Reactant* add_reactant(Species *spec, int coeff);
  Reaction* add_reaction(ReactantVec *lhs, bool reversible, ReactantVec *rhs, double a, double beta, double e);
  ThirdBody* add_third_body(void);
  ThirdBody* find_or_add_third_body(const std::map<Species*,double> &coeffs);
public:
  void set_pressure_species(const char *spec_name);
  void add_thb(Species *spec, double coef);
  void add_duplicate(void);
  void add_low(double a, double beta, double e);
  void add_troe3(double a, double t3, double t1);
  void add_troe4(double a, double t3, double t1, double t2);
  void add_sri3(double a, double b, double c);
  void add_sri5(double a, double b, double c, double d, double e);
  void add_hv(double wavelength);
  void add_rev(double a, double beta, double e);
  void add_lt(double b, double c);
  void add_rlt(double b, double c);
public:
  void check_species_coefficients(void) const;
  void check_species_masses(void) const;
  inline size_t get_num_species(void) const { return (species.size()-1/*ignore third body species*/); }
public:
  // Call this when parsing is finished so we can clean up the translation unit
  void post_parse(void);
  void non_dimensionalize(void);
  void print_summary(void) const;
public:
  void emit_physics_profiling(void);
  void emit_chemistry_profiling(void);
public:
  void emit_gpu_constant_header(void);
  void emit_macros(CodeOutStream &ost);
public:
  void emit_baseline_cpu_physics(const char *prefix);
  void emit_baseline_cpu_chemistry(const char *prefix);
  void emit_sse_cpu_physics(const char *prefix);
  void emit_sse_cpu_chemistry(const char *prefix);
  void emit_avx_cpu_physics(const char *prefix);
  void emit_avx_cpu_chemistry(const char *prefix);
  void emit_cuda_physics(const char *prefix);
  void emit_cuda_chemistry(const char *prefix);
public:
  void emit_cpu_mole_masses(CodeOutStream &ost);
  void emit_cpu_recip_mole_masses(CodeOutStream &ost);
  void emit_baseline_cpu_physics_header_file(const char *file_name);
  void emit_baseline_cpu_physics_source_file(const char *file_name, const char *header_name);
public:
  void emit_vector_cpu_physics_header_file(const char *file_name, bool sse, bool avx);
  void emit_vector_cpu_physics_source_file(const char *file_name, const char *header_name,
                                           bool sse, bool avx);
public: 
  void emit_gpu_mole_masses(CodeOutStream &ost);
  void emit_gpu_recip_mole_masses(CodeOutStream &ost);
  void emit_gpu_physics_header_file(const char *file_name);
  void emit_gpu_physics_source_file(const char *file_name, const char *header_name);
public:
  void emit_baseline_cpu_chemistry_header_file(const char *file_name);
  void emit_baseline_cpu_chemistry_source_file(const char *file_name, const char *header_name);
  void emit_vector_cpu_chemistry_header_file(const char *file_name, bool sse, bool avx);
  void emit_vector_cpu_chemistry_source_file(const char *file_name, const char *header_name, 
                                             bool sse, bool avx);
  void emit_gpu_chemistry_header_file(const char *file_name);
  void emit_gpu_chemistry_source_file(const char *file_name, const char *header_name);
public:
  void emit_cpu_getrates_declaration(CodeOutStream &ost);
  void emit_baseline_cpu_getrates(CodeOutStream &ost);
  void emit_vector_getrates_declaration(CodeOutStream &ost, bool sse, bool avx);
  void emit_sse_cpu_getrates(CodeOutStream &ost);
  void emit_avx_cpu_getrates(CodeOutStream &ost);
  void emit_gpu_getrates_declaration(CodeOutStream &ost);
  void emit_baseline_gpu_getrates(CodeOutStream &ost);
  void emit_warp_specialized_gpu_getrates(CodeOutStream &ost);
  void emit_getrates_numerical_constants(CodeOutStream &ost);
  void emit_getrates_temperature_dependent(CodeOutStream &ost, Profiler *profiler);
  void emit_getrates_gibbs_computation(CodeOutStream &ost, Profiler *profiler);
  void emit_getrates_mole_frac_computation(CodeOutStream &ost, Profiler *profiler);
  void emit_getrates_third_body_computation(CodeOutStream &ost, Profiler *profiler);
  void emit_getrates_reaction_rates(CodeOutStream &ost, Profiler *profiler);
  void emit_getrates_wdot_computation(CodeOutStream &ost, Profiler *profiler);
public:
  void emit_sse_temperature_dependent(CodeOutStream &ost, bool main, size_t iters);
  void emit_sse_gibbs_computation(CodeOutStream &ost, bool main, size_t iters);
  void emit_sse_mole_frac_computation(CodeOutStream &ost, bool main, size_t iters);
  void emit_sse_third_body_computation(CodeOutStream &ost, bool main, size_t iters);
  void emit_sse_reaction_rates(CodeOutStream &ost, bool main, size_t iters);
  void emit_sse_wdot_computation(CodeOutStream &ost, bool main, size_t iters);
  void emit_sse_gibbs_recurse(CodeOutStream &ost, Species *spec, int offset, int mask);
  void emit_sse_gibbs_impl(CodeOutStream &ost, Species *spec, int mask);
public:
  void emit_avx_temperature_dependent(CodeOutStream &ost, bool main, size_t iters);
  void emit_avx_gibbs_computation(CodeOutStream &ost, bool main, size_t iters);
  void emit_avx_mole_frac_computation(CodeOutStream &ost, bool main, size_t iters);
  void emit_avx_third_body_computation(CodeOutStream &ost, bool main, size_t iters);
  void emit_avx_reaction_rates(CodeOutStream &ost, bool main, size_t iters);
  void emit_avx_wdot_computation(CodeOutStream &ost, bool main, size_t iters);
  void emit_avx_gibbs_recurse(CodeOutStream &ost, Species *spec, int offset, int mask);
  void emit_avx_gibbs_impl(CodeOutStream &ost, Species *spec, int mask);
public:
  void emit_generalized_gibbs_impl(CodeOutStream &ost, int offset, int mask, bool sse, bool avx);
  void emit_generalized_sse_gibbs(CodeOutStream &ost, int mask);
  void emit_generalized_avx_gibbs(CodeOutStream &ost, int mask);
public:
  // Warp-specialized getrates code generation
  void assign_reactions_to_warps(void);
  void construct_shared_memory_analyzer();
  void emit_warp_specialized_chem_code(CodeOutStream &ost);
  void emit_warp_specialized_chemistry_level(CodeOutStream &ost, int level, int buffer, int version);
  void emit_warp_specialized_reac_code(CodeOutStream &ost, int version, int buffer);
  void emit_warp_specialized_write_code(CodeOutStream &ost);
  void emit_warp_specialized_stiffness_code(CodeOutStream &ost, int version, int buffer);
  void emit_warp_specialized_output_code(CodeOutStream &ost, int version);
  void emit_warp_specialized_update_pointers_code(CodeOutStream &ost);
  void emit_warp_specialized_qssa_code(CodeOutStream &ost, int version);
  void emit_warp_specialized_cc(CodeOutStream &ost, ConnectedComponent *cc, int version);
  void emit_warp_specialized_full_code(CodeOutStream &ost,
      const char *temp_chem_name, const char *temp_qssa_name);
  void emit_warp_specialized_constants(CodeOutStream &ost);
  void emit_warp_specialized_load_rates(CodeOutStream &ost, const std::vector<int> &locations,
        const std::vector<Warp*> &warps, const char *var_name, int version, int warp_offset);
public:
  // Multi-pass warp-specialized code generation
  void emit_multi_gpu_getrates(CodeOutStream &ost);
  int multi_assign_reactions_to_warps(void);
  void multi_construct_shared_memory_analyzers(int chem_levels_per_pass, int chem_passes,
                                               int qssa_levels_per_pass, int qssa_passes,
                                               int stif_levels_per_pass, int stif_passes,
                                               int output_levels_per_pass, int output_passes,
                                               int offset);
  void emit_multi_chem_code(CodeOutStream &ost, int chem_passes, int chem_levels_per_pass,
                                                int qssa_passes, int qssa_levels_per_pass, int qssa_levels,
                                                int stif_passes, int stif_levels_per_pass,
                                                int output_passes, int output_levels_per_pass,
                                                int shared_offset, int chem_pass_values);
  void emit_multi_init_chem_code(CodeOutStream &ost);
  void emit_multi_chem_pass(CodeOutStream &ost, int start_level, int stop_level, int pass, bool qssa_pass);
  void emit_multi_chem_level(CodeOutStream &ost, int level);
  void emit_multi_qssa_scaling(CodeOutStream &ost, int qssa_levels);
  void emit_multi_init_stif_code(CodeOutStream &ost);
  void emit_multi_stif_pass(CodeOutStream &ost, int pass, int start_level, int stop_level);
  void emit_multi_stif_scaling(CodeOutStream &ost);
  void emit_multi_init_out_code(CodeOutStream &ost);
  void emit_multi_out_pass(CodeOutStream &ost, int pass, int start_level, int stop_level);
  void emit_multi_output(CodeOutStream &ost);
  void emit_multi_qssa_code(CodeOutStream &ost, int qssa_passes, int levels_per_pass, int shared_rrs);
  void emit_multi_qssa_init(CodeOutStream &ost);
  void emit_multi_qssa_pass(CodeOutStream &ost, int pass, int start_level, int stop_level);
  void emit_multi_qssa_scaling(CodeOutStream &ost);
  void emit_multi_cc_init(CodeOutStream &ost, ConnectedComponent *cc);
  void emit_multi_cc_pass(CodeOutStream &ost, ConnectedComponent *cc, 
                          int pass, int start_level, int stop_level);
  void emit_multi_cc_scaling(CodeOutStream &ost, ConnectedComponent *cc);
  void emit_multi_full_code(CodeOutStream &ost, 
        const char * temp_chem_name, const char *temp_qssa_name,
        int cta_shared_size);
  void emit_multi_load_rates(CodeOutStream &ost, const std::vector<int> &locations,
        const std::vector<Warp*> &warps, const char *var_name, int warp_offset, int pass);
  void emit_multi_reduce(CodeOutStream &ost, const char *var_name);
  void emit_multi_warp_spec_qssa_init(CodeOutStream &ost, int wid);
  void emit_multi_warp_spec_qssa_pass(CodeOutStream &ost, int pass, int start_level,
                                      int stop_level, int wid);
  void emit_multi_warp_spec_qssa_scaling(CodeOutStream &ost, QSSASharedMemoryStore &store, int wid);
  void emit_multi_inline_qssa_code(CodeOutStream &ost, int shared_size);
  QSSASharedMemoryStore* configure_warp_spec_qssa(int shared_size, int global_offset);
public:
  // Small GPU warp-specialized code generation for smaller effective warp sizes
  void emit_small_gpu_getrates(CodeOutStream &ost);
  void emit_small_chem_code(CodeOutStream &ost, int chem_passes, int chem_levels_per_pass,
                                                int qssa_passes, int qssa_levels_per_pass, int qssa_levels,
                                                int stif_passes, int stif_levels_per_pass,
                                                int output_passes, int output_levels_per_pass,
                                                int shared_offset);
  void emit_small_full_code(CodeOutStream &ost, const char * temp_chem_name, int cta_shared_size);
  void emit_small_chem_pass(CodeOutStream &ost, int start_level, int stop_level, int pass, bool qssa_pass);
  void emit_small_inline_qssa_code(CodeOutStream &ost, int shared_size);
  void emit_small_qssa_scaling(CodeOutStream &ost, int qssa_levels);
  void emit_small_init_stif_code(CodeOutStream &ost);
  void emit_small_stif_pass(CodeOutStream &ost, int stif_pass, int start_level, int stop_level);
  void emit_small_stif_scaling(CodeOutStream &ost);
  void emit_small_init_out_code(CodeOutStream &ost);
  void emit_small_out_pass(CodeOutStream &ost, int pass, int start_level, int stop_level);
  void emit_small_output(CodeOutStream &ost);
  int small_distribute_indexes(const std::vector<int> &indexes,
                               std::vector<std::vector<int> > &dist, bool pad_with_zeros);
public:
  int find_ordered_index(Species *spec) const;
  int find_ordered_reaction(Reaction *reac) const;
  int find_gibbs_index(Species *spec) const;
  int find_troe_index(Reaction *reac) const;
public:
  bool use_dim;
  bool has_thermos;
  bool needs_dln10; // set in post_parse
  bool k20; // target k20 gpus
  bool multi_pass_warp_specialized;
  bool use_indexing;
  bool qssa_in_shared;
  bool spill_reac_rates;
  bool spill_gibbs;
  bool spill_qssa;
  bool emit_debug;
  bool unroll_loops;
  bool unaligned;
  bool perfect;
  bool profile;
  int warp_size; // 1 or 2 or 4 or 8 or 16 or 32
  int viscosity_warps;
  int diffusion_warps;
  int reac_warps;
  int qss_warps;
  int target_ctas;
  int qssa_skip;
  int qssa_spill_size;
  int taylor_stages;
  size_t cache_size;
  const char *small_mole_frac;
  const char *small_reac_rate;
  const char *large_reac_rate;
public:
  TranLib *tran; 
  ChemistryUnit *chem;
private:
  friend class Slicing;
  friend class TranLib;
  ElementSet elements;
public:
  SpeciesSet species;
  // No third-body in ordered species
  // No QSSA species in ordered species
  SpeciesVec ordered_species; 
  // Species for which we need to compute
  // the gibbs energy value
  SpeciesVec gibbs_species;
  // The set of actual reactions with no
  // duplicates (only owners)
  ReactionVec act_reactions;
  // Reactions that need troe constants
  ReactionVec troe_reactions;
public:
  ReactantVec all_reactants;
  ReactionVec all_reactions;
  ThirdBodyVec third_bodies;
private:
  // Used for parsing only
  bool pressure_dep;
  Species *pressure_spec;
  Reaction *current_reac;
private:
  // Used for warp-specialized chemistry code generation
  friend class QSS;
  int points_per_cta;
  std::vector<Warp*> chem_warps;
  std::vector<Warp*> qssa_warps;
  BankAnalyzer *bank_analyzer;
  //int reac_unity;
  //int reac_zero;
  //int spec_unity;
  int shared_unity;
  int shared_zero;
  //MultiAnalyzer *multi_analyzer;
  std::vector<int> stif_meta;
  std::set<std::pair<int,int> > multi_emitted;
  MultiAnalyzer *chem_analyzer;
  MultiAnalyzer *stif_analyzer;
  MultiAnalyzer *out_analyzer;
  MultiAnalyzer *global_analyzer;
  int thb_offset;
  int gibbs_offset;
  int reac_offset;
  int sm_offset;
private:
  Profiler *conductivity_profiler;
  Profiler *diffusion_profiler;
  Profiler *viscosity_profiler;
  Profiler *chemistry_profiler;
};

class Element {
public:
  Element(const char *name, unsigned idx);
public:
  const unsigned idx;
  const char *name;
};

class Species {
public:
  Species(TranslationUnit *unit, const char *name, unsigned idx);
  ~Species(void);
public:
  void notify(Reaction* reac, unsigned reac_idx, int coeff);
public:
  void set_coefficients(double low, double high, double common, double *coeffs);
  bool coefficients_set(void) const;
public:
  void update_mass(const char *element_name, int element_count);
  bool is_mass_set(void) const { return mass_set; }
public:
  double get_common_temperature(void) const;
  // Sometimes we want the original coefficients (see s3dgen_parse.cc)
  // but most times we want the scaled versions, so if you want the 
  // original ones say so.
  double get_high_coefficient(unsigned index, bool orig = false) const;
  double get_low_coefficient(unsigned index, bool orig = false) const;
public:
  void emit_contributions(CodeOutStream &ost, unsigned local_idx, Profiler *profiler);
  void emit_sse_contributions(CodeOutStream &ost, unsigned local_idx, int iters);
  void emit_avx_contributions(CodeOutStream &ost, unsigned local_idx, int iters);
private:
  TranslationUnit *const unit;
public:
  const unsigned idx;
  const char *name;
  char *code_name;
  bool qss_species;
  double low_temp, high_temp, common_temp;
private:
  double *thermo_coefficients;
public:
  double mass_set;
  double molecular_mass;
  std::map<Reaction*,int/*coeff*/> reaction_contributions;
};

class Reactant {
public:
  Reactant(Species *spec, int coeff);
public:

public:
  Species *spec;
  int coeff;
};

class Reaction {
public:
  Reaction(unsigned id, TranslationUnit *unit, ReactantVec *lhs, bool reversible, ReactantVec *rhs, 
           double a, double beta, double e,
           bool pressure_dep, Species *pressure_species);
public:
  void add_thb(Species *spec, double coeff);
  void set_duplicate(void);
  void add_low(double a, double beta, double e);
  void add_troe3(double a, double t3, double t1);
  void add_troe4(double a, double t3, double t1, double t2);
  void add_sri3(double a, double b, double c);
  void add_sri5(double a, double b, double c, double d, double e);
  void add_hv(double wavelength);
  void add_rev(double a, double beta, double e);
  void add_lt(double b, double c);
  void add_rlt(double b, double c);
public:
  bool need_default_third_body(void) const;
  void find_third_body(void);
  bool is_duplicate(void) const;
  void check_duplicate_and_reverse(void);
  bool is_owned(void) const;
  bool is_owner(void) const { return !is_owned(); }
  bool matches(const Reaction *other) const;
  void add_to_duplicates(Reaction *other);
  void set_owner(Reaction *owner);
  void non_dimensionalize(void);
  void notify_species(unsigned reaction_idx);
  void find_gibbs_species(std::set<Species*> &gibbs) const;
public:
  void emit_reaction_code(CodeOutStream &ost, unsigned local_idx, Profiler *profiler);
  void emit_reaction_comment(CodeOutStream &ost, unsigned local_idx);
  void emit_forward_reaction_rate(CodeOutStream &ost, Profiler *profiler);
  void emit_single_forward(CodeOutStream &ost, const char *prefix, bool first, Profiler *profiler);
  void emit_reverse_reaction_rate(CodeOutStream &ost, Profiler *profiler);
  void arrhenius(CodeOutStream &ost, double prefix, double beta, double e, Profiler *profiler);
  void emit_warp_specialized_weird_reaction(CodeOutStream &ost, int level, int buffer);
public:
  void emit_sse_reaction_code(CodeOutStream &ost, unsigned local_idx, int iters);
  void emit_sse_forward_reaction_rate(CodeOutStream &ost, int iters);
  void emit_sse_single_forward_reaction_rate(CodeOutStream &ost, bool first, int iters);
  void emit_sse_reverse_reaction_rate(CodeOutStream &ost, int iters);
  void sse_arrhenius(CodeOutStream &ost, const char *var, int iters, 
                     double prefix, double beta, double e); 
public:
  void emit_avx_reaction_code(CodeOutStream &ost, unsigned local_idx, int iters);
  void emit_avx_forward_reaction_rate(CodeOutStream &ost, int iters);
  void emit_avx_single_forward_reaction_rate(CodeOutStream &ost, bool first, int iters);
  void emit_avx_reverse_reaction_rate(CodeOutStream &ost, int iters);
  void avx_arrhenius(CodeOutStream &ost, const char *var, int iters, 
                     double prefix, double beta, double e); 
public:
  // The index of this reaction in the translation unit's data structures
  const unsigned int idx;
public:
  unsigned int get_global_idx(void) const { return idx; }
  // The index of the reaction in emitted code, always holds that (reaction_idx <= idx)
  int reaction_idx;
  const SpeciesSet& get_species(void) const { return all_species; }
  bool has_species_coeff(Species *spec) const;
  int get_species_coeff(Species *spec) const;
  bool needs_dln10(void) const { return (low.enabled && (troe.num > 0)); }
  bool is_weird_reaction(void) const;
public:
  TranslationUnit *const unit;
  double a, beta, e;
  bool third_body, reversible, pressure_dep;
  Species *pressure_species;
  std::map<Species*,double> thb_values; // Used only in parsing, will go away when we common sub-expression eliminate
  ThirdBody       *thb;
  SpeciesCoeffMap stoich; /* stoichometry */
  SpeciesCoeffMap forward; /* forward atomic rate */
  SpeciesCoeffMap backward; /* backward atomic rate */
  SpeciesSet      all_species; /* list of species mentioned in the reaction at all */
  struct {
    bool enabled;
    bool owner;
    // Either this reaction is an owner in which case we keep a list of the duplicates
    // or it is one of the duplicates in which case we remember the owner of this duplicate
    union {
      Reaction *owner;
      std::list<Reaction*> *duplicates;
    } ptr;
  } duplicate;
  struct {
    bool enabled;
    double a, beta, e;
  } low;
  struct {
    int num;
    double a, t3, t1, t2;
  } troe;
  struct {
    int num;
    double a, b, c, d, e;
  } sri;
  struct {
    bool enabled;
    double wavelength;
  } hv;
  struct {
    bool enabled;
    double a, beta, e;
  } rev;
  struct {
    bool enabled;
    double b, c;
  } lt;
  struct {
    bool enabled;
    double b, c;
  } rlt;
};

class ThirdBody {
public:
  ThirdBody(unsigned idx);
public:
  void initialize(const std::map<Species*,double> &coeffs);
  bool matches(const std::map<Species*,double> &coeffs) const;
public:
  const unsigned idx;
public:
  std::map<Species*,double> components;
};

class TranLib {
public:
  TranLib(int order, int num_species, int nlite, double patmos, TranslationUnit *unit);
public:
  bool is_diffusion_symmetric(void) const;
public:
  int order, num_species, nlite;
  double patmos;
  TranslationUnit *const unit;
  double *nwt; // molecular weights for the species (num_species)
  double *neps; // epsilon/k well depth for the species (num_species)
  double *nsig; // collision diameter for the species (num_species)
  double *ndip; // dipole moments for the species (num_species)
  double *npol; // polarizabilities for the species (num_species)
  double *nzrot; // rotational relaxation collision numbers (num_species)
  int    *inlin; // indicators for molecule linearity
  double *nlam; // fit coefficients for conductivity (num_speces * order)
  double *neta; // fit coefficients for viscosity (num_species * order)
  double *ndif; // fit coefficients for diffusion (num_species * num_species * order)
  int    *iktdif; // species indicies for the light species (nlite)
  double *ntdif; // fit coefficients for thermal diffiusion ratio (num_species * order * nlite)
public:
  void emit_baseline_cpu_conductivity(CodeOutStream &ost, Profiler *profiler);
  void emit_baseline_cpu_viscosity(CodeOutStream &ost, Profiler *profiler);
  void emit_baseline_cpu_diffusion(CodeOutStream &ost, Profiler *profiler);
  void emit_baseline_cpu_thermal(CodeOutStream &ost);
  void emit_cpu_conductivity_declaration(CodeOutStream &ost);
  void emit_cpu_viscosity_declaration(CodeOutStream &ost);
  void emit_cpu_diffusion_declaration(CodeOutStream &ost);
  void emit_cpu_thermal_declaration(CodeOutStream &ost);
public:
  void emit_sse_cpu_conductivity(CodeOutStream &ost);
  void emit_sse_cpu_viscosity(CodeOutStream &ost);
  void emit_sse_cpu_diffusion(CodeOutStream &ost);
  void emit_sse_cpu_thermal(CodeOutStream &ost);
  void emit_avx_cpu_conductivity(CodeOutStream &ost);
  void emit_avx_cpu_viscosity(CodeOutStream &ost);
  void emit_avx_cpu_diffusion(CodeOutStream &ost);
  void emit_avx_cpu_thermal(CodeOutStream &ost);
  void emit_vector_conductivity_declaration(CodeOutStream &ost, bool sse, bool avx);
  void emit_vector_viscosity_declaration(CodeOutStream &ost, bool sse, bool avx);
  void emit_vector_diffusion_declaration(CodeOutStream &ost, bool sse, bool avx);
  void emit_vector_thermal_declaration(CodeOutStream &ost, bool sse, bool avx);
public:
  void emit_cuda_conductivity(CodeOutStream &ost);
  void emit_cuda_viscosity(CodeOutStream &ost);
  void emit_cuda_diffusion(CodeOutStream &ost);
  void emit_cuda_thermal(CodeOutStream &ost);
  void emit_cuda_warp_specialized_viscosity(CodeOutStream &ost);
  void emit_cuda_warp_specialized_diffusion(CodeOutStream &ost);
  void emit_gpu_conductivity_declaration(CodeOutStream &ost);
  void emit_gpu_viscosity_declaration(CodeOutStream &ost);
  void emit_gpu_diffusion_declaration(CodeOutStream &ost);
  void emit_gpu_thermal_declaration(CodeOutStream &ost);
public:
  // For instruction cache experiments
  void emit_cuda_warp_specialized_naive_viscosity(CodeOutStream &ost);
};

class Profiler {
public:
  Profiler(const char *name);
  Profiler(const Profiler &rhs);
  ~Profiler(void);
public:
  Profiler& operator=(const Profiler &rhs);
public:
  inline void reads(unsigned r) { read_count += r; }
  inline void writes(unsigned w) { write_count += w; }
  inline void adds(unsigned a) { add_count += a; }
  inline void subtracts(unsigned s) { subtract_count += s; }
  inline void multiplies(unsigned m) { multiply_count += m; }
  inline void divides(unsigned d) { divide_count += d; }
  inline void exponents(unsigned e) { exponent_count += e; }
  inline void logarithms(unsigned l) { logarithm_count += l; }
  inline void sqrts(unsigned s) { sqrt_count += s; }
protected:
  const char *const name;
  unsigned read_count;
  unsigned write_count;
  unsigned add_count;
  unsigned subtract_count;
  unsigned multiply_count;
  unsigned divide_count;
  unsigned exponent_count;
  unsigned logarithm_count;
  unsigned sqrt_count;
};

class ChemistryUnit {
public:
  ChemistryUnit(TranslationUnit *unit);
  friend class TranslationUnit;
public:
  void emit_stiffness_information(CodeOutStream &ost);
public:
  void add_component(ConnectedComponent *comp);
  void add_unimportant(int backward, Reaction *reac);
  // For the Aq_0
  void add_forward_zero(Reaction *reac, int qss_idx, int coeff);
  void add_backward_zero(Reaction *reac, int qss_idx, int coeff);
  // For Aq_other
  void add_forward_contribution(Reaction *reac, int qss_idx, int other_idx, int coeff);
  void add_backward_contribution(Reaction *reac, int qss_idx, int other_idx, int coeff);
  // For denom
  void add_forward_denom(int qss_idx, Reaction *reac);
  void add_backward_denom(int qss_idx, Reaction *reac);
  // For final correction
  void add_forward_correction(Reaction *reac, int qss_idx);
  void add_backward_correction(Reaction *reac, int qss_idx);
public:
  bool is_qssa_reaction(Reaction *reac) const;
  int find_qss_index(Reaction *reac, bool forward) const;
public:
  void add_stif(Stif *stif);
public:
  void emit_unimportant_rates(CodeOutStream &ost, Profiler *profiler);
  void emit_qssa_modifications(CodeOutStream &ost, Profiler *profiler);
  void emit_stiffness_modifications(CodeOutStream &ost, Profiler *profiler);
public:
  void emit_sse_unimportant_rates(CodeOutStream &ost, bool main, int iters);
  void emit_sse_qssa_modifications(CodeOutStream &ost, bool main, int iters);
  void emit_sse_stiffness_modifications(CodeOutStream &ost, bool main, int iters);
public:
  void emit_avx_unimportant_rates(CodeOutStream &ost, bool main, int iters);
  void emit_avx_qssa_modifications(CodeOutStream &ost, bool main, int iters);
  void emit_avx_stiffness_modifications(CodeOutStream &ost, bool main, int iters);
public:
  void remove_duplicate_reactions(void);
  void construct_qss_graph(std::vector<QSS*> &qss_nodes);
private:
  ConnectedComponent *get_qss(int spec);
public:
  int reaction_granularity;
private:
  TranslationUnit *const unit;
  std::vector<ConnectedComponent*> connected_components;
  std::set<Reaction*> forward_unimportant;
  std::set<Reaction*> backward_unimportant;
  std::vector<Stif*> stif_operations;
private:
  std::vector<Warp*> warps;
  std::map<Reaction*,int/*location*/> reaction_map;
  std::map<int/*reac idx*/,int/*location*/> reaction_index_map;
private:
  std::vector<Warp*> qssa_warps;
  std::vector<Warp*> stif_warps;
  std::vector<Warp*> last_warps;
  std::vector<Warp*> full_warps;
private:
  std::vector<Species*> gibbs_species;
  std::vector<Reaction*> gibbs_reactions;
  std::vector<Reaction*> needed_troe_reactions;
};

class Operation {
public:
  Operation(void) { }
public:
  void sanity_check(void);
  void get_read_values(std::set<int> &forward, std::set<int> &backward) const;
  void get_write_values(std::set<int> &forward, std::set<int> &backward) const;
  void remove_read_values(std::set<int> &forward, std::set<int> &backward) const;
  void remove_write_values(std::set<int> &forward, std::set<int> &backward) const;
  bool can_swap(const Operation *previous) const;
public:
  virtual const char* get_name(void) const = 0;
  virtual bool is_stif(void) const = 0;
public:
  size_t ffirst_size(void) const { return ffirst.size(); }
  size_t bfirst_size(void) const { return bfirst.size(); }
  size_t flive_size(void) const { return flive.size(); }
  size_t blive_size(void) const { return blive.size(); }
  size_t first_reac_size(void) const { return reactions.size(); }
  size_t fread_size(void) const { return fread.size(); }
  size_t bread_size(void) const { return bread.size(); }
  size_t fwrite_size(void) const { return fwrite.size(); }
  size_t bwrite_size(void) const { return bwrite.size(); }
protected:
  void add_fread(int x) { fread.insert(x); }
  void add_bread(int x) { bread.insert(x); }
  void add_fwrite(int x) { fwrite.insert(x); }
  void add_bwrite(int x) { bwrite.insert(x); }
protected:
  friend class ChemistryUnit;
  std::set<int> fread; // reading forward reaction rates
  std::set<int> bread; // reading backward reaction rates
  std::set<int> fwrite; // writing forward reaction rates
  std::set<int> bwrite; // writing backward reaction rates
protected:
  std::set<int> flive; // live forward reaction rates
  std::set<int> blive; // live backward reaction rates
  std::set<int> ffirst; // first forward reaction rates
  std::set<int> bfirst; // first backward reaction rates
  std::set<int> reactions; // first reactions
};

class ConnectedComponent : public Operation {
public:
  ConnectedComponent(TranslationUnit *unit, ConnectedComponent *orig = NULL);
public:
  void add_forward_zero(Reaction*, int qss_idx, int coeff);
  void add_backward_zero(Reaction*, int qss_idx, int coeff);
  void add_forward_contribution(Reaction*, int qss_idx, int other_idx, int coeff);
  void add_backward_contribution(Reaction*, int qss_idx, int other_idx, int coeff);
  void add_forward_denom(int qss_idx, Reaction*);
  void add_backward_denom(int qss_idx, Reaction*);
  void add_forward_correction(Reaction*, int qss_idx);
  void add_backward_correction(Reaction*, int qss_idx);
public:
  // Ay_0 = Ay_0 + Ay_x * Ax_0
  void add_zero_statement(int y, int x);
  void begin_den_statement(void);
  void end_den_statement(void);
  // Add DEN pair for 1 - Ay_x*Ax_y for all pairs
  void add_den_pair(int y, int x);
  // Ay_z = Ay_x * Ax_z
  void add_multiply_statement(int z, int y, int x);
  // Ay_z = Ay_z + Ay_x * Ax_z
  void add_add_statement(int z, int y, int x);
  // Ay_z = Ay_z/DEN
  void add_divide_statement(int z, int y);
  // XQ(x) = Ax_0
  void start_x_statement(int x);
  void finish_x_statement(void);
  // +Ax_y*XQ(y)
  void add_x_pair(int x, int y);
public:
  void add_species(int id, int spec);
  bool has_species(int id);
  size_t num_species(void) const { return species.size(); }
  bool is_qssa_reaction(Reaction *reac) const;
  int find_qss_index(Reaction *reac, bool forward) const;
public:
  virtual const char* get_name(void) const { return "Connected Component"; }
  virtual bool is_stif(void) const { return false; }
public:
  bool is_simple(void) const;
  bool has_qss(QSS *qss) const;
public:
  void remove_duplicate_reactions(void);
  void emit_statements(CodeOutStream &ost, Profiler *profiler);
  void emit_sse_statements(CodeOutStream &ost, int iters);
  void emit_avx_statements(CodeOutStream &ost, int iters);
  void construct_qss_graph(std::vector<QSS*> &qss_nodes);
private:
  QSS* get_qss(int spec);
public:
  std::map<int,QSS*> species;
  std::vector<Statement*> statements;
  TranslationUnit *unit;
private:
  // For parsing only
  Den *current_den;
  Xstat *current_x;
};

class QSS {
public:
  QSS(int id, int spec, TranslationUnit *unit);
public:
  int get_total_values(void) const;
public:
  void add_forward_zero(Reaction*, int coeff);
  void add_backward_zero(Reaction*, int coeff);
  void add_forward_contribution(Reaction*, int other_idx, int coeff);
  void add_backward_contribution(Reaction*, int other_idx, int coeff);
  void add_forward_denom(Reaction*);
  void add_backward_denom(Reaction*);
  void add_forward_correction(Reaction*);
  void add_backward_correction(Reaction*);
public:
  bool is_qssa_reaction(Reaction *reac) const;
  int find_qss_index(Reaction *reac, bool forward) const;
  void remove_duplicate_reactions(void);
public:
  void emit_statements(CodeOutStream &ost, 
              std::set<std::pair<int,int> > &already_emitted, Profiler *profiler);
  void emit_updates(CodeOutStream &ost, Profiler *profiler);
public:
  void emit_sse_statements(CodeOutStream &ost,
              std::set<std::pair<int,int> > &already_emitted, int iters);
  void emit_sse_updates(CodeOutStream &ost, int iters);
public:
  void emit_avx_statements(CodeOutStream &ost,
              std::set<std::pair<int,int> > &already_emitted, int iters);
  void emit_avx_updates(CodeOutStream &ost, int iters);
public:
  void emit_warp_specialized_statements(CodeOutStream &ost,
              std::set<std::pair<int,int> > &already_emitted, 
              BankAnalyzer *bank_analyzer, 
              std::vector<Warp*> &qssa_warps, int version, int warp_offset);
  void emit_warp_specialized_updates(CodeOutStream &ost, 
              BankAnalyzer *bank_analyzer, 
              std::vector<Warp*> &qssa_warps, int version, int warp_offset);
  void emit_multi_loads(CodeOutStream &ost, int pass, MultiAnalyzer *multi_analyzer,
              std::vector<Warp*> &qssa_warps, int warp_offset);
  void emit_multi_reductions(CodeOutStream &ost);
public:
  const int qss_idx;
  const int species_idx;
public:
  std::set<int> member_species;
  std::map<Reaction*,int/*coeff*/> forward_zeros;
  std::map<Reaction*,int/*coeff*/> backward_zeros;
  std::map<std::pair<int/*other idx*/,Reaction*>,int/*coeff*/> forward_contributions;
  std::map<std::pair<int/*other idx*/,Reaction*>,int/*coeff*/> backward_contributions;
  std::set<Reaction*> forward_denom;
  std::set<Reaction*> backward_denom;
  std::set<Reaction*> forward_corrections;
  std::set<Reaction*> backward_corrections;
public:
  TranslationUnit *unit;
public:
  // QSS DAG information for generation of warp-specialized QSSA code
  std::set<QSS*> inputs;
  std::set<QSS*> outputs;
  // All inputs from the start of the DAG
  std::set<QSS*> closure_inputs;
  std::map<QSS*,int> input_barriers;
  std::map<QSS*,int> output_barriers;
  std::map<QSS*,int> xstat_input_barriers;
  std::map<QSS*,int> xstat_output_barriers;
  std::vector<Statement*> statements;
  Xstat *xstat; // The xstat statement for this species 
};

class Statement { 
public:
  virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const = 0;
  virtual void emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const = 0;
  virtual void emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const = 0;
  virtual Zero* as_zero(void) const = 0;
  virtual Xstat* as_xstat(void) const = 0;
  virtual void find_allocations(Warp *warp, QSSASharedMemoryStore *store) const = 0;
  virtual void emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store, Warp *warp) const = 0;
  virtual void emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store) const = 0;
  virtual void emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store, 
                                          Warp *warp) const = 0;
};

class Zero : public Statement {
public:
  Zero(int yp, int xp) : y(yp), x(xp) { }
public:
  int y, x;
public:
  virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const;
  virtual void emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual void emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual Zero* as_zero(void) const { return const_cast<Zero*>(this); }
  virtual Xstat* as_xstat(void) const { return NULL; }
  virtual void find_allocations(Warp *warp, QSSASharedMemoryStore *store) const;
  virtual void emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store, Warp *warp) const;
  virtual void emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store) const;
  virtual void emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store, 
                                          Warp *warp) const;
};

class Den : public Statement {
public:
  Den(void) { }
public:
  void add_pair(int y, int x) { pairs.push_back(std::pair<int,int>(y,x)); }
public:
  virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const;
  virtual void emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual void emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual Zero* as_zero(void) const { return NULL; }
  virtual Xstat* as_xstat(void) const { return NULL; }
  virtual void find_allocations(Warp *warp, QSSASharedMemoryStore *store) const;
  virtual void emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store, Warp *warp) const;
  virtual void emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store) const;
  virtual void emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store, 
                                          Warp *warp) const;
public:
  std::vector<std::pair<int,int> > pairs;
};

class Multiply : public Statement {
public:
  Multiply(int zp, int yp, int xp) : z(zp), y(yp), x(xp) { }
public:
  virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const;
  virtual void emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual void emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual Zero* as_zero(void) const { return NULL; }
  virtual Xstat* as_xstat(void) const { return NULL; }
  virtual void find_allocations(Warp *warp, QSSASharedMemoryStore *store) const;
  virtual void emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store, Warp *warp) const;
  virtual void emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store) const;
  virtual void emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store, 
                                          Warp *warp) const;
public:
  int z, y, x;
};

class Add : public Statement {
public:
  Add(int zp, int yp, int xp) : z(zp), y(yp), x(xp) { }
public:
  virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const;
  virtual void emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual void emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual Zero* as_zero(void) const { return NULL; }
  virtual Xstat* as_xstat(void) const { return NULL; }
  virtual void find_allocations(Warp *warp, QSSASharedMemoryStore *store) const;
  virtual void emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store, Warp *warp) const;
  virtual void emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store) const;
  virtual void emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store, 
                                          Warp *warp) const;
public:
  int z, y, x;
};

class Divide : public Statement {
public:
  Divide(int zp, int yp) : z(zp), y(yp) { }
public:
  virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const;
  virtual void emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual void emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual Zero* as_zero(void) const { return NULL; }
  virtual Xstat* as_xstat(void) const { return NULL; }
  virtual void find_allocations(Warp *warp, QSSASharedMemoryStore *store) const;
  virtual void emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store, Warp *warp) const;
  virtual void emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store) const;
  virtual void emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store, 
                                          Warp *warp) const;
public:
  int z, y;
};

class Xstat : public Statement {
public:
  Xstat(int xp) : x(xp) { }
  public:
  virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const;
  virtual void emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual void emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const;
  virtual Zero* as_zero(void) const { return NULL; }
  virtual Xstat* as_xstat(void) const { return const_cast<Xstat*>(this); }
  virtual void find_allocations(Warp *warp, QSSASharedMemoryStore *store) const;
  virtual void emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store, Warp *warp) const;
  virtual void emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                    QSSASharedMemoryStore &store) const;
  virtual void emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store, 
                                          Warp *warp) const;
public:
  void add_pair(int xp, int y) { assert(x == xp); assert(y_vals.find(y) == y_vals.end()); y_vals.insert(y);  }
public:
  int x;
  std::set<int> y_vals;
};

class Stif : public Operation {
public:
  Stif(const char *name, int k, int k2, TranslationUnit *unit);
public:
  void add_forward_d(Reaction *reac, int coeff);
  void add_forward_c(Reaction *reac, int coeff);
  void add_backward_d(Reaction *reac, int coeff);
  void add_backward_c(Reaction *reac, int coeff);
public:
  virtual const char* get_name(void) const { return "Stiff Species"; }
  virtual bool is_stif(void) const { return true; }
public:
  void remove_duplicate_reactions(void);
  void emit_statements(CodeOutStream &ost, TranslationUnit *unit, Profiler *profiler); 
  void emit_sse_statements(CodeOutStream &ost, TranslationUnit *unit, int iters);
  void emit_avx_statements(CodeOutStream &ost, TranslationUnit *unit, int iters);
public:
  Species *species;
public:
  const char *const name;
  const int k;
  const int k2;
  TranslationUnit *unit;
  // Creation and deletion
  std::map<Reaction*,int/*coeff*/> forward_d;
  std::map<Reaction*,int/*coeff*/> forward_c;
  std::map<Reaction*,int/*coeff*/> backward_d;
  std::map<Reaction*,int/*coeff*/> backward_c;
};

class Warp {
public:
  Warp (int id);
public:
  void add_reaction(Reaction *reac);
  void add_stif(Stif *stif);
  void add_output(Species *spec);
  void add_qss(QSS* qss);
  int add_index(int index, int version);
  int add_real(double real, int version);
  bool has_species(int qss_idx) const;
public:
  const int wid;
  std::vector<Reaction*> reacs;
  std::vector<Stif*> stifs;
  std::vector<Species*> specs;
  std::vector<QSS*> qss_specs;
  std::set<std::pair<int,int> > warp_emitted;
  std::vector<std::vector<int> > int_constants;
  std::vector<std::vector<double> > real_constants;
};

class BankAnalyzer {
public:
  BankAnalyzer(int bank, int max_reacs);
public:
  void add_forward_reac(Reaction *reac, int loc);
  void add_reverse_reac(Reaction *reac, int loc);
  int find_forward_location(Reaction *reac) const;
  int find_reverse_location(Reaction *reac) const;
  // Take in a list of locations and assign them in order
  // to a number of warps equal to the number of banks
  void perform_assignment(const std::vector<int> &locations,
          std::vector<std::vector<int> > &assignment) const;
public:
  // Number of effective banks
  const int banks;
  std::map<Reaction*,int/*loc*/> forward_banks;
  std::map<Reaction*,int/*loc*/> reverse_banks;
  std::vector<Reaction*> locations;
  std::vector<bool> is_forward;
};

class MultiAnalyzer {
public:
  MultiAnalyzer(int bank, int passes, int locations);
public:
  void add_forward_reac(Reaction *reac, int pass, int loc);
  void add_reverse_reac(Reaction *reac, int pass, int loc);
  int find_forward_location(Reaction *reac, int pass) const;
  int find_reverse_location(Reaction *reac, int pass) const;
public:
  const int banks;
  const int passes;
  std::vector<std::map<Reaction*,int/*loc*/> > forward_banks;
  std::vector<std::map<Reaction*,int/*loc*/> > reverse_banks;
  std::vector<std::vector<Reaction*> > locations;
  std::vector<std::vector<bool> > is_forward;
  void perform_assignment(int pass, const std::vector<int> &locations,
          std::vector<std::vector<int> > &assignment) const;
};

class QSSASharedMemoryStore {
public:
  QSSASharedMemoryStore(int max_locations, int qss_values, int offset);
public:
  void analyze_warp(Warp *warp);
  bool has_value(std::pair<int,int> key) const;
  int find_value(std::pair<int,int> key) const;
  void allocate_value(std::pair<int,int> key);
public:
  int global_offset;
  int next_loc;
  std::vector<std::pair<int,int> > allocations;
  std::map<std::pair<int,int>,int> locations;
};

#endif // __S3DGEN__
