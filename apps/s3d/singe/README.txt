
Singe: A DSL compiler for S3D chemistry and transport computations

Singe is a DSL compiler that generates the transport and chemistry kernels
for use in the S3D combustion simulation application.  There are four 
transport kernels and one chemistry kernel that need to be generated for
each chemical mechanism:

 - Conductivity
 - Viscosity
 - Diffusion
 - Thermal
 - Chemistry

To generate these kernels Singe requires three or four input files
depending on whether QSSA and stiffness analysis has been run for
the corresponding mechanism:

 - chem.inp: chemistry input file
 - tran.dat: transport input file
 - therm.dat: thermal data input file
 - qssa_stif.txt: QSSA and stiffness from 
    Professor Lu's MATLAB script (optional and only need for chemistry)

Singe is capable of generating both low-performance CPU kernels
for correctness testing, high-performance vectorized CPU kernels, 
and high-performance GPU kernels for both Kepler and Fermi GPU 
architectures.

Singe supports the following input options:

 --dir/-d: the input directory (required)

 --dim/-n: produce dimensionalized code

 --cuda: emit code for the GPU

 --nophy: do not generate physics code 

 --nochem: no not generate chemistry code

 --k20: generate GPU code for Kepler K20 arhitecture

 --warp <16,32>: specify the effective warp size (for physics only)

 --visc: specify the number of warps for warp-specialized viscosity

 --diff: specify the number of warps for warp-specialized diffusion

 --reac: specify the number of warps for warp-specialized chemistry

 --multi: generate multi-pass warp-specialized GPU code

 --qssa: specify the number of warps for QSSA multi-pass code

 --ctas: specify the target number of CTAs per SM

 --vector: specify the target vector instruction set

 --cache: specify the cache size to block for in KB

Some examples of Singe usage:

- Emit basic CPU kernels for each mechanism
   ./singe --dir inputs/h2/
   ./singe --dir inputs/DME/
   ./singe --dir inputs/nC7H16/

- Emit vectorized SSE and AVX code for the DME mechanism blocked for 32KB caches
   ./singe --dir inputs/DME/ --vector sse --cache 32
   ./singe --dir inputs/DME/ --vector avx --cache 32

- Emit data parallel CUDA code for the H2 mechanism on Fermi and Kepler respectively
   ./singe --dir inputs/h2/ --cuda
   ./singe --dir inputs/h2/ --cuda --k20

- Emit warp-specialized transport code for the heptane mechanism (parameters determined by autotuning)
   ./singe --dir inputs/nC7H16/ --nochem --cuda --visc 13 --diff 13 --warp 16 --k20 

- Emit warp-specialized chemistry code for the heptane mechanism (parameters determined by autotuning)
   ./singe --dir inputs/nC7H16/ --nophy --cuda --reac 20 --qssa 2 --qshared --ctas 1
   ./singe --dir inputs/nC7H16/ --nophy --cuda --reac 16 --qssa 2 --skip 2 --ctas 1 --k20

