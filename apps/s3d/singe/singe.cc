
#include <cmath>
#include <cstring>
#include <cassert>
#include <cstdlib>

#include "globals.h"
#include "singe.h"

static inline char* capitalize(char *name)
{
  int length = strlen(name);
  for (int i = 0; i < length; i++)
  {
    if ((name[i] >= 'a') && (name[i] <= 'z'))
      name[i] = char(int(name[i])-32);
  }
  return name;
}

/////////////////////////////////////////////
//  Translation Unit
/////////////////////////////////////////////

TranslationUnit::TranslationUnit(void)
  : use_dim(true), has_thermos(false), needs_dln10(false),
    k20(false), warp_size(32), viscosity_warps(0),
    diffusion_warps(0), qssa_spill_size(0), tran(NULL), chem(NULL), 
    pressure_dep(false), pressure_spec(NULL), current_reac(NULL),
    conductivity_profiler(NULL), diffusion_profiler(NULL),
    viscosity_profiler(NULL), chemistry_profiler(NULL)
{
  assert(species.empty());
  species.insert(new Species(this, "M", 0));
}

TranslationUnit::~TranslationUnit(void)
{
  // Free up all our memory
  for (ElementSet::iterator it = elements.begin();
        it != elements.end(); it++)
  {
    delete *it;
  }
  for (SpeciesSet::iterator it = species.begin();
        it != species.end(); it++)
  {
    delete *it;
  }
  for (ReactantVec::iterator it = all_reactants.begin();
        it != all_reactants.end(); it++)
  {
    delete *it;
  }
  for (ReactionVec::iterator it = all_reactions.begin();
        it != all_reactions.end(); it++)
  {
    delete *it;
  }
}

Element* TranslationUnit::add_element(const char *name)
{
  // Make sure it doesn't exist already
  for (ElementSet::const_iterator it = elements.begin();
        it != elements.end(); it++)
  {
    assert(strcmp((*it)->name, name) != 0);
  }
  unsigned idx = elements.size();
  Element *result = new Element(name, idx);
  elements.insert(result);
  return result;
}

Species* TranslationUnit::add_species(const char *name)
{
  char *sanitized = strdup(name);
  capitalize(sanitized);
  for (SpeciesSet::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    assert(strcmp((*it)->name, sanitized) != 0);
  }
  free(sanitized);
  unsigned idx = species.size();
  Species *result = new Species(this, name, idx);
  species.insert(result);
  return result;
}

Species* TranslationUnit::find_or_add_species(const char *name)
{
  char *sanitized = strdup(name);
  capitalize(sanitized);
  for (SpeciesSet::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    if (strcmp((*it)->name, sanitized) == 0)
      return *it;
  }
  fprintf(stderr,"WARNING: Species \"%s\" was not declared!\n", name);
  free(sanitized);
  return add_species(name);
}

Species* TranslationUnit::find_species(const char *name, bool must)
{
  char *sanitized = strdup(name);
  capitalize(sanitized);
  for (SpeciesSet::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    if (strcmp((*it)->name, sanitized) == 0)
      return *it;
  }
  free(sanitized);
  if (must)
    assert(false);
  return NULL;
}

Reactant* TranslationUnit::add_reactant(Species *spec, int coeff)
{
  assert(spec != NULL);
  assert(coeff > 0);
  Reactant *result = new Reactant(spec, coeff);  
  all_reactants.push_back(result);
  return result;
}

Reaction* TranslationUnit::add_reaction(ReactantVec *lhs, bool reversible, ReactantVec *rhs, double a, double beta, double e)
{
  unsigned idx = all_reactions.size();
  Reaction *result = new Reaction(idx, this, lhs, reversible, rhs, a, beta, e, pressure_dep, pressure_spec); 
  // Clear the pressure species
  pressure_dep = false;
  pressure_spec = NULL;
  all_reactions.push_back(result);
  // Set this reaction as the current reaction
  current_reac = result;
  return result;
}

ThirdBody* TranslationUnit::add_third_body(void)
{
  unsigned idx = third_bodies.size(); 
  ThirdBody *result = new ThirdBody(idx);
  third_bodies.push_back(result);
  return result;
}

ThirdBody* TranslationUnit::find_or_add_third_body(const std::map<Species*,double> &coeffs)
{
  for (ThirdBodyVec::const_iterator it = third_bodies.begin();
        it != third_bodies.end(); it++)
  {
    if ((*it)->matches(coeffs))
      return *it;
  }
  ThirdBody *result = add_third_body();
  result->initialize(coeffs);
  return result;
}

void TranslationUnit::set_pressure_species(const char *spec_name)
{
  pressure_dep = true;
  pressure_spec = find_species(spec_name);
}

void TranslationUnit::add_thb(Species *spec, double coeff)
{
  assert(current_reac != NULL);
  current_reac->add_thb(spec, coeff);
}

void TranslationUnit::add_duplicate(void)
{
  assert(current_reac != NULL);
  current_reac->set_duplicate();
}

void TranslationUnit::add_low(double a, double beta, double e)
{
  assert(current_reac != NULL);
  current_reac->add_low(a, beta, e);
}

void TranslationUnit::add_troe3(double a, double t3, double t1)
{
  assert(current_reac != NULL);
  current_reac->add_troe3(a, t3, t1);
}

void TranslationUnit::add_troe4(double a, double t3, double t1, double t2)
{
  assert(current_reac != NULL);
  current_reac->add_troe4(a, t3, t1, t2);
}

void TranslationUnit::add_sri3(double a, double b, double c)
{
  assert(current_reac != NULL);
  current_reac->add_sri3(a, b, c);
}

void TranslationUnit::add_sri5(double a, double b, double c, double d, double e)
{
  assert(current_reac != NULL);
  current_reac->add_sri5(a, b, c, d, e);
}

void TranslationUnit::add_hv(double wavelength)
{
  assert(current_reac != NULL);
  current_reac->add_hv(wavelength);
}

void TranslationUnit::add_rev(double a, double beta, double e)
{
  assert(current_reac != NULL);
  current_reac->add_rev(a, beta, e);
}

void TranslationUnit::add_lt(double b, double c)
{
  assert(current_reac != NULL);
  current_reac->add_lt(b, c);
}

void TranslationUnit::add_rlt(double b, double c)
{
  assert(current_reac != NULL);
  current_reac->add_rlt(b, c);
}

void TranslationUnit::check_species_coefficients(void) const
{
  for (SpeciesSet::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    // Don't need to worry about finding coefficients for M
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if (!(*it)->coefficients_set())
    {
      fprintf(stderr,"Failure to find thermodynamic coefficients for species %s in therm.dat\n", (*it)->name);
      exit(1);
    }
  }
}

void TranslationUnit::check_species_masses(void) const
{
  for (SpeciesSet::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if (!(*it)->is_mass_set())
    {
      fprintf(stderr,"Failure to compute atomic mass for species %s in therm.dat\n", (*it)->name);
      exit(1);
    }
  }
}

void TranslationUnit::post_parse(void)
{
  // First go through and build all the three body elements
  // This is effectively a common sub-expression elimination problem so that
  // reactions that share the same three-body elements can use the same
  // values that are computed for the three-body part of the reaction.
  //
  // First see if we need to create a "special" three body element, as defined by cgetrates
  {
    bool need_default_thb = false;
    for (ReactionVec::const_iterator it = all_reactions.begin();
          it != all_reactions.end(); it++)
    {
      if ((*it)->need_default_third_body())
      {
        need_default_thb = true;
        break;
      }
    }
    if (need_default_thb)
    {
      // This should be the first third body element if we add it
      assert(third_bodies.empty());
      add_third_body();
      assert(!third_bodies.empty());
    }
  }
  // Now for each of the reactions, go through and find the third body or add it
  for (ReactionVec::const_iterator it = all_reactions.begin();
        it != all_reactions.end(); it++)
  {
    (*it)->find_third_body(); 
  }

  // Now that we've handled doing CSE for third-body objects handle all the duplicates

  // First go through all the reactions and check to see if they are both marked
  // duplicates and reversible, if they are, then mark them as no longer duplicates
  for (ReactionVec::const_iterator it = all_reactions.begin();
        it != all_reactions.end(); it++)
  {
    if ((*it)->is_duplicate())
      (*it)->check_duplicate_and_reverse();
  }

  // Now go through all the reactions and build the duplicate lists of the 
  // reactions which are actually duplicates.  We'll only do this when
  // we don't have a chem file already.
  if (chem == NULL)
  {
    for (ReactionVec::const_iterator it = all_reactions.begin();
          it != all_reactions.end(); it++)
    {
      // Check to see if it is a duplicate which hasn't already
      // been claimed as being owned by someone else
      if ((*it)->is_duplicate() && !(*it)->is_owned())
      {
        ReactionVec::const_iterator rit = it;
        // Start from the next reaction in the list
        rit++;
        for ( ; rit != all_reactions.end(); rit++)
        {
          if ((*it)->matches(*rit))
          {
            (*it)->add_to_duplicates(*rit);
            (*rit)->set_owner(*it);
          }
        }
      }
    }
  }

  // Find and mark any QSSA species
  if (chem != NULL)
  {
    for (SpeciesSet::const_iterator it = species.begin();
          it != species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      // Otherwise see if it is a QSS species
      bool qss_spec = false;
      (*it)->qss_species = false;
      for (std::vector<ConnectedComponent*>::const_iterator cc = chem->connected_components.begin();
            cc != chem->connected_components.end(); cc++)
      {
        for (std::map<int,QSS*>::const_iterator sp = (*cc)->species.begin();
              sp != (*cc)->species.end(); sp++)
        {
          assert(sp->first == sp->second->qss_idx);
          if (int((*it)->idx) == sp->second->species_idx)
          {
            (*it)->qss_species = true;
            qss_spec = true;
            break;
          }
        }
        if (qss_spec)
          break;
      }
      // Also check to see if this is a stif species
      for (std::vector<Stif*>::const_iterator st = chem->stif_operations.begin();
            st != chem->stif_operations.end(); st++)
      {
        if ((*st)->k == int((*it)->idx))
        {
          (*st)->species = *it;
          break;
        }
      }
    }
    // Remove any duplicate reactions from the data structures
    chem->remove_duplicate_reactions();
  }
  
  // Make an ordered list of species of all the species
  // that are not the third body species and are not QSSA species
  ordered_species.resize(species.size());
  for (unsigned int i = 0; i < ordered_species.size(); i++)
    ordered_species[i] = NULL;
  for (SpeciesSet::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    assert(ordered_species[(*it)->idx] == NULL);
    ordered_species[(*it)->idx] = *it;
  }
  for (unsigned int i = 0; i < ordered_species.size(); i++)
    assert(ordered_species[i] != NULL);
  // Now remove any species that are the third body species
  // or are a QSSA species
  std::vector<Species*> filter;
  for (unsigned int i = 0; i < ordered_species.size(); i++)
  {
    if (strcmp(ordered_species[i]->name,"M") == 0)
      continue;
    if (ordered_species[i]->qss_species)
      continue;
    filter.push_back(ordered_species[i]);
  }
  ordered_species = filter;

  // Figure out which species are need to have their gibbs values computed
  {
    std::set<Species*> gibbs;
    for (unsigned idx = 0; idx < all_reactions.size(); idx++)
    {
      all_reactions[idx]->find_gibbs_species(gibbs);
    }
    for (std::set<Species*>::const_iterator it = gibbs.begin();
          it != gibbs.end(); it++)
    {
      // It better not be a third-body species
      assert(strcmp((*it)->name,"M") != 0);
      gibbs_species.push_back(*it);
    }
  }

  // Find the troe reactions
  for (unsigned idx = 0; idx < all_reactions.size(); idx++)
  {
    Reaction *reac = all_reactions[idx];
    if (reac->low.enabled && (reac->troe.num > 0))
      troe_reactions.push_back(reac);
  }
    
  // See if we need the dln10 parameter
  for (unsigned idx = 0; idx < all_reactions.size(); idx++)
  {
    if (all_reactions[idx]->needs_dln10())
    {
      needs_dln10 = true;
      break;
    }
  }

  // Tell the species how many actual reactions there are
  for (unsigned idx = 0; idx < all_reactions.size(); idx++)
  {
    Reaction *reac = all_reactions[idx];
    if (reac->is_duplicate() && reac->is_owned())
      continue;
    reac->notify_species(act_reactions.size());
    act_reactions.push_back(reac);
  }
}

void TranslationUnit::non_dimensionalize(void)
{
  use_dim = false;
#if 0
  fprintf(stdout,"  Non-dimensionalizing parameters:\n");
  fprintf(stdout,"  reference temperature: %f\n", TEMPERATURE_REF);
  fprintf(stdout,"  reference length: %f\n", LENGTH_REF);
  fprintf(stdout,"  reference pressure %f\n", PRESSURE_REF);
  fprintf(stdout,"  reference reaction rate: %f\n", REACTION_RATE_REF);
  fprintf(stdout,"  reference concentration: %f\n", CONCENTRATION_REF);
  fprintf(stdout,"  reference molar: %f\n", MOLAR_REF); 
  fprintf(stdout,"  reference time: %f\n", TIME_REF);
  for (ReactionVec::const_iterator it = all_reactions.begin();
        it != all_reactions.end(); it++)
  {
    (*it)->non_dimensionalize();
  }
#endif
}

// TranslationUnit::emit_code in code_gen.cc

void TranslationUnit::print_summary(void) const
{
  fprintf(stdout,"  Elements: %ld\n", elements.size());
  assert(!species.empty());
  unsigned num_qssa = 0;
  for (std::set<Species*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    if ((*it)->qss_species)
      num_qssa++;
  }
  fprintf(stdout,"  Total Species: %ld\n", species.size()-1); // ignore the "M" species
  fprintf(stdout,"    QSSA Species: %d\n", num_qssa);
  fprintf(stdout,"    Norm Species: %ld\n", species.size() - (1+num_qssa));
  fprintf(stdout,"    Gibbs Species: %ld\n", gibbs_species.size());
  fprintf(stdout,"  Reactions: %ld\n", all_reactions.size());
  fprintf(stdout,"    Unique Reactions: %ld\n", act_reactions.size());
  unsigned num_weird = 0;
  for (unsigned idx = 0; idx < act_reactions.size(); idx++)
  {
    if (act_reactions[idx]->is_weird_reaction())
      num_weird++;
  }
  fprintf(stdout,"    Normal Reactions: %ld\n", act_reactions.size()-num_weird);
  fprintf(stdout,"    Weird Reactions: %d\n", num_weird);
}

void TranslationUnit::emit_physics_profiling(void)
{
  assert(conductivity_profiler != NULL);
  assert(diffusion_profiler != NULL);
  assert(viscosity_profiler != NULL);
  delete conductivity_profiler;
  conductivity_profiler = NULL;
  delete diffusion_profiler;
  diffusion_profiler = NULL;
  delete viscosity_profiler;
  viscosity_profiler = NULL;
}

void TranslationUnit::emit_chemistry_profiling(void)
{
  assert(chemistry_profiler != NULL);
  delete chemistry_profiler;
  chemistry_profiler = NULL;
}

/////////////////////////////////////////////
//  Element 
/////////////////////////////////////////////

Element::Element(const char *n, unsigned id)
  : idx(id), name(n)
{
  assert(name != NULL);
}

/////////////////////////////////////////////
//  Species 
/////////////////////////////////////////////

Species::Species(TranslationUnit *u, const char *n, unsigned id)
  : unit(u), idx(id), name(capitalize(strdup(n))), qss_species(false),
    thermo_coefficients(NULL), mass_set(false), molecular_mass(0.0)
{
  assert(name != NULL);
  size_t length = strlen(name);
  code_name = (char*)malloc((length+8)*sizeof(char));
  sprintf(code_name,"NC_%s",name);
  length = strlen(code_name);
  for (unsigned idx = 0; idx < length; idx++)
  {
    char c = code_name[idx];
    if ((c >= 'a') && (c <= 'z')) 
      code_name[idx] = c - ('a' - 'A');
    if ((c == '(') || (c == ')'))
      code_name[idx] = '_';
    if (c == '-')
      code_name[idx] = '_';
    if (c == ',')
      code_name[idx] = '_';
    if (c == '*')
      code_name[idx] = '_';
  }
}

Species::~Species(void)
{
  free(code_name);
  code_name = NULL;
}

void Species::notify(Reaction *reac, unsigned reac_idx, int coeff)
{
  assert(reac->reaction_idx == int(reac_idx));
  assert(reaction_contributions.find(reac) == reaction_contributions.end());
  reaction_contributions[reac] = coeff;
}

void Species::set_coefficients(double low, double high, double common, double *coeffs)
{
  if (thermo_coefficients == NULL)
  {
    low_temp = low;
    high_temp = high;
    common_temp = common;
    thermo_coefficients = coeffs;
  }
  else
  {
    bool diff = false;
    for (int i = 0; i < 14; i++)
    {
      if (thermo_coefficients[i] != coeffs[i])
      {
        diff = true;
        break;
      }
    }
    if (diff)
    {
      fprintf(stderr,"Warning: overwiting thermo coefficients for %s\n", name); 
      free(thermo_coefficients);
      thermo_coefficients = coeffs;
    }
    else
    {
      free(coeffs);
    }
  }
#if 0
  printf("Species %s\n", name);
  printf("  Low Temp: %lf\n", low_temp);
  printf("  High Temp: %lf\n", high_temp);
  printf("  Common Temp: %lf\n", common_temp);
  for (int i = 0; i < 14; i++)
    printf("    %lf\n", thermo_coefficients[i]);
#endif
}

void Species::update_mass(const char *element_name, int element_count)
{
  size_t length = strlen(element_name);
  unsigned idx = 0;
  // Read the letter
  char c = element_name[idx++];
  // Convert any lower case letters to upper case
  if ((c >= 'a') && (c <= 'z'))
    c = char(int(c)-32);
  // If it's a zero, then ignore it
  // Someone thinks this is valid input so screw it
  if (c == '0')
    return;
  double atomic_mass = 0.0;
  switch (c)
  {
    case 'A':
      {
        // Better be argon
        assert((idx < length) && ((name[idx] == 'r') || (name[idx] == 'R')));
        idx++;
        //atomic_mass = 39.9481;
        atomic_mass = 39.94800;
        break;
      }
    case 'H':
      {
        // Peek ahead to see if this is helium
        if ((idx < length) && ((name[idx] == 'e') || (name[idx] == 'E')))
        {
          // Helium case
          idx++;
          //atomic_mass = 4.002602;
          atomic_mass = 4.00260;
        }
        else
        {
          // Hydrogen case
          //atomic_mass = 1.007947; 
          atomic_mass = 1.00797;
        }
        break;
      }
    case 'C':
      {
        //atomic_mass = 12.01078;
        atomic_mass = 12.01115;
        break;
      }
    case 'O':
      {
        //atomic_mass = 15.99943;
        atomic_mass = 15.99940;
        break;
      }
    case 'N':
      {
        //atomic_mass = 14.00672;
        atomic_mass = 14.00670;
        break;
      }
    default:
      fprintf(stderr,"Unsupported chemical character in species %s starting at %s\n", name, &(name[idx-1]));
      exit(1);
  }
  // Update the molecular mass
  molecular_mass += (float(element_count) * atomic_mass);
  mass_set = true;
}

bool Species::coefficients_set(void) const
{
  return (thermo_coefficients != NULL);
}

double Species::get_common_temperature(void) const
{
  if (unit->use_dim)
    return common_temp;
  else
    return (common_temp/TEMPERATURE_REF);
}

double Species::get_high_coefficient(unsigned index, 
                                     bool orig /*= false*/) const
{
  assert(index < 7);
  // Set a scaling factor if we're doing non-dimensional code
  double scaling_factor = 1.0;
  if (!orig)
  {
    switch (index)
    {
      // do nothing cases
      case 0:
      case 5:
      case 6:
        break;
      case 1:
        scaling_factor = ((1.0/2.0) - 1.0);
        break;
      case 2:
        scaling_factor = ((1.0/3.0) - (1.0/2.0));
        break;
      case 3:
        scaling_factor = ((1.0/4.0) - (1.0/3.0));
        break;
      case 4:
        scaling_factor = ((1.0/5.0) - (1.0/4.0));
        break;
      default:
        assert(false);
    }
    if (!unit->use_dim)
    {
      switch (index)
      {
        // do nothing cases
        case 0:
        case 6:
          break;
        case 1:
          scaling_factor *= (TEMPERATURE_REF);
          break;
        case 2:
          scaling_factor *= (TEMPERATURE_REF*TEMPERATURE_REF);
          break;
        case 3:
          scaling_factor *= (TEMPERATURE_REF*TEMPERATURE_REF*TEMPERATURE_REF);
          break;
        case 4:
          scaling_factor *= (TEMPERATURE_REF*TEMPERATURE_REF*TEMPERATURE_REF*TEMPERATURE_REF);
          break;
        case 5:
          scaling_factor *= (1/(TEMPERATURE_REF));
          break;
        default:
          // should never get here
          assert(false);
      }
    }
  }
  return (scaling_factor * thermo_coefficients[index]);
}

double Species::get_low_coefficient(unsigned index,
                                    bool orig /*= false*/) const
{
  assert(index < 7);
  // Set a scaling factor if we're doing non-dimensional code
  double scaling_factor = 1.0;
  if (!orig)
  {
    switch (index)
    {
      // do nothing cases
      case 0:
      case 5:
      case 6:
        break;
      case 1:
        scaling_factor = ((1.0/2.0) - 1.0);
        break;
      case 2:
        scaling_factor = ((1.0/3.0) - (1.0/2.0));
        break;
      case 3:
        scaling_factor = ((1.0/4.0) - (1.0/3.0));
        break;
      case 4:
        scaling_factor = ((1.0/5.0) - (1.0/4.0));
        break;
      default:
        assert(false);
    }
    if (!unit->use_dim)
    {
      switch (index)
      {
        // do nothing cases
        case 0:
        case 6:
          break;
        case 1:
          scaling_factor *= TEMPERATURE_REF;
          break;
        case 2:
          scaling_factor *= (TEMPERATURE_REF*TEMPERATURE_REF);
          break;
        case 3:
          scaling_factor *= (TEMPERATURE_REF*TEMPERATURE_REF*TEMPERATURE_REF);
          break;
        case 4:
          scaling_factor *= (TEMPERATURE_REF*TEMPERATURE_REF*TEMPERATURE_REF*TEMPERATURE_REF);
          break;
        case 5:
          scaling_factor *= (1/(TEMPERATURE_REF));
          break;
        default:
          // should never get here
          assert(false);
      }
    }
  }
  return (scaling_factor * thermo_coefficients[7+index]); 
}

/////////////////////////////////////////////
//  Reactant 
/////////////////////////////////////////////

Reactant::Reactant(Species *s, int c)
  : spec(s), coeff(c)
{
  assert(spec != NULL);
}

/////////////////////////////////////////////
//  Reaction 
/////////////////////////////////////////////

Reaction::Reaction(unsigned id, TranslationUnit *u, ReactantVec *lhs, bool reverse, ReactantVec *rhs, 
                   double aprime, double betaprime, double eprime,
                   bool press_dep, Species *press_spec)
  : idx(id), reaction_idx(-1), unit(u), a(aprime), beta(betaprime), e(eprime), 
    third_body(false), reversible(reverse),
    pressure_dep(press_dep), pressure_species(press_spec), thb(NULL)
{
  assert(lhs != NULL);
  assert(rhs != NULL);
  assert(!pressure_dep || (pressure_species != NULL));
  Species *const third_body_spec = unit->find_species("M");
  // Dedup the lhs species
  for (ReactantVec::const_iterator it = lhs->begin();
        it != lhs->end(); it++)
  {
    if ((*it)->spec == third_body_spec)
    {
      third_body = true;
      continue;
    }
    // Otherwise see if it is already in the set of forward reactants
    SpeciesCoeffMap::iterator finder = forward.find((*it)->spec);
    if (finder == forward.end())
      forward[(*it)->spec] = (*it)->coeff;
    else
      finder->second += (*it)->coeff;
  }
  // We can delete lhs now (no need to delete the reactants in this list
  // since they are owned by the TranslationUnit)
  delete lhs;
  lhs = NULL;
  // Dedup the rhs species
  for (ReactantVec::const_iterator it = rhs->begin();
        it != rhs->end(); it++)
  {
    if ((*it)->spec == third_body_spec)
    {
      if (!third_body)
      {
        fprintf(stderr,"thirdy body found on LHS but not RHS\n");
        exit(1);
      }
      continue;
    }
    SpeciesCoeffMap::iterator finder = backward.find((*it)->spec);
    if (finder == backward.end())
      backward[(*it)->spec] = (*it)->coeff;
    else
      finder->second += (*it)->coeff;
  }
  // We can delete the rhs now too
  delete rhs;
  rhs = NULL;
  
  // Combine the forward and backward species to get the stoichometry 
  stoich = backward;
  for (SpeciesCoeffMap::const_iterator it = forward.begin();
        it != forward.end(); it++)
  {
    SpeciesCoeffMap::iterator finder = stoich.find(it->first);
    if (finder == stoich.end()) // doesn't already exist
      stoich[it->first] = - it->second; // negate the forward coeff
    else
      finder->second -= it->second;
  }
  // Remove any species that ended up going to zero
  {
    std::vector<Species*> to_delete;
    for (SpeciesCoeffMap::const_iterator it = stoich.begin();
          it != stoich.end(); it++)
    {
      if (it->second == 0)
        to_delete.push_back(it->first);
    }
    for (std::vector<Species*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      stoich.erase(*it);
    }
  }
  // Generate the species set
  for (SpeciesCoeffMap::const_iterator it = forward.begin();
        it != forward.end(); it++)
  {
    all_species.insert(it->first);
  }
  for (SpeciesCoeffMap::const_iterator it = backward.begin();
        it != backward.end(); it++)
  {
    all_species.insert(it->first);
  }
  // All the other stuff
  if (pressure_dep && (pressure_species == third_body_spec))
    third_body = true;
  duplicate.enabled = false;
  duplicate.owner = true;
  duplicate.ptr.duplicates = NULL;
  low.enabled = false;
  troe.num = 0;
  sri.num = 0;
  hv.enabled = false;
  rev.enabled = false;
  lt.enabled = false;
  rlt.enabled = false;
}

void Reaction::add_thb(Species *spec, double coeff)
{
  assert(thb_values.find(spec) == thb_values.end());
  // Show adjustment away from unity
  // (that's what the baseline implementation does)
  thb_values[spec] = coeff - 1.0;
}

void Reaction::set_duplicate(void)
{
  assert(!duplicate.enabled);
  duplicate.enabled = true;
}

void Reaction::add_low(double a, double beta, double e)
{
  low.enabled = true;
  low.a = a;
  low.beta = beta;
  low.e = e;
}

void Reaction::add_troe3(double a, double t3, double t1)
{
  troe.num = 3;
  troe.a = a;
  troe.t3 = t3;
  troe.t1 = t1;
}

void Reaction::add_troe4(double a, double t3, double t1, double t2)
{
  troe.num = 4;
  troe.a = a;
  troe.t3 = t3;
  troe.t1 = t1;
  troe.t2 = t2;
}

void Reaction::add_sri3(double a, double b, double c)
{
  sri.num = 3;
  sri.a = a;
  sri.b = b;
  sri.c = c;
}

void Reaction::add_sri5(double a, double b, double c, double d, double e)
{
  sri.num = 5;
  sri.a = a;
  sri.b = b;
  sri.c = c;
  sri.d = d;
  sri.e = e;
}

void Reaction::add_hv(double wavelength)
{
  hv.enabled = true;
  hv.wavelength = wavelength;
}

void Reaction::add_rev(double a, double beta, double e)
{
  rev.enabled = true;
  rev.a = a;
  rev.beta = beta;
  rev.e = e;
}

void Reaction::add_lt(double b, double c)
{
  lt.enabled = true;
  lt.b = b;
  lt.c = c;
}

void Reaction::add_rlt(double b, double c)
{
  rlt.enabled = true;
  rlt.b = b;
  rlt.c = c;
}

bool Reaction::need_default_third_body(void) const
{
  if (third_body && thb_values.empty())
    return true;
  return false;
}

void Reaction::find_third_body(void)
{
  if (third_body)
    thb = unit->find_or_add_third_body(thb_values);
}

bool Reaction::is_duplicate(void) const
{
  return duplicate.enabled;
}

void Reaction::check_duplicate_and_reverse(void)
{
  assert(duplicate.enabled);
  if (rev.enabled)
    duplicate.enabled = false;
}

bool Reaction::is_owned(void) const
{
  assert(duplicate.enabled);
  return (!duplicate.owner);
}

bool Reaction::matches(const Reaction *other) const
{
  if (stoich.size() != other->stoich.size())
    return false;
  if (forward.size() != other->forward.size())
    return false;
  if (backward.size() != other->backward.size())
    return false;
  for (SpeciesCoeffMap::const_iterator it = stoich.begin();
        it != stoich.end(); it++)
  {
    SpeciesCoeffMap::const_iterator finder = other->stoich.find(it->first);
    if (finder == other->stoich.end())
      return false;
    if (it->second != finder->second)
      return false;
  }
  for (SpeciesCoeffMap::const_iterator it = forward.begin();
        it != forward.end(); it++)
  {
    SpeciesCoeffMap::const_iterator finder = other->forward.find(it->first);
    if (finder == other->forward.end())
      return false;
    if (it->second != finder->second)
      return false;
  }
  for (SpeciesCoeffMap::const_iterator it = backward.begin();
        it != backward.end(); it++)
  {
    SpeciesCoeffMap::const_iterator finder = other->backward.find(it->first);
    if (finder == other->backward.end())
      return false;
    if (it->second != finder->second)
      return false;
  }
  return true;
}

void Reaction::add_to_duplicates(Reaction *other)
{
  assert(duplicate.owner); 
  if (duplicate.ptr.duplicates == NULL)
    duplicate.ptr.duplicates = new std::list<Reaction*>();
  duplicate.ptr.duplicates->push_back(other);
}

void Reaction::set_owner(Reaction *owner)
{
  assert(duplicate.ptr.owner == NULL);
  duplicate.owner = false;
  duplicate.ptr.owner = owner;
}


/*
 * A (pre-exponential coefficient) has to soak up a lot:
 *   Sum[abs(nu_i), i in reactants] conc_ref units  (conc factors)
 *   beta temp_ref units (T^beta factor)
 *   Reaction rate reference
 * And it is supplied in g-mole/cm^3 s units, not MKS which is used
 * everywhere else here, hence the unit changes in the lines below.
 *   0.  (input units)  g-mol/(cm^3 sec) (1/K)^beta (1/(g-mol/cm^3))^nu
 *   1.  g-mol/(cm^3 sec) (1/(g-mol/cm^3))^nu
 *   2.  g-mol/(cm^3 sec)
 *   3.  kg-mol/(m^3 sec)
 *   4.  kg/(m^3 sec)
 *   5.  (unity)
 */
static double preexp_convert(double a, double beta, int order)
{
    /* printf("a  in = %9.2e\n", a); */
    a *= pow(TEMPERATURE_REF, beta);
    a *= pow(CONCENTRATION_REF * KGMOL__TO__GMOL * (1./M3__TO__CM3),
             (double)order);
    a *= GMOL__TO__KGMOL * (1./CM3__TO__M3);
    a *= MOLAR_REF;
    a /= REACTION_RATE_REF;
    /* printf("a out = %9.2e\n", a); */
    return a;
}


void Reaction::non_dimensionalize(void)
{
  int nu;
  // Comments brought over from original cgetrates for clarity
  /*
   * All preexponentials
   *
   * The presence of ``third-body'' participation affects the
   * pre-exponential constants only.  K_infinity (high pressure limit)
   * already has the correct units, as the third body is _not_ included
   * in the list of forward or backward reactants.  The high limit is
   * zeroth-order in third body.  The low pressure limit K_0 must be
   * multiplied by one inverse concentration unit so that the units of
   * k_0 [M] and k_infty are the same.  This is regardless of whether
   * the third body is composed of a single species or many, and
   * regardless (hence) of any acceleration factors.
   */
  nu = 0;
  for (SpeciesCoeffMap::const_iterator it = forward.begin();
        it != forward.end(); it++)
  {
    nu += it->second;
  }
  if (low.enabled)
  {
    a = preexp_convert(a, beta, nu);
    low.a = preexp_convert(low.a, low.beta, nu+1);
  }
  else if (thb != NULL)
    a = preexp_convert(a, beta, nu+1);
  else
    a = preexp_convert(a, beta, nu);
  nu = 0;
  for (SpeciesCoeffMap::const_iterator it = backward.begin();
        it != backward.end(); it++)
  {
    nu += it->second;
  }
  if (rev.enabled)
    rev.a = preexp_convert(rev.a, rev.beta, nu);

  /*
   * beta already non-dimensional
   */
  
  /*
   * E is in cal/g-mole, convert to J/kg-mole then divide by R (gas
   * constant) to get K (temperature units).  Finally divide by
   * the reference temperature to get unitless numbers.
   */
  e *= CAL_PER_GMOLE__TO__KELVIN / TEMPERATURE_REF;
  if (low.enabled)
    low.e *= CAL_PER_GMOLE__TO__KELVIN / TEMPERATURE_REF;
  if (rev.enabled)
    rev.e *= CAL_PER_GMOLE__TO__KELVIN / TEMPERATURE_REF;
  /*
   * Troe form falloff has three temperature paramaters which
   * must be nondimensionalized.
   */
  if (troe.num > 0)
  {
    troe.t3 /= TEMPERATURE_REF;
    troe.t1 /= TEMPERATURE_REF;
    if (troe.num == 4)
      troe.t2 /= TEMPERATURE_REF;
  }
  /*
   * SRI form falloff has temperature parameters too.  Soak the
   * dimension of T^e into the d parameter for 5-param SRI.
   */
  if (sri.num > 0)
  {
    sri.b /= TEMPERATURE_REF;
    sri.c /= TEMPERATURE_REF;
    if (sri.num == 5)
      sri.d *= pow(TEMPERATURE_REF, sri.e);
  }
  /*
   * Landau Teller B and C coefficients must be adjusted for
   * non-dimensional temperature as well.  Remaining LT parameters
   * taken care of above, when considered as Arrhenius.
   */
  if (lt.enabled)
  {
    lt.b *= pow(TEMPERATURE_REF, -1.0/3.0);
    lt.c *= pow(TEMPERATURE_REF, -2.0/3.0);
  }
}

void Reaction::notify_species(unsigned reac_idx)
{
  assert(reaction_idx == -1);
  assert(reac_idx >= 0);
  reaction_idx = reac_idx;
  for (SpeciesCoeffMap::const_iterator it = stoich.begin();
        it != stoich.end(); it++)
  {
    it->first->notify(this, reaction_idx, it->second);
  }
}

void Reaction::find_gibbs_species(std::set<Species*> &gibbs) const
{
  if (reversible && !rev.enabled)
  {
    for (SpeciesCoeffMap::const_iterator it = stoich.begin();
          it != stoich.end(); it++)
    {
      gibbs.insert(it->first);
    }
  }
}

bool Reaction::is_weird_reaction(void) const
{
  bool weird = low.enabled || lt.enabled;
  // Check the backward conditions for weirdness
  weird = weird || rlt.enabled;
  weird = weird || duplicate.enabled;
  return weird;
}

/////////////////////////////////////////////
//  Third Body 
/////////////////////////////////////////////

ThirdBody::ThirdBody(unsigned i)
  : idx(i) 
{ 
}

void ThirdBody::initialize(const std::map<Species*,double> &coeffs)
{
  components = coeffs;
}

bool ThirdBody::matches(const std::map<Species*,double> &coeffs) const
{
  if (components.size() != coeffs.size())
    return false;
  for (std::map<Species*,double>::const_iterator it = coeffs.begin();
        it != coeffs.end(); it++)
  {
    std::map<Species*,double>::const_iterator finder = components.find(it->first);
    if (finder == components.end())
      return false;
    if (finder->second != it->second)
      return false;
  }
  return true;
}

/////////////////////////////////////////////
//  Profiler 
/////////////////////////////////////////////

Profiler::Profiler(const char *n)
  : name(n), read_count(0), write_count(0), add_count(0), subtract_count(0),
    multiply_count(0), divide_count(0), exponent_count(0), logarithm_count(0), sqrt_count(0)
{
}

Profiler::Profiler(const Profiler &rhs)
  : name(NULL)
{
  // should never be called
  assert(false);
}

Profiler::~Profiler(void)
{
  fprintf(stdout,"Profiler Statistics for %s (per point)\n", name);
  if (read_count > 0)
    fprintf(stdout,"  Reads:      %10d\n", read_count);
  if (write_count > 0)
    fprintf(stdout,"  Writes:     %10d\n", write_count);
  if (add_count > 0)
    fprintf(stdout,"  Adds:       %10d\n", add_count);
  if (subtract_count > 0)
    fprintf(stdout,"  Subtracts:  %10d\n", subtract_count);
  if (multiply_count > 0)
    fprintf(stdout,"  Multiplies: %10d\n", multiply_count);
  if (divide_count > 0)
    fprintf(stdout,"  Divides:    %10d\n", divide_count);
  if (exponent_count > 0)
    fprintf(stdout,"  Exponents:  %10d\n", exponent_count);
  if (logarithm_count > 0)
    fprintf(stdout,"  Logarithms: %10d\n", logarithm_count);
  if (sqrt_count > 0)
    fprintf(stdout,"  Sqrts:      %10d\n", sqrt_count);
}

Profiler& Profiler::operator=(const Profiler &rhs)
{
  // should never be called
  assert(false);
  return *this;
}

/////////////////////////////////////////////
//  TranLib 
/////////////////////////////////////////////

TranLib::TranLib(int ord, int num_spec, int nli, double pat, TranslationUnit *u)
  : order(ord), num_species(num_spec), nlite(nli), patmos(pat), unit(u) { }

bool TranLib::is_diffusion_symmetric(void) const
{
  // Symmetric unless we find a counterexample
  bool result = true;
  for (int idx1 = 0; idx1 < num_species; idx1++)
  {
    for (int idx2 = 0; idx2 < num_species; idx2++)
    {
      for (int idx = 0; idx < order; idx++)
      {
        double one = ndif[idx + idx1*order + idx2*order*num_species];
        double two = ndif[idx + idx2*order + idx1*order*num_species];
        if (one != two)
          result = false;
      }
    }
  }
  return result;
}

/////////////////////////////////////////////
//  Chemistry Unit Stuff 
/////////////////////////////////////////////

ChemistryUnit::ChemistryUnit(TranslationUnit *u)
  : unit(u)
{

}

void ChemistryUnit::add_component(ConnectedComponent *qss)
{
  connected_components.push_back(qss); 
}

void ChemistryUnit::add_unimportant(int backward, Reaction *reac)
{
  assert((backward==0) || (backward==1));
  if (backward==0)
  {
    if (reac->is_duplicate() && !reac->is_owner())
      reac = reac->duplicate.ptr.owner;
    assert(forward_unimportant.find(reac) == forward_unimportant.end());
    forward_unimportant.insert(reac);
  }
  else
  {
    if (reac->is_duplicate() && !reac->is_owner())
      reac = reac->duplicate.ptr.owner;
    assert(backward_unimportant.find(reac) == backward_unimportant.end());
    backward_unimportant.insert(reac);
  }
}

void ChemistryUnit::add_forward_zero(Reaction *reac, int qss_idx, int coeff)
{
  get_qss(qss_idx)->add_forward_zero(reac, qss_idx, coeff);
}

void ChemistryUnit::add_backward_zero(Reaction *reac, int qss_idx, int coeff)
{
  get_qss(qss_idx)->add_backward_zero(reac, qss_idx, coeff);
}

void ChemistryUnit::add_forward_contribution(Reaction *reac, int qss_idx, int other_idx, int coeff)
{
  get_qss(qss_idx)->add_forward_contribution(reac, qss_idx, other_idx, coeff);
}

void ChemistryUnit::add_backward_contribution(Reaction *reac, int qss_idx, int other_idx, int coeff)
{
  get_qss(qss_idx)->add_backward_contribution(reac, qss_idx, other_idx, coeff);
}

void ChemistryUnit::add_forward_denom(int qss_idx, Reaction *reac)
{
  get_qss(qss_idx)->add_forward_denom(qss_idx, reac);
}

void ChemistryUnit::add_backward_denom(int qss_idx, Reaction *reac)
{
  get_qss(qss_idx)->add_backward_denom(qss_idx, reac);
}

void ChemistryUnit::add_forward_correction(Reaction *reac, int qss_idx)
{
  get_qss(qss_idx)->add_forward_correction(reac, qss_idx);
}

void ChemistryUnit::add_backward_correction(Reaction *reac, int qss_idx)
{
  get_qss(qss_idx)->add_backward_correction(reac, qss_idx);
}

void ChemistryUnit::add_stif(Stif *stif)
{
  stif_operations.push_back(stif); 
}

ConnectedComponent* ChemistryUnit::get_qss(int spec)
{
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    if ((*it)->has_species(spec))
      return *it;
  }
  assert(false);
  return NULL;
}

void ChemistryUnit::remove_duplicate_reactions(void)
{
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->remove_duplicate_reactions();
  }
  for (std::vector<Stif*>::const_iterator it = stif_operations.begin();
        it != stif_operations.end(); it++)
  {
    (*it)->remove_duplicate_reactions();
  }
}

/////////////////////////////
//
// ConnectedComponent 
//
/////////////////////////////

ConnectedComponent::ConnectedComponent(TranslationUnit *u, ConnectedComponent *o)
  : unit(u), current_den(NULL), current_x(NULL)  
{

}

void ConnectedComponent::add_forward_zero(Reaction *reac, int qss_idx, int coeff)
{
  add_fread(reac->idx);
  get_qss(qss_idx)->add_forward_zero(reac, coeff);
}

void ConnectedComponent::add_backward_zero(Reaction *reac, int qss_idx, int coeff)
{
  add_bread(reac->idx);
  get_qss(qss_idx)->add_backward_zero(reac, coeff);
}

void ConnectedComponent::add_forward_contribution(Reaction *reac, int qss_idx, int other_idx, int coeff)
{
  add_fread(reac->idx);
  get_qss(qss_idx)->add_forward_contribution(reac, other_idx, coeff);
}

void ConnectedComponent::add_backward_contribution(Reaction *reac, int qss_idx, int other_idx, int coeff)
{
  add_bread(reac->idx);
  get_qss(qss_idx)->add_backward_contribution(reac, other_idx, coeff);
}

void ConnectedComponent::add_forward_denom(int qss_idx, Reaction *reac)
{
  add_fread(reac->idx);
  get_qss(qss_idx)->add_forward_denom(reac);
}

void ConnectedComponent::add_backward_denom(int qss_idx, Reaction *reac)
{
  add_bread(reac->idx);
  get_qss(qss_idx)->add_backward_denom(reac);
}

void ConnectedComponent::add_forward_correction(Reaction *reac, int qss_idx)
{
  add_fread(reac->idx);
  add_fwrite(reac->idx);
  get_qss(qss_idx)->add_forward_correction(reac);
}

void ConnectedComponent::add_backward_correction(Reaction *reac, int qss_idx)
{
  add_bread(reac->idx);
  add_bwrite(reac->idx);
  get_qss(qss_idx)->add_backward_correction(reac);
}

void ConnectedComponent::add_zero_statement(int y, int x)
{
  statements.push_back(new Zero(y, x));
}

void ConnectedComponent::begin_den_statement(void)
{
  assert(current_den == NULL);
  current_den = new Den();
}

void ConnectedComponent::end_den_statement(void)
{
  assert(current_den != NULL);
  statements.push_back(current_den);
  current_den = NULL;
}

void ConnectedComponent::add_den_pair(int y, int x)
{
  assert(current_den != NULL);
  current_den->add_pair(y,x);
}

void ConnectedComponent::add_multiply_statement(int z, int y, int x)
{
  statements.push_back(new Multiply(z,y,x));
}

void ConnectedComponent::add_add_statement(int z, int y, int x)
{
  statements.push_back(new Add(z,y,x));
}

void ConnectedComponent::add_divide_statement(int z, int y)
{
  statements.push_back(new Divide(z,y));
}

void ConnectedComponent::start_x_statement(int x)
{
  assert(current_x == NULL);
  current_x = new Xstat(x);
}

void ConnectedComponent::finish_x_statement(void)
{
  assert(current_x != NULL);
  statements.push_back(current_x);
  current_x = NULL;
}

void ConnectedComponent::add_x_pair(int x, int y)
{
  assert(current_x != NULL);
  current_x->add_pair(x,y);
}

void ConnectedComponent::add_species(int id, int spec)
{
  assert(species.find(id) == species.end());
  species[id] = new QSS(id, spec, unit);
}

bool ConnectedComponent::has_species(int spec)
{
  return (species.find(spec) != species.end());
}

QSS* ConnectedComponent::get_qss(int spec)
{
  assert(species.find(spec) != species.end());
  return species[spec];
}

void ConnectedComponent::remove_duplicate_reactions(void)
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->remove_duplicate_reactions();
  }
}

/////////////////////////////
//
// QSS
//
/////////////////////////////

QSS::QSS(int id, int spec, TranslationUnit *u)
  : qss_idx(id), species_idx(spec), unit(u)
{

}

int QSS::get_total_values(void) const
{
  std::set<int> others;
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  return (others.size()+1);
}

void QSS::add_forward_zero(Reaction *reac, int coeff)
{
  assert(coeff != 0);
  assert(forward_zeros.find(reac) == forward_zeros.end());
  forward_zeros[reac] = coeff;
}

void QSS::add_backward_zero(Reaction *reac, int coeff)
{
  assert(coeff != 0);
  assert(backward_zeros.find(reac) == backward_zeros.end());
  backward_zeros[reac] = coeff;
}

void QSS::add_forward_contribution(Reaction *reac, int other_idx, int coeff)
{
  assert(coeff != 0);
  std::pair<int,Reaction*> key(other_idx,reac);
  assert(forward_contributions.find(key) == forward_contributions.end());
  forward_contributions[key] = coeff;
  member_species.insert(other_idx);
}

void QSS::add_backward_contribution(Reaction *reac, int other_idx, int coeff)
{
  assert(coeff != 0);
  std::pair<int,Reaction*> key(other_idx,reac);
  assert(backward_contributions.find(key) == backward_contributions.end());
  backward_contributions[key] = coeff;
  member_species.insert(other_idx);
}

void QSS::add_forward_denom(Reaction *reac)
{
  assert(forward_denom.find(reac) == forward_denom.end());
  forward_denom.insert(reac);
}

void QSS::add_backward_denom(Reaction *reac)
{
  assert(backward_denom.find(reac) == backward_denom.end());
  backward_denom.insert(reac);
}

void QSS::add_forward_correction(Reaction *reac)
{
  assert(forward_corrections.find(reac) == forward_corrections.end());
  forward_corrections.insert(reac);
}

void QSS::add_backward_correction(Reaction *reac)
{
  assert(backward_corrections.find(reac) == backward_corrections.end());
  backward_corrections.insert(reac);
}

void QSS::remove_duplicate_reactions(void)
{
  {
    std::vector<Reaction*> to_delete;
    std::map<Reaction*,int> to_add;
    for (std::map<Reaction*,int>::const_iterator it = forward_zeros.begin();
          it != forward_zeros.end(); it++)
    {
      if (it->first->is_duplicate() && !it->first->is_owner())
      {
        to_delete.push_back(it->first);
        to_add[it->first->duplicate.ptr.owner] = it->second;
      }
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      forward_zeros.erase(*it);
    }
    forward_zeros.insert(to_add.begin(),to_add.end());
  }
  {
    std::vector<Reaction*> to_delete;
    std::map<Reaction*,int> to_add;
    for (std::map<Reaction*,int>::const_iterator it = backward_zeros.begin();
          it != backward_zeros.end(); it++)
    {
      if (it->first->is_duplicate() && !it->first->is_owner())
      {
        to_delete.push_back(it->first);
        to_add[it->first->duplicate.ptr.owner] = it->second;
      }
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      backward_zeros.erase(*it);
    }
    backward_zeros.insert(to_add.begin(),to_add.end());
  }
  {
    std::vector<std::pair<int,Reaction*> > to_delete;
    std::map<std::pair<int,Reaction*>,int> to_add;
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = forward_contributions.begin();
          it != forward_contributions.end(); it++)
    {
      if (it->first.second->is_duplicate() && !it->first.second->is_owner())
      {
        to_delete.push_back(it->first);
        to_add[std::pair<int,Reaction*>(it->first.first,it->first.second->duplicate.ptr.owner)] = it->second;
      }
    }
    for (std::vector<std::pair<int,Reaction*> >::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      forward_contributions.erase(*it);
    }
    forward_contributions.insert(to_add.begin(),to_add.end());
  }
  {
    std::vector<std::pair<int,Reaction*> > to_delete;
    std::map<std::pair<int,Reaction*>,int> to_add;
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = backward_contributions.begin();
          it != backward_contributions.end(); it++)
    {
      if (it->first.second->is_duplicate() && !it->first.second->is_owner())
      {
        to_delete.push_back(it->first);
        to_add[std::pair<int,Reaction*>(it->first.first,it->first.second->duplicate.ptr.owner)] = it->second;
      }
    }
    for (std::vector<std::pair<int,Reaction*> >::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      backward_contributions.erase(*it);
    }
    backward_contributions.insert(to_add.begin(),to_add.end());
  }
  {
    std::vector<Reaction*> to_delete;
    std::set<Reaction*> to_add;
    for (std::set<Reaction*>::const_iterator it = forward_denom.begin();
          it != forward_denom.end(); it++)
    {
      if ((*it)->is_duplicate() && !(*it)->is_owner())
      {
        to_delete.push_back(*it);
        to_add.insert((*it)->duplicate.ptr.owner);
      }
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      forward_denom.erase(*it);
    }
    forward_denom.insert(to_add.begin(),to_add.end());
  }
  {
    std::vector<Reaction*> to_delete;
    std::set<Reaction*> to_add;
    for (std::set<Reaction*>::const_iterator it = backward_denom.begin();
          it != backward_denom.end(); it++)
    {
      if ((*it)->is_duplicate() && !(*it)->is_owner())
      {
        to_delete.push_back(*it);
        to_add.insert((*it)->duplicate.ptr.owner);
      }
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      backward_denom.erase(*it);
    }
    backward_denom.insert(to_add.begin(),to_add.end());
  }
  {
    std::vector<Reaction*> to_delete;
    std::set<Reaction*> to_add;
    for (std::set<Reaction*>::const_iterator it = forward_corrections.begin();
          it != forward_corrections.end(); it++)
    {
      if ((*it)->is_duplicate() && !(*it)->is_owner())
      {
        to_delete.push_back(*it);
        to_add.insert((*it)->duplicate.ptr.owner);
      }
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      forward_corrections.erase(*it);
    }
    forward_corrections.insert(to_add.begin(),to_add.end());
  }
  {
    std::vector<Reaction*> to_delete;
    std::set<Reaction*> to_add;
    for (std::set<Reaction*>::const_iterator it = backward_corrections.begin();
          it != backward_corrections.end(); it++)
    {
      if ((*it)->is_duplicate() && !(*it)->is_owner())
      {
        to_delete.push_back(*it);
        to_add.insert((*it)->duplicate.ptr.owner);
      }
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      backward_corrections.erase(*it);
    }
    backward_corrections.insert(to_add.begin(),to_add.end());
  }
}


/////////////////////////////
//
// Stif 
//
/////////////////////////////

Stif::Stif(const char *n, int kp, int k2p, TranslationUnit *u)
  : species(NULL), name(n), k(kp), k2(k2p), unit(u)
{
  
}

void Stif::add_forward_d(Reaction *reac, int coeff)
{
  assert(coeff != 0);
  assert(forward_d.find(reac) == forward_d.end());
  forward_d[reac] = coeff;
  add_fread(reac->idx);
  add_fwrite(reac->idx);
}

void Stif::add_forward_c(Reaction *reac, int coeff)
{
  assert(coeff != 0);
  assert(forward_c.find(reac) == forward_c.end());
  forward_c[reac] = coeff;
  add_fread(reac->idx);
  add_fwrite(reac->idx);
}

void Stif::add_backward_d(Reaction *reac, int coeff)
{
  assert(coeff != 0);
  assert(backward_d.find(reac) == backward_d.end());
  backward_d[reac] = coeff;
  add_bread(reac->idx);
  add_bwrite(reac->idx);
}

void Stif::add_backward_c(Reaction *reac, int coeff)
{
  assert(coeff != 0);
  assert(backward_c.find(reac) == backward_c.end());
  backward_c[reac] = coeff;
  add_bread(reac->idx);
  add_bwrite(reac->idx);
}

void Stif::remove_duplicate_reactions(void)
{
  {
    std::vector<Reaction*> to_delete;
    std::map<Reaction*,int> to_add;
    for (std::map<Reaction*,int>::const_iterator it = forward_d.begin();
          it != forward_d.end(); it++)
    {
      if (it->first->is_duplicate() && !it->first->is_owner())
      {
        to_delete.push_back(it->first);
        to_add[it->first->duplicate.ptr.owner] = it->second;
      }
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      forward_d.erase(*it);
    }
    forward_d.insert(to_add.begin(),to_add.end());
  }
  {
    std::vector<Reaction*> to_delete;
    std::map<Reaction*,int> to_add;
    for (std::map<Reaction*,int>::const_iterator it = backward_d.begin();
          it != backward_d.end(); it++)
    {
      if (it->first->is_duplicate() && !it->first->is_owner())
      {
        to_delete.push_back(it->first);
        to_add[it->first->duplicate.ptr.owner] = it->second;
      }
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      backward_d.erase(*it);
    }
    backward_d.insert(to_add.begin(),to_add.end());
  }
  {
    std::vector<Reaction*> to_delete;
    std::map<Reaction*,int> to_add;
    for (std::map<Reaction*,int>::const_iterator it = forward_c.begin();
          it != forward_c.end(); it++)
    {
      if (it->first->is_duplicate() && !it->first->is_owner())
      {
        to_delete.push_back(it->first);
        to_add[it->first->duplicate.ptr.owner] = it->second;
      }
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      forward_c.erase(*it);
    }
    forward_c.insert(to_add.begin(),to_add.end());
  }
  {
    std::vector<Reaction*> to_delete;
    std::map<Reaction*,int> to_add;
    for (std::map<Reaction*,int>::const_iterator it = backward_c.begin();
          it != backward_c.end(); it++)
    {
      if (it->first->is_duplicate() && !it->first->is_owner())
      {
        to_delete.push_back(it->first);
        to_add[it->first->duplicate.ptr.owner] = it->second;
      }
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      backward_c.erase(*it);
    }
    backward_c.insert(to_add.begin(),to_add.end());
  }
}

// EOF

