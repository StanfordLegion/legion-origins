
#include <cmath>

#include <algorithm>

#include "globals.h"
#include "singe.h"
#include "codegen.h"

#if 0
static double factorial(unsigned num_stages)
{
  double result = 1.0;
  for (unsigned i = 2; i <= num_stages; i++)
    result *= double(i);
  return result;
}

static void emit_sse_exp_taylor_series_expansion(CodeOutStream &ost, const char *var_name, unsigned num_stages)
{
  // The inspiration for this taylor series approximation came from the cuda libraries
  ost << "// Taylor series expansion for " << num_stages << " stages of exp\n";
  PairDelim taylor_pair(ost);
  ost << VECTOR2 << " scale = _mm_round_pd(_mm_mul_pd(" << var_name << ",_mm_set1_pd(1.4426950408889634e+0)),_MM_FROUND_NINT);\n";
  ost << "__m128i power = _mm_cvtpd_epi32(scale);\n";
  ost << "power = _mm_add_epi32(power, _mm_set_epi32(0,0,1023,1023));\n";
  ost << "power = _mm_sll_epi32(power, _mm_set1_epi32(20));\n";
  ost << "power = _mm_shuffle_epi32(power, _MM_SHUFFLE(1,3,0,2));\n";
  ost << VECTOR2 << " remainder = _mm_add_pd(" << var_name << ", _mm_mul_pd(scale, _mm_set1_pd(-6.9314718055994529e-1)));\n";
  ost << "remainder = _mm_add_pd(remainder, _mm_mul_pd(scale, _mm_set1_pd(-2.3190468138462996e-17)));\n";
  ost << var_name << " = _mm_set1_pd(" << (1.0/factorial(num_stages)) << ");\n";
  for (unsigned stage = num_stages-1; stage >= 1; stage--)
  {
    ost << "// Stage " << stage << "\n";
    ost << var_name << " = _mm_add_pd(_mm_mul_pd(" << var_name << ",remainder), _mm_set1_pd(" << (1.0/factorial(stage)) << "));\n"; 
  }
  ost << var_name << " = _mm_add_pd(_mm_mul_pd(" << var_name << ",remainder), _mm_set1_pd(" << (1.0) << "));\n";
  // Now do the bit manipulation to put them into floating point
  ost << var_name << " = _mm_mul_pd(" << var_name << ", _mm_castsi128_pd(power));\n";
  
}
#else
static void emit_sse_exp_taylor_series_expansion(CodeOutStream &ost, const char *var_name, unsigned num_stages)
{
  ost << var_name << " = _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(" << var_name
      << "," << var_name << ",1))),exp(_mm_cvtsd_f64(" << var_name << ")));\n";
}
#endif

#if 0
static void emit_avx_exp_taylor_series_expansion(CodeOutStream &ost, const char *var_name, unsigned num_stages)
{
  ost << "// Taylor series expansion for " << num_stages << " stages of exp\n";
  PairDelim taylor_pair(ost);
  ost << VECTOR4 << " scale = _mm256_mul_pd(" << var_name << ", _mm256_set1_pd(1.4426950408889634e+0));\n";
#if 1
  ost << VECTOR2 << " upper = _mm_round_pd(_mm256_extractf128_pd(scale,1),_MM_FROUND_NINT);\n";
  ost << VECTOR2 << " lower = _mm_round_pd(_mm256_extractf128_pd(scale,0),_MM_FROUND_NINT);\n";
  ost << INT << " i1 = int(_mm_cvtsd_f64(lower));\n";
  ost << INT << " i2 = int(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)));\n";
  ost << INT << " i3 = int(_mm_cvtsd_f64(upper));\n";
  ost << INT << " i4 = int(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)));\n";
#else
  ost << "__m128i power = _mm256_cvtpd_epi32(scale);\n";
  ost << "power = _mm_add_epi32(power, _mm_set1_epi32(1023));\n";
  ost << "power = _mm_sll_epi32(power, _mm_set1_epi32(20));\n";
  ost << "__m256i power2 = _mm256_setzero_si256();\n";
  // Apparently this is an AVX2 instruction which won't appear until Haswell
  ost << "power2 = _mm256_insertf128_si256(power2, power, 0);\n";
  ost << "power2 = _mm256_shuffle_epi32(power2, _MM_SHUFFLE(3,7,2,6,1,5,0,4));\n";
#endif
  ost << VECTOR4 << " remainder = _mm256_add_pd(" << var_name << ", _mm256_mul_pd(scale, _mm256_set1_pd(-6.9314718055994529e-1)));\n";
  ost << "remainder = _mm256_add_pd(remainder, _mm256_mul_pd(scale, _mm256_set1_pd(-2.3190468138462996e-17)));\n";
  ost << var_name << " = _mm256_set1_pd(" << (1.0/factorial(num_stages)) << ");\n";
  for (unsigned stage = num_stages-1; stage >= 1; stage--)
  {
    ost << "// Stage " << stage << "\n";
    ost << var_name << " = _mm256_add_pd(_mm256_mul_pd(" << var_name << ",remainder), _mm256_set1_pd(" << (1.0/factorial(stage)) << "));\n";
  }
  ost << var_name << " = _mm256_add_pd(_mm256_mul_pd(" << var_name << ",remainder), _mm256_set1_pd(" << (1.0) << "));\n";
#if 1
  ost << "volatile " << INT << " bit_vector[8];\n";
  ost << "bit_vector[0] = 0;\n";
  ost << "bit_vector[1] = (i1 + 1023) << 20;\n";
  ost << "bit_vector[2] = 0;\n";
  ost << "bit_vector[3] = (i2 + 1023) << 20;\n";
  ost << "bit_vector[4] = 0;\n";
  ost << "bit_vector[5] = (i3 + 1023) << 20;\n";
  ost << "bit_vector[6] = 0;\n";
  ost << "bit_vector[7] = (i4 + 1023) << 20;\n";
  ost << var_name << " = _mm256_mul_pd(" << var_name << ", _mm256_set_pd(((volatile double*)bit_vector)[3], "
      << "((volatile double*)bit_vector)[2], ((volatile double*)bit_vector)[1] ((volatile double*)bit_vector)[0]));\n";
#else
  ost << var_name << " = _mm256_mul_pd(" << var_name << ", _mm256_castsi256_pd(power2));\n";
#endif
}
#else
static void emit_avx_exp_taylor_series_expansion(CodeOutStream &ost, const char *var_name, unsigned num_stages)
{
  PairDelim exp_pair(ost);
  ost << VECTOR2 << " lower = _mm256_extractf128_pd(" << var_name << ",0);\n";
  ost << VECTOR2 << " upper = _mm256_extractf128_pd(" << var_name << ",1);\n";
  ost << var_name << " = _mm256_set_pd("
      << "exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
      << "exp(_mm_cvtsd_f64(upper)),"
      << "exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
      << "exp(_mm_cvtsd_f64(lower)));\n"; 
}
#endif



static void emit_cuda_load(CodeOutStream &ost, const char *dst,
                           const char *src, bool k20, const char *qualifier = ".cg")
{
  if (k20)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << ") : \"memory\");\n";
  }
}

static void emit_cuda_load(CodeOutStream &ost, const char *dst, 
                           const char *src, const char *src_offset, bool k20, const char *qualifier = ".cg")
{
  if (k20)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
}

static void emit_cuda_load(CodeOutStream &ost, const char *dst, const char *dst_offset,
                           const char *src, const char *src_offset, bool k20, const char *qualifier = ".cg")
{
  if (k20)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << "[" << dst_offset << "]) ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << "[" << dst_offset << "]) ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
}

static void emit_cuda_int_load(CodeOutStream &ost, const char *dst, const char *dst_offset,
                           const char *src, const char *src_offset, bool k20, const char *qualifier = ".cg")
{
  if (k20)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".s32 %0, [%1];\" : \"=r\"(" << dst << "[" << dst_offset << "]) ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".s32 %0, [%1];\" : \"=r\"(" << dst << "[" << dst_offset << "]) ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
}

#if 0
static void emit_cuda_store(CodeOutStream &ost, const char *dst,
                            const char *src)
{
  ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << dst << "), "
      << "\"d\"(" << src << ") : \"memory\");\n";
}
#endif

static void emit_cuda_store(CodeOutStream &ost, const char *dst, const char *dst_offset,
                            const char *src, const char *qualifier = ".cs")
{
  ost << "asm volatile(\"st.global" << qualifier << ".f64 [%0], %1;\" : : \"l\"(" << dst << "+" << dst_offset << ") ";
  ost << ", \"d\"(" << src << ") : \"memory\");\n";
}

static void emit_barrier(CodeOutStream &ost, int barrier_id, int num_warps, bool blocking)
{
  if (blocking)
    ost << "asm volatile(\"bar.sync " << barrier_id << "," << (num_warps*32) << ";\" : : : \"memory\");\n";
  else
    ost << "asm volatile(\"bar.arrive " << barrier_id << "," << (num_warps*32) << ";\" : : : \"memory\");\n";
}

void TranslationUnit::emit_baseline_cpu_chemistry(const char *file_prefix)
{
  char header_name[128];
  char source_name[128];
  snprintf(header_name,127,"%s.h",file_prefix);
  printf("Writing baseline CPU chemistry header file %s...\n",header_name);
  emit_baseline_cpu_chemistry_header_file(header_name);
  snprintf(source_name,127,"%s.cc",file_prefix);
  printf("Writing baseline CPU chemistry source file %s...\n",source_name);
  emit_baseline_cpu_chemistry_source_file(source_name, header_name);
  emit_gpu_constant_header();
}

void TranslationUnit::emit_sse_cpu_chemistry(const char *file_prefix)
{
  char header_name[128];
  char source_name[128];
  snprintf(header_name,127,"%s.h",file_prefix);
  printf("Writing SSE CPU chemistry header file %s...\n",header_name);
  emit_vector_cpu_chemistry_header_file(header_name, true, false);
  snprintf(source_name,127,"%s.cc",file_prefix);
  printf("Writing SSE CPU chemistry source file %s...\n",source_name);
  emit_vector_cpu_chemistry_source_file(source_name, header_name, true, false);
}

void TranslationUnit::emit_avx_cpu_chemistry(const char *file_prefix)
{
  char header_name[128];
  char source_name[128];
  snprintf(header_name,127,"%s.h",file_prefix);
  printf("Writing AVX CPU chemistry header file %s...\n",header_name);
  emit_vector_cpu_chemistry_header_file(header_name, false, true);
  snprintf(source_name,127,"%s.cc",file_prefix);
  printf("Writing AVX CPU chemistry source file %s...\n",source_name);
  emit_vector_cpu_chemistry_source_file(source_name, header_name, false, true);
}

void TranslationUnit::emit_cuda_chemistry(const char *file_prefix)
{
  char header_name[128];
  char source_name[128];
  snprintf(header_name,127,"%s.h",file_prefix);
  printf("Writing CUDA chemistry header file %s...\n",header_name);
  emit_gpu_chemistry_header_file(header_name);
  snprintf(source_name,127,"%s.cu",file_prefix);
  printf("Writing CUDA chemistry source file %s...\n",source_name);
  emit_gpu_chemistry_source_file(source_name, header_name);
  emit_gpu_constant_header();
}

void TranslationUnit::emit_gpu_constant_header(void)
{
  CodeOutStream ost("gpu_s3d_constants.h", true/*bound line length*/, 80);
  ost << "#ifndef __GPU_GETRATES_CONSTANTS__\n";
  ost << "#define __GPU_GETRATES_CONSTANTS__\n";
  ost << "\n"; 
  emit_gpu_mole_masses(ost);
  ost << "\n";
  emit_gpu_recip_mole_masses(ost);
  ost << "\n";
  ost << "#endif // __GPU_GETRATES_CONSTANTS__\n";
}

void TranslationUnit::emit_macros(CodeOutStream &ost)
{
  ost << "#ifndef MAX\n";
  ost << "#define MAX(a,b) (((a) > (b)) ? (a) : (b))\n";
  ost << "#endif\n";
  ost << "#ifndef MIN\n";
  ost << "#define MIN(a,b) (((a) < (b)) ? (a) : (b))\n";
  ost << "#endif\n";
}

void TranslationUnit::emit_baseline_cpu_chemistry_header_file(const char *file_name)
{
  CodeOutStream ost(file_name, true/*bound line lengths*/, 80);

  ost << "#ifndef __BASELINE_CPU_GET_RATES__\n";
  ost << "#define __BASELINE_CPU_GET_RATES__\n";
  ost << "\n";
  if (chem != NULL)
  {
    // If we have a chemistry unit then we need diffusion
    // and we need to say how many stiff species there are
    ost << "#define GETRATES_NEEDS_DIFFUSION\n";
    ost << "\n";
    chem->emit_stiffness_information(ost);
    ost << "\n";
  }
  emit_cpu_mole_masses(ost);
  ost << "\n";
  emit_cpu_recip_mole_masses(ost);
  ost << "\n";
  emit_cpu_getrates_declaration(ost);
  ost << ";\n";
  ost << "#endif // __BASELINE_CPU_GET_RATES__\n";
}

void TranslationUnit::emit_baseline_cpu_chemistry_source_file(const char *file_name, 
                                                              const char *header_name)
{
  CodeOutStream ost(file_name, true/*bound line lengths*/, 80);
  
  ost << "\n";
  ost << "#include \"" << header_name << "\"\n";
  ost << "#include <cmath>\n";
  ost << "#include <cassert>\n";
  ost << "#include <cstdlib>\n";
  ost << "\n";
  emit_macros(ost);
  ost << "\n";
  emit_cpu_mole_masses(ost);
  ost << "\n";
  emit_cpu_recip_mole_masses(ost);
  ost << "\n";

  emit_baseline_cpu_getrates(ost);

  ost << "\n";
}

void TranslationUnit::emit_vector_cpu_chemistry_header_file(const char *file_name,
                                                            bool sse, bool avx)
{
  CodeOutStream ost(file_name, true/*bound line lengths*/, 80);

  if (sse)
  {
    ost << "#ifndef __SSE_CPU_GET_RATES__\n";
    ost << "#define __SSE_CPU_GET_RATES__\n";
  }
  else if (avx)
  {
    ost << "#ifndef __AVX_CPU_GET_RATES__\n";
    ost << "#define __AVX_CPU_GET_RATES__\n";
  }
  ost << "\n";
  emit_vector_getrates_declaration(ost, sse, avx);
  ost << ";\n";
  if (sse)
    ost << "#endif // __SSE_CPU_GET_RATES__\n";
  else if (avx)
    ost << "#endif // __AVX_CPU_GET_RATES__\n";
}

void TranslationUnit::emit_vector_cpu_chemistry_source_file(const char *file_name, 
                                                            const char *header_name,
                                                            bool sse, bool avx)
{
  CodeOutStream ost(file_name, true/*bound line lengths*/, 80);
  
  ost << "\n";
  ost << "#include \"" << header_name << "\"\n";
  ost << "#include <cmath>\n";
  ost << "#include <cassert>\n";
  ost << "#include <cstdlib>\n";
  if (sse || avx)
  {
    ost << "#include <emmintrin.h>\n";
    ost << "#include <smmintrin.h>\n";
  }
  if (avx) {
    ost << "#include <immintrin.h>\n";
    ost << "#include <malloc.h>\n";
  }
  ost << "\n";
  emit_macros(ost);
  ost << "\n";
  emit_cpu_mole_masses(ost);
  ost << "\n";
  emit_cpu_recip_mole_masses(ost);
  ost << "\n";

  if (sse)
    emit_sse_cpu_getrates(ost);
  else if (avx)
    emit_avx_cpu_getrates(ost);

  ost << "\n";
}

void TranslationUnit::emit_cpu_getrates_declaration(CodeOutStream &ost)
{
  ost << VOID << " " << "getrates(";
  ost << "const " << REAL << " " << PRESSURE;
  ost << ", " << "const " << REAL << " " << TEMPERATURE;
  ost << ", " << "const " << REAL << " " << AVMOLWT;
  ost << ", " << "const " << REAL << " *" << MASS_FRAC;
  // Check to see if there is a chemistry unit, if there is
  // then we need to pass diffusion to the function
  if (chem != NULL)
  {
    ost << ", " << "const " << REAL << " *" << DIFFUSION;
    ost << ", " << "const " << REAL << " dt";
  }
  ost << ", " << REAL << " *" << WDOT << ")";
}

void TranslationUnit::emit_baseline_cpu_getrates(CodeOutStream &ost)
{
  assert(chemistry_profiler == NULL);
  if (profile)
    chemistry_profiler = new Profiler("Getrates");
  emit_cpu_getrates_declaration(ost);
  ost << "\n";

  PairDelim func_pair(ost);
  ost << "\n";
  emit_getrates_numerical_constants(ost);
  ost << "\n";
  emit_getrates_temperature_dependent(ost, chemistry_profiler);
  ost << "\n";
  // Need to compute gibbs species values for all species
  // except the third body species
  ost << REAL << " " << GIBBS << "[" << species.size()-1 << "];\n";
  emit_getrates_gibbs_computation(ost, chemistry_profiler);
  ost << "\n";
  ost << REAL << " " << MOLE_FRAC << "[" << ordered_species.size() << "];\n";
  emit_getrates_mole_frac_computation(ost, chemistry_profiler);
  ost << "\n";
  ost << REAL << " " << THBCONC << "[" << third_bodies.size() << "];\n";
  emit_getrates_third_body_computation(ost, chemistry_profiler);
  ost << "\n";
  ost << REAL << " " << "rr_f" << "[" << act_reactions.size() << "];\n";
  ost << REAL << " " << "rr_r" << "[" << act_reactions.size() << "];\n";
  emit_getrates_reaction_rates(ost, chemistry_profiler);
  // If we have modifications to do for QSSA and Stiffness then we do them now
  if (chem != NULL)
  {
    chem->emit_unimportant_rates(ost, chemistry_profiler);
    chem->emit_qssa_modifications(ost, chemistry_profiler);
    chem->emit_stiffness_modifications(ost, chemistry_profiler);
  }
  emit_getrates_wdot_computation(ost, chemistry_profiler);
}

void TranslationUnit::emit_vector_getrates_declaration(CodeOutStream &ost,
                                                        bool sse, bool avx)
{
  ost << VOID;
  if (sse)
    ost << " sse_";
  else if (avx)
    ost << " avx_";
  ost << "getrates("
      << "const " << REAL << " *" << PRESSURE_ARRAY
      << ", const " << REAL << " *" << TEMPERATURE_ARRAY
      << ", const " << REAL << " *" << AVMOLWT_ARRAY
      << ", const " << REAL << " *" << MASS_FRAC_ARRAY
      << ", const " << INT << " num_elmts"
      << ", const " << INT << " spec_stride";
  if (chem != NULL)
  {
    ost << ", const " << REAL << " *" << DIFFUSION_ARRAY
        << ", const " << REAL << " dt";
  }
  ost << ", " << REAL << " *" << WDOT_ARRAY << ")";
}

void TranslationUnit::emit_sse_cpu_getrates(CodeOutStream &ost)
{
  if (!unroll_loops)
  {
    unsigned spec_count = 0;
    for (std::set<Species*>::const_iterator it = species.begin();
          it != species.end(); it++)
    {
      Species *spec = *it;
      if (strcmp(spec->name,"M") == 0)
        continue;
      spec_count++;
    }
    bool first = true;
    ost << "const " << REAL << " gibbs_constants[" << (15*spec_count) << "] = {";
    for (std::set<Species*>::const_iterator it = species.begin();
          it != species.end(); it++)
    {
      Species *spec = *it;
      if (strcmp(spec->name,"M") == 0)
        continue;
      if (!first)
        ost << ",";
      else
        first = false;
      ost << spec->get_common_temperature();
      for (int i = 0; i < 5; i++)
      {
        ost << ", " << spec->get_high_coefficient(i)
            << ", " << spec->get_low_coefficient(i);
      }
      // Reverse the order of these intentionally
      ost << ", " << spec->get_high_coefficient(6)
          << ", " << spec->get_low_coefficient(6);
      ost << ", " << spec->get_high_coefficient(5)
          << ", " << spec->get_low_coefficient(5);
    }
    ost << "};\n\n";
  }
  emit_vector_getrates_declaration(ost, true, false);
  ost << "\n";
  PairDelim func_pair(ost);
  ost << "\n";
  emit_getrates_numerical_constants(ost);
  ost << "\n";
  // The real working set here are the reaction rates and their constants
  // so figure out how many of them we can fit in the cache
  const size_t total_values = cache_size * (1 << 10) / sizeof(REAL_TYPE);
  // Subtract out the constant values
  // Assume there are about 6 per reaction
  // Ignore constant values for now
  //size_t iter_values = total_values - (6*act_reactions.size());
  // Now divide that by the number of reactions we want to store
  // and the number of mole fractions that we need to keep
  size_t iter_values = total_values / (2*act_reactions.size()+ordered_species.size()+
                    third_bodies.size()+6/*temperature*/);
  // Round down if we have too many
  if ((iter_values % 2) != 0)
    iter_values--;
  // Now halve that so we get the number of vector elements
  size_t sse_values = iter_values / 2;
  if (iter_values < 2)
  {
    fprintf(stderr,"Cannot block SSE kernel for cache size %ld for chemistry kernel\n", cache_size);
    exit(1);
  }
  ost << VECTOR2 << " *" << GIBBS << " = (" << VECTOR2 << "*)malloc("
      << (species.size()-1) << "*" << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *" << TEMPERATURE << " = (" << VECTOR2 << "*)malloc("
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *otc = (" << VECTOR2 << "*)malloc("
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *ortc = (" << VECTOR2 << "*)malloc("
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *vlntemp = (" << VECTOR2 << "*)malloc("
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *prt = (" << VECTOR2 << "*)malloc("
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *oprt = (" << VECTOR2 << "*)malloc("
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *" << MOLE_FRAC << " = (" << VECTOR2 << "*)malloc("
      << (ordered_species.size()) << "*" << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *" << THBCONC << " = (" << VECTOR2 << "*)malloc("
      << (third_bodies.size()) << "*" << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *" << "rr_f = (" << VECTOR2 << "*)malloc("
      << (act_reactions.size()) << "*" << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *" << "rr_r = (" << VECTOR2 << "*)malloc("
      << (act_reactions.size()) << "*" << sse_values << "*sizeof(" << VECTOR2 << "));\n";

  // Emit the main loop
  ost << "size_t remaining_elmts = num_elmts;\n";
  if (perfect)
    ost << "assert((remaining_elmts % " << iter_values << ") == 0);\n";
  ost << "while (remaining_elmts >= " << iter_values << ")\n";
  {
    PairDelim while_pair(ost);
    emit_sse_temperature_dependent(ost, true, sse_values);
    emit_sse_gibbs_computation(ost, true, sse_values);
    emit_sse_mole_frac_computation(ost, true, sse_values);
    emit_sse_third_body_computation(ost, true, sse_values);
    emit_sse_reaction_rates(ost, true, sse_values);
    if (chem != NULL)
    {
      chem->emit_sse_unimportant_rates(ost, true, sse_values);
      chem->emit_sse_qssa_modifications(ost, true, sse_values);
      chem->emit_sse_stiffness_modifications(ost, true, sse_values);
    }
    emit_sse_wdot_computation(ost, true, sse_values);
    ost << "remaining_elmts -= " << iter_values << ";\n";
    // Also update the arrays that need to be scaled
    // Pressure, temperature, and avmolwt are scaled internally
    ost << MASS_FRAC_ARRAY << " += " << iter_values << ";\n";
    if (chem != NULL)
      ost << DIFFUSION_ARRAY << " += " << iter_values << ";\n";
    ost << WDOT_ARRAY << " += " << iter_values << ";\n";
  }
  // Now do the clean up code
  if (!perfect)
  {
    ost << "if (remaining_elmts > 0)\n";
    PairDelim if_pair(ost);
    ost << "assert((remaining_elmts % 2) == 0);\n";
    emit_sse_temperature_dependent(ost, false, sse_values);
    emit_sse_gibbs_computation(ost, false, sse_values);
    emit_sse_mole_frac_computation(ost, false, sse_values);
    emit_sse_third_body_computation(ost, false, sse_values);
    emit_sse_reaction_rates(ost, false, sse_values);
    if (chem != NULL)
    {
      chem->emit_sse_unimportant_rates(ost, false, sse_values);
      chem->emit_sse_qssa_modifications(ost, false, sse_values);
      chem->emit_sse_stiffness_modifications(ost, false, sse_values);
    }
    emit_sse_wdot_computation(ost, false, sse_values);
  }

  ost << "free(" << GIBBS << ");\n";
  ost << "free(" << TEMPERATURE << ");\n";
  ost << "free(" << MOLE_FRAC << ");\n";
  ost << "free(" << THBCONC << ");\n";
  ost << "free(rr_f);\n";
  ost << "free(rr_r);\n";
  ost << "free(otc);\n";
  ost << "free(ortc);\n";
  ost << "free(vlntemp);\n";
  ost << "free(prt);\n";
  ost << "free(oprt);\n";
}

void TranslationUnit::emit_avx_cpu_getrates(CodeOutStream &ost)
{
  if (!unroll_loops)
  {
    unsigned spec_count = 0;
    for (std::set<Species*>::const_iterator it = species.begin();
          it != species.end(); it++)
    {
      Species *spec = *it;
      if (strcmp(spec->name,"M") == 0)
        continue;
      spec_count++;
    }
    bool first = true;
    ost << "const " << REAL << " gibbs_constants[" << (15*spec_count) << "] = {";
    for (std::set<Species*>::const_iterator it = species.begin();
          it != species.end(); it++)
    {
      Species *spec = *it;
      if (strcmp(spec->name,"M") == 0)
        continue;
      if (!first)
        ost << ",";
      else
        first = false;
      ost << spec->get_common_temperature();
      for (int i = 0; i < 5; i++)
      {
        ost << ", " << spec->get_high_coefficient(i)
            << ", " << spec->get_low_coefficient(i);
      }
      // Reverse the order of these intentionally
      ost << ", " << spec->get_high_coefficient(6)
          << ", " << spec->get_low_coefficient(6);
      ost << ", " << spec->get_high_coefficient(5)
          << ", " << spec->get_low_coefficient(5);
    }
    ost << "};\n\n";
  }
  ost << "template<unsigned BOUNDARY>\n";
  ost << "static inline bool aligned(const void *ptr)\n";
  {
    PairDelim func_pair(ost);
    ost << "return ((((unsigned long)ptr) & (BOUNDARY-1)) == 0);\n";
  }
  ost << "\n";
  emit_vector_getrates_declaration(ost, false, true);
  ost << "\n";
  PairDelim func_pair(ost);
  ost << "\n";
  emit_getrates_numerical_constants(ost);
  ost << "\n";
  // The real working set here are the reaction rates and their constants
  // so figure out how many of them we can fit in the cache
  const size_t total_values = cache_size * (1 << 10) / sizeof(REAL_TYPE);
  // Subtract out the constant values
  // Assume there are about 6 per reaction
  // Ignore constants for now
  //size_t iter_values = total_values - (6*act_reactions.size());
  // Now divide that by the number of reactions we want to store
  // and the number of mole fractions that we need to keep
  size_t iter_values = total_values / (2*act_reactions.size()+ordered_species.size()+
                        third_bodies.size()+6/*temperature*/);
  // Round down if we have too many
  while ((iter_values % 4) != 0)
    iter_values--;
  // Now halve that so we get the number of vector elements
  size_t avx_values = iter_values / 4;
  if (iter_values < 4)
  {
    fprintf(stderr,"Cannot block AVX kernel for cache size %ld for chemistry kernel.\n",
            cache_size);
    exit(1);
  }
  ost << VECTOR4 << " *" << GIBBS << " = (" << VECTOR4 << "*)memalign(32,"
      << (species.size()-1) << "*" << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *" << TEMPERATURE << " = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *otc = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *ortc = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *vlntemp = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *prt = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *oprt = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *" << MOLE_FRAC << " = (" << VECTOR4 << "*)memalign(32,"
      << (ordered_species.size()) << "*" << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *" << THBCONC << " = (" << VECTOR4 << "*)memalign(32,"
      << (third_bodies.size()) << "*" << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *" << "rr_f = (" << VECTOR4 << "*)memalign(32,"
      << (act_reactions.size()) << "*" << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *" << "rr_r = (" << VECTOR4 << "*)memalign(32,"
      << (act_reactions.size()) << "*" << avx_values << "*sizeof(" << VECTOR4 << "));\n";

  // Emit the main loop
  ost << "size_t remaining_elmts = num_elmts;\n";
  if (perfect)
    ost << "assert((remaining_elmts % " << iter_values <<") == 0);\n";
  ost << "while (remaining_elmts >= " << iter_values << ")\n";
  {
    PairDelim while_pair(ost);
    emit_avx_temperature_dependent(ost, true, avx_values);
    emit_avx_gibbs_computation(ost, true, avx_values);
    emit_avx_mole_frac_computation(ost, true, avx_values);
    emit_avx_third_body_computation(ost, true, avx_values);
    emit_avx_reaction_rates(ost, true, avx_values);
    if (chem != NULL)
    {
      chem->emit_avx_unimportant_rates(ost, true, avx_values);
      chem->emit_avx_qssa_modifications(ost, true, avx_values);
      chem->emit_avx_stiffness_modifications(ost, true, avx_values);
    }
    emit_avx_wdot_computation(ost, true, avx_values);
    ost << "remaining_elmts -= " << iter_values << ";\n";
    // Also update the arrays that need to be scaled
    // Pressure, temperature, and avmolwt are scaled internally
    ost << MASS_FRAC_ARRAY << " += " << iter_values << ";\n";
    if (chem != NULL)
      ost << DIFFUSION_ARRAY << " += " << iter_values << ";\n";
    ost << WDOT_ARRAY << " += " << iter_values << ";\n";
  }
  // Now do the clean up code
  if (!perfect)
  {
    ost << "if (remaining_elmts > 0)\n";
    PairDelim if_pair(ost);
    ost << "assert((remaining_elmts % 4) == 0);\n";
    emit_avx_temperature_dependent(ost, false, avx_values);
    emit_avx_gibbs_computation(ost, false, avx_values);
    emit_avx_mole_frac_computation(ost, false, avx_values);
    emit_avx_third_body_computation(ost, false, avx_values);
    emit_avx_reaction_rates(ost, false, avx_values);
    if (chem != NULL)
    {
      chem->emit_avx_unimportant_rates(ost, false, avx_values);
      chem->emit_avx_qssa_modifications(ost, false, avx_values);
      chem->emit_avx_stiffness_modifications(ost, false, avx_values);
    }
    emit_avx_wdot_computation(ost, false, avx_values);
  }

  ost << "free(" << GIBBS << ");\n";
  ost << "free(" << TEMPERATURE << ");\n";
  ost << "free(" << MOLE_FRAC << ");\n";
  ost << "free(" << THBCONC << ");\n";
  ost << "free(rr_f);\n";
  ost << "free(rr_r);\n";
  ost << "free(otc);\n";
  ost << "free(ortc);\n";
  ost << "free(vlntemp);\n";
  ost << "free(prt);\n";
  ost << "free(oprt);\n";

}

void TranslationUnit::emit_getrates_numerical_constants(CodeOutStream &ost)
{
  ost << "const " << REAL << " PA = 1.013250e+06;\n";
  if (use_dim)
  {
    ost << "const " << REAL << " R0 = 8.314510e+07;\n";
    ost << "const " << REAL << " R0c = 1.9872155832;\n";
  }
  else
  {
    ost << "// Scaled R0 for non-dimensionalization\n";
    ost << "const " << REAL << " R0 = " << (8.314510e+07 * TEMPERATURE_REF) << ";\n";
    ost << "// Scaled R0c for non-dimensionalization\n";
    ost << "const " << REAL << " R0c = " << (1.9872155832 * TEMPERATURE_REF) << ";\n";
  }
  if (needs_dln10)
    ost << "const " << REAL << " DLn10 = 2.3025850929940459e0;\n";
}

void TranslationUnit::emit_getrates_temperature_dependent(CodeOutStream &ost,
                                                          Profiler *profiler)
{
  ost << "const " << REAL << " otc     = 1.0 / " << TEMPERATURE << ";\n";
  ost << "const " << REAL << " ortc    = 1.0 / (" << TEMPERATURE << " * R0c);\n";
  ost << "const " << REAL << " vlntemp = log(" << TEMPERATURE << ");\n";
  ost << "const " << REAL << " prt     = PA / (R0 * " << TEMPERATURE << ");\n";
  ost << "const " << REAL << " oprt    = 1.0 / prt;\n";
  if (profiler != NULL)
  {
    profiler->multiplies(2);
    profiler->divides(4);
    profiler->logarithms(1);
  }
}

void TranslationUnit::emit_getrates_gibbs_computation(CodeOutStream &ost,
                                                      Profiler *profiler)
{
  ost << "// Gibbs computation\n";
  PairDelim gibbs_pair(ost);
  ost << "const " << REAL << " &tk1 = " << TEMPERATURE << ";\n";
  ost << REAL << " tklog = log(tk1);\n";
  ost << REAL << " tk2 = tk1 * tk1;\n";
  ost << REAL << " tk3 = tk1 * tk2;\n";
  ost << REAL << " tk4 = tk1 * tk3;\n";
  ost << REAL << " tk5 = tk1 * tk4;\n";
  ost << "\n";
  if (profiler != NULL)
  {
    profiler->logarithms(1);
    profiler->multiplies(4);
  }

  for (std::set<Species*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    Species *spec = *it;
    // Skip the third body species
    if (strcmp(spec->name,"M") == 0)
      continue;
    ost << "// Species " << spec->name << "\n";
    PairDelim spec_pair(ost);
    ost << "if (tk1 > " << spec->get_common_temperature() << ")\n";
    {
      PairDelim if_pair(ost);
      ost << GIBBS << "[" << (spec->idx-1) << "] = ";
      ost << spec->get_high_coefficient(0) << "*tk1*(";
      if (use_dim)
        ost << "1";
      else
        ost << (1-log(TEMPERATURE_REF));
      ost << "-tklog) + ";
      ost << spec->get_high_coefficient(1) << "*tk2 + ";
      ost << spec->get_high_coefficient(2) << "*tk3 + ";
      ost << spec->get_high_coefficient(3) << "*tk4 + ";
      ost << spec->get_high_coefficient(4) << "*tk5 + ";
      ost << "(";
      ost << spec->get_high_coefficient(5) << " - ";
      ost << "tk1*" << spec->get_high_coefficient(6) << ");\n";
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << GIBBS << "[" << (spec->idx-1) << "] = ";
      ost << spec->get_low_coefficient(0) << "*tk1*(";
      if (use_dim)
        ost << "1";
      else
        ost << (1-log(TEMPERATURE_REF));
      ost << "-tklog) + ";
      ost << spec->get_low_coefficient(1) << "*tk2 + ";
      ost << spec->get_low_coefficient(2) << "*tk3 + ";
      ost << spec->get_low_coefficient(3) << "*tk4 + ";
      ost << spec->get_low_coefficient(4) << "*tk5 + ";
      ost << "(";
      ost << spec->get_low_coefficient(5) << " - ";
      ost << "tk1*" << spec->get_low_coefficient(6) << ");\n";
    }
    if (profiler != NULL)
    {
      profiler->reads(1);
      profiler->adds(5);
      profiler->subtracts(1);
      profiler->multiplies(7);
    }
  }
}

void TranslationUnit::emit_getrates_mole_frac_computation(CodeOutStream &ost,
                                                          Profiler *profiler)
{
  // Compute the mole fractions for each of the mass fractions  
  ost << "// Compute mole fractions\n";
  PairDelim mole_pair(ost);
  ost << REAL << " sumyow = " << TEMPERATURE 
      << " * " << AVMOLWT << " * ";
  // Scale by 1e-3 to account for conversion in
  // the computation of avmolwt
  if (use_dim)
    ost << "8.314510e+04";
  else
    ost << (8.314510e+04 * TEMPERATURE_REF / PRESSURE_REF);
  ost << ";\n";
  ost << "sumyow = " << PRESSURE << "/sumyow;\n";
  if (profiler != NULL)
  {
    // read temperature and pressure 
    profiler->reads(2);
    profiler->multiplies(2);
    profiler->divides(1);
  }
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
  {
    ost << MOLE_FRAC << "[" << idx << "] = " << MASS_FRAC << "[" << idx << "]"
        << " * recip_molecular_masses[" << idx << "];\n";
    // Check for the minimum sized mole fraction
    ost << MOLE_FRAC << "[" << idx << "] = (" << MOLE_FRAC << "[" << idx << "] > "
        << small_mole_frac << ") ? " << MOLE_FRAC << "[" << idx << "] : " << small_mole_frac << ";\n";
    ost << MOLE_FRAC << "[" << idx << "] *= sumyow;\n";
    if (profiler != NULL)
    {
      profiler->reads(1);
      profiler->writes(1);
      profiler->multiplies(1);
    }
  }
}

void TranslationUnit::emit_getrates_third_body_computation(CodeOutStream &ost,
                                                           Profiler *profiler)
{
  ost << "// Computing third body values\n";
  PairDelim third_pair(ost);
  ost << REAL << " ctot = 0.0;\n";
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
  {
    ost << "ctot += " << MOLE_FRAC << "[" << idx << "];\n";
    if (profiler != NULL)
    {
      profiler->reads(1);
      profiler->adds(1);
    }
  }
  for (unsigned idx = 0; idx < third_bodies.size(); idx++)
  {
    ost << THBCONC << "[" << idx << "] = ctot";
    ThirdBody *thb = third_bodies[idx];
    for (std::map<Species*,double>::const_iterator it = thb->components.begin();
          it != thb->components.end(); it++)
    {
      if (it->second >= 0.0)
      {
        ost << " + ";
        if (it->second != 1.0)
        {
          ost << it->second << "*";
          if (profiler != NULL)
            profiler->multiplies(1);
        }
        int index = find_ordered_index(it->first);
        // Better not be a species we don't have mole frac for
        assert(index != -1);
        ost << MOLE_FRAC << "[" << index << "]";
        if (profiler != NULL)
        {
          profiler->reads(1);
          profiler->adds(1);
        }
      }
      else
      {
        ost << " - ";
        if (it->second != -1.0)
        {
          ost << (-it->second) << "*";
          if (profiler != NULL)
            profiler->multiplies(1);
        }
        int index = find_ordered_index(it->first);
        // Better not be a species we don't have a mole frac for
        assert(index != -1);
        ost << MOLE_FRAC << "[" << index << "]";
        if (profiler != NULL)
        {
          profiler->reads(1);
          profiler->subtracts(1);
        }
      }
    }
    if (profiler != NULL)
      profiler->writes(1);
    ost << ";\n";
  }
}

void TranslationUnit::emit_getrates_reaction_rates(CodeOutStream &ost, Profiler *profiler)
{
  unsigned actual_idx = 0;
  for (unsigned idx = 0; idx < all_reactions.size(); idx++)
  {
    Reaction *reac = all_reactions[idx];
    // Skip all duplicate reactions that aren't the owner
    if (reac->is_duplicate() && reac->is_owned())
      continue;
    all_reactions[idx]->emit_reaction_code(ost, actual_idx, profiler); 
    actual_idx++;
  }
  // This should be true when we're done
  assert(actual_idx == act_reactions.size());
}

void TranslationUnit::emit_getrates_wdot_computation(CodeOutStream &ost,
                                                     Profiler *profiler)
{
  // First reduce everything into ROPL values
  ost << REAL << " ropl[" << act_reactions.size() << "];\n";
  ost << "for (int i = 0; i < " << act_reactions.size() << "; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << "ropl[i] = rr_f[i] - rr_r[i];\n";
    if (profiler != NULL)
    {
      profiler->reads(2);
      profiler->writes(1);
      profiler->subtracts(1);
    }
  }
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
  {
    ordered_species[idx]->emit_contributions(ost, idx, profiler);
  }
}

void TranslationUnit::emit_sse_temperature_dependent(CodeOutStream &ost,
                                                 bool main, size_t iters)
{
  ost << "// Temperature dependent\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
  PairDelim for_pair(ost);
  // First load the temperature
  ost << TEMPERATURE << "[idx] = _mm_load_pd(" << TEMPERATURE_ARRAY << ");\n";
  ost << TEMPERATURE_ARRAY << " += 2;\n";
  ost << "otc[idx] = _mm_div_pd(_mm_set1_pd(1.0)," << TEMPERATURE << "[idx]);\n";
  ost << "ortc[idx] = _mm_div_pd(_mm_set1_pd(1.0),_mm_mul_pd("
      << TEMPERATURE << "[idx],_mm_set1_pd(R0c)));\n";
  ost << REAL << " t1 = _mm_cvtsd_f64(" << TEMPERATURE << "[idx]);\n";
  ost << REAL << " log1 = log(t1);\n";
  ost << REAL << " t0 = _mm_cvtsd_f64(_mm_shuffle_pd(" << TEMPERATURE << "[idx],"
      << TEMPERATURE << "[idx],1));\n";
  ost << REAL << " log0 = log(t0);\n";
  ost << "vlntemp[idx] = _mm_set_pd(log0,log1);\n";
  ost << "prt[idx] = _mm_div_pd(_mm_set1_pd(PA),_mm_mul_pd("
      << TEMPERATURE << "[idx],_mm_set1_pd(R0)));\n";
  ost << "oprt[idx] = _mm_div_pd(_mm_set1_pd(1.0),prt[idx]);\n";
}

void TranslationUnit::emit_sse_gibbs_computation(CodeOutStream &ost,
                                                 bool main, size_t iters)
{
  ost << "// Gibbs computation\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
  PairDelim gibbs_pair(ost);
  ost << VECTOR2 << " tk1 = " << TEMPERATURE << "[idx];\n";
  ost << VECTOR2 << " tklog = vlntemp[idx];\n";
  ost << VECTOR2 << " tk2 = _mm_mul_pd(tk1,tk1);\n";
  ost << VECTOR2 << " tk3 = _mm_mul_pd(tk1,tk2);\n";
  ost << VECTOR2 << " tk4 = _mm_mul_pd(tk1,tk3);\n";
  ost << VECTOR2 << " tk5 = _mm_mul_pd(tk1,tk4);\n";
  if (!unroll_loops)
  {
    // Count how many species there are
    unsigned spec_count = 0;
    for (std::set<Species*>::const_iterator it = species.begin();
          it != species.end(); it++)
    {
      Species *spec = *it;
      if (strcmp(spec->name,"M") == 0)
        continue;
      spec_count++;
    }
    ost << "for (int spec = 0; spec < " << spec_count << "; spec++)\n";
    PairDelim spec_pair(ost);
    ost << VECTOR2 << " _" << GIBBS << ", temp;\n";
    emit_generalized_gibbs_impl(ost, 1, 0, true, false);
    ost << GIBBS << "[spec*" << iters << "+idx] = _"
        << GIBBS << ";\n";
  }
  else
  {
    unsigned spec_idx = 0;
    for (std::set<Species*>::const_iterator it = species.begin();
          it != species.end(); it++)
    {
      Species *spec = *it;
      // Skip the third body species
      if (strcmp(spec->name,"M") == 0)
        continue;
      ost << "// Species " << spec->name << "\n";
      PairDelim spec_pair(ost);
      ost << VECTOR2 << " " GIBBS << "_" << spec->code_name <<", temp;\n";
      emit_sse_gibbs_recurse(ost, spec, 1, 0); 
      ost << GIBBS << "[" << (spec_idx*iters) << "+idx] = "
          << GIBBS << "_" << spec->code_name << ";\n";
      spec_idx++;
    }
  }
}

void TranslationUnit::emit_sse_gibbs_recurse(CodeOutStream &ost, Species *spec, 
                                             int offset, int mask)
{
  if (offset == 0)
  {
    ost << "if (_mm_cvtsd_f64(tk1) > "
        << spec->get_common_temperature() << ")\n";
    {
      PairDelim if_pair(ost);
      emit_sse_gibbs_impl(ost, spec, mask);
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      emit_sse_gibbs_impl(ost, spec, mask | 1);
    }
  }
  else
  {
    ost << "if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > "
        << spec->get_common_temperature() << ")\n";
    {
      PairDelim if_pair(ost);
      emit_sse_gibbs_recurse(ost, spec, offset-1, mask);
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      emit_sse_gibbs_recurse(ost, spec, offset-1, mask | (1 << offset));
    }
  }
}

void TranslationUnit::emit_sse_gibbs_impl(CodeOutStream &ost, Species *spec, int mask)
{
  ost << VECTOR2 << " coeff1 = _mm_blend_pd(_mm_set1_pd(" << spec->get_high_coefficient(0)
      << "),_mm_set1_pd(" << spec->get_low_coefficient(0) << "), " << mask << ");\n"; 
  ost << GIBBS << "_" << spec->code_name << " = _mm_mul_pd(coeff1,tk1);\n";
  if (!use_dim)
    ost << VECTOR2 << " coefflog = _mm_set1_pd(" << (1-log(TEMPERATURE_REF)) << ");\n";
  else
    ost << VECTOR2 << " coefflog = _mm_set1_pd(1.0);\n";
  ost << "temp = _mm_sub_pd(coefflog,tklog);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm_mul_pd(" << GIBBS << "_" << spec->code_name << ",temp);\n";
  ost << VECTOR2 << " coeff2 = _mm_blend_pd(_mm_set1_pd(" << spec->get_high_coefficient(1)
      << "),_mm_set1_pd(" << spec->get_low_coefficient(1) << "), " << mask << ");\n";
  ost << "temp = _mm_mul_pd(coeff2,tk2);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm_add_pd(" << GIBBS << "_" << spec->code_name << ",temp);\n";
  ost << VECTOR2 << " coeff3 = _mm_blend_pd(_mm_set1_pd(" << spec->get_high_coefficient(2) 
      << "),_mm_set1_pd(" << spec->get_low_coefficient(2) << "), " << mask << ");\n";
  ost << "temp = _mm_mul_pd(coeff3,tk3);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm_add_pd(" << GIBBS << "_" << spec->code_name << ",temp);\n";
  ost << VECTOR2 << " coeff4 = _mm_blend_pd(_mm_set1_pd(" << spec->get_high_coefficient(3) 
      << "),_mm_set1_pd(" << spec->get_low_coefficient(3) << "), " << mask << ");\n";
  ost << "temp = _mm_mul_pd(coeff4,tk4);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm_add_pd(" << GIBBS << "_" << spec->code_name << ",temp);\n";
  ost << VECTOR2 << " coeff5 = _mm_blend_pd(_mm_set1_pd(" << spec->get_high_coefficient(4) 
      << "),_mm_set1_pd(" << spec->get_low_coefficient(4) << ")," << mask << ");\n";
  ost << "temp = _mm_mul_pd(coeff5,tk5);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm_add_pd(" << GIBBS << "_" << spec->code_name << ",temp);\n";
  ost << VECTOR2 << " coeff7 = _mm_blend_pd(_mm_set1_pd(" << spec->get_high_coefficient(6) 
      << "),_mm_set1_pd(" << spec->get_low_coefficient(6) << ")," << mask << ");\n";
  ost << "temp = _mm_mul_pd(coeff7,tk1);\n";
  ost << VECTOR2 << " coeff6 = _mm_blend_pd(_mm_set1_pd(" << spec->get_high_coefficient(5)
      << "),_mm_set1_pd(" << spec->get_low_coefficient(5) << ")," << mask << ");\n";
  ost << "temp = _mm_sub_pd(coeff6,temp);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm_add_pd(" << GIBBS << "_" << spec->code_name
      << ",temp);\n";
}

void TranslationUnit::emit_sse_mole_frac_computation(CodeOutStream &ost,
                                                     bool main, size_t iters)
{
  ost << "// Compute mole fractions\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
  PairDelim mole_pair(ost);
  ost << VECTOR2 << " avmolwt = _mm_load_pd(" << AVMOLWT_ARRAY << ");\n";
  ost << VECTOR2 << " pressure = _mm_load_pd(" << PRESSURE_ARRAY << ");\n";
  ost << AVMOLWT_ARRAY << " += 2;\n";
  ost << PRESSURE_ARRAY << " += 2;\n";
  ost << VECTOR2 << " sumyow = _mm_mul_pd(_mm_mul_pd(" << TEMPERATURE
      << "[idx],avmolwt),_mm_set1_pd(";
  if (use_dim)
    ost << "8.314510e+04";
  else
    ost << (8.314510e+04 * TEMPERATURE_REF / PRESSURE_REF);
  ost << "));\n";
  ost << "sumyow = _mm_div_pd(pressure,sumyow);\n";
  ost << VECTOR2 << " mask, mass_frac;\n";
  ost << VECTOR2 << " small = _mm_set1_pd(" << small_mole_frac << ");\n";
  for (unsigned spec_idx = 0; spec_idx < ordered_species.size(); spec_idx++)
  {
    ost << "mass_frac = _mm_load_pd("
        << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<1));\n";
    ost << "mass_frac = _mm_mul_pd(mass_frac,_mm_set1_pd(recip_molecular_masses["
        << spec_idx << "]));\n";
    ost << "mask = _mm_cmpgt_pd(mass_frac,small);\n";
    ost << "mass_frac = _mm_and_pd(mass_frac,mask);\n";
    ost << "mole_frac[" << (spec_idx*iters) << "+idx] = _mm_mul_pd(sumyow,"
        << "_mm_add_pd(mass_frac,_mm_andnot_pd(mask,small)));\n";
  }
}

void TranslationUnit::emit_sse_third_body_computation(CodeOutStream &ost,
                                                      bool main, size_t iters)
{
  ost << "// Compute third body values\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
  PairDelim third_pair(ost);
  ost << VECTOR2 << " ctot = _mm_set1_pd(0.0);\n";
  for (unsigned spec_idx = 0; spec_idx < ordered_species.size(); spec_idx++)
  {
    ost << "ctot = _mm_add_pd(ctot,mole_frac[" << (spec_idx*iters) << "+idx]);\n";
  }
  for (unsigned idx = 0; idx < third_bodies.size(); idx++)
  {
    ost << THBCONC << "[" << (idx*iters) << "+idx] = ctot;\n";
    ThirdBody *thb = third_bodies[idx];
    for (std::map<Species*,double>::const_iterator it = thb->components.begin();
          it != thb->components.end(); it++)
    {
      ost << THBCONC << "[" << (idx*iters) << "+idx] = ";
      if (it->second >= 0.0)
      {
        ost << "_mm_add_pd(" << THBCONC << "[" << (idx*iters) << "+idx],"; 
        int index = find_ordered_index(it->first);
        // Better not be a species we don't have mole frac for
        assert(index != -1);
        if (it->second != 1.0)
        {
          ost << "_mm_mul_pd(_mm_set1_pd(" << it->second << "),"
              << "mole_frac[" << (index*iters) << "+idx])";
        }
        else
          ost << "mole_frac[" << (index*iters) << "+idx]";
      }
      else
      {
        ost << "_mm_sub_pd(" << THBCONC << "[" << (idx*iters) << "+idx],";
        int index = find_ordered_index(it->first);
        // Better not be a species we don't have mole frac for
        assert(index != -1);
        if (it->second != 1.0)
        {
          ost << "_mm_mul_pd(_mm_set1_pd(" << (-it->second) << "),"
              << "mole_frac[" << (index*iters) << "+idx])";
        }
        else
          ost << "mole_frac[" << (index*iters) << "+idx]";
      }
      ost << ");\n";
    }
  }
}

void TranslationUnit::emit_sse_reaction_rates(CodeOutStream &ost, 
                                              bool main, size_t iters)
{
  ost << "// Reaction rates\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
  PairDelim reac_pair(ost);
  unsigned actual_idx = 0;
  for (unsigned idx = 0; idx < all_reactions.size(); idx++)
  {
    Reaction *reac = all_reactions[idx];
    // Skip all duplicate reactions that aren't the owner
    if (reac->is_duplicate() && reac->is_owned())
      continue;
    all_reactions[idx]->emit_sse_reaction_code(ost, actual_idx, iters); 
    actual_idx++;
  }
  // This should be true when we're done
  assert(actual_idx == act_reactions.size());
}

void TranslationUnit::emit_sse_wdot_computation(CodeOutStream &ost,
                                                bool main, size_t iters)
{
  ost << "// Output rates\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
  PairDelim for_pair(ost);
  ost << VECTOR2 << " ropl[" << act_reactions.size() << "];\n";
  for (unsigned idx = 0; idx < act_reactions.size(); idx++)
    ost << "ropl[" << idx << "] = _mm_sub_pd(rr_f["
        << (idx*iters) << "+idx],rr_r[" << (idx*iters) << "+idx]);\n";
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
    ordered_species[idx]->emit_sse_contributions(ost, idx, iters);
}

int TranslationUnit::find_ordered_index(Species *spec) const
{
  int result = 0;
  for (std::vector<Species*>::const_iterator it = ordered_species.begin();
        it != ordered_species.end(); it++)
  {
    if ((*it) == spec)
      return result;
    result++;
  }
  return -1;
}

void TranslationUnit::emit_avx_temperature_dependent(CodeOutStream &ost,
                                              bool main, size_t iters)
{
  ost << "// Temperature dependent\n";
  ost << "if (aligned<32>(" << TEMPERATURE_ARRAY << "))\n";
  {
    PairDelim if_pair(ost);
    if (main)
      ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
    else
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
    PairDelim for_pair(ost);
    ost << TEMPERATURE << "[idx] = _mm256_load_pd(" << TEMPERATURE_ARRAY << ");\n";
    ost << TEMPERATURE_ARRAY << " += 4;\n";
    ost << "otc[idx] = _mm256_div_pd(_mm256_set1_pd(1.0)," << TEMPERATURE << "[idx]);\n";
    ost << "ortc[idx] = _mm256_div_pd(_mm256_set1_pd(1.0),_mm256_mul_pd("
        << TEMPERATURE << "[idx],_mm256_set1_pd(R0c)));\n";
    {
      PairDelim vln_pair(ost);
      ost << VECTOR2 << " lower = _mm256_extractf128_pd(temperature[idx],0);\n";
      ost << VECTOR2 << " upper = _mm256_extractf128_pd(temperature[idx],1);\n";
      ost << "vlntemp[idx] = _mm256_set_pd("
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
          << "log(_mm_cvtsd_f64(upper)),"
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
          << "log(_mm_cvtsd_f64(lower)));\n"; 
    }
    ost << "prt[idx] = _mm256_div_pd(_mm256_set1_pd(PA),_mm256_mul_pd("
        << TEMPERATURE << "[idx],_mm256_set1_pd(R0)));\n";
    ost << "oprt[idx] = _mm256_div_pd(_mm256_set1_pd(1.0),prt[idx]);\n";
  }
  ost << "else\n";
  PairDelim else_pair(ost);
  {
    if (main)
      ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
    else
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
    PairDelim for_pair(ost);
    // First load the temperature
    ost << TEMPERATURE << "[idx] = _mm256_loadu_pd(" << TEMPERATURE_ARRAY << ");\n";
    ost << TEMPERATURE_ARRAY << " += 4;\n";
    ost << "otc[idx] = _mm256_div_pd(_mm256_set1_pd(1.0)," << TEMPERATURE << "[idx]);\n";
    ost << "ortc[idx] = _mm256_div_pd(_mm256_set1_pd(1.0),_mm256_mul_pd("
        << TEMPERATURE << "[idx],_mm256_set1_pd(R0c)));\n";
    {
      PairDelim vln_pair(ost);
      ost << VECTOR2 << " lower = _mm256_extractf128_pd(temperature[idx],0);\n";
      ost << VECTOR2 << " upper = _mm256_extractf128_pd(temperature[idx],1);\n";
      ost << "vlntemp[idx] = _mm256_set_pd("
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
          << "log(_mm_cvtsd_f64(upper)),"
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
          << "log(_mm_cvtsd_f64(lower)));\n"; 
    }
    ost << "prt[idx] = _mm256_div_pd(_mm256_set1_pd(PA),_mm256_mul_pd("
        << TEMPERATURE << "[idx],_mm256_set1_pd(R0)));\n";
    ost << "oprt[idx] = _mm256_div_pd(_mm256_set1_pd(1.0),prt[idx]);\n";
  }
}

void TranslationUnit::emit_avx_gibbs_computation(CodeOutStream &ost,
                                                 bool main, size_t iters)
{
  ost << "// Gibbs computation\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
  PairDelim gibbs_pair(ost);
  ost << VECTOR4 << " tk1 = " << TEMPERATURE << "[idx];\n";
  ost << VECTOR4 << " tklog = vlntemp[idx];\n";
  ost << VECTOR4 << " tk2 = _mm256_mul_pd(tk1,tk1);\n";
  ost << VECTOR4 << " tk3 = _mm256_mul_pd(tk1,tk2);\n";
  ost << VECTOR4 << " tk4 = _mm256_mul_pd(tk1,tk3);\n";
  ost << VECTOR4 << " tk5 = _mm256_mul_pd(tk1,tk4);\n";
  ost << VECTOR2 << " lower = _mm256_extractf128_pd(tk1,0);\n";
  ost << VECTOR2 << " upper = _mm256_extractf128_pd(tk1,1);\n";
  if (!unroll_loops)
  {
    unsigned spec_count = 0;
    for (std::set<Species*>::const_iterator it = species.begin();
          it != species.end(); it++)
    {
      Species *spec = *it;
      if (strcmp(spec->name,"M") == 0)
        continue;
      spec_count++;
    }
    ost << "for (int spec = 0; spec < " << spec_count << "; spec++)\n";
    PairDelim spec_pair(ost);
    ost << VECTOR4 << " _" << GIBBS << ", temp;\n";
    emit_generalized_gibbs_impl(ost, 3, 0, false, true);
    ost << GIBBS << "[spec*" << iters << "+idx] = _"
        << GIBBS << ";\n";
  }
  else
  {
    unsigned spec_idx = 0;
    for (std::set<Species*>::const_iterator it = species.begin();
          it != species.end(); it++)
    {
      Species *spec = *it;
      // Skip the third body species
      if (strcmp(spec->name,"M") == 0)
        continue;
      ost << "// Species " << spec->name << "\n";
      PairDelim spec_pair(ost);
      ost << VECTOR4 << " " GIBBS << "_" << spec->code_name <<", temp;\n";
      emit_avx_gibbs_recurse(ost, spec, 3, 0); 
      ost << GIBBS << "[" << (spec_idx*iters) << "+idx] = "
          << GIBBS << "_" << spec->code_name << ";\n";
      spec_idx++;
    }
  }
}

void TranslationUnit::emit_avx_gibbs_recurse(CodeOutStream &ost, Species *spec,
                                             int offset, int mask)
{
  if (offset == 0)
  {
    ost << "if (_mm_cvtsd_f64(lower) > "
        << spec->get_common_temperature() << ")\n";
    {
      PairDelim if_pair(ost);
      emit_avx_gibbs_impl(ost, spec, mask);
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      emit_avx_gibbs_impl(ost, spec, mask | 1);
    }
  }
  else if (offset == 1)
  {
    ost << "if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > "
        << spec->get_common_temperature() << ")\n";
    {
      PairDelim if_pair(ost);
      emit_avx_gibbs_recurse(ost, spec, offset-1, mask);
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      emit_avx_gibbs_recurse(ost, spec, offset-1, mask | (1 << offset));
    }
  }
  else if (offset == 2)
  {
    ost << "if (_mm_cvtsd_f64(upper) > "
        << spec->get_common_temperature() << ")\n";
    {
      PairDelim if_pair(ost);
      emit_avx_gibbs_recurse(ost, spec, offset-1, mask);
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      emit_avx_gibbs_recurse(ost, spec, offset-1, mask | (1 << offset));
    }
  }
  else
  {
    ost << "if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > "
        << spec->get_common_temperature() << ")\n";
    {
      PairDelim if_pair(ost);
      emit_avx_gibbs_recurse(ost, spec, offset-1, mask);
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      emit_avx_gibbs_recurse(ost, spec, offset-1, mask | (1 << offset));
    }
  }
}

void TranslationUnit::emit_avx_gibbs_impl(CodeOutStream &ost, Species *spec, int mask)
{
  ost << VECTOR4 << " coeff1 = _mm256_blend_pd(_mm256_set1_pd(" << spec->get_high_coefficient(0)
      << "),_mm256_set1_pd(" << spec->get_low_coefficient(0) << "), " << mask << ");\n"; 
  ost << GIBBS << "_" << spec->code_name << " = _mm256_mul_pd(coeff1,tk1);\n";
  if (!use_dim)
    ost << VECTOR4 << " coefflog = _mm256_set1_pd(" << (1-log(TEMPERATURE_REF)) << ");\n";
  else
    ost << VECTOR4 << " coefflog = _mm256_set1_pd(1.0);\n";
  ost << "temp = _mm256_sub_pd(coefflog,tklog);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm256_mul_pd(" << GIBBS << "_" << spec->code_name << ",temp);\n";
  ost << VECTOR4 << " coeff2 = _mm256_blend_pd(_mm256_set1_pd(" << spec->get_high_coefficient(1)
      << "),_mm256_set1_pd(" << spec->get_low_coefficient(1) << "), " << mask << ");\n";
  ost << "temp = _mm256_mul_pd(coeff2,tk2);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm256_add_pd(" << GIBBS << "_" << spec->code_name << ",temp);\n";
  ost << VECTOR4 << " coeff3 = _mm256_blend_pd(_mm256_set1_pd(" << spec->get_high_coefficient(2) 
      << "),_mm256_set1_pd(" << spec->get_low_coefficient(2) << "), " << mask << ");\n";
  ost << "temp = _mm256_mul_pd(coeff3,tk3);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm256_add_pd(" << GIBBS << "_" << spec->code_name << ",temp);\n";
  ost << VECTOR4 << " coeff4 = _mm256_blend_pd(_mm256_set1_pd(" << spec->get_high_coefficient(3) 
      << "),_mm256_set1_pd(" << spec->get_low_coefficient(3) << "), " << mask << ");\n";
  ost << "temp = _mm256_mul_pd(coeff4,tk4);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm256_add_pd(" << GIBBS << "_" << spec->code_name << ",temp);\n";
  ost << VECTOR4 << " coeff5 = _mm256_blend_pd(_mm256_set1_pd(" << spec->get_high_coefficient(4) 
      << "),_mm256_set1_pd(" << spec->get_low_coefficient(4) << ")," << mask << ");\n";
  ost << "temp = _mm256_mul_pd(coeff5,tk5);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm256_add_pd(" << GIBBS << "_" << spec->code_name << ",temp);\n";
  ost << VECTOR4 << " coeff7 = _mm256_blend_pd(_mm256_set1_pd(" << spec->get_high_coefficient(6) 
      << "),_mm256_set1_pd(" << spec->get_low_coefficient(6) << ")," << mask << ");\n";
  ost << "temp = _mm256_mul_pd(coeff7,tk1);\n";
  ost << VECTOR4 << " coeff6 = _mm256_blend_pd(_mm256_set1_pd(" << spec->get_high_coefficient(5)
      << "),_mm256_set1_pd(" << spec->get_low_coefficient(5) << ")," << mask << ");\n";
  ost << "temp = _mm256_sub_pd(coeff6,temp);\n";
  ost << GIBBS << "_" << spec->code_name << " = _mm256_add_pd(" << GIBBS << "_" << spec->code_name
      << ",temp);\n";
}                      

void TranslationUnit::emit_avx_mole_frac_computation(CodeOutStream &ost,
                                                     bool main, size_t iters)
{
  ost << "// Compute mole fractions\n";
  ost << "if (aligned<32>(" << AVMOLWT_ARRAY << ") && aligned<32>("
      << PRESSURE_ARRAY << ") && aligned<32>(" << MASS_FRAC_ARRAY << "))\n";
  {
    PairDelim if_pair(ost);
    if (main)
      ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
    else
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
    PairDelim mole_pair(ost);
    ost << VECTOR4 << " avmolwt = _mm256_load_pd(" << AVMOLWT_ARRAY << ");\n";
    ost << VECTOR4 << " pressure = _mm256_load_pd(" << PRESSURE_ARRAY << ");\n";
    ost << AVMOLWT_ARRAY << " += 4;\n";
    ost << PRESSURE_ARRAY << " += 4;\n";
    ost << VECTOR4 << " sumyow = _mm256_mul_pd(_mm256_mul_pd(" << TEMPERATURE
        << "[idx],avmolwt),_mm256_set1_pd(";
    if (use_dim)
      ost << "8.314510e+04";
    else
      ost << (8.314510e+04 * TEMPERATURE_REF / PRESSURE_REF);
    ost << "));\n";
    ost << "sumyow = _mm256_div_pd(pressure,sumyow);\n";
    ost << VECTOR4 << " mask, mass_frac;\n";
    ost << VECTOR4 << " small = _mm256_set1_pd(" << small_mole_frac << ");\n";
    for (unsigned spec_idx = 0; spec_idx < ordered_species.size(); spec_idx++)
    {
      ost << "mass_frac = _mm256_load_pd("
          << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
      ost << "mass_frac = _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[" 
          << spec_idx << "]));\n";
      ost << "mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);\n";
      ost << "mass_frac = _mm256_and_pd(mass_frac,mask);\n";
      ost << "mole_frac[" << (spec_idx*iters) << "+idx] = _mm256_mul_pd(sumyow,"
          << "_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small)));\n";
    }
  }
  ost << "else\n";
  {
    PairDelim else_pair(ost);
    if (main)
      ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
    else
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
    PairDelim mole_pair(ost);
    ost << VECTOR4 << " avmolwt = _mm256_loadu_pd(" << AVMOLWT_ARRAY << ");\n";
    ost << VECTOR4 << " pressure = _mm256_loadu_pd(" << PRESSURE_ARRAY << ");\n";
    ost << AVMOLWT_ARRAY << " += 4;\n";
    ost << PRESSURE_ARRAY << " += 4;\n";
    ost << VECTOR4 << " sumyow = _mm256_mul_pd(_mm256_mul_pd(" << TEMPERATURE
        << "[idx],avmolwt),_mm256_set1_pd(";
    if (use_dim)
      ost << "8.314510e+04";
    else
      ost << (8.314510e+04 * TEMPERATURE_REF / PRESSURE_REF);
    ost << "));\n";
    ost << "sumyow = _mm256_div_pd(pressure,sumyow);\n";
    ost << VECTOR4 << " mask, mass_frac;\n";
    ost << VECTOR4 << " small = _mm256_set1_pd(" << small_mole_frac << ");\n";
    for (unsigned spec_idx = 0; spec_idx < ordered_species.size(); spec_idx++)
    {
      ost << "mass_frac = _mm256_loadu_pd("
          << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
      ost << "mass_frac = _mm256_mul_pd(mass_frac,_mm256_set1_pd(recip_molecular_masses[" 
          << spec_idx << "]));\n";
      ost << "mask = _mm256_cmp_pd(mass_frac,small,_CMP_GT_OQ);\n";
      ost << "mass_frac = _mm256_and_pd(mass_frac,mask);\n";
      ost << "mole_frac[" << (spec_idx*iters) << "+idx] = _mm256_mul_pd(sumyow,"
          << "_mm256_add_pd(mass_frac,_mm256_andnot_pd(mask,small)));\n";
    }
  }
}

void TranslationUnit::emit_avx_third_body_computation(CodeOutStream &ost,
                                                      bool main, size_t iters)
{
  ost << "// Compute third body values\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
  PairDelim third_pair(ost);
  ost << VECTOR4 << " ctot = _mm256_set1_pd(0.0);\n";
  for (unsigned spec_idx = 0; spec_idx < ordered_species.size(); spec_idx++)
  {
    ost << "ctot = _mm256_add_pd(ctot,mole_frac[" << (spec_idx*iters) << "+idx]);\n";
  }
  for (unsigned idx = 0; idx < third_bodies.size(); idx++)
  {
    ost << THBCONC << "[" << (idx*iters) << "+idx] = ctot;\n";
    ThirdBody *thb = third_bodies[idx];
    for (std::map<Species*,double>::const_iterator it = thb->components.begin();
          it != thb->components.end(); it++)
    {
      ost << THBCONC << "[" << (idx*iters) << "+idx] = ";
      if (it->second >= 0.0)
      {
        ost << "_mm256_add_pd(" << THBCONC << "[" << (idx*iters) << "+idx],"; 
        int index = find_ordered_index(it->first);
        // Better not be a species we don't have mole frac for
        assert(index != -1);
        if (it->second != 1.0)
        {
          ost << "_mm256_mul_pd(_mm256_set1_pd(" << it->second << "),"
              << "mole_frac[" << (index*iters) << "+idx])";
        }
        else
          ost << "mole_frac[" << (index*iters) << "+idx]";
      }
      else
      {
        ost << "_mm256_sub_pd(" << THBCONC << "[" << (idx*iters) << "+idx],";
        int index = find_ordered_index(it->first);
        // Better not be a species we don't have mole frac for
        assert(index != -1);
        if (it->second != 1.0)
        {
          ost << "_mm256_mul_pd(_mm256_set1_pd(" << (-it->second) << "),"
              << "mole_frac[" << (index*iters) << "+idx])";
        }
        else
          ost << "mole_frac[" << (index*iters) << "+idx]";
      }
      ost << ");\n";
    }
  }
}

void TranslationUnit::emit_avx_reaction_rates(CodeOutStream &ost,
                                              bool main, size_t iters)
{
  ost << "// Reaction rates\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
  PairDelim reac_pair(ost);
  unsigned actual_idx = 0;
  for (unsigned idx = 0; idx < all_reactions.size(); idx++)
  {
    Reaction *reac = all_reactions[idx];
    // Skip all duplicate reactions that aren't the owner
    if (reac->is_duplicate() && reac->is_owned())
      continue;
    all_reactions[idx]->emit_avx_reaction_code(ost, actual_idx, iters); 
    actual_idx++;
  }
  // This should be true when we're done
  assert(actual_idx == act_reactions.size());
}

void TranslationUnit::emit_avx_wdot_computation(CodeOutStream &ost,
                                                bool main, size_t iters)
{
  ost << "// Output rates\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
  PairDelim for_pair(ost);
  ost << VECTOR4 << " ropl[" << act_reactions.size() << "];\n";
  for (unsigned idx = 0; idx < act_reactions.size(); idx++)
    ost << "ropl[" << idx << "] = _mm256_sub_pd(rr_f["
        << (idx*iters) << "+idx],rr_r[" << (idx*iters) << "+idx]);\n";
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
    ordered_species[idx]->emit_avx_contributions(ost, idx, iters);
}

void TranslationUnit::emit_generalized_gibbs_impl(CodeOutStream &ost, 
                                                  int offset, int mask,
                                                  bool sse, bool avx)
{
  if (offset == 0)
  {
    if (sse)
    {
      ost << "if (_mm_cvtsd_f64(tk1) > gibbs_constants[spec*15])\n";
      {
        PairDelim if_pair(ost);
        emit_generalized_sse_gibbs(ost, mask);
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        emit_generalized_sse_gibbs(ost, mask | 1);
      }
    }
    else
    {
      ost << "if (_mm_cvtsd_f64(lower) > gibbs_constants[spec*15])\n";
      {
        PairDelim if_pair(ost);
        emit_generalized_avx_gibbs(ost, mask);
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        emit_generalized_avx_gibbs(ost, mask | 1);
      }
    }
  }
  else if (offset == 1)
  {
    if (sse)
    {
      ost << "if (_mm_cvtsd_f64(_mm_shuffle_pd(tk1,tk1,1)) > gibbs_constants[spec*15])\n"; 
      {
        PairDelim if_pair(ost);
        emit_generalized_gibbs_impl(ost, offset-1, mask, sse, avx);
      }
      ost << "else\n";
      {
        PairDelim if_pair(ost);
        emit_generalized_gibbs_impl(ost, offset-1, mask | (1 << offset), sse, avx);
      }
    }
    else
    {
      ost << "if (_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) > gibbs_constants[spec*15])\n";
      {
        PairDelim if_pair(ost);
        emit_generalized_gibbs_impl(ost, offset-1, mask, sse, avx);
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        emit_generalized_gibbs_impl(ost, offset-1, mask | (1 << offset), sse, avx);
      }
    }
  }
  else if (offset == 2)
  {
    assert(avx && !sse);
    ost << "if (_mm_cvtsd_f64(upper) > gibbs_constants[spec*15])\n";
    {
      PairDelim if_pair(ost);
      emit_generalized_gibbs_impl(ost, offset-1, mask, sse, avx);
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      emit_generalized_gibbs_impl(ost, offset-1, mask | (1 << offset), sse, avx);
    }
  }
  else if (offset == 3)
  {
    assert(avx && !sse);
    ost << "if (_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)) > gibbs_constants[spec*15])\n";
    {
      PairDelim if_pair(ost);
      emit_generalized_gibbs_impl(ost, offset-1, mask, sse, avx);
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      emit_generalized_gibbs_impl(ost, offset-1, mask | (1 << offset), sse, avx);
    }
  }
  else
    assert(false);
}

void TranslationUnit::emit_generalized_sse_gibbs(CodeOutStream &ost, int mask)
{
  ost << VECTOR2 << " coeff1 = _mm_blend_pd(_mm_set1_pd(gibbs_constants[spec*15+1]),"
      << "_mm_set1_pd(gibbs_constants[spec*15+2])," << mask << ");\n";
  ost << "_" << GIBBS << " = _mm_mul_pd(coeff1,tk1);\n";
  if (!use_dim)
    ost << VECTOR2 << " coefflog = _mm_set1_pd(" << (1-log(TEMPERATURE_REF)) << ");\n";
  else
    ost << VECTOR2 << " coefflog = _mm_set1_pd(1.0);\n";
  ost << "temp = _mm_sub_pd(coefflog,tklog);\n";
  ost << "_" << GIBBS << " = _mm_mul_pd(_" << GIBBS << ",temp);\n";
  ost << VECTOR2 << " coeff2 = _mm_blend_pd(_mm_set1_pd(gibbs_constants[spec*15+3]),"
      << "_mm_set1_pd(gibbs_constants[spec*15+4])," << mask << ");\n";
  ost << "temp = _mm_mul_pd(coeff2,tk2);\n";
  ost << "_" << GIBBS << " = _mm_add_pd(_" << GIBBS << ",temp);\n";
  ost << VECTOR2 << " coeff3 = _mm_blend_pd(_mm_set1_pd(gibbs_constants[spec*15+5]),"
      << "_mm_set1_pd(gibbs_constants[spec*15+6])," << mask << ");\n";
  ost << "temp = _mm_mul_pd(coeff3,tk3);\n";
  ost << "_" << GIBBS << " = _mm_add_pd(_" << GIBBS << ",temp);\n";
  ost << VECTOR2 << " coeff4 = _mm_blend_pd(_mm_set1_pd(gibbs_constants[spec*15+7]),"
      << "_mm_set1_pd(gibbs_constants[spec*15+8])," << mask << ");\n";
  ost << "temp = _mm_mul_pd(coeff4,tk4);\n";
  ost << "_" << GIBBS << " = _mm_add_pd(_" << GIBBS << ",temp);\n";
  ost << VECTOR2 << " coeff5 = _mm_blend_pd(_mm_set1_pd(gibbs_constants[spec*15+9]),"
      << "_mm_set1_pd(gibbs_constants[spec*15+10])," << mask << ");\n";
  ost << "temp = _mm_mul_pd(coeff5,tk5);\n";
  ost << "_" << GIBBS << " = _mm_add_pd(_" << GIBBS << ",temp);\n";
  ost << VECTOR2 << " coeff7 = _mm_blend_pd(_mm_set1_pd(gibbs_constants[spec*15+11]),"
      << "_mm_set1_pd(gibbs_constants[spec*15+12])," << mask << ");\n";
  ost << "temp = _mm_mul_pd(coeff7,tk1);\n";
  ost << VECTOR2 << " coeff6 = _mm_blend_pd(_mm_set1_pd(gibbs_constants[spec*15+13]),"
      << "_mm_set1_pd(gibbs_constants[spec*15+14])," << mask << ");\n";
  ost << "temp = _mm_sub_pd(coeff6,temp);\n";
  ost << "_" << GIBBS << " = _mm_add_pd(_" << GIBBS << ",temp);\n";
}

void TranslationUnit::emit_generalized_avx_gibbs(CodeOutStream &ost, int mask)
{
  ost << VECTOR4 << " coeff1 = _mm256_blend_pd(_mm256_set1_pd(gibbs_constants[spec*15+1]),"
      << "_mm256_set1_pd(gibbs_constants[spec*15+2])," << mask << ");\n";
  ost << "_" << GIBBS << " = _mm256_mul_pd(coeff1,tk1);\n";
  if (!use_dim)
    ost << VECTOR4 << " coefflog = _mm256_set1_pd(" << (1-log(TEMPERATURE_REF)) << ");\n";
  else
    ost << VECTOR4 << " coefflog = _mm256_set1_pd(1.0);\n";
  ost << "temp = _mm256_sub_pd(coefflog,tklog);\n";
  ost << "_" << GIBBS << " = _mm256_mul_pd(_" << GIBBS << ",temp);\n";
  ost << VECTOR4 << " coeff2 = _mm256_blend_pd(_mm256_set1_pd(gibbs_constants[spec*15+3]),"
      << "_mm256_set1_pd(gibbs_constants[spec*15+4])," << mask << ");\n";
  ost << "temp = _mm256_mul_pd(coeff2,tk2);\n";
  ost << "_" << GIBBS << " = _mm256_add_pd(_" << GIBBS << ",temp);\n";
  ost << VECTOR4 << " coeff3 = _mm256_blend_pd(_mm256_set1_pd(gibbs_constants[spec*15+5]),"
      << "_mm256_set1_pd(gibbs_constants[spec*15+6])," << mask << ");\n";
  ost << "temp = _mm256_mul_pd(coeff3,tk3);\n";
  ost << "_" << GIBBS << " = _mm256_add_pd(_" << GIBBS << ",temp);\n";
  ost << VECTOR4 << " coeff4 = _mm256_blend_pd(_mm256_set1_pd(gibbs_constants[spec*15+7]),"
      << "_mm256_set1_pd(gibbs_constants[spec*15+8])," << mask << ");\n";
  ost << "temp = _mm256_mul_pd(coeff4,tk4);\n";
  ost << "_" << GIBBS << " = _mm256_add_pd(_" << GIBBS << ",temp);\n";
  ost << VECTOR4 << " coeff5 = _mm256_blend_pd(_mm256_set1_pd(gibbs_constants[spec*15+9]),"
      << "_mm256_set1_pd(gibbs_constants[spec*15+10])," << mask << ");\n";
  ost << "temp = _mm256_mul_pd(coeff5,tk5);\n";
  ost << "_" << GIBBS << " = _mm256_add_pd(_" << GIBBS << ",temp);\n";
  ost << VECTOR4 << " coeff7 = _mm256_blend_pd(_mm256_set1_pd(gibbs_constants[spec*15+11]),"
      << "_mm256_set1_pd(gibbs_constants[spec*15+12])," << mask << ");\n";
  ost << "temp = _mm256_mul_pd(coeff7,tk1);\n";
  ost << VECTOR4 << " coeff6 = _mm256_blend_pd(_mm256_set1_pd(gibbs_constants[spec*15+13]),"
      << "_mm256_set1_pd(gibbs_constants[spec*15+14])," << mask << ");\n";
  ost << "temp = _mm256_sub_pd(coeff6,temp);\n";
  ost << "_" << GIBBS << " = _mm256_add_pd(_" << GIBBS << ",temp);\n";
}

int TranslationUnit::find_ordered_reaction(Reaction *reac) const
{
  int result = 0;
  for (std::vector<Reaction*>::const_iterator it = act_reactions.begin();
        it != act_reactions.end(); it++)
  {
    if ((*it) == reac)
      return result;
    result++;
  }
  return -1;
}

int TranslationUnit::find_gibbs_index(Species *spec) const
{
  int result = 0;
  for (std::vector<Species*>::const_iterator it = gibbs_species.begin();
        it != gibbs_species.end(); it++)
  {
    if ((*it) == spec)
      return result;
    result++;
  }
  return -1;
}

int TranslationUnit::find_troe_index(Reaction *reac) const
{
  int result = 0;
  for (std::vector<Reaction*>::const_iterator it = troe_reactions.begin();
        it != troe_reactions.end(); it++)
  {
    if ((*it) == reac)
      return result;
    result++;
  }
  return -1;
}

void TranslationUnit::emit_gpu_chemistry_header_file(const char *file_name)
{
  CodeOutStream ost(file_name, true/*bound line lengths*/, 80);

  ost << "#ifndef __BASELINE_GPU_GET_RATES__\n";
  ost << "#define __BASELINE_GPU_GET_RATES__\n";
  ost << "\n";
  emit_gpu_getrates_declaration(ost);
  ost << ";\n";
  ost << "#endif // __BASELINE_GPU_GET_RATES__\n";
}

void TranslationUnit::emit_gpu_chemistry_source_file(const char *file_name, 
                                                    const char *header_name)
{
  CodeOutStream ost(file_name, true/*bound line lengths*/, 80);

  ost << "\n";
  ost << "#include \"" << header_name << "\"\n";
  ost << "\n";
  emit_macros(ost);
  ost << "\n";
  emit_gpu_mole_masses(ost);
  ost << "\n";
  emit_gpu_recip_mole_masses(ost);
  ost << "\n";
  if (emit_debug)
  {
    ost << "#include <cstdio>\n";
    ost << "\n";
  }

  if (reac_warps == 0)
    emit_baseline_gpu_getrates(ost);
  else if (warp_size < 32)
    emit_small_gpu_getrates(ost);
  else if (multi_pass_warp_specialized)
    emit_multi_gpu_getrates(ost);
  else 
    emit_warp_specialized_gpu_getrates(ost);

  ost << "\n";
}

void TranslationUnit::emit_gpu_getrates_declaration(CodeOutStream &ost)
{
  ost << "__global__ " << VOID << "\n";
  // Assuming for now we'll only get on CTA/SM
  if (reac_warps > 0)
    ost << "__launch_bounds__(" //<< ((reac_warps + ((chem == NULL) ? 0 : qss_warps)) << 5) << ","
        << (reac_warps << 5) << ","
        << (target_ctas == 0 ? 1 : target_ctas) << ")\n";
  ost << "gpu_getrates(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << PRESSURE_ARRAY;
  ost << ", const " << REAL << " *" << AVMOLWT_ARRAY;
  ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
  if (chem != NULL)
  {
    ost << ", const " << REAL << " *" << DIFFUSION_ARRAY;
    ost << ", const " << REAL << " dt";
    ost << ", const " << REAL << " recip_dt";
  }
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", const " << INT << " step_stride/*always zero*/";
  ost << ", " << REAL << " *" << WDOT_ARRAY << ")";
}

void TranslationUnit::emit_baseline_gpu_getrates(CodeOutStream &ost)
{
  emit_gpu_getrates_declaration(ost);
  ost << "\n";

  PairDelim func_pair(ost);
  ost << "\n";
  emit_getrates_numerical_constants(ost);
  ost << "\n";
  // Compute the offset and update the pointers
  {
    PairDelim off_pair(ost);
    ost << "const " << INT << " offset = "
        << "(blockIdx.x*blockDim.x + threadIdx.x);\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << AVMOLWT_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    if (chem != NULL)
      ost << DIFFUSION_ARRAY << " += offset;\n";
    ost << WDOT_ARRAY << " += offset;\n";
  }
  // Load the temperature
  ost << REAL << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,k20);
  emit_getrates_temperature_dependent(ost, NULL);
  ost << "\n";
  // Load the mass fractions, avmolwt, and the pressure
  ost << REAL << " " << MASS_FRAC << "[" << ordered_species.size() << "];\n";
  ost << REAL << " " << AVMOLWT << ";\n";
  ost << REAL << " " << PRESSURE << ";\n";
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
  {
    char dst_offset[64];
    sprintf(dst_offset,"%d",idx);
    char src_offset[64];
    sprintf(src_offset,"%d*spec_stride",idx);
    emit_cuda_load(ost,MASS_FRAC,dst_offset,MASS_FRAC_ARRAY,src_offset,k20);
  }
  emit_cuda_load(ost,AVMOLWT,AVMOLWT_ARRAY,k20);
  emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,k20);
  // Now do all the normal computations
  ost << REAL << " " << GIBBS << "[" << species.size()-1 << "];\n";
  emit_getrates_gibbs_computation(ost, NULL);
  ost << "\n";
  ost << REAL << " " << MOLE_FRAC << "[" << ordered_species.size() << "];\n";
  emit_getrates_mole_frac_computation(ost, NULL);
  ost << "\n";
  ost << REAL << " " << THBCONC << "[" << third_bodies.size() << "];\n";
  emit_getrates_third_body_computation(ost, NULL);
  ost << "\n";
  ost << REAL << " " << "rr_f" << "[" << act_reactions.size() << "];\n";
  ost << REAL << " " << "rr_r" << "[" << act_reactions.size() << "];\n";
  emit_getrates_reaction_rates(ost, NULL);
  if (emit_debug)
  {
    ost << "if ((blockIdx.x == 0) && (blockIdx.y == 0) && (blockIdx.z == 0) && (threadIdx.x==0) && (threadIdx.y==0))\n";
    PairDelim if_pair(ost);
    for (unsigned idx = 0; idx < act_reactions.size(); idx++)
    {
      ost << "printf(\"base_rr_f[" << idx << "] = %8.3g\\n\", rr_f[" << idx << "]);\n";
      ost << "printf(\"base_rr_r[" << idx << "] = %8.3g\\n\", rr_r[" << idx << "]);\n";
    }
  }
  // Handle QSSA and Stiffness code generation if necessary
  if (chem != NULL)
  {
    chem->emit_unimportant_rates(ost, NULL);
    chem->emit_qssa_modifications(ost, NULL);
    if (emit_debug)
    {
      ost << "if ((blockIdx.x == 0) && (blockIdx.y == 0) && (blockIdx.z == 0) && (threadIdx.x==0) && (threadIdx.y==0))\n";
      PairDelim if_pair(ost);
      for (unsigned idx = 0; idx < act_reactions.size(); idx++)
      {
        ost << "printf(\"base_qssa_f[" << idx << "] = %.8g\\n\", rr_f[" << idx << "]);\n";
        ost << "printf(\"base_qssa_r[" << idx << "] = %.8g\\n\", rr_r[" << idx << "]);\n";
      }
    }
    // Now we need to load the diffusion points
    ost << REAL << " " << DIFFUSION << "[" << ordered_species.size() << "];\n";
    for (unsigned idx = 0; idx < ordered_species.size(); idx++)
    {
      char dst_offset[64];
      sprintf(dst_offset,"%d",idx);
      char src_offset[64];
      sprintf(src_offset,"%d*spec_stride",idx);
      emit_cuda_load(ost,DIFFUSION,dst_offset,DIFFUSION_ARRAY,src_offset,k20);
    }
    chem->emit_stiffness_modifications(ost, NULL);
    if (emit_debug)
    {
      ost << "if ((blockIdx.x == 0) && (blockIdx.y == 0) && (blockIdx.z == 0) && (threadIdx.x==0) && (threadIdx.y==0))\n";
      PairDelim if_pair(ost);
      for (unsigned idx = 0; idx < act_reactions.size(); idx++)
      {
        ost << "printf(\"base_stif_f[" << idx << "] = %.8g\\n\", rr_f[" << idx << "]);\n";
        ost << "printf(\"base_stif_r[" << idx << "] = %.8g\\n\", rr_r[" << idx << "]);\n";
      }
    }
  }
  ost << REAL << " " << WDOT << "[" << ordered_species.size() << "];\n";
  emit_getrates_wdot_computation(ost, NULL);
  if (emit_debug)
  {
    ost << "if ((blockIdx.x == 0) && (blockIdx.y == 0) && (blockIdx.z == 0) && (threadIdx.x==0) && (threadIdx.y==0))\n";
    PairDelim if_pair(ost);
    for (unsigned idx = 0; idx < ordered_species.size(); idx++)
    {
      ost << "printf(\"base_spec[" << idx << "] = %.8g\\n\", wdot[" << idx << "]);\n";
    }
  }
  // Now write all the results back
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
  {
    char dst_offset[64];
    sprintf(dst_offset,"%d*spec_stride",idx);
    char src[64];
    sprintf(src,"%s[%d]",WDOT,idx);
    emit_cuda_store(ost,WDOT_ARRAY,dst_offset,src);
  }
}

void TranslationUnit::emit_warp_specialized_gpu_getrates(CodeOutStream &ost)
{
  // First figure out how many points we can handle and the amount of shared
  // memory that we need
  {
    int data_values = (2*all_reactions.size()+1/*identity*/);
    if (chem != NULL)
      data_values += (2*(ordered_species.size()+1/*unity*/+third_bodies.size()));
    else
      data_values += (ordered_species.size()+1/*unity*/+third_bodies.size());
    data_values += (gibbs_species.size()+1/*unity*/);
    // Multiply by the number of bytes
    int total_bytes_per_point = data_values * sizeof(REAL_TYPE);
    // Now figure out how many points we can get
    int max_points = MAX_SHARED/total_bytes_per_point;
    // Round to the nearest power of 2
    double power = log(max_points)/log(2);
    power = floor(power);
    int total_shared, total_warps;
    do {
      if (power < 0.0)
      {
        fprintf(stderr,"YOU MY FRIEND HAVE STEPPED OFF THE EDGE OF THE WORLD!\n");
        fprintf(stderr,"There are so many reactions and species that they will\n");
        fprintf(stderr,"not all fit in the on-chip shared-memory.  Warp-specialization\n");
        fprintf(stderr,"is not an option for this mechanism on current GPUs.\n");
        exit(1);
      }
      double points = pow(2.0,power);
      this->points_per_cta = int(points);
      // Cap the pointers per cta at 32
      // If we have room for more, we'll go for more occupancy
      if (points_per_cta > 32)
        points_per_cta = 32;
      total_shared = points_per_cta * total_bytes_per_point;
      total_warps = reac_warps + ((chem == NULL) ? 0 : 1);
      // We also need a mirror for each of the warps
      if (!k20)
      {
        assert((32 % points_per_cta) == 0);
        total_shared += ((total_warps * (32/points_per_cta)) * (2*sizeof(REAL_TYPE) + sizeof(INT_TYPE)));
        // Check to make sure we didn't overflow it
        if (total_shared <= MAX_SHARED)
          break; // We're good to go
        // Otherwise, decrease power by 1 and try again
        power -= 1.0;
      }
      else
      {
        // No need for a mirror for K20 GPUs with shuffle
        assert(total_shared <= MAX_SHARED);
        break;
      }
    }
    while (true);
    fprintf(stdout,"INFO: Warp-specialized getrates stats\n");
    fprintf(stdout,"  Points-per-CTA: %d\n", points_per_cta);
    fprintf(stdout,"  Shared-Memory Usage: %d bytes\n", total_shared);
    fprintf(stdout,"  Total Warps: %d\n", total_warps);
  }

  // Figure out our assignment of reactions to warps
  assign_reactions_to_warps();

  // Build the shared memory analyzer
  construct_shared_memory_analyzer();

  // Emit the compute warp block of code into a temporary file
  const char *temp_chem_name = "__temp_chem__.cu";
  {
    CodeOutStream temp_ost(temp_chem_name, true, 80);  
    emit_warp_specialized_chem_code(temp_ost);
  }
  const char *temp_qssa_name = "__temp_qssa__.cu";
  // Only need to do this if chem is not NULL
  if (chem != NULL)
  {
    CodeOutStream temp_ost(temp_qssa_name, true, 80);
    emit_warp_specialized_qssa_code(temp_ost, 0/*version*/);
  }
  else
    temp_qssa_name = NULL;

  // Emit the complete block of code into the actual output stream
  emit_warp_specialized_full_code(ost, temp_chem_name, temp_qssa_name); 

  // Clean up our temporary files
  assert(system("rm -f __temp_chem__.cu") == 0);
  if (chem != NULL)
    assert(system("rm -f __temp_qssa__.cu") == 0);
}

void TranslationUnit::assign_reactions_to_warps(void)
{
  // Make all the warps and a QSSA warp if we need one 
  int total_warps = reac_warps * (32/points_per_cta); 
  for (int idx = 0; idx < total_warps; idx++)
  {
    chem_warps.push_back(new Warp(idx));
  }
  // Also make a QSSA warp if we need one
  if (chem != NULL)
  {
    for (int idx = 0; idx < (32/points_per_cta); idx++)
      qssa_warps.push_back(new Warp(qssa_warps.size()));
  }

  // First sort reactions into weird reactions and normal reactions
  std::vector<Reaction*> weird_reacs;
  std::vector<Reaction*> normal_reacs;
  for (std::vector<Reaction*>::const_iterator it = all_reactions.begin();
        it != all_reactions.end(); it++)
  {
    // Skip any reactions that are duplicates and not the owner
    if ((*it)->is_duplicate() && (*it)->is_owned())
      continue;
    if ((*it)->is_weird_reaction())
    {
      if ((*it)->lt.enabled)
      {
        fprintf(stderr,"Not currently supporting Landau-Teller reactions for warp-specialization!  Sorry.\n");
        exit(1);
      }
      if ((*it)->duplicate.enabled)
      {
        fprintf(stderr,"Not currently supporting duplicate reactions for warp-specialization!  Sorry.\n");
        exit(1);
      }
      weird_reacs.push_back(*it);
    }
    else
      normal_reacs.push_back(*it);
  }
  unsigned index = 0;
  // Round-robin assign weird reactions to warps
  for (std::vector<Reaction*>::const_iterator it = weird_reacs.begin();
        it != weird_reacs.end(); it++)
  {
    chem_warps[index]->add_reaction(*it);
    index++;
    if (index == chem_warps.size())
      index = 0;
  }
  // Round-robin the normal reactions to warps
  for (std::vector<Reaction*>::const_iterator it = normal_reacs.begin();
        it != normal_reacs.end(); it++)
  {
    chem_warps[index]->add_reaction(*it);
    index++;
    if (index == chem_warps.size())
      index = 0;
  }
}

void TranslationUnit::construct_shared_memory_analyzer(void)
{
  // Create a shared analyzer and then figure out where
  // reactions are mapped into shared memory
  bank_analyzer = new BankAnalyzer((32/points_per_cta), all_reactions.size());
  int reverse_offset = all_reactions.size();
  for (unsigned wid = 0; wid < chem_warps.size(); wid++)
  {
    int reac_loc = wid; 
    for (std::vector<Reaction*>::const_iterator it = 
          chem_warps[wid]->reacs.begin(); it !=
          chem_warps[wid]->reacs.end(); it++)
    {
      bank_analyzer->add_forward_reac((*it),reac_loc);
      bank_analyzer->add_reverse_reac((*it),reac_loc+reverse_offset);
      // Stride by chem warps
      reac_loc += chem_warps.size();
    }
  }
}

void TranslationUnit::emit_warp_specialized_chem_code(CodeOutStream &ost)
{
  if (chem != NULL)
  {
    ost << "// Preamble\n";
    {
      PairDelim pre_pair(ost);
      ost << REAL << " rr_f[" << chem_warps[0]->reacs.size() << "];\n";
      ost << REAL << " rr_r[" << chem_warps[0]->reacs.size() << "];\n";
      emit_warp_specialized_reac_code(ost, 0/*version*/, 0/*buffer*/);
      // Sending buffer0 rates
      emit_warp_specialized_write_code(ost);
    }
    emit_barrier(ost, 0, (reac_warps+1), false/*wait*/);
    // We need a barrier here to know that everyone has finished
    // the previous chemistry batch before starting the next one
    emit_barrier(ost, 2, reac_warps, true/*wait*/);
    ost << "// Main loop\n";
    ost << "for (step = 0; step < (total_steps-2); step+=2)\n";
    {
      PairDelim for_pair(ost);
      emit_warp_specialized_update_pointers_code(ost);
      {
        PairDelim first_pair(ost);
        ost << REAL << " rr_f[" << chem_warps[0]->reacs.size() << "];\n";
        ost << REAL << " rr_r[" << chem_warps[0]->reacs.size() << "];\n";
        emit_warp_specialized_reac_code(ost, 1/*version*/, 1/*buffer*/);
        // Receiving buffer0 rates
        emit_barrier(ost, 1, (reac_warps+1), true/*wait*/);
        emit_warp_specialized_stiffness_code(ost, 0/*version*/, 0/*buffer*/);
        emit_warp_specialized_output_code(ost, 0/*version*/);
        // Make sure everyone is done writing before continuing
        emit_barrier(ost, 2, (reac_warps), true/*wait*/);
        // Sending buffer1 rates
        emit_warp_specialized_write_code(ost);
      }
      emit_barrier(ost, 0, (reac_warps+1), false/*wait*/);
      // Start the second round
      emit_warp_specialized_update_pointers_code(ost);
      {
        PairDelim second_pair(ost);
        ost << REAL << " rr_f[" << chem_warps[0]->reacs.size() << "];\n";
        ost << REAL << " rr_r[" << chem_warps[0]->reacs.size() << "];\n";
        emit_warp_specialized_reac_code(ost, 2/*version*/, 0/*buffer*/);
        // Receiving buffer1 rates
        emit_barrier(ost, 1, (reac_warps+1), true/*wait*/);
        emit_warp_specialized_stiffness_code(ost, 1/*version*/, 1/*buffer*/);
        emit_warp_specialized_output_code(ost, 1/*version*/);
        // Make sure everyone is done writing before continuing
        emit_barrier(ost, 2, (reac_warps), true/*wait*/);
        // Sending buffer0 rates
        emit_warp_specialized_write_code(ost);
      }
      emit_barrier(ost, 0, (reac_warps+1), false/*wait*/);
    }
    ost << "// Postamble\n";
    emit_warp_specialized_update_pointers_code(ost);
    {
      PairDelim post_pair(ost);
      ost << REAL << " rr_f[" << chem_warps[0]->reacs.size() << "];\n";
      ost << REAL << " rr_r[" << chem_warps[0]->reacs.size() << "];\n";
      emit_warp_specialized_reac_code(ost, 3/*version*/, 1/*buffer*/);
      // Receiving buffer0 rates
      emit_barrier(ost, 1, (reac_warps+1), true/*wait*/);
      emit_warp_specialized_stiffness_code(ost, 2/*version*/, 0/*buffer*/);
      emit_warp_specialized_output_code(ost, 2/*version*/);
      // Make sure everyone is done writing
      emit_barrier(ost, 2, (reac_warps), true/*wait*/);
      emit_warp_specialized_write_code(ost);
    }
    // Sending buffer1 rates
    emit_barrier(ost, 0, (reac_warps+1), false/*wait*/);
    // Receiving buffer1 rates
    emit_barrier(ost, 1, (reac_warps+1), true/*wait*/);
    emit_warp_specialized_stiffness_code(ost, 3/*version*/, 1/*buffer*/);
    emit_warp_specialized_output_code(ost, 3/*version*/);
  }
  else
  {
    // Look at how much easier the code is without QSSA and Stiffness
    ost << "// Main loop\n";
    ost << "for (step = 0; step < total_steps; step++)\n";
    {
      PairDelim for_pair(ost);
      ost << REAL << " rr_f[" << chem_warps[0]->reacs.size() << "];\n";
      ost << REAL << " rr_r[" << chem_warps[0]->reacs.size() << "];\n";
      emit_warp_specialized_reac_code(ost, 0/*version*/, 0/*buffer*/);
      emit_warp_specialized_write_code(ost);
      emit_barrier(ost, 0, (reac_warps), true/*wait*/);
      emit_warp_specialized_output_code(ost, 0/*version*/);
      emit_warp_specialized_update_pointers_code(ost);
      emit_barrier(ost, 0, (reac_warps), true/*wait*/);
    }
  }
}

void TranslationUnit::emit_warp_specialized_reac_code(CodeOutStream &ost,
          int version, int buffer)
{
  ost << "/* Warp specialized reaction code for buffer " << buffer << " */\n";
  PairDelim chem_pair(ost);
  // First emit loads for the quantities that we need
  {
    emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,k20);
    emit_cuda_load(ost,AVMOLWT,AVMOLWT_ARRAY,k20);
    emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,k20);
    const unsigned num_mass_fracs = (ordered_species.size()+chem_warps.size()-1)/chem_warps.size();
    const bool uniform_mass_fracs = ((ordered_species.size()%chem_warps.size()) == 0);
    ost << REAL << " local_mass_frac[" << num_mass_fracs << "];\n";
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx == (num_mass_fracs-1)))
        ost << "if ((wid+" << (idx*chem_warps.size()) << ") < " << ordered_species.size() << ")\n";
      char src_offset[128];
      sprintf(src_offset,"(wid+%ld)*spec_stride",idx*chem_warps.size());
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      emit_cuda_load(ost,"local_mass_frac",dst_offset,MASS_FRAC_ARRAY,src_offset,k20);
    }

    ost << "const " << REAL << " otc = 1.0 / " << TEMPERATURE << ";\n";
    ost << "const " << REAL << " ortc = 1.0 / (" << TEMPERATURE << " * R0c);\n";
    ost << "const " << REAL << " vlntemp = log(" << TEMPERATURE ");\n";
    ost << "const " << REAL << " prt     = PA / (R0 * " << TEMPERATURE << ");\n";
    ost << "const " << REAL << " oprt    = 1.0 / prt;\n";
    ost << REAL << " sumyow = " << TEMPERATURE << " * " << AVMOLWT << " * ";
    // Scale by 1e-3 to account for conversion in
    // the computation of avmolwt
    if (use_dim)
      ost << "8.314510e+04";
    else
      ost << (8.314510e+04 * TEMPERATURE_REF / PRESSURE_REF);
    ost << ";\n";
    ost << "sumyow = " << PRESSURE << "/sumyow;\n";
    // Scale the mass fractions and write them out to shared memory
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx == (num_mass_fracs-1)))
        ost << "if ((wid+" << (idx*chem_warps.size()) << ") < " << ordered_species.size() << ")\n";
      // Clamp the local mass frac
      ost << "local_mass_frac[" << idx << "] *= recip_molecular_masses[wid+"
          << (idx*chem_warps.size()) << "+step*step_stride];\n";
      ost << "local_mass_frac[" << idx << "] = (local_mass_frac[" << idx << "] > "
          << small_mole_frac << ") ? local_mass_frac[" << idx << "] : " << small_mole_frac << ";\n";
      ost << "buffer" << buffer << "[wid+" << (idx*chem_warps.size()) << "][tid] = "
          << "local_mass_frac[" << idx << "] * sumyow;\n";
    }
    // Indicate that everyone is done writing out values
    emit_barrier(ost, 2, reac_warps, true/*blocking*/);
    if (reac_warps < 2)
    {
      fprintf(stderr,"ERROR: We need at least two reaction warps for warp-specialization!\n");
      exit(1);
    }
    // Pull off the first warp and have it compute the third-body values
    // Use the remaining warps to compute the gibbs values
    ost << "if ((threadIdx.x >> 5) == 0)\n";
    {
      PairDelim third_body_pair(ost);
      ost << REAL << " ctot = 0.0;\n";
      const unsigned thb_fracs = (ordered_species.size()+(32/points_per_cta)-1)/(32/points_per_cta);
      const bool uniform_thb_fracs = ((ordered_species.size()%(32/points_per_cta)) == 0);
      for (unsigned idx = 0; idx < thb_fracs; idx++)
      {
        if (!uniform_thb_fracs && (idx == (thb_fracs-1)))
          ost << "if ((wid+" << (idx*(32/points_per_cta)) << ") < " << ordered_species.size() << ")\n";
        ost << "ctot += buffer" << buffer << "[wid+" << (idx*(32/points_per_cta)) << "][tid];\n";
      }
      // Reduce ctot across all the sub-warps
      if (k20)
      {
        if (points_per_cta < 32)
          ost << INT << " hi_part,lo_part;\n";
        for (int i = points_per_cta; i < 32; i*=2)
        {
          ost << "hi_part = __shfl_xor(__double2hiint(ctot)," << i << ",32);\n"; 
          ost << "lo_part = __shfl_xor(__double2loint(ctot)," << i << ",32);\n";
          ost << "ctot += __hiloint2double(hi_part,lo_part);\n";
        }
      }
      else
      {
        char mask[64];
        sprintf(mask,"0x%x",(32/points_per_cta)-1);
        // Here we use warp-synchronous reductions linearly
        for (int idx = 1; idx < (32/points_per_cta); idx++)
        {
          ost << "if ((wid & " << mask << ") == " << idx << ")\n";
          ost << "  reduc_mirror[0][tid] = ctot;\n";
          ost << "if ((wid & " << mask << ") == 0)\n";
          ost << "  ctot += reduc_mirror[0][tid];\n";
        }
        // Then scatter back out so everyone has the same value
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  reduc_mirror[0][tid] = ctot;\n";
        ost << "ctot = reduc_mirror[0][tid];\n";
      }
      // Everybdoy now has the same value of ctot
      for (unsigned idx = 0; idx < third_bodies.size(); idx++)
      {
        ost << REAL << " thb_" << idx << " = ctot";
        for (std::map<Species*,double>::const_iterator it = third_bodies[idx]->components.begin();
              it != third_bodies[idx]->components.end(); it++)
        {
          int index = find_ordered_index(it->first);
          if (index != -1)
            ost << " + " << it->second << "*buffer" << buffer << "[" << index << "][tid]";
        }
        ost << ";\n";
        ost << "if (wid == 0)\n";
        ost << "  buffer" << buffer << "[" << (ordered_species.size()+idx) << "][tid] = thb_" << idx << ";\n";
      }
    }
    ost << "else\n";
    {
      PairDelim gibbs_pair(ost);
      ost << "const " << REAL << " &tk1 = " << TEMPERATURE << ";\n";
      ost << "const " << REAL << " &tklog = vlntemp;\n";
      ost << REAL << " tk2 = tk1 * tk1;\n";
      ost << REAL << " tk3 = tk1 * tk2;\n";
      ost << REAL << " tk4 = tk1 * tk3;\n";
      ost << REAL << " tk5 = tk1 * tk4;\n";
      const unsigned gibbs_warps = chem_warps.size() - (32/points_per_cta);
      const unsigned gibbs_steps = (gibbs_species.size() + gibbs_warps-1)/gibbs_warps;
      const unsigned uniform_gibbs = ((gibbs_species.size()%gibbs_warps) == 0);
      for (unsigned step = 0; step < gibbs_steps; step++)
      {
        if (!uniform_gibbs && (step == (gibbs_steps-1)))
          ost << "if ((wid+" << (int(step*gibbs_warps)-(32/points_per_cta)) << ") < " << gibbs_species.size() << ")\n";
        PairDelim step_pair(ost);
        ost << INT << " index = wid+" << (int(step*gibbs_warps)-(32/points_per_cta)) << ";\n";
        ost << "if (tk1 > gibbs_temp[index])\n";
        {
          PairDelim if_pair(ost); 
          ost << "gibbs[index][tid] = ";
          ost << "gibbs_high[index][0]*tk1*(";
          if (use_dim)
            ost << "1";
          else
            ost << (1-log(TEMPERATURE_REF));
          ost << "-tklog) + ";
          ost << "gibbs_high[index][1]*tk2 + ";
          ost << "gibbs_high[index][2]*tk3 + ";
          ost << "gibbs_high[index][3]*tk4 + ";
          ost << "gibbs_high[index][4]*tk5 + ";
          ost << "(";
          ost << "gibbs_high[index][5] - ";
          ost << "tk1*gibbs_high[index][6]);\n";
        }
        ost << "else\n";
        {
          PairDelim else_pair(ost);
          ost << "gibbs[index][tid] = ";
          ost << "gibbs_low[index][0]*tk1*(";
          if (use_dim)
            ost << "1";
          else
            ost << (1-log(TEMPERATURE_REF));
          ost << "-tklog) + ";
          ost << "gibbs_low[index][1]*tk2 + ";
          ost << "gibbs_low[index][2]*tk3 + ";
          ost << "gibbs_low[index][3]*tk4 + ";
          ost << "gibbs_low[index][4]*tk5 + ";
          ost << "(";
          ost << "gibbs_low[index][5] - ";
          ost << "tk1*gibbs_low[index][6]);\n";
        }
      }
    }
    // Barrier when we're done here since we're now ready to start computing reaction rates
    emit_barrier(ost, 2, reac_warps, true/*blocking*/);
  }
  // Now emit the reactions
  for (unsigned level = 0; level < chem_warps[0]->reacs.size(); level++)
  {
    emit_warp_specialized_chemistry_level(ost, level, buffer, version);
  }
}

void TranslationUnit::emit_warp_specialized_chemistry_level(CodeOutStream &ost, int level, int buffer, int version)
{
  ost << "/* Chemistry for level " << level << " */\n";
  int last_warp = -1;
  for (unsigned idx = 0; idx < chem_warps.size(); idx++)
  {
    if (level < int(chem_warps[idx]->reacs.size()))
    {
      last_warp = idx;
    }
  }
  assert(last_warp != -1);
  if (last_warp < int(chem_warps.size()-1))
    ost << "if (wid < " << (last_warp+1) << ")\n";
  PairDelim level_pair(ost);
  // For all reactions, regardless of weirdness, we want to set up the constants and do the two 
  // initial exponent computations
  int meta_indexes[6];
  for (int idx = 0; idx < 6; idx++)
    meta_indexes[idx] = -1;

  for (int wid = 0; wid < int(chem_warps.size()); wid++)
  {
    Reaction *reac = chem_warps[wid]->reacs[level];
    double coeffs[6];
    if (wid <= last_warp)
    {
      if (reac->low.enabled)
      {
        coeffs[0] = reac->low.beta;
        coeffs[1] = -reac->low.e;
        if (use_dim)
          coeffs[2] = reac->low.a;
        else
          coeffs[2] = reac->low.a*REACTION_RATE_REF*pow(TEMPERATURE_REF,reac->low.beta);
        coeffs[3] = reac->beta;
        coeffs[4] = -reac->e;
        if (use_dim)
          coeffs[5] = reac->a;
        else
          coeffs[5] = reac->a*REACTION_RATE_REF*pow(TEMPERATURE_REF,reac->beta);
      }
      else
      {
        coeffs[0] = reac->beta;
        coeffs[1] = -reac->e;
        if (use_dim)
          coeffs[2] = reac->a;
        else
          coeffs[2] = reac->a*REACTION_RATE_REF*pow(TEMPERATURE_REF,reac->beta);
        if (reac->reversible)
        {
          assert(reac->rev.enabled);
          coeffs[3] = reac->rev.beta;
          coeffs[4] = -reac->rev.e;
          if (use_dim)
            coeffs[5] = reac->rev.a;
          else
            coeffs[5] = reac->rev.a*REACTION_RATE_REF*pow(TEMPERATURE_REF,reac->rev.beta);
        }
        else
        {
          coeffs[3] = 0.0;
          coeffs[4] = 0.0;
          coeffs[5] = 0.0;
        }
      }
    }
    else
    {
      for (int idx = 0; idx < 6; idx++)
        coeffs[idx] = 0.0;
    }
    for (int idx = 0; idx < 6; idx++)
    {
      int global_idx = chem_warps[wid]->add_real(coeffs[idx], version);
      if (meta_indexes[idx] == -1)
        meta_indexes[idx] = global_idx;
      else
        assert(meta_indexes[idx] == global_idx);
    }
  }
  // Now we can emit the code for the first two arrhenius operations
  if (k20)
  {
    // First arrhenius
    {
      PairDelim arr_pair(ost);
      ost << INT << " hi_part, lo_part;\n";
      int index_offset = meta_indexes[0]/points_per_cta;
      int index_thread = meta_indexes[0]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << REAL << " arrhenius = __hiloint2double(hi_part,lo_part) * vlntemp;\n";
      index_offset = meta_indexes[1]/points_per_cta;
      index_thread = meta_indexes[1]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "arrhenius = __fma_rn(__hiloint2double(hi_part,lo_part), ortc, arrhenius);\n";
      ost << "arrhenius = exp(arrhenius);\n";
      index_offset = meta_indexes[2]/points_per_cta;
      index_thread = meta_indexes[2]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "rr_f[" << level << "] = __hiloint2double(hi_part,lo_part) * arrhenius;\n";
    }
    // Second arrhenius
    {
      PairDelim arr_pair(ost);
      ost << INT << " hi_part, lo_part;\n";
      int index_offset = meta_indexes[3]/points_per_cta;
      int index_thread = meta_indexes[3]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << REAL << " arrhenius = __hiloint2double(hi_part,lo_part) * vlntemp;\n";
      index_offset = meta_indexes[4]/points_per_cta;
      index_thread = meta_indexes[4]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "arrhenius = __fma_rn(__hiloint2double(hi_part,lo_part), ortc, arrhenius);\n";
      ost << "arrhenius = exp(arrhenius);\n";
      index_offset = meta_indexes[5]/points_per_cta;
      index_thread = meta_indexes[5]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "rr_r[" << level << "] = __hiloint2double(hi_part,lo_part) * arrhenius;\n";
    }
  }
  else
  {
    // First arrhenius
    {
      PairDelim arr_pair(ost);
      int index_offset = meta_indexes[0]/points_per_cta;
      int index_thread = meta_indexes[0]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << REAL << " arrhenius = real_mirror[wid] * vlntemp;\n";
      index_offset = meta_indexes[1]/points_per_cta;
      index_thread = meta_indexes[1]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << "arrhenius = __fma_rn(real_mirror[wid], ortc, arrhenius);\n";
      ost << "arrhenius = exp(arrhenius);\n";
      index_offset = meta_indexes[2]/points_per_cta;
      index_thread = meta_indexes[2]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << "rr_f[" << level << "] = real_mirror[wid] * arrhenius;\n";
    }
    // Second arrhenius
    {
      PairDelim arr_pair(ost);
      int index_offset = meta_indexes[3]/points_per_cta;
      int index_thread = meta_indexes[3]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << REAL << " arrhenius = real_mirror[wid] * vlntemp;\n";
      index_offset = meta_indexes[4]/points_per_cta;
      index_thread = meta_indexes[4]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << "arrhenius = __fma_rn(real_mirror[wid], ortc, arrhenius);\n";
      ost << "arrhenius = exp(arrhenius);\n";
      index_offset = meta_indexes[5]/points_per_cta;
      index_thread = meta_indexes[5]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << "rr_r[" << level << "] = real_mirror[wid] * arrhenius;\n";
    }
  }
  // Now emit code for any weird reactions
  {
    bool first_weird = true;
    for (int wid = 0; wid <= last_warp; wid++)
    {
      Reaction *reac = chem_warps[wid]->reacs[level];
      if (reac->is_weird_reaction())
      {
        if (first_weird)
          ost << "if (wid == " << wid << ")\n";
        else
          ost << "else if (wid == " << wid << ")\n";
        PairDelim weird_pair(ost);
        reac->emit_warp_specialized_weird_reaction(ost, level, buffer);
        first_weird = false;
      }
    }
  }
  // Finally scale all the reaction rates by the molar values
  {
    int max_forward = -1;
    int max_reverse = -1;
    for (int wid = 0; wid <= last_warp; wid++)
    {
      Reaction *reac = chem_warps[wid]->reacs[level];
      int num_forward = 0;
      int num_backward = 0;
      for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
            it != reac->forward.end(); it++)
      {
        assert(it->second > 0);
        if (it->first->qss_species)
          continue;
        num_forward += it->second;
        // Nasty corner case here, if this is a low reaction
        // then it also needs to scale the reverse rate by
        // the forward mole fractions as well
        if (reac->low.enabled)
          num_backward += it->second;
      }
      if ((reac->thb != NULL) && !reac->pressure_dep)
        num_forward++;
      if (num_forward > max_forward)
        max_forward = num_forward;
      if (reac->reversible)
      {
        for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
              it != reac->backward.end(); it++)
        {
          assert(it->second > 0);
          if (it->first->qss_species)
            continue;
          num_backward += it->second;
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
          num_backward++;
      }
      if (num_backward > max_reverse)
        max_reverse = num_backward;
    }
    // Set up the indexes for each of the warps 
    std::vector<int> forward_meta(max_forward, -1);
    std::vector<int> reverse_meta(max_reverse, -1);
    const int unity_index = ordered_species.size() + third_bodies.size();
    for (int wid = 0; wid < int(chem_warps.size()); wid++)
    {
      if (wid <= last_warp)
      {
        Reaction *reac = chem_warps[wid]->reacs[level];
        int forward_idx = 0;
        for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
              it != reac->forward.end(); it++)
        {
          assert(it->second > 0);
          if (it->first->qss_species)
            continue;
          int index = find_ordered_index(it->first);
          assert(index != -1);
          for (int idx = 0; idx < it->second; idx++)
          {
            int global_idx = chem_warps[wid]->add_index(index, version);
            if (forward_meta[forward_idx] == -1)
              forward_meta[forward_idx] = global_idx;
            else
              assert(forward_meta[forward_idx] == global_idx);
            forward_idx++;
          }
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          int index = ordered_species.size() + reac->thb->idx;
          int global_idx = chem_warps[wid]->add_index(index, version);
          if (forward_meta[forward_idx] == -1)
            forward_meta[forward_idx] = global_idx;
          else
            assert(forward_meta[forward_idx] == global_idx);
          forward_idx++;
        }
        // Handle any open slots by pointing them at the unity value
        while (forward_idx < int(forward_meta.size()))
        {
          int global_idx = chem_warps[wid]->add_index(unity_index, version);
          if (forward_meta[forward_idx] == -1)
            forward_meta[forward_idx] = global_idx;
          else
            assert(forward_meta[forward_idx] == global_idx);
          forward_idx++;
        }
        int reverse_idx = 0;
        // First check to see if it is a low reaction which also needs
        // the forward mole fraction values applied to the reverse rate
        if (reac->low.enabled)
        {
          for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
                it != reac->forward.end(); it++)
          {
            assert(it->second > 0);
            if (it->first->qss_species)
              continue;
            int index = find_ordered_index(it->first);
            assert(index != -1);
            for (int idx = 0; idx < it->second; idx++)
            {
              int global_idx = chem_warps[wid]->add_index(index, version);
              if (reverse_meta[reverse_idx] == -1)
                reverse_meta[reverse_idx] = global_idx;
              else
                assert(reverse_meta[reverse_idx] == global_idx);
              reverse_idx++;
            }
          }
        }
        if (reac->reversible)
        {
          for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                it != reac->backward.end(); it++)
          {
            assert(it->second > 0);
            if (it->first->qss_species)
              continue;
            int index = find_ordered_index(it->first);
            assert(index != -1);
            for (int idx = 0; idx < it->second; idx++)
            {
              int global_idx = chem_warps[wid]->add_index(index, version);
              if (reverse_meta[reverse_idx] == -1)
                reverse_meta[reverse_idx] = global_idx;
              else
                assert(reverse_meta[reverse_idx] == global_idx);
              reverse_idx++;
            }
          }
          if ((reac->thb != NULL) && !reac->pressure_dep)
          {
            int index = ordered_species.size() + reac->thb->idx;
            int global_idx = chem_warps[wid]->add_index(index, version);
            if (reverse_meta[reverse_idx] == -1)
              reverse_meta[reverse_idx] = global_idx;
            else
              assert(reverse_meta[reverse_idx] == global_idx);
            reverse_idx++;
          }
        }
        // Handle any leftover open spots
        while (reverse_idx < int(reverse_meta.size()))
        {
          int global_idx = chem_warps[wid]->add_index(unity_index, version);
          if (reverse_meta[reverse_idx] == -1)
            reverse_meta[reverse_idx] = global_idx;
          else
            assert(reverse_meta[reverse_idx] == global_idx);
          reverse_idx++;
        }
      }
      else
      {
        // No reaction here so make everything point to the unity value
        for (unsigned idx = 0; idx < forward_meta.size(); idx++)
        {
          int global_idx = chem_warps[wid]->add_index(unity_index, version);
          if (forward_meta[idx] == -1)
            forward_meta[idx] = global_idx;
          else
            assert(forward_meta[idx] == global_idx);
        }
        for (unsigned idx = 0; idx < reverse_meta.size(); idx++)
        {
          int global_idx = chem_warps[wid]->add_index(unity_index, version);
          if (reverse_meta[idx] == -1)
            reverse_meta[idx] = global_idx;
          else
            assert(reverse_meta[idx] == global_idx);
        }
      }
    }
    ost << INT << " index;\n";
    // Now we get to emit the code for doing the updates
    for (unsigned idx = 0; idx < forward_meta.size(); idx++)
    {
      int index_offset = forward_meta[idx]/points_per_cta;
      int index_thread = forward_meta[idx]%points_per_cta;  
      if (k20)
      {
        ost << "index = __shfl(index_constants[" << index_offset << "],"
            << index_thread << "," << points_per_cta << ");\n";
      }
      else
      {
        ost << "if (tid == " << index_thread << ")\n";
        ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
        ost << "index = int_mirror[wid];\n";
      }
      ost << "rr_f[" << level << "] *= buffer" << buffer << "[index][tid];\n";
    }
    for (unsigned idx = 0; idx < reverse_meta.size(); idx++)
    {
      int index_offset = reverse_meta[idx]/points_per_cta;
      int index_thread = reverse_meta[idx]%points_per_cta;
      if (k20)
      {
        ost << "index = __shfl(index_constants[" << index_offset << "],"
            << index_thread << "," << points_per_cta << ");\n";
      }
      else
      {
        ost << "if (tid == " << index_thread << ")\n";
        ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
        ost << "index = int_mirror[wid];\n";
      }
      ost << "rr_r[" << level << "] *= buffer" << buffer << "[index][tid];\n";
    }
  }
}

void TranslationUnit::emit_warp_specialized_write_code(CodeOutStream &ost)
{
  ost << "/* Warp specialized store code */\n";
  const unsigned actual_reactions = act_reactions.size();
  // Write out the reaction rates into shared memory
  const unsigned total_writes = (actual_reactions+chem_warps.size()-1)/chem_warps.size();
  // Sanity check
  assert(total_writes == (chem_warps[0]->reacs.size()));
  const bool uniform_writes = ((actual_reactions%chem_warps.size()) == 0);
  for (unsigned idx = 0; idx < total_writes; idx++)
  {
    if (!uniform_writes && (idx == (total_writes-1)))
      ost << "if ((wid+" << (idx*chem_warps.size()) << ") < " << actual_reactions << ")\n";
    PairDelim write_pair(ost);
    ost << "rrs[wid+" << (idx*chem_warps.size()) << "][tid] = rr_f[" << idx << "];\n";
    ost << "rrs[wid+" << (idx*chem_warps.size()+actual_reactions) << "][tid] = rr_r[" << idx << "];\n";
  }
}

void TranslationUnit::emit_warp_specialized_stiffness_code(CodeOutStream &ost, int version, int buffer)
{
  assert(chem != NULL);
  ost << "/* Warp specialized stiffness code for buffer " << buffer << " */\n";
  const unsigned num_stif = chem->stif_operations.size();
  const unsigned num_specs = ((num_stif+reac_warps-1)/reac_warps);
  const unsigned uniform_specs = ((num_stif%reac_warps) == 0);
  std::vector<Stif*> output_order;
  {
    std::set<unsigned> used;
    std::vector<Stif*> batch;
    bool even = false;
    while (used.size() < chem->stif_operations.size())
    {
      while ((int(batch.size()) < reac_warps) && 
             (used.size() < chem->stif_operations.size()))
      {
        int max_contributions = -1;
        int max_index = -1;
        for (unsigned idx = 0; idx < chem->stif_operations.size(); idx++)
        {
          if (used.find(idx) != used.end())
            continue;
          Stif *op = chem->stif_operations[idx];
          int contributions = op->forward_d.size() + op->backward_d.size() +
                              op->forward_c.size() + op->backward_c.size();
          if (contributions > max_contributions)
          {
            max_contributions = contributions;
            max_index = idx;
          }
        }
        assert(max_index != -1);
        batch.push_back(chem->stif_operations[max_index]);
        used.insert(max_index);
      }
      // We've got a batch, so add it to the order
      if (even)
      {
        for (std::vector<Stif*>::const_iterator it = batch.begin();
              it != batch.end(); it++)
        {
          output_order.push_back(*it);
        }
        even = false;
      }
      else
      {
        for (std::vector<Stif*>::const_reverse_iterator it = batch.rbegin();
              it != batch.rend(); it++)
        {
          output_order.push_back(*it);
        }
        even = true;
      }
      batch.clear();
    }
  }
  std::vector<Stif*>::const_iterator spec_it = output_order.begin();
  for (unsigned level = 0; level < num_specs; level++)
  {
    ost << "// Stiffness for level " << level << "\n";
    if (!uniform_specs && (level == (num_specs-1)))
      ost << "if (((threadIdx.x >> 5)+" << (level*reac_warps) << ") < " << chem->stif_operations.size() << ")\n";
    PairDelim output_pair(ost);
    std::vector<std::vector<std::vector<int> > > ddot_assignments(reac_warps);
    std::vector<std::vector<std::vector<int> > > cdot_assignments(reac_warps);
    int max_d_steps = -1;
    int max_c_steps = -1;
    std::vector<Stif*> local_species; 
    for (int wid = 0; wid < reac_warps; wid++)
    {
      std::vector<std::vector<int> > &ddot_assignment = ddot_assignments[wid];
      std::vector<std::vector<int> > &cdot_assignment = cdot_assignments[wid];
      if (spec_it == output_order.end())
      {
        ddot_assignment.resize(32/points_per_cta);
        cdot_assignment.resize(32/points_per_cta);
        continue;
      }
      std::vector<int> ddot_locations;
      std::vector<int> cdot_locations;
      Stif *spec = *spec_it;
      local_species.push_back(spec);
      spec_it++;
      // Get the locations needed for each of the computations
      for (std::map<Reaction*,int>::const_iterator it = spec->forward_d.begin();
            it != spec->forward_d.end(); it++)
      {
        assert(it->second > 0);
        int loc = bank_analyzer->find_forward_location(it->first);
        for (int idx = 0; idx < it->second; idx++)
          ddot_locations.push_back(loc);
      }
      for (std::map<Reaction*,int>::const_iterator it = spec->backward_d.begin();
            it != spec->backward_d.end(); it++)
      {
        assert(it->second > 0);
        int loc = bank_analyzer->find_reverse_location(it->first);
        for (int idx = 0; idx < it->second; idx++)
          ddot_locations.push_back(loc);
      }
      for (std::map<Reaction*,int>::const_iterator it = spec->forward_c.begin();
            it != spec->forward_c.end(); it++)
      {
        assert(it->second > 0);
        int loc = bank_analyzer->find_forward_location(it->first);
        for (int idx = 0; idx < it->second; idx++)
          cdot_locations.push_back(loc);
      }
      for (std::map<Reaction*,int>::const_iterator it = spec->backward_c.begin();
            it != spec->backward_c.end(); it++)
      {
        assert(it->second > 0);
        int loc = bank_analyzer->find_reverse_location(it->first);
        for (int idx = 0; idx < it->second; idx++)
          cdot_locations.push_back(loc);
      }
      bank_analyzer->perform_assignment(ddot_locations, ddot_assignment);
      for (unsigned idx = 0; idx < ddot_assignment.size(); idx++)
      {
        int steps = ddot_assignment[idx].size();
        if (steps > max_d_steps)
          max_d_steps = steps;
      }
      bank_analyzer->perform_assignment(cdot_locations, cdot_assignment);
      for (unsigned idx = 0; idx < cdot_assignment.size(); idx++)
      {
        int steps = cdot_assignment[idx].size();
        if (steps > max_c_steps)
          max_c_steps = steps;
      }
    }
    ost << REAL << " ddot = 0.0;\n";
    ost << INT << " index;\n";
    ost << REAL << " d_rr[" << max_d_steps << "];\n";
    // Store the meta indexes and masks so we can replay
    // them for the code to update the local ddot inputs
    std::vector<int> local_meta_indexes;
    std::vector<int> local_masks;
    // do the computation for ddot
    for (int step = 0; step < max_d_steps; step++)
    {
      // For each warp, set the index location and get back
      // the meta-index.  Make sure all the meta-index values
      // are the same.
      int meta_index = -1;
      unsigned valid_mask = 0;
      bool needs_mask = false;
      for (int wid = 0; wid < reac_warps; wid++)
      {
        assert((32/points_per_cta) == int(ddot_assignments[wid].size()));
        bool local_mask = true;
        for (unsigned idx = 0; idx < ddot_assignments[wid].size(); idx++)
        {
          // Check to see if we have an index
          int global_idx;
          int local_wid = wid*(32/points_per_cta)+idx;
          if (step < int(ddot_assignments[wid][idx].size()))
          {
            int index = ddot_assignments[wid][idx][step];
            Warp *w = chem_warps[local_wid];
            global_idx = w->add_index(index, version);
            // As long as we have something to do then
            // we don't need to be masked off
            local_mask = false;
          }
          else
          {
            // We need the identity index 
            global_idx = chem_warps[local_wid]->add_index(2*act_reactions.size(), version);
          }
          // Check to make sure all the warps on the same page
          if (meta_index == -1)
            meta_index = global_idx;
          else
            assert(meta_index == global_idx);
        }
        if (local_mask)
          needs_mask = true;
        else
          valid_mask += (1 << wid);
      }
      // Check to see if we need to worry about the mask here
      // Check to see if we need a condition on this reduction
      if (needs_mask)
      {
        char mask_string[64];
        sprintf(mask_string,"0x%x",valid_mask);
        ost << "if ((1 << (threadIdx.x >> 5)) & " << mask_string << ")\n";
        local_masks.push_back(valid_mask);
      }
      else
        local_masks.push_back(0);
      PairDelim reduc_pair(ost);
      // Figure out where to get the index from
      local_meta_indexes.push_back(meta_index);
      int index_offset = meta_index/points_per_cta;
      int index_thread = meta_index%points_per_cta;
      if (k20)
        ost << "index = __shfl(index_constants[" << index_offset << "]," 
            << index_thread << "," << points_per_cta << ");\n";
      else
      {
        // Rely on warp-synchronous programming
        ost << "if (tid == " << index_thread << ")\n";
        ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
        ost << "index = int_mirror[wid];\n";
      }
      ost << "d_rr[" << step << "] = rrs[index][tid];\n";
      ost << "ddot += d_rr[" << step << "];\n";
    }
    // Now we've got our local ddot value, reduce it with everyone else in the warp
    if (k20)
    {
      ost << INT << " hi_part,lo_part;\n";
      for (int i = points_per_cta; i < 32; i*=2)
      {
        ost << "hi_part = __shfl_xor(__double2hiint(ddot)," << i << ",32);\n"; 
        ost << "lo_part = __shfl_xor(__double2loint(ddot)," << i << ",32);\n";
        ost << "ddot += __hiloint2double(hi_part,lo_part);\n";
      }
    }
    else
    {
      char mask[64];
      sprintf(mask,"0x%x",(32/points_per_cta)-1);
      // Here we use warp-synchronous reductions linearly
      for (int idx = 1; idx < (32/points_per_cta); idx++)
      {
        ost << "if ((wid & " << mask << ") == " << idx << ")\n";
        ost << "  reduc_mirror[(threadIdx.x >> 5)][tid] = ddot;\n";
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  ddot += reduc_mirror[(threadIdx.x >> 5)][tid];\n";
      }
      // Then scatter back out so everyone has the same value
      ost << "if ((wid & " << mask << ") == 0)\n";
      ost << "  reduc_mirror[(threadIdx.x >> 5)][tid] = ddot;\n";
      ost << "ddot = reduc_mirror[(threadIdx.x >> 5)][tid];\n";
    }
    // Now we need to get the index for the mole fraction that we need
    {
      int meta_index = -1;
      for (int wid = 0; wid < reac_warps; wid++)
      {
        if (wid < int(local_species.size()))
        {
          Stif *spec = local_species[wid];
          int mole_frac_index = find_ordered_index(spec->species);
          assert(mole_frac_index != -1);
          for (int idx = 0; idx < (32/points_per_cta); idx++)
          {
            int local_wid = wid*(32/points_per_cta)+idx;  
            int global_idx = chem_warps[local_wid]->add_index(mole_frac_index,version);
            if (meta_index == -1)
              meta_index = global_idx;
            else
              assert(meta_index == global_idx);
          }
        }
        else
        {
          for (int idx = 0; idx < (32/points_per_cta); idx++)
          {
            int local_wid = wid*(32/points_per_cta)+idx;  
            int global_idx = chem_warps[local_wid]->add_index(0,version);
            if (meta_index == -1)
              meta_index = global_idx;
            else
              assert(meta_index == global_idx);
          }
        }
      }
      int index_offset = meta_index/points_per_cta;
      int index_thread = meta_index%points_per_cta;
      if (k20)
        ost << "index = __shfl(index_constants[" << index_offset << "]," 
            << index_thread << "," << points_per_cta << ");\n";
      else
      {
        // Rely on warp-synchronous programming
        ost << "if (tid == " << index_thread << ")\n";
        ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
        ost << "index = int_mirror[wid];\n";
      }
      ost << REAL << " local_mole_frac = buffer" << buffer << "[index][tid];\n";
    }
    // Now do the comparison, note we'll do the math if any thread meets the condition
    // so that we have all threads in the warp available for exchanging indexes
    ost << "if (__any((ddot * dt * " << (1.0/REACTION_RATE_REF) << ") > local_mole_frac))\n";
    {
      PairDelim if_pair(ost);
      // Emit the load for the diffusion value that we need
      ost << REAL << " " << DIFFUSION << ";\n";
      emit_cuda_load(ost,DIFFUSION,DIFFUSION_ARRAY,"index*spec_stride",k20);
      ost << REAL << " recip_mass = recip_molecular_masses[index];\n";
      // Now we need to compute cdot
      ost << REAL << " cdot = 0.0;\n";
      for (int step = 0; step < max_c_steps; step++)
      {
        // For each warp, set the index location and get back
        // the meta-index.  Make sure all the meta-index values
        // are the same.
        int meta_index = -1;
        unsigned valid_mask = 0;
        bool needs_mask = false;
        for (int wid = 0; wid < reac_warps; wid++)
        {
          assert((32/points_per_cta) == int(cdot_assignments[wid].size()));
          bool local_mask = true;
          for (unsigned idx = 0; idx < cdot_assignments[wid].size(); idx++)
          {
            // Check to see if we have an index
            int global_idx;
            int local_wid = wid*(32/points_per_cta)+idx;
            if (step < int(cdot_assignments[wid][idx].size()))
            {
              int index = cdot_assignments[wid][idx][step];
              Warp *w = chem_warps[local_wid];
              global_idx = w->add_index(index, version);
              // As long as we have something to do then
              // we don't need to be masked off
              local_mask = false;
            }
            else
            {
              // We need the identity index 
              global_idx = chem_warps[local_wid]->add_index(2*act_reactions.size(), version);
            }
            // Check to make sure all the warps on the same page
            if (meta_index == -1)
              meta_index = global_idx;
            else
              assert(meta_index == global_idx);
          }
          if (local_mask)
            needs_mask = true;
          else
            valid_mask += (1 << wid);
        }
        // Check to see if we need to worry about the mask here
        // Check to see if we need a condition on this reduction
        if (needs_mask)
        {
          char mask_string[64];
          sprintf(mask_string,"0x%x",valid_mask);
          ost << "if ((1 << (threadIdx.x >> 5)) & " << mask_string << ")\n";
        }
        PairDelim reduc_pair(ost);
        // Figure out where to get the index from
        int index_offset = meta_index/points_per_cta;
        int index_thread = meta_index%points_per_cta;
        if (k20)
          ost << "index = __shfl(index_constants[" << index_offset << "]," 
              << index_thread << "," << points_per_cta << ");\n";
        else
        {
          // Rely on warp-synchronous programming
          ost << "if (tid == " << index_thread << ")\n";
          ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
          ost << "index = int_mirror[wid];\n";
        }
        ost << "cdot += rrs[index][tid];\n";
      }
      // Now we've got our local cdot value, reduce it with everyone else in the warp
      if (k20)
      {
        ost << INT << " hi_part,lo_part;\n";
        for (int i = points_per_cta; i < 32; i*=2)
        {
          ost << "hi_part = __shfl_xor(__double2hiint(cdot)," << i << ",32);\n"; 
          ost << "lo_part = __shfl_xor(__double2loint(cdot)," << i << ",32);\n";
          ost << "cdot += __hiloint2double(hi_part,lo_part);\n";
        }
      }
      else
      {
        char mask[64];
        sprintf(mask,"0x%x",(32/points_per_cta)-1);
        // Here we use warp-synchronous reductions linearly
        for (int idx = 1; idx < (32/points_per_cta); idx++)
        {
          ost << "if ((wid & " << mask << ") == " << idx << ")\n";
          ost << "  reduc_mirror[(threadIdx.x >> 5)][tid] = cdot;\n";
          ost << "if ((wid & " << mask << ") == 0)\n";
          ost << "  cdot += reduc_mirror[(threadIdx.x >> 5)][tid];\n";
        }
        // Then scatter back out so everyone has the same value
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  reduc_mirror[(threadIdx.x >> 5)][tid] = cdot;\n";
        ost << "cdot = reduc_mirror[(threadIdx.x >> 5)][tid];\n";
      }
      // Now we've got cdot, so we can do the computation for the scaling factor
      ost << REAL << " recip_ddot = 1.0/ddot;\n";
      ost << REAL << " part_sum = cdot + " << DIFFUSION << " * recip_mass;\n"; 
      ost << REAL << " c0 = local_mole_frac * part_sum * recip_ddot;\n";
      ost << "c0 = local_mole_frac * (part_sum + (local_mole_frac - c0) * recip_dt) * recip_ddot;\n";
      ost << REAL << " scale = 1.0;\n";
      // Check for dumb zero condition
      ost << "if (local_mole_frac > 0.0)\n";
      //ost << "if ((ddot * dt) > local_mole_frac)\n";
      ost << "  scale = c0/local_mole_frac;\n";
      // Now scale all the local rr values and put them back in the right places
      assert(int(local_meta_indexes.size()) == max_d_steps);
      assert(int(local_masks.size()) == max_d_steps);
      for (int step = 0; step < max_d_steps; step++)
      {
        if (local_masks[step] > 0)
        {
          char mask_string[64];
          sprintf(mask_string,"0x%x",local_masks[step]);
          ost << "if ((1 << (threadIdx.x >> 5)) & " << mask_string << ")\n";   
        }
        PairDelim scale_pair(ost);
        int index_offset = local_meta_indexes[step]/points_per_cta;
        int index_thread = local_meta_indexes[step]%points_per_cta;
        if (k20)
          ost << "index = __shfl(index_constants[" << index_offset << "]," 
              << index_thread << "," << points_per_cta << ");\n";
        else
        {
          // Rely on warp-synchronous programming
          ost << "if (tid == " << index_thread << ")\n";
          ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
          ost << "index = int_mirror[wid];\n";
        }
        // Scale the value and write it back
        ost << "d_rr[" << step << "] *= scale;\n";
        ost << "rrs[index][tid] = d_rr[" << step << "];\n";
      }
    }
  }

  // finally update the diffusion pointer
  ost << DIFFUSION_ARRAY << " += slice_stride;\n";
}

void TranslationUnit::emit_warp_specialized_output_code(CodeOutStream &ost, int version)
{
  ost << "/* Warp specialized output code */\n";  
  // First reduce the reactions to their actual output values
  // val = rr_f - rr_r
  // Store val in the forward rate location and 
  // store -val in the reverse rate location
  {
    emit_barrier(ost, 2, reac_warps, true/*blocking*/);
    const unsigned actual_reactions = act_reactions.size();
    // Write out the reaction rates into shared memory
    const unsigned total_writes = (actual_reactions+chem_warps.size()-1)/chem_warps.size();
    // Sanity check
    assert(total_writes == (chem_warps[0]->reacs.size()));
    const bool uniform_writes = ((actual_reactions%chem_warps.size()) == 0);
    for (unsigned idx = 0; idx < total_writes; idx++)
    {
      if (!uniform_writes && (idx == (total_writes-1)))
        ost << "if ((wid+" << (idx*chem_warps.size()) << ") < " << actual_reactions << ")\n";
      PairDelim write_pair(ost);
      ost << INT << " index = wid+" << (idx*chem_warps.size()) << ";\n";
      ost << REAL << " rr = rrs[index][tid] - rrs[index+" << actual_reactions << "][tid];\n";
      ost << "rrs[index][tid] = rr;\n";
      ost << "rrs[index+" << actual_reactions << "][tid] = -rr;\n";
    } 
    emit_barrier(ost, 2, reac_warps, true/*blocking*/);
  }

  // We do output based on the number of full warps and not sub-warps 
  const unsigned num_specs = (ordered_species.size()+reac_warps-1)/reac_warps;
  const bool uniform_specs = ((ordered_species.size()%reac_warps) == 0);
  // First sort the species based on the number of reactions they need
  // For the even groups of reac_warps, put them in order from 
  // largest to smallest, but for odd groups of reac warps, put them
  // in order from smallest to largest
  std::list<Species*> output_order;
  {
    std::set<unsigned> used;
    std::vector<Species*> batch;
    bool even = false;
    while (used.size() < ordered_species.size())
    {
      while ((int(batch.size()) < reac_warps) && 
             (used.size() < ordered_species.size()))
      {
        int max_contributions = -1;
        int max_index = -1;
        for (unsigned idx = 0; idx < ordered_species.size(); idx++)
        {
          if (used.find(idx) != used.end())
            continue;
          int contributions = ordered_species[idx]->reaction_contributions.size();
          if (contributions > max_contributions)
          {
            max_contributions = contributions;
            max_index = idx;
          }
        }
        assert(max_index != -1);
        batch.push_back(ordered_species[max_index]);
        used.insert(max_index);
      }
      // We've got a batch, so add it to the order
      if (even)
      {
        for (std::vector<Species*>::const_iterator it = batch.begin();
              it != batch.end(); it++)
        {
          output_order.push_back(*it);
        }
        even = false;
      }
      else
      {
        for (std::vector<Species*>::const_reverse_iterator it = batch.rbegin();
              it != batch.rend(); it++)
        {
          output_order.push_back(*it);
        }
        even = true;
      }
      batch.clear();
    }
  }
#if 0
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
  {
    Species *spec = ordered_species[idx];
    bool inserted = false;
    for (std::list<Species*>::iterator it = output_order.begin();
          it != output_order.end(); it++)
    {
      if (spec->reaction_contributions.size() > 
          (*it)->reaction_contributions.size())
      {
        output_order.insert(it,spec);
        inserted = true;
        break;
      }
    }
    if (!inserted)
      output_order.push_back(spec);
  }
#endif
  // Now for each species output we need to do
  std::list<Species*>::const_iterator spec_it = output_order.begin();
  for (unsigned level = 0; level < num_specs; level++)
  {
    ost << "// Species output for level " << level << "\n";
    if (!uniform_specs && (level == (num_specs-1)))
      ost << "if (((threadIdx.x >> 5)+" << (level*reac_warps) << ") < " << ordered_species.size() << ")\n";
    PairDelim output_pair(ost);
    std::vector<std::vector<std::vector<int> > > assignments(reac_warps);
    // Compute the sets for each output warp and then ask the bank analzer to do the partioning of loads
    int max_steps = 0;
    std::vector<Species*> local_species;
    for (int wid = 0; wid < reac_warps; wid++)
    {
      std::vector<std::vector<int> > &assignment = assignments[wid];
      // Handle the case when we're done with all the species
      if (spec_it == output_order.end())
      {
        assignment.resize(32/points_per_cta);
        // Continue so we can resize everything
        continue;
      }
      std::vector<int> locations;
      // Figure out what species we are handling
      Species *spec = *spec_it;
      local_species.push_back(spec);
      // Update the species iterator
      spec_it++;
      // Get the locations that we need for this species
      for (std::map<Reaction*,int>::const_iterator it = spec->reaction_contributions.begin();
            it != spec->reaction_contributions.end(); it++)
      {
        assert(it->second != 0);
        int loc, abs_val;
        // If it is a positive coefficient, then we get the
        // value from where the forward rate was
        if (it->second > 0)
        {
          loc = bank_analyzer->find_forward_location(it->first);
          abs_val = it->second;
        }
        // Otherwise we get the negative value from where the
        // reverse reaction rate was stored
        else
        {
          loc = bank_analyzer->find_reverse_location(it->first);
          abs_val = -it->second;
        }
        // Push back how many times we need this value
        for (int idx = 0; idx < abs_val; idx++)
          locations.push_back(loc);
      }
      //printf("Species %s (%d) has %ld locations\n", spec->name, find_ordered_index(spec), locations.size());
      bank_analyzer->perform_assignment(locations, assignment);
      for (unsigned idx = 0; idx < assignment.size(); idx++)
      {
        int steps = assignment[idx].size();
        if (steps > max_steps)
          max_steps = steps;
      }
    }
    ost << REAL << " " << WDOT << " = 0.0;\n";
    ost << INT << " index;\n";
    // Figure out how many local reduction we need to do for each thread
    for (int step = 0; step < max_steps; step++)
    {
      // For each warp, set the index location and get back
      // the meta-index.  Make sure all the meta-index values
      // are the same.
      int meta_index = -1;
      unsigned valid_mask = 0;
      bool needs_mask = false;
      for (int wid = 0; wid < reac_warps; wid++)
      {
        assert((32/points_per_cta) == int(assignments[wid].size()));
        bool local_mask = true;
        for (unsigned idx = 0; idx < assignments[wid].size(); idx++)
        {
          // Check to see if we have an index
          int global_idx;
          int local_wid = wid*(32/points_per_cta)+idx;
          if (step < int(assignments[wid][idx].size()))
          {
            int index = assignments[wid][idx][step];
            Warp *w = chem_warps[local_wid];
            global_idx = w->add_index(index, version);
            // As long as we have something to do then
            // we don't need to be masked off
            local_mask = false;
          }
          else
          {
            // We need the identity index 
            global_idx = chem_warps[local_wid]->add_index(2*act_reactions.size(), version);
          }
          // Check to make sure all the warps on the same page
          if (meta_index == -1)
            meta_index = global_idx;
          else
            assert(meta_index == global_idx);
        }
        if (local_mask)
          needs_mask = true;
        else
          valid_mask += (1 << wid);
      }
      // Check to see if we need to worry about the mask here
      // Check to see if we need a condition on this reduction
      if (needs_mask)
      {
        char mask_string[64];
        sprintf(mask_string,"0x%x",valid_mask);
        ost << "if ((1 << (threadIdx.x >> 5)) & " << mask_string << ")\n";
      }
      PairDelim reduc_pair(ost);
      // Figure out where to get the index from
      int index_offset = meta_index/points_per_cta;
      int index_thread = meta_index%points_per_cta;
      if (k20)
        ost << "index = __shfl(index_constants[" << index_offset << "]," 
            << index_thread << "," << points_per_cta << ");\n";
      else
      {
        // Rely on warp-synchronous programming
        ost << "if (tid == " << index_thread << ")\n";
        ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
        ost << "index = int_mirror[wid];\n";
      }
      ost << WDOT << " += rrs[index][tid];\n";
    }
    // Once we've got all our local reduction values, 
    // then we reduce across the warps and then write out our value
    if (points_per_cta < 32)
    {
      if (k20)
      {
        ost << INT << " hi_part,lo_part;\n";
        for (int i = points_per_cta; i < 32; i*=2)
        {
          ost << "hi_part = __shfl_xor(__double2hiint(" << WDOT << ")," << i << ",32);\n"; 
          ost << "lo_part = __shfl_xor(__double2loint(" << WDOT << ")," << i << ",32);\n";
          ost << WDOT << " += __hiloint2double(hi_part,lo_part);\n";
        }
      }
      else
      {
        char mask[64];
        sprintf(mask,"0x%x",(32/points_per_cta)-1);
        // Here we use warp-synchronous reductions linearly
        for (int idx = 1; idx < (32/points_per_cta); idx++)
        {
          ost << "if ((wid & " << mask << ") == " << idx << ")\n";
          ost << "  reduc_mirror[(threadIdx.x >> 5)][tid] = " << WDOT << ";\n";
          ost << "if ((wid & " << mask << ") == 0)\n";
          ost << "  " << WDOT << " += reduc_mirror[(threadIdx.x >> 5)][tid];\n";
        }
        // Then scatter back out so everyone has the same value
        // No need to do this since only the bottom sub-warp will write out the value
        //ost << "if ((wid & " << mask << ") == 0)\n";
        //ost << "  reduc_mirror[(threadIdx.x >> 5)][tid] = " << WDOT << ";\n";
        //ost << WDOT << " = reduc_mirror[(threadIdx.x >> 5)][tid];\n";
      }
    }
    // Now that we're here, the WDOT value in the lowest sub-warp within
    // a warp stores the final output for the given species
    // Figure out where to write it back 
    int meta_index = -1;
    for (int wid = 0; wid < reac_warps; wid++)
    {
      if (wid < int(local_species.size()))
      {
        Species *spec = local_species[wid];
        int index = find_ordered_index(spec);
        assert(index != -1);
        for (int idx = 0; idx < (32/points_per_cta); idx++)
        {
          int local_wid = wid*(32/points_per_cta) + idx;
          int global_idx = chem_warps[local_wid]->add_index(index, version);
          if (meta_index == -1)
            meta_index = global_idx;
          else
            assert(meta_index == global_idx);
        }
      }
      else
      {
        // Otherwise add dummy index values
        for (int idx = 0; idx < (32/points_per_cta); idx++)
        {
          int local_wid = wid*(32/points_per_cta) + idx;
          int global_idx = chem_warps[local_wid]->add_index(0, version);
          if (meta_index == -1)
            meta_index = global_idx;
          else
            assert(meta_index == global_idx);
        }
      }
    }
    // Figure out where to get the index from
    int index_offset = meta_index/points_per_cta;
    int index_thread = meta_index%points_per_cta;
    if (k20)
    {
      ost << "index = __shfl(index_constants[" << index_offset << "]," 
            << index_thread << "," << points_per_cta << ");\n";
    }
    else
    {
      // Rely on warp-synchronous programming
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
      ost << "index = int_mirror[wid];\n";
    }
    // Scale by the recip molecular mass
    ost << WDOT << " *= (1e-3 * molecular_masses[index]);\n";
    // Now emit the store for WDOT with a conditional if necessary
    if (points_per_cta < 32)
    {
      char mask[64];
      sprintf(mask,"0x%x",(32/points_per_cta)-1);
      ost << "if ((wid & " << mask << ") == 0)\n";
    }
    emit_cuda_store(ost, WDOT_ARRAY, "index*spec_stride", WDOT);
  }

  // finally update the output pointer
  ost << WDOT_ARRAY << " += slice_stride;\n";
}

void TranslationUnit::emit_warp_specialized_update_pointers_code(CodeOutStream &ost)
{
  ost << "/* Update pointers code */\n";
  PairDelim pair(ost);
  ost << PRESSURE_ARRAY << " += slice_stride;\n";
  ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
  ost << AVMOLWT_ARRAY << " += slice_stride;\n";
  ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
  // We'll update diffusion pointers post stiffness computation
  // We'll update output pointers post output computation
}

void TranslationUnit::emit_warp_specialized_qssa_code(CodeOutStream &ost, int version)
{
  ost << "// Main loop\n";
  ost << "for (step = 0; step < total_steps; step++)\n";
  {
    PairDelim for_pair(ost);
    emit_barrier(ost, 0, (reac_warps+1), true/*wait*/);
    // Write the unimportant reaction rates
    ost << "// Unimportant rates\n";
    ost << "if (wid == 0)\n";
    {
      PairDelim if_pair(ost);
      for (std::set<Reaction*>::const_iterator it = chem->forward_unimportant.begin();
            it != chem->forward_unimportant.end(); it++)
      {
        int loc = bank_analyzer->find_forward_location(*it);
        assert(loc != -1);
        ost << "rrs[" << loc << "][tid] = 0.0;\n";
      }
      for (std::set<Reaction*>::const_iterator it = chem->backward_unimportant.begin();
            it != chem->backward_unimportant.end(); it++)
      {
        int loc = bank_analyzer->find_reverse_location(*it);
        assert(loc != -1);
        ost << "rrs[" << loc << "][tid] = 0.0;\n";
      }
    }
    for (std::vector<ConnectedComponent*>::const_iterator it = 
          chem->connected_components.begin();
          it != chem->connected_components.end(); it++)
    {
      emit_warp_specialized_cc(ost, *it, version);
    }
    emit_barrier(ost, 1, (reac_warps+1), false/*wait*/);
  }
}

void TranslationUnit::emit_warp_specialized_cc(CodeOutStream &ost, ConnectedComponent *cc, int version)
{
  std::set<std::pair<int,int> > already_emitted;
  // First tell all the species to emit their statements
  for (std::map<int,QSS*>::const_iterator it = cc->species.begin();
        it != cc->species.end(); it++)
  {
    it->second->emit_warp_specialized_statements(ost,already_emitted,
                    bank_analyzer, qssa_warps, version, chem_warps.size());
  }
  // Emit forward declarations for all the X values
  PairDelim cc_pair(ost);
  bool first_decl = true;
  ost << REAL;
  if (cc->statements.size() > 1)
  {
    ost << " den"; 
    first_decl = false;
  }
  for (std::map<int,QSS*>::const_iterator it = cc->species.begin();
        it != cc->species.end(); it++)
  {
    if (first_decl)
      ost << " xq_" << it->first;
    else
      ost << ", xq_" << it->first;
    first_decl = false;
  }
  ost << ";\n";
  ost << INT << " index;\n";
  // Now we can emit the statements for this connected component
  for (std::vector<Statement*>::const_iterator it = cc->statements.begin();
        it != cc->statements.end(); it++)
  {
    (*it)->emit_code(ost, already_emitted, NULL);
  }

  // Rely on warp-synchronous behaviour here so we don't need a barrier
  // to separate the shared memory reads from the writes

  // Finally update all the reactions 
  for (std::map<int,QSS*>::const_iterator it = cc->species.begin();
        it != cc->species.end(); it++)
  {
    it->second->emit_warp_specialized_updates(ost, bank_analyzer, 
                            qssa_warps, version, chem_warps.size());
  }
}

void TranslationUnit::emit_warp_specialized_constants(CodeOutStream &ost)
{
  // Gibbs constants
  {
    ost << "__constant__ " << REAL << " gibbs_temp[" << gibbs_species.size() << "] = {";
    bool first = true;
    for (std::vector<Species*>::const_iterator it = gibbs_species.begin();
          it != gibbs_species.end(); it++)
    {
      if (first)
        ost << (*it)->get_common_temperature();
      else
        ost << ", " << (*it)->get_common_temperature();
      first = false;
    }
    ost << "};\n\n";
    ost << "__constant__ " << REAL << " gibbs_high[" << gibbs_species.size() << "][7] = {";
    first = true;
    for (std::vector<Species*>::const_iterator it = gibbs_species.begin();
          it != gibbs_species.end(); it++)
    {
      if (first)
        ost << "{";
      else
        ost << ", {";
      for (unsigned idx = 0; idx < 7; idx++)
      {
        if (idx == 0)
          ost << (*it)->get_high_coefficient(idx);
        else
          ost << ", " << (*it)->get_high_coefficient(idx);
      }
      ost << "}";
      first = false;
    }
    ost << "};\n\n";
    ost << "__constant__ " << REAL << " gibbs_low[" << gibbs_species.size() << "][7] = {";
    first = true;
    for (std::vector<Species*>::const_iterator it = gibbs_species.begin();
          it != gibbs_species.end(); it++)
    {
      if (first)
        ost << "{";
      else
        ost << ", {";
      for (unsigned idx = 0; idx < 7; idx++)
      {
        if (idx == 0)
          ost << (*it)->get_low_coefficient(idx);
        else
          ost << ", " << (*it)->get_low_coefficient(idx);
      }
      ost << "}";
      first = false;
    }
    ost << "};\n\n";
#if 0
    first = true;
    ost << "__constant__ " << REAL << " troe_constants[" << troe_reactions.size() << "][5] = {";
    for (std::vector<Reaction*>::const_iterator it = troe_reactions.begin();
          it != troe_reactions.end(); it++)
    {
      if (first)
        ost << "{";
      else
        ost << ", {";
      ost << (1.0-(*it)->troe.a);
      if (use_dim)
        ost << ", " << (-1.0/(*it)->troe.t3);
      else
        ost << ", " << (-1.0*TEMPERATURE_REF/(*it)->troe.t3);
      ost << ", " << (*it)->troe.a;
      if (use_dim)
        ost << ", " << (-1.0/(*it)->troe.t1);
      else
        ost << ", " << (-1.0*TEMPERATURE_REF/(*it)->troe.t1);
      if (use_dim)
        ost << ", " << (-(*it)->troe.t2);
      else
        ost << ", " << (-(*it)->troe.t2/TEMPERATURE_REF);
      ost << "}";
      first = false;
    }
    ost << "};\n\n";
#endif
  }

  // Chemistry index
  {
    // Find the warp with the most constants
    size_t num_constants = 0;
    for (int wid = 0; wid < int(chem_warps.size()); wid++)
    {
      if (chem_warps[wid]->int_constants[0].size() > num_constants)
        num_constants = chem_warps[wid]->int_constants[0].size();
    }
    if (num_constants > 0)
    {
      // Pad out the constants so they are a multiple of points_per_cta for each warp
      while ((num_constants%points_per_cta) != 0)
        num_constants++;
      for (unsigned wid = 0; wid < chem_warps.size(); wid++)
      {
        while (chem_warps[wid]->int_constants[0].size() < num_constants)
          chem_warps[wid]->add_index(0, 0);
      }
      assert((num_constants%points_per_cta) == 0);
      const unsigned num_passes = num_constants/points_per_cta;
      ost << "__device__ const " << INT << " chem_index[" << (num_constants*chem_warps.size()) << "] = {";
      for (unsigned pass = 0; pass < num_passes; pass++)
      {
        for (unsigned wid = 0; wid < chem_warps.size(); wid++)
        {
          unsigned offset = pass * points_per_cta;
          for (int idx = 0; idx < points_per_cta; idx++)
          {
            assert((offset+idx) < chem_warps[wid]->int_constants[0].size());
            if ((pass == 0) && (wid == 0) && (idx == 0))
              ost << chem_warps[wid]->int_constants[0][offset+idx];
            else
              ost << ", " << chem_warps[wid]->int_constants[0][offset+idx];
          }
        }
      }
      ost << "};\n\n";
    }
  }

  // Chemistry real
  {
    // First check that all the warps have the same number of constants
    size_t num_constants = chem_warps[0]->real_constants[0].size();
    if (num_constants > 0)
    {
      for (unsigned wid = 1; wid < chem_warps.size(); wid++)
      {
        assert(num_constants == chem_warps[wid]->real_constants[0].size());
      }
      // Pad out the constants so they are a multiple of points_per_cta for each warp
      while ((num_constants%points_per_cta) != 0)
        num_constants++;
      for (unsigned wid = 0; wid < chem_warps.size(); wid++)
      {
        while (chem_warps[wid]->real_constants[0].size() < num_constants)
          chem_warps[wid]->add_real(0.0, 0);
      }
      assert((num_constants%points_per_cta) == 0);
      const unsigned num_passes = num_constants/points_per_cta;
      ost << "__device__ const " << REAL << " chem_real[" << (num_constants*chem_warps.size()) << "] = {";
      for (unsigned pass = 0; pass < num_passes; pass++)
      {
        for (unsigned wid = 0; wid < chem_warps.size(); wid++)
        {
          unsigned offset = pass * points_per_cta;
          for (int idx = 0; idx < points_per_cta; idx++)
          {
            assert((offset+idx) < chem_warps[wid]->real_constants[0].size());
            if ((pass == 0) && (wid == 0) && (idx == 0))
              ost << chem_warps[wid]->real_constants[0][offset+idx];
            else
              ost << ", " << chem_warps[wid]->real_constants[0][offset+idx];
          }
        }
      }
      ost << "};\n\n";
    }
  }

  // QSSA index
  {
    // First check that all the warps have the same number of constants
    size_t num_constants = qssa_warps[0]->int_constants[0].size();
    if (num_constants > 0)
    {
      for (unsigned wid = 1; wid < qssa_warps.size(); wid++)
      {
        assert(num_constants == qssa_warps[wid]->int_constants[0].size());
      }
      // Pad out the constants so they are a multiple of points_per_cta for each warp
      while ((num_constants%points_per_cta) != 0)
        num_constants++;
      for (unsigned wid = 0; wid < qssa_warps.size(); wid++)
      {
        while (qssa_warps[wid]->int_constants[0].size() < num_constants)
          qssa_warps[wid]->add_index(0, 0);
      }
      assert((num_constants%points_per_cta) == 0);
      const unsigned num_passes = num_constants/points_per_cta;
      ost << "__device__ const " << INT << " qssa_index[" << (num_constants*qssa_warps.size()) << "] = {";
      for (unsigned pass = 0; pass < num_passes; pass++)
      {
        for (unsigned wid = 0; wid < qssa_warps.size(); wid++)
        {
          unsigned offset = pass * points_per_cta;
          for (int idx = 0; idx < points_per_cta; idx++)
          {
            assert((offset+idx) < qssa_warps[wid]->int_constants[0].size());
            if ((pass == 0) && (wid == 0) && (idx == 0))
              ost << qssa_warps[wid]->int_constants[0][offset+idx];
            else
              ost << ", " << qssa_warps[wid]->int_constants[0][offset+idx];
          }
        }
      }
      ost << "};\n\n";
    }
  }

  // Reaction rate scratch
  if (spill_reac_rates)
    ost << "__device__ " << REAL << " rr_g[" << MAX_SM_COUNT << "*" << sm_offset << "][" << warp_size <<"];\n\n";
  if (spill_gibbs)
    ost << "__device__ " << REAL << " gibbs_g[" << MAX_SM_COUNT << "*" << gibbs_species.size() 
        << "][" << warp_size << "];\n\n";
  if (spill_qssa && (qssa_spill_size > 0))
    ost << "__device__ " << REAL << " qssa_g[" << MAX_SM_COUNT << "*" << qssa_spill_size << "]["
        << warp_size << "];\n\n";
}

void TranslationUnit::emit_warp_specialized_load_rates(CodeOutStream &ost,
    const std::vector<int> &locations, const std::vector<Warp*> &warps, 
    const char *var_name, int version, int warp_offset)
{
  std::vector<std::vector<int> > assignment;
  bank_analyzer->perform_assignment(locations, assignment);
  int max_steps = -1;
  for (unsigned idx = 0; idx < assignment.size(); idx++)
  {
    if (int(assignment[idx].size()) > max_steps)
      max_steps = assignment[idx].size();
  }
  assert(assignment.size() == warps.size());
  // Now emit the loads for the warp 
  for (int step = 0; step < max_steps; step++)
  {
    int meta_index = -1;
    // Update all the qssa_warps with the appropriate index
    for (unsigned wid = 0; wid < warps.size(); wid++)
    {
      if (step < int(assignment[wid].size()))
      {
        int global_idx = warps[wid]->add_index(
                  assignment[wid][step], version); 
        if (meta_index == -1)
          meta_index = global_idx;
        else
          assert(meta_index == global_idx);
      }
      else
      {
        int global_idx = warps[wid]->add_index(
                  2*act_reactions.size(), version);
        if (meta_index == -1)
          meta_index = global_idx;
        else
          assert(meta_index == global_idx);
      }
    }
    // Now emit the code for the exchange of index
    int index_offset = meta_index/points_per_cta;
    int index_thread = meta_index%points_per_cta;
    if (k20)
      ost << "index = __shfl(index_constants[" << index_offset << "]," 
          << index_thread << "," << points_per_cta << ");\n";
    else
    {
      // Rely on warp-synchronous programming
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  int_mirror[wid+" << (warp_offset) 
          << "] = index_constants[" << index_offset << "];\n";
      ost << "index = int_mirror[wid+" 
          << (warp_offset) << "];\n";
    }
    ost << var_name << " += rrs[index][tid];\n";
  }
  // Now we've got all our local values, reduce across the sub-warps
  if (k20)
  {
    for (int i = points_per_cta; i < 32; i*=2)
    {
      ost << "hi_part = __shfl_xor(__double2hiint(" << var_name << ")," << i << ",32);\n"; 
      ost << "lo_part = __shfl_xor(__double2loint(" << var_name << ")," << i << ",32);\n";
      ost << var_name << " += __hiloint2double(hi_part,lo_part);\n";
    }
  }
  else
  {
    char mask[64];
    sprintf(mask,"0x%x",(32/points_per_cta)-1);
    // Here we use warp-synchronous reductions linearly
    for (int idx = 1; idx < (32/points_per_cta); idx++)
    {
      ost << "if ((wid & " << mask << ") == " << idx << ")\n";
      ost << "  reduc_mirror[(threadIdx.x >> 5)][tid] = " << var_name << ";\n";
      ost << "if ((wid & " << mask << ") == 0)\n";
      ost << "  " << var_name << " += reduc_mirror[(threadIdx.x >> 5)][tid];\n";
    }
    // Then scatter back out so everyone has the same value
    ost << "if ((wid & " << mask << ") == 0)\n";
    ost << "  reduc_mirror[(threadIdx.x >> 5)][tid] = " << var_name << ";\n";
    ost << var_name << " = reduc_mirror[(threadIdx.x >> 5)][tid];\n";
  }
}

void TranslationUnit::emit_warp_specialized_full_code(CodeOutStream &ost, 
                const char *temp_chem_name, const char *temp_qssa_name)
{
  emit_warp_specialized_constants(ost); 

  emit_gpu_getrates_declaration(ost);
  ost << "\n";

  PairDelim func_pair(ost);

  // Emit declarations for shared memory
  ost << "__shared__ volatile " << REAL << " rrs[" << (2*act_reactions.size()+1) << "][" << points_per_cta << "];\n";
  ost << "__shared__ volatile " << REAL << " gibbs[" << (gibbs_species.size()+1) << "][" << points_per_cta << "];\n";
  if (!k20)
  {
    assert((32 % points_per_cta) == 0);
    int total_warps = (reac_warps + ((chem == NULL) ? 0 : 1)) * (32/points_per_cta);
    ost << "__shared__ volatile " << REAL << " real_mirror[" << total_warps << "];\n";
    ost << "__shared__ volatile " << INT << " int_mirror[" << total_warps << "];\n";
    ost << "__shared__ volatile " << REAL << " reduc_mirror[" << (reac_warps+((chem == NULL) ? 0 : 1)) << "][" << (points_per_cta) << "];\n";
  }

  int power = int(log(points_per_cta)/log(2));
  char mask[8];
  sprintf(mask,"0x%x",points_per_cta-1);
  if (chem != NULL)
  {
    ost << "__shared__ volatile " << REAL << " buffer0[" << (ordered_species.size()+third_bodies.size()+1) << "][" << points_per_cta << "];\n";    
    ost << "__shared__ volatile " << REAL << " buffer1[" << (ordered_species.size()+third_bodies.size()+1) << "][" << points_per_cta << "];\n";
    ost << "if (threadIdx.x < " << (reac_warps*32) << ")\n";
    {
      PairDelim if_pair(ost);

      ost << "const " << INT << " wid = threadIdx.x >> " << power << ";\n";
      ost << "const " << INT << " tid = threadIdx.x & " << mask << ";\n";
      ost << INT << " step = 0;\n";

      emit_getrates_numerical_constants(ost);

      // Scale the pointers
      {
        PairDelim off_pair(ost);
        ost << "const " << INT << " offset = (blockIdx.x*"
            << points_per_cta << " + tid);\n";
        ost << PRESSURE_ARRAY << " += offset;\n";
        ost << TEMPERATURE_ARRAY << " += offset;\n";
        ost << MASS_FRAC_ARRAY << " += offset;\n";
        ost << AVMOLWT_ARRAY << " += offset;\n";
        ost << DIFFUSION_ARRAY << " += offset;\n";
        ost << WDOT_ARRAY << " += offset;\n";
      }
      
      // Write the unity locations
      ost << "if (wid == 0)\n";
      {
        PairDelim unity_pair(ost);
        ost << "rrs[" << (2*act_reactions.size()) << "][tid] = 0.0;\n";
        ost << "gibbs[" << (gibbs_species.size()) << "][tid] = 1.0;\n";
        ost << "buffer0[" << (ordered_species.size()+third_bodies.size()) << "][tid] = 1.0;\n";
        ost << "buffer1[" << (ordered_species.size()+third_bodies.size()) << "][tid] = 1.0;\n";
      }

      // Emit declarations for needed variables
      ost << REAL << " " << TEMPERATURE << ", " << PRESSURE << ", " << AVMOLWT << ";\n";
      //ost << REAL << " rr_f[" << chem_warps[0]->reacs.size() << "];\n";
      //ost << REAL << " rr_r[" << chem_warps[0]->reacs.size() << "];\n";

      // Emit the code for loading the constants that we need
      assert((chem_warps[0]->int_constants[0].size()%points_per_cta) == 0);
      const unsigned num_index_vals = chem_warps[0]->int_constants[0].size()/points_per_cta;
      ost << INT << " index_constants[" << (num_index_vals) << "];\n";
      assert((chem_warps[0]->real_constants[0].size()%points_per_cta) == 0);
      const unsigned num_real_vals = chem_warps[0]->real_constants[0].size()/points_per_cta;
      ost << REAL << " real_constants[" << (num_real_vals) << "];\n";
      // Emit the code to load the constants
      for (unsigned idx = 0; idx < num_index_vals; idx++)
      {
        char src_index[64];
        sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*chem_warps.size()*points_per_cta);
        char dst_index[64];
        sprintf(dst_index,"%d",idx);
        emit_cuda_int_load(ost, "index_constants", dst_index, "chem_index", src_index, k20);
      }
      for (unsigned idx = 0; idx < num_real_vals; idx++)
      {
        char src_index[64];
        sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*chem_warps.size()*points_per_cta);
        char dst_index[64];
        sprintf(dst_index,"%d",idx);
        emit_cuda_load(ost, "real_constants", dst_index, "chem_real", src_index, k20);
      }

      // Then copy over the code from the temporary file
      FILE *temp_file = fopen(temp_chem_name,"r");
      assert(temp_file != NULL);
      char c = fgetc(temp_file);
      while (c != EOF)
      {
        ost << c;
        c = fgetc(temp_file);
      }
      assert(fclose(temp_file) == 0);
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "const " << INT << " wid = (threadIdx.x - " << (reac_warps*32) << ") >> " << power << ";\n";
      ost << "const " << INT << " tid = threadIdx.x & " << mask << ";\n";
      ost << INT << " step = 0;\n";

      assert((qssa_warps[0]->int_constants[0].size()%points_per_cta) == 0);
      const unsigned num_index_vals = qssa_warps[0]->int_constants[0].size()/points_per_cta;
      ost << INT << " index_constants[" << (num_index_vals) << "];\n";
      for (unsigned idx = 0; idx < num_index_vals; idx++)
      {
        char src_index[64];
        sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*qssa_warps.size()*points_per_cta);
        char dst_index[64];
        sprintf(dst_index,"%d",idx);
        emit_cuda_int_load(ost, "index_constants", dst_index, "qssa_index", src_index, k20);
      }

      // Then copy over the code from the temporary file
      FILE *temp_file = fopen(temp_qssa_name,"r");
      assert(temp_file != NULL);
      char c = fgetc(temp_file);
      while (c != EOF)
      {
        ost << c;
        c = fgetc(temp_file);
      }
      assert(fclose(temp_file) == 0);
    }
  }
  else
  {
    ost << "__shared__ volatile " << REAL << " buffer[" << (ordered_species.size()+third_bodies.size()+1) << "][" << points_per_cta << "];\n";
    ost << "const " << INT << " wid = threadIdx.x >> " << power << ";\n";
    ost << "const " << INT << " tid = threadIdx.x & " << mask << ";\n";
    ost << INT << " step = 0;\n";

    emit_getrates_numerical_constants(ost);

    // Scale the pointers
    {
      PairDelim off_pair(ost);
      ost << "const " << INT << " offset = (blockIdx.x*"
          << points_per_cta << " + tid);\n";
      ost << PRESSURE_ARRAY << " += offset;\n";
      ost << TEMPERATURE_ARRAY << " += offset;\n";
      ost << MASS_FRAC_ARRAY << " += offset;\n";
      ost << AVMOLWT_ARRAY << " += offset;\n";
      ost << WDOT_ARRAY << " += offset;\n";
    }

    ost << "if (wid == 0)\n";
    {
      PairDelim unity_pair(ost);
      ost << "rrs[" << (2*act_reactions.size()) << "][tid] = 0.0;\n";
      ost << "gibbs[" << (gibbs_species.size()) << "][tid] = 1.0;\n";
      ost << "buffer[" << (ordered_species.size()+third_bodies.size()) << "][tid] = 1.0;\n";
    }

    // Emit the code for loading the constants that we need
    assert((chem_warps[0]->int_constants[0].size()%points_per_cta) == 0);
    const unsigned num_index_vals = chem_warps[0]->int_constants[0].size()/points_per_cta;
    ost << INT << " index_constants[" << (num_index_vals) << "];\n";
    assert((chem_warps[0]->real_constants[0].size()%points_per_cta) == 0);
    const unsigned num_real_vals = chem_warps[0]->real_constants[0].size()/points_per_cta;
    ost << REAL << " real_constants[" << (num_real_vals) << "];\n";
    // Emit the code to load the constants
    for (unsigned idx = 0; idx < num_index_vals; idx++)
    {
      char src_index[64];
      sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*chem_warps.size()*points_per_cta);
      char dst_index[64];
      sprintf(dst_index,"%d",idx);
      emit_cuda_int_load(ost, "index_constants", dst_index, "chem_index", src_index, k20);
    }
    for (unsigned idx = 0; idx < num_real_vals; idx++)
    {
      char src_index[64];
      sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*chem_warps.size()*points_per_cta);
      char dst_index[64];
      sprintf(dst_index,"%d",idx);
      emit_cuda_load(ost, "real_constants", dst_index, "chem_real", src_index, k20);
    }

    // Emit declarations for needed variables
    ost << REAL << " " << TEMPERATURE << ", " << PRESSURE << ", " << AVMOLWT << ";\n";
    //ost << REAL << " rr_f[" << chem_warps[0]->reacs.size() << "];\n";
    //ost << REAL << " rr_r[" << chem_warps[0]->reacs.size() << "];\n";

    // Then copy over the code from the temporary file
    FILE *temp_file = fopen(temp_chem_name,"r");
    assert(temp_file != NULL);
    char c = fgetc(temp_file);
    while (c != EOF)
    {
      ost << c;
      c = fgetc(temp_file);
    }
    assert(fclose(temp_file) == 0);
  }
}

static void compute_pass_information(int shared_values, int total_warps, int levels,
                                     int scalar, int &passes, int &levels_per_pass)
{
  assert(shared_values >= (scalar*total_warps));
  levels_per_pass = shared_values/(scalar*total_warps);
  assert(levels_per_pass > 0);
  passes = (levels + levels_per_pass - 1)/levels_per_pass;
}

void TranslationUnit::emit_multi_gpu_getrates(CodeOutStream &ost)
{
  if (qss_warps == 0)
    this->qss_warps = 1; // default this to one
  if (target_ctas == 0)
    this->target_ctas = 1;
  // Quick check that you won't get the asked for number of CTAs
  // because of total warp count
  {
    int requested_warps = (reac_warps + qss_warps)*target_ctas;
    if (k20 && (requested_warps > 64))
    {
      fprintf(stderr,"WARNING: You've requested %d chemistry warps and "
                     "%d QSSA warps for %d CTAs/SM for a total of %d warps "
                     "which is more than Kepler's max of 64 per SM.\n",
                     reac_warps, qss_warps, target_ctas, requested_warps);
    }
    if (!k20 && (requested_warps > 48))
    {
      fprintf(stderr,"WARNING: You've requested %d chemistry warps and "
                     "%d QSSA warps for %d CTAs/SM for a total of %d warps "
                     "which is more than Fermi's max of 48 warps per SM.\n",
                     reac_warps, qss_warps, target_ctas, requested_warps);
    }
  }
  int qssa_levels = multi_assign_reactions_to_warps();

  // Use the target number of CTAs to figure out how much shared memory
  // we get.  Then use that to compute the number of passes.
  int cta_values_per_point, chem_pass_values;
  int chem_passes, chem_levels_per_pass;
  int qssa_passes, qssa_levels_per_pass;
  int stif_passes, stif_levels_per_pass;
  int output_passes, output_levels_per_pass;
  int shared_offset;
  {
    this->points_per_cta = this->warp_size;
    int available_shared = MAX_SHARED;
    int total_warps = reac_warps * (32/warp_size);
    // If this is fermi, count the space needed for exchanging data
    // otherwise we can use the full shared memory
    if (!k20)
    {
      // Real mirror
      available_shared -= (total_warps*sizeof(REAL_TYPE));
      // Int mirror
      available_shared -= (total_warps*sizeof(INT_TYPE));
      // Reduction mirror
      available_shared -= ((reac_warps + ((chem == NULL) ? 0 : 1))*warp_size*sizeof(REAL_TYPE));
    }
    int cta_shared = available_shared/target_ctas;
    cta_values_per_point = cta_shared/(sizeof(REAL_TYPE)*warp_size);
    if (use_indexing)
    {
      fprintf(stderr,"ERROR: Update Singe multi pass logic for using indexing!\n");
      exit(1);
    }
    shared_offset = ordered_species.size() + 
                      (use_indexing ? 1/*unity*/ : 0) + 
                      third_bodies.size() +
                      (spill_gibbs ? 0 : gibbs_species.size()) + 
                      (use_indexing ? 1/*gibbs unity*/ : 0) + 
                      (use_indexing ? 2/*unity and zero rrs*/ : 0);
    // need at least 2 values per warp for forward and reverse reaction rates
    int needed_values = shared_offset;
    if (!spill_reac_rates)
      needed_values += 2*total_warps; // need at least 2 values per warp
    if (needed_values > cta_values_per_point)
    {
      fprintf(stderr,"Too many warps or too many target CTAs specified for shared memory!\n");
      fprintf(stderr,"Need %d values per point, but only have %d values available.\n",
                needed_values, cta_values_per_point);
      fprintf(stderr,"Reduce either the requested number of reac warps or the requested "
                     "target number of CTAs.\n");
      exit(1);
    }
    chem_pass_values = cta_values_per_point - shared_offset;
    if (!spill_reac_rates) {
      compute_pass_information(chem_pass_values,total_warps,chem_warps[0]->reacs.size(),
                               2, chem_passes, chem_levels_per_pass);
      compute_pass_information(chem_pass_values,total_warps,qssa_levels,
                               2, qssa_passes, qssa_levels_per_pass);
      compute_pass_information(cta_values_per_point,total_warps,chem_warps[0]->reacs.size(),
                               2, stif_passes, stif_levels_per_pass);
      compute_pass_information(cta_values_per_point,total_warps,chem_warps[0]->reacs.size(),
                               1, output_passes, output_levels_per_pass);
    } else {
      chem_passes = (chem_warps[0]->reacs.size()+qssa_levels-1)/qssa_levels;
      chem_levels_per_pass = qssa_levels;
      qssa_passes = 1;
      qssa_levels_per_pass = qssa_levels;
      stif_passes = 1;
      stif_levels_per_pass = chem_warps[0]->reacs.size();
      output_passes = 1;
      output_levels_per_pass = chem_warps[0]->reacs.size();
    }
    fprintf(stdout,"INFO: Multi-Pass Warp-Specialized getrates stats\n");
    fprintf(stdout,"  Points-per-CTA: %d\n", points_per_cta);
    fprintf(stdout,"  Chemistry Warps: %d\n", reac_warps);
    fprintf(stdout,"  QSSA Warps: %d\n", qss_warps);
    fprintf(stdout,"  Target CTAs/SM: %d\n", target_ctas);
    fprintf(stdout,"  Shared-Memory Usage per CTA: %d\n", cta_shared);
    fprintf(stdout,"  Other shared values: %d\n", shared_offset);
    fprintf(stdout,"  Pre-QSSA levels: %d\n", qssa_levels);
    fprintf(stdout,"  Post-QSSA levels: %ld\n", chem_warps[0]->reacs.size() - qssa_levels);
    fprintf(stdout,"  QSSA passes: %d\n", chem_passes);
    fprintf(stdout,"  Stiffness passes: %d\n", stif_passes);
    fprintf(stdout,"  Output passes: %d\n", output_passes);
  }

  // Build the shared memory analyzer
  if (!spill_reac_rates)
    multi_construct_shared_memory_analyzers(chem_levels_per_pass, chem_passes,
                                            qssa_levels_per_pass, qssa_passes,
                                            stif_levels_per_pass, stif_passes,
                                            output_levels_per_pass, output_passes,
                                            shared_offset);
  else
    multi_construct_shared_memory_analyzers(chem_levels_per_pass, chem_passes,
                                            qssa_levels_per_pass, qssa_passes,
                                            stif_levels_per_pass, stif_passes,
                                            output_levels_per_pass, output_passes,
                                            0/*offset*/);

  // Before emitting any code, set the values for the identity locations
  this->thb_offset = ordered_species.size();
  if (!spill_gibbs) {
    this->gibbs_offset = thb_offset + third_bodies.size();
    this->reac_offset = gibbs_offset + gibbs_species.size();
  } else {
    this->gibbs_offset = 0;
    this->reac_offset = thb_offset + third_bodies.size();
  }
#if 0
  this->reac_unity = rrs_per_pass;
  this->reac_zero = rrs_per_pass+1;
  this->spec_unity = ordered_species.size() + third_bodies.size();
#endif

  // Emit the compute warp block of code into a temporary file
  const char *temp_chem_name = "__temp_chem__.cu";
  {
    CodeOutStream temp_ost(temp_chem_name, true, 80);  
    emit_multi_chem_code(temp_ost, chem_passes, chem_levels_per_pass,
                          qssa_passes, qssa_levels_per_pass, qssa_levels,
                          stif_passes, stif_levels_per_pass, output_passes,
                          output_levels_per_pass, shared_offset, chem_pass_values);
  }
#if 0
  const char *temp_qssa_name = "__temp_qssa__.cu";
  // Only need to do this if chem is not NULL
  if (chem != NULL)
  {
    CodeOutStream temp_ost(temp_qssa_name, true, 80);
    emit_multi_qssa_code(temp_ost, qssa_passes, levels_per_pass, rrs_per_pass);
  }
  else
    temp_qssa_name = NULL;
#endif

  // Emit the full block of code into the actual output stream
  emit_multi_full_code(ost, temp_chem_name, NULL/*temp_qssa_name*/, cta_values_per_point);

  // Clean up our temporary files
  assert(system("rm -f __temp_chem__.cu") == 0);
#if 0
  if (chem != NULL)
    assert(system("rm -f __temp_qssa__.cu") == 0);
#endif
}

int TranslationUnit::multi_assign_reactions_to_warps(void) 
{
  // Sort reactions into three groups, weird, qssa-normal, remaining-normal
  // and then put them into warps based on this order so we can optimize both
  // for load balancing and getting the QSSA warps going as soon as possible.
  std::vector<Reaction*> weird_reacs;
  std::vector<Reaction*> qssa_normal;
  std::vector<Reaction*> other_normal;
  for (std::vector<Reaction*>::const_iterator it = act_reactions.begin();
        it != act_reactions.end(); it++)
  {
    if ((*it)->is_weird_reaction())
      weird_reacs.push_back(*it);
    else if ((chem != NULL) && (chem->is_qssa_reaction(*it)))
      qssa_normal.push_back(*it);
    else
      other_normal.push_back(*it);
  }
  // Make the set of chem and qssa warps
  unsigned total_warps = reac_warps * (32/warp_size);
  for (unsigned idx = 0; idx < total_warps; idx++)
    chem_warps.push_back(new Warp(idx));
  unsigned total_qssa = qss_warps;
  for (unsigned idx = 0; idx < total_qssa; idx++)
    qssa_warps.push_back(new Warp(chem_warps.size()+idx));

  if (chem != NULL)
  {
    // Do this differently depending on whether
    // we have a smaller effective warp size or not
    if (warp_size == 32)
    {
      unsigned index = 0;
      // Round-robin QSSA species to chemistry warps
      for (std::vector<ConnectedComponent*>::const_iterator cc = 
            chem->connected_components.begin(); cc !=
            chem->connected_components.end(); cc++)
      {
        ConnectedComponent *c = *cc;
        for (std::map<int,QSS*>::const_iterator it = c->species.begin();
              it != c->species.end(); it++)
        {
          chem_warps[index]->add_qss(it->second);
          index++;
          if (index == chem_warps.size())
            index = 0;
        }
      }
    }
    else
    {
      // If we have a smaller warp size, have all the sub-warps
      // within a full warp collaborate on the values
      unsigned index = 0;
      for (std::vector<ConnectedComponent*>::const_iterator cc = 
            chem->connected_components.begin(); cc !=
            chem->connected_components.end(); cc++)
      {
        ConnectedComponent *c = *cc;
        for (std::map<int,QSS*>::const_iterator it = c->species.begin();
              it != c->species.end(); it++)
        {
          for (int i = 0; i < int(32/warp_size); i++)
          {
            assert((index+i) < chem_warps.size());
            chem_warps[index+i]->add_qss(it->second);
          }
          index += (32/warp_size);
          if (index == chem_warps.size())
            index = 0;
        }
      }
    }
  }

  unsigned index = 0;
  int qssa_levels = 1; // start off having one
  // Round-robin assign weird reactions to warps
  for (std::vector<Reaction*>::const_iterator it = weird_reacs.begin();
        it != weird_reacs.end(); it++)
  {
    chem_warps[index]->add_reaction(*it);
    index++;
    if (index == chem_warps.size())
    {
      index = 0;
      qssa_levels++;
    }
  } 
  for (std::vector<Reaction*>::const_iterator it = qssa_normal.begin();
        it != qssa_normal.end(); it++)
  {
    chem_warps[index]->add_reaction(*it);
    index++;
    if (index == chem_warps.size())
    {
      index = 0;
      qssa_levels++;
    }
  }
  // Only assign reactions to the qssa warps as long our post qssa level
  // is less than the load, unless we are using all our warps for QSSA
  // in which case we ignore the load parameter.
  // Doesn't make sense to skip reactions if everyone is participating in QSSA
  if ((chem_warps.size()/(32/warp_size)) == qssa_warps.size())
    qssa_skip = 0;
  int post_qssa_level = 0;
  for (std::vector<Reaction*>::const_iterator it = other_normal.begin();
        it != other_normal.end(); it++)
  {
    chem_warps[index]->add_reaction(*it);
    index++;
    if (index >= ((post_qssa_level >= qssa_skip) ? chem_warps.size() : (chem_warps.size()-qss_warps)))
    {
      index = 0;
      post_qssa_level++;
    }
  }
  return qssa_levels;
}

void TranslationUnit::multi_construct_shared_memory_analyzers(int chem_levels_per_pass,
      int chem_passes, int qssa_levels_per_pass,
      int qssa_passes, int stif_levels_per_pass, int stif_passes,
      int output_levels_per_pass, int output_passes, int offset)
{
  if (spill_reac_rates) {
    chem_analyzer = NULL;
    stif_analyzer = NULL;
    out_analyzer = NULL;
    // Make the maximum number of reactions equal to the maximum of the total
    // number of reaction rates we need or the maximum we might ever need to store
    size_t total_rates = 2 * act_reactions.size();
    size_t total_possible = 2 * (chem_levels_per_pass+qssa_levels_per_pass) * chem_warps.size();
    sm_offset = total_rates > total_possible ? total_rates : total_possible;
    global_analyzer = new MultiAnalyzer((32/points_per_cta), chem_passes,
                                        offset+sm_offset);
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      int level = 0;
      for (std::vector<Reaction*>::const_iterator it = 
            chem_warps[wid]->reacs.begin(); it !=
            chem_warps[wid]->reacs.end(); it++)
      {
        size_t level_space = 2 * chem_warps.size();    
        int chem_loc = offset + level*level_space + wid;
        global_analyzer->add_forward_reac((*it),0/*pass*/,chem_loc);
        global_analyzer->add_reverse_reac((*it),0/*pass*/,chem_loc+chem_warps.size());
        level++;
      }
    }
  } else {
    global_analyzer = NULL;
    chem_analyzer = new MultiAnalyzer((32/points_per_cta), chem_passes, offset+2*chem_levels_per_pass*chem_warps.size());
    stif_analyzer = new MultiAnalyzer((32/points_per_cta), stif_passes, 2*stif_levels_per_pass*chem_warps.size());
    out_analyzer = new MultiAnalyzer((32/points_per_cta), output_passes, output_levels_per_pass*chem_warps.size());
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      int level = 0;
      for (std::vector<Reaction*>::const_iterator it = 
            chem_warps[wid]->reacs.begin(); it !=
            chem_warps[wid]->reacs.end(); it++)
      {
        int chem_pass = level/chem_levels_per_pass;
        int chem_loc = offset + wid + (level%chem_levels_per_pass)*2*chem_warps.size();
        chem_analyzer->add_forward_reac((*it),chem_pass,chem_loc);
        chem_analyzer->add_reverse_reac((*it),chem_pass,chem_loc+chem_warps.size());
        int stif_pass = level/stif_levels_per_pass;
        int stif_loc = wid + (level%stif_levels_per_pass)*2*chem_warps.size();
        stif_analyzer->add_forward_reac((*it),stif_pass,stif_loc);
        stif_analyzer->add_reverse_reac((*it),stif_pass,stif_loc+chem_warps.size());
        int output_pass = level/output_levels_per_pass;
        int output_loc = wid + (level%output_levels_per_pass)*chem_warps.size();
        out_analyzer->add_forward_reac((*it),output_pass,output_loc);
        level++;
      }
    }
  }
}

void TranslationUnit::emit_multi_chem_code(CodeOutStream &ost, int chem_passes, int chem_levels_per_pass,
                                            int qssa_passes, int qssa_levels_per_pass, 
                                            int qssa_levels, int stif_passes, int stif_levels_per_pass,
                                            int output_passes, int output_levels_per_pass, 
                                            int shared_offset, int free_shared_values)
{
  ost << "// Main loop\n";
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim for_pair(ost); 
  // Need a barrier here for anything in initialization or
  // from previous iterations
  emit_barrier(ost, 0, reac_warps, true/*wait*/);
  // Emit the code for computing mole fractions, gibbs species, etc
  emit_multi_init_chem_code(ost);
  // Emit the reaction code
  for (int pass = 0; pass < chem_passes; pass++)
  {
    int start_level = pass*chem_levels_per_pass;
    int stop_level = (pass+1)*chem_levels_per_pass;//exclusive
    // clamp the max level if necessary
    if (stop_level > int(chem_warps[0]->reacs.size()))
      stop_level = chem_warps[0]->reacs.size();
    bool qssa_pass = (chem != NULL) && (pass < qssa_passes);
    emit_multi_chem_pass(ost, start_level, stop_level, pass, qssa_pass);
    if ((chem != NULL) && (pass == (qssa_passes-1)))
      emit_multi_inline_qssa_code(ost,free_shared_values);
  }
  if (emit_debug)
  {
    ost << "if ((blockIdx.x == 0) && (blockIdx.y == 0) && (tid == 0) && (step == 0))\n";
    PairDelim if_pair(ost);
    // Iterate over all the warps and emit their reaction rates
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      if (wid == 0)
        ost << "if (wid == 0)\n";
      else
        ost << "else if (wid == " << wid << ")\n";
      PairDelim warp_pair(ost);
      Warp *warp = chem_warps[wid];
      for (unsigned idx = 0; idx < warp->reacs.size(); idx++)
      {
        Reaction *reac = warp->reacs[idx];
        ost << "printf(\"warp_rr_f[" << reac->idx << "] = %.8g\\n\", rr_f_" << idx << ");\n";
        ost << "printf(\"warp_rr_r[" << reac->idx << "] = %.8g\\n\", rr_r_" << idx << ");\n";
      }
    }
  }
  // Do QSSA and Stiffness if necessary
  if (chem != NULL)
  {
    // Scale by QSSA first
    emit_multi_qssa_scaling(ost, qssa_levels);
    if (emit_debug)
    {
      ost << "if ((blockIdx.x == 0) && (blockIdx.y == 0) && (tid == 0) && (step == 0))\n";
      PairDelim if_pair(ost);
      // Iterate over all the warps and emit their reaction rates
      for (unsigned wid = 0; wid < chem_warps.size(); wid++)
      {
        if (wid == 0)
          ost << "if (wid == 0)\n";
        else
          ost << "else if (wid == " << wid << ")\n";
        PairDelim warp_pair(ost);
        Warp *warp = chem_warps[wid];
        for (unsigned idx = 0; idx < warp->reacs.size(); idx++)
        {
          Reaction *reac = warp->reacs[idx];
          ost << "printf(\"warp_qssa_f[" << reac->idx << "] = %.8g\\n\", rr_f_" << idx << ");\n";
          ost << "printf(\"warp_qssa_r[" << reac->idx << "] = %.8g\\n\", rr_r_" << idx << ");\n";
        }
      }
    }
    emit_multi_init_stif_code(ost);
    // Now do the passes for stiffness
    for (int pass = 0; pass < stif_passes; pass++)
    {
      int start_level = pass*stif_levels_per_pass;
      int stop_level = (pass+1)*stif_levels_per_pass;//exclusive
      // clamp the max level if necessary
      if (stop_level > int(chem_warps[0]->reacs.size()))
        stop_level = chem_warps[0]->reacs.size(); 
      emit_multi_stif_pass(ost, pass, start_level, stop_level);
    }
    emit_multi_stif_scaling(ost);
    if (emit_debug)
    {
      ost << "if ((blockIdx.x == 0) && (blockIdx.y == 0) && (tid == 0) && (step == 0))\n";
      PairDelim if_pair(ost);
      // Iterate over all the warps and emit their reaction rates
      for (unsigned wid = 0; wid < chem_warps.size(); wid++)
      {
        if (wid == 0)
          ost << "if (wid == 0)\n";
        else
          ost << "else if (wid == " << wid << ")\n";
        PairDelim warp_pair(ost);
        Warp *warp = chem_warps[wid];
        for (unsigned idx = 0; idx < warp->reacs.size(); idx++)
        {
          Reaction *reac = warp->reacs[idx];
          ost << "printf(\"warp_stif_f[" << reac->idx << "] = %.8g\\n\", rr_f_" << idx << ");\n";
          ost << "printf(\"warp_stif_r[" << reac->idx << "] = %.8g\\n\", rr_r_" << idx << ");\n";
        }
      }
    }
  }
  emit_multi_init_out_code(ost);
  if (use_indexing)
  {
    assert(false); // this needs to be updated to use indexing
    for (int pass = 0; pass < output_passes; pass++)
    {
      int start_level = pass*output_levels_per_pass;
      int stop_level = (pass+1)*output_levels_per_pass;//exclusive
      // clamp the max level if necessary
      if (stop_level > int(chem_warps[0]->reacs.size()))
        stop_level = chem_warps[0]->reacs.size();
      emit_multi_out_pass(ost, pass, start_level, stop_level);
    }
  }
  else
  {
    // If we're not using indexing, we can do 2 passes at once 
    for (int pass = 0; pass < output_passes; pass++)
    {
      int start_level = pass*output_levels_per_pass;
      int stop_level = (pass+1)*output_levels_per_pass;//exclusive
      // clamp the max level if necessary
      if (stop_level > int(chem_warps[0]->reacs.size()))
        stop_level = chem_warps[0]->reacs.size();
      emit_multi_out_pass(ost, pass, start_level, stop_level);
    }
  }
  emit_multi_output(ost);
  // Finally update the pointers
  {
    PairDelim ptr_pair(ost);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << AVMOLWT_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
    if (chem != NULL)
      ost << DIFFUSION_ARRAY << " += slice_stride;\n";
    ost << WDOT_ARRAY << " += slice_stride;\n";
  } 
}

void TranslationUnit::emit_multi_init_chem_code(CodeOutStream &ost)
{
  ost << "/* Chemistry */\n";
  ost << REAL << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,k20);
  ost << REAL << " " << AVMOLWT << ";\n";
  emit_cuda_load(ost,AVMOLWT,AVMOLWT_ARRAY,k20);
  ost << REAL << " "  << PRESSURE << ";\n";
  emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,k20);
  const unsigned num_mass_fracs = (ordered_species.size()+chem_warps.size()-1)/chem_warps.size();
  const bool uniform_mass_fracs = ((ordered_species.size()%chem_warps.size()) == 0);
  ost << REAL << " local_mass_frac[" << num_mass_fracs << "];\n";
  for (unsigned idx = 0; idx < num_mass_fracs; idx++)
  {
    if (!uniform_mass_fracs && (idx == (num_mass_fracs-1)))
      ost << "if ((wid+" << (idx*chem_warps.size()) << ") < " << ordered_species.size() << ")\n";
    char src_offset[128];
    sprintf(src_offset,"(wid+%ld)*spec_stride",idx*chem_warps.size());
    char dst_offset[128];
    sprintf(dst_offset,"%d",idx);
    emit_cuda_load(ost,"local_mass_frac",dst_offset,MASS_FRAC_ARRAY,src_offset,k20);
  }
  ost << "const " << REAL << " otc = 1.0 / " << TEMPERATURE << ";\n";
  ost << "const " << REAL << " ortc = 1.0 / (" << TEMPERATURE << " * R0c);\n";
  ost << "const " << REAL << " vlntemp = log(" << TEMPERATURE ");\n";
  ost << "const " << REAL << " prt     = PA / (R0 * " << TEMPERATURE << ");\n";
  ost << "const " << REAL << " oprt    = 1.0 / prt;\n";
  ost << REAL << " sumyow = " << TEMPERATURE << " * " << AVMOLWT << " * ";
  // Scale by 1e-3 to account for conversion in
  // the computation of avmolwt
  if (use_dim)
    ost << "8.314510e+04";
  else
    ost << (8.314510e+04 * TEMPERATURE_REF / PRESSURE_REF);
  ost << ";\n";
  ost << "sumyow = " << PRESSURE << "/sumyow;\n";
  ost << REAL << " local_ctot = 0.0;\n";
  // Scale the mass fractions and write them out to shared memory
  for (unsigned idx = 0; idx < num_mass_fracs; idx++)
  {
    if (!uniform_mass_fracs && (idx == (num_mass_fracs-1)))
      ost << "if ((wid+" << (idx*chem_warps.size()) << ") < " << ordered_species.size() << ")\n";
    PairDelim spec_pair(ost);
    // Clamp the values if necessary
    ost << "local_mass_frac[" << idx << "] *= recip_molecular_masses[wid+"
        << (idx*chem_warps.size()) << "+step*step_stride];\n";
    ost << "local_mass_frac[" << idx << "] = (local_mass_frac[" << idx << "] > "
        << small_mole_frac << ") ? local_mass_frac[" << idx << "] : " << small_mole_frac << ";\n";
    ost << REAL << " temp_" << idx << " = "
        << "local_mass_frac[" << idx << "] * sumyow;\n";
    ost << "scratch[wid+" << (idx*chem_warps.size()) << "][tid] = "
        << "temp_" << idx << ";\n";
    ost << "local_ctot += temp_" << idx << ";\n";
  }
  // Write out our local ctot value
  ost << "scratch[wid+" << reac_offset << "][tid] = local_ctot;\n";
  // Indicate that everyone is done writing out values
  emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  if (reac_warps < 2)
  {
    fprintf(stderr,"ERROR: We need at least two reaction warps for warp-specialization!\n");
    exit(1);
  }
  // Pull off the first warp and have it compute the third-body values
  // Use the remaining warps to compute the gibbs values
  ost << "if ((threadIdx.x >> 5) == 0)\n";
  {
    PairDelim third_body_pair(ost);
    ost << REAL << " ctot = 0.0;\n";
    const unsigned thb_fracs = (chem_warps.size()+(32/points_per_cta)-1)/(32/points_per_cta);
    const bool uniform_thb_fracs = ((chem_warps.size()%(32/points_per_cta)) == 0);
    for (unsigned idx = 0; idx < thb_fracs; idx++)
    {
      if (!uniform_thb_fracs && (idx == (thb_fracs-1)))
        ost << "if ((wid+" << (idx*(32/points_per_cta)) << ") < " << ordered_species.size() << ")\n";
      ost << "ctot += scratch[wid+" << (reac_offset+(idx*(32/points_per_cta))) << "][tid];\n";
    }
    // Reduce ctot across all the sub-warps
    if (k20)
    {
      if (points_per_cta < 32)
        ost << INT << " hi_part,lo_part;\n";
      for (int i = points_per_cta; i < 32; i*=2)
      {
        ost << "hi_part = __shfl_xor(__double2hiint(ctot)," << i << ",32);\n"; 
        ost << "lo_part = __shfl_xor(__double2loint(ctot)," << i << ",32);\n";
        ost << "ctot += __hiloint2double(hi_part,lo_part);\n";
      }
    }
    else
    {
      char mask[64];
      sprintf(mask,"0x%x",(32/points_per_cta)-1);
      // Here we use warp-synchronous reductions linearly
      for (int idx = 1; idx < (32/points_per_cta); idx++)
      {
        ost << "if ((wid & " << mask << ") == " << idx << ")\n";
        ost << "  reduc_mirror[0][tid] = ctot;\n";
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  ctot += reduc_mirror[0][tid];\n";
      }
      // Then scatter back out so everyone has the same value
      ost << "if ((wid & " << mask << ") == 0)\n";
      ost << "  reduc_mirror[0][tid] = ctot;\n";
      ost << "ctot = reduc_mirror[0][tid];\n";
    }
    // Everybdoy now has the same value of ctot
    for (unsigned idx = 0; idx < third_bodies.size(); idx++)
    {
      ost << REAL << " thb_" << idx << " = ctot";
      for (std::map<Species*,double>::const_iterator it = third_bodies[idx]->components.begin();
            it != third_bodies[idx]->components.end(); it++)
      {
        int index = find_ordered_index(it->first);
        if (index != -1)
          ost << " + " << it->second << "*scratch[" << index << "][tid]";
      }
      ost << ";\n";
      if (points_per_cta < 32)
        ost << "if (wid == 0)\n";
      ost << "scratch[" << (thb_offset+idx) << "][tid] = thb_" << idx << ";\n";
    }
  }
  ost << "else\n";
  {
    PairDelim gibbs_pair(ost);
    ost << "const " << REAL << " &tk1 = " << TEMPERATURE << ";\n";
    ost << "const " << REAL << " &tklog = vlntemp;\n";
    ost << REAL << " tk2 = tk1 * tk1;\n";
    ost << REAL << " tk3 = tk1 * tk2;\n";
    ost << REAL << " tk4 = tk1 * tk3;\n";
    ost << REAL << " tk5 = tk1 * tk4;\n";
    const unsigned gibbs_warps = chem_warps.size() - (32/points_per_cta);
    const unsigned gibbs_steps = (gibbs_species.size() + gibbs_warps-1)/gibbs_warps;
    const unsigned uniform_gibbs = ((gibbs_species.size()%gibbs_warps) == 0);
    for (unsigned step = 0; step < gibbs_steps; step++)
    {
      if (!uniform_gibbs && (step == (gibbs_steps-1)))
        ost << "if ((wid+" << (int(step*gibbs_warps)-(32/points_per_cta)) << ") < " << gibbs_species.size() << ")\n";
      PairDelim step_pair(ost);
      ost << INT << " index = wid+" << (int(step*gibbs_warps)-(32/points_per_cta)) << "+step*step_stride;\n";
      ost << "if (tk1 > gibbs_temp[index])\n";
      {
        PairDelim if_pair(ost); 
        if (!spill_gibbs)
          ost << "scratch[index+" << gibbs_offset << "][tid] = ";
        else
          ost << "gibbs_g[smid*" << gibbs_species.size() << "+" << gibbs_offset << "][tid] = ";
        ost << "gibbs_high[index][0]*tk1*(";
        if (use_dim)
          ost << "1";
        else
          ost << (1-log(TEMPERATURE_REF));
        ost << "-tklog) + ";
        ost << "gibbs_high[index][1]*tk2 + ";
        ost << "gibbs_high[index][2]*tk3 + ";
        ost << "gibbs_high[index][3]*tk4 + ";
        ost << "gibbs_high[index][4]*tk5 + ";
        ost << "(";
        ost << "gibbs_high[index][5] - ";
        ost << "tk1*gibbs_high[index][6]);\n";
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        if (!spill_gibbs)
          ost << "scratch[index+" << gibbs_offset << "][tid] = ";
        else
          ost << "gibbs_g[smid*" << gibbs_species.size() << "+" << gibbs_offset << "][tid] = ";
        ost << "gibbs_low[index][0]*tk1*(";
        if (use_dim)
          ost << "1";
        else
          ost << (1-log(TEMPERATURE_REF));
        ost << "-tklog) + ";
        ost << "gibbs_low[index][1]*tk2 + ";
        ost << "gibbs_low[index][2]*tk3 + ";
        ost << "gibbs_low[index][3]*tk4 + ";
        ost << "gibbs_low[index][4]*tk5 + ";
        ost << "(";
        ost << "gibbs_low[index][5] - ";
        ost << "tk1*gibbs_low[index][6]);\n";
      }
    }
  }
  // Barrier when we're done here since we're now ready to start computing reaction rates
  emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  // Emit declarations for our a values and denom val
  if (chem != NULL)
  {
    // Figure out the maximum number of a values and denoms that we need for each warp  
    int max_avals = -1;
    int max_denoms = -1;
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      int avals = 0;
      int denoms = 0;
      for (std::vector<QSS*>::const_iterator it = chem_warps[wid]->qss_specs.begin();
            it != chem_warps[wid]->qss_specs.end(); it++)
      {
        denoms++;
        avals += (1 + (*it)->member_species.size());
      }
      if (avals > max_avals)
        max_avals = avals;
      if (denoms > max_denoms)
        max_denoms = denoms;
    }
    assert(max_avals > 0);
    assert(max_denoms > 0);
    ost << REAL << " avals[" << max_avals << "];\n";
    ost << REAL << " denoms[" << max_denoms << "];\n";
    for (int idx = 0; idx < max_avals; idx++)
      ost << "avals[" << idx << "] = 0.0;\n";
    for (int idx = 0; idx < max_denoms; idx++)
      ost << "denoms[" << idx << "] = 0.0;\n";
  }
}

void TranslationUnit::emit_multi_chem_pass(CodeOutStream &ost, int start_level, int stop_level, int pass, bool qssa_pass)
{
  for (int level = start_level; level < stop_level; level++)
  {
    // Little hack to keep the compiler from doing dumb things
    if (!spill_reac_rates) 
    {
      if (level == 0)
        ost << REAL << " rr_f_0 = 0.0, rr_r_0 = 0.0;\n";
      else
        ost << REAL << " rr_f_" << level << " = rr_f_" << (level-1)
            << ", rr_r_" << level << " = rr_r_" << (level-1) << ";\n";
    }
    emit_multi_chem_level(ost, level);
  }
  // If we're not using indexing, now we need to emit the scaling factors for each warp
  if (!use_indexing)
  {
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      if (wid == 0)
        ost << "if (wid == " << wid << ")\n";
      else
        ost << "else if (wid == " << wid << ")\n";
      PairDelim frac_pair(ost);
      std::set<Species*> loaded_fracs;
      std::set<ThirdBody*> loaded_thbs;
      for (int level = start_level; level < stop_level; level++)
      {
        if (level < int(chem_warps[wid]->reacs.size()))
        {
          Reaction *reac = chem_warps[wid]->reacs[level];
          // Do the forward contributions first unless it is an unimportant reaction
          if ((chem == NULL) || (chem->forward_unimportant.find(reac) == chem->forward_unimportant.end()))
          {
            bool forward_first_iter = true;
            for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
                  it != reac->forward.end(); it++)
            {
              if (it->first->qss_species)
                continue;
              // Load the species if we don't have it yet 
              if (loaded_fracs.find(it->first) == loaded_fracs.end())
              {
                int index = find_ordered_index(it->first);
                assert(index != -1);
                ost << REAL << " mole_frac_" << it->first->code_name << " = scratch[" << index << "][tid];\n";
                loaded_fracs.insert(it->first);
              }
              assert(it->second > 0);
              for (int idx = 0; idx < it->second; idx++)
              {
                if (spill_reac_rates && forward_first_iter)
                {
                  int offset = level * 2 * chem_warps.size();
                  ost << REAL << " rr_f_" << level << " = rr_g[smid*"
                      << sm_offset << "+" << offset << "+wid][tid];\n";
                  forward_first_iter = false;
                }
                ost << "rr_f_" << level << " *= mole_frac_" << it->first->code_name << ";\n";
              }
            }
            if ((reac->thb != NULL) && !reac->pressure_dep)
            {
              if (loaded_thbs.find(reac->thb) == loaded_thbs.end())
              {
                int index = thb_offset + reac->thb->idx;
                assert(index != -1);
                ost << REAL << " thb_" << reac->thb->idx << " = scratch[" << index << "][tid];\n";
                loaded_thbs.insert(reac->thb);
              }
              if (spill_reac_rates && forward_first_iter)
              {
                int offset = level * 2 * chem_warps.size();
                ost << REAL << " rr_f_" << level << " = rr_g[smid*"
                    << sm_offset << "+" << offset << "+wid][tid];\n";
                forward_first_iter = false;
              }
              ost << "rr_f_" << level << " *= thb_" << reac->thb->idx << ";\n";
            }
            if (!forward_first_iter)
            {
              int offset = level * 2 * chem_warps.size();
              ost << "rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = "
                  << "rr_f_" << level << ";\n";
            }
          }
          else
          {
            // This is an unimportant reaction
            if (!spill_reac_rates)
              ost << "rr_f_" << level << " = 0.0;\n";
            else
            {
              int offset = level * 2 * chem_warps.size();
              ost << "rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = 0.0;\n";
            }
          }
          if ((chem == NULL) || (chem->backward_unimportant.find(reac) == chem->backward_unimportant.end()))
          {
#if 0
            if (reac->low.enabled)
            {
              // Handle the case where we need to scale the reverse rate by the forward
              // fractions as well for low reactions
              for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
                    it != reac->forward.end(); it++)
              {
                if (it->first->qss_species)
                  continue;
                // Load the species if we don't have it yet 
                if (loaded_fracs.find(it->first) == loaded_fracs.end())
                {
                  int index = find_ordered_index(it->first);
                  assert(index != -1);
                  ost << REAL << " mole_frac_" << it->first->code_name << " = scratch[" << index << "][tid];\n";
                  loaded_fracs.insert(it->first);
                }
                assert(it->second > 0);
                for (int idx = 0; idx < it->second; idx++)
                {
                  ost << "rr_r_" << level << " *= mole_frac_" << it->first->code_name << ";\n";
                }
              }
            }
#endif
            // Do backwards
            if (reac->reversible)
            {
              bool reverse_first_iter = true;
              for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                    it != reac->backward.end(); it++)
              {
                if (it->first->qss_species)
                  continue;
                if (loaded_fracs.find(it->first) == loaded_fracs.end())
                {
                  int index = find_ordered_index(it->first);
                  ost << REAL << " mole_frac_" << it->first->code_name << " = scratch[" << index << "][tid];\n";
                  loaded_fracs.insert(it->first);
                }
                assert(it->second > 0);
                for (int idx = 0; idx < it->second; idx++)
                {
                  if (spill_reac_rates && reverse_first_iter)
                  {
                    int offset = level * 2 * chem_warps.size() + chem_warps.size();
                    ost << REAL << " rr_r_" << level << " = rr_g[smid*"
                        << sm_offset << "+" << offset << "+wid][tid];\n";
                    reverse_first_iter = false;
                  }
                  ost << "rr_r_" << level << " *= mole_frac_" << it->first->code_name << ";\n";
                }
              }
              if ((reac->thb != NULL) && !reac->pressure_dep)
              {
                if (loaded_thbs.find(reac->thb) == loaded_thbs.end())
                {
                  int index = thb_offset + reac->thb->idx;
                  ost << REAL << " thb_" << reac->thb->idx << " = scratch[" << index << "][tid];\n";
                  loaded_thbs.insert(reac->thb);
                }
                if (spill_reac_rates && reverse_first_iter)
                {
                  int offset = level * 2 * chem_warps.size() + chem_warps.size();
                  ost << REAL << " rr_r_" << level << " = rr_g[smid*"
                      << sm_offset << "+" << offset << "+wid][tid];\n";
                  reverse_first_iter = false;
                }
                ost << "rr_r_" << level << " *= thb_" << reac->thb->idx << ";\n";
              }
              if (!reverse_first_iter)
              {
                int offset = level * 2 * chem_warps.size() + chem_warps.size();
                ost << "rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = "
                    << "rr_r_" << level << ";\n";
              }
            }
          }
          else
          {
            // This is an unimportant rate
            if (!spill_reac_rates)
              ost << "rr_r_" << level << " = 0.0;\n";
            else
            {
              int offset = level * 2 * chem_warps.size() + chem_warps.size();
              ost << "rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = 0.0;\n";
            }
          }
        }
      }
    }
  }
  if (qssa_pass)
  {
    // Wait until it is safe to write values into shared memory
    //emit_barrier(ost, 0, (reac_warps+qss_warps), true/*blocking*/);
    // Write our values into shared
    if (!spill_reac_rates)
    {
      for (int level = start_level; level < stop_level; level++)
      {
        ost << "scratch[wid+" << (reac_offset+(chem_warps.size()*(2*(level-start_level)))) << "][tid] = rr_f_" << level << ";\n";
        ost << "scratch[wid+" << (reac_offset+(chem_warps.size()*(2*(level-start_level)+1))) << "][tid] = rr_r_" << level << ";\n";
      }
    }
    // Inidicate that we are done writing
    //emit_barrier(ost, 1, (reac_warps+qss_warps), false/*blocking*/);
    emit_barrier(ost, 0, reac_warps, true/*blocking*/);
    // Now update our avalues and denoms
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      if (wid == 0)
        ost << "if (wid == " << wid << ")\n";
      else
        ost << "else if (wid == " << wid << ")\n";
      PairDelim warp_pair(ost);
      std::set<int> loaded_rates;
      int aindex = 0;
      int dindex = 0;
      for (std::vector<QSS*>::const_iterator qit = chem_warps[wid]->qss_specs.begin();
            qit != chem_warps[wid]->qss_specs.end(); qit++)
      {
        QSS *qss = *qit;
        // Zero value first
        for (std::map<Reaction*,int>::const_iterator it = qss->forward_zeros.begin();
              it != qss->forward_zeros.end(); it++)
        {
          int loc = !spill_reac_rates ? chem_analyzer->find_forward_location(it->first, pass) :
                                        global_analyzer->find_forward_location(it->first, pass);
          if (loc == -1)
            continue;
          assert(it->second > 0);
          if (loaded_rates.find(loc) == loaded_rates.end())
          {
            if (!spill_reac_rates)
              ost << REAL << " rate_" << loc << " = scratch[" << loc << "][tid];\n";
            else
              ost << REAL << " rate_" << loc << " = rr_g[smid*"
                  << sm_offset << "+" << loc << "][tid];\n";
            loaded_rates.insert(loc);
          }
          if (it->second == 1)
            ost << "avals[" << aindex << "] += rate_" << loc << ";\n";
          else
            ost << "avals[" << aindex << "] = __fma_rn(" << double(it->second)
                << ",rate_" << loc << ",avals[" << aindex << "]);\n";
        }
        for (std::map<Reaction*,int>::const_iterator it = qss->backward_zeros.begin();
              it != qss->backward_zeros.end(); it++)
        {
          int loc = !spill_reac_rates ? chem_analyzer->find_reverse_location(it->first, pass) :
                                        global_analyzer->find_reverse_location(it->first, pass);
          if (loc == -1)
            continue;
          assert(it->second > 0);
          if (loaded_rates.find(loc) == loaded_rates.end())
          {
            if (!spill_reac_rates)
              ost << REAL << " rate_" << loc << " = scratch[" << loc << "][tid];\n";
            else
              ost << REAL << " rate_" << loc << " = rr_g[smid*"
                  << sm_offset << "+" << loc << "][tid];\n";
            loaded_rates.insert(loc);
          }
          if (it->second == 1)
            ost << "avals[" << aindex << "] += rate_" << loc << ";\n";
          else
            ost << "avals[" << aindex << "] = __fma_rn(" << double(it->second)
                << ",rate_" << loc << ",avals[" << aindex << "]);\n";
        }
        aindex++;
        // Then other values
        for (std::set<int>::const_iterator ait = qss->member_species.begin();
              ait != qss->member_species.end(); ait++)
        {
          for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = 
                qss->forward_contributions.begin(); it !=
                qss->forward_contributions.end(); it++)
          {
            if (it->first.first != *ait)
              continue;
            int loc = !spill_reac_rates ? 
              chem_analyzer->find_forward_location(it->first.second, pass) :
              global_analyzer->find_forward_location(it->first.second, pass);
            if (loc == -1)
              continue;
            assert(it->second > 0);
            if (loaded_rates.find(loc) == loaded_rates.end())
            {
              if (!spill_reac_rates)
                ost << REAL << " rate_" << loc << " = scratch[" << loc << "][tid];\n";
              else
                ost << REAL << " rate_" << loc << " = rr_g[smid*"
                    << sm_offset << "+" << loc << "][tid];\n";
              loaded_rates.insert(loc);
            }
            if (it->second == 1)
              ost << "avals[" << aindex << "] += rate_" << loc << ";\n";
            else
              ost << "avals[" << aindex << "] = __fma_rn(" << double(it->second)
                  << ",rate_" << loc << ",avals[" << aindex << "]);\n";
          }
          for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = 
                qss->backward_contributions.begin(); it !=
                qss->backward_contributions.end(); it++)
          {
            if (it->first.first != *ait)
              continue;
            int loc = !spill_reac_rates ? 
              chem_analyzer->find_reverse_location(it->first.second, pass) :
              global_analyzer->find_reverse_location(it->first.second, pass);
            if (loc == -1)
              continue;
            if (loaded_rates.find(loc) == loaded_rates.end())
            {
              if (!spill_reac_rates)
                ost << REAL << " rate_" << loc << " = scratch[" << loc << "][tid];\n";
              else
                ost << REAL << " rate_" << loc << " = rr_g[smid*"
                    << sm_offset << "+" << loc << "][tid];\n";
              loaded_rates.insert(loc);
            }
            assert(it->second > 0);
            if (it->second == 1)
              ost << "avals[" << aindex << "] += rate_" << loc << ";\n";
            else
              ost << "avals[" << aindex << "] = __fma_rn(" << double(it->second)
                  << ",rate_" << loc << ",avals[" << aindex << "]);\n";
          }
          aindex++;
        }
        // Then denom
        for (std::set<Reaction*>::const_iterator it = qss->forward_denom.begin();
              it != qss->forward_denom.end(); it++)
        {
          int loc = !spill_reac_rates ? 
            chem_analyzer->find_forward_location(*it, pass) :
            global_analyzer->find_forward_location(*it, pass);
          if (loc == -1)
            continue;
          if (loaded_rates.find(loc) == loaded_rates.end())
          {
            if (!spill_reac_rates)
              ost << REAL << " rate_" << loc << " = scratch[" << loc << "][tid];\n";
            else
              ost << REAL << " rate_" << loc << " = rr_g[smid*"
                  << sm_offset << "+" << loc << "][tid];\n";
            loaded_rates.insert(loc);
          }
          ost << "denoms[" << dindex << "] += rate_" << loc << ";\n";
        }
        for (std::set<Reaction*>::const_iterator it = qss->backward_denom.begin();
              it != qss->backward_denom.end(); it++)
        {
          int loc = !spill_reac_rates ? 
            chem_analyzer->find_reverse_location(*it, pass) :
            global_analyzer->find_reverse_location(*it, pass);
          if (loc == -1)
            continue;
          if (loaded_rates.find(loc) == loaded_rates.end())
          {
            if (!spill_reac_rates)
              ost << REAL << " rate_" << loc << " = scratch[" << loc << "][tid];\n";
            else
              ost << REAL << " rate_" << loc << " = rr_g[smid*"
                  << sm_offset << "+" << loc << "][tid];\n";
            loaded_rates.insert(loc);
          }
          ost << "denoms[" << dindex << "] += rate_" << loc << ";\n";
        }
        dindex++;
      }
    }
    emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  }
}

void TranslationUnit::emit_multi_chem_level(CodeOutStream &ost, int level)
{
  ost << "/* Chemistry for level " << level << " */\n";
  int last_warp = -1;
  for (unsigned idx = 0; idx < chem_warps.size(); idx++)
  {
    if (level < int(chem_warps[idx]->reacs.size()))
    {
      last_warp = idx;
    }
  }
  assert(last_warp != -1);
  if (last_warp < int(chem_warps.size()-1))
    ost << "if (wid < " << (last_warp+1) << ")\n";
  PairDelim level_pair(ost);
  if (spill_reac_rates)
    ost << REAL << " rr_f_" << level << " = 0.0, rr_r_" << level << " = 0.0;\n";
  // For all reactions, regardless of weirdness, we want to set up the constants and do the two 
  // initial exponent computations
  int meta_indexes[6];
  for (int idx = 0; idx < 6; idx++)
    meta_indexes[idx] = -1;

  for (int wid = 0; wid < int(chem_warps.size()); wid++)
  {
    Reaction *reac = chem_warps[wid]->reacs[level];
    double coeffs[6];
    if (wid <= last_warp)
    {
      if (reac->low.enabled)
      {
        coeffs[0] = reac->low.beta;
        coeffs[1] = -reac->low.e;
        if (use_dim)
          coeffs[2] = reac->low.a;
        else
          coeffs[2] = reac->low.a*REACTION_RATE_REF*pow(TEMPERATURE_REF,reac->low.beta);
        coeffs[3] = reac->beta;
        coeffs[4] = -reac->e;
        if (use_dim)
          coeffs[5] = reac->a;
        else
          coeffs[5] = reac->a*REACTION_RATE_REF*pow(TEMPERATURE_REF,reac->beta);
      }
      else
      {
        coeffs[0] = reac->beta;
        coeffs[1] = -reac->e;
        if (use_dim)
          coeffs[2] = reac->a;
        else
          coeffs[2] = reac->a*REACTION_RATE_REF*pow(TEMPERATURE_REF,reac->beta);
        if (reac->reversible)
        {
          if (reac->rev.enabled)
          {
            coeffs[3] = reac->rev.beta;
            coeffs[4] = -reac->rev.e;
            if (use_dim)
              coeffs[5] = reac->rev.a;
            else
              coeffs[5] = reac->rev.a*REACTION_RATE_REF*pow(TEMPERATURE_REF,reac->rev.beta);
          }
          else
          {
            // Otherwise, we can handle up to one duplicate rate
            if (reac->duplicate.enabled && (reac->duplicate.ptr.duplicates != NULL))
            {
              assert(reac->duplicate.enabled);
              assert(reac->duplicate.owner);
              if (reac->duplicate.ptr.duplicates->size() > 1)
              {
                fprintf(stderr,"ERROR: Only supporting reactions with a single duplicate for warp-specialzation!\n");
                fprintf(stderr,"Reaction %d has %ld duplicates\n", reac->idx, reac->duplicate.ptr.duplicates->size());
                exit(1);
              }
              Reaction *dup_reac = reac->duplicate.ptr.duplicates->front();
              coeffs[3] = dup_reac->beta;
              coeffs[4] = dup_reac->e;
              if (use_dim)
                coeffs[5] = dup_reac->a;
              else
                coeffs[5] = dup_reac->a*REACTION_RATE_REF*pow(TEMPERATURE_REF,dup_reac->beta);
            }
            else
            {
              // Just use dummy values
              coeffs[3] = 0.0;
              coeffs[4] = 0.0;
              coeffs[5] = 0.0;
            }
          }
        }
        else
        {
          coeffs[3] = 0.0;
          coeffs[4] = 0.0;
          coeffs[5] = 0.0;
        }
      }
    }
    else
    {
      for (int idx = 0; idx < 6; idx++)
        coeffs[idx] = 0.0;
    }
    for (int idx = 0; idx < 6; idx++)
    {
      int global_idx = chem_warps[wid]->add_real(coeffs[idx], 0/*version*/);
      if (meta_indexes[idx] == -1)
        meta_indexes[idx] = global_idx;
      else
        assert(meta_indexes[idx] == global_idx);
    }
  }
  // Now we can emit the code for the first two arrhenius operations
  if (k20)
  {
    // First arrhenius
    {
      PairDelim arr_pair(ost);
      ost << INT << " hi_part, lo_part;\n";
      int index_offset = meta_indexes[0]/points_per_cta;
      int index_thread = meta_indexes[0]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << REAL << " arrhenius = __hiloint2double(hi_part,lo_part) * vlntemp;\n";
      index_offset = meta_indexes[1]/points_per_cta;
      index_thread = meta_indexes[1]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "arrhenius = __fma_rn(__hiloint2double(hi_part,lo_part), ortc, arrhenius);\n";
      ost << "arrhenius = exp(arrhenius);\n";
      index_offset = meta_indexes[2]/points_per_cta;
      index_thread = meta_indexes[2]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "rr_f_" << level << " = __hiloint2double(hi_part,lo_part) * arrhenius;\n";
    }
    // Second arrhenius
    {
      PairDelim arr_pair(ost);
      ost << INT << " hi_part, lo_part;\n";
      int index_offset = meta_indexes[3]/points_per_cta;
      int index_thread = meta_indexes[3]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << REAL << " arrhenius = __hiloint2double(hi_part,lo_part) * vlntemp;\n";
      index_offset = meta_indexes[4]/points_per_cta;
      index_thread = meta_indexes[4]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "arrhenius = __fma_rn(__hiloint2double(hi_part,lo_part), ortc, arrhenius);\n";
      ost << "arrhenius = exp(arrhenius);\n";
      index_offset = meta_indexes[5]/points_per_cta;
      index_thread = meta_indexes[5]%points_per_cta;
      ost << "hi_part = __shfl(__double2hiint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "lo_part = __shfl(__double2loint(real_constants[" << index_offset << "]),"
          << index_thread << "," << points_per_cta << ");\n";
      ost << "rr_r_" << level << " = __hiloint2double(hi_part,lo_part) * arrhenius;\n";
    }
  }
  else
  {
    // First arrhenius
    {
      PairDelim arr_pair(ost);
      int index_offset = meta_indexes[0]/points_per_cta;
      int index_thread = meta_indexes[0]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << REAL << " arrhenius = real_mirror[wid] * vlntemp;\n";
      index_offset = meta_indexes[1]/points_per_cta;
      index_thread = meta_indexes[1]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << "arrhenius = __fma_rn(real_mirror[wid], ortc, arrhenius);\n";
      ost << "arrhenius = exp(arrhenius);\n";
      index_offset = meta_indexes[2]/points_per_cta;
      index_thread = meta_indexes[2]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << "rr_f_" << level << " = real_mirror[wid] * arrhenius;\n";
    }
    // Second arrhenius
    {
      PairDelim arr_pair(ost);
      int index_offset = meta_indexes[3]/points_per_cta;
      int index_thread = meta_indexes[3]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << REAL << " arrhenius = real_mirror[wid] * vlntemp;\n";
      index_offset = meta_indexes[4]/points_per_cta;
      index_thread = meta_indexes[4]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << "arrhenius = __fma_rn(real_mirror[wid], ortc, arrhenius);\n";
      ost << "arrhenius = exp(arrhenius);\n";
      index_offset = meta_indexes[5]/points_per_cta;
      index_thread = meta_indexes[5]%points_per_cta;
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  real_mirror[wid] = real_constants[" << index_offset << "];\n";
      ost << "rr_r_" << level << " = real_mirror[wid] * arrhenius;\n";
    }
  }
  // Now emit code for any weird reactions
  {
    bool first_weird = true;
    for (int wid = 0; wid <= last_warp; wid++)
    {
      Reaction *reac = chem_warps[wid]->reacs[level];
      if (reac->is_weird_reaction())
      {
        if (first_weird)
          ost << "if (wid == " << wid << ")\n";
        else
          ost << "else if (wid == " << wid << ")\n";
        PairDelim weird_pair(ost);
        reac->emit_warp_specialized_weird_reaction(ost, level, -1/*buffer*/);
        first_weird = false;
      }
    }
  }
  // Finally scale all the reaction rates by the molar values
  if (use_indexing)
  {
    int max_forward = -1;
    int max_reverse = -1;
    for (int wid = 0; wid <= last_warp; wid++)
    {
      Reaction *reac = chem_warps[wid]->reacs[level];
      int num_forward = 0;
      int num_backward = 0;
      for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
            it != reac->forward.end(); it++)
      {
        assert(it->second > 0);
        if (it->first->qss_species)
          continue;
        num_forward += it->second;
        // Nasty case here, if it is a low reaction then we
        // also need to apply the forward mole fractions to 
        // the reverse rate as well
        //if (reac->low.enabled)
        //  num_backward += it->second;
      }
      if ((reac->thb != NULL) && !reac->pressure_dep)
        num_forward++;
      if (num_forward > max_forward)
        max_forward = num_forward;
      if (reac->reversible)
      {
        for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
              it != reac->backward.end(); it++)
        {
          assert(it->second > 0);
          if (it->first->qss_species)
            continue;
          num_backward += it->second;
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
          num_backward++;
      }
      if (num_backward > max_reverse)
        max_reverse = num_backward;
    }
    // Set up the indexes for each of the warps 
    std::vector<int> forward_meta(max_forward, -1);
    std::vector<int> reverse_meta(max_reverse, -1);
    for (int wid = 0; wid < int(chem_warps.size()); wid++)
    {
      if (wid <= last_warp)
      {
        Reaction *reac = chem_warps[wid]->reacs[level];
        int forward_idx = 0;
        for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
              it != reac->forward.end(); it++)
        {
          assert(it->second > 0);
          if (it->first->qss_species)
            continue;
          int index = find_ordered_index(it->first);
          assert(index != -1);
          for (int idx = 0; idx < it->second; idx++)
          {
            int global_idx = chem_warps[wid]->add_index(index, 0/*version*/);
            if (forward_meta[forward_idx] == -1)
              forward_meta[forward_idx] = global_idx;
            else
              assert(forward_meta[forward_idx] == global_idx);
            forward_idx++;
          }
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          int index = ordered_species.size() + reac->thb->idx;
          int global_idx = chem_warps[wid]->add_index(index, 0/*version*/);
          if (forward_meta[forward_idx] == -1)
            forward_meta[forward_idx] = global_idx;
          else
            assert(forward_meta[forward_idx] == global_idx);
          forward_idx++;
        }
        // Handle any open slots by pointing them at the unity value
        while (forward_idx < int(forward_meta.size()))
        {
          int global_idx = chem_warps[wid]->add_index(this->shared_unity, 0/*version*/);
          if (forward_meta[forward_idx] == -1)
            forward_meta[forward_idx] = global_idx;
          else
            assert(forward_meta[forward_idx] == global_idx);
          forward_idx++;
        }
        int reverse_idx = 0;
        // For any low reaction, also apply forward rates to the backward rate
#if 0
        if (reac->low.enabled)
        {
          for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
                it != reac->forward.end(); it++)
          {
            assert(it->second > 0);
            if (it->first->qss_species)
              continue;
            int index = find_ordered_index(it->first);
            assert(index != -1);
            for (int idx = 0; idx < it->second; idx++)
            {
              int global_idx = chem_warps[wid]->add_index(index, 0/*version*/);
              if (reverse_meta[reverse_idx] == -1)
                reverse_meta[reverse_idx] = global_idx;
              else
                assert(reverse_meta[reverse_idx] == global_idx);
              reverse_idx++;
            }
          }
        }
#endif
        if (reac->reversible)
        {
          for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                it != reac->backward.end(); it++)
          {
            assert(it->second > 0);
            if (it->first->qss_species)
              continue;
            int index = find_ordered_index(it->first);
            assert(index != -1);
            for (int idx = 0; idx < it->second; idx++)
            {
              int global_idx = chem_warps[wid]->add_index(index, 0/*version*/);
              if (reverse_meta[reverse_idx] == -1)
                reverse_meta[reverse_idx] = global_idx;
              else
                assert(reverse_meta[reverse_idx] == global_idx);
              reverse_idx++;
            }
          }
          if ((reac->thb != NULL) && !reac->pressure_dep)
          {
            int index = ordered_species.size() + reac->thb->idx;
            int global_idx = chem_warps[wid]->add_index(index, 0/*version*/);
            if (reverse_meta[reverse_idx] == -1)
              reverse_meta[reverse_idx] = global_idx;
            else
              assert(reverse_meta[reverse_idx] == global_idx);
            reverse_idx++;
          }
        }
        // Handle any leftover open spots
        while (reverse_idx < int(reverse_meta.size()))
        {
          int global_idx = chem_warps[wid]->add_index(this->shared_unity, 0/*version*/);
          if (reverse_meta[reverse_idx] == -1)
            reverse_meta[reverse_idx] = global_idx;
          else
            assert(reverse_meta[reverse_idx] == global_idx);
          reverse_idx++;
        }
      }
      else
      {
        // No reaction here so make everything point to the unity value
        for (unsigned idx = 0; idx < forward_meta.size(); idx++)
        {
          int global_idx = chem_warps[wid]->add_index(this->shared_unity, 0/*version*/);
          if (forward_meta[idx] == -1)
            forward_meta[idx] = global_idx;
          else
            assert(forward_meta[idx] == global_idx);
        }
        for (unsigned idx = 0; idx < reverse_meta.size(); idx++)
        {
          int global_idx = chem_warps[wid]->add_index(this->shared_unity, 0/*version*/);
          if (reverse_meta[idx] == -1)
            reverse_meta[idx] = global_idx;
          else
            assert(reverse_meta[idx] == global_idx);
        }
      }
    }
    ost << INT << " index;\n";
    // Now we get to emit the code for doing the updates
    bool forward_first_iter = true;
    for (unsigned idx = 0; idx < forward_meta.size(); idx++)
    {
      if (spill_reac_rates && forward_first_iter)
      {
        int offset = level * 2 * chem_warps.size();
        ost << REAL << " rr_f_" << level << " = rr_g[smid*"
            << sm_offset << "+" << offset << "+wid][tid];\n";
        forward_first_iter = false;
      }
      int index_offset = forward_meta[idx]/points_per_cta;
      int index_thread = forward_meta[idx]%points_per_cta;  
      if (k20)
      {
        ost << "index = __shfl(index_constants[" << index_offset << "],"
            << index_thread << "," << points_per_cta << ");\n";
      }
      else
      {
        ost << "if (tid == " << index_thread << ")\n";
        ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
        ost << "index = int_mirror[wid];\n";
      }
      ost << "rr_f_" << level << " *= buffer[index][tid];\n";
    }
    bool reverse_first_iter = true; 
    for (unsigned idx = 0; idx < reverse_meta.size(); idx++)
    {
      if (spill_reac_rates && reverse_first_iter)
      {
        int offset = level * 2 * chem_warps.size() + chem_warps.size();
        ost << REAL << " rr_r_" << level << " = rr_g[smid*"
            << sm_offset << "+" << offset << "+wid][tid];\n";
        reverse_first_iter = false;
      }
      int index_offset = reverse_meta[idx]/points_per_cta;
      int index_thread = reverse_meta[idx]%points_per_cta;
      if (k20)
      {
        ost << "index = __shfl(index_constants[" << index_offset << "],"
            << index_thread << "," << points_per_cta << ");\n";
      }
      else
      {
        ost << "if (tid == " << index_thread << ")\n";
        ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
        ost << "index = int_mirror[wid];\n";
      }
      ost << "rr_r_" << level << " *= buffer[index][tid];\n";
    }
    if (!forward_first_iter)
    {
      int offset = level * 2 * chem_warps.size();
      ost << "rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = "
          << "rr_f_" << level << ";\n";
    }
    if (!reverse_first_iter)
    {
      int offset = level * 2 * chem_warps.size() + chem_warps.size();
      ost << "rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = "
          << "rr_r_" << level << ";\n";
    }
    // Finally if we need to set any unimportant reaction rates
    if (chem != NULL)
    {
      for (unsigned wid = 0; wid < chem_warps.size(); wid++)
      {
        if (level < int(chem_warps[wid]->reacs.size()))
        {
          Reaction *reac = chem_warps[wid]->reacs[level];
          if (chem->forward_unimportant.find(reac) !=
              chem->forward_unimportant.end())
          {
            ost << "if (wid == " << wid << ")\n";
            if (!spill_reac_rates)
              ost << "  rr_f_" << level << " = 0.0;\n";
            else
            {
              int offset = level * 2 * chem_warps.size();
              ost << "  rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = 0.0;\n";
            }
          }
          if (chem->backward_unimportant.find(reac) !=
              chem->backward_unimportant.end())
          {
            ost << "if (wid == " << wid << ")\n";
            if (!spill_reac_rates)
              ost << " rr_r_" << level << " = 0.0;\n";
            else
            {
              int offset = level * 2 * chem_warps.size() + chem_warps.size();
              ost << "  rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = 0.0;\n";
            }
          }
        }
      }
    }
  }
  // For when this code is called by the small-warp code generation routine
  else if (warp_size < 32)
  {
    for (int wid = 0; wid < int(chem_warps.size()); wid+=(32/warp_size))
    {
      if (wid == 0)
        ost << "if (wid < " << (32/warp_size) << ")\n";
      else
        ost << "else if (wid < " << (wid+(32/warp_size)) << ")\n";
      PairDelim warp_pair(ost);
      ost << INT << " index;\n";
      std::vector<std::vector<int> > forward_scaling_indexes(32/warp_size);
      std::vector<std::vector<int> > reverse_scaling_indexes(32/warp_size);
      for (int w = 0; w < (32/warp_size); w++)
      {
        if (level < int(chem_warps[wid+w]->reacs.size()))
        {
          Reaction *reac = chem_warps[wid+w]->reacs[level];
          if ((chem == NULL) || (chem->forward_unimportant.find(reac) == chem->forward_unimportant.end()))
          {
            for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
                  it != reac->forward.end(); it++)
            {
              if (it->first->qss_species)
                continue;
              int index = find_ordered_index(it->first);
              assert(index != -1);
              assert(it->second > 0);
              for (int idx = 0; idx < it->second; idx++)
                forward_scaling_indexes[w].push_back(index);
            }
            if ((reac->thb != NULL) && !reac->pressure_dep)
            {
              int index = thb_offset + reac->thb->idx;
              assert(index != -1);
              forward_scaling_indexes[w].push_back(index);
            }
          }
          if ((chem != NULL) || (chem->backward_unimportant.find(reac) == chem->backward_unimportant.end()))
          {
            if (reac->reversible)
            {
              for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                    it != reac->backward.end(); it++)
              {
                if (it->first->qss_species)
                  continue;
                int index = find_ordered_index(it->first);
                assert(index != -1);
                assert(it->second > 0);
                for (int idx = 0; idx < it->second; idx++)
                  reverse_scaling_indexes[w].push_back(index);
              }
              if ((reac->thb != NULL) && !reac->pressure_dep)
              {
                int index = thb_offset + reac->thb->idx;
                assert(index != -1);
                reverse_scaling_indexes[w].push_back(index);
              }
            }
          }
        }
      }
      // Now find the maximum number of scaling values and pad the rest
      int max_forward = -1;
      int max_reverse = -1;
      for (int w = 0; w < (32/warp_size); w++)
      {
        if (int(forward_scaling_indexes[w].size()) > max_forward)
          max_forward = forward_scaling_indexes[w].size();
        if (int(reverse_scaling_indexes[w].size()) > max_reverse)
          max_reverse = reverse_scaling_indexes[w].size();
      }
      assert(max_forward >= 0);
      assert(max_reverse >= 0);
      bool forward_first_iter = true;
      if (max_forward > 0)
      {
        for (int w = 0; w < (32/warp_size); w++)
        {
          while (int(forward_scaling_indexes[w].size()) < max_forward)
            forward_scaling_indexes[w].push_back(shared_unity);
        }
        for (int idx = 0; idx < max_forward; idx++)
        {
          if (spill_reac_rates && forward_first_iter)
          {
            int offset = level * 2 * chem_warps.size();
            ost << REAL << " rr_f_" << level << " = rr_g[smid*"
                << sm_offset << "+" << offset << "+wid][tid];\n";
            forward_first_iter = false;
          }
          int global_idx = chem_warps[wid]->add_index(forward_scaling_indexes[0][idx],0/*version*/);
          for (int w = 1; w < (32/warp_size); w++)
            assert(global_idx == chem_warps[wid+w]->add_index(forward_scaling_indexes[w][idx],0/*version*/));
          int index_offset = global_idx/warp_size;
          int index_thread = global_idx%warp_size;
          ost << "index = __shfl(index_constants[" << index_offset << "],"
              << index_thread << "," << warp_size << ");\n";
          ost << "rr_f_" << level << " *= scratch[index][tid];\n";
        }
      }
      if (!forward_first_iter)
      {
        int offset = level * 2 * chem_warps.size();
        ost << "rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = "
            << "rr_f_" << level << ";\n";
      }
      bool reverse_first_iter = true;
      if (max_reverse > 0)
      {
        for (int w = 0; w < (32/warp_size); w++)
        {
          while (int(reverse_scaling_indexes[w].size()) < max_reverse)
            reverse_scaling_indexes[w].push_back(shared_unity);
        }
        for (int idx = 0; idx < max_reverse; idx++)
        {
          if (spill_reac_rates && reverse_first_iter)
          {
            int offset = level * 2 * chem_warps.size() + chem_warps.size();
            ost << REAL << " rr_r_" << level << " = rr_g[smid*"
                << sm_offset << "+" << offset << "+wid][tid];\n";
            reverse_first_iter = false;
          }
          int global_idx = chem_warps[wid]->add_index(reverse_scaling_indexes[0][idx],0/*version*/);
          for (int w = 1; w < (32/warp_size); w++)
            assert(global_idx == chem_warps[wid+w]->add_index(reverse_scaling_indexes[w][idx],0/*version*/));
          int index_offset = global_idx/warp_size;
          int index_thread = global_idx%warp_size;
          ost << "index = __shfl(index_constants[" << index_offset << "],"
              << index_thread << "," << warp_size << ");\n";
          ost << "rr_r_" << level << " *= scratch[index][tid];\n";
        }
      } 
      if (!reverse_first_iter)
      {
        int offset = level * 2 * chem_warps.size() + chem_warps.size();
        ost << "rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = "
            << "rr_r_" << level << ";\n";
      }
    }
    // Handle any unimportant reaction rates
    if (chem != NULL)
    {
      for (unsigned wid = 0; wid < chem_warps.size(); wid++)
      {
        if (level < int(chem_warps[wid]->reacs.size()))
        {
          Reaction *reac = chem_warps[wid]->reacs[level];
          if (chem->forward_unimportant.find(reac) !=
              chem->forward_unimportant.end())
          {
            ost << "if (wid == " << wid << ")\n";
            if (!spill_reac_rates)
              ost << "  rr_f_" << level << " = 0.0;\n";
            else
            {
              int offset = level * 2 * chem_warps.size();
              ost << "  rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = 0.0;\n";
            }
          }
          if (chem->backward_unimportant.find(reac) !=
              chem->backward_unimportant.end())
          {
            ost << "if (wid == " << wid << ")\n";
            if (!spill_reac_rates)
              ost << " rr_r_" << level << " = 0.0;\n";
            else
            {
              int offset = level * 2 * chem_warps.size() + chem_warps.size();
              ost << "  rr_g[smid*" << sm_offset << "+" << offset << "+wid][tid] = 0.0;\n";
            }
          }
        }
      }
    }
  }
  // Finally if we are spilling reaction rates then write out the result
  if (spill_reac_rates) {
    size_t level_space = 2 * chem_warps.size();
    size_t level_offset = level * level_space;
    ost << "rr_g[smid*" << sm_offset << "+" << level_offset  
        << "+wid][tid] = rr_f_" << level << ";\n";
    ost << "rr_g[smid*" << sm_offset << "+" << (level_offset+warp_size)  
        << "+wid][tid] = rr_r_" << level << ";\n";
  }
}

void TranslationUnit::emit_multi_qssa_scaling(CodeOutStream &ost, int qssa_levels)
{
  assert(chem != NULL);
  ost << "/* QSSA scaling */\n";
  // first wait for the signal from the QSSA warps that the values are ready
  //emit_barrier(ost, 0, (reac_warps+qss_warps), true/*blocking*/);
  emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  if (use_indexing)
  {
    assert(false);
#if 0
    for (int level = 0; level < qssa_levels; level++)
    {
      // compute the index that each warp needs for its forward and
      // backward scaling value
      int forward_meta = -1;
      int reverse_meta = -1;
      for (unsigned wid = 0; wid < chem_warps.size(); wid++)
      {
        if (level < int(chem_warps[wid]->reacs.size()))
        {
          Reaction *reac = chem_warps[wid]->reacs[level];
          int qss_idx = chem->find_qss_index(reac, true/*forward*/);
          int global_idx;
          if (qss_idx != -1)
            global_idx = chem_warps[wid]->add_index(qss_idx,0/*version*/);
          else
            global_idx = chem_warps[wid]->add_index(reac_unity,0/*version*/);
          if (forward_meta == -1)
            forward_meta = global_idx;
          else
            assert(forward_meta == global_idx);
          // Now do the reverse rate
          qss_idx = chem->find_qss_index(reac, false/*forward*/);
          if (qss_idx != -1)
            global_idx = chem_warps[wid]->add_index(qss_idx,0/*version*/);
          else
            global_idx = chem_warps[wid]->add_index(reac_unity,0/*version*/);
          if (reverse_meta == -1)
            reverse_meta = global_idx;
          else
            assert(reverse_meta == global_idx);
        }
        else
        {
          int forward_loc = chem_warps[wid]->add_index(reac_unity,0/*version*/);
          if (forward_meta == -1)
            forward_meta = forward_loc;
          else
            assert(forward_meta == forward_loc);
          int reverse_loc = chem_warps[wid]->add_index(reac_unity,0/*version*/);
          if (reverse_meta == -1)
            reverse_meta = reverse_loc;
          else
            assert(reverse_meta == reverse_loc);
        }
      }
      // Now we can emit the code to actually do the scaling
      int index_offset = forward_meta/points_per_cta;
      int index_thread = forward_meta%points_per_cta;
      PairDelim scale_pair(ost);
      if (k20)
        ost << INT << " index = __shfl(index_constants[" << index_offset << "]," 
            << index_thread << "," << points_per_cta << ");\n";
      else
      {
        // Rely on warp-synchronous programming
        ost << "if (tid == " << index_thread << ")\n";
        ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
        ost << INT << " index = int_mirror[wid];\n";
      }
      // Now we can do the scaling
      ost << "rr_f_" << level << " *= rrs[index][tid];\n";
      // Do the reverse case
      index_offset = reverse_meta/points_per_cta;
      index_thread = reverse_meta%points_per_cta;
      if (k20)
        ost << "index = __shfl(index_constants[" << index_offset << "]," 
            << index_thread << "," << points_per_cta << ");\n";
      else
      {
        // Rely on warp-synchronous programming
        ost << "if (tid == " << index_thread << ")\n";
        ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
        ost << "index = int_mirror[wid];\n";
      }
      ost << "rr_r_" << level << " *= rrs[index][tid];\n";
    }
#endif
  }
  else
  {
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      if (wid == 0)
        ost << "if (wid == " << wid << ")\n";
      else
        ost << "else if (wid == " << wid << ")\n";
      PairDelim warp_pair(ost);
      std::set<int> loaded_qss;
      for (int level = 0; level < qssa_levels; level++)
      {
        if (level < int(chem_warps[wid]->reacs.size()))
        {
          Reaction *reac = chem_warps[wid]->reacs[level];
          int qss_idx = chem->find_qss_index(reac, true/*forward*/);
          if (qss_idx != -1)
          {
            if (loaded_qss.find(qss_idx) == loaded_qss.end())
            {
              ost << REAL << " xq_" << qss_idx << " = scratch[" << (reac_offset+qss_idx) << "][tid];\n";
              loaded_qss.insert(qss_idx);
            }
            if (!spill_reac_rates)
              ost << "rr_f_" << level << " *= xq_" << qss_idx << ";\n";
            else
            {
              int loc = global_analyzer->find_forward_location(reac, 0/*pass*/);
              assert(loc != -1);
              ost << "rr_g[smid*" << sm_offset << "+" << loc << "][tid] *= xq_" << qss_idx << ";\n";
            }
          }
          qss_idx = chem->find_qss_index(reac, false/*forward*/);
          if (qss_idx != -1)
          {
            if (loaded_qss.find(qss_idx) == loaded_qss.end())
            {
              ost << REAL << " xq_" << qss_idx << " = scratch[" << (reac_offset+qss_idx) << "][tid];\n";
              loaded_qss.insert(qss_idx);
            }
            if (!spill_reac_rates)
              ost << "rr_r_" << level << " *= xq_" << qss_idx << ";\n";
            else
            {
              int loc = global_analyzer->find_reverse_location(reac, 0/*pass*/);
              assert(loc != -1);
              ost << "rr_g[smid*" << sm_offset << "+" << loc << "][tid] *= xq_" << qss_idx << ";\n";
            }
          }
        }
      }
    }
  }
  // signal the QSSA warps that we are done consuming
  //emit_barrier(ost, 1, (reac_warps+qss_warps), false/*blocking*/);
}

void TranslationUnit::emit_multi_init_stif_code(CodeOutStream &ost)
{
  assert(chem != NULL);
  ost << "/* Stiffness */\n";
  const unsigned num_stif = chem->stif_operations.size();
  const unsigned num_specs = ((num_stif+reac_warps-1)/reac_warps);
  //const unsigned uniform_specs = ((num_stif%reac_warps) == 0);
  std::vector<Stif*> stif_order;
  {
    std::set<unsigned> used;
    std::vector<Stif*> batch;
    bool even = false;
    while (used.size() < chem->stif_operations.size())
    {
      while ((int(batch.size()) < reac_warps) && 
             (used.size() < chem->stif_operations.size()))
      {
        int max_contributions = -1;
        int max_index = -1;
        for (unsigned idx = 0; idx < chem->stif_operations.size(); idx++)
        {
          if (used.find(idx) != used.end())
            continue;
          Stif *op = chem->stif_operations[idx];
          int contributions = op->forward_d.size() + op->backward_d.size() +
                              op->forward_c.size() + op->backward_c.size();
          if (contributions > max_contributions)
          {
            max_contributions = contributions;
            max_index = idx;
          }
        }
        assert(max_index != -1);
        batch.push_back(chem->stif_operations[max_index]);
        used.insert(max_index);
      }
      // We've got a batch, so add it to the order
      if (even)
      {
        for (std::vector<Stif*>::const_iterator it = batch.begin();
              it != batch.end(); it++)
        {
          stif_order.push_back(*it);
        }
        even = false;
      }
      else
      {
        for (std::vector<Stif*>::const_reverse_iterator it = batch.rbegin();
              it != batch.rend(); it++)
        {
          stif_order.push_back(*it);
        }
        even = true;
      }
      batch.clear();
    }
  }
  // Round robin the stiffness species onto the full warps
  // so that groups of sub-warps are sharing the same stif species
  int warp_index = 0;
  for (std::vector<Stif*>::const_iterator it = stif_order.begin();
        it != stif_order.end(); it++)
  {
    for (int wid = 0; wid < (32/warp_size); wid++)
      chem_warps[warp_index+wid]->add_stif(*it);
    warp_index += (32/warp_size);
    assert(warp_index <= int(chem_warps.size()));
    if (warp_index == int(chem_warps.size()))
      warp_index = 0;
  }
  // Emit the loads and for diffusion and mole fractions
  for (unsigned idx = 0; idx < num_specs; idx++)
  {
    int meta_index = -1;
    // first we need to get the index values
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      if (idx < chem_warps[wid]->stifs.size())
      {
        Stif *stif = chem_warps[wid]->stifs[idx]; 
        int index = find_ordered_index(stif->species);
        assert(index != -1);
        int global_idx = chem_warps[wid]->add_index(index, 0);
        if (meta_index == -1)
          meta_index = global_idx;
        else
          assert(meta_index == global_idx);
      }
      else
      {
        int global_idx = chem_warps[wid]->add_index(0/*spec_unity*/,0);
        if (meta_index == -1)
          meta_index = global_idx;
        else
          assert(meta_index == global_idx);
      }
    }
    // Get the index
    stif_meta.push_back(meta_index);
    int index_offset = meta_index/points_per_cta;
    int index_thread = meta_index%points_per_cta;
    char diff_name[64];
    sprintf(diff_name,"stif_diffusion_%d", idx);
    ost << REAL << " " << diff_name << ";\n";
    ost << REAL << " stif_mole_frac_" << idx << ";\n";
    PairDelim spec_pair(ost);
    if (k20)
      ost << INT << " index = __shfl(index_constants[" << index_offset << "]," 
          << index_thread << "," << points_per_cta << ");\n";
    else
    {
      // Rely on warp-synchronous programming
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
      ost << INT << " index = int_mirror[wid];\n";
    }
    
    emit_cuda_load(ost,diff_name,DIFFUSION_ARRAY,"index*spec_stride",k20);
    ost << "stif_mole_frac_" << idx << " = scratch[index][tid];\n";
  }

  // Emit declarations for cdot and ddot
  for (unsigned idx = 0; idx < num_specs; idx++)
  {
    ost << REAL << " ddot_" << idx << " = 0.0;\n";
    ost << REAL << " cdot_" << idx << " = 0.0;\n";
  }
}

void TranslationUnit::emit_multi_stif_pass(CodeOutStream &ost, int pass, int start_level, int stop_level)
{
  // Emit a barrier to wait before writing into shared
  if (!spill_reac_rates)
  {
    emit_barrier(ost, 0, reac_warps, true/*blocking*/);
    for (int level = start_level; level < stop_level; level++)
    {
      ost << "scratch[wid+" << (chem_warps.size()*(2*(level-start_level))) << "][tid] = rr_f_" << level << ";\n";
      ost << "scratch[wid+" << (chem_warps.size()*(2*(level-start_level)+1)) << "][tid] = rr_r_" << level << ";\n";   
    }
  }
  // Emit a barrier before reading
  emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  if (use_indexing)
  {
    assert(false);
#if 0
    const unsigned num_stif = chem->stif_operations.size();
    const unsigned num_specs = ((num_stif+reac_warps-1)/reac_warps);
    // For each species, first figure out how many locations are needed by each of
    // the different warps, then compute the assignment
    for (unsigned spec_idx = 0; spec_idx < num_specs; spec_idx++)
    {
      std::vector<std::vector<std::vector<int> > > ddot_assignments(reac_warps);
      std::vector<std::vector<std::vector<int> > > cdot_assignments(reac_warps);
      // Keep track of the maximum number of locations needed
      int maxd = -1;
      int maxc = -1;
      for (int wid = 0; wid < reac_warps; wid++)
      {
        std::vector<int> ddot_locations;
        std::vector<int> cdot_locations;
        if (spec_idx < chem_warps[wid*(32/warp_size)]->stifs.size())
        {
          Stif *stif = chem_warps[wid*(32/warp_size)]->stifs[spec_idx];
          for (std::map<Reaction*,int>::const_iterator it = stif->forward_d.begin();
                it != stif->forward_d.end(); it++)
          {
            assert(it->second > 0);
            int loc = multi_analyzer->find_forward_location(it->first, pass);
            if (loc == -1)
              continue;
            for (int idx = 0; idx < it->second; idx++)
              ddot_locations.push_back(loc);
          }
          for (std::map<Reaction*,int>::const_iterator it = stif->backward_d.begin();
                it != stif->backward_d.end(); it++)
          {
            assert(it->second > 0);
            int loc = multi_analyzer->find_reverse_location(it->first, pass);
            if (loc == -1)
              continue;
            for (int idx = 0; idx < it->second; idx++)
              ddot_locations.push_back(loc);
          }
          for (std::map<Reaction*,int>::const_iterator it = stif->forward_c.begin();
                it != stif->forward_c.end(); it++)
          {
            assert(it->second > 0);
            int loc = multi_analyzer->find_forward_location(it->first, pass);
            if (loc == -1)
              continue;
            for (int idx = 0; idx < it->second; idx++)
              cdot_locations.push_back(loc);
          }
          for (std::map<Reaction*,int>::const_iterator it = stif->backward_c.begin();
                it != stif->backward_c.end(); it++)
          {
            assert(it->second > 0);
            int loc = multi_analyzer->find_reverse_location(it->first, pass);
            if (loc == -1)
              continue;
            for (int idx = 0; idx < it->second; idx++)
              cdot_locations.push_back(loc);
          }
        }
        multi_analyzer->perform_assignment(pass, ddot_locations, ddot_assignments[wid]);
        multi_analyzer->perform_assignment(pass, cdot_locations, cdot_assignments[wid]);
        assert(int(ddot_assignments[wid].size()) == (32/warp_size));
        assert(int(cdot_assignments[wid].size()) == (32/warp_size));
        for (int idx = 0; idx < (32/warp_size); idx++)
        {
          if (maxd < int(ddot_assignments[wid][idx].size()))
            maxd = ddot_assignments[wid][idx].size();
          if (maxc < int(cdot_assignments[wid][idx].size()))
            maxc = cdot_assignments[wid][idx].size();
        }
      }
      // Now we have all the locations for all the warps, so do the assignments
      // ddot first
      for (int step = 0; step < maxd; step++)
      {
        int meta_index = -1;
        unsigned valid_mask = 0;
        bool needs_mask = false;
        // for each warp, do the assignment and then save the indexes
        for (int wid = 0; wid < reac_warps; wid++)
        {
          bool local_mask = true;
          assert(int(ddot_assignments[wid].size()) == (32/warp_size));
          for (unsigned idx = 0; idx < ddot_assignments[wid].size(); idx++)
          {
            // Check to see if we have an index
            int global_idx;
            int local_wid = wid*(32/warp_size)+idx;
            if (step < int(ddot_assignments[wid][idx].size()))
            {
              int index = ddot_assignments[wid][idx][step];
              Warp *w = chem_warps[local_wid];
              global_idx = w->add_index(index, 0/*version*/);
              // As long as we have something to do then
              // we don't need to be masked off
              local_mask = false;
            }
            else
            {
              // We need the identity index 
              global_idx = chem_warps[local_wid]->add_index(reac_zero, 0/*version*/);
            }
            // Check to make sure all the warps on the same page
            if (meta_index == -1)
              meta_index = global_idx;
            else
              assert(meta_index == global_idx);
          }
          if (local_mask)
            needs_mask = true;
          else
            valid_mask += (1 << wid);
        }
        if (needs_mask)
        {
          char mask_string[64];
          sprintf(mask_string,"0x%x",valid_mask);
          ost << "if ((1 << (threadIdx.x >> 5)) & " << mask_string << ")\n";
        }
        PairDelim reduc_pair(ost);
        int index_offset = meta_index/points_per_cta;
        int index_thread = meta_index%points_per_cta;
        if (k20)
          ost << INT << " index = __shfl(index_constants[" << index_offset << "]," 
              << index_thread << "," << points_per_cta << ");\n";
        else
        {
          // Rely on warp-synchronous programming
          ost << "if (tid == " << index_thread << ")\n";
          ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
          ost << INT << " index = int_mirror[wid];\n";
        }
        ost << "ddot_" << spec_idx << " += rrs[index][tid];\n";
      }
      // Now for cdot
      for (int step = 0; step < maxc; step++)
      {
        int meta_index = -1;
        unsigned valid_mask = 0;
        bool needs_mask = false;
        // for each warp, do the assignment and then save the indexes
        for (int wid = 0; wid < reac_warps; wid++)
        {
          bool local_mask = true;
          assert(int(cdot_assignments[wid].size()) == (32/warp_size));
          for (unsigned idx = 0; idx < cdot_assignments[wid].size(); idx++)
          {
            // Check to see if we have an index
            int global_idx;
            int local_wid = wid*(32/warp_size)+idx;
            if (step < int(cdot_assignments[wid][idx].size()))
            {
              int index = cdot_assignments[wid][idx][step];
              Warp *w = chem_warps[local_wid];
              global_idx = w->add_index(index, 0/*version*/);
              // As long as we have something to do then
              // we don't need to be masked off
              local_mask = false;
            }
            else
            {
              // We need the identity index 
              global_idx = chem_warps[local_wid]->add_index(reac_zero, 0/*version*/);
            }
            // Check to make sure all the warps on the same page
            if (meta_index == -1)
              meta_index = global_idx;
            else
              assert(meta_index == global_idx);
          }
          if (local_mask)
            needs_mask = true;
          else
            valid_mask += (1 << wid);
        }
        if (needs_mask)
        {
          char mask_string[64];
          sprintf(mask_string,"0x%x",valid_mask);
          ost << "if ((1 << (threadIdx.x >> 5)) & " << mask_string << ")\n";
        }
        PairDelim reduc_pair(ost);
        int index_offset = meta_index/points_per_cta;
        int index_thread = meta_index%points_per_cta;
        if (k20)
          ost << INT << " index = __shfl(index_constants[" << index_offset << "]," 
              << index_thread << "," << points_per_cta << ");\n";
        else
        {
          // Rely on warp-synchronous programming
          ost << "if (tid == " << index_thread << ")\n";
          ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
          ost << INT << " index = int_mirror[wid];\n";
        }
        ost << "cdot_" << spec_idx << " += rrs[index][tid];\n";
      }
    }
#endif
  }
  else // No indexing
  {
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      if (wid == 0)
        ost << "if (wid == " << wid << ")\n";
      else
        ost << "else if (wid == " << wid << ")\n";
      PairDelim warp_pair(ost);
      std::set<Reaction*> loaded_forward;
      std::set<Reaction*> loaded_reverse;
      for (unsigned spec_idx = 0; spec_idx < chem_warps[wid]->stifs.size(); spec_idx++)
      {
        Stif *stif = chem_warps[wid]->stifs[spec_idx];
        for (std::map<Reaction*,int>::const_iterator it = stif->forward_d.begin();
              it != stif->forward_d.end(); it++)
        {
          assert(it->second > 0);
          if (loaded_forward.find(it->first) == loaded_forward.end())
          {
            int loc = !spill_reac_rates ? 
              stif_analyzer->find_forward_location(it->first, pass) :
              global_analyzer->find_forward_location(it->first, pass);
            if (loc == -1)
              continue;
            if (!spill_reac_rates)
              ost << REAL << " forward_" << it->first->idx << " = scratch[" << loc << "][tid];\n";
            else
              ost << REAL << " forward_" << it->first->idx << " = rr_g[smid*"
                  << sm_offset << "+" << loc << "][tid];\n";
            loaded_forward.insert(it->first);
          }
          if (it->second == 1)
            ost << "ddot_" << spec_idx << " += forward_" << it->first->idx << ";\n";
          else
            ost << "ddot_" << spec_idx << " = __fma_rn(" << double(it->second) << ",forward_" 
                << it->first->idx << ",ddot_" << spec_idx << ");\n";
        }
        for (std::map<Reaction*,int>::const_iterator it = stif->backward_d.begin();
              it != stif->backward_d.end(); it++)
        {
          assert(it->second > 0);
          if (loaded_reverse.find(it->first) == loaded_reverse.end())
          {
            int loc = !spill_reac_rates ? 
              stif_analyzer->find_reverse_location(it->first, pass) :
              global_analyzer->find_reverse_location(it->first, pass);
            if (loc == -1)
              continue;
            if (!spill_reac_rates)
              ost << REAL << " backward_" << it->first->idx << " = scratch[" << loc << "][tid];\n";
            else
              ost << REAL << " backward_" << it->first->idx << " = rr_g[smid*"
                  << sm_offset << "+" << loc << "][tid];\n";
            loaded_reverse.insert(it->first);
          }
          if (it->second == 1)
            ost << "ddot_" << spec_idx << " += backward_" << it->first->idx << ";\n";
          else
            ost << "ddot_" << spec_idx << " = __fma_rn(" << double(it->second) << ",backward_" 
                << it->first->idx << ",ddot_" << spec_idx << ");\n";
        }
        for (std::map<Reaction*,int>::const_iterator it = stif->forward_c.begin();
              it != stif->forward_c.end(); it++)
        {
          assert(it->second > 0);
          if (loaded_forward.find(it->first) == loaded_forward.end())
          {
            int loc = !spill_reac_rates ? 
              stif_analyzer->find_forward_location(it->first, pass) :
              global_analyzer->find_forward_location(it->first, pass);
            if (loc == -1)
              continue;
            if (!spill_reac_rates)
              ost << REAL << " forward_" << it->first->idx << " = scratch[" << loc << "][tid];\n";
            else
              ost << REAL << " forward_" << it->first->idx << " = rr_g[smid*"
                  << sm_offset << "+" << loc << "][tid];\n";
            loaded_forward.insert(it->first);
          }
          if (it->second == 1)
            ost << "cdot_" << spec_idx << " += forward_" << it->first->idx << ";\n";
          else
            ost << "cdot_" << spec_idx << " = __fma_rn(" << double(it->second) << ",forward_" 
                << it->first->idx << ",cdot_" << spec_idx << ");\n";
        }
        for (std::map<Reaction*,int>::const_iterator it = stif->backward_c.begin();
              it != stif->backward_c.end(); it++)
        {
          assert(it->second > 0);
          if (loaded_reverse.find(it->first) == loaded_reverse.end())
          {
            int loc = !spill_reac_rates ? 
              stif_analyzer->find_reverse_location(it->first, pass) :
              global_analyzer->find_reverse_location(it->first, pass);
            if (loc == -1)
              continue;
            if (!spill_reac_rates)
              ost << REAL << " backward_" << it->first->idx << " = scratch[" << loc << "][tid];\n";
            else
              ost << REAL << " backward_" << it->first->idx << " = rr_g[smid*"
                  << sm_offset << "+" << loc << "][tid];\n";
            loaded_reverse.insert(it->first);
          }
          if (it->second == 1)
            ost << "cdot_" << spec_idx << " += backward_" << it->first->idx << ";\n";
          else
            ost << "cdot_" << spec_idx << " = __fma_rn(" << double(it->second) << ",backward_" 
                << it->first->idx << ",cdot_" << spec_idx << ");\n";
        }
      }
    }
  }
}

void TranslationUnit::emit_multi_stif_scaling(CodeOutStream &ost)
{
  // Need a barrier here before writing
  if (!spill_reac_rates)
    emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  const unsigned num_stif = chem->stif_operations.size();
  const unsigned num_specs = ((num_stif+reac_warps-1)/reac_warps);
  const bool uniform_specs = ((num_stif%reac_warps) == 0);
  for (unsigned spec_idx = 0; spec_idx < num_specs; spec_idx++)
  {
    PairDelim spec_pair(ost);
    // First reduce the cdot and ddot value across the sub-warps in a warp
    if (k20)
    {
      if (points_per_cta < 32)
        ost << INT << " hi_part,lo_part;\n";
      for (int i = points_per_cta; i < 32; i*=2)
      {
        ost << "hi_part = __shfl_xor(__double2hiint(ddot_" << spec_idx << ")," << i << ",32);\n"; 
        ost << "lo_part = __shfl_xor(__double2loint(ddot_" << spec_idx << ")," << i << ",32);\n";
        ost << "ddot_" << spec_idx << " += __hiloint2double(hi_part,lo_part);\n";
      }
      for (int i = points_per_cta; i < 32; i*=2)
      {
        ost << "hi_part = __shfl_xor(__double2hiint(cdot_" << spec_idx << ")," << i << ",32);\n"; 
        ost << "lo_part = __shfl_xor(__double2loint(cdot_" << spec_idx << ")," << i << ",32);\n";
        ost << "cdot_" << spec_idx << " += __hiloint2double(hi_part,lo_part);\n";
      }
    }
    else if (points_per_cta < 32)
    {
      char mask[64];
      sprintf(mask,"0x%x",(32/points_per_cta)-1);
      // Here we use warp-synchronous reductions linearly
      for (int idx = 1; idx < (32/points_per_cta); idx++)
      {
        ost << "if ((wid & " << mask << ") == " << idx << ")\n";
        ost << "  reduc_mirror[0][tid] = ddot_" << spec_idx << ";\n";
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  ddot_" << spec_idx << " += reduc_mirror[0][tid];\n";
      }
      // Then scatter back out so everyone has the same value
      ost << "if ((wid & " << mask << ") == 0)\n";
      ost << "  reduc_mirror[0][tid] = ddot_" << spec_idx << ";\n";
      ost << "ddot_" << spec_idx << " = reduc_mirror[0][tid];\n";
      for (int idx = 1; idx < (32/points_per_cta); idx++)
      {
        ost << "if ((wid & " << mask << ") == " << idx << ")\n";
        ost << "  reduc_mirror[0][tid] = cdot_" << spec_idx << ";\n";
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  cdot_" << spec_idx << " += reduc_mirror[0][tid];\n";
      }
      // Then scatter back out so everyone has the same value
      ost << "if ((wid & " << mask << ") == 0)\n";
      ost << "  reduc_mirror[0][tid] = cdot_" << spec_idx << ";\n";
      ost << "cdot_" << spec_idx << " = reduc_mirror[0][tid];\n";
    }
    // Now we can actually compute our scaling value
    ost << REAL << " scale = 1.0;\n";
    ost << "const bool need_scale = ((ddot_" << spec_idx << " * dt * " 
        << (1.0/REACTION_RATE_REF) << ") > stif_mole_frac_" << spec_idx << ");\n";
    ost << "if (__any(need_scale))\n";
    {
      PairDelim if_pair(ost);
      ost << REAL << " recip_ddot = 1.0/ddot_" << spec_idx << ";\n";
      assert(spec_idx < stif_meta.size());
      int index_offset = stif_meta[spec_idx]/points_per_cta;
      int index_thread = stif_meta[spec_idx]%points_per_cta;
      if (k20)
        ost << INT << " index = __shfl(index_constants[" << index_offset << "]," 
            << index_thread << "," << points_per_cta << ");\n";
      else
      {
        // Rely on warp-synchronous programming
        ost << "if (tid == " << index_thread << ")\n";
        ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
        ost << INT << " index = int_mirror[wid];\n";
      }
      ost << REAL << " recip_mass = recip_molecular_masses[index];\n";
      ost << REAL << " part_sum = cdot_" << spec_idx << " + stif_diffusion_" << spec_idx << " * recip_mass;\n"; 
      ost << REAL << " c0 = stif_mole_frac_" << spec_idx << " * part_sum * recip_ddot;\n";
      ost << "c0 = (part_sum + "
          << REACTION_RATE_REF << " * (stif_mole_frac_"
          << spec_idx << " - c0) * recip_dt) * recip_ddot;\n";
      // Check for dumb zero condition
      //ost << "if (stif_mole_frac_" << spec_idx << " > 0.0)\n";
      ost << "if (need_scale)\n";
      //ost << "if ((ddot * dt) > local_mole_frac)\n";
      ost << "  scale = c0;\n";
    }
    // Now write out our value into the buffer which we know is safe since
    // everyone has by this point read their mole frac values from the buffer
    if (!uniform_specs && (spec_idx == (num_specs-1)))
      ost << "if (((threadIdx.x >> 5)+" << (spec_idx*reac_warps) << ") < " << chem->stif_operations.size() << ")\n";
    PairDelim write_pair(ost);
    if (warp_size < 32)
    {
      char mask[64];
      sprintf(mask,"0x%x",(32/points_per_cta)-1);
      ost << "if ((wid & " << mask << ") == 0)\n";
    }
    ost << "scratch[(threadIdx.x >> 5)+" << (spec_idx*reac_warps) << "][tid] = scale;\n";
  }
  // Now we barrier to make sure everyone has written their values in
  emit_barrier(ost, 0, reac_warps, true/*wait*/);
  if (use_indexing)
  {
    assert(false);
#if 0
    // For every forward and backward reaction rate we now need to check to see if we need to
    // scale it by one of the stiffness correction values
    for (unsigned reac_idx = 0; reac_idx < chem_warps[0]->reacs.size(); reac_idx++)
    {
      std::vector<std::vector<int> > forward_locations(chem_warps.size());
      std::vector<std::vector<int> > reverse_locations(chem_warps.size());
      int max_forward = 0;
      int max_reverse = 0;
      for (unsigned wid = 0; wid < chem_warps.size(); wid++)
      {
        if (reac_idx < chem_warps[wid]->reacs.size())
        {
          Reaction *reac = chem_warps[wid]->reacs[reac_idx];
          // Go through each of the stif operations and see if they update the reaction
          for (unsigned wid2 = 0; wid2 < chem_warps.size(); wid2+=(32/warp_size))
          {
            for (unsigned idx = 0; idx < chem_warps[wid2]->stifs.size(); idx++)
            {
              int index = (wid2/(32/warp_size))+idx*(32/warp_size);
              Stif *stif = chem_warps[wid2]->stifs[idx];
              if (stif->forward_d.find(reac) != stif->forward_d.end())
                forward_locations[wid].push_back(index);
              if (stif->backward_d.find(reac) != stif->backward_d.end())
                reverse_locations[wid].push_back(index);
            }
          }
        }
        if (max_forward < int(forward_locations[wid].size()))
          max_forward = forward_locations[wid].size();
        if (max_reverse < int(reverse_locations[wid].size()))
          max_reverse = reverse_locations[wid].size();
      }
      // Set the index values for each of the forward and backward reactions
      std::vector<int> forward_meta(max_forward,-1);
      std::vector<int> reverse_meta(max_reverse,-1);
      for (unsigned wid = 0; wid < chem_warps.size(); wid++)
      {
        for (int idx = 0; idx < max_forward; idx++)
        {
          if (idx < int(forward_locations[wid].size()))
          {
            int global_idx = chem_warps[wid]->add_index(forward_locations[wid][idx],0);
            if (forward_meta[idx] == -1)
              forward_meta[idx] = global_idx;
            else
              assert(forward_meta[idx] == global_idx);
          }
          else
          {
            int global_idx = chem_warps[wid]->add_index(spec_unity,0);
            if (forward_meta[idx] == -1)
              forward_meta[idx] = global_idx;
            else
              assert(forward_meta[idx] == global_idx);
          }
        }
        // Now do backward
        for (int idx = 0; idx < max_reverse; idx++)
        {
          if (idx < int(reverse_locations[wid].size()))
          {
            int global_idx = chem_warps[wid]->add_index(reverse_locations[wid][idx],0);
            if (reverse_meta[idx] == -1)
              reverse_meta[idx] = global_idx;
            else
              assert(reverse_meta[idx] == global_idx);
          }
          else
          {
            int global_idx = chem_warps[wid]->add_index(spec_unity,0);
            if (reverse_meta[idx] == -1)
              reverse_meta[idx] = global_idx;
            else
              assert(reverse_meta[idx] == global_idx);
          }
        }
      }
      // Then emit the code to actually do the scaling
      for (int idx = 0; idx < max_forward; idx++)
      {
        int index_offset = forward_meta[idx]/points_per_cta;
        int index_thread = forward_meta[idx]%points_per_cta;
        PairDelim forward_pair(ost);
        if (k20)
          ost << INT << " index = __shfl(index_constants[" << index_offset << "]," 
              << index_thread << "," << points_per_cta << ");\n";
        else
        {
          // Rely on warp-synchronous programming
          ost << "if (tid == " << index_thread << ")\n";
          ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
          ost << INT << " index = int_mirror[wid];\n";
        }
        ost << "rr_f_" << reac_idx << " *= buffer[index][tid];\n"; 
      }
      for (int idx = 0; idx < max_reverse; idx++)
      {
        int index_offset = reverse_meta[idx]/points_per_cta;
        int index_thread = reverse_meta[idx]%points_per_cta;
        PairDelim reverse_pair(ost);
        if (k20)
          ost << INT << " index = __shfl(index_constants[" << index_offset << "]," 
              << index_thread << "," << points_per_cta << ");\n";
        else
        {
          // Rely on warp-synchronous programming
          ost << "if (tid == " << index_thread << ")\n";
          ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
          ost << INT << " index = int_mirror[wid];\n";
        }
        ost << "rr_r_" << reac_idx << " *= buffer[index][tid];\n";
      }
    }
#endif
  }
  else
  {
    // Not using indexing
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      if (wid == 0)
        ost << "if (wid == " << wid << ")\n";
      else
        ost << "else if (wid == " << wid << ")\n";
      PairDelim warp_pair(ost);
      std::set<Stif*> loaded_stifs;
      for (unsigned reac_idx = 0; reac_idx < chem_warps[wid]->reacs.size(); reac_idx++)
      {
        Reaction *reac = chem_warps[wid]->reacs[reac_idx];
        // Go through each of the stif operations and see if they update the reaction
        for (unsigned wid2 = 0; wid2 < chem_warps.size(); wid2+=(32/warp_size))
        {
          for (unsigned idx = 0; idx < chem_warps[wid2]->stifs.size(); idx++)
          {
            Stif *stif = chem_warps[wid2]->stifs[idx];
            if (stif->forward_d.find(reac) != stif->forward_d.end())
            {
              if (loaded_stifs.find(stif) == loaded_stifs.end())
              {
                int index = (wid2/(32/warp_size))+idx*(32/warp_size)*chem_warps.size();
                ost << REAL << " scale_" << stif->species->code_name << " = scratch[" << index << "][tid];\n";
                loaded_stifs.insert(stif);
              }
              if (!spill_reac_rates)
                ost << "rr_f_" << reac_idx << " *= scale_" << stif->species->code_name << ";\n";
              else
              {
                int loc = global_analyzer->find_forward_location(reac,0/*pass*/);
                assert(loc != -1);
                ost << "rr_g[smid*" << sm_offset << "+" << loc << "][tid] *= scale_"
                    << stif->species->code_name << ";\n";
              }
            }
            if (stif->backward_d.find(reac) != stif->backward_d.end())
            {
              if (loaded_stifs.find(stif) == loaded_stifs.end())
              {
                int index = (wid2/(32/warp_size))+idx*(32/warp_size)*chem_warps.size();
                ost << REAL << " scale_" << stif->species->code_name << " = scratch[" << index << "][tid];\n";
                loaded_stifs.insert(stif);
              }
              if (!spill_reac_rates)
                ost << "rr_r_" << reac_idx << " *= scale_" << stif->species->code_name << ";\n";
              else
              {
                int loc = global_analyzer->find_reverse_location(reac,0/*pass*/);
                assert(loc != -1);
                ost << "rr_g[smid*" << sm_offset << "+" << loc << "][tid] *= scale_"
                    << stif->species->code_name << ";\n";
              }
            }
          }
        }      
      }
    }
  }
}

void TranslationUnit::emit_multi_init_out_code(CodeOutStream &ost)
{
  ost << "/* Warp specialized output code */\n"; 
  // First sort the species based on the number of reactions they need
  // For the even groups of reac_warps, put them in order from 
  // largest to smallest, but for odd groups of reac warps, put them
  // in order from smallest to largest
  std::vector<Species*> output_order;
  {
    std::set<unsigned> used;
    std::vector<Species*> batch;
    bool even = false;
    while (used.size() < ordered_species.size())
    {
      while ((int(batch.size()) < reac_warps) && 
             (used.size() < ordered_species.size()))
      {
        int max_contributions = -1;
        int max_index = -1;
        for (unsigned idx = 0; idx < ordered_species.size(); idx++)
        {
          if (used.find(idx) != used.end())
            continue;
          int contributions = ordered_species[idx]->reaction_contributions.size();
          if (contributions > max_contributions)
          {
            max_contributions = contributions;
            max_index = idx;
          }
        }
        assert(max_index != -1);
        batch.push_back(ordered_species[max_index]);
        used.insert(max_index);
      }
      // We've got a batch, so add it to the order
      if (even)
      {
        for (std::vector<Species*>::const_iterator it = batch.begin();
              it != batch.end(); it++)
        {
          output_order.push_back(*it);
        }
        even = false;
      }
      else
      {
        for (std::vector<Species*>::const_reverse_iterator it = batch.rbegin();
              it != batch.rend(); it++)
        {
          output_order.push_back(*it);
        }
        even = true;
      }
      batch.clear();
    }
  }
  // Round robin the output species onto the full warps
  // so that groups of sub-warps are sharing the same stif species
  int warp_index = 0;
  for (std::vector<Species*>::const_iterator it = output_order.begin();
        it != output_order.end(); it++)
  {
    for (int wid = 0; wid < (32/warp_size); wid++)
      chem_warps[warp_index+wid]->add_output(*it);
    warp_index += (32/warp_size);
    assert(warp_index <= int(chem_warps.size()));
    if (warp_index == int(chem_warps.size()))
      warp_index = 0;
  }
  const unsigned num_specs = (ordered_species.size()+reac_warps-1)/reac_warps;
  for (unsigned idx = 0; idx < num_specs; idx++)
  {
    ost << REAL << " output_" << idx << " = 0.0;\n";
  }
}

void TranslationUnit::emit_multi_out_pass(CodeOutStream &ost, int pass, int start_level, int stop_level)
{
  // First we need a barrier before we can start writing
  emit_barrier(ost, 0, reac_warps, true/*wait*/);
  if (use_indexing)
  {
    for (int level = start_level; level < stop_level; level++)
    {
      ost << REAL << " ropl_" << level << " = rr_f_" << level << " - rr_r_" << level << ";\n";  
      ost << "rrs[wid+" << (chem_warps.size()*(2*(level-start_level))) << "][tid] = ropl_" << level << ";\n";
      ost << "rrs[wid+" << (chem_warps.size()*(2*(level-start_level)+1)) << "][tid] = -ropl_" << level << ";\n";   
    }
  }
  else
  {
    if (!spill_reac_rates) {
      for (int level = start_level; level < stop_level; level++)
      {
        PairDelim ropl_pair(ost);
        ost << REAL << " ropl_" << level << " = rr_f_" << level << " - rr_r_" << level << ";\n";  
        ost << "scratch[wid+" << (chem_warps.size()*(level-start_level)) << "][tid] = ropl_" << level << ";\n";
      }
    } else {
      for (int level = start_level; level < stop_level; level++)
        ost << "rr_g[smid*" << sm_offset << "+" << (level*2*chem_warps.size()) 
            << "+wid][tid] -= rr_g[smid*" << sm_offset << "+" 
            << (level*2*chem_warps.size()+chem_warps.size()) << "+wid][tid];\n";
    }
  }
  emit_barrier(ost, 0, reac_warps, true/*wait*/);
  if (use_indexing)
  {
    assert(false);
#if 0
    const unsigned num_specs = (ordered_species.size()+reac_warps-1)/reac_warps;
    // For each of the species, figure out which locations are needed
    for (unsigned spec_idx = 0; spec_idx < num_specs; spec_idx++)
    {
      std::vector<std::vector<std::vector<int> > > assignments(reac_warps);
      int max_vals = -1;
      for (int wid = 0; wid < reac_warps; wid++)
      {
        std::vector<int> locations;
        if (spec_idx < chem_warps[wid*(32/warp_size)]->specs.size())
        {
          Species *spec = chem_warps[wid*(32/warp_size)]->specs[spec_idx];
          for (std::map<Reaction*,int>::const_iterator it = spec->reaction_contributions.begin();
                it != spec->reaction_contributions.end(); it++)
          {
            assert(it->second != 0);
            if (it->second > 0)
            {
              int loc = multi_analyzer->find_forward_location(it->first, pass);
              if (loc == -1)
                continue;
              for (int idx = 0; idx < it->second; idx++)
              {
                locations.push_back(loc);
              }
            }
            else
            {
              int loc = multi_analyzer->find_reverse_location(it->first, pass);
              if (loc == -1)
                continue;
              for (int idx = 0; idx < (-it->second); idx++)
              {
                locations.push_back(loc);
              }
            }
          }
        }
        multi_analyzer->perform_assignment(pass, locations, assignments[wid]);
        assert(int(assignments[wid].size()) == (32/warp_size));
        for (int idx = 0; idx < (32/warp_size); idx++)
        {
          if (max_vals < int(assignments[wid][idx].size()))
            max_vals = assignments[wid][idx].size();
        }
      }
      // Now we have all the locations for all the warps, so assign the indexes
      for (int step = 0; step < max_vals; step++)
      {
        int meta_index = -1;
        unsigned valid_mask = 0;
        bool needs_mask = false;
        // for each warp, do the assignment and then save the indexes
        for (int wid = 0; wid < reac_warps; wid++)
        {
          bool local_mask = true;
          assert(int(assignments[wid].size()) == (32/warp_size));
          for (unsigned idx = 0; idx < assignments[wid].size(); idx++)
          {
            // Check to see if we have an index
            int global_idx;
            int local_wid = wid*(32/warp_size)+idx;
            if (step < int(assignments[wid][idx].size()))
            {
              int index = assignments[wid][idx][step];
              Warp *w = chem_warps[local_wid];
              global_idx = w->add_index(index, 0/*version*/);
              // As long as we have something to do then
              // we don't need to be masked off
              local_mask = false;
            }
            else
            {
              // We need the identity index 
              global_idx = chem_warps[local_wid]->add_index(reac_zero, 0/*version*/);
            }
            // Check to make sure all the warps on the same page
            if (meta_index == -1)
              meta_index = global_idx;
            else
              assert(meta_index == global_idx);
          }
          if (local_mask)
            needs_mask = true;
          else
            valid_mask += (1 << wid);
        }
        if (needs_mask)
        {
          char mask_string[64];
          sprintf(mask_string,"0x%x",valid_mask);
          ost << "if ((1 << (threadIdx.x >> 5)) & " << mask_string << ")\n";
        }
        PairDelim reduc_pair(ost);
        int index_offset = meta_index/points_per_cta;
        int index_thread = meta_index%points_per_cta;
        if (k20)
          ost << INT << " index = __shfl(index_constants[" << index_offset << "]," 
              << index_thread << "," << points_per_cta << ");\n";
        else
        {
          // Rely on warp-synchronous programming
          ost << "if (tid == " << index_thread << ")\n";
          ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
          ost << INT << " index = int_mirror[wid];\n";
        }
        ost << "output_" << spec_idx << " += rrs[index][tid];\n";
      }
    }
#endif
  }
  else // Not using indexing
  {
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      if (wid == 0)
        ost << "if (wid == " << wid << ")\n";
      else
        ost << "else if (wid == " << wid << ")\n";
      PairDelim warp_pair(ost);
      std::set<Reaction*> loaded_ropls;
      for (unsigned spec_idx = 0; spec_idx < chem_warps[wid]->specs.size(); spec_idx++)
      {
        Species *output = chem_warps[wid]->specs[spec_idx];
        // Go through all the reactions and see if we need the ropl value
        for (unsigned wid2 = 0; wid2 < chem_warps.size(); wid2++)
        {
          for (int level = start_level; level < stop_level; level++)
          {
            if (level < int(chem_warps[wid2]->reacs.size()))
            {
              Reaction *reac = chem_warps[wid2]->reacs[level];
              if (output->reaction_contributions.find(reac) !=
                  output->reaction_contributions.end())
              {
                // See if we loaded it yet
                if (loaded_ropls.find(reac) == loaded_ropls.end())
                {
                  if (!spill_reac_rates)
                  {
                    int index = wid2+(level-start_level)*chem_warps.size();
                    ost << REAL << " ropl_" << reac->idx << " = scratch[" << index << "][tid];\n";
                  }
                  else
                  {
                    int forward_loc = global_analyzer->find_forward_location(reac, 0/*pass*/);
                    assert(forward_loc != -1);
                    ost << REAL << " ropl_" << reac->idx << " = rr_g[smid*"
                        << sm_offset << "+" << forward_loc << "][tid];\n";
                  }
                  loaded_ropls.insert(reac);
                }
                int coeff = output->reaction_contributions[reac];
                if (coeff == 1)
                  ost << "output_" << spec_idx << " += ropl_" << reac->idx << ";\n";
                else if (coeff == -1)
                  ost << "output_" << spec_idx << " -= ropl_" << reac->idx << ";\n";
                else
                  ost << "output_" << spec_idx << " = __fma_rn(" << double(coeff) << ",ropl_"
                      << reac->idx << ",output_" << spec_idx << ");\n";
              }
            }
          }
        }
      }
    }
  }
}

void TranslationUnit::emit_multi_output(CodeOutStream &ost)
{
  const unsigned num_specs = (ordered_species.size()+reac_warps-1)/reac_warps;
  const bool uniform_specs = ((ordered_species.size()%reac_warps) == 0);
  for (unsigned spec_idx = 0; spec_idx < num_specs; spec_idx++)
  {
    PairDelim spec_pair(ost);
    // First reduce the values across the sub-warps
    if (k20)
    {
      if (points_per_cta < 32)
        ost << INT << " hi_part,lo_part;\n";
      for (int i = points_per_cta; i < 32; i*=2)
      {
        ost << "hi_part = __shfl_xor(__double2hiint(output_" << spec_idx << ")," << i << ",32);\n"; 
        ost << "lo_part = __shfl_xor(__double2loint(output_" << spec_idx << ")," << i << ",32);\n";
        ost << "output_" << spec_idx << " += __hiloint2double(hi_part,lo_part);\n";
      }
    }
    else if (points_per_cta < 32)
    {
      char mask[64];
      sprintf(mask,"0x%x",(32/points_per_cta)-1);
      // Here we use warp-synchronous reductions linearly 
      for (int idx = 1; idx < (32/points_per_cta); idx++)
      {
        ost << "if ((wid & " << mask << ") == " << idx << ")\n";
        ost << "  reduc_mirror[threadIdx.x >> 5][tid] = output_" << spec_idx << ";\n";
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  output_" << spec_idx << " += reduc_mirror[threadIdx.x >> 5][tid];\n";
      }
      // Then scatter back out so everyone has the same value
      ost << "if ((wid & " << mask << ") == 0)\n";
      ost << "  reduc_mirror[threadIdx.x >> 5][tid] = output_" << spec_idx << ";\n";
      ost << "output_" << spec_idx << " = reduc_mirror[threadIdx.x >> 5][tid];\n";
    }
    // Get the index for the species
    int meta_index = -1;
    for (unsigned wid = 0; wid < chem_warps.size(); wid++)
    {
      if (spec_idx < chem_warps[wid]->specs.size())
      {
        Species *spec = chem_warps[wid]->specs[spec_idx];
        int index = find_ordered_index(spec);
        assert(index != -1);
        int global_idx = chem_warps[wid]->add_index(index, 0);
        if (meta_index == -1)
          meta_index = global_idx;
        else
          assert(meta_index == global_idx);
      }
      else
      {
        int global_idx = chem_warps[wid]->add_index(0, 0);
        if (meta_index == -1)
          meta_index = global_idx;
        else
          assert(meta_index == global_idx);
      }
    }
    int index_offset = meta_index/points_per_cta;
    int index_thread = meta_index%points_per_cta;
    if (k20)
      ost << INT << " index = __shfl(index_constants[" << index_offset << "]," 
          << index_thread << "," << points_per_cta << ");\n";
    else
    {
      // Rely on warp-synchronous programming
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
      ost << INT << " index = int_mirror[wid];\n";
    }
    if (!uniform_specs && (spec_idx == (num_specs-1)))
      ost << "if (((threadIdx.x >> 5)+" << (spec_idx*reac_warps) << ") < " << ordered_species.size() << ")\n";
    PairDelim output_pair(ost);
    ost << "output_" << spec_idx << " *= (1e-3 * molecular_masses[index]);\n";
    char out_name[64];
    sprintf(out_name,"output_%d",spec_idx);
    emit_cuda_store(ost,WDOT_ARRAY,"index*spec_stride",out_name);
    if (emit_debug)
    {
      ost << "if ((blockIdx.x == 0) && (blockIdx.y == 0) && (tid==0) && (step==0))\n";
      PairDelim if_pair(ost);
      bool first = true;
      for (unsigned wid = 0; wid < chem_warps.size(); wid++)
      {
        if (spec_idx >= chem_warps[wid]->specs.size())
          continue;
        if (first)
          ost << "if (wid == " << wid << ")\n";
        else
          ost << "else if (wid == " << wid << ")\n";
        first = false;
        PairDelim warp_pair(ost);
        Species *spec = chem_warps[wid]->specs[spec_idx];
        int index = find_ordered_index(spec);
        ost << "printf(\"warp_spec[" << index << "] = %.8g\\n\", " << out_name << ");\n";
      }
    }
  }
}

static bool input_sort_function(QSS *left, QSS *right)
{
  // Sort them backwards so greatest is first
  if (left->closure_inputs.size() <= right->closure_inputs.size())
    return false;
  return true;
}

void TranslationUnit::emit_multi_qssa_code(CodeOutStream &ost, int qssa_passes, 
                          int levels_per_pass, int shared_rrs)
{
  assert(chem != NULL);
  ost << "// Main loop\n";
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim for_pair(ost);
  if (use_indexing)
    ost << INT << " index;\n";
  if (k20 && (warp_size < 32))
    ost << INT << " hi_part, lo_part;\n";
  if (qssa_warps.size() == 1)
  {
    emit_multi_qssa_init(ost);
    for (int pass = 0; pass < qssa_passes; pass++)
    {
      emit_barrier(ost, 0, (reac_warps+qss_warps), false/*wait*/);
      emit_barrier(ost, 1, (reac_warps+qss_warps), true/*wait*/);
      int start_level = pass*levels_per_pass;
      int stop_level = (pass+1)*levels_per_pass;
      if (stop_level > int(chem_warps[0]->reacs.size()))
        stop_level = chem_warps[0]->reacs.size();
      emit_multi_qssa_pass(ost, pass, start_level, stop_level);
    }
    emit_multi_qssa_scaling(ost);
    emit_barrier(ost, 0, (reac_warps+qss_warps), false/*wait*/);
    emit_barrier(ost, 1, (reac_warps+qss_warps), true/*wait*/);
  }
  else
  {
    // We're going to do warp-specialization for QSSA
    if (warp_size != 32)
    {
      fprintf(stderr,"ERROR: Multiple QSSA warps are only supported for warp-sizes of 32!\n");
      fprintf(stderr,"Named barrier synchronization only works at full warp granularity.\n");
      exit(1);
    }
    
    QSSASharedMemoryStore *store = configure_warp_spec_qssa(shared_rrs, 0);

    // Now we're ready to do code generation
    for (unsigned wid = 0; wid < qssa_warps.size(); wid++)
    {
      if (wid == 0)
        ost << "if (wid == " << wid << ")\n";
      else
        ost << "else if (wid == " << wid << ")\n";
      PairDelim warp_pair(ost);
      emit_multi_warp_spec_qssa_init(ost, wid);
      for (int pass = 0; pass < qssa_passes; pass++)
      {
        emit_barrier(ost, 0, (reac_warps+qss_warps), false/*wait*/);
        emit_barrier(ost, 1, (reac_warps+qss_warps), true/*wait*/);
        int start_level = pass*levels_per_pass;
        int stop_level = (pass+1)*levels_per_pass;
        if (stop_level > int(chem_warps[0]->reacs.size()))
          stop_level = chem_warps[0]->reacs.size();
        emit_multi_warp_spec_qssa_pass(ost, pass, start_level, stop_level, wid);
      }
      // Emit a barrier here to make sure everyone is done reading
      // before writing values into shared memory
      emit_barrier(ost, 3, qss_warps, true/*wait*/);
      emit_multi_warp_spec_qssa_scaling(ost, *store, wid);
      emit_barrier(ost, 0, (reac_warps+qss_warps), false/*wait*/);
      emit_barrier(ost, 1, (reac_warps+qss_warps), true/*wait*/);
    }
  }
}

QSSASharedMemoryStore* TranslationUnit::configure_warp_spec_qssa(int shared_size, int global_offset)
{
  // Construct the DAG of QSS nodes as well as the dependences
  // between all of them 
  std::vector<QSS*> qss_nodes;
  chem->construct_qss_graph(qss_nodes);
  // Sort the list based on the number of closure input locations
  std::sort(qss_nodes.begin(),qss_nodes.end(),input_sort_function);
  // Assign the values with the most unique inputs to different warps
  {
    std::set<QSS*> assigned_nodes;
    for (unsigned idx = 0; (idx < qss_nodes.size()) && (int(idx) < qss_warps); idx++)
    {
      qssa_warps[idx]->add_qss(qss_nodes[idx]); 
      assigned_nodes.insert(qss_nodes[idx]);
      //printf("Assigning QSS %d to warp %d\n", qss_nodes[idx]->qss_idx, idx);
    }
    // Now for each input, grow backwards assigning dependent nodes
    // to the same warp, unless the node has already been assigned, 
    // in which case set up the named barriers for the exchange
    int next_barrier = 2; // 0 and 1 between QSSA and reac warps, 2 for reac, 3 for qssa
    for (int idx = 0; idx < qss_warps; idx++)
    {
      if (qssa_warps[idx]->qss_specs.empty())
        continue;
      QSS *root = qssa_warps[idx]->qss_specs[0];
      std::set<QSS*> local_nodes; // nodes local to this warp
      local_nodes.insert(root);
      std::vector<QSS*> frontier;
      frontier.push_back(root);
      while (!frontier.empty())
      {
        QSS *current = frontier.back();
        frontier.pop_back();
        for (std::set<QSS*>::const_iterator it = current->inputs.begin();
              it != current->inputs.end(); it++)
        {
          if (assigned_nodes.find(*it) != assigned_nodes.end())
          {
            // This node has already been asigned, check for the same warp
            // If not setup the named barrier information
            if (local_nodes.find(*it) == local_nodes.end())
            {
              assert(current->input_barriers.find(*it) == current->input_barriers.end());
              assert(current->xstat_output_barriers.find(*it) == current->xstat_output_barriers.end());
              assert((*it)->output_barriers.find(current) == (*it)->output_barriers.end());
              assert((*it)->xstat_input_barriers.find(current) == (*it)->xstat_input_barriers.end());
              current->input_barriers[*it] = next_barrier;
              (*it)->output_barriers[current] = next_barrier;
              next_barrier++;
              (*it)->xstat_input_barriers[current] = next_barrier;
              current->xstat_output_barriers[*it] = next_barrier;
              next_barrier++;
            }
            // Otherwise we've already explored it, no need to do it again
          }
          else
          {
            // Assign it to our warp
            qssa_warps[idx]->add_qss(*it);
            local_nodes.insert(*it);
            assigned_nodes.insert(*it);
            frontier.push_back(*it);
          }
        }
      }
    }
    // Finally load balance any other nodes and all their dependences across the warps
    for (std::vector<QSS*>::const_iterator it = qss_nodes.begin();
          it != qss_nodes.end(); it++)
    {
      if (assigned_nodes.find(*it) != assigned_nodes.end())
        continue;
      int fewest_qss = qss_nodes.size();
      int fewest_idx = -1;
      for (unsigned idx = 0; idx < qssa_warps.size(); idx++)
      {
        if (int(qssa_warps[idx]->qss_specs.size()) < fewest_qss)
        {
          fewest_qss = qssa_warps[idx]->qss_specs.size();
          fewest_idx = idx;
        }
      }
      assert(fewest_idx != -1);
      qssa_warps[fewest_idx]->add_qss(*it);
      assigned_nodes.insert(*it);
      std::set<QSS*> local_nodes(qssa_warps[fewest_idx]->qss_specs.begin(),
                                 qssa_warps[fewest_idx]->qss_specs.end()); // nodes local to this warp
      std::vector<QSS*> frontier;
      frontier.push_back(*it);
      while (!frontier.empty())
      {
        QSS *current = frontier.back();
        frontier.pop_back();
        for (std::set<QSS*>::const_iterator it = current->inputs.begin();
              it != current->inputs.end(); it++)
        {
          if (assigned_nodes.find(*it) != assigned_nodes.end())
          {
            // This node has already been asigned, check for the same warp
            // If not setup the named barrier information
            if (local_nodes.find(*it) == local_nodes.end())
            {
              assert(current->input_barriers.find(*it) == current->input_barriers.end());
              assert(current->output_barriers.find(*it) == current->output_barriers.end());
              assert((*it)->xstat_input_barriers.find(current) == (*it)->xstat_input_barriers.end());
              assert((*it)->xstat_output_barriers.find(current) == (*it)->xstat_output_barriers.end());
              current->input_barriers[*it] = next_barrier;
              (*it)->output_barriers[current] = next_barrier;
              next_barrier++;
              (*it)->xstat_input_barriers[current] = next_barrier;
              current->xstat_output_barriers[*it] = next_barrier;
              next_barrier++;
            }
            // Otherwise we've already explored it, no need to do it again
          }
          else
          {
            // Assign it to our warp
            qssa_warps[fewest_idx]->add_qss(*it);
            local_nodes.insert(*it);
            assigned_nodes.insert(*it);
            frontier.push_back(*it);
          }
        }
      }
    }
    if ((next_barrier) <= MAX_BARRIERS)
    {
      fprintf(stdout,"INFO: Warp-specialized QSSA kernel needs a total of %d named barriers per CTA.\n", next_barrier);
    }
    else
    {
      fprintf(stderr,"ERROR: Too many named barriers required for warp-specialized QSSA.  Requested %d "
        "but only %d available.  Reduce number of CTAs or number of QSSA warps.\n", next_barrier*target_ctas, MAX_BARRIERS);
    }
  }
  // Before doing the scaling, construct a shared memory map
  // for values that need to be passed between warps
  QSSASharedMemoryStore *result = new QSSASharedMemoryStore(shared_size, qss_nodes.size(), global_offset);
  for (unsigned wid = 0; wid < qssa_warps.size(); wid++)
  {
    result->analyze_warp(qssa_warps[wid]);
  }
  return result;
}

void TranslationUnit::emit_multi_qssa_init(CodeOutStream &ost)
{
  for (std::vector<ConnectedComponent*>::const_iterator cc = 
        chem->connected_components.begin(); cc !=
        chem->connected_components.end(); cc++)
  {
    emit_multi_cc_init(ost, *cc);
  }
}

void TranslationUnit::emit_multi_qssa_pass(CodeOutStream &ost, int pass, int start_level, int stop_level)
{
  if (use_indexing)
  {
    for (std::vector<ConnectedComponent*>::const_iterator cc = 
          chem->connected_components.begin(); cc !=
          chem->connected_components.end(); cc++)
    {
      emit_multi_cc_pass(ost, *cc, pass, start_level, stop_level);
    }
  }
  else
  {
    for (unsigned wid = 0; wid < qssa_warps.size(); wid++)
    {
      if (qssa_warps.size() > 1)
      {
        if (wid == 0)
          ost << "if (wid == " << wid << ")\n";
        else
          ost << "else if (wid == " << wid << ")\n";
      }
      PairDelim warp_pair(ost);
      std::set<Reaction*> loaded_forward;
      std::set<Reaction*> loaded_reverse;
      for (std::vector<ConnectedComponent*>::const_iterator cc = 
            chem->connected_components.begin(); cc !=
            chem->connected_components.end(); cc++)
      {
        for (std::map<int,QSS*>::const_iterator qs = (*cc)->species.begin();
              qs != (*cc)->species.end(); qs++)
        {
          QSS *qss = qs->second;
          for (std::map<Reaction*,int>::const_iterator it = qss->forward_zeros.begin();
                it != qss->forward_zeros.end(); it++)
          {
            int loc = chem_analyzer->find_forward_location(it->first, pass);
            if (loc == -1)
              continue;
            if (loaded_forward.find(it->first) == loaded_forward.end())
            {
              ost << REAL << " forward_" << it->first->idx << " = rrs[" << loc << "][tid];\n";
              loaded_forward.insert(it->first);
            }
            assert(it->second > 0);
            if (it->second == 1)
              ost << "a" << qss->qss_idx << "_0 += forward_" << it->first->idx << ";\n";
            else
              ost << "a" << qss->qss_idx << "_0 = __fma_rn(" << double(it->second) << ",forward_"
                  << it->first->idx << ",a" << qss->qss_idx << "_0);\n";
          }
          for (std::map<Reaction*,int>::const_iterator it = qss->backward_zeros.begin();
                it != qss->backward_zeros.end(); it++)
          {
            int loc = chem_analyzer->find_reverse_location(it->first, pass);
            if (loc == -1)
              continue;
            if (loaded_reverse.find(it->first) == loaded_reverse.end())
            {
              ost << REAL << " reverse_" << it->first->idx << " = rrs[" << loc << "][tid];\n";
              loaded_reverse.insert(it->first);
            }
            assert(it->second > 0);
            if (it->second == 1)
              ost << "a" << qss->qss_idx << "_0 += reverse_" << it->first->idx << ";\n";
            else
              ost << "a" << qss->qss_idx << "_0 = __fma_rn(" << double(it->second) << ",reverse_"
                  << it->first->idx << ",a" << qss->qss_idx << "_0);\n";
          }
          for (std::set<Reaction*>::const_iterator it = qss->forward_denom.begin();
                it != qss->forward_denom.end(); it++)
          {
            int loc = chem_analyzer->find_forward_location(*it, pass);
            if (loc == -1)
              continue;
            if (loaded_forward.find(*it) == loaded_forward.end())
            {
              ost << REAL << " forward_" << (*it)->idx << " = rrs[" << loc << "][tid];\n";
              loaded_forward.insert(*it);
            }
            ost << "denom_" << qss->qss_idx << " += forward_" << (*it)->idx << ";\n";
          }
          for (std::set<Reaction*>::const_iterator it = qss->backward_denom.begin();
                it != qss->backward_denom.end(); it++)
          {
            int loc = chem_analyzer->find_reverse_location(*it, pass);
            if (loc == -1)
              continue;
            if (loaded_reverse.find(*it) == loaded_reverse.end())
            {
              ost << REAL << " reverse_" << (*it)->idx << " = rrs[" << loc << "][tid];\n";
              loaded_reverse.insert(*it);
            }
            ost << "denom_" << qss->qss_idx << " += reverse_" << (*it)->idx << ";\n";
          }
          for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = qss->forward_contributions.begin();
                it != qss->forward_contributions.end(); it++)
          {
            int loc = chem_analyzer->find_forward_location(it->first.second, pass);
            if (loc == -1)
              continue;
            if (loaded_forward.find(it->first.second) == loaded_forward.end())
            {
              ost << REAL << " forward_" << it->first.second->idx << " = rrs[" << loc << "][tid];\n";
              loaded_forward.insert(it->first.second);
            }
            assert(it->second > 0);
            if (it->second == 1)
              ost << "a" << qss->qss_idx << "_" << it->first.first << " += forward_" << it->first.second->idx << ";\n";
            else
              ost << "a" << qss->qss_idx << "_" << it->first.first << " = __fma_rn(" << double(it->second) << ", forward_"
                  << it->first.second->idx << ", a" << qss->qss_idx << "_" << it->first.first << ");\n";
          }
          for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = qss->backward_contributions.begin();
                it != qss->backward_contributions.end(); it++)
          {
            int loc = chem_analyzer->find_reverse_location(it->first.second, pass);
            if (loc == -1)
              continue;
            if (loaded_reverse.find(it->first.second) == loaded_reverse.end())
            {
              ost << REAL << " reverse_" << it->first.second->idx << " = rrs[" << loc << "][tid];\n";
              loaded_reverse.insert(it->first.second);
            }
            assert(it->second > 0);
            if (it->second == 1)
              ost << "a" << qss->qss_idx << "_" << it->first.first << " += reverse_" << it->first.second->idx << ";\n";
            else
              ost << "a" << qss->qss_idx << "_" << it->first.first << " = __fma_rn(" << double(it->second) << ", reverse_"
                  << it->first.second->idx << ", a" << qss->qss_idx << "_" << it->first.first << ");\n";
          }
        }
      }
    }
  }
}

void TranslationUnit::emit_multi_qssa_scaling(CodeOutStream &ost)
{
  for (std::vector<ConnectedComponent*>::const_iterator cc = 
        chem->connected_components.begin(); cc !=
        chem->connected_components.end(); cc++)
  {
    emit_multi_cc_scaling(ost, *cc);
  }
}

void TranslationUnit::emit_multi_cc_init(CodeOutStream &ost, ConnectedComponent *cc)
{
  for (std::map<int,QSS*>::const_iterator qs = cc->species.begin();
        qs != cc->species.end(); qs++)
  {
    QSS *qss = qs->second;
    ost << REAL << " a" << qss->qss_idx << "_0 = 0.0"; 
    multi_emitted.insert(std::pair<int,int>(qss->qss_idx,0));
    ost << ", denom_" << qss->qss_idx << " = 0.0";
    std::set<int> others;
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = qss->forward_contributions.begin();
          it != qss->forward_contributions.end(); it++)
    {
      others.insert(it->first.first);
    }
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = qss->backward_contributions.begin();
          it != qss->backward_contributions.end(); it++)
    {
      others.insert(it->first.first);
    }
    for (std::set<int>::const_iterator it = others.begin();
          it != others.end(); it++)
    {
      ost << ", a" << qss->qss_idx << "_" << (*it) << " = 0.0";
      multi_emitted.insert(std::pair<int,int>(qss->qss_idx,*it));
    }
    ost << ";\n";
  }
}

void TranslationUnit::emit_multi_cc_pass(CodeOutStream &ost, ConnectedComponent *cc,
                                          int pass, int start_level, int stop_level)
{
  for (std::map<int,QSS*>::const_iterator it = cc->species.begin();
        it != cc->species.end(); it++)
  {
    it->second->emit_multi_loads(ost, pass, chem_analyzer, qssa_warps, chem_warps.size());
  }
}

void TranslationUnit::emit_multi_cc_scaling(CodeOutStream &ost, ConnectedComponent *cc)
{
  ost << "// Connected Component\n";
  PairDelim cc_pair(ost);
  // Reduce all the values across the different sub-warps
  for (std::map<int, QSS*>::const_iterator it = cc->species.begin();
        it != cc->species.end(); it++)
  {
    it->second->emit_multi_reductions(ost);
  }
  ost << REAL << " den";
  for (std::map<int, QSS*>::const_iterator it = cc->species.begin();
        it != cc->species.end(); it++)
  {
    ost << ", xq_" << it->second->qss_idx;
  }
  ost << ";\n";
  // Emit the code
  for (std::vector<Statement*>::const_iterator it = cc->statements.begin();
        it != cc->statements.end(); it++)
  {
    (*it)->emit_code(ost, multi_emitted, NULL);
  }
  // Then store each of the qss values into shared memory
  if (points_per_cta < 32)
  {
    char mask[64];
    sprintf(mask,"0x%x",(32/points_per_cta)-1);
    ost << "if ((wid & " << mask << ") == 0)\n";
  }
  PairDelim output_pair(ost);
  for (std::map<int, QSS*>::const_iterator it = cc->species.begin();
        it != cc->species.end(); it++)
  {
    assert(it->first > 0);
    ost << "rrs[" << (it->first-1) << "][tid] = xq_" << it->first << ";\n";
  }
}

void TranslationUnit::emit_multi_full_code(CodeOutStream &ost, const char *temp_chem_name,
                                const char *temp_qssa_name, int cta_shared_size)
{
  emit_warp_specialized_constants(ost);

  emit_gpu_getrates_declaration(ost);
  ost << "\n";

  PairDelim func_pair(ost);

  ost << "__shared__ volatile " << REAL << " scratch[" << cta_shared_size << "][" << points_per_cta << "];\n";
  
  if (!k20)
  {
    assert((32 % points_per_cta) == 0);
    int total_warps = (reac_warps + ((chem == NULL) ? 0 : 1)) * (32/points_per_cta);
    ost << "__shared__ volatile " << REAL << " real_mirror[" << total_warps << "];\n";
    ost << "__shared__ volatile " << INT << " int_mirror[" << total_warps << "];\n";
    ost << "__shared__ volatile " << REAL << " reduc_mirror[" << (reac_warps+((chem == NULL) ? 0 : 1)) << "][" << (points_per_cta) << "];\n";
  }
  int power = int(log(points_per_cta)/log(2));
  char mask[8];
  sprintf(mask,"0x%x",points_per_cta-1);
  if (chem != NULL)
  {
    //ost << "if (threadIdx.x < " << (reac_warps*32) << ")\n";
    {
      //PairDelim if_pair(ost);
      ost << "const " << INT << " wid = threadIdx.x >> " << power << ";\n";
      ost << "const " << INT << " tid = threadIdx.x & " << mask << ";\n";
      if (spill_reac_rates)
      {
        ost << INT << " smid;\n";
        ost << "asm volatile(\"mov.u32 %0, %smid;\" : \"=r\"(smid) : : );\n";
      }

      emit_getrates_numerical_constants(ost);

      // Scale the pointers
      {
        PairDelim off_pair(ost);
        ost << "const " << INT << " offset = (blockIdx.x*"
            << points_per_cta << " + tid);\n";
        ost << PRESSURE_ARRAY << " += offset;\n";
        ost << TEMPERATURE_ARRAY << " += offset;\n";
        ost << MASS_FRAC_ARRAY << " += offset;\n";
        ost << AVMOLWT_ARRAY << " += offset;\n";
        ost << DIFFUSION_ARRAY << " += offset;\n";
        ost << WDOT_ARRAY << " += offset;\n";
      }

      // Write the identity locations
#if 0
      ost << "if (wid == 0)\n";
      {
        PairDelim identity_pair(ost);
        ost << "rrs[" << reac_unity << "][tid] = 1.0;\n";
        ost << "rrs[" << reac_zero << "][tid] = 0.0;\n";
        ost << "gibbs[" << (gibbs_species.size()) << "][tid] = 1.0;\n";
        ost << "buffer[" << spec_unity << "][tid] = 1.0;\n";
      }
#endif

      // Emit the code for loading the constants that we need
      assert((chem_warps[0]->int_constants[0].size()%points_per_cta) == 0);
      const unsigned num_index_vals = chem_warps[0]->int_constants[0].size()/points_per_cta;
      if (num_index_vals > 0)
      {
        ost << INT << " index_constants[" << (num_index_vals) << "];\n";
        // Emit the code to load the constants
        for (unsigned idx = 0; idx < num_index_vals; idx++)
        {
          char src_index[64];
          sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*chem_warps.size()*points_per_cta);
          char dst_index[64];
          sprintf(dst_index,"%d",idx);
          emit_cuda_int_load(ost, "index_constants", dst_index, "chem_index", src_index, k20);
        }
      }
      assert((chem_warps[0]->real_constants[0].size()%points_per_cta) == 0);
      const unsigned num_real_vals = chem_warps[0]->real_constants[0].size()/points_per_cta;
      if (num_real_vals > 0)
      {
        ost << REAL << " real_constants[" << (num_real_vals) << "];\n";
        for (unsigned idx = 0; idx < num_real_vals; idx++)
        {
          char src_index[64];
          sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*chem_warps.size()*points_per_cta);
          char dst_index[64];
          sprintf(dst_index,"%d",idx);
          emit_cuda_load(ost, "real_constants", dst_index, "chem_real", src_index, k20);
        }
      }

      // Then copy over the code from the temporary file
      FILE *temp_file = fopen(temp_chem_name,"r");
      assert(temp_file != NULL);
      char c = fgetc(temp_file);
      while (c != EOF)
      {
        ost << c;
        c = fgetc(temp_file);
      }
      assert(fclose(temp_file) == 0);
    }
#if 0
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "const " << INT << " wid = (threadIdx.x - " << (reac_warps*32) << ") >> " << power << ";\n";
      ost << "const " << INT << " tid = threadIdx.x & " << mask << ";\n";

      assert((qssa_warps[0]->int_constants[0].size()%points_per_cta) == 0);
      const unsigned num_index_vals = qssa_warps[0]->int_constants[0].size()/points_per_cta;
      if (num_index_vals > 0)
      {
        ost << INT << " index_constants[" << (num_index_vals) << "];\n";
        for (unsigned idx = 0; idx < num_index_vals; idx++)
        {
          char src_index[64];
          sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*qssa_warps.size()*points_per_cta);
          char dst_index[64];
          sprintf(dst_index,"%d",idx);
          emit_cuda_int_load(ost, "index_constants", dst_index, "qssa_index", src_index, k20);
        }
      }

      // Then copy over the code from the temporary file
      FILE *temp_file = fopen(temp_qssa_name,"r");
      assert(temp_file != NULL);
      char c = fgetc(temp_file);
      while (c != EOF)
      {
        ost << c;
        c = fgetc(temp_file);
      }
      assert(fclose(temp_file) == 0);
    }
#endif
  }
  else
  {
    ost << "const " << INT << " wid = threadIdx.x >> " << power << ";\n";
    ost << "const " << INT << " tid = threadIdx.x & " << mask << ";\n";
    if (spill_reac_rates)
    {
      ost << INT << " smid;\n";
      ost << "asm volatile(\"mov.u32 %0, %smid;\" : \"=r\"(smid) : : );\n";
    }

    emit_getrates_numerical_constants(ost);

    // Scale the pointers
    {
      PairDelim off_pair(ost);
      ost << "const " << INT << " offset = (blockIdx.x*"
          << points_per_cta << " + tid);\n";
      ost << PRESSURE_ARRAY << " += offset;\n";
      ost << TEMPERATURE_ARRAY << " += offset;\n";
      ost << MASS_FRAC_ARRAY << " += offset;\n";
      ost << AVMOLWT_ARRAY << " += offset;\n";
      ost << DIFFUSION_ARRAY << " += offset;\n";
      ost << WDOT_ARRAY << " += offset;\n";
    }

    // Write the identity locations
#if 0
    ost << "if (wid == 0)\n";
    {
      PairDelim identity_pair(ost);
      ost << "rrs[" << reac_unity << "][tid] = 1.0;\n";
      ost << "rrs[" << reac_zero << "][tid] = 0.0;\n";
      ost << "gibbs[" << (gibbs_species.size()) << "][tid] = 1.0;\n";
      ost << "buffer[" << spec_unity << "][tid] = 1.0;\n";
    }
#endif

    // Emit the code for loading the constants that we need
    assert((chem_warps[0]->int_constants[0].size()%points_per_cta) == 0);
    const unsigned num_index_vals = chem_warps[0]->int_constants[0].size()/points_per_cta;
    ost << INT << " index_constants[" << (num_index_vals) << "];\n";
    assert((chem_warps[0]->real_constants[0].size()%points_per_cta) == 0);
    const unsigned num_real_vals = chem_warps[0]->real_constants[0].size()/points_per_cta;
    ost << REAL << " real_constants[" << (num_real_vals) << "];\n";
    // Emit the code to load the constants
    for (unsigned idx = 0; idx < num_index_vals; idx++)
    {
      char src_index[64];
      sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*chem_warps.size()*points_per_cta);
      char dst_index[64];
      sprintf(dst_index,"%d",idx);
      emit_cuda_int_load(ost, "index_constants", dst_index, "chem_index", src_index, k20);
    }
    for (unsigned idx = 0; idx < num_real_vals; idx++)
    {
      char src_index[64];
      sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*chem_warps.size()*points_per_cta);
      char dst_index[64];
      sprintf(dst_index,"%d",idx);
      emit_cuda_load(ost, "real_constants", dst_index, "chem_real", src_index, k20);
    }

    // Then copy over the code from the temporary file
    FILE *temp_file = fopen(temp_chem_name,"r");
    assert(temp_file != NULL);
    char c = fgetc(temp_file);
    while (c != EOF)
    {
      ost << c;
      c = fgetc(temp_file);
    }
    assert(fclose(temp_file) == 0);
  }
}

void TranslationUnit::emit_multi_load_rates(CodeOutStream &ost, const std::vector<int> &locations,
      const std::vector<Warp*> &warps, const char *var_name, int warp_offset, int pass)
{
  assert(false);
#if 0
  std::vector<std::vector<int> > assignment;
  chem_analyzer->perform_assignment(pass, locations, assignment);
  int max_steps = -1;
  for (unsigned idx = 0; idx < assignment.size(); idx++)
  {
    if (int(assignment[idx].size()) > max_steps)
      max_steps = assignment[idx].size();
  }
  assert(assignment.size() == warps.size());
  // Now emit the loads for the warp 
  for (int step = 0; step < max_steps; step++)
  {
    int meta_index = -1;
    // Update all the qssa_warps with the appropriate index
    for (unsigned wid = 0; wid < warps.size(); wid++)
    {
      if (step < int(assignment[wid].size()))
      {
        int global_idx = warps[wid]->add_index(
                  assignment[wid][step], 0); 
        if (meta_index == -1)
          meta_index = global_idx;
        else
          assert(meta_index == global_idx);
      }
      else
      {
        int global_idx = warps[wid]->add_index(
                  reac_zero, 0);
        if (meta_index == -1)
          meta_index = global_idx;
        else
          assert(meta_index == global_idx);
      }
    }
    // Now emit the code for the exchange of index
    int index_offset = meta_index/points_per_cta;
    int index_thread = meta_index%points_per_cta;
    if (k20)
      ost << "index = __shfl(index_constants[" << index_offset << "]," 
          << index_thread << "," << points_per_cta << ");\n";
    else
    {
      // Rely on warp-synchronous programming
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  int_mirror[wid+" << (warp_offset) 
          << "] = index_constants[" << index_offset << "];\n";
      ost << "index = int_mirror[wid+" 
          << (warp_offset) << "];\n";
    }
    ost << var_name << " += rrs[index][tid];\n";
  }
#endif
}

void TranslationUnit::emit_multi_reduce(CodeOutStream &ost, const char *var_name)
{
  if (k20)
  {
    for (int i = points_per_cta; i < 32; i*=2)
    {
      ost << "hi_part = __shfl_xor(__double2hiint(" << var_name << ")," << i << ",32);\n"; 
      ost << "lo_part = __shfl_xor(__double2loint(" << var_name << ")," << i << ",32);\n";
      ost << var_name << " += __hiloint2double(hi_part,lo_part);\n";
    }
  }
  else
  {
    char mask[64];
    sprintf(mask,"0x%x",(32/points_per_cta)-1);
    // Here we use warp-synchronous reductions linearly
    for (int idx = 1; idx < (32/points_per_cta); idx++)
    {
      ost << "if ((wid & " << mask << ") == " << idx << ")\n";
      ost << "  reduc_mirror[(threadIdx.x >> 5)][tid] = " << var_name << ";\n";
      ost << "if ((wid & " << mask << ") == 0)\n";
      ost << "  " << var_name << " += reduc_mirror[(threadIdx.x >> 5)][tid];\n";
    }
    // Then scatter back out so everyone has the same value
    ost << "if ((wid & " << mask << ") == 0)\n";
    ost << "  reduc_mirror[(threadIdx.x >> 5)][tid] = " << var_name << ";\n";
    ost << var_name << " = reduc_mirror[(threadIdx.x >> 5)][tid];\n";
  }
}

void TranslationUnit::emit_multi_warp_spec_qssa_init(CodeOutStream &ost, int wid)
{
  Warp *warp = qssa_warps[wid];
  for (std::vector<QSS*>::const_iterator qs = warp->qss_specs.begin();
        qs != warp->qss_specs.end(); qs++)
  {
    QSS *qss = *qs;
    ost << REAL << " a" << qss->qss_idx << "_0 = 0.0"; 
    warp->warp_emitted.insert(std::pair<int,int>(qss->qss_idx,0));
    ost << ", denom_" << qss->qss_idx << " = 0.0";
    std::set<int> others;
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = qss->forward_contributions.begin();
          it != qss->forward_contributions.end(); it++)
    {
      others.insert(it->first.first);
    }
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = qss->backward_contributions.begin();
          it != qss->backward_contributions.end(); it++)
    {
      others.insert(it->first.first);
    }
    for (std::set<int>::const_iterator it = others.begin();
          it != others.end(); it++)
    {
      ost << ", a" << qss->qss_idx << "_" << (*it) << " = 0.0";
      warp->warp_emitted.insert(std::pair<int,int>(qss->qss_idx,*it));
    }
    ost << ";\n";
  }
}

void TranslationUnit::emit_multi_warp_spec_qssa_pass(CodeOutStream &ost, int pass, int start_level,
                                                      int stop_level, int wid)
{
  Warp *warp = qssa_warps[wid];
  std::set<Reaction*> loaded_forward;
  std::set<Reaction*> loaded_reverse;
  // Emit the loads for the values that we need
  for (std::vector<QSS*>::const_iterator qs = warp->qss_specs.begin();
        qs != warp->qss_specs.end(); qs++)
  {
    QSS *qss = *qs;
    for (std::map<Reaction*,int>::const_iterator it = qss->forward_zeros.begin();
          it != qss->forward_zeros.end(); it++)
    {
      int loc = chem_analyzer->find_forward_location(it->first, pass);
      if (loc == -1)
        continue;
      if (loaded_forward.find(it->first) == loaded_forward.end())
      {
        ost << REAL << " forward_" << it->first->idx << " = rrs[" << loc << "][tid];\n";
        loaded_forward.insert(it->first);
      }
      assert(it->second > 0);
      if (it->second == 1)
        ost << "a" << qss->qss_idx << "_0 += forward_" << it->first->idx << ";\n";
      else
        ost << "a" << qss->qss_idx << "_0 = __fma_rn(" << double(it->second) << ",forward_"
            << it->first->idx << ",a" << qss->qss_idx << "_0);\n";
    }
    for (std::map<Reaction*,int>::const_iterator it = qss->backward_zeros.begin();
          it != qss->backward_zeros.end(); it++)
    {
      int loc = chem_analyzer->find_reverse_location(it->first, pass);
      if (loc == -1)
        continue;
      if (loaded_reverse.find(it->first) == loaded_reverse.end())
      {
        ost << REAL << " reverse_" << it->first->idx << " = rrs[" << loc << "][tid];\n";
        loaded_reverse.insert(it->first);
      }
      assert(it->second > 0);
      if (it->second == 1)
        ost << "a" << qss->qss_idx << "_0 += reverse_" << it->first->idx << ";\n";
      else
        ost << "a" << qss->qss_idx << "_0 = __fma_rn(" << double(it->second) << ",reverse_"
            << it->first->idx << ",a" << qss->qss_idx << "_0);\n";
    }
    for (std::set<Reaction*>::const_iterator it = qss->forward_denom.begin();
          it != qss->forward_denom.end(); it++)
    {
      int loc = chem_analyzer->find_forward_location(*it, pass);
      if (loc == -1)
        continue;
      if (loaded_forward.find(*it) == loaded_forward.end())
      {
        ost << REAL << " forward_" << (*it)->idx << " = rrs[" << loc << "][tid];\n";
        loaded_forward.insert(*it);
      }
      ost << "denom_" << qss->qss_idx << " += forward_" << (*it)->idx << ";\n";
    }
    for (std::set<Reaction*>::const_iterator it = qss->backward_denom.begin();
          it != qss->backward_denom.end(); it++)
    {
      int loc = chem_analyzer->find_reverse_location(*it, pass);
      if (loc == -1)
        continue;
      if (loaded_reverse.find(*it) == loaded_reverse.end())
      {
        ost << REAL << " reverse_" << (*it)->idx << " = rrs[" << loc << "][tid];\n";
        loaded_reverse.insert(*it);
      }
      ost << "denom_" << qss->qss_idx << " += reverse_" << (*it)->idx << ";\n";
    }
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = qss->forward_contributions.begin();
          it != qss->forward_contributions.end(); it++)
    {
      int loc = chem_analyzer->find_forward_location(it->first.second, pass);
      if (loc == -1)
        continue;
      if (loaded_forward.find(it->first.second) == loaded_forward.end())
      {
        ost << REAL << " forward_" << it->first.second->idx << " = rrs[" << loc << "][tid];\n";
        loaded_forward.insert(it->first.second);
      }
      assert(it->second > 0);
      if (it->second == 1)
        ost << "a" << qss->qss_idx << "_" << it->first.first << " += forward_" << it->first.second->idx << ";\n";
      else
        ost << "a" << qss->qss_idx << "_" << it->first.first << " = __fma_rn(" << double(it->second) << ", forward_"
            << it->first.second->idx << ", a" << qss->qss_idx << "_" << it->first.first << ");\n";
    }
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = qss->backward_contributions.begin();
          it != qss->backward_contributions.end(); it++)
    {
      int loc = chem_analyzer->find_reverse_location(it->first.second, pass);
      if (loc == -1)
        continue;
      if (loaded_reverse.find(it->first.second) == loaded_reverse.end())
      {
        ost << REAL << " reverse_" << it->first.second->idx << " = rrs[" << loc << "][tid];\n";
        loaded_reverse.insert(it->first.second);
      }
      assert(it->second > 0);
      if (it->second == 1)
        ost << "a" << qss->qss_idx << "_" << it->first.first << " += reverse_" << it->first.second->idx << ";\n";
      else
        ost << "a" << qss->qss_idx << "_" << it->first.first << " = __fma_rn(" << double(it->second) << ", reverse_"
            << it->first.second->idx << ", a" << qss->qss_idx << "_" << it->first.first << ");\n";
    }
  }
}

void TranslationUnit::emit_multi_warp_spec_qssa_scaling(CodeOutStream &ost, QSSASharedMemoryStore &store, int wid)
{
  Warp *warp = qssa_warps[wid];
  // First emit the reduction which scale by denominator
#if 0
  for (std::vector<QSS*>::const_iterator it = warp->qss_specs.begin();
        it != warp->qss_specs.end(); it++)
  {
    (*it)->emit_multi_reductions(ost);
  }
#endif
  ost << REAL << " den;\n"; // forward declaration for denominator computations
  // Schedule QSS specs which have all their dependences satisfied
  // In order to avoid deadlock we employ the following strategy:
  // Use the names for the named barriers to enforce a global ordering
  // on named barriers.  Within a warp, all operations which arrive
  // at a higher named barrier, must come before a wait for a lower 
  // named barrier in the same warp (this is because we assign named barriers
  // using backward BFS when assigning QSS nodes to warps).  This allows us to 
  // lift arrive nodes up the list and to reorder waits, but never to move an
  // arrive below a wait operation with a smaller named barrier.
  {
    std::set<QSS*> scheduled;
    std::set<int> performed_barriers;
    while (scheduled.size() < warp->qss_specs.size())
    {
      QSS *next = NULL;
      bool kills_barrier = false;
      for (unsigned spec_idx = 0; spec_idx < warp->qss_specs.size(); spec_idx++)
      {
        QSS *spec = warp->qss_specs[spec_idx];
        if (scheduled.find(spec) != scheduled.end())
          continue;
        bool satisfied = true;
        for (std::set<QSS*>::const_iterator it = spec->inputs.begin();
              satisfied && (it != spec->inputs.end()); it++)
        {
          if (scheduled.find(*it) != scheduled.end())
            continue;
          else if (spec->input_barriers.find(*it) != spec->input_barriers.end())
          {
            // Check to see if there are any output barriers larger than
            // the needed input barrier that have yet to be scheduled
            int wait_barrier = spec->input_barriers[*it];
            for (std::map<QSS*,int>::const_iterator bar_it = spec->output_barriers.begin();
                  bar_it != spec->output_barriers.end(); bar_it++)
            {
              if (performed_barriers.find(bar_it->second) !=
                  performed_barriers.end())
                continue;
              // Otherwise check to see if it is larger than the barrier we need
              assert(bar_it->second != wait_barrier);
              if (bar_it->second > wait_barrier)
              {
                satisfied = false;
                break;
              }
            }
          }
          else
            satisfied = false;
        }
        if (!satisfied)
          continue;
        if (next == NULL)
        {
          kills_barrier = !spec->output_barriers.empty();
          next = spec;
          continue;
        }
        // Give preference to things that are ready that
        // kill barriers
        if (!kills_barrier && !spec->output_barriers.empty())
        {
          kills_barrier = true;
          next = spec;
        }
      }
      assert(next != NULL);
      scheduled.insert(next);
      ost << "// QSS " << next->qss_idx << "\n";
      // Wait for any barriers
      for (std::map<QSS*,int>::const_iterator it = next->input_barriers.begin();
            it != next->input_barriers.end(); it++)
      {
        emit_barrier(ost, it->second, 2/*two warps*/, true/*wait*/);
      }
      // Emit the code for the operation
      if (qssa_in_shared)
      {
        for (std::vector<Statement*>::const_iterator it = next->statements.begin();
              it != next->statements.end(); it++)
        {
          (*it)->emit_warp_spec_shared_code(ost, store, warp);
        }
      }
      else
      {
        for (std::vector<Statement*>::const_iterator it = next->statements.begin();
              it != next->statements.end(); it++)
        {
          (*it)->emit_warp_spec_code(ost, warp->warp_emitted, store, warp);
        }
        // Emit the stores for any shared variables, do this in reverse order
        // so we only need to store the last time a value is written
        {
          std::set<std::pair<int,int> > emitted_stores;
          if (store.has_value(std::pair<int,int>(next->qss_idx,0)))
          {
            int loc = store.find_value(std::pair<int,int>(next->qss_idx,0));
            ost << "scratch[" << loc << "][tid] = a" << next->qss_idx << "_0;\n";
            emitted_stores.insert(std::pair<int,int>(next->qss_idx,0));
          }
          for (std::set<int>::const_iterator it = next->member_species.begin();
                it != next->member_species.end(); it++)
          {
            if (store.has_value(std::pair<int,int>(next->qss_idx,*it)))
            {
              int loc = store.find_value(std::pair<int,int>(next->qss_idx,*it));
              ost << "scratch[" << loc << "][tid] = a" << next->qss_idx << "_" << *it << ";\n";
              emitted_stores.insert(std::pair<int,int>(next->qss_idx,*it));
            }
          }
          for (std::vector<Statement*>::const_reverse_iterator it = 
                next->statements.rbegin(); it != next->statements.rend(); it++)
          {
            (*it)->emit_warp_spec_stores(ost, emitted_stores, store);
          }
        }
      }
      // Arrive at any barriers we've satisfied
      for (std::map<QSS*,int>::const_iterator it = next->output_barriers.begin();
            it != next->output_barriers.end(); it++)
      {
        emit_barrier(ost, it->second, 2/*two warps*/, false/*wait*/);
        performed_barriers.insert(it->second);
      }
    }
  }
  // Now do the xstat computations which go the opposite direction in terms of
  // satisfying dependences
  // Use the same named barrier approach as above to avoid deadlock bu
  // with the polarity of larger and smaller named barrier reversed
  {
    std::set<QSS*> scheduled;
    std::set<int> performed_barriers;
    while (scheduled.size() < warp->qss_specs.size())
    {
      QSS *next = NULL;
      bool kills_barrier = false;
      for (std::vector<QSS*>::const_iterator it = warp->qss_specs.begin();
            it != warp->qss_specs.end(); it++)
      for (unsigned spec_idx = 0; spec_idx < warp->qss_specs.size(); spec_idx++)
      {
        QSS *spec = warp->qss_specs[spec_idx];
        if (scheduled.find(spec) != scheduled.end())
          continue;
        bool satisfied = true;
        for (std::set<QSS*>::const_iterator it = spec->outputs.begin();
              satisfied && (it != spec->outputs.end()); it++)
        {
          if (scheduled.find(*it) != scheduled.end())
            continue;
          else if (spec->xstat_input_barriers.find(*it) != spec->xstat_input_barriers.end())
          {
            // Check to see if there are any output barriers larger than
            // the needed input barrier that have yet to be scheduled
            int wait_barrier = spec->xstat_input_barriers[*it];
            for (std::map<QSS*,int>::const_iterator bar_it = spec->xstat_output_barriers.begin();
                  bar_it != spec->xstat_output_barriers.end(); bar_it++)
            {
              if (performed_barriers.find(bar_it->second) !=
                  performed_barriers.end())
                continue;
              // Otherwise check to see if it is smaller than the barrier we need
              assert(bar_it->second != wait_barrier);
              if (bar_it->second < wait_barrier)
              {
                satisfied = false;
                break;
              }
            }
          }
          else
            satisfied = false;
        }
        if (!satisfied)
          continue;
        if (next == NULL)
        {
          kills_barrier = !spec->xstat_output_barriers.empty();
          next = spec;
          continue;
        }
        // Give preference to things that are ready that
        // kill barriers
        if (!kills_barrier && !spec->xstat_output_barriers.empty())
        {
          kills_barrier = true;
          next = spec;
        }
      }
      assert(next != NULL);
      scheduled.insert(next);
      ost << "// Xstat " << next->qss_idx << "\n";
      if (!qssa_in_shared)
        ost << REAL << " xq_" << next->qss_idx << ";\n";
      // Wait for any barriers
      for (std::map<QSS*,int>::const_iterator it = next->xstat_input_barriers.begin();
            it != next->xstat_input_barriers.end(); it++)
      {
        emit_barrier(ost, it->second, 2/*two warps*/, true/*wait*/);
      }
      // Emit the code for the operation
      if (qssa_in_shared)
        next->xstat->emit_warp_spec_shared_code(ost, store, warp);
      else
        next->xstat->emit_warp_spec_code(ost, warp->warp_emitted, store, warp);
      // Arrive at any barriers we've satisfied
      for (std::map<QSS*,int>::const_iterator it = next->xstat_output_barriers.begin();
            it != next->xstat_output_barriers.end(); it++)
      {
        emit_barrier(ost, it->second, 2/*two warps*/, false/*wait*/);
        performed_barriers.insert(it->second);
      }
    }
  }
}

void TranslationUnit::emit_multi_inline_qssa_code(CodeOutStream &ost, int shared_size)
{
  QSSASharedMemoryStore *store = configure_warp_spec_qssa(shared_size, reac_offset);
  std::map<std::pair<int,int>,int> a_locations;
  // Compute the locations where each warp will store it's values
  for (unsigned wid = 0; wid < chem_warps.size(); wid++)
  {
    for (unsigned idx = 0; idx < chem_warps[wid]->qss_specs.size(); idx++)
    {
      QSS *qss = chem_warps[wid]->qss_specs[idx];
      std::pair<int,int> zero_key(qss->qss_idx,0);
      if (qssa_in_shared)
      {
        store->allocate_value(zero_key);
        int zero_loc = store->find_value(zero_key);
        a_locations[zero_key] = zero_loc;
      }
      else
      {
        if (a_locations.size() >= unsigned(shared_size)) {
          if (!spill_qssa) {
            fprintf(stderr,"Out of shared memory for QSSA a-value exchange. You "
                    "can enable spilling with the --spill-qssa flag.\n");
            exit(1);
          } else {
            int zero_loc = qssa_spill_size++;
            a_locations[zero_key] = -zero_loc;
          }
        } else {
          int zero_loc = reac_offset + a_locations.size();
          a_locations[zero_key] = zero_loc;
        }
      }
      for (std::set<int>::const_iterator it = qss->member_species.begin();
            it != qss->member_species.end(); it++)
      {
        std::pair<int,int> key(qss->qss_idx,*it);
        if (qssa_in_shared)
        {
          store->allocate_value(key);
          int a_loc = store->find_value(key);
          a_locations[key] = a_loc;
        }
        else
        {
          if (a_locations.size() >= unsigned(shared_size)) {
            if (!spill_qssa) {
              fprintf(stderr,"Out of shared memory for QSSA a-value exchange. You "
                      "can enable spilling with the --spill-qssa flag.\n");
              exit(1);
            } else {
              int a_loc = qssa_spill_size++;
              a_locations[key] = -a_loc;
            }
          } else {
            int a_loc = reac_offset + a_locations.size();
            a_locations[key] = a_loc;
          }
        }
      }
    }
  }
  for (unsigned wid = 0; wid < chem_warps.size(); wid++)
  {
    if (wid == 0)
      ost << "if (wid == " << wid << ")\n";
    else
      ost << "else if (wid == " << wid << ")\n";
    PairDelim warp_pair(ost);
    // Emit the stores for this location
    int a_index = 0;
    int d_index = 0;
    for (unsigned idx = 0; idx < chem_warps[wid]->qss_specs.size(); idx++)
    {
      QSS *qss = chem_warps[wid]->qss_specs[idx];
      ost << "denoms[" << d_index << "] = 1.0/denoms[" << d_index << "];\n";
      // Zero value first
      std::pair<int,int> zero_key(qss->qss_idx,0);
      int z_loc = a_locations[zero_key];
      if (z_loc > 0)
        ost << "scratch[" << z_loc << "][tid] = avals[" << a_index << "]"
            << "*denoms[" << d_index << "];\n";
      else
        ost << "qssa_g[smid*" << qssa_spill_size << "+" << -z_loc << "][tid] = avals["
            << a_index << "]*denoms[" << d_index << "];\n";
      a_index++;
      for (std::set<int>::const_iterator it = qss->member_species.begin();
            it != qss->member_species.end(); it++)
      {
        std::pair<int,int> key(qss->qss_idx,*it);
        int a_loc = a_locations[key];
        if (a_loc > 0)
          ost << "scratch[" << a_loc << "][tid] = avals[" << a_index << "]"
              << "*denoms[" << d_index << "];\n";
        else
          ost << "qssa_g[smid*" << qssa_spill_size << "+" << -a_loc << "][tid] = avals["
              << a_index << "]*denoms[" << d_index << "];\n";
        a_index++;
      }
      d_index++;
    }
    if (wid >= (chem_warps.size()-qss_warps))
    {
      int qss_id = wid - (chem_warps.size()-qss_warps);
      assert(qss_id >= 0);
      // This is a warp being used for QSSA
      // Wait for the value
      emit_barrier(ost, 1, reac_warps, true/*blocking*/);
      if (!qssa_in_shared)
      {
        for (unsigned idx = 0; idx < qssa_warps[qss_id]->qss_specs.size(); idx++)
        {
          QSS *qss = qssa_warps[qss_id]->qss_specs[idx];
          std::pair<int,int> zero_key(qss->qss_idx,0);
          assert(a_locations.find(zero_key) != a_locations.end());
          int z_loc = a_locations[zero_key];
          if (z_loc > 0)
            ost << REAL << " a" << qss->qss_idx << "_0 = "
                << "scratch[" << a_locations[zero_key] << "][tid];\n";
          else
            ost << REAL << " a" << qss->qss_idx << "_0 = "
                << "qssa_g[smid*" << qssa_spill_size << "+" << -z_loc << "][tid];\n";
          qssa_warps[qss_id]->warp_emitted.insert(zero_key);
          for (std::set<int>::const_iterator it = qss->member_species.begin();
                it != qss->member_species.end(); it++)
          {
            std::pair<int,int> key(qss->qss_idx, *it);
            assert(a_locations.find(key) != a_locations.end());
            int a_loc = a_locations[key];
            if (a_loc > 0)
              ost << REAL << " a" << qss->qss_idx << "_" << (*it) << " = "
                  << "scratch[" << a_locations[key] << "][tid];\n";
            else
              ost << REAL << " a" << qss->qss_idx << "_" << (*it) << " = "
                  << "qssa_g[smid*" << qssa_spill_size << "+" << -a_loc << "][tid];\n";
            qssa_warps[qss_id]->warp_emitted.insert(key);
          }
        }
      }
      // Now we can emit the code for doing the computation
      emit_multi_warp_spec_qssa_scaling(ost, *store, qss_id);
    }
    else
    {
      // Normal reaction warp, indicate we've arrived and move on
      emit_barrier(ost, 1, reac_warps, false/*blocking*/);
    }
  }
}

void TranslationUnit::emit_small_gpu_getrates(CodeOutStream &ost)
{
  if (qss_warps == 0)
    this->qss_warps = 1; // default this to one
  if (target_ctas == 0)
    this->target_ctas = 1;
  // Quick check that you won't get the asked for number of CTAs
  // because of total warp count
  {
    int requested_warps = (reac_warps + qss_warps)*target_ctas;
    if (k20 && (requested_warps > 64))
    {
      fprintf(stderr,"WARNING: You've requested %d chemistry warps and "
                     "%d QSSA warps for %d CTAs/SM for a total of %d warps "
                     "which is more than Kepler's max of 64 per SM.\n",
                     reac_warps, qss_warps, target_ctas, requested_warps);
    }
    if (!k20 && (requested_warps > 48))
    {
      fprintf(stderr,"WARNING: You've requested %d chemistry warps and "
                     "%d QSSA warps for %d CTAs/SM for a total of %d warps "
                     "which is more than Fermi's max of 48 warps per SM.\n",
                     reac_warps, qss_warps, target_ctas, requested_warps);
    }
  }
  int qssa_levels = multi_assign_reactions_to_warps();

  // Use the target number of CTAs to figure out how much shared memory
  // we get.  Then use that to compute the number of passes.
  int cta_values_per_point;
  int chem_passes, chem_levels_per_pass;
  int qssa_passes, qssa_levels_per_pass;
  int stif_passes, stif_levels_per_pass;
  int output_passes, output_levels_per_pass;
  int shared_offset;
  {
    this->points_per_cta = this->warp_size;
    int available_shared = MAX_SHARED;
    int total_warps = reac_warps * (32/warp_size);
    // If this is fermi, count the space needed for exchanging data
    // otherwise we can use the full shared memory
    if (!k20)
    {
      // Real mirror
      available_shared -= (total_warps*sizeof(REAL_TYPE));
      // Int mirror
      available_shared -= (total_warps*sizeof(INT_TYPE));
      // Reduction mirror
      available_shared -= ((reac_warps + ((chem == NULL) ? 0 : 1))*warp_size*sizeof(REAL_TYPE));
    }
    int cta_shared = available_shared/target_ctas;
    cta_values_per_point = cta_shared/(sizeof(REAL_TYPE)*warp_size);
    assert(!use_indexing);
    shared_offset = ordered_species.size() + third_bodies.size() +
                        gibbs_species.size() + 
                        2/*unity and zero rrs*/;
    // need at least 2 values per warp for forward and reverse reaction rates
    int needed_values = shared_offset + 2*total_warps; // need at least 2 values per warp
    if (needed_values > cta_values_per_point)
    {
      fprintf(stderr,"Too many warps or too many target CTAs specified for shared memory!\n");
      fprintf(stderr,"Need %d values per point, but only have %d values available.\n",
                needed_values, cta_values_per_point);
      fprintf(stderr,"Reduce either the requested number of reac warps or the requested "
                     "target number of CTAs.\n");
      exit(1);
    }
    int chem_pass_values = cta_values_per_point - (shared_offset+2);
    compute_pass_information(chem_pass_values,total_warps,chem_warps[0]->reacs.size(),
                             2, chem_passes, chem_levels_per_pass);
    compute_pass_information(chem_pass_values,total_warps,qssa_levels,
                             2, qssa_passes, qssa_levels_per_pass);
    compute_pass_information(cta_values_per_point-2,total_warps,chem_warps[0]->reacs.size(),
                             2, stif_passes, stif_levels_per_pass);
    compute_pass_information(cta_values_per_point-2,total_warps,chem_warps[0]->reacs.size(),
                             1, output_passes, output_levels_per_pass);
    fprintf(stdout,"INFO: Small Multi-Pass Warp-Specialized getrates stats\n");
    fprintf(stdout,"  Points-per-CTA: %d\n", points_per_cta);
    fprintf(stdout,"  Chemistry Warps: %d\n", reac_warps);
    fprintf(stdout,"  QSSA Warps: %d\n", qss_warps);
    fprintf(stdout,"  Target CTAs/SM: %d\n", target_ctas);
    fprintf(stdout,"  Shared-Memory Usage per CTA: %d\n", cta_shared);
    fprintf(stdout,"  Other shared values: %d\n", shared_offset);
    fprintf(stdout,"  QSSA passes: %d\n", chem_passes);
    fprintf(stdout,"  Stiffness passes: %d\n", stif_passes);
    fprintf(stdout,"  Output passes: %d\n", output_passes);
  }

  // Build the shared memory analyzer
  multi_construct_shared_memory_analyzers(chem_levels_per_pass, chem_passes,
                                          qssa_levels_per_pass, qssa_passes,
                                          stif_levels_per_pass, stif_passes,
                                          output_levels_per_pass, output_passes,
                                          shared_offset);

  // Before emitting any code, set the values for the identity locations
  this->thb_offset = ordered_species.size();
  this->gibbs_offset = thb_offset + third_bodies.size();
  this->reac_offset = gibbs_offset + gibbs_species.size();
  this->shared_zero = cta_values_per_point-2;
  this->shared_unity = cta_values_per_point-1;

  const char *temp_chem_name = "__temp_chem__.cu";
  {
    CodeOutStream temp_ost(temp_chem_name, true, 80);  
    emit_small_chem_code(temp_ost, chem_passes, chem_levels_per_pass,
                          qssa_passes, qssa_levels_per_pass, qssa_levels,
                          stif_passes, stif_levels_per_pass, output_passes,
                          output_levels_per_pass, shared_offset);
  }

  emit_small_full_code(ost, temp_chem_name, cta_values_per_point);

  // Cleanup when we are done
  assert(system("rm -f __temp_chem__.cu") == 0);
}

void TranslationUnit::emit_small_chem_code(CodeOutStream &ost, int chem_passes, int chem_levels_per_pass,
                                                int qssa_passes, int qssa_levels_per_pass, int qssa_levels,
                                                int stif_passes, int stif_levels_per_pass,
                                                int output_passes, int output_levels_per_pass,
                                                int shared_offset)
{
  ost << "// Main loop\n";
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim for_pair(ost);
  // Need a barrier here for anything in initialization or
  // from previous iterations
  emit_barrier(ost, 0, reac_warps, true/*wait*/);
  // Emit the code for computing mole fractions, gibbs species, etc
  // This version works for all effective warp sizes
  emit_multi_init_chem_code(ost);
  // Emit the reaction code
  for (int pass = 0; pass < chem_passes; pass++)
  {
    int start_level = pass*chem_levels_per_pass;
    int stop_level = (pass+1)*chem_levels_per_pass;//exclusive
    // clamp the max level if necessary
    if (stop_level > int(chem_warps[0]->reacs.size()))
      stop_level = chem_warps[0]->reacs.size();
    bool qssa_pass = (chem != NULL) && (pass < qssa_passes);
    emit_small_chem_pass(ost, start_level, stop_level, pass, qssa_pass);
    if ((chem != NULL) && (pass == (qssa_passes-1)))
      emit_small_inline_qssa_code(ost,qssa_levels_per_pass*2*chem_warps.size());
  }
  // Do QSSA and Stiffness if necessary
  if (chem != NULL)
  {
    // Scale qssa first
    emit_small_qssa_scaling(ost, qssa_levels);
    // Now do stiffness
    emit_small_init_stif_code(ost);
    for (int pass = 0; pass < stif_passes; pass++)
    {
      int start_level = pass*stif_levels_per_pass;
      int stop_level = (pass+1)*stif_levels_per_pass;//exclusive
      // clamp the max level if necessary
      if (stop_level > int(chem_warps[0]->reacs.size()))
        stop_level = chem_warps[0]->reacs.size(); 
      emit_small_stif_pass(ost, pass, start_level, stop_level);
    }
    emit_small_stif_scaling(ost);
  }
  emit_small_init_out_code(ost);
  for (int pass = 0; pass < output_passes; pass++)
  {
    int start_level = pass*output_levels_per_pass;
    int stop_level = (pass+1)*output_levels_per_pass;//exclusive
    // clamp the max level if necessary
    if (stop_level > int(chem_warps[0]->reacs.size()))
      stop_level = chem_warps[0]->reacs.size();
    emit_small_out_pass(ost, pass, start_level, stop_level);
  }
  emit_small_output(ost);
  // Finally update the pointers
  {
    PairDelim ptr_pair(ost);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << AVMOLWT_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
    if (chem != NULL)
      ost << DIFFUSION_ARRAY << " += slice_stride;\n";
    ost << WDOT_ARRAY << " += slice_stride;\n";
  }
}

void TranslationUnit::emit_small_full_code(CodeOutStream &ost,
                                           const char *temp_chem_name,
                                           int cta_shared_size)
{
  emit_warp_specialized_constants(ost);

  emit_gpu_getrates_declaration(ost);
  ost << "\n";

  PairDelim func_pair(ost);
  ost << "__shared__ volatile " << REAL << " scratch[" << cta_shared_size << "][" << points_per_cta << "];\n";
  
  if (!k20)
  {
    assert((32 % points_per_cta) == 0);
    int total_warps = (reac_warps + ((chem == NULL) ? 0 : 1)) * (32/points_per_cta);
    ost << "__shared__ volatile " << REAL << " real_mirror[" << total_warps << "];\n";
    ost << "__shared__ volatile " << INT << " int_mirror[" << total_warps << "];\n";
    ost << "__shared__ volatile " << REAL << " reduc_mirror[" << (reac_warps+((chem == NULL) ? 0 : 1)) << "][" << (points_per_cta) << "];\n";
  }
  int power = int(log(points_per_cta)/log(2));
  char mask[8];
  sprintf(mask,"0x%x",points_per_cta-1);

  ost << "const " << INT << " wid = threadIdx.x >> " << power << ";\n";
  ost << "const " << INT << " tid = threadIdx.x & " << mask << ";\n";

  emit_getrates_numerical_constants(ost);

  // Scale the pointers
  {
    PairDelim off_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.x*"
        << points_per_cta << " + tid);\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << AVMOLWT_ARRAY << " += offset;\n";
    ost << DIFFUSION_ARRAY << " += offset;\n";
    ost << WDOT_ARRAY << " += offset;\n";
  }

  ost << "if (wid == 0)\n";
  {
    PairDelim identity_pair(ost);
    ost << "scratch[" << shared_zero << "][tid] = 0.0;\n";
    ost << "scratch[" << shared_unity << "][tid] = 0.0;\n";
  }

  // Emit the code for loading the constants that we need
  assert((chem_warps[0]->int_constants[0].size()%points_per_cta) == 0);
  const unsigned num_index_vals = chem_warps[0]->int_constants[0].size()/points_per_cta;
  if (num_index_vals > 0)
  {
    ost << INT << " index_constants[" << (num_index_vals) << "];\n";
    // Emit the code to load the constants
    for (unsigned idx = 0; idx < num_index_vals; idx++)
    {
      char src_index[64];
      sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*chem_warps.size()*points_per_cta);
      char dst_index[64];
      sprintf(dst_index,"%d",idx);
      emit_cuda_int_load(ost, "index_constants", dst_index, "chem_index", src_index, k20);
    }
  }
  assert((chem_warps[0]->real_constants[0].size()%points_per_cta) == 0);
  const unsigned num_real_vals = chem_warps[0]->real_constants[0].size()/points_per_cta;
  if (num_real_vals > 0)
  {
    ost << REAL << " real_constants[" << (num_real_vals) << "];\n";
    for (unsigned idx = 0; idx < num_real_vals; idx++)
    {
      char src_index[64];
      sprintf(src_index,"(wid*%d+tid+%ld)",points_per_cta,idx*chem_warps.size()*points_per_cta);
      char dst_index[64];
      sprintf(dst_index,"%d",idx);
      emit_cuda_load(ost, "real_constants", dst_index, "chem_real", src_index, k20);
    }
  }

  // Then copy over the code from the temporary file
  FILE *temp_file = fopen(temp_chem_name,"r");
  assert(temp_file != NULL);
  char c = fgetc(temp_file);
  while (c != EOF)
  {
    ost << c;
    c = fgetc(temp_file);
  }
  assert(fclose(temp_file) == 0);
}

void TranslationUnit::emit_small_chem_pass(CodeOutStream &ost, 
                                           int start_level, int stop_level,
                                           int pass, bool qssa_pass)
{
  for (int level = start_level; level < stop_level; level++)
  {
    // Little hack to keep the compiler from doing dumb things
    if (level == 0)
      ost << REAL << " rr_f_0 = 0.0, rr_r_0 = 0.0;\n";
    else
      ost << REAL << " rr_f_" << level << " = rr_f_" << (level-1)
          << ", rr_r_" << level << " = rr_r_" << (level-1) << ";\n";
    // We can use this to generate code for each of the warps
    emit_multi_chem_level(ost, level);
  } 
  if (qssa_pass)
  {
    // Write our values into shared memory
    for (int level = start_level; level < stop_level; level++)
    {
      ost << "scratch[wid+" << (reac_offset+(chem_warps.size()*(2*(level-start_level)))) << "][tid] = rr_f_" << level << ";\n";
      ost << "scratch[wid+" << (reac_offset+(chem_warps.size()*(2*(level-start_level)+1))) << "][tid] = rr_r_" << level << ";\n";
    }
    // Indicate that we are done writing
    emit_barrier(ost, 0, reac_warps, true/*blocing*/);
    // Iterate over the chem warps in blocks of full warps
    for (unsigned wid = 0; wid < chem_warps.size(); (wid+=(32/warp_size)))
    {
      if (wid == 0)
        ost << "if (wid < " << (32/warp_size) << ")\n";
      else
        ost << "else if (wid < " << (wid+(32/warp_size)) << ")\n";
      PairDelim warp_pair(ost);
      ost << INT << " index;\n";
      int aindex = 0;
      int dindex = 0;
      for (std::vector<QSS*>::const_iterator qit = chem_warps[wid]->qss_specs.begin();
            qit != chem_warps[wid]->qss_specs.end(); qit++)
      {
        QSS *qss = *qit;
        // Zero value first
        // Compute the indexes that need loading
        std::vector<int> indexes; 
        for (std::map<Reaction*,int>::const_iterator it = qss->forward_zeros.begin();
              it != qss->forward_zeros.end(); it++)
        {
          int loc = chem_analyzer->find_forward_location(it->first, pass);
          if (loc == -1)
            continue;
          assert(it->second > 0);
          for (int i = 0; i < it->second; i++)
            indexes.push_back(loc);
        }
        for (std::map<Reaction*,int>::const_iterator it = qss->backward_zeros.begin();
              it != qss->backward_zeros.end(); it++)
        {
          int loc = chem_analyzer->find_reverse_location(it->first, pass);
          if (loc == -1)
            continue;
          assert(it->second > 0);
          for (int i = 0; i < it->second; i++)
            indexes.push_back(loc);
        }
        // Assign the indexes to warps and emit the code
        {
          std::vector<std::vector<int> > distribution(32/warp_size);
          int depth = small_distribute_indexes(indexes, distribution, true/*pad with zero*/);
          for (int idx = 0; idx < depth; idx++)
          {
            int global_idx = chem_warps[wid]->add_index(distribution[0][idx], 0/*version*/);
            // Add it to the other warps as well
            for (int w = 1; w < (32/warp_size); w++)
            {
              int gidx = chem_warps[wid+w]->add_index(distribution[w][idx], 0/*version*/);
              assert(gidx == global_idx);
            }
            int index_offset = global_idx/warp_size;
            int index_thread = global_idx%warp_size;
            if (k20)
            {
              ost << "index = __shfl(index_constants[" << index_offset << "],"
                  << index_thread << "," << warp_size << ");\n";
            }
            else
            {
              ost << "if (tid == " << index_thread << ")\n";
              ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
              ost << "index = int_mirror[wid];\n";
            }
            // Now we can do the load and add it to our value
            ost << "avals[" << aindex << "] += scratch[index][tid];\n";
          }
        }
        aindex++;
        // Now the other a values
        for (std::set<int>::const_iterator ait = qss->member_species.begin();
              ait != qss->member_species.end(); ait++)
        {
          indexes.clear();
          for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = 
                qss->forward_contributions.begin(); it !=
                qss->forward_contributions.end(); it++)
          {
            if (it->first.first != *ait)
              continue;
            int loc = chem_analyzer->find_forward_location(it->first.second, pass);
            if (loc == -1)
              continue;
            assert(it->second > 0);
            for (int i = 0; i < it->second; i++)
              indexes.push_back(loc);
          }
          for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = 
                qss->backward_contributions.begin(); it !=
                qss->backward_contributions.end(); it++)
          {
            if (it->first.first != *ait)
              continue;
            int loc = chem_analyzer->find_reverse_location(it->first.second, pass);
            if (loc == -1)
              continue;
            assert(it->second > 0);
            for (int i = 0; i < it->second; i++)
              indexes.push_back(loc);
          }
          std::vector<std::vector<int> > distribution(32/warp_size);
          int depth = small_distribute_indexes(indexes, distribution, true/*pad with zero*/);
          for (int idx = 0; idx < depth; idx++)
          {
            int global_idx = chem_warps[wid]->add_index(distribution[0][idx], 0/*version*/);
            // Add it to the other warps as well
            for (int w = 1; w < (32/warp_size); w++)
            {
              int gidx = chem_warps[wid+w]->add_index(distribution[w][idx], 0/*version*/);
              assert(gidx == global_idx);
            }
            int index_offset = global_idx/warp_size;
            int index_thread = global_idx%warp_size;
            if (k20)
            {
              ost << "index = __shfl(index_constants[" << index_offset << "],"
                  << index_thread << "," << warp_size << ");\n";
            }
            else
            {
              ost << "if (tid == " << index_thread << ")\n";
              ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
              ost << "index = int_mirror[wid];\n";
            }
            // Now we can do the load and add it to our value
            ost << "avals[" << aindex << "] += scratch[index][tid];\n";
          }
          aindex++;
        }
        // Then the denominator
        indexes.clear();
        for (std::set<Reaction*>::const_iterator it = qss->forward_denom.begin();
              it != qss->forward_denom.end(); it++)
        {
          int loc = chem_analyzer->find_forward_location(*it, pass);
          if (loc == -1)
            continue;
          indexes.push_back(loc);
        }
        for (std::set<Reaction*>::const_iterator it = qss->backward_denom.begin();
              it != qss->backward_denom.end(); it++)
        {
          int loc = chem_analyzer->find_reverse_location(*it, pass);
          if (loc == -1)
            continue;
          indexes.push_back(loc);
        }
        // Assign the indexes to warps and emit the code
        {
          std::vector<std::vector<int> > distribution(32/warp_size);
          int depth = small_distribute_indexes(indexes, distribution, true/*pad with zero*/);
          for (int idx = 0; idx < depth; idx++)
          {
            int global_idx = chem_warps[wid]->add_index(distribution[0][idx], 0/*version*/);
            // Add it to the other warps as well
            for (int w = 1; w < (32/warp_size); w++)
            {
              int gidx = chem_warps[wid+w]->add_index(distribution[w][idx], 0/*version*/);
              assert(gidx == global_idx);
            }
            int index_offset = global_idx/warp_size;
            int index_thread = global_idx%warp_size;
            if (k20)
            {
              ost << "index = __shfl(index_constants[" << index_offset << "],"
                  << index_thread << "," << warp_size << ");\n";
            }
            else
            {
              ost << "if (tid == " << index_thread << ")\n";
              ost << "  int_mirror[wid] = index_constants[" << index_offset << "];\n";
              ost << "index = int_mirror[wid];\n";
            }
            // Now we can do the load and add it to our value
            ost << "denoms[" << dindex << "] += scratch[index][tid];\n";
          }
        }
        dindex++;
      }
    }
    // When we're done, emit a barrier to mark we are done reading
    emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  }
}

void TranslationUnit::emit_small_inline_qssa_code(CodeOutStream &ost, int shared_size)
{
  QSSASharedMemoryStore *store = configure_warp_spec_qssa(shared_size, reac_offset);
  std::map<std::pair<int,int>,int> a_locations;
  // Compute the locations where all the values will be stored
  for (unsigned wid = 0; wid < chem_warps.size(); wid+=(32/warp_size))
  {
    for (unsigned idx = 0; idx < chem_warps[wid]->qss_specs.size(); idx++)
    {
      QSS *qss = chem_warps[wid]->qss_specs[idx];
      std::pair<int,int> zero_key(qss->qss_idx,0);
      if (qssa_in_shared)
      {
        store->allocate_value(zero_key);
        int zero_loc = store->find_value(zero_key);
        a_locations[zero_key] = zero_loc;
      }
      else
      {
        int zero_loc = reac_offset + a_locations.size();
        a_locations[zero_key] = zero_loc;
      }
      for (std::set<int>::const_iterator it = qss->member_species.begin();
            it != qss->member_species.end(); it++)
      {
        std::pair<int,int> key(qss->qss_idx,*it);
        if (qssa_in_shared)
        {
          store->allocate_value(key);
          int a_loc = store->find_value(key);
          a_locations[key] = a_loc;
        }
        else
        {
          int a_loc = reac_offset + a_locations.size();
          a_locations[key] = a_loc;
        }
      }
    }
  }
  for (unsigned wid = 0; wid < chem_warps.size(); wid+=(32/warp_size))
  {
    if (wid == 0)
      ost << "if (wid < " << (32/warp_size) << ")\n";
    else
      ost << "else if (wid < " << (wid+(32/warp_size)) << ")\n";
    PairDelim warp_pair(ost);
    ost << INT << " hi_part, lo_part;\n";
    // Emit the stores for this location
    int a_index = 0;
    int d_index = 0;
    char mask[64];
    sprintf(mask,"0x%x",(32/points_per_cta)-1);
    for (unsigned idx = 0; idx < chem_warps[wid]->qss_specs.size(); idx++)
    {
      QSS *qss = chem_warps[wid]->qss_specs[idx];
      // Reduce the denominator first
      if (k20)
      {
        for (int i = points_per_cta; i < 32; i*=2)
        {
          ost << "hi_part = __shfl_xor(__double2hiint(denoms[" << d_index << "]),"
              << i << ",32);\n";
          ost << "lo_part = __shfl_xor(__double2loint(denoms[" << d_index << "]),"
              << i << ",32);\n";
          ost << "denoms[" << d_index << "] += __hiloint2double(hi_part,lo_part);\n";
        }
      }
      else
      {
        char mask[64];
        sprintf(mask,"0x%x",(32/points_per_cta)-1);
        // Here we use warp-synchronous reductions linearly
        for (int idx = 1; idx < (32/points_per_cta); idx++)
        {
          ost << "if ((wid & " << mask << ") == " << idx << ")\n";
          ost << "  reduc_mirror[0][tid] = denoms[" << d_index << "];\n";
          ost << "if ((wid & " << mask << ") == 0)\n";
          ost << "  denoms[" << d_index << "] += reduc_mirror[0][tid];\n";
        }
        // Then scatter back out so everyone has the same value
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  reduc_mirror[0][tid] = denoms[" << d_index << "];\n";
        ost << "denoms[" << d_index << "] = reduc_mirror[0][tid];\n";
      }
      // Now everybody has the same denominator value, invert it
      ost << "denoms[" << d_index << "] = 1.0/denoms[" << d_index << "];\n";
      // Zero value first
      if (k20)
      {
        // Reduce a value across everyone
        for (int i = points_per_cta; i < 32; i*=2)
        {
          ost << "hi_part = __shfl_xor(__double2hiint(avals[" << a_index << "]),"
              << i << ",32);\n";
          ost << "lo_part = __shfl_xor(__double2loint(avals[" << a_index << "]),"
              << i << ",32);\n";
          ost << "avals[" << a_index << "] += __hiloint2double(hi_part,lo_part);\n";
        }
      }
      else
      {
        for (int idx = 1; idx < (32/points_per_cta); idx++)
        {
          ost << "if ((wid & " << mask << ") == " << idx << ")\n";
          ost << "  reduc_mirror[0][tid] = avals[" << a_index << "];\n";
          ost << "if ((wid & " << mask << ") == 0)\n";
          ost << "  avals[" << a_index << "] += reduc_mirror[0][tid];\n";
        }
        // No need to scatter back out
      }
      // Scale by denominator
      ost << "avals[" << a_index << "] *= denoms[" << d_index << "];\n";
      std::pair<int,int> zero_key(qss->qss_idx,0);
      ost << "if ((wid & " << mask << ") == 0)\n"; 
      ost << "  scratch[" << a_locations[zero_key] << "][tid] = avals[" << a_index << "];\n";
      a_index++;
      // Now do the other avalues
      for (std::set<int>::const_iterator it = qss->member_species.begin();
            it != qss->member_species.end(); it++)
      {
        if (k20)
        {
          // Reduce a value across everyone
          for (int i = points_per_cta; i < 32; i*=2)
          {
            ost << "hi_part = __shfl_xor(__double2hiint(avals[" << a_index << "]),"
                << i << ",32);\n";
            ost << "lo_part = __shfl_xor(__double2loint(avals[" << a_index << "]),"
                << i << ",32);\n";
            ost << "avals[" << a_index << "] += __hiloint2double(hi_part,lo_part);\n";
          }
        }
        else
        {
          for (int idx = 1; idx < (32/points_per_cta); idx++)
          {
            ost << "if ((wid & " << mask << ") == " << idx << ")\n";
            ost << "  reduc_mirror[0][tid] = avals[" << a_index << "];\n";
            ost << "if ((wid & " << mask << ") == 0)\n";
            ost << "  avals[" << a_index << "] += reduc_mirror[0][tid];\n";
          }
          // No need to scatter back out
        }
        // Scale by denominator
        ost << "avals[" << a_index << "] *= denoms[" << d_index << "];\n";
        std::pair<int,int> key(qss->qss_idx,*it);
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  scratch[" << a_locations[key] << "][tid] = avals[" << a_index << "];\n";
        a_index++;
      }
      d_index++;
    }
    if (wid >= (chem_warps.size()-(qss_warps*(32/warp_size))))
    {
      int qss_id = (wid/(32/warp_size)) - (reac_warps-qss_warps);
      assert(qss_id >= 0);
      // This is a warp being used for QSSA, wait for the values
      emit_barrier(ost, 1, reac_warps, true/*blocking*/);
      if (!qssa_in_shared)
      {
        for (unsigned idx = 0; idx < qssa_warps[qss_id]->qss_specs.size(); idx++)
        {
          QSS *qss = qssa_warps[qss_id]->qss_specs[idx];
          std::pair<int,int> zero_key(qss->qss_idx,0);
          assert(a_locations.find(zero_key) != a_locations.end());
          ost << REAL << " a" << qss->qss_idx << "_0 = "
              << "scratch[" << a_locations[zero_key] << "][tid];\n";
          qssa_warps[qss_id]->warp_emitted.insert(zero_key);
          for (std::set<int>::const_iterator it = qss->member_species.begin();
                it != qss->member_species.end(); it++)
          {
            std::pair<int,int> key(qss->qss_idx, *it);
            assert(a_locations.find(key) != a_locations.end());
            ost << REAL << " a" << qss->qss_idx << "_" << (*it) << " = "
                << "scratch[" << a_locations[key] << "][tid];\n";
            qssa_warps[qss_id]->warp_emitted.insert(key);
          }
        }
      }
      emit_multi_warp_spec_qssa_scaling(ost, *store, qss_id); 
    }
    else
    {
      // Normal reaction warp, indicate that we've arrived and move on
      emit_barrier(ost, 1, reac_warps, false/*blocing*/);
    }
  }
}

void TranslationUnit::emit_small_qssa_scaling(CodeOutStream &ost, int qssa_levels)
{
  assert(chem != NULL);
  ost << "/* QSSA scaling */\n";
  // Wait for the QSSA values to be ready
  emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  for (unsigned wid = 0; wid < chem_warps.size(); wid+=(32/warp_size))
  {
    if (wid == 0)
      ost << "if (wid < " << (32/warp_size) << ")\n";
    else
      ost << "if (wid < " << (wid+(32/warp_size)) << ")\n";
    PairDelim warp_pair(ost);
    // Iterate over the levels, at each level see if anyone
    // has a load that they need to do for each of their forward
    // or backward reactions.
    for (int level = 0; level < qssa_levels; level++)
    {
      std::vector<int> forward_idx(32/warp_size,-1);
      std::vector<int> reverse_idx(32/warp_size,-1);
      bool has_forward = false;
      bool has_reverse = false;
      for (int w = 0; w < (32/warp_size); w++)
      {
        if (level < int(chem_warps[wid+w]->reacs.size()))
        {
          Reaction *reac = chem_warps[wid+w]->reacs[level];
          int qss_idx = chem->find_qss_index(reac, true/*forward*/);
          if (qss_idx != -1)
          {
            forward_idx[w] = qss_idx;
            has_forward = true;
          }
          qss_idx = chem->find_qss_index(reac, false/*forward*/);
          if (qss_idx != -1)
          {
            reverse_idx[w] = qss_idx;
            has_reverse = true;
          }
        }
      }
      if (has_forward)
      {
        PairDelim forward_pair(ost);
        // Emit the scaling factor for all the threads in the full warp
        int global_idx;
        if (forward_idx[0] == -1)
          global_idx = chem_warps[wid]->add_index(this->shared_unity, 0/*version*/);
        else
          global_idx = chem_warps[wid]->add_index(forward_idx[0], 0/*version*/);
        for (int w = 1; w < (32/warp_size); w++)
        {
          if (forward_idx[w] == -1)
            assert(global_idx == chem_warps[wid+w]->add_index(this->shared_unity, 0/*version*/));
          else
            assert(global_idx == chem_warps[wid+w]->add_index(forward_idx[w], 0/*version*/));
        }
        int index_offset = global_idx/warp_size;
        int index_thread = global_idx%warp_size;
        ost << INT << " index = __shfl(index_constants[" << index_offset << "],"
            << index_thread << "," << warp_size << ");\n";
        ost << "rr_f_" << level << " *= scratch[index][tid];\n";
      }
      if (has_reverse)
      {
        PairDelim reverse_pair(ost);
        int global_idx;
        if (reverse_idx[0] == -1)
          global_idx = chem_warps[wid]->add_index(this->shared_unity, 0/*version*/);
        else
          global_idx = chem_warps[wid]->add_index(reverse_idx[0], 0/*version*/);
        for (int w = 1; w < (32/warp_size); w++)
        {
          if (reverse_idx[w] == -1)
            assert(global_idx == chem_warps[wid+w]->add_index(this->shared_unity, 0/*version*/));
          else
            assert(global_idx == chem_warps[wid+w]->add_index(reverse_idx[w], 0/*version*/));
        }
        int index_offset = global_idx/warp_size;
        int index_thread = global_idx%warp_size;
        ost << INT << " index = __shfl(index_constants[" << index_offset << "],"
            << index_thread << "," << warp_size << ");\n";
        ost << "rr_r_" << level << " *= scratch[index][tid];\n";
      }
    }
  }
}

void TranslationUnit::emit_small_init_stif_code(CodeOutStream &ost)
{
  assert(chem != NULL);
  ost << "/* Stiffness */\n";
  const unsigned num_stif = chem->stif_operations.size();
  const unsigned num_specs = ((num_stif+reac_warps-1)/reac_warps);
  // Assign stif species to warps so that all sub-warps within
  // a full warp do the same stiff species.  After keeping species
  // balanced across warps, load balance for number of index constants.
  std::set<Stif*> remaining_stif(chem->stif_operations.begin(),
                                 chem->stif_operations.end());
  while (!remaining_stif.empty())
  {
    std::set<int> unassigned_warps;
    for (int idx = 0; idx < reac_warps; idx++)
      unassigned_warps.insert(idx);
    while (!unassigned_warps.empty() && !remaining_stif.empty())
    {
      // Find the largest remaining species
      int max_contributions = -1;
      Stif *max_stif = NULL;
      for (std::set<Stif*>::const_iterator it = remaining_stif.begin();
            it != remaining_stif.end(); it++)
      {
        int contributions = (*it)->forward_d.size() + (*it)->backward_d.size() +
                            (*it)->forward_c.size() + (*it)->backward_c.size();
        if (contributions > max_contributions)
        {
          max_contributions = contributions;
          max_stif = *it;
        }
      }
      assert(max_stif != NULL);
      unsigned min_constants;
      int min_index = -1;
      // Now find the unassigned warp with the fewest index constants
      for (std::set<int>::const_iterator it = unassigned_warps.begin();
            it != unassigned_warps.end(); it++)
      {
        if (min_index == -1)
        {
          min_index = *it;
          min_constants = chem_warps[(*it)*(32/warp_size)]->int_constants[0].size();
        }
        else if (min_constants > chem_warps[(*it)*(32/warp_size)]->int_constants[0].size())
        {
          min_index = *it;
          min_constants = chem_warps[(*it)*(32/warp_size)]->int_constants[0].size();
        }
      }
      assert(min_index >= 0);
      // Now assign the stip operation to all the sub-warps
      for (int w = 0; w < (32/warp_size); w++)
        chem_warps[min_index*(32/warp_size)+w]->add_stif(max_stif); 
      // Now mark that we are done with this stif species
      remaining_stif.erase(max_stif);
      // Mark not to reuse this warp until we've finished this batch
      unassigned_warps.erase(min_index);
    }
  }
  // Emit the loads for diffusion and mole fractions
  for (unsigned spec = 0; spec < num_specs; spec++)
  {
    ost << REAL << " stif_diffusion_" << spec << ";\n";
    ost << REAL << " stif_mole_frac_" << spec << ";\n";
  }
  for (unsigned wid = 0; wid < chem_warps.size(); wid+=(32/warp_size))
  {
    if (wid == 0)
      ost << "if (wid < " << (32/warp_size) << ")\n";
    else
      ost << "else if (wid < " << (wid+(32/warp_size)) << ")\n";
    PairDelim warp_pair(ost);
    for (unsigned idx = 0; idx < num_specs; idx++)
    {
      if (idx < chem_warps[wid]->stifs.size())
      {
        char diff_name[64];
        sprintf(diff_name,"stif_diffusion_%d", idx);
        Stif *stif = chem_warps[wid]->stifs[idx];
        int index = find_ordered_index(stif->species);
        char offset[64];
        sprintf(offset,"%d*spec_stride",index);
        emit_cuda_load(ost,diff_name,DIFFUSION_ARRAY,offset,k20);
        ost << "stif_mole_frac_" << idx << " = scratch[" << index << "][tid];\n";
      }
      else
      {
        ost << "stif_diffusion_" << idx << " = 0.0;\n";
        ost << "stif_mole_frac_" << idx << " = 1.0;\n";
      }
    }
  }
  // Emit declarations for cdot and ddot
  for (unsigned idx = 0; idx < num_specs; idx++)
  {
    ost << REAL << " ddot_" << idx << " = 0.0;\n";
    ost << REAL << " cdot_" << idx << " = 0.0;\n";
  }
}

void TranslationUnit::emit_small_stif_pass(CodeOutStream &ost, int pass,
                                           int start_level, int stop_level)
{
  emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  for (int level = start_level; level < stop_level; level++)
  {
    ost << "scratch[wid+" << (chem_warps.size()*(2*(level-start_level))) 
        << "][tid] = rr_f_" << level << ";\n";
    ost << "scratch[wid+" << (chem_warps.size()*(2*(level-start_level)+1)) 
        << "][tid] = rr_r_" << level << ";\n";   
  }
  // Emit a barrier before reading
  emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  for (unsigned wid = 0; wid < chem_warps.size(); wid+=(32/warp_size))
  {
    if (wid == 0)
      ost << "if (wid < " << (32/warp_size) << ")\n";
    else
      ost << "else if (wid < " << (wid+(32/warp_size)) << ")\n";
    PairDelim warp_pair(ost);
    ost << INT << " index;\n";
    // For each of our species
    for (unsigned spec_idx = 0; spec_idx < chem_warps[wid]->stifs.size(); spec_idx++)
    {
      std::vector<int> ddot_indexes;
      std::vector<int> cdot_indexes;
      Stif *stif = chem_warps[wid]->stifs[spec_idx];
      for (std::map<Reaction*,int>::const_iterator it = stif->forward_d.begin();
            it != stif->forward_d.end(); it++)
      {
        int loc = stif_analyzer->find_forward_location(it->first, pass);
        if (loc == -1)
          continue;
        assert(it->second > 0);
        for (int i = 0; i < it->second; i++)
          ddot_indexes.push_back(loc);
      }
      for (std::map<Reaction*,int>::const_iterator it = stif->backward_d.begin();
            it != stif->backward_d.end(); it++)
      {
        int loc = stif_analyzer->find_reverse_location(it->first, pass);
        if (loc == -1)
          continue;
        assert(it->second > 0);
        for (int i = 0; i < it->second; i++)
          ddot_indexes.push_back(loc);
      }
      if (!ddot_indexes.empty())
      {
        std::vector<std::vector<int> > dist(32/warp_size);
        int depth = small_distribute_indexes(ddot_indexes, dist, true/*pad with zeros*/);
        for (int idx = 0; idx < depth; idx++)
        {
          int global_idx = chem_warps[wid]->add_index(dist[0][idx], 0/*version*/);
          for (int w = 1; w < (32/warp_size); w++)
            assert(global_idx == chem_warps[wid+w]->add_index(dist[w][idx], 0/*version*/));
          int index_offset = global_idx/warp_size;
          int index_thread = global_idx%warp_size;
          ost << "index = __shfl(index_constants[" << index_offset << "],"
              << index_thread << "," << warp_size << ");\n";
          ost << "ddot_" << spec_idx << " += scratch[index][tid];\n";
        }
      }
      for (std::map<Reaction*,int>::const_iterator it = stif->forward_c.begin();
            it != stif->forward_c.end(); it++)
      {
        int loc = stif_analyzer->find_forward_location(it->first, pass);
        if (loc == -1)
          continue;
        assert(it->second > 0);
        for (int i = 0; i < it->second; i++)
          cdot_indexes.push_back(loc);
      }
      for (std::map<Reaction*,int>::const_iterator it = stif->backward_c.begin();
            it != stif->backward_c.end(); it++)
      {
        int loc = stif_analyzer->find_reverse_location(it->first, pass);
        if (loc == -1)
          continue;
        assert(it->second > 0);
        for (int i = 0; i < it->second; i++)
          cdot_indexes.push_back(loc);
      }
      if (!cdot_indexes.empty())
      {
        std::vector<std::vector<int> > dist(32/warp_size);
        int depth = small_distribute_indexes(cdot_indexes, dist, true/*pad with zeros*/);
        for (int idx = 0; idx < depth; idx++)
        {
          int global_idx = chem_warps[wid]->add_index(dist[0][idx], 0/*version*/);
          for (int w = 1; w < (32/warp_size); w++)
            assert(global_idx == chem_warps[wid+w]->add_index(dist[w][idx], 0/*version*/));
          int index_offset = global_idx/warp_size;
          int index_thread = global_idx%warp_size;
          ost << "index = __shfl(index_constants[" << index_offset << "],"
              << index_thread << "," << warp_size << ");\n";
          ost << "cdot_" << spec_idx << " += scratch[index][tid];\n";
        }
      }
    }
  }
}

void TranslationUnit::emit_small_stif_scaling(CodeOutStream &ost)
{
  emit_barrier(ost, 0, reac_warps, true/*blocking*/);
  const unsigned num_stif = chem->stif_operations.size();
  const unsigned num_specs = ((num_stif+reac_warps-1)/reac_warps);
  for (unsigned spec_idx = 0; spec_idx < num_specs; spec_idx++)
  {
    PairDelim spec_pair(ost);
    // First reduce the cdot and ddot value across the sub-warps in a warp
    if (k20)
    {
      if (points_per_cta < 32)
        ost << INT << " hi_part,lo_part;\n";
      for (int i = points_per_cta; i < 32; i*=2)
      {
        ost << "hi_part = __shfl_xor(__double2hiint(ddot_" << spec_idx << ")," << i << ",32);\n"; 
        ost << "lo_part = __shfl_xor(__double2loint(ddot_" << spec_idx << ")," << i << ",32);\n";
        ost << "ddot_" << spec_idx << " += __hiloint2double(hi_part,lo_part);\n";
      }
      for (int i = points_per_cta; i < 32; i*=2)
      {
        ost << "hi_part = __shfl_xor(__double2hiint(cdot_" << spec_idx << ")," << i << ",32);\n"; 
        ost << "lo_part = __shfl_xor(__double2loint(cdot_" << spec_idx << ")," << i << ",32);\n";
        ost << "cdot_" << spec_idx << " += __hiloint2double(hi_part,lo_part);\n";
      }
    }
    else
    {
      char mask[64];
      sprintf(mask,"0x%x",(32/points_per_cta)-1);
      // Here we use warp-synchronous reductions linearly
      for (int idx = 1; idx < (32/points_per_cta); idx++)
      {
        ost << "if ((wid & " << mask << ") == " << idx << ")\n";
        ost << "  reduc_mirror[0][tid] = ddot_" << spec_idx << ";\n";
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  ddot_" << spec_idx << " += reduc_mirror[0][tid];\n";
      }
      // Then scatter back out so everyone has the same value
      ost << "if ((wid & " << mask << ") == 0)\n";
      ost << "  reduc_mirror[0][tid] = ddot_" << spec_idx << ";\n";
      ost << "ddot_" << spec_idx << " = reduc_mirror[0][tid];\n";
      for (int idx = 1; idx < (32/points_per_cta); idx++)
      {
        ost << "if ((wid & " << mask << ") == " << idx << ")\n";
        ost << "  reduc_mirror[0][tid] = cdot_" << spec_idx << ";\n";
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  cdot_" << spec_idx << " += reduc_mirror[0][tid];\n";
      }
      // Then scatter back out so everyone has the same value
      ost << "if ((wid & " << mask << ") == 0)\n";
      ost << "  reduc_mirror[0][tid] = cdot_" << spec_idx << ";\n";
      ost << "cdot_" << spec_idx << " = reduc_mirror[0][tid];\n";
    }
    ost << REAL << " scale = 1.0;\n";
    ost << "const bool need_scale = ((ddot_" << spec_idx << " * dt * " 
        << (1.0/REACTION_RATE_REF) << ") > stif_mole_frac_" << spec_idx << ");\n";
    ost << "if (__any(need_scale))\n";
    {
      PairDelim if_pair(ost);
      ost << REAL << " recip_ddot = 1.0/ddot_" << spec_idx << ";\n";
      ost << INT << " index;\n";
      for (unsigned wid = 0; wid < chem_warps.size(); wid+=(32/warp_size))
      {
        if (wid == 0)
          ost << "if (wid < " << (32/warp_size) << ")\n";
        else
          ost << "else if (wid < " << (wid+(32/warp_size)) << ")\n";
        if (spec_idx < chem_warps[wid]->stifs.size())
        {
          Stif *stif = chem_warps[wid]->stifs[spec_idx];
          int index = find_ordered_index(stif->species);
          assert(index != -1);
          ost << "index = " << index << ";\n";
        }
        else
          ost << "index = 0;\n";
      }
      ost << REAL << " recip_mass = recip_molecular_masses[index];\n";
      ost << REAL << " part_sum = cdot_" << spec_idx << " + stif_diffusion_" << spec_idx << " * recip_mass;\n"; 
      ost << REAL << " c0 = stif_mole_frac_" << spec_idx << " * part_sum * recip_ddot;\n";
      ost << "c0 = (part_sum + (stif_mole_frac_"
          << spec_idx << " - c0) * recip_dt) * recip_ddot;\n";
      // Check for dumb zero condition
      //ost << "if (stif_mole_frac_" << spec_idx << " > 0.0)\n";
      ost << "if (need_scale)\n";
      //ost << "if ((ddot * dt) > local_mole_frac)\n";
      ost << "  scale = c0;\n";
    }
    char mask[64];
    sprintf(mask,"0x%x",(32/points_per_cta)-1);
    ost << "if ((wid & " << mask << ") == 0)\n";  
    ost << "  scratch[(threadIdx.x >> 5)+" << (spec_idx*reac_warps) << "][tid] = scale;\n";
  }
  // Now emit a barrier to make sure that everyone has written their values
  emit_barrier(ost, 0, reac_warps, true/*wait*/);
  for (unsigned wid = 0; wid < chem_warps.size(); wid+=(32/warp_size))
  {
    if (wid == 0)
      ost << "if (wid < " << (32/warp_size) << ")\n";
    else
      ost << "else if (wid < " << (wid+(32/warp_size)) << ")\n";
    PairDelim warp_pair(ost);
    ost << INT << " index;\n";
    // Go through all the reactions
    for (unsigned reac_idx = 0; reac_idx < chem_warps[wid]->reacs.size(); reac_idx++)
    {
      // for each of our sub-warps, count how many scaling factors they need
      // for each of their forward and backward reactions
      std::vector<std::vector<int> > forward_scaling_indexes(32/warp_size);
      std::vector<std::vector<int> > reverse_scaling_indexes(32/warp_size);
      for (unsigned wid2 = 0; wid2 < chem_warps.size(); wid2+=(32/warp_size))
      {
        for (unsigned s = 0; s < chem_warps[wid2]->stifs.size(); s++)
        {
          Stif *stif = chem_warps[wid2]->stifs[s];
          for (int w = 0; w < (32/warp_size); w++)
          {
            if (reac_idx < chem_warps[wid+w]->reacs.size())
            {
              Reaction *reac = chem_warps[wid+w]->reacs[reac_idx];
              if (stif->forward_d.find(reac) != stif->forward_d.end())
              {
                int index = (wid2/(32/warp_size))+s*reac_warps;
                forward_scaling_indexes[w].push_back(index);
              }
              if (stif->backward_d.find(reac) != stif->backward_d.end())
              {
                int index = (wid2/(32/warp_size))+s*reac_warps;
                reverse_scaling_indexes[w].push_back(index);
              }
            }
          }
        }
      }
      // Now find the maximum number of scaling values for each sub-warp
      int max_forward = -1;
      int max_reverse = -1;
      for (int w = 0; w < (32/warp_size); w++)
      {
        if (int(forward_scaling_indexes[w].size()) > max_forward)
          max_forward = forward_scaling_indexes[w].size();
        if (int(reverse_scaling_indexes[w].size()) > max_reverse)
          max_reverse = reverse_scaling_indexes[w].size();
      }
      assert(max_forward >= 0);
      assert(max_reverse >= 0);
      if (max_forward > 0)
      {
        // Pad all sub-warps so they have the same number of indexes 
        for (int w = 0; w < (32/warp_size); w++)
        {
          while (int(forward_scaling_indexes[w].size()) < max_forward)
            forward_scaling_indexes[w].push_back(shared_unity);
        }
        for (int idx = 0; idx < max_forward; idx++)
        {
          int global_idx = chem_warps[wid]->add_index(forward_scaling_indexes[0][idx],0/*version*/);
          for (int w = 1; w < (32/warp_size); w++)
            assert(global_idx == chem_warps[wid+w]->add_index(forward_scaling_indexes[w][idx],0/*version*/));
          int index_offset = global_idx/warp_size;
          int index_thread = global_idx%warp_size;
          ost << "index = __shfl(index_constants[" << index_offset << "],"
              << index_thread << "," << warp_size << ");\n";
          ost << "rr_f_" << reac_idx << " *= scratch[index][tid];\n";
        }
      }
      if (max_reverse > 0)
      {
        for (int w = 0; w < (32/warp_size); w++)
        {
          while (int(reverse_scaling_indexes[w].size()) < max_reverse)
            reverse_scaling_indexes[w].push_back(shared_unity);
        }
        for (int idx = 0; idx < max_reverse; idx++)
        {
          int global_idx = chem_warps[wid]->add_index(reverse_scaling_indexes[0][idx],0/*version*/);
          for (int w = 1; w < (32/warp_size); w++)
            assert(global_idx == chem_warps[wid+w]->add_index(reverse_scaling_indexes[w][idx],0/*version*/));
          int index_offset = global_idx/warp_size;
          int index_thread = global_idx%warp_size;
          ost << "index = __shfl(index_constants[" << index_offset << "],"
              << index_thread << "," << warp_size << ");\n";
          ost << "rr_r_" << reac_idx << " *= scratch[index][tid];\n";
        }
      }
    }
  }
}

void TranslationUnit::emit_small_init_out_code(CodeOutStream &ost)
{
  ost << "/* Warp specialized output code*/\n";
  std::set<Species*> remaining_species(ordered_species.begin(),
                                       ordered_species.end());
  while (!remaining_species.empty())
  {
    std::set<int> unassigned_warps;
    for (int idx = 0; idx < reac_warps; idx++)
      unassigned_warps.insert(idx);
    while (!unassigned_warps.empty() && !remaining_species.empty())
    {
      // Find the largest remaining species
      int max_contributions = -1;
      Species *max_spec = NULL;
      for (std::set<Species*>::const_iterator it = remaining_species.begin();
            it != remaining_species.end(); it++)
      {
        int contributions = (*it)->reaction_contributions.size();
        if (contributions > max_contributions)
        {
          max_contributions = contributions;
          max_spec = (*it);
        }
      }
      assert(max_spec != NULL);
      unsigned min_constants;
      int min_index = -1;
      // Now find the unassigned warp with the fewest index constants
      for (std::set<int>::const_iterator it = unassigned_warps.begin();
            it != unassigned_warps.end(); it++)
      {
        if (min_index == -1)
        {
          min_index = *it;
          min_constants = chem_warps[(*it)*(32/warp_size)]->int_constants[0].size();
        }
        else if (min_constants > chem_warps[(*it)*(32/warp_size)]->int_constants[0].size())
        {
          min_index = *it;
          min_constants = chem_warps[(*it)*(32/warp_size)]->int_constants[0].size();
        }
      }
      assert(min_index >= 0);
      // Now assign the max output species to the smallest warp
      for (int w = 0; w < (32/warp_size); w++)
        chem_warps[min_index*(32/warp_size)+w]->add_output(max_spec);
      remaining_species.erase(max_spec);
      unassigned_warps.erase(min_index);
    }
  }
  const unsigned num_specs = ((ordered_species.size()+reac_warps-1)/reac_warps);
  for (unsigned idx = 0; idx < num_specs; idx++)
  {
    ost << REAL << " output_" << idx << " = 0.0;\n";
  }
}

void TranslationUnit::emit_small_out_pass(CodeOutStream &ost, int pass, 
                                          int start_level, int stop_level)
{
  // First we need a barrier before we can start writing
  emit_barrier(ost, 0, reac_warps, true/*wait*/);
  for (int level = start_level; level < stop_level; level++)
  {
    PairDelim ropl_pair(ost);
    ost << REAL << " ropl_" << level << " = rr_f_" << level << " - rr_r_" << level << ";\n";  
    ost << "scratch[wid+" << (chem_warps.size()*(level-start_level)) << "][tid] = ropl_" << level << ";\n";
  }
  emit_barrier(ost, 0, reac_warps, true/*wait*/);
  for (unsigned wid = 0; wid < chem_warps.size(); wid+=(32/warp_size))
  {
    if (wid == 0)
      ost << "if (wid < " << (32/warp_size) << ")\n";
    else
      ost << "else if (wid < " << (wid+(32/warp_size)) << ")\n";
    PairDelim warp_pair(ost);
    ost << INT << " index;\n";
    for (unsigned spec_idx = 0; spec_idx < chem_warps[wid]->specs.size(); spec_idx++)
    {
      Species *output = chem_warps[wid]->specs[spec_idx];
      std::vector<int> add_indexes;
      std::vector<int> sub_indexes;
      // Go through all the reactions and see if we need the ropl values
      for (unsigned wid2 = 0; wid2 < chem_warps.size(); wid2++)
      {
        for (int level = start_level; level < stop_level; level++)
        {
          if (level < int(chem_warps[wid2]->reacs.size()))
          {
            Reaction *reac = chem_warps[wid2]->reacs[level];
            if (output->reaction_contributions.find(reac) !=
                output->reaction_contributions.end())
            {
              int index = wid2+(level-start_level)*chem_warps.size();
              int coeff = output->reaction_contributions[reac];
              if (coeff > 0)
              {
                for (int idx = 0; idx < coeff; idx++)
                  add_indexes.push_back(index);
              }
              else
              {
                for (int idx = 0; idx < (-coeff); idx++)
                  sub_indexes.push_back(index);
              }
            }
          }
        }
      }
      // Now figure out how to divide the adding and subtracting coefficients
      // among each of our sub-warps
      if (!add_indexes.empty())
      {
        std::vector<std::vector<int> > dist(32/warp_size); 
        int depth = small_distribute_indexes(add_indexes, dist, true/*pad with zeros*/);
        for (int idx = 0; idx < depth; idx++)
        {
          int global_idx = chem_warps[wid]->add_index(dist[0][idx],0/*version*/);
          for (int w = 1; w < (32/warp_size); w++)
            assert(global_idx == chem_warps[wid+w]->add_index(dist[w][idx],0/*version*/));
          int index_offset = global_idx/warp_size;
          int index_thread = global_idx%warp_size;
          ost << "index = __shfl(index_constants[" << index_offset << "],"
              << index_thread << "," << warp_size << ");\n";
          ost << "output_" << spec_idx << " += scratch[index][tid];\n";
        }
      }
      if (!sub_indexes.empty())
      {
        std::vector<std::vector<int> > dist(32/warp_size);
        int depth = small_distribute_indexes(sub_indexes, dist, true/*pad with zeros*/);
        for (int idx = 0; idx < depth; idx++)
        {
          int global_idx = chem_warps[wid]->add_index(dist[0][idx],0/*version*/);
          for (int w = 1; w < (32/warp_size); w++)
            assert(global_idx == chem_warps[wid+w]->add_index(dist[w][idx],0/*version*/));
          int index_offset = global_idx/warp_size;
          int index_thread = global_idx%warp_size;
          ost << "index = __shfl(index_constants[" << index_offset << "],"
              << index_thread << "," << warp_size << ");\n";
          ost << "output_" << spec_idx << " -= scratch[index][tid];\n";
        }
      }
    }
  }
}

void TranslationUnit::emit_small_output(CodeOutStream &ost)
{
  const unsigned num_specs = (ordered_species.size()+reac_warps-1)/reac_warps;
  char mask[64];
  sprintf(mask,"0x%x",(32/points_per_cta)-1);
  for (unsigned spec_idx = 0; spec_idx < num_specs; spec_idx++)
  {
    PairDelim spec_pair(ost);
    // First reduce the values across the sub-warps
    if (k20)
    {
      if (points_per_cta < 32)
        ost << INT << " hi_part,lo_part;\n";
      for (int i = points_per_cta; i < 32; i*=2)
      {
        ost << "hi_part = __shfl_xor(__double2hiint(output_" << spec_idx << ")," << i << ",32);\n"; 
        ost << "lo_part = __shfl_xor(__double2loint(output_" << spec_idx << ")," << i << ",32);\n";
        ost << "output_" << spec_idx << " += __hiloint2double(hi_part,lo_part);\n";
      }
    }
    else if (points_per_cta < 32)
    {
      // Here we use warp-synchronous reductions linearly 
      for (int idx = 1; idx < (32/points_per_cta); idx++)
      {
        ost << "if ((wid & " << mask << ") == " << idx << ")\n";
        ost << "  reduc_mirror[threadIdx.x >> 5][tid] = output_" << spec_idx << ";\n";
        ost << "if ((wid & " << mask << ") == 0)\n";
        ost << "  output_" << spec_idx << " += reduc_mirror[threadIdx.x >> 5][tid];\n";
      }
      // Then scatter back out so everyone has the same value
      ost << "if ((wid & " << mask << ") == 0)\n";
      ost << "  reduc_mirror[threadIdx.x >> 5][tid] = output_" << spec_idx << ";\n";
      ost << "output_" << spec_idx << " = reduc_mirror[threadIdx.x >> 5][tid];\n";
    }
    ost << INT << " index;\n";
    if (spec_idx == (num_specs-1))
      ost << "bool do_output;\n";
    for (int wid = 0; wid < int(chem_warps.size()); wid+=(32/warp_size))
    {
      if (wid == 0)
        ost << "if (wid < " << (32/warp_size) << ")\n";
      else
        ost << "else if (wid < " << (wid+(32/warp_size)) << ")\n";
      PairDelim warp_pair(ost);
      if (spec_idx < chem_warps[wid]->specs.size())
      {
        Species *spec = chem_warps[wid]->specs[spec_idx];
        int index = find_ordered_index(spec);
        assert(index != -1);
        ost << "index = " << index << ";\n";
        if (spec_idx == (num_specs-1))
          ost << "do_output = true;\n";
      }
      else
      {
        ost << "index = 0;\n";
        if (spec_idx == (num_specs-1))
          ost << " do_output = false;\n";
      }
    }
    // Now that we've got our index scale the value and do the write
    if (spec_idx == (num_specs-1))
      ost << "if (do_output)\n";
    PairDelim output_pair(ost);
    ost << "output_" << spec_idx << " *= (1e-3 * molecular_masses[index]);\n";
    ost << "if ((wid & " << mask << ") == 0)\n";
    char out_name[64];
    sprintf(out_name,"output_%d",spec_idx);
    emit_cuda_store(ost,WDOT_ARRAY,"index*spec_stride",out_name);
  }
}

int TranslationUnit::small_distribute_indexes(const std::vector<int> &indexes,
                                              std::vector<std::vector<int> > &dist,
                                              bool pad_with_zeros)
{
  const int banks = (32/warp_size);
  assert(int(dist.size()) == banks); 
  // Sort elements by bank to avoid shared memory bank conflicts
  for (std::vector<int>::const_iterator it = indexes.begin();
        it != indexes.end(); it++)
  {
    int bank = (*it) % banks;
    dist[bank].push_back(*it);
  }
  // Now figure out how to balance everything out
  int lower_bound = indexes.size()/banks;
  int upper_bound = (indexes.size() % banks) == 0 ? lower_bound : lower_bound+1;
  bool unbalanced = true;
  while (unbalanced)
  {
    unbalanced = false;
    for (int idx = 0; idx < banks; idx++)
    {
      int num_elmts = dist[idx].size();
      if (num_elmts < lower_bound)
      {
        // Find the one with the most values and steal something
        int most_idx = -1;
        int most_values = -1;
        for (int i = 0; i < banks; i++)
        {
          if (i == idx) 
            continue;
          if (most_values == -1)
          {
            most_idx = i;
            most_values = dist[i].size();
          }
          else if(int(dist[i].size()) > most_values)
          {
            most_idx = i;
            most_values = dist[i].size();
          }
        }
        assert(most_idx >= 0);
        assert(most_values > lower_bound);
        dist[idx].push_back(dist[most_idx].back());
        dist[most_idx].pop_back();
        unbalanced = unbalanced || ((num_elmts+1) < lower_bound);
      }
      else if (num_elmts > upper_bound)
      {
        // Find the one with the fewest values and donate a value
        int fewest_idx = -1;
        int fewest_values = -1;
        for (int i = 0; i < banks; i++)
        {
          if (i == idx)
            continue;
          if (fewest_values == -1)
          {
            fewest_idx = i;
            fewest_values = dist[i].size();
          }
          else if (int(dist[i].size()) < fewest_values)
          {
            fewest_idx = i;
            fewest_values = dist[i].size();
          }
        }
        assert(fewest_idx >= 0);
        assert(fewest_values < upper_bound);
        dist[fewest_idx].push_back(dist[idx].back());
        dist[idx].pop_back();
        unbalanced = unbalanced || ((num_elmts-1) > upper_bound);
      }
    }
  }
  int depth = -1;
  for (int idx = 0; idx < banks; idx++)
  {
    if (int(dist[idx].size()) > depth)
      depth = dist[idx].size();
  }
  assert(depth >= 0);
  // Fill out all the empty spaces with zero or unity
  for (int idx = 0; idx < banks; idx++)
  {
    while (int(dist[idx].size()) < depth)
    {
      if (pad_with_zeros)
        dist[idx].push_back(shared_zero);
      else
        dist[idx].push_back(shared_unity);
    }
  }
  return depth;
}

// Reaction code

void Reaction::emit_reaction_code(CodeOutStream &ost, unsigned local_idx, Profiler *profiler)
{
  emit_reaction_comment(ost, local_idx);

  if (hv.enabled)
    fprintf(stderr,"WARNING: HV parameter on reaction %d ignored\n", idx);

  PairDelim pair(ost);

  emit_forward_reaction_rate(ost, profiler);

  if (reversible)
    emit_reverse_reaction_rate(ost, profiler);

  ost << "rr_f[" << local_idx << "] = forward";
  if (profiler != NULL)
    profiler->writes(1);
  for (SpeciesCoeffMap::const_iterator it = forward.begin();
        it != forward.end(); it++)
  {
    assert(it->second > 0); 
    int index = unit->find_ordered_index(it->first);
    // Skip any QSSA species
    if (index == -1)
      continue;
    for (int i = 0; i < it->second; i++)
    {
      ost << " * " << MOLE_FRAC << "[" << index << "]";
      if (profiler != NULL)
      {
        profiler->reads(1);
        profiler->multiplies(1);
      }
    }
  }
  ost << ";\n";
  
  if (reversible)
  {
    ost << "rr_r[" << local_idx << "] = reverse";
    if (profiler != NULL)
      profiler->writes(1);
    for (SpeciesCoeffMap::const_iterator it = backward.begin();
          it != backward.end(); it++)
    {
      assert(it->second > 0);
      int index = unit->find_ordered_index(it->first);
      // Skip any QSSA species
      if (index == -1)
        continue;
      for (int i = 0; i < it->second; i++)
      {
        ost << " * " << MOLE_FRAC << "[" << index << "]";
        if (profiler != NULL)
        {
          profiler->reads(1);
          profiler->multiplies(1);
        }
      }
    }
    ost << ";\n";
  }
  else
  {
    ost << "rr_r[" << local_idx << "] = 0.0;\n";
    if (profiler != NULL)
      profiler->writes(1);
  }
  if ((thb != NULL) && !pressure_dep)
  {
    ost << "rr_f[" << local_idx << "] *= " << THBCONC << "[" << thb->idx << "];\n";
    if (profiler != NULL)
    {
      profiler->reads(1);
      profiler->multiplies(1);
    }
    if (reversible)
    {
      ost << "rr_r[" << local_idx << "] *= " << THBCONC << "[" << thb->idx << "];\n";
      if (profiler != NULL)
      {
        profiler->reads(1);
        profiler->multiplies(1);
      }
    }
  }
}

void Reaction::emit_reaction_comment(CodeOutStream &ost, unsigned local_idx)
{
  if (duplicate.enabled)
  {
    assert(duplicate.owner);
    ost.print("// %3d", local_idx);
    std::list<Reaction*> *duplicates = duplicate.ptr.duplicates;
    if (duplicates != NULL)
    {
      for (std::list<Reaction*>::const_iterator it = duplicates->begin();
            it != duplicates->end(); it++)
      {
        ost.print(", %d", (*it)->idx);
      }
    }
    ost << ")  ";
  }
  else
    ost.print("// %3d)  ", local_idx);
  bool first = true;
  for (SpeciesCoeffMap::const_iterator it = forward.begin();
        it != forward.end(); it++)
  {
    if (first)
      first = false;
    else
      ost << " + ";
    if (it->second != 1)
      ost << it->second << " ";
    ost << it->first->name;
  }

  // extra stuff
  if (pressure_dep)
    ost.print(" (+%s)", (pressure_species == NULL) ? "M" : pressure_species->name);
  else if (thb != NULL)
    ost << " + M";

  ost.print("%s", (reversible ? " <=> " : " => "));

  first = true;
  for (SpeciesCoeffMap::const_iterator it = backward.begin();
        it != backward.end(); it++)
  {
    if (first)
      first = false;
    else
      ost << " + ";
    if (it->second != 1)
      ost << it->second << " ";
    ost << it->first->name;
  }

  if (pressure_dep)
    ost.print(" (+%s)", (pressure_species == NULL) ? "M" : pressure_species->name);
  else if (thb != NULL)
    ost << " + M";
  ost << "\n";
}

void Reaction::emit_forward_reaction_rate(CodeOutStream &ost, Profiler *profiler)
{
  emit_single_forward(ost, "=", true/*first*/, profiler);
  if (duplicate.enabled)
  {
    assert(duplicate.owner);
    std::list<Reaction*> *duplicates = duplicate.ptr.duplicates;
    if (duplicates != NULL)
    {
      for (std::list<Reaction*>::const_iterator it = duplicates->begin();
            it != duplicates->end(); it++)
      {
        (*it)->emit_single_forward(ost, "= forward +", false/*first*/, profiler);
      }
    }
  }
}

void Reaction::emit_single_forward(CodeOutStream &ost, 
                  const char *prefix, bool first, Profiler *profiler)
{
  if (low.enabled)
  {
    ost << REAL << " rr_k0 = "; // << low.a;
    if (unit->use_dim)
      arrhenius(ost, low.a, low.beta, low.e, profiler);
    else
      arrhenius(ost, (low.a*REACTION_RATE_REF), low.beta, low.e, profiler);
    ost << ";\n";
    ost << REAL << " rr_kinf = "; //<< a;
    if (unit->use_dim)
      arrhenius(ost, a, beta, e, profiler);
    else
      arrhenius(ost, (a*REACTION_RATE_REF), beta, e, profiler);
    ost << ";\n";
    /* calculate pr */
    ost << REAL << " pr = rr_k0 / rr_kinf * ";
    if (profiler != NULL)
    {
      profiler->multiplies(1);
      profiler->divides(1);
      profiler->reads(1);
    }
    if ((pressure_species != NULL) && (pressure_species != unit->find_species("M")))
    {
      int index = unit->find_ordered_index(pressure_species); 
      assert(index != -1);
      ost << MOLE_FRAC << "[" << index << "]";
    }
    else
    {
      assert(thb != NULL);
      ost << THBCONC << "[" << (thb == NULL ? 0 : thb->idx) << "]";
    }
    ost << ";\n";
    if (troe.num > 0)
    {
      ost << REAL << " fcent = log10(MAX(" << (1.0-troe.a) << " * exp(";
      if (!unit->use_dim)
        ost << (-1.0*TEMPERATURE_REF/troe.t3) << " * " << TEMPERATURE << ")";
      else
        ost << (-1.0/troe.t3) << " * " << TEMPERATURE << ")";
      if (profiler != NULL)
      {
        profiler->logarithms(1);
        profiler->exponents(1);
        profiler->multiplies(2);
      }
      // Check for 0.0 here since in some cases
      // if we emit it we end up getting -inf for -1.0/troe.t1
      if (troe.a != 0.0)
      {
        if (!unit->use_dim)
          ost << " + " << troe.a << " * exp(" 
              << (-1.0*TEMPERATURE_REF/troe.t1) << " * " << TEMPERATURE << ")";
        else
          ost << " + " << troe.a << " * exp(" << (-1.0/troe.t1) << " * " 
              << TEMPERATURE << ")";
        if (profiler != NULL)
        {
          profiler->adds(1);
          profiler->multiplies(2);
          profiler->exponents(1);
        }
      }
      if (troe.num == 4)
      {
        if (unit->use_dim)
          ost << " + exp(" << (-troe.t2) << " * otc)"; 
        else
          ost << " + exp(" << (-troe.t2/TEMPERATURE_REF) << " * otc)";
        if (profiler != NULL)
        {
          profiler->adds(1);
          profiler->multiplies(1);
          profiler->exponents(1);
        }
      }
      ost << "," << unit->small_reac_rate << "));\n";
      ost << REAL << " flogpr = log10(MAX(pr," << unit->small_reac_rate << ")) - 0.4 - 0.67 * fcent;\n";
      if (profiler != NULL)
      {
        profiler->logarithms(1);
        profiler->multiplies(1);
      }
      ost << REAL << " fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;\n";
      if (profiler != NULL)
      {
        profiler->multiplies(2);
        profiler->subtracts(2);
      }
      ost << REAL << " fquan = flogpr / fdenom;\n";
      if (profiler != NULL)
        profiler->divides(1);
      ost << "fquan = fcent / (1.0 + fquan * fquan);\n";
      if (profiler != NULL)
      {
        profiler->adds(1);
        profiler->multiplies(1);
        profiler->divides(1);
      }
      if (first)
      {
        ost.print("%s forward %s rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);\n", REAL, prefix);
        if (profiler != NULL)
        {
          profiler->adds(1);
          profiler->multiplies(3);
          profiler->divides(1);
          profiler->exponents(1);
        }
      }
      else
      {
        ost.print("forward %s rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);\n", prefix);
        if (profiler != NULL)
        {
          profiler->adds(2);
          profiler->multiplies(3);
          profiler->divides(1);
          profiler->exponents(1);
        }
      }
    }
    else if (sri.num > 0)
    {
      ost << REAL << " fpexp = log10(pr);\n";
      if (profiler != NULL)
        profiler->logarithms(1);
      ost << REAL << " fpexp = 1. / (1. + fpexp * fpexp);\n";
      if (profiler != NULL)
      {
        profiler->adds(1);
        profiler->multiplies(1);
        profiler->divides(1);
      }
      ost << REAL << " fquan = (";
      ost << sri.a;
      ost << " * exp(";
      if (unit->use_dim)
        ost << (-sri.b);
      else
        ost << (-sri.b/TEMPERATURE_REF);
      ost << "*otc) + exp(";
      ost << (-1./sri.c);
      assert(false); // need to handle power
      ost << "*tc)) ** fpexp;\n";
      if (sri.num == 5) {
          ost << " fquan = fquan * ";
          ost << sri.d;
          ost << " * pow(" << TEMPERATURE <<",";
          ost << sri.e;
          ost << ");\n";
      }
      if (first)
      {
        ost.print("%s forward %s rr_kinf * pr/(1.0 + pr) * fquan;\n", REAL, prefix);
      }
      else
      {
        ost.print("forward %s rr_kinf * pr/(1.0 + pr) * fquan;\n", prefix);
      }
    }
    else
    {
      // plain old Lindemann
      if (first)
      {
        ost.print("%s forward %s rr_kinf * pr/(1.0 + pr);\n", REAL, prefix);
        if (profiler != NULL)
        {
          profiler->adds(1);
          profiler->multiplies(1);
          profiler->divides(1);
        }
      }
      else
      {
        ost.print("forward %s rr_kinf * pr/(1.0 + pr);\n", prefix);
        if (profiler != NULL)
        {
          profiler->adds(2);
          profiler->multiplies(1);
          profiler->divides(1);
        }
      }
    }
  }
  else if (lt.enabled)
  {
    /* Landau-Teller formulation */
    if (unit->use_dim)
      ost << REAL << " ftroot = pow(otc,(1./3.));\n";
    else
    {
      ost << REAL << " ftroot = pow(otc/" << TEMPERATURE_REF << ",(1./3.));\n";
      if (profiler != NULL)
        profiler->divides(1);
    }
    if (profiler != NULL)
      profiler->exponents(1);
    if (first)
      ost.print("%s forward %s ", REAL, prefix);
    else
      ost.print("forward %s ", prefix);
    if (!unit->use_dim)
      ost << (REACTION_RATE_REF * a * pow(TEMPERATURE_REF,beta));
    else
      ost << a;
    ost << " * exp(";
    ost << beta;
    ost << "*vlntemp - ";
    if (unit->use_dim)
      ost << e;
    else
      ost << (e/TEMPERATURE_REF);
    ost << "*otc + ";
    ost << lt.b;
    ost << "*ftroot + ";
    ost << lt.c;
    ost << "*ftroot*ftroot);\n";
    if (profiler != NULL)
    {
      profiler->multiplies(6);
      profiler->adds(2);
      profiler->subtracts(1);
      profiler->exponents(1);
    }
  }
  else
  {
    if (first)
      ost << REAL << " forward " << prefix << " "; //<< a;
    else
      ost << "forward " << prefix << " "; // << a;
    if (!unit->use_dim)
      arrhenius(ost, (a*REACTION_RATE_REF), beta, e, profiler);
    else
      arrhenius(ost, a, beta, e, profiler);
    ost << ";\n";
  }
}

void Reaction::emit_reverse_reaction_rate(CodeOutStream &ost, Profiler *profiler)
{
  if (rev.enabled)
  {
    if (rlt.enabled)
    {
      if (!lt.enabled)
      {
        if (unit->use_dim)
          ost << REAL << " ftroot = pow(otc,(1./3.));\n";
        else
          ost << REAL << " ftroot = pow(otc/" << TEMPERATURE_REF << ", (1./3.));\n";
        if (profiler != NULL)
        {
          profiler->exponents(1);
          if (!unit->use_dim)
            profiler->divides(1);
        }
      }
      ost << REAL << " forward = ";
      if (!unit->use_dim)
        ost << (a*pow(TEMPERATURE_REF,beta));
      else
        ost << a;
      ost << " * exp(";
      ost << beta;
      ost << "*vlntemp - ";
      if (unit->use_dim)
        ost << e;
      else
        ost << (e / TEMPERATURE_REF); 
      ost << "*otc + ";
      ost << rlt.b;
      ost << "*ftroot + ";
      ost << rlt.c;
      ost << "*ftroot*ftroot);\n";
      if (profiler != NULL)
      {
        profiler->adds(2);
        profiler->subtracts(1);
      }
    }
    else
    {
      /* explicit Arrhenius rate given */
      ost << REAL << " reverse = "; //<< rev.a;
      if (!unit->use_dim)
        arrhenius(ost, (REACTION_RATE_REF*rev.a), rev.beta, rev.e, profiler);
      else
        arrhenius(ost, rev.a, rev.beta, rev.e, profiler);
      ost << ";\n";
    }
  }
  else
  {
    /* use equilibrium relation */
    /* total nu */
    int nutot = 0;
    for (SpeciesCoeffMap::const_iterator it = stoich.begin();
          it != stoich.end(); it++)
      nutot += it->second;
    /* total stoich gibbs energy */
    ost << REAL << " xik = ";
    bool first = true;
    for (SpeciesCoeffMap::const_iterator it = stoich.begin();
          it != stoich.end(); it++)
    {
      if (first)
      {
        first = false;
        if (it->second > 1)
        {
          ost.print("%d.0 * ", it->second);
          if (profiler != NULL)
            profiler->multiplies(1);
        }
        else if (it->second == -1)
        {
          ost << "-";
          if (profiler != NULL)
            profiler->subtracts(1);
        }
        else if (it->second < -1)
        {
          ost.print("-%d.0 * ", -(it->second));
          if (profiler != NULL)
            profiler->multiplies(1);
        }
      }
      else
      {
        if (it->second == 1)
        {
          ost << " + ";
          if (profiler != NULL)
            profiler->adds(1);
        }
        else if (it->second > 1)
        {
          ost.print(" + %d.0 * ", it->second);
          if (profiler != NULL)
          {
            profiler->adds(1);
            profiler->multiplies(1);
          }
        }
        else if (it->second == -1)
        {
          ost << " - ";
          if (profiler != NULL)
            profiler->subtracts(1);
        }
        else if (it->second < -1)
        {
          ost.print(" - %d.0 * ", -it->second);
          if (profiler != NULL)
          {
            profiler->subtracts(1);
            profiler->multiplies(1);
          }
        }
      }
      // Subtract one because we ignore the third body species
      ost << GIBBS << "[" << (it->first->idx-1) << "]";
      if (profiler != NULL)
        profiler->reads(1);
    }
    if (first)
      ost << "0.0";
    ost << ";\n";
    ost << REAL << " reverse = forward";
    // This case of otc gets handled by folding it back through
    // all the gibbs free energy constants
    ost.print(" * MIN(%s(xik*otc", GETEXP);
    if (profiler != NULL)
    {
      profiler->multiplies(2);
      profiler->exponents(1);
    }
    //if (unit->no_nondim) 
    {
        if (nutot == 0)
            ost << ")";
        else if (nutot == 1)
        {
            ost << ") * oprt";
            if (profiler != NULL)
              profiler->multiplies(1);
        }
        else if (nutot == 2)
        {
            ost << ") * oprt * oprt";
            if (profiler != NULL)
              profiler->multiplies(2);
        }
        else if (nutot == -1)
        {
            ost << ") * prt";
            if (profiler != NULL)
              profiler->multiplies(1);
        }
        else if (nutot == -2)
        {
            ost << ") * prt * prt";
            if (profiler != NULL)
              profiler->multiplies(2);
        }
        else if (nutot > 0)
        {
            if (unit->use_dim)
              ost.print(" - %d.0*vlntemp)", nutot);
            else
              ost << " - " << nutot << ".0*(vlntemp + " << TEMPERATURE_REF << "))";
            if (profiler != NULL)
            {
              profiler->subtracts(1);
              profiler->multiplies(1);
              if (!unit->use_dim)
                profiler->adds(1);
            }
        }
        else 
        {
            if (unit->use_dim)
              ost.print(" + %d.0*vlntemp)", -nutot);
            else
              ost << " + " << (-nutot) << ".0*(vlntemp + " << TEMPERATURE_REF << "))";
            if (profiler != NULL)
            {
              profiler->adds(1);
              profiler->multiplies(1);
              if (!unit->use_dim)
                profiler->adds(1);
            }
        }
    } 
#if 0
    else {
        if (nutot == 0)
            ost << ")";
        else if (nutot == 1)
        {
            ost << ") * " << TEMPERATURE;
            cnt.add_flop();
        }
        else if (nutot == 2)
        {
            ost << ") * " << TEMPERATURE << " * " << TEMPERATURE;
            cnt.add_flop(2);
        }
        else if (nutot == -1)
        {
            ost << ") * otc";
            cnt.add_flop();
        }
        else if (nutot == -2)
        {
            ost << ") * otc * otc";
            cnt.add_flop(2);
        }
        else if (nutot > 0)
        {
            ost.print(" + %d.0*vlntemp)", nutot);
            cnt.add_flop(2);
        }
        else 
        {
            ost.print(" - %d.0*vlntemp)", -nutot);
            cnt.add_flop(2);
        }
    }
#endif
    ost << "," << unit->large_reac_rate << ");\n";
  }
}

void Reaction::arrhenius(CodeOutStream &ost, double prefix, double beta, double e, Profiler *profiler)
{
  if (e == 0.)
  {
    if (beta == 0.)
      ost << prefix;
    else if (beta == 1.)
    {
      if (!unit->use_dim)
        ost << (prefix * TEMPERATURE_REF) << " * " << TEMPERATURE;
      else
        ost << prefix << " * " << TEMPERATURE;
      if (profiler != NULL)
        profiler->multiplies(1);
    }
    else if (beta == 2.)
    {
      if (!unit->use_dim)
        ost << (prefix*TEMPERATURE_REF*TEMPERATURE_REF) << " * " 
            << TEMPERATURE << " * " << TEMPERATURE;
      else
        ost << prefix << " * " << TEMPERATURE << " * " << TEMPERATURE;
      if (profiler != NULL)
        profiler->multiplies(2);
    }
    else if (beta == -1.)
    {
      if (!unit->use_dim)
        ost << (prefix / TEMPERATURE_REF) << " * otc";
      else
        ost << prefix << " * otc";
      if (profiler != NULL)
        profiler->multiplies(1);
    }
    else if (beta == -2.)
    {
      if (!unit->use_dim)
        ost << (prefix / (TEMPERATURE_REF * TEMPERATURE_REF)) << " * otc * otc";
      else 
        ost << prefix << " * otc * otc";
      if (profiler != NULL)
        profiler->multiplies(2);
    }
    else
    {
      if (!unit->use_dim)
        ost << (prefix*pow(TEMPERATURE_REF, beta));
      else
        ost << prefix;
      ost.print(" * %s(", GETEXP); 
      ost << beta << " * vlntemp)";
      if (profiler != NULL)
      {
        profiler->multiplies(1);
        profiler->exponents(1);
      }
    }
  }
  else
  {
    if (beta == 0.)
    {
      ost << prefix;
      ost.print(" * %s(", GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
      if (profiler != NULL)
      {
        profiler->multiplies(2);
        profiler->exponents(1);
      }
    }
    else if (beta == 1.)
    {
      if (!unit->use_dim)
        ost << (prefix * TEMPERATURE_REF);
      else
        ost << prefix;
      ost.print(" * %s * %s(", TEMPERATURE, GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
      if (profiler != NULL)
      {
        profiler->multiplies(3);
        profiler->exponents(1);
      }
    }
    else if (beta == -1.)
    {
      if (!unit->use_dim)
        ost << (prefix / TEMPERATURE_REF);
      else
        ost << prefix;
      ost.print(" * otc * %s(", GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
      if (profiler != NULL)
      {
        profiler->multiplies(3);
        profiler->exponents(1);
      }
    }
    else if (beta == 2.)
    {
      if (!unit->use_dim)
        ost << (prefix*TEMPERATURE_REF*TEMPERATURE_REF);
      else
        ost << prefix;
      ost.print(" * %s * %s * %s(", TEMPERATURE, TEMPERATURE, GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
      if (profiler != NULL)
      {
        profiler->multiplies(4);
        profiler->exponents(1);
      }
    }
    else if (beta == -2.)
    {
      if (!unit->use_dim)
        ost << (prefix/(TEMPERATURE_REF*TEMPERATURE_REF));
      else
        ost << prefix;
      ost.print(" * otc * otc * %s(", GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
      if (profiler != NULL)
      {
        profiler->multiplies(4);
        profiler->exponents(1);
      }
    }
    else
    {
      if (!unit->use_dim)
        ost << (prefix*pow(TEMPERATURE_REF,beta));
      else
        ost << prefix;
      ost.print(" * %s(", GETEXP);
      ost << beta << "*vlntemp ";
      if (e < 0.)
        ost << "+ " << -e;
      else
        ost << "- " << e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
      if (profiler != NULL)
      {
        profiler->multiplies(3);
        profiler->exponents(1);
        if (e < 0.)
          profiler->adds(1);
        else
          profiler->subtracts(1);
      }
    }
  }
}

void Reaction::emit_sse_reaction_code(CodeOutStream &ost, unsigned local_idx, int iters)
{
  emit_reaction_comment(ost, local_idx);

  if (hv.enabled)
    fprintf(stderr,"WARNING: HV parameter on reaction %d ignored\n", idx);

  PairDelim pair(ost);

  ost << VECTOR2 << " forward, reverse;\n";
  emit_sse_forward_reaction_rate(ost, iters);

  if (reversible)
    emit_sse_reverse_reaction_rate(ost, iters);

  ost << "rr_f[" << (local_idx*iters) << "+idx] = forward;\n";
  for (SpeciesCoeffMap::const_iterator it = forward.begin();
        it != forward.end(); it++)
  {
    assert(it->second > 0); 
    int index = unit->find_ordered_index(it->first);
    // Skip any QSSA species
    if (index == -1)
      continue;
    for (int i = 0; i < it->second; i++)
    {
      ost << "rr_f[" << (local_idx*iters) << "+idx] = _mm_mul_pd("
          << "rr_f[" << (local_idx*iters) << "+idx],mole_frac["
          << (index*iters) << "+idx]);\n";
    }
  }
  if (reversible)
  {
    ost << "rr_r[" << (local_idx*iters) << "+idx] = reverse;\n"; 
    for (SpeciesCoeffMap::const_iterator it = backward.begin();
          it != backward.end(); it++)
    {
      assert(it->second > 0);
      int index = unit->find_ordered_index(it->first);
      // Skip any QSSA species
      if (index == -1)
        continue;
      for (int i = 0; i < it->second; i++)
      {
        ost << "rr_r[" << (local_idx*iters) << "+idx] = _mm_mul_pd("
            << "rr_r[" << (local_idx*iters) << "+idx],mole_frac["
            << (index*iters) << "+idx]);\n";
      }
    }
  }
  else
    ost << "rr_r[" << (local_idx*iters) << "+idx] = _mm_set1_pd(0.0);\n";
  if ((thb != NULL) && !pressure_dep)
  {
    ost << "rr_f[" << (local_idx*iters) << "+idx] = _mm_mul_pd("
        << "rr_f[" << (local_idx*iters) << "+idx]," << THBCONC
        << "[" << (thb->idx*iters) << "+idx]);\n";
    if (reversible)
     ost << "rr_r[" << (local_idx*iters) << "+idx] = _mm_mul_pd("
          << "rr_r[" << (local_idx*iters) << "+idx]," << THBCONC
          << "[" << (thb->idx*iters) << "+idx]);\n"; 
  }
}

void Reaction::emit_sse_forward_reaction_rate(CodeOutStream &ost, int iters)
{
  emit_sse_single_forward_reaction_rate(ost, true/*first*/, iters); 
  if (duplicate.enabled)
  {
    assert(duplicate.owner);
    std::list<Reaction*> *duplicates = duplicate.ptr.duplicates;
    if (duplicates != NULL)
    {
      for (std::list<Reaction*>::const_iterator it = duplicates->begin();
            it != duplicates->end(); it++)
      {
        (*it)->emit_sse_single_forward_reaction_rate(ost, false/*first*/, iters);
      }
    }
  }
}

void Reaction::emit_sse_single_forward_reaction_rate(CodeOutStream &ost, bool first, int iters)
{
  if (low.enabled)
  {
    ost << VECTOR2 << " rr_k0, rr_kinf;\n";
    if (unit->use_dim)
      sse_arrhenius(ost, "rr_k0", iters, low.a, low.beta, low.e);
    else
      sse_arrhenius(ost, "rr_k0", iters, (low.a*REACTION_RATE_REF), low.beta, low.e);
    if (unit->use_dim)
      sse_arrhenius(ost, "rr_kinf", iters, a, beta, e);
    else
      sse_arrhenius(ost, "rr_kinf", iters, (a*REACTION_RATE_REF), beta, e);
    ost << VECTOR2 << " pr = _mm_mul_pd(_mm_div_pd(rr_k0,rr_kinf),";
    if ((pressure_species != NULL) && (pressure_species != unit->find_species("M")))
    {
      int index = unit->find_ordered_index(pressure_species); 
      assert(index != -1);
      ost << "mole_frac[" << (index*iters) << "+idx]";
    }
    else
    {
      assert(thb != NULL);
      ost << THBCONC << "[" << ((thb == NULL ? 0 : thb->idx)*iters) << "+idx]";
    }
    ost << ");\n";
    if (troe.num > 0)
    {
      ost << VECTOR2 << " fcent;\n";
      PairDelim fcent_pair(ost);
      if (!unit->use_dim)
        ost << "fcent = _mm_mul_pd(" << TEMPERATURE << "[idx], _mm_set1_pd(" 
            << (-1.0*TEMPERATURE_REF/troe.t3) << "));\n";
      else
        ost << "fcent = _mm_mul_pd(" << TEMPERATURE << "[idx], _mm_set1_pd(" 
            << (-1.0/troe.t3) << "));\n";
      emit_sse_exp_taylor_series_expansion(ost, "fcent", unit->taylor_stages);
      ost << "fcent = _mm_mul_pd(fcent, _mm_set1_pd(" << (1.0-troe.a) << "));\n";
      if (troe.a != 0.0)
      {
        PairDelim temp_pair(ost);
        if (!unit->use_dim)
          ost << VECTOR2 << " fctemp = _mm_mul_pd(" << TEMPERATURE 
              << "[idx], _mm_set1_pd(" << (-1.0*TEMPERATURE_REF/troe.t1) << "));\n";
        else
          ost << VECTOR2 << " fctemp = _mm_mul_pd(" << TEMPERATURE
              << "[idx], _mm_set1_pd(" << (-1.0/troe.t1) << "));\n";
        emit_sse_exp_taylor_series_expansion(ost, "fctemp", unit->taylor_stages);
        ost << "fcent = _mm_add_pd(fcent, _mm_mul_pd(fctemp, _mm_set1_pd(" << (troe.a) << ")));\n";
      }
      if (troe.num == 4)
      {
        PairDelim temp_pair(ost);
        if (!unit->use_dim)
          ost << VECTOR2 << " fctemp = _mm_mul_pd(otc[idx], _mm_set1_pd(" << (-troe.t2/TEMPERATURE_REF) << "));\n";
        else
          ost << VECTOR2 << " fctemp = _mm_mul_pd(otc[idx], _mm_set1_pd(" << (-troe.t2) << "));\n";
        emit_sse_exp_taylor_series_expansion(ost, "fctemp", unit->taylor_stages);
        ost << "fcent = _mm_add_pd(fcent, fctemp);\n";
      }
      ost << VECTOR2 << " small = _mm_set1_pd(" << unit->small_reac_rate << ");\n";
      ost << VECTOR2 << " mask = _mm_cmpgt_pd(fcent,small);\n";
      ost << "fcent = _mm_and_pd(fcent, mask);\n";
      ost << "fcent = _mm_add_pd(fcent,_mm_andnot_pd(mask,small));\n";
      ost << "fcent = _mm_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(fcent,fcent,1))), log10(_mm_cvtsd_f64(fcent)));\n";
      ost << "mask = _mm_cmpgt_pd(pr,small);\n";
      ost << VECTOR2 << " prclamp = _mm_and_pd(pr,mask);\n";
      ost << "prclamp = _mm_add_pd(prclamp,_mm_andnot_pd(mask,small));\n";
      ost << VECTOR2 << " flogpr = _mm_set_pd(log10(_mm_cvtsd_f64(_mm_shuffle_pd(prclamp,prclamp,1))),log10(_mm_cvtsd_f64(prclamp)));\n";
      ost << "flogpr = _mm_sub_pd(flogpr, _mm_add_pd(_mm_set1_pd(0.4),_mm_mul_pd(_mm_set1_pd(0.67),fcent)));\n";
      ost << VECTOR2 << " fdenom = _mm_add_pd(_mm_set1_pd(0.75),_mm_mul_pd(_mm_set1_pd(-1.27),fcent));\n";
      ost << "fdenom = _mm_add_pd(fdenom,_mm_mul_pd(_mm_set1_pd(-0.14),flogpr));\n";
      ost << VECTOR2 << " fquan = _mm_div_pd(flogpr,fdenom);\n";
      ost << "fquan = _mm_div_pd(fcent,_mm_add_pd(_mm_set1_pd(1.0),_mm_mul_pd(fquan,fquan)));\n";
      if (first)
      {
        ost << VECTOR2 << " ftemp = _mm_mul_pd(fquan,_mm_set1_pd(DLn10));\n";
        emit_sse_exp_taylor_series_expansion(ost, "ftemp", unit->taylor_stages);
        ost << "forward = _mm_mul_pd(rr_kinf,pr);\n";
        ost << "forward = _mm_div_pd(forward,_mm_add_pd(_mm_set1_pd(1.0),pr));\n";
        ost << "forward = _mm_mul_pd(forward, ftemp);\n";
      }
      else
      {
        ost << VECTOR2 << " ftemp = _mm_mul_pd(fquan,_mm_set1_pd(DLn10));\n";
        emit_sse_exp_taylor_series_expansion(ost, "ftemp", unit->taylor_stages);
        ost << VECTOR2 << " ftemp2 = _mm_mul_pd(rr_kinf,pr);\n";
        ost << "ftemp2 = _mm_div_pd(ftemp2,_mm_add_pd(_mm_set1_pd(1.0),pr));\n";
        ost << "forward = _mm_add_pd(forward, _mm_mul_pd(ftemp2,ftemp));\n";
      }
    }
    else if (sri.num > 0)
    {
      // TODO: handle power in SRI
      assert(false);
    }
    else
    {
      // plain old Lindermann
      if (first)
        ost << "forward = _mm_div_pd(_mm_mul_pd(rr_kinf,pr),_mm_add_pd(_mm_set1_pd(1.0),pr));\n";
      else
        ost << "forward = _mm_add_pd(forward,_mm_div_pd(_mm_mul_pd(rr_kinf,pr),_mm_add_pd(_mm_set1_pd(1.0),pr)));\n";
    }
  }
  else if (lt.enabled)
  {
    // Laundau-Teller
    if (!unit->use_dim)
    {
        ost << VECTOR2 << " scaleotc = _mm_mul_pd(otc[idx],_mm_set1_pd(" 
            << (1.0/TEMPERATURE_REF) << "));\n";
        ost << VECTOR2 << " ftroot = _mm_set_pd(pow(_mm_cvtsd_f64("
            << "_mm_shuffle_pd(scaleotc,scaleotc,1)),(1./3.)),pow("
            << "_mm_cvtsd_f64(scaleotc),(1./3.)));\n";
    }
    else
      ost << VECTOR2 << " ftroot = _mm_set_pd(pow(_mm_cvtsd_f64("
          << "_mm_shuffle_pd(otc[idx],otc[idx],1)),(1./3.)),pow(_mm_cvtsd_f64(otc[idx]),(1./3.)));\n"; 
    ost << VECTOR2 << " etemp = _mm_mul_pd(_mm_set1_pd(" << beta << "),vlntemp);\n";
    ost << "etemp = _mm_sub_pd(etemp,_mm_mul_pd(_mm_set1_pd(" << (unit->use_dim ? e : (e/TEMPERATURE_REF)) << "),otc[idx]));\n";
    ost << "etemp = _mm_add_pd(etemp,_mm_mul_pd(_mm_set1_pd(" << lt.b << "),ftroot));\n";
    ost << "etemp = _mm_add_pd(etemp,_mm_mul_pd(_mm_set1_pd(" << lt.c << "),_mm_mul_pd(ftroot,ftroot)));\n";
    emit_sse_exp_taylor_series_expansion(ost, "etemp", unit->taylor_stages);
    if (first)
      ost << "forward = _mm_mul_pd(_mm_set1_pd(" << (!unit->use_dim ? (REACTION_RATE_REF * a * pow(TEMPERATURE_REF,beta)) : a) << "),etemp);\n"; 
    else
      ost << "forward = _mm_add_pd(forward,_mm_mul_pd(_mm_set1_pd(" << (!unit->use_dim ? (REACTION_RATE_REF * a * pow(TEMPERATURE_REF,beta)) : a) << "),etemp));\n";
  }
  else
  {
    PairDelim next_pair(ost);
    ost << VECTOR2 << " next;\n";
    if (!unit->use_dim)
      sse_arrhenius(ost, "next", iters, (a*REACTION_RATE_REF), beta, e);
    else
      sse_arrhenius(ost, "next", iters, a, beta, e);
    if (first)
      ost << "forward = next;\n";
    else
      ost << "forward = _mm_add_pd(forward,next);\n";
  }
}

void Reaction::emit_sse_reverse_reaction_rate(CodeOutStream &ost, int iters)
{
  if (rev.enabled)
  {
    if (rlt.enabled)
    {
      if (!lt.enabled)
      {
        if (unit->use_dim)
          ost << VECTOR2 << " ftroot = _mm_set_pd(pow(_mm_cvtsd_f64(_mm_shuffle_pd(otc,otc,1)),(1./3.)),pow(_mm_cvtsd_f64(otc),(1./3.)));\n";
        else
        {
          ost << VECTOR2 << " ftroot = _mm_div_pd(otc,_mm_set1_pd(" 
              << TEMPERATURE_REF << "));\n";
          ost << "ftroot = _mm_set_pd(pow(_mm_cvtsd_f64(_mm_shuffle_pd(otc,otc,1)),"
              << "(1./3.)),pow(_mm_cvtsd_f64(ftroot),(1./3.)));\n";
        }
      }
      ost << VECTOR2 << " forward = _mm_mul_pd(_mm_set1_pd(" << beta << "),vlntemp);\n";
      ost << "forward = _mm_sub_pd(forward,_mm_mul_pd(_mm_set1_pd(";
      if (unit->use_dim)
        ost << e;
      else
        ost << (e/TEMPERATURE_REF);
      ost << "),otc));\n";
      ost << "forward = _mm_add_pd(rr_f,_mm_mul_pd(_mm_set1_pd(" << rlt.b << "),ftroot));\n";
      ost << "forward = _mm_add_pd(rr_f,_mm_mul_pd(_mm_mul_pd(_mm_set1_pd(" << rlt.c << "),ftroot),ftroot));\n";
    }
    else
    {
      if (!unit->use_dim)
        sse_arrhenius(ost, "reverse", iters, (REACTION_RATE_REF*rev.a), rev.beta, rev.e);
      else
        sse_arrhenius(ost, "reverse", iters, rev.a, rev.beta, rev.e);
    }
  }
  else
  {
    /* use equilibrium relation */
    /* total nu */
    int nutot = 0;
    for (SpeciesCoeffMap::const_iterator it = stoich.begin();
          it != stoich.end(); it++)
      nutot += it->second;
    ost << VECTOR2 << " xik = ";
    bool first_invocation = true;
    for (SpeciesCoeffMap::const_iterator it = stoich.begin();
          it != stoich.end(); it++)
    {
      // Subtract one because we ignore the third body species
      int spec_id = it->first->idx-1;
      if (first_invocation)
      {
        first_invocation = false;
        if (it->second > 1)
        {
          ost << "_mm_mul_pd(_mm_set1_pd(double(" << it->second << "))," 
              << GIBBS << "[" << (spec_id*iters) << "+idx]);\n";
        }
        else if (it->second == -1)
        {
          // do fast negation
          ost << "_mm_xor_pd(_mm_set1_pd(-0.0)," 
              << GIBBS << "[" << (spec_id*iters) << "+idx]);\n";
        }
        else if (it->second < -1)
        {
          ost << "_mm_mul_pd(_mm_set1_pd(double(" << it->second << "))," 
              << GIBBS << "[" << (spec_id*iters) << "+idx]);\n";
        }
        else
        {
          assert(it->second == 1);
          ost << GIBBS << "[" << (spec_id*iters) << "+idx];\n";
        }
      }
      else
      {
        if (it->second == 1)
        {
          ost << "xik = _mm_add_pd(xik," << GIBBS << "[" 
              << (spec_id*iters) << "+idx]);\n";
        }
        else if (it->second > 1)
        {
          ost << "xik = _mm_add_pd(xik,_mm_mul_pd(_mm_set1_pd(double(" 
              << it->second << "))," << GIBBS << "[" << (spec_id*iters) << "+idx]));\n";
        }
        else if (it->second == -1)
        {
          ost << "xik = _mm_sub_pd(xik," << GIBBS << "[" << (spec_id*iters) << "+idx]);\n";
        }
        else if (it->second < -1)
        {
          ost << "xik = _mm_add_pd(xik,_mm_mul_pd(_mm_set1_pd(double(" 
              << it->second << "))," << GIBBS << "[" << (spec_id*iters) << "+idx]));\n";
        }
      }
    }
    ost << "reverse = _mm_mul_pd(xik,otc[idx]);\n";
    if ((nutot > 2) || (nutot < -2))
    {
      ost << "reverse = _mm_add_pd(reverse,_mm_mul_pd(_mm_set1_pd(" << nutot << "),vlntemp));\n";
    }
    emit_sse_exp_taylor_series_expansion(ost, "reverse", unit->taylor_stages);
    ost << "reverse = _mm_mul_pd(forward,reverse);\n";
    {
        if (nutot == 0)
        {
            //ost << ")";
        }
        else if (nutot == 1)
        {
            //ost << ") * oprt" << POSTFIX;
            ost << "reverse = _mm_mul_pd(reverse,oprt[idx]);\n";
        }
        else if (nutot == 2)
        {
            //ost << ") * oprt" << POSTFIX << " * oprt" << POSTFIX;
            ost << "reverse = _mm_mul_pd(_mm_mul_pd(reverse,oprt[idx]),oprt[idx]);\n";
        }
        else if (nutot == -1)
        {
            //ost << ") * prt" << POSTFIX;
            ost << "reverse = _mm_mul_pd(reverse,prt[idx]);\n";
        }
        else if (nutot == -2)
        {
            //ost << ") * prt" << POSTFIX << " * prt" << POSTFIX;
            ost << "reverse = _mm_mul_pd(_mm_mul_pd(reverse,prt[idx]),prt[idx]);\n";
        }
        else if (nutot > 0)
        {
            //ost.print(" - %d.0*vlntemp%s)", nutot,POSTFIX);
            if (!unit->use_dim)
              assert(false); // handle this case
        }
        else 
        {
            //ost.print(" + %d.0*vlntemp%s)", -nutot,POSTFIX);
            if (!unit->use_dim)
              assert(false); // handle this case
        }
    }
  }
}

void Reaction::sse_arrhenius(CodeOutStream &ost, const char *var_name, int iters,
                             double prefix, double beta, double e)
{
  PairDelim arr_pair(ost);
  if (e == 0.)
  {
    if (beta == 0.)
    {
      ost << VECTOR2 << " arrtemp = _mm_set1_pd(" << prefix << ");\n"; //ost << prefix;
    }
    else if (beta == 1.)
    {
      if (!unit->use_dim)
      {
        //ost << (prefix * TEMPERATURE_REF) << " * " << TEMPERATURE;
        ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << (prefix * TEMPERATURE_REF) << ")," << TEMPERATURE << "[idx]);\n";
      }
      else
      {
        //ost << prefix << " * " << TEMPERATURE;
        ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << prefix << "),"
            << TEMPERATURE << "[idx]);\n";
      }
    }
    else if (beta == 2.)
    {
      if (!unit->use_dim)
      {
        //ost << (prefix*TEMPERATURE_REF*TEMPERATURE_REF) << " * " << TEMPERATURE << " * " << TEMPERATURE;
        ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(" 
            << (prefix*TEMPERATURE_REF*TEMPERATURE_REF) << "),"
            << TEMPERATURE << "[idx]),"
            << TEMPERATURE << "[idx]);\n";
      }
      else
      {
        //ost << prefix << " * " << TEMPERATURE << " * " << TEMPERATURE;
        ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(" << prefix << "),"
            << TEMPERATURE << "[idx]), " << TEMPERATURE << "[idx]);\n";
      }
    }
    else if (beta == -1.)
    {
      if (!unit->use_dim)
      {
        //ost << (prefix / TEMPERATURE_REF) << " * otc";
        ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << (prefix/TEMPERATURE_REF) << "),otc[idx]);\n";
      }
      else
      {
        //ost << prefix << " * otc";
        ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << prefix << "),otc[idx]);\n";
      }
    }
    else if (beta == -2.)
    {
      if (!unit->use_dim)
      {
        //ost << (prefix / (TEMPERATURE_REF * TEMPERATURE_REF)) << " * otc * otc";
        ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(" << (prefix/(TEMPERATURE_REF*TEMPERATURE_REF)) << "),otc[idx]),otc[idx]);\n";
      }
      else 
      {
        //ost << prefix << " * otc * otc";
        ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(" << prefix << "),otc[idx]),otc[idx]);\n";
      }
    }
    else
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix*pow(TEMPERATURE_REF, beta));
      else
        ost << prefix;
      ost.print(" * %s(", GETEXP); 
      ost << beta << " * vlntemp)";
#endif
      ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << beta << "),vlntemp[idx]);\n";
      emit_sse_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      if (!unit->use_dim)
        ost << "arrtemp = _mm_mul_pd(_mm_set1_pd(" << (prefix*pow(TEMPERATURE_REF,beta)) << "), arrtemp);\n";
      else
        ost << "arrtemp = _mm_mul_pd(_mm_set1_pd(" << prefix << "), arrtemp);\n";
    }
  }
  else
  {
    if (beta == 0.)
    {
#if 0
      ost << prefix;
      ost.print(" * %s(", GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << (-e) << "), ortc[idx]);\n";
      emit_sse_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      ost << "arrtemp = _mm_mul_pd(_mm_set1_pd(" << prefix << "),arrtemp);\n";
    }
    else if (beta == 1.)
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix * TEMPERATURE_REF);
      else
        ost << prefix;
      ost.print(" * %s * %s(", TEMPERATURE, GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << (-e) << "), ortc[idx]);\n";
      emit_sse_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      ost << "arrtemp = _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(" << (prefix*TEMPERATURE_REF) << "),"
          << TEMPERATURE << "[idx]),arrtemp);\n";
    }
    else if (beta == -1.)
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix / TEMPERATURE_REF);
      else
        ost << prefix;
      ost.print(" * otc * %s(", GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << (-e) << "), ortc[idx]);\n";
      emit_sse_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      ost << "arrtemp = _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(" << (prefix/TEMPERATURE_REF) << "), otc[idx]),arrtemp);\n";
    }
    else if (beta == 2.)
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix*TEMPERATURE_REF*TEMPERATURE_REF);
      else
        ost << prefix;
      ost.print(" * %s * %s * %s(", TEMPERATURE, TEMPERATURE, GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << (-e) << "),ortc[idx]);\n";
      emit_sse_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      ost << "arrtemp = _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(_mm_set1_pd(" 
          << (prefix*TEMPERATURE_REF*TEMPERATURE_REF)
          << ")," << TEMPERATURE << "[idx]),"
          << TEMPERATURE << "[idx]),arrtemp);\n";
    }
    else if (beta == -2.)
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix/(TEMPERATURE_REF*TEMPERATURE_REF));
      else
        ost << prefix;
      ost.print(" * otc * otc * %s(", GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << (-e) << "),ortc[idx]);\n";
      emit_sse_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      ost << "arrtemp = _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(_mm_set1_pd(" << (prefix/(TEMPERATURE_REF*TEMPERATURE_REF))
          << "),otc[idx]),otc[idx]),arrtemp);\n";
    }
    else
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix*pow(TEMPERATURE_REF,beta));
      else
        ost << prefix;
      ost.print(" * %s(", GETEXP);
      ost << beta << "*vlntemp ";
      if (e < 0.)
        ost << "+ " << -e;
      else
        ost << "- " << e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR2 << " arrtemp = _mm_mul_pd(_mm_set1_pd(" << beta << "),vlntemp[idx]);\n";
      if (e < 0.)
        ost << "arrtemp = _mm_add_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(" << (-e) << "),ortc[idx]));\n";
      else
        ost << "arrtemp = _mm_sub_pd(arrtemp,_mm_mul_pd(_mm_set1_pd(" << (e) << "),ortc[idx]));\n";
      emit_sse_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      if (!unit->use_dim)
        ost << "arrtemp = _mm_mul_pd(_mm_set1_pd(" << (prefix*pow(TEMPERATURE_REF,beta)) << "),arrtemp);\n";
      else
        ost << "arrtemp = _mm_mul_pd(_mm_set1_pd(" << prefix << "),arrtemp);\n";
    }
  }
  ost << var_name << " = arrtemp;\n";
}

void Reaction::emit_avx_reaction_code(CodeOutStream &ost, unsigned local_idx, int iters)
{
  emit_reaction_comment(ost, local_idx);

  if (hv.enabled)
    fprintf(stderr,"WARNING: HV parameter on reaction %d ignored\n", idx);

  PairDelim pair(ost);

  ost << VECTOR4 << " forward, reverse;\n";
  emit_avx_forward_reaction_rate(ost, iters);

  if (reversible)
    emit_avx_reverse_reaction_rate(ost, iters);

  ost << "rr_f[" << (local_idx*iters) << "+idx] = forward;\n";
  for (SpeciesCoeffMap::const_iterator it = forward.begin();
        it != forward.end(); it++)
  {
    assert(it->second > 0); 
    int index = unit->find_ordered_index(it->first);
    // Skip any QSSA species
    if (index == -1)
      continue;
    for (int i = 0; i < it->second; i++)
    {
      ost << "rr_f[" << (local_idx*iters) << "+idx] = _mm256_mul_pd("
          << "rr_f[" << (local_idx*iters) << "+idx],mole_frac["
          << (index*iters) << "+idx]);\n";
    }
  }
  if (reversible)
  {
    ost << "rr_r[" << (local_idx*iters) << "+idx] = reverse;\n"; 
    for (SpeciesCoeffMap::const_iterator it = backward.begin();
          it != backward.end(); it++)
    {
      assert(it->second > 0);
      int index = unit->find_ordered_index(it->first);
      // Skip any QSSA species
      if (index == -1)
        continue;
      for (int i = 0; i < it->second; i++)
      {
        ost << "rr_r[" << (local_idx*iters) << "+idx] = _mm256_mul_pd("
            << "rr_r[" << (local_idx*iters) << "+idx],mole_frac["
            << (index*iters) << "+idx]);\n";
      }
    }
  }
  else
    ost << "rr_r[" << (local_idx*iters) << "+idx] = _mm256_set1_pd(0.0);\n";
  if ((thb != NULL) && !pressure_dep)
  {
    ost << "rr_f[" << (local_idx*iters) << "+idx] = _mm256_mul_pd("
        << "rr_f[" << (local_idx*iters) << "+idx]," << THBCONC
        << "[" << (thb->idx*iters) << "+idx]);\n";
    if (reversible)
     ost << "rr_r[" << (local_idx*iters) << "+idx] = _mm256_mul_pd("
          << "rr_r[" << (local_idx*iters) << "+idx]," << THBCONC
          << "[" << (thb->idx*iters) << "+idx]);\n"; 
  }
}

void Reaction::emit_avx_forward_reaction_rate(CodeOutStream &ost, int iters)
{
  emit_avx_single_forward_reaction_rate(ost, true/*first*/, iters); 
  if (duplicate.enabled)
  {
    assert(duplicate.owner);
    std::list<Reaction*> *duplicates = duplicate.ptr.duplicates;
    if (duplicates != NULL)
    {
      for (std::list<Reaction*>::const_iterator it = duplicates->begin();
            it != duplicates->end(); it++)
      {
        (*it)->emit_avx_single_forward_reaction_rate(ost, false/*first*/, iters);
      }
    }
  }
}

void Reaction::emit_avx_single_forward_reaction_rate(CodeOutStream &ost, bool first, int iters)
{
  if (low.enabled)
  {
    ost << VECTOR4 << " rr_k0, rr_kinf;\n";
    if (unit->use_dim)
      avx_arrhenius(ost, "rr_k0", iters, low.a, low.beta, low.e);
    else
      avx_arrhenius(ost, "rr_k0", iters, (low.a*REACTION_RATE_REF), low.beta, low.e);
    if (unit->use_dim)
      avx_arrhenius(ost, "rr_kinf", iters, a, beta, e);
    else
      avx_arrhenius(ost, "rr_kinf", iters, (a*REACTION_RATE_REF), beta, e);
    ost << VECTOR4 << " pr = _mm256_mul_pd(_mm256_div_pd(rr_k0,rr_kinf),";
    if ((pressure_species != NULL) && (pressure_species != unit->find_species("M")))
    {
      int index = unit->find_ordered_index(pressure_species); 
      assert(index != -1);
      ost << "mole_frac[" << (index*iters) << "+idx]";
    }
    else
    {
      assert(thb != NULL);
      ost << THBCONC << "[" << ((thb == NULL ? 0 : thb->idx)*iters) << "+idx]";
    }
    ost << ");\n";
    if (troe.num > 0)
    {
      ost << VECTOR4 << " fcent;\n";
      PairDelim fcent_pair(ost);
      if (!unit->use_dim)
        ost << "fcent = _mm256_mul_pd(" << TEMPERATURE << "[idx], _mm256_set1_pd(" 
            << (-1.0*TEMPERATURE_REF/troe.t3) << "));\n";
      else
        ost << "fcent = _mm256_mul_pd(" << TEMPERATURE << "[idx], _mm256_set1_pd(" 
            << (-1.0/troe.t3) << "));\n";
      emit_avx_exp_taylor_series_expansion(ost, "fcent", unit->taylor_stages);
      ost << "fcent = _mm256_mul_pd(fcent, _mm256_set1_pd(" << (1.0-troe.a) << "));\n";
      if (troe.a != 0.0)
      {
        PairDelim temp_pair(ost);
        if (!unit->use_dim)
          ost << VECTOR4 << " fctemp = _mm256_mul_pd(" << TEMPERATURE 
              << "[idx], _mm256_set1_pd(" << (-1.0*TEMPERATURE_REF/troe.t1) << "));\n";
        else
          ost << VECTOR4 << " fctemp = _mm256_mul_pd(" << TEMPERATURE
              << "[idx], _mm256_set1_pd(" << (-1.0/troe.t1) << "));\n";
        emit_avx_exp_taylor_series_expansion(ost, "fctemp", unit->taylor_stages);
        ost << "fcent = _mm256_add_pd(fcent, _mm256_mul_pd(fctemp, _mm256_set1_pd(" << (troe.a) << ")));\n";
      }
      if (troe.num == 4)
      {
        PairDelim temp_pair(ost);
        if (!unit->use_dim)
          ost << VECTOR4 << " fctemp = _mm256_mul_pd(otc[idx], _mm256_set1_pd(" << (-troe.t2/TEMPERATURE_REF) << "));\n";
        else
          ost << VECTOR4 << " fctemp = _mm256_mul_pd(otc[idx], _mm256_set1_pd(" << (-troe.t2) << "));\n";
        emit_avx_exp_taylor_series_expansion(ost, "fctemp", unit->taylor_stages);
        ost << "fcent = _mm256_add_pd(fcent, fctemp);\n";
      }
      ost << VECTOR4 << " small = _mm256_set1_pd(" << unit->small_reac_rate << ");\n";
      ost << VECTOR4 << " mask = _mm256_cmp_pd(fcent,small,_CMP_GT_OQ);\n";
      ost << "fcent = _mm256_and_pd(fcent,mask);\n";
      ost << "fcent = _mm256_add_pd(fcent,_mm256_andnot_pd(mask,small));\n";
      {
        PairDelim log10_pair(ost);
        ost << VECTOR2 << " lower = _mm256_extractf128_pd(fcent,0);\n";
        ost << VECTOR2 << " upper = _mm256_extractf128_pd(fcent,1);\n";
        ost << "fcent = _mm256_set_pd("
            << "log10(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
            << "log10(_mm_cvtsd_f64(upper)),"
            << "log10(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
            << "log10(_mm_cvtsd_f64(lower)));\n"; 
      }
      ost << "mask = _mm256_cmp_pd(pr,small,_CMP_GT_OQ);\n";
      ost << VECTOR4 << " flogpr = _mm256_and_pd(pr,mask);\n";
      ost << "flogpr = _mm256_add_pd(flogpr,_mm256_andnot_pd(mask,small));\n";
      {
        ost << VECTOR2 << " lower = _mm256_extractf128_pd(flogpr,0);\n";
        ost << VECTOR2 << " upper = _mm256_extractf128_pd(flogpr,1);\n";
        ost << "flogpr = _mm256_set_pd("
            << "log10(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
            << "log10(_mm_cvtsd_f64(upper)),"
            << "log10(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
            << "log10(_mm_cvtsd_f64(lower)));\n"; 
      }
      ost << "flogpr = _mm256_sub_pd(flogpr, _mm256_add_pd(_mm256_set1_pd(0.4),_mm256_mul_pd(_mm256_set1_pd(0.67),fcent)));\n";
      ost << VECTOR4 << " fdenom = _mm256_add_pd(_mm256_set1_pd(0.75),_mm256_mul_pd(_mm256_set1_pd(-1.27),fcent));\n";
      ost << "fdenom = _mm256_add_pd(fdenom,_mm256_mul_pd(_mm256_set1_pd(-0.14),flogpr));\n";
      ost << VECTOR4 << " fquan = _mm256_div_pd(flogpr,fdenom);\n";
      ost << "fquan = _mm256_div_pd(fcent,_mm256_add_pd(_mm256_set1_pd(1.0),_mm256_mul_pd(fquan,fquan)));\n";
      if (first)
      {
        ost << VECTOR4 << " ftemp = _mm256_mul_pd(fquan,_mm256_set1_pd(DLn10));\n";
        emit_avx_exp_taylor_series_expansion(ost, "ftemp", unit->taylor_stages);
        ost << "forward = _mm256_mul_pd(rr_kinf,pr);\n";
        ost << "forward = _mm256_div_pd(forward,_mm256_add_pd(_mm256_set1_pd(1.0),pr));\n";
        ost << "forward = _mm256_mul_pd(forward, ftemp);\n";
      }
      else
      {
        ost << VECTOR4 << " ftemp = _mm256_mul_pd(fquan,_mm256_set1_pd(DLn10));\n";
        emit_avx_exp_taylor_series_expansion(ost, "ftemp", unit->taylor_stages);
        ost << VECTOR4 << " ftemp2 = _mm256_mul_pd(rr_kinf,pr);\n";
        ost << "ftemp2 = _mm256_div_pd(ftemp2,_mm256_add_pd(_mm256_set1_pd(1.0),pr));\n";
        ost << "forward = _mm256_add_pd(forward, _mm256_mul_pd(ftemp2,ftemp));\n";
      }
    }
    else if (sri.num > 0)
    {
      // TODO: handle power in SRI
      assert(false);
    }
    else
    {
      // plain old Lindermann
      if (first)
        ost << "forward = _mm256_div_pd(_mm256_mul_pd(rr_kinf,pr),_mm256_add_pd(_mm256_set1_pd(1.0),pr));\n";
      else
        ost << "forward = _mm256_add_pd(forward,_mm256_div_pd(_mm256_mul_pd(rr_kinf,pr),_mm256_add_pd(_mm256_set1_pd(1.0),pr)));\n";
    }
  }
  else if (lt.enabled)
  {
    // Laundau-Teller
    if (!unit->use_dim)
    {
        ost << VECTOR4 << " scaleotc = _mm256_mul_pd(otc[idx],_mm256_set1_pd(" 
            << (1.0/TEMPERATURE_REF) << "));\n";
        ost << VECTOR4 << " ftroot;\n";
        {
          PairDelim root_pair(ost);
          ost << VECTOR2 << " lower = _mm256_extractf128_pd(scaleotc,0);\n";
          ost << VECTOR2 << " upper = _mm256_extractf128_pd(scaleotc,1);\n";
          ost << "ftroot = _mm256_set_pd("
              << "pow(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)),(1./3.)),"
              << "pow(_mm_cvtsd_f64(upper),(1./3.)),"
              << "pow(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)),(1./3.)),"
              << "pow(_mm_cvtsd_f64(lower),(1./3.)));\n"; 
        }
    }
    else
    {
      ost << VECTOR4 << " ftroot;\n";
      PairDelim root_pair(ost);
      ost << VECTOR2 << " lower = _mm256_extractf128_pd(otc[idx],0);\n";
      ost << VECTOR2 << " upper = _mm256_extractf128_pd(otc[idx],1);\n";
      ost << "ftroot = _mm256_set_pd("
          << "pow(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)),(1./3.)),"
          << "pow(_mm_cvtsd_f64(upper),(1./3.)),"
          << "pow(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)),(1./3.)),"
          << "pow(_mm_cvtsd_f64(lower),(1./3.)));\n"; 
    }
    ost << VECTOR4 << " etemp = _mm256_mul_pd(_mm256_set1_pd(" << beta << "),vlntemp);\n";
    ost << "etemp = _mm256_sub_pd(etemp,_mm256_mul_pd(_mm256_set1_pd(" << (unit->use_dim ? e : (e/TEMPERATURE_REF)) << "),otc[idx]));\n";
    ost << "etemp = _mm256_add_pd(etemp,_mm256_mul_pd(_mm256_set1_pd(" << lt.b << "),ftroot));\n";
    ost << "etemp = _mm256_add_pd(etemp,_mm256_mul_pd(_mm256_set1_pd(" << lt.c << "),_mm256_mul_pd(ftroot,ftroot)));\n";
    emit_avx_exp_taylor_series_expansion(ost, "etemp", unit->taylor_stages);
    if (first)
      ost << "forward = _mm256_mul_pd(_mm256_set1_pd(" << (!unit->use_dim ? (REACTION_RATE_REF * a * pow(TEMPERATURE_REF,beta)) : a) << "),etemp);\n"; 
    else
      ost << "forward = _mm256_add_pd(forward,_mm256_mul_pd(_mm256_set1_pd(" << (!unit->use_dim ? (REACTION_RATE_REF * a * pow(TEMPERATURE_REF,beta)) : a) << "),etemp));\n";
  }
  else
  {
    PairDelim next_pair(ost);
    ost << VECTOR4 << " next;\n";
    if (!unit->use_dim)
      avx_arrhenius(ost, "next", iters, (a*REACTION_RATE_REF), beta, e);
    else
      avx_arrhenius(ost, "next", iters, a, beta, e);
    if (first)
      ost << "forward = next;\n";
    else
      ost << "forward = _mm256_add_pd(forward,next);\n";
  }
}

void Reaction::emit_avx_reverse_reaction_rate(CodeOutStream &ost, int iters)
{
  if (rev.enabled)
  {
    if (rlt.enabled)
    {
      if (!lt.enabled)
      {
        if (unit->use_dim)
        {
          ost << VECTOR4 << " ftroot;\n";
          PairDelim root_pair(ost);
          ost << VECTOR2 << " lower = _mm256_extractf128_pd(otc,0);\n";
          ost << VECTOR2 << " upper = _mm256_extractf128_pd(otc,1);\n";
          ost << "ftroot = _mm256_set_pd("
              << "pow(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)),(1./3.)),"
              << "pow(_mm_cvtsd_f64(upper),(1./3.)),"
              << "pow(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)),(1./3.)),"
              << "pow(_mm_cvtsd_f64(lower),(1./3.)));\n"; 
        }
        else
        {
          ost << VECTOR4 << " ftroot = _mm256_div_pd(otc,_mm256_set1_pd(" 
              << TEMPERATURE_REF << "));\n";
          PairDelim root_pair(ost);
          ost << VECTOR2 << " lower = _mm256_extractf128_pd(otc,0);\n";
          ost << VECTOR2 << " upper = _mm256_extractf128_pd(otc,1);\n";
          ost << "ftroot = _mm256_set_pd("
              << "pow(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)),(1./3.)),"
              << "pow(_mm_cvtsd_f64(upper),(1./3.)),"
              << "pow(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)),(1./3.)),"
              << "pow(_mm_cvtsd_f64(lower),(1./3.)));\n"; 
        }
      }
      ost << VECTOR4 << " forward = _mm256_mul_pd(_mm256_set1_pd(" << beta << "),vlntemp);\n";
      ost << "forward = _mm256_sub_pd(forward,_mm256_mul_pd(_mm256_set1_pd(";
      if (unit->use_dim)
        ost << e;
      else
        ost << (e/TEMPERATURE_REF);
      ost << "),otc));\n";
      ost << "forward = _mm256_add_pd(rr_f,_mm256_mul_pd(_mm256_set1_pd(" << rlt.b << "),ftroot));\n";
      ost << "forward = _mm256_add_pd(rr_f,_mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(" << rlt.c << "),ftroot),ftroot));\n";
    }
    else
    {
      if (!unit->use_dim)
        avx_arrhenius(ost, "reverse", iters, (REACTION_RATE_REF*rev.a), rev.beta, rev.e);
      else
        avx_arrhenius(ost, "reverse", iters, rev.a, rev.beta, rev.e);
    }
  }
  else
  {
    /* use equilibrium relation */
    /* total nu */
    int nutot = 0;
    for (SpeciesCoeffMap::const_iterator it = stoich.begin();
          it != stoich.end(); it++)
      nutot += it->second;
    ost << VECTOR4 << " xik = ";
    bool first_invocation = true;
    for (SpeciesCoeffMap::const_iterator it = stoich.begin();
          it != stoich.end(); it++)
    {
      // Subtract one because we ignore the third body species
      int spec_id = it->first->idx-1;
      if (first_invocation)
      {
        first_invocation = false;
        if (it->second > 1)
        {
          ost << "_mm256_mul_pd(_mm256_set1_pd(double(" << it->second << "))," 
              << GIBBS << "[" << (spec_id*iters) << "+idx]);\n";
        }
        else if (it->second == -1)
        {
          // do fast negation
          ost << "_mm256_xor_pd(_mm256_set1_pd(-0.0)," 
              << GIBBS << "[" << (spec_id*iters) << "+idx]);\n";
        }
        else if (it->second < -1)
        {
          ost << "_mm256_mul_pd(_mm256_set1_pd(double(" << it->second << "))," 
              << GIBBS << "[" << (spec_id*iters) << "+idx]);\n";
        }
        else
        {
          assert(it->second == 1);
          ost << GIBBS << "[" << (spec_id*iters) << "+idx];\n";
        }
      }
      else
      {
        if (it->second == 1)
        {
          ost << "xik = _mm256_add_pd(xik," << GIBBS << "[" 
              << (spec_id*iters) << "+idx]);\n";
        }
        else if (it->second > 1)
        {
          ost << "xik = _mm256_add_pd(xik,_mm256_mul_pd(_mm256_set1_pd(double(" 
              << it->second << "))," << GIBBS << "[" << (spec_id*iters) << "+idx]));\n";
        }
        else if (it->second == -1)
        {
          ost << "xik = _mm256_sub_pd(xik," << GIBBS << "[" << (spec_id*iters) << "+idx]);\n";
        }
        else if (it->second < -1)
        {
          ost << "xik = _mm256_add_pd(xik,_mm256_mul_pd(_mm256_set1_pd(double(" 
              << it->second << "))," << GIBBS << "[" << (spec_id*iters) << "+idx]));\n";
        }
      }
    }
    ost << "reverse = _mm256_mul_pd(xik,otc[idx]);\n";
    if ((nutot > 2) || (nutot < -2))
    {
      ost << "reverse = _mm256_add_pd(reverse,_mm256_mul_pd(_mm256_set1_pd(" << nutot << "),vlntemp));\n";
    }
    emit_avx_exp_taylor_series_expansion(ost, "reverse", unit->taylor_stages);
    ost << "reverse = _mm256_mul_pd(forward,reverse);\n";
    {
        if (nutot == 0)
        {
            //ost << ")";
        }
        else if (nutot == 1)
        {
            //ost << ") * oprt" << POSTFIX;
            ost << "reverse = _mm256_mul_pd(reverse,oprt[idx]);\n";
        }
        else if (nutot == 2)
        {
            //ost << ") * oprt" << POSTFIX << " * oprt" << POSTFIX;
            ost << "reverse = _mm256_mul_pd(_mm256_mul_pd(reverse,oprt[idx]),oprt[idx]);\n";
        }
        else if (nutot == -1)
        {
            //ost << ") * prt" << POSTFIX;
            ost << "reverse = _mm256_mul_pd(reverse,prt[idx]);\n";
        }
        else if (nutot == -2)
        {
            //ost << ") * prt" << POSTFIX << " * prt" << POSTFIX;
            ost << "reverse = _mm256_mul_pd(_mm256_mul_pd(reverse,prt[idx]),prt[idx]);\n";
        }
        else if (nutot > 0)
        {
            //ost.print(" - %d.0*vlntemp%s)", nutot,POSTFIX);
            if (!unit->use_dim)
              assert(false); // handle this case
        }
        else 
        {
            //ost.print(" + %d.0*vlntemp%s)", -nutot,POSTFIX);
            if (!unit->use_dim)
              assert(false); // handle this case
        }
    }
  }
}

void Reaction::avx_arrhenius(CodeOutStream &ost, const char *var_name, int iters,
                             double prefix, double beta, double e)
{
  PairDelim arr_pair(ost);
  if (e == 0.)
  {
    if (beta == 0.)
    {
      ost << VECTOR4 << " arrtemp = _mm256_set1_pd(" << prefix << ");\n"; //ost << prefix;
    }
    else if (beta == 1.)
    {
      if (!unit->use_dim)
      {
        //ost << (prefix * TEMPERATURE_REF) << " * " << TEMPERATURE;
        ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << (prefix * TEMPERATURE_REF) << ")," << TEMPERATURE << "[idx]);\n";
      }
      else
      {
        //ost << prefix << " * " << TEMPERATURE;
        ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << prefix << "),"
            << TEMPERATURE << "[idx]);\n";
      }
    }
    else if (beta == 2.)
    {
      if (!unit->use_dim)
      {
        //ost << (prefix*TEMPERATURE_REF*TEMPERATURE_REF) << " * " << TEMPERATURE << " * " << TEMPERATURE;
        ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(" 
            << (prefix*TEMPERATURE_REF*TEMPERATURE_REF) << "),"
            << TEMPERATURE << "[idx]),"
            << TEMPERATURE << "[idx]);\n";
      }
      else
      {
        //ost << prefix << " * " << TEMPERATURE << " * " << TEMPERATURE;
        ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(" << prefix << "),"
            << TEMPERATURE << "[idx]), " << TEMPERATURE << "[idx]);\n";
      }
    }
    else if (beta == -1.)
    {
      if (!unit->use_dim)
      {
        //ost << (prefix / TEMPERATURE_REF) << " * otc";
        ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << (prefix/TEMPERATURE_REF) << "),otc[idx]);\n";
      }
      else
      {
        //ost << prefix << " * otc";
        ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << prefix << "),otc[idx]);\n";
      }
    }
    else if (beta == -2.)
    {
      if (!unit->use_dim)
      {
        //ost << (prefix / (TEMPERATURE_REF * TEMPERATURE_REF)) << " * otc * otc";
        ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(" << (prefix/(TEMPERATURE_REF*TEMPERATURE_REF)) << "),otc[idx]),otc[idx]);\n";
      }
      else 
      {
        //ost << prefix << " * otc * otc";
        ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(" << prefix << "),otc[idx]),otc[idx]);\n";
      }
    }
    else
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix*pow(TEMPERATURE_REF, beta));
      else
        ost << prefix;
      ost.print(" * %s(", GETEXP); 
      ost << beta << " * vlntemp)";
#endif
      ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << beta << "),vlntemp[idx]);\n";
      emit_avx_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      if (!unit->use_dim)
        ost << "arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << (prefix*pow(TEMPERATURE_REF,beta)) << "), arrtemp);\n";
      else
        ost << "arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << prefix << "), arrtemp);\n";
    }
  }
  else
  {
    if (beta == 0.)
    {
#if 0
      ost << prefix;
      ost.print(" * %s(", GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << (-e) << "), ortc[idx]);\n";
      emit_avx_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      ost << "arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << prefix << "),arrtemp);\n";
    }
    else if (beta == 1.)
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix * TEMPERATURE_REF);
      else
        ost << prefix;
      ost.print(" * %s * %s(", TEMPERATURE, GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << (-e) << "), ortc[idx]);\n";
      emit_avx_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      ost << "arrtemp = _mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(" << (prefix*TEMPERATURE_REF) << "),"
          << TEMPERATURE << "[idx]),arrtemp);\n";
    }
    else if (beta == -1.)
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix / TEMPERATURE_REF);
      else
        ost << prefix;
      ost.print(" * otc * %s(", GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << (-e) << "), ortc[idx]);\n";
      emit_avx_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      ost << "arrtemp = _mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(" << (prefix/TEMPERATURE_REF) << "), otc[idx]),arrtemp);\n";
    }
    else if (beta == 2.)
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix*TEMPERATURE_REF*TEMPERATURE_REF);
      else
        ost << prefix;
      ost.print(" * %s * %s * %s(", TEMPERATURE, TEMPERATURE, GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << (-e) << "),ortc[idx]);\n";
      emit_avx_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      ost << "arrtemp = _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(" 
          << (prefix*TEMPERATURE_REF*TEMPERATURE_REF)
          << ")," << TEMPERATURE << "[idx]),"
          << TEMPERATURE << "[idx]),arrtemp);\n";
    }
    else if (beta == -2.)
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix/(TEMPERATURE_REF*TEMPERATURE_REF));
      else
        ost << prefix;
      ost.print(" * otc * otc * %s(", GETEXP);
      ost << -e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << (-e) << "),ortc[idx]);\n";
      emit_avx_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      ost << "arrtemp = _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(" << (prefix/(TEMPERATURE_REF*TEMPERATURE_REF))
          << "),otc[idx]),otc[idx]),arrtemp);\n";
    }
    else
    {
#if 0
      if (!unit->no_nondim)
        ost << (prefix*pow(TEMPERATURE_REF,beta));
      else
        ost << prefix;
      ost.print(" * %s(", GETEXP);
      ost << beta << "*vlntemp ";
      if (e < 0.)
        ost << "+ " << -e;
      else
        ost << "- " << e;
      //if (unit->no_nondim)
        ost << "*ortc)";
      //else
      //  ost << "*otc)";
#endif
      ost << VECTOR4 << " arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << beta << "),vlntemp[idx]);\n";
      if (e < 0.)
        ost << "arrtemp = _mm256_add_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(" << (-e) << "),ortc[idx]));\n";
      else
        ost << "arrtemp = _mm256_sub_pd(arrtemp,_mm256_mul_pd(_mm256_set1_pd(" << (e) << "),ortc[idx]));\n";
      emit_avx_exp_taylor_series_expansion(ost, "arrtemp", unit->taylor_stages);
      if (!unit->use_dim)
        ost << "arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << (prefix*pow(TEMPERATURE_REF,beta)) << "),arrtemp);\n";
      else
        ost << "arrtemp = _mm256_mul_pd(_mm256_set1_pd(" << prefix << "),arrtemp);\n";
    }
  }
  ost << var_name << " = arrtemp;\n";
}

void Reaction::emit_warp_specialized_weird_reaction(CodeOutStream &ost, int level, int buffer)
{
  if (low.enabled)
  {
    // Litte hack here, use buffer to figure out which version rr_* to use
    if (buffer > -1)
    {
      ost << REAL << " rr_k0 = rr_f[" << level << "];\n";
      ost << REAL << " rr_kinf = rr_r[" << level << "];\n";
    }
    else
    {
      ost << REAL << " rr_k0 = rr_f_" << level << ";\n";
      ost << REAL << " rr_kinf = rr_r_" << level << ";\n";
    }
    /* calculate pr */
    ost << REAL << " pr = rr_k0 / rr_kinf * ";
    if ((pressure_species != NULL) && (pressure_species != unit->find_species("M")))
    {
      int index = unit->find_ordered_index(pressure_species); 
      assert(index != -1);
      if (buffer > -1)
        ost << "buffer" << buffer << "[" << index << "][tid]";
      else
        ost << "scratch[" << index << "][tid]";
    }
    else
    {
      assert(thb != NULL);
      if (buffer > -1)
        ost << "buffer" << buffer << "[" << (unit->ordered_species.size() + (thb == NULL ? 0 : thb->idx)) << "][tid]";
      else
        ost << "scratch[" << (unit->ordered_species.size() + (thb == NULL ? 0 : thb->idx)) << "][tid]";
    }
    ost << ";\n";
    if (troe.num > 0)
    {
      const int troe_idx = unit->find_troe_index(this);
      assert(troe_idx != -1);
#if 0
      if (buffer > -1)
      {
        ost << REAL << " fcent = log10(troe_constants[" << troe_idx << "+(step+" << buffer << ")*step_stride][0] * exp(";
        ost << "troe_constants[" << troe_idx << "+(step+" << buffer << ")*step_stride][1] * " << TEMPERATURE << ")";
      }
      else
      {
        ost << REAL << " fcent = log10(troe_constants[" << troe_idx << "+step*step_stride][0] * exp(";
        ost << "troe_constants[" << troe_idx << "+step*step_stride][1] * " << TEMPERATURE << ")";
      }
#else
      ost << REAL << " fcent = log10(MAX(" << (1.0-troe.a) << " * exp(";
      if (!unit->use_dim)
        ost << (-1.0*TEMPERATURE_REF/troe.t3) << " * " << TEMPERATURE << ")";
      else
        ost << (-1.0/troe.t3) << " * " << TEMPERATURE << ")";
#endif
      // Check for 0.0 here since in some cases
      // if we emit it we end up getting -inf for -1.0/troe.t1
      if (troe.a != 0.0)
      {
#if 0
        if (buffer > -1)
          ost << " + troe_constants[" << troe_idx << "+(step+" << buffer << ")*step_stride][2]"
              << " * exp(troe_constants[" << troe_idx << "+(step+" << buffer << ")*step_stride][3] * " << TEMPERATURE << ")";
        else
          ost << " + troe_constants[" << troe_idx << "+step*step_stride][2]"
              << " * exp(troe_constants[" << troe_idx << "+step*step_stride][3] * " << TEMPERATURE << ")";
#else
        if (!unit->use_dim)
          ost << " + " << troe.a << " * exp(" 
              << (-1.0*TEMPERATURE_REF/troe.t1) << " * " << TEMPERATURE << ")";
        else
          ost << " + " << troe.a << " * exp(" << (-1.0/troe.t1) << " * " 
              << TEMPERATURE << ")";
#endif
      }
      if (troe.num == 4)
      {
#if 0
        if (buffer > -1)
          ost << " + exp(troe_constants[" << troe_idx << "+(step+" << buffer << ")*step_stride][4] * otc)";
        else
          ost << " + exp(troe_constants[" << troe_idx << "+step*step_stride][4] * otc)";
#else
        if (unit->use_dim)
          ost << " + exp(" << (-troe.t2) << " * otc)"; 
        else
          ost << " + exp(" << (-troe.t2/TEMPERATURE_REF) << " * otc)";
#endif
      }
      ost << "," << unit->small_reac_rate << "));\n";
      ost << REAL << " flogpr = log10(MAX(pr," << unit->small_reac_rate << ")) - 0.4 - 0.67 * fcent;\n";
      ost << REAL << " fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;\n";
      ost << REAL << " fquan = flogpr / fdenom;\n";
      ost << "fquan = fcent / (1.0 + fquan * fquan);\n";
      if (buffer > -1)
        ost << "rr_f[" << level << "] = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);\n";
      else
        ost << "rr_f_" << level << " = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);\n";
    }
    else if (sri.num > 0)
    {
      ost << REAL << " fpexp = log10(pr);\n";
      ost << REAL << " fpexp = 1. / (1. + fpexp * fpexp);\n";
      ost << REAL << " fquan = (";
      ost << sri.a;
      ost << " * exp(";
      if (unit->use_dim)
        ost << (-sri.b);
      else
        ost << (-sri.b/TEMPERATURE_REF);
      ost << "*otc) + exp(";
      ost << (-1./sri.c);
      assert(false); // need to handle power
      ost << "*tc)) ** fpexp;\n";
      if (sri.num == 5) {
          ost << " fquan = fquan * ";
          ost << sri.d;
          ost << " * pow(" << TEMPERATURE <<",";
          ost << sri.e;
          ost << ");\n";
      }
      if (buffer > -1)
        ost << "rr_f[" << level << "] = rr_kinf * pr/(1.0 + pr) * fquan;\n";
      else
        ost << "rr_f_" << level << " = rr_kinf * pr/(1.0 + pr) * fquan;\n";
    }
    else
    {
      // plain old lindermann
      if (buffer > -1)
        ost << "rr_f[" << level << "] = rr_kinf * pr/(1.0 + pr);\n";
      else
        ost << "rr_f_" << level << " = rr_kinf * pr/(1.0 + pr);\n";
    }
  }
  else // Not low-enabled
  {
    // Handle the duplicate case
    if (duplicate.enabled && (duplicate.ptr.duplicates != NULL))
    {
      assert(duplicate.owner);
      // This would be the place to handle additional duplicates if necessary
      assert(duplicate.ptr.duplicates->size() == 1);
      if (buffer > -1)
        ost << "rr_f[" << level << "] += rr_r[" << level << "];\n";
      else
        ost << "rr_r_" << level << " += rr_r_" << level << ";\n";
    }
  }
  // Now do the backwards part
  assert(!rev.enabled);
  int nutot = 0;
  for (SpeciesCoeffMap::const_iterator it = stoich.begin();
        it != stoich.end(); it++)
    nutot += it->second;
  /* total stoich gibbs energy */
  ost << REAL << " xik = ";
  bool first = true;
  for (SpeciesCoeffMap::const_iterator it = stoich.begin();
        it != stoich.end(); it++)
  {
    if (first)
    {
      first = false;
      if (it->second > 1)
      {
        ost.print("%d.0 * ", it->second);
      }
      else if (it->second == -1)
      {
        ost << "-";
      }
      else if (it->second < -1)
      {
        ost.print("-%d.0 * ", -(it->second));
      }
    }
    else
    {
      if (it->second == 1)
      {
        ost << " + ";
      }
      else if (it->second > 1)
      {
        ost.print(" + %d.0 * ", it->second);
      }
      else if (it->second == -1)
        ost << " - ";
      else if (it->second < -1)
      {
        ost.print(" - %d.0 * ", -it->second);
      }
    }
    // Subtract one because we ignore the third body species
    if (!unit->spill_gibbs) {
      int index = unit->ordered_species.size() + unit->third_bodies.size() + unit->find_gibbs_index(it->first);
      assert(index != -1);
      ost << "scratch[" << index << "][tid]";
    } else {
      int index = unit->find_gibbs_index(it->first);
      assert(index != -1);
      assert(unsigned(index) < unit->gibbs_species.size());
      ost << "gibbs_g[smid*" << unit->gibbs_species.size() << "+" << index << "][tid]";
    }
  }
  if (first)
    ost << "0.0";
  ost << ";\n";
  if (buffer > -1)
    ost << "rr_r[" << level << "] = rr_f[" << level << "]";
  else
    ost << "rr_r_" << level << " = rr_f_" << level;
  // This case of otc gets handled by folding it back through
  // all the gibbs free energy constants
  ost.print(" * %s(xik*otc", GETEXP);
  //if (unit->no_nondim) 
  {
    if (nutot == 0)
        ost << ")";
    else if (nutot == 1)
    {
        ost << ") * oprt";
    }
    else if (nutot == 2)
    {
        ost << ") * oprt * oprt";
    }
    else if (nutot == -1)
    {
        ost << ") * prt";
    }
    else if (nutot == -2)
    {
        ost << ") * prt * prt";
    }
    else if (nutot > 0)
    {
        if (unit->use_dim)
          ost.print(" - %d.0*vlntemp)", nutot);
        else
          ost << " - " << nutot << ".0*(vlntemp + " << TEMPERATURE_REF << "))";
    }
    else 
    {
        if (unit->use_dim)
          ost.print(" + %d.0*vlntemp)", -nutot);
        else
          ost << " + " << (-nutot) << ".0*(vlntemp + " << TEMPERATURE_REF << "))";
    }
    ost << ";\n";
  } 
}

// Species

void Species::emit_contributions(CodeOutStream &ost, unsigned local_idx, Profiler *profiler)
{
  ost << "// " << local_idx << ". " << name << "\n";
  ost << WDOT << "[" << local_idx << "]" << " = ";
  if (reaction_contributions.empty())
  {
    ost << "0.0;\n";
    return;
  }
  if (profiler != NULL)
    profiler->writes(1);
  if (!unit->use_dim)
  {
    ost << (molecular_mass/1000.0) << " * (";
    if (profiler != NULL)
      profiler->multiplies(1);
  }
  bool first = true;
  char coef_string[256];
  for (std::map<Reaction*,int>::const_iterator it = reaction_contributions.begin();
        it != reaction_contributions.end(); it++)
  {
    // Empty string in case we don't print anything
    coef_string[0] = '\0';
    if (first)
    {
      first = false;
      if (it->second > 1)
        snprintf(coef_string,256,"%d.0*", it->second);
      else if (it->second == -1)
        snprintf(coef_string,256,"-");
      else if (it->second < -1)
        snprintf(coef_string,256,"-%d.0*", -it->second);
      if ((profiler != NULL) && (it->second != -1))
        profiler->multiplies(1);
    }
    else
    {
      if (it->second == 1)
        snprintf(coef_string,256," + ");
      else if (it->second > 1)
        snprintf(coef_string,256," + %d.0*", it->second);
      else if (it->second == -1)
        snprintf(coef_string,256," - ");
      else if (it->second < -1)
        snprintf(coef_string,256," - %d.0*", -it->second);
      if (profiler != NULL)
      {
        if (it->second >= 1)
          profiler->adds(1);
        else
          profiler->subtracts(1);
        if ((it->second > 1) || (it->second < -1))
          profiler->multiplies(1);
      }
    }
    ost.print("%sropl[%d]", coef_string, it->first->reaction_idx);
    if (profiler != NULL)
      profiler->reads(1);
  }
  if (!unit->use_dim)
    ost << ")";
  ost << ";\n";
}

void Species::emit_sse_contributions(CodeOutStream &ost, unsigned local_idx, int iters)
{
  ost << "// " << local_idx << ". " << name << "\n";
  PairDelim spec_pair(ost);
  ost << VECTOR2 << " result = _mm_set1_pd(0.0);\n";
  for (std::map<Reaction*,int>::const_iterator it = reaction_contributions.begin();
        it != reaction_contributions.end(); it++)
  {
    if (it->second == 1)
    {
      ost << "result = _mm_add_pd(result,ropl[" << it->first->reaction_idx << "]);\n";
    }
    else if (it->second > 1)
    {
      ost << "result = _mm_add_pd(result,_mm_mul_pd(_mm_set1_pd("
          << it->second << ".0),ropl[" << it->first->reaction_idx << "]));\n";
    }
    else if (it->second == -1)
    {
      ost << "result = _mm_sub_pd(result,ropl[" << it->first->reaction_idx << "]);\n";
    }
    else if (it->second < -1)
    {
      ost << "result = _mm_sub_pd(result,_mm_mul_pd(_mm_set1_pd("
          << (-it->second) << ".0),ropl[" << it->first->reaction_idx << "]));\n";
    }
  }
  if (!unit->use_dim)
    ost << "result = _mm_mul_pd(result,_mm_set1_pd(" << (molecular_mass/1000.0) << "));\n";
  // Now write out the result
  ost << "_mm_store_pd(" << WDOT_ARRAY << "+(" << local_idx << "*spec_stride)+(idx<<1),"
      << "result);\n";
}

void Species::emit_avx_contributions(CodeOutStream &ost, unsigned local_idx, int iters)
{
  ost << "// " << local_idx << ". " << name << "\n";
  PairDelim spec_pair(ost);
  ost << VECTOR4 << " result = _mm256_set1_pd(0.0);\n";
  for (std::map<Reaction*,int>::const_iterator it = reaction_contributions.begin();
        it != reaction_contributions.end(); it++)
  {
    if (it->second == 1)
    {
      ost << "result = _mm256_add_pd(result,ropl[" << it->first->reaction_idx << "]);\n";
    }
    else if (it->second > 1)
    {
      ost << "result = _mm256_add_pd(result,_mm256_mul_pd(_mm256_set1_pd("
          << it->second << ".0),ropl[" << it->first->reaction_idx << "]));\n";
    }
    else if (it->second == -1)
    {
      ost << "result = _mm256_sub_pd(result,ropl[" << it->first->reaction_idx << "]);\n";
    }
    else if (it->second < -1)
    {
      ost << "result = _mm256_sub_pd(result,_mm256_mul_pd(_mm256_set1_pd("
          << (-it->second) << ".0),ropl[" << it->first->reaction_idx << "]));\n";
    }
  }
  if (!unit->use_dim)
    ost << "result = _mm256_mul_pd(result,_mm256_set1_pd(" << (molecular_mass/1000.0) << "));\n";
  // Now write out the result
  ost << "if (aligned<32>(" WDOT_ARRAY << "))\n";
  ost << "_mm256_stream_pd(" << WDOT_ARRAY << "+(" << local_idx << "*spec_stride)+(idx<<2),"
      << "result);\n";
  ost << "else\n";
  ost << "_mm256_storeu_pd(" << WDOT_ARRAY << "+(" << local_idx << "*spec_stride)+(idx<<2),"
      << "result);\n";
}

// Chemistry

void ChemistryUnit::emit_stiffness_information(CodeOutStream &ost)
{
  ost << "#define GETRATES_STIFF_SPECIES " << stif_operations.size() << "\n";
  ost << "\n";
  ost << "const unsigned int stif_species_indexes[" << stif_operations.size() << "] = {";
  bool first = true;
  for (std::vector<Stif*>::const_iterator it = stif_operations.begin();
        it != stif_operations.end(); it++)
  {
    if (!first)
      ost << ", ";
    else
      first = false;
    ost << unit->find_ordered_index((*it)->species);
  }
  ost << "};\n";
}

bool ChemistryUnit::is_qssa_reaction(Reaction *reac) const
{
  for (std::vector<ConnectedComponent*>::const_iterator it = 
        connected_components.begin(); it !=
        connected_components.end(); it++)
  {
    if ((*it)->is_qssa_reaction(reac))
      return true;
  }
  return false;
}

int ChemistryUnit::find_qss_index(Reaction *reac, bool forward) const
{
  for (std::vector<ConnectedComponent*>::const_iterator it = 
        connected_components.begin(); it !=
        connected_components.end(); it++)
  {
    int loc = (*it)->find_qss_index(reac, forward);
    if (loc != -1)
      return loc;
  }
  return -1;
}

void ChemistryUnit::emit_unimportant_rates(CodeOutStream &ost, 
                                           Profiler *profiler)
{
  ost << "// Unimportant reaction rates\n";
  // Emit the fowrad and backward unimportant rates
  for (std::set<Reaction*>::const_iterator it = forward_unimportant.begin();
        it != forward_unimportant.end(); it++)
  {
    ost << "rr_f[" << ((*it)->idx) << "] = 0.0;\n";
    if (profiler != NULL)
      profiler->writes(1);
  }
  for (std::set<Reaction*>::const_iterator it = backward_unimportant.begin();
        it != backward_unimportant.end(); it++)
  {
    ost << "rr_r[" << ((*it)->idx) << "] = 0.0;\n";
    if (profiler != NULL)
      profiler->writes(1);
  }
}

void ChemistryUnit::emit_qssa_modifications(CodeOutStream &ost, Profiler *profiler)
{
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->emit_statements(ost, profiler); 
  }
}

void ChemistryUnit::emit_stiffness_modifications(CodeOutStream &ost, Profiler *profiler)
{
  for (std::vector<Stif*>::const_iterator it = stif_operations.begin();
        it != stif_operations.end(); it++)
  {
    (*it)->emit_statements(ost, unit, profiler);
  }
}

void ChemistryUnit::emit_sse_unimportant_rates(CodeOutStream &ost, bool main, int iters)
{
  ost << "// Unimportant rates\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
  PairDelim for_pair(ost);
  for (std::set<Reaction*>::const_iterator it = forward_unimportant.begin();
        it != forward_unimportant.end(); it++)
  {
    ost << "rr_f[" << (((*it)->idx)*iters) << "+idx] = _mm_set1_pd(0.0);\n";
  }
  for (std::set<Reaction*>::const_iterator it = backward_unimportant.begin();
        it != backward_unimportant.end(); it++)
  {
    ost << "rr_r[" << (((*it)->idx)*iters) << "+idx] = _mm_set1_pd(0.0);\n";
  }
}

void ChemistryUnit::emit_sse_qssa_modifications(CodeOutStream &ost, bool main, int iters)
{
  ost << "// QSSA Scaling\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
  PairDelim for_pair(ost);
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->emit_sse_statements(ost, iters); 
  }
}

void ChemistryUnit::emit_sse_stiffness_modifications(CodeOutStream &ost, bool main, int iters)
{
  ost << "// Stiffness Scaling\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
  PairDelim for_pair(ost);
  for (std::vector<Stif*>::const_iterator it = stif_operations.begin();
        it != stif_operations.end(); it++)
  {
    (*it)->emit_sse_statements(ost, unit, iters);
  }
}

void ChemistryUnit::emit_avx_unimportant_rates(CodeOutStream &ost, bool main, int iters)
{
  ost << "// Unimportant rates\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
  PairDelim for_pair(ost);
  for (std::set<Reaction*>::const_iterator it = forward_unimportant.begin();
        it != forward_unimportant.end(); it++)
  {
    ost << "rr_f[" << (((*it)->idx)*iters) << "+idx] = _mm256_set1_pd(0.0);\n";
  }
  for (std::set<Reaction*>::const_iterator it = backward_unimportant.begin();
        it != backward_unimportant.end(); it++)
  {
    ost << "rr_r[" << (((*it)->idx)*iters) << "+idx] = _mm256_set1_pd(0.0);\n";
  }
}

void ChemistryUnit::emit_avx_qssa_modifications(CodeOutStream &ost, bool main, int iters)
{
  ost << "// QSSA Scaling\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
  PairDelim for_pair(ost);
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->emit_avx_statements(ost, iters); 
  }
}

void ChemistryUnit::emit_avx_stiffness_modifications(CodeOutStream &ost, bool main, int iters)
{
  ost << "// Stiffness Scaling\n";
  if (main)
    ost << "for (unsigned idx = 0; idx < " << iters << "; idx++)\n";
  else
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
  PairDelim for_pair(ost);
  for (std::vector<Stif*>::const_iterator it = stif_operations.begin();
        it != stif_operations.end(); it++)
  {
    (*it)->emit_avx_statements(ost, unit, iters);
  }
}

void ChemistryUnit::construct_qss_graph(std::vector<QSS*> &qss_nodes)
{
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->construct_qss_graph(qss_nodes);
  }
}

// ConnectedComponent

bool ConnectedComponent::is_qssa_reaction(Reaction *reac) const
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    if (it->second->is_qssa_reaction(reac))
      return true;
  }
  return false;
}

int ConnectedComponent::find_qss_index(Reaction *reac, bool forward) const
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    int loc = it->second->find_qss_index(reac, forward);
    if (loc != -1)
      return loc;
  }
  return -1;
}

void ConnectedComponent::emit_statements(CodeOutStream &ost, Profiler *profiler)
{
  ost << "// QSSA connected component\n";
  PairDelim qssa_pair(ost);
  std::set<std::pair<int,int> > already_emitted;
  // First tell all the species to emit their statements
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_statements(ost,already_emitted,profiler); 
  }
  // Emit forward declarations for all the X values
  ost << REAL << " den";
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    ost << ", xq_" << it->first;
  }
  ost << ";\n";
  // Now we can emit the statements for this connected component
  for (std::vector<Statement*>::const_iterator it = statements.begin();
        it != statements.end(); it++)
  {
    (*it)->emit_code(ost, already_emitted, profiler); 
  }
  // Finally update all the reactions 
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_updates(ost, profiler);
  }
}

void ConnectedComponent::emit_sse_statements(CodeOutStream &ost, int iters)
{
  ost << "// QSSA connected component\n";
  PairDelim qssa_pair(ost);
  std::set<std::pair<int,int> > already_emitted;
  // First tell all the species to emit their statements
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_sse_statements(ost, already_emitted, iters); 
  }
  // Emit forward declarations for all the X values
  ost << VECTOR2 << " den";
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    ost << ", xq_" << it->first;
  }
  ost << ";\n";
  // Now we can emit the statements for this connected component
  for (std::vector<Statement*>::const_iterator it = statements.begin();
        it != statements.end(); it++)
  {
    (*it)->emit_sse_code(ost, already_emitted); 
  }
  // Finally update all the reactions 
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_sse_updates(ost, iters);
  }
}

void ConnectedComponent::emit_avx_statements(CodeOutStream &ost, int iters)
{
  ost << "// QSSA connected component\n";
  PairDelim qssa_pair(ost);
  std::set<std::pair<int,int> > already_emitted;
  // First tell all the species to emit their statements
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_avx_statements(ost, already_emitted, iters); 
  }
  // Emit forward declarations for all the X values
  ost << VECTOR4 << " den";
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    ost << ", xq_" << it->first;
  }
  ost << ";\n";
  // Now we can emit the statements for this connected component
  for (std::vector<Statement*>::const_iterator it = statements.begin();
        it != statements.end(); it++)
  {
    (*it)->emit_avx_code(ost, already_emitted); 
  }
  // Finally update all the reactions 
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_avx_updates(ost, iters);
  }
}

void Zero::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const
{
  std::pair<int,int> key(y,0);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << REAL << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_0 = a" << y << "_0 + a" << y << "_" << x << " * a" << x << "_0;\n";
  if (profiler != NULL)
  {
    profiler->adds(1);
    profiler->multiplies(1);
  }
}

void Zero::emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  std::pair<int,int> key(y,0);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << VECTOR2 << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_0 = _mm_add_pd(a" << y << "_0,_mm_mul_pd(a" << y << "_" << x
      << ",a" << x << "_0));\n";
}

void Zero::emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  std::pair<int,int> key(y,0);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << VECTOR4 << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_0 = _mm256_add_pd(a" << y << "_0,_mm256_mul_pd(a" << y << "_" << x
      << ",a" << x << "_0));\n";
}

void Zero::find_allocations(Warp *warp, QSSASharedMemoryStore *store) const
{
  // Check x_0
  if (!warp->has_species(x))
  {
    store->allocate_value(std::pair<int,int>(x,0));
  }
}

void Zero::emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                QSSASharedMemoryStore &store, Warp *warp) const
{
  // first see if we need to load the value
  if (store.has_value(std::pair<int,int>(x,0)))
  {
    if (already_emitted.find(std::pair<int,int>(x,0)) == already_emitted.end())
    {
      already_emitted.insert(std::pair<int,int>(x,0));
      int loc = store.find_value(std::pair<int,int>(x,0));
      ost << REAL << " a" << x << "_0 = scratch[" << loc << "][tid];\n";
    }
    
  }
  emit_code(ost, already_emitted, NULL); 
}

void Zero::emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                  QSSASharedMemoryStore &store) const
{
  std::pair<int,int> key(y,0);
  if ((already_emitted.find(key) == already_emitted.end()) && store.has_value(key))
  {
    int loc = store.find_value(key);
    ost << "scratch[" << loc << "][tid] = a" << y << "_0;\n";
    already_emitted.insert(key);
  }
}

void Zero::emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store,
                                       Warp *warp) const
{
  std::pair<int,int> zero_key(y,0);
  assert(store.has_value(zero_key));
  int zero_loc = store.find_value(zero_key);
  std::pair<int,int> first_key(y,x);
  assert(store.has_value(first_key));
  int first_loc = store.find_value(first_key);
  std::pair<int,int> second_key(x,0);
  assert(store.has_value(second_key));
  int second_loc = store.find_value(second_key);

  ost << "scratch[" << zero_loc << "][tid] += scratch[" << first_loc
      << "][tid] * scratch[" << second_loc << "][tid];\n";
}

void Den::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const
{
  ost << "den = 1.0/(1.0";
  for (std::vector<std::pair<int,int> >::const_iterator it = pairs.begin();
        it != pairs.end(); it++)
  {
    ost << " - a" << it->second << "_" << it->first << "*a" << it->first << "_" << it->second;
  }
  ost << ");\n";
  if (profiler != NULL)
  {
    profiler->divides(1);
    profiler->subtracts(1);
    profiler->multiplies(1);
  }
}

void Den::emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  PairDelim den_pair(ost);
  ost << VECTOR2 << " temp = _mm_set1_pd(1.0);\n";
  for (std::vector<std::pair<int,int> >::const_iterator it = pairs.begin();
        it != pairs.end(); it++)
  {
    ost << "temp = _mm_sub_pd(temp,_mm_mul_pd(a" << it->second << "_" << it->first
        << ",a" << it->first << "_" << it->second << "));\n";
  }
  ost << "den = _mm_div_pd(_mm_set1_pd(1.0),temp);\n";
}

void Den::emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  PairDelim den_pair(ost);
  ost << VECTOR4 << " temp = _mm256_set1_pd(1.0);\n";
  for (std::vector<std::pair<int,int> >::const_iterator it = pairs.begin();
        it != pairs.end(); it++)
  {
    ost << "temp = _mm256_sub_pd(temp,_mm256_mul_pd(a" << it->second << "_" << it->first
        << ",a" << it->first << "_" << it->second << "));\n";
  }
  ost << "den = _mm256_div_pd(_mm256_set1_pd(1.0),temp);\n";
}

void Den::find_allocations(Warp *warp, QSSASharedMemoryStore *store) const
{
  for (std::vector<std::pair<int,int> >::const_iterator it = pairs.begin();
        it != pairs.end(); it++)
  {
    if (!warp->has_species(it->first))
    {
      store->allocate_value(*it);
    }
    if (!warp->has_species(it->second))
    {
      store->allocate_value(std::pair<int,int>(it->second,it->first));
    }
  }
}

void Den::emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                QSSASharedMemoryStore &store, Warp *warp) const
{
  for (std::vector<std::pair<int,int> >::const_iterator it = pairs.begin();
        it != pairs.end(); it++)
  {
    if (store.has_value(*it))
    {
      if (already_emitted.find(*it) == already_emitted.end())
      {
        already_emitted.insert(*it);
        int loc = store.find_value(*it);
        ost << REAL << " a" << it->first << "_" << it->second << " = scratch[" << loc << "][tid];\n";
      }
    }
    if (store.has_value(std::pair<int,int>(it->second,it->first)))
    {
      if (already_emitted.find(std::pair<int,int>(it->second,it->first)) == 
          already_emitted.end())
      {
        already_emitted.insert(std::pair<int,int>(it->second,it->first));
        int loc = store.find_value(std::pair<int,int>(it->second,it->first));
        ost << REAL << " a" << it->second << "_" << it->first << " = scratch[" << loc << "][tid];\n";
      }
      
    }
  }
  emit_code(ost, already_emitted, NULL);
}

void Den::emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store,
                                       Warp *warp) const
{
  std::vector<int> first_locs;
  std::vector<int> second_locs;
  for (std::vector<std::pair<int,int> >::const_iterator it = pairs.begin();
        it != pairs.end(); it++)
  {
    std::pair<int,int> key_two(it->second,it->first);
    assert(store.has_value(*it));
    first_locs.push_back(store.find_value(*it));
    assert(store.has_value(key_two));
    second_locs.push_back(store.find_value(key_two));
  }
  ost << "den = 1.0/(1.0";
  for (unsigned idx = 0; idx < first_locs.size(); idx++)
  {
    ost << " - scratch[" << first_locs[idx] << "][tid]*scratch["
        << second_locs[idx] << "][tid]";
  }
  ost << ");\n";
}

void Den::emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                  QSSASharedMemoryStore &store) const
{
  // Do nothing
}

void Multiply::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const
{
  std::pair<int,int> key(y,z);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << REAL << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_" << z << " = a" << y << "_" << x << " * a" << x << "_" << z << ";\n"; 
  if (profiler != NULL)
    profiler->multiplies(1);
}

void Multiply::emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  std::pair<int,int> key(y,z);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << VECTOR2 << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_" << z << " = _mm_mul_pd(a" << y << "_" << x << ",a" << x << "_" << z << ");\n";
}

void Multiply::emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  std::pair<int,int> key(y,z);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << VECTOR4 << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_" << z << " = _mm256_mul_pd(a" << y << "_" << x << ",a" << x << "_" << z << ");\n";
}

void Multiply::find_allocations(Warp *warp, QSSASharedMemoryStore *store) const
{
  if (!warp->has_species(y))
    store->allocate_value(std::pair<int,int>(y,x));
  if (!warp->has_species(x))
    store->allocate_value(std::pair<int,int>(x,z));
}

void Multiply::emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                QSSASharedMemoryStore &store, Warp *warp) const
{
  if (store.has_value(std::pair<int,int>(y,x)))
  {
    if (already_emitted.find(std::pair<int,int>(y,x)) == already_emitted.end())
    {
      already_emitted.insert(std::pair<int,int>(y,x));
      int loc = store.find_value(std::pair<int,int>(y,x));
      ost << REAL << " a" << y << "_" << x << " = scratch[" << loc << "][tid];\n";
    }
  }
  if (store.has_value(std::pair<int,int>(x,z)))
  {
    if (already_emitted.find(std::pair<int,int>(x,z)) == already_emitted.end())
    {
      already_emitted.insert(std::pair<int,int>(x,z));
      int loc = store.find_value(std::pair<int,int>(x,z));
      ost << REAL << " a" << x << "_" << z << " = scratch[" << loc << "][tid];\n";
    }
  }
  emit_code(ost, already_emitted, NULL);
}

void Multiply::emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                  QSSASharedMemoryStore &store) const
{
  std::pair<int,int> key(y,z);
  if ((already_emitted.find(key) == already_emitted.end()) && store.has_value(key))
  {
    int loc = store.find_value(key);
    ost << "scratch[" << loc << "][tid] = a" << y << "_" << z << ";\n";
    already_emitted.insert(key);
  }
}

void Multiply::emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store,
                                       Warp *warp) const
{
  std::pair<int,int> out_key(y,z);
  if (!store.has_value(out_key))
    store.allocate_value(out_key);
  int out_loc = store.find_value(out_key);
  std::pair<int,int> first_key(y,x);
  assert(store.has_value(first_key));
  int loc1 = store.find_value(first_key);
  std::pair<int,int> second_key(x,z);
  assert(store.has_value(second_key));
  int loc2 = store.find_value(second_key);

  ost << "scratch[" << out_loc << "][tid] = scratch[" << loc1 << "][tid] * scratch["
      << loc2 << "][tid];\n";
}

void Add::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const
{
  std::pair<int,int> key(y,z);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << REAL << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_" << z << " = a" << y << "_" << z << " + a" << y << "_" << x << " * a" << x << "_" << z << ";\n";
  if (profiler != NULL)
  {
    profiler->adds(1);
    profiler->multiplies(1);
  }
}

void Add::emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  std::pair<int,int> key(y,z);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << VECTOR2 << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_" << z << " = _mm_add_pd(a" << y << "_" << z << ",_mm_mul_pd(a" << y 
      << "_" << x << ",a" << x << "_" << z << "));\n";
}

void Add::emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  std::pair<int,int> key(y,z);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << VECTOR4 << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_" << z << " = _mm256_add_pd(a" << y << "_" << z << ",_mm256_mul_pd(a" << y 
      << "_" << x << ",a" << x << "_" << z << "));\n";
}

void Add::find_allocations(Warp *warp, QSSASharedMemoryStore *store) const
{
  if (!warp->has_species(y))
    store->allocate_value(std::pair<int,int>(y,x));
  if (!warp->has_species(x))
    store->allocate_value(std::pair<int,int>(x,z));
}

void Add::emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                QSSASharedMemoryStore &store, Warp *warp) const
{
  if (store.has_value(std::pair<int,int>(y,x)))
  {
    if (already_emitted.find(std::pair<int,int>(y,x)) == already_emitted.end())
    {
      already_emitted.insert(std::pair<int,int>(y,x));
      int loc = store.find_value(std::pair<int,int>(y,x));
      ost << REAL << " a" << y << "_" << x << " = scratch[" << loc << "][tid];\n";
    }
  }
  if (store.has_value(std::pair<int,int>(x,z)))
  {
    if (already_emitted.find(std::pair<int,int>(x,z)) == already_emitted.end())
    {
      already_emitted.insert(std::pair<int,int>(x,z));
      int loc = store.find_value(std::pair<int,int>(x,z));
      ost << REAL << " a" << x << "_" << z << " = scratch[" << loc << "][tid];\n";
    }
  }
  emit_code(ost, already_emitted, NULL);
}

void Add::emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                  QSSASharedMemoryStore &store) const
{
  std::pair<int,int> key(y,z);
  if ((already_emitted.find(key) == already_emitted.end()) && store.has_value(key))
  {
    int loc = store.find_value(key);
    ost << "scratch[" << loc << "][tid] = a" << y << "_" << z << ";\n";
    already_emitted.insert(key);
  }
}

void Add::emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store,
                                       Warp *warp) const
{
  std::pair<int,int> key0(y,z);
  assert(store.has_value(key0));
  int loc0 = store.find_value(key0);
  std::pair<int,int> key1(y,x);
  assert(store.has_value(key1));
  int loc1 = store.find_value(key1);
  std::pair<int,int> key2(x,z);
  assert(store.has_value(key2));
  int loc2 = store.find_value(key2);

  ost << "scratch[" << loc0 << "][tid] += scratch[" << loc1 << "][tid]*scratch["
      << loc2 << "][tid];\n";
}

void Divide::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const
{
  std::pair<int,int> key(z,y);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << REAL << " ";
    already_emitted.insert(key);
  }
  ost << "a" << z << "_" << y << " = a" << z << "_" << y << "*den;\n";
  if (profiler != NULL)
    profiler->multiplies(1);
}

void Divide::emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  std::pair<int,int> key(z,y);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << VECTOR2 << " ";
    already_emitted.insert(key);
  }
  ost << "a" << z << "_" << y << " = _mm_mul_pd(a" << z << "_" << y << ",den);\n";
}

void Divide::emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  std::pair<int,int> key(z,y);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << VECTOR4 << " ";
    already_emitted.insert(key);
  }
  ost << "a" << z << "_" << y << " = _mm256_mul_pd(a" << z << "_" << y << ",den);\n";
}

void Divide::find_allocations(Warp *warp, QSSASharedMemoryStore *store) const
{
  // Do nothing
}

void Divide::emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                QSSASharedMemoryStore &store, Warp *warp) const
{
  emit_code(ost, already_emitted, NULL);
}

void Divide::emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                  QSSASharedMemoryStore &store) const
{
  std::pair<int,int> key(z,y);
  if ((already_emitted.find(key) == already_emitted.end()) && store.has_value(key))
  {
    int loc = store.find_value(key);
    ost << "scratch[" << loc << "][tid] = a" << z << "_" << y << ";\n";
    already_emitted.insert(key);
  }
}

void Divide::emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store,
                                       Warp *warp) const
{
  std::pair<int,int> key(z,y);
  assert(store.has_value(key));
  int loc = store.find_value(key);
  ost << "scratch[" << loc << "][tid] *= den;\n";
}

void Xstat::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, Profiler *profiler) const
{
  ost << "xq_" << x << " = a" << x << "_0";
  for (std::set<int>::const_iterator it = y_vals.begin();
        it != y_vals.end(); it++)
  {
    ost << " + a" << x << "_" << (*it) << "*xq_" << (*it);
  }
  ost << ";\n";
  if (profiler != NULL)
  {
    profiler->adds(1);
    profiler->multiplies(1);
  }
}

void Xstat::emit_sse_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  ost << "xq_" << x << " = a" << x << "_0;\n";
  for (std::set<int>::const_iterator it = y_vals.begin();
        it != y_vals.end(); it++)
  {
    ost << "xq_" << x << " = _mm_add_pd(xq_" << x << ",_mm_mul_pd(a"
        << x << "_" << (*it) << ",xq_" << (*it) << "));\n";
  }
}

void Xstat::emit_avx_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) const
{
  ost << "xq_" << x << " = a" << x << "_0;\n";
  for (std::set<int>::const_iterator it = y_vals.begin();
        it != y_vals.end(); it++)
  {
    ost << "xq_" << x << " = _mm256_add_pd(xq_" << x << ",_mm256_mul_pd(a"
        << x << "_" << (*it) << ",xq_" << (*it) << "));\n";
  }
}

void Xstat::find_allocations(Warp *warp, QSSASharedMemoryStore *store) const
{
  // Do nothing
}

void Xstat::emit_warp_spec_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                QSSASharedMemoryStore &store, Warp *warp) const
{
  for (std::set<int>::const_iterator it = y_vals.begin();
        it != y_vals.end(); it++)
  {
    std::pair<int,int> key((*it)-1,(*it)-1);
    if (!warp->has_species(*it) && (already_emitted.find(key) == already_emitted.end()))
    {
      int loc = store.find_value(std::pair<int,int>((*it)-1,(*it)-1));
      ost << REAL << " xq_" << (*it) << " = scratch[" << loc << "][tid];\n";
      already_emitted.insert(key);
    }
  }
  emit_code(ost, already_emitted, NULL);
  int loc = store.find_value(std::pair<int,int>(x-1,x-1));
  ost << "scratch[" << loc << "][tid] = xq_" << x << ";\n";
}

void Xstat::emit_warp_spec_stores(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                                  QSSASharedMemoryStore &store) const
{
  // Do nothing
}

void Xstat::emit_warp_spec_shared_code(CodeOutStream &ost, QSSASharedMemoryStore &store,
                                       Warp *warp) const
{
  std::vector<int> locations;
  std::vector<int> xlocations;
  for (std::set<int>::const_iterator it = y_vals.begin();
        it != y_vals.end(); it++)
  {
    std::pair<int,int> key(x,*it);
    assert(store.has_value(key));
    locations.push_back(store.find_value(key));
    std::pair<int,int> xkey((*it)-1,(*it)-1);
    assert(store.has_value(xkey));
    xlocations.push_back(store.find_value(xkey));
  }
  std::pair<int,int> out_key(x-1,x-1);
  assert(store.has_value(out_key));
  int out_loc = store.find_value(out_key);
  std::pair<int,int> zero_key(x,0);
  assert(store.has_value(zero_key));
  int zero_loc = store.find_value(zero_key);
  ost << "scratch[" << out_loc << "][tid] = scratch[" << zero_loc
      << "][tid]";
  for (unsigned idx = 0; idx < locations.size(); idx++)
  {
    ost << " + scratch[" << locations[idx] << "][tid]*scratch["
        << xlocations[idx] << "][tid]";
  }
  ost << ";\n";
}

void ConnectedComponent::construct_qss_graph(std::vector<QSS*> &qss_nodes)
{
  // Go through each of the statements, and look for multiplies as they 
  // indicate that one node flows into another
  QSS *current = NULL;
  for (std::vector<Statement*>::const_iterator it = statements.begin();
        it != statements.end(); it++)
  {
    Zero *zero= (*it)->as_zero();
    Xstat *xstat = (*it)->as_xstat();
    if (zero != NULL)
    {
      // Figure out what the old and new QSS values are 
      int src_qss = zero->x;
      int dst_qss = zero->y;
      assert(species.find(src_qss) != species.end());
      assert(species.find(dst_qss) != species.end());
      QSS *src_spec = species[src_qss];
      QSS *dst_spec = species[dst_qss];
      src_spec->outputs.insert(dst_spec);
      dst_spec->inputs.insert(src_spec);
      dst_spec->closure_inputs.insert(src_spec);
      // Make the destination species the current species
      current = dst_spec;
      current->statements.push_back(*it);
    }
    else if (xstat != NULL)
    {
      assert(species.find(xstat->x) != species.end());
      species[xstat->x]->xstat = xstat;
    }
    else
    {
      assert(current != NULL);
      current->statements.push_back(*it);
    }
  }
  // Now compute the closure of the inputs for all our qss species
  bool modified = true;
  while (modified)
  {
    modified = false;
    for (std::map<int,QSS*>::const_iterator qss_it = species.begin();
          qss_it != species.end(); qss_it++)
    {
      QSS *spec = qss_it->second;
      size_t old_inputs = spec->closure_inputs.size(); 
      for (std::set<QSS*>::const_iterator it = spec->inputs.begin();
            it != spec->inputs.end(); it++)
      {
        spec->closure_inputs.insert((*it)->closure_inputs.begin(),
                                    (*it)->closure_inputs.end());
      }
      size_t new_inputs = spec->closure_inputs.size();
      if (new_inputs != old_inputs)
        modified = true;
    }
  }
  // Now put our species on the list of qss_nodes
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    qss_nodes.push_back(it->second);
  }
}

// QSS

bool QSS::is_qssa_reaction(Reaction *reac) const
{
  if (forward_zeros.find(reac) != forward_zeros.end())
    return true;
  if (backward_zeros.find(reac) != backward_zeros.end())
    return true;
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = 
        forward_contributions.begin(); it !=
        forward_contributions.end(); it++)
  {
    if (it->first.second == reac)
      return true;
  }
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = 
        backward_contributions.begin(); it !=
        backward_contributions.end(); it++)
  {
    if (it->first.second == reac)
      return true;
  }
  if (forward_denom.find(reac) != forward_denom.end())
    return true;
  if (backward_denom.find(reac) != backward_denom.end())
    return true;
  return false;
}

int QSS::find_qss_index(Reaction *reac, bool forward) const
{
  // Return qss_idx minus one for zero based indexing
  if (forward)
  {
    if (forward_corrections.find(reac) != forward_corrections.end())
      return (qss_idx-1);
    else
      return -1;
  }
  else
  {
    if (backward_corrections.find(reac) != backward_corrections.end())
      return (qss_idx-1);
    else
      return -1;
  }
}

void QSS::emit_statements(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted,
                          Profiler *profiler)
{
  ost << REAL << " a" << qss_idx << "_0";
  already_emitted.insert(std::pair<int,int>(qss_idx,0));
  std::set<int> others;
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << ", a" << qss_idx << "_" << (*it);
    already_emitted.insert(std::pair<int,int>(qss_idx,*it));
  }
  ost << ";\n";
  PairDelim spec_pair(ost);
  // First compute the denominator
  ost << REAL << " den = ";
  bool first = true;
  for (std::set<Reaction*>::const_iterator it = forward_denom.begin();
        it != forward_denom.end(); it++)
  {
    if (!first)
    {
      ost << " + ";
      if (profiler != NULL)
        profiler->adds(1);
    }
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "rr_f[" << index << "]";
    if (profiler != NULL)
      profiler->reads(1);
    first = false;
  }
  for (std::set<Reaction*>::const_iterator it = backward_denom.begin();
        it != backward_denom.end(); it++)
  {
    if (!first)
    {
      ost << " + ";
      if (profiler != NULL)
        profiler->adds(1);
    }
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "rr_r[" << index << "]";
    first = false;
    if (profiler != NULL)
      profiler->reads(1);
  }
  ost << ";\n";
  ost << "a" << qss_idx << "_0 = (";
  first = true;
  for (std::map<Reaction*,int>::const_iterator it = forward_zeros.begin();
        it != forward_zeros.end(); it++)
  {
    int index = unit->find_ordered_reaction(it->first);
    assert(index != -1);
    for (int i = 0; i < it->second; i++)
    {
      if (!first)
      {
        ost << " + ";
        if (profiler != NULL)
          profiler->adds(1);
      }
      ost << "rr_f[" << index << "]";
      first = false;
      if (profiler != NULL)
        profiler->reads(1);
    }
  }
  for (std::map<Reaction*,int>::const_iterator it = backward_zeros.begin();
        it != backward_zeros.end(); it++)
  {
    int index = unit->find_ordered_reaction(it->first);
    assert(index != -1);
    for (int i = 0; i < it->second; i++)
    {
      if (!first)
      {
        ost << " + ";
        if (profiler != NULL)
          profiler->adds(1);
      }
      ost << "rr_r[" << index << "]";
      first = false;
      if (profiler != NULL)
        profiler->reads(1);
    }
  }
  if (forward_zeros.empty() && backward_zeros.empty())
    ost << "0.0";
  ost << ")/den;\n";
  if (profiler != NULL)
    profiler->divides(1);
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << "a" << qss_idx << "_" << (*it) << " = (";
    first = true;
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator pit = forward_contributions.begin();
          pit != forward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        int index = unit->find_ordered_reaction(pit->first.second);
        assert(index != -1);
        for (int i = 0; i < pit->second; i++)
        {
          if (!first)
          {
            ost << " + ";
            if (profiler != NULL)
              profiler->adds(1);
          }
          ost << "rr_f[" << index << "]";
          first = false;
          if (profiler != NULL)
            profiler->reads(1);
        }
      }
    }
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator pit = backward_contributions.begin();
          pit != backward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        int index = unit->find_ordered_reaction(pit->first.second);
        assert(index != -1);
        for (int i = 0; i < pit->second; i++)
        {
          if (!first)
          {
            ost << " + ";
            if (profiler != NULL)
              profiler->adds(1);
          }
          ost << "rr_r[" << index << "]";
          first = false;
          if (profiler != NULL)
            profiler->reads(1);
        }
      }
    }
    ost << ")/den;\n";
    if (profiler != NULL)
      profiler->divides(1);
  }
}

void QSS::emit_updates(CodeOutStream &ost, Profiler *profiler)
{
  for (std::set<Reaction*>::const_iterator it = forward_corrections.begin();
        it != forward_corrections.end(); it++)
  {
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "rr_f[" << index << "] *= xq_" << qss_idx << ";\n";
    if (profiler != NULL)
    {
      profiler->reads(1);
      profiler->writes(1);
      profiler->multiplies(1);
    }
  }
  for (std::set<Reaction*>::const_iterator it = backward_corrections.begin();
        it != backward_corrections.end(); it++)
  {
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "rr_r[" << index << "] *= xq_" << qss_idx << ";\n";
    if (profiler != NULL)
    {
      profiler->reads(1);
      profiler->writes(1);
      profiler->multiplies(1);
    }
  }
}

void QSS::emit_sse_statements(CodeOutStream &ost, 
    std::set<std::pair<int,int> > &already_emitted, int iters)
{
  ost << VECTOR2 << " a" << qss_idx << "_0";
  already_emitted.insert(std::pair<int,int>(qss_idx,0));
  std::set<int> others;
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << ", a" << qss_idx << "_" << (*it);
    already_emitted.insert(std::pair<int,int>(qss_idx,*it));
  }
  ost << ";\n";
  PairDelim spec_pair(ost);
  // First compute the denominator
  ost << VECTOR2 << " den = _mm_set1_pd(0.0);\n";
  for (std::set<Reaction*>::const_iterator it = forward_denom.begin();
        it != forward_denom.end(); it++)
  {
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "den = _mm_add_pd(den,rr_f[" << (index*iters) << "+idx]);\n";
  }
  for (std::set<Reaction*>::const_iterator it = backward_denom.begin();
        it != backward_denom.end(); it++)
  {
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "den = _mm_add_pd(den,rr_r[" << (index*iters) << "+idx]);\n";
  }
  ost << ";\n";
  ost << "a" << qss_idx << "_0 = _mm_set1_pd(0.0);\n";
  for (std::map<Reaction*,int>::const_iterator it = forward_zeros.begin();
        it != forward_zeros.end(); it++)
  {
    int index = unit->find_ordered_reaction(it->first);
    assert(index != -1);
    for (int i = 0; i < it->second; i++)
    {
      ost << "a" << qss_idx << "_0 = _mm_add_pd(a" << qss_idx
          << "_0,rr_f[" << (index*iters) << "+idx]);\n";
    }
  }
  for (std::map<Reaction*,int>::const_iterator it = backward_zeros.begin();
        it != backward_zeros.end(); it++)
  {
    int index = unit->find_ordered_reaction(it->first);
    assert(index != -1);
    for (int i = 0; i < it->second; i++)
    {
      ost << "a" << qss_idx << "_0 = _mm_add_pd(a" << qss_idx
          << "_0,rr_r[" << (index*iters) << "+idx]);\n";
    }
  }
  ost << "a" << qss_idx << "_0 = _mm_div_pd(a" << qss_idx
      << "_0,den);\n";
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << "a" << qss_idx << "_" << (*it) << " = _mm_set1_pd(0.0);\n";
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator pit = forward_contributions.begin();
          pit != forward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        int index = unit->find_ordered_reaction(pit->first.second);
        assert(index != -1);
        for (int i = 0; i < pit->second; i++)
        {
          ost << "a" << qss_idx << "_" << (*it) << " = _mm_add_pd(a"
              << qss_idx << "_" << (*it) << ",rr_f[" << (index*iters) << "+idx]);\n";
        }
      }
    }
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator pit = backward_contributions.begin();
          pit != backward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        int index = unit->find_ordered_reaction(pit->first.second);
        assert(index != -1);
        for (int i = 0; i < pit->second; i++)
        {
          ost << "a" << qss_idx << "_" << (*it) << " = _mm_add_pd(a"
              << qss_idx << "_" << (*it) << ",rr_r[" << (index*iters) << "+idx]);\n";
        }
      }
    }
    ost << "a" << qss_idx << "_" << (*it) << " = _mm_div_pd(a"
        << qss_idx << "_" << (*it) << ",den);\n";
  }
}

void QSS::emit_sse_updates(CodeOutStream &ost, int iters)
{
  for (std::set<Reaction*>::const_iterator it = forward_corrections.begin();
        it != forward_corrections.end(); it++)
  {
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "rr_f[" << (index*iters) << "+idx] = _mm_mul_pd("
        << "rr_f[" << (index*iters) << "+idx], xq_" << qss_idx << ");\n";
  }
  for (std::set<Reaction*>::const_iterator it = backward_corrections.begin();
        it != backward_corrections.end(); it++)
  {
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "rr_r[" << (index*iters) << "+idx] = _mm_mul_pd("
        << "rr_r[" << (index*iters) << "+idx], xq_" << qss_idx << ");\n";
  }
}

void QSS::emit_avx_statements(CodeOutStream &ost,
    std::set<std::pair<int,int> > &already_emitted, int iters)
{
  ost << VECTOR4 << " a" << qss_idx << "_0";
  already_emitted.insert(std::pair<int,int>(qss_idx,0));
  std::set<int> others;
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << ", a" << qss_idx << "_" << (*it);
    already_emitted.insert(std::pair<int,int>(qss_idx,*it));
  }
  ost << ";\n";
  PairDelim spec_pair(ost);
  // First compute the denominator
  ost << VECTOR4 << " den = _mm256_set1_pd(0.0);\n";
  for (std::set<Reaction*>::const_iterator it = forward_denom.begin();
        it != forward_denom.end(); it++)
  {
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "den = _mm256_add_pd(den,rr_f[" << (index*iters) << "+idx]);\n";
  }
  for (std::set<Reaction*>::const_iterator it = backward_denom.begin();
        it != backward_denom.end(); it++)
  {
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "den = _mm256_add_pd(den,rr_r[" << (index*iters) << "+idx]);\n";
  }
  ost << ";\n";
  ost << "a" << qss_idx << "_0 = _mm256_set1_pd(0.0);\n";
  for (std::map<Reaction*,int>::const_iterator it = forward_zeros.begin();
        it != forward_zeros.end(); it++)
  {
    int index = unit->find_ordered_reaction(it->first);
    assert(index != -1);
    for (int i = 0; i < it->second; i++)
    {
      ost << "a" << qss_idx << "_0 = _mm256_add_pd(a" << qss_idx
          << "_0,rr_f[" << (index*iters) << "+idx]);\n";
    }
  }
  for (std::map<Reaction*,int>::const_iterator it = backward_zeros.begin();
        it != backward_zeros.end(); it++)
  {
    int index = unit->find_ordered_reaction(it->first);
    assert(index != -1);
    for (int i = 0; i < it->second; i++)
    {
      ost << "a" << qss_idx << "_0 = _mm256_add_pd(a" << qss_idx
          << "_0,rr_r[" << (index*iters) << "+idx]);\n";
    }
  }
  ost << "a" << qss_idx << "_0 = _mm256_div_pd(a" << qss_idx
      << "_0,den);\n";
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << "a" << qss_idx << "_" << (*it) << " = _mm256_set1_pd(0.0);\n";
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator pit = forward_contributions.begin();
          pit != forward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        int index = unit->find_ordered_reaction(pit->first.second);
        assert(index != -1);
        for (int i = 0; i < pit->second; i++)
        {
          ost << "a" << qss_idx << "_" << (*it) << " = _mm256_add_pd(a"
              << qss_idx << "_" << (*it) << ",rr_f[" << (index*iters) << "+idx]);\n";
        }
      }
    }
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator pit = backward_contributions.begin();
          pit != backward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        int index = unit->find_ordered_reaction(pit->first.second);
        assert(index != -1);
        for (int i = 0; i < pit->second; i++)
        {
          ost << "a" << qss_idx << "_" << (*it) << " = _mm256_add_pd(a"
              << qss_idx << "_" << (*it) << ",rr_r[" << (index*iters) << "+idx]);\n";
        }
      }
    }
    ost << "a" << qss_idx << "_" << (*it) << " = _mm256_div_pd(a"
        << qss_idx << "_" << (*it) << ",den);\n";
  }
}

void QSS::emit_avx_updates(CodeOutStream &ost, int iters)
{
  for (std::set<Reaction*>::const_iterator it = forward_corrections.begin();
        it != forward_corrections.end(); it++)
  {
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "rr_f[" << (index*iters) << "+idx] = _mm256_mul_pd("
        << "rr_f[" << (index*iters) << "+idx], xq_" << qss_idx << ");\n";
  }
  for (std::set<Reaction*>::const_iterator it = backward_corrections.begin();
        it != backward_corrections.end(); it++)
  {
    int index = unit->find_ordered_reaction(*it);
    assert(index != -1);
    ost << "rr_r[" << (index*iters) << "+idx] = _mm256_mul_pd("
        << "rr_r[" << (index*iters) << "+idx], xq_" << qss_idx << ");\n";
  }
}

void QSS::emit_warp_specialized_statements(CodeOutStream &ost,
                          std::set<std::pair<int,int> > &already_emitted, 
                          BankAnalyzer *bank_analyzer, 
                          std::vector<Warp*> &qssa_warps, 
                          int version, int warp_offset)
{
  ost << REAL << " a" << qss_idx << "_0 = 0.0";
  already_emitted.insert(std::pair<int,int>(qss_idx,0));
  std::set<int> others;
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << ", a" << qss_idx << "_" << (*it) << " = 0.0";
    already_emitted.insert(std::pair<int,int>(qss_idx,*it));
  }
  ost << ";\n";
  PairDelim spec_pair(ost);
  ost << INT << " index;\n";
  if (unit->k20)
    ost << INT << " hi_part,lo_part;\n";
  // First compute the set of denominator locations
  ost << REAL << " denom = 0.0;\n";
  {
    std::vector<int> denom_locations;
    for (std::set<Reaction*>::const_iterator it = forward_denom.begin();
          it != forward_denom.end(); it++)
    {
      denom_locations.push_back(bank_analyzer->find_forward_location(*it));
    }
    for (std::set<Reaction*>::const_iterator it = backward_denom.begin();
          it != backward_denom.end(); it++)
    {
      denom_locations.push_back(bank_analyzer->find_reverse_location(*it));
    }
    unit->emit_warp_specialized_load_rates(ost, denom_locations, qssa_warps,
        "denom", version, warp_offset);
    // Invert the denominator
    ost << REAL << " recip_denom = 1.0/denom;\n";
  }
  // Now do the zero value
  {
    char var_name[64];
    sprintf(var_name,"a%d_0",qss_idx);
    if (!forward_zeros.empty() || !backward_zeros.empty())
    {
      std::vector<int> zero_locations;
      for (std::map<Reaction*,int>::const_iterator it = forward_zeros.begin();
            it != forward_zeros.end(); it++)
      {
        int loc = bank_analyzer->find_forward_location(it->first);
        assert(it->second > 0);
        for (int idx = 0; idx < it->second; idx++)
          zero_locations.push_back(loc);
      }
      for (std::map<Reaction*,int>::const_iterator it = backward_zeros.begin();
            it != backward_zeros.end(); it++)
      {
        int loc = bank_analyzer->find_reverse_location(it->first);
        assert(it->second > 0);
        for (int idx = 0; idx < it->second; idx++)
          zero_locations.push_back(loc);
      }
      unit->emit_warp_specialized_load_rates(ost, zero_locations, qssa_warps,
          var_name, version, warp_offset);
      ost << var_name << " *= recip_denom;\n";
    }
  }
  // Finally do all the reamining values
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    char var_name[64];
    sprintf(var_name,"a%d_%d",qss_idx, *it);
    std::vector<int> locations;
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator pit = forward_contributions.begin();
          pit != forward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        int loc = bank_analyzer->find_forward_location(pit->first.second);
        assert(pit->second > 0);
        for (int i = 0; i < pit->second; i++)
        {
          locations.push_back(loc);
        }
      }
    }
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator pit = backward_contributions.begin();
          pit != backward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        int loc = bank_analyzer->find_reverse_location(pit->first.second);
        assert(pit->second > 0);
        for (int i = 0; i < pit->second; i++)
        {
          locations.push_back(loc);
        }
      }
    }
    if (!locations.empty())
    {
      unit->emit_warp_specialized_load_rates(ost, locations, qssa_warps,
          var_name, version, warp_offset);
      ost << var_name << " *= recip_denom;\n";
    }
  }
}

void QSS::emit_warp_specialized_updates(CodeOutStream &ost,
                          BankAnalyzer *bank_analyzer, 
                          std::vector<Warp*> &qssa_warps, 
                          int version, int warp_offset)
{
  // Now for performing the updates, build the set of locations that
  // we need to update, load each of them, scale them by the output
  // and then write them back out
  std::vector<int> update_locations;
  for (std::set<Reaction*>::const_iterator it = forward_corrections.begin();
        it != forward_corrections.end(); it++)
  {
    update_locations.push_back(bank_analyzer->find_forward_location(*it));
  }
  for (std::set<Reaction*>::const_iterator it = backward_corrections.begin();
        it != backward_corrections.end(); it++)
  {
    update_locations.push_back(bank_analyzer->find_reverse_location(*it));
  }
  std::vector<std::vector<int> > assignment;
  bank_analyzer->perform_assignment(update_locations, assignment);
  int max_steps = -1;
  for (unsigned idx = 0; idx < assignment.size(); idx++)
  {
    if (int(assignment[idx].size()) > max_steps)
      max_steps = assignment[idx].size();
  }
  assert(assignment.size() == qssa_warps.size());
  // Now emit the loads for the warp 
  for (int step = 0; step < max_steps; step++)
  {
    int meta_index = -1;
    // Update all the qssa_warps with the appropriate index
    for (unsigned wid = 0; wid < qssa_warps.size(); wid++)
    {
      if (step < int(assignment[wid].size()))
      {
        int global_idx = qssa_warps[wid]->add_index(
                  assignment[wid][step], version); 
        if (meta_index == -1)
          meta_index = global_idx;
        else
          assert(meta_index == global_idx);
      }
      else
      {
        int global_idx = qssa_warps[wid]->add_index(
                  2*unit->act_reactions.size(), version);
        if (meta_index == -1)
          meta_index = global_idx;
        else
          assert(meta_index == global_idx);
      }
    }
    // Now emit the code for the exchange of index
    int index_offset = meta_index/unit->points_per_cta;
    int index_thread = meta_index%unit->points_per_cta;
    if (unit->k20)
      ost << "index = __shfl(index_constants[" << index_offset << "]," 
          << index_thread << "," << unit->points_per_cta << ");\n";
    else
    {
      // Rely on warp-synchronous programming
      ost << "if (tid == " << index_thread << ")\n";
      ost << "  int_mirror[wid+" << (warp_offset) 
          << "] = index_constants[" << index_offset << "];\n";
      ost << "index = int_mirror[wid+" 
          << (warp_offset) << "];\n";
    }
    // Scale the value
    ost << "rrs[index][tid] *= xq_" << qss_idx << ";\n";
  }
}

void QSS::emit_multi_loads(CodeOutStream &ost, int pass, MultiAnalyzer *multi_analyzer,
                           std::vector<Warp*> &qssa_warps, int warp_offset)
{
  // Denominator first
  {
    char denom_name[64];
    sprintf(denom_name,"denom_%d", qss_idx);
    std::vector<int> denom_locations;
    for (std::set<Reaction*>::const_iterator it = forward_denom.begin();
          it != forward_denom.end(); it++)
    {
      int loc = multi_analyzer->find_forward_location(*it, pass);
      if (loc == -1)
        continue;
      denom_locations.push_back(loc);
    }
    for (std::set<Reaction*>::const_iterator it = backward_denom.begin();
          it != backward_denom.end(); it++)
    {
      int loc = multi_analyzer->find_reverse_location(*it, pass);
      if (loc == -1)
        continue;
      denom_locations.push_back(loc);
    }
    unit->emit_multi_load_rates(ost, denom_locations, qssa_warps, denom_name, warp_offset, pass);
  }
  // Zero-value
  {
    char var_name[64];
    sprintf(var_name,"a%d_0",qss_idx);
    if (!forward_zeros.empty() || !backward_zeros.empty())
    {
      std::vector<int> zero_locations;
      for (std::map<Reaction*,int>::const_iterator it = forward_zeros.begin();
            it != forward_zeros.end(); it++)
      {
        int loc = multi_analyzer->find_forward_location(it->first, pass);
        if (loc == -1)
          continue;
        assert(it->second > 0);
        for (int idx = 0; idx < it->second; idx++)
          zero_locations.push_back(loc);
      }
      for (std::map<Reaction*,int>::const_iterator it = backward_zeros.begin();
            it != backward_zeros.end(); it++)
      {
        int loc = multi_analyzer->find_reverse_location(it->first, pass);
        if (loc == -1)
          continue;
        assert(it->second > 0);
        for (int idx = 0; idx < it->second; idx++)
          zero_locations.push_back(loc);
      }
      unit->emit_multi_load_rates(ost, zero_locations, qssa_warps, var_name, warp_offset, pass);
    }
  }
  // All remaining values
  std::set<int> others;
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    char var_name[64];
    sprintf(var_name,"a%d_%d",qss_idx, *it);
    std::vector<int> locations;
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator pit = forward_contributions.begin();
          pit != forward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        int loc = multi_analyzer->find_forward_location(pit->first.second, pass);
        if (loc == -1)
          continue;
        assert(pit->second > 0);
        for (int i = 0; i < pit->second; i++)
        {
          locations.push_back(loc);
        }
      }
    }
    for (std::map<std::pair<int,Reaction*>,int>::const_iterator pit = backward_contributions.begin();
          pit != backward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        int loc = multi_analyzer->find_reverse_location(pit->first.second, pass);
        if (loc == -1)
          continue;
        assert(pit->second > 0);
        for (int i = 0; i < pit->second; i++)
        {
          locations.push_back(loc);
        }
      }
    }
    if (!locations.empty())
    {
      unit->emit_multi_load_rates(ost, locations, qssa_warps, var_name, warp_offset, pass);
    }
  }
}

void QSS::emit_multi_reductions(CodeOutStream &ost)
{
  char denom_name[64];
  sprintf(denom_name,"denom_%d", qss_idx);
  if (unit->warp_size < 32)
    unit->emit_multi_reduce(ost, denom_name);
  ost << denom_name << " = 1.0/" << denom_name << ";\n";
  char zero_name[64];
  sprintf(zero_name,"a%d_0", qss_idx);
  if (unit->warp_size < 32)
    unit->emit_multi_reduce(ost, zero_name);
  ost << zero_name << " *= " << denom_name << ";\n";
  std::set<int> others;
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,Reaction*>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    char var_name[64];
    sprintf(var_name,"a%d_%d",qss_idx, *it);
    if (unit->warp_size < 32)
      unit->emit_multi_reduce(ost, var_name);
    ost << var_name << " *= " << denom_name << ";\n";
  }
}

// Stif

void Stif::emit_statements(CodeOutStream &ost, TranslationUnit *unit, Profiler *profiler)
{
  assert(species != NULL);
  ost << "// Stiff species " << name << "\n";
  PairDelim stif_pair(ost);
  // First compute the destruction rate for this species
  ost << REAL << " ddot = ";
  bool first = true;
  for (std::map<Reaction*,int>::const_iterator it = forward_d.begin();
        it != forward_d.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      if (!first)
      {
        ost << " + ";
        if (profiler != NULL)
          profiler->adds(1);
      }
      int index = unit->find_ordered_reaction(it->first);
      assert(index != -1);
      ost << "rr_f[" << index << "]";
      first = false;
      if (profiler != NULL)
        profiler->reads(1);
    }
  }
  for (std::map<Reaction*,int>::const_iterator it = backward_d.begin();
        it != backward_d.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      if (!first)
      {
        ost << " + ";
        if (profiler != NULL)
          profiler->adds(1);
      }
      int index = unit->find_ordered_reaction(it->first);
      assert(index != -1);
      ost << "rr_r[" << index << "]";
      first = false;
      if (profiler != NULL)
        profiler->reads(1);
    }
  }
  if (first)
    ost << "0.0";
  ost << ";\n";
  int spec_index = unit->find_ordered_index(species);
  // Better be able to find the species index
  assert(spec_index != -1);
  ost << "if ((ddot * dt * " << (1.0/REACTION_RATE_REF) << ") > " 
      << MOLE_FRAC << "[" << spec_index << "])\n";
  if (profiler != NULL)
  {
    profiler->reads(1);
    profiler->multiplies(2);
  }
  {
    PairDelim if_pair(ost);
    ost << REAL << " cdot = ";
    first = true;
    for (std::map<Reaction*,int>::const_iterator it = forward_c.begin();
          it != forward_c.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        if (!first)
        {
          ost << " + ";
          if (profiler != NULL)
            profiler->adds(1);
        }
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "rr_f[" << index << "]";
        first = false;
        if (profiler != NULL)
          profiler->reads(1);
      }
    }
    for (std::map<Reaction*,int>::const_iterator it = backward_c.begin();
          it != backward_c.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        if (!first)
        {
          ost << " + ";
          if (profiler != NULL)
            profiler->adds(1);
        }
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "rr_r[" << index << "]";
        first = false;
        if (profiler != NULL)
          profiler->reads(1);
      }
    }
    if (first)
      ost << "0.0";
    ost << ";\n";
    ost << REAL << " recip_ddot = 1.0/ddot;\n";
    if (profiler != NULL)
      profiler->divides(1);
    if (!unit->use_dim)
      ost << REAL << " part_sum = cdot + " << DIFFUSION 
          << "[" << spec_index << "] * " << (DIFF_REF * (1.0/species->molecular_mass)) << ";\n";
    else
      ost << REAL << " part_sum = cdot + " << DIFFUSION 
          << "[" << spec_index << "] * " << (1.0/species->molecular_mass) << ";\n";
    if (profiler != NULL)
    {
      profiler->reads(1);
      profiler->adds(1);
      profiler->multiplies(1);
    }
    ost << REAL << " c0 = " << MOLE_FRAC 
        << "[" << spec_index << "] * part_sum * recip_ddot;\n";
    if (profiler != NULL)
    {
      profiler->reads(1);
      profiler->multiplies(2);
    }
    ost << REAL " scale_r = (part_sum + "
        << REACTION_RATE_REF << " * ("
        << MOLE_FRAC << "[" << spec_index << "]- c0) / dt) * recip_ddot;\n";
    if (profiler != NULL)
    {
      profiler->reads(1);
      profiler->adds(1);
      profiler->subtracts(1);
      profiler->multiplies(2);
      profiler->divides(1);
    }
    // appy the updates to all the destruction rates
    for (std::map<Reaction*,int>::const_iterator it = forward_d.begin();
          it != forward_d.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "rr_f[" << index << "] *= scale_r;\n";
        if (profiler != NULL)
        {
          profiler->reads(1);
          profiler->writes(1);
          profiler->multiplies(1);
        }
      }
    }
    for (std::map<Reaction*,int>::const_iterator it = backward_d.begin();
          it != backward_d.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "rr_r[" << index << "] *= scale_r;\n";
        if (profiler != NULL)
        {
          profiler->reads(1);
          profiler->writes(1);
          profiler->multiplies(1);
        }
      }
    }
  }
}

void Stif::emit_sse_statements(CodeOutStream &ost, TranslationUnit *unit, int iters)
{
  assert(species != NULL);
  ost << "// Stiff species " << name << "\n";
  PairDelim stif_pair(ost);
  ost << VECTOR2 << " ddot = _mm_set1_pd(0.0);\n";
  for (std::map<Reaction*,int>::const_iterator it = forward_d.begin();
        it != forward_d.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      int index = unit->find_ordered_reaction(it->first);
      assert(index != -1);
      ost << "ddot = _mm_add_pd(ddot,rr_f[" << (index*iters) << "+idx]);\n";
    }
  }
  for (std::map<Reaction*,int>::const_iterator it = backward_d.begin();
        it != backward_d.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      int index = unit->find_ordered_reaction(it->first);
      assert(index != -1);
      ost << "ddot = _mm_add_pd(ddot,rr_r[" << (index*iters) << "+idx]);\n";
    }
  }
  int spec_index = unit->find_ordered_index(species);
  // Better be able to find the species index
  assert(spec_index != -1);
  ost << VECTOR2 << " test = _mm_sub_pd(_mm_mul_pd(ddot,_mm_mul_pd(_mm_set1_pd(dt),_mm_set1_pd("
      << (1.0/REACTION_RATE_REF) << "))),mole_frac[" << (spec_index*iters) << "+idx]);\n";
  ost << "if ((_mm_cvtsd_f64(test) > 0.0) || "
      << "(_mm_cvtsd_f64(_mm_shuffle_pd(test,test,1)) > 0.0))\n";
  {
    PairDelim if_pair(ost);
    ost << VECTOR2 << " cdot = _mm_set1_pd(0.0);\n";
    for (std::map<Reaction*,int>::const_iterator it = forward_c.begin();
          it != forward_c.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "cdot = _mm_add_pd(cdot,rr_f[" << (index*iters) << "+idx]);\n";
      }
    }
    for (std::map<Reaction*,int>::const_iterator it = backward_c.begin();
          it != backward_c.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "cdot = _mm_add_pd(cdot,rr_r[" << (index*iters) << "+idx]);\n";
      }
    }
    ost << VECTOR2 << " recip_ddot = _mm_div_pd(_mm_set1_pd(1.0),ddot);\n";
    ost << VECTOR2 << " diff_val = _mm_load_pd(" << DIFFUSION_ARRAY
        << "+(" << spec_index << "*spec_stride)+(idx<<1));\n";
    if (!unit->use_dim)
    {
      ost << VECTOR2 << " part_sum = _mm_add_pd(cdot,_mm_mul_pd("
          << "diff_val,_mm_set1_pd(" << (DIFF_REF * (1.0/species->molecular_mass)) << ")));\n";
    }
    else
    {
      ost << VECTOR2 << " part_sum = _mm_add_pd(cdot,_mm_mul_pd("
          << "diff_val,_mm_set1_pd(" << (1.0/species->molecular_mass) << ")));\n";
    }
    ost << VECTOR2 << " c0 = _mm_mul_pd(_mm_mul_pd(mole_frac["
        << (spec_index*iters) << "+idx],part_sum),recip_ddot);\n";
    ost << "c0 = _mm_add_pd(part_sum,_mm_div_pd(_mm_mul_pd(_mm_sub_pd(mole_frac["
        << (spec_index*iters) << "+idx],c0),_mm_set1_pd(" << REACTION_RATE_REF
        << ")),_mm_set1_pd(dt)));\n";
    ost << VECTOR2 << " scale_r = _mm_mul_pd(c0,recip_ddot);\n";
    // Handle the case where only one of these was should have
    // been done so we don't get any NAN values
    ost << VECTOR2 << " mask = _mm_cmpgt_pd(test,_mm_set1_pd(0.0));\n";
    ost << "scale_r = _mm_and_pd(scale_r,mask);\n";
    ost << "scale_r = _mm_add_pd(scale_r,_mm_andnot_pd(mask,_mm_set1_pd(1.0)));\n";
    // apply the updates to all the destruction rates
    for (std::map<Reaction*,int>::const_iterator it = forward_d.begin();
          it != forward_d.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "rr_f[" << (index*iters) << "+idx] = _mm_mul_pd("
            << "rr_f[" << (index*iters) << "+idx],scale_r);\n";
      }
    }
    for (std::map<Reaction*,int>::const_iterator it = backward_d.begin();
          it != backward_d.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "rr_r[" << (index*iters) << "+idx] = _mm_mul_pd("
            << "rr_r[" << (index*iters) << "+idx],scale_r);\n";
      }
    }
  }
}

void Stif::emit_avx_statements(CodeOutStream &ost, TranslationUnit *unit, int iters)
{
  assert(species != NULL);
  ost << "// Stiff species " << name << "\n";
  PairDelim stif_pair(ost);
  ost << VECTOR4 << " ddot = _mm256_set1_pd(0.0);\n";
  for (std::map<Reaction*,int>::const_iterator it = forward_d.begin();
        it != forward_d.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      int index = unit->find_ordered_reaction(it->first);
      assert(index != -1);
      ost << "ddot = _mm256_add_pd(ddot,rr_f[" << (index*iters) << "+idx]);\n";
    }
  }
  for (std::map<Reaction*,int>::const_iterator it = backward_d.begin();
        it != backward_d.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      int index = unit->find_ordered_reaction(it->first);
      assert(index != -1);
      ost << "ddot = _mm256_add_pd(ddot,rr_r[" << (index*iters) << "+idx]);\n";
    }
  }
  int spec_index = unit->find_ordered_index(species);
  // Better be able to find the species index
  assert(spec_index != -1);
  ost << VECTOR4 << " test = _mm256_sub_pd(_mm256_mul_pd(ddot,_mm256_mul_pd(_mm256_set1_pd(dt),"
      << "_mm256_set1_pd(" << (1.0/REACTION_RATE_REF) << "))),"
      << "mole_frac[" << (spec_index*iters) << "+idx]);\n";
  ost << VECTOR2 << " test_upper = _mm256_extractf128_pd(test,0);\n";
  ost << VECTOR2 << " test_lower = _mm256_extractf128_pd(test,1);\n";
  ost << "if ((_mm_cvtsd_f64(test_upper) > 0.0) || "
      << "(_mm_cvtsd_f64(test_lower) > 0.0) || "
      << "(_mm_cvtsd_f64(_mm_shuffle_pd(test_upper,test_upper,1)) > 0.0) || "
      << "(_mm_cvtsd_f64(_mm_shuffle_pd(test_lower,test_lower,1)) > 0.0))\n";
  {
    PairDelim if_pair(ost);
    ost << VECTOR4 << " cdot = _mm256_set1_pd(0.0);\n";
    for (std::map<Reaction*,int>::const_iterator it = forward_c.begin();
          it != forward_c.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "cdot = _mm256_add_pd(cdot,rr_f[" << (index*iters) << "+idx]);\n";
      }
    }
    for (std::map<Reaction*,int>::const_iterator it = backward_c.begin();
          it != backward_c.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "cdot = _mm256_add_pd(cdot,rr_r[" << (index*iters) << "+idx]);\n";
      }
    }
    ost << VECTOR4 << " recip_ddot = _mm256_div_pd(_mm256_set1_pd(1.0),ddot);\n";
    ost << VECTOR4 << " diff_val;\n";
    ost << "if (aligned<32>(" << DIFFUSION_ARRAY << "))\n";
    ost << "  diff_val = _mm256_load_pd(" << DIFFUSION_ARRAY
          << "+(" << spec_index << "*spec_stride)+(idx<<2));\n";
    ost << "else\n";
    ost << "  diff_val = _mm256_loadu_pd(" << DIFFUSION_ARRAY
        << "+(" << spec_index << "*spec_stride)+(idx<<2));\n";
    if (!unit->use_dim)
    {
      ost << VECTOR4 << " part_sum = _mm256_add_pd(cdot,_mm256_mul_pd("
          << "diff_val,_mm256_set1_pd(" << (DIFF_REF * (1.0/species->molecular_mass)) << ")));\n";
    }
    else
    {
      ost << VECTOR4 << " part_sum = _mm256_add_pd(cdot,_mm256_mul_pd("
          << "diff_val,_mm256_set1_pd(" << (1.0/species->molecular_mass) << ")));\n";
    }
    ost << VECTOR4 << " c0 = _mm256_mul_pd(_mm256_mul_pd(mole_frac["
        << (spec_index*iters) << "+idx],part_sum),recip_ddot);\n";
    ost << "c0 = _mm256_add_pd(part_sum,_mm256_div_pd(_mm256_mul_pd(_mm256_sub_pd(mole_frac["
        << (spec_index*iters) << "+idx],c0),_mm256_set1_pd("
        << REACTION_RATE_REF << ")),_mm256_set1_pd(dt)));\n";
    ost << VECTOR4 << " scale_r = _mm256_mul_pd(c0,recip_ddot);\n";
    // Handle the case where only one of these was should have
    // been done so we don't get any NAN values
    ost << VECTOR4 << " mask = _mm256_cmp_pd(test,_mm256_set1_pd(0.0),_CMP_GT_OQ);\n";
    ost << "scale_r = _mm256_and_pd(scale_r,mask);\n";
    ost << "scale_r = _mm256_add_pd(scale_r,_mm256_andnot_pd(mask,_mm256_set1_pd(1.0)));\n";
    // apply the updates to all the destruction rates
    for (std::map<Reaction*,int>::const_iterator it = forward_d.begin();
          it != forward_d.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "rr_f[" << (index*iters) << "+idx] = _mm256_mul_pd("
            << "rr_f[" << (index*iters) << "+idx],scale_r);\n";
      }
    }
    for (std::map<Reaction*,int>::const_iterator it = backward_d.begin();
          it != backward_d.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        int index = unit->find_ordered_reaction(it->first);
        assert(index != -1);
        ost << "rr_r[" << (index*iters) << "+idx] = _mm256_mul_pd("
            << "rr_r[" << (index*iters) << "+idx],scale_r);\n";
      }
    }
  }
}

// Warp

Warp::Warp(int id)
  : wid(id)
{
  int_constants.resize(1);
  real_constants.resize(1);
}

int Warp::add_index(int index, int version)
{
  if (version == 0)
  {
    int loc = int_constants[0].size();
    int_constants[0].push_back(index);
    return loc;
  }
  else
  {
    if (version >= int(int_constants.size()))
      int_constants.resize(version+1);
    int loc = int_constants[version].size();
    assert(loc < int(int_constants[0].size()));
    assert(index == int_constants[0][loc]);
    int_constants[version].push_back(index);
    return loc;
  }
}

int Warp::add_real(double real, int version)
{
  if (version == 0)
  {
    int loc = real_constants[0].size();
    real_constants[0].push_back(real);
    return loc;
  }
  else
  {
    if (version >= int(real_constants.size()))
      real_constants.resize(version+1);
    int loc = real_constants[version].size();
    assert(loc < int(real_constants[0].size()));
    assert(real == real_constants[0][loc]);
    real_constants[version].push_back(real);
    return loc;
  }
}

void Warp::add_reaction(Reaction *reac)
{
  reacs.push_back(reac);
}

void Warp::add_stif(Stif *stif)
{
  stifs.push_back(stif);
}

void Warp::add_output(Species *spec)
{
  specs.push_back(spec);
}

void Warp::add_qss(QSS *qss)
{
  qss_specs.push_back(qss);
}

bool Warp::has_species(int qss_idx) const
{
  for (std::vector<QSS*>::const_iterator it = qss_specs.begin();
        it != qss_specs.end(); it++)
  {
    if ((*it)->qss_idx == qss_idx)
      return true;
  }
  return false;
}

// Bank Analyzer

BankAnalyzer::BankAnalyzer(int b, int max_reacs)
  : banks(b)
{
  locations.resize(2*max_reacs);
  for (unsigned idx = 0; idx < locations.size(); idx++)
    locations[idx] = NULL;
  is_forward.resize(2*max_reacs);
}

void BankAnalyzer::add_forward_reac(Reaction *reac, int loc)
{
  assert(locations[loc] == NULL);
  assert(forward_banks.find(reac) == forward_banks.end());
  locations[loc] = reac;
  forward_banks[reac] = loc;
  is_forward[loc] = true;
}

void BankAnalyzer::add_reverse_reac(Reaction *reac, int loc)
{
  assert(locations[loc] == NULL);
  assert(reverse_banks.find(reac) == reverse_banks.end());
  locations[loc] = reac;
  reverse_banks[reac] = loc;
  is_forward[loc] = false;
}

int BankAnalyzer::find_forward_location(Reaction *reac) const
{
  std::map<Reaction*,int>::const_iterator finder = 
    forward_banks.find(reac);
  assert(finder != forward_banks.end());
  return finder->second;
}

int BankAnalyzer::find_reverse_location(Reaction *reac) const
{
  std::map<Reaction*,int>::const_iterator finder = 
    reverse_banks.find(reac);
  assert(finder != reverse_banks.end());
  return finder->second;
}

void BankAnalyzer::perform_assignment(const std::vector<int> &locs,
      std::vector<std::vector<int> > &assignment) const
{
  std::vector<std::vector<int> > banked_locations(banks); 
  // First sort locations into banks
  for (std::vector<int>::const_iterator it = locs.begin();
        it != locs.end(); it++)
  {
    int bank = (*it) % banks;
    banked_locations[bank].push_back(*it);
  }
  // Sort each of the location from largest to smallest
  // so that as we balance out things we can maximize the
  // chances of getting broadcasts
  for (unsigned idx = 0; idx < banked_locations.size(); idx++)
  {
    std::sort(banked_locations[idx].begin(),banked_locations[idx].end());
  }
  // Put the bank with the most locations at the lowest assignment
  std::set<unsigned> used;
  while (int(used.size()) < banks)
  {
    int index = -1;
    int largest = -1;
    for (int idx = 0; idx < banks; idx++)
    {
      if (used.find(idx) != used.end())
        continue;
      if (int(banked_locations[idx].size()) > largest)
      {
        index = idx;
        largest = banked_locations[idx].size();
      }
    }
    assert(index != -1);
    assert(largest > -1);
    assignment.push_back(banked_locations[index]);
    used.insert(index);
  }
  // balance out all the columns so that no warp needs more than one
  // more location than any other column, bank conflicts be damned
  if (assignment.size() > 2)
  {
    std::set<size_t> column_sizes;
    for (unsigned idx = 0; idx < assignment.size(); idx++)
      column_sizes.insert(assignment[idx].size());
    while (column_sizes.size() > 2)
    {
      // Find the right-most largest column and pop its last element off
      int large_index = -1;
      int large_size = -1;
      int small_index = -1;
      size_t small_size = *(column_sizes.begin());
      for (unsigned idx = 0; idx < assignment.size(); idx++)
      {
        if (int(assignment[idx].size()) >= large_size)
        {
          large_size = assignment[idx].size();
          large_index = idx;
        }
        if (assignment[idx].size() <= small_size)
        {
          small_size = assignment[idx].size();
          small_index = idx;
        }
      }
      assert(large_index != -1);
      assert(small_index != -1);
      assignment[small_index].push_back(assignment[large_index].back()); 
      assignment[large_index].pop_back();

      // Rebuild the set of column sizes
      column_sizes.clear();
      for (unsigned idx = 0; idx < assignment.size(); idx++)
        column_sizes.insert(assignment[idx].size());
    }
  }
  else
  {
    // Cases of 1 and 2 columns
    // 1 is easy
    if (assignment.size() == 1)
      return;
    // Case of 2 columns is easy too
    assert(assignment[0].size() >= assignment[1].size());
    while ((assignment[0].size() - assignment[1].size()) > 1)
    {
      assignment[1].push_back(assignment[0].back());
      assignment[0].pop_back();
      assert(assignment[0].size() >= assignment[1].size());
    }
  }
}

// Multi Analyzer

MultiAnalyzer::MultiAnalyzer(int b, int pass, int locs)
  : banks(b), passes(pass)
{
  forward_banks.resize(passes);
  reverse_banks.resize(passes);
  locations.resize(passes);
  is_forward.resize(passes);
  for (unsigned idx = 0; idx < locations.size(); idx++)
  {
    locations[idx].resize(locs);
    is_forward[idx].resize(locs);
    for (unsigned i = 0; i < locations[idx].size(); i++)
      locations[idx][i] = NULL;
  }
}

void MultiAnalyzer::add_forward_reac(Reaction *reac, int pass, int loc)
{
  assert(pass < passes);
  assert(loc < int(locations[pass].size()));
  assert(loc < int(is_forward[pass].size()));
  assert(locations[pass][loc] == NULL);
  assert(forward_banks[pass].find(reac) == forward_banks[pass].end());
  locations[pass][loc] = reac;
  forward_banks[pass][reac] = loc;
  is_forward[pass][loc] = true;
}

void MultiAnalyzer::add_reverse_reac(Reaction *reac, int pass, int loc)
{
  assert(pass < passes);
  assert(loc < int(locations[pass].size()));
  assert(loc < int(is_forward[pass].size()));
  assert(locations[pass][loc] == NULL);
  assert(reverse_banks[pass].find(reac) == reverse_banks[pass].end());
  locations[pass][loc] = reac;
  reverse_banks[pass][reac] = loc;
  is_forward[pass][loc] = false;
}

int MultiAnalyzer::find_forward_location(Reaction *reac, int pass) const
{
  assert(pass < passes);
  std::map<Reaction*,int>::const_iterator finder = 
    forward_banks[pass].find(reac);
  if (finder == forward_banks[pass].end())
    return -1;
  return finder->second;
}

int MultiAnalyzer::find_reverse_location(Reaction *reac, int pass) const
{
  assert(pass < passes);
  std::map<Reaction*,int>::const_iterator finder = 
    reverse_banks[pass].find(reac);
  if (finder == reverse_banks[pass].end())
    return -1;
  return finder->second;
}

void MultiAnalyzer::perform_assignment(int pass, const std::vector<int> &locs,
        std::vector<std::vector<int> > &assignment) const
{
  assert(pass < passes);
  std::vector<std::vector<int> > banked_locations(banks); 
  // First sort locations into banks
  for (std::vector<int>::const_iterator it = locs.begin();
        it != locs.end(); it++)
  {
    int bank = (*it) % banks;
    banked_locations[bank].push_back(*it);
  }
  // Sort each of the location from largest to smallest
  // so that as we balance out things we can maximize the
  // chances of getting broadcasts
  for (unsigned idx = 0; idx < banked_locations.size(); idx++)
  {
    std::sort(banked_locations[idx].begin(),banked_locations[idx].end());
  }
  // Put the bank with the most locations at the lowest assignment
  std::set<unsigned> used;
  while (int(used.size()) < banks)
  {
    int index = -1;
    int largest = -1;
    for (int idx = 0; idx < banks; idx++)
    {
      if (used.find(idx) != used.end())
        continue;
      if (int(banked_locations[idx].size()) > largest)
      {
        index = idx;
        largest = banked_locations[idx].size();
      }
    }
    assert(index != -1);
    assert(largest > -1);
    assignment.push_back(banked_locations[index]);
    used.insert(index);
  }
  // balance out all the columns so that no warp needs more than one
  // more location than any other column, bank conflicts be damned
  if (assignment.size() > 2)
  {
    std::set<size_t> column_sizes;
    for (unsigned idx = 0; idx < assignment.size(); idx++)
      column_sizes.insert(assignment[idx].size());
    while (column_sizes.size() > 2)
    {
      // Find the right-most largest column and pop its last element off
      int large_index = -1;
      int large_size = -1;
      int small_index = -1;
      size_t small_size = *(column_sizes.begin());
      for (unsigned idx = 0; idx < assignment.size(); idx++)
      {
        if (int(assignment[idx].size()) >= large_size)
        {
          large_size = assignment[idx].size();
          large_index = idx;
        }
        if (assignment[idx].size() <= small_size)
        {
          small_size = assignment[idx].size();
          small_index = idx;
        }
      }
      assert(large_index != -1);
      assert(small_index != -1);
      assignment[small_index].push_back(assignment[large_index].back()); 
      assignment[large_index].pop_back();

      // Rebuild the set of column sizes
      column_sizes.clear();
      for (unsigned idx = 0; idx < assignment.size(); idx++)
        column_sizes.insert(assignment[idx].size());
    }
  }
  else
  {
    // Cases of 1 and 2 columns
    // 1 is easy
    if (assignment.size() == 1)
      return;
    // Case of 2 columns is easy too
    assert(assignment[0].size() >= assignment[1].size());
    while ((assignment[0].size() - assignment[1].size()) > 1)
    {
      assignment[1].push_back(assignment[0].back());
      assignment[0].pop_back();
      assert(assignment[0].size() >= assignment[1].size());
    }
  }
}

// QSSA Shared Memory Store 

QSSASharedMemoryStore::QSSASharedMemoryStore(int max_locations, int qss_values, int offset)
{
  global_offset = offset;
  next_loc = qss_values;
  allocations.resize(max_locations);
  for (int idx = 0; idx < qss_values; idx++)
  {
    std::pair<int,int> key(idx,idx);
    allocations[idx] = key;
    locations[key] = idx;
  }
}

void QSSASharedMemoryStore::analyze_warp(Warp *warp)
{
  for (unsigned idx = 0; idx < warp->qss_specs.size(); idx++)
  {
    QSS *qss = warp->qss_specs[idx];
    for (std::vector<Statement*>::const_iterator it = qss->statements.begin();
          it != qss->statements.end(); it++)
    {
      (*it)->find_allocations(warp, this);
    }
  }
}

bool QSSASharedMemoryStore::has_value(std::pair<int,int> key) const
{
  return (locations.find(key) != locations.end());
}

int QSSASharedMemoryStore::find_value(std::pair<int,int> key) const
{
  std::map<std::pair<int,int>,int>::const_iterator finder = locations.find(key);
  assert(finder != locations.end());
  return (finder->second+global_offset);
}

void QSSASharedMemoryStore::allocate_value(std::pair<int,int> key)
{
  if (locations.find(key) != locations.end())
    return;
  if (next_loc == int(allocations.size()))
  {
    fprintf(stderr,"ERROR: Out of shared memory for warp-specialized QSSA!\n");
    fprintf(stderr,"Either reduce the number of CTAs or the number of QSSA warps.\n");
    exit(1);
  }
  allocations[next_loc] = key;
  locations[key] = next_loc;
  next_loc++;
}


