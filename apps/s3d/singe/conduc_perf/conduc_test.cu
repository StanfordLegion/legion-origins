
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>

#include "cuda.h"
#include "cuda_runtime.h"

#include "gpu_getcoeffs.h"

#define CUDA_SAFE_CALL(expr)  \
  { \
    cudaError_t err = (expr); \
    if (err != cudaSuccess) \
    { \
      printf("Cuda error %s\n", cudaGetErrorString(err)); \
      assert(false);  \
    } \
  }

const int num_experiments = 20;

__host__
void run_conductivity_experiment(const double *temp, const double *mass_frac,
                                 const double *avmolwt, const int nx, 
                                 const int ny, const int nz, double *output)
{
  cudaStream_t timing_stream;
  CUDA_SAFE_CALL(cudaStreamCreate(&timing_stream));
  
  cudaEvent_t start, stop;
  CUDA_SAFE_CALL(cudaEventCreate(&start));
  CUDA_SAFE_CALL(cudaEventCreate(&stop));

  dim3 grid(nx/32,ny/4,nz);
  dim3 block(32,4,1);

  CUDA_SAFE_CALL(cudaEventRecord(start, timing_stream));
  for (int i = 0; i < num_experiments; i++)
  {
    gpu_conductivity<<<grid,block,0,timing_stream>>>(temp,mass_frac,avmolwt, 
                                                     nx*ny,nx,nz,nx*ny*nz,
                                                     output);
  }
  
  CUDA_SAFE_CALL(cudaEventRecord(stop, timing_stream));
  CUDA_SAFE_CALL(cudaStreamSynchronize(timing_stream));
  float exec_time;
  CUDA_SAFE_CALL(cudaEventElapsedTime(&exec_time,start,stop));
  CUDA_SAFE_CALL(cudaStreamDestroy(timing_stream));
  CUDA_SAFE_CALL(cudaDeviceSynchronize());
  float total_points = float(nx)*float(ny)*float(nz);
  float avg_time = exec_time/float(num_experiments);
  printf("Conductivity: %dx%dx%d\n",nx,ny,nz);
  printf("  Total time: %12.3f ms\n", exec_time);
  printf("  Average time: %12.3f ms\n", avg_time);
  float throughput = total_points*1e-3/avg_time; // in Mpoints/s
  printf("  Throughput: %12.3f Mpoints/s\n",throughput);
  printf("\n");
}

__host__
int main(int argc, char **argv)
{
  if (argc < 2)
  {
    fprintf(stderr,"Specify number of species!\n");
    exit(1);
  }
  const int nx = 128;
  const int ny = 128;
  const int nz = 128;
  const int ns = atoi(argv[1]);
  
  double *temperature_h = (double*)malloc(nx*ny*nz*sizeof(double));
  double *pressure_h = (double*)malloc(nx*ny*nz*sizeof(double));
  double *avmolwt_h = (double*)malloc(nx*ny*nz*sizeof(double));
  double *mass_frac_h = (double*)malloc(ns*nx*ny*nz*sizeof(double));
  for (int i = 0; i < nx; i++)
  {
    for (int j = 0; j < ny; j++)
    {
      for (int k = 0; k < nz; k++)
      {
        temperature_h[k + j*nx + i*nx*ny] = drand48();
        pressure_h[k + j*nx + i*nx*ny] = drand48();
        avmolwt_h[k + j*nx + i*nx*ny] = drand48();
      }
    }
  }
  for (int s = 0; s < ns; s++)
  {
    for (int i = 0; i < nx; i++)
    {
      for (int j = 0; j < ny; j++)
      {
        for (int k = 0; k < nz; k++)
        {  
          mass_frac_h[k + j*nx + i*nx*ny + s*nx*ny*nz] = drand48();
        }
      }
    }
  }

  CUDA_SAFE_CALL(cudaSetDevice(0));

  double *temperature_d, *pressure_d, *avmolwt_d, *mass_frac_d, *output_d;
  CUDA_SAFE_CALL(cudaMalloc((void**)&temperature_d,nx*ny*nz*sizeof(double)));
  CUDA_SAFE_CALL(cudaMalloc((void**)&pressure_d,nx*ny*nz*sizeof(double)));
  CUDA_SAFE_CALL(cudaMalloc((void**)&avmolwt_d,nx*ny*nz*sizeof(double)));
  CUDA_SAFE_CALL(cudaMalloc((void**)&mass_frac_d,nx*ny*nz*ns*sizeof(double)));
  CUDA_SAFE_CALL(cudaMalloc((void**)&output_d,nx*ny*nz*ns*sizeof(double)));

  CUDA_SAFE_CALL(cudaMemcpy(temperature_d,temperature_h,nx*ny*nz*sizeof(double),cudaMemcpyHostToDevice));
  CUDA_SAFE_CALL(cudaMemcpy(pressure_d,pressure_h,nx*ny*nz*sizeof(double),cudaMemcpyHostToDevice));
  CUDA_SAFE_CALL(cudaMemcpy(avmolwt_d,avmolwt_h,nx*ny*nz*sizeof(double),cudaMemcpyHostToDevice));
  CUDA_SAFE_CALL(cudaMemcpy(mass_frac_d,mass_frac_h,nx*ny*nz*ns*sizeof(double),cudaMemcpyHostToDevice));

  CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));

  run_conductivity_experiment(temperature_d,mass_frac_d,avmolwt_d,32,32,32,output_d);
  run_conductivity_experiment(temperature_d,mass_frac_d,avmolwt_d,64,64,64,output_d);
  run_conductivity_experiment(temperature_d,mass_frac_d,avmolwt_d,128,128,128,output_d);

  CUDA_SAFE_CALL(cudaFree(temperature_d));
  CUDA_SAFE_CALL(cudaFree(pressure_d));
  CUDA_SAFE_CALL(cudaFree(avmolwt_d));
  CUDA_SAFE_CALL(cudaFree(mass_frac_d));
  CUDA_SAFE_CALL(cudaFree(output_d));

  free(temperature_h);
  free(pressure_h);
  free(avmolwt_h);
  free(mass_frac_h);

  return 0;
}

