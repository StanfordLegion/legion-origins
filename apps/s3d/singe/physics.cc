
#include "globals.h"
#include "singe.h"
#include "codegen.h"

static int next_largest_power(int value, int power)
{
  assert(value > 0);
  int result = 1;
  while (value > result)
  {
    result *= power;
  }
  return result;
}

#if 0
static double factorial(unsigned num_stages)
{
  double result = 1.0;
  for (unsigned i = 2; i <= num_stages; i++)
    result *= double(i);
  return result;
}

static void emit_sse_exp_taylor_series_expansion(CodeOutStream &ost, const char *var_name, unsigned num_stages)
{
  // The inspiration for this taylor series approximation came from the cuda libraries
  ost << "// Taylor series expansion for " << num_stages << " stages of exp\n";
  PairDelim taylor_pair(ost);
  ost << VECTOR2 << " scale = _mm_round_pd(_mm_mul_pd(" << var_name << ",_mm_set1_pd(1.4426950408889634e+0)),_MM_FROUND_NINT);\n";
  ost << "__m128i power = _mm_cvtpd_epi32(scale);\n";
  ost << "power = _mm_add_epi32(power, _mm_set_epi32(0,0,1023,1023));\n";
  ost << "power = _mm_sll_epi32(power, _mm_set1_epi32(20));\n";
  ost << "power = _mm_shuffle_epi32(power, _MM_SHUFFLE(1,3,0,2));\n";
  ost << VECTOR2 << " remainder = _mm_add_pd(" << var_name << ", _mm_mul_pd(scale, _mm_set1_pd(-6.9314718055994529e-1)));\n";
  ost << "remainder = _mm_add_pd(remainder, _mm_mul_pd(scale, _mm_set1_pd(-2.3190468138462996e-17)));\n";
  ost << var_name << " = _mm_set1_pd(" << (1.0/factorial(num_stages)) << ");\n";
  for (unsigned stage = num_stages-1; stage >= 1; stage--)
  {
    ost << "// Stage " << stage << "\n";
    ost << var_name << " = _mm_add_pd(_mm_mul_pd(" << var_name << ",remainder), _mm_set1_pd(" << (1.0/factorial(stage)) << "));\n"; 
  }
  ost << var_name << " = _mm_add_pd(_mm_mul_pd(" << var_name << ",remainder), _mm_set1_pd(" << (1.0) << "));\n";
  // Now do the bit manipulation to put them into floating point
  ost << var_name << " = _mm_mul_pd(" << var_name << ", _mm_castsi128_pd(power));\n";
}
#else
static void emit_sse_exp_taylor_series_expansion(CodeOutStream &ost, const char *var_name, unsigned num_stages)
{
  ost << var_name << " = _mm_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(" << var_name
      << "," << var_name << ",1))),exp(_mm_cvtsd_f64(" << var_name << ")));\n";
}
#endif

#if 0
static void emit_avx_exp_taylor_series_expansion(CodeOutStream &ost, const char *var_name, unsigned num_stages)
{
  ost << "// Taylor series expansion for " << num_stages << " stages of exp\n";
  PairDelim taylor_pair(ost);
  ost << VECTOR4 << " scale = _mm256_mul_pd(" << var_name << ", _mm256_set1_pd(1.4426950408889634e+0));\n";
#if 1
  ost << VECTOR2 << " upper = _mm_round_pd(_mm256_extractf128_pd(scale,1),_MM_FROUND_NINT);\n";
  ost << VECTOR2 << " lower = _mm_round_pd(_mm256_extractf128_pd(scale,0),_MM_FROUND_NINT);\n";
  ost << INT << " i1 = int(_mm_cvtsd_f64(lower));\n";
  ost << INT << " i2 = int(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)));\n";
  ost << INT << " i3 = int(_mm_cvtsd_f64(upper));\n";
  ost << INT << " i4 = int(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1)));\n";
#else
  ost << "__m128i power = _mm256_cvtpd_epi32(scale);\n";
  ost << "power = _mm_add_epi32(power, _mm_set1_epi32(1023));\n";
  ost << "power = _mm_sll_epi32(power, _mm_set1_epi32(20));\n";
  ost << "__m256i power2 = _mm256_setzero_si256();\n";
  // Apparently this is an AVX2 instruction which won't appear until Haswell
  ost << "power2 = _mm256_insertf128_si256(power2, power, 0);\n";
  ost << "power2 = _mm256_shuffle_epi32(power2, _MM_SHUFFLE(3,7,2,6,1,5,0,4));\n";
#endif
  ost << VECTOR4 << " remainder = _mm256_add_pd(" << var_name << ", _mm256_mul_pd(scale, _mm256_set1_pd(-6.9314718055994529e-1)));\n";
  ost << "remainder = _mm256_add_pd(remainder, _mm256_mul_pd(scale, _mm256_set1_pd(-2.3190468138462996e-17)));\n";
  ost << var_name << " = _mm256_set1_pd(" << (1.0/factorial(num_stages)) << ");\n";
  for (unsigned stage = num_stages-1; stage >= 1; stage--)
  {
    ost << "// Stage " << stage << "\n";
    ost << var_name << " = _mm256_add_pd(_mm256_mul_pd(" << var_name << ",remainder), _mm256_set1_pd(" << (1.0/factorial(stage)) << "));\n";
  }
  ost << var_name << " = _mm256_add_pd(_mm256_mul_pd(" << var_name << ",remainder), _mm256_set1_pd(" << (1.0) << "));\n";
#if 1
  ost << "volatile " << INT << " bit_vector[8];\n";
  ost << "bit_vector[0] = 0;\n";
  ost << "bit_vector[1] = (i1 + 1023) << 20;\n";
  ost << "bit_vector[2] = 0;\n";
  ost << "bit_vector[3] = (i2 + 1023) << 20;\n";
  ost << "bit_vector[4] = 0;\n";
  ost << "bit_vector[5] = (i3 + 1023) << 20;\n";
  ost << "bit_vector[6] = 0;\n";
  ost << "bit_vector[7] = (i4 + 1023) << 20;\n";
  ost << var_name << " = _mm256_mul_pd(" << var_name << ", _mm256_set_pd(((volatile double*)bit_vector)[3], "
      << "((volatile double*)bit_vector)[2], ((volatile double*)bit_vector)[1] ((volatile double*)bit_vector)[0]));\n";
#else
  ost << var_name << " = _mm256_mul_pd(" << var_name << ", _mm256_castsi256_pd(power2));\n";
#endif
}
#else
static void emit_avx_exp_taylor_series_expansion(CodeOutStream &ost, const char *var_name, unsigned num_stages)
{
  PairDelim exp_pair(ost);
  ost << VECTOR2 << " lower = _mm256_extractf128_pd(" << var_name << ",0);\n";
  ost << VECTOR2 << " upper = _mm256_extractf128_pd(" << var_name << ",1);\n";
  ost << var_name << " = _mm256_set_pd("
      << "exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
      << "exp(_mm_cvtsd_f64(upper)),"
      << "exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
      << "exp(_mm_cvtsd_f64(lower)));\n"; 
}
#endif

static void emit_cuda_load(CodeOutStream &ost, const char *dst,
                           const char *src, bool k20, const char *qualifier = ".cg")
{
  if (k20)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << ") : \"memory\");\n";
  }
}

static void emit_cuda_load(CodeOutStream &ost, const char *dst, 
                           const char *src, const char *src_offset, bool k20, const char *qualifier = ".cg")
{
  if (k20)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
}

static void emit_cuda_load(CodeOutStream &ost, const char *dst, const char *dst_offset,
                           const char *src, const char *src_offset, bool k20, const char *qualifier = ".cg")
{
  if (k20)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << "[" << dst_offset << "]) ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << "[" << dst_offset << "]) ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
}

static void emit_cuda_store(CodeOutStream &ost, const char *dst,
                            const char *src)
{
  ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << dst << "), "
      << "\"d\"(" << src << ") : \"memory\");\n";
}

static void emit_cuda_store(CodeOutStream &ost, const char *dst, const char *dst_offset,
                            const char *src, const char *qualifier = ".cs")
{
  ost << "asm volatile(\"st.global" << qualifier << ".f64 [%0], %1;\" : : \"l\"(" << dst << "+" << dst_offset << ") ";
  ost << ", \"d\"(" << src << ") : \"memory\");\n";
}

void TranslationUnit::emit_baseline_cpu_physics(const char *file_prefix)
{
  char header_name[128];
  char source_name[128];
  snprintf(header_name,127,"%s.h",file_prefix);
  printf("Writing baseline CPU coefficient header file %s...\n",header_name);
  emit_baseline_cpu_physics_header_file(header_name);
  snprintf(source_name,127,"%s.cc",file_prefix);
  printf("Writing baseline CPU coefficient source file %s...\n",source_name);
  emit_baseline_cpu_physics_source_file(source_name, header_name);
}

void TranslationUnit::emit_sse_cpu_physics(const char *file_prefix)
{
  char header_name[128];
  char source_name[128];
  snprintf(header_name,127,"%s.h",file_prefix);
  printf("Writing SSE CPU coefficient header file %s...\n",header_name);
  emit_vector_cpu_physics_header_file(header_name, true, false);
  snprintf(source_name,127,"%s.cc",file_prefix);
  printf("Writing SSE CPU coefficient source file %s...\n",source_name);
  emit_vector_cpu_physics_source_file(source_name, header_name, true, false);
}

void TranslationUnit::emit_avx_cpu_physics(const char *file_prefix)
{
  char header_name[128];
  char source_name[128];
  snprintf(header_name,127,"%s.h",file_prefix);
  printf("Writing AVX CPU coefficient header file %s...\n",header_name);
  emit_vector_cpu_physics_header_file(header_name, false, true);
  snprintf(source_name,127,"%s.cc",file_prefix);
  printf("Writing AVX CPU coefficient source file %s...\n",source_name);
  emit_vector_cpu_physics_source_file(source_name, header_name, false, true);
}

void TranslationUnit::emit_cuda_physics(const char *file_prefix)
{
  char header_name[128];
  char source_name[128];
  snprintf(header_name,127,"%s.h",file_prefix);
  printf("Writing CUDA coefficient header file %s...\n",header_name);
  emit_gpu_physics_header_file(header_name);
  snprintf(source_name,127,"%s.cu",file_prefix);
  printf("Writing CUDA coefficient source file %s...\n",source_name);
  emit_gpu_physics_source_file(source_name, header_name);
}

void TranslationUnit::emit_cpu_mole_masses(CodeOutStream &ost)
{
  ost << "#ifndef __SINGE_MOLE_MASSES__\n";
  ost << "#define __SINGE_MOLE_MASSES__\n";
  ost << "const " << REAL << " molecular_masses[" << ordered_species.size() << "] = {";
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
  {
    if (idx > 0)
      ost << ", ";
    ost << ordered_species[idx]->molecular_mass;
  }
  ost << "};\n";
  ost << "#endif\n\n";
}

void TranslationUnit::emit_cpu_recip_mole_masses(CodeOutStream &ost)
{
  ost << "#ifndef __SINGE_RECIP_MOLE_MASSES__\n";
  ost << "#define __SINGE_RECIP_MOLE_MASSES__\n";
  ost << "const " << REAL << " recip_molecular_masses[" << ordered_species.size() << "] = {";
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
  {
    if (idx > 0)
      ost << ", ";
    ost << (1.0/ordered_species[idx]->molecular_mass);
  }
  ost << "};\n";
  ost << "#endif\n\n";
}

void TranslationUnit::emit_gpu_mole_masses(CodeOutStream &ost)
{
  ost << "__constant__ ";
  ost << REAL << " molecular_masses[" << ordered_species.size() << "] = {";
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
  {
    if (idx > 0)
      ost << ", ";
    ost << ordered_species[idx]->molecular_mass;
  }
  ost << "};\n\n";
}

void TranslationUnit::emit_gpu_recip_mole_masses(CodeOutStream &ost)
{
  ost << "__constant__ ";
  ost << REAL << " recip_molecular_masses[" << ordered_species.size() << "] = {";
  for (unsigned idx = 0; idx < ordered_species.size(); idx++)
  {
    if (idx > 0)
      ost << ", ";
    ost << (1.0/ordered_species[idx]->molecular_mass);
  }
  ost << "};\n\n";
}

//  CPU Code Generation

void TranslationUnit::emit_baseline_cpu_physics_header_file(const char *file_name)
{
  CodeOutStream ost(file_name, true/*bound line length*/, 80);
  ost << "\n";
  ost << "#ifndef __BASELINE_CPU_GET_COEFFS__\n";
  ost << "#define __BASELINE_CPU_GET_COEFFS__\n";
  ost << "\n";

  emit_cpu_mole_masses(ost);
  ost << "\n";
  emit_cpu_recip_mole_masses(ost);
  ost << "\n";
  tran->emit_cpu_conductivity_declaration(ost);
  ost << ";\n";
  tran->emit_cpu_viscosity_declaration(ost);
  ost << ";\n";
  tran->emit_cpu_diffusion_declaration(ost);
  ost << ";\n";
  tran->emit_cpu_thermal_declaration(ost);
  ost << ";\n";

  ost << "\n";
  ost << "#endif // __BASELINE_CPU_GET_COEFFS__\n";
  ost << "\n";
}

void TranslationUnit::emit_baseline_cpu_physics_source_file(const char *file_name, 
                                                    const char *header_name)
{
  CodeOutStream ost(file_name, true/*bound line length*/, 80);

  ost << "\n";
  ost << "#include \"" << header_name << "\"\n";
  ost << "#include <cmath>\n";
  ost << "#include <cassert>\n";
  ost << "#include <cstdio>\n";
  ost << "\n";

  if (profile)
  {
    conductivity_profiler = new Profiler("Conductivity");
    diffusion_profiler = new Profiler("Diffusion");
    viscosity_profiler = new Profiler("Viscosity");
  }
  tran->emit_baseline_cpu_conductivity(ost, conductivity_profiler);
  ost << "\n";
  tran->emit_baseline_cpu_viscosity(ost, viscosity_profiler);
  ost << "\n";
  tran->emit_baseline_cpu_diffusion(ost, diffusion_profiler);
  ost << "\n";
  tran->emit_baseline_cpu_thermal(ost);
  ost << "\n";
}

void TranslationUnit::emit_vector_cpu_physics_header_file(const char *file_name,
                                                          bool sse, bool avx)
{
  CodeOutStream ost(file_name, true/*bound line length*/, 80);
  ost << "\n";
  if (sse)
  {
    ost << "#ifndef __SSE_CPU_GET_COEFFS__\n";
    ost << "#define __SSE_CPU_GET_COEFFS__\n";
  }
  else if (avx)
  {
    ost << "#ifndef __AVX_CPU_GET_COEFFS__\n";
    ost << "#define __AVX_CPU_GET_COEFFS__\n";
  }
  ost << "\n";

  emit_cpu_mole_masses(ost);
  ost << "\n";
  emit_cpu_recip_mole_masses(ost);
  ost << "\n";
  tran->emit_vector_conductivity_declaration(ost, sse, avx);
  ost << ";\n";
  tran->emit_vector_viscosity_declaration(ost, sse, avx);
  ost << ";\n";
  tran->emit_vector_diffusion_declaration(ost, sse, avx);
  ost << ";\n";
  tran->emit_vector_thermal_declaration(ost, sse, avx);
  ost << ";\n";

  ost << "\n";
  if (sse)
    ost << "#endif // __SSE_CPU_GET_COEFFS__\n";
  else if (avx)
    ost << "#endif // __AVX_CPU_GET_COEFFS__\n";
  ost << "\n";
}

void TranslationUnit::emit_vector_cpu_physics_source_file(const char *file_name,
                                                        const char *header_name,
                                                        bool sse, bool avx)
{
  CodeOutStream ost(file_name, true/*bound line length*/, 80);

  ost << "\n";
  ost << "#include \"" << header_name << "\"\n";
  ost << "#include <cmath>\n";
  ost << "#include <cstdio>\n";
  ost << "#include <cassert>\n";
  ost << "#include <cstdlib>\n";
  if (sse || avx)
  {
    ost << "#include <emmintrin.h>\n";
    ost << "#include <smmintrin.h>\n";
  }
  if (avx) {
    ost << "#include <immintrin.h>\n";
    ost << "#include <malloc.h>\n";
  }
  ost << "\n";

  if (sse)
  {
    tran->emit_sse_cpu_conductivity(ost);
    ost << "\n";
    tran->emit_sse_cpu_viscosity(ost);
    ost << "\n";
    tran->emit_sse_cpu_diffusion(ost);
    ost << "\n";
    tran->emit_sse_cpu_thermal(ost);
    ost << "\n";
  }
  else if (avx)
  {
    ost << "template<unsigned BOUNDARY>\n";
    ost << "static inline bool aligned(const void *ptr)\n";
    {
      PairDelim func_pair(ost);
      ost << "return ((((unsigned long)ptr) & (BOUNDARY-1)) == 0);\n";
    }
    ost << "\n";
    tran->emit_avx_cpu_conductivity(ost);
    ost << "\n";
    tran->emit_avx_cpu_viscosity(ost);
    ost << "\n";
    tran->emit_avx_cpu_diffusion(ost);
    ost << "\n";
    tran->emit_avx_cpu_thermal(ost);
    ost << "\n";
  }
}

void TranLib::emit_cpu_conductivity_declaration(CodeOutStream &ost)
{
  ost << REAL << " base_conductivity("
      << REAL << " " << TEMPERATURE
      << ", const " << REAL << " *" << MASS_FRAC 
      << ", const " << REAL << " " << MIXMW << ")";
}

void TranLib::emit_cpu_viscosity_declaration(CodeOutStream &ost)
{
  ost << REAL << " base_viscosity("
      << REAL << " " << TEMPERATURE
      << ", const " << REAL << " *" << MASS_FRAC 
      << ", const " << REAL << " " << MIXMW << ")";
}

void TranLib::emit_cpu_diffusion_declaration(CodeOutStream &ost)
{
  ost << "void base_diffusion("
      << REAL << " " << PRESSURE
      << ", " << REAL << " " << TEMPERATURE
      << ", const " << REAL << " *" << MASS_FRAC
      << ", const " << REAL << " " << MIXMW 
      << ", " << REAL << " *" << "diffusion" << ")";
}

void TranLib::emit_cpu_thermal_declaration(CodeOutStream &ost)
{
  ost << "void base_thermal("
      << REAL << " " << TEMPERATURE
      << ", const " << REAL << " *" << MASS_FRAC
      << ", const " << REAL << " " << MIXMW 
      << ", " << REAL << " *" << "thermal" << ")";
}

void TranLib::emit_vector_conductivity_declaration(CodeOutStream &ost, bool sse, bool avx)
{
  ost << VOID;
  if (sse)
    ost << " sse_";
  else if (avx)
    ost << " avx_";
  ost << "conductivity("
      << "const " << REAL << " *" << TEMPERATURE_ARRAY
      << ", const " << REAL << " *" << MASS_FRAC_ARRAY
      << ", const " << REAL << " *" << MIXMW_ARRAY
      << ", const int num_elmts"
      << ", const int spec_stride"
      << ", " << REAL << " *" << "lambda" << ")";
}

void TranLib::emit_vector_viscosity_declaration(CodeOutStream &ost, bool sse, bool avx)
{
  ost << VOID;
  if (sse)
    ost << " sse_";
  else if (avx)
    ost << " avx_";
  ost << "viscosity("
      << "const " << REAL << " *" << TEMPERATURE_ARRAY
      << ", const " << REAL << " *" << MASS_FRAC_ARRAY
      << ", const " << REAL << " *" << MIXMW_ARRAY
      << ", const int num_elmts"
      << ", const int spec_stride"
      << ", " << REAL << " *" << "viscosity" << ")";
}

void TranLib::emit_vector_diffusion_declaration(CodeOutStream &ost, bool sse, bool avx)
{
  ost << VOID;
  if (sse)
    ost << " sse_";
  else if (avx)
    ost << " avx_";
  ost << "diffusion("
      << "const " << REAL << " *" << TEMPERATURE_ARRAY
      << ", const " << REAL << " *" << PRESSURE_ARRAY
      << ", const " << REAL << " *" << MASS_FRAC_ARRAY
      << ", const " << REAL << " *" << MIXMW_ARRAY
      << ", const int num_elmts"
      << ", const int spec_stride"
      << ", " << REAL << " *" << "diffusion" << ")";
}

void TranLib::emit_vector_thermal_declaration(CodeOutStream &ost, bool sse, bool avx)
{
  ost << VOID;
  if (sse)
    ost << " sse_";
  else if (avx)
    ost << " avx_";
  ost << "thermal("
      << "const " << REAL << " *" << TEMPERATURE_ARRAY
      << ", const " << REAL << " *" << MASS_FRAC_ARRAY
      << ", const " << REAL << " *" << MIXMW_ARRAY
      << ", const int num_elmts"
      << ", const int spec_stride"
      << ", " << REAL << " *" << "thermal" << ")";
}

void TranLib::emit_baseline_cpu_conductivity(CodeOutStream &ost, Profiler *profiler)
{
  emit_cpu_conductivity_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  // Dimensionalize the temperature if necessary
  if (!unit->use_dim)
  {
    ost << TEMPERATURE << " *= " << T_REF << ";\n";
    if (profiler != NULL)
      profiler->multiplies(1);
  }
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  if (profiler != NULL)
  {
    profiler->reads(1);
    profiler->logarithms(1);
  }
  ost << REAL << " sum  = 0.0;\n";
  ost << REAL << " sumr = 0.0;\n";
  // For each species emit the code to compute the value for that species 
  unsigned spec_idx = 0;
  //unsigned offset = 0 * num_species;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Make sure the molecular masses are the same otherwise we're doing something wrong
    assert((*it)->molecular_mass == nwt[spec_idx]);
    ost << "// Species " << (*it)->name << "\n";
    PairDelim spec_pair(ost);
    ost << REAL " val = " << nlam[(spec_idx+1)*order-1] << ";\n";;
    for (int i = (order-1); i > 0; i--)
    {
      ost << "val = val * logt + " << nlam[(spec_idx)*order+i-1] << ";\n";
      if (profiler != NULL)
      {
        profiler->adds(1);
        profiler->multiplies(1);
      }
    }
    ost << "val = exp(val);\n";
    if (profiler != NULL)
      profiler->exponents(1);
    // Convert from mass fractions to mole fractions
    ost << REAL << " frac = " << MASS_FRAC << "[" << spec_idx << "];\n";
    if (profiler != NULL)
      profiler->reads(1);
    ost << REAL << " mole = frac * recip_molecular_masses[" 
        << spec_idx << "] * 1e3 * " << MIXMW << ";\n";
    if (profiler != NULL)
      profiler->multiplies(3);
    ost << "sum  += (mole*val);\n";
    ost << "sumr += (mole/val);\n";
    if (profiler != NULL)
    {
      profiler->adds(2);
      profiler->multiplies(1);
      profiler->divides(1);
    }
    spec_idx++;
  }
  ost << REAL << " result = (0.5 * (sum + 1.0/sumr));\n";
  if (profiler != NULL)
  {
    profiler->reads(1); //mixmw
    profiler->adds(1);
    profiler->multiplies(1);
    profiler->divides(1);
  }
  if (!unit->use_dim)
  {
    double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
    lambda_ref *= 1e5;
    ost << "result *= " << (1.0/lambda_ref) << ";\n";
    if (profiler != NULL)
      profiler->multiplies(1);
  }
  if (profiler != NULL)
    profiler->writes(1);
  ost << "return result;\n";
}

void TranLib::emit_baseline_cpu_viscosity(CodeOutStream &ost, Profiler *profiler)
{
  emit_cpu_viscosity_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  // Convert mass fractions to mole fractions
  ost << REAL << " " << MOLE_FRAC << "[" << unit->ordered_species.size() << "];\n";
  ost << "for (int i = 0; i < " << unit->ordered_species.size() << "; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << MOLE_FRAC << "[i] = " << MASS_FRAC 
        << "[i] * recip_molecular_masses[i] * 1e3 * " 
        << MIXMW << ";\n";
  }
  if (profiler != NULL)
  {
    profiler->reads(unit->ordered_species.size()+1);
    profiler->multiplies(3*unit->ordered_species.size());
  }
  if (!unit->use_dim)
  {
    ost << TEMPERATURE << " *= " << T_REF << ";\n";
    if (profiler != NULL)
      profiler->multiplies(1);
  }
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  if (profiler != NULL)
  {
    profiler->reads(1);
    profiler->logarithms(1);
  }
  ost << REAL << " spec_visc[" << num_species << "];\n";
  assert(num_species == int(unit->ordered_species.size()));
  unsigned spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Make sure the molecular masses are the same otherwise we're doing something wrong
    assert((*it)->molecular_mass == nwt[spec_idx]); 
    ost << "// Species " << (*it)->name << "\n";
    PairDelim spec_pair(ost);
    ost << REAL << " val = " << neta[(spec_idx+1)*order-1] << ";\n";
    for (int i = (order-1); i > 0; i--)
    {
      ost << "val = val * logt + " << neta[(spec_idx)*order+i-1] << ";\n";
      if (profiler != NULL)
      {
        profiler->adds(1);
        profiler->multiplies(1);
      }
    }
    ost << "spec_visc[" << spec_idx << "] = exp(val);\n";
    if (profiler != NULL)
      profiler->exponents(1);
    spec_idx++;
  }

  ost << REAL << " result = 0.0;\n";
  spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << "// Species " << (*it)->name << "\n";
    PairDelim spec_pair(ost);
    ost << REAL " spec_sum = 0.0;\n";
    for (int idx = 0; idx < num_species; idx++)
    {
      PairDelim inner_pair(ost);
      if (idx == int(spec_idx))
      {
        ost << "spec_sum += (" << MOLE_FRAC << "[" << idx << "]*4.0/"
            << (sqrt(2.0)) << ");\n";
        if (profiler != NULL)
        {
          profiler->adds(1);
          profiler->multiplies(1);
          profiler->divides(1);
        }
      }
      else
      {
        ost << REAL << " numer = 1.0 + sqrt(spec_visc[" << spec_idx << "]/spec_visc[" << idx << "]"
            << "*" << (sqrt(nwt[idx]/nwt[spec_idx])) << ");\n";
        ost << "spec_sum += (" << MOLE_FRAC << "[" << idx << "]*numer*numer/"
            << (sqrt(1.0+(nwt[spec_idx]/nwt[idx]))) << ");\n";
        if (profiler != NULL)
        {
          profiler->adds(2);
          profiler->divides(2);
          profiler->sqrts(2);
          profiler->multiplies(1);
        }
      }
    }
    ost << "result += (" << MOLE_FRAC << "[" << spec_idx << "]*spec_visc[" << spec_idx << "]/spec_sum);\n";
    if (profiler != NULL)
    {
      profiler->adds(1);
      profiler->multiplies(1);
      profiler->divides(1);
    }
    spec_idx++;
  }
  if (!unit->use_dim)
  {
    double scaling_factor = sqrt(8.0) / (VIS_REF * 10.0);
    ost << "return (" << (scaling_factor) << "*result);\n";
  }
  else
  {
    double scaling_factor = sqrt(8.0);
    ost << "return (" << (scaling_factor) << "*result);\n";
  }
  if (profiler != NULL)
  {
    profiler->multiplies(1);
    profiler->writes(1);
  }
}

void TranLib::emit_baseline_cpu_diffusion(CodeOutStream &ost, Profiler *profiler)
{
  emit_cpu_diffusion_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  // Convert mass fractions to mole fractions
  ost << REAL << " " << MOLE_FRAC << "[" << unit->ordered_species.size() << "];\n";
  ost << "for (int i = 0; i < " << unit->ordered_species.size() << "; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << MOLE_FRAC << "[i] = " << MASS_FRAC 
        << "[i] * recip_molecular_masses[i] * 1e3 * " 
        << MIXMW << ";\n";
    if (profiler != NULL)
    {
      profiler->reads(unit->ordered_species.size()+1);
      profiler->multiplies(3*unit->ordered_species.size());
    }
  }
  if (!unit->use_dim)
  {
    ost << TEMPERATURE << " *= " << T_REF << ";\n";
    ost << PRESSURE << " *= " << P_REF << ";\n";
    if (profiler != NULL)
      profiler->multiplies(2);
  }
  if (profiler != NULL)
    profiler->reads(2);
  bool symmetric = is_diffusion_symmetric();
  // compute the clamped mole weights
  ost << REAL << " clamped[" << num_species << "];\n";
  ost << "for (int i = 0; i < " << num_species << "; i++)\n";
  ost << "  clamped[i] = ((" << MOLE_FRAC << "[i] > " << SMALL << ") ? " << MOLE_FRAC << "[i] : " << SMALL << ");\n";
  ost << REAL << " sumxod[" << num_species << "];\n";
  ost << "for (int i = 0; i < " << num_species << "; i++)\n";
  ost << "  sumxod[i] = 0.0;\n";
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  if (profiler != NULL)
    profiler->logarithms(1);
  if (symmetric)
  {
    for (int k = 0; k < num_species; k++)
    {
      for (int j = k+1; j < num_species; j++)
      {
        ost << "// D_" << j << "_" << k << " and D_" << k << "_" << j << "\n";
        PairDelim d_pair(ost);
        int offset = j*order + k*order*num_species;
        ost << REAL << " val = " << ndif[offset+order-1] << ";\n";
        for (int i = (order-1); i > 0; i--)
        {
          ost << "val = val * logt + " << ndif[offset+i-1] << ";\n";
          if (profiler != NULL)
          {
            profiler->adds(1);
            profiler->multiplies(1);
          }
        }
        ost << "val = exp(-val);\n";
        ost << "sumxod[" << k << "] += (clamped[" << j << "]*val);\n";
        ost << "sumxod[" << j << "] += (clamped[" << k << "]*val);\n";
        if (profiler != NULL)
        {
          profiler->exponents(1);
          profiler->adds(2);
          profiler->multiplies(2);
        }
      }
    }
  }
  else
  {
    for (int k = 0; k < num_species; k++)
    {
      for (int j = 0; j < num_species; j++)
      {
        // Zeros on the diagonal
        if (k == j)
          continue;
        ost << "// D_" << j << "_" << k << "\n";
        PairDelim d_pair(ost);
        int offset = j*order + k*order*num_species;
        ost << REAL << " val = " << ndif[offset+order-1] << ";\n";
        for (int i = (order-1); i > 0; i--)
        {
          ost << "val = val * logt + " << ndif[offset+i-1] << ";\n";
          if (profiler != NULL)
          {
            profiler->adds(1);
            profiler->multiplies(1);
          }
        }
        ost << "val = exp(-val);\n";
        ost << "sumxod[" << k << "] += (clamped[" << j << "]*val);\n";
        if (profiler != NULL)
        {
          profiler->exponents(1);
          profiler->adds(1);
          profiler->multiplies(1);
        }
      }
    }
  }
  ost << REAL << " sumxw = 0.0;\n";
  ost << REAL << " wtm = 0.0;\n";
  unsigned spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Make sure the molecular masses are the same otherwise we're doing something wrong
    assert((*it)->molecular_mass == nwt[spec_idx]);
    ost << "sumxw += (clamped[" << spec_idx << "]*" << nwt[spec_idx] << ");\n";
    ost << "wtm += (" << MOLE_FRAC << "[" << spec_idx << "]*" << nwt[spec_idx] << ");\n";
    if (profiler != NULL)
    {
      profiler->adds(2);
      profiler->multiplies(2);
    }
    spec_idx++;
  }
  //if (unit->no_nondim)
    ost << REAL << " pfac = " << patmos << "/" << PRESSURE << ";\n";
  if (profiler != NULL)
    profiler->divides(1);
  //else
  //  ost << REAL << " pfac = " << (patmos/PRESSURE_REF) << "/" << PRESSURE << ";\n";
  // Now compute the diffusion coefficients, and overwrite them in the sumxod
  spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue; 
    ost << "diffusion[" << spec_idx << "] = pfac * (sumxw - (" << nwt[spec_idx] << "*clamped[" << spec_idx << "]))"
        << " / (wtm * sumxod[" << spec_idx << "]);\n";
    if (profiler != NULL)
    {
      profiler->subtracts(1);
      profiler->multiplies(3);
      profiler->divides(1);
      profiler->writes(1);
    }
    if (!unit->use_dim)
    {
      ost << "diffusion[" << spec_idx << "] *= " << (1.0/(DIF_REF * 1e4)) << ";\n";
      if (profiler != NULL)
        profiler->multiplies(1);
    }
    spec_idx++;
  }
}

void TranLib::emit_baseline_cpu_thermal(CodeOutStream &ost)
{
  emit_cpu_thermal_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  // Convert mass fractions to mole fractions
  ost << REAL << " " << MOLE_FRAC << "[" << unit->ordered_species.size() << "];\n";
  ost << "for (int i = 0; i < " << unit->ordered_species.size() << "; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << MOLE_FRAC << "[i] = " << MASS_FRAC 
        << "[i] * recip_molecular_masses[i] * 1e3 * " 
        << MIXMW << ";\n";
  }
  if (!unit->use_dim)
    ost << TEMPERATURE << " *= " << T_REF << ";\n";
  ost << "for (int i = 0; i < " << unit->ordered_species.size() << "; i++)\n";
  ost << "  thermal[i] = 0.0;\n";
  for (int i = 0; i < nlite; i++)
  {
    // Subtract one because of fortran
    int light_idx = iktdif[i] - 1;
    int offset = i*order*num_species;
    // Find the species name
    int search_idx = 0;
    Species *light_spec = NULL;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      assert((*it)->molecular_mass == nwt[search_idx]);
      if (search_idx == light_idx)
      {
        light_spec = *it;
        break;
      }
      search_idx++;
    }
    assert(light_spec != NULL);
    assert(light_spec->molecular_mass == nwt[light_idx]);
    ost << "// Light species " << light_spec->name << "\n";
    PairDelim light_pair(ost);
    // Now for the each of the other species, compute their interaction
    // with the light species
    unsigned other_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if (light_idx == int(other_idx))
      {
        other_idx++;
        continue;
      }
      assert((*it)->molecular_mass == nwt[other_idx]);
      ost << "// Interaction with " << (*it)->name << "\n";
      PairDelim inter_pair(ost);
      ost << REAL << " val = " << ntdif[offset+(other_idx+1)*order-1] << ";\n";
      for (int j = (order-1); j > 0; j--)
      {
        ost << "val = val * " << TEMPERATURE << " + " << ntdif[offset+other_idx*order+j-1] << ";\n";
      }
      ost << "thermal[" << light_idx << "] += (" << MOLE_FRAC << "[" << light_idx << "]"
          << "*" << MOLE_FRAC << "[" << other_idx << "]*val);\n";
      other_idx++;
    }
  }
}

void TranLib::emit_sse_cpu_conductivity(CodeOutStream &ost)
{
  emit_vector_conductivity_declaration(ost, true, false);
  ost << "\n";
  PairDelim func_pair(ost);
  // Conducitivity needs four values: sum, sumr, log(T), mixmw
  // Also need to store constants in cache
  // Figure out how many values we can fit at a time
  const size_t total_values = unit->cache_size * (1 << 10) / sizeof(REAL_TYPE);
  // Subtract out the constant values
  // Ignore the constants for now
  //size_t iter_values = total_values - (4*unit->ordered_species.size());
  // Now divide that number by 4 for each of the values we need to store
  size_t iter_values = total_values / 4;
  // Round down if we have too many
  if ((iter_values % 2) != 0)
    iter_values--;
  // Now halve that so we get the number of vector elements
  size_t sse_values = iter_values / 2;
  // And now we have the number of elements we can store
  if (iter_values < 2)
  {
    fprintf(stderr,"Cannot block SSE kernel for cache size %ld for conductivity kernel\n", unit->cache_size);
    exit(1);
  }
  // Allocate the buffers that we'll use
  ost << VECTOR2 << " *sum = (" << VECTOR2 << "*)malloc(" 
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *sumr = (" << VECTOR2 << "*)malloc("
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *logt = (" << VECTOR2 << "*)malloc("
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *mixmw = (" << VECTOR2 << "*)malloc("
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";

  // Emit the main loop
  ost << "size_t remaining_elmts = num_elmts;\n";
  ost << "while (remaining_elmts >= " << iter_values << ")\n";
  {
    PairDelim while_pair(ost);
    // First load temperatures and compute log values, 
    // initialize sum and sumr, and load mixmw
    ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << "logt[idx] = _mm_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 2;\n";
      ost << "mixmw[idx] = _mm_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 2;\n";
      ost << "sum[idx] = _mm_set1_pd(0.0);\n";
      ost << "sumr[idx] = _mm_set1_pd(0.0);\n";
      // Compute the log of the temperatures
      if (!unit->use_dim)
        ost << "logt[idx] = _mm_mul_pd(logt[idx],_mm_set1_pd(" << T_REF << "));\n";
      ost << REAL << " t1 = _mm_cvtsd_f64(logt[idx]);\n";
      ost << REAL << " log1 = log(t1);\n";
      ost << REAL << " t0 = _mm_cvtsd_f64(_mm_shuffle_pd(logt[idx],logt[idx],1));\n";
      ost << REAL << " log0 = log(t0);\n";
      ost << "logt[idx] = _mm_set_pd(log0,log1);\n";
    }
    // Now emit the code for all the species
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      assert((*it)->molecular_mass == nwt[spec_idx]);
      ost << "// Species " << (*it)->name << "\n";
      ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
      PairDelim spec_pair(ost);
      ost << VECTOR2 << " val = _mm_set1_pd(" << nlam[(spec_idx+1)*order-1] << ");\n";
      for (int i = (order-1); i > 0; i--)
      {
        ost << "val = _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(" 
            << nlam[(spec_idx)*order+i-1] << "));\n";
      }
      // emit the exponential
      emit_sse_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
      // Convert from mass fractions to mole fractions
      ost << VECTOR2 << " frac = _mm_load_pd(" << MASS_FRAC_ARRAY << "+("
          << spec_idx << "*spec_stride)+(idx<<1));\n";
      ost << VECTOR2 << " mole = _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,"
          << "_mm_set1_pd(recip_molecular_masses[" << spec_idx << "])),"
          << "_mm_set1_pd(1e3)),mixmw[idx]);\n";
      ost << "sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);\n";
      ost << "sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);\n";
      spec_idx++;
    }
    // Now we get to write out the answer
    ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      // Unfortunately no _mm_rcp_pd
      ost << VECTOR2 << " result = _mm_add_pd(sum[idx],_mm_div_pd(_mm_set1_pd(1.0),sumr[idx]));\n";
      ost << "result = _mm_mul_pd(result,_mm_set1_pd(0.5));\n";
      if (!unit->use_dim)
      {
        double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
        lambda_ref *= 1e5;
        ost << "result = _mm_mul_pd(result,_mm_set1_pd(" << (1.0/lambda_ref) << "));\n";
      }
      ost << "_mm_store_pd(lambda,result);\n";
      ost << "lambda += 2;\n";
    }
    ost << "remaining_elmts -= " << iter_values << ";\n";
    // Also update the mole fraction array
    ost << MASS_FRAC_ARRAY << " += " << iter_values << ";\n";
  }
  // Now do the loop to clean everything up
  // Assume right now that this number is divisible by 2
  ost << "if (remaining_elmts > 0)\n";
  {
    PairDelim if_pair(ost);
    ost << "assert((remaining_elmts % 2) == 0);\n";
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << "logt[idx] = _mm_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 2;\n";
      ost << "mixmw[idx] = _mm_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 2;\n";
      ost << "sum[idx] = _mm_set1_pd(0.0);\n";
      ost << "sumr[idx] = _mm_set1_pd(0.0);\n";
      // Compute the log of the temperatures
      if (!unit->use_dim)
        ost << "logt[idx] = _mm_mul_pd(logt[idx],_mm_set1_pd(" << T_REF << "));\n";
      ost << REAL << " t1 = _mm_cvtsd_f64(logt[idx]);\n";
      ost << REAL << " log1 = log(t1);\n";
      ost << REAL << " t0 = _mm_cvtsd_f64(_mm_shuffle_pd(logt[idx],logt[idx],1));\n";
      ost << REAL << " log0 = log(t0);\n";
      ost << "logt[idx] = _mm_set_pd(log0,log1);\n";
    }
    // Now emit the code for all the species
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      assert((*it)->molecular_mass == nwt[spec_idx]);
      ost << "// Species " << (*it)->name << "\n";
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
      PairDelim spec_pair(ost);
      ost << VECTOR2 << " val = _mm_set1_pd(" << nlam[(spec_idx+1)*order-1] << ");\n";
      for (int i = (order-1); i > 0; i--)
      {
        ost << "val = _mm_add_pd(_mm_mul_pd(logt[idx],val),_mm_set1_pd(" 
            << nlam[(spec_idx)*order+i-1] << "));\n";
      }
      // emit the exponential
      emit_sse_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
      // Convert from mass fractions to mole fractions
      ost << VECTOR2 << " frac = _mm_load_pd(" << MASS_FRAC_ARRAY << "+("
          << spec_idx << "*spec_stride)+(idx<<1));\n";
      ost << VECTOR2 << " mole = _mm_mul_pd(_mm_mul_pd(_mm_mul_pd(frac,"
          << "_mm_set1_pd(recip_molecular_masses[" << spec_idx << "])),"
          << "_mm_set1_pd(1e3)),mixmw[idx]);\n";
      ost << "sum[idx] = _mm_add_pd(_mm_mul_pd(mole,val),sum[idx]);\n";
      ost << "sumr[idx] = _mm_add_pd(_mm_div_pd(mole,val),sumr[idx]);\n";
      spec_idx++;
    }
    // Now we get to write out the answer
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
    {
      PairDelim for_pair(ost);
      // Unfortunately no _mm_rcp_pd
      ost << VECTOR2 << " result = _mm_add_pd(sum[idx],_mm_div_pd(_mm_set1_pd(1.0),sumr[idx]));\n";
      ost << "result = _mm_mul_pd(result,_mm_set1_pd(0.5));\n";
      if (!unit->use_dim)
      {
        double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
        lambda_ref *= 1e5;
        ost << "result = _mm_mul_pd(result,_mm_set1_pd(" << (1.0/lambda_ref) << "));\n";
      }
      ost << "_mm_store_pd(lambda,result);\n";
      ost << "lambda += 2;\n";
    }
  }

  ost << "free(sum);\n";
  ost << "free(sumr);\n";
  ost << "free(logt);\n";
  ost << "free(mixmw);\n";
}

void TranLib::emit_sse_cpu_viscosity(CodeOutStream &ost)
{
  if (!unit->unroll_loops)
  {
    ost << "const " << REAL << " visc_constants["
        << (2*num_species*(num_species-1)) << "] = {";
    unsigned spec_idx = 0;
    bool first = true;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      for (int idx = 0; idx < num_species; idx++)
      {
        if (idx == int(spec_idx))
          continue;
        if (!first)
          ost << ",";
        else
          first = false;
        ost << (sqrt(nwt[idx]/nwt[spec_idx]));
        ost << ", " << (1.0/(sqrt(1.0+(nwt[spec_idx]/nwt[idx]))));
      }
      spec_idx++;
    }
    ost << "};\n\n";
  }
  emit_vector_viscosity_declaration(ost, true, false);
  ost << "\n";
  PairDelim func_pair(ost);
  // Viscosity needs 3 values for each point
  // and has (num_species+4)*num_species constants to store
  const size_t total_values = unit->cache_size * (1 << 10) / sizeof(REAL_TYPE);
  // Subtract out the constant values
  //size_t iter_values = total_values - ((4+unit->ordered_species.size())*unit->ordered_species.size());
  // Ignore the constants for now
  size_t iter_values = total_values;
  // Now divide that number by num_species for each of the values we need to store
  if (!unit->unroll_loops)
    iter_values /= (2+2*unit->ordered_species.size());
  else
    iter_values /= (1+2*unit->ordered_species.size());
  // Round down if we have too many
  if ((iter_values % 2) != 0)
    iter_values--;
  // Now halve that so we get the number of vector elements
  size_t sse_values = iter_values / 2;
  if (iter_values < 2)
  {
    fprintf(stderr,"Cannot block SSE kernel for cache size %ld for viscosity kernel\n", unit->cache_size);
    exit(1);
  }
  // Allocate the buffers that we'll use
  ost << VECTOR2 << " *mole_frac = (" << VECTOR2 << "*)malloc(" 
      << sse_values << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *spec_visc = (" << VECTOR2 << "*)malloc("
      << sse_values << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *result_vec = (" << VECTOR2 << "*)malloc("
      << sse_values << "*sizeof(" << VECTOR2 << "));\n";
  if (!unit->unroll_loops)
    ost << VECTOR2 << " *spec_sum = (" << VECTOR2 << "*)malloc("
        << sse_values << "*sizeof(" << VECTOR2 << "));\n";

  // Emit the main loop
  ost << "size_t remaining_elmts = num_elmts;\n";
  ost << "while (remaining_elmts >= " << iter_values << ")\n";
  {
    PairDelim while_pair(ost);
    // Load the mole fractions and compute the spec_visc values   
    ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << VECTOR2 << " temp = _mm_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 2;\n";
      unsigned spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = _mm_load_pd("
            << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<1));\n";
        spec_idx++;
      }
      // Also load the mixmw value
      ost << VECTOR2 << " mixmw = _mm_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 2;\n";
      // Initialize the output
      ost << "result_vec[idx] = _mm_set1_pd(0.0);\n";
      // Compute the temperature
      if (!unit->use_dim)
        ost << "temp = _mm_mul_pd(temp,_mm_set1_pd(" << T_REF << "));\n";
      ost << REAL << " t1 = _mm_cvtsd_f64(temp);\n";
      ost << REAL << " log1 = log(t1);\n";
      ost << REAL << " t0 = _mm_cvtsd_f64(_mm_shuffle_pd(temp,temp,1));\n";
      ost << REAL << " log0 = log(t0);\n";
      ost << VECTOR2 << " logt = _mm_set_pd(log0,log1);\n";
      // Use the log of the temperature to compute the spec viscosities
      spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        assert((*it)->molecular_mass == nwt[spec_idx]); 
        ost << "// Species " << (*it)->name << "\n";
        PairDelim spec_pair(ost);
        ost << VECTOR2 << " val = _mm_set1_pd(" << neta[(spec_idx+1)*order-1] << ");\n";
        for (int i = (order-1); i > 0; i--)
        {
          ost << "val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(" << neta[(spec_idx)*order+i-1] << "));\n";
        }
        emit_sse_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
        ost << "spec_visc[" << (spec_idx*sse_values) << "+idx] = val;\n";
        spec_idx++;
      }
      // finally compute the mole fractions from the mass fractions
      for (spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = "
            << "_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac["
            << (spec_idx*sse_values) << "+idx],_mm_set1_pd(recip_molecular_masses[" 
            << spec_idx << "])),"
            << "_mm_set1_pd(1e3)),mixmw);\n";
      }
    }
    // Now that we've got everything loaded in, do the computation
    if (!unit->unroll_loops)
    {
      ost << INT << " visc_idx = 0;\n"; 
      ost << "for (int spec_idx = 0; spec_idx < " << num_species << "; spec_idx++)\n";
      PairDelim outer_loop(ost);
      // Zero out our spec sum
      for (unsigned i = 0; i < sse_values; i++)
        ost << "spec_sum[" << i << "] = _mm_set1_pd(0.0);\n";
      ost << "for (int idx = 0; idx < " << num_species << "; idx++)\n";
      {
        PairDelim inner_loop(ost);
        ost << "if (idx == spec_idx)\n";
        {
          PairDelim if_pair(ost);
          for (unsigned i = 0; i < sse_values; i++)
            ost << "spec_sum[" << i << "] = _mm_add_pd(spec_sum[" << i << "],"
                << "_mm_mul_pd(" << MOLE_FRAC << "[idx*" << sse_values << "+" << i << "],"
                << "_mm_set1_pd(" << (4.0/sqrt(2.0)) << ")));\n";
        }
        ost << "else\n";
        {
          PairDelim else_pair(ost);
          ost << VECTOR2 << " c1 = _mm_set1_pd(visc_constants[visc_idx++]);\n";
          ost << VECTOR2 << " c2 = _mm_set1_pd(visc_constants[visc_idx++]);\n";
          ost << VECTOR2 << " numer;\n";
          for (unsigned i = 0; i < sse_values; i++)
          {
            ost << "numer = _mm_div_pd(spec_visc[spec_idx*" << sse_values << "+" << i << "],"
                << "spec_visc[idx*" << sse_values << "+" << i << "]);\n";
            ost << "numer = _mm_mul_pd(c1,numer);\n";
            ost << "numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));\n";
            ost << "spec_sum[" << i << "] = _mm_add_pd(spec_sum[" << i << "],"
                << "_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*" << sse_values << "+" << i << "],"
                << "numer),numer),c2));\n";
          }
        }
      }
      for (unsigned i = 0; i < sse_values; i++)
      {
        ost << "result_vec[" << i << "] = _mm_add_pd(result_vec[" << i << "],"
            << "_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*" << sse_values << "+" << i << "],"
            << "spec_visc[spec_idx*" << sse_values << "+" << i << "]),"
            << "spec_sum[" << i << "]));\n";
      }
    }
    else
    {
      ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
      PairDelim for_pair(ost);
      unsigned spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        ost << "// Species " << (*it)->name << "\n";
        PairDelim spec_pair(ost);
        ost << VECTOR2 << " spec_sum = _mm_set1_pd(0.0);\n";
        for (int idx = 0; idx < num_species; idx++)
        {
          PairDelim inner_pair(ost);
          if (idx == int(spec_idx))
          {
            ost << "spec_sum = _mm_add_pd(_mm_mul_pd("
                << "mole_frac[" << (idx*sse_values) << "+idx],_mm_set1_pd(" << (4.0/sqrt(2.0))
                << ")),spec_sum);\n";
          }
          else
          {
            ost << VECTOR2 << " numer = _mm_div_pd(spec_visc[" << (spec_idx*sse_values) << "+idx],spec_visc[" << (idx*sse_values) << "+idx]);\n";
            // Now take the square root
            ost << " numer = _mm_mul_pd(_mm_set1_pd("
                << (sqrt(nwt[idx]/nwt[spec_idx])) << "),numer);\n";
            ost << "numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));\n";
            ost << "spec_sum = _mm_add_pd(_mm_mul_pd(_mm_mul_pd(_mm_mul_pd("
                << "mole_frac[" << (idx*sse_values) << "+idx],numer),numer),"
                << "_mm_set1_pd(" << (1.0/(sqrt(1.0+(nwt[spec_idx]/nwt[idx])))) 
                << ")),spec_sum);\n";
          }
        }
        ost << "result_vec[idx] = _mm_add_pd(_mm_div_pd(_mm_mul_pd("
            << "mole_frac[" << (spec_idx*sse_values) << "+idx],"
            << "spec_visc[" << (spec_idx*sse_values) << "+idx]),"
            << "spec_sum),result_vec[idx]);\n";
        spec_idx++;
      }
    }
    // Finally we get to do the output
    ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      double scaling_factor = sqrt(8.0);
      if (!unit->use_dim)
        scaling_factor /= (VIS_REF * 10.0);
      ost << "result_vec[idx] = _mm_mul_pd(_mm_set1_pd(" << scaling_factor << "),result_vec[idx]);\n";
      ost << "_mm_store_pd(viscosity,result_vec[idx]);\n";
      ost << "viscosity += 2;\n";
    }
    // Advance the number of consumed species and the mass frac pointer
    ost << "remaining_elmts -= " << iter_values << ";\n";
    // Also update the mole fraction array
    ost << MASS_FRAC_ARRAY << " += " << iter_values << ";\n";
  }
  // Now do the loop to clean everything up
  // Assume right now that this number is divisible by 2
  ost << "if (remaining_elmts > 0)\n";
  {
    PairDelim if_pair(ost);
    ost << "assert((remaining_elmts % 2) == 0);\n";
    // Load the mole fractions and compute the spec_visc values   
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << VECTOR2 << " temp = _mm_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 2;\n";
      unsigned spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = _mm_load_pd("
            << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<1));\n";
        spec_idx++;
      }
      // Also load the mixmw value
      ost << VECTOR2 << " mixmw = _mm_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 2;\n";
      // Initialize the output
      ost << "result_vec[idx] = _mm_set1_pd(0.0);\n";
      // Compute the temperature
      if (!unit->use_dim)
        ost << " temp = _mm_mul_pd(temp,_mm_set1_pd(" << T_REF << "));\n";
      ost << REAL << " t1 = _mm_cvtsd_f64(temp);\n";
      ost << REAL << " log1 = log(t1);\n";
      ost << REAL << " t0 = _mm_cvtsd_f64(_mm_shuffle_pd(temp,temp,1));\n";
      ost << REAL << " log0 = log(t0);\n";
      ost << VECTOR2 << " logt = _mm_set_pd(log0,log1);\n";
      // Use the log of the temperature to compute the spec viscosities
      spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        assert((*it)->molecular_mass == nwt[spec_idx]); 
        ost << "// Species " << (*it)->name << "\n";
        PairDelim spec_pair(ost);
        ost << VECTOR2 << " val = _mm_set1_pd(" << neta[(spec_idx+1)*order-1] << ");\n";
        for (int i = (order-1); i > 0; i--)
        {
          ost << "val = _mm_add_pd(_mm_mul_pd(val,logt),_mm_set1_pd(" << neta[(spec_idx)*order+i-1] << "));\n";
        }
        emit_sse_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
        ost << "spec_visc[" << (spec_idx*sse_values) << "+idx] = val;\n";
        spec_idx++;
      }
      // finally compute the mole fractions from the mass fractions
      for (spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = "
            << "_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac["
            << (spec_idx*sse_values) << "+idx],_mm_set1_pd(recip_molecular_masses[" 
            << spec_idx << "])),"
            << "_mm_set1_pd(1e3)),mixmw);\n";
      }
    }
    // Now that we've got everything loaded in, do the computation
    if (!unit->unroll_loops)
    {
      ost << INT << " visc_idx = 0;\n"; 
      ost << "for (int spec_idx = 0; spec_idx <" << num_species << "; spec_idx++)\n";
      {
        PairDelim outer_loop(ost);
        // Zero out our spec sum
        for (unsigned i = 0; i < sse_values; i++)
          ost << "spec_sum[" << i << "] = _mm_set1_pd(0.0);\n";
        ost << "for (int idx = 0; idx < " << num_species << "; idx++)\n";
        {
          PairDelim inner_loop(ost);
          ost << "if (idx == spec_idx)\n";
          {
            PairDelim if_pair(ost);
            for (unsigned i = 0; i < sse_values; i++)
              ost << "spec_sum[" << i << "] = _mm_add_pd(spec_sum[" << i << "],"
                  << "_mm_mul_pd(" << MOLE_FRAC << "[idx*" << sse_values << "+" << i << "],"
                  << "_mm_set1_pd(" << (4.0/sqrt(2.0)) << ")));\n";
          }
          ost << "else\n";
          {
            PairDelim else_pair(ost);
            ost << VECTOR2 << " c1 = _mm_set1_pd(visc_constants[visc_idx++]);\n";
            ost << VECTOR2 << " c2 = _mm_set1_pd(visc_constants[visc_idx++]);\n";
            ost << VECTOR2 << " numer;\n";
            for (unsigned i = 0; i < sse_values; i++)
            {
              ost << "numer = _mm_div_pd(spec_visc[spec_idx*" << sse_values << "+" << i << "],"
                  << "spec_visc[idx*" << sse_values << "+" << i << "]);\n";
              ost << "numer = _mm_mul_pd(c1,numer);\n";
              ost << "numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));\n";
              ost << "spec_sum[" << i << "] = _mm_add_pd(spec_sum[" << i << "],"
                  << "_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac[idx*" << sse_values << "+" << i << "],"
                  << "numer),numer),c2));\n";
            }
          }
        }
        for (unsigned i = 0; i < sse_values; i++)
        {
          ost << "result_vec[" << i << "] = _mm_add_pd(result_vec[" << i << "],"
              << "_mm_div_pd(_mm_mul_pd(mole_frac[spec_idx*" << sse_values << "+" << i << "],"
              << "spec_visc[spec_idx*" << sse_values << "+" << i << "]),"
              << "spec_sum[" << i << "]));\n";
        }
      }
    }
    else
    {
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
      {
        PairDelim for_pair(ost);
        unsigned spec_idx = 0;
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          ost << "// Species " << (*it)->name << "\n";
          PairDelim spec_pair(ost);
          ost << VECTOR2 << " spec_sum = _mm_set1_pd(0.0);\n";
          for (int idx = 0; idx < num_species; idx++)
          {
            PairDelim inner_pair(ost);
            if (idx == int(spec_idx))
            {
              ost << "spec_sum = _mm_add_pd(_mm_mul_pd("
                  << "mole_frac[" << (idx*sse_values) << "+idx],_mm_set1_pd(" << (4.0/sqrt(2.0))
                  << ")),spec_sum);\n";
            }
            else
            {
              ost << VECTOR2 << " numer = _mm_div_pd(spec_visc[" << (spec_idx*sse_values) << "+idx],spec_visc[" << (idx*sse_values) << "+idx]);\n";
              // Now take the square root
              ost << " numer = _mm_mul_pd(_mm_set1_pd("
                  << (sqrt(nwt[idx]/nwt[spec_idx])) << "),numer);\n";
              ost << "numer = _mm_add_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(numer));\n";
              ost << "spec_sum = _mm_add_pd(_mm_mul_pd(_mm_mul_pd(_mm_mul_pd("
                  << "mole_frac[" << (idx*sse_values) << "+idx],numer),numer),"
                  << "_mm_set1_pd(" << (1.0/(sqrt(1.0+(nwt[spec_idx]/nwt[idx])))) 
                  << ")),spec_sum);\n";
            }
          }
          ost << "result_vec[idx] = _mm_add_pd(_mm_div_pd(_mm_mul_pd("
              << "mole_frac[" << (spec_idx*sse_values) << "+idx],"
              << "spec_visc[" << (spec_idx*sse_values) << "+idx]),"
              << "spec_sum),result_vec[idx]);\n";
          spec_idx++;
        }
      }
    }
    // Finally we get to do the output
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
    {
      PairDelim for_pair(ost);
      double scaling_factor = sqrt(8.0);
      if (!unit->use_dim)
        scaling_factor /= (VIS_REF * 10.0);
      ost << "result_vec[idx] = _mm_mul_pd(_mm_set1_pd(" << scaling_factor << "),result_vec[idx]);\n";
      ost << "_mm_store_pd(viscosity,result_vec[idx]);\n";
      ost << "viscosity += 2;\n";
    }
  }
  ost << "free(mole_frac);\n";
  ost << "free(spec_visc);\n";
  ost << "free(result_vec);\n";
  if (!unit->unroll_loops)
    ost << "free(spec_sum);\n";
}

void TranLib::emit_sse_cpu_diffusion(CodeOutStream &ost)
{
  if (!unit->unroll_loops)
  {
    ost << "const " << REAL << " diff_constants[" << (order*num_species*(num_species-1)/2) << "] = {";
    bool first = true;
    for (int k = 0; k < num_species; k++)
    {
      for (int j = k+1; j < num_species; j++)
      {
        int offset = j*order + k*order*num_species;
        for (int i = (order-1); i >= 0; i--)
        {
          if (!first)
            ost << ", ";
          else
            first = false;
          ost << ndif[offset+i];
        }
      }
    }
    ost << "};\n\n";
  }
  emit_vector_diffusion_declaration(ost, true, false);
  ost << "\n";
  PairDelim func_pair(ost);
  // Diffusion needs 1+3*N values for each point
  // and has (num_species+4)*num_species constants to store
  const size_t total_values = unit->cache_size * (1 << 10) / sizeof(REAL_TYPE);
  // Subtract out the constant values
  //size_t iter_values = total_values - (order*(unit->ordered_species.size()*(unit->ordered_species.size()-1)));
  // Ignore the constants for now
  size_t iter_values = total_values;
  // Now divide that number by num_species for each of the values we need to store
  iter_values /= (1+3*unit->ordered_species.size());
  // Round down if we have too many
  if ((iter_values % 2) != 0)
    iter_values--;
  // Now halve that so we get the number of vector elements
  size_t sse_values = iter_values / 2;
  if (iter_values < 2)
  {
    fprintf(stderr,"Cannot block SSE kernel for cache size %ld for diffusion kernel\n", unit->cache_size);
    exit(1);
  }
  ost << VECTOR2 << " *mole_frac = (" << VECTOR2 << "*)malloc(" << sse_values
      << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *clamped = (" << VECTOR2 << "*)malloc(" << sse_values
      << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *logt = (" << VECTOR2 << "*)malloc(" << sse_values
      << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *sumxod = (" << VECTOR2 << "*)malloc(" << sse_values
      << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR2 << "));\n";
  // Emit the main loop
  ost << "size_t remaining_elmts = num_elmts;\n";
  ost << "while (remaining_elmts >= " << iter_values << ")\n";
  {
    PairDelim while_pair(ost);
    // Emit the code to load mole fractions and the temperature
    ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      // First load the temperature 
      ost << "logt[idx] = _mm_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 2;\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = _mm_load_pd("
            << MASS_FRAC_ARRAY << "+" << spec_idx << "*spec_stride+(idx<<1));\n";
      }
      ost << VECTOR2 << " mixmw = _mm_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 2;\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "sumxod[" << (spec_idx*sse_values) << "+idx] = _mm_set1_pd(0.0);\n";
      }
      // Compute the log of the temperature
      if (!unit->use_dim)
        ost << "logt[idx] = _mm_mul_pd(logt[idx],_mm_set1_pd(" << T_REF << "));\n";
      ost << REAL << " t1 = _mm_cvtsd_f64(logt[idx]);\n";
      ost << REAL << " log1 = log(t1);\n";
      ost << REAL << " t0 = _mm_cvtsd_f64(_mm_shuffle_pd(logt[idx],logt[idx],1));\n";
      ost << REAL << " log0 = log(t0);\n";
      ost << "logt[idx] = _mm_set_pd(log0,log1);\n";
      // Compute the mole fractions
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        PairDelim spec_pair(ost);
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = "
            << "_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac["
            << (spec_idx*sse_values) << "+idx],_mm_set1_pd(recip_molecular_masses["
            << spec_idx << "])),_mm_set1_pd(1e3)),mixmw);\n";
        // Now compute the clamped values
        ost << REAL << " m1 = _mm_cvtsd_f64(mole_frac[" << (spec_idx*sse_values) << "+idx]);\n";
        ost << REAL << " c1 = (m1 > " << SMALL << ") ? m1 : " << SMALL << ";\n";
        ost << REAL << " m0 = _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[" << (spec_idx*sse_values) << "+idx],mole_frac[" << (spec_idx*sse_values) << "+idx],1));\n";
        ost << REAL << " c0 = (m0 > " << SMALL << ") ? m0 : " << SMALL << ";\n";
        ost << "clamped[" << (spec_idx*sse_values) << "+idx] = _mm_set_pd(c0, c1);\n";
      }
    }
    // Now we can do the actual computation
    if (!unit->unroll_loops)
    {
      assert(is_diffusion_symmetric()); 
      ost << INT << " diff_off = 0;\n";
      ost << "for (int k = 0; k < " << num_species << "; k++)\n";
      PairDelim outer_pair(ost);
      ost << "for (int j = k+1; j < " << num_species << "; j++)\n";
      {
        PairDelim inner_pair(ost);
        for (int i = 0; i < order; i++)
          ost << VECTOR2 << " c" << i << " = _mm_set1_pd(diff_constants[diff_off+" << i << "]);\n";
        ost << "diff_off += " << order << ";\n";
        ost << VECTOR2 << " val;\n";
        for (unsigned i = 0; i < sse_values; i++)
        {
          ost << "val = c0;\n"; 
          for (int j = 1; j < order; j++)
            ost << "val = _mm_add_pd(_mm_mul_pd(val,logt[" << i << "]),c" << j << ");\n";
          ost << "val = _mm_xor_pd(val,_mm_set1_pd(-0.0));\n";
          emit_sse_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
          ost << "sumxod[k*" << sse_values << "+" << i << "] = _mm_add_pd(_mm_mul_pd("
              << "clamped[j*" << sse_values << "+" << i << "],val),"
              << "sumxod[k*" << sse_values << "+" << i << "]);\n";
          ost << "sumxod[j*" << sse_values << "+" << i << "] = _mm_add_pd(_mm_mul_pd("
              << "clamped[k*" << sse_values << "+" << i << "],val),"
              << "sumxod[j*" << sse_values << "+" << i << "]);\n";
        }
      }
    }
    else
    {
      ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
      PairDelim for_pair(ost);
      // Just going to assume this for now
      assert(is_diffusion_symmetric());
      for (int k = 0; k < num_species; k++)
      {
        for (int j = k+1; j < num_species; j++)
        {
          ost << "// D_" << j << "_" << k << " and D_" << k << "_" << j << "\n";
          PairDelim d_pair(ost);
          int offset = j*order + k*order*num_species;
          ost << VECTOR2 << " val = _mm_set1_pd(" << ndif[offset+order-1] << ");\n";
          for (int i = (order-1); i > 0; i--)
            ost << "val = _mm_add_pd(_mm_mul_pd(val,logt[idx]),_mm_set1_pd("
                << ndif[offset+i-1] << "));\n";
          // do fast negation with -0.0, a useful case of negative zero 
          // woot floating point!
          ost << "val = _mm_xor_pd(val,_mm_set1_pd(-0.0));\n";
          emit_sse_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
          ost << "sumxod[" << (k*sse_values) << "+idx] = _mm_add_pd(_mm_mul_pd("
              << "clamped[" << (j*sse_values) << "+idx],val),"
              << "sumxod[" << (k*sse_values) << "+idx]);\n";
          ost << "sumxod[" << (j*sse_values) << "+idx] = _mm_add_pd(_mm_mul_pd("
              << "clamped[" << (k*sse_values) << "+idx],val),"
              << "sumxod[" << (j*sse_values) << "+idx]);\n";
        }
      }
    }
    // Finally we get to do the output
    ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << VECTOR2 << " sumxw = _mm_set1_pd(0.0);\n";
      ost << VECTOR2 << " wtm = _mm_set1_pd(0.0);\n";
      ost << VECTOR2 << " " << PRESSURE << " = _mm_load_pd(" << PRESSURE_ARRAY << ");\n";
      ost << PRESSURE_ARRAY << " += 2;\n";
      unsigned spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        // Make sure the molecular masses are the same otherwise we're doing something wrong
        assert((*it)->molecular_mass == nwt[spec_idx]);
        ost << "sumxw = _mm_add_pd(_mm_mul_pd(clamped[" << (spec_idx*sse_values)
            << "+idx],_mm_set1_pd(" << nwt[spec_idx] << ")),sumxw);\n";
        ost << "wtm = _mm_add_pd(_mm_mul_pd(mole_frac[" << (spec_idx*sse_values)
            << "+idx],_mm_set1_pd(" << nwt[spec_idx] << ")),wtm);\n";
        spec_idx++;
      }
      if (!unit->use_dim)
        ost << PRESSURE << " = _mm_mul_pd(" << PRESSURE << ",_mm_set1_pd("
            << P_REF << "));\n";
      ost << VECTOR2 << " pfac = _mm_div_pd(_mm_set1_pd(" << patmos << "),"
          << PRESSURE << ");\n";
      spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        PairDelim spec_pair(ost);
        ost << VECTOR2 << " result = _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd("
            << "_mm_set1_pd(" << nwt[spec_idx] << "),clamped[" << (spec_idx*sse_values)
            << "+idx])));\n";
        ost << "result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[" << (spec_idx*sse_values)
            << "+idx]));\n";
        if (!unit->use_dim)
          ost << "result = _mm_mul_pd(result,_mm_set1_pd(" << (1.0/(DIF_REF * 1e4)) << "));\n";
        // Write the value out
        ost << "_mm_store_pd(diffusion+(" << spec_idx << "*spec_stride)+(idx<<1),result);\n";
        spec_idx++;
      }
    }
    // Advance the number of consumed species and the mass frac pointer
    ost << "remaining_elmts -= " << iter_values << ";\n";
    // Also update the mole fraction array
    ost << MASS_FRAC_ARRAY << " += " << iter_values << ";\n";
    // Also update the diffusion array
    ost << "diffusion += " << iter_values << ";\n";
  }

  // Now do the loop to clean everything up
  // Assume right now that this number is divisible by 2
  ost << "if (remaining_elmts > 0)\n";
  {
    PairDelim if_pair(ost);
    ost << "assert((remaining_elmts % 2) == 0);\n";
    // Emit the code to load mole fractions and the temperature
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
    {
      PairDelim for_pair(ost);
      // First load the temperature 
      ost << "logt[idx] = _mm_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 2;\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = _mm_load_pd("
            << MASS_FRAC_ARRAY << "+" << spec_idx << "*spec_stride+(idx<<1));\n";
      }
      ost << VECTOR2 << " mixmw = _mm_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 2;\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "sumxod[" << (spec_idx*sse_values) << "+idx] = _mm_set1_pd(0.0);\n";
      }
      // Compute the log of the temperature
      if (!unit->use_dim)
        ost << "logt[idx] = _mm_mul_pd(logt[idx],_mm_set1_pd(" << T_REF << "));\n";
      ost << REAL << " t1 = _mm_cvtsd_f64(logt[idx]);\n";
      ost << REAL << " log1 = log(t1);\n";
      ost << REAL << " t0 = _mm_cvtsd_f64(_mm_shuffle_pd(logt[idx],logt[idx],1));\n";
      ost << REAL << " log0 = log(t0);\n";
      ost << "logt[idx] = _mm_set_pd(log0,log1);\n";
      // Compute the mole fractions
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        PairDelim spec_pair(ost);
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = "
            << "_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(mole_frac["
            << (spec_idx*sse_values) << "+idx],_mm_set1_pd(recip_molecular_masses["
            << spec_idx << "])),_mm_set1_pd(1e3)),mixmw);\n";
        // Now compute the clamped values
        ost << REAL << " m1 = _mm_cvtsd_f64(mole_frac[" << (spec_idx*sse_values) << "+idx]);\n";
        ost << REAL << " c1 = (m1 > " << SMALL << ") ? m1 : " << SMALL << ";\n";
        ost << REAL << " m0 = _mm_cvtsd_f64(_mm_shuffle_pd(mole_frac[" << (spec_idx*sse_values) << "+idx],mole_frac[" << (spec_idx*sse_values) << "+idx],1));\n";
        ost << REAL << " c0 = (m0 > " << SMALL << ") ? m0 : " << SMALL << ";\n";
        ost << "clamped[" << (spec_idx*sse_values) << "+idx] = _mm_set_pd(c0, c1);\n";
      }
    }
    // Now we can do the actual computation
    if (!unit->unroll_loops)
    {
      assert(is_diffusion_symmetric()); 
      ost << INT << " diff_off = 0;\n";
      ost << "for (int k = 0; k < " << num_species << "; k++)\n";
      PairDelim outer_pair(ost);
      ost << "for (int j = k+1; j < " << num_species << "; j++)\n";
      {
        PairDelim inner_pair(ost);
        assert(order == 4);
        for (int i = 0; i < order; i++)
          ost << VECTOR2 << " c" << i << " = _mm_set1_pd(diff_constants[diff_off+" << i << "]);\n";
        ost << "diff_off += " << order << ";\n";
        ost << VECTOR2 << " val;\n";
        for (unsigned i = 0; i < sse_values; i++)
        {
          ost << "val = c0;\n"; 
          for (int j = 1; j < order; j++)
            ost << "val = _mm_add_pd(_mm_mul_pd(val,logt[" << i << "]),c" << j << ");\n";
          ost << "val = _mm_xor_pd(val,_mm_set1_pd(-0.0));\n";
          emit_sse_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
          ost << "sumxod[k*" << sse_values << "+" << i << "] = _mm_add_pd(_mm_mul_pd("
              << "clamped[j*" << sse_values << "+" << i << "],val),"
              << "sumxod[k*" << sse_values << "+" << i << "]);\n";
          ost << "sumxod[j*" << sse_values << "+" << i << "] = _mm_add_pd(_mm_mul_pd("
              << "clamped[k*" << sse_values << "+" << i << "],val),"
              << "sumxod[j*" << sse_values << "+" << i << "]);\n";
        }
      }
    }
    else
    {
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
      PairDelim for_pair(ost);
      // Just going to assume this for now
      assert(is_diffusion_symmetric());
      for (int k = 0; k < num_species; k++)
      {
        for (int j = k+1; j < num_species; j++)
        {
          ost << "// D_" << j << "_" << k << " and D_" << k << "_" << j << "\n";
          PairDelim d_pair(ost);
          int offset = j*order + k*order*num_species;
          ost << VECTOR2 << " val = _mm_set1_pd(" << ndif[offset+order-1] << ");\n";
          for (int i = (order-1); i > 0; i--)
            ost << "val = _mm_add_pd(_mm_mul_pd(val,logt[idx]),_mm_set1_pd("
                << ndif[offset+i-1] << "));\n";
          // do fast negation with -0.0, a useful case of negative zero 
          // woot floating point!
          ost << "val = _mm_xor_pd(val,_mm_set1_pd(-0.0));\n";
          emit_sse_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
          ost << "sumxod[" << (k*sse_values) << "+idx] = _mm_add_pd(_mm_mul_pd("
              << "clamped[" << (j*sse_values) << "+idx],val),"
              << "sumxod[" << (k*sse_values) << "+idx]);\n";
          ost << "sumxod[" << (j*sse_values) << "+idx] = _mm_add_pd(_mm_mul_pd("
              << "clamped[" << (k*sse_values) << "+idx],val),"
              << "sumxod[" << (j*sse_values) << "+idx]);\n";
        }
      }
    }
    // Finally we get to do the output
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << VECTOR2 << " sumxw = _mm_set1_pd(0.0);\n";
      ost << VECTOR2 << " wtm = _mm_set1_pd(0.0);\n";
      ost << VECTOR2 << " " << PRESSURE << " = _mm_load_pd(" << PRESSURE_ARRAY << ");\n";
      ost << PRESSURE_ARRAY << " += 2;\n";
      unsigned spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        // Make sure the molecular masses are the same otherwise we're doing something wrong
        assert((*it)->molecular_mass == nwt[spec_idx]);
        ost << "sumxw = _mm_add_pd(_mm_mul_pd(clamped[" << (spec_idx*sse_values)
            << "+idx],_mm_set1_pd(" << nwt[spec_idx] << ")),sumxw);\n";
        ost << "wtm = _mm_add_pd(_mm_mul_pd(mole_frac[" << (spec_idx*sse_values)
            << "+idx],_mm_set1_pd(" << nwt[spec_idx] << ")),wtm);\n";
        spec_idx++;
      }
      if (!unit->use_dim)
        ost << PRESSURE << " = _mm_mul_pd(" << PRESSURE << ",_mm_set1_pd("
            << P_REF << "));\n";
      ost << VECTOR2 << " pfac = _mm_div_pd(_mm_set1_pd(" << patmos << "),"
          << PRESSURE << ");\n";
      spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        PairDelim spec_pair(ost);
        ost << VECTOR2 << " result = _mm_mul_pd(pfac,_mm_sub_pd(sumxw,_mm_mul_pd("
            << "_mm_set1_pd(" << nwt[spec_idx] << "),clamped[" << (spec_idx*sse_values)
            << "+idx])));\n";
        ost << "result = _mm_div_pd(result,_mm_mul_pd(wtm,sumxod[" << (spec_idx*sse_values)
            << "+idx]));\n";
        if (!unit->use_dim)
          ost << "result = _mm_mul_pd(result,_mm_set1_pd(" << (1.0/(DIF_REF * 1e4)) << "));\n";
        // Write the value out
        ost << "_mm_store_pd(diffusion+(" << spec_idx << "*spec_stride)+(idx<<1),result);\n";
        spec_idx++;
      }
    }
  }

  ost << "free(mole_frac);\n";
  ost << "free(clamped);\n";
  ost << "free(logt);\n";
  ost << "free(sumxod);\n";
}

void TranLib::emit_sse_cpu_thermal(CodeOutStream &ost)
{
  emit_vector_thermal_declaration(ost, true, false);
  ost << "\n";
  PairDelim func_pair(ost);
  // Thermal needs 1+N values for each point
  // and has (num_species+4)*num_species constants to store
  const size_t total_values = unit->cache_size * (1 << 10) / sizeof(REAL_TYPE);
  // Subtract out the constant values
  // Ignore the constants for now
  //size_t iter_values = total_values - (4*(unit->ordered_species.size()*nlite));
  // Now divide that number by num_species for each of the values we need to store
  size_t iter_values = total_values / (1+2*unit->ordered_species.size());
  // Round down if we have too many
  if ((iter_values % 2) != 0)
    iter_values--;
  // Now halve that so we get the number of vector elements
  size_t sse_values = iter_values / 2;
  if (iter_values < 2)
  {
    fprintf(stderr,"Cannot block SSE kernel for cache size %ld for thermal kernel\n", unit->cache_size);
    exit(1);
  }
  ost << VECTOR2 << " *mole_frac = (" << VECTOR2 << "*)malloc(" << sse_values
      << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *temperature = (" << VECTOR2 << "*)malloc(" << sse_values
      << "*sizeof(" << VECTOR2 << "));\n";
  ost << VECTOR2 << " *thermal_vec = (" << VECTOR2 << "*)malloc(" << sse_values
      << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR2 << "));\n";
  // Emit the main loop
  ost << "size_t remaining_elmts = num_elmts;\n";
  ost << "while (remaining_elmts >= " << iter_values << ")\n";
  {
    PairDelim while_pair(ost);
    ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << "temperature[idx] = _mm_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 2;\n";
      ost << VECTOR2 << " mixmw = _mm_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 2;\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = _mm_load_pd("
            << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<1));\n";
      }
      if (!unit->use_dim)
        ost << "temperature[idx] = _mm_mul_pd(temperature[idx],_mm_set1_pd(" << T_REF << "));\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = _mm_mul_pd(_mm_mul_pd("
            << "_mm_mul_pd(mole_frac[" << (spec_idx*sse_values) << "+idx],"
            << "_mm_set1_pd(recip_molecular_masses[" << spec_idx << "])),"
            << "_mm_set1_pd(1e3)),mixmw);\n";
        ost << "thermal_vec[" << (spec_idx*sse_values) << "+idx] = _mm_set1_pd(0.0);\n";
      }
    }
    // Now we've got all the values so do the computation
    ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      for (int i = 0; i < nlite; i++)
      {
        // Subtract one because of fortran
        int light_idx = iktdif[i] - 1;
        int offset = i*order*num_species;
        // Find the species name
        int search_idx = 0;
        Species *light_spec = NULL;
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          if (strcmp((*it)->name,"M") == 0)
            continue;
          assert((*it)->molecular_mass == nwt[search_idx]);
          if (search_idx == light_idx)
          {
            light_spec = *it;
            break;
          }
          search_idx++;
        }
        assert(light_spec != NULL);
        assert(light_spec->molecular_mass == nwt[light_idx]);
        ost << "// Light species " << light_spec->name << "\n";
        PairDelim light_pair(ost);
        unsigned other_idx = 0;
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          if (light_idx == int(other_idx))
          {
            other_idx++;
            continue;
          }
          assert((*it)->molecular_mass == nwt[other_idx]);
          ost << "// Interaction with " << (*it)->name << "\n";
          PairDelim inter_pair(ost);
          ost << VECTOR2 << " val = _mm_set1_pd(" << ntdif[offset+(other_idx+1)*order-1] << ");\n";
          for (int j = (order-1); j > 0; j--)
          {
            ost << "val = _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd("
                << ntdif[offset+other_idx*order+j-1] << "));\n";
          }
          ost << "thermal_vec[" << (light_idx*sse_values) << "+idx] = _mm_add_pd("
              << "_mm_mul_pd(mole_frac[" << (light_idx*sse_values) << "+idx],"
              << "mole_frac[" << (other_idx*sse_values) << "+idx]),thermal_vec["
              << (light_idx*sse_values) << "+idx]);\n";
          other_idx++;
        }
      }
    }    
    // Finally we can write out the values
    ost << "for (unsigned idx = 0; idx < " << sse_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "_mm_store_pd(thermal+(" << spec_idx << "*spec_stride)+(idx<<1),"
            << "thermal_vec[" << (spec_idx*sse_values) << "+idx]);\n";
      }
    }
    // Advance the number of consumed species and the mass frac pointer
    ost << "remaining_elmts -= " << iter_values << ";\n";
    // Also update the mole fraction array
    ost << MASS_FRAC_ARRAY << " += " << iter_values << ";\n";
    // Also update the diffusion array
    ost << "thermal += " << iter_values << ";\n";
  }

  // Now do the loop to clean everything up
  // Assume right now that this number is divisible by 2
  ost << "if (remaining_elmts > 0)\n";
  {
    PairDelim if_pair(ost);
    ost << "assert((remaining_elmts % 2) == 0);\n";
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << "temperature[idx] = _mm_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 2;\n";
      ost << VECTOR2 << " mixmw = _mm_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 2;\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = _mm_load_pd("
            << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<1));\n";
      }
      if (!unit->use_dim)
        ost << "temperature[idx] = _mm_mul_pd(temperature[idx],_mm_set1_pd(" << T_REF << "));\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*sse_values) << "+idx] = _mm_mul_pd(_mm_mul_pd("
            << "_mm_mul_pd(mole_frac[" << (spec_idx*sse_values) << "+idx],"
            << "_mm_set1_pd(recip_molecular_masses[" << spec_idx << "])),"
            << "_mm_set1_pd(1e3)),mixmw);\n";
        ost << "thermal_vec[" << (spec_idx*sse_values) << "+idx] = _mm_set1_pd(0.0);\n";
      }
    }
    // Now we've got all the values so do the computation
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
    {
      PairDelim for_pair(ost);
      for (int i = 0; i < nlite; i++)
      {
        // Subtract one because of fortran
        int light_idx = iktdif[i] - 1;
        int offset = i*order*num_species;
        // Find the species name
        int search_idx = 0;
        Species *light_spec = NULL;
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          if (strcmp((*it)->name,"M") == 0)
            continue;
          assert((*it)->molecular_mass == nwt[search_idx]);
          if (search_idx == light_idx)
          {
            light_spec = *it;
            break;
          }
          search_idx++;
        }
        assert(light_spec != NULL);
        assert(light_spec->molecular_mass == nwt[light_idx]);
        ost << "// Light species " << light_spec->name << "\n";
        PairDelim light_pair(ost);
        unsigned other_idx = 0;
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          if (light_idx == int(other_idx))
          {
            other_idx++;
            continue;
          }
          assert((*it)->molecular_mass == nwt[other_idx]);
          ost << "// Interaction with " << (*it)->name << "\n";
          PairDelim inter_pair(ost);
          ost << VECTOR2 << " val = _mm_set1_pd(" << ntdif[offset+(other_idx+1)*order-1] << ");\n";
          for (int j = (order-1); j > 0; j--)
          {
            ost << "val = _mm_add_pd(_mm_mul_pd(val,temperature[idx]),_mm_set1_pd("
                << ntdif[offset+other_idx*order+j-1] << "));\n";
          }
          ost << "thermal_vec[" << (light_idx*sse_values) << "+idx] = _mm_add_pd("
              << "_mm_mul_pd(mole_frac[" << (light_idx*sse_values) << "+idx],"
              << "mole_frac[" << (other_idx*sse_values) << "+idx]),thermal_vec["
              << (light_idx*sse_values) << "+idx]);\n";
          other_idx++;
        }
      }
    }    
    // Finally we can write out the values
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/2); idx++)\n";
    {
      PairDelim for_pair(ost);
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "_mm_store_pd(thermal+(" << spec_idx << "*spec_stride)+(idx<<1),"
            << "thermal_vec[" << (spec_idx*sse_values) << "+idx]);\n";
      }
    }
  }

  ost << "free(mole_frac);\n";
  ost << "free(temperature);\n";
  ost << "free(thermal_vec);\n";
}

void TranLib::emit_avx_cpu_conductivity(CodeOutStream &ost)
{
  emit_vector_conductivity_declaration(ost, false, true);
  ost << "\n";
  PairDelim func_pair(ost);
  // Conducitivity needs four values: sum, sumr, log(T), mixmw
  // Also need to store constants in cache
  // Figure out how many values we can fit at a time
  const size_t total_values = unit->cache_size * (1 << 10) / sizeof(REAL_TYPE);
  // Subtract out the constant values
  // Ignore the constant values for now
  //size_t iter_values = total_values - (4*unit->ordered_species.size());
  // Now divide that number by 4 for each of the values we need to store
  size_t iter_values = total_values / 4;
  // ROund down if we have too many
  while ((iter_values % 4) != 0)
    iter_values--;
  // Now divide by four to get the number of vector elements
  size_t avx_values = iter_values / 4;
  if (iter_values < 4)
  {
    fprintf(stderr,"Cannot block AVX kernel for cache size %ld for conductivity kernel\n",
            unit->cache_size);
    exit(1);
  }
  // Allocate the buffers that we'll use
  ost << VECTOR4 << " *sum = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *sumr = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *logt = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *mixmw = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";

  // Emit the main loop
  ost << "size_t remaining_elmts = num_elmts;\n";
  ost << "while (remaining_elmts >= " << iter_values << ")\n";
  {
    PairDelim while_pair(ost);
    // First load temperature and compute log values
    ost << "if (aligned<32>(" << TEMPERATURE_ARRAY << ") && aligned<32>("
        << MIXMW_ARRAY << "))\n";
    {
      PairDelim if_pair(ost);
      ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
      {
        PairDelim for_pair(ost);
        ost << "logt[idx] = _mm256_load_pd(" << TEMPERATURE_ARRAY << ");\n";
        ost << TEMPERATURE_ARRAY << " += 4;\n";
        ost << "mixmw[idx] = _mm256_load_pd(" << MIXMW_ARRAY << ");\n";
        ost << MIXMW_ARRAY << " += 4;\n";
        ost << "sum[idx] = _mm256_set1_pd(0.0);\n";
        ost << "sumr[idx] = _mm256_set1_pd(0.0);\n";
        if (!unit->use_dim)
          ost << "logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd("
              << T_REF << "));\n";
        // Compute the log
        ost << VECTOR2 << " lower = _mm256_extractf128_pd(logt[idx],0);\n";
        ost << VECTOR2 << " upper = _mm256_extractf128_pd(logt[idx],1);\n";
        ost << "logt[idx] = _mm256_set_pd("
            << "log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
            << "log(_mm_cvtsd_f64(upper)),"
            << "log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
            << "log(_mm_cvtsd_f64(lower)));\n"; 
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
      {
        PairDelim for_pair(ost);
        ost << "logt[idx] = _mm256_loadu_pd(" << TEMPERATURE_ARRAY << ");\n";
        ost << TEMPERATURE_ARRAY << " += 4;\n";
        ost << "mixmw[idx] = _mm256_loadu_pd(" << MIXMW_ARRAY << ");\n";
        ost << MIXMW_ARRAY << " += 4;\n";
        ost << "sum[idx] = _mm256_set1_pd(0.0);\n";
        ost << "sumr[idx] = _mm256_set1_pd(0.0);\n";
        if (!unit->use_dim)
          ost << "logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd("
              << T_REF << "));\n";
        // Compute the log
        ost << VECTOR2 << " lower = _mm256_extractf128_pd(logt[idx],0);\n";
        ost << VECTOR2 << " upper = _mm256_extractf128_pd(logt[idx],1);\n";
        ost << "logt[idx] = _mm256_set_pd("
            << "log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
            << "log(_mm_cvtsd_f64(upper)),"
            << "log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
            << "log(_mm_cvtsd_f64(lower)));\n"; 
      }
    }
    // Now wemit the code for all the species
    unsigned spec_idx = 0;
    ost << "if (aligned<32>(" MASS_FRAC_ARRAY << "))\n";
    {
      PairDelim if_pair(ost);
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        assert((*it)->molecular_mass == nwt[spec_idx]);
        ost << "// Species " << (*it)->name << "\n";
        ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
        PairDelim spec_pair(ost);
        ost << VECTOR4 << " val = _mm256_set1_pd(" << nlam[(spec_idx+1)*order-1] << ");\n";
        for (int i = (order-1); i > 0; i--)
        {
          ost << "val = _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd("
              << nlam[(spec_idx)*order+i-1] << "));\n";
        }
        // emit the exponential
        emit_avx_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
        // Convert from mass fractions to mole fractions
        ost << VECTOR4 << " frac = _mm256_load_pd("<< MASS_FRAC_ARRAY << "+("
            << spec_idx << "*spec_stride)+(idx<<2));\n";
        ost << VECTOR4 << " mole = _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,"
            << "_mm256_set1_pd(recip_molecular_masses[" << spec_idx << "])),"
            << "_mm256_set1_pd(1e3)),mixmw[idx]);\n";
        ost << "sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);\n";
        ost << "sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);\n";
        spec_idx++;
      }
    }
    spec_idx = 0;
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        assert((*it)->molecular_mass == nwt[spec_idx]);
        ost << "// Species " << (*it)->name << "\n";
        ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
        PairDelim spec_pair(ost);
        ost << VECTOR4 << " val = _mm256_set1_pd(" << nlam[(spec_idx+1)*order-1] << ");\n";
        for (int i = (order-1); i > 0; i--)
        {
          ost << "val = _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd("
              << nlam[(spec_idx)*order+i-1] << "));\n";
        }
        // emit the exponential
        emit_avx_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
        // Convert from mass fractions to mole fractions
        ost << VECTOR4 << " frac = _mm256_loadu_pd("<< MASS_FRAC_ARRAY << "+("
            << spec_idx << "*spec_stride)+(idx<<2));\n";
        ost << VECTOR4 << " mole = _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,"
            << "_mm256_set1_pd(recip_molecular_masses[" << spec_idx << "])),"
            << "_mm256_set1_pd(1e3)),mixmw[idx]);\n";
        ost << "sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);\n";
        ost << "sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);\n";
        spec_idx++;
      }
    }
    // Now we write out the answer
    ost << "if (aligned<32>(lambda))\n";
    {
      PairDelim if_pair(ost);
      ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
      {
        PairDelim for_pair(ost);
        ost << VECTOR4 << " result = _mm256_add_pd(sum[idx],_mm256_div_pd(_mm256_set1_pd(1.0),sumr[idx]));\n";
        ost << "result = _mm256_mul_pd(result,_mm256_set1_pd(0.5));\n";
        if (!unit->use_dim)
        {
          double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
          lambda_ref *= 1e5;
          ost << "result = _mm256_mul_pd(result,_mm256_set1_pd(" << (1.0/lambda_ref) << "));\n";
        }
        ost << "_mm256_stream_pd(lambda,result);\n";
        ost << "lambda += 4;\n";
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
      {
        PairDelim for_pair(ost);
        ost << VECTOR4 << " result = _mm256_add_pd(sum[idx],_mm256_div_pd(_mm256_set1_pd(1.0),sumr[idx]));\n";
        ost << "result = _mm256_mul_pd(result,_mm256_set1_pd(0.5));\n";
        if (!unit->use_dim)
        {
          double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
          lambda_ref *= 1e5;
          ost << "result = _mm256_mul_pd(result,_mm256_set1_pd(" << (1.0/lambda_ref) << "));\n";
        }
        ost << "_mm256_storeu_pd(lambda,result);\n";
        ost << "lambda += 4;\n";
      }
    }
    ost << "remaining_elmts -= " << iter_values << ";\n";
    // Also update the mole fraction array
    ost << MASS_FRAC_ARRAY << " += " << iter_values << ";\n";
  }
  // Now to the loop to clean everything up
  // Assume right now that this is divisble by 4
  ost << "if (remaining_elmts > 0)\n";
  {
    PairDelim if_pair(ost);
    ost << "assert((remaining_elmts % 4) == 0);\n";
    ost << "if (aligned<32>(" << TEMPERATURE_ARRAY << ") && aligned<32>("
        << MIXMW_ARRAY << "))\n";
    {
      PairDelim if2_pair(ost);
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
      {
        PairDelim for_pair(ost);
        ost << "logt[idx] = _mm256_load_pd(" << TEMPERATURE_ARRAY << ");\n";
        ost << TEMPERATURE_ARRAY << " += 4;\n";
        ost << "mixmw[idx] = _mm256_load_pd(" << MIXMW_ARRAY << ");\n";
        ost << MIXMW_ARRAY << " += 4;\n";
        ost << "sum[idx] = _mm256_set1_pd(0.0);\n";
        ost << "sumr[idx] = _mm256_set1_pd(0.0);\n";
        if (!unit->use_dim)
          ost << "logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd("
              << T_REF << "));\n";
        // Compute the log
        ost << VECTOR2 << " lower = _mm256_extractf128_pd(logt[idx],0);\n";
        ost << VECTOR2 << " upper = _mm256_extractf128_pd(logt[idx],1);\n";
        ost << "logt[idx] = _mm256_set_pd("
            << "log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
            << "log(_mm_cvtsd_f64(upper)),"
            << "log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
            << "log(_mm_cvtsd_f64(lower)));\n"; 
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
      {
        PairDelim for_pair(ost);
        ost << "logt[idx] = _mm256_loadu_pd(" << TEMPERATURE_ARRAY << ");\n";
        ost << TEMPERATURE_ARRAY << " += 4;\n";
        ost << "mixmw[idx] = _mm256_loadu_pd(" << MIXMW_ARRAY << ");\n";
        ost << MIXMW_ARRAY << " += 4;\n";
        ost << "sum[idx] = _mm256_set1_pd(0.0);\n";
        ost << "sumr[idx] = _mm256_set1_pd(0.0);\n";
        if (!unit->use_dim)
          ost << "logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd("
              << T_REF << "));\n";
        // Compute the log
        ost << VECTOR2 << " lower = _mm256_extractf128_pd(logt[idx],0);\n";
        ost << VECTOR2 << " upper = _mm256_extractf128_pd(logt[idx],1);\n";
        ost << "logt[idx] = _mm256_set_pd("
            << "log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
            << "log(_mm_cvtsd_f64(upper)),"
            << "log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
            << "log(_mm_cvtsd_f64(lower)));\n"; 
      }
    }
    // Now wemit the code for all the species
    unsigned spec_idx = 0;
    ost << "if (aligned<32>(" << MASS_FRAC_ARRAY << "))\n";
    {
      PairDelim if_pair(ost);
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        assert((*it)->molecular_mass == nwt[spec_idx]);
        ost << "// Species " << (*it)->name << "\n";
        ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
        PairDelim spec_pair(ost);
        ost << VECTOR4 << " val = _mm256_set1_pd(" << nlam[(spec_idx+1)*order-1] << ");\n";
        for (int i = (order-1); i > 0; i--)
        {
          ost << "val = _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd("
              << nlam[(spec_idx)*order+i-1] << "));\n";
        }
        // emit the exponential
        emit_avx_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
        // Convert from mass fractions to mole fractions
        ost << VECTOR4 << " frac = _mm256_load_pd("<< MASS_FRAC_ARRAY << "+("
            << spec_idx << "*spec_stride)+(idx<<2));\n";
        ost << VECTOR4 << " mole = _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,"
            << "_mm256_set1_pd(recip_molecular_masses[" << spec_idx << "])),"
            << "_mm256_set1_pd(1e3)),mixmw[idx]);\n";
        ost << "sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);\n";
        ost << "sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);\n";
        spec_idx++;
      }
    }
    spec_idx = 0;
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        assert((*it)->molecular_mass == nwt[spec_idx]);
        ost << "// Species " << (*it)->name << "\n";
        ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
        PairDelim spec_pair(ost);
        ost << VECTOR4 << " val = _mm256_set1_pd(" << nlam[(spec_idx+1)*order-1] << ");\n";
        for (int i = (order-1); i > 0; i--)
        {
          ost << "val = _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd("
              << nlam[(spec_idx)*order+i-1] << "));\n";
        }
        // emit the exponential
        emit_avx_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
        // Convert from mass fractions to mole fractions
        ost << VECTOR4 << " frac = _mm256_loadu_pd("<< MASS_FRAC_ARRAY << "+("
            << spec_idx << "*spec_stride)+(idx<<2));\n";
        ost << VECTOR4 << " mole = _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,"
            << "_mm256_set1_pd(recip_molecular_masses[" << spec_idx << "])),"
            << "_mm256_set1_pd(1e3)),mixmw[idx]);\n";
        ost << "sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);\n";
        ost << "sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);\n";
        spec_idx++;
      }
    }
    // Now we write out the answer
    ost << "if (aligned<32>(lambda))\n";
    {
      PairDelim if_pair(ost);
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
      {
        PairDelim for_pair(ost);
        ost << VECTOR4 << " result = _mm256_add_pd(sum[idx],_mm256_div_pd(_mm256_set1_pd(1.0),sumr[idx]));\n";
        ost << "result = _mm256_mul_pd(result,_mm256_set1_pd(0.5));\n";
        if (!unit->use_dim)
        {
          double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
          lambda_ref *= 1e5;
          ost << "result = _mm256_mul_pd(result,_mm256_set1_pd(" << (1.0/lambda_ref) << "));\n";
        }
        ost << "_mm256_stream_pd(lambda,result);\n";
        ost << "lambda += 4;\n";
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
      {
        PairDelim for_pair(ost);
        ost << VECTOR4 << " result = _mm256_add_pd(sum[idx],_mm256_div_pd(_mm256_set1_pd(1.0),sumr[idx]));\n";
        ost << "result = _mm256_mul_pd(result,_mm256_set1_pd(0.5));\n";
        if (!unit->use_dim)
        {
          double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
          lambda_ref *= 1e5;
          ost << "result = _mm256_mul_pd(result,_mm256_set1_pd(" << (1.0/lambda_ref) << "));\n";
        }
        ost << "_mm256_storeu_pd(lambda,result);\n";
        ost << "lambda += 4;\n";
      }
    }
  }
  ost << "free(sum);\n";
  ost << "free(sumr);\n";
  ost << "free(logt);\n";
  ost << "free(mixmw);\n";
}

void TranLib::emit_avx_cpu_viscosity(CodeOutStream &ost)
{
  if (!unit->unroll_loops)
  {
    ost << "const " << REAL << " visc_constants["
        << (2*num_species*(num_species-1)) << "] = {";
    unsigned spec_idx = 0;
    bool first = true;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      for (int idx = 0; idx < num_species; idx++)
      {
        if (idx == int(spec_idx))
          continue;
        if (!first)
          ost << ",";
        else
          first = false;
        ost << (sqrt(nwt[idx]/nwt[spec_idx]));
        ost << ", " << (1.0/(sqrt(1.0+(nwt[spec_idx]/nwt[idx]))));
      }
      spec_idx++;
    }
    ost << "};\n\n";
  }
  emit_vector_viscosity_declaration(ost, false, true);
  ost << "\n";
  PairDelim func_pair(ost);
  // Viscosity needs 3 values for each point
  // and has (num_species+4)*num_species constants to store
  const size_t total_values = unit->cache_size * (1 << 10) / sizeof(REAL_TYPE);
  // Subtract out the constant values
  // Ignore the constants for now
  //size_t iter_values = total_values - ((4+unit->ordered_species.size())*unit->ordered_species.size());
  size_t iter_values = total_values;
  // Now divide that number by num_species for each of the values we need to store
  if (!unit->unroll_loops)
    iter_values /= (2+2*unit->ordered_species.size());
  else
    iter_values /= (1+2*unit->ordered_species.size());
  // Round down if we have too many
  while ((iter_values % 4) != 0)
    iter_values--;
  size_t avx_values = iter_values / 4;
  if (iter_values < 4)
  {
    fprintf(stderr,"Cannot block AVX kernel for cache size %ld for viscosity kernel\n",
            unit->cache_size);
    exit(1);
  }
  // Allocate the buffers that we'll use
  ost << VECTOR4 << " *mole_frac = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *spec_visc = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *result_vec = (" << VECTOR4 << "*)memalign(32,"
      << avx_values << "*sizeof(" << VECTOR4 << "));\n";
  if (!unit->unroll_loops)
    ost << VECTOR4 << " *spec_sum = (" << VECTOR4 << "*)memalign(32,"
        << avx_values << "*sizeof(" << VECTOR4 << "));\n";

  // Emit the main loop
  ost << "size_t remaining_elmts = num_elmts;\n";
  ost << "while (remaining_elmts >= " << iter_values << ")\n";
  {
    PairDelim while_pair(ost);
    // Load the mole fractions and compute the spec values
    ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << VECTOR4 << " temp;\n";
      ost << "if (aligned<32>(" << TEMPERATURE_ARRAY << "))\n";
      ost << "  temp = _mm256_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << "else\n";
      ost << "  temp = _mm256_loadu_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 4;\n";
      unsigned spec_idx = 0;
      ost << "if (aligned<32>(" << MASS_FRAC_ARRAY << "))\n";
      {
        PairDelim if_pair(ost);
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_load_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
          spec_idx++;
        }
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_loadu_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
          spec_idx++;
        }
      }
      // Alos load the mixmw value
      ost << VECTOR4 << " mixmw;\n"; 
      ost << "if (aligned<32>(" << MIXMW_ARRAY << "))\n";
      ost << "  mixmw = _mm256_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << "else\n";
      ost << "  mixmw = _mm256_loadu_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 4;\n";
      // Initialize the output
      ost << "result_vec[idx] = _mm256_set1_pd(0.0);\n";
      if (!unit->use_dim)
        ost << "temp = _mm256_mul_pd(temp,_mm256_set1_pd(" << T_REF << "));\n";
      ost << VECTOR2 << " lower = _mm256_extractf128_pd(temp,0);\n";
      ost << VECTOR2 << " upper = _mm256_extractf128_pd(temp,1);\n";
      ost << VECTOR4 << " logt = _mm256_set_pd("
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
          << "log(_mm_cvtsd_f64(upper)),"
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
          << "log(_mm_cvtsd_f64(lower)));\n";
      // Use the temperature to compute the spec viscosities
      spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        assert((*it)->molecular_mass == nwt[spec_idx]); 
        ost << "// Species " << (*it)->name << "\n";
        PairDelim spec_pair(ost);
        ost << VECTOR4 << " val = _mm256_set1_pd(" << neta[(spec_idx+1)*order-1] << ");\n";
        for (int i = (order-1); i > 0; i--)
        {
          ost << "val = _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd("
              << neta[(spec_idx)*order+i-1] << "));\n";
        }
        emit_avx_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
        ost << "spec_visc[" << (spec_idx*avx_values) << "+idx] = val;\n";
        spec_idx++;
      }
      // Finally compute the mole fractions from the mass fractions
      for (spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = "
            << "_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac["
            << (spec_idx*avx_values) << "+idx],_mm256_set1_pd(recip_molecular_masses["
            << spec_idx << "])),"
            << "_mm256_set1_pd(1e3)),mixmw);\n";
      }
    }
    // Now that we've got everything loaded in, do the computation
    if (!unit->unroll_loops)
    {
      ost << INT << " visc_idx = 0;\n";
      ost << "for (int spec_idx = 0; spec_idx < " << num_species << "; spec_idx++)\n";
      PairDelim outer_loop(ost);
      // Zero out our spec sum
      for (unsigned i = 0; i < avx_values; i++)
        ost << "spec_sum[" << i << "] = _mm256_set1_pd(0.0);\n";
      ost << "for (int idx = 0; idx < " << num_species << "; idx++)\n";
      {
        PairDelim inner_loop(ost);
        ost << "if (idx == spec_idx)\n";
        {
          PairDelim if_pair(ost);
          for (unsigned i = 0; i < avx_values; i++)
            ost << "spec_sum[" << i << "] = _mm256_add_pd(spec_sum[" << i << "],"
                << "_mm256_mul_pd(" << MOLE_FRAC << "[idx*" << avx_values << "+" << i << "],"
                << "_mm256_set1_pd(" << (4.0/sqrt(2.0)) << ")));\n";
        }
        ost << "else\n";
        {
          PairDelim else_pair(ost);
          ost << VECTOR4 << " c1 = _mm256_set1_pd(visc_constants[visc_idx++]);\n";
          ost << VECTOR4 << " c2 = _mm256_set1_pd(visc_constants[visc_idx++]);\n";
          ost << VECTOR4 << " numer;\n";
          for (unsigned i = 0; i < avx_values; i++)
          {
            ost << "numer = _mm256_div_pd(spec_visc[spec_idx*" << avx_values << "+" << i << "],"
                << "spec_visc[idx*" << avx_values << "+" << i << "]);\n";
            ost << "numer = _mm256_mul_pd(c1,numer);\n";
            ost << "numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));\n";
            ost << "spec_sum[" << i << "] = _mm256_add_pd(spec_sum[" << i << "],"
                << "_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*" << avx_values << "+" << i << "],"
                << "numer),numer),c2));\n";
          }
        }
      }
      for (unsigned i = 0; i < avx_values; i++)
      {
        ost << "result_vec[" << i << "] = _mm256_add_pd(result_vec[" << i << "],"
            << "_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*" << avx_values << "+" << i << "],"
            << "spec_visc[spec_idx*" << avx_values << "+" << i << "]),"
            << "spec_sum[" << i << "]));\n";
      }
    }
    else
    {
      ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
      PairDelim for_pair(ost);
      unsigned spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        ost << "// Species " << (*it)->name << "\n";
        PairDelim spec_pair(ost);
        ost << VECTOR4 << " spec_sum = _mm256_set1_pd(0.0);\n";
        for (int idx = 0; idx < num_species; idx++)
        {
          PairDelim inner_pair(ost);
          if (idx == int(spec_idx))
          {
            ost << "spec_sum = _mm256_add_pd(_mm256_mul_pd("
                << "mole_frac[" << (idx*avx_values) << "+idx],_mm256_set1_pd("
                << (4.0/sqrt(2.0)) << ")),spec_sum);\n";
          }
          else
          {
            ost << VECTOR4 << " numer = _mm256_div_pd(spec_visc["
                << (spec_idx*avx_values) << "+idx],spec_visc["
                << (idx*avx_values) << "+idx]);\n";
            // Now take the square root
            ost << "numer = _mm256_mul_pd(_mm256_set1_pd("
                << (sqrt(nwt[idx]/nwt[spec_idx])) << "),numer);\n";
            ost << "numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));\n";
            ost << "spec_sum = _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd("
                << "mole_frac[" << (idx*avx_values) << "+idx],numer),numer),"
                << "_mm256_set1_pd(" << (1.0/sqrt(1.0+(nwt[spec_idx]/nwt[idx])))
                << ")),spec_sum);\n";
          }
        }
        ost << "result_vec[idx] = _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd("
            << "mole_frac[" << (spec_idx*avx_values) << "+idx],"
            << "spec_visc[" << (spec_idx*avx_values) << "+idx]),"
            << "spec_sum),result_vec[idx]);\n";
        spec_idx++;
      }
    }
    // Finally we get to do the output
    ost << "if (aligned<32>(viscosity))\n";
    {
      PairDelim if_pair(ost);
      ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
      {
        PairDelim for_pair(ost);
        double scaling_factor = sqrt(8.0);
        if (!unit->use_dim)
          scaling_factor /= (VIS_REF * 10.0);
        ost << "result_vec[idx] = _mm256_mul_pd(_mm256_set1_pd("
            << scaling_factor << "),result_vec[idx]);\n";
        ost << "_mm256_stream_pd(viscosity,result_vec[idx]);\n";
        ost << "viscosity += 4;\n";
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
      {
        PairDelim for_pair(ost);
        double scaling_factor = sqrt(8.0);
        if (!unit->use_dim)
          scaling_factor /= (VIS_REF * 10.0);
        ost << "result_vec[idx] = _mm256_mul_pd(_mm256_set1_pd("
            << scaling_factor << "),result_vec[idx]);\n";
        ost << "_mm256_storeu_pd(viscosity,result_vec[idx]);\n";
        ost << "viscosity += 4;\n";
      }
    }
    // Advance the number of consumed species and the mass frac pointer
    ost << "remaining_elmts -= " << iter_values << ";\n";
    ost << MASS_FRAC_ARRAY << " += " << iter_values << ";\n";
  }
  // Now do the clean up loop
  // Assume that remaining elmts is divisible by 4
  ost << "if (remaining_elmts > 0)\n";
  {
    PairDelim if_pair(ost);
    ost << "assert((remaining_elmts % 4) == 0);\n";
    // Load the mole fractions and compute the spec values
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << VECTOR4 << " temp;\n";
      ost << "if (aligned<32>(" << TEMPERATURE_ARRAY << "))\n";
      ost << "  temp = _mm256_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << "else\n";
      ost << "  temp = _mm256_loadu_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 4;\n";
      unsigned spec_idx = 0;
      ost << "if (aligned<32>(" << MASS_FRAC_ARRAY << "))\n";
      {
        PairDelim if_pair(ost);
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_load_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
          spec_idx++;
        }
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_loadu_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
          spec_idx++;
        }
      }
      // Alos load the mixmw value
      ost << VECTOR4 << " mixmw;\n";
      ost << "if (aligned<32>(" << MIXMW_ARRAY << "))\n";
      ost << "  mixmw = _mm256_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << "else\n";
      ost << "  mixmw = _mm256_loadu_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 4;\n";
      // Initialize the output
      ost << "result_vec[idx] = _mm256_set1_pd(0.0);\n";
      if (!unit->use_dim)
        ost << "temp = _mm256_mul_pd(temp,_mm256_set1_pd(" << T_REF << "));\n";
      ost << VECTOR2 << " lower = _mm256_extractf128_pd(temp,0);\n";
      ost << VECTOR2 << " upper = _mm256_extractf128_pd(temp,1);\n";
      ost << VECTOR4 << " logt = _mm256_set_pd("
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
          << "log(_mm_cvtsd_f64(upper)),"
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
          << "log(_mm_cvtsd_f64(lower)));\n";
      // Use the temperature to compute the spec viscosities
      spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        assert((*it)->molecular_mass == nwt[spec_idx]); 
        ost << "// Species " << (*it)->name << "\n";
        PairDelim spec_pair(ost);
        ost << VECTOR4 << " val = _mm256_set1_pd(" << neta[(spec_idx+1)*order-1] << ");\n";
        for (int i = (order-1); i > 0; i--)
        {
          ost << "val = _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd("
              << neta[(spec_idx)*order+i-1] << "));\n";
        }
        emit_avx_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
        ost << "spec_visc[" << (spec_idx*avx_values) << "+idx] = val;\n";
        spec_idx++;
      }
      // Finally compute the mole fractions from the mass fractions
      for (spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = "
            << "_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac["
            << (spec_idx*avx_values) << "+idx],_mm256_set1_pd(recip_molecular_masses["
            << spec_idx << "])),"
            << "_mm256_set1_pd(1e3)),mixmw);\n";
      }
    }
    // Now that we've got everything loaded in, do the computation
    if (!unit->unroll_loops)
    {
      ost << INT << " visc_idx = 0;\n";
      ost << "for (int spec_idx = 0; spec_idx < " << num_species << "; spec_idx++)\n";
      PairDelim outer_loop(ost);
      // Zero out our spec sum
      for (unsigned i = 0; i < avx_values; i++)
        ost << "spec_sum[" << i << "] = _mm256_set1_pd(0.0);\n";
      ost << "for (int idx = 0; idx < " << num_species << "; idx++)\n";
      {
        PairDelim inner_loop(ost);
        ost << "if (idx == spec_idx)\n";
        {
          PairDelim if_pair(ost);
          for (unsigned i = 0; i < avx_values; i++)
            ost << "spec_sum[" << i << "] = _mm256_add_pd(spec_sum[" << i << "],"
                << "_mm256_mul_pd(" << MOLE_FRAC << "[idx*" << avx_values << "+" << i << "],"
                << "_mm256_set1_pd(" << (4.0/sqrt(2.0)) << ")));\n";
        }
        ost << "else\n";
        {
          PairDelim else_pair(ost);
          ost << VECTOR4 << " c1 = _mm256_set1_pd(visc_constants[visc_idx++]);\n";
          ost << VECTOR4 << " c2 = _mm256_set1_pd(visc_constants[visc_idx++]);\n";
          ost << VECTOR4 << " numer;\n";
          for (unsigned i = 0; i < avx_values; i++)
          {
            ost << "numer = _mm256_div_pd(spec_visc[spec_idx*" << avx_values << "+" << i << "],"
                << "spec_visc[idx*" << avx_values << "+" << i << "]);\n";
            ost << "numer = _mm256_mul_pd(c1,numer);\n";
            ost << "numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));\n";
            ost << "spec_sum[" << i << "] = _mm256_add_pd(spec_sum[" << i << "],"
                << "_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*" << avx_values << "+" << i << "],"
                << "numer),numer),c2));\n";
          }
        }
      }
      for (unsigned i = 0; i < avx_values; i++)
      {
        ost << "result_vec[" << i << "] = _mm256_add_pd(result_vec[" << i << "],"
            << "_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*" << avx_values << "+" << i << "],"
            << "spec_visc[spec_idx*" << avx_values << "+" << i << "]),"
            << "spec_sum[" << i << "]));\n";
      }
    }
    else
    {
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
      PairDelim for_pair(ost);
      unsigned spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        ost << "// Species " << (*it)->name << "\n";
        PairDelim spec_pair(ost);
        ost << VECTOR4 << " spec_sum = _mm256_set1_pd(0.0);\n";
        for (int idx = 0; idx < num_species; idx++)
        {
          PairDelim inner_pair(ost);
          if (idx == int(spec_idx))
          {
            ost << "spec_sum = _mm256_add_pd(_mm256_mul_pd("
                << "mole_frac[" << (idx*avx_values) << "+idx],_mm256_set1_pd("
                << (4.0/sqrt(2.0)) << ")),spec_sum);\n";
          }
          else
          {
            ost << VECTOR4 << " numer = _mm256_div_pd(spec_visc["
                << (spec_idx*avx_values) << "+idx],spec_visc["
                << (idx*avx_values) << "+idx]);\n";
            // Now take the square root
            ost << "numer = _mm256_mul_pd(_mm256_set1_pd("
                << (sqrt(nwt[idx]/nwt[spec_idx])) << "),numer);\n";
            ost << "numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));\n";
            ost << "spec_sum = _mm256_add_pd(_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd("
                << "mole_frac[" << (idx*avx_values) << "+idx],numer),numer),"
                << "_mm256_set1_pd(" << (1.0/sqrt(1.0+(nwt[spec_idx]/nwt[idx])))
                << ")),spec_sum);\n";
          }
        }
        ost << "result_vec[idx] = _mm256_add_pd(_mm256_div_pd(_mm256_mul_pd("
            << "mole_frac[" << (spec_idx*avx_values) << "+idx],"
            << "spec_visc[" << (spec_idx*avx_values) << "+idx]),"
            << "spec_sum),result_vec[idx]);\n";
        spec_idx++;
      }
    }
    // Finally we get to do the output
    ost << "if (aligned<32>(viscosity))\n";
    {
      PairDelim if_pair(ost);
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
      {
        PairDelim for_pair(ost);
        double scaling_factor = sqrt(8.0);
        if (!unit->use_dim)
          scaling_factor /= (VIS_REF * 10.0);
        ost << "result_vec[idx] = _mm256_mul_pd(_mm256_set1_pd("
            << scaling_factor << "),result_vec[idx]);\n";
        ost << "_mm256_stream_pd(viscosity,result_vec[idx]);\n";
        ost << "viscosity += 4;\n";
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
      {
        PairDelim for_pair(ost);
        double scaling_factor = sqrt(8.0);
        if (!unit->use_dim)
          scaling_factor /= (VIS_REF * 10.0);
        ost << "result_vec[idx] = _mm256_mul_pd(_mm256_set1_pd("
            << scaling_factor << "),result_vec[idx]);\n";
        ost << "_mm256_storeu_pd(viscosity,result_vec[idx]);\n";
        ost << "viscosity += 4;\n";
      }
    }
  }
  ost << "free(mole_frac);\n";
  ost << "free(spec_visc);\n";
  ost << "free(result_vec);\n";
  if (!unit->unroll_loops)
    ost << "free(spec_sum);\n";
}

void TranLib::emit_avx_cpu_diffusion(CodeOutStream &ost)
{
  if (!unit->unroll_loops)
  {
    ost << "const " << REAL << " diff_constants[" << (order*num_species*(num_species-1)/2) << "] = {";
    bool first = true;
    for (int k = 0; k < num_species; k++)
    {
      for (int j = k+1; j < num_species; j++)
      {
        int offset = j*order + k*order*num_species;
        for (int i = (order-1); i >= 0; i--)
        {
          if (!first)
            ost << ", ";
          else
            first = false;
          ost << ndif[offset+i];
        }
      }
    }
    ost << "};\n\n";
  }
  emit_vector_diffusion_declaration(ost, false, true);
  ost << "\n";
  PairDelim func_pair(ost);
  // Diffusion needs 1+3*N values for each point
  // and has (num_species+4)*num_species constants to store
  const size_t total_values = unit->cache_size * (1 << 10) / sizeof(REAL_TYPE);
  // Subtract out the constant values
  // Ignore the constants for now
  //size_t iter_values = total_values - (order*(unit->ordered_species.size()*(unit->ordered_species.size()-1)));
  size_t iter_values = total_values;
  // Now divide that number by num_species for each of the values we need to store
  iter_values /= (1+3*unit->ordered_species.size());
  // Round down if we have too many
  while ((iter_values % 4) != 0)
    iter_values--;
  size_t avx_values = iter_values / 4;
  if (iter_values < 4)
  {
    fprintf(stderr,"Cannot block AVX kernel for cache size %ld for diffusion kernel\n",
            unit->cache_size);
    exit(1);
  }
  ost << VECTOR4 << " *mole_frac = (" << VECTOR4 << "*)memalign(32," << avx_values
      << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *clamped = (" << VECTOR4 << "*)memalign(32," << avx_values
      << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *logt = (" << VECTOR4 << "*)memalign(32," << avx_values
      << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *sumxod = (" << VECTOR4 << "*)memalign(32," << avx_values
      << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR4 << "));\n";
  // Emit the main loop
  ost << "size_t remaining_elmts = num_elmts;\n";
  ost << "while (remaining_elmts >= " << iter_values << ")\n";
  {
    PairDelim while_pair(ost);
    // Emit code to load mole fractions and the temperature
    ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << "if (aligned<32>(" << TEMPERATURE_ARRAY << "))\n";
      ost << "  logt[idx] = _mm256_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << "else\n";
      ost << "  logt[idx] = _mm256_loadu_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 4;\n";
      ost << "if (aligned<32>(" << MASS_FRAC_ARRAY << "))\n";
      {
        PairDelim if_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_load_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
        }
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
            ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_loadu_pd("
                << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
        }
      }
      ost << VECTOR4 << " mixmw;\n";
      ost << "if (aligned<32>(" << MIXMW_ARRAY << "))\n";
      ost << "  mixmw = _mm256_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << "else\n";
      ost << "  mixmw = _mm256_loadu_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 4;\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "sumxod[" << (spec_idx*avx_values) << "+idx] = _mm256_set1_pd(0.0);\n";
      }
      // Compute the log of the temperature
      if (!unit->use_dim)
        ost << "logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(" << T_REF << "));\n";
      ost << VECTOR2 << " lower = _mm256_extractf128_pd(logt[idx],0);\n";
      ost << VECTOR2 << " upper = _mm256_extractf128_pd(logt[idx],1);\n";
      ost << "logt[idx] = _mm256_set_pd("
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
          << "log(_mm_cvtsd_f64(upper)),"
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
          << "log(_mm_cvtsd_f64(lower)));\n"; 
      // Compute the mole fractions
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        PairDelim spec_pair(ost);
        ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = "
            << "_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac["
            << (spec_idx*avx_values) << "+idx],_mm256_set1_pd(recip_molecular_masses["
            << spec_idx << "])),_mm256_set1_pd(1e3)),mixmw);\n";
        // Now compute the clamped values
        ost << VECTOR4 << " mask = _mm256_cmp_pd(mole_frac["
            << (spec_idx*avx_values) << "+idx],_mm256_set1_pd(" << SMALL << "),"
            << "_CMP_LT_OQ);\n";
        ost << "clamped[" << (spec_idx*avx_values) << "+idx] = "
            << "_mm256_and_pd(mask,_mm256_set1_pd(" << SMALL << "));\n";
        ost << "clamped[" << (spec_idx*avx_values) << "+idx] = "
            << "_mm256_add_pd(clamped[" << (spec_idx*avx_values)
            << "+idx],_mm256_andnot_pd(mask,mole_frac["
            << (spec_idx*avx_values) << "+idx]));\n";
      }
    }
    // Now do the actual computation
    if (!unit->unroll_loops)
    {
      assert(is_diffusion_symmetric());
      ost << INT << " diff_off = 0;\n";
      ost << "for (int k = 0; k < " << num_species << "; k++)\n";
      PairDelim outer_pair(ost);
      ost << "for (int j = k+1; j < " << num_species << "; j++)\n";
      {
        PairDelim inner_pair(ost);
        for (int i = 0; i < order; i++)
          ost << VECTOR4 << " c" << i << " = _mm256_set1_pd(diff_constants[diff_off+" << i << "]);\n";
        ost << "diff_off += " << order << ";\n";
        ost << VECTOR4 << " val;\n";
        for (unsigned i = 0; i < avx_values; i++)
        {
          ost << "val = c0;\n";
          for (int j = 1; j < order; j++)
            ost << "val = _mm256_add_pd(_mm256_mul_pd(val,logt[" << i << "]),c" << j << ");\n";
          ost << "val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));\n";
          emit_avx_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
          ost << "sumxod[k*" << avx_values << "+" << i << "] = _mm256_add_pd(_mm256_mul_pd("
              << "clamped[j*" << avx_values << "+" << i << "],val),"
              << "sumxod[k*" << avx_values << "+" << i << "]);\n";
          ost << "sumxod[j*" << avx_values << "+" << i << "] = _mm256_add_pd(_mm256_mul_pd("
              << "clamped[k*" << avx_values << "+" << i << "],val),"
              << "sumxod[j*" << avx_values << "+" << i << "]);\n";
        }
      }
    }
    else
    {
      ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
      PairDelim for_pair(ost);
      // Just going to assume this for now
      assert(is_diffusion_symmetric());
      for (int k = 0; k < num_species; k++)
      {
        for (int j = k+1; j < num_species; j++)
        {
          ost << "// D_" << j << "_" << k << " and D_" << k << "_" << j << "\n";
          PairDelim d_pair(ost);
          int offset = j*order + k*order*num_species;
          ost << VECTOR4 << " val = _mm256_set1_pd(" << ndif[offset+order-1] << ");\n";
          for (int i = (order-1); i > 0; i--)
            ost << "val = _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd("
                << ndif[offset+i-1] << "));\n";
          // Do fast negation with -0.0
          ost << "val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));\n";
          emit_avx_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
          ost << "sumxod[" << (k*avx_values) << "+idx] = _mm256_add_pd(_mm256_mul_pd("
              << "clamped[" << (j*avx_values) << "+idx],val),"
              << "sumxod[" << (k*avx_values) << "+idx]);\n";
          ost << "sumxod[" << (j*avx_values) << "+idx] = _mm256_add_pd(_mm256_mul_pd("
              << "clamped[" << (k*avx_values) << "+idx],val),"
              << "sumxod[" << (j*avx_values) << "+idx]);\n";
        }
      }
    }
    // Findally we get to do the output
    ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << VECTOR4 << " sumxw = _mm256_set1_pd(0.0);\n";
      ost << VECTOR4 << " wtm = _mm256_set1_pd(0.0);\n";
      ost << VECTOR4 << " " << PRESSURE << ";\n";
      ost << "if (aligned<32>(" << PRESSURE_ARRAY << "))\n";
      ost << "  " << PRESSURE << " = _mm256_load_pd("
          << PRESSURE_ARRAY << ");\n";
      ost << "else\n";
      ost << "  " << PRESSURE << " = _mm256_loadu_pd("
          << PRESSURE_ARRAY << ");\n";
      ost << PRESSURE_ARRAY << " += 4;\n";
      unsigned spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        // Make sure the molecular masses are the same otherwise we're doing something wrong
        assert((*it)->molecular_mass == nwt[spec_idx]);
        ost << "sumxw = _mm256_add_pd(_mm256_mul_pd(clamped["
            << (spec_idx*avx_values) << "+idx],_mm256_set1_pd("
            << nwt[spec_idx] << ")),sumxw);\n";
        ost << "wtm = _mm256_add_pd(_mm256_mul_pd(mole_frac["
            << (spec_idx*avx_values) << "+idx],_mm256_set1_pd("
            << nwt[spec_idx] << ")),wtm);\n";
        spec_idx++;
      }
      if (!unit->use_dim)
        ost << PRESSURE << " = _mm256_mul_pd(" << PRESSURE << ",_mm256_set1_pd("
            << P_REF << "));\n";
      ost << VECTOR4 << " pfac = _mm256_div_pd(_mm256_set1_pd(" << patmos << "),"
          << PRESSURE << ");\n";
      spec_idx = 0;
      ost << "if (aligned<32>(diffusion))\n";
      {
        PairDelim if_pair(ost);
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          PairDelim spec_pair(ost);
          ost << VECTOR4 << " result = _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,"
              << "_mm256_mul_pd(_mm256_set1_pd(" << nwt[spec_idx] << "),clamped["
              << (spec_idx*avx_values) << "+idx])));\n";
          ost << "result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod["
              << (spec_idx*avx_values) << "+idx]));\n";
          if (!unit->use_dim)
            ost << "result = _mm256_mul_pd(result,_mm256_set1_pd("
                << (1.0/(DIF_REF * 1e4)) << "));\n";
          // Write the result out
          ost << "_mm256_stream_pd(diffusion+(" << spec_idx << "*spec_stride)+(idx<<2),"
              << "result);\n";
          spec_idx++;
        }
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          PairDelim spec_pair(ost);
          ost << VECTOR4 << " result = _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,"
              << "_mm256_mul_pd(_mm256_set1_pd(" << nwt[spec_idx] << "),clamped["
              << (spec_idx*avx_values) << "+idx])));\n";
          ost << "result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod["
              << (spec_idx*avx_values) << "+idx]));\n";
          if (!unit->use_dim)
            ost << "result = _mm256_mul_pd(result,_mm256_set1_pd("
                << (1.0/(DIF_REF * 1e4)) << "));\n";
          // Write the result out
          ost << "_mm256_storeu_pd(diffusion+(" << spec_idx << "*spec_stride)+(idx<<2),"
              << "result);\n";
          spec_idx++;
        }
      }
    }
    // Advance the elements and pointers
    ost << "remaining_elmts -= " << iter_values << ";\n";
    ost << MASS_FRAC_ARRAY << " += " << iter_values << ";\n";
    ost << "diffusion += " << iter_values << ";\n";
  }
  // Now handle any leftover items
  ost << "if (remaining_elmts > 0)\n";
  {
    PairDelim if_pair(ost);
    ost << "assert((remaining_elmts % 4) == 0);\n";
    // Emit code to load mole fractions and the temperature
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << "if (aligned<32>(" << TEMPERATURE_ARRAY << "))\n";
      ost << "  logt[idx] = _mm256_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << "else\n";
      ost << "  logt[idx] = _mm256_loadu_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 4;\n";
      ost << "if (aligned<32>(" << MASS_FRAC_ARRAY << "))\n";
      {
        PairDelim if_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_load_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
        }
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_loadu_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
        }
      }
      ost << VECTOR4 << " mixmw;\n";
      ost << "if (aligned<32>(" << MIXMW_ARRAY << "))\n";
      ost << "  mixmw = _mm256_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << "else\n";
      ost << "  mixmw = _mm256_loadu_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 4;\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "sumxod[" << (spec_idx*avx_values) << "+idx] = _mm256_set1_pd(0.0);\n";
      }
      // Compute the log of the temperature
      if (!unit->use_dim)
        ost << "logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(" << T_REF << "));\n";
      ost << VECTOR2 << " lower = _mm256_extractf128_pd(logt[idx],0);\n";
      ost << VECTOR2 << " upper = _mm256_extractf128_pd(logt[idx],1);\n";
      ost << "logt[idx] = _mm256_set_pd("
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),"
          << "log(_mm_cvtsd_f64(upper)),"
          << "log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),"
          << "log(_mm_cvtsd_f64(lower)));\n"; 
      // Compute the mole fractions
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        PairDelim spec_pair(ost);
        ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = "
            << "_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac["
            << (spec_idx*avx_values) << "+idx],_mm256_set1_pd(recip_molecular_masses["
            << spec_idx << "])),_mm256_set1_pd(1e3)),mixmw);\n";
        // Now compute the clamped values
        ost << VECTOR4 << " mask = _mm256_cmp_pd(mole_frac["
            << (spec_idx*avx_values) << "+idx],_mm256_set1_pd(" << SMALL << "),"
            << "_CMP_LT_OQ);\n";
        ost << "clamped[" << (spec_idx*avx_values) << "+idx] = "
            << "_mm256_and_pd(mask,_mm256_set1_pd(" << SMALL << "));\n";
        ost << "clamped[" << (spec_idx*avx_values) << "+idx] = "
            << "_mm256_add_pd(clamped[" << (spec_idx*avx_values)
            << "+idx],_mm256_andnot_pd(mask,mole_frac["
            << (spec_idx*avx_values) << "+idx]));\n";
      }
    }
    // Now do the actual computation
    if (!unit->unroll_loops)
    {
      assert(is_diffusion_symmetric());
      ost << INT << " diff_off = 0;\n";
      ost << "for (int k = 0; k < " << num_species << "; k++)\n";
      PairDelim outer_pair(ost);
      ost << "for (int j = k+1; j < " << num_species << "; j++)\n";
      {
        PairDelim inner_pair(ost);
        for (int i = 0; i < order; i++)
          ost << VECTOR4 << " c" << i << " = _mm256_set1_pd(diff_constants[diff_off+" << i << "]);\n";
        ost << "diff_off += " << order << ";\n";
        ost << VECTOR4 << " val;\n";
        for (unsigned i = 0; i < avx_values; i++)
        {
          ost << "val = c0;\n";
          for (int j = 1; j < order; j++)
            ost << "val = _mm256_add_pd(_mm256_mul_pd(val,logt[" << i << "]),c" << j << ");\n";
          ost << "val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));\n";
          emit_avx_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
          ost << "sumxod[k*" << avx_values << "+" << i << "] = _mm256_add_pd(_mm256_mul_pd("
              << "clamped[j*" << avx_values << "+" << i << "],val),"
              << "sumxod[k*" << avx_values << "+" << i << "]);\n";
          ost << "sumxod[j*" << avx_values << "+" << i << "] = _mm256_add_pd(_mm256_mul_pd("
              << "clamped[k*" << avx_values << "+" << i << "],val),"
              << "sumxod[j*" << avx_values << "+" << i << "]);\n";
        }
      }
    }
    else
    {
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
      PairDelim for_pair(ost);
      // Just going to assume this for now
      assert(is_diffusion_symmetric());
      for (int k = 0; k < num_species; k++)
      {
        for (int j = k+1; j < num_species; j++)
        {
          ost << "// D_" << j << "_" << k << " and D_" << k << "_" << j << "\n";
          PairDelim d_pair(ost);
          int offset = j*order + k*order*num_species;
          ost << VECTOR4 << " val = _mm256_set1_pd(" << ndif[offset+order-1] << ");\n";
          for (int i = (order-1); i > 0; i--)
            ost << "val = _mm256_add_pd(_mm256_mul_pd(val,logt[idx]),_mm256_set1_pd("
                << ndif[offset+i-1] << "));\n";
          // Do fast negation with -0.0
          ost << "val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));\n";
          emit_avx_exp_taylor_series_expansion(ost, "val", unit->taylor_stages);
          ost << "sumxod[" << (k*avx_values) << "+idx] = _mm256_add_pd(_mm256_mul_pd("
              << "clamped[" << (j*avx_values) << "+idx],val),"
              << "sumxod[" << (k*avx_values) << "+idx]);\n";
          ost << "sumxod[" << (j*avx_values) << "+idx] = _mm256_add_pd(_mm256_mul_pd("
              << "clamped[" << (k*avx_values) << "+idx],val),"
              << "sumxod[" << (j*avx_values) << "+idx]);\n";
        }
      }
    }
    // Findally we get to do the output
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << VECTOR4 << " sumxw = _mm256_set1_pd(0.0);\n";
      ost << VECTOR4 << " wtm = _mm256_set1_pd(0.0);\n";
      ost << VECTOR4 << " " << PRESSURE << ";\n";
      ost << "if (aligned<32>(" << PRESSURE_ARRAY << "))\n";
      ost << "  " << PRESSURE << " = _mm256_load_pd("
          << PRESSURE_ARRAY << ");\n";
      ost << "else\n";
      ost << "  " << PRESSURE << " = _mm256_loadu_pd("
          << PRESSURE_ARRAY << ");\n";
      ost << PRESSURE_ARRAY << " += 4;\n";
      unsigned spec_idx = 0;
      for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
            it != unit->ordered_species.end(); it++)
      {
        // Make sure the molecular masses are the same otherwise we're doing something wrong
        assert((*it)->molecular_mass == nwt[spec_idx]);
        ost << "sumxw = _mm256_add_pd(_mm256_mul_pd(clamped["
            << (spec_idx*avx_values) << "+idx],_mm256_set1_pd("
            << nwt[spec_idx] << ")),sumxw);\n";
        ost << "wtm = _mm256_add_pd(_mm256_mul_pd(mole_frac["
            << (spec_idx*avx_values) << "+idx],_mm256_set1_pd("
            << nwt[spec_idx] << ")),wtm);\n";
        spec_idx++;
      }
      if (!unit->use_dim)
        ost << PRESSURE << " = _mm256_mul_pd(" << PRESSURE << ",_mm256_set1_pd("
            << P_REF << "));\n";
      ost << VECTOR4 << " pfac = _mm256_div_pd(_mm256_set1_pd(" << patmos << "),"
          << PRESSURE << ");\n";
      spec_idx = 0;
      ost << "if (aligned<32>(diffusion))\n";
      {
        PairDelim if_pair(ost);
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          PairDelim spec_pair(ost);
          ost << VECTOR4 << " result = _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,"
              << "_mm256_mul_pd(_mm256_set1_pd(" << nwt[spec_idx] << "),clamped["
              << (spec_idx*avx_values) << "+idx])));\n";
          ost << "result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod["
              << (spec_idx*avx_values) << "+idx]));\n";
          if (!unit->use_dim)
            ost << "result = _mm256_mul_pd(result,_mm256_set1_pd("
                << (1.0/(DIF_REF * 1e4)) << "));\n";
          // Write the result out
          ost << "_mm256_stream_pd(diffusion+(" << spec_idx << "*spec_stride)+(idx<<2),"
              << "result);\n";
          spec_idx++;
        }
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          PairDelim spec_pair(ost);
          ost << VECTOR4 << " result = _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,"
              << "_mm256_mul_pd(_mm256_set1_pd(" << nwt[spec_idx] << "),clamped["
              << (spec_idx*avx_values) << "+idx])));\n";
          ost << "result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod["
              << (spec_idx*avx_values) << "+idx]));\n";
          if (!unit->use_dim)
            ost << "result = _mm256_mul_pd(result,_mm256_set1_pd("
                << (1.0/(DIF_REF * 1e4)) << "));\n";
          // Write the result out
          ost << "_mm256_storeu_pd(diffusion+(" << spec_idx << "*spec_stride)+(idx<<2),"
              << "result);\n";
          spec_idx++;
        }
      }
    }
  }

  ost << "free(mole_frac);\n";
  ost << "free(clamped);\n";
  ost << "free(logt);\n";
  ost << "free(sumxod);\n";
}

void TranLib::emit_avx_cpu_thermal(CodeOutStream &ost)
{
  emit_vector_thermal_declaration(ost, false, true);
  ost << "\n";
  PairDelim func_pair(ost);
  // Thermal needs 1+N values for each point
  // and has (num_species+4)*num_species constants to store
  const size_t total_values = unit->cache_size * (1 << 10) / sizeof(REAL_TYPE);
  // Subtract out the constant values
  // Ignore the constants for now
  //size_t iter_values = total_values - (4*(unit->ordered_species.size()*nlite));
  // Now divide that number by num_species for each of the values we need to store
  size_t iter_values = total_values / (1+2*unit->ordered_species.size());
  // Round down if we have too many
  while ((iter_values % 4) != 0)
    iter_values--;
  size_t avx_values = iter_values / 4;
  if (iter_values < 4)
  {
    fprintf(stderr,"Cannot block AVX kernel for cache size %ld for thermal kernel\n",
            unit->cache_size);
    exit(1);
  }
  ost << VECTOR4 << " *mole_frac = (" << VECTOR4 << "*)memalign(32," << avx_values
      << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *temperature = (" << VECTOR4 << "*)memalign(32," << avx_values
      << "*sizeof(" << VECTOR4 << "));\n";
  ost << VECTOR4 << " *thermal_vec = (" << VECTOR4 << "*)memalign(32," << avx_values
      << "*" << unit->ordered_species.size() << "*sizeof(" << VECTOR4 << "));\n";
  // Emit the main loop
  ost << "size_t remaining_elmts = num_elmts;\n";
  ost << "while (remaining_elmts >= " << iter_values << ")\n";
  {
    PairDelim while_pair(ost);
    ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << "if (aligned<32>(" << TEMPERATURE_ARRAY << "))\n";
      ost << "  temperature[idx] = _mm256_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << "else\n";
      ost << "  temperature[idx] = _mm256_loadu_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 4;\n";
      ost << VECTOR4 << " mixmw;\n";
      ost << "if (aligned<32>(" << MIXMW_ARRAY << "))\n";
      ost << "  mixmw = _mm256_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << "else\n";
      ost << "  mixmw = _mm256_loadu_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 4;\n";
      ost << "if (aligned<32>(" << MASS_FRAC_ARRAY << "))\n";
      {
        PairDelim if_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_load_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
        }
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_loadu_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
        }
      }
      if (!unit->use_dim)
        ost << "temperature[idx] = _mm256_mul_pd(temperature[idx],_mm256_set1_pd("
            << T_REF << "));\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_mul_pd("
            << "_mm256_mul_pd(_mm256_mul_pd(mole_frac[" << (spec_idx*avx_values)
            << "+idx],_mm256_set1_pd(recip_molecular_masses[" << spec_idx
            << "])),_mm256_set1_pd(1e3)),mixmw);\n";
        ost << "thermal_vec[" << (spec_idx*avx_values) << "+idx] = _mm256_set1_pd(0.0);\n";
      }
    }
    // Now we've got all the values so do the computation
    ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
    {
      PairDelim for_pair(ost);
      for (int i = 0; i < nlite; i++)
      {
        // Subtract one because of fortran
        int light_idx = iktdif[i] - 1;
        int offset = i*order*num_species;
        // Find the species name
        int search_idx = 0;
        Species *light_spec = NULL;
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          if (strcmp((*it)->name,"M") == 0)
            continue;
          assert((*it)->molecular_mass == nwt[search_idx]);
          if (search_idx == light_idx)
          {
            light_spec = *it;
            break;
          }
          search_idx++;
        }
        assert(light_spec != NULL);
        assert(light_spec->molecular_mass == nwt[light_idx]);
        ost << "// Light species " << light_spec->name << "\n";
        PairDelim light_pair(ost);
        unsigned other_idx = 0;
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          if (light_idx == int(other_idx))
          {
            other_idx++;
            continue;
          }
          assert((*it)->molecular_mass == nwt[other_idx]);
          ost << "// Interaction with " << (*it)->name << "\n";
          PairDelim inter_pair(ost);
          ost << VECTOR4 << " val = _mm256_set1_pd(" 
              << ntdif[offset+(other_idx+1)*order-1] << ");\n";
          for (int j = (order-1); j > 0; j--)
          {
            ost << "val = _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),"
                << "_mm256_set1_pd(" << ntdif[offset+other_idx*order+j-1] << "));\n";
          }
          ost << "thermal_vec[" << (light_idx*avx_values) << "+idx] = _mm256_add_pd("
              << "_mm256_mul_pd(mole_frac[" << (light_idx*avx_values) << "+idx],"
              << "mole_frac[" << (other_idx*avx_values) << "+idx]),thermal_vec["
              << (light_idx*avx_values) << "+idx]);\n";
          other_idx++;
        }
      }
    }
    // Finally we can write out the values
    ost << "if (aligned<32>(thermal))\n";
    {
      PairDelim if_pair(ost);
      ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
      {
        PairDelim for_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "_mm256_stream_pd(thermal+(" << spec_idx << "*spec_stride)+(idx<<2),"
              << "thermal_vec[" << (spec_idx*avx_values) << "+idx]);\n";
        }
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "for (unsigned idx = 0; idx < " << avx_values << "; idx++)\n";
      {
        PairDelim for_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "_mm256_storeu_pd(thermal+(" << spec_idx << "*spec_stride)+(idx<<2),"
              << "thermal_vec[" << (spec_idx*avx_values) << "+idx]);\n";
        }
      }
    }
    // Advance the number of consumed species and the mass frac pointer
    ost << "remaining_elmts -= " << iter_values << ";\n";
    // Update the pointers for other arrays too
    ost << MASS_FRAC_ARRAY << " += " << iter_values << ";\n";
    ost << "thermal += " << iter_values << ";\n";
  }
  // Now do the loop to clean everything up
  ost << "if (remaining_elmts > 0)\n";
  {
    PairDelim if_pair(ost);
    ost << "assert((remaining_elmts % 4) == 0);\n";
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
    {
      PairDelim for_pair(ost);
      ost << "if (aligned<32>(" << TEMPERATURE_ARRAY << "))\n";
      ost << "  temperature[idx] = _mm256_load_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << "else\n";
      ost << "  temperature[idx] = _mm256_loadu_pd(" << TEMPERATURE_ARRAY << ");\n";
      ost << TEMPERATURE_ARRAY << " += 4;\n";
      ost << VECTOR4 << " mixmw;\n";
      ost << "if (aligned<32>(" << MIXMW_ARRAY << "))\n";
      ost << "  mixmw = _mm256_load_pd(" << MIXMW_ARRAY << ");\n";
      ost << "else\n";
      ost << "  mixmw = _mm256_loadu_pd(" << MIXMW_ARRAY << ");\n";
      ost << MIXMW_ARRAY << " += 4;\n";
      ost << "if (aligned<32>(" << MASS_FRAC_ARRAY << "))\n";
      {
        PairDelim if_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_load_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
        }
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_loadu_pd("
              << MASS_FRAC_ARRAY << "+(" << spec_idx << "*spec_stride)+(idx<<2));\n";
        }
      }
      if (!unit->use_dim)
        ost << "temperature[idx] = _mm256_mul_pd(temperature[idx],_mm256_set1_pd("
            << T_REF << "));\n";
      for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
      {
        ost << "mole_frac[" << (spec_idx*avx_values) << "+idx] = _mm256_mul_pd("
            << "_mm256_mul_pd(_mm256_mul_pd(mole_frac[" << (spec_idx*avx_values)
            << "+idx],_mm256_set1_pd(recip_molecular_masses[" << spec_idx
            << "])),_mm256_set1_pd(1e3)),mixmw);\n";
        ost << "thermal_vec[" << (spec_idx*avx_values) << "+idx] = _mm256_set1_pd(0.0);\n";
      }
    }
    // Now we've got all the values so do the computation
    ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
    {
      PairDelim for_pair(ost);
      for (int i = 0; i < nlite; i++)
      {
        // Subtract one because of fortran
        int light_idx = iktdif[i] - 1;
        int offset = i*order*num_species;
        // Find the species name
        int search_idx = 0;
        Species *light_spec = NULL;
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          if (strcmp((*it)->name,"M") == 0)
            continue;
          assert((*it)->molecular_mass == nwt[search_idx]);
          if (search_idx == light_idx)
          {
            light_spec = *it;
            break;
          }
          search_idx++;
        }
        assert(light_spec != NULL);
        assert(light_spec->molecular_mass == nwt[light_idx]);
        ost << "// Light species " << light_spec->name << "\n";
        PairDelim light_pair(ost);
        unsigned other_idx = 0;
        for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
              it != unit->ordered_species.end(); it++)
        {
          if (light_idx == int(other_idx))
          {
            other_idx++;
            continue;
          }
          assert((*it)->molecular_mass == nwt[other_idx]);
          ost << "// Interaction with " << (*it)->name << "\n";
          PairDelim inter_pair(ost);
          ost << VECTOR4 << " val = _mm256_set1_pd(" 
              << ntdif[offset+(other_idx+1)*order-1] << ");\n";
          for (int j = (order-1); j > 0; j--)
          {
            ost << "val = _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),"
                << "_mm256_set1_pd(" << ntdif[offset+other_idx*order+j-1] << "));\n";
          }
          ost << "thermal_vec[" << (light_idx*avx_values) << "+idx] = _mm256_add_pd("
              << "_mm256_mul_pd(mole_frac[" << (light_idx*avx_values) << "+idx],"
              << "mole_frac[" << (other_idx*avx_values) << "+idx]),thermal_vec["
              << (light_idx*avx_values) << "+idx]);\n";
          other_idx++;
        }
      }
    }
    // Finally we can write out the values
    ost << "if (aligned<32>(thermal))\n";
    {
      PairDelim if_pair(ost);
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
      {
        PairDelim for_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "_mm256_stream_pd(thermal+(" << spec_idx << "*spec_stride)+(idx<<2),"
              << "thermal_vec[" << (spec_idx*avx_values) << "+idx]);\n";
        }
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)\n";
      {
        PairDelim for_pair(ost);
        for (unsigned spec_idx = 0; spec_idx < unit->ordered_species.size(); spec_idx++)
        {
          ost << "_mm256_storeu_pd(thermal+(" << spec_idx << "*spec_stride)+(idx<<2),"
              << "thermal_vec[" << (spec_idx*avx_values) << "+idx]);\n";
        }
      }
    }
  }

  ost << "free(mole_frac);\n";
  ost << "free(temperature);\n";
  ost << "free(thermal_vec);\n";
}

// GPU Code Generation

void TranslationUnit::emit_gpu_physics_header_file(const char *file_name)
{
  CodeOutStream ost(file_name, true/*bound line length*/, 80);
  ost << "\n";
  ost << "#ifndef __GPU_GET_COEFFS__\n";
  ost << "#define __GPU_GET_COEFFS__\n";
  ost << "\n";

  emit_gpu_mole_masses(ost);
  ost << "\n";
  emit_gpu_recip_mole_masses(ost);
  ost << "\n";
  tran->emit_gpu_conductivity_declaration(ost);
  ost << ";\n";
  tran->emit_gpu_viscosity_declaration(ost);
  ost << ";\n";
  tran->emit_gpu_diffusion_declaration(ost);
  ost << ";\n";
  tran->emit_gpu_thermal_declaration(ost);
  ost << ";\n";

  ost << "\n";
  ost << "#endif // __GPU_GET_COEFFS__\n";
  ost << "\n";
}

void TranslationUnit::emit_gpu_physics_source_file(const char *file_name, const char *header_name)
{
  CodeOutStream ost(file_name, true/*bound line length*/, 80);

  ost << "\n";
  ost << "#include \"" << header_name << "\"\n";
  ost << "#include <cmath>\n";
  ost << "#include <cassert>\n";
  ost << "#include <cstdio>\n";
  ost << "\n";

  tran->emit_cuda_conductivity(ost);
  ost << "\n";
  tran->emit_cuda_viscosity(ost);
  ost << "\n";
  tran->emit_cuda_diffusion(ost);
  ost << "\n";
  tran->emit_cuda_thermal(ost);
  ost << "\n";
}

void TranLib::emit_gpu_conductivity_declaration(CodeOutStream &ost)
{
  ost << "__global__ void\n";
  ost << "gpu_conductivity(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
  ost << ", const " << REAL << " *" << MIXMW_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "conductivity" << ")";
}

void TranLib::emit_gpu_viscosity_declaration(CodeOutStream &ost)
{
  ost << "__global__ void\n"; 
  ost << "gpu_viscosity(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
  ost << ", const " << REAL << " *" << MIXMW_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "viscosity" << ")";
}

void TranLib::emit_gpu_diffusion_declaration(CodeOutStream &ost)
{
  ost << "__global__ void\n";
  ost << "gpu_diffusion(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << PRESSURE_ARRAY;
  ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
  ost << ", const " << REAL << " *" << MIXMW_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "diffusion" << ")";
}

void TranLib::emit_gpu_thermal_declaration(CodeOutStream &ost)
{
  ost << "__global__ void\n";
  ost << "gpu_thermal(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
  ost << ", const " << REAL << " *" << MIXMW_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "thermal_out" << ")";
}

void TranLib::emit_cuda_conductivity(CodeOutStream &ost)
{
  emit_gpu_conductivity_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.x*blockDim.x+threadIdx.x);\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << MIXMW_ARRAY << " += offset;\n";
    ost << "conductivity += offset;\n";
  }
  ost << "// Load the temperatures\n";
  ost << REAL << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,unit->k20);
  ost << "// Load the average mole weight\n";
  ost << REAL << " " << MIXMW << ";\n";
  emit_cuda_load(ost,MIXMW,MIXMW_ARRAY,unit->k20);
  // Scale the temperature if needed
  if (!unit->use_dim)
    ost << TEMPERATURE << " *= " << T_REF << ";\n";
  ost << "// Compute log(t)\n";
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  // Invert the average mole weight
  ost << REAL << " sum = 0.0;\n";
  ost << REAL << " sumr = 0.0;\n";
  unsigned spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++,spec_idx++)
  {
    ost << "// Species " << (*it)->name << "\n";
    PairDelim spec_pair(ost);
    // Load the mass fraction
    ost << REAL << " " << MASS_FRAC << ";\n";
    char src_offset[128];
    sprintf(src_offset,"%d*spec_stride",spec_idx);
    emit_cuda_load(ost,MASS_FRAC,MASS_FRAC_ARRAY,src_offset,unit->k20);
    // Convert to mole fraction
    ost << REAL << " " << MOLE_FRAC << " = " << MASS_FRAC << " * recip_molecular_masses[" << spec_idx << "] * 1e3 * " << MIXMW << ";\n";
    // Now do the math  
    for (int i = (order-1); i > 0; i--)
    {
      if (i == (order-1))
        ost << REAL << " val = __fma_rn(" << nlam[(spec_idx)*order+i] << ",logt," << nlam[(spec_idx)*order+i-1] << ");\n";
      else
        ost << "val = __fma_rn(val,logt," << nlam[(spec_idx)*order+i-1] << ");\n";
    }
    ost << "val = exp(val);\n";
    ost << "sum = __fma_rn(" << MOLE_FRAC << ",val,sum);\n";
    ost << "sumr = __fma_rn(" << MOLE_FRAC << ",1.0/val,sumr);\n";
  }
  ost << "// Write out the coefficients\n";
  ost << REAL << " result = 0.5 * (sum + (1.0/sumr));\n";
  if (!unit->use_dim)
  {
    double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
    lambda_ref *= 1e5;
    ost << "result *= " << (1.0/lambda_ref) << ";\n";
  }
  emit_cuda_store(ost,"conductivity","result");
}

void TranLib::emit_cuda_viscosity(CodeOutStream &ost)
{ 
  if (unit->viscosity_warps > 0)
  {
    emit_cuda_warp_specialized_viscosity(ost);
    //emit_cuda_warp_specialized_naive_viscosity(ost);
    return;
  }
  unsigned spec_idx = 0;
  bool first = true;
  if (!unit->unroll_loops)
  {
    ost << "__device__ const " << REAL 
        << " visc_constants[" << (2*num_species*(num_species-1)) << "] = {";
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      for (int idx = 0; idx < num_species; idx++)
      {
        if (idx == int(spec_idx))
          continue;
        if (!first)
          ost << ",";
        else
          first = false;
        ost << (sqrt(nwt[idx]/nwt[spec_idx]));
        ost << ", " << (1.0/(sqrt(1.0+(nwt[spec_idx]/nwt[idx]))));
      }
      spec_idx++;
    }
    ost << "};\n\n";
  }
  emit_gpu_viscosity_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.x*blockDim.x+threadIdx.x);\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << MIXMW_ARRAY << " += offset;\n";
    ost << "viscosity += offset;\n";
  }
  ost << REAL << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,unit->k20);
  ost << REAL << " " << MIXMW << ";\n";
  emit_cuda_load(ost,MIXMW,MIXMW_ARRAY,unit->k20);
  ost << REAL << " " << MOLE_FRAC << "[" << unit->ordered_species.size() << "];\n";
  for (unsigned idx = 0; idx < unit->ordered_species.size(); idx++)
  {
    char dst_offset[128];
    sprintf(dst_offset,"%d",idx);
    char src_offset[128];
    sprintf(src_offset,"%d*spec_stride",idx);
    emit_cuda_load(ost,MOLE_FRAC,dst_offset,MASS_FRAC_ARRAY,src_offset,unit->k20);
  }
  // Convert mass fractions to mole fractions
  ost << "for (int i = 0; i < " << unit->ordered_species.size() << "; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << MOLE_FRAC << "[i] *= recip_molecular_masses[i] * 1e3 * " 
        << MIXMW << ";\n";
  }
  if (!unit->use_dim)
    ost << TEMPERATURE << " *= " << T_REF << ";\n";
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  ost << REAL << " spec_visc[" << num_species << "];\n";
  assert(num_species == int(unit->ordered_species.size()));
  spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Make sure the molecular masses are the same otherwise we're doing something wrong
    assert((*it)->molecular_mass == nwt[spec_idx]); 
    ost << "// Species " << (*it)->name << "\n";
    PairDelim spec_pair(ost);
    ost << REAL << " val = " << neta[(spec_idx+1)*order-1] << ";\n";
    for (int i = (order-1); i > 0; i--)
    {
      ost << "val = val * logt + " << neta[(spec_idx)*order+i-1] << ";\n";
    }
    //ost << "spec_visc[" << spec_idx << "] = exp(val);\n";
    ost << "spec_visc[" << spec_idx << "] = val;\n";
    spec_idx++;
  }

  ost << REAL << " result = 0.0;\n";
  if (!unit->unroll_loops)
  {
    ost << INT << " visc_idx = 0;\n";
    ost << "for (int spec_idx = 0; spec_idx < " << num_species << "; spec_idx++)\n";
    {
      PairDelim outer_loop(ost);
      ost << REAL << " spec_sum = 0.0;\n";
      ost << "#pragma unroll\n";
      ost << "for (int idx = 0; idx < " << num_species << "; idx++)\n";
      {
        PairDelim inner_loop(ost);
        ost << "if (idx == spec_idx)\n";
        {
          PairDelim if_pair(ost);
          ost << "spec_sum += (" << MOLE_FRAC << "[idx]*4.0*" << (1.0/sqrt(2.0)) << ");\n";
        }
        ost << "else\n";
        {
          PairDelim else_pair(ost);
          ost << REAL << " numer = 0.5 * (visc_constants[visc_idx++] + spec_visc[spec_idx] - "
              << "spec_visc[idx]);\n";
          ost << "numer = exp(numer);\n";
          ost << "numer += 1.0;\n";
          ost << "spec_sum += (" << MOLE_FRAC << "[idx]*numer*numer*visc_constants[visc_idx++]);\n";
        }
      }
      ost << "result += (" << MOLE_FRAC << "[spec_idx]*exp(spec_visc[spec_idx])/spec_sum);\n";
    }
  }
  else
  {
    spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      // Skip the third body species
      if (strcmp((*it)->name,"M") == 0)
        continue;
      ost << "// Species " << (*it)->name << "\n";
      PairDelim spec_pair(ost);
      ost << REAL " spec_sum = 0.0;\n";
      for (int idx = 0; idx < num_species; idx++)
      {
        PairDelim inner_pair(ost);
        if (idx == int(spec_idx))
        {
          ost << "spec_sum += (" << MOLE_FRAC << "[" << idx << "]*4.0/"
              << (sqrt(2.0)) << ");\n";
        }
        else
        {
#if 0
          ost << REAL << " numer = 1.0 + sqrt(spec_visc[" << spec_idx << "]/spec_visc[" << idx << "]"
              << "*" << (sqrt(nwt[idx]/nwt[spec_idx])) << ");\n";
          ost << "spec_sum += (" << MOLE_FRAC << "[" << idx << "]*numer*numer/"
              << (sqrt(1.0+(nwt[spec_idx]/nwt[idx]))) << ");\n";
#endif
          ost << REAL << " numer = 0.5 * (" << (sqrt(nwt[idx]/nwt[spec_idx])) << " + spec_visc[" 
              << spec_idx << "] - spec_visc[" << idx << "]);\n"; 
          ost << "numer = exp(numer);\n";
          ost << "numer += 1.0;\n";
          ost << "spec_sum += (" << MOLE_FRAC << "[" << idx << "]*numer*numer*"
              << (1.0/(sqrt(1.0+(nwt[spec_idx]/nwt[idx])))) << ");\n";
        }
      }
      //ost << "result += (" << MOLE_FRAC << "[" << spec_idx << "]*spec_visc[" << spec_idx << "]/spec_sum);\n";
      ost << "result += (" << MOLE_FRAC << "[" << spec_idx << "]*exp(spec_visc[" << spec_idx << "])/spec_sum);\n";
      spec_idx++;
    }
  }
  if (!unit->use_dim)
  {
    double scaling_factor = sqrt(8.0) / (VIS_REF * 10.0);
    ost << "result *= " << (scaling_factor) << ";\n";
  }
  else
  {
    double scaling_factor = sqrt(8.0);
    ost << "result *= " << (scaling_factor) << ";\n";
  }
  emit_cuda_store(ost,"viscosity","result");
}

void TranLib::emit_cuda_diffusion(CodeOutStream &ost)
{
  if (unit->diffusion_warps > 0)
  {
    emit_cuda_warp_specialized_diffusion(ost);
    return;
  }
  bool first = true;
  if (!unit->unroll_loops)
  {
    ost << "__device__ const " << REAL << " diff_constants[" << (order*num_species*(num_species-1)/2) << "] = {";
    for (int k = 0; k < num_species; k++)
    {
      for (int j = k+1; j < num_species; j++)
      {
        int offset = j*order + k*order*num_species;
        for (int i = (order-1); i >= 0; i--)
        {
          if (!first)
            ost << ", ";
          else
            first = false;
          ost << ndif[offset+i];
        }
      }
    }
    ost << "};\n\n";
  }
  emit_gpu_diffusion_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.x*blockDim.x+threadIdx.x);\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << MIXMW_ARRAY << " += offset;\n";
    ost << "diffusion += offset;\n";
  }
  ost << REAL << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,unit->k20);
  ost << REAL << " " << PRESSURE << ";\n";
  emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,unit->k20);
  ost << REAL << " " MIXMW << ";\n";
  emit_cuda_load(ost,MIXMW,MIXMW_ARRAY,unit->k20);
  ost << REAL << " " << MOLE_FRAC << "[" << unit->ordered_species.size() << "];\n";
  for (unsigned idx = 0; idx < unit->ordered_species.size(); idx++)
  {
    char src_offset[128];
    sprintf(src_offset,"%d*spec_stride",idx);
    char dst_offset[128];
    sprintf(dst_offset,"%d",idx);
    emit_cuda_load(ost,MOLE_FRAC,dst_offset,MASS_FRAC_ARRAY,src_offset,unit->k20);
  }
  // Convert from mass fractions to mole fractions
  ost << "for (int i = 0; i < " << unit->ordered_species.size() << "; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << MOLE_FRAC << "[i] *= recip_molecular_masses[i] * 1e3 * " 
        << MIXMW << ";\n";
  }
  if (!unit->use_dim)
  {
    ost << TEMPERATURE << " *= " << T_REF << ";\n";
    ost << PRESSURE << " *= " << P_REF << ";\n";
  }
  bool symmetric = is_diffusion_symmetric();
  // compute the clamped mole weights
  ost << REAL << " clamped[" << num_species << "];\n";
  ost << "for (int i = 0; i < " << num_species << "; i++)\n";
  ost << "  clamped[i] = ((" << MOLE_FRAC << "[i] > " << SMALL << ") ? " << MOLE_FRAC << "[i] : " << SMALL << ");\n";
  ost << REAL << " sumxod[" << num_species << "];\n";
  ost << "for (int i = 0; i < " << num_species << "; i++)\n";
  ost << "  sumxod[i] = 0.0;\n";
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  if (!unit->unroll_loops)
  {
    assert(symmetric);
    ost << INT << " diff_off = 0;\n";
    ost << "for (int k = 0; k < " << num_species << "; k++)\n";
    {
      PairDelim outer_pair(ost);
      ost << "for (int j = k+1; j < " << num_species << "; j++)\n";
      {
        PairDelim inner_pair(ost);
        ost << REAL << " val = diff_constants[diff_off];\n";
        for (int i = 1; i < order; i++)
          ost << "val = val * logt + diff_constants[diff_off+" << i << "];\n";
        ost << "val = exp(-val);\n";
        ost << "sumxod[k] += (clamped[j]*val);\n";
        ost << "sumxod[j] += (clamped[k]*val);\n";
        ost << "diff_off += " << order << ";\n";
      }
    }
  }
  else
  {
    if (symmetric)
    {
      for (int k = 0; k < num_species; k++)
      {
        for (int j = k+1; j < num_species; j++)
        {
          ost << "// D_" << j << "_" << k << " and D_" << k << "_" << j << "\n";
          PairDelim d_pair(ost);
          int offset = j*order + k*order*num_species;
          ost << REAL << " val = " << ndif[offset+order-1] << ";\n";
          for (int i = (order-1); i > 0; i--)
            ost << "val = val * logt + " << ndif[offset+i-1] << ";\n";
          ost << "val = exp(-val);\n";
          ost << "sumxod[" << k << "] += (clamped[" << j << "]*val);\n";
          ost << "sumxod[" << j << "] += (clamped[" << k << "]*val);\n";
        }
      }
    }
    else
    {
      for (int k = 0; k < num_species; k++)
      {
        for (int j = 0; j < num_species; j++)
        {
          // Zeros on the diagonal
          if (k == j)
            continue;
          ost << "// D_" << j << "_" << k << "\n";
          PairDelim d_pair(ost);
          int offset = j*order + k*order*num_species;
          ost << REAL << " val = " << ndif[offset+order-1] << ";\n";
          for (int i = (order-1); i > 0; i--)
            ost << "val = val * logt + " << ndif[offset+i-1] << ";\n";
          ost << "val = exp(-val);\n";
          ost << "sumxod[" << k << "] += (clamped[" << j << "]*val);\n";
        }
      }
    }
  }
  ost << REAL << " sumxw = 0.0;\n";
  ost << REAL << " wtm = 0.0;\n";
  unsigned spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Make sure the molecular masses are the same otherwise we're doing something wrong
    assert((*it)->molecular_mass == nwt[spec_idx]);
    ost << "sumxw += (clamped[" << spec_idx << "]*" << nwt[spec_idx] << ");\n";
    ost << "wtm += (" << MOLE_FRAC << "[" << spec_idx << "]*" << nwt[spec_idx] << ");\n";
    spec_idx++;
  }
  //if (unit->no_nondim)
    ost << REAL << " pfac = " << patmos << "/" << PRESSURE << ";\n";
  //else
  //  ost << REAL << " pfac = " << (patmos/PRESSURE_REF) << "/" << PRESSURE << ";\n";
  // Now compute the diffusion coefficients, and overwrite them in the sumxod
  spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue; 
    PairDelim spec_pair(ost);
    ost << REAL << " result = pfac * (sumxw - (" << nwt[spec_idx] << "*clamped[" << spec_idx << "]))"
        << " / (wtm * sumxod[" << spec_idx << "]);\n";
    if (!unit->use_dim)
      ost << "result *= " << (1.0/(DIF_REF * 1e4)) << ";\n";
    char dst_offset[128];
    sprintf(dst_offset,"%d*spec_stride",spec_idx);
    emit_cuda_store(ost,"diffusion",dst_offset,"result");
    spec_idx++;
  }
}

void TranLib::emit_cuda_thermal(CodeOutStream &ost)
{
  emit_gpu_thermal_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.x*blockDim.x+threadIdx.x);\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << MIXMW_ARRAY << " += offset;\n";
    ost << "thermal_out += offset;\n";
  }
  ost << "// Load the temperatures\n";
  ost << REAL << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,unit->k20);
  ost << "// Load the average mole weight\n";
  ost << REAL << " " << MIXMW << ";\n";
  emit_cuda_load(ost,MIXMW,MIXMW_ARRAY,unit->k20);
  ost << REAL << " thermal[" << nlite << "];\n";
  for (int idx = 0; idx < nlite; idx++)
  {
    ost << "thermal[" << idx << "] = 0.0;\n";
    ost << REAL << " " << MASS_FRAC << "_light_" << (iktdif[idx]-1) << ";\n";
    char dst[128];
    sprintf(dst,"%s_light_%d",MASS_FRAC,(iktdif[idx]-1));
    char src_offset[128];
    sprintf(src_offset,"%d*spec_stride",(iktdif[idx]-1));
    emit_cuda_load(ost,dst,MASS_FRAC_ARRAY,src_offset,unit->k20);
  }
  // Scale the temperature if needed
  if (!unit->use_dim)
    ost << TEMPERATURE << " *= " << T_REF << ";\n";
  // Convert to mole fractions 
  for (int idx = 0; idx < nlite; idx++)
  {
    ost << REAL << " " << MOLE_FRAC << "_light_" << (iktdif[idx]-1)
        << " = " << MASS_FRAC << "_light_" << (iktdif[idx]-1)
        << " * recip_molecular_masses[" << (iktdif[idx]-1) << "] * 1e3"
        << " * " << MIXMW << ";\n";
  }
  unsigned spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++,spec_idx++)
  {
    PairDelim spec_pair(ost);
    // First emit the load for this mole fraction if it is not a light
    // species, otherwise the mole fraction is just whichever light species
    // it is
    int spec_light_idx = -1;
    for (int i = 0; i < nlite; i++)
    {
      if (int(spec_idx) == (iktdif[i]-1))
      {
        spec_light_idx = iktdif[i]-1;
        assert(spec_light_idx >= 0);
        break;
      }
    }
    if (spec_light_idx == -1)
    {
      ost << REAL << " " << MASS_FRAC << ";\n";
      char src_offset[128];
      sprintf(src_offset,"%d*spec_stride",spec_idx);
      emit_cuda_load(ost,MASS_FRAC,MASS_FRAC_ARRAY,src_offset,unit->k20);
      // Convert to mole fraction
      ost << REAL << " " << MOLE_FRAC << " = " << MASS_FRAC << " * recip_molecular_masses[" << spec_idx << "]"
          << " * 1e3 * " << MIXMW << ";\n";
    }
    else
    {
      // Convert to mole fraction
      ost << REAL << " " << MOLE_FRAC
          << " = " << MOLE_FRAC << "_light_" << spec_light_idx << ";\n";
    }
    // Now compute the interactions for each of the lite species
    for (int i = 0; i < nlite; i++)
    {
      int light_idx = iktdif[i]-1; // subtract one because of fortran
      int offset = i*order*num_species;
      // don't need to do interactions with themselves
      if (spec_light_idx == light_idx)
      {
        ost << "// No interaction for species " << light_idx << " with itself\n";
        continue;
      }
      PairDelim light_pair(ost);
      for (int j = (order-1); j > 0; j--)
      {
        if (j == (order-1))
          ost << REAL << " val = __fma_rn(" << ntdif[offset+spec_idx*order+j] << ","
              << TEMPERATURE << "," << ntdif[offset+spec_idx*order+j-1] << ");\n";
        else
          ost << "val = __fma_rn(val," << TEMPERATURE << "," << ntdif[offset+spec_idx*order+j-1] << ");\n";
      }
      ost << "val = val * " << MOLE_FRAC << ";\n";
      ost << "thermal[" << i << "] = __fma_rn(" << MOLE_FRAC << "_light_" << light_idx << ",val,thermal[" << i << "]);\n";
    }
  }

  spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++,spec_idx++)
  {
    // Check to see if this is a light species
    int light_idx = -1;
    for (int i = 0; i < nlite; i++)
    {
      if (int(spec_idx) == (iktdif[i]-1))
      {
        light_idx = i;
        break;
      }
    }
    char dst_offset[128];
    sprintf(dst_offset,"%d*spec_stride",spec_idx);
    if (light_idx > -1)
    {
      char src[128];
      sprintf(src,"thermal[%d]",light_idx);
      emit_cuda_store(ost,"thermal_out",dst_offset,src);
    }
    else
    {
      emit_cuda_store(ost,"thermal_out",dst_offset,"0.0");
    }
  }
}

void TranLib::emit_cuda_warp_specialized_naive_viscosity(CodeOutStream &ost)
{
  assert(unit->viscosity_warps > 0);
  {
    int warps_per_cta = unit->viscosity_warps/(32/unit->warp_size);
    int shared_memory = unit->warp_size * 8/*bytes per element*/
                        * (unit->ordered_species.size())/*number of species*/ * 2/*number of arrays*/;
    // If not K20 need a little extra shared memory for bouncing constants
    if (!unit->k20)
      shared_memory += (warps_per_cta * 8);
    if (shared_memory > MAX_SHARED)
    {
      fprintf(stderr,"Too many species to fit in shared memory for sliced visocisty kernel: needed %d bytes\n", shared_memory);
      exit(1);
    }
    // Figure out how many CTAs we can get on an SM, assuming shared memory limited
    int ctas_per_sm = MAX_SHARED/shared_memory;
    int total_warps = ctas_per_sm * warps_per_cta;
    fprintf(stdout,"    Warp-Specialized Viscosity Kernel:\n");
    fprintf(stdout,"      Warps-per-CTA:         %d\n", warps_per_cta);
    fprintf(stdout,"      Shared Memory per CTA: %d\n", shared_memory);
    fprintf(stdout,"      CTAs-per-SM:           %d\n", ctas_per_sm);
    fprintf(stdout,"      Warps-per-SM:          %d\n", total_warps);
  }
  unsigned max_specs = 0;
  const unsigned warp_size = unit->warp_size;
  assert((warp_size == 8) || (warp_size == 16) || (warp_size == 32));
  const unsigned viscosity_warps = unit->viscosity_warps * (32/warp_size);
  {
    std::vector<std::map<Species*,unsigned/*idx*/> > species_cuts;
    species_cuts.resize(viscosity_warps);
    // Round robin distribute species.  All work is the same
    // so it doesn't matter
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      unsigned index = spec_idx % viscosity_warps;
      species_cuts[index].insert(std::pair<Species*,unsigned>(*it,spec_idx));
      spec_idx++;
    }
    max_specs = species_cuts[0].size();
  }
  const char *thread_mask;
  unsigned warp_shift;
  if (warp_size == 8)
  {
    thread_mask = "0x7";
    warp_shift = 3;
  }
  else if (warp_size == 16)
  {
    thread_mask = "0xf";
    warp_shift = 4;
  }
  else
  {
    thread_mask = "0x1f";
    warp_shift = 5;
  }
  const unsigned num_species = unit->ordered_species.size();
  unsigned warp_constant_stride = 0;
  // Emit the constants for this kernel
  {
    // Always put the neta values in constant cache since they should always fit easily
    ost << "__constant__ " << REAL << " viscosity_neta[" << num_species << "][" << order << "] = {";
    for (unsigned idx = 0; idx < num_species; idx++)
    {
      if (idx == 0)
        ost << " {";
      else
        ost << ", {";
      for (int i = order ; i > 0; i--)
      {
        if (i == order)
          ost << (neta[idx*order+i-1]);
        else
          ost << ", " << (neta[idx*order+i-1]);
      }
      ost << "} ";
    }
    ost << "};\n\n";
    // Figure out parameters of specifying constants
    bool has_remainder = ((num_species%viscosity_warps) != 0);
    unsigned num_columns = (num_species/viscosity_warps);
    unsigned max_points_per_thread = (num_columns + (has_remainder ? 1 : 0))*(num_species);
    warp_constant_stride = (max_points_per_thread);
    // Make everything fit nicely by padding with zeros
    // Also guarantees aligned loads for the threads
    while ((warp_constant_stride%warp_size) != 0)
      warp_constant_stride++;
    // Now do the viscosity mass ratios
    ost << "__device__ const " << REAL << " viscosity_mass_ratios[" << (viscosity_warps*warp_constant_stride) << "] = {";
    bool first = true;
    for (unsigned wid = 0; wid < viscosity_warps; wid++)
    {
      unsigned num_created = 0;
      for (unsigned col_idx = 0; col_idx < num_columns; col_idx++)
      {
        unsigned column = col_idx*viscosity_warps + wid;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (first)
          {
            first = false;
            if (row == column)
              ost << "0.0";
            else
              ost << (log(sqrt(nwt[row]/nwt[column])));
          }
          else
          {
            if (row == column)
              ost << ", " << "0.0";
            else
              ost << ", " << (log(sqrt(nwt[row]/nwt[column]))); 
          }
          num_created++;
        }
      }
      assert(!first);
      if (has_remainder)
      {
        unsigned column = (wid+(num_columns*viscosity_warps))%num_species;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (row == column)
            ost << ", " << "0.0";
          else
            ost << ", " << (log(sqrt(nwt[row]/nwt[column])));
          num_created++;
        }
      }
      assert(num_created == max_points_per_thread);
      // Pad everything else with zeros
      while (num_created < warp_constant_stride)
      {
        ost << ", 0.0";
        num_created++;
      }
    }
    ost << "};\n\n";
    // Finally do the viscosity factor prefixes
    first = true;
    ost << "__device__ const " << REAL << " viscosity_factor_prefix[" << (viscosity_warps*warp_constant_stride) << "] = {";
    for (unsigned wid = 0; wid < viscosity_warps; wid++)
    {
      unsigned num_created = 0;
      for (unsigned col_idx = 0; col_idx < num_columns; col_idx++)
      {
        unsigned column = col_idx*viscosity_warps + wid;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (first)
          {
            first = false;
            if (row == column)
              ost << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
            else
              ost << (1.0/sqrt(2.0));
          }
          else
          {
            if (row == column)
              ost << ", " << (1.0/sqrt(2.0));
            else
              ost << ", " << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
          }
          num_created++;
        }
      }
      assert(!first);
      if (has_remainder)
      {
        unsigned column = (wid+(num_columns*viscosity_warps))%num_species;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (row == column)
            ost << ", " << (1.0/sqrt(2.0));
          else
            ost << ", " << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
          num_created++;
        }
      }
      assert(num_created == max_points_per_thread);
      // Pad everything else with zeros
      while (num_created < warp_constant_stride)
      {
        ost << ", 0.0";
        num_created++;
      }
    }
    ost << "};\n\n";
  }
  emit_gpu_viscosity_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  ost << "const " << INT << " tid = threadIdx.x & " << thread_mask << ";\n";
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.x*"
        << warp_size << " + tid);\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << MIXMW_ARRAY << " += offset;\n";
    ost << "viscosity += offset;\n";
  }
  ost << "volatile __shared__ " << REAL << " spec_visc[" << num_species << "][" << warp_size << "];\n";
  // Mark these as volatile so the compiler doesn't do
  // something stupid like try to cache them in registers
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << num_species << "][" << warp_size << "];\n";
  if (!unit->k20)
    ost << "volatile __shared__ " << REAL << " mirror[" << viscosity_warps << "];\n";

  // Iterate over all the warps and emit code for each one separately
  for (unsigned warp_idx = 0; warp_idx < viscosity_warps; warp_idx++)
  {
    if (warp_idx == 0)
      ost << "if ((threadIdx.x >> " << warp_shift << ") == " << warp_idx << ")\n";
    else
      ost << "else if ((threadIdx.x >> " << warp_shift << ") == " << warp_idx << ")\n";
    PairDelim warp_pair(ost);
    // Emit loads for our constants
    const unsigned constants_per_thread = ((num_species)*max_specs+(warp_size-1))/warp_size;
    assert(warp_constant_stride == (warp_size*constants_per_thread));
    ost << REAL << " mass_ratios[" << constants_per_thread << "];\n";
    ost << REAL << " factor_prefix[" << constants_per_thread << "];\n";
    {
      PairDelim load_pair(ost);
      ost << "const " << INT << " offset = " << warp_idx << "*" << warp_constant_stride << "+tid;\n";
      for (unsigned idx = 0; idx < constants_per_thread; idx++)
      {
        char dst_offset[128];
        sprintf(dst_offset,"%d",idx);
        char src_offset[128];
        sprintf(src_offset,"offset+%d",(idx*warp_size));
        emit_cuda_load(ost,"mass_ratios",dst_offset,"viscosity_mass_ratios",src_offset,unit->k20);
        emit_cuda_load(ost,"factor_prefix",dst_offset,"viscosity_factor_prefix",src_offset,unit->k20);
      }
    }

    ost << REAL << " " << MOLE_FRAC << "_temp[" << max_specs << "];\n";
    ost << REAL << " " << TEMPERATURE << ";\n";
    ost << REAL << " " << MIXMW << ";\n";

    // emit the main loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim step_loop_pair(ost);

    emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, "0", unit->k20);
    emit_cuda_load(ost, MIXMW, MIXMW_ARRAY, "0", unit->k20);
    // Issue loads for our mole fractions
    {
      for (unsigned idx = 0; idx < max_specs; idx++)
      {
        char dst[128];
        sprintf(dst,"%s_temp",MOLE_FRAC);
        char dst_offset[128];
        sprintf(dst_offset,"%d",idx);
        char src_offset[128];
        sprintf(src_offset,"(%d+%d)*spec_stride",warp_idx,(idx*viscosity_warps));
        //if (!uniform && (idx == (max_specs-1)))
          //ost << "if ((" << warp_idx << "+" << (idx*viscosity_warps) << ") < " << num_species << ")\n";
        if ((warp_idx+idx*viscosity_warps) < num_species)
          emit_cuda_load(ost,dst,dst_offset,MASS_FRAC_ARRAY,src_offset,unit->k20,".cg");
      }
    }
    // Scale the temperature if necessary
    if (!unit->use_dim)
      ost << TEMPERATURE << " *= " << T_REF << ";\n";
    // Compute logt
    ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
    // Compute the species specific viscosities
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      //if (!uniform && (idx == (max_specs-1)))
      //  ost << "if ((" << warp_idx << "+" << (idx*viscosity_warps) << ") < " << num_species << ")\n";
      if ((warp_idx+idx*viscosity_warps) >= num_species)
        continue;
      PairDelim spec_pair(ost);
      // For right now we'll take replays on the reads from viscosity_neta here.  We'll come
      // back and fix it later
      // Do some fancy math to make sure the compiler reloads this value and doesn't hoist it
      // out of the loop to consume registers
      ost << "const " << INT << " neta_index = (" << warp_idx << "+" << (idx*viscosity_warps) << "+step*"
          << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2) << ";\n";
      ost << REAL << " val = viscosity_neta[neta_index][0];\n";
      for (int i = (order-1); i > 0; i--)
      {
        ost << "val = __fma_rn(val,logt,viscosity_neta[neta_index][" << (order-i) << "]);\n";
      }
      ost << "spec_visc[" << warp_idx << "+" << (idx*viscosity_warps) << "][tid] = val;\n";
    }
    // Compute mole fractions and write them into shared memory
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      if ((warp_idx + idx*viscosity_warps) >= num_species)
        continue;
      //if (!uniform && (idx == (max_specs-1)))
        //ost << "if ((" << warp_idx << "+" << (idx*viscosity_warps) << ") < " << num_species << ")\n";
      PairDelim spec_pair(ost);
      ost << MOLE_FRAC << "_temp[" << idx << "] *= recip_molecular_masses[" << warp_idx << "+" << (idx*viscosity_warps) << "] * 1e3 * " << MIXMW << ";\n";
      ost << MOLE_FRAC << "[" << warp_idx << "+" << (idx*viscosity_warps) << "][tid] = " << MOLE_FRAC << "_temp[" << idx << "];\n";
    }
    // Synchronize to say that everything important is in shared memory
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    // Now do the code to compute the output coefficient for each point
    ost << REAL << " result = 0.0;\n";
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      //if (!uniform && (idx == (max_specs-1)))
      //  ost << "if ((" << warp_idx << "+" << (idx*viscosity_warps) << ") < " << num_species << ")\n";
      if ((warp_idx+idx*viscosity_warps) >= num_species)
        continue;
      PairDelim idx_pair(ost);
      ost << REAL << " spec_sum = 0.0;\n";
      ost << "const " << INT << " point = " << warp_idx << " + " << (idx*viscosity_warps) << ";\n";
      ost << REAL << " local_visc = spec_visc[point][tid];\n";
      // Do all the species here including ourselves so we can avoid bank conflicts even though
      // it will cost us an extra step
      for (unsigned spec_idx = 0; spec_idx < num_species; spec_idx++)
      {
        PairDelim spec_pair(ost);
        ost << REAL << " visc_" << spec_idx << " = spec_visc[" << spec_idx << "][tid];\n";
        ost << REAL << " mole_" << spec_idx << " = " << MOLE_FRAC << "[" << spec_idx << "][tid];\n";
        const unsigned coeff_index = idx*num_species + spec_idx;
        const unsigned lane_index = coeff_index % warp_size;
        const unsigned thread_index = coeff_index / warp_size;
        if (unit->k20)
        {
          ost << INT << " hi_part, lo_part;\n";
          ost << "lo_part = __shfl(__double2loint(mass_ratios[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << "hi_part = __shfl(__double2hiint(mass_ratios[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << REAL << " numer = 0.5 * (__hiloint2double(hi_part,lo_part) + local_visc - visc_" << spec_idx << ");\n";
          ost << "lo_part = __shfl(__double2loint(factor_prefix[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << "hi_part = __shfl(__double2hiint(factor_prefix[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << "numer = exp(numer);\n";
          ost << "numer += 1.0;\n";
          ost << REAL << " factor = __hiloint2double(hi_part,lo_part)*numer*numer;\n";
        }
        else
        {
          // Otherwise we can bounce this off a shared memory mirror
          // using warp-synchronous programming
          ost << "if (tid == " << lane_index << ")\n";
          ost << "  mirror[" << warp_idx << "] = mass_ratios[" << thread_index << "];\n";
          ost << REAL << " local_mass_ratio = mirror[" << warp_idx << "];\n";
          ost << REAL << " numer = 0.5 * (local_mass_ratio + local_visc - visc_" << spec_idx << ");\n";
          ost << "if (tid == " << lane_index << ")\n";
          ost << "  mirror[" << warp_idx << "] = factor_prefix[" << thread_index << "];\n";
          ost << REAL << " local_factor_prefix = mirror[" << warp_idx << "];\n";
          ost << "numer = exp(numer);\n";
          ost << "numer += 1.0;\n";
          ost << REAL << " factor = local_factor_prefix*numer*numer;\n";
        }
        ost << "spec_sum = __fma_rn(factor,mole_" << spec_idx << ",spec_sum);\n";
      }
      // Update the contribution to result
      ost << REAL << " local_mole = " << MOLE_FRAC << "[" << warp_idx << "+" << (idx*viscosity_warps) << "][tid];\n";
      ost << "local_visc = exp(local_visc);\n";
      ost << REAL << " ratio = local_visc/spec_sum;\n";
      ost << "result = __fma_rn(local_mole,ratio,result);\n";
    }
    // Write our results into shared memory, then do the reduction and have warp 0 write out the results
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    // Must be true for how we are going to do this reduction
    assert(viscosity_warps <= num_species);
    ost << "spec_visc[" << warp_idx << "][tid] = result;\n";
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    if (warp_idx == 0)
    {
      PairDelim reduc_pair(ost);
      ost << REAL << " accum = 0.0;\n";
      ost << "#pragma unroll\n";
      ost << "for (int i = 0; i < " << viscosity_warps << "; i++)\n";
      {
        PairDelim loop_pair(ost);
        ost << "accum += spec_visc[i][tid];\n";
      }
      if (!unit->use_dim)
      {
        double scaling_factor = sqrt(8.0) / (VIS_REF * 10.0);
        ost << "accum *= " << (scaling_factor) << ";\n";
      }
      else
      {
        double scaling_factor = sqrt(8.0);
        ost << "accum *= " << (scaling_factor) << ";\n";
      }
      emit_cuda_store(ost,"viscosity","accum");
    }
    // Update the pointers, then synchronize before going around the loop again
    {
      PairDelim pointer_pair(ost);
      ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
      ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
      ost << MIXMW_ARRAY << " += slice_stride;\n";
      ost << "viscosity += slice_stride;\n";
    }
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  }
}

void TranLib::emit_cuda_warp_specialized_viscosity(CodeOutStream &ost)
{
  assert(unit->viscosity_warps > 0);
  {
    int warps_per_cta = unit->viscosity_warps/(32/unit->warp_size);
    int shared_memory = unit->warp_size * 8/*bytes per element*/
                        * (unit->ordered_species.size())/*number of species*/ * 2/*number of arrays*/;
    // If not K20 need a little extra shared memory for bouncing constants
    if (!unit->k20)
      shared_memory += (warps_per_cta * 8);
    if (shared_memory > MAX_SHARED)
    {
      fprintf(stderr,"Too many species to fit in shared memory for sliced visocisty kernel: needed %d bytes\n", shared_memory);
      exit(1);
    }
    // Figure out how many CTAs we can get on an SM, assuming shared memory limited
    int ctas_per_sm = MAX_SHARED/shared_memory;
    int total_warps = ctas_per_sm * warps_per_cta;
    fprintf(stdout,"    Warp-Specialized Viscosity Kernel:\n");
    fprintf(stdout,"      Warps-per-CTA:         %d\n", warps_per_cta);
    fprintf(stdout,"      Shared Memory per CTA: %d\n", shared_memory);
    fprintf(stdout,"      CTAs-per-SM:           %d\n", ctas_per_sm);
    fprintf(stdout,"      Warps-per-SM:          %d\n", total_warps);
  }
  unsigned max_specs = 0;
  bool uniform = false;
  const unsigned warp_size = unit->warp_size;
  assert((warp_size == 8) || (warp_size == 16) || (warp_size == 32));
  const unsigned viscosity_warps = unit->viscosity_warps * (32/warp_size);
  {
    std::vector<std::map<Species*,unsigned/*idx*/> > species_cuts;
    species_cuts.resize(viscosity_warps);
    // Round robin distribute species.  All work is the same
    // so it doesn't matter
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      unsigned index = spec_idx % viscosity_warps;
      species_cuts[index].insert(std::pair<Species*,unsigned>(*it,spec_idx));
      spec_idx++;
    }
    max_specs = species_cuts[0].size();
    uniform = (max_specs == (species_cuts.back().size()));
  }
  const char *thread_mask;
  unsigned warp_shift;
  if (warp_size == 8)
  {
    thread_mask = "0x7";
    warp_shift = 3;
  }
  else if (warp_size == 16)
  {
    thread_mask = "0xf";
    warp_shift = 4;
  }
  else
  {
    thread_mask = "0x1f";
    warp_shift = 5;
  }
  const unsigned num_species = unit->ordered_species.size();
  unsigned warp_constant_stride = 0;
  // Emit the constants for this kernel
  {
    // Always put the neta values in constant cache since they should always fit easily
    ost << "__constant__ " << REAL << " viscosity_neta[" << num_species << "][" << order << "] = {";
    for (unsigned idx = 0; idx < num_species; idx++)
    {
      if (idx == 0)
        ost << " {";
      else
        ost << ", {";
      for (int i = order ; i > 0; i--)
      {
        if (i == order)
          ost << (neta[idx*order+i-1]);
        else
          ost << ", " << (neta[idx*order+i-1]);
      }
      ost << "} ";
    }
    ost << "};\n\n";
    // Figure out parameters of specifying constants
    bool has_remainder = ((num_species%viscosity_warps) != 0);
    unsigned num_columns = (num_species/viscosity_warps);
    unsigned max_points_per_thread = (num_columns + (has_remainder ? 1 : 0))*(num_species);
    warp_constant_stride = (max_points_per_thread);
    // Make everything fit nicely by padding with zeros
    // Also guarantees aligned loads for the threads
    while ((warp_constant_stride%warp_size) != 0)
      warp_constant_stride++;
    // Now do the viscosity mass ratios
    ost << "__device__ const " << REAL << " viscosity_mass_ratios[" << (viscosity_warps*warp_constant_stride) << "] = {";
    bool first = true;
    for (unsigned wid = 0; wid < viscosity_warps; wid++)
    {
      unsigned num_created = 0;
      for (unsigned col_idx = 0; col_idx < num_columns; col_idx++)
      {
        unsigned column = col_idx*viscosity_warps + wid;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (first)
          {
            first = false;
            if (row == column)
              ost << "0.0";
            else
              ost << (log(sqrt(nwt[row]/nwt[column])));
          }
          else
          {
            if (row == column)
              ost << ", " << "0.0";
            else
              ost << ", " << (log(sqrt(nwt[row]/nwt[column]))); 
          }
          num_created++;
        }
      }
      assert(!first);
      if (has_remainder)
      {
        unsigned column = (wid+(num_columns*viscosity_warps))%num_species;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (row == column)
            ost << ", " << "0.0";
          else
            ost << ", " << (log(sqrt(nwt[row]/nwt[column])));
          num_created++;
        }
      }
      assert(num_created == max_points_per_thread);
      // Pad everything else with zeros
      while (num_created < warp_constant_stride)
      {
        ost << ", 0.0";
        num_created++;
      }
    }
    ost << "};\n\n";
    // Finally do the viscosity factor prefixes
    first = true;
    ost << "__device__ const " << REAL << " viscosity_factor_prefix[" << (viscosity_warps*warp_constant_stride) << "] = {";
    for (unsigned wid = 0; wid < viscosity_warps; wid++)
    {
      unsigned num_created = 0;
      for (unsigned col_idx = 0; col_idx < num_columns; col_idx++)
      {
        unsigned column = col_idx*viscosity_warps + wid;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (first)
          {
            first = false;
            if (row == column)
              ost << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
            else
              ost << (1.0/sqrt(2.0));
          }
          else
          {
            if (row == column)
              ost << ", " << (1.0/sqrt(2.0));
            else
              ost << ", " << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
          }
          num_created++;
        }
      }
      assert(!first);
      if (has_remainder)
      {
        unsigned column = (wid+(num_columns*viscosity_warps))%num_species;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (row == column)
            ost << ", " << (1.0/sqrt(2.0));
          else
            ost << ", " << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
          num_created++;
        }
      }
      assert(num_created == max_points_per_thread);
      // Pad everything else with zeros
      while (num_created < warp_constant_stride)
      {
        ost << ", 0.0";
        num_created++;
      }
    }
    ost << "};\n\n";
  }
  emit_gpu_viscosity_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  ost << "const " << INT << " tid = threadIdx.x & " << thread_mask << ";\n";
  ost << "const " << INT << " wid = threadIdx.x >> " << warp_shift << ";\n";
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.x*"
        << warp_size << " + tid);\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << MIXMW_ARRAY << " += offset;\n";
    ost << "viscosity += offset;\n";
  }
  ost << "volatile __shared__ " << REAL << " spec_visc[" << num_species << "][" << warp_size << "];\n";
  // Mark these as volatile so the compiler doesn't do
  // something stupid like try to cache them in registers
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << num_species << "][" << warp_size << "];\n";
  if (!unit->k20)
    ost << "volatile __shared__ " << REAL << " mirror[" << viscosity_warps << "];\n";

  // Emit loads for our constants
  const unsigned constants_per_thread = ((num_species)*max_specs+(warp_size-1))/warp_size;
  assert(warp_constant_stride == (warp_size*constants_per_thread));
  ost << REAL << " mass_ratios[" << constants_per_thread << "];\n";
  ost << REAL << " factor_prefix[" << constants_per_thread << "];\n";
  {
    PairDelim load_pair(ost);
    ost << "const " << INT << " offset = wid*" << warp_constant_stride << "+tid;\n";
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"offset+%d",(idx*warp_size));
      emit_cuda_load(ost,"mass_ratios",dst_offset,"viscosity_mass_ratios",src_offset,unit->k20);
      emit_cuda_load(ost,"factor_prefix",dst_offset,"viscosity_factor_prefix",src_offset,unit->k20);
    }
  }

  ost << REAL << " " << MOLE_FRAC << "_temp[" << max_specs << "];\n";
  ost << REAL << " " << TEMPERATURE << ";\n";
  ost << REAL << " " << MIXMW << ";\n";

  // emit the main loop
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim step_loop_pair(ost);

  emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, "0", unit->k20);
  emit_cuda_load(ost, MIXMW, MIXMW_ARRAY, "0", unit->k20);
  // Issue loads for our mole fractions
  {
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      char dst[128];
      sprintf(dst,"%s_temp",MOLE_FRAC);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride",(idx*viscosity_warps));
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*viscosity_warps) << ") < " << num_species << ")\n";
      emit_cuda_load(ost,dst,dst_offset,MASS_FRAC_ARRAY,src_offset,unit->k20,".cg");
    }
  }

  // Scale the temperature if necessary
  if (!unit->use_dim)
    ost << TEMPERATURE << " *= " << T_REF << ";\n";
  // Compute logt
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  // Compute the species specific viscosities
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*viscosity_warps) << ") < " << num_species << ")\n";
    PairDelim spec_pair(ost);
    // For right now we'll take replays on the reads from viscosity_neta here.  We'll come
    // back and fix it later
    // Do some fancy math to make sure the compiler reloads this value and doesn't hoist it
    // out of the loop to consume registers
    ost << "const " << INT << " neta_index = (wid+" << (idx*viscosity_warps) << "+step*"
        << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2) << ";\n";
    ost << REAL << " val = viscosity_neta[neta_index][0];\n";
    for (int i = (order-1); i > 0; i--)
    {
      ost << "val = __fma_rn(val,logt,viscosity_neta[neta_index][" << (order-i) << "]);\n";
    }
    ost << "spec_visc[wid+" << (idx*viscosity_warps) << "][tid] = val;\n";
  }
  // Compute mole fractions and write them into shared memory
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*viscosity_warps) << ") < " << num_species << ")\n";
    PairDelim spec_pair(ost);
    ost << MOLE_FRAC << "_temp[" << idx << "] *= recip_molecular_masses[wid+" << (idx*viscosity_warps) << "] * 1e3 * " << MIXMW << ";\n";
    ost << MOLE_FRAC << "[wid+" << (idx*viscosity_warps) << "][tid] = " << MOLE_FRAC << "_temp[" << idx << "];\n";
  }
  // Synchronize to say that everything important is in shared memory
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  // Now do the code to compute the output coefficient for each point
  ost << REAL << " result = 0.0;\n";
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*viscosity_warps) << ") < " << num_species << ")\n";
    PairDelim idx_pair(ost);
    ost << REAL << " spec_sum = 0.0;\n";
    ost << "const " << INT << " point = wid + " << (idx*viscosity_warps) << ";\n";
    ost << REAL << " local_visc = spec_visc[point][tid];\n";
    // Do all the species here including ourselves so we can avoid bank conflicts even though
    // it will cost us an extra step
    for (unsigned spec_idx = 0; spec_idx < num_species; spec_idx++)
    {
      PairDelim spec_pair(ost);
      ost << REAL << " visc_" << spec_idx << " = spec_visc[" << spec_idx << "][tid];\n";
      ost << REAL << " mole_" << spec_idx << " = " << MOLE_FRAC << "[" << spec_idx << "][tid];\n";
      const unsigned coeff_index = idx*num_species + spec_idx;
      const unsigned lane_index = coeff_index % warp_size;
      const unsigned thread_index = coeff_index / warp_size;
      if (unit->k20)
      {
        ost << INT << " hi_part, lo_part;\n";
        ost << "lo_part = __shfl(__double2loint(mass_ratios[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
        ost << "hi_part = __shfl(__double2hiint(mass_ratios[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
        ost << REAL << " numer = 0.5 * (__hiloint2double(hi_part,lo_part) + local_visc - visc_" << spec_idx << ");\n";
        ost << "lo_part = __shfl(__double2loint(factor_prefix[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
        ost << "hi_part = __shfl(__double2hiint(factor_prefix[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
        ost << "numer = exp(numer);\n";
        ost << "numer += 1.0;\n";
        ost << REAL << " factor = __hiloint2double(hi_part,lo_part)*numer*numer;\n";
      }
      else
      {
        // Otherwise we can bounce this off a shared memory mirror
        // using warp-synchronous programming
        ost << "if (tid == " << lane_index << ")\n";
        ost << "  mirror[wid] = mass_ratios[" << thread_index << "];\n";
        ost << REAL << " local_mass_ratio = mirror[wid];\n";
        ost << REAL << " numer = 0.5 * (local_mass_ratio + local_visc - visc_" << spec_idx << ");\n";
        ost << "if (tid == " << lane_index << ")\n";
        ost << "  mirror[wid] = factor_prefix[" << thread_index << "];\n";
        ost << REAL << " local_factor_prefix = mirror[wid];\n";
        ost << "numer = exp(numer);\n";
        ost << "numer += 1.0;\n";
        ost << REAL << " factor = local_factor_prefix*numer*numer;\n";
      }
      ost << "spec_sum = __fma_rn(factor,mole_" << spec_idx << ",spec_sum);\n";
    }
    // Update the contribution to result
    ost << REAL << " local_mole = " << MOLE_FRAC << "[wid+" << (idx*viscosity_warps) << "][tid];\n";
    ost << "local_visc = exp(local_visc);\n";
    ost << REAL << " ratio = local_visc/spec_sum;\n";
    ost << "result = __fma_rn(local_mole,ratio,result);\n";
  }
  // Write our results into shared memory, then do the reduction and have warp 0 write out the results
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  // Must be true for how we are going to do this reduction
  assert(viscosity_warps <= num_species);
  ost << "spec_visc[wid][tid] = result;\n";
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  {
    ost << "if (wid == 0)\n";
    PairDelim reduc_pair(ost);
    ost << REAL << " accum = 0.0;\n";
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < " << viscosity_warps << "; i++)\n";
    {
      PairDelim loop_pair(ost);
      ost << "accum += spec_visc[i][tid];\n";
    }
    if (!unit->use_dim)
    {
      double scaling_factor = sqrt(8.0) / (VIS_REF * 10.0);
      ost << "accum *= " << (scaling_factor) << ";\n";
    }
    else
    {
      double scaling_factor = sqrt(8.0);
      ost << "accum *= " << (scaling_factor) << ";\n";
    }
    emit_cuda_store(ost,"viscosity","accum");
  }
  // Update the pointers, then synchronize before going around the loop again
  {
    PairDelim pointer_pair(ost);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
    ost << MIXMW_ARRAY << " += slice_stride;\n";
    ost << "viscosity += slice_stride;\n";
  }
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
}

void TranLib::emit_cuda_warp_specialized_diffusion(CodeOutStream &ost)
{
  assert(unit->diffusion_warps > 0);
  {
    int warps_per_cta = unit->diffusion_warps;
    int shared_memory = unit->warp_size * 8/*bytes per element*/
                        * (unit->ordered_species.size())/*number of species*/ * 2/*number of arrays*/;
    // If not K20 need a little extra shared memory for bouncing constants
    if (!unit->k20)
      shared_memory += (warps_per_cta * 8);
    if (shared_memory > MAX_SHARED)
    {
      fprintf(stderr,"Too many species to fit in shared memory for sliced diffusion kernel: needed %d bytes\n", shared_memory);
      exit(1);
    }
    // Figure out how many CTAs we can get on an SM, assuming shared memory limited
    int ctas_per_sm = MAX_SHARED/shared_memory;
    int total_warps = ctas_per_sm * warps_per_cta;
    fprintf(stdout,"    Warp-Specialized Diffusion Kernel:\n");
    fprintf(stdout,"      Warps-per-CTA:         %d\n", warps_per_cta);
    fprintf(stdout,"      Shared Memory per CTA: %d\n", shared_memory);
    fprintf(stdout,"      CTAs-per-SM:           %d\n", ctas_per_sm);
    fprintf(stdout,"      Warps-per-SM:          %d\n", total_warps);
  }
  unsigned max_specs = 0;
  bool uniform = false;
  const unsigned warp_size = unit->warp_size;
  assert((warp_size == 8) || (warp_size == 16) || (warp_size == 32));
  const unsigned diffusion_warps = unit->diffusion_warps * (32/warp_size);
  const unsigned threads_per_full_warp = 32/warp_size;
  {
    std::vector<std::map<Species*,unsigned/*idx*/> > species_cuts;
    species_cuts.resize(diffusion_warps);
    // Round robin distribute species.  All work is the same
    // so it doesn't matter
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      unsigned index = spec_idx % diffusion_warps;
      species_cuts[index].insert(std::pair<Species*,unsigned>(*it,spec_idx));
      spec_idx++;
    }
    max_specs = species_cuts[0].size();
    uniform = (max_specs == (species_cuts.back().size()));
  }
  const char *thread_mask;
  const char *local_mask;
  unsigned warp_shift;
  if (warp_size == 8)
  {
    thread_mask = "0x7";
    warp_shift = 3;
    local_mask = "0x3"; 
  }
  else if (warp_size == 16)
  {
    thread_mask = "0xf";
    warp_shift = 4;
    local_mask = "0x1";
  }
  else
  {
    thread_mask = "0x1f";
    warp_shift = 5;
    local_mask = "0x0";
  }
  const unsigned num_species = unit->ordered_species.size();
  unsigned warp_constant_stride = 0;
  unsigned max_points_per_thread = 0;
  // Emit the constants for this kernel
  {
    // Diffusion masses should always fit in the constant cache
    ost << "__constant__ " << REAL << " diffusion_masses[" << num_species << "] = {";
    for (unsigned idx = 0; idx < num_species; idx++)
    {
      if (idx == 0)
        ost << nwt[idx];
      else
        ost << ", " << nwt[idx];
    }
    ost << "};\n\n";
  }
  {
    bool has_remainder = ((num_species%diffusion_warps) != 0);
    unsigned warp_columns = (num_species/(diffusion_warps/threads_per_full_warp));
    unsigned num_full_points = (num_species/2);
    // Compute the maximum number of points per thread
    {
      max_points_per_thread = 0;
      for (unsigned point_idx = 0; point_idx < ((num_species/2)+warp_columns-1); point_idx++)
      {
        const unsigned start_across = (point_idx < num_full_points ? 0 : point_idx-num_full_points+1);
        const unsigned stop_across = (point_idx < warp_columns ? point_idx+1 : warp_columns);
        unsigned loop_start_across = start_across;
        unsigned loop_stop_across = stop_across;
        while ((loop_start_across%threads_per_full_warp) != 0)
        {
          assert(loop_start_across > 0);
          loop_start_across--;
        }
        while ((loop_stop_across%threads_per_full_warp) != 0)
        {
          loop_stop_across++;
        }
        for (unsigned across_idx = loop_start_across; across_idx < loop_stop_across; across_idx+=threads_per_full_warp)
        {
          max_points_per_thread++;
        }
      }
      if (has_remainder)
      {
        for (unsigned point_idx = 0; point_idx < (num_species/2); point_idx++)
          max_points_per_thread++;
      }
    }
    warp_constant_stride = max_points_per_thread*order;
    while ((warp_constant_stride%warp_size) != 0)
      warp_constant_stride++;
    ost << "__device__ const " << REAL << " across_coeffs[" << (diffusion_warps*warp_constant_stride) << "] = {";
    bool first = true;
    for (unsigned wid = 0; wid < diffusion_warps; wid++)
    {
      unsigned thread_offset = wid%threads_per_full_warp;
      unsigned num_created = 0;
      for (unsigned point_idx = 0; point_idx < ((num_species/2)+warp_columns-1); point_idx++)
      {
        const unsigned start_across = (point_idx < num_full_points ? 0 : point_idx-num_full_points+1);
        const unsigned stop_across = (point_idx < warp_columns ? point_idx+1 : warp_columns);
        unsigned loop_start_across = start_across;
        unsigned loop_stop_across = stop_across;
        while ((loop_start_across%threads_per_full_warp) != 0)
        {
          assert(loop_start_across > 0);
          loop_start_across--;
        }
        while ((loop_stop_across%threads_per_full_warp) != 0)
        {
          loop_stop_across++;
        }
        for (unsigned across_idx = loop_start_across; across_idx < loop_stop_across; across_idx+=threads_per_full_warp)
        {
          assert((across_idx%threads_per_full_warp)==0);
          //const unsigned thread_across_idx = across_idx%threads_per_full_warp;
          unsigned x_loc = ((wid/threads_per_full_warp)*warp_columns+thread_offset+across_idx)%num_species;
          unsigned y_loc = ((wid/threads_per_full_warp)*warp_columns+1+point_idx)%num_species;
          assert((x_loc < num_species) && (y_loc < num_species));
          //ost << "/*(" << wid << "," << x_loc << "," << y_loc << ")*/";
          for (int k = order; k > 0; k--)
          {
            if (first)
              first = false;
            else
              ost << ", ";
            ost << ndif[(k-1)+y_loc*order+x_loc*order*num_species];
          }
          num_created++;
        }
      }
      assert(!first);
      if (has_remainder)
      {
        unsigned x_loc = (wid+(warp_columns*(diffusion_warps/threads_per_full_warp)))%num_species;
        for (unsigned point_idx = 0; point_idx < (num_species/2); point_idx++)
        {
          unsigned y_loc = (x_loc+1+point_idx)%num_species;
          for (int k = order; k > 0; k--)
          {
            ost << ", ";
            ost << ndif[(k-1)+y_loc*order+x_loc*order*num_species];
          }
          num_created++;
        }
      }
      assert(num_created == max_points_per_thread);
      // Otherwise, pad with zeros
      assert(num_created <= warp_constant_stride);
      for (int i = 0; i < (int(warp_constant_stride)-int(num_created*order)); i++)
        ost << ", 0.0";
    }
    ost << "};\n\n";
  }
  emit_gpu_diffusion_declaration(ost);
  ost << "\n";
  PairDelim func_pair(ost);
  ost << "const " << INT << " tid = threadIdx.x & " << thread_mask << ";\n";
  ost << "const " << INT << " wid = threadIdx.x >> " << warp_shift << ";\n";
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.x*"
        << warp_size << " + tid);\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << MIXMW_ARRAY << " += offset;\n";
    ost << "diffusion += offset;\n";
  }
  ost << "volatile __shared__ " << REAL << " sumxod[" << num_species << "][" << warp_size << "];\n";
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << num_species << "][" << warp_size << "];\n";
  if (!unit->k20)
    ost << "volatile __shared__ " << REAL << " mirror[" << diffusion_warps << "];\n";

  ost << REAL << " " << TEMPERATURE << ";\n";
  ost << REAL << " " << MIXMW << ";\n";
  ost << REAL << " " << MOLE_FRAC << "_temp[" << max_specs << "];\n";

  // Emit loads for our constants  
  assert(warp_constant_stride > 0);
  assert(max_points_per_thread > 0);
  const unsigned constants_per_thread = (max_points_per_thread*order+(warp_size-1))/warp_size; 
  assert(warp_constant_stride == (warp_size*constants_per_thread));
  ost << REAL << " thread_coeffs[" << (constants_per_thread) << "];\n";
  {
    PairDelim constant_pair(ost);
    ost << "const " << INT << " offset = wid*" << (warp_constant_stride) << "+tid;\n";
    // Issue loads for our constants
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"offset+%d",(idx*warp_size));
      emit_cuda_load(ost,"thread_coeffs",dst_offset,"across_coeffs",src_offset,false/*double2*/);
    }
  }
  // Emit the major loop
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim step_loop_pair(ost);
  // Issue the load for the temperature
  emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, unit->k20);
  emit_cuda_load(ost, MIXMW, MIXMW_ARRAY, unit->k20);
  // Emit the loads for the mole fractions
  {
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      char dst[128];
      sprintf(dst,"%s_temp",MOLE_FRAC);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride",(idx*diffusion_warps));
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*diffusion_warps) << ") < " << num_species << ")\n";
      emit_cuda_load(ost,dst,dst_offset,MASS_FRAC_ARRAY,src_offset,false,".cg");
    }
  }
  if (!unit->use_dim)
    ost << TEMPERATURE << " *= " << T_REF << ";\n";
  // Compute the log of the temperature
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  // Initialize all sumxod values to 0.0
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*diffusion_warps) << ") < " << num_species << ")\n";
    ost << "sumxod[wid+" << (idx*diffusion_warps) << "][tid] = 0.0;\n";
  }
  // Write our mole fractions into shared memory
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*diffusion_warps) << ") < " << num_species << ")\n";
    PairDelim spec_pair(ost);
    // Convert the mass fraction to a mole fraction
    ost << MOLE_FRAC << "_temp[" << idx << "] *= recip_molecular_masses[wid+" << (idx*diffusion_warps) << "] * 1e3 * " << MIXMW << ";\n";
    ost << MOLE_FRAC << "[wid+" << (idx*diffusion_warps) << "][tid] = " << MOLE_FRAC << "_temp[" << idx << "];\n";
  }
  // Compute our local values of sumxw and wtm
  ost << REAL << " local_sumxw = 0.0;\n";
  ost << REAL << " local_wtm = 0.0;\n";
  // Barrier to say everything is written into shared memory
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  {
    // Figure out what we're doing for this across instance
    const bool needs_horizontal_check = ((num_species%2) == 0);
    assert((diffusion_warps%threads_per_full_warp) == 0);
    const unsigned warp_columns = num_species/(diffusion_warps/threads_per_full_warp);
    const unsigned thread_columns = (warp_columns+(threads_per_full_warp-1))/threads_per_full_warp;
    const bool has_remainder = ((num_species%(diffusion_warps/threads_per_full_warp))!=0);
    const bool local_uniform = ((thread_columns%threads_per_full_warp)==0);
    const unsigned num_full_points = (num_species/2);
    const unsigned num_partial_points = num_full_points-1;
    // Emit code for storing our local sumxod values 
    ost << REAL << " local_fracs[" << thread_columns << "];\n";
    ost << "const " << INT << " local_wid = wid >> " << (threads_per_full_warp-1) << ";\n";
    ost << "const " << INT << " local_offset = wid & " << local_mask << ";\n";
    // Note we group threads based on which full warp they fall into
    for (unsigned idx = 0; idx < thread_columns; idx++)
    {
      if (!local_uniform && (idx == (thread_columns-1)))
        ost << "if ((local_offset+" << (idx*threads_per_full_warp) << ") < " << warp_columns << ")\n";
      ost << "local_fracs[" << idx << "] = " << MOLE_FRAC << "[local_wid*" << warp_columns 
          << "+" << (idx*threads_per_full_warp) << "+local_offset][tid];\n";
    }
    for (unsigned idx = 0; idx < thread_columns; idx++)
    {
      if (!local_uniform && (idx == (thread_columns-1)))
        ost << "if ((local_offset+" << (idx*threads_per_full_warp) << ") < " << warp_columns << ")\n";
      PairDelim spec_pair(ost);
      ost << "const " << REAL << " local_mass = diffusion_masses[(local_wid*" << warp_columns << "+"
          << (idx*threads_per_full_warp) << "+local_offset+step*" << (2*num_species) << ")%" << (2*num_species) << "];\n";
      ost << "local_wtm = __fma_rn(local_fracs[" << idx << "],local_mass,local_wtm);\n";
      ost << "local_fracs[" << idx << "] = (local_fracs[" << idx << "] > " << SMALL << " ? "
          << "local_fracs[" << idx << "] : " << SMALL << ");\n";
      ost << "local_sumxw = __fma_rn(local_fracs[" << idx << "],local_mass,local_sumxw);\n";
    }
    // Now we can emit code for storing our sumxod values
    ost << REAL << " local_sumxod[" << thread_columns << "];\n";
    for (unsigned idx = 0; idx < thread_columns; idx++)
      ost << "local_sumxod[" << idx << "] = 0.0;\n";
    // Now iterate over the number of full points that we have and for each of them do our
    // cut across points
    unsigned coeff_offset = 0;
    for (unsigned point_idx = 0; point_idx < (num_full_points+warp_columns-1); point_idx++)
    {
      bool needs_start_check = false;
      bool needs_stop_check = false;
      // This is the range for all the threads in the full warp
      const unsigned start_across = (point_idx < num_full_points ? 0 : point_idx-num_full_points+1);
      const unsigned stop_across = (point_idx < warp_columns ? point_idx+1 : warp_columns);
      unsigned loop_start_across = start_across;
      unsigned loop_stop_across = stop_across;
      while ((loop_start_across%threads_per_full_warp) != 0)
      {
        assert(loop_start_across > 0);
        loop_start_across--;
        needs_start_check = true;
      }
      while ((loop_stop_across%threads_per_full_warp) != 0)
      {
        loop_stop_across++;
        needs_stop_check = true;
      }
      // Also see if we need a bottom check
      bool needs_bottom_check = needs_horizontal_check && (point_idx >= num_partial_points);
      ost << "// Starting point " << point_idx << "\n";
      PairDelim across_pair(ost);
      // Get the index for the species we are handling
      ost << "unsigned index = (local_wid*" << warp_columns << "+" << (point_idx+1) 
          << "+step*" << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2) << ";\n";
      // Handle the case of wrapped index
      ost << "if (index >= " << num_species << ") index -= " << num_species << ";\n";
      ost << REAL << " index_frac = " << MOLE_FRAC << "[index][tid];\n";
      ost << REAL << " index_sumxod = 0.0;\n";
      //ost << REAL << " index_sumxod = sumxod[index][tid];\n";
      ost << REAL << " index_clamped = (index_frac > " << SMALL << " ? index_frac : " << SMALL << ");\n";
      // Now go across for this point
      for (unsigned across_idx = loop_start_across; across_idx < loop_stop_across; across_idx+=threads_per_full_warp)
      {
        assert((across_idx%threads_per_full_warp)==0);
        const unsigned thread_across_idx = across_idx/threads_per_full_warp;
        ost << "// Across point (offset+" << (thread_across_idx) << ")\n";
        // Three possible checks here, one to see if we should even be doing the row (bottom)
        // one for the left side and one for the right side
        if (needs_bottom_check || (needs_start_check && (across_idx==loop_start_across)) 
                               || (needs_stop_check && (across_idx==(loop_stop_across-threads_per_full_warp))))
        {
          ost << "if (";
          if (needs_bottom_check)
          {
            //ost << "((local_wid*" << warp_columns << "+" << (across_idx)
            //    << "+local_offset) < " << num_partial_points << ")";
            ost << "((" << point_idx << " - (" << across_idx << "+local_offset)) < "
                << "(" << num_partial_points << "+"
                << "((local_wid*" << warp_columns << "+" << (across_idx) << "+local_offset)"
                << " < " << num_full_points << " ? 1 : 0)"
                << "))";
            // If we need the second check put an && operator
            if ((needs_start_check && (across_idx==loop_start_across)) 
                  || (needs_stop_check && (across_idx==(loop_stop_across-threads_per_full_warp))))
              ost << " && ";
          }
          if (needs_start_check && (across_idx==loop_start_across))
          {
            ost << "(" << (start_across%threads_per_full_warp) << " <= local_offset)"; 
            if (needs_stop_check && (across_idx==(loop_stop_across-threads_per_full_warp)))
              ost << " && ";
          }
          if (needs_stop_check && (across_idx==(loop_stop_across-threads_per_full_warp)))
          {
            ost << "((local_offset+" << (across_idx) << ") < " << stop_across << ")";
          }
          ost << ")\n";
        }
        PairDelim index_pair(ost);
        assert(point_idx >= across_idx);
        unsigned constant_index = coeff_offset*order+0;
        unsigned lane_index = constant_index%warp_size;
        unsigned thread_index = constant_index/warp_size;
        if (unit->k20)
        {
          ost << INT << " lo_part, hi_part;\n";
          ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << REAL << " val = __hiloint2double(hi_part,lo_part);\n";
          for (int i = 1; i < order; i++)
          {
            constant_index = coeff_offset*order+i;
            lane_index = constant_index%warp_size;
            thread_index = constant_index/warp_size;
            ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
            ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
            ost << "val = __fma_rn(val,logt,__hiloint2double(hi_part,lo_part));\n";
          }
        }
        else
        {
          ost << REAL << " local_thread_coeff;\n";
          // We don't have shuffle so bounce the values off the shared memory mirror
          // using warp-synchronous programming
          ost << "if (tid == " << lane_index << ")\n";
          ost << "  mirror[wid] = thread_coeffs[" << thread_index << "];\n";
          ost << REAL << " val = mirror[wid];\n";
          for (int i = 1; i < order; i++)
          {
            constant_index = coeff_offset*order+i;
            lane_index = constant_index%warp_size;
            thread_index = constant_index/warp_size;
            ost << "if (tid == " << lane_index << ")\n";
            ost << "  mirror[wid] = thread_coeffs[" << thread_index << "];\n";
            ost << "local_thread_coeff = mirror[wid];\n";
            ost << "val = __fma_rn(val,logt,local_thread_coeff);\n";
          }
        }
        // Update the coeff offset
        coeff_offset++;
        ost << "val = -val;\n";
        ost << "val = exp(val);\n";
        // Now update both the index species and our local value
        ost << "index_sumxod = __fma_rn(local_fracs[" << thread_across_idx << "],val,index_sumxod);\n";
        ost << "local_sumxod[" << thread_across_idx << "] = __fma_rn(index_clamped,val,local_sumxod[" << thread_across_idx << "]);\n";
      }
      // We've finished across this point, we need to reduce our temporaries out to shared memory
      
      if (warp_size < 32)
      {
        // If we're on Kepler, do the warp reduction with shuffles and then have
        // the first thread write out the result
        // For right now we'll only suppor the case of warp_size 16
        // To do more smaller warp sizes we'll need to emit as many shuffles as there are lanes
        if (unit->k20)
        {
          PairDelim reduc_pair(ost);
          ost << INT << " lo_part, hi_part;\n";
          for (unsigned stage = warp_size; stage < 32; stage *=2)
          {
            ost << "lo_part = __shfl_xor(__double2loint(index_sumxod), " << stage << ");\n"; 
            ost << "hi_part = __shfl_xor(__double2hiint(index_sumxod), " << stage << ");\n";
            ost << "index_sumxod += __hiloint2double(hi_part,lo_part);\n"; 
          }
          // Only the first thread in the warp needs to write the result back
          ost << "if (local_offset==0)\n";
          ost << "  sumxod[index][tid] += index_sumxod;\n";
        }
        else
        {
          // If we're on Fermi, we'll do this with warp synchronous programming
          for (unsigned mod = 0; mod < threads_per_full_warp; mod++)
          {
            {
              ost << "if ((wid%" << threads_per_full_warp<< ")==" << mod << ")\n";
              ost << "  sumxod[index][tid] += index_sumxod;\n";
              // Don't need this barrier since we're warp-synchronous
              //ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
            }
          }
        }
      }
      else
      {
        // Otherwise no need for special reductions, we can just do our update
        ost << "sumxod[index][tid] += index_sumxod;\n";
      }
      // We only need barriers after the every num_column number of points (Yay group theory!)
      // or it is the last iteration through
      if ((((point_idx+1)%warp_columns) == 0) || (point_idx == (num_full_points+warp_columns-2)))
        ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    }
    // Now that we've finished all our points, we need to reduce them out to shared
    for (unsigned idx = 0; idx < thread_columns; idx++)
    {
      if (!local_uniform && (idx == (thread_columns-1)))
       ost << "if ((local_offset+" << (idx*threads_per_full_warp) << ") < " << warp_columns << ")\n";       
      ost << "sumxod[local_wid*" << warp_columns << "+" << (idx*threads_per_full_warp) 
          << "+local_offset][tid] += local_sumxod[" << idx << "];\n";
    }
    // Put a barrier after we're done so we know we're done
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    if (has_remainder)
    {
      unsigned offset = (diffusion_warps/threads_per_full_warp)*warp_columns;
      assert(offset < num_species);
      unsigned predicated_threads = (num_species-offset)*warp_size;
      // Round up to a multiple of 32 to get a full warp
      while ((predicated_threads%32)!=0)
        predicated_threads++;
      ost << "// Handling remainders\n";
      ost << "if ((wid+" << offset << ") < " << num_species << ")\n";
      PairDelim remainder_pair(ost);
      // First emit the code to update our local values of wtm and sumxw
      ost << REAL << " local_frac = " << MOLE_FRAC << "[wid+" << (offset) << "][tid];\n";
      ost << REAL << " local_mass = diffusion_masses[wid+" << offset << "];\n";
      ost << "local_wtm = __fma_rn(local_frac,local_mass,local_wtm);\n";
      ost << REAL << " local_clamped = (local_frac > " << SMALL << " ? local_frac : " << SMALL << ");\n";
      ost << "local_sumxw = __fma_rn(local_frac,local_mass,local_sumxw);\n";
      ost << REAL << " local_dsum = 0.0;\n";
      // Now iterate over the points for this species
      unsigned num_remainder_points = (((num_species%2)==1) ? num_full_points : num_partial_points);
      // A check to make sure that we are either handling all partial points
      // and not a combination of partial points.  This will only fail in mechanisms
      // with very few species and large numbers of threads per points which should
      // be uncommon so we're going to ignore it for now
      if (num_remainder_points == num_partial_points)
        assert(diffusion_warps <= num_partial_points);
      for (unsigned point_idx = 0; point_idx < num_remainder_points; point_idx++)
      {
        PairDelim point_pair(ost);
        //ost << "unsigned index = (wid + " << (offset+1+point_idx) << "+ step*" << total_species << ")%" << total_species << ";\n";
        ost << "unsigned index = (wid + " << (offset+1+point_idx) << " + step*" << next_largest_power(2*num_species,2)
            << ")%" << next_largest_power(2*num_species,2) << ";\n";
        ost << "if (index >= " << num_species << ") index -= " << num_species<< ";\n";
        ost << REAL << " index_frac = " << MOLE_FRAC << "[index][tid];\n";
        ost << REAL << " index_sumxod = sumxod[index][tid];\n";
        unsigned constant_index = coeff_offset*order+0;
        unsigned lane_index = constant_index%warp_size;
        unsigned thread_index = constant_index/warp_size;
        if (unit->k20)
        {
          ost << INT << " hi_part, lo_part;\n";
          ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << REAL << " val = __hiloint2double(hi_part,lo_part);\n";
          for (int i = 1; i < order; i++)
          {
            constant_index = coeff_offset*order+i;
            lane_index = constant_index%warp_size;
            thread_index = constant_index/warp_size;
            ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
            ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
            ost << "val = __fma_rn(val,logt,__hiloint2double(hi_part,lo_part));\n";
          }
        }
        else
        {
          // Otherwise we don't have shuffles so bounce everything
          // off a shared memory mirror with warp-synchronous programming.
          ost << REAL << " local_thread_coeff;\n";
          ost << "if (tid == " << lane_index << ")\n";
          ost << "  mirror[wid] = thread_coeffs[" << thread_index << "];\n";
          ost << REAL << " val = mirror[wid];\n";
          for (int i = 1; i < order; i++)
          {
            constant_index = coeff_offset*order+i;
            lane_index = constant_index%warp_size;
            thread_index = constant_index/warp_size;
            ost << "if (tid == " << lane_index << ")\n";
            ost << "  mirror[wid] = thread_coeffs[" << thread_index << "];\n";
            ost << "local_thread_coeff = mirror[wid];\n";
            ost << "val = __fma_rn(val,logt,local_thread_coeff);\n";
          }
        }
        ost << "val = -val;\n";
        // Update the coeff offset
        coeff_offset++;

        ost << "val = exp(val);\n";
        ost << REAL << " index_clamped = (index_frac > " << SMALL << " ? index_frac : " << SMALL << ");\n";
        ost << "index_sumxod = __fma_rn(local_clamped,val,index_sumxod);\n";
        ost << "sumxod[index][tid] = index_sumxod;\n";
        ost << "local_dsum = __fma_rn(index_clamped,val,local_dsum);\n";
        // Need a barrier here, unless it is warp-synchronous
        if (predicated_threads > 32)
          ost << "asm volatile(\"bar.sync 1, " << predicated_threads << ";\" : : : \"memory\");\n";
      }
      // When we're done with all our points, then we get to write our result back
      ost << REAL << " local_sumxod = sumxod[wid+" << offset << "][tid];\n";
      ost << "local_sumxod += local_dsum;\n";
      ost << "sumxod[wid+" << offset << "][tid] = local_sumxod;\n";
    }
    // If we had a remainder, then we need an extra barrier here
    if (has_remainder)
      ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  }
  // Now we're ready to do the output computation
  // Emit the load for our pressure
  ost << REAL << " " << PRESSURE << ";\n";
  emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,unit->k20);
  // Reload the mole fractions
  {
    ost << REAL << " output_fracs[" << max_specs << "];\n";
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*diffusion_warps) << ") < " << num_species << ")\n";
      ost << "output_fracs[" << idx << "] = " << MOLE_FRAC << "[wid+" << (idx*diffusion_warps) << "][tid];\n";
    }
    // Emit a syncthreads before the next phase
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  }
  
  if ((warp_size < 32) && unit->k20)
  {
    if ((2*(diffusion_warps/threads_per_full_warp)) > num_species)
    {
      fprintf(stderr,"Warp-specialized diffusion kernel needs at least twice as many points as species. "
              "Decrease number of warps to meet this constraint.  Species %d, warps %d\n", 
              num_species, (diffusion_warps/threads_per_full_warp));
      exit(1);
    }
    {
      PairDelim reduc_pair(ost);
      ost << INT << " lo_part, hi_part;\n";
      for (unsigned stage = warp_size; stage < 32; stage *=2)
      {
        ost << "lo_part = __shfl_xor(__double2loint(local_sumxw), " << stage << ");\n"; 
        ost << "hi_part = __shfl_xor(__double2hiint(local_sumxw), " << stage << ");\n";
        ost << "local_sumxw += __hiloint2double(hi_part,lo_part);\n"; 
        ost << "lo_part = __shfl_xor(__double2loint(local_wtm), " << stage << ");\n";
        ost << "hi_part = __shfl_xor(__double2hiint(local_wtm), " << stage << ");\n";
        ost << "local_wtm += __hiloint2double(hi_part,lo_part);\n";
      }
    }
    // Now write out the value for local warp 0 inside our larger warp
    {
      ost << "if (local_offset==0)\n";
      PairDelim write_pair(ost);
      ost << MOLE_FRAC << "[local_wid][tid] = local_sumxw;\n";
      ost << MOLE_FRAC << "[local_wid+" << (diffusion_warps/threads_per_full_warp) << "][tid] = local_wtm;\n";
    }
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    {
      ost << REAL << " sumxw = 0.0;\n";
      ost << REAL << " wtm = 0.0;\n";
      PairDelim reduc_pair(ost); 
      ost << REAL << " reduc_sumxw[" << (diffusion_warps/threads_per_full_warp) << "];\n";
      ost << REAL << " reduc_wtm[" << (diffusion_warps/threads_per_full_warp) << "];\n";
      for (unsigned i = 0; i < (diffusion_warps/threads_per_full_warp); i++)
      {
        ost << "reduc_sumxw[" << i << "] = " << MOLE_FRAC << "[" << i << "][tid];\n";
        ost << "reduc_wtm[" << i << "] = " << MOLE_FRAC << "[" << ((diffusion_warps/threads_per_full_warp)+i) << "][tid];\n";
      }
      for (unsigned i = 0; i < (diffusion_warps/threads_per_full_warp); i++)
      {
        ost << "sumxw += reduc_sumxw[" << i << "];\n";
        ost << "wtm += reduc_wtm[" << i << "];\n";
      }
    }
  }
  else
  {
    if ((2*diffusion_warps) > num_species)
    {
      fprintf(stderr,"Warp-specialized diffusion kernel needs at least twice as many points as species. "
              "Decrease number of warps to meet this constraint.  Species %d, warps %d\n", num_species, diffusion_warps);
      exit(1);
    }
    ost << MOLE_FRAC << "[wid][tid] = local_sumxw;\n";
    ost << MOLE_FRAC << "[wid+" << diffusion_warps << "][tid] = local_wtm;\n";
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    {
      ost << REAL << " sumxw = 0.0;\n";
      ost << REAL << " wtm = 0.0;\n";
      PairDelim reduc_pair(ost);
      ost << REAL << " reduc_sumxw[" << diffusion_warps << "];\n";
      ost << REAL << " reduc_wtm[" << diffusion_warps << "];\n";
      // Do all the reads first so we get pipelined reads from shared memory
      for (unsigned i = 0; i < diffusion_warps; i++)
      {
        ost << "reduc_sumxw[" << i << "] = " << MOLE_FRAC << "[" << i << "][tid];\n";
        ost << "reduc_wtm[" << i << "] = " << MOLE_FRAC << "[" << (diffusion_warps+i) << "][tid];\n";
      }
      for (unsigned i = 0; i < diffusion_warps; i++)
      {
        ost << "sumxw += reduc_sumxw[" << i << "];\n";
        ost << "wtm += reduc_wtm[" << i << "];\n";
      }
    }
  }
  // Scale the pressure if necessary
  if (!unit->use_dim)
    ost << PRESSURE << " *= " << P_REF << ";\n";
  // Do the output computation
  {
    ost << REAL << " pfac = " << patmos << "/" << PRESSURE << ";\n";
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*diffusion_warps) << ") < " << num_species << ")\n";
      PairDelim output_pair(ost);
      ost << REAL << " clamped = (output_fracs[" << idx << "] > " << SMALL 
          << " ? output_fracs[" << idx << "] : " << SMALL << ");\n";
      ost << REAL << " result = pfac * (sumxw - (diffusion_masses[(wid+" << (idx*diffusion_warps) 
          << "+step*" << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2) << "]"
          << "*clamped)) / (wtm * sumxod[wid+" << (idx*diffusion_warps) << "][tid]);\n";
      if (!unit->use_dim)
        ost << "result *= " << (1.0/(DIF_REF * 1e4)) << ";\n";
      // Write out the result
      ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(diffusion+(wid+" 
          << (idx*diffusion_warps) << ")*spec_stride), " << "\"d\"(result) : \"memory\");\n";
    }
  }
  // We need a barrier at the end of the loop
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  ost << "// Update pointers for the next iteration\n";
  // Need to update all our pointers
  {
    PairDelim pointer_pair(ost);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
    ost << MIXMW_ARRAY << " += slice_stride;\n";
    ost << "diffusion += slice_stride;\n";
  }
}

