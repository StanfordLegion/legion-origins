
#ifndef __S3DGEN_GLOBALS__
#define __S3DGEN_GLOBALS__

// Definitions for types
#define REAL              "double"
#define INT               "int"
#define VOID              "void"
#define REAL_TYPE         double
#define INT_TYPE          int
#define VECTOR2           "__m128d"
#define VECTOR4           "__m256d"

#define TEMPERATURE       "temperature"
#define PRESSURE          "pressure"
#define MASS_FRAC         "mass_frac"
#define MOLE_FRAC         "mole_frac"
#define DIFFUSION         "diffusion"
#define WDOT              "wdot"
#define GIBBS             "cgspl"
#define THBCONC           "thbctemp"
#define MIXMW             "mixmw"
#define AVMOLWT           "avmolwt"

#define TEMPERATURE_ARRAY "temperature_array"
#define PRESSURE_ARRAY    "pressure_array"
#define MASS_FRAC_ARRAY   "mass_frac_array"
#define MIXMW_ARRAY       "mixmw_array"
#define AVMOLWT_ARRAY     "avmolwt_array"
#define DIFFUSION_ARRAY   "diffusion_array"
#define WDOT_ARRAY        "wdot_array"

#define GETEXP            "exp"

// Maximum amount of shared memory on NVIDIA GPUs
#define MAX_SHARED        49152
// Maximum number of named barriers on NVIDIA GPUs
#define MAX_BARRIERS      16
// Maximum number of SMs on NVIDIA GPUs
#define MAX_SM_COUNT      16

// Helper stuff for non-dimensionalization
/*
 * Physical quantities (units).  Do not change.
 *   R (gas constant):  J/(kg-mol K)
 *   cal/gmole --> K :  J/cal * g/kg / (J / (kg-mol K)) == gmole K / cal
 *   kg-mol --> g-mol:  g-mol/kg-mol
 *   cm3 --> m3      :  m^3/cm^3
 */
#define R_GAS_CONST	(8314.51)
#define CAL_PER_GMOLE__TO__KELVIN  (4.184 * 1000. / R_GAS_CONST)
#define GMOL__TO__KGMOL  (1.0e-3)
#define KGMOL__TO__GMOL  (1.0e+3)
#define CM3__TO__M3      (1.0e-6)
#define M3__TO__CM3      (1.0e+6)

/*
 * Reference quantities.  Ensure consistency.
 *   temperature:   kelvin
 *   length:        meters
 *   mol. wt.:      kg/kg-mol
 *   pressure:      N/m^2  (atmospheric)
 *   density:       kg/m^3
 *   concentration: kg-mol/m^3
 *   reaction rate: kg/(m^3 s)
 */
#if 0
// These are the old reference temperatures migrated over from
// the cgetrates code generator
#define TEMPERATURE_REF	  (300.0)
#define LENGTH_REF	  (1.0e-2)
#define MOLAR_REF	  (28.0)
#define PRESSURE_REF	  (101325.0)
#define RGAS_REF	  (R_GAS_CONST / MOLAR_REF)
#define DENSITY_REF       (PRESSURE_REF / (RGAS_REF * TEMPERATURE_REF))
/* #define CONCENTRATION_REF (DENSITY_REF / MOLAR_REF) */
/* equivalent formulation, gets rid of MOLAR_REF cancellation: */
#define CONCENTRATION_REF (PRESSURE_REF / (R_GAS_CONST * TEMPERATURE_REF))
#define REACTION_RATE_REF (500.0)
#else
// These are the reference values from observed runs of S3D
#define MACH_NO           (0.001)
#define RE_REAL           (100.0)
#define PARAM_PR          (0.708)
#define G_REF             (1.4)
#define A_REF             (347.2) // m/s
#define T_0               (300.0) // K
#define RHO_REF           (1.1766)  // Kg/m3
#define LAMBDA_REF        (26.14e-3)
#define PARAM_RE          (RE_REAL / MACH_NO)
#define T_REF             (T_0 * (G_REF - 1))
#define CP_REF            ((A_REF * A_REF) / T_REF)
#define L_REF             (PARAM_PR * LAMBDA_REF / CP_REF * PARAM_RE / RHO_REF / A_REF)
#define MU_REF            (RHO_REF * L_REF * A_REF)
#define P_REF             ((A_REF * A_REF) * RHO_REF * 10.0)
#define TIME_REF          (L_REF / A_REF)
#define RR_REF            (L_REF * 1e6 / (RHO_REF * A_REF))
#define TEMPERATURE_REF   (T_REF)
#define LENGTH_REF        (L_REF)
#define PRESSURE_REF      (P_REF)
#define REACTION_RATE_REF (RR_REF)
#define GASCONST          (8.31451)
#define CONCENTRATION_REF (PRESSURE_REF / (GASCONST * TEMPERATURE_REF))
//#define MOLAR_REF         (R_GAS_CONST * T_REF * A_REF * TIME_REF * TIME_REF * TIME_REF / (RHO_REF * 1000.0))
#define MOLAR_REF         (1.0)
#define TCONV             ((G_REF - 1.0)*T_0)
#define DIFF_REF          (RHO_REF/TCONV/1.0e3)
#define VIS_REF           (RHO_REF * A_REF * L_REF)
#define DIF_REF           (A_REF * L_REF)
// Defined as constant in tranlib
#define SMALL 1.0e-20
#endif

#endif // __S3DGEN_GLOBALS__
