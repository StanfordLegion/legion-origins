
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <unistd.h>

#include "singe.h"

static void usage(const char *progname);

int main(int argc, char **argv)
{
  bool dimensional = false;
  bool emit_cuda = false;
  bool emit_physics = true;
  bool emit_chemistry = true;
  bool emit_profiling = false;
  bool target_k20 = false;
  bool multi_pass = false;
  bool use_indexing = false;
  bool qssa_in_shared = false;
  bool spill_reac_rates = false;
  bool spill_gibbs = false;
  bool spill_qssa = false;
  const char *dir_name = NULL;
  bool emit_sse = false;
  bool emit_avx = false;
  bool emit_debug = false;
  bool unroll_loops = false;
  bool unaligned = false;
  bool perfect = false;
  int warp_size = 32;
  int visc_warps = 0;
  int diff_warps = 0;
  int rate_warps = 0;
  int qssa_warps = 0;
  int target_ctas = 0;
  int qssa_skip = 0;
  int taylor_stages = 12; // default from libm
  size_t cache_size = 128; // good default value
  const char *small_mole_frac = "1e-200";
  const char *small_reac_rate = "1e-200";
  const char *large_reac_rate = "1e200";

#define MATCH(str) (strcmp(argv[idx],str) == 0)
  for (int idx = 1; idx < argc; idx++)
  {
    if (MATCH("--dim") || MATCH("-n"))
    {
      dimensional = true;
      continue;
    }
    if (MATCH("--dir") || MATCH("-d"))
    {
      dir_name = argv[++idx];
      continue;
    }
    if (MATCH("--cuda") || MATCH("-c"))
    {
      emit_cuda = true;
      continue;
    }
    if (MATCH("--nophy"))
    {
      emit_physics = false;
      continue;
    }
    if (MATCH("--nochem"))
    {
      emit_chemistry = false;
      continue;
    }
    if (MATCH("--profile"))
    {
      emit_profiling = true;
      continue;
    }
    if (MATCH("--k20"))
    {
      target_k20 = true;
      continue;
    }
    if (MATCH("--warp"))
    {
      int val = atoi(argv[++idx]);
      if ((val != 4) && (val != 8) && (val != 16) && (val != 32))
      {
        fprintf(stderr,"ERROR: Effective warp must be 4, 8, 16, or 32!\n");
        exit(1);
      }
      warp_size = val;
      continue;
    }
    if (MATCH("--visc"))
    {
      visc_warps = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("--diff"))
    {
      diff_warps = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("--reac"))
    {
      rate_warps = atoi(argv[++idx]);
      // Default multi-pass to true
      multi_pass = true;
      continue;
    }
    if (MATCH("--multi"))
    {
      multi_pass = true;
      continue;
    }
    if (MATCH("--qssa"))
    {
      qssa_warps = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("--ctas"))
    {
      target_ctas = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("--skip"))
    {
      qssa_skip = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("--qshared"))
    {
      qssa_in_shared = true;
      continue;
    }
    if (MATCH("--spill-reacs"))
    {
      spill_reac_rates = true;
      continue;
    }
    if (MATCH("--spill-gibbs"))
    {
      spill_gibbs = true;
      continue;
    }
    if (MATCH("--spill-qssa")) {
      spill_qssa = true;
      continue;
    }
    if (MATCH("--vector"))
    {
      if (strcmp(argv[++idx],"sse") == 0)
        emit_sse = true;
      else if (strcmp(argv[idx],"avx") == 0)
        emit_avx = true;
      else
        fprintf(stderr,"Unrecognized vector instruction set: %s.  Option ignored.\n", argv[idx]);
      continue;
    }
    if (MATCH("--cache"))
    {
      cache_size = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("--perfect"))
    {
      perfect = true;
      continue;
    }
    if (MATCH("--debug"))
    {
      emit_debug = true; 
      continue;
    }
    if (MATCH("--unroll"))
    {
      unroll_loops = true;
      continue;
    }
    if (MATCH("--unaligned"))
    {
      unaligned = true;
      continue;
    }
    if (MATCH("--smallmole"))
    {
      small_mole_frac = argv[++idx];
      continue;
    }
    if (MATCH("--smallreac"))
    {
      small_reac_rate = argv[++idx];
      continue;
    }
    if (MATCH("--largereac"))
    {
      large_reac_rate = argv[++idx];
      continue;
    }
    // Not suporting this anymore
#if 0
    if (MATCH("--index"))
    {
      use_indexing = true;
      continue;
    }
#endif
  }
#undef MATCH
  if (argc == 1)
  {
    usage(argv[0]);
    return 1;
  }
  TranslationUnit *unit = NULL;
  // Parse the chemistry file
  {
    char source[128];
    strcpy(source,dir_name);
    strcat(source,"/chem.inp");
    unit = TranslationUnit::parse_source_file(source);
    assert(unit != NULL);
  }
  // Parse the QSSA file
  {
    char source[128];
    strcpy(source,dir_name);
    strcat(source,"/qss_stif.txt");
    unit->parse_qss_stif_file(source);
  }
  // Do this after QSSA so we can weed out any
  // nasty QSSA species
  unit->post_parse();
  // Parse the thermodynamics file
  {
    char source[128];
    strcpy(source,dir_name);
    strcat(source,"/therm.dat");
    unit->parse_therm_file(source);
  }
  // Parse the transport file
  {
    char source[128];
    strcpy(source,dir_name);
    strcat(source,"/tran.dat");
    if (!unit->parse_tran_dat_file(source))
    {
      strcpy(source,dir_name);
      strcat(source,"/tran.asc"); 
      unit->parse_tran_asc_file(source);
    }
  }
  unit->print_summary();
  unit->k20 = target_k20;
  unit->multi_pass_warp_specialized = multi_pass;
  unit->warp_size = warp_size;
  unit->viscosity_warps = visc_warps;
  unit->diffusion_warps = diff_warps;
  unit->reac_warps = rate_warps;
  unit->qss_warps = qssa_warps;
  unit->target_ctas = target_ctas;
  unit->use_indexing = use_indexing;
  unit->qssa_skip = qssa_skip;
  unit->qssa_in_shared = qssa_in_shared;
  unit->spill_reac_rates = spill_reac_rates;
  unit->spill_gibbs = spill_gibbs;
  unit->spill_qssa = spill_qssa;
  unit->taylor_stages = taylor_stages;
  unit->cache_size = cache_size;
  unit->perfect = perfect;
  unit->profile = emit_profiling;
  unit->emit_debug = emit_debug;
  unit->unroll_loops = unroll_loops;
  unit->unaligned = unaligned;
  unit->small_mole_frac = small_mole_frac;
  unit->small_reac_rate = small_reac_rate;
  unit->large_reac_rate = large_reac_rate;

  // Do any non-dimensionalization if necessary
  if (!dimensional)
    unit->non_dimensionalize();

  // Emit the code
  if (emit_physics)
  {
    unit->emit_baseline_cpu_physics("getcoeffs");
    if (emit_sse)
      unit->emit_sse_cpu_physics("sse_getcoeffs");
    if (emit_avx)
      unit->emit_avx_cpu_physics("avx_getcoeffs");
  }
  if (emit_chemistry)
  {
    unit->emit_baseline_cpu_chemistry("getrates");
    if (emit_sse)
      unit->emit_sse_cpu_chemistry("sse_getrates");
    if (emit_avx)
      unit->emit_avx_cpu_chemistry("avx_getrates");
  }

  // Emit cuda code
  if (emit_cuda)
  {
    if (emit_physics)
      unit->emit_cuda_physics("gpu_getcoeffs");
    if (emit_chemistry)
      unit->emit_cuda_chemistry("gpu_getrates");
  }

  // Emit profiling information
  if (emit_profiling)
  {
    if (emit_physics)
      unit->emit_physics_profiling();
    if (emit_chemistry)
      unit->emit_chemistry_profiling();
  }

  return 0;
}

static void usage(const char *progname)
{
  fprintf(stderr,"Usage: %s [options]\n", progname);
  fprintf(stderr,"  --dim/-n : produce code that takes dimensionalized inputs\n");
  fprintf(stderr,"  --dir/-d <directory> : target directory for input files\n");
  fprintf(stderr,"  --cuda : emit CUDA GPU code\n");
  fprintf(stderr,"  --nophy : do not emit physics code\n");
  fprintf(stderr,"  --nochem : do not emit chemistry code\n");
  fprintf(stderr,"  --profile : emit profiling information about each kernel\n");
  fprintf(stderr,"  --k20 : target K20 GPUs\n");
  fprintf(stderr,"  --warp <1,2,4,8,16,32> : change the effective warp size for warp-specialized code\n");
  fprintf(stderr,"  --visc <warps> : specify the number of warps to use for warp-specialized viscosity code\n");
  fprintf(stderr,"  --diff <warps> : specify the number of warps to use for warp-specialized diffusion code\n");
  fprintf(stderr,"  --reac <warps> : specify the number of warps to use for warp-specialized getrates code\n");
  fprintf(stderr,"  --multi : generate multi-pass warp-specialized GPU code\n");
  fprintf(stderr,"  --qssa <warps> : specify the number of QSSA warps for multi-pass code generation\n");
  fprintf(stderr,"  --ctas <ctas> : target CTAs/SM for multi-pass code generation\n");
  fprintf(stderr,"  --skip <reacs> : specify the number of reactions to skip for QSSA warps\n");
  fprintf(stderr,"  --qshared : perform QSSA computation out of shared memory\n");
  fprintf(stderr,"  --spill-reacs: spill reaction rates to global memory\n");
  fprintf(stderr,"  --spill-gibbs: spill gibbs rates to global memory\n");
  fprintf(stderr,"  --spill-qssa: spill values associated with QSSA\n");
  fprintf(stderr,"  --vector <sse,avx> : emit vectorized sse or avx code\n");
  fprintf(stderr,"  --cache <size> : emit cache blocking size in KB\n");
  fprintf(stderr,"  --perfect : assume elements are divible by blocking factor\n");
  fprintf(stderr,"  --unroll : unroll loops when generating source code\n");
  fprintf(stderr,"  --unaligned : emit AVX code for handling pointers not aligned to 32 byte boundary\n");
  fprintf(stderr,"  --taylor <stages> : number of taylor series terms for exponentials\n");
  fprintf(stderr,"  --debug : emit debugging code for GPU chemistry code\n");
  fprintf(stderr,"  --smallmole <value> : clamp value for small mole fractions, default=1e-200\n");
  fprintf(stderr,"  --smallreac <value> : clamp value for small reaction rates, default=1e-200\n");
  fprintf(stderr,"  --largereac <value> : clamp value for large reaction rates, default=1e200\n");
  //fprintf(stderr,"  --index : use shared memory indexing instead of per warp branching\n");
}
