
#include "codegen.h"

// CodeOutStream

CodeOutStream::CodeOutStream(const char *fname, bool bound, 
                             unsigned max, unsigned indent)
  : file_name(fname), target(NULL), 
    buffer_length(DEFAULT_LINE_LENGTH*sizeof(char)),
    line_buffer((char*)malloc(buffer_length)),
    depth(0), line_index(0), indent_length(indent),
    bound_lines(bound), max_line_length(bound ? max : DEFAULT_LINE_LENGTH) 
{
  target = fopen(file_name,"w");
  if (target == NULL)
  {
    fprintf(stderr,"Failure opening file %s\n", file_name);
    exit(1);
  }
}

CodeOutStream::~CodeOutStream(void)
{
  if (fclose(target) != 0)
  {
    fprintf(stderr,"Failture closing file %s\n", file_name);
  }
  free(line_buffer);
}

CodeOutStream& CodeOutStream::operator<<(const char *message)
{
  print_while_inserting_indentation(message);
  return *this;
}

CodeOutStream& CodeOutStream::operator<<(char val)
{
  char buffer[2];
  buffer[0] = val;
  buffer[1] = '\0';
  // Make sure we handle new line characters correctly
  print_while_inserting_indentation(buffer);
  return *this;
}

CodeOutStream& CodeOutStream::operator<<(bool val)
{
  if (val)
    print_while_inserting_indentation("true");
  else
    print_while_inserting_indentation("false");
  return *this;
}

CodeOutStream& CodeOutStream::operator<<(int val)
{
  char buffer[128];
  snprintf(buffer,128,"%d",val);
  print_while_inserting_indentation(buffer);
  return *this;
}

CodeOutStream& CodeOutStream::operator<<(unsigned int val)
{
  char buffer[128];
  snprintf(buffer,128,"%u",val);
  print_while_inserting_indentation(buffer);
  return *this;
}

CodeOutStream& CodeOutStream::operator<<(long val)
{
  char buffer[128];
  snprintf(buffer,128,"%ld",val);
  print_while_inserting_indentation(buffer);
  return *this;
}

CodeOutStream& CodeOutStream::operator<<(unsigned long val)
{
  char buffer[128];
  snprintf(buffer,128,"%lu",val);
  print_while_inserting_indentation(buffer);
  return *this;
}

CodeOutStream& CodeOutStream::operator<<(float val)
{
  char buffer[128];
  snprintf(buffer,128,"%.15e",val);
  clean_floating_point_buffer(buffer);
  print_while_inserting_indentation(buffer);
  return *this;
}

CodeOutStream& CodeOutStream::operator<<(double val)
{
  char buffer[128];
  snprintf(buffer,128,"%.15e",val);
  clean_floating_point_buffer(buffer);
  print_while_inserting_indentation(buffer);
  return *this;
}

void CodeOutStream::print_while_inserting_indentation(const char *message)
{
  assert(target != NULL);
  size_t length = strlen(message);
  for (unsigned idx = 0; idx < length; idx++)
  {
    assert(line_index <= buffer_length);
    if (line_index == buffer_length)
      resize_buffer();
    char c = message[idx];
    if (c == '\n')
      flush_line_buffer();
    else
      line_buffer[line_index++] = c;
  }
}

void CodeOutStream::resize_buffer(void)
{
  assert(line_buffer != NULL);
  assert(buffer_length > 0);
  line_buffer = (char*)realloc(line_buffer, 2*buffer_length);
  assert(line_buffer != NULL);
  buffer_length *= 2;
  // Make sure there is no integer overflow
  assert(buffer_length > 0);
}

void CodeOutStream::print_single_indent(bool add)
{
  assert(target != NULL);
  for (unsigned i = 0; i < indent_length; i++)
  {
    if (add)
    {
      assert(line_index <= buffer_length);
      if (line_index == buffer_length)
        resize_buffer();
      line_buffer[line_index++] = ' ';
    }
    else
    {
      assert(line_index > 0);
      line_index--;
    }
  }
}

void CodeOutStream::flush_line_buffer(void)
{
  assert(target != NULL);
  assert(line_index < buffer_length);
  line_buffer[line_index] = '\0';
  if (bound_lines)
  {
    // If we're bounding line lengths, print out the
    // line in nice chunks
    if (line_index <= max_line_length)
      fprintf(target,"%s\n", line_buffer);
    else
    {
      // Otherwise tokenize it and print tokens until
      // you run over budget on a line
      // Print the indent for the start of the line
      for (unsigned i = 0; i < depth; i++)
        for (unsigned j = 0; j < indent_length; j++)
          fprintf(target," ");
      char *pch = strtok(line_buffer," ");
      unsigned current_line = (depth*indent_length);
      while (pch != NULL)
      {
        size_t token_length = strlen(pch);
        if ((current_line+token_length) > max_line_length)
        {
          fprintf(target,"\n");
          // Indent by one more to show this line is wrapped
          for (unsigned i = 0; i < (depth+1); i++)
            for (unsigned j = 0; j < indent_length; j++)
              fprintf(target," ");
          current_line = ((depth+1)*indent_length);
        }
        fprintf(target,"%s ",pch);
        current_line += (token_length+1);
        pch = strtok(NULL," ");
      }
      // Once we're done, print the final newline
      fprintf(target,"\n");
    }
  }
  else
  {
    fprintf(target,"%s\n", line_buffer);
  }
  line_index = 0;
  // Reset the indent
  for (unsigned i = 0; i < depth; i++)
    print_single_indent();
}

void CodeOutStream::clean_floating_point_buffer(char *s)
{
  // This code is copied directly from the orignal cgetrates
  // to maintain compatibility with floating point numbers
  char *cp, *cq;
  for (cp=s; *cp; cp++) ;
  for (--cp; *cp != 'e'; cp--) ;
  cq = cp;
  for (--cp; *cp == '0'; cp--) ;
  ++cp;
  if (cp != cq) {
      if (*(cp-1) == '.')  ++cp;
      for (; *cq != '\0'; )
          *cp++ = *cq++;
      *cp = '\0';
  }
  /* printf("to %15s ", s); */
  for (cp=s; *cp; cp++) ;
  if (*(--cp-1) == '0') {
      if (*cp == '0')  *(cp-3) = '\0';
      else if (*(cp-2) == '+') {
          if (*cp == '1') {
              /* printf("from %s", s); */
              if (*(cq=s) == '-')  ++cq;
              cq[1] = cq[2];
              cq[2] = '.';
              *(cp-3) = '\0';
              for (cp=s; *cp != '.'; cp++) ;
              if (*++cp == '\0') {
                  *cp++ = '0';
                  *cp = '\0';
              }
              /* printf(" to %s\n", s); */
          } else if (*cp == '2') {
              /* printf("from %s", s); */
              if (*(cq=s) == '-')  ++cq;
              cq[1] = cq[2];
              if ((cq[2] = cq[3]) == 'e')
                  cq[2] = '0';
              cq[3] = '.';
              *(cp-3) = '\0';
              for (cp=s; *cp != '\0' && *cp != '.'; cp++) ;
              if (*cp == '\0') {
                  *cp++ = '.';
                  *cp++ = '0';
                  *cp = '\0';
              } else if (*++cp == '\0') {
                  *cp++ = '0';
                  *cp = '\0';
              }
              /* printf(" to %s\n", s); */
          }
      } else {  /* - */
          if (*cp == '1') {
              /* printf("from %s", s); */
              *(cp-2) = '\0';
              cq = (s[0] == '-' ? s+1 : s);
              for (cp-=3; cp != cq+2; cp--)
                  *cp = *(cp-1);
              *cp = cq[0];
              cq[0] = '0';
              for (cp=s; *cp; cp++) ;
              if (*--cp == '0')
                  *cp = '\0';
              /* printf(" to %s\n", s); */
          } else if (*cp == '2') {
              /* printf("from %s", s); */
              *(cp-1) = '\0';
              cq = (s[0] == '-' ? s+1 : s);
              for (cp-=2; cp != cq+3; cp--)
                  *cp = *(cp-2);
              *cp-- = cq[0];
              *cp = '0';
              cq[0] = '0';
              for (cp=s; *cp; cp++) ;
              if (*--cp == '0')
                  *cp = '\0';
              /* printf(" to %s\n", s); */
          }
      }
  }
}

// PairDelim

PairDelim::PairDelim(CodeOutStream &o)
  : ost(o), close("}\n")
{
  ost.print("{\n");
  ost.down();
}

PairDelim::PairDelim(CodeOutStream &o, const char *message)
  : ost(o), close("}\n")
{
  ost.print(message);
  ost.print("{\n");
  ost.down();
}

PairDelim::PairDelim(CodeOutStream &o, const char *message, const char *open, const char *cl)
  : ost(o), close(cl)
{
  ost.print(message);
  ost.print(open);
  ost.down();
}

PairDelim::~PairDelim(void)
{
  ost.up();
  ost.print(close);
}


