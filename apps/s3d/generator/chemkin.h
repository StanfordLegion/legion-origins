
#ifndef __S3D_TEMPLATES__
#define __S3D_TEMPLATES__

#include "getrates.h"

#include <cmath>
#include <cassert>

template<typename TI,typename TR>
inline TI select_coefficients(const TR tk1, const TI *ickwrk, const TR *rckwrk, const int i)
{
  // number of temperature ranges for this species
  TI ntr = ickwrk[IcNT+i] - 1;
  // location of the first set of thermodynamic fit coefficients
  TI na1 = NcAA + i*NCP2T; 
  // location of upp limit of first temperature range
  TI ktemp = NcTT + i*MXTP + 1; 

  // This loop is selecting the right set of coefficients to
  // use based on the current temperature range.  Only took
  // 15 minutes to figure that out from the fortran.
  while ((ntr > 1) && (tk1 > rckwrk[ktemp]))
  {
    // Remaining number of temperature ranges
    ntr = ntr - 1;
    // Location of the next set of coefficients
    na1 = na1 + NCP2;
    ktemp = ktemp+1;
  }
  return na1;
}

template<typename TI, typename TR, unsigned NUM_SPECIES>
inline void compute_enthalpies(const TR temperature, const TI *ickwrk, const TR *rckwrk, TR enthalpy[NUM_SPECIES])
{
  // Compute temperature powers
  TR tk1 = temperature;
  TR tk2 = tk1 * tk1;
  TR tk3 = tk1 * tk2;
  TR tk4 = tk1 * tk3;
  TR tk5 = tk1 * tk4;
  tk2 /= 2;
  tk3 /= 3;
  tk4 /= 4;
  tk5 /= 5;

  for (unsigned int i = 0; i < NUM_SPECIES; i++)
  {
    // Make sure we can use the temperatures computed above
    assert(ickwrk[IcKTF+i] == 1);
    
    TI na1 = select_coefficients<TI,TR>(tk1, ickwrk, rckwrk, i);
    
    // Now do the output computation
    enthalpy[i] = rckwrk[NcRU] * (rckwrk[na1]*tk1
                  + rckwrk[na1+1] * tk2 + rckwrk[na1+2] * tk3
                  + rckwrk[na1+3] * tk4 + rckwrk[na1+4] * tk5
                  + rckwrk[na1+NCP1-1]);
  }
}

template<typename TI, typename TR, unsigned NUM_SPECIES>
inline void compute_entropies(const TR temperature, const TI *ickwrk, const TR *rckwrk, TR entropy[NUM_SPECIES])
{
  TR tk1 = temperature;
  TR tklog = log(tk1);
  TR tk2 = tk1 * tk1;
  TR tk3 = tk1 * tk2;
  TR tk4 = tk1 * tk3;
  tk2 /= 2;
  tk3 /= 3;
  tk4 /= 4;

  for (unsigned int i = 0; i < NUM_SPECIES; i++)
  {
    // Make sure we can use temperatures computed above 
    assert(ickwrk[IcKTF+i] == 1);

    TI na1 = select_coefficients<TI,TR>(tk1, ickwrk, rckwrk, i);

    // Now do the output computation
    entropy[i] = rckwrk[NcRU] * (rckwrk[na1] * tklog
                  + rckwrk[na1+1] * tk1 + rckwrk[na1+2] * tk2
                  + rckwrk[na1+3] * tk3 + rckwrk[na1+4] * tk4
                  + rckwrk[na1+NCP2-1]);
  }
}

template<typename TI, typename TR, unsigned NUM_SPECIES>
inline void compute_gibbs(const TR temperature, const TI *ickwrk, const TR *rckwrk, TR gml[NUM_SPECIES])
{
  TR enthalpy[NUM_SPECIES];
  TR  entropy[NUM_SPECIES];
  // Compute enthalpies
  compute_enthalpies<TI,TR,NUM_SPECIES>(temperature, ickwrk, rckwrk, enthalpy); 
  compute_entropies<TI,TR,NUM_SPECIES>(temperature, ickwrk, rckwrk, entropy);

  for (unsigned int i = 0; i < NUM_SPECIES; i++)
  {
    // Make sure all the temperatures point to one temperature
    // so we can use the input temperature and not the species-specific temp
    assert(ickwrk[IcKTF+i] == 1);
    gml[i] = enthalpy[i] - (temperature * entropy[i]);
  }
}

inline void compute_gibbs_fast(const double temperature, double gml[NUM_SPECIES])
{
  double tk1 = temperature;
  double logtk = log(tk1);
  double tk2 = tk1 * tk1;
  double tk3 = tk1 * tk2;
  double tk4 = tk1 * tk3;
  double tk5 = tk1 * tk4;
  tk2 /= 2;
  tk3 /= 3;
  tk4 /= 4;
  tk5 /= 5;

  double temp_splits[NUM_SPECIES] = { };
  double high_coeffs[7][NUM_SPECIES] = { };
  double low_coeffs[7][NUM_SPECIES] = { };

  for (unsigned int i = 0; i < NUM_SPECIES; i++)
  {
     
  }
}

template<typename TI, typename TR, unsigned NUM_SPECIES>
inline void compute_molar_concentrations(const TR temperature, const TR pressure, 
      const TR *mass_fractions, const TI *ickwrk, const TR *rckwrk, TR concentrations[NUM_SPECIES])
{
  TR sumyow = 0.0; 
  for (unsigned int i = 0; i < NUM_SPECIES; i++)
  {
    // Make sure we can use the same temperature for all species
    assert(ickwrk[IcKTF+i] == 1);
    sumyow += (mass_fractions[i] * temperature / rckwrk[NcWT+i]);  
  }
  sumyow *= rckwrk[NcRU];
  for (unsigned int i = 0; i < NUM_SPECIES; i++)
  {
    concentrations[i] = pressure * mass_fractions[i]/(sumyow * rckwrk[NcWT+i]);
  }
}

#endif // __S3D_TEMPLATES__
