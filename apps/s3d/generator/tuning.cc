
#include <cstdio>
#include <cstdlib>
#include <cassert>

#include <algorithm>

#include "tuning.h"
#include "s3dgen_ast.h"

void TranslationUnit::tune_gpu(unsigned int max_iters)
{
  // Construct a graph of the mechanism with reactions as nodes and
  // edges between reactions that share at least one species in common.
  // The edge weight will be the number of species that the reactions
  // have in common.  We'll use a greedy algorithm to do an appoximation
  // of finding the mimimum cuts of the graph.  Pick the reaction with
  // the most species.  Gradually expand the chunk by picking nodes
  // which have the greatest overlap with species already in the current
  // chunk.  Break ties randomly.  Continue adding species until the
  // kernel starts spilling to local memory.  Then pick the next biggest
  // reaction and repeat until we've generated kernels to cover all
  // reactions.  Compute the amount of transfers required and use that to
  // evaluate the cost of the chunking.  Keep track of the best configuration.

  ReactionGraph *graph = new ReactionGraph(species, all_reactions);
  Slicing *best_slicing = NULL;
  unsigned best_cost = 1 << 30;
  // Iterate for the maximum number of iterations
  for (unsigned int iter = 0; iter < max_iters; iter++)
  {
    fprintf(stdout,"Starting iteration %d\n", iter);
    Slicing *current_slicing = new Slicing(this);
    ReactionNode *chunk_start = graph->find_largest_uncolored();
    while (chunk_start != NULL)
    {
      unsigned chunk_color = current_slicing->get_next_chunk_color();
      //fprintf(stdout,"Starting chunk %d\n", chunk_color);
      ReactionChunk *current = new ReactionChunk(chunk_color, this);
      current->add_reaction(chunk_start->reaction);
      graph->color_node(chunk_start, chunk_color);
      current->compute_ordered_species();
      // Keep adding reactions until we no longer fit in registers
      // and we still have adjacent reactions
      while (true)
      {
        std::map<ReactionNode*,unsigned> adjacent;
        graph->find_adjacent(chunk_color, current->get_species(), adjacent);
        // Make sure there are adjacent nodes
        if (adjacent.empty())
          break;
        ReactionNode *next = NULL;
        if (adjacent.size() == 1)
          next = (adjacent.begin())->first;
        else
        {
          // Select randomly with each adjacent node's probability
          // of being selected proportional to the square the weight
          // of the edge that they share.
          // Sum up the square of the weights
          unsigned total_weight = 0;
          for (std::map<ReactionNode*,unsigned>::const_iterator it = adjacent.begin();
                it != adjacent.end(); it++)
          {
            total_weight += (it->second * it->second);
          }
          unsigned chosen_index = lrand48() % total_weight;
          // Find the chosen reaction
          for (std::map<ReactionNode*,unsigned>::const_iterator it = adjacent.begin();
                it != adjacent.end(); it++)
          {
            unsigned range = it->second * it->second;
            if (chosen_index >= range)
            {
              // Subtract off this range and continue
              chosen_index -= range;
              continue;
            }
            // Otherwise we found our match
            next = it->first;
            break;
          }
          assert(next != NULL);
        }
        assert(next != NULL);
        ReactionChunk *next_potential = current->clone(); 
        next_potential->add_reaction(next->reaction);
        next_potential->compute_ordered_species();
        //fprintf(stdout,"Attempting to add reaction %d\n", next->reaction->reaction_idx);
        // Test to see if it still compiles into registers
        if (next_potential->compiles_to_registers())
        {
          // Update current, delete the old one
          delete current;
          current = next_potential;
          // Color the new node as being a part of this chunk
          graph->color_node(next, chunk_color);
        }
        else
        {
          // failed to compile into registers
          // delete the next_potential
          // break out of the loop
          delete next_potential;
          break;
        }
      }
      // Done with the current chunk, at it to the set and start again
      current_slicing->add_chunk(current);
      chunk_start = graph->find_largest_uncolored();
    }
    // We've got our set of chunks, so do our analysis
    unsigned slicing_cost = current_slicing->compute_cost();
    fprintf(stdout,"Current slicing %d    Best slicing %d\n", slicing_cost, best_cost);
    if ((best_slicing == NULL) || (slicing_cost < best_cost))
    {
      fprintf(stdout, "FOUND NEW BEST SLICING COST!!!!!!!!!!!!!\n");
      if (best_slicing != NULL)
        delete best_slicing;
      best_slicing = current_slicing;
      best_cost = slicing_cost;
      best_slicing->dump_slicing("best_slicing.cfg");
    }
    else
    {
      delete current_slicing;
      current_slicing = NULL;
    }
    // Clean the graph before starting again
    graph->clean_graph();
  }
  delete graph;
}

void TranslationUnit::tune_cpu(unsigned int max_tuning_iters, unsigned int max_chunks)
{
  ReactionGraph *graph = new ReactionGraph(species, all_reactions);
  Slicing *best_slicing = NULL;
  float best_time = 1e9f;
  for (unsigned int iter = 0; iter < max_tuning_iters; iter++)
  {
    fprintf(stdout,"Starting iteration %d\n", iter);
    //for (unsigned int chunk_size = 2; chunk_size <= max_chunks; chunk_size++)
    unsigned int chunk_size = max_chunks;
    {
      std::vector<ReactionChunk*> current_chunks(chunk_size);
      // Seed each of the chunks
      for (unsigned chunk_color = 0; chunk_color < chunk_size; chunk_color++)
      {
        current_chunks[chunk_color] = new ReactionChunk(chunk_color, this);
        ReactionNode *chunk_start = graph->find_largest_uncolored();
        current_chunks[chunk_color]->add_reaction(chunk_start->reaction);
        graph->color_node(chunk_start, chunk_color);
      }
      bool has_adjacent = true;
      while (has_adjacent)
      {
        has_adjacent = false;
        for (unsigned chunk_color = 0; chunk_color < chunk_size; chunk_color++)
        {
          std::map<ReactionNode*,unsigned> adjacent;
          graph->find_adjacent(chunk_color, current_chunks[chunk_color]->get_species(), adjacent);
          if (adjacent.empty())
            continue;
          has_adjacent = true;
          // Pick a reaction to add to this chunk
          ReactionNode *next = NULL;
          // Select randomly with each adjacent node's probability
          // of being selected proportional to the square the weight
          // of the edge that they share.
          // Sum up the square of the weights
          unsigned total_weight = 0;
          for (std::map<ReactionNode*,unsigned>::const_iterator it = adjacent.begin();
                it != adjacent.end(); it++)
          {
            total_weight += (it->second * it->second);
          }
          unsigned chosen_index = lrand48() % total_weight;
          // Find the chosen reaction
          for (std::map<ReactionNode*,unsigned>::const_iterator it = adjacent.begin();
                it != adjacent.end(); it++)
          {
            unsigned range = it->second * it->second;
            if (chosen_index >= range)
            {
              // Subtract off this range and continue
              chosen_index -= range;
              continue;
            }
            // Otherwise we found our match
            next = it->first;
            break;
          }
          assert(next != NULL);
          current_chunks[chunk_color]->add_reaction(next->reaction);
          graph->color_node(next, chunk_color);
        }
      }
      // Handle any nodes that are still uncolored
      if (graph->has_uncolored())
      {
        std::set<ReactionNode*> uncolored;
        graph->find_uncolored(uncolored);
        // Fill in each chunk so they all have about the same number of reactions
        unsigned max_reactions = 0;
        for (std::vector<ReactionChunk*>::const_iterator it = current_chunks.begin();
              it != current_chunks.end(); it++)
        {
          if ((*it)->reactions.size() > max_reactions)
            max_reactions = (*it)->reactions.size();
        }
        bool has_under = false;
        for (std::vector<ReactionChunk*>::const_iterator it = current_chunks.begin();
              it != current_chunks.end(); it++)
        {
          if ((*it)->reactions.size() < max_reactions)
          {
            has_under = true;
            break;
          }
        }
        while (has_under && !uncolored.empty())
        {
          has_under = false;
          for (unsigned chunk_color = 0; (chunk_color < current_chunks.size()) && (!uncolored.empty()); chunk_color++)
          {
            if (current_chunks[chunk_color]->reactions.size() < max_reactions)
            {
              unsigned random_index = lrand48() % uncolored.size();
              std::set<ReactionNode*>::iterator finder = uncolored.begin();
              for (unsigned step = 0; step < random_index; step++)
                finder++;
              ReactionNode *next = *finder;
              uncolored.erase(finder);
              current_chunks[chunk_color]->add_reaction(next->reaction);
              graph->color_node(next, chunk_color);
              has_under = has_under && (current_chunks[chunk_color]->reactions.size() < max_reactions);
            }
          }
        }
        // Now that everyone is balanced, assing out reactions evenly to 
        // each chunk to keep everything balanced
        unsigned chunk_color = max_chunks-1;
        while (!uncolored.empty())
        {
          // Pick a random reaction and add it to the current chunk
          unsigned random_index = lrand48() % uncolored.size();
          std::set<ReactionNode*>::iterator finder = uncolored.begin();
          for (unsigned step = 0; step < random_index; step++)
            finder++;
          ReactionNode *next = *finder;
          uncolored.erase(finder);
          current_chunks[chunk_color]->add_reaction(next->reaction);
          graph->color_node(next, chunk_color);
          if (chunk_color == 0)
            chunk_color = max_chunks-1;
          else
            chunk_color--;
        }
        // Everybody should be assigned now
        assert(!graph->has_uncolored());
      }
      // now that we're done adding reactions we can compute the ordered species
      Slicing *current_slicing = new Slicing(this);
      for (std::vector<ReactionChunk*>::const_iterator it = current_chunks.begin();
            it != current_chunks.end(); it++)
      {
        (*it)->compute_ordered_species();
        current_slicing->add_chunk(*it);
      }
      // Now run the experiment to see how long this coloring takes
      {
        const char *temp_file_name = "xxx__temp__xxx.cc";
        {
          CodeOutStream ost(temp_file_name, true, 80);
          ost << "#include <cmath>\n";
          ost << "#include <emmintrin.h>\n";
          ost << "#include <smmintrin.h>\n";
          if (!use_sse)
            ost << "#include <immintrin.h>\n";
          //ost << "#define POINTS_PER_THREAD " << points_per_thread << "\n";
          if (hybrid_layout)
            emit_vector_hybrid_type(ost);
          if (use_atomics)
            emit_vector_atomic_operations(ost);
          current_slicing->emit_vector_getrates(ost);
          emit_vector_experiment(ost, threads_per_block, load_balance, false/*macosx*/, current_slicing);
        }
        int start_ppt, scale_ppt;
        if (use_sse)
        {
          start_ppt = 2;
          scale_ppt = 2;
        }
        else
        {
          start_ppt = 4;
          scale_ppt = 4;
        }
        float local_best_time = 1e9;
        int best_ppt = 0;
        for (int ppt = start_ppt; ppt <= 128; ppt *= scale_ppt)
        {
          char command_buffer[1024];
          snprintf(command_buffer,1023,"g++ -o xxx__experiment__xxx -O3 -DPOINTS_PER_THREAD=%d -march=native -funroll-loops -ffast-math -fassociative-math -funsafe-math-optimizations xxx__temp__xxx.cc -lpthread -lrt",ppt);
          system(command_buffer);
          // Now do the execution command
          switch (socket_config)
          {
            case 0:
              snprintf(command_buffer,1023,"numactl -C 0-5 -m 0 -- ./xxx__experiment__xxx 4 128 128 128");
              break;
            case 1:
              snprintf(command_buffer,1023,"numactl -C 0-5,12-17 -m 0 -- ./xxx__experiment__xxx 4 128 128 128");
              break;
            case 2:
              snprintf(command_buffer,1023,"numactl -C 0-11 -- ./xxx__experiment__xxx 4 128 128 128");
              break;
            case 3:
              snprintf(command_buffer,1023,"numactl -C 0-23 -- ./xxx__experiment__xxx 4 128 128 128");
              break;
            default:
              assert(false);
          }
          FILE *execution_results = popen(command_buffer, "r");
          assert(execution_results != NULL);
          float experiment_time = 1e9;
          assert(fscanf(execution_results,"%f", &experiment_time) > 0);
          assert(pclose(execution_results) == 0);
          fprintf(stdout,"Chunks %2d   Threads %2d   PPT %3d   Current time %12.3f   Best time %12.3f\n", chunk_size, threads_per_block, ppt, experiment_time, best_time);
          if (experiment_time < local_best_time)
          {
            local_best_time = experiment_time;
            best_ppt = ppt;
          }
        }
        system("rm -f xxx__temp__xxx.* xxx__experiment__xxx");
        // check to see how we compared
        if (local_best_time < best_time)
        {
          fprintf(stdout,"FOUND NEW BEST CPU CONFIGURATION!!!!!!!!!!!!\n");
          best_time = local_best_time;
          if (best_slicing != NULL)
            delete best_slicing;
          best_slicing = current_slicing;
          char out_file[128];
          snprintf(out_file,127,"best_slicing_%d.cfg", best_ppt);
          best_slicing->dump_slicing(out_file);
        }
        else
        {
          delete current_slicing;
          current_slicing = NULL;
        }
      }
      graph->clean_graph();
    }
  }
  delete graph;
}

bool ReactionChunk::compiles_to_registers(void)
{
  const char *temp_file_name = "xxx__temp__xxx.cu";
  {
    CodeOutStream ost(temp_file_name, true, 80);
    ost << "#define POINTS_PER_THREAD " << unit->points_per_thread << "\n";
    ost << "#define THREADS_PER_BLOCK " << unit->threads_per_block << "\n";
    emit_gpu_getrates(ost, true/*emit header*/);
  }
  // file is now close so compile it and get the results
  FILE *compilation_results = popen("nvcc -c -arch=compute_35 -code=sm_35 -Xptxas \"-v -abi=no\" xxx__temp__xxx.cu 2>&1 | ./kernel_info.py", "r");
  assert(compilation_results != NULL);
  int chunk_id=-1, num_regs=-1, amnt_lmem=-1;
#if 0
  char buffer[128];
  while (fgets(buffer,128,compilation_results) != NULL)
    fprintf(stdout,"%s",buffer);
#endif
  assert(fscanf(compilation_results, "%d %d %d", &chunk_id, &num_regs, &amnt_lmem) > 0);
  assert(chunk_id == int(idx));
  assert(pclose(compilation_results) == 0);
  //fprintf(stdout,"Chunk %d compiled to %d registers with %d bytes of local memory\n", chunk_id, num_regs, amnt_lmem);
  // Delete the file we created
  system("rm -f xxx__temp__xxx.*");
  return (amnt_lmem == 0);
}

// Implementation of ReactionGraph

ReactionGraph::ReactionGraph(const SpeciesSet &species, const ReactionVec &reactions)
{
  // Make all the nodes first 
  for (ReactionVec::const_iterator it = reactions.begin();
        it != reactions.end(); it++)
  {
    // Skip any reactions that are duplicates
    if ((*it)->is_duplicate() && (*it)->is_owned())
      continue;
    ReactionNode *node = new ReactionNode(*it); 
    nodes.push_back(node);
  }
  // For each species, get the set of reactions
  // that touch that species.  Add edges
  for (SpeciesSet::const_iterator spec_it = species.begin();
        spec_it != species.end(); spec_it++)
  {
    // Skip the third body species
    if (strcmp((*spec_it)->name,"M") == 0)
      continue;
    std::vector<ReactionNode*> matches;
    for (std::vector<ReactionNode*>::const_iterator reac_it = nodes.begin();
          reac_it != nodes.end(); reac_it++)
    {
      if ((*reac_it)->has_species(*spec_it))
        matches.push_back(*reac_it);
    }
    for (unsigned idx1 = 0; idx1 < matches.size(); idx1++)
    {
      ReactionNode *node1 = matches[idx1];
      for (unsigned idx2 = idx1+1; idx2 < matches.size(); idx2++)
      {
        ReactionNode *node2 = matches[idx2];
        SpeciesEdge *edge = node1->find_edge(node2);
        if (edge == NULL)
        {
          // Edge doesn't exist yet
          edge = new SpeciesEdge(node1, node2, *spec_it);
          edges.push_back(edge);
          node1->add_edge(edge);
          node2->add_edge(edge);
        }
        else
        {
          // Already exists
          edge->add_weight(*spec_it);
        }
      }
    }
  }
}

ReactionGraph::~ReactionGraph(void)
{
  for (std::vector<ReactionNode*>::iterator it = nodes.begin();
        it != nodes.end(); it++)
  {
    delete *it;
  }
  for (std::vector<SpeciesEdge*>::iterator it = edges.begin();
        it != edges.end(); it++)
  {
    delete *it;
  }
}

void ReactionGraph::color_node(ReactionNode *node, unsigned color)
{
  // should be uncolored right now
  assert(node->color == -1);
  node->color = int(color);
}

ReactionNode* ReactionGraph::find_largest_uncolored(void)
{
  unsigned most_species = 0;
  std::vector<ReactionNode*> largest;
  for (std::vector<ReactionNode*>::const_iterator it = nodes.begin();
        it != nodes.end(); it++)
  {
    if ((*it)->color == -1)
    {
      unsigned num_species = (*it)->get_num_species();
      if (num_species > most_species)
      {
        largest.clear();
        largest.push_back(*it);
        most_species = num_species;
      }
      else if (num_species == most_species)
      {
        largest.push_back(*it);
      }
    }
  }
  if (!largest.empty())
  {
    if (largest.size() == 1)
      return largest.front();
    else
    {
      unsigned index = lrand48() % largest.size();
      return largest[index];
    }
  }
  return NULL;
}

void ReactionGraph::find_adjacent(unsigned color, const std::set<Species*> &color_set, std::map<ReactionNode*,unsigned> &adjacent)
{
  // don't want to accidentally reuse nodes
  assert(adjacent.empty());
  for (std::vector<ReactionNode*>::const_iterator node_it = nodes.begin();
        node_it != nodes.end(); node_it++)
  {
    // Look at nodes that have this color
    if ((*node_it)->color == int(color))
    {
      // for each edge of this node, go over all the outgoing
      // edges and find nodes that are uncolored
      for (std::vector<SpeciesEdge*>::const_iterator edge_it = (*node_it)->edges.begin();
            edge_it != (*node_it)->edges.end(); edge_it++)
      {
        ReactionNode *other = NULL;
        if ((*edge_it)->left == *node_it)
        {
          other = (*edge_it)->right;
        }
        else
        {
          assert((*edge_it)->right == *node_it);
          other = (*edge_it)->left;
        } 
        // See if the other node is uncolored
        if (other->color == -1)
        {
          // See how many species they have in common
          std::vector<Species*> overlap(color_set.size());
          const std::set<Species*> &other_species = other->reaction->get_species();
          std::vector<Species*>::iterator end_it = std::set_intersection(
                                color_set.begin(),color_set.end(),
                                other_species.begin(),other_species.end(),
                                overlap.begin());
          overlap.resize(end_it-overlap.begin());
          // should be greater than 0 since we had at least one edge
          assert(!overlap.empty());
          adjacent[other] = overlap.size();
        }
      }
    }
  }
}

void ReactionGraph::clean_graph(void)
{
  for (std::vector<ReactionNode*>::const_iterator it = nodes.begin();
        it != nodes.end(); it++)
  {
    (*it)->color = -1;
  }
}

bool ReactionGraph::has_uncolored(void) const
{
  for (std::vector<ReactionNode*>::const_iterator it = nodes.begin();
        it != nodes.end(); it++)
  {
    if ((*it)->color == -1)
      return true;
  }
  return false;
}

void ReactionGraph::find_uncolored(std::set<ReactionNode*> &uncolored)
{
  assert(uncolored.empty());
  for (std::vector<ReactionNode*>::const_iterator it = nodes.begin();
        it != nodes.end(); it++)
  {
    if ((*it)->color == -1)
      uncolored.insert(*it);
  }
}

// Implementation of ReactionNode

ReactionNode::ReactionNode(Reaction *r)
  : reaction(r), color(-1)
{
}

void ReactionNode::add_edge(SpeciesEdge *edge)
{
  assert((edge->left == this) || (edge->right == this));
  edges.push_back(edge);
}

bool ReactionNode::has_species(Species *spec) const
{
  const SpeciesSet &species = reaction->get_species();
  return (species.find(spec) != species.end());
}

unsigned ReactionNode::get_num_species(void) const
{
  return (reaction->get_species().size());
}

SpeciesEdge* ReactionNode::find_edge(ReactionNode *node) const
{
  for (std::vector<SpeciesEdge*>::const_iterator it = edges.begin();
        it != edges.end(); it++)
  {
    assert(((*it)->left == this) || ((*it)->right == this));
    if (((*it)->left == this) && ((*it)->right == node))
      return *it;
    if (((*it)->right == this) && ((*it)->left == node))
      return *it;
  }
  return NULL;
}

// Implementation of SpeciesEdge

SpeciesEdge::SpeciesEdge(ReactionNode *l, ReactionNode *r, Species *init)
  : left(l) , right(r)
{
  assert(left != NULL);
  assert(right != NULL);
  assert(init != NULL);
  weights.insert(init);
}

void SpeciesEdge::add_weight(Species *spec)
{
  // Note this automatically handles duplicates
  weights.insert(spec);
}

const std::set<Species*>& SpeciesEdge::get_species(void) const
{
  return weights;
}

unsigned SpeciesEdge::get_weight(void) const
{
  return weights.size();
}

// Implementation of Slicing

unsigned Slicing::compute_cost(void) const
{
  // Go through each of the chunks and count the number of species
  // that will need to be loaded for that chunk 
  // + 1 for temperature of each chunk
  unsigned cost = chunks.size(); // cost of loading temperatures
  for (std::vector<ReactionChunk*>::const_iterator it = chunks.begin();
        it != chunks.end(); it++)
  {
    cost += (*it)->get_total_species_count(); 
  }
  return cost;
}

// EOF

