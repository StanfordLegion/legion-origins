
#include <cstdio>
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <cmath>

#include <set>

#include "s3dgen_ast.h"
#include "s3dgen_parse.h"

// Kind of ugly to include source files direclty, but whatever
#include "lex.s3dgen.c"
#include "s3dgen.tab.c"

// 80 characters + 1 for the end of line string
#define MAX_LINE_LENGTH 81

#define PI 3.14159265359

static inline char* capitalize(char *name)
{
  int length = strlen(name);
  for (int i = 0; i < length; i++)
  {
    if ((name[i] >= 'a') && (name[i] <= 'z'))
      name[i] = char(int(name[i])-32);
  }
  return name;
}

TranslationUnit* parse_source_file(const char *file_name)
{
  FILE *source = fopen(file_name, "r");
  if (source == NULL)
  {
    fprintf(stderr,"Bad file descriptor for input file.  File %s does not exist.\n", file_name);
    exit(1);
  }
  YY_BUFFER_STATE buffer = s3dgen_create_buffer(source, YY_BUF_SIZE);
  s3dgen_switch_to_buffer(buffer);

  assert(unit == NULL);
  unit = new TranslationUnit();
  assert(unit != NULL);
  s3dgenparse();

  s3dgen_delete_buffer(buffer);

  assert(fclose(source) == 0);

  return unit;
}

static void read_line(char target[MAX_LINE_LENGTH], FILE *source)
{
  unsigned index = 0; 
  char c = getc(source);
  while ((c != '\n') && (c != EOF))
  {
    if (index < (MAX_LINE_LENGTH-1))
      target[index++] = c;
    c = getc(source);
  }
  assert(index < MAX_LINE_LENGTH);
  target[index] = '\0';
}

void parse_therm_file(TranslationUnit *unit, const char *therm_file)
{
  // Parse the thermodynamic part of the input file based on the CHEMKIN manual 
  FILE *source = fopen(therm_file, "r");
  if (source == NULL)
  {
    fprintf(stderr,"Bad file descriptor for thermodynamics file.  File %s does not exist.\n", therm_file);
    exit(1);
  }
  // First scan through the file looking for the start of the thermodynamics
  char buffer[MAX_LINE_LENGTH];
  read_line(buffer, source);
  capitalize(buffer);
  while (strncmp(buffer, "THERMO", 5) != 0)
  {
    read_line(buffer, source);
    capitalize(buffer);
  }
  // Read the second line
  read_line(buffer, source);
  // Skip commented lines
  while (buffer[0] == '!')
  {
    read_line(buffer, source);
  }
  double lowest, common, highest;
  assert(sscanf(buffer, "%lf %lf %lf", &lowest, &common, &highest) == 3);

  read_line(buffer, source);
  while ((buffer[0] == '!') || (buffer[0] == ' ') 
            || (buffer[0] == '\t') || (buffer[0] == '\0'))
    read_line(buffer, source);
  // Go until we've got no more species
  while ((strncmp(buffer,"END",3) != 0) && (strncmp(buffer,"end",3) != 0))
  {
    do {
      assert(strlen(buffer) == 80);
      // Read from right so we can gradually filter things out
      assert(buffer[79] == '1');
      buffer[73] = '\0';
      double temp_low, temp_high, temp_common;
      sscanf(&(buffer[45]), "%lf %lf %lf", &temp_low, &temp_high, &temp_common);
      // Mark the species name
      Species *species = NULL;
      {
        char spec_name[32];
        int num_read = sscanf(buffer,"%s",spec_name);
        assert(num_read == 1);
        species = unit->find_species(spec_name, false/*must*/);
      }
      if (species == NULL)
      {
        // This is a species which isn't listed in the reactions
        // so just read its next 3 lines and then end
        for (int i = 0; i < 3; i++)
        {
          read_line(buffer, source);
          while (buffer[0] == '!')
            read_line(buffer, source);
        }
        break; // break out of the do-while loop
      }
      // Compute the specie's atomic mass
      for (int i = 0; i < 4; i++)
      {
        char element[8];
        for (int j = 0; j < 5; j++)
          element[j] = buffer[24+i*5+j];
        element[5] = '\0';
        char element_str[8];
        int element_cnt;
        int num_read = sscanf(element,"%s %d",element_str,&element_cnt);
        if (num_read < 2)
          continue;
        species->update_mass(element_str, element_cnt);
      }

      // Start line 2
      // Read the next line
      read_line(buffer, source);
      while (buffer[0] == '!')
        read_line(buffer, source);
      assert(strlen(buffer) == 80);
      assert(buffer[79] == '2');
      double *coefficients = (double*)malloc(14*sizeof(double));
      // Read from the right
      buffer[75] = '\0';
      sscanf(&(buffer[60]), "%lf", &(coefficients[4]));
      buffer[60] = '\0';
      sscanf(&(buffer[45]), "%lf", &(coefficients[3]));
      buffer[45] = '\0';
      sscanf(&(buffer[30]), "%lf", &(coefficients[2]));
      buffer[30] = '\0';
      sscanf(&(buffer[15]), "%lf", &(coefficients[1]));
      buffer[15] = '\0';
      sscanf(buffer, "%lf", &(coefficients[0]));

      // Start line 3
      read_line(buffer, source);
      while (buffer[0] == '!')
        read_line(buffer, source);
      assert(strlen(buffer) == 80);
      assert(buffer[79] == '3');
      // Read from the right
      buffer[75] = '\0';
      sscanf(&(buffer[60]), "%lf", &(coefficients[9]));
      buffer[60] = '\0';
      sscanf(&(buffer[45]), "%lf", &(coefficients[8]));
      buffer[45] = '\0';
      sscanf(&(buffer[30]), "%lf", &(coefficients[7]));
      buffer[30] = '\0';
      sscanf(&(buffer[15]), "%lf", &(coefficients[6]));
      buffer[15] = '\0';
      sscanf(buffer, "%lf", &(coefficients[5]));

      // Start line 4
      read_line(buffer, source);
      while (buffer[0] == '!')
        read_line(buffer, source);
      assert(strlen(buffer) == 80);
      assert(buffer[79] == '4');
      buffer[60] = '\0';
      sscanf(&(buffer[45]), "%lf", &(coefficients[13]));
      buffer[45] = '\0';
      sscanf(&(buffer[30]), "%lf", &(coefficients[12]));
      buffer[30] = '\0';
      sscanf(&(buffer[15]), "%lf", &(coefficients[11]));
      buffer[15] = '\0';
      sscanf(buffer, "%lf", &(coefficients[10]));

      // Set the species coefficients
      if (species != NULL)
        species->set_coefficients(temp_low, temp_high, temp_common, coefficients);
      else
        free(coefficients);
    } while (0);

    // Start reading the next line
    // Skip commented lines or lines that start with whitespace
    read_line(buffer, source);
    while ((buffer[0] == '!') || (buffer[0] == ' ') 
            || (buffer[0] == '\t') || (buffer[0] == '\0'))
      read_line(buffer, source);
  }
  assert(fclose(source) == 0);
  // Make sure that all the species have coefficients set
  unit->check_species_coefficients();
  // Make sure that all the species have their masses set
  unit->check_species_masses();
}

static void parse_array(FILE *source, double *&target, int num_doubles)
{
  assert(num_doubles > 0);
  target = (double*)malloc(num_doubles*sizeof(double));
  assert(target != NULL);
  for (int i = 0; i < num_doubles; i++)
  {
    assert(fscanf(source,"%lf",&(target[i])) > 0); 
  }
}

static void parse_int_array(FILE *source, int *&target, int num_ints)
{
  assert(num_ints > 0);
  target = (int*)malloc(num_ints*sizeof(int));
  assert(target != NULL);
  for (int i = 0; i < num_ints; i++)
  {
    assert(fscanf(source,"%d",&(target[i])) > 0);
  }
}

TranLib* parse_tran_file(TranslationUnit *unit, const char *tran_file)
{
  double version, preversion;
  char buffer[64];
  int li, lr, order, num_species, nlite;
  double patmos;
  FILE *source = fopen(tran_file, "r");
  if (source == NULL)
  {
    fprintf(stderr,"Bad file descriptor for thermodynamics file.  File %s does not exist.\n", tran_file);
    exit(1);
  } 
  assert(fscanf(source,"%lf",&version) > 0);
  assert(fscanf(source,"%lf",&preversion) > 0);
  assert(fscanf(source,"%s",buffer) > 0);
  assert(fscanf(source,"%s",buffer) > 0);
  assert(fscanf(source,"%d %d %d %d %d",&li, &lr, &order, &num_species, 
                                                  &nlite) > 0);
  assert(fscanf(source,"%lf",&patmos) > 0);

  if (num_species != int(unit->get_num_species()))
  {
    fprintf(stderr,"Species count mismatch when parsing tran file %s (%ld != %d). "
                    "Are you sure you are correctly matching mechanisms?\n", tran_file,
                    unit->get_num_species(), num_species);
    exit(1);
  }

  TranLib *result = new TranLib(order, num_species, nlite, patmos, unit);

  parse_array(source, result->nwt, result->num_species);
  parse_array(source, result->neps, result->num_species);
  parse_array(source, result->nsig, result->num_species);
  parse_array(source, result->ndip, result->num_species);
  parse_array(source, result->npol, result->num_species);
  parse_array(source, result->nzrot, result->num_species);
  parse_int_array(source, result->inlin, result->num_species);
  parse_array(source, result->nlam, result->num_species*result->order);
  parse_array(source, result->neta, result->num_species*result->order);
  parse_array(source, result->ndif, result->num_species*result->num_species*result->order);
  parse_int_array(source, result->iktdif, result->nlite);
  parse_array(source, result->ntdif, result->num_species*result->order*result->nlite);

  assert(fclose(source) == 0);
  return result;
}

extern "C" int dpolft(int*, double*, double*, double*, int*, int*, double*, double*, int*, double*);
extern "C" int dpcoef(int*, double*, double*, double*);

static double compute_ckcvml(Species *spec, const double temperature)
{
  const double &tk1 = temperature;
  const double tk2 = tk1*tk1;
  const double tk3 = tk1*tk2;
  const double tk4 = tk1*tk3;
  double result = 0.0;
  // Note we want the original coefficients
  // here since we're not doing any
  // non dimensionalization
  if (temperature > spec->common_temp)
  {
    // high coefficients
    result += spec->get_high_coefficient(0,true);
    result += (spec->get_high_coefficient(1,true) * tk1);
    result += (spec->get_high_coefficient(2,true) * tk2);
    result += (spec->get_high_coefficient(3,true) * tk3);
    result += (spec->get_high_coefficient(4,true) * tk4);
  }
  else
  {
    // low coefficients
    result += spec->get_low_coefficient(0,true);
    result += (spec->get_low_coefficient(1,true) * tk1);
    result += (spec->get_low_coefficient(2,true) * tk2);
    result += (spec->get_low_coefficient(3,true) * tk3);
    result += (spec->get_low_coefficient(4,true) * tk4);
  }
  // specific heats should always be positive
  assert(result > 1.0);
  return (8.314510e+07*(result - 1.0));
}

static double parker(const double t, const double eps)
{
  static const double PI32O2 = 2.7841639984158539e+00;
  static const double P2O4P2 = 4.4674011002723397e+00;
  static const double PI32   = 5.5683279968317078e+00;

  const double d = eps/t;
  const double dr = eps/298.0;
  return ((1.0 + PI32O2*sqrt(dr) + P2O4P2*dr + PI32*pow(dr,1.5)) /
          (1.0 + PI32O2*sqrt(d)  + P2O4P2*d  + PI32*pow(d,1.5)));
}

static double qinterp(const double arg, const double x[3], const double y[3])
{
  double val1 = y[0] + (arg-x[0]) * (y[1]-y[0]) / (x[1]-x[0]);
  double val2 = y[1] + (arg-x[1]) * (y[2]-y[1]) / (x[2]-x[1]);
  double fac1 = (arg-x[0]) / (x[1]-x[0]) / 2.0;
  double fac2 = (x[2]-arg) / (x[2]-x[1]) / 2.0;
  if (arg >= x[1])
    return ((val1*fac2+val2) / (1.0+fac2));
  else
    return ((val1+val2*fac1) / (1.0+fac1));
}

static double omeg12(int n, double tr, double dr)
{
  static const double tstern[37] = {.1,.2,.3,.4,.5,.6,.7,.8,.9,1.,1.2,1.4,1.6,1.8,2.,2.5,
                             3.,3.5,4.,5.,6.,7.,8.,9.,10.,12.,14.,16.,18.,20.,25.,
                             30.,35.,40.,50.,75.,100.};
  static const double delta[8] = { 0.,.25,.5,.75,1.,1.5,2.,2.5 };
  static const double oarray[37][8] = { 
      { 4.008 , 4.002 , 4.655 , 5.52  , 6.454 , 8.214 , 9.824 ,11.31 },
      { 3.130 , 3.164 , 3.355 , 3.721 , 4.198 , 5.23  , 6.225 , 7.160 },
      { 2.649 , 2.657 , 2.77  , 3.002 , 3.319 , 4.054 , 4.785 , 5.483 },
      { 2.314 , 2.32  , 2.402 , 2.572 , 2.812 , 3.386 , 3.972 , 4.539 },
      { 2.066 , 2.073 , 2.14  , 2.278 , 2.472 , 2.946 , 3.437 , 3.918 },
      { 1.877 , 1.885 , 1.944 , 2.06  , 2.225 , 2.628 , 3.054 , 3.747 },
      { 1.729 , 1.738 , 1.79  , 1.893 , 2.036 , 2.388 , 2.763 , 3.137 },
      { 1.6122, 1.622 , 1.67  , 1.76  , 1.886 , 2.198 , 2.535 , 2.872 },
      { 1.517 , 1.527 , 1.572 , 1.653 , 1.765 , 2.044 , 2.35  , 2.657 },
      { 1.44  , 1.45  , 1.49  , 1.564 , 1.665 , 1.917 , 2.196 , 2.4780 },
      { 1.3204, 1.33  , 1.364 , 1.425 , 1.51  , 1.72  , 1.956 , 2.199 },
      { 1.234 , 1.24  , 1.272 , 1.324 , 1.394 , 1.573 , 1.777 , 1.99 },
      { 1.168 , 1.176 , 1.202 , 1.246 , 1.306 , 1.46  , 1.64  , 1.827 },
      { 1.1166, 1.124 , 1.146 , 1.185 , 1.237 , 1.372 , 1.53  , 1.7 },
      { 1.075 , 1.082 , 1.102 , 1.135 , 1.181 , 1.3   , 1.441 , 1.592 },
      { 1.0006, 1.005 , 1.02  , 1.046 , 1.08  , 1.17  , 1.278 , 1.397 },
      { .95  ,  .9538,  .9656,  .9852, 1.012 , 1.082 , 1.168 , 1.265 },
      { .9131,  .9162,  .9256,  .9413,  .9626, 1.019 , 1.09  , 1.17  },
      { .8845,  .8871,  .8948,  .9076,  .9252,  .972 , 1.03  , 1.098 },
      { .8428,  .8446,  .850 ,  .859 ,  .8716,  .9053,  .9483,  .9984 },
      { .813 ,  .8142,  .8183,  .825 ,  .8344,  .8598,  .8927,  .9316 },
      { .7898,  .791 ,  .794 ,  .7993,  .8066,  .8265,  .8526,  .8836 },
      { .7711,  .772 ,  .7745,  .7788,  .7846,  .8007,  .822 ,  .8474 },
      { .7555,  .7562,  .7584,  .7619,  .7667,  .78  ,  .7976,  .8189 },
      { .7422,  .743 ,  .7446,  .7475,  .7515,  .7627,  .7776,  .796 },
      { .72022, .7206,  .722 ,  .7241,  .7271,  .7354,  .7464,  .76  },
      { .7025,  .703 ,  .704 ,  .7055,  .7078,  .7142,  .7228,  .7334 },
      { .68776, .688,   .6888,  .6901,  .6919,  .697 ,  .704 ,  .7125 },
      { .6751,  .6753,  .676 ,  .677 ,  .6785,  .6827,  .6884,  .6955 },
      { .664 ,  .6642,  .6648,  .6657,  .6669,  .6704,  .6752,  .681 },
      { .6414,  .6415,  .6418,  .6425,  .6433,  .6457,  .649 ,  .653 },
      { .6235,  .6236,  .6239,  .6243,  .6249,  .6267,  .629 ,  .632 },
      { .60882, .6089,  .6091,  .6094,  .61  ,  .6112,  .613 ,  .6154 },
      { .5964,  .5964,  .5966,  .597 ,  .5972,  .5983,  .600 ,  .6017 },
      { .5763,  .5763,  .5764,  .5766,  .5768,  .5775,  .5785,  .58  },
      { .5415,  .5415,  .5416,  .5416,  .5418,  .542 ,  .5424,  .543 },
      { .518 ,  .518 ,  .5182,  .5184,  .5184,  .5185,  .5186,  .5187 } };
  static const double parray[37][8] = {
      { 4.1   , 4.266 , 4.833 , 5.742 , 6.729 , 8.624 ,10.34  ,11.890 },
      { 3.263 , 3.305 , 3.516 , 3.914 , 4.433 , 5.57  , 6.637 , 7.618 },
      { 2.84  , 2.836 , 2.936 , 3.168 , 3.511 , 4.329 , 5.126 , 5.874 },
      { 2.531 , 2.522 , 2.586 , 2.749 , 3.004 , 3.64  , 4.282 , 4.895 },
      { 2.284 , 2.277 , 2.329 , 2.46  , 2.665 , 3.187 , 3.727 , 4.249 },
      { 2.084 , 2.081 , 2.13  , 2.243 , 2.417 , 2.862 , 3.329 , 3.786 },
      { 1.922 , 1.924 , 1.97  , 2.072 , 2.225 , 2.641 , 3.028 , 3.435 },
      { 1.7902, 1.795 , 1.84  , 1.934 , 2.07  , 2.417 , 2.788 , 3.156 },
      { 1.682 , 1.689 , 1.733 , 1.82  , 1.944 , 2.258 , 2.596 , 2.933 },
      { 1.593 , 1.60  , 1.644 , 1.725 , 1.84  , 2.124 , 2.435 , 2.746 },
      { 1.455 , 1.465 , 1.504 , 1.574 , 1.67  , 1.913 , 2.181 , 2.45  },
      { 1.355 , 1.365 , 1.4   , 1.461 , 1.544 , 1.754 , 1.989 , 2.228 },
      { 1.28  , 1.289 , 1.321 , 1.374 , 1.447 , 1.63  , 1.838 , 2.053 },
      { 1.222 , 1.231 , 1.26  , 1.306 , 1.37  , 1.532 , 1.718 , 1.912 },
      { 1.176 , 1.184 , 1.209 , 1.25  , 1.307 , 1.45  , 1.618 , 1.795 },
      { 1.0933, 1.1   , 1.119 , 1.15  , 1.193 , 1.304 , 1.435 , 1.578 },
      { 1.039 , 1.044 , 1.06  , 1.083 , 1.117 , 1.204 , 1.31  , 1.428 },
      { .9996, 1.004 , 1.016 , 1.035 , 1.062 , 1.133 , 1.22  , 1.32  },
      { .9699,  .9732,  .983 ,  .9991, 1.021 , 1.08  , 1.153 , 1.236 },
      { .9268,  .9291,  .936 ,  .9473,  .9628, 1.005 , 1.058 , 1.12 },
      { .8962,  .8979,  .903 ,  .9114,  .923 ,  .9545,  .9955, 1.044 },
      { .8727,  .8741,  .878 ,  .8845,  .8935,  .918 ,  .9505,  .9893 },
      { .8538,  .8549,  .858 ,  .8632,  .8703,  .890 ,  .9164,  .9482 },
      { .8379,  .8388,  .8414,  .8456,  .8515,  .868 ,  .8895,  .916 },
      { .8243,  .8251,  .8273,  .8308,  .8356,  .8493,  .8676,  .89  },
      { .8018,  .8024,  .8039,  .8065,  .810 ,  .820 ,  .8337,  .8504 },
      { .7836,  .784 ,  .7852,  .7872,  .7899,  .7976,  .808 ,  .8212 },
      { .7683,  .7687,  .7696,  .771 ,  .7733,  .7794,  .788 ,  .7983 },
      { .7552,  .7554,  .7562,  .7575,  .7592,  .764 ,  .771 ,  .7797 },
      { .7436,  .7438,  .7445,  .7455,  .747 ,  .7512,  .757 ,  .7642 },
      { .71982, .72  ,  .7204,  .7211,  .7221,  .725 ,  .7289,  .7339 },
      { .701 ,  .7011,  .7014,  .702 ,  .7026,  .7047,  .7076,  .7112 },
      { .68545, .6855,  .686 ,  .686 ,  .6867,  .6883,  .6905,  .693 },
      { .6723,  .6724,  .6726,  .673 ,  .6733,  .6745,  .676 ,  .6784 },
      { .651 ,  .651 ,  .6512,  .6513,  .6516,  .6524,  .6534,  .6546 },
      { .614 ,  .614 ,  .6143,  .6145,  .6147,  .6148,  .6148,  .6147 },
      { .5887,  .5889,  .5894,  .59  ,  .5903,  .5901,  .5895,  .5885 } };
  if ((dr < -0.00001) || (dr > 2.5) || (tr < 0.09) || (tr > 500) ||
      ((fabs(dr) > 1e-5) && (tr > 75.0)))
    assert(false); // collision integral undefined
  if (tr > 75.0)
  {
    if (n == 1)
      return (0.623 - 0.136e-2*tr + 0.346e-5*tr*tr - 0.343e-8*tr*tr*tr);
    else if (n == 2)
      return (0.703 - 0.146e-2*tr + 0.357E-5*tr*tr - 0.343e-8*tr*tr*tr);
    else
    {
      assert(false);
      return 0.0;
    }
  }
  int ii;
  if (tr <= 0.2)
    ii = 1;
  else
  {
    ii = 36;
    for (int i = 1; i <= 35; i++)
    {
      if ((tstern[i-1] < tr) && (tstern[i] >= tr))
        ii = i;
    }
  }
  double arg[3];
  double val[3];
  if (fabs(dr) < 1.0e-5)
  {
    for (int i = 0; i < 3; i++)
    {
      arg[i] = tstern[ii-1+i];
      if (n == 1)
        val[i] = oarray[ii-1+i][0];
      else if (n == 2)
        val[i] = parray[ii-1+i][0];
      else
        assert(false); // should never happen
    }
    return qinterp(tr, arg, val);
  }
  int kk;
  if (dr <= 0.25)
    kk = 1;
  else
  {
    kk = 6;
    for (int k = 1; k <= 6; k++)
    {
      if ((delta[k-1] < dr) && (delta[k] >= dr))
        kk = k;
    }
  }
  // Should never be 36 here, or we could read off the end of the array
  assert(ii < 36);
  double vert[3];
  for (int i = 0; i < 3; i++)
  {
    for (int k = 0; k < 3; k++)
    {
      arg[k] = delta[kk-1+k];
      if (n == 1)
        val[k] = oarray[ii-1+i][kk-1+k];
      else if (n == 2)
        val[i] = parray[ii-1+i][kk-1+k];
      else
        assert(false);
    }
    vert[i] = qinterp(dr, arg, val);
  }
  for (int i = 0; i < 3; i++)
    arg[i] = tstern[ii-1+i];
  return qinterp(tr, arg, vert);
}

static double d12(const double w1, const double w2, const double t,
                  const double eps1, const double eps2,
                  const double sig1, const double sig2, const double dst)
{
  const double sumw = w1 + w2;
  const double sig12 = 0.5 * (sig1 + sig2);
  const double tr11 = t / eps1;
  const double tr22 = t / eps2;
  const double tr12 = t / sqrt(eps1*eps2);

  const double om2f11 = omeg12(2, tr11, dst);
  const double om2f22 = omeg12(2, tr22, dst);
  const double om1f12 = omeg12(1, tr12, dst);
  const double om2f12 = omeg12(2, tr12, dst);

  double bst12, cst12;
  if (tr12 <= 5.0)
  {
    bst12 = 1.36 - 0.223*tr12 + 0.0613*(tr12*tr12) -0.00554*(tr12*tr12*tr12);
    cst12 = 0.823 + 0.0128*tr12 + 0.0112*(tr12*tr12) -0.00193*(tr12*tr12*tr12);
  }
  else
  {
    bst12 = 1.095;
    cst12 = 0.9483;
  }
  const double ast12 = om2f12 / om1f12;

  const double h1 = (sig1/sig2)*(sig1/sig2) * sqrt(2.0*w2/sumw) * 2.0 * (w1 * w1)/(sumw * w2);
  const double p1 = h1 * om2f11/om1f12;

  const double h2 = (sig2/sig1)*(sig2/sig1) * sqrt(2.0*w1/sumw) * 2.0 * (w2 * w2)/(sumw * w1);
  const double p2 = h2 * om2f22/om1f12;

  const double p12 = 15.0 * ((w1 - w2)/sumw)*((w1 - w2)/sumw) + 8.0*w1*w2*ast12/(sumw*sumw);

  const double hq1 = 2.0/(w2 * sumw) * sqrt(2.0*w2/sumw) * om2f11/om1f12 * (sig1/sig12) * (sig1/sig12);
  const double q1 = ((2.5-1.2*bst12)*(w1*w1) + 3.0*(w2*w2) + 1.6*w1*w2*ast12) * hq1;

  const double hq2 = 2.0/(w1 * sumw) * sqrt(2.0*w1/sumw) * om2f22/om1f12 * (sig2/sig12) * (sig2/sig12);
  const double q2 = ((2.5-1.2*bst12)*(w2*w2) + 3.0*(w1*w1) + 1.6*w1*w2*ast12) * hq2;

  const double h12 = ((w1-w2)/sumw)*((w1-w2)/sumw) * (2.5-1.2*bst12)*15.0 +
                      4.0*w1*w2*ast12/(sumw*sumw) * (11.0 - 2.4*bst12);
  const double q12 = h12 + 1.6*sumw*om2f11*om2f22 / (sqrt(w1*w2)*om1f12*om1f12) *
                      ((sig1 * sig1)*(sig2 * sig2)) / (sig12 * sig12 * sig12 * sig12);

  return (((6.0*cst12-5.0)*(6.0*cst12-5.0)/10.0) * (p1 + p2 + p12) / (q1 + q2 + q12) + 1.0);
}

static double horner(int n, const double a[7], const double x)
{
  double b = a[n-1];
  for (int i = 2; i <= n; i++)
    b = a[n-i] + b*x;
  return b;
}

TranLib* parse_tran_dat_file(TranslationUnit *unit, const char *tran_file)
{
  // In this case we're parsing the actual tran.dat file, so we need to read everything
  // in and then compute the coefficients that we care about
  int nlite = 0;    
  // Count how many light species we have
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if ((*it)->molecular_mass > 5.0)
      continue;
    nlite++;
  }
  const double patmos = 1.01325e6; 
  const double ru = 8.314510e+07;
  TranLib *result = new TranLib(4/*order*/, unit->species.size()-1/*ignore third body species*/,
                                nlite, patmos, unit);
  result->nwt = (double*)malloc(result->num_species*sizeof(double));
  result->neps = (double*)malloc(result->num_species*sizeof(double));
  result->nsig = (double*)malloc(result->num_species*sizeof(double));
  result->ndip = (double*)malloc(result->num_species*sizeof(double));
  result->npol = (double*)malloc(result->num_species*sizeof(double));
  result->nzrot = (double*)malloc(result->num_species*sizeof(double));
  result->inlin = (int*)malloc(result->num_species*sizeof(int));
  result->iktdif = (int*)malloc(result->nlite*sizeof(int));

  // There should be exactly the number of species that we expect
  FILE *source = fopen(tran_file, "r");
  char buffer[1024];
  int buffer_idx;
  char c;
  int nlite_idx = 0;
  std::set<unsigned> found_specs;
  while (feof(source) == 0)
  {
    // Read in the whole line 
    buffer_idx = 0;
    do {
      c = getc(source);
      if (c == EOF) break;
      assert(buffer_idx < 1024);
      buffer[buffer_idx++] = c;
    } while ((c != '\n') && (c != EOF));
    if (buffer_idx == 0)
      break;
    assert(buffer_idx < 1024);
    buffer[buffer_idx] = '\0';
    // Skip any commented lines
    if (buffer[0] == '!')
      continue;
    // If we're done, then exit
    if (strncmp(buffer,"END",3) == 0)
      break;
    // Now read the out the information for the species
    char spec_name[256];
    int lin;
    double eps, sig, dip, pol, zrot;
    int num_match = sscanf(buffer,"%s %d %lf %lf %lf %lf %lf", spec_name, &lin, &eps, &sig, &dip, &pol, &zrot);
    // Empty line
    if (num_match <= 0)
      continue;
    if (num_match != 7)
    {
      fprintf(stderr,"Bad line in transport file: %s\n", buffer);
      exit(1);
    }
    // Capitalize the string name for doing comparison
    capitalize(spec_name);
    // Now find the index for the species and put the value in the right place
    unsigned spec_idx = 0; 
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if (strcmp((*it)->name,spec_name) == 0)
      {
        // Grab the species weight while we have it
        result->nwt[spec_idx] = (*it)->molecular_mass;
        if ((*it)->molecular_mass <= 5.0)
          result->iktdif[nlite_idx++] = spec_idx+1; // plus 1 for fortran
        break;
      }
      spec_idx++;
    }
    // Check to see if we found it
    if (spec_idx < (unit->species.size()-1))
    {
      if (found_specs.find(spec_idx) == found_specs.end())
      {
        found_specs.insert(spec_idx);
        //printf("Found species %s: %lf %lf %lf %lf %lf %d\n", spec_name, eps, sig, dip, pol, zrot, lin);
        // Fill in the data
        result->neps[spec_idx] = eps;
        result->nsig[spec_idx] = sig;
        result->ndip[spec_idx] = dip;
        result->npol[spec_idx] = pol;
        result->nzrot[spec_idx] = zrot;
        result->inlin[spec_idx] = lin;
      }
      else
      {
        // Check to make sure they are the same, if not issue a warning
        if (result->neps[spec_idx] != eps)
        {
          fprintf(stderr,"Warning: overwriting duplicate eps for %s to %lf\n", spec_name, eps);
          result->neps[spec_idx] = eps;
        }
        if (result->nsig[spec_idx] != sig)
        {
          fprintf(stderr,"Warning: overwriting duplicate sig for %s to %lf\n", spec_name, sig);
          result->nsig[spec_idx] = sig;
        }
        if (result->ndip[spec_idx] != dip)
        {
          fprintf(stderr,"Warning: overwriting duplicate dip for %s to %lf\n", spec_name, dip);
          result->ndip[spec_idx] = dip;
        }
        if (result->npol[spec_idx] != pol)
        {
          fprintf(stderr,"Warning: overwriting duplicate pol for %s to %lf\n", spec_name, pol);
          result->npol[spec_idx] = pol;
        }
        if (result->nzrot[spec_idx] != zrot)
        {
          fprintf(stderr,"Warning: overwriting duplicate zrot for %s to %lf\n", spec_name, zrot);
          result->nzrot[spec_idx] = zrot;
        }
        if (result->inlin[spec_idx] != lin)
        {
          fprintf(stderr,"Warning: overwriting duplicate inlin for %s to %d\n", spec_name, lin);
          result->inlin[spec_idx] = lin;
        }
      }
    }
  }
  assert(fclose(source) == 0);
  // Should have found all the species
  assert(found_specs.size() == (unit->species.size()-1));
  // Should have found all the nlite species
  assert(nlite_idx == nlite);

  // Now we need to compute all the coefficients for each of the different phases

  // First build all the needed intermediate arrays
  double tlow, thigh;
  bool first = true;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if (first)
    {
      first = false;
      tlow = (*it)->low_temp;
      thigh = (*it)->high_temp;
    }
    else
    {
      tlow = fmax(tlow,(*it)->low_temp);
      thigh = fmin(thigh,(*it)->high_temp);
    }
  }
  int nt = 50;
  int no = 4; // order
  const double dt = (thigh - tlow) / double(nt-1);
  double tn = tlow;
  double *alogt = (double*)malloc(nt*sizeof(double));
  double *tfit = (double*)malloc(nt*sizeof(double));
  double *fitwt = (double*)malloc(nt*sizeof(double));
  double *fitres = (double*)malloc(nt*sizeof(double));
  double *wrkarray = (double*)malloc((3*nt+3*no+3)*sizeof(double));
  for (int i = 0; i < nt; i++)
  {
    tfit[i] = tn;
    alogt[i] = log(tn);
    tn = tn + dt;
  }
  const double epsil = 0.0;
  const double fdtcgs = 1.0e-18;
  const double fatcm = 1.0e08;
  const double boltz = ru / 6.0221367e+23;
  const double fcon = 0.5 * (fdtcgs * fdtcgs) * (fatcm * fatcm * fatcm) / boltz;

  // Conductivity and viscosity 
  {
    result->nlam = (double*)malloc(result->num_species*result->order*sizeof(double));
    result->neta = (double*)malloc(result->num_species*result->order*sizeof(double));
    double *xla = (double*)malloc(nt*sizeof(double));
    double *xeta = (double*)malloc(nt*sizeof(double));
    unsigned spec_idx = 0; 
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      const double &eps = result->neps[spec_idx];
      const double &sig = result->nsig[spec_idx];
      const double &dip = result->ndip[spec_idx];
      //const double &pol = result->npol[spec_idx];
      const double &zrot = result->nzrot[spec_idx];
      const int &lin = result->inlin[spec_idx];
      const double dst = fcon * (dip * dip) / (eps * (sig * sig * sig));
      const double helpe = 2.6693e-5 * sqrt((*it)->molecular_mass) / (sig * sig);
      const double helpd = 2.6280E-3 * 1.0/*pressure factor*/ / (sqrt((*it)->molecular_mass) * (sig * sig));
      for (int i = 0; i < nt; i++)
      {
        const double &t = tfit[i];
        const double cv = compute_ckcvml(*it, t);
        // Specific heat should always be positive
        assert(cv > 0.0);
        const double tr = t / eps;
        const double dii = sqrt(t*t*t)*helpd / omeg12(1,tr,dst);
        xeta[i] = sqrt(t)*helpe / omeg12(2,tr,dst);
        const double prut = patmos / (ru * t);
        const double rodet = dii * (*it)->molecular_mass * prut / xeta[i];
        const double aa = 2.5 - rodet;
        double bb, croct;
        if (lin == 2)
        {
          bb = zrot * parker(t,eps) + 2.0*(5.0/2.0 + rodet)/PI;
          croct = 1.0;
        }
        else
        {
          bb = zrot * parker(t,eps) + 2.0*(5.0/3.0 + rodet)/PI;
          croct = 2.0/3.0;
        }
        const double ftra = 2.5 * (1.0 - 2.0*croct * aa / (bb*PI));
        const double frot = rodet * (1.0 + 2.0 * aa / (bb*PI));
        const double fvib = rodet;
        double faktor;
        if (lin == 0)
          faktor = 5.0/2.0 * 1.5*ru;
        else if(lin == 1)
          faktor = (ftra*1.5 + frot)*ru + fvib*(cv-2.5*ru);
        else if(lin == 2)
          faktor = (ftra+frot)*1.5*ru + fvib*(cv-3.0*ru);
        else
          assert(false);
        xla[i] = log(xeta[i]/(*it)->molecular_mass * faktor);
        xeta[i] = log(xeta[i]);
      }
      // Now to call the devil function for conductivity
      fitwt[0] = -1.0;
      double epsl = epsil;
      int order = no-1;
      int nord, ierr;
      dpolft(&nt, alogt, xla, fitwt, &order, &nord, &epsl, fitres, &ierr, wrkarray); 
      assert(ierr == 1); // better not fail
      assert(nord == order); // better match the expected max polynomial fit
      double ccc = 0.0;
      double *coeffs = (double*)malloc(no*sizeof(double));
      dpcoef(&nord, &ccc, coeffs, wrkarray);
      // we have the coefficients for this species, so put them in the right place
      for (int i =0; i < no; i++)
      {
        if (isnan(coeffs[i]))
          fprintf(stderr,"Warning: NAN in %d conductivity coefficient for species %s\n", i, (*it)->name);
        result->nlam[spec_idx*no+i] = coeffs[i];
      }

      // Now do the same thing for the viscosity
      fitwt[0] = -1.0;
      epsl = epsil;
      order = no-1;
      dpolft(&nt, alogt, xeta, fitwt, &order, &nord, &epsl, fitres, &ierr, wrkarray);
      assert(ierr == 1);
      assert(nord == order);
      ccc = 0.0;
      dpcoef(&nord, &ccc, coeffs, wrkarray);
      // We've got the viscosity coefficients, put them in the right place
      for (int i = 0; i < no; i++)
      {
        if (isnan(coeffs[i]))
          fprintf(stderr,"Warning: NAN in %d viscosity coefficient for species %s\n", i, (*it)->name);
        result->neta[spec_idx*no+i] = coeffs[i];
      }
      free(coeffs);
      // Update the spec_idx
      spec_idx++;
    }

    free(xla);
    free(xeta);
  }
  // Diffusion
  {
    result->ndif = (double*)malloc(result->num_species*result->num_species*result->order*sizeof(double));
    const double dipmin = 1.0e-20;
    double *xd = (double*)malloc(nt*sizeof(double));
    // For every pair of species
    for (int j = 0; j < result->num_species; j++)
    {
      for (int k = 0; k <= j; k++)
      {
        const double &sigj = result->nsig[j];
        const double &sigk = result->nsig[k];
        const double &epsj = result->neps[j];
        const double &epsk = result->neps[k];
        const double &polj = result->npol[j];
        const double &polk = result->npol[k];
        const double &dipj = result->ndip[j];
        const double &dipk = result->ndip[k];
        const double &wtj = result->nwt[j];
        const double &wtk = result->nwt[k];

        double sigm = 0.5 * (sigj + sigk);
        double epsm = sqrt(epsj * epsk);
        double dst, xi;
        if ((dipj < dipmin) && (dipk > dipmin))
        {
          // k is polar, j is nonpolar
          dst = 0.0;
          xi = 1.0 + polj * fcon * (dipk*dipk) * sqrt(epsk/epsj) /
                (2.0 * (sigj * sigj * sigj) * epsk * (sigk * sigk * sigk));
        }
        else if ((dipj > dipmin) && (dipk < dipmin))
        {
          // j is polar, k is nonpolar
          dst = 0.0;
          xi = 1.0 + polk * fcon * (dipj * dipj) * sqrt(epsj/epsk) /
                (2.0 * (sigk * sigk * sigk) * epsj * (sigj * sigj * sigj));
        }
        else
        {
          // normal case, both the same
          dst = fcon * dipj * dipk / (epsm * (sigm * sigm * sigm));
          xi = 1.0;
        }
        sigm = sigm * pow(xi,(-1.0/6.0));
        epsm = epsm * (xi * xi);
        const double help1 = (wtj + wtk) / (2.0*wtj*wtk);
        for (int i = 0; i < nt; i++)
        {
          const double &t = tfit[i]; 
          const double tr = t / epsm;
          const double help2 = 0.002628 * sqrt(help1 * (t * t * t));
          xd[i] = help2*patmos / (omeg12(1,tr,dst) * (sigm * sigm) * patmos);
          xd[i] *= d12(wtj,wtk,t,epsj,epsk,sigj,sigk,dst);
          xd[i] = log(xd[i]);
        }
        fitwt[0] = -1.0;
        double epsl = epsil;
        int order = no-1;
        int nord, ierr;
        // time to call the devil
        dpolft(&nt, alogt, xd, fitwt, &order, &nord, &epsl, fitres, &ierr, wrkarray);
        assert(ierr == 1); // better not fail
        assert(nord == order); // better match the expected max polynomial fit
        double ccc = 0.0; 
        double *coeffs = (double*)malloc(no*sizeof(double));
        dpcoef(&nord, &ccc, coeffs, wrkarray);
        // Now we've got our coefficients so put them in the right place
        // The array is symmetric so we can put it in both places
        for (int i = 0; i < no; i++)
        {
          if (isnan(coeffs[i]))
            fprintf(stderr,"Warning: NAN in %d diffusion coefficient for species interactions %d and %d\n", i, j, k);
          result->ndif[i+j*no+k*no*result->num_species] = coeffs[i];
          result->ndif[i+k*no+j*no*result->num_species] = coeffs[i];
        }
        free(coeffs);
      }
    }
    free(xd);
  }
  // Finally compute the light coefficients
  {
    result->ntdif = (double*)malloc(result->nlite*result->num_species*result->order*sizeof(double));
    double *td = (double*)malloc(nt*sizeof(double));

    static const double fitast[7] = { .1106910525E+01, -.7065517161E-02,-.1671975393E-01,
                                      .1188708609E-01,  .7569367323E-03,-.1313998345E-02,
                                      .1720853282E-03 };
    static const double fitbst[7] = { .1199673577E+01, -.1140928763E+00,-.2147636665E-02,
                                      .2512965407E-01, -.3030372973E-02,-.1445009039E-02,
                                      .2492954809E-03 };
    static const double fitcst[7] = { .8386993788E+00,  .4748325276E-01, .3250097527E-01,
                                     -.1625859588E-01, -.2260153363E-02, .1844922811E-02,
                                     -.2115417788E-03 };

    unsigned light_idx = 0;
    for (int j = 0; j < result->num_species; j++)
    {
      // Only care about the light species
      if (result->nwt[j] > 5.0)
        continue;
      // Make sure we're in the right place
      assert(int(light_idx) < result->nlite);
      // Make sure we can find this species in the list of light indexes
      {
        bool found = false;
        for (int i = 0; i < result->nlite; i++)
        {
          if (result->iktdif[i] == (j+1))
          {
            found = true;
            break;
          }
        }
        assert(found);
      }
      const double epsj = result->neps[j] * boltz;
      const double sigj = result->nsig[j] * 1.0e-8;
      const double polj = result->npol[j] * 1.0e-24;
      const double poljst = polj / (sigj * sigj * sigj);

      // for each of the other species
      for (int k = 0; k < result->num_species; k++)
      {
        const double epsk = result->neps[k] * boltz;
        const double sigk = result->nsig[k] * 1.0e-8;
        const double dipk = result->ndip[k] * 1.0e-18;
        const double dipkst = dipk / sqrt(epsk*(sigk*sigk*sigk));
        const double ekoej = epsk / epsj;
        const double tse = 1.0 + 0.25*poljst*(dipkst*dipkst)*sqrt(ekoej);
        const double eok = (tse * tse) * sqrt(result->neps[j] * result->neps[k]);
        const double wtkj = (result->nwt[k] - result->nwt[j]) / (result->nwt[k] + result->nwt[j]);
        const double eqlog = log(eok);
        for (int i = 0; i < nt; i++)
        {
          const double tslog = log(tfit[i]) - eqlog;
          const double astar = horner(7, fitast, tslog);
          const double bstar = horner(7, fitbst, tslog);
          const double cstar = horner(7, fitcst, tslog);
          td[i] = 7.5 * wtkj * (2.0*astar + 5.0) * (6.0*cstar - 5.0) /
                  (astar * (16.0*astar - 12.0*bstar + 55.0));
        }
        // Back to the devil
        fitwt[0] = -1.0;
        double epsl = epsil;
        int order = no-1;
        int nord, ierr;
        dpolft(&nt, tfit, td, fitwt, &order, &nord, &epsl, fitres, &ierr, wrkarray);
        assert(ierr == 1); // better not fail
        assert(nord == order); // better match the expected max polynomial fit
        double ccc = 0.0; 
        double *coeffs = (double*)malloc(no*sizeof(double));
        dpcoef(&nord, &ccc, coeffs, wrkarray); 
        // Now that we've got our coefficients, put them in the right place
        for (int i = 0; i < no; i++)
        {
          if (isnan(coeffs[i]))
            fprintf(stderr,"Warning: NAN coefficient %d for light species %d with species %d\n", i, j, k);
          result->ntdif[i+k*no+light_idx*no*result->num_species] = coeffs[i];
        }
        free(coeffs);
      }
      // Update the light idx
      light_idx++;
    }
    free(td);
  }

  free(alogt);
  free(tfit);
  free(fitwt);
  free(fitres);
  free(wrkarray);

  return result;
}

ChemistryUnit* parse_qss_stif_file(TranslationUnit *unit, const char *file_name)
{
  FILE *source = fopen(file_name, "r");
  if (source == NULL)
  {
    fprintf(stderr,"Bad file descriptor for qss-stif file.  File %s does not exist.\n", file_name);
    exit(1);
  }
  ChemistryUnit *chem = new ChemistryUnit(unit);
  // First read out the information for the QSS species
  int num_qss;
  assert(fscanf(source,"%d",&num_qss) > 0);
  fprintf(stdout,"  QSS connected components: %d\n", num_qss);
  for (int i = 0; i < num_qss; i++)
  {
    ConnectedComponent *qss = new ConnectedComponent(unit); 
    chem->add_component(qss);
    int num_species;
    assert(fscanf(source,"%d",&num_species) > 0);
    for (int j = 0; j < num_species; j++)
    {
      int idx, spec_idx;
      assert(fscanf(source,"%d %d",&idx,&spec_idx) > 0);
      qss->add_species(idx,spec_idx);
    }
    char command;
    assert(fscanf(source,"%c",&command) > 0);
    while ((command == ' ') || (command == '\n'))
    {
      assert(fscanf(source,"%c",&command) > 0);
    }
    // While it's not zero 
    while (command != '0')
    {
      int x, y, z;
      switch (command)
      {
        case 'z':
          {
            assert(fscanf(source,"%d %d", &y, &x) > 0);
            qss->add_zero_statement(y, x);
            break;
          }
        case 'b':
          {
            qss->begin_den_statement();
            break;
          }
        case 'e':
          {
            qss->end_den_statement();
            break;
          }
        case 'n':
          {
            assert(fscanf(source,"%d %d", &y, &x) > 0);
            qss->add_den_pair(y, x);
            break;
          }
        case 'm':
          {
            assert(fscanf(source,"%d %d %d", &z, &y, &x) > 0);
            qss->add_multiply_statement(z, y, x);
            break;
          }
        case 'a':
          {
            assert(fscanf(source,"%d %d %d", &z, &y, &x) > 0);
            qss->add_add_statement(z, y, x);
            break;
          }
        case 'd':
          {
            assert(fscanf(source,"%d %d", &z, &y) > 0);
            qss->add_divide_statement(z,y);
            break;
          }
        case 'x':
          {
            assert(fscanf(source,"%d", &x) > 0);
            qss->start_x_statement(x);
            break;
          }
        case 'p':
          {
            assert(fscanf(source,"%d %d", &x, &y) > 0);
            qss->add_x_pair(x,y);
            break;
          }
        case 'f':
          {
            qss->finish_x_statement();
            break;
          }
        default:
          assert(false);
      }
      assert(fscanf(source,"%c",&command) > 0);
      while ((command == ' ') || (command == '\n'))
      {
        assert(fscanf(source,"%c",&command) > 0);
      }
    }
  }
  // Read out the unimportant reactions
  int num_unimp;
  assert(fscanf(source,"%d", &num_unimp) > 0);
  for (int i = 0; i < num_unimp; i++)
  {
    int backward, index;
    assert(fscanf(source,"%d %d", &backward, &index));
    chem->add_unimportant(backward, index);
  }
  // Read out the list of qss species expressions
  int num_reacs;
  assert(fscanf(source,"%d", &num_reacs) > 0);
  for (int reac_id = 0; reac_id < num_reacs; reac_id++)
  {
    char command;
    assert(fscanf(source,"%c",&command) > 0);
    while ((command == ' ') || (command == '\n'))
    {
      assert(fscanf(source,"%c",&command) > 0);
    }
    while (command != 'e')
    {
      int reac_idx,qss_idx,other_idx,coeff;
      switch (command)
      {
        case 'z':
          {
            assert(fscanf(source,"%d %d %d", &reac_idx,&qss_idx,&coeff) > 0);
            chem->add_forward_zero(reac_idx,qss_idx,coeff);
            break;
          }
        case 'f':
          {
            assert(fscanf(source,"%d %d %d %d", &reac_idx,&qss_idx,&other_idx,&coeff) > 0); 
            chem->add_forward_contribution(reac_idx,qss_idx,other_idx,coeff);
            break;
          }
        case 'b':
          {
            assert(fscanf(source,"%d %d %d", &reac_idx,&qss_idx,&coeff) > 0);
            chem->add_backward_zero(reac_idx,qss_idx,coeff);
            break;
          }
        case 'r':
          {
            assert(fscanf(source,"%d %d %d %d", &reac_idx,&qss_idx,&other_idx,&coeff) > 0);
            chem->add_backward_contribution(reac_idx,qss_idx,other_idx,coeff);
            break;
          }
        default:
          assert(false);
      }
      assert(fscanf(source,"%c",&command) > 0);
      while ((command == ' ') || (command == '\n'))
      {
        assert(fscanf(source,"%c",&command) > 0);
      }
    }
  }
  // Read out the initial denominator values
  int num_denom_spec;
  assert(fscanf(source,"%d",&num_denom_spec) > 0);
  for (int spec = 0; spec < num_denom_spec; spec++)
  {
    int spec_idx;
    assert(fscanf(source,"%d",&spec_idx) > 0);
    int num_forward, num_backward;
    assert(fscanf(source,"%d",&num_forward) > 0);
    for (int j = 0; j < num_forward; j++)
    {
      int reac_idx;
      assert(fscanf(source,"%d",&reac_idx) > 0);
      chem->add_forward_denom(spec_idx,reac_idx);
    }
    assert(fscanf(source,"%d",&num_backward) > 0);
    for (int j = 0; j < num_backward; j++)
    {
      int reac_idx;
      assert(fscanf(source,"%d",&reac_idx) > 0);
      chem->add_backward_denom(spec_idx,reac_idx);
    }
  }
  // Read out the QSS correction terms
  assert(fscanf(source,"%d",&num_reacs) > 0);
  for (int i = 0; i < num_reacs; i++)
  {
    char command;
    assert(fscanf(source,"%c",&command) > 0);
    while ((command == ' ') || (command == '\n'))
    {
      assert(fscanf(source,"%c",&command) > 0);
    }
    while (command != '2')
    {
      int reac_idx, qss_idx;
      switch (command)
      {
        case '0':
          {
            assert(fscanf(source,"%d %d",&reac_idx,&qss_idx) > 0);
            chem->add_forward_correction(reac_idx,qss_idx);
            break;
          }
        case '1':
          {
            assert(fscanf(source,"%d %d",&reac_idx,&qss_idx) > 0);
            chem->add_backward_correction(reac_idx,qss_idx);
            break;
          }
        default:
          assert(false);
      }
      assert(fscanf(source,"%c",&command) > 0);
      while ((command == ' ') || (command == '\n'))
      {
        assert(fscanf(source,"%c",&command) > 0);
      }
    }
  }
  // Now we read in the stiff species
  int num_stif;
  assert(fscanf(source,"%d", &num_stif) > 0);
  fprintf(stdout,"  Stiff species: %d\n", num_stif);
  for (int i = 0; i < num_stif; i++)
  {
    int k, k2;
    char *spec_name = (char*)malloc(128*sizeof(char));
    assert(fscanf(source,"%d %d %s", &k, &k2, spec_name) > 0);
    Stif *stif = new Stif(spec_name, k, k2, unit);
    chem->add_stif(stif);
    int forward_d, forward_c, backward_d, backward_c;
    assert(fscanf(source,"%d", &forward_d) > 0);
    for (int j = 0; j < forward_d; j++)
    {
      int reac_idx, coeff;
      assert(fscanf(source,"%d %d", &reac_idx, &coeff) > 0);
      stif->add_forward_d(reac_idx, coeff);
    }
    assert(fscanf(source,"%d", &forward_c) > 0);
    for (int j = 0; j < forward_c; j++)
    {
      int reac_idx, coeff;
      assert(fscanf(source,"%d %d", &reac_idx, &coeff) > 0);
      stif->add_forward_c(reac_idx, coeff);
    }
    assert(fscanf(source,"%d", &backward_d) > 0);
    for (int j = 0; j < backward_d; j++)
    {
      int reac_idx, coeff;
      assert(fscanf(source,"%d %d", &reac_idx, &coeff) > 0);
      stif->add_backward_d(reac_idx, coeff);
    }
    assert(fscanf(source,"%d", &backward_c) > 0);
    for (int j = 0; j < backward_c; j++)
    {
      int reac_idx, coeff;
      assert(fscanf(source,"%d %d", &reac_idx, &coeff) > 0);
      stif->add_backward_c(reac_idx, coeff);
    }
  }
  assert(fclose(source)==0);
  return chem;
}

// EOF

