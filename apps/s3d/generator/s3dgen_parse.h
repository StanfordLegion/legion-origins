
#ifndef __S3D_PARSE__
#define __S3D_PARSE__

#include "s3dgen_ast.h"
#include "chemistry.h"

TranslationUnit* parse_source_file(const char *file_name);

void parse_therm_file(TranslationUnit *unit, const char *file_name);

TranLib* parse_tran_file(TranslationUnit *unit, const char *file_name);

TranLib* parse_tran_dat_file(TranslationUnit *unit, const char *file_name);

ChemistryUnit* parse_qss_stif_file(TranslationUnit *unit, const char *file_name);

#endif // __S3D_PARSE__

