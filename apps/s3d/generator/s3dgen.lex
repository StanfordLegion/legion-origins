
/**
 * s3dgen.lex
 *
 * lexical analysis for the s3d generator
 *
 */

%{
  #include <string>
  #include <cstring>
  #include <cstdlib> 
  #include "s3dgen.tab.h"
  #include "s3dgen_ast.h"

  static int colnum = 0;
  #define YY_USER_ACTION {                \
    s3dgenlloc.first_line = yylineno;      \
    s3dgenlloc.first_column = colnum;      \
    colnum += yyleng;                     \
    s3dgenlloc.last_column = colnum;       \
    s3dgenlloc.last_line = yylineno;       \
  }

  char* local_strdup(const char *ptr);
  char* local_strcat(const char *current, const char *text);

  extern TranslationUnit *unit;
%}

alpha	[a-zA-Z]
digit	[0-9]
name	{alpha}({alpha}|{digit}|-|\,|\*)*(\([sS]\))?
dig	{digit}
float	-?(({dig}+)|({dig}+"."{dig}*)|("."{dig}+))([eE][+-]?{dig}+)?

%option noyywrap nounput
%option yylineno
%start S_EOL

%%

ELEMENTS |
elements	{ return T_ELEMENTS; }
SPECIES |
species		{ return T_SPECIES; }
REACTIONS |
reactions	{ return T_REACTIONS; }
THERMO |
thermo		{ return T_THERMO; }
END |
end		{ return T_END; }
[Dd][Uu][Pp][Ll][Ii][Cc][Aa][Tt][Ee] |
[Dd][Uu][Pp]    { return T_DUPLICATE; }
[Ll][Oo][Ww]    { return T_LOW; }
[Tt][Rr][Oo][Ee] { return T_TROE; }
[Ss][Rr][Ii]    { return T_SRI; }
[Hh][Vv]        { return T_HV; }
[Rr][Ee][Vv]    { return T_REV; }
[Ll][Tt]        { return T_LT; }
[Rr][Ll][Tt]    { return T_RLT; }
"(+"{name}")"   { 
                  // Remove stuff around the name
                  yytext[strlen((char*)yytext)-1] = '\0';
                  s3dgenlval.str = local_strdup(((char*)yytext)+2); 
                  unit->set_pressure_species(s3dgenlval.str); 
                }
{float}         { s3dgenlval.data = atof((char *)yytext); return T_FLOAT; }
{name}          { s3dgenlval.str = local_strdup(yytext); return T_NAME; }
"!".*		; /* comment */
[ \t]+		; /* whitespace */
"+"		{ return T_PLUS; }
"="		{ return T_EQUALS; }
"<=>"		{ return T_LRARROW; }
"=>"		{ return T_RARROW; }
"/"		{ return T_SLASH; }

<S_EOL>"\n"     { return T_EOL; }
"\n"            { ; /* ignore newlines */ }
.               { fprintf(stderr,"lex: unknown character '%c', line %d", yytext[0], yylineno); fflush(stderr); exit(1); }

%%

void lex_eol(bool on)
{
  if (on)
    BEGIN S_EOL;
  else
    BEGIN 0;
}

char* local_strdup(const char *ptr)
{
  size_t length = strlen(ptr);
  // Length + 2 just to be safe
  char *buffer = (char*)malloc((length+2)*sizeof(char));
  strcpy(buffer, ptr);
  return buffer;
}

// EOF

