function reac = ckreac(chem)
%
% function [reac] = ckreac(chem)
%  Purpose:  get reaction information
%  Input:  
%        chem: chemkin workspace 
%  Output: 
%        reac: cell array for each reaction, each cell is a structure
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

comn = ckcomn(chem);

NcCO = comn.NcCO;
IcNS = comn.IcNS;
NPAR = comn.NPAR;

NREV = comn.NREV;
IcRV = comn.IcRV;
NcRV = comn.NcRV;

NTHB = comn.NTHB;
MXTB = comn.MXTB;
IcTB = comn.IcTB; % ITHB
IcKN = comn.IcKN; % NTBS
NcKT = comn.NcKT; % AIK(MXTB, *)
IcKT = comn.IcKT; % NKTB(MXTB, *)

NFAL = comn.NFAL; 
NFAR = comn.NFAR; % NFAR = 8
IcFL = comn.IcFL; % IFAL
IcFO = comn.IcFO; % IFOP
IcKF = comn.IcKF; % KFAL
NcFL = comn.NcFL; % FPAR(NFAR,*)

KK = chem.KK;
II = chem.II;

reac = cell(II,1);

% whether the reaction is reversible
IREV = zeros(II,1);
for N = 1:II
    if chem.ICKWRK(IcNS + N - 1)<0
        IREV(N) = 0;
    else
        IREV(N) = 1;
    end
end

% forward reaction parameters 
for N = 1:II
    r = [];
    IND = NcCO + (N-1)*(NPAR+1);
    r.RA = chem.RCKWRK(IND);
    r.RB = chem.RCKWRK(IND+1);
    r.RE = chem.RCKWRK(IND+2);

    r.IREV = IREV(N);
    r.ITHB = -1;
    r.IFAL = 0;
    r.KFAL = 0;
    r.RABE = 0;

    reac{N} = r;
end


% reverse reaction parameters
for N=1:NREV
    I = chem.ICKWRK(IcRV + N - 1);
    reac{I}.RABE = 1;

    IND = NcRV + (N-1)*(NPAR+1);
    reac{I}.RAR = chem.RCKWRK(IND);
    reac{I}.RBR = chem.RCKWRK(IND+1);
    reac{I}.RER = chem.RCKWRK(IND+2);
end


% Third body coefficients
for N=1:NTHB
    I = chem.ICKWRK(IcTB + N - 1);
    reac{I}.ITHB = chem.ICKWRK(IcKN + N - 1);
    NTBS = chem.ICKWRK(IcKN+N-1);
    Ind = IcKT+(N-1)*MXTB;
    reac{I}.NKTB = chem.ICKWRK(Ind:Ind+NTBS-1);
    Ind = NcKT+(N-1)*MXTB;
    reac{I}.AIK = chem.RCKWRK(Ind:Ind+NTBS-1);
    reac{I}.NTBS = NTBS;
end


% Fall off parameters
for N=1:NFAL
    I = chem.ICKWRK(IcFL + N - 1);
    reac{I}.IFAL = 1;
    reac{I}.KFAL = chem.ICKWRK(IcKF+N-1);
    reac{I}.IFOP = chem.ICKWRK(IcFO+N-1);
    
    Ind = NcFL+(N-1)*NFAR;
    reac{I}.FPAR = chem.RCKWRK(Ind:Ind+NFAR-1);
end


