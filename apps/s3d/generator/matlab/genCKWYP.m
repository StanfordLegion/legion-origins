%
%   function genCKWYP(fid, chem, QSSList, bISO)
%   function genCKWYP(fid, chem, QSSList)
%   function genCKWYP(fid, chem)
%
%   purpose: generate subroutine CKWYP
%       fid: file id to print to
%       chem: the skeletal or detailed mechanism before QSSA
%       QSSList: array of IDs for the QSS species
%       bISO: integer: 0: no isomuer lumping, 1: with isomer lumping
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genCKWYP(fid, chem, QSSList, bISO)

if nargin<4, bISO = []; end
if nargin<3, QSSList = []; end

if isempty(bISO), bISO = 0; end
if isempty(QSSList)
    NQSS = 0;
else
    NQSS = length(QSSList);
end

comn = ckcomn(chem);

fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');




if bISO
    
fprintf(fid, '      SUBROUTINE CKWYP  (P, T, YISO, ICKWRK, RCKWRK, WISO)\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      DIMENSION YISO(*),WISO(*),ICKWRK(*),RCKWRK(*)\n');
fprintf(fid, '      DIMENSION RF(%d),RB(%d),RKLOW(%d)\n', chem.II, chem.II, max(comn.NFAL, 1));
fprintf(fid, '      DIMENSION C(%d),Y(%d),WDOT(%d)\n', chem.KK-NQSS, chem.KK-NQSS, chem.KK-NQSS);
if NQSS
fprintf(fid, '      DIMENSION XQ(%d)\n', NQSS);
end

fprintf(fid, 'C      DIMENSION DIFF(*), DIF(%d)\n', chem.KK-NQSS);

fprintf(fid, 'C\n');
fprintf(fid, '      CALL XISO(YISO, Y)\n');

% to lump the diffusin for lumped isomers
fprintf(fid, 'C      CALL XISO(DIFF, DIF)\n');
fprintf(fid, 'C      DO N=1, %d\n', chem.KK-NQSS);
fprintf(fid, 'C      DIF(N) = MAX(DIF(N), 0D0)\n');
fprintf(fid, 'C      ENDDO\n');

fprintf(fid, '     CALL YTCP(P, T, Y, C)\n');
fprintf(fid, '     CALL RATT(T, RF, RB, RKLOW)\n');
fprintf(fid, '      CALL RATX(T, C, RF, RB, RKLOW)\n');
if ~isempty(QSSList)
fprintf(fid, '      CALL QSSA(RF, RB, XQ)\n');
end
fprintf(fid, '      CALL RDOT(RF, RB, WDOT)\n');
fprintf(fid, '      CALL RISO(WDOT, WISO)\n');
fprintf(fid, 'C\n');
fprintf(fid, '      END\n');

else

fprintf(fid, '      SUBROUTINE CKWYP  (P, T, Y, ICKWRK, RCKWRK, WDOT)\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      DIMENSION Y(*),WDOT(*),ICKWRK(*),RCKWRK(*)\n');
fprintf(fid, '      DIMENSION RF(%d),RB(%d),RKLOW(%d),C(%d)\n', chem.II, chem.II, max(comn.NFAL, 1), chem.KK-NQSS);

if NQSS
fprintf(fid, '      DIMENSION XQ(%d)\n', NQSS);
end
fprintf(fid, 'C\n');
fprintf(fid, '      CALL YTCP(P, T, Y, C)\n');
fprintf(fid, '      CALL RATT(T, RF, RB, RKLOW)\n');
fprintf(fid, '      CALL RATX(T, C, RF, RB, RKLOW)\n');
if ~isempty(QSSList)
fprintf(fid, '      CALL QSSA(RF, RB, XQ)\n');
end
fprintf(fid, '      CALL RDOT(RF, RB, WDOT)\n');
fprintf(fid, 'C\n');
fprintf(fid, '      END\n');
    
end

end
