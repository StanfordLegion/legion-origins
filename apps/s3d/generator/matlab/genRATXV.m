%
% function genRATXV(fid, chem, QSSList)
% function genRATXV(fid, chem)
%
%   purpose: generate subroutine RATX, vectorized version
%       fid: file id to print to
%       chem: the skeletal or detailed mechanism before QSSA
%       QSSList: array of QSS species, integers
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genRATXV(fid, chem, QSSList)

if nargin<3, QSSList = []; end

comn = ckcomn(chem);
if comn.NFT1 || comn.NJAN || comn.NLAN || comn.NEIM || comn.NRLT || comn.NRLT 
    error('RATT: type of reaction not supported');
end

fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');

fprintf(fid, '      SUBROUTINE RATX (VL, T, C, RF, RB, RKLOW)\n');
fprintf(fid, '      USE chemkin_m, only : MAXVL\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      PARAMETER (SMALL = 1D-200)\n');
fprintf(fid, '      DIMENSION C(MAXVL,*),RF(MAXVL,*),RB(MAXVL,*),RKLOW(MAXVL,*)\n');
fprintf(fid, '      DIMENSION T(MAXVL),CTOT(MAXVL),CTB(MAXVL)\n');
fprintf(fid, '      DIMENSION PR(MAXVL),PRLOG(MAXVL),PCOR(MAXVL)\n');
fprintf(fid, '      DIMENSION FCLOG(MAXVL),FC(MAXVL),XN(MAXVL)\n');
fprintf(fid, '      DIMENSION FCENT0(MAXVL),FCENT1(MAXVL),FCENT2(MAXVL)\n');
fprintf(fid, '      DIMENSION CPRLOG(MAXVL),FLOG(MAXVL),FCENT(MAXVL)\n');
fprintf(fid, '        INTEGER VL\n');
fprintf(fid, 'C\n');
%fprintf(fid, '      CALL VRDA_LOG(VL,T,TLOG)\n');

if comn.NTHB
fprintf(fid, '      CTOT(:VL) = 0.0\n');
fprintf(fid, '      DO K = 1, %d\n', chem.KK-length(QSSList));
fprintf(fid, '         CTOT(:VL) = CTOT(:VL) + C(:VL,K)\n');
fprintf(fid, '      ENDDO\n');
fprintf(fid, 'C\n');
end

R = ckreac(chem);
S = cknu(chem);
SF = cknuf(chem);
SR = S-SF;
SF = -SF;

% generate the index of a species id to that in C(*)
I = ones(chem.KK,1);
I(QSSList) = 0;
I = find(I);
IC = zeros(chem.KK,1);
IC(I) = 1:length(I);

% generate IFAL list
NFAL = comn.NFAL;
IcFL = comn.IcFL;
I = chem.ICKWRK(IcFL:IcFL+NFAL-1);
IFAL = zeros(chem.II, 1);
IFAL(I) = 1:NFAL; % reverse index of fall off reactions

for i=1:chem.II
    r = R{i};
    
    %fprintf(fid, 'C\n');
    %rstr=ckprintreac(S(:, i), chem);
    %fprintf(fid, 'C      R%d: %s\n', i, rstr{1});
    fprintf(fid, 'C      R%d\n', i);
    
    % if third body is needed
    switch r.ITHB
        case -1 % no third body
        case 0 % third body without enhancement
            fprintf(fid, '      CTB(:VL) = CTOT(:VL)\n');
        otherwise % N third body enhancement
            fprintf(fid, '      CTB(:VL) = CTOT(:VL)');
            nCounter=0;
            for m=1:r.NTBS
                k = r.NKTB(m);
                
                if any(QSSList==k), continue, end
                
                aNM = r.AIK(m) - 1;
                if aNM == 0, continue; end
                
                if nCounter>=3
                    fprintf(fid, '\n     * ');
                    nCounter=0;
                end
                
                switch aNM
                    case 1
                        fprintf(fid, '+C(:VL,%d)', IC(k));
                    case -1
                        fprintf(fid, '-C(:VL,%d)', IC(k));
                    otherwise
                        fprintf(fid, '%s*C(:VL,%d)', fmtG(aNM, 1), IC(k));
                end
                nCounter = nCounter+1;
            end
            fprintf(fid, '\n');
    end
    
    % if fall off reaction
    if r.IFAL
        
        if r.KFAL==0 % type of bath gas: CTB
            fprintf(fid, '      PR(:VL) = RKLOW(:VL,%d) * CTB(:VL) / RF(:VL,%d)\n', IFAL(i), i);
        elseif ~any(QSSList==r.KFAL) % type of bath gas: species r.KFAL
            fprintf(fid, '      PR(:VL) = RKLOW(:VL,%d) * C(:VL,%d)/ RF(:VL,%d)\n', IFAL(i), IC(r.KFAL), i);
        else %if QSS species is third body, assume Rf=Rlow
            fprintf(fid, '      PR(:VL) = 0.0\n');
        end
        
        fprintf(fid, '      PCOR(:VL) = PR(:VL) / (1.0 + PR(:VL))\n');
        
        % switch r.IFOP
        % 0 - No fall-off behavior
        % 1 - fall-off behavior - Lindeman form (3 parameters)
        % 2 - fall-off behavior - SRI form      (8 parameters)
        % 3 - fall-off behavior - Troe form     (6 parameters)
        % 4 - fall-off behavior - Troe form     (7 parameters)
        
        if r.IFOP==3 && r.FPAR(5)<1.0E-2 && r.FPAR(6)>1.0E6
            % 6-Parameter TROE, FC=CONST
            if abs(r.FPAR(4)-1)<1E-9
                % Lindeman
            else
                fprintf(fid, '      PRLOG(:VL) = MAX(PR(:VL),SMALL)\n');
                fprintf(fid, '      CALL VRDA_LOG10(VL,PRLOG,PRLOG)\n');
                fprintf(fid, '      FC = (PRLOG(:VL) %s)\n     *     /(%s -0.14D0*(PRLOG(:VL) %s))\n', ...
                    fmtG(-0.4-0.67*log10(r.FPAR(4)), 1), ...
                    fmtG(0.75 - 1.27*log10(r.FPAR(4))), ...
                    fmtG(-0.4 - 0.67*log10(r.FPAR(4)), 1));
                
                fprintf(fid, ...
                    '      FC(:VL) = %s /(1.0D0 + FC(:VL)*FC(:VL))\n', fmtG(log(r.FPAR(4))));
                fprintf(fid, ...
                    '      CALL VRDA_EXP(VL, FC, FC)\n');
                
                fprintf(fid,'      PCOR(:VL) = FC(:VL) * PCOR(:VL)\n');
            end
            
        elseif r.IFOP>1
            fprintf(fid, '      PRLOG(:VL) = MAX(PR(:VL),SMALL)\n');
            fprintf(fid, '      CALL VRDA_LOG10(VL,PRLOG,PRLOG)\n');
            if r.IFOP==2 % 8-parameter SRI
                error('this need to be revised further')

            else %6,7-Parameter TROE
                fprintf(fid, '      FCENT0(:VL) = T(:VL)/(%s)\n', fmtG(-r.FPAR(5),1));
                fprintf(fid, '      CALL VRDA_EXP(VL,FCENT0,FCENT0)\n');
                if r.FPAR(4)~=0
                fprintf(fid, '      FCENT1(:VL) = T(:VL)/(%s)\n', fmtG(-r.FPAR(6),1));
                fprintf(fid, '      CALL VRDA_EXP(VL,FCENT1,FCENT1)\n');
                else
                fprintf(fid, '      FCENT1(:VL) = 0D0\n');    
                end
                if r.IFOP==4 %7-parameter TROE
                    fprintf(fid, '      FCENT2(:VL) = %s/T(:VL)\n', fmtG(-r.FPAR(7),1));
                    fprintf(fid, '      CALL VRDA_EXP(VL,FCENT2,FCENT2)\n');
                    fprintf(fid, '      FCENT(:VL) = %s*FCENT0(:VL) %s*FCENT1(:VL)\n     1             +FCENT2(:VL)\n', ...
                        fmtG(1.0-r.FPAR(4)), fmtG(r.FPAR(4),1));
                else
                    fprintf(fid, '      FCENT(:VL) = %s*FCENT0(:VL) %s*FCENT1(:VL)\n', fmtG(1.0-r.FPAR(4)), fmtG(r.FPAR(4),1));
                end
                
                fprintf(fid, '      FCLOG(:VL) = MAX(FCENT(:VL),SMALL)\n');
                fprintf(fid, '      CALL VRDA_LOG10(VL,FCLOG,FCLOG)\n');
                fprintf(fid, '      XN(:VL)    = 0.75 - 1.27*FCLOG(:VL)\n');
                fprintf(fid, '      CPRLOG(:VL) = PRLOG(:VL) - (0.4 + 0.67*FCLOG(:VL))\n');
                fprintf(fid, '      FLOG(:VL) = CPRLOG(:VL)/(XN(:VL)-0.14*CPRLOG(:VL))\n');
                fprintf(fid, '      FLOG(:VL) = 1D0+FLOG(:VL)*FLOG(:VL)\n');
                fprintf(fid, '      FLOG(:VL) = FCLOG(:VL)/FLOG(:VL)\n');
                fprintf(fid, '      FC(:VL) = %.16fD0*FLOG(:VL)\n', log(10));
                fprintf(fid, '      CALL VRDA_EXP(VL,FC,FC)\n');
                
                fprintf(fid, '      PCOR(:VL) = FC(:VL) * PCOR(:VL)\n');
            end
            
        end
        fprintf(fid, '      RF(:VL,%d) = RF(:VL,%d) * PCOR(:VL)\n', i, i);
        if r.IREV
            if r.RABE
                if r.RAR == 0
                else
                    fprintf(fid, '      RB(:VL,%d) = RB(:VL,%d) * PCOR(:VL)\n', i, i);
                end
            else
                fprintf(fid, '      RB(:VL,%d) = RB(:VL,%d) * PCOR(:VL)\n', i, i);
            end
        end
    end
    
    % multiply reaction rate by CTB or non-QSS species concentrations
    
    bfwd = 0;
    brev = 0;
    bthb = 0;
    if r.ITHB>=0 && ~r.IFAL
        bthb = 1;
        bfwd = 1;
        if r.IREV
            if r.RABE
                if r.RAR~=0
                    brev = 1;
                end
            else
                brev = 1;
            end
        end
    end
    
    sf = SF(:, i);
    lf = [];
    for k=1:chem.KK
        if ~sf(k), continue; end
        if any(QSSList==k), continue; end % skip QSS species
        for m=1:sf(k)
            lf = [lf k]; %#ok<AGROW>
        end
    end
    if ~isempty(lf), bfwd = 1; end
        
    sr = SR(:, i);
    lr = [];
    for k=1:chem.KK
        if ~r.IREV, break, end
        if r.RABE
            if r.RAR==0
                break
            end
        end
        if ~sr(k), continue; end
        if any(QSSList==k), continue; end % skip QSS species
        for m=1:sr(k)
            lr = [lr k];  %#ok<AGROW>
        end
    end
    if ~isempty(lr), brev = 1; end
   
    if bfwd
        fprintf(fid, '      RF(:VL,%d) = RF(:VL,%d)', i, i);
        if bthb
            fprintf(fid, '*CTB(:VL)');
        end
        for m=1:length(lf)
            fprintf(fid, '*C(:VL,%d)', IC(lf(m)));
        end
        fprintf(fid, '\n');
    end
    if brev
        fprintf(fid, '      RB(:VL,%d) = RB(:VL,%d)', i, i);
        if bthb
            fprintf(fid, '*CTB(:VL)');
        end
        for m=1:length(lr)
            fprintf(fid, '*C(:VL,%d)', IC(lr(m)));
        end
        fprintf(fid, '\n');
    end
     
end

fprintf(fid, 'C\n');
fprintf(fid, '      END\n');

end
