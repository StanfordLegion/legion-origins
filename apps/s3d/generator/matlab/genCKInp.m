% generate a chem.inp for the reduced mechanism
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

clear all

%bISO = 0; % compatible with isomer lumping

chem = ckinit;

[el, sp] = ckname(chem);

%load QSSList
if exist('qsslist.txt', 'file')
    spQSS = textread('qsslist.txt', '%s');
else
    spQSS = [];
end
QSSList = ckfindsp(spQSS, chem);
NQ = length(QSSList);

if length(QSSList) ~= length(spQSS)
    error('not all QSS species found in the detailed mechanism');
end


finp = sprintf('chem.red%d.inp', chem.KK-NQ);
fid = fopen(finp, 'w');
if fid<0
    error('error writing %s', finp);
end


fprintf(fid, '!%d-species reduced mechanism\n', chem.KK-NQ);
fprintf(fid, '!genrated by: Tianfeng Lu, tlu@engr.uconn.edu\n');
fprintf(fid, '!%s\n', date);

% element section
fprintf(fid, '\n');
fprintf(fid, 'ELEMENTS\n');

% get element names
NCF = ckncf(chem);
NCF = max(NCF, [], 2);
IELEM = find(NCF);

for i=IELEM
    fprintf(fid, '%s ', el{i});
end

fprintf(fid, '\nEND\n');


% species section
fprintf(fid, '\n');
fprintf(fid, 'SPECIES');

spNQSS = setdiff(sp, spQSS);
ISP = ckfindsp(spNQSS, chem);
ISP = sort(ISP, 'ascend');

for i=1:length(ISP)
    if mod(i, 4)==1
        fprintf(fid, '\n');
    end
    fprintf(fid, '%-16s\t', sp{ISP(i)});
end

fprintf(fid, '\nEND\n');

fprintf(fid, '\n');
fprintf(fid, 'REACTIONS\n');
fprintf(fid, 'END\n');


fclose(fid);
