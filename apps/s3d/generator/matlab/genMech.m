% This script automatically generate the fortran subroutines for specified
% QSS species
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

clear all


bISO = 0; % compatible with isomer lumping

chem = ckinit;

%load the list of QSS species
if exist('qsslist.txt', 'file')
    spQSS = textread('qsslist.txt', '%s');
else
    spQSS = [];
end
QSSList = ckfindsp(spQSS, chem);

if length(QSSList) ~= length(spQSS)
    error('not all QSS species found in the detailed mechanism');
end

% load the list of stiff species
if exist('stiflist.txt', 'file')
    spSTIF = textread('stiflist.txt','%s');
else
    spSTIF = [];
end
STIFList = ckfindsp(spSTIF, chem);


% generate CKWYP.F
fid = fopen('ckwyp.f', 'w');
if fid<0
    error('error writing ckwyp.f');
end

did = fopen('s3dgen.txt','w');
if did<0
    error('error writing s3dgen.txt');
end

genCKWYP(fid, chem, QSSList, bISO);
genYTCP(fid, chem, QSSList);
genRATT(fid, chem);
genRDSMH(fid, chem);
genRATX(fid, chem, QSSList);
genRDOT(fid, chem, QSSList);
if ~isempty(QSSList)
data = load('QSS.mat'); % load QSS species data
genQSSA(fid, data, chem, did);
end
if ~isempty(STIFList)
genSTIF(fid, chem, QSSList, STIFList, did); 
end

fclose(fid);
fclose(did);

genCKInp;
