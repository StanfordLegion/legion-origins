function [nu] = cknu(chem)
%
% function [nu] = cknu(chem)
%  Purpose:  get forward stoichiometric coefficient matrix 
%  Input:  
%        chem: chemkin workspace 
%  Output: 
%        NU
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

comn = ckcomn(chem);

MXSP = comn.MXSP;
IcNU = comn.IcNU;
IcNK = comn.IcNK;

KK = chem.KK;
II = chem.II;

nu = zeros(KK, II);

for N = 1:MXSP
    for I = 1:II
        K = chem.ICKWRK(IcNK + (I-1)*MXSP + N - 1);
        if (K ~= 0) 
            NU= chem.ICKWRK(IcNU + (I-1)*MXSP + N -1);
            nu(K,I) = nu(K,I) + NU;
        end
    end
end

return
end
