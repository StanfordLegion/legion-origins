%
% function genRATT(fid, chem)
%
%   purpose: generate subroutine RATT
%       fid: file id to print to
%       chem: the skeletal or detailed mechanism before QSSA
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genRATT(fid, chem)


comn = ckcomn(chem);
% these reaction types are not supported
if comn.NFT1 || comn.NJAN || comn.NLAN || comn.NEIM || comn.NRLT || comn.NRNU
    error('RATT: type of reaction not supported');
end

fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');

fprintf(fid, '      SUBROUTINE RATT (T, RF, RB, RKLOW)\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      PARAMETER (RU=8.31451D7, SMALL=1.D-200, PATM=1.01325D6)\n');
fprintf(fid, '      DIMENSION RF(*), RB(*), RKLOW(*)\n');

% find reactions requiring equilibrium constants
IcNS = comn.IcNS;
NREV = comn.NREV;
IcRV = comn.IcRV;
% whether the reaction is reversible
IREV = chem.ICKWRK(IcNS:(IcNS+chem.II-1));
I = chem.ICKWRK(IcRV:(IcRV+NREV-1));
IREV(I) = 0;
I = find(IREV>0);
% find participating species
S = cknu(chem);
S = S(:, I);
if isempty(S), return, end
I = max(abs(S), [], 2)~=0;
I = find(I);
if isempty(I)
    IMAX = 0;
else
    IMAX = max(I);
end

if IMAX
fprintf(fid, '      DIMENSION SMH(%d), EG(%d)\n', IMAX, IMAX);
end

fprintf(fid, 'C\n');
fprintf(fid, '      ALOGT = LOG(T)\n');
fprintf(fid, '      TI = 1D0/T\n');
fprintf(fid, '      TI2 = TI*TI\n');
fprintf(fid, 'C\n');
if IMAX
    fprintf(fid, '      CALL RDSMH (T, SMH)\n');
    for i=1:length(I)
        fprintf(fid, '      EG(%d) = EXP(SMH(%d))\n', I(i), I(i));
    end
    fprintf(fid, '      PFAC1 = PATM / (RU*T)\n');
    fprintf(fid, '      PFAC2 = PFAC1*PFAC1\n');
    fprintf(fid, '      PFAC3 = PFAC2*PFAC1\n');
    fprintf(fid, 'C\n');
    fprintf(fid, 'C\n');
end

R = ckreac(chem);
S = cknu(chem);

% reaction specified with A B E is marked 1 
IABEF = zeros(chem.II,1);
IABER = zeros(chem.II,1);

for i=1:chem.II
    r = R{i};
    
    % search for a reaction with idenitical B & E
    dup = 0;
    for m=1:i-1
        if r.RB==0 && r.RE==0, break; end
        if IABEF(m)
            if r.RA==R{m}.RA && r.RB==R{m}.RB && r.RE==R{m}.RE
                fprintf(fid, '      RF(%d) = RF(%d)\n', i, m);
                dup = 1;
                break
            elseif r.RB==R{m}.RB && r.RE==R{m}.RE
                fprintf(fid, '      RF(%d) = %s*RF(%d)\n', i, fmtG(r.RA/R{m}.RA), m);
                dup = 1;
                break
            end
        end
        
        if IABER(m)
            if r.RA==R{m}.RAR && r.RB==R{m}.RBR && r.RE==R{m}.RER
                fprintf(fid, '      RF(%d) = RB(%d)\n', i, m);
                dup = 1;
                break
            elseif r.RB==R{m}.RBR && r.RE==R{m}.RER
                fprintf(fid, '      RF(%d) = %s*RB(%d)\n', i, fmtG(r.RA/R{m}.RAR), m);
                dup = 1;
                break
            end
        end
    end
    
    if ~dup
        IABEF(i) = 1;
        if r.RA == 0
            fprintf(fid, '      RF(%d) = 0D0\n', i);
            IABEF(i) = 0;
        elseif r.RB==0 && r.RE==0
            fprintf(fid, '      RF(%d) = %s\n', i, fmtG(r.RA));
            IABEF(i) = 0;
        elseif r.RB==0
            fprintf(fid, '      RF(%d) = EXP(%s %s*TI)\n', i, fmtG(log(r.RA)), fmtG(-r.RE, 1) );
        elseif r.RE==0
            fprintf(fid, '      RF(%d) = EXP(%s %s*ALOGT)\n', i, fmtG(log(r.RA)), fmtG(r.RB, 1) );
        else
            fprintf(fid, '      RF(%d) = EXP(%s %s*ALOGT %s*TI)\n', i, fmtG(log(r.RA)), fmtG(r.RB, 1), fmtG(-r.RE, 1) );
        end
    end

    if ~r.IREV
        fprintf(fid, '      RB(%d) = 0D0\n', i);
    elseif r.RABE
        % reverse rate specified

        dup = 0;
        for m=1:i-1
            if r.RBR==0 && r.RER==0, break; end
            if IABEF(m)
                if r.RAR==R{m}.RA && r.RBR==R{m}.RB && r.RER==R{m}.RE
                    fprintf(fid, '      RB(%d) = RF(%d)\n', i, m);
                    dup = 1;
                    break
                elseif r.RBR==R{m}.RB && r.RER==R{m}.RE
                    fprintf(fid, '      RB(%d) = %s*RF(%d)\n', i, fmtG(r.RAR/R{m}.RA), m);
                    dup = 1;
                    break
                end
            end
            
            if IABER(m)
                if r.RAR==R{m}.RAR && r.RBR==R{m}.RBR && r.RER==R{m}.RER
                    fprintf(fid, '      RB(%d) = RB(%d)\n', i, m);
                    dup = 1;
                    break
                elseif r.RBR==R{m}.RBR && r.RER==R{m}.RER
                    fprintf(fid, '      RB(%d) = %s*RB(%d)\n', i, fmtG(r.RAR/R{m}.RAR), m);
                    dup = 1;
                    break
                end
            end
        end
        
        if ~dup
            IABER(i) = 1;
            if r.RAR == 0
                fprintf(fid, '      RB(%d) = 0D0\n', i);
                IABER(i) = 0;
            elseif r.RBR==0 && r.RER==0
                fprintf(fid, '      RB(%d) = %s\n', i, fmtG(r.RAR));
                IABER(i) = 0;
            elseif r.RBR==0
                fprintf(fid, '      RB(%d) = EXP(%s %s*TI)\n', i, fmtG(log(r.RAR)), fmtG(-r.RER, 1) );
            elseif r.RER==0
                fprintf(fid, '      RB(%d) = EXP(%s %s*ALOGT)\n', i, fmtG(log(r.RAR)), fmtG(r.RBR, 1) );
            else
                fprintf(fid, '      RB(%d) = EXP(%s %s*ALOGT %s*TI)\n', i, fmtG(log(r.RAR)), fmtG(r.RBR, 1), fmtG(-r.RER, 1) );
            end
            
        end
    else
        % need equilbrium constant
        s = S(:, i);
        
        fprintf(fid, '      EQK = ');
        
        i0 = 0;
        for m = 1:length(s)
            if s(m)<=0, continue; end
            sm = s(m);
            for j=1:sm
                if i0==0
                    fprintf(fid, 'EG(%d)', m);
                else
                    fprintf(fid, '*EG(%d)', m);
                end
                i0 = i0 + 1;
            end
        end
        for m = 1:length(s)
            if s(m)>=0, continue; end
            sm = -s(m);
            for j=1:sm
                fprintf(fid, '/EG(%d)', m);
            end
        end
        
        ssum = sum(s);
        switch ssum
            case {0}
                fprintf(fid, '\n');
            case {1, 2 ,3}
                fprintf(fid, '*PFAC%d\n', ssum);
            case {-1, -2, -3}
                fprintf(fid, '/PFAC%d\n', -ssum);
            otherwise
                fprintf(fid, '*PFAC1**(%+d)\n', ssum);
        end
        
        fprintf(fid, '      RB(%d) = RF(%d) / MAX(EQK, SMALL)\n', i, i);
        
        
    end        
end

fprintf(fid, 'C\n');

NFAL = comn.NFAL;
NcFL = comn.NcFL;
NFAR = comn.NFAR;
FPAR = chem.RCKWRK(NcFL:NcFL+NFAR*NFAL-1);
FPAR = reshape(FPAR, NFAR, NFAL);

for i=1:NFAL
    RAF = FPAR(1, i);
    RBF = FPAR(2, i);
    REF = FPAR(3, i);
    
    if RAF == 0
        fprintf(fid, '      RKLOW(%d) = 0D0\n', i);
    elseif RBF==0 && REF==0
        fprintf(fid, '      RKLOW(%d) = %s\n', i, fmtG(RAF));
    elseif RBF==0
        fprintf(fid, '      RKLOW(%d) = EXP(%s %s*TI)\n', i, fmtG(log(RAF)), fmtG(-REF, 1) );
    elseif REF==0
        fprintf(fid, '      RKLOW(%d) = EXP(%s %s*ALOGT)\n', i, fmtG(log(RAF)), fmtG(RBF, 1) );
    else
        fprintf(fid, '      RKLOW(%d) = EXP(%s %s*ALOGT %s*TI)\n', i, fmtG(log(RAF)), fmtG(RBF, 1), fmtG(-REF, 1) );
    end
    
end

fprintf(fid, 'C\n');
fprintf(fid, '      END\n');

end
