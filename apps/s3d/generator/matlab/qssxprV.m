%function xpr = qssxprV(G, v)
% generate explicit expression of qss species for species group v
% vectorized version
%   G: the ADJ matrix of a graph
%   v: array of species list strongly coupled
%   xpr: qss expressions, string array
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%


function [xpr, AIJ] = qssxprV(G, v)
N = length(v);

% initilization
xpr = [];
AIJ = [];

% termination condition
if N==0 
    return
end

x = v(N);
v2 = v(1:N-1);

% save the to and from link of x
xto = G(x, v2);

% pre recusion expression
for i=1:length(v2)
    bdiag = 0;
    strdiag = 'DEN(:VL) = 1D0';
    y = v2(i);
    if G(y, x)==0 
        continue
    end % not affected
    str = sprintf('A%d_0(:VL) = A%d_0(:VL) + A%d_%d(:VL)*A%d_0(:VL)', y, y, y, x, x);
    %AIJ = [AIJ; y 0];
    
    xpr = [xpr; {str}];
    for j = 1:length(v2)
        z = v2(j);
        if G(x,z)==0 
            continue
        end % note affected
        if z==y % need to update DEN
            strdiag = sprintf('%s -A%d_%d(:VL)*A%d_%d(:VL)', strdiag, y, x, x, y);
            bdiag = 1;
            continue
        end
        if G(y,z)==0
            str = sprintf('A%d_%d(:VL) = A%d_%d(:VL)*A%d_%d(:VL)', y,z, y,x, x,z);
            AIJ = [AIJ; y z];
        else
            str = sprintf('A%d_%d(:VL) = A%d_%d(:VL) + A%d_%d(:VL)*A%d_%d(:VL)', y,z, y,z, y,x, x,z);
            %AIJ = [AIJ; y z];
        end
        G(y,z) = 1; % now there is an edge y->z
        xpr = [xpr; str];
    end
    
    if bdiag==0 
        continue
    end % G(y,y) not changed
    xpr = [xpr; {strdiag}];
    % update Ai_0
    str = sprintf('A%d_0(:VL) = A%d_0(:VL)/MAX(DEN(:VL), SMALL)', y, y);
    xpr = [xpr; str];
    %AIJ = [AIJ; y 0];

    % update Ai_j
    for j=1:length(v2)
        z = v2(j);
        if G(y,z)==0 
            continue
        end
        str = sprintf('A%d_%d(:VL) = A%d_%d(:VL)/MAX(DEN(:VL), SMALL)', y, z, y, z);
        xpr = [xpr; str];
        %AIJ = [AIJ; y z];
    end
end

% recursion
% eliminate the variable in G, no one points to x now
G(v2, x) = 0;
[xpr1, AIJ1] = qssxprV(G, v2);
xpr = [xpr; xpr1];
AIJ = [AIJ; AIJ1];

% post recursion expression
I = find(xto>0);
str = sprintf('XQ(:VL,%d) = A%d_0(:VL)', x, x);
for i=1:length(I)
    str = sprintf('%s \n     *      +A%d_%d(:VL)*XQ(:VL,%d)', str, x, v2(I(i)), v2(I(i)));
end
xpr = [xpr; {str}];

