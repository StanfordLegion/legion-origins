%
%   function genSTIF(fid, chem, KQSS, KFAST)
%
%   purpose: generate subroutine STIF
%       fid: file id to print to
%       chem: the skeletal or detailed mechanism before QSSA
%       KQSS: array of IDs for the QSS species
%       KFAST: array of IDs for the QSS species
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genSTIF(fid, chem, KQSS, KFAST, did)

KK = chem.KK;
II = chem.II;
WT = ckwt(chem);

[m_names, k_names] = ckname(chem);

KQSS = sort(KQSS, 'ascend');
KNQS = setdiff(1:chem.KK, KQSS);
KNQS = sort(KNQS, 'ascend');

KFAST = setdiff(KFAST, KQSS); % do not include global QSS species

%species index in the reduced mechanism
[KFAST, IA, KFAST2] = intersect(KFAST, KNQS);
[KFAST, I] = sort(KFAST, 'ascend');
KFAST2 = KFAST2(I);

ID = zeros(KK,1);
ID(KFAST) = 1;

nu = cknu(chem);
nuf = cknuf(chem);
nur = nu-nuf;
nuf = -nuf;


nuf = nuf(KFAST, :);
nur = nur(KFAST, :);

% find trivial reverse rates
IR = ones(II,1);
[ITHB, IREV1] = ckitr(chem);
I = find(IREV1<0);
IR(I) = 0;
[IREV2, A, B, E] = ckaber(chem);
I = find(A==0);
IR(IREV2(I)) = 0;

% generate STIF subroutine
fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');
fprintf(fid, '      SUBROUTINE STIF(RF, RB, DIFF, DT, C)\n');
fprintf(fid, '        IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      DIMENSION RF(*), RB(*), DIFF(*), C(*)\n');
%fprintf(fid, '      DATA TC/%G/\n', cr);
fprintf(fid, '      TC = 1./DT\n');
fprintf(did,'%d\n',length(KFAST));
for n = 1:length(KFAST)
    k = KFAST(n);
    k2 = KFAST2(n);
    toksD = [{'DDOT='}];
    toksC = [{'CDOT='}];
    fprintf(did,'%d %d %s\n',k, k2, k_names{k});
    didfd = [];
    didfc = [];
    didrd = [];
    didrc = [];
    for i = 1:II
      if nuf(n,i)>0
        didfd = [didfd; [i, nuf(n,i)]];
        if IR(i)~=0
          didrc = [didrc; [i, nuf(n,i)]];
        end
      end
      for j=1:nuf(n,i)
        tok = sprintf('+RF(%d)', i);
        toksD = [toksD; {tok}];
        if IR(i)~=0
            tok = sprintf('+RB(%d)', i);
            toksC = [toksC; {tok}];
        end
      end
      if nur(n,i)>0
        if IR(i)~=0
          didrd = [didrd; [i, nur(n,i)]]; 
        end
        didfc = [didfc; [i, nur(n,i)]];
      end
      for j=1:nur(n,i)
          if IR(i)~=0
            tok = sprintf('+RB(%d)', i);
            toksD = [toksD; {tok}];
          end
          tok = sprintf('+RF(%d)', i);
          toksC = [toksC; {tok}];
      end
    end
    fprintf(did,'%d\n',size(didfd,1));
    for i = 1:size(didfd,1)
      fprintf(did,'%d %d\n',didfd(i,1),didfd(i,2));
    end
    fprintf(did,'%d\n',size(didfc,1));
    for i = 1:size(didfc,1)
      fprintf(did,'%d %d\n',didfc(i,1),didfc(i,2));
    end
    fprintf(did,'%d\n',size(didrd,1));
    for i = 1:size(didrd,1)
      fprintf(did,'%d %d\n',didrd(i,1),didrd(i,2));
    end
    fprintf(did,'%d\n',size(didrc,1));
    for i = 1:size(didrc,1)
      fprintf(did,'%d %d\n',didrc(i,1),didrc(i,2)); 
    end
    strD = fmt77(toksD);
    strC = fmt77(toksC);
    fprintf(fid, 'C\n');
    fprintf(fid, 'C     %s\n', k_names{k});
    fprintf(fid, '%s', strD);
    fprintf(fid, '      TINV=DDOT/C(%d)\n', k2);
    fprintf(fid, '      IF (TINV .GT. TC) THEN\n');
    fprintf(fid, '%s', strC);
    fprintf(fid, '      C0=C(%d)*(CDOT+DIFF(%d)/%.8f)/DDOT\n', k2, k2, WT(k));
    fprintf(fid, '      C0=C(%d)*(CDOT+DIFF(%d)/%.8f+(C(%d)-C0)/DT)/DDOT\n', k2, k2, WT(k), k2);

    fprintf(fid, '      R=C0/C(%d)\n', k2);
    % update RF and RB
    for i = 1:II
      for j=1:nuf(n,i)
        fprintf(fid, '      RF(%d)=RF(%d)*R\n', i, i);
      end
      for j=1:nur(n,i)
          if IR(i)~=0
            fprintf(fid, '      RB(%d)=RB(%d)*R\n', i, i);
          end
      end
    end

    fprintf(fid, '      ENDIF\n');
end
fprintf(fid, '      END\n');

end

