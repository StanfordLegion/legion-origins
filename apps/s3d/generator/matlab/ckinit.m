function [chem] = ckinit()
% function [chem] = ckinit()
%  Purpose:  initialize CHEMKIN workspace 
%
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%
D = load('chem.mat');
chem = D.chem;

end
