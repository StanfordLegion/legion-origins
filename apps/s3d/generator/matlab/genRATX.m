%
% function genRATX(fid, chem, QSSList)
% function genRATX(fid, chem)
%
%   purpose: generate subroutine RATX
%       fid: file id to print to
%       chem: the skeletal or detailed mechanism before QSSA
%       QSSList: array of QSS species, integers
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genRATX(fid, chem, QSSList)

if nargin<3, QSSList = []; end

comn = ckcomn(chem);
if comn.NFT1 || comn.NJAN || comn.NLAN || comn.NEIM || comn.NRLT || comn.NRLT 
    error('RATT: type of reaction not supported');
end

fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');

fprintf(fid, '      SUBROUTINE RATX (T, C, RF, RB, RKLOW)\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      PARAMETER (SMALL = 1D-200)\n');
fprintf(fid, '      DIMENSION C(*), RF(*), RB(*), RKLOW(*)\n');
fprintf(fid, 'C\n');
fprintf(fid, '      ALOGT = LOG(T)\n');

if comn.NTHB
fprintf(fid, '      CTOT = 0.0\n');
fprintf(fid, '      DO K = 1, %d\n', chem.KK-length(QSSList));
fprintf(fid, '         CTOT = CTOT + C(K)\n');
fprintf(fid, '      ENDDO\n');
fprintf(fid, 'C\n');
end

R = ckreac(chem);
SF = cknuf(chem);
SR = cknu(chem)-SF;
SF = -SF;

% generate the index of a species id to that in C(*)
I = ones(chem.KK,1);
I(QSSList) = 0;
I = find(I);
IC = zeros(chem.KK,1);
IC(I) = 1:length(I);

% generate IFAL list
NFAL = comn.NFAL;
IcFL = comn.IcFL;
I = chem.ICKWRK(IcFL:IcFL+NFAL-1);
IFAL = zeros(chem.II, 1);
IFAL(I) = 1:NFAL; % reverse index of fall off reactions

for i=1:chem.II
    r = R{i};
    
    %fprintf(fid, 'C\n');
    
    % if third body is needed
    switch r.ITHB
        case -1 % no third body
        case 0 % third body without enhancement
            fprintf(fid, '      CTB = CTOT\n');
        otherwise % N third body enhancement
            fprintf(fid, '      CTB = CTOT');
            nCounter=0;
            for m=1:r.NTBS
                k = r.NKTB(m);
                
                if any(QSSList==k), continue, end
                
                aNM = r.AIK(m) - 1;
                if aNM == 0, continue; end
                
                if nCounter>=4
                    fprintf(fid, '\n     * ');
                    nCounter=0;
                end
                
                switch aNM
                    case 1
                        fprintf(fid, '+C(%d)', IC(k));
                    case -1
                        fprintf(fid, '-C(%d)', IC(k));
                    otherwise
                        fprintf(fid, '%s*C(%d)', fmtG(aNM, 1), IC(k));
                end
                nCounter = nCounter+1;
            end
            fprintf(fid, '\n');
    end
    
    % if fall off reaction
    if r.IFAL
        
        if r.KFAL==0 % type of bath gas: CTB
            fprintf(fid, '      PR = RKLOW(%d) * CTB / RF(%d)\n', IFAL(i), i);
        elseif ~any(QSSList==r.KFAL) % type of bath gas: species r.KFAL
            fprintf(fid, '      PR = RKLOW(%d) * C(%d)/ RF(%d)\n', IFAL(i), IC(r.KFAL), i);
        else %if QSS species is third body, assume Rf=Rlow
            fprintf(fid, '      PR = 0.0\n');
        end
        
        fprintf(fid, '      PCOR = PR / (1.0 + PR)\n');
        
        % switch r.IFOP
        % 0 - No fall-off behavior
        % 1 - fall-off behavior - Lindeman form (3 parameters)
        % 2 - fall-off behavior - SRI form      (8 parameters)
        % 3 - fall-off behavior - Troe form     (6 parameters)
        % 4 - fall-off behavior - Troe form     (7 parameters)
        
        if r.IFOP==3 && r.FPAR(5)<1.0E-2 && r.FPAR(6)>1.0E6
            % 6-Parameter TROE, FC=CONST
            if abs(r.FPAR(4)-1)<1E-9
                % Lindeman
            else
                fprintf(fid, '      PRLOG = LOG10(MAX(PR,SMALL))\n');
                fprintf(fid, '      FC = (PRLOG %s)\n     *     /(%s -0.14D0*(PRLOG %s))\n', ...
                    fmtG(-0.4-0.67*log10(r.FPAR(4)), 1), ...
                    fmtG(0.75 - 1.27*log10(r.FPAR(4))), ...
                    fmtG(-0.4 - 0.67*log10(r.FPAR(4)), 1));
                
                fprintf(fid, ...
                    '      FC = EXP(%s /(1.0D0 + FC*FC))\n', fmtG(log(r.FPAR(4))));
                
                fprintf(fid,'      PCOR = FC * PCOR\n');
            end
            
        elseif r.IFOP>1
            fprintf(fid, '      PRLOG = LOG10(MAX(PR,SMALL))\n');
            if r.IFOP==2 % 8-parameter SRI
                fprintf(fid, '      XP = 1.0/(1.0 + PRLOG*PRLOG)\n');
                
                if r.FPAR(5) == 0
                    fprintf(fid, '      FC = (%s ', fmtG(r.FPAR(4)));
                else
                    fprintf(fid, '      FC = (%s*EXP(%s/T) ', fmt(r.FPAR(4)), fmtG(-r.FPAR(5), 1));
                end
                
                if r.FPAR(6)>0
                    fprintf(fid, '+ EXP(-T/%s)', fmtG(r.FPAR(6)));
                elseif r.FPAR(6)<0
                    fprintf(fid, '+ EXP(T/%s)', fmtG(-r.FPAR(6)));
                end
                
                fprintf(fid, ')**XP\n');
                
                if r.FPAR(7)~=1 || r.FPAR(8)~=0
                    fprintf(fid, '     *    ');
                end
                
                if r.FPAR(7)==1
                    %
                elseif r.FPAR(7)>=0
                    fprintf(fid, '* %s', fmtG(r.FPAR(7)));
                else
                    fprintf(fid, '* (%s)', fmtG(r.FPAR(7)));
                end
                
                if r.FPAR(8)>0
                    fprintf(fid, ' * T**%s', fmtG(r.FPAR(8)));
                elseif r.FPAR(8)<0
                    fprintf(fid, ' * T**(%s)', fmtG(r.FPAR(8)));
                end
                
                if r.FPAR(7)~=1 || r.FPAR(8)~=0
                    fprintf(fid, '\n');
                end
                
            else %6,7-Parameter TROE
                
                fprintf(fid, '      FCENT = %s', fmtG(1.0-r.FPAR(4)));
                
                if r.FPAR(5)>=0
                    fprintf(fid,'*EXP(-T/%s)', fmtG(r.FPAR(5)));
                else
                    fprintf(fid,'*EXP(T/%s)', fmtG(-r.FPAR(5)));
                end
                
                if r.FPAR(4)~=0
                if r.FPAR(4)>=0
                    fprintf(fid, ' + %s', fmtG(r.FPAR(4)));
                else
                    fprintf(fid, ' - %s', fmtG(-r.FPAR(4)));
                end
                
                if r.FPAR(6)>=0
                    fprintf(fid, '*EXP(-T/%s)', fmtG(r.FPAR(6)));
                else
                    fprintf(fid, '*EXP(T/%s)', fmtG(-r.FPAR(6)));
                end
                end
                
                if r.IFOP==4 %7-parameter TROE
                    if r.FPAR(7)>=0
                        fprintf(fid, '\n     *     + EXP(-%s/T)', fmtG(r.FPAR(7)));
                    else
                        fprintf(fid, '\n     *     + EXP(%s/T)', fmtG(-r.FPAR(7)));
                    end
                    
                end
                fprintf(fid, '\n');

                fprintf(fid, '      FCLOG = LOG10(MAX(FCENT,SMALL))\n');
                fprintf(fid, '      XN    = 0.75 - 1.27*FCLOG\n');
                fprintf(fid, '      CPRLOG= PRLOG - (0.4 + 0.67*FCLOG)\n');
                fprintf(fid, '      FLOG = FCLOG/(1.0 + (CPRLOG/(XN-0.14*CPRLOG))**2)\n');
                fprintf(fid, '      FC = 10.0**FLOG\n');
                fprintf(fid, '      PCOR = FC * PCOR\n');
            end
        end
            
        fprintf(fid, '      RF(%d) = RF(%d) * PCOR\n', i, i);
        if r.IREV
            if r.RABE
                if r.RAR == 0
                else
                    fprintf(fid, '      RB(%d) = RB(%d) * PCOR\n', i, i);
                end
            else
                fprintf(fid, '      RB(%d) = RB(%d) * PCOR\n', i, i);
            end
        end
    end
    
    % multiply reaction rate by CTB or non-QSS species concentrations
    
    bfwd = 0;
    brev = 0;
    bthb = 0;
    if r.ITHB>=0 && ~r.IFAL
        bthb = 1;
        bfwd = 1;
        if r.IREV
            if r.RABE
                if r.RAR~=0
                    brev = 1;
                end
            else
                brev = 1;
            end
        end
    end
    
    sf = SF(:, i);
    lf = [];
    for k=1:chem.KK
        if ~sf(k), continue; end
        if any(QSSList==k), continue; end % skip QSS species
        for m=1:sf(k)
            lf = [lf k]; %#ok<AGROW>
        end
    end
    if ~isempty(lf), bfwd = 1; end
        
    sr = SR(:, i);
    lr = [];
    for k=1:chem.KK
        if ~r.IREV, break, end
        if r.RABE
            if r.RAR==0
                break
            end
        end
        if ~sr(k), continue; end
        if any(QSSList==k), continue; end % skip QSS species
        for m=1:sr(k)
            lr = [lr k];  %#ok<AGROW>
        end
    end
    if ~isempty(lr), brev = 1; end
   
    if bfwd
        fprintf(fid, '      RF(%d) = RF(%d)', i, i);
        if bthb
            fprintf(fid, '*CTB');
        end
        for m=1:length(lf)
            fprintf(fid, '*C(%d)', IC(lf(m)));
        end
        fprintf(fid, '\n');
    end
    if brev
        fprintf(fid, '      RB(%d) = RB(%d)', i, i);
        if bthb
            fprintf(fid, '*CTB');
        end
        for m=1:length(lr)
            fprintf(fid, '*C(%d)', IC(lr(m)));
        end
        fprintf(fid, '\n');
    end
     
end

fprintf(fid, 'C\n');
fprintf(fid, '      END\n');

end
