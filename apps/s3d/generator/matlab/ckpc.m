function [P] = ckpc(T, C, chem)
%
% function [P] = ckpc(T, C, chem)
%  Purpose:  return pressure for given T and C
%  Input:  
%        T:   temperature
%        C:   mole concentration 
%        chem: chemkin workspace 
%  Output: 
%        P:   pressure 
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

comn = ckcomn(chem);
NcRU = comn.NcRU;

P    = chem.RCKWRK(NcRU) * T * sum(C);
return
end
