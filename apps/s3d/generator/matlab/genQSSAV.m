%   function genQSSAV(fid, data, chem)
%   a function to generate the vectorized QSSA subroutine
%       fid: file ID
%       data: QSSGraph data to be loaded from QSS.mat
%       chem: chemkin workspace returned from ckinit
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genQSSAV(fid, data, chem)

% retrieve graph data
G = data.G;
G0 = data.G0;
G2 = data.G2;
grps = data.grps;
topo = data.topo;

% 
g = unique(grps);
ng = length(g);
[Y, I] = sort(topo, 'ascend');
g_topo = g(I);


% also find the sequence of elimination of nodes in each group
xpr = [];
AIJs = [];
I_done = [];
for i=1:length(g)
    I = find(grps == g_topo(i));
    %fprintf(1, '\ngroup %d:\n', g(i))

    if(length(I)>1)
        GC = G2(I, I);
        for j=1:size(GC,1)
            % normalize the jth column of GC
            GC(:, j) = GC(:, j)/sum(GC(:, j));
        end
        [V, D] = eig(GC);
        DD = diag(D);
        [DD, DI] = sort(DD, 'descend');
        V1 = V(:,DI(1)); % principal eigenvalue/eigenvector
        if(min(V1)<0) V1 = -V1; end
        [V1, II] = sort(V1, 'descend');
        clear V D DD DI 

        I = I(II);
        %for k=1:size(GC, 1)
        %    fprintf(1, '%2d, %8s, %.3f\n', I(k), char(G.k_names(I(k))), V1(k));
        %end
    else
        % only a single node
        %fprintf(1, '%2d, %8s\n', I(1), char(G.k_names(I(1))));
    end
    
    % now I contains the index of strongly connected component to solve
    [xpr1, AIJ1] =  qssxprV(G2, I);
    xpr = [xpr; {''}; xpr1];
    AIJs = [AIJs; AIJ1];
    
    % now merge the terms involving species in this group to the An_0 terms
    strs = [];
    I_done = [I_done; I];
    I2 = setdiff([1:length(grps)]', I_done);
    for j=1:length(I2)
        u = I2(j);
        if(max(G2(u, I))==0) 
            continue
        end
        str = sprintf('A%d_0(:VL) = A%d_0(:VL)', u, u);
        for k=1:length(I)
            v = I(k);
            if G2(u,v)==0
                continue;
            end
            str = sprintf('%s +A%d_%d(:VL)*XQ(:VL,%d)', str, u, v, v);
        end
        strs = [strs; {str}];
    end
    xpr = [xpr; strs];
end



% create a FORTRAN subroutine for the QSS solutions
N = size(G0,1);
GD = G0-G2;

% QSS species list
QSS = G.QSS;


% stoichiometric coefficients
nu = cknu(chem);
nuf = cknuf(chem);
nur = nu-nuf;
nuf = -nuf;

f_mark = zeros(chem.II,1);
r_mark = zeros(chem.II,1);

% mark quadratic reactions
q_nuf = nuf(QSS, :);
q_nur = nur(QSS, :);
I = find(sum(q_nuf,1)>1);
f_mark(I) = 1;
I = find(sum(q_nur,1)>1);
r_mark(I) = 1;

% marked unimportant linear reactions 
for i=1:N
    for j=1:N
        if GD(i,j)==0
            continue
        end
        % edge i->j is trimmed
        k_i = QSS(i);
        k_j = QSS(j);
        for k=1:chem.II
            if( nur(k_i,k)>0 && nuf(k_j,k)>0 )
                f_mark(k) = 1;
            end
            if( nuf(k_i,k)>0 && nur(k_j,k)>0 )
                r_mark(k) = 1;
            end
        end
    end
end

% generate fortran subroutine

% print function header
fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');

fprintf(fid, '      SUBROUTINE QSSA(VL, RF, RB, XQ)\n');
fprintf(fid, '      USE chemkin_m, only : MAXVL\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      DIMENSION RF(MAXVL,*),RB(MAXVL,*),XQ(MAXVL,*)\n');
fprintf(fid, '      DIMENSION DEN(MAXVL)\n');

for i=1:N
    % define array for Aij
    fprintf(fid, '      DIMENSION A%d_0(MAXVL)\n', i);
    for j=1:N
        if G2(i, j)>0
            fprintf(fid, '      DIMENSION A%d_%d(MAXVL)\n', i, j);
        end
    end
end

for i=1:size(AIJs,1)
    fprintf(fid, '      DIMENSION A%d_%d(MAXVL)\n', AIJs(i,1), AIJs(i,2));
end

fprintf(fid, '        INTEGER VL\n');
fprintf(fid, '      PARAMETER (SMALL = 1.D-200)\n');
fprintf(fid, 'C\n');

% trim the unimportant reactions in RF and RB
for i=1:chem.II
    if f_mark(i)==1
        fprintf(fid, '      RF(:VL,%d) = 0D0\n', i);
    end
    if r_mark(i)==1
        fprintf(fid, '      RB(:VL,%d) = 0D0\n', i);
    end
end
fprintf(fid, 'C\n');


% generate the terms for Aij
AIJ = cell(N, N);
A0 = cell(N,1);
for i=1:chem.II
    qf = nuf(QSS, i);
    qr = nur(QSS, i);
    If = find(qf>0);
    Ir = find(qr>0);
    
    % forward reaction
    if f_mark(i)==0
        if length(If)==0 % no QSS species on LHS
            % RF should goto the A0 terms for QSS species on RHS
            for j=1:length(Ir)
                for k=1:qr(Ir(j))
                    A0{Ir(j)} = [A0{Ir(j)}; {sprintf('+RF(:VL,%d)', i)}];
                end
            end
        else % 1 QSS species on LHS
            % should goto the Aij terms
            for j=1:length(Ir) % RHS might have more than one QSS species
                for k=1:qr(Ir(j)) % each QSS species might have coefficient > 1
                    AIJ{Ir(j), If} = [AIJ{Ir(j), If}; {sprintf('+RF(:VL,%d)', i)}];
                end
            end
        end
    end
    
    % reverse reaction
    if r_mark(i)==0
        if length(Ir)==0 % no QSS species on RHS
            % RB should goto the A0 terms for QSS species on LHS
            for j=1:length(If)
                for k=1:qf(If(j))
                    A0{If(j)} = [A0{If(j)}; {sprintf('+RB(:VL,%d)', i)}];
                end
            end
        else
            % should goto the Aij terms
            for j=1:length(If)
                for k=1:qf(If(j))
                    AIJ{If(j), Ir} = [AIJ{If(j), Ir}; {sprintf('+RB(:VL,%d)', i)}];
                end
            end
        end
    end
end

% print the expression of coefficients
% get reaction information
%reac = ckreac(chem);


for i=1:N
    id = QSS(i); % get QSS species id
    fprintf(fid, 'C     %s\n', G.k_names{i});

    % check irreversible reaction or zero rate reactions, skipped for now
    % reac = ckreac(chem);
    % ... , to be filled in later
    
    % generate expressions for the coefficients of XQ
    % find all the reactions with the kth QSS species
    IKF = find(nuf(id,:)>0);
    IKR = find(nur(id,:)>0);
    IKF_X = find(f_mark>0);
    IKR_X = find(r_mark>0);
    IKF = setdiff(IKF, IKF_X);
    IKR = setdiff(IKR, IKR_X);
    trms = [];
    for j=1:length(IKF)
        trms = [trms; {sprintf('+RF(:VL,%d)',IKF(j))}];
    end
    for j=1:length(IKR)
        trms = [trms; {sprintf('+RB(:VL,%d)',IKR(j))}];
    end

    fprintf(fid, '      DEN(:VL) = ');
    for j=1:size(trms,1)
        if mod(j, 4)==0
            fprintf(fid, '\n     *  ');
        end
        fprintf(fid, '%s ', trms{j});
    end
    fprintf(fid, '\n');

    % print Aij
    fprintf(fid, '      A%d_0(:VL) = ( ', i);
    for j=1:size(A0{i},1)
        if mod(j, 4) == 0
            fprintf(fid, '\n     *  ');
        end
        fprintf(fid, '%s ', A0{i}{j});
    end
    %fprintf(fid, '\n');
    %fprintf(fid, '      A%d_0 = A%d_0/DEN\n', i, i);
    fprintf(fid, ')\n     *      /MAX(DEN(:VL), SMALL)\n');
    
    for j=1:N
        if G2(i, j)>0
            fprintf(fid, '      A%d_%d(:VL) = ( ', i, j);
            for k=1:size(AIJ{i, j},1)
                if mod(k, 4) == 0
                    fprintf(fid, '\n     *  ');
                end
                fprintf(fid, '%s ', AIJ{i,j}{k});
            end
            %fprintf(fid, '\n');
            %fprintf(fid, '      A%d_%d = A%d_%d/DEN\n', i, j, i, j);
            fprintf(fid, ')\n     *      /MAX(DEN(:VL), SMALL)\n');
        end
    end
end
clear id trms IKF IKR IKF_X IKR_X
fprintf(fid, 'C\n');

% print expressions
for i=1:size(xpr,1)
    str = xpr{i};
    if(length(str)==0)
        %fprintf(fid, 'C\n');
        continue
    else
        fprintf(fid, '      %s\n', str);
    end
end
fprintf(fid, 'C\n');

% print the QSS correction terms
for i=1:chem.II
    qf = nuf(QSS, i);
    qr = nur(QSS, i);
    If = find(qf>0);
    Ir = find(qr>0);
    if (f_mark(i)==0 && length(If)>0)
        fprintf(fid, '      RF(:VL,%d) = RF(:VL,%d)*XQ(:VL,%d)\n', i, i, If);
    end
    
    if(r_mark(i)==0 && length(Ir)>0)
        fprintf(fid, '      RB(:VL,%d) = RB(:VL,%d)*XQ(:VL,%d)\n', i, i, Ir);
    end
end

fprintf(fid, 'C\n');
fprintf(fid, '      END\n', str);

end

