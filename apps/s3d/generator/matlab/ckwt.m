function [WT] = ckwt(chem)
%
% function [WT] = ckwt(chem)
%  Purpose:  return molecular weights 
%  Input:  
%        chem: chemkin workspace 
%  Output: 
%        WT:  molecular weights 
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

comn = ckcomn(chem);
KK = chem.KK;

NcWT = comn.NcWT;

WT = chem.RCKWRK(NcWT:NcWT+KK-1);

return
end
