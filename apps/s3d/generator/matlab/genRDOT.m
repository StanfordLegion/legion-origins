%
% function genRDOT(fid, chem, QSSList)
% function genRDOT(fid, chem)
%
%   purpose: generate subroutine RDOT
%       fid: file id to print to
%       chem: the skeletal or detailed mechanism before QSSA
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genRDOT(fid, chem, QSSList)

if nargin<3, QSSList = []; end

% generate the index of a species id to that in C(*)
I = ones(chem.KK,1);
I(QSSList) = 0;
I = find(I);
IC = zeros(chem.KK,1);
IC(I) = 1:length(I);


fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');

fprintf(fid, '      SUBROUTINE RDOT(RF, RB, WDOT)\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      DIMENSION RF(*), RB(*), WDOT(*)\n');
fprintf(fid, 'C\n');

fprintf(fid, '      DO K = 1, %d\n', chem.KK-length(QSSList));
fprintf(fid, '         WDOT(K) = 0D0\n');
fprintf(fid, '      ENDDO\n');
fprintf(fid, 'C\n');

S = cknu(chem);
R = ckreac(chem);

for i=1:chem.II
    r = R{i};
    
    brev = 0;
    if r.IREV
        if r.RABE
            if r.RAR
                brev = 1;
            end
        else
            brev = 1;
        end
    end
    if brev
        fprintf(fid, '      ROP = RF(%d)-RB(%d)\n', i, i);
    else
        fprintf(fid, '      ROP = RF(%d)\n', i);
    end
    
    s = S(:, i);
    for k=1:chem.KK
        if ~s(k), continue, end
        if any(QSSList==k), continue, end
        switch s(k)
            case 1
                fprintf(fid, '      WDOT(%d) = WDOT(%d) +ROP\n', IC(k), IC(k));
            case -1
                fprintf(fid, '      WDOT(%d) = WDOT(%d) -ROP\n', IC(k), IC(k));
            case 2
                fprintf(fid, '      WDOT(%d) = WDOT(%d) +ROP +ROP\n', IC(k), IC(k));
            case -2
                fprintf(fid, '      WDOT(%d) = WDOT(%d) -ROP -ROP\n', IC(k), IC(k));
            otherwise
                fprintf(fid, '      WDOT(%d) = WDOT(%d) %+d*ROP\n', IC(k), IC(k), s(k));
        end
    end
end

fprintf(fid, 'C\n');
fprintf(fid, '      END\n');

end
