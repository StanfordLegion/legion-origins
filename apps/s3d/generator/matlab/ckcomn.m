function [comn] = ckcomn(chem)
%
% function [comn] = ckcomn(chem)
%  Purpose:  get structure of the common block
%  Input:  
%        chem: chemkin workspace 
%  Output: 
%        structure of the common block
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

NCMN = chem.NCOMN;
RCMN = chem.RCOMN;

comn.NMM	= NCMN(1) ;
comn.NKK	= NCMN(2);
comn.NII 	= NCMN(3) ;
comn.MXSP	= NCMN(4);
comn.MXTB	= NCMN(5);
comn.MXTP	= NCMN(6);
comn.NCP 	= NCMN(7);
comn.NCP1	= NCMN(8);
comn.NCP2	= NCMN(9);
comn.NCP2T	= NCMN(10);
comn.NPAR	= NCMN(11);
comn.NLAR	= NCMN(12);
comn.NFAR	= NCMN(13);
comn.NLAN	= NCMN(14);
comn.NFAL	= NCMN(15);
comn.NREV	= NCMN(16);
comn.NTHB	= NCMN(17);
comn.NRLT	= NCMN(18);
comn.NWL	= NCMN(19) ; 
comn.NEIM	= NCMN(20) ;
comn.NJAN	= NCMN(21);
comn.NJAR	= NCMN(22);
comn.NFT1	= NCMN(23);
comn.NF1R	= NCMN(24);
comn.NEXC	= NCMN(25);
comn.NRNU	= NCMN(26);
comn.NORD	= NCMN(27);
comn.MXORD	= NCMN(28);
comn.IcMM	= NCMN(29);
comn.IcKK	= NCMN(30);
comn.IcNC	= NCMN(31);
comn.IcPH	= NCMN(32);
comn.IcCH	= NCMN(33);
comn.IcNT	= NCMN(34);
comn.IcNU	= NCMN(35);
comn.IcNK	= NCMN(36);
comn.IcNS	= NCMN(37);
comn.IcNR	= NCMN(38);
comn.IcLT	= NCMN(39);
comn.IcRL	= NCMN(40);
comn.IcRV	= NCMN(41);
comn.IcWL	= NCMN(42);
comn.IcFL	= NCMN(43);
comn.IcFO	= NCMN(44);
comn.IcKF	= NCMN(45);
comn.IcTB	= NCMN(46);
comn.IcKN	= NCMN(47);
comn.IcKT	= NCMN(48);
comn.IcEI	= NCMN(49);
comn.IcTD	= NCMN(50);
comn.IcJN	= NCMN(51);
comn.IcF1	= NCMN(52);
comn.IcEX	= NCMN(53);
comn.IcRNU	= NCMN(54);
comn.IcORD	= NCMN(55);
comn.IcKOR	= NCMN(56);
comn.NcAW	= NCMN(57);
comn.NcWT	= NCMN(58);
comn.NcTT	= NCMN(59);
comn.NcAA	= NCMN(60);
comn.NcCO	= NCMN(61);
comn.NcRV	= NCMN(62);
comn.NcLT	= NCMN(63);
comn.NcRL	= NCMN(64);
comn.NcFL	= NCMN(65);
comn.NcKT	= NCMN(66);
comn.NcWL	= NCMN(67);
comn.NcJN	= NCMN(68);
comn.NcF1	= NCMN(69);
comn.NcEX	= NCMN(70);
comn.NcRU	= NCMN(71);
comn.NcRC	= NCMN(72);
comn.NcPA	= NCMN(73);
comn.NcKF	= NCMN(74);
comn.NcKR	= NCMN(75);
comn.NcRNU	= NCMN(76);
comn.NcKOR	= NCMN(77);
comn.NcK1	= NCMN(78);
comn.NcK2	= NCMN(79);
comn.NcK3	= NCMN(80);
comn.NcK4	= NCMN(81);
comn.NcI1	= NCMN(82);
comn.NcI2	= NCMN(83);
comn.NcI3	= NCMN(84);
comn.NcI4	= NCMN(85);

comn.SMALL  = RCMN(1);
comn.BIG    = RCMN(2);
comn.EXPARG = RCMN(3);
comn.CKMIN  = RCMN(4);

return
end
