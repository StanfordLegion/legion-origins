% function [s] = fmtG(x, bsign)
% purpose: format a double prevision number
%   x: input value
%   bsign: 1: print +sign, otherwise, do not print the +sign
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function [s] = fmtG(x, bsign)

s = sprintf('%.16E', x);
n = strfind(s,'E');
x1 = str2double(s(1:n-1));
x2 = str2double(s(n+1:length(s)));


if nargin<2, bsign = 0; end
    
if ~bsign
s = sprintf('%.9gD%g', x1, x2);
else
s = sprintf('%+.9gD%g', x1, x2);
end

end


