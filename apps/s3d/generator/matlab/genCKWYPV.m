%
%   function genCKWYPV(fid, chem, QSSList, bISO)
%   function genCKWYPV(fid, chem, QSSList)
%   function genCKWYPV(fid, chem)
%
%   purpose: generate subroutine CKWYP, vectorized version
%       fid: file id to print to
%       chem: the skeletal or detailed mechanism before QSSA
%       QSSList: array of IDs for the QSS species
%       bISO: integer: 0: no isomuer lumping, 1: with isomer lumping
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genCKWYPV(fid, chem, QSSList, bISO)

if nargin<4, bISO = []; end
if nargin<3, QSSList = []; end

if isempty(bISO), bISO = 0; end
if isempty(QSSList)
    NQSS = 0;
else
    NQSS = length(QSSList);
end

comn = ckcomn(chem);

fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');




if bISO
    
fprintf(fid, '      SUBROUTINE GETRATES  (P, T, VL, DIFF, DT, YISO, \n');
fprintf(fid, '     *                      ICKWRK, RCKWRK, WISO)\n');
fprintf(fid, '      USE chemkin_m, only : MAXVL\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      DIMENSION YISO(MAXVL,*),WISO(MAXVL,*),ICKWRK(*),RCKWRK(*)\n');
fprintf(fid, '      DIMENSION RF(MAXVL,%d),RB(MAXVL,%d),RKLOW(MAXVL,%d)\n', chem.II, chem.II, max(comn.NFAL, 1));
fprintf(fid, '      DIMENSION C(MAXVL,%d),Y(MAXVL,%d),WDOT(MAXVL,%d)\n', chem.KK-NQSS, chem.KK-NQSS, chem.KK-NQSS);
fprintf(fid, '      DIMENSION P(MAXVL),T(MAXVL)\n');
fprintf(fid, '      DIMENSION DIFF(MAXVL,*), DIF(MAXVL,%d)\n', chem.KK-NQSS);
if NQSS
fprintf(fid, '      DIMENSION XQ(MAXVL,%d)\n', NQSS);
end
fprintf(fid, '        INTEGER VL\n');
fprintf(fid, 'C\n');

fprintf(fid, '      CALL XISO(VL, YISO, Y)\n');
fprintf(fid, '      CALL XISO(VL, DIFF, DIF)\n');
fprintf(fid, '      DO N=1, %d\n', chem.KK-NQSS);
fprintf(fid, '      DIF(:VL,N) = MAX(DIF(:VL,N), 0D0)\n');
fprintf(fid, '      ENDDO\n');
fprintf(fid, '      CALL YTCP(VL, P, T, Y, C)\n');
fprintf(fid, '      CALL RATT(VL, T, RF, RB, RKLOW)\n');
fprintf(fid, '      CALL RATX(VL, T, C, RF, RB, RKLOW)\n');
if ~isempty(QSSList)
fprintf(fid, '      CALL QSSA(VL, RF, RB, XQ)\n');
end
fprintf(fid, '      CALL STIF(VL, RF, RB, DIFF, DT, C)\n');
fprintf(fid, '      CALL RDOT(VL, RF, RB, WDOT)\n');
fprintf(fid, '      CALL RISO(VL, WDOT, WISO)\n');
fprintf(fid, 'C\n');
fprintf(fid, '      END\n');

else

fprintf(fid, '      SUBROUTINE GETRATES  (P, T, VL, Y, ICKWRK, RCKWRK, WDOT)\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      DIMENSION Y(MAXVL,*),WDOT(MAXVL,*),ICKWRK(*),RCKWRK(*)\n');
fprintf(fid, '      DIMENSION RF(MAXVL,%d),RB(MAXVL,%d),RKLOW(MAXVL,%d),C(MAXVL,%d)\n', chem.II, chem.II, max(comn.NFAL, 1), chem.KK-NQSS);
fprintf(fid, '      DIMENSION P(MAXVL),T(MAXVL)\n');
fprintf(fid, '        INTEGER VL\n');

if NQSS
fprintf(fid, '      DIMENSION XQ(MAXVL,%d)\n', NQSS);
end
fprintf(fid, 'C\n');
fprintf(fid, '      CALL YTCP(VL, P, T, Y, C)\n');
fprintf(fid, '      CALL RATT(VL, T, RF, RB, RKLOW)\n');
fprintf(fid, '      CALL RATX(VL, T, C, RF, RB, RKLOW)\n');
if ~isempty(QSSList)
fprintf(fid, '      CALL QSSA(VL, RF, RB, XQ)\n');
end
fprintf(fid, '      CALL RDOT(VL, RF, RB, WDOT)\n');
fprintf(fid, 'C\n');
fprintf(fid, '      END\n');
    
end

end
