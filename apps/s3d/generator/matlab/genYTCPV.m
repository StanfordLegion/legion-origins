%
%   function genYTCPV(fid, chem, QSSList)
%   function genYTCPV(fid, chem)
%
%   purpose: generate subroutine YTCP, vectorized version
%       fid: file id to print to
%       chem: the skeletal or detailed mechanism before QSSA
%       QSSList: array of IDs for the QSS species
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genYTCPV(fid, chem, QSSList)

if nargin<3, QSSList = []; end

comn = ckcomn(chem);
WT = ckwt(chem);

IK = setdiff(1:chem.KK, QSSList);
IK = sort(IK, 'ascend');

fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');

fprintf(fid, '      SUBROUTINE YTCP (VL, P, T, Y, C)\n');
fprintf(fid, '      USE chemkin_m, only : MAXVL\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      DIMENSION Y(MAXVL,*), C(MAXVL,*)\n');
fprintf(fid, '      DIMENSION P(MAXVL),T(MAXVL),SUM(MAXVL)\n');
fprintf(fid, '        INTEGER VL\n');
fprintf(fid, '      DATA SMALL/1D-50/\n');
fprintf(fid, 'C\n');

[el, sp] = ckname(chem);

%fprintf(fid, '      DO I=1,VL\n');

for k=1:length(IK)
fprintf(fid, 'C     %s\n', sp{IK(k)});
fprintf(fid, '      C(:VL, %d) = Y(:VL, %d)/%s\n', k, k, fmtG(WT(IK(k))));
end

%fprintf(fid, '      ENDDO\n');

fprintf(fid, 'C\n');

fprintf(fid, '      SUM(:VL) = 0D0\n');
fprintf(fid, '      DO K = 1, %d\n', length(IK));
%fprintf(fid, '      DO I=1,VL\n');
fprintf(fid, '         SUM(:VL) = SUM(:VL) + C(:VL,K)\n');
%fprintf(fid, '      ENDDO\n');
fprintf(fid, '      ENDDO\n');
fprintf(fid, '      SUM(:VL) = P(:VL)/(SUM(:VL)*T(:VL)*8.314510D7)\n');
fprintf(fid, 'C\n');
fprintf(fid, '      DO K = 1, %d\n', length(IK));
%fprintf(fid, '      DO I=1,VL\n');
fprintf(fid, '         C(:VL,K) = MAX(C(:VL,K),SMALL) * SUM(:VL)\n');
%fprintf(fid, '      ENDDO\n');
fprintf(fid, '      ENDDO\n');

fprintf(fid, 'C\n');
fprintf(fid, '      END\n');

end
