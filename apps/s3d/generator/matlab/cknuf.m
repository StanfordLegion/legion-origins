function [nuf] = cknuf(chem)
%
% function [nuf] = cknuf(chem)
%  Purpose:  get forward stoichiometric coefficient matrix 
%  Input:  
%        chem: chemkin workspace 
%  Output: 
%        NUF
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

comn = ckcomn(chem);

MXSP = comn.MXSP;
IcNU = comn.IcNU;
IcNK = comn.IcNK;

KK = chem.KK;
II = chem.II;

nuf = zeros(KK, II);

for n = 1:min(3, MXSP/2)
    for i = 1:II
        k = chem.ICKWRK(IcNK + (i-1)*MXSP + n - 1);
        if (k ~= 0) 
            NU = chem.ICKWRK(IcNU + (i-1)*MXSP + n - 1);
            nuf(k,i) = nuf(k,i) + NU;
        end
     end
end

return
end
