%function [str] = fmt77(toks)
% print token array to f77 multiple lines
% Input: toks: token array
% output: str: the string in F77 format
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function [str] = fmt77(toks)
    str = '      ';
    c = 6;
    for n=1:length(toks)
        t = toks{n};
        dc = length(t);
        if c+dc>72
            str = sprintf('%s\n     *', str);
            c = 6;
        end
        str = sprintf('%s%s', str, t);
        c = c+dc;
    end
    str = sprintf('%s\n', str);
end
