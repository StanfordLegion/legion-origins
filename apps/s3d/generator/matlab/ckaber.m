function [IREV, A, B, E] = ckaber(chem)
%
% function [IREV, A, B, E] = ckaber(chem)
%  Purpose:  get specified reverse reaction parameters A, B, and E 
%  Input:  
%        chem: chemkin workspace 
%  Output: 
%        IREV: index of the reverse reaction; A, B, E: parameters
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

comn = ckcomn(chem);

IcRV = comn.IcRV;
NcRV = comn.NcRV;
NREV = comn.NREV;
NPAR = comn.NPAR;

KK = chem.KK;
II = chem.II;

IREV = zeros(NREV, 1);
A = zeros(NREV, 1);
B = zeros(NREV, 1);
E = zeros(NREV, 1);

for N = 1:NREV
    IREV(N) = chem.ICKWRK(IcRV+N-1);
    IND = NcRV+(N-1)*(NPAR+1);
    A(N) = chem.RCKWRK(IND);
    B(N) = chem.RCKWRK(IND+1);
    E(N) = chem.RCKWRK(IND+2);
end

return
end
