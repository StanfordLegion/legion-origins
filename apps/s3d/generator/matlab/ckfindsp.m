function [ind] = ckfindsp(spnames, chem)
%
% function [ind] = ckfindsp(species, chem)
%  Purpose:  return species index for given names
%  Input:  
%        spnames: string cell array of species names 
%        chem: chemkin workspace 
%  Output: 
%        ind:  species index 
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

[m_names, k_names] = ckname(chem);

ind = zeros(length(spnames),1);

[C, IA, IB] = intersect(k_names, spnames);
ind(IB) = IA;
