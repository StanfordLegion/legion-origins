function [ITHB, IREV] = ckitr(chem)
%
% function [ITHB, IREV] = ckitr(chem)
%  Purpose:  get reaction THB and REV flags 
%  Input:  
%        chem: chemkin workspace 
%  Output: 
%     ITHB   - Third-body flags for the reactions;
%              ITHB(I)= -1  reaction I is not a third-body reactions
%              ITHB(I)=  0  reaction I is is a third-body reaction with
%                           no enhanced third body efficiencies
%              ITHB(I)=  N  reaction I is a third-body reaction with
%                           N species enhanced third-body efficiencies.
%                   Data type - integer array
%                   Dimension ITHB(*) at least II, the total number of
%                   reactions.
%
%     IREV   - Reversibility flags and number of species
%              (reactants plus products) for reactions.
%              IREV(I)=+N, reversible reaction I has N species
%              IREV(I)=-N, irreversible reaction I has N species
%                   Data type - integer array
%                   Dimension IREV(*) at least II, the total number of
%                   reactions.
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

comn = ckcomn(chem);

IcNS = comn.IcNS;
IcKN = comn.IcKN;
IcTB = comn.IcTB;
NTHB = comn.NTHB;

KK = chem.KK;
II = chem.II;

IREV = zeros(II,1);
ITHB = zeros(II,1);
for I = 1:II
    IREV(I) = chem.ICKWRK(IcNS + I - 1);
    ITHB(I) = -1;
end
for N = 1:NTHB
    ITHB(chem.ICKWRK(IcTB + N - 1)) = chem.ICKWRK(IcKN + N - 1);
end
