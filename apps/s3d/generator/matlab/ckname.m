function [m_names, k_names] = ckname(chem)
%
% function [m_names, k_names] = ckname(chem)
%  Purpose:  get species names from chemkin workspace
%  Input:  
%        chem: chemkin workspace 
%  Output: 
%        m_names: element names
%        k_names: species names
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

m_names = cell(chem.MM, 1);
k_names = cell(chem.KK, 1);

for i=1:chem.MM
    name = chem.CCKWRK(chem.STR_LEN*(i-1)+1:(chem.STR_LEN*i),1)';
    m_names(i) = cellstr(char(name));
end

for i=1:chem.KK
    name = chem.CCKWRK(chem.STR_LEN*(i+chem.MM-1)+1:(chem.STR_LEN*(i+chem.MM)),1)';
    k_names(i) = cellstr(char(name));
end

return
end
