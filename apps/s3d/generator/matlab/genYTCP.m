%
%   function genYTCP(fid, chem, QSSList)
%   function genYTCP(fid, chem)
%
%   purpose: generate subroutine YTCP
%       fid: file id to print to
%       chem: the skeletal or detailed mechanism before QSSA
%       QSSList: array of IDs for the QSS species
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genYTCP(fid, chem, QSSList)

if nargin<3, QSSList = []; end

comn = ckcomn(chem);
WT = ckwt(chem);

IK = setdiff(1:chem.KK, QSSList);
IK = sort(IK, 'ascend');

fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');

fprintf(fid, '      SUBROUTINE YTCP (P, T, Y, C)\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      DIMENSION Y(*), C(*)\n');
fprintf(fid, '      DATA SMALL/1D-50/\n');
fprintf(fid, 'C\n');

[el, sp] = ckname(chem);
for k=1:length(IK)
%fprintf(fid, 'C     %s\n', sp{IK(k)});
fprintf(fid, '      C(%d) = Y(%d)/%s\n', k, k, fmtG(WT(IK(k))));
end

fprintf(fid, 'C\n');

fprintf(fid, '      SUM = 0D0\n');
fprintf(fid, '      DO K = 1, %d\n', length(IK));
fprintf(fid, '         SUM = SUM + C(K)\n');
fprintf(fid, '      ENDDO\n');
fprintf(fid, '      SUM = P/(SUM*T*8.314510D7)\n');
fprintf(fid, 'C\n');
fprintf(fid, '      DO K = 1, %d\n', length(IK));
fprintf(fid, '         C(K) = MAX(C(K),SMALL) * SUM\n');
fprintf(fid, '      ENDDO\n');

fprintf(fid, 'C\n');
fprintf(fid, '      END\n');

end
