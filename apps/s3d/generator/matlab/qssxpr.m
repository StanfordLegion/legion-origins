%function xpr = qssxpr(G, v)
% generate explicit expression of qss species for species group v
%   G: the ADJ matrix of a graph
%   v: array of species list strongly coupled
%   xpr: qss expressions, string array
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function xpr = qssxpr(G, v, did)
N = length(v);

% initilization
xpr = [];

% termination condition
if N==0 
    return
end

x = v(N);
v2 = v(1:N-1);

% save the to and from link of x
xto = G(x, v2);

% pre recusion expression
for i=1:length(v2)
    bdiag = 0;
    strdiag = 'DEN = 1';
    y = v2(i);
    if G(y, x)==0 
        continue
    end % not affected
    str = sprintf('A%d_0 = A%d_0 + A%d_%d*A%d_0', y, y, y, x, x);
    fprintf(did,'z %d %d\n', y, x);
    xpr = [xpr; {str}];
    others = [];
    fprintf(did,'b\n'); % begin
    for j = 1:length(v2)
        z = v2(j);
        if G(x,z)==0 
            continue
        end % note affected
        if z==y % need to update DEN
            strdiag = sprintf('%s -A%d_%d*A%d_%d', strdiag, y, x, x, y);
            fprintf(did,'n %d %d\n',y, x);
            bdiag = 1;
            continue
        end
        if G(y,z)==0
            str = sprintf('A%d_%d = A%d_%d*A%d_%d', y,z, y,x, x,z);
            others = [others; [1, z, y, x]];
        else
            str = sprintf('A%d_%d = A%d_%d + A%d_%d*A%d_%d', y,z, y,z, y,x, x,z);
            others = [others; [2, z, y, x]];
        end
        G(y,z) = 1; % now there is an edge y->z
        xpr = [xpr; str];
    end
    fprintf(did,'e\n'); %end 
    [px,py] = size(others);
    for i = 1:px
        if others(i,1)==1
            fprintf(did,'m %d %d %d\n',others(i,2),others(i,3),others(i,4));
        elseif others(i,1)==2
            fprintf(did,'a %d %d %d\n',others(i,2),others(i,3),others(i,4));
        else
            error('Bad s3dgen!')
        end
    end
    
    if bdiag==0 
        continue
    end % G(y,y) not changed
    xpr = [xpr; {strdiag}];
    % update Ai_0
    str = sprintf('A%d_0 = A%d_0/DEN', y, y);
    fprintf(did,'d %d 0\n', y);
    xpr = [xpr; str];
    % update Ai_j
    for j=1:length(v2)
        z = v2(j);
        if G(y,z)==0 
            continue
        end
        str = sprintf('A%d_%d = A%d_%d/DEN', y, z, y, z);
        fprintf(did,'d %d %d\n', y, z);
        xpr = [xpr; str];
    end
end

% recursion
% eliminate the variable in G, no one points to x now
G(v2, x) = 0;
xpr = [xpr; qssxpr(G, v2, did)];

% post recursion expression
I = find(xto>0);
str = sprintf('XQ(%d) = A%d_0', x, x);
fprintf(did,'x %d\n', x);
for i=1:length(I)
    str = sprintf('%s +A%d_%d*XQ(%d)', str, x, v2(I(i)), v2(I(i)));
    fprintf(did,'p %d %d\n', x, v2(I(i)));
end
fprintf(did,'f\n');
xpr = [xpr; {str}];

