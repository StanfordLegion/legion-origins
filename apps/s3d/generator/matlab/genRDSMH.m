%
% function genRDSMH(fid, chem)
%
%   purpose: generate subroutine RDSMH
%       fid: file id to print to
%       chem: the skeletal or detailed mechanism before QSSA
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

function genRDSMH(fid, chem)


fprintf(fid, 'C                                                                      C\n');
fprintf(fid, 'C----------------------------------------------------------------------C\n');
fprintf(fid, 'C                                                                      C\n');

fprintf(fid, '      SUBROUTINE RDSMH  (T, SMH)\n');
fprintf(fid, '      IMPLICIT DOUBLE PRECISION (A-H, O-Z), INTEGER (I-N)\n');
fprintf(fid, '      DIMENSION SMH(*), TN(5)\n');
fprintf(fid, 'C\n');
fprintf(fid, '      TLOG = LOG(T)\n');
fprintf(fid, '      TI = 1D0/T\n');
fprintf(fid, 'C\n');
fprintf(fid, '      TN(1) = TLOG - 1D0\n');
fprintf(fid, '      TN(2) = T\n');
fprintf(fid, '      TN(3) = TN(2)*T\n');
fprintf(fid, '      TN(4) = TN(3)*T\n');
fprintf(fid, '      TN(5) = TN(4)*T\n');

% find reactions requiring equilibrium constants
comn = ckcomn(chem);

IcNS = comn.IcNS;
NREV = comn.NREV;
IcRV = comn.IcRV;

IcNT = comn.IcNT;
MXTP = comn.MXTP;
NcTT = comn.NcTT;
NcAA = comn.NcAA;
NCP = comn.NCP;
NCP1 = comn.NCP1;
NCP2 = comn.NCP2;
NCP2T = comn.NCP2T;

% whether the reaction is reversible
IREV = chem.ICKWRK(IcNS:(IcNS+chem.II-1));
I = chem.ICKWRK(IcRV:(IcRV+NREV-1));
IREV(I) = 0;
I = find(IREV>0);

% find participating species
S = cknu(chem);
S = S(:, I);
if isempty(S), return, end
IS = max(abs(S), [], 2)~=0;

[el, sp] = ckname(chem);

for n = 1:chem.KK
   if ~IS(n), continue, end % skip non-participating species
   
   fprintf(fid, 'C %s\n', sp{n});

   NT = chem.ICKWRK(IcNT+n-1);
   for m = (NT-1):-1:1
       Temp = chem.RCKWRK(NcTT + (n-1)*MXTP + m-1);
       if m == NT-1
           fprintf(fid, '      IF (T .GT. %g) THEN\n', Temp);
       elseif m > 1
           fprintf(fid, '      ELSEIF (T .GT. %g) THEN\n', Temp);
       else
           fprintf(fid, '      ELSE\n');
       end
       
       NA1 = NcAA + (m-1)*NCP2 + (n-1)*NCP2T;
       
       a = chem.RCKWRK(NA1 + NCP2-1);  %a7
       fprintf(fid, '      SMH(%d) = %s ', n, fmtG(a));
       
       a = chem.RCKWRK(NA1 + NCP1-1);  %a6
       fprintf(fid, '%s*TI ', fmtG(-a, 1));
               
       nCount = 2;
       for k=1:double(NCP)
           a = chem.RCKWRK(NA1 + k-1);
           x = a;
           if k>1, x = a/(k-1)/k; end
           if x~=0
               if nCount>=2
                   fprintf(fid, '\n     *         ');
                   nCount = 0;
               end
               fprintf(fid, '%s*TN(%d) ', fmtG(x, 1), k);
               nCount = nCount+1;
           end
       end
       fprintf(fid, '\n');
   end
   fprintf(fid, '      ENDIF\n');
end

fprintf(fid, 'C\n');
fprintf(fid, '      END\n');

end
