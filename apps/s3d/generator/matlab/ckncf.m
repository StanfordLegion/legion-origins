function [ncf] = ckncf(chem)
%
% function [ncf] = ckncf(chem)
%  Purpose:  get element composition matrix 
%  Input:  
%        chem: chemkin workspace 
%  Output: 
%        NCF: element composition in species
%
%   Copyright: Tianfeng Lu, 2013, all rights reserved
%
%   Tianfeng Lu
%   191 Auditorium Road U-3139
%   Department of Mechanical Engineering
%   University of Connecticut, Storrs, CT 06296, USA
%   Email: tlu@engr.uconn.edu
%   Phone: 860-486-3942
%

comn = ckcomn(chem);

MXSP = comn.MXSP;
IcNC = comn.IcNC;
IcNK = comn.IcNK;

KK = chem.KK;
MM = chem.MM;

ncf = zeros(MM, KK);

for K = 1:KK
    J = IcNC + (K-1)*MM;
    for M = 1:MM
        ncf(M,K) = chem.ICKWRK(J + M - 1);
    end
end

return
