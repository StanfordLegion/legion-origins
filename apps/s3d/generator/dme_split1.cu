        
        if (wid < 15)
        {
          asm volatile("bar.arrive 1,512;" : : : "memory");
        }
        else if (wid == 15)
        {
          asm volatile("bar.sync 1,512;" : : : "memory");
          double den;
          /* a6_0 = a6_0 + a6_8 * a8_0; */
          scratch[AVAL_OFFSET+11][tid] += (scratch[AVAL_OFFSET+13][tid] * 
            scratch[AVAL_OFFSET+16][tid]); 
          /* den = 1 - a8_6*a6_8; */
          den = 1.0/(1.0 - scratch[AVAL_OFFSET+17][tid] * scratch[AVAL_OFFSET+13][tid]);
          /* a6_0 = a6_0/den; */
          scratch[AVAL_OFFSET+11][tid] *= den;
          /* a6_4 = a6_4/den; */
          scratch[AVAL_OFFSET+12][tid] *= den;
          /* a4_0 = a4_0 + a4_3 * a3_0; */
          scratch[AVAL_OFFSET+0][tid] += (scratch[AVAL_OFFSET+3][tid] * 
            scratch[AVAL_OFFSET+14][tid]); 
          /* den = 1 - a3_4*a4_3; */
          den = 1.0/(1.0 - scratch[AVAL_OFFSET+15][tid] * scratch[AVAL_OFFSET+3][tid]);
          /* a4_0 = a4_0/den; */
          scratch[AVAL_OFFSET+0][tid] *= den;
          /* a4_1 = a4_1/den; */
          scratch[AVAL_OFFSET+1][tid] *= den;
          /* a4_2 = a4_2/den; */
          scratch[AVAL_OFFSET+2][tid] *= den;
          /* a4_6 = a4_6/den; */
          scratch[AVAL_OFFSET+4][tid] *= den;
          /* a4_0 = a4_0 + a4_6 * a6_0; */
          scratch[AVAL_OFFSET+0][tid] += (scratch[AVAL_OFFSET+4][tid] * 
            scratch[AVAL_OFFSET+11][tid]); 
          /* den = 1 - a6_4*a4_6; */
          den = 1.0/(1.0 - scratch[AVAL_OFFSET+12][tid] * scratch[AVAL_OFFSET+4][tid]);
          /* a4_0 = a4_0/den; */
          scratch[AVAL_OFFSET+0][tid] *= den;
          /* a4_1 = a4_1/den; */
          scratch[AVAL_OFFSET+1][tid] *= den;
          /* a4_2 = a4_2/den; */
          scratch[AVAL_OFFSET+2][tid] *= den;
          /* a4_0 = a4_0 + a4_2 * a2_0; */
          scratch[AVAL_OFFSET+0][tid] += (scratch[AVAL_OFFSET+2][tid] * 
            scratch[AVAL_OFFSET+8][tid]); 
          /* den = 1 - a2_4*a4_2; */
          den = 1.0/(1.0 - scratch[AVAL_OFFSET+10][tid] * scratch[AVAL_OFFSET+2][tid]);
          /* a4_1 = a4_1 + a4_2 * a2_1; */
          scratch[AVAL_OFFSET+1][tid] = scratch[AVAL_OFFSET+1][tid] + 
            scratch[AVAL_OFFSET+2][tid] * scratch[AVAL_OFFSET+9][tid]; 
          /* a4_0 = a4_0/den; */
          scratch[AVAL_OFFSET+0][tid] *= den;
          /* a4_1 = a4_1/den; */
          scratch[AVAL_OFFSET+1][tid] *= den;
          /* a1_0 = a1_0 + a1_2 * a2_0; */
          scratch[AVAL_OFFSET+5][tid] += (scratch[AVAL_OFFSET+6][tid] * 
            scratch[AVAL_OFFSET+8][tid]); 
          /* den = 1 - a2_1*a1_2; */
          den = 1.0/(1.0 - scratch[AVAL_OFFSET+9][tid] * scratch[AVAL_OFFSET+6][tid]);
          /* a1_4 = a1_4 + a1_2 * a2_4; */
          scratch[AVAL_OFFSET+7][tid] = scratch[AVAL_OFFSET+7][tid] + 
            scratch[AVAL_OFFSET+6][tid] * scratch[AVAL_OFFSET+10][tid]; 
          /* a1_0 = a1_0/den; */
          scratch[AVAL_OFFSET+5][tid] *= den;
          /* a1_4 = a1_4/den; */
          scratch[AVAL_OFFSET+7][tid] *= den;
          /* a4_0 = a4_0 + a4_1 * a1_0; */
          scratch[AVAL_OFFSET+0][tid] += (scratch[AVAL_OFFSET+1][tid] * 
            scratch[AVAL_OFFSET+5][tid]); 
          /* den = 1 - a1_4*a4_1; */
          den = (1.0 - scratch[AVAL_OFFSET+7][tid] * scratch[AVAL_OFFSET+1][tid]);
          /* a4_0 = a4_0/den; */
          scratch[AVAL_OFFSET+0][tid] /= den;
          /* xq_4 = a4_0; */
          scratch[QSS_OFFSET+3][tid] = scratch[AVAL_OFFSET+0][tid];
          /* xq_1 = a1_0 + a1_4*xq_4; */
          scratch[QSS_OFFSET+0][tid] = scratch[AVAL_OFFSET+5][tid] + 
            scratch[AVAL_OFFSET+7][tid]*scratch[QSS_OFFSET+3][tid]; 
          /* xq_2 = a2_0 + a2_1*xq_1 + a2_4*xq_4; */
          scratch[QSS_OFFSET+1][tid] = scratch[AVAL_OFFSET+8][tid] + 
            scratch[AVAL_OFFSET+9][tid]*scratch[QSS_OFFSET+0][tid] + 
            scratch[AVAL_OFFSET+10][tid]*scratch[QSS_OFFSET+3][tid]; 
          /* xq_6 = a6_0 + a6_4*xq_4; */
          scratch[QSS_OFFSET+5][tid] = scratch[AVAL_OFFSET+11][tid] + 
            scratch[AVAL_OFFSET+12][tid]*scratch[QSS_OFFSET+3][tid]; 
          /* xq_3 = a3_0 + a3_4*xq_4; */
          scratch[QSS_OFFSET+2][tid] = scratch[AVAL_OFFSET+14][tid] + 
            scratch[AVAL_OFFSET+15][tid]*scratch[QSS_OFFSET+3][tid]; 
          /* xq_8 = a8_0 + a8_6*xq_6; */
          scratch[QSS_OFFSET+7][tid] = scratch[AVAL_OFFSET+16][tid] + 
            scratch[AVAL_OFFSET+17][tid]*scratch[QSS_OFFSET+5][tid]; 
        }

