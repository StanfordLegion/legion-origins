
#include "chemistry.h"

#include <cstdio>
#include <cstdlib>
#include <cmath>

#define MAX_SHARED 49152
#define POINTS 32

#define INT "int"
#define REAL "double"
#define REAL_TYPE double

#define PRESSURE "pressure"
#define TEMPERATURE "temperature"
#define MASS_FRAC "mass_frac"
#define MOLE_FRAC "mole_frac"
#define DIFFUSION "diffusion"
#define WDOT      "wdot"
#define THIRD_BODY "third_body"

#define PRESSURE_ARRAY "pressure_array"
#define TEMPERATURE_ARRAY "temperature_array"
#define MASS_FRAC_ARRAY "mass_frac_array"
#define DIFFUSION_ARRAY "diffusion_array"
#define WDOT_ARRAY "wdot_array"
#define QSSA_ARRAY "qssa_array"

#define PRESSURE_IDX 191
#define TEMPERATURE_IDX 190
#define MASS_FRAC_IDX 189
#define DIFFUSION_IDX 188
#define WDOT_IDX 187
#define TOTAL_SHARED_POINTERS 5

#define ENABLE_LDG

#define MAX_REACTION_CONSTANTS 6

#define TARGET_NUM_CTAS       2
#define TUNING_PARAM          1

#define SPILL_RATES           7

#define POINTS_PER_CTA        32 

#define EXTRA_WARPS           1

// Switch between indexing into an array of reaction rates
// or explicitly named each rate to fool the cuda compiler.
//#define RR_INDEXING

//#define INSTR_INIT  "int ptxas_is_worthless = 0;\n"
//#define INSTR_LIMITER "if (clock() == 0) ptxas_is_worthless++;\n";
#define INSTR_LIMITER  "__threadfence_block();\n";

#define MAX_SHARED_LOADS    32 

// The number of intermediate mole fraction warps
#define MOLE_FRAC_WARPS     2
#define MOLE_FRAC_WARP_MASK "0x3"

#define QSSA_WARPS          4
#define STIF_WARPS          4
#define LAST_WARPS          4

//#define LOAD_BALANCE

#define MAX_QSSA_LOADS    16
#define MAX_STIF_LOADS    16

#define DEFAULT_COMPLEX_COMPONENTS 3 
#define COMPLEX_COMPONENT_SKIPS    0
#define EXPENSIVE_REAC_COST        3
#define QSSA_COST                  3

//#define TARGET_FERMI

// Helper functions that does all the magic
static int find_warp(int reac_idx, const std::vector<Warp*> &warps)
{
  int location = -1; 
  unsigned index = 0;
  for (std::vector<Warp*>::const_iterator it = warps.begin();
        it != warps.end(); it++,index++)
  {
    if ((*it)->has_reaction(reac_idx))
    {
      location = index;
      break;
    }
  }
  return location;
}

static int select_target_warp(const std::vector<Warp*> &warps)
{
  int result = -1;
  int min_reactions = -1;
  assert(warps.size() > 0);
  unsigned index = 0;
  for (std::vector<Warp*>::const_iterator it = warps.begin();
        it != warps.end(); it++,index++)
  {
    if (min_reactions == -1)
    {
      result = index;
      min_reactions = (*it)->num_reactions();
    }
    else if (min_reactions > int((*it)->num_reactions()))
    {
      result = index;
      min_reactions = (*it)->num_reactions();
    }
  }
  assert(result != -1);
  assert(result < int(warps.size()));
  return result;
}

static void help_compute_allocations(std::vector<Allocation*> &needed_allocations, std::map<int,Allocation*> &warp_allocations,
                              std::vector<Allocation*> &expression_allocations, const std::map<int,int> &reactions,
                              const std::vector<Warp*> &warps, int &target_warp, int &remaining_allocations,
                              bool forwards, int reaction_granularity)
{
  for (std::map<int,int>::const_iterator it = reactions.begin();
        it != reactions.end(); it++)
  {
    int warp_id = find_warp(it->first,warps);  
    if (warp_id == -1)
    {
      // It's not in a warp, see if we selected a target warp yet
      if (target_warp == -1)
      {
        target_warp = select_target_warp(warps);
        assert(target_warp >= 0);
        remaining_allocations = reaction_granularity;
        Allocation *new_alloc = new Allocation(target_warp);
        warp_allocations[target_warp] = new_alloc;
        needed_allocations.push_back(new_alloc);
        expression_allocations.push_back(new_alloc);
      }
      else if (remaining_allocations == 0) // check to see if we are out of allocations on the target warp
      {
        // Pick a new target warp and see if we have an allocation for it already
        target_warp = select_target_warp(warps);
        assert(target_warp >= 0);
        remaining_allocations = reaction_granularity;
        std::map<int,Allocation*>::const_iterator finder = warp_allocations.find(target_warp);
        if (finder == warp_allocations.end())
        {
          Allocation *new_alloc = new Allocation(target_warp);
          warp_allocations[target_warp] = new_alloc;
          needed_allocations.push_back(new_alloc);
          expression_allocations.push_back(new_alloc);
        }
      }
      warps[target_warp]->add_reaction(it->first);
      assert(warp_allocations.find(target_warp) != warp_allocations.end());
      if (forwards)
        warp_allocations[target_warp]->add_forward_expression(it->first,it->second);
      else
        warp_allocations[target_warp]->add_backward_expression(it->first,it->second);
      // If we're tracking target warp allocations, remove an allocation
      if (remaining_allocations != -1)
      {
        assert(remaining_allocations > 0);
        remaining_allocations--;
      }
    }
    else
    {
      assert(warp_id >= 0);
      // This reaction has already been assigned a warp, see if
      // we already have an allocation for it, if not make one
      std::map<int,Allocation*>::const_iterator finder = warp_allocations.find(warp_id);
      if (finder == warp_allocations.end())
      {
        // Make a new allocation for this warp and add it to all the necessary allocations
        Allocation *new_alloc = new Allocation(warp_id);
        warp_allocations[warp_id] = new_alloc;
        needed_allocations.push_back(new_alloc);
        expression_allocations.push_back(new_alloc);
      }
      if (forwards)
        warp_allocations[warp_id]->add_forward_expression(it->first,it->second);
      else
        warp_allocations[warp_id]->add_backward_expression(it->first,it->second);
    }
  }
}

static int find_minimum_warp(const std::vector<Warp*> &warps, std::map<int,int> &overheads, int pass, std::set<int> &filter)
{
  int min_idx = -1;
  int min_cost = -1;
  for (unsigned idx = 0; idx < warps.size(); idx++)
  {
    int cost = warps[idx]->num_reactions(pass);
    if (overheads.find(idx) != overheads.end())
      cost += overheads[idx];
    if (min_cost == -1)
    {
      min_idx = idx;
      min_cost = cost;
    }
    else if (cost < min_cost)
    {
      min_idx = idx;
      min_cost = cost;
    }
  }
  if (filter.find(min_idx) != filter.end())
  {
    // See if we can find another point not in the filter with the same cost
    for (unsigned idx = 0; idx < warps.size(); idx++)
    {
      if (int(idx) == min_idx)
        continue;
      int cost = warps[idx]->num_reactions(pass);
      if (overheads.find(idx) != overheads.end())
        cost += overheads[idx];
      if ((cost == min_cost) && (filter.find(idx) == filter.end()))
      {
        min_idx = idx;
        break;
      }
    }
  }
  assert(min_idx != -1);
  return min_idx;
}

static bool balanced_warps(const std::vector<Warp*> &warps, std::map<int,int> &overheads, int pass)
{
  int first_cost = warps[0]->num_reactions(pass);
  if (overheads.find(0) != overheads.end())
    first_cost += overheads[0];
  for (unsigned idx = 1; idx < warps.size(); idx++)
  {
    int cost = warps[idx]->num_reactions(pass);
    if (overheads.find(idx) != overheads.end())
      cost += overheads[idx];
    if (cost != first_cost)
      return false;
  }
  return true;
}

static void emit_barrier(CodeOutStream &ost, int barrier_id, int num_warps, bool blocking)
{
  if (blocking)
    ost << "asm volatile(\"bar.sync " << barrier_id << "," << (num_warps*32) << ";\" : : : \"memory\");\n";
  else
    ost << "asm volatile(\"bar.arrive " << barrier_id << "," << (num_warps*32) << ";\" : : : \"memory\");\n";
}

static void emit_cuda_load(CodeOutStream &ost, const char *dst,
                           const char *src, bool double_two, const char *qualifier = ".cg")
{
#ifdef ENABLE_LDG
  ost << "#if __CUDA_ARCH__ >= 350\n";
  if (double_two)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << ".x), ";
    ost << "\"=d\"(" << dst << ".y) : \"l\"(" << src << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << ") : \"memory\");\n";
  }
  ost << "#else\n";
#endif
  if (double_two)
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << ".x), ";
    ost << "\"=d\"(" << dst << ".y) : \"l\"(" << src << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << ") : \"memory\");\n";
  }
#ifdef ENABLE_LDG
  ost << "#endif\n";
#endif
}

static void emit_cuda_load(CodeOutStream &ost, const char *dst, 
                           const char *src, const char *src_offset, bool double_two, const char *qualifier = ".cg")
{
#ifdef ENABLE_LDG
  ost << "#if __CUDA_ARCH__ >= 350\n";
  if (double_two)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << ".x), ";
    ost << "\"=d\"(" << dst << ".y) : \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  ost << "#else\n";
#endif
  if (double_two)
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << ".x), ";
    ost << "\"=d\"(" << dst << ".y) : \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
#ifdef ENABLE_LDG
  ost << "#endif\n";
#endif
}

static void emit_cuda_load(CodeOutStream &ost, const char *dst, const char *dst_offset,
                           const char *src, const char *src_offset, bool double_two, const char *qualifier = ".cg")
{
  ost << "#if __CUDA_ARCH__ >= 350\n";
  if (double_two)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << "[" << dst_offset << "].x), ";
    ost << "\"=d\"(" << dst << "[" << dst_offset << "].y) : \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << "[" << dst_offset << "]) ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  ost << "#else\n";
  if (double_two)
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << "[" << dst_offset << "].x), ";
    ost << "\"=d\"(" << dst << "[" << dst_offset << "].y) : \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << "[" << dst_offset << "]) ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  ost << "#endif\n";
}

static int next_largest_power(int value, int power)
{
  assert(value > 0);
  int result = 1;
  while (value > result)
  {
    result *= power;
  }
  return result;
}

static bool is_weird_reaction(Reaction *reac)
{
  bool weird = reac->low.enabled || reac->lt.enabled;
  // Check the backward conditions for weirdness
  weird = weird || (!reac->rev.enabled) || reac->rlt.enabled;
  return weird;
}

ChemistryUnit::ChemistryUnit(TranslationUnit *u)
  : unit(u)
{

}

void ChemistryUnit::add_component(ConnectedComponent *qss)
{
  connected_components.push_back(qss); 
}

void ChemistryUnit::add_unimportant(int backward, int idx)
{
  assert((backward==0) || (backward==1));
  if (backward==0)
  {
    assert(forward_unimportant.find(idx) == forward_unimportant.end());
    forward_unimportant.insert(idx);
  }
  else
  {
    assert(backward_unimportant.find(idx) == backward_unimportant.end());
    backward_unimportant.insert(idx);
  }
}

void ChemistryUnit::add_forward_zero(int reac_idx, int qss_idx, int coeff)
{
  get_qss(qss_idx)->add_forward_zero(reac_idx, qss_idx, coeff);
}

void ChemistryUnit::add_backward_zero(int reac_idx, int qss_idx, int coeff)
{
  get_qss(qss_idx)->add_backward_zero(reac_idx, qss_idx, coeff);
}

void ChemistryUnit::add_forward_contribution(int reac_idx, int qss_idx, int other_idx, int coeff)
{
  get_qss(qss_idx)->add_forward_contribution(reac_idx, qss_idx, other_idx, coeff);
}

void ChemistryUnit::add_backward_contribution(int reac_idx, int qss_idx, int other_idx, int coeff)
{
  get_qss(qss_idx)->add_backward_contribution(reac_idx, qss_idx, other_idx, coeff);
}

void ChemistryUnit::add_forward_denom(int qss_idx, int reac_idx)
{
  get_qss(qss_idx)->add_forward_denom(qss_idx, reac_idx);
}

void ChemistryUnit::add_backward_denom(int qss_idx, int reac_idx)
{
  get_qss(qss_idx)->add_backward_denom(qss_idx, reac_idx);
}

void ChemistryUnit::add_forward_correction(int reac_idx, int qss_idx)
{
  get_qss(qss_idx)->add_forward_correction(reac_idx, qss_idx);
}

void ChemistryUnit::add_backward_correction(int reac_idx, int qss_idx)
{
  get_qss(qss_idx)->add_backward_correction(reac_idx, qss_idx);
}

void ChemistryUnit::add_stif(Stif *stif)
{
  stif_operations.push_back(stif); 
}

ConnectedComponent* ChemistryUnit::get_qss(int spec)
{
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    if ((*it)->has_species(spec))
      return *it;
  }
  assert(false);
  return NULL;
}

void ChemistryUnit::analyze_working_set(void)
{
  std::vector<Operation*> ops;
  ops.insert(ops.end(), connected_components.begin(), connected_components.end());
  ops.insert(ops.end(), stif_operations.begin(), stif_operations.end());
  // Do a little sanity check
  for (std::vector<Operation*>::const_iterator it = ops.begin();
        it != ops.end(); it++)
  {
    (*it)->sanity_check();
  }
#if 1
  {
    std::vector<Operation*> reordering;
    compute_heuristic_ordering(ops, reordering);
    //ops.clear();
    //ops.insert(ops.end(),connected_components.begin(),connected_components.end());
    //ops.insert(ops.end(),reordering.begin(),reordering.end());
    ops = reordering;
  }
#endif
  Firstness first_analyzer;
  // Go forwards through  the list and compute the first creations
  for (std::vector<Operation*>::const_iterator it = ops.begin();
        it != ops.end(); it++)
  {
    (*it)->compute_first(first_analyzer);
  }
  Liveness live_analyzer;
  std::list<int> working_set;
  // Go backwards through the list and compute the liveness
  for (std::vector<Operation*>::const_reverse_iterator it = ops.rbegin();
        it != ops.rend(); it++)
  {
    (*it)->compute_liveness(live_analyzer);
    working_set.push_front(live_analyzer.fneeded.size()+live_analyzer.bneeded.size());
  }
  // the live analyzer should be empty now
  assert(live_analyzer.fneeded.empty());
  assert(live_analyzer.bneeded.empty());
  assert(working_set.size() == ops.size());
  unsigned idx = 0;
  for (std::list<int>::const_iterator it = working_set.begin();
        it != working_set.end(); it++, idx++)
  {
    size_t needed_values = (ops[idx]->ffirst_size()+ops[idx]->bfirst_size());
    size_t actually_needed = (ops[idx]->fread_size() + ops[idx]->bread_size());
    fprintf(stdout,"    Operation %s: Previous Working Set %d values    Needs %ld new values   Total %ld   "
                    "New reactions %ld  Actually needed %ld\n", 
        ops[idx]->get_name(), *it, needed_values, (needed_values+*it), ops[idx]->first_reac_size(), actually_needed);
    //fprintf(stdout,"    Operation %s has first values (%ld,%ld) and live values (%ld,%ld)\n",
    //        ops[idx]->get_name(), ops[idx]->ffirst_size(), ops[idx]->bfirst_size(),
    //        ops[idx]->flive_size(), ops[idx]->blive_size());
  }
}

void ChemistryUnit::compute_heuristic_ordering(const std::vector<Operation*> &ops,
                                               std::vector<Operation*> &result)
{
  // Get the stiff operations 
  std::set<Operation*> stiff_ops;
  std::vector<Operation*> qss_ops;
  for (std::vector<Operation*>::const_iterator it = ops.begin();
        it != ops.end(); it++)
  {
    if ((*it)->is_stif())
      stiff_ops.insert(*it);
    else
      qss_ops.push_back(*it);
  }
  std::list<Operation*> temp_ordering;
  while (!stiff_ops.empty())
  {
    Operation *best_stiff = NULL;
    size_t num_final_values = 0;
    for (std::set<Operation*>::const_iterator it = stiff_ops.begin();
          it != stiff_ops.end(); it++)
    {
      Operation *current = *it;
      std::set<int> forward;
      std::set<int> backward;
      current->get_read_values(forward,backward);
      for (std::set<Operation*>::const_iterator it2 = stiff_ops.begin();
            it2 != stiff_ops.end(); it2++)
      {
        if ((*it) == (*it2))
          continue;
        (*it2)->remove_read_values(forward,backward);
      }
      if (best_stiff == NULL)
      {
        best_stiff = current;
        num_final_values = (forward.size()+backward.size());
      }
      else if ((forward.size()+backward.size()) > num_final_values)
      {
        best_stiff = current;
        num_final_values = (forward.size()+backward.size());
      }
    }
    assert(best_stiff != NULL);
    temp_ordering.push_back(best_stiff);
    stiff_ops.erase(best_stiff);
  }
  // Now get all the QSS operations and push them through the list
  for (std::vector<Operation*>::const_reverse_iterator op_it = qss_ops.rbegin();
        op_it != qss_ops.rend(); op_it++)
  {
    bool added = false;
    for (std::list<Operation*>::iterator it = temp_ordering.begin();
          it != temp_ordering.end(); it++)
    {
      if (!(*it)->can_swap(*op_it))
      {
        temp_ordering.insert(it, *op_it);
        added = true;
        break;
      }
    }
    if (!added)
      temp_ordering.push_back(*op_it);
  }
  // Now put everything in the temp ordering into the final result
  assert(temp_ordering.size() == ops.size());
  result.insert(result.end(),temp_ordering.begin(),temp_ordering.end());
}

void ChemistryUnit::emit_code(const char *file_prefix)
{
  char header_name[128];
  char source_name[128];
  // Overwrite the old version of getrates
  snprintf(header_name,127,"%s.h", file_prefix);
  printf("Writing header file %s...\n", header_name);
  emit_cpp_header_file(header_name);
  snprintf(source_name,127,"%s.cc", file_prefix);
  printf("Writing source file %s...\n", source_name);
  emit_cpp_source_file(header_name, source_name);
}

void ChemistryUnit::emit_cpp_header_file(const char *file_name)
{
  CodeOutStream ost(file_name, true/*bound line lengths*/, 80);
  ost << "\n";
  ost << "#ifndef __LEGION_GETRATES__\n";
  ost << "#define __LEGION_GETRATES__\n";
  ost << "\n";
  ost << "#define GETRATES_NEEDS_DIFFUSION\n";
  if (!stif_operations.empty())
    ost << "#define GETRATES_STIFF_SPECIES " << stif_operations.size() << "\n";
  ost << "\n";

  // first need to mark the QSS species in case it hasn't been done yet
  for (SpeciesSet::const_iterator it = unit->species.begin();
        it != unit->species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Otherwise see if it is a QSS species
    bool qss_spec = false;
    (*it)->qss_species = false;
    for (std::vector<ConnectedComponent*>::const_iterator cc = connected_components.begin();
          cc != connected_components.end(); cc++)
    {
      for (std::map<int,QSS*>::const_iterator sp = (*cc)->species.begin();
            sp != (*cc)->species.end(); sp++)
      {
        assert(sp->first == sp->second->qss_idx);
        if (int((*it)->idx) == sp->second->species_idx)
        {
          (*it)->qss_species = true;
          qss_spec = true;
          break;
        }
      }
      if (qss_spec)
        break;
    }
  }

  {
    unsigned spec_idx = 0;
    unsigned total_non_qssa = 0;
    for (std::vector<Species*>::const_iterator sit = unit->ordered_species.begin();
          sit != unit->ordered_species.end(); sit++)
    {
      if (strcmp((*sit)->name,"M") == 0)
        continue;
      //ost << "#define " << (*it)->code_name << "  " << spec_idx << "\n";
      spec_idx++;
      if (!(*sit)->qss_species)
        total_non_qssa++;
    }
    ost << "\n";
    bool first = true;
#if 1
    ost << "const double molecular_masses[" << total_non_qssa << "] = {";
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      if (!first)
        ost << ", ";
      ost << (*it)->molecular_mass;
      first = false;
    }
    ost << "};\n\n";
#endif
#if 0
    ost << "const double molecular_masses[" << spec_idx << "] = {";
    first = true;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if (!first)
        ost << ", ";
      ost << (*it)->molecular_mass;
      first = false;
    }
    ost << "};\n\n";
#endif
#if 1
    ost << "const double recip_molecular_masses[" << total_non_qssa << "] = {";
    first = true;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      if (!first)
        ost << ", ";
      ost << (1.0/(*it)->molecular_mass);
      first = false;
    }
    ost << "};\n\n";
#endif
#if 0
    ost << "const double recip_molecular_masses[" << spec_idx << "] = {";
    first = true;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if (!first)
        ost << ", ";
      ost << (1.0/(*it)->molecular_mass);
      first = false;
    }
    ost << "};\n\n";
#endif
    if (!stif_operations.empty())
    {
      ost << "const unsigned int stif_species_indexes[" << stif_operations.size() << "] = {";
      first = true;
      for (std::vector<Stif*>::const_iterator it = stif_operations.begin();
            it != stif_operations.end(); it++)
      {
        if (!first)
          ost << ", ";
        ost << (*it)->k2;
        first = false;
      }
      ost << "};\n\n";
    }
  }
  emit_cpp_declaration(ost);
  ost << ";\n";
  ost << "\n";
  ost << "#endif // __LEGION_GETRATES__\n";
  ost << "\n";
}

void ChemistryUnit::emit_cpp_declaration(CodeOutStream &ost)
{
  ost << "void getrates(";
  ost << "const " << REAL << " " << PRESSURE;
  ost << ", const " << REAL << " " << TEMPERATURE;
  ost << ", const " << REAL << " *" << MASS_FRAC;
  ost << ", const " << REAL << " *" << DIFFUSION;
  ost << ", const " << REAL << " dt";
  ost << ", " << REAL << " *" << WDOT;
  ost << ")\n";
}

void ChemistryUnit::emit_cpp_source_file(const char *header_name, const char *file_name)
{
  int num_species = 0;
  // first need to mark the QSS species in case it hasn't been done yet
  for (SpeciesSet::const_iterator it = unit->species.begin();
        it != unit->species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Otherwise see if it is a QSS species
    bool qss_spec = false;
    (*it)->qss_species = false;
    for (std::vector<ConnectedComponent*>::const_iterator cc = connected_components.begin();
          cc != connected_components.end(); cc++)
    {
      for (std::map<int,QSS*>::const_iterator sp = (*cc)->species.begin();
            sp != (*cc)->species.end(); sp++)
      {
        assert(sp->first == sp->second->qss_idx);
        if (int((*it)->idx) == sp->second->species_idx)
        {
          (*it)->qss_species = true;
          qss_spec = true;
          break;
        }
      }
      if (qss_spec)
        break;
    }
    if (!qss_spec)
      num_species++;
    // Also check to see if it is a stif species
    for (std::vector<Stif*>::const_iterator st = stif_operations.begin();
          st != stif_operations.end(); st++)
    {
      if ((*st)->k == int((*it)->idx))
      {
        (*st)->species = *it;
        break;
      }
    }
  }
  CodeOutStream ost(file_name, true/*bound line lengths*/, 80);
  ost << "\n";
  ost << "#include \"" << header_name << "\"\n";
  ost << "#include <cmath>\n";
  ost << "\n";
#if 1
  {
    int idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M")==0)
        continue;
      if ((*it)->qss_species)
        continue;
      ost << "#define " << (*it)->code_name << "  " << idx << "\n";
      idx++;
    }
    assert(idx == num_species);
  }
#endif
  ost << "\n";
  emit_cpp_declaration(ost);
  ost << "\n";
  PairDelim pair(ost); 
  ost << "\n";
  unit->emit_numerical_constants(ost);
  ost << "\n";
  unit->emit_temperature_dependent(ost);
  ost << "\n";

  // Emit code to compute the gibbs free energies
  ost << REAL << " cgspl[" << num_species << "];\n";
  ost << "// Gibbs free energies\n";
  {
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      ost << REAL << " cgspl_" << (*it)->code_name << ";\n";
    }
    PairDelim gibbs_pair(ost);
    ost << "const " << REAL << " &tk1 = " << TEMPERATURE << ";\n";
    ost << "const " << REAL << " tk2 = tk1*tk1;\n";
    ost << "const " << REAL << " tk3 = tk1*tk2;\n";
    ost << "const " << REAL << " tk4 = tk1*tk3;\n";
    ost << "const " << REAL << " tk5 = tk1*tk4;\n";
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      ost << "// Species " << (*it)->name << "\n";
      ost << "if (tk1 > " << (*it)->get_common_temperature() << ")\n";
      {
        PairDelim if_pair(ost);
        ost << "cgspl_" << (*it)->code_name << " = ";
        ost << (*it)->get_high_coefficient(0) << "*tk1*(";
        if (!unit->no_nondim)
          ost << (1-log(TEMPERATURE_REF));
        else
          ost << "1";
        ost << "-vlntemp) + ";
        ost << (*it)->get_high_coefficient(1) << "*tk2 + ";
        ost << (*it)->get_high_coefficient(2) << "*tk3 + ";
        ost << (*it)->get_high_coefficient(3) << "*tk4 + ";
        ost << (*it)->get_high_coefficient(4) << "*tk5 + ";
        ost << "(" << (*it)->get_high_coefficient(5) << " - ";
        ost << "tk1*" << (*it)->get_high_coefficient(6) << ");\n";
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        ost << "cgspl_" << (*it)->code_name << " = ";
        ost << (*it)->get_low_coefficient(0) << "*tk1*(";
        if (!unit->no_nondim)
          ost << (1-log(TEMPERATURE_REF));
        else
          ost << "1";
        ost << "-vlntemp) + ";
        ost << (*it)->get_low_coefficient(1) << "*tk2 + ";
        ost << (*it)->get_low_coefficient(2) << "*tk3 + ";
        ost << (*it)->get_low_coefficient(3) << "*tk4 + ";
        ost << (*it)->get_low_coefficient(4) << "*tk5 + ";
        ost << "(" << (*it)->get_low_coefficient(5) << " - ";
        ost << "tk1*" << (*it)->get_low_coefficient(6) << ");\n";
      }
      spec_idx++;
    }
  }
  // emit code to compute the molar fractions 
  ost << "// Compute molar fractions\n";
  ost << REAL << " cspl[" << num_species << "];\n";
  {
    // For each of the species, compute the molar fraction
    PairDelim molar_pair(ost);
    ost << REAL << " sumyow = 0.0;\n";
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      ost << "sumyow += " << MASS_FRAC << "[" << (*it)->code_name << "] * " << (1.0/(*it)->molecular_mass) << ";\n";
    }
    ost << "sumyow *= (";
    if (unit->no_nondim)
      ost << "8.314510e+07 * " << TEMPERATURE;
    else
      ost << (8.314510e+07 * TEMPERATURE_REF / PRESSURE_REF) << " * " << TEMPERATURE;
    ost << ");\n";
    ost << "sumyow = " << PRESSURE << "/sumyow;\n";
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      ost << "cspl[" << (*it)->code_name << "] = " << MASS_FRAC << "[" << (*it)->code_name
          << "] * (sumyow * " << (1.0/(*it)->molecular_mass) << ");\n";
    }
  }
  // Compute the third body values
  ost << "// Third-body computation\n";
  ost << REAL << " thbctemp[" << unit->third_bodies.size() << "];\n";
  {
    PairDelim thb_pair(ost);
    ost << REAL << " ctot = 0.0;\n";
    ost << "for (int i = 0; i < " << num_species << "; i++)\n";
    {
      PairDelim loop_pair(ost);
      ost << "ctot += cspl[i];\n";
    }
    unsigned idx = 0;
    for (std::vector<ThirdBody*>::const_iterator it = unit->third_bodies.begin();
          it != unit->third_bodies.end(); it++,idx++)
    {
      ost << "thbctemp[" << idx << "] = ctot";
      for (std::map<Species*,double>::const_iterator comp_it = (*it)->components.begin();
            comp_it != (*it)->components.end(); comp_it++)
      {
        ost << " + " << comp_it->second << "*cspl[" << comp_it->first->code_name << "]";
      }
      ost << ";\n"; 
    }
  }
  // Now do the reactions
  ost << REAL << " forward_r[" << unit->all_reactions.size() << "];\n";
  ost << REAL << " reverse_r[" << unit->all_reactions.size() << "];\n";
  {
    unsigned idx = 0;
    for (std::vector<Reaction*>::const_iterator it = unit->all_reactions.begin();
          it != unit->all_reactions.end(); it++,idx++)
    {
      Reaction *reac = *it;
      if (reac->is_duplicate() && reac->is_owned())
        continue;
      reac->emit_reaction_comment(ost);
      PairDelim reac_pair(ost);
      IntensityCounter cnt;
      reac->emit_forward_reaction_rate(ost, cnt, false/*vector*/);
      ost << "forward_r[" << idx << "] = rr_f";
      for (SpeciesCoeffMap::const_iterator co_it = reac->forward.begin();
            co_it != reac->forward.end(); co_it++)
      {
        if (co_it->first->qss_species)
          continue;
        assert(co_it->second > 0);
        for (int i = 0; i < co_it->second; i++)
          ost << "*cspl[" << co_it->first->code_name << "]";
      }
      if ((reac->thb != NULL) && !reac->pressure_dep)
      {
        ost << "*thbctemp[" << reac->thb->idx << "]";
      }
      ost << ";\n";
      if (reac->reversible)
      {
        reac->emit_reverse_reaction_rate(ost, cnt, false/*indexing*/);
        ost << "reverse_r[" << idx << "] = rr_r";
        for (SpeciesCoeffMap::const_iterator co_it = reac->backward.begin();
            co_it != reac->backward.end(); co_it++)
        {
          if (co_it->first->qss_species)
            continue;
          assert(co_it->second > 0);
          for (int i = 0; i < co_it->second; i++)
            ost << "*cspl[" << co_it->first->code_name << "]";
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          ost << "*thbctemp[" << reac->thb->idx << "]";
        }
        ost << ";\n";
      }
      else
        ost << "reverse_r[" << idx << "] = 0.0;\n";
    }
  }
  // Emit the fowrad and backward unimportant rates
  for (std::set<int>::const_iterator it = forward_unimportant.begin();
        it != forward_unimportant.end(); it++)
  {
    ost << "forward_r[" << ((*it)-1) << "] = 0.0;\n";
  }
  for (std::set<int>::const_iterator it = backward_unimportant.begin();
        it != backward_unimportant.end(); it++)
  {
    ost << "reverse_r[" << ((*it)-1) << "] = 0.0;\n";
  }
  // Now do the QSSA
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->emit_cpp_statements(ost); 
  }

  // Then do the Stiffness
  for (std::vector<Stif*>::const_iterator it = stif_operations.begin();
        it != stif_operations.end(); it++)
  {
    (*it)->emit_cpp_statements(ost, unit);
  }
  
  // compute the ropl values
  ost << REAL << " ropl[" << unit->all_reactions.size() << "];\n";
  ost << "for (int i = 0; i < " << unit->all_reactions.size() << "; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << "ropl[i] = forward_r[i] - reverse_r[i];\n";
  }

  // Finally compute the wdot values
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if ((*it)->qss_species)
      continue;
    IntensityCounter cnt;
    (*it)->emit_contributions(ost, cnt, false/*vector*/);
  }
}

void ChemistryUnit::emit_cuda(const char *out_name, bool emit_experiment)
{
  // Subtract out the QSS species which we won't have to do anything for
  const int total_species = (unit->species.size()-1);
  int non_qss_species = 0;
  // Go through and mark all the QSS species
  for (SpeciesSet::const_iterator it = unit->species.begin();
        it != unit->species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    assert((1 <= int((*it)->idx)) && (int((*it)->idx) <= total_species));
    // Otherwise see if it is a QSS species
    bool qss_spec = false;
    for (std::vector<ConnectedComponent*>::const_iterator cc = connected_components.begin();
          cc != connected_components.end(); cc++)
    {
      for (std::map<int,QSS*>::const_iterator sp = (*cc)->species.begin();
            sp != (*cc)->species.end(); sp++)
      {
        assert(sp->first == sp->second->qss_idx);
        if (int((*it)->idx) == sp->second->species_idx)
        {
          (*it)->qss_species = true;
          qss_spec = true;
          break;
        }
      }
      if (qss_spec)
        break;
    }
    if (!qss_spec)
      non_qss_species++;
    // Also check to see if it is a stif species
    for (std::vector<Stif*>::const_iterator st = stif_operations.begin();
          st != stif_operations.end(); st++)
    {
      if ((*st)->k == int((*it)->idx))
      {
        (*st)->species = *it;
        break;
      }
    }
  }
  fprintf(stdout,"  INFO: Non-QSSA species: %d\n", non_qss_species);
  fprintf(stdout,"  INFO: QSSA species: %d\n",total_species-non_qss_species);

  // Create all the warps that we will need for this computation
  for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
  {
    qssa_warps.push_back(new Warp(idx));
    stif_warps.push_back(new Warp(idx));
    last_warps.push_back(new Warp(idx));
    full_warps.push_back(new Warp(idx));
  }
  // Find the set of QSSA reactions
  std::set<Reaction*> qssa_reactions;
  for (unsigned idx = 0; idx < connected_components.size(); idx++)
  {
    ConnectedComponent *cc = connected_components[idx];
    for (std::map<int,QSS*>::const_iterator qit = cc->species.begin();
          qit != cc->species.end(); qit++)
    {
      for (std::map<int,int>::const_iterator it = qit->second->forward_zeros.begin();
            it != qit->second->forward_zeros.end(); it++)
      {
        assert((it->first) > 0);
        assert((it->first-1) < int(unit->all_reactions.size()));
        assert(int(unit->all_reactions[it->first-1]->get_global_idx()) == it->first);
        qssa_reactions.insert(unit->all_reactions[it->first-1]);
      }
      for (std::map<int,int>::const_iterator it = qit->second->backward_zeros.begin();
            it != qit->second->backward_zeros.end(); it++)
      {
        assert((it->first) > 0);
        assert((it->first-1) < int(unit->all_reactions.size()));
        assert(int(unit->all_reactions[it->first-1]->get_global_idx()) == it->first);
        qssa_reactions.insert(unit->all_reactions[it->first-1]);
      }
      for (std::set<int>::const_iterator it = qit->second->forward_denom.begin();
            it != qit->second->forward_denom.end(); it++)
      {
        assert((*it) > 0);
        assert(((*it)-1) < int(unit->all_reactions.size()));
        assert(int(unit->all_reactions[(*it)-1]->get_global_idx()) == (*it));
        qssa_reactions.insert(unit->all_reactions[(*it)-1]);
      }
      for (std::set<int>::const_iterator it = qit->second->backward_denom.begin();
            it != qit->second->backward_denom.end(); it++)
      {
        assert((*it) > 0);
        assert(((*it)-1) < int(unit->all_reactions.size()));
        assert(int(unit->all_reactions[(*it)-1]->get_global_idx()) == (*it));
        qssa_reactions.insert(unit->all_reactions[(*it)-1]);
      }
      for (std::map<std::pair<int,int>,int>::const_iterator it = qit->second->forward_contributions.begin();
            it != qit->second->forward_contributions.end(); it++)
      {
        assert(it->first.second > 0);
        assert((it->first.second-1) < int(unit->all_reactions.size()));
        assert(int(unit->all_reactions[it->first.second-1]->get_global_idx()) == it->first.second);
        qssa_reactions.insert(unit->all_reactions[it->first.second-1]);
      }
      for (std::map<std::pair<int,int>,int>::const_iterator it = qit->second->backward_contributions.begin();
            it != qit->second->backward_contributions.end(); it++)
      {
        assert(it->first.second > 0);
        assert((it->first.second-1) < int(unit->all_reactions.size()));
        assert(int(unit->all_reactions[it->first.second-1]->get_global_idx()) == it->first.second);
        qssa_reactions.insert(unit->all_reactions[it->first.second-1]);
      }
    }
  }
  fprintf(stdout,"  INFO: There are %ld reactions needed for QSSA\n", qssa_reactions.size());
  std::set<Reaction*> complement_qssa_reactions;
  for (std::vector<Reaction*>::const_iterator it = unit->all_reactions.begin();
        it != unit->all_reactions.end(); it++)
  {
    if (qssa_reactions.find(*it) == qssa_reactions.end())
      complement_qssa_reactions.insert(*it);
  }
  fprintf(stdout,"  INFO: There are %ld compliment reactions post QSSA\n",complement_qssa_reactions.size());
  // Find the set of Stif reactions
  std::set<Reaction*> stif_reactions;
  for (unsigned idx = 0; idx < stif_operations.size(); idx++)
  {
    Stif *stif = stif_operations[idx];
    for (std::map<int,int>::const_iterator it = stif->forward_d.begin();
          it != stif->forward_d.end(); it++)
    {
      assert(it->first > 0);
      assert((it->first-1) < int(unit->all_reactions.size()));
      assert(int(unit->all_reactions[it->first-1]->get_global_idx()) == it->first);
      stif_reactions.insert(unit->all_reactions[it->first-1]);
    }
    for (std::map<int,int>::const_iterator it = stif->backward_d.begin();
          it != stif->backward_d.end(); it++)
    {
      assert(it->first > 0);
      assert((it->first-1) < int(unit->all_reactions.size()));
      assert(int(unit->all_reactions[it->first-1]->get_global_idx())== it->first);
      stif_reactions.insert(unit->all_reactions[it->first-1]);
    }
    for (std::map<int,int>::const_iterator it = stif->forward_c.begin();
          it != stif->forward_c.end(); it++)
    {
      assert(it->first > 0);
      assert((it->first-1) < int(unit->all_reactions.size()));
      assert(int(unit->all_reactions[it->first-1]->get_global_idx()) == it->first);
      stif_reactions.insert(unit->all_reactions[it->first-1]);
    }
    for (std::map<int,int>::const_iterator it = stif->backward_c.begin();
          it != stif->backward_c.end(); it++)
    {
      assert(it->first > 0);
      assert((it->first-1) < int(unit->all_reactions.size()));
      assert(int(unit->all_reactions[it->first-1]->get_global_idx()) == it->first);
      stif_reactions.insert(unit->all_reactions[it->first-1]);
    }
  }
  // Filter out any reactions which are duplicates but are not owners
  {
    std::vector<Reaction*> to_delete;
    for (std::set<Reaction*>::iterator it = qssa_reactions.begin();
          it != qssa_reactions.end(); it++)
    {
      if ((*it)->duplicate.enabled && !(*it)->duplicate.owner)
        to_delete.push_back(*it);
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      qssa_reactions.erase(*it);
    }
    to_delete.clear();
    for (std::set<Reaction*>::iterator it = stif_reactions.begin();
          it != stif_reactions.end(); it++)
    {
      if ((*it)->duplicate.enabled && !(*it)->duplicate.owner)
        to_delete.push_back(*it);
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      stif_reactions.erase(*it);
    }
    to_delete.clear();
    for (std::set<Reaction*>::iterator it = complement_qssa_reactions.begin();
          it != complement_qssa_reactions.end(); it++)
    {
      if ((*it)->duplicate.enabled && !(*it)->duplicate.owner)
        to_delete.push_back(*it);
    }
    for (std::vector<Reaction*>::const_iterator it = to_delete.begin();
          it != to_delete.end(); it++)
    {
      complement_qssa_reactions.erase(*it);
    }
    to_delete.clear();
  }
  fprintf(stdout,"  INFO: There are %ld reactions needed for Stiffness\n", stif_reactions.size());
  // First break the QSSA reactions across the warps
  int last_qss_idx = -1;
  int first_post_qss_idx = -1;
  int max_reactions = -1;
  // Keep track of where the low reactions are so we can load
  // balance them across the warps
  {
    // Sort them into weird reactions and normal reactions
    std::vector<Reaction*> weird_reactions;
    std::vector<Reaction*> normal_reactions;
    for (std::set<Reaction*>::const_iterator it = qssa_reactions.begin();
          it != qssa_reactions.end(); it++)
    {
      if (is_weird_reaction(*it))
        weird_reactions.push_back(*it);
      else
        normal_reactions.push_back(*it);
    }
    std::vector<Reaction*> other_weird; // weird by not low or troe
    std::vector<Reaction*> low_reactions; // low reactions but not troe
    std::vector<Reaction*> troe_reactions;
    {
      int num_expensive_reactions = 0;
      for (std::vector<Reaction*>::const_iterator it = weird_reactions.begin();
            it != weird_reactions.end(); it++)
      {
        if ((*it)->low.enabled)
        {
          num_expensive_reactions++;
          if ((*it)->troe.num > 0)
            troe_reactions.push_back(*it);
          else
            low_reactions.push_back(*it);
        }
        else
          other_weird.push_back(*it);
      }
      // Try to keep the expensive reactions balanced by pulling more
      // expensive reactions into this phase
      while (!((num_expensive_reactions%unit->threads_per_point) == 0))
      {
        bool found_expensive = false;
        for (std::set<Reaction*>::iterator it = complement_qssa_reactions.begin();
              it != complement_qssa_reactions.end(); it++)
        {
          if ((*it)->low.enabled)
          {
            num_expensive_reactions++;
            found_expensive = true;
            weird_reactions.push_back(*it);
            if ((*it)->troe.num > 0)
              troe_reactions.push_back(*it);
            else
              low_reactions.push_back(*it);
            complement_qssa_reactions.erase(it);
            break;
          }
        }
        if (!found_expensive)
          break;
      }
    }
    int next_index = 0;
    for (std::vector<Reaction*>::const_iterator it = weird_reactions.begin();
          it != weird_reactions.end(); it++)
    {
      qssa_warps[next_index]->add_reaction((*it)->get_global_idx());
      qssa_warps[next_index]->set_reaction((*it)->get_global_idx(), (*it));
      next_index++;
      if (next_index == int(qssa_warps.size()))
        next_index = 0;
    }
    for (std::vector<Reaction*>::const_iterator it = normal_reactions.begin();
          it != normal_reactions.end(); it++)
    {
      qssa_warps[next_index]->add_reaction((*it)->get_global_idx());
      qssa_warps[next_index]->set_reaction((*it)->get_global_idx(), (*it));
      next_index++;
      if (next_index == int(qssa_warps.size()))
        next_index = 0;
    }
    std::map<int/*warp idx*/,int/*overhead*/> overheads;
    std::set<int> expensive_filter;
    std::set<int> empty_filter;
    // Schedule for the first pass
    for (std::vector<Reaction*>::const_iterator it = troe_reactions.begin();
          it != troe_reactions.end(); it++)
    {
      int min_index = find_minimum_warp(full_warps, overheads, 0/*pass*/, expensive_filter);
      full_warps[min_index]->add_reaction((*it)->get_global_idx(), 0/*pass*/);
      full_warps[min_index]->set_reaction((*it)->get_global_idx(), (*it), 0/*pass*/);
      if (overheads.find(min_index) == overheads.end())
        overheads[min_index] = EXPENSIVE_REAC_COST;
      else
        overheads[min_index] += EXPENSIVE_REAC_COST;
      expensive_filter.insert(min_index);
      if (expensive_filter.size() == full_warps.size())
        expensive_filter.clear();
    }

    for (std::vector<Reaction*>::const_iterator it = low_reactions.begin();
          it != low_reactions.end(); it++)
    {
      int min_index = find_minimum_warp(full_warps, overheads, 0/*pass*/, expensive_filter);
      full_warps[min_index]->add_reaction((*it)->get_global_idx(), 0/*pass*/);
      full_warps[min_index]->set_reaction((*it)->get_global_idx(), (*it), 0/*pass*/);
      if (overheads.find(min_index) == overheads.end())
        overheads[min_index] = EXPENSIVE_REAC_COST;
      else
        overheads[min_index] += EXPENSIVE_REAC_COST;
      expensive_filter.insert(min_index);
      if (expensive_filter.size() == full_warps.size())
        expensive_filter.clear();
    }

    for (std::vector<Reaction*>::const_iterator it = other_weird.begin();
          it != other_weird.end(); it++)
    {
      int min_index = find_minimum_warp(full_warps, overheads, 0/*pass*/, empty_filter);
      full_warps[min_index]->add_reaction((*it)->get_global_idx(), 0/*pass*/);
      full_warps[min_index]->set_reaction((*it)->get_global_idx(), (*it), 0/*pass*/);
    }

    for (std::vector<Reaction*>::const_iterator it = normal_reactions.begin();
          it != normal_reactions.end(); it++)
    {
      int min_index = find_minimum_warp(full_warps, overheads, 0/*pass*/, empty_filter);
      full_warps[min_index]->add_reaction((*it)->get_global_idx(), 0/*pass*/);
      full_warps[min_index]->set_reaction((*it)->get_global_idx(), (*it), 0/*pass*/);
    }

    normal_reactions.clear();
    weird_reactions.clear();
    other_weird.clear();
    low_reactions.clear();
    troe_reactions.clear();
    for (std::set<Reaction*>::const_iterator it = complement_qssa_reactions.begin();
          it != complement_qssa_reactions.end(); it++)
    {
      if (is_weird_reaction(*it))
      {
        weird_reactions.push_back(*it);
        if ((*it)->low.enabled)
        {
          if ((*it)->troe.num > 0)
            troe_reactions.push_back(*it);
          else
            low_reactions.push_back(*it);
        }
        else
          other_weird.push_back(*it);
      }
      else
        normal_reactions.push_back(*it);
    }

    // Finish load balancing the first pass
    while (!balanced_warps(full_warps, overheads, 0/*pass*/) && !normal_reactions.empty())
    {
      //assert(!normal_reactions.empty());  
      Reaction *reac = normal_reactions.back();
      normal_reactions.pop_back();
      int min_index = find_minimum_warp(full_warps, overheads, 0/*pass*/, empty_filter);
      full_warps[min_index]->add_reaction(reac->get_global_idx(), 0/*pass*/);
      full_warps[min_index]->set_reaction(reac->get_global_idx(), reac, 0/*pass*/);
    }

    // Figure out the last qss idx for any of the warps and the first non qss idx
    for (unsigned idx = 0; idx < full_warps.size(); idx++)
    {
      int num_reactions = full_warps[idx]->num_reactions(0/*pass*/);
      assert(num_reactions > 0);
      if (idx == 0)
      {
        last_qss_idx = num_reactions-1;
        first_post_qss_idx = num_reactions;
      }
      else
      {
        if ((num_reactions-1) > last_qss_idx)
          last_qss_idx = num_reactions-1;
        if (num_reactions < first_post_qss_idx)
          first_post_qss_idx = num_reactions;
      }
    }
    fprintf(stdout,"  INFO: last qss idx %d, first post qss idx %d\n", last_qss_idx, first_post_qss_idx);

    unsigned num_complex_components = DEFAULT_COMPLEX_COMPONENTS;
    for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
          it != connected_components.end(); it++)
    {
      if (!(*it)->is_simple())
        num_complex_components++;
    }

    overheads.clear(); // clear overheads since there is a barrier between pass 0 and 1

    // Assign penalties to warps doing QSSA
#if 0
    for (unsigned idx = 0; idx < num_complex_components; idx++)
    {
      int warp_idx = (full_warps.size()-1) - (idx % full_warps.size());
      assert(warp_idx >= 0);
      assert(warp_idx < int(full_warps.size()));
      if (overheads.find(warp_idx) == overheads.end())
        overheads[warp_idx] = QSSA_COST;
      else
        overheads[warp_idx] += QSSA_COST;
    }
#else
    for (unsigned widx = 0; widx < full_warps.size(); widx++)
    {
      if (expensive_filter.find(widx) == expensive_filter.end())
        overheads[widx] = EXPENSIVE_REAC_COST;
    }
#endif

    // Now assign reactions to the warps again
    for (std::vector<Reaction*>::const_iterator it = troe_reactions.begin();
          it != troe_reactions.end(); it++)
    {
      int min_index = find_minimum_warp(full_warps, overheads, 1/*pass*/, expensive_filter);
      full_warps[min_index]->add_reaction((*it)->get_global_idx(), 1/*pass*/);
      full_warps[min_index]->set_reaction((*it)->get_global_idx(), (*it), 1/*pass*/);
      if (overheads.find(min_index) == overheads.end())
        overheads[min_index] = EXPENSIVE_REAC_COST;
      else
        overheads[min_index] += EXPENSIVE_REAC_COST;
      expensive_filter.insert(min_index);
      if (expensive_filter.size() == full_warps.size())
        expensive_filter.clear();
    }

    for (std::vector<Reaction*>::const_iterator it = low_reactions.begin();
          it != low_reactions.end(); it++)
    {
      int min_index = find_minimum_warp(full_warps, overheads, 1/*pass*/, expensive_filter);
      full_warps[min_index]->add_reaction((*it)->get_global_idx(), 1/*pass*/);
      full_warps[min_index]->set_reaction((*it)->get_global_idx(), (*it), 1/*pass*/);
      if (overheads.find(min_index) == overheads.end())
        overheads[min_index] = EXPENSIVE_REAC_COST;
      else
        overheads[min_index] += EXPENSIVE_REAC_COST;
      expensive_filter.insert(min_index);
      if (expensive_filter.size() == full_warps.size())
        expensive_filter.clear();
    }

    for (std::vector<Reaction*>::const_iterator it = other_weird.begin();
          it != other_weird.end(); it++)
    {
      int min_index = find_minimum_warp(full_warps, overheads, 1/*pass*/, empty_filter);
      full_warps[min_index]->add_reaction((*it)->get_global_idx(), 1/*pass*/);
      full_warps[min_index]->set_reaction((*it)->get_global_idx(), (*it), 1/*pass*/);
    }

    for (std::vector<Reaction*>::const_iterator it = normal_reactions.begin();
          it != normal_reactions.end(); it++)
    {
      int min_index = find_minimum_warp(full_warps, overheads, 1/*pass*/, empty_filter);
      full_warps[min_index]->add_reaction((*it)->get_global_idx(), 1/*pass*/);
      full_warps[min_index]->set_reaction((*it)->get_global_idx(), (*it), 1/*pass*/);
    }

    for (unsigned idx = 0; idx < full_warps.size(); idx++)
    {
      int num_reactions = full_warps[idx]->num_reactions();
      assert(num_reactions > 0);
      if (idx == 0)
        max_reactions = num_reactions;
      else
      {
        if (num_reactions > max_reactions)
          max_reactions = num_reactions;
      }
    }
    fprintf(stdout,"  INFO: maximum reactions %d\n", max_reactions);

#if 0
    // For the full warps, assign weird reactions from right to left based on computational
    // intensity, then fill in the current row and switch to left to right for remaining reactions
    next_index = full_warps.size()-1;
    for (std::vector<Reaction*>::const_iterator it = troe_reactions.begin();
          it != troe_reactions.end(); it++)
    {
      full_warps[next_index]->add_reaction((*it)->get_global_idx());
      full_warps[next_index]->set_reaction((*it)->get_global_idx(), (*it));
      next_index--;
      if (next_index < 0)
      {
        next_index = full_warps.size()-1;
        last_qss_idx++;
      }
    }
    for (std::vector<Reaction*>::const_iterator it = low_reactions.begin();
          it != low_reactions.end(); it++)
    {
      full_warps[next_index]->add_reaction((*it)->get_global_idx());
      full_warps[next_index]->set_reaction((*it)->get_global_idx(), (*it));
      next_index--;
      if (next_index < 0)
      {
        next_index = full_warps.size()-1;
        last_qss_idx++;
      }
    }
    // Track where we should start assigning the next low values
    int next_low_index = next_index;
    for (std::vector<Reaction*>::const_iterator it = other_weird.begin();
          it != other_weird.end(); it++)
    {
      full_warps[next_index]->add_reaction((*it)->get_global_idx());
      full_warps[next_index]->set_reaction((*it)->get_global_idx(), (*it));
      next_index--;
      if (next_index < 0)
      {
        next_index = full_warps.size()-1;
        last_qss_idx++;
      }
    }
    // Fill out the current row with normal values
    if (next_index < int(full_warps.size()-1))
    {
      while (next_index >= 0)
      {
        assert(!normal_reactions.empty());
        Reaction *reac = normal_reactions.back();
        normal_reactions.pop_back();
        full_warps[next_index]->add_reaction(reac->get_global_idx());
        full_warps[next_index]->set_reaction(reac->get_global_idx(), reac);
        next_index--;
      }
      assert(next_index == -1);
    }
    next_index = 0;
    last_qss_idx++;
    // Now assign the remaining normal reaction values
    for (std::vector<Reaction*>::const_iterator it = normal_reactions.begin();
          it != normal_reactions.end(); it++)
    {
      full_warps[next_index]->add_reaction((*it)->get_global_idx());
      full_warps[next_index]->set_reaction((*it)->get_global_idx(), (*it));
      next_index++;
      if (next_index == int(full_warps.size()))
      {
        next_index = 0;
        last_qss_idx++;
      }
    }
    if (next_index == 0)
      last_qss_idx--;
    // Mark the last qss_off
    if (next_index > 0)
      last_qss_off = next_index;
    else
      last_qss_off = full_warps.size();

    fprintf(stdout,"  INFO: the last needed index for QSS is row %d column %d\n", last_qss_idx, last_qss_off);
    // Finish off doing the full warps, add normal reactions until the end
    // of the current index, then do all the weird reactions, then finish
    // off the normal reactions
    normal_reactions.clear();
    weird_reactions.clear();
    other_weird.clear();
    low_reactions.clear();
    troe_reactions.clear();
    for (std::set<Reaction*>::const_iterator it = complement_qssa_reactions.begin();
          it != complement_qssa_reactions.end(); it++)
    {
      if (is_weird_reaction(*it))
      {
        weird_reactions.push_back(*it);
        if ((*it)->low.enabled)
        {
          if ((*it)->troe.num > 0)
            troe_reactions.push_back(*it);
          else
            low_reactions.push_back(*it);
        }
        else
          other_weird.push_back(*it);
      }
      else
        normal_reactions.push_back(*it);
    }
    // Now figure out how many warps we need for doing complicated QSSA
    // connected components
    unsigned num_complex_components = DEFAULT_COMPLEX_COMPONENTS;
    for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
          it != connected_components.end(); it++)
    {
      if (!(*it)->is_simple())
        num_complex_components++;
    }
    assert(num_complex_components < full_warps.size());
    // fill out the current row with normal reactions only if there was a partial row
    if (next_index > 0)
    {
      for (int index = (full_warps.size()-(1+num_complex_components));
            index >= next_index; index--)
      {
        assert(!normal_reactions.empty());
        Reaction *reac = normal_reactions.back();
        normal_reactions.pop_back();
        full_warps[index]->add_reaction(reac->get_global_idx());
        full_warps[index]->set_reaction(reac->get_global_idx(), reac);
      }
    }
    // Now go through and assign normal reactions from left to right until
    // we get back to the next low index
    unsigned num_complex_skips = 0;
    next_index = full_warps.size()-(1+num_complex_components);
    assert(next_low_index > -1);
#if 0
    while (next_index > next_low_index)
    {
      assert(!normal_reactions.empty()); 
      Reaction *reac = normal_reactions.back();
      normal_reactions.pop_back();
      full_warps[next_index]->add_reaction(reac->get_global_idx());
      full_warps[next_index]->set_reaction(reac->get_global_idx(), reac);
      next_index--;
    }
#endif
    std::map<int/*index*/,int/*to skip*/> to_skip;
    // Now assign the troe, low, and other weird reaction from right to left
    for (std::vector<Reaction*>::const_iterator it = troe_reactions.begin();
          it != troe_reactions.end(); it++)
    {
      full_warps[next_index]->add_reaction((*it)->get_global_idx());
      full_warps[next_index]->set_reaction((*it)->get_global_idx(), (*it));
      if (to_skip.find(next_index) == to_skip.end())
        to_skip[next_index] = EXPENSIVE_REAC_COST;
      else
        to_skip[next_index] += EXPENSIVE_REAC_COST;
      next_index--;
      if (next_index < 0)
      {
        num_complex_skips++;
        if (num_complex_skips < COMPLEX_COMPONENT_SKIPS)
          next_index = full_warps.size()-(1+num_complex_components);
        else
          next_index = full_warps.size()-1;
      }
    }
    for (std::vector<Reaction*>::const_iterator it = low_reactions.begin();
          it != low_reactions.end(); it++)
    {
      full_warps[next_index]->add_reaction((*it)->get_global_idx());
      full_warps[next_index]->set_reaction((*it)->get_global_idx(), (*it));
      if (to_skip.find(next_index) == to_skip.end())
        to_skip[next_index] = EXPENSIVE_REAC_COST;
      else
        to_skip[next_index] += EXPENSIVE_REAC_COST;
      next_index--;
      if (next_index < 0)
      {
        num_complex_skips++;
        if (num_complex_skips < COMPLEX_COMPONENT_SKIPS)
          next_index = full_warps.size()-(1+num_complex_components);
        else
          next_index = full_warps.size()-1;
      }
    }
    fprintf(stdout,"  INFO: the last low-troe index is %d\n", (next_index+1));
    for (std::vector<Reaction*>::const_iterator it = other_weird.begin();
          it != other_weird.end(); it++)
    {
      full_warps[next_index]->add_reaction((*it)->get_global_idx());
      full_warps[next_index]->set_reaction((*it)->get_global_idx(), (*it));
      next_index--;
      if (next_index < 0)
      {
        num_complex_skips++;
        if (num_complex_skips < COMPLEX_COMPONENT_SKIPS)
          next_index = full_warps.size()-(1+num_complex_components);
        else
          next_index = full_warps.size()-1;
      }
    }
    // finish off the current row, then go left to right for the remaining normal
    // reactions until we are done
    if (next_index < int(full_warps.size()-(1+num_complex_components)))
    {
      while (next_index >= 0)
      {
        assert(!normal_reactions.empty());
        Reaction *reac = normal_reactions.back();
        normal_reactions.pop_back();
        full_warps[next_index]->add_reaction(reac->get_global_idx());
        full_warps[next_index]->set_reaction(reac->get_global_idx(), reac);
        next_index--;
      }
      assert(next_index == -1);
    }
    next_index = 0;
    num_complex_skips++;
    for (std::vector<Reaction*>::const_iterator it = normal_reactions.begin();
          it != normal_reactions.end(); it++)
    {
      while ((to_skip.find(next_index) != to_skip.end()) && (to_skip[next_index] > 0))
      {
        to_skip[next_index]--;
        next_index++;
        if (num_complex_skips < COMPLEX_COMPONENT_SKIPS)
        {
          if (next_index == int(full_warps.size()-num_complex_components))
          {
            num_complex_skips++;
            next_index = 0;
          }
        }
        else
        {
          if (next_index == int(full_warps.size()))
            next_index = 0;
        }
      }
      full_warps[next_index]->add_reaction((*it)->get_global_idx());
      full_warps[next_index]->set_reaction((*it)->get_global_idx(), (*it));
      next_index++;
      if (num_complex_skips < COMPLEX_COMPONENT_SKIPS)
      {
        if (next_index == int(full_warps.size()-num_complex_components))
        {
          num_complex_skips++;
          next_index = 0;
        }
      }
      else
      {
        if (next_index == int(full_warps.size()))
          next_index = 0;
      }
    }
#endif
  }
  // Now break the Stiffness reactions across the warps making sure that
  // reactions that are also QSSA assigned to the same warp
  {
    std::vector<Reaction*> weird_reactions;
    std::vector<Reaction*> normal_reactions;
    for (std::set<Reaction*>::const_iterator it = stif_reactions.begin();
          it != stif_reactions.end(); it++)
    {
      if (is_weird_reaction(*it))
        weird_reactions.push_back(*it);
      else
        normal_reactions.push_back(*it);
    }
    for (std::vector<Reaction*>::const_iterator it = weird_reactions.begin();
          it != weird_reactions.end(); it++)
    {
#if 0
      if (qssa_reactions.find(*it) != qssa_reactions.end())
      {
        int qssa_index = -1;
        for (unsigned index = 0; index < qssa_warps.size(); index++)
        {
          if (qssa_warps[index]->has_reaction((*it)->get_global_idx()))
          {
            qssa_index = index;
            break;
          }
        }
        assert(qssa_index >= 0);
        stif_warps[qssa_index]->add_reaction((*it)->get_global_idx());
        stif_warps[qssa_index]->set_reaction((*it)->get_global_idx(),(*it));
      }
      else
#endif
      {
        assert(!stif_warps.empty());
        // Otherwise load balance reactions
        int smallest_index = 0;
        int smallest_reactions = stif_warps[0]->reactions.size();
        for (unsigned index = 1; index < stif_warps.size(); index++)
        {
          if (int(stif_warps[index]->reactions.size()) < smallest_reactions)
          {
            smallest_index = index;
            smallest_reactions = stif_warps[index]->reactions.size();
          }
        }
        stif_warps[smallest_index]->add_reaction((*it)->get_global_idx());
        stif_warps[smallest_index]->set_reaction((*it)->get_global_idx(),(*it));
      }
    }
    for (std::vector<Reaction*>::const_iterator it = normal_reactions.begin();
          it != normal_reactions.end(); it++)
    {
#if 0
      if (qssa_reactions.find(*it) != qssa_reactions.end())
      {
        int qssa_index = -1;
        for (unsigned index = 0; index < qssa_warps.size(); index++)
        {
          if (qssa_warps[index]->has_reaction((*it)->get_global_idx()))
          {
            qssa_index = index;
            break;
          }
        }
        assert(qssa_index >= 0);
        stif_warps[qssa_index]->add_reaction((*it)->get_global_idx());
        stif_warps[qssa_index]->set_reaction((*it)->get_global_idx(),(*it));
      }
      else
#endif
      {
        assert(!stif_warps.empty());
        // Otherwise load balance reactions
        int smallest_index = 0;
        int smallest_reactions = stif_warps[0]->reactions.size();
        for (unsigned index = 1; index < stif_warps.size(); index++)
        {
          if (int(stif_warps[index]->reactions.size()) < smallest_reactions)
          {
            smallest_index = index;
            smallest_reactions = stif_warps[index]->reactions.size();
          }
        }
        stif_warps[smallest_index]->add_reaction((*it)->get_global_idx());
        stif_warps[smallest_index]->set_reaction((*it)->get_global_idx(),(*it));
      }
    }
  }
  // Finally assign all the reactions to warps
  {
    std::vector<Reaction*> weird_reactions;
    std::vector<Reaction*> normal_reactions;
    for (std::vector<Reaction*>::const_iterator it = unit->all_reactions.begin();
          it != unit->all_reactions.end(); it++)
    {
      if (is_weird_reaction(*it))
        weird_reactions.push_back(*it);
      else
        normal_reactions.push_back(*it);
    }
    for (std::vector<Reaction*>::const_iterator it = weird_reactions.begin();
          it != weird_reactions.end(); it++)
    {
#if 0
      if (qssa_reactions.find(*it) != qssa_reactions.end())
      {
        int qssa_index = -1;
        for (unsigned index = 0; index < qssa_warps.size(); index++)
        {
          if (qssa_warps[index]->has_reaction((*it)->get_global_idx()))
          {
            qssa_index = index;
            break;
          }
        }
        assert(qssa_index >= 0);
        last_warps[qssa_index]->add_reaction((*it)->get_global_idx());
        last_warps[qssa_index]->set_reaction((*it)->get_global_idx(),(*it));
      }
      else if (stif_reactions.find(*it) != stif_reactions.end())
      {
        int stif_index = -1;
        for (unsigned index = 0; index < stif_warps.size(); index++)
        {
          if (stif_warps[index]->has_reaction((*it)->get_global_idx()))
          {
            stif_index = index;
            break;
          }
        }
        assert(stif_index >= 0);
        last_warps[stif_index]->add_reaction((*it)->get_global_idx());
        last_warps[stif_index]->set_reaction((*it)->get_global_idx(),(*it));
      }
      else
#endif
      {
        assert(!last_warps.empty());
        int smallest_index = 0;
        int smallest_reactions = last_warps[0]->reactions.size();
        for (unsigned index = 1; index < last_warps.size(); index++)
        {
          if (int(last_warps[index]->reactions.size()) < smallest_reactions)
          {
            smallest_index = index;
            smallest_reactions = last_warps[index]->reactions.size();
          }
        }
        last_warps[smallest_index]->add_reaction((*it)->get_global_idx());
        last_warps[smallest_index]->set_reaction((*it)->get_global_idx(),(*it));
      }
    }
    for (std::vector<Reaction*>::const_iterator it = normal_reactions.begin();
          it != normal_reactions.end(); it++)
    {
#if 0
      if (qssa_reactions.find(*it) != qssa_reactions.end())
      {
        int qssa_index = -1;
        for (unsigned index = 0; index < qssa_warps.size(); index++)
        {
          if (qssa_warps[index]->has_reaction((*it)->get_global_idx()))
          {
            qssa_index = index;
            break;
          }
        }
        assert(qssa_index >= 0);
        last_warps[qssa_index]->add_reaction((*it)->get_global_idx());
        last_warps[qssa_index]->set_reaction((*it)->get_global_idx(),(*it));
      }
      else if (stif_reactions.find(*it) != stif_reactions.end())
      {
        int stif_index = -1;
        for (unsigned index = 0; index < stif_warps.size(); index++)
        {
          if (stif_warps[index]->has_reaction((*it)->get_global_idx()))
          {
            stif_index = index;
            break;
          }
        }
        assert(stif_index >= 0);
        last_warps[stif_index]->add_reaction((*it)->get_global_idx());
        last_warps[stif_index]->set_reaction((*it)->get_global_idx(),(*it));
      }
      else
#endif
      {
        assert(!last_warps.empty());
        int smallest_index = 0;
        int smallest_reactions = last_warps[0]->reactions.size();
        for (unsigned index = 1; index < last_warps.size(); index++)
        {
          if (int(last_warps[index]->reactions.size()) < smallest_reactions)
          {
            smallest_index = index;
            smallest_reactions = last_warps[index]->reactions.size();
          }
        }
        last_warps[smallest_index]->add_reaction((*it)->get_global_idx());
        last_warps[smallest_index]->set_reaction((*it)->get_global_idx(),(*it));
      }
    }
  }
  // Now let's do the full warps
  {
    // Sort reactions into four categories listed here in
    // order of reverse computational intensity
    std::vector<Reaction*> normal_reactions;
    
  }
  for (unsigned index = 0; index < last_warps.size(); index++)
  {
    fprintf(stdout,"  INFO: Warp %d has reactions qssa: %ld  stif: %ld  last: %ld  full: %ld\n",
      index, qssa_warps[index]->reactions.size(), stif_warps[index]->reactions.size(), 
      last_warps[index]->reactions.size(), full_warps[index]->reactions.size());
  }
  fprintf(stdout,"  INFO: the last QSS idx for the full warps is %d\n", last_qss_idx);
  // Now we can do the code generation
  gibbs_species.clear();

  const char *temp_last_file  = "__temp_last__.cc";
  {
    std::vector<std::map<QSS*,int> > qss_partition(unit->threads_per_point);
    int max_qss_values, max_qss_denoms;
    // Partition up the QSS values
    {
      std::list<std::pair<QSS*,int> > qss_elements;
      for (std::vector<ConnectedComponent*>::const_iterator cc = connected_components.begin();
            cc != connected_components.end(); cc++)
      {
        for (std::map<int,QSS*>::const_iterator qit = (*cc)->species.begin();
              qit != (*cc)->species.end(); qit++)
        {
          int num_vals = qit->second->get_total_values();
          bool added = false;
          for (std::list<std::pair<QSS*,int> >::iterator it = qss_elements.begin();
                it != qss_elements.end(); it++)
          {
            if (num_vals > it->second)
            {
              qss_elements.insert(it,std::pair<QSS*,int>(qit->second,num_vals));
              added = true;
              break;
            }
          }
          if (!added)
            qss_elements.push_back(std::pair<QSS*,int>(qit->second,num_vals));
        }
      }
      // Now assign the elements to warps
      std::vector<int> total_avals(unit->threads_per_point);
      for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
        total_avals[idx] = 0;
      for (std::list<std::pair<QSS*,int> >::const_iterator it = qss_elements.begin();
            it != qss_elements.end(); it++)
      {
        // Find the warp with the fewest elements
        int fewest_idx = 0;
        int fewest_elements = total_avals[0];
        for (unsigned idx = 1; idx < total_avals.size(); idx++)
        {
          if (total_avals[idx] < fewest_elements)
          {
            fewest_elements = total_avals[idx];
            fewest_idx = idx;
          }
        }
        qss_partition[fewest_idx][it->first] = it->second;
        total_avals[fewest_idx] += it->second;
      }
      // Now compute the max of each 
      max_qss_values = 0;
      max_qss_denoms = 0;
      for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
      {
        if (total_avals[idx] > max_qss_values)
          max_qss_values = total_avals[idx];
        if (int(qss_partition[idx].size()) > max_qss_denoms)
          max_qss_denoms = qss_partition[idx].size();
      }
    }
    CodeOutStream ost(temp_last_file, true/*bound line lengths*/, 80);
    ost << "#pragma unroll 1\n"; // don't ever unroll this loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim step_loop_pair(ost);
    emit_last_reaction_code(ost, full_warps, non_qss_species, last_qss_idx, first_post_qss_idx, max_reactions, 
                            qss_partition, max_qss_values, max_qss_denoms);
    //emit_multi_reaction_code(ost, qssa_warps, non_qss_species, 0/*pass*/, QSSA_WARPS);
    // Need updates to temperature, pressure, and mass fraction
#if 1
#if 0
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
    ost << DIFFUSION_ARRAY << " += slice_stride;\n";
    ost << WDOT << " += slice_stride;\n";
#endif
#else
    {
      PairDelim update_pair(ost);
      ost << REAL << " *temp_temp = (" << REAL << "*)(__double_as_longlong(__hiloint2double(hi_temp,lo_temp)));\n";
      ost << "temp_temp += slice_stride;\n";
      ost << "long long int temp_long = (long long int)temp_temp;\n";
      ost << "hi_temp = __double2hiint(__longlong_as_double(temp_long));\n";
      ost << "lo_temp = __double2loint(__longlong_as_double(temp_long));\n";
      ost << "temp_long = __double_as_longlong(__hiloint2double(lo_temp,hi_temp)) + step*step_stride;\n";
      ost << "hi_temp = __double2loint(__longlong_as_double(temp_long));\n";
      ost << "lo_temp = __double2hiint(__longlong_as_double(temp_long));\n";
      ost << REAL << " *press_temp = (" << REAL << "*)(__double_as_longlong(__hiloint2double(hi_press,lo_press)));\n";
      ost << "press_temp += slice_stride;\n";
      ost << "long long int press_long = (long long int)press_temp;\n";
      ost << "hi_press = __double2hiint(__longlong_as_double(press_long));\n";
      ost << "lo_press = __double2loint(__longlong_as_double(press_long));\n";
      ost << "press_long = __double_as_longlong(__hiloint2double(lo_press,hi_press)) + step*step_stride;\n";
      ost << "hi_press = __double2loint(__longlong_as_double(press_long));\n";
      ost << "lo_press = __double2hiint(__longlong_as_double(press_long));\n";
      ost << REAL << " *mass_temp = (" << REAL << "*)(__double_as_longlong(__hiloint2double(hi_mass,lo_mass)));\n";
      ost << "mass_temp += slice_stride;\n";
      ost << "long long int mass_long = (long long int)mass_temp;\n";
      ost << "hi_mass = __double2hiint(__longlong_as_double(mass_long));\n";
      ost << "lo_mass = __double2loint(__longlong_as_double(mass_long));\n";
      ost << "mass_long = __double_as_longlong(__hiloint2double(lo_mass,hi_mass)) + step*step_stride;\n";
      ost << "hi_mass = __double2loint(__longlong_as_double(mass_long));\n";
      ost << "lo_mass = __double2hiint(__longlong_as_double(mass_long));\n";
      ost << REAL << " *diff_temp = (" << REAL << "*)(__double_as_longlong(__hiloint2double(hi_diff,lo_diff)));\n";
      ost << "diff_temp += slice_stride;\n";
      ost << "long long int diff_long = (long long int)diff_temp;\n";
      ost << "hi_diff = __double2hiint(__longlong_as_double(diff_long));\n";
      ost << "lo_diff = __double2loint(__longlong_as_double(diff_long));\n";
      ost << "diff_long = __double_as_longlong(__hiloint2double(lo_diff,hi_diff)) + step*step_stride;\n";
      ost << "hi_diff = __double2loint(__longlong_as_double(diff_long));\n";
      ost << "lo_diff = __double2hiint(__longlong_as_double(diff_long));\n";
      ost << REAL << " *wdot_temp = (" << REAL << "*)(__double_as_longlong(__hiloint2double(hi_wdot,lo_wdot)));\n";
      ost << "wdot_temp += slice_stride;\n";
      ost << "long long int wdot_long = (long long int)wdot_temp;\n";
      ost << "hi_wdot = __double2hiint(__longlong_as_double(wdot_long));\n";
      ost << "lo_wdot = __double2loint(__longlong_as_double(wdot_long));\n";
      ost << "wdot_long = __double_as_longlong(__hiloint2double(lo_wdot,hi_wdot)) + step*step_stride;\n";
      ost << "hi_wdot = __double2loint(__longlong_as_double(wdot_long));\n";
      ost << "lo_wdot = __double2hiint(__longlong_as_double(wdot_long));\n";
    }
#endif
  }
  {
    char source_name[128];
    snprintf(source_name,127,"%s_gpu.cu",out_name);
    CodeOutStream ost(source_name,true/*bound line lengths*/, 80);
    emit_last_getrates_code(ost, non_qss_species, temp_last_file);
    if (emit_experiment)
      emit_gpu_experiment(ost, non_qss_species, "gpu_getrates", true/*striding z*/, false/*multi*/);
  }

#if 0
  const char *temp_qssa_file = "__temp_qssa__.cc";
  const char *temp_stif_file = "__temp_stif__.cc";
  const char *temp_last_file  = "__temp_last__.cc";
  {
    std::vector<std::map<QSS*,int> > qss_partition(unit->threads_per_point);
    int max_qss_values, max_qss_denoms;
    // Partition up the QSS values
    {
      std::list<std::pair<QSS*,int> > qss_elements;
      for (std::vector<ConnectedComponent*>::const_iterator cc = connected_components.begin();
            cc != connected_components.end(); cc++)
      {
        for (std::map<int,QSS*>::const_iterator qit = (*cc)->species.begin();
              qit != (*cc)->species.end(); qit++)
        {
          int num_vals = qit->second->get_total_values();
          bool added = false;
          for (std::list<std::pair<QSS*,int> >::iterator it = qss_elements.begin();
                it != qss_elements.end(); it++)
          {
            if (num_vals > it->second)
            {
              qss_elements.insert(it,std::pair<QSS*,int>(qit->second,num_vals));
              added = true;
              break;
            }
          }
          if (!added)
            qss_elements.push_back(std::pair<QSS*,int>(qit->second,num_vals));
        }
      }
      // Now assign the elements to warps
      std::vector<int> total_avals(unit->threads_per_point);
      for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
        total_avals[idx] = 0;
      for (std::list<std::pair<QSS*,int> >::const_iterator it = qss_elements.begin();
            it != qss_elements.end(); it++)
      {
        // Find the warp with the fewest elements
        int fewest_idx = 0;
        int fewest_elements = total_avals[0];
        for (unsigned idx = 1; idx < total_avals.size(); idx++)
        {
          if (total_avals[idx] < fewest_elements)
          {
            fewest_elements = total_avals[idx];
            fewest_idx = idx;
          }
        }
        qss_partition[fewest_idx][it->first] = it->second;
        total_avals[fewest_idx] += it->second;
      }
      // Now compute the max of each 
      max_qss_values = 0;
      max_qss_denoms = 0;
      for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
      {
        if (total_avals[idx] > max_qss_values)
          max_qss_values = total_avals[idx];
        if (int(qss_partition[idx].size()) > max_qss_denoms)
          max_qss_denoms = qss_partition[idx].size();
      }
    }
    CodeOutStream ost(temp_qssa_file, true/*bound line lengths*/, 80);
    ost << "#pragma unroll 1\n"; // don't ever unroll this loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim step_loop_pair(ost);
    emit_multi_qssa_kernel(ost, qssa_warps, non_qss_species, qss_partition, max_qss_values, max_qss_denoms);
    //emit_multi_reaction_code(ost, qssa_warps, non_qss_species, 0/*pass*/, QSSA_WARPS);
    // Need updates to temperature, pressure, and mass fraction
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
    ost << QSSA_ARRAY << " += slice_stride;\n";
  }
  {
    CodeOutStream ost(temp_stif_file, true/*bound line lengths*/, 80);
    ost << "#pragma unroll 1\n"; // don't ever unroll this loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim step_loop_pair(ost);
    emit_multi_reaction_code(ost, stif_warps, non_qss_species, 1/*pass*/, STIF_WARPS);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
  }
  {
    CodeOutStream ost(temp_last_file, true/*bound line lengths*/, 80);
    ost << "#pragma unroll 1\n"; // don't ever unroll this loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim step_loop_pair(ost);
    emit_multi_reaction_code(ost, last_warps, non_qss_species, 2/*pass*/, LAST_WARPS);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
  }
  // Now emit the getrates code
  {
    char source_name[128];
    snprintf(source_name,127,"%s_gpu.cu",out_name);
    CodeOutStream ost(source_name,true/*bound line lengths*/, 80);
    emit_multi_getrates_code(ost, non_qss_species, temp_qssa_file, temp_stif_file, temp_last_file);
    if (emit_experiment)
      emit_gpu_experiment(ost, non_qss_species, "gpu_getrates", true/*striding z*/, true/*multi*/);
  }
#endif

#if 0
  const char *temp_file_name = "__temp__getrates__.cc";
  {
    CodeOutStream ost(temp_file_name, true/*bound line lengths*/, 80);   
    ost << "#pragma unroll 1\n"; // don't ever unroll this loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim step_loop_pair(ost);
    // Emit each of the three phases interleaved 
    emit_pipe_reaction_code(ost, qssa_warps, non_qss_species, 0/*phase*/, 0/*pass*/);
    emit_pipe_reaction_code(ost, qssa_warps, non_qss_species, 1/*phase*/, 0/*pass*/);
    emit_pipe_reaction_code(ost, stif_warps, non_qss_species, 0/*phase*/, 1/*pass*/);
    emit_pipe_reaction_code(ost, stif_warps, non_qss_species, 1/*phase*/, 1/*pass*/);
    emit_pipe_reaction_code(ost, last_warps, non_qss_species, 0/*phase*/, 2/*pass*/);
    emit_pipe_reaction_code(ost, last_warps, non_qss_species, 1/*phase*/, 2/*pass*/);
    // Emit the updates to the arrays
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
  }
  // Now emit the actual getrates code
  {
    char source_name[128];
    snprintf(source_name,127,"%s_gpu.cu",out_name);
    CodeOutStream ost(source_name,true/*bound line lengths*/, 80);
    emit_pipe_getrates_code(ost,non_qss_species,temp_file_name);
    if (emit_experiment)
      emit_gpu_experiment(ost, non_qss_species, "gpu_getrates", true/*striding z*/);
  }
#endif

  // Emit the baseline version of the code too
  {
    char baseline_name[128];
    snprintf(baseline_name,127,"%s_basegpu.cu",out_name);
    CodeOutStream ost(baseline_name,true/*bound line lengths*/, 80);
    emit_baseline_cuda(ost,non_qss_species);
    if (emit_experiment)
      emit_gpu_experiment(ost, non_qss_species, "getrates_baseline_gpu", false/*striding z*/);
  }
}

#if 0
void ChemistryUnit::emit_cuda(const char *out_name, bool emit_experiment)
{
  // Figure out how many micro-warps there are in each warp
  const int micro_warps = 32/POINTS_PER_CTA;
  // Create warps
  for (unsigned idx = 0; idx < (micro_warps * unit->threads_per_point); idx++)
  {
    warps.push_back(new Warp(idx));
  }
  // Assign the reactions to each of the warps
  int normal_reaction_cnt = 0;
  int weird_reaction_cnt = 0;
  {
    std::vector<Reaction*> weird_reactions;
    std::vector<Reaction*> normal_reactions;
    for (std::vector<Reaction*>::const_iterator it = unit->all_reactions.begin();
          it != unit->all_reactions.end(); it++)
    {
      if (is_weird_reaction(*it))
      {
        weird_reactions.push_back(*it);
        weird_reaction_cnt++;
      }
      else
      {
        normal_reactions.push_back(*it);
        normal_reaction_cnt++;
      }
    }
    // For each of the weird reactions add them to all of the micro-warps
    // within a warp so that we don't have any divergence
    unsigned next_warp_id = 0;
    for (std::vector<Reaction*>::const_iterator it = weird_reactions.begin();
          it != weird_reactions.end(); it++)
    {
      for (int i = 0; i < micro_warps; i++)
      {
        warps[next_warp_id]->add_reaction((*it)->get_global_idx());
        warps[next_warp_id]->set_reaction((*it)->get_global_idx(),*it);
        next_warp_id++;
        if (next_warp_id == warps.size())
          next_warp_id = 0;
      }
    }
    // Now stripe the normal reactions across the rest of the warps like normal
    for (std::vector<Reaction*>::const_iterator it = normal_reactions.begin();
          it != normal_reactions.end(); it++)
    {
      warps[next_warp_id]->add_reaction((*it)->get_global_idx());
      warps[next_warp_id]->set_reaction((*it)->get_global_idx(),*it);
      next_warp_id++;
      if (next_warp_id == warps.size())
        next_warp_id = 0;
    }
  }
  // Subtract out the QSS species which we won't have to do anything for
  const int total_species = (unit->species.size()-1);
  int non_qss_species = 0;
  // Go through and mark all the QSS species
  for (SpeciesSet::const_iterator it = unit->species.begin();
        it != unit->species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    assert((1 <= int((*it)->idx)) && (int((*it)->idx) <= total_species));
    // Otherwise see if it is a QSS species
    bool qss_spec = false;
    for (std::vector<ConnectedComponent*>::const_iterator cc = connected_components.begin();
          cc != connected_components.end(); cc++)
    {
      for (std::map<int,QSS*>::const_iterator sp = (*cc)->species.begin();
            sp != (*cc)->species.end(); sp++)
      {
        assert(sp->first == sp->second->qss_idx);
        if (int((*it)->idx) == sp->second->species_idx)
        {
          (*it)->qss_species = true;
          qss_spec = true;
          break;
        }
      }
      if (qss_spec)
        break;
    }
    if (!qss_spec)
      non_qss_species++;
    // Also check to see if it is a stif species
    for (std::vector<Stif*>::const_iterator st = stif_operations.begin();
          st != stif_operations.end(); st++)
    {
      if ((*st)->k == int((*it)->idx))
      {
        (*st)->species = *it;
        break;
      }
    }
  }
  fprintf(stdout,"  INFO: Non-QSSA species: %d\n", non_qss_species);
  fprintf(stdout,"  INFO: QSSA species: %d\n",total_species-non_qss_species);
  fprintf(stdout,"  INFO: Weird Reactions: %d\n", weird_reaction_cnt);
  fprintf(stdout,"  INFO: Normal Reactions: %d\n", normal_reaction_cnt);

  // Now we can do the code generation
  const char *temp_file_name = "__temp__getrates__.cc";
  const char *fun_file_name = "__func__file__.cc";
  const int phases = 2;
  {
    CodeOutStream ost(temp_file_name, true/*bound line lengths*/, 80);   
    CodeOutStream fun_ost(fun_file_name, true/*bound line lengths*/, 80);
    // Tell all the loading warps that they can start all their operations
    for (int idx = 0; idx < phases; idx++)
    {
      const int start_spec = (idx*4) + 0 + 2;
      const int start_diff = (idx*4) + 2 + 2;
      ost << "asm volatile(\"bar.arrive " << start_spec << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      ost << "asm volatile(\"bar.arrive " << start_diff << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
    }
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim step_loop_pair(ost);
    for (int idx = 0; idx < phases; idx++)
      emit_bulk_experiment_code(ost,fun_ost,non_qss_species,micro_warps,idx/*offset*/,phases);
    // Emit the updates to the arrays
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << WDOT << " += slice_stride;\n";
  }
  // Now emit the actual getrates code
  {
    char source_name[128];
    snprintf(source_name,127,"%s_gpu.cu",out_name);
    CodeOutStream ost(source_name,true/*bound line lengths*/, 80);
    emit_bulk_getrates_code(ost,non_qss_species,temp_file_name,fun_file_name,micro_warps,phases);
    if (emit_experiment)
      emit_gpu_experiment(ost, non_qss_species, "gpu_getrates", true/*striding z*/);
  }

  // Emit the baseline version of the code too
  {
    char baseline_name[128];
    snprintf(baseline_name,127,"%s_basegpu.cu",out_name);
    CodeOutStream ost(baseline_name,true/*bound line lengths*/, 80);
    emit_baseline_cuda(ost,non_qss_species);
    if (emit_experiment)
      emit_gpu_experiment(ost, non_qss_species, "getrates_baseline_gpu", false/*striding z*/);
  }
}
#endif

// Old version of emit cuda
#if 0
void ChemistryUnit::emit_cuda(const char *out_name, bool emit_experiment)
{
  // Compute the amount of shared memory we can have in our shared stores
  const int total_species = (unit->species.size()-1);
  const int shared_per_point = MAX_SHARED/POINTS;
  const int shared_values_per_point = shared_per_point/sizeof(double);
  // Subtract out the QSS species which we won't have to do anything for
  int non_qss_species = 0;
  // Go through and mark all the QSS species
  for (SpeciesSet::const_iterator it = unit->species.begin();
        it != unit->species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    assert((1 <= int((*it)->idx)) && (int((*it)->idx) <= total_species));
    // Otherwise see if it is a QSS species
    bool qss_spec = false;
    for (std::vector<ConnectedComponent*>::const_iterator cc = connected_components.begin();
          cc != connected_components.end(); cc++)
    {
      for (std::map<int,QSS*>::const_iterator sp = (*cc)->species.begin();
            sp != (*cc)->species.end(); sp++)
      {
        assert(sp->first == sp->second->qss_idx);
        if (int((*it)->idx) == sp->second->species_idx)
        {
          (*it)->qss_species = true;
          qss_spec = true;
          break;
        }
      }
      if (qss_spec)
        break;
    }
    if (!qss_spec)
      non_qss_species++;
    // Also check to see if it is a stif species
    for (std::vector<Stif*>::const_iterator st = stif_operations.begin();
          st != stif_operations.end(); st++)
    {
      if ((*st)->k == int((*it)->idx))
      {
        (*st)->species = *it;
        break;
      }
    }
  }
  int normal_reactions = 0;
  int weird_reactions = 0;
  for (std::vector<Reaction*>::const_iterator it = unit->all_reactions.begin();
        it != unit->all_reactions.end(); it++)
  {
    if (is_weird_reaction(*it))
      weird_reactions++;
    else
      normal_reactions++;
  }
  fprintf(stdout,"  INFO: Non-QSSA species: %d\n", non_qss_species);
  fprintf(stdout,"  INFO: QSSA species: %d\n",total_species-non_qss_species);
  fprintf(stdout,"  INFO: Weird Reactions: %d\n", weird_reactions);
  fprintf(stdout,"  INFO: Normal Reactions: %d\n", normal_reactions);
  // First divide our number of values that we have by the target number of CTAs then
  // Subtract out the number of species molar mass spots we need
  // If we have any third body species, then subtract one for storing the total
  const int available_values = ((shared_values_per_point/TARGET_NUM_CTAS) - non_qss_species) 
                                  - (unit->third_bodies.empty() ? 0 : 1);
  // Now figure out the target number of values in each stage based on the number of named barriers
  // that we have.  Subtract out the 0 named barrier and then divide by two times the number
  // of CTAs to figure out how many pairs we have in each CTA
  const int named_barrier_pairs = (16-TARGET_NUM_CTAS)/(2*TARGET_NUM_CTAS);
  const int max_shared_values = available_values * named_barrier_pairs - (TUNING_PARAM*TARGET_NUM_CTAS)/*give us some degrees of freedom*/;
  for (unsigned i = 0; i < unit->threads_per_point; i++)
  {
    warps.push_back(new Warp(i));
  }

  // For the second pass figure out how many warps are in each stage for the second pass
  const int warps_per_stage = (unit->threads_per_point+(named_barrier_pairs-1))/named_barrier_pairs;
  // Figure our how many reactions can be done in each stage of the second pass
  const int reaction_shared = available_values - (stif_operations.size()+SPILL_RATES); 
  const int reactions_per_warp = reaction_shared/warps_per_stage;
  const int second_pass_offset = stif_operations.size() + SPILL_RATES;
  assert(reactions_per_warp > 0);

  fprintf(stdout,"  INFO: Generation of QSSA+STIF getrates for CUDA\n");
  fprintf(stdout,"    Number of points per CTA: %d\n", POINTS);
  fprintf(stdout,"    Available shared memory values per point per CTA: %d\n",available_values);
  fprintf(stdout,"    Named barrier pairs per CTA: %d\n", named_barrier_pairs);
  fprintf(stdout,"    Maximum shared values per point per CTA: %d\n", max_shared_values);
  fprintf(stdout,"    Second pass warps per stage: %d\n", warps_per_stage);
  fprintf(stdout,"    Second pass reactions per warp: %d\n", reactions_per_warp);
  fprintf(stdout,"    Second pass shared spill rates: %d\n", SPILL_RATES);

  phases.push_back(new Phase(0,max_shared_values));
  Phase *current_phase = phases.back();
  // For each of the connected components, see if we can add it to the
  // current phase, if not start the next phase
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    if (!current_phase->add_connected_component(*it, warps, reaction_granularity))
    {
      int next_phase = phases.size();
      current_phase->finalize(warps);
      phases.push_back(new Phase(next_phase,max_shared_values));
      current_phase = phases.back();
      // Better not fails, if it does then we have a connected component
      // which is too big to move all its data at once through shared memory
      // and we'll have to figure out how to fission the connected component
      assert(current_phase->add_connected_component(*it, warps, reaction_granularity));
    }
  }
  size_t num_qss_phases = phases.size();
  fprintf(stdout,"    Total QSSA phases: %ld\n",num_qss_phases);
  for (std::vector<Phase*>::const_iterator it = phases.begin();
        it != phases.end(); it++)
  {
    fprintf(stdout,"    Phase %d uses %ld value locations and has %ld operations\n",
        (*it)->phase_id,(*it)->store->allocations.size(),(*it)->components.size()+(*it)->stifs.size());
#if 0
    for (std::vector<Warp*>::const_iterator wit = warps.begin();
          wit != warps.end(); wit++)
    {
      unsigned total_warp_allocations = 0; 
      for (std::vector<Allocation*>::const_iterator ait = (*it)->store->allocations.begin();
            ait != (*it)->store->allocations.end(); ait++)
      {
        if ((*ait)->warp_id == (*wit)->wid)
          total_warp_allocations++;
      }
      fprintf(stdout,"      Warp %d has %d allocations\n", (*wit)->wid, total_warp_allocations);
    }
#endif
  }
  fprintf(stdout,"  INFO: Warp information for QSSA species generation\n");
  for (std::vector<Warp*>::const_iterator it = warps.begin();
        it != warps.end(); it++)
  {
    fprintf(stdout,"    Warp %d manages %ld reactions\n",(*it)->wid,(*it)->num_reactions());
  }
#if 1
  // Start a new phase for the Stiff components
  {
    int next_phase = phases.size();
    current_phase->finalize(warps);
    phases.push_back(new Phase(next_phase,max_shared_values));
    current_phase = phases.back();
  }
  // Now when adding the stiff species, find the largest one that will fit in
  // the current phase, then add it.  If none of the remaining ones fit then we 
  // start a new phase.
  std::set<Stif*> stif_ops(stif_operations.begin(),stif_operations.end());
  while (!stif_ops.empty())
  {
    Stif *largest_op = NULL;
    int largest_usage = -1;
    for (std::set<Stif*>::const_iterator it = stif_ops.begin();
          it != stif_ops.end(); it++)
    {
      int usage = current_phase->can_add_stif(*it, warps, reaction_granularity);
      if (usage > largest_usage)
      {
        largest_op = *it;
        largest_usage = usage;
      }
    }
    if (largest_op != NULL)
    {
      current_phase->add_stif(largest_op, warps, reaction_granularity);
      stif_ops.erase(largest_op);
    }
    else
    {
      // The phase better not be empty or there are things we can't add which means
      // we would have to fission them, which is not something we handle right now.
      assert(!current_phase->empty());
      int next_phase = phases.size();
      current_phase->finalize(warps);
      phases.push_back(new Phase(next_phase,max_shared_values));
      current_phase = phases.back();
    }
  }
  current_phase->finalize(warps);
  // Finally go through and get allocate the reactions that aren't involved in any of the 
  // QSSA or stiffness operations
  for (ReactionVec::const_iterator it = unit->all_reactions.begin();
        it != unit->all_reactions.end(); it++)
  {
    unsigned reac_idx = (*it)->get_global_idx();
    bool added = false;
    // Go through all of the warps and find the one we want
    for (std::vector<Warp*>::const_iterator wit = warps.begin();
          wit != warps.end(); wit++)
    {
      if ((*wit)->has_reaction(reac_idx))
      {
        (*wit)->set_reaction(reac_idx,*it);
        added = true;
        break;
      }
    }
    // If we didn't add it anywhere, then pick a warp to add it to
    if (!added)
    {
      int target_warp = select_target_warp(warps);
      warps[target_warp]->add_reaction(reac_idx);
      warps[target_warp]->set_reaction(reac_idx,*it);
    }
  }
  // Sanity check that the warps have a reaction object for every reaction
  // they've been assigned
  for (std::vector<Warp*>::const_iterator it = warps.begin();
        it != warps.end(); it++)
  {
    assert((*it)->reaction_indexes.size() == (*it)->reaction_order.size());
    assert((*it)->reaction_indexes.size() == (*it)->reactions.size());
  }
  // Now sort the warps so that they are in order from the warps with the most reactions
  // to the ones with the fewest reactions
  {
    std::list<Warp*> sorted_warps;
    for (std::vector<Warp*>::const_iterator it = warps.begin();
          it != warps.end(); it++)
    {
      bool added = false;
      for (std::list<Warp*>::iterator inserter = sorted_warps.begin();
            inserter != sorted_warps.end(); inserter++)
      {
        if ((*it)->num_reactions() >= (*inserter)->num_reactions())
        {
          sorted_warps.insert(inserter,*it);
          added = true;
          break;
        }
      }
      if (!added)
        sorted_warps.push_back(*it);
    }
    warps.clear();
    for (std::list<Warp*>::const_iterator it = sorted_warps.begin();
          it != sorted_warps.end(); it++)
    {
      warps.push_back(*it);
    }
  }
#endif

  // Now have each phase group it's transfers into stages and then resort the allocations
  // so that they are named with the right shared memory locations.  Here is where we find
  // out if everything really fits or not
  for (std::vector<Phase*>::const_iterator it = phases.begin(); 
        it != phases.end(); it++)
  {
    for (std::vector<Warp*>::const_iterator wit = warps.begin();
          wit != warps.end(); wit++)
    {
      unsigned total_warp_allocations = 0; 
      for (std::vector<Allocation*>::const_iterator ait = (*it)->store->allocations.begin();
            ait != (*it)->store->allocations.end(); ait++)
      {
        if ((*ait)->warp_id == (*wit)->wid)
          total_warp_allocations++;
      }
      fprintf(stdout,"      Warp %d has %d allocations\n", (*wit)->wid, total_warp_allocations);
    }
    (*it)->sort_into_stages(named_barrier_pairs, available_values, warps);
  }
  // Now that we've broken each phase into stages, also go through and sort the reactions
  // in each warp into phases so all the weird reactions are at the front to minimize
  // the divergence
  {
    unsigned phase_start = 0;
    for (std::vector<Phase*>::const_iterator it = phases.begin();
          it != phases.end(); it++)
    {
      unsigned phase_stop = (*it)->max_reaction;
      for (std::vector<Warp*>::const_iterator wit = warps.begin();
            wit != warps.end(); wit++)
      {
        (*wit)->sort_weird_reactions(phase_start, phase_stop);
      }
      phase_start = phase_stop + 1;
    }
  }

  fprintf(stdout,"    Total STIF phases: %ld\n",phases.size()-num_qss_phases);
  for (std::vector<Phase*>::const_iterator it = phases.begin();
        it != phases.end(); it++)
  {
    fprintf(stdout,"    Phase %d uses %ld value locations and has %ld operations\n",
        (*it)->phase_id,(*it)->store->allocations.size(),(*it)->components.size()+(*it)->stifs.size());
  }
  fprintf(stdout,"  INFO: Warp information for QSSA+STIF species generation\n");
#if 1
  for (std::vector<Warp*>::const_iterator it = warps.begin();
        it != warps.end(); it++)
  {
    std::set<Species*> unique_species;
    std::set<Species*> stif_species;
    for (std::map<int,Reaction*>::const_iterator reac_it = (*it)->reactions.begin();
          reac_it != (*it)->reactions.end(); reac_it++)
    {
      Reaction *reac = reac_it->second;
      unique_species.insert(reac->all_species.begin(),reac->all_species.end());
      for (std::vector<Stif*>::const_iterator stif_it = stif_operations.begin();
            stif_it != stif_operations.end(); stif_it++)
      {
        if ((*stif_it)->forward_d.find(reac_it->first) != (*stif_it)->forward_d.end())
          stif_species.insert((*stif_it)->species);
        else if ((*stif_it)->backward_d.find(reac_it->first) != (*stif_it)->backward_d.end())
          stif_species.insert((*stif_it)->species);
      }
    }
    std::vector<int> needed_xqs;
    (*it)->compute_needed_xqs(connected_components,needed_xqs);
    fprintf(stdout,"    Warp %d manages %ld reactions with %ld unique species and %ld XQ species and %ld stiff species\n",
        (*it)->wid,(*it)->num_reactions(),unique_species.size(), needed_xqs.size(), stif_species.size());
  }
#endif
#if 0
  for (std::set<Species*>::const_iterator it = unit->species.begin();
        it != unit->species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    fprintf(stdout,"  Species %s has contributions from %ld reactions\n", (*it)->name, (*it)->reaction_contributions.size()); 
  }
#endif
#if 0
  {
    // Compute the number of live species and dead species at each point in the reduction warp
    const unsigned max_warp_reactions = warps.front()->reactions.size();
    std::set<Species*> live_species;
    std::set<Species*> dead_species;
    std::map<Species*,unsigned> reference_counts;
    // Fill in the reference counts
    for (std::vector<Reaction*>::const_iterator it = unit->all_reactions.begin();
          it != unit->all_reactions.end(); it++)
    {
      Reaction *reac = *it;
      for (std::map<Species*,int>::const_iterator spec_it = reac->stoich.begin();
            spec_it != reac->stoich.end(); spec_it++)
      {
        if (strcmp(spec_it->first->name,"M") == 0)
          continue;
        if (spec_it->first->qss_species)
          continue;
        if (reference_counts.find(spec_it->first) == reference_counts.end())
          reference_counts[spec_it->first] = 1;
        else
          reference_counts[spec_it->first]++;
      }
    }
    // Find any species which are updated by no reactions and put them in the dead list
    for (std::set<Species*>::const_iterator it = unit->species.begin();
          it != unit->species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      if ((*it)->reaction_contributions.empty())
        dead_species.insert(*it);
    }
    for (unsigned reac_idx = 0; reac_idx < max_warp_reactions; reac_idx++)
    {
      // Update the list of live and dead species
      for (std::vector<Warp*>::const_iterator wit = warps.begin();
            wit != warps.end(); wit++)
      {
        if (reac_idx >= (*wit)->reactions.size())
          continue;
        int reaction_idx = (*wit)->get_reaction_idx(reac_idx); 
        Reaction *reac = (*wit)->get_reaction(reaction_idx);
        assert(reac != NULL);
        for (std::map<Species*,int>::const_iterator it = reac->stoich.begin();
              it != reac->stoich.end(); it++)
        {
          if (strcmp(it->first->name,"M") == 0)
            continue;
          if (it->first->qss_species)
            continue;
          live_species.insert(it->first);
          std::map<Species*,unsigned>::iterator finder = reference_counts.find(it->first); 
          assert(finder != reference_counts.end());
          assert(finder->second > 0);
          finder->second--;
          if (finder->second == 0)
          {
            // Move this species from the live list into the dead list
            live_species.erase(it->first);
            dead_species.insert(it->first);
          }
        }
      }
      // count the number of available slots in the shared memory
      unsigned available_slots = 0;
      for (std::map<Species*,unsigned>::const_iterator it = reference_counts.begin();
            it != reference_counts.end(); it++)
      {
        if (it->second == 0)
          available_slots++;
      }
      // Print out the information for this stage
      fprintf(stdout,"    INFO: after reaction index %d: %ld live species   %ld dead species  %d available species\n",
              reac_idx, live_species.size(), dead_species.size(), available_slots);
    }
  }
#endif
  // Emit the baseline cuda implementation
  {
    char baseline_name[128];
    snprintf(baseline_name,127,"%s_basegpu.cu",out_name);
    CodeOutStream ost(baseline_name,true/*bound line lengths*/, 80);
    emit_baseline_cuda(ost,non_qss_species);
    if (emit_experiment)
      emit_gpu_experiment(ost, non_qss_species, "getrates_baseline_gpu", false/*striding z*/);
  }
  // Now we're going to emit the code.  Emit the code into a temporary file, then
  // come back later and emit the constants into the actual output file and copy
  // the temporary file into the final output file.

  // The old code for doing this
  const char *temp_file_name = "__temp__getrates__.cc";
  {
    CodeOutStream ost(temp_file_name, true/*bound line lengths*/, 80);   
#if 0
    emit_experiment_code(ost,non_qss_species,available_values);
#else
    emit_two_pass_experiment_code(ost,non_qss_species,available_values,warps_per_stage,reactions_per_warp,second_pass_offset);
#endif
  }
  // Now emit the actual getrates code
  {
    char source_name[128];
    snprintf(source_name,127,"%s_gpu.cu",out_name);
    CodeOutStream ost(source_name,true/*bound line lengths*/, 80);
#if 0
    emit_getrates_code(ost,non_qss_species,available_values,temp_file_name);
#else
    emit_two_pass_getrates_code(ost,non_qss_species,available_values,temp_file_name,warps_per_stage,reactions_per_warp,second_pass_offset);
#endif
    if (emit_experiment)
      emit_gpu_experiment(ost, non_qss_species, "gpu_getrates", true/*striding z*/);
  }
}
#endif

void ChemistryUnit::emit_baseline_cuda(CodeOutStream &ost, int num_species)
{
  {
    int idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M")==0)
        continue;
      if ((*it)->qss_species)
        continue;
      ost << "#define " << (*it)->code_name << "  " << idx << "\n";
      idx++;
    }
    assert(idx == num_species);
  }
  ost << "\n";
  ost << "__global__ void\n";
  ost << "__launch_bounds__(128,5)\n";
  ost << "getrates_baseline_gpu(";
  ost << "const " << REAL << " *" << PRESSURE_ARRAY;
  ost << ", const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
  ost << ", const " << REAL << " *" << DIFFUSION_ARRAY;
  ost << ", const " << REAL << " dt";
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << WDOT_ARRAY;
  ost << ")\n";
  PairDelim pair(ost); 
  ost << "\n";
  // Emit the load for the temperature
  ost << REAL << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,false/*double2*/,".cg");
  // Compute the offsets for each of the arrays
  {
    PairDelim offset_pair(ost); 
    ost << "const " << INT << " offset = (blockIdx.y*blockDim.y + threadIdx.y)*row_stride"
        << " + (blockIdx.x * blockDim.x + threadIdx.x) + blockIdx.z*slice_stride;\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << DIFFUSION_ARRAY << " += offset;\n";
    ost << WDOT_ARRAY << " += offset;\n";
  }
  unit->emit_numerical_constants(ost);
  ost << "\n";
  unit->emit_temperature_dependent(ost);
  ost << "\n";
  // Emit the code to compute the gibbs free energies
  ost << REAL << " cgspl[" << num_species << "];\n";
  ost << "// Gibbs free energies\n";
  {
    PairDelim gibbs_pair(ost);
    ost << "const " << REAL << " &tk1 = " << TEMPERATURE << ";\n";
    ost << "const " << REAL << " tk2 = tk1*tk1;\n";
    ost << "const " << REAL << " tk3 = tk1*tk2;\n";
    ost << "const " << REAL << " tk4 = tk1*tk3;\n";
    ost << "const " << REAL << " tk5 = tk1*tk4;\n";
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      ost << "// Species " << (*it)->name << "\n";
      ost << "if (tk1 > " << (*it)->get_common_temperature() << ")\n";
      {
        PairDelim if_pair(ost);
        ost << "cgspl[" << (*it)->code_name << "] = ";
        ost << (*it)->get_high_coefficient(0) << "*tk1*(";
        if (!unit->no_nondim)
          ost << (1-log(TEMPERATURE_REF));
        else
          ost << "1";
        ost << "-vlntemp) + ";
        ost << (*it)->get_high_coefficient(1) << "*tk2 + ";
        ost << (*it)->get_high_coefficient(2) << "*tk3 + ";
        ost << (*it)->get_high_coefficient(3) << "*tk4 + ";
        ost << (*it)->get_high_coefficient(4) << "*tk5 + ";
        ost << "(" << (*it)->get_high_coefficient(5) << " - ";
        ost << "tk1*" << (*it)->get_high_coefficient(6) << ");\n";
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        ost << "cgspl[" << (*it)->code_name << "] = ";
        ost << (*it)->get_low_coefficient(0) << "*tk1*(";
        if (!unit->no_nondim)
          ost << (1-log(TEMPERATURE_REF));
        else
          ost << "1";
        ost << "-vlntemp) + ";
        ost << (*it)->get_low_coefficient(1) << "*tk2 + ";
        ost << (*it)->get_low_coefficient(2) << "*tk3 + ";
        ost << (*it)->get_low_coefficient(3) << "*tk4 + ";
        ost << (*it)->get_low_coefficient(4) << "*tk5 + ";
        ost << "(" << (*it)->get_low_coefficient(5) << " - ";
        ost << "tk1*" << (*it)->get_low_coefficient(6) << ");\n";
      }
      spec_idx++;
    }
  }
  ost << "// Compute molar fractions\n";
  ost << REAL << " cspl[" << num_species << "];\n";
  // Emit the loads for all of the species and all the pressure
  ost << REAL << " " << PRESSURE << ";\n";
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if ((*it)->qss_species)
      continue;
    char src_offset[128];
    sprintf(src_offset,"%s*slice_stride",(*it)->code_name);
    emit_cuda_load(ost,"cspl",(*it)->code_name,MASS_FRAC_ARRAY,src_offset,false/*double2*/,".cg");
  }
  emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,false/*double2*/,".cg");
  {
    // For each of the species, compute the molar fraction
    PairDelim molar_pair(ost);
    ost << REAL << " sumyow = 0.0;\n";
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      ost << "sumyow += cspl[" << (*it)->code_name << "] * " << (1.0/(*it)->molecular_mass) << ";\n";
    }
    ost << "sumyow *= (";
    if (unit->no_nondim)
      ost << "8.314510e+07 * " << TEMPERATURE;
    else
      ost << (8.314510e+07 * TEMPERATURE_REF / PRESSURE_REF) << " * " << TEMPERATURE;
    ost << ");\n";
    ost << "sumyow = " << PRESSURE << "/sumyow;\n";
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      ost << "cspl[" << (*it)->code_name << "] = cspl[" << (*it)->code_name
          << "] * (sumyow * " << (1.0/(*it)->molecular_mass) << ");\n";
    }
  }
  // Compute the third body values
  ost << "// Third-body computation\n";
  ost << REAL << " thbctemp[" << unit->third_bodies.size() << "];\n";
  {
    PairDelim thb_pair(ost);
    ost << REAL << " ctot = 0.0;\n";
    ost << "for (int i = 0; i < " << num_species << "; i++)\n";
    {
      PairDelim loop_pair(ost);
      ost << "ctot += cspl[i];\n";
    }
    unsigned idx = 0;
    for (std::vector<ThirdBody*>::const_iterator it = unit->third_bodies.begin();
          it != unit->third_bodies.end(); it++,idx++)
    {
      ost << "thbctemp[" << idx << "] = ctot";
      for (std::map<Species*,double>::const_iterator comp_it = (*it)->components.begin();
            comp_it != (*it)->components.end(); comp_it++)
      {
        ost << " + " << comp_it->second << "*cspl[" << comp_it->first->code_name << "]";
      }
      ost << ";\n"; 
    }
  }
  ost << REAL << " forward_r[" << unit->all_reactions.size() << "];\n";
  ost << REAL << " reverse_r[" << unit->all_reactions.size() << "];\n";
  {
    unsigned idx = 0;
    for (std::vector<Reaction*>::const_iterator it = unit->all_reactions.begin();
          it != unit->all_reactions.end(); it++,idx++)
    {
      Reaction *reac = *it;
      if (reac->duplicate.enabled && !reac->duplicate.owner)
        continue;
      reac->emit_reaction_comment(ost);
      PairDelim reac_pair(ost);
      IntensityCounter cnt;
      reac->emit_forward_reaction_rate(ost, cnt, false/*vector*/);
      ost << "forward_r[" << idx << "] = rr_f";
      for (SpeciesCoeffMap::const_iterator co_it = reac->forward.begin();
            co_it != reac->forward.end(); co_it++)
      {
        if (co_it->first->qss_species)
          continue;
        assert(co_it->second > 0);
        for (int i = 0; i < co_it->second; i++)
          ost << "*cspl[" << co_it->first->code_name << "]";
      }
      if ((reac->thb != NULL) && !reac->pressure_dep)
      {
        ost << "*thbctemp[" << reac->thb->idx << "]";
      }
      ost << ";\n";
      if (reac->reversible)
      {
        reac->emit_reverse_reaction_rate(ost, cnt, true/*indexing*/);
        ost << "reverse_r[" << idx << "] = rr_r";
        for (SpeciesCoeffMap::const_iterator co_it = reac->backward.begin();
            co_it != reac->backward.end(); co_it++)
        {
          if (co_it->first->qss_species)
            continue;
          assert(co_it->second > 0);
          for (int i = 0; i < co_it->second; i++)
            ost << "*cspl[" << co_it->first->code_name << "]";
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          ost << "*thbctemp[" << reac->thb->idx << "]";
        }
        ost << ";\n";
      }
      else
        ost << "reverse_r[" << idx << "] = 0.0;\n";
    }
  }
  // Emit the fowrad and backward unimportant rates
  for (std::set<int>::const_iterator it = forward_unimportant.begin();
        it != forward_unimportant.end(); it++)
  {
    ost << "forward_r[" << ((*it)-1) << "] = 0.0;\n";
  }
  for (std::set<int>::const_iterator it = backward_unimportant.begin();
        it != backward_unimportant.end(); it++)
  {
    ost << "reverse_r[" << ((*it)-1) << "] = 0.0;\n";
  }
  // Now do the QSSA
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->emit_cpp_statements(ost); 
  }

  ost << REAL << " " << DIFFUSION << "[" << num_species << "];\n";
  // Then do the Stiffness
  for (std::vector<Stif*>::const_iterator it = stif_operations.begin();
        it != stif_operations.end(); it++)
  {
    // Emit the load for the diffusion element in case we need it
    char src_offset[128];
    sprintf(src_offset,"%s*slice_stride",(*it)->species->code_name);
    emit_cuda_load(ost,DIFFUSION,(*it)->species->code_name,DIFFUSION_ARRAY,src_offset,false/*double2*/,".cg");
    (*it)->emit_cpp_statements(ost, unit);
  }
  
  // compute the ropl values
  ost << REAL << " ropl[" << unit->all_reactions.size() << "];\n";
  ost << "for (int i = 0; i < " << unit->all_reactions.size() << "; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << "ropl[i] = forward_r[i] - reverse_r[i];\n";
  }

  ost << REAL << " " << WDOT << "[" << num_species << "];\n";
  // Finally compute the wdot values
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if ((*it)->qss_species)
      continue;
    IntensityCounter cnt;
    (*it)->emit_contributions(ost, cnt, false/*vector*/);
    // Emit the store
    ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << WDOT_ARRAY << "+" 
        << (*it)->code_name << "*slice_stride), "
        << "\"d\"(" << WDOT << "[" << (*it)->code_name << "]) : \"memory\");\n";
  }
}

void ChemistryUnit::emit_getrates_code(CodeOutStream &ost, unsigned num_species/*non-qssa-species*/, 
                                       unsigned scratch_values, const char *temp_file_name)
{
  // Emit the constant declarations
  unsigned constants_per_thread;
  unsigned warp_constant_stride;
  emit_constant_declarations(ost, num_species, constants_per_thread, warp_constant_stride);

  ost << "__global__ void\n";
  ost << "gpu_getrates(";
  ost << "const " << REAL << " *" << PRESSURE_ARRAY;
  ost << ", const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
  ost << ", const " << REAL << " *" << DIFFUSION_ARRAY;
  ost << ", const " << REAL << " recip_dt/*reciprocal of dt*/";
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << WDOT;
  ost << ")\n";
  PairDelim pair(ost);
  ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
  ost << "const " << INT << " wid = threadIdx.x >> 5;\n";
  // Compute the offset for this thread
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*row_stride) + "
        << "(blockIdx.x*" << POINTS << " + tid);\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << DIFFUSION_ARRAY << " += offset;\n";
    ost << WDOT << " += offset;\n";
  }
  // Decleare our shared memory arrays
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << num_species << "][" << POINTS << "];\n";
  ost << "volatile __shared__ " << REAL << " scratch[" << scratch_values << "][" << POINTS << "];\n";
  // check to see if need a third body value
  if (!unit->third_bodies.empty())
    ost << "volatile __shared__ " << REAL << " " << THIRD_BODY << "[" << POINTS << "];\n";
  // Now we can do the warp specialization
  ost << "if (__any(wid < " << unit->threads_per_point << "))\n";
  {
    PairDelim experiment_pair(ost);
    ost << REAL << " reaction_constants[" << constants_per_thread << "];\n";
    // emit the loads for the reaction constants
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char src_offset[128];
      sprintf(src_offset,"wid*%d+%d",warp_constant_stride,(idx*32));
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      emit_cuda_load(ost,"reaction_constants",dst_offset,"warp_constants",src_offset,false/*double2*/,".cg");
    }
    // Then copy over the code from the temporary file
    FILE *temp_file = fopen(temp_file_name,"r");
    assert(temp_file != NULL);
    char c = fgetc(temp_file);
    while (c != EOF)
    {
      ost << c;
      c = fgetc(temp_file);
    }
    assert(fclose(temp_file) == 0);
  }
  ost << "else\n";
  {
    PairDelim reduction_pair(ost);
    emit_reducer_code(ost,num_species,scratch_values);
  }
}

void ChemistryUnit::emit_experiment_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values)
{
  const int max_specs = (num_species+(unit->threads_per_point-1))/unit->threads_per_point;
  const bool uniform_specs = ((num_species%unit->threads_per_point) == 0);
  // Really don't want the compiler unrolling this loop since it would
  // be really bad for the instruction cache
  ost << "#pragma unroll 1\n";
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim step_loop_pair(ost);
  // First issue loads for our temperature and mass fractions
  ost << REAL << " " << TEMPERATURE << ";\n";
  ost << REAL << " " << MASS_FRAC << "[" << max_specs << "];\n";
  ost << "\n";
  // Cache at all levels since lots of people
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,false/*double2*/,".ca");  
  for (int idx = 0; idx < max_specs; idx++)
  {
    char src_offset[128];
    sprintf(src_offset,"(wid+%d)*spec_stride",idx);
    char dst_offset[128];
    sprintf(dst_offset,"%d",idx);
    if (!uniform_specs && (idx == (max_specs-1)))
      ost << "if (__any((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << "))\n";
    emit_cuda_load(ost,MASS_FRAC,dst_offset,MASS_FRAC_ARRAY,src_offset,false/*double2*/,".cg");
  }
  ost << "\n";
  // Now we can compute a whole bunch of things that we'll need later 
  // for doing the reaction computations
  unit->emit_numerical_constants(ost); 
  ost << "\n";
  unit->emit_temperature_dependent(ost);
  ost << "\n";
  // Do a barrier here to make sure any stragglers from the previous loop have
  // finished reading from shared memory
  ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  // Now we compute our updated mass fractions and write them into shared memory
  // along with our partial value of sumyow
  {
    PairDelim mass_frac_pair(ost);
    ost << REAL << " sumyow = 0.0;\n";
    for (int idx = 0; idx < max_specs; idx++)
    {
      if (!uniform_specs && (idx == (max_specs-1)))
        ost << "if (__any((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << "))\n";
      PairDelim spec_pair(ost);   
      ost << REAL << " temp = " << MASS_FRAC << "[" << idx << "] * recip_molecular_mass[wid+" << (idx*unit->threads_per_point) << "];\n";
      ost << "sumyow = __fma_rn(temp," << TEMPERATURE << ",sumyow);\n";
      // Write the value into mole frac for the reducer to find
      ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid] = " << MASS_FRAC << "[" << idx << "];\n";
    }
    // This better be true or we are in a world of trouble
    assert(unit->threads_per_point <= scratch_values);
    // Write our partial sumyow into scratch space
    ost << "scratch[wid][tid] = sumyow;\n";
  }
  ost << "\n";
  // Now tell the reducer warp that we've written our values into shared
  ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  ost << "\n";
  // Now we can start doing the experiment phases
  // Figure out the maximum number of reactions in any warp
  int max_reactions = -1;
  for (std::vector<Warp*>::const_iterator it = warps.begin();
        it != warps.end(); it++)
  {
    if (int((*it)->num_reactions()) > max_reactions)
      max_reactions = (*it)->num_reactions();
  }
  assert(max_reactions > 0);
  ost << REAL << " rr_f[" << max_reactions << "];\n";
  ost << REAL << " rr_r[" << max_reactions << "];\n";
  // For each phase, compute the number of needed reactions, write the values into shared memory
  // and then mark that we are done
  {
    unsigned next_reaction = 0; 
    bool first = true;
    Phase *prev_phase = NULL;
    for (std::vector<Phase*>::const_iterator it = phases.begin();
          it != phases.end(); it++)
    {
      emit_phase_reactions(ost, next_reaction,(*it)->max_reaction,first,*it,prev_phase);
      next_reaction = (*it)->max_reaction+1;
      first = false;
      prev_phase = *it;
    }
    // If there were any leftovers do them as well
    // and handle the resulting returns of the last phase
    assert(int(next_reaction) <= (max_reactions+1));
    assert(!first);
    emit_phase_reactions(ost, next_reaction,(max_reactions-1),false,NULL,prev_phase);
  }
  // Now we've finished doing all the reactions, barrier to make sure
  // all the other reaction warps have finished reading from shared memory
  ost << "asm volatile(\"bar.sync 3," << (unit->threads_per_point*32) << ";\" : : : \"memory\");\n";
  {
    // Figure out how many values we can send in each phase
    unsigned reactions_per_phase = scratch_values/unit->threads_per_point;
    unsigned num_phases = (warps.front()->reactions.size()+reactions_per_phase-1)/reactions_per_phase;
    for (unsigned idx = 0; idx < num_phases; idx++)
    {
      // Write out our reaction values
      for (unsigned reac_idx = 0; reac_idx < reactions_per_phase; reac_idx++)
      {
        unsigned offset = idx*reactions_per_phase+reac_idx;
        // See if we need a check on this write
        int check_id = -1;
        unsigned warp_idx = 0;
        for (std::vector<Warp*>::const_iterator it = warps.begin();
              it != warps.end(); it++,warp_idx++)
        {
          if (offset >= (*it)->reactions.size())
          {
            check_id = warp_idx;
            break;
          }
        }
        if (check_id > 0)
        {
          ost << "if (__any(wid < " << check_id << "))\n";
        }
        // in the case where it doesn't apply to any of them, then skip it
        else if (check_id == 0) // && (offset > (*(warps.begin()))->reactions.size())) 
          continue;
        ost << "scratch[wid+" << (reac_idx*unit->threads_per_point) << "][tid] = "
            << "rr_f[" << (idx*reactions_per_phase+reac_idx) << "] - "
            << "rr_r[" << (idx*reactions_per_phase+reac_idx) << "];\n";
      }
      // Signal that we are done
      ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      if (idx < (num_phases-1))
        ost << "asm volatile(\"bar.sync 2," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
    }
  }
  // finally update the pointers that we need
  {
    PairDelim update_pair(ost);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
  }
}

void ChemistryUnit::emit_reducer_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values)
{
  // Really don't want the compiler unrolling this loop
  // since it would be really bad for the instruction cache
  ost << "#pragma unroll 1\n";
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim step_loop_pair(ost);
  // Issue the load for the pressure that we're going to need
  ost << REAL << " " << PRESSURE << ";\n";
  emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,false/*double2*/,".cg");
  // Do a barrier here to say that we've made it to this point in case we
  // were slow from the previous loop
  ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  // Now wait for the experiment warps to say they've written their values into shared
  ost << "\n";
  ost << "asm volatile(\"bar.sync 1," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  ost << "\n";
  // Do the sumyow reduction and then compute all the molar fractions
  {
    PairDelim sumyow_pair(ost);
    ost << REAL << " sumyow = scratch[0][tid];\n";
    for (unsigned idx = 1; idx < unit->threads_per_point; idx++)
      ost << "sumyow += scratch[" << idx << "][tid];\n";
    // Scale it and then invert it
    ost << "sumyow *= (";
    if (unit->no_nondim)
      ost << "8.314510e+07";
    else
      ost << (8.314510e+07 * TEMPERATURE_REF / PRESSURE_REF);
    ost << ");\n";
    {
      PairDelim spec_pair(ost);
      // Pipeline all the shared memory loads
      ost << REAL << " " << MASS_FRAC << "[" << num_species << "];\n";
      for (unsigned idx = 0; idx < num_species; idx++)
        ost << MASS_FRAC << "[" << idx << "] = " << MOLE_FRAC << "[" << idx << "][tid];\n";
      if (!unit->third_bodies.empty())
      {
        ost << REAL << " ctot = 0.0;\n";
        ost << REAL << " frac;\n";
        for (unsigned idx = 0; idx < num_species; idx++)
        {
          ost << "frac = " << PRESSURE << " * " << MASS_FRAC << "[" << idx << "]"
              << " * sumyow * recip_molecular_mass[" << idx << "];\n";
          ost << MOLE_FRAC << "[" << idx << "][tid] = frac;\n";
          ost << "ctot += frac;\n";
        }
        // Now write the total out to shared memory for use as the third body value
        ost << THIRD_BODY << "[tid] = ctot;\n";
      }
      else
      {
        // Now do all the math and write out the results
        for (unsigned idx = 0; idx < num_species; idx++)
        {
          ost << MOLE_FRAC << "[" << idx << "][tid] = " << PRESSURE << " * " << MASS_FRAC << "[" << idx << "]"
              << " * sumyow * recip_molecular_mass[" << idx << "];\n";
        }
      }
    }
  }
  ost << "\n";
  // Now mark that we've written the mole fractions into shared memory
  ost << "asm volatile(\"bar.arrive 2," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  ost << "\n";
  // Now go through and emit code for handling the reductions for each of the different phases
  for (std::vector<Phase*>::const_iterator it = phases.begin();
        it != phases.end(); it++)
  {
    // First mark that we need to wait until the data is ready
    ost << "asm volatile(\"bar.sync 1," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
    (*it)->emit_operations(ost,num_species);
    // Then mark that we've completed the operation
    ost << "asm volatile(\"bar.arrive 2," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  }
  // Now we ge get to handle the outputs for each of the species
  {
    ost << REAL << " spec_wdot[" << num_species << "];\n";
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < " << num_species << "; i++)\n";
    ost << "  spec_wdot[i] = 0.0;\n";
    unsigned reactions_per_phase = scratch_values/unit->threads_per_point;
    unsigned num_phases = (warps.front()->reactions.size()+reactions_per_phase-1)/reactions_per_phase;
    // Keep track of how many updates have been applied to each species, once the species
    // is done then we can write out its wdot
    std::map<Species*,unsigned> num_updates;
    unsigned performed_writes = 0;
    // See if there are any species which don't get touched by any reactions, if so we can emit their write
    // right now
    for (SpeciesSet::const_iterator it = unit->species.begin();
          it != unit->species.end(); it++)
    {
      if ((*it)->reaction_contributions.empty() && (strcmp((*it)->name,"M")!=0))
      {
        // Do some fancy math here to prevent the compiler from hoisting integer math and eating registers
        ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << WDOT << "+((" 
            << (*it)->code_name << "+step*" << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2)
            << ")*slice_stride), \"d\"(0.0) : \"memory\");\n";
        performed_writes++;
      }
    }
    unsigned handled_reactions = 0;
    for (unsigned idx = 0; idx < num_phases; idx++)
    {
      // Wait until the values are ready
      ost << "asm volatile(\"bar.sync 1," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      for (unsigned reac_idx = 0; reac_idx < reactions_per_phase; reac_idx++)
      {
        unsigned warp_idx = 0;
        for (std::vector<Warp*>::const_iterator it = warps.begin();
              it != warps.end(); it++,warp_idx++)
        {
          unsigned offset = idx*reactions_per_phase+reac_idx;
          // Check to see if there is even a reaction here
          if (offset >= (*it)->reactions.size())
            continue;
          handled_reactions++;
          // Get the reaction that we are performing
          int reaction_idx = (*it)->get_reaction_idx(offset);
          assert(reaction_idx != -1);
          Reaction *reaction = (*it)->get_reaction(reaction_idx);
          assert(reaction != NULL);
          PairDelim reac_pair(ost); 
          // Check to see if we have any species to update
          // that aren't QSS species
          bool has_non_qss_species = false;
          for (SpeciesCoeffMap::const_iterator co_it = reaction->stoich.begin();
                co_it != reaction->stoich.end(); co_it++)
          {
            if (!co_it->first->qss_species)
            {
              has_non_qss_species = true;
              break;
            }
          }
          if (has_non_qss_species)
            ost << REAL << " rate = scratch[" << (reac_idx*unit->threads_per_point+warp_idx) << "][tid];\n";
          else
            ost << "// All species are QSS species\n";
          // Go through each species in the stoichiometry and apply the updates
          for (SpeciesCoeffMap::const_iterator co_it = reaction->stoich.begin();
                co_it != reaction->stoich.end(); co_it++)
          {
            Species *spec = co_it->first;
            // Skip any of the QSS species since we already handled them
            if (spec->qss_species)
              continue;
            // Also skip any third body species
            if (strcmp(spec->name,"M")==0)
              continue;
            ost << "spec_wdot[" << spec->code_name << "] = __fma_rn(" << double(co_it->second) << ",rate,spec_wdot[" << spec->code_name << "]);\n";
            // Update the species updates
            if (num_updates.find(spec) == num_updates.end())
              num_updates[spec] = 0;
            num_updates[spec]++;
            // check to see if we've done all the updates for the species
            if (num_updates[spec] == spec->reaction_contributions.size())
            {
              // This species is done, multiply by the molecular mass and do the write
              // Do some fancy integer math here that reduces to shifts to prevent the compiler from hoisting integer
              // math and eating registers
              ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << WDOT << "+((" 
                  << spec->code_name << "+step*" << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2)
                  << ")*slice_stride), "
                  << "\"d\"(molecular_mass[" << spec->code_name << "]*spec_wdot[" << spec->code_name << "]) : \"memory\");\n";
              performed_writes++;
              num_updates.erase(spec);
            }
          }
        }
      }
      // For every phase except the last one, signal that we are done
      if (idx < (num_phases-1))
        ost << "asm volatile(\"bar.arrive 2," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
    }
    assert(handled_reactions == unit->all_reactions.size());
    if (!num_updates.empty())
    {
      for (std::map<Species*,unsigned>::const_iterator it = num_updates.begin();
            it != num_updates.end(); it++)
      {
        fprintf(stderr,"Species %s has %d reaction updates, but should have %ld\n", 
            it->first->name, it->second, it->first->reaction_contributions.size());
      }
    }
    assert(performed_writes == num_species);
  }
  // Finally update the pointers that we need here
  {
    PairDelim update_pair(ost);
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << DIFFUSION_ARRAY << " += slice_stride;\n";
    ost << WDOT << " += slice_stride;\n";
  }
}

void ChemistryUnit::emit_phase_reactions(CodeOutStream &ost, unsigned start_reac, unsigned term_reac, bool first, 
                                         Phase *current_phase, Phase *prev_phase)
{
  // First emit the reactions for this phase
  for (unsigned reac_idx = start_reac; reac_idx <= term_reac; reac_idx++)
  {
    emit_phase_reaction(ost, reac_idx, false/*second pass*/, 0/*offset*/);
  }
  if (first)
  {
    assert(prev_phase == NULL);
    // We need a barrier in the first case here since we can't be sure that the mole fractions have
    // been written yet
    ost << "asm volatile(\"bar.sync 2," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  }
  // Now we can update reactions with the necessary mole fractions
  {
    unsigned warp_idx = 0;
    for (std::vector<Warp*>::const_iterator it = warps.begin();
          it != warps.end(); it++,warp_idx++)
    {
      if (warp_idx == 0)    
        ost << "if (__any(wid == 0))\n";
      else
        ost << "else if (__any(wid == " << warp_idx << "))\n";
      PairDelim warp_pair(ost);
            // Finally emit zeros for any reactions which are unimportant
      // or do not have backward reaction rates
      for (unsigned reac_idx = start_reac; reac_idx <= term_reac; reac_idx++)
      {
        int reaction_idx = (*it)->get_reaction_idx(reac_idx);
        Reaction *reac = (*it)->get_reaction(reaction_idx);
        if (reac != NULL)
        {
#if RR_INDEXING
          if (forward_unimportant.find(reaction_idx) != forward_unimportant.end())
            ost << "rr_f[" << reac_idx << "] = 0.0;\n";
          if (!reac->reversible || (backward_unimportant.find(reaction_idx) != backward_unimportant.end()))
            ost << "rr_r[" << reac_idx << "] = 0.0;\n";
#else
          if (forward_unimportant.find(reaction_idx) != forward_unimportant.end())
            ost << "rr_f_" << reac_idx << " = 0.0;\n";
          if (!reac->reversible || (backward_unimportant.find(reaction_idx) != backward_unimportant.end()))
            ost << "rr_r_" << reac_idx << " = 0.0;\n";
#endif
        }
      }
      // If this is not the first pass then we have to pull out any values from
      // the previous pass before we can write out values into shared memory
      if (!first)
      {
        assert(prev_phase != NULL);
        ost << "asm volatile(\"bar.sync 2," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
        // Read out the values from the previous phase
        prev_phase->emit_read_scaling_factors(ost,*it); 
        // Then we need a barrier to make sure that everyone can re-use the shared scratch space
        ost << "asm volatile(\"bar.sync 3," << (unit->threads_per_point*32) << ";\" : : : \"memory\");\n";
      }
      // Now we write out our values into the scratch space
      if (current_phase != NULL)
      {
        current_phase->emit_write_store_values(ost,*it);
        // Mark that we've written our values into shared memory
        ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      }
      if (!first)
      {
        // Finally if this is not the first step, apply our scaling factors to our local values
        prev_phase->emit_apply_scaling_factors(ost,*it);
      }
    }
  }
}

void ChemistryUnit::emit_mole_fraction_updates(CodeOutStream &ost, Warp *warp, unsigned start_reac, unsigned term_reac, unsigned offset)
{
  // First compute the set of species that we need so we can load them
  // from shared and get good pipelined loads
  std::set<Species*> needed_mole_fracs;
  bool needs_third_body = false;
  std::map<int,bool> needs_scaling;
  for (unsigned reac_idx = start_reac; reac_idx <= term_reac; reac_idx++)
  {
    int reaction_idx = warp->get_reaction_idx(reac_idx);
    Reaction *reac = warp->get_reaction(reaction_idx);
    needs_scaling[reac_idx] = false;
    if (reac != NULL)
    {
      if (forward_unimportant.find(reaction_idx) == forward_unimportant.end())
      {
        for (SpeciesCoeffMap::const_iterator co_it = reac->forward.begin();
              co_it != reac->forward.end(); co_it++)
        {
          if (!co_it->first->qss_species)
          {
            needed_mole_fracs.insert(co_it->first);
            needs_scaling[reac_idx] = true;
          }
        }
      }
      else
        needs_scaling[reac_idx] = true; // needs scaling for assignment to zero
      if (reac->reversible && (backward_unimportant.find(reaction_idx) == backward_unimportant.end()))
      {
        for (SpeciesCoeffMap::const_iterator co_it = reac->backward.begin();
              co_it != reac->backward.end(); co_it++)
        {
          if (!co_it->first->qss_species)
          {
            needed_mole_fracs.insert(co_it->first);
            needs_scaling[reac_idx] = true;
          }
        }
      }
      else
        needs_scaling[reac_idx] = true; // needs scaling assignment to zero
      if ((reac->thb != NULL) && !reac->pressure_dep)
      {
        needs_third_body = true;
        // Get the set of mole fraction concentrations that we need for the third body as well
        for (std::map<Species*,double>::const_iterator co_it = reac->thb->components.begin();
              co_it != reac->thb->components.end(); co_it++)
        {
          if (!co_it->first->qss_species)
          {
            needed_mole_fracs.insert(co_it->first);
            needs_scaling[reac_idx] = true;
          }
        }
      }
    }
  }
#if 0
  // Also check to see if we need a third-body and not already handle by pressure falloff
  // Emit the loads for the needed species, then emit the code to do the scaling
  std::map<Species*,unsigned/*idx*/> local_mole_frac_index;
  if (!needed_mole_fracs.empty())
    ost << REAL << " local_frac[" << needed_mole_fracs.size() << "];\n";
  for (std::set<Species*>::const_iterator spec_it = needed_mole_fracs.begin();
        spec_it != needed_mole_fracs.end(); spec_it++)
  {
    unsigned index = local_mole_frac_index.size();
    ost << "local_frac[" << index << "] = " << MOLE_FRAC << "[" << (*spec_it)->code_name << "][tid];\n";
    local_mole_frac_index[*spec_it] = index;
  }
  if (needs_third_body)
    ost << REAL << " local_thb = " << THIRD_BODY << "[tid];\n";
  for (unsigned reac_idx = start_reac; reac_idx <= term_reac; reac_idx++)
  {
    int reaction_idx = (*it)->get_reaction_idx(reac_idx);
    Reaction *reac = (*it)->get_reaction(reaction_idx);
    if (reac != NULL && needs_scaling[reac_idx])
    {
      // Now emit the scaling for both the forward and reverse reaction rates
      // Check to make sure that it is not in the list of unimportant
      // rates for the QSSA species
      if (forward_unimportant.find(reaction_idx) != forward_unimportant.end())
        ost << "rr_f[" << reac_idx << "] = 0.0;\n";
      else
      {
        ost << "rr_f[" << reac_idx << "] = rr_f[" << reac_idx << "]"; 
        for (SpeciesCoeffMap::const_iterator co_it = reac->forward.begin();
              co_it != reac->forward.end(); co_it++)
        {
          if (co_it->first->qss_species)
            continue;
          assert(co_it->second > 0);
          assert(local_mole_frac_index.find(co_it->first) != local_mole_frac_index.end());
          for (int i = 0; i < co_it->second; i++)
          {
            ost << " * local_frac[" << local_mole_frac_index[co_it->first] << "]";
          }
        }
        ost << ";\n";
      }
      if (reac->reversible)
      {
        if (backward_unimportant.find(reaction_idx) != backward_unimportant.end())
          ost << "rr_r[" << reac_idx << "] = 0.0;\n";
        else
        {
          ost << "rr_r[" << reac_idx << "] = rr_r[" << reac_idx << "]";
          for (SpeciesCoeffMap::const_iterator co_it = reac->backward.begin();
                co_it != reac->backward.end(); co_it++)
          {
            if (co_it->first->qss_species)
              continue;
            assert(co_it->second > 0);
            assert(local_mole_frac_index.find(co_it->first) != local_mole_frac_index.end());
            for (int i = 0; i < co_it->second; i++)
            {
              ost << " * local_frac[" << local_mole_frac_index[co_it->first] << "]";
            }
          }
          ost << ";\n";
        }
      }
      else
        ost << "rr_r[" << reac_idx << "] = 0.0;\n";
      if ((reac->thb != NULL) && !reac->pressure_dep)
      {
        assert(needs_third_body);
        ost << REAL << " thb_" << reac_idx << " = local_thb";
        for (std::map<Species*,double>::const_iterator co_it = reac->thb->components.begin();
              co_it != reac->thb->components.end(); co_it++)
        {
          if (co_it->first->qss_species)
            continue;
          assert(local_mole_frac_index.find(co_it->first) != local_mole_frac_index.end());
          ost << " + " << co_it->second << "*local_frac[" << local_mole_frac_index[co_it->first] << "]";
        }
        ost << ";\n";
        ost << "rr_f[" << reac_idx << "] *= thb_" << reac_idx << ";\n";
        ost << "rr_r[" << reac_idx << "] *= thb_" << reac_idx << ";\n";
      }
    }
  }
#else
  for (std::set<Species*>::const_iterator co_it = needed_mole_fracs.begin();
        co_it != needed_mole_fracs.end(); co_it++)
  {
    PairDelim local_pair(ost);
    ost << REAL << " local_frac = " << MOLE_FRAC << "[" << (*co_it)->code_name << "][tid];\n";
    // Go through and find all the reactions that to be scaled by this value
    for (unsigned reac_idx = start_reac; reac_idx <= term_reac; reac_idx++)
    {
      int reaction_idx = warp->get_reaction_idx(reac_idx);
      Reaction *reac = warp->get_reaction(reaction_idx);
      if (reac != NULL)
      {
        if ((reac->forward.find(*co_it) != reac->forward.end()) &&
            (forward_unimportant.find(reaction_idx) == forward_unimportant.end()))
        {
          int coeff = reac->forward[*co_it];
          for (int i = 0; i < coeff; i++)
#ifdef RR_INDEXING
            ost << "rr_f[" << (reac_idx-offset) << "] *= local_frac;\n";
#else
            ost << "rr_f_" << (reac_idx-offset) << " *= local_frac;\n";
#endif
        }
        if (reac->reversible)
        {
          if ((reac->backward.find(*co_it) != reac->backward.end()) &&
              (backward_unimportant.find(reaction_idx) == backward_unimportant.end()))
          {
            int coeff = reac->backward[*co_it];
            for (int i = 0; i < coeff; i++)
#ifdef RR_INDEXING
              ost << "rr_r[" << (reac_idx-offset) << "] *= local_frac;\n";
#else
              ost << "rr_r_" << (reac_idx-offset) << " *= local_frac;\n";
#endif
          }
        }
      }
    }
  }
  // Now do the third body if necessary
  if (needs_third_body)
  {
    PairDelim thb_pair(ost);
    ost << REAL << " local_thb = " << THIRD_BODY << "[tid];\n";
    for (unsigned reac_idx = start_reac; reac_idx <= term_reac; reac_idx++)
    {
      int reaction_idx = warp->get_reaction_idx(reac_idx);
      Reaction *reac = warp->get_reaction(reaction_idx);
      if (reac != NULL)
      {
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          ost << REAL << " thb_" << reac_idx << " = local_thb";
          for (std::map<Species*,double>::const_iterator co_it = reac->thb->components.begin();
                co_it != reac->thb->components.end(); co_it++)
          {
            if (co_it->first->qss_species)
              continue;
            ost << " + " << co_it->second << "*" << MOLE_FRAC << "[" << co_it->first->code_name << "][tid]";
          }
          ost << ";\n";
#ifdef RR_INDEXING
          ost << "rr_f[" << (reac_idx-offset) << "] *= thb_" << reac_idx << ";\n";
          ost << "rr_r[" << (reac_idx-offset) << "] *= thb_" << reac_idx << ";\n";
#else
          ost << "rr_f_" << (reac_idx-offset) << " *= thb_" << reac_idx << ";\n";
          ost << "rr_r_" << (reac_idx-offset) << " *= thb_" << reac_idx << ";\n";
#endif
        }
      }
    }
  }
#endif
}

void ChemistryUnit::emit_phase_reaction(CodeOutStream &ost, unsigned reac_idx, bool second_pass, int dst_offset)
{
  // Look at this reaction for each of these warps and see if any of them are special
  std::map<Reaction*,Warp*> arrhenius_reactions; // the common case
  std::map<Reaction*,std::pair<Warp*,int> > weird_reactions; // the uncommon case
  unsigned warp_idx = 0;
  for (std::vector<Warp*>::const_iterator it = warps.begin();
        it != warps.end(); it++,warp_idx++)
  {
    // First check to see if the warp has this many reactions
    if (reac_idx >= (*it)->num_reactions())
      continue;
    int reaction_idx = (*it)->get_reaction_idx(reac_idx);  
    Reaction *reac = (*it)->get_reaction(reaction_idx);
    assert(reac != NULL);
    bool weird = is_weird_reaction(reac);
    assert(!reac->duplicate.enabled);
    if (weird)
      weird_reactions[reac] = std::pair<Warp*,int>(*it,warp_idx);
    else
      arrhenius_reactions[reac] = *it;
  }
  // Emit the weird reactions first
  bool first = true;
  ost << "// Reaction " << reac_idx << "\n";
#ifndef RR_INDEXING
  ost << REAL << " rr_f_" << (reac_idx-dst_offset) << ", rr_r_" << (reac_idx-dst_offset) << ";\n";
#endif
  for (std::map<Reaction*,std::pair<Warp*,int> >::const_iterator it = weird_reactions.begin();
        it != weird_reactions.end(); it++)
  {
    if (first)
    {
      ost << "if (__any(wid == " << it->second.second << "))\n";
      first = false;
    }
    else
      ost << "else if (__any(wid == " << it->second.second << "))\n";
    PairDelim weird_pair(ost);
    emit_weird_reaction(ost, it->first, it->second.first, reac_idx, second_pass, dst_offset);
  }
  // Make sure we have general reactions to do
  if (arrhenius_reactions.empty())
    return;
  // Now do the general reaction
  if (!first)
    ost << "else\n";
  PairDelim normal_pair(ost);
  emit_normal_reaction(ost, arrhenius_reactions, reac_idx, second_pass, dst_offset);
}

void ChemistryUnit::emit_weird_reaction(CodeOutStream &ost, Reaction *reac, Warp *warp, int reac_idx, 
                                        bool second_pass, int dst_offset)
{
  // Not handling this case right now
  assert(!reac->duplicate.enabled);
  const char *prefix = " = ";
  // Emit the forward part first
  if (reac->low.enabled)
  {
    ost << REAL << " rr_k0;\n"; // << low.a;
    if (unit->no_nondim)
      arrhenius(ost, warp, true/*code*/, true/*weird*/, reac_idx, "rr_k0", 
                      reac->low.a, reac->low.beta, reac->low.e, second_pass);
    else
      arrhenius(ost, warp, true/*code*/, true/*weird*/, reac_idx, "rr_k0", 
                      (reac->low.a*REACTION_RATE_REF), reac->low.beta, reac->low.e, second_pass);
    ost << REAL << " rr_kinf;\n"; //<< a;
    if (unit->no_nondim)
      arrhenius(ost, warp, true/*code*/, true/*weird*/, reac_idx, "rr_kinf", 
                      reac->a, reac->beta, reac->e, second_pass);
    else
      arrhenius(ost, warp, true/*code*/, true/*weird*/, reac_idx, "rr_kinf", 
                      (reac->a*REACTION_RATE_REF), reac->beta, reac->e, second_pass);
    /* calculate pr */
    ost << REAL << " pr = rr_k0 / rr_kinf * ";
    if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
    {
      ost << MOLE_FRAC << "[" << reac->pressure_species->code_name << "][tid]";
    }
    else
    {
      assert(reac->thb != NULL);
      ost << "(" << THIRD_BODY << "[tid]";
      for (std::map<Species*,double>::const_iterator it = reac->thb->components.begin();
            it != reac->thb->components.end(); it++)
      {
        if (!it->first->qss_species)
          ost << " + " << it->second << "*" << MOLE_FRAC << "[" << it->first->code_name << "][tid]";
      }
      ost << ")";
    }
    ost << ";\n";
    if (reac->troe.num > 0)
    {
      ost << REAL << " fcent = log10(" << (1.0-(reac->troe.a)) << " * exp(";
      if (!unit->no_nondim)
        ost << (-1.0*TEMPERATURE_REF/reac->troe.t3) << " * " << TEMPERATURE << ")";
      else
        ost << (-1.0/reac->troe.t3) << " * " << TEMPERATURE << ")";
      // Check for 0.0 here since in some cases
      // if we emit it we end up getting -inf for -1.0/troe.t1
      if (reac->troe.a != 0.0)
      {
        if (!unit->no_nondim)
          ost << " + " << reac->troe.a << " * exp(" << (-1.0*TEMPERATURE_REF/reac->troe.t1) << " * " << TEMPERATURE << ")";
        else
          ost << " + " << reac->troe.a << " * exp(" << (-1.0/reac->troe.t1) << " * " << TEMPERATURE << ")";
      }
      if (reac->troe.num == 4)
      {
        if (unit->no_nondim)
          ost << " + exp(" << (-(reac->troe.t2)) << " * otc)"; 
        else
          ost << " + exp(" << (-(reac->troe.t2)/TEMPERATURE_REF) << " * otc)";
      }
      ost << ");\n";
      ost << REAL << " flogpr = log10(pr) - 0.4 - 0.67 * fcent;\n";
      ost << REAL << " fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;\n";
      ost << REAL << " fquan = flogpr / fdenom;\n";
      ost << "fquan = fcent / (1.0 + fquan * fquan);\n";
#ifdef RR_INDEXING
      ost.print("rr_f[%d] %s rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);\n", (reac_idx-dst_offset), prefix);
#else
      ost << "rr_f_" << (reac_idx-dst_offset) << " " << prefix << " rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);\n";
#endif
    }
    else if (reac->sri.num > 0)
    {
      ost << REAL << " fpexp = log10(pr);\n";
      ost << REAL << " fpexp = 1. / (1. + fpexp * fpexp);\n";
      ost << REAL << " fquan = (";
      ost << reac->sri.a;
      ost << " * exp(";
      if (unit->no_nondim)
        ost << (-(reac->sri.b));
      else
        ost << (-(reac->sri.b)/TEMPERATURE_REF);
      ost << "*otc) + exp(";
      ost << (-1./(reac->sri.c));
      assert(false); // need to handle power
      ost << "*tc)) ** fpexp;\n";
      if (reac->sri.num == 5) {
          ost << " fquan = fquan * ";
          ost << reac->sri.d;
          ost << " * pow(" << TEMPERATURE <<",";
          ost << reac->sri.e;
          ost << ");\n";
      }
#ifdef RR_INDEXING
      ost.print("rr_f[%d] %s rr_kinf * pr/(1.0 + pr) * fquan;\n", (reac_idx-dst_offset), prefix);
#else
      ost << "rr_f_" << (reac_idx-dst_offset) << " " << prefix << " rr_kinf * pr/(1.0 + pr) * fquan;\n";
#endif
    }
    else
    {
      // plain old Lindemann
#ifdef RR_INDEXING
      ost.print("rr_f[%d] %s rr_kinf * pr/(1.0 + pr);\n", (reac_idx-dst_offset), prefix);
#else
      ost << "rr_f_" << (reac_idx-dst_offset) << " " << prefix << " rr_kinf * pr/(1.0 + pr);\n";
#endif
    }
  }
  else if (reac->lt.enabled)
  {
    /* Landau-Teller formulation */
    if (unit->no_nondim)
      ost << REAL << " ftroot = pow(otc,(1./3.));\n";
    else
      ost << REAL << " ftroot = pow(otc/" << TEMPERATURE_REF << ",(1./3.));\n";
#ifdef RR_INDEXING
    ost.print("rr_f[%d] %s ", (reac_idx-dst_offset), prefix);
#else
    ost << "rr_f_" << (reac_idx-dst_offset) << " " << prefix << " ";
#endif
    if (!unit->no_nondim)
      ost << (REACTION_RATE_REF * reac->a * pow(TEMPERATURE_REF,reac->beta));
    else
      ost << reac->a;
    ost << " * exp(";
    ost << reac->beta;
    ost << "*vlntemp- ";
    if (unit->no_nondim)
      ost << reac->e;
    else
      ost << (reac->e/TEMPERATURE_REF);
    ost << "*otc + ";
    ost << reac->lt.b;
    ost << "*ftroot + ";
    ost << reac->lt.c;
    ost << "*ftroot*ftroot);\n";
  }
  else
  {
    char target[128];
#ifdef RR_INDEXING
    sprintf(target,"rr_f[%d]", (reac_idx-dst_offset));
#else
    sprintf(target,"rr_f_%d", (reac_idx-dst_offset));
#endif
    if (!unit->no_nondim)
      arrhenius(ost, warp, true/*code*/, true/*weird*/, reac_idx, target, (reac->a*REACTION_RATE_REF), reac->beta, reac->e, second_pass);
    else
      arrhenius(ost, warp, true/*code*/, true/*weird*/, reac_idx, target, reac->a, reac->beta, reac->e, second_pass);
  } 
  // Now do the reverse case
  if (!reac->reversible)
  {
#ifdef RR_INDEXING
    ost << "rr_r[" << (reac_idx-dst_offset) << "] = 0.0;\n";
#else
    ost << "rr_r_" << (reac_idx-dst_offset) << " = 0.0;\n";
#endif
    return;
  }
  if (reac->rev.enabled)
  {
    if (reac->rlt.enabled)
    {
      if (!reac->lt.enabled)
      {
        if (unit->no_nondim)
          ost << REAL << " ftroot = pow(otc,(1./3.));\n";
        else
          ost << REAL << " ftroot = pow(otc/" << TEMPERATURE_REF << ", (1./3.));\n";
      }
#ifdef RR_INDEXING
      ost << "rr_r[" << (reac_idx-dst_offset) << "] = ";
#else
      ost << "rr_r_" << (reac_idx-dst_offset) << " = ";
#endif
      if (!unit->no_nondim)
        ost << (reac->a*pow(TEMPERATURE_REF,reac->beta));
      else
        ost << reac->a;
      ost << " * exp(";
      ost << reac->beta;
      ost << "*vlntemp- ";
      if (unit->no_nondim)
        ost << reac->e;
      else
        ost << (reac->e / TEMPERATURE_REF); // scale by temperature ref for non-dimensionalized case
      ost << "*otc + ";
      ost << reac->rlt.b;
      ost << "*ftroot + ";
      ost << reac->rlt.c;
      ost << "*ftroot*ftroot);\n";
    }
    else
    {
      /* explicit Arrhenius rate given */
      char target[128];
#ifdef RR_INDEXING
      sprintf(target,"rr_r[%d]",(reac_idx-dst_offset));
#else
      sprintf(target,"rr_r_%d", (reac_idx-dst_offset));
#endif
      if (!unit->no_nondim)
        arrhenius(ost, warp, true/*code*/, true/*weird*/, reac_idx, target, 
                              (REACTION_RATE_REF*reac->rev.a), reac->rev.beta, reac->rev.e, second_pass);
      else
        arrhenius(ost, warp, true/*code*/, true/*weird*/, reac_idx, target, 
                              reac->rev.a, reac->rev.beta, reac->rev.e, second_pass);
    }
  }
  else
  {
    /* use equilibrium relation */
    /* total nu */
    int nutot = 0;
    std::set<Species*> needed_gibbs;
    for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
          it != reac->stoich.end(); it++)
    {
      nutot += it->second;
      needed_gibbs.insert(it->first);
    }
    /* total stoich gibbs energy */
    emit_gibbs_computations(ost, needed_gibbs); 
    ost << REAL << " xik = ";
    bool first = true;
    for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
          it != reac->stoich.end(); it++)
    {
      if (first)
      {
        first = false;
        if (it->second > 1)
        {
          ost.print("%d.0 * ", it->second);
        }
        else if (it->second == -1)
        {
          ost << "-";
        }
        else if (it->second < -1)
        {
          ost.print("-%d.0 * ", -(it->second));
        }
      }
      else
      {
        if (it->second == 1)
        {
          ost << " + ";
        }
        else if (it->second > 1)
        {
          ost.print(" + %d.0 * ", it->second);
        }
        else if (it->second == -1)
          ost << " - ";
        else if (it->second < -1)
        {
          ost.print(" - %d.0 * ", -it->second);
        }
      }
      ost << "gibbs_" << it->first->code_name;
    }
    ost << ";\n";
#ifdef RR_INDEXING
    ost << "rr_r[" << (reac_idx-dst_offset) << "] = rr_f[" << (reac_idx-dst_offset) << "]";
#else
    ost << "rr_r_" << (reac_idx-dst_offset) << " = rr_f_" << (reac_idx-dst_offset);
#endif
    // This case of otc gets handled by folding it back through
    // all the gibbs free energy constants
    ost.print(" * exp(xik*otc");
    //if (unit->no_nondim) 
    {
      if (nutot == 0)
        ost << ")";
      else if (nutot == 1)
      {
        ost << ") * oprt";
      }
      else if (nutot == 2)
      {
        ost << ") * oprt * oprt";
      }
      else if (nutot == -1)
      {
        ost << ") * prt";
      }
      else if (nutot == -2)
      {
        ost << ") * prt * prt";
      }
      else if (nutot > 0)
      {
        if (unit->no_nondim)
          ost.print(" - %d.0*vlntemp)", nutot);
        else
          ost << " - " << nutot << ".0*(vlntemp + " << TEMPERATURE_REF << "))";
      }
      else 
      {
        if (unit->no_nondim)
          ost.print(" + %d.0*vlntemp)", -nutot);
        else
          ost << " + " << (-nutot) << ".0*(vlntemp + " << TEMPERATURE_REF << "))";
      }
    }
    ost << ";\n";
  }
}

void ChemistryUnit::emit_gibbs_computations(CodeOutStream &ost, const std::set<Species*> &needed_gibbs)
{
  assert(!needed_gibbs.empty());
  ost << REAL << " ";
  bool first = true;
  for (std::set<Species*>::const_iterator it = needed_gibbs.begin();
        it != needed_gibbs.end(); it++)
  {
    if (first)
      ost << "gibbs_" << (*it)->code_name;
    else
      ost << ", gibbs_" << (*it)->code_name;
    first = false;
  }
  ost << ";\n";
  PairDelim gibbs_pair(ost);
  ost << "const " << REAL << " &tk1 = " << TEMPERATURE << ";\n";
  ost << "const " << REAL << " tk2 = tk1 * tk1;\n";
  ost << "const " << REAL << " tk3 = tk1 * tk2;\n";
  ost << "const " << REAL << " tk4 = tk1 * tk3;\n";
  ost << "const " << REAL << " tk5 = tk1 * tk4;\n";
  for (std::set<Species*>::const_iterator it = needed_gibbs.begin();
        it != needed_gibbs.end(); it++)
  {
    Species *spec = *it;
    // Find the index in the gibbs species for this species or make a new one
    int spec_idx = -1;
    for (unsigned idx = 0; idx < gibbs_species.size(); idx++)
    {
      if ((*it) == gibbs_species[idx])
      {
        spec_idx = idx;
        break;
      }
    }
    if (spec_idx == -1)
    {
      spec_idx = gibbs_species.size();
      gibbs_species.push_back(*it);
    }
    ost << "// " << spec->name << "\n";
    PairDelim spec_pair(ost);
    ost << "if (tk1 > gibbs_temperatures[" << spec_idx << "])\n";
    ost.down();
    ost << "gibbs_" << spec->code_name << " = ";
    ost << "gibbs_high[" << spec_idx << "][0]*tk1*(";
    if (!unit->no_nondim)
      ost << (1-log(TEMPERATURE_REF));
    else
      ost << "1";
    ost << "-vlntemp) + ";
    ost << "gibbs_high[" << spec_idx << "][1]*tk2 + ";
    ost << "gibbs_high[" << spec_idx << "][2]*tk3 + ";
    ost << "gibbs_high[" << spec_idx << "][3]*tk4 + ";
    ost << "gibbs_high[" << spec_idx << "][4]*tk5 + ";
    ost << "(gibbs_high[" << spec_idx << "][5] - ";
    ost << "tk1*gibbs_high[" << spec_idx << "][6]);\n";
    ost.up();
    ost << "else\n";
    ost.down();
    ost << "gibbs_" << spec->code_name << " = ";
    ost << "gibbs_low[" << spec_idx << "][0]*tk1*(";
    if (!unit->no_nondim)
      ost << (1-log(TEMPERATURE_REF));
    else
      ost << "1";
    ost << "-vlntemp) + ";
    ost << "gibbs_low[" << spec_idx << "][1]*tk2 + ";
    ost << "gibbs_low[" << spec_idx << "][2]*tk3 + ";
    ost << "gibbs_low[" << spec_idx << "][3]*tk4 + ";
    ost << "gibbs_low[" << spec_idx << "][4]*tk5 + ";
    ost << "(gibbs_low[" << spec_idx << "][5] - ";
    ost << "tk1*gibbs_low[" << spec_idx << "][6]);\n";
    ost.up();
  }
}

void ChemistryUnit::emit_normal_reaction(CodeOutStream &ost, const std::map<Reaction*,Warp*> &arrhenius_reactions, 
                                          int reac_idx, bool second_pass, int dst_offset)
{
  // Do the forward
  {
    char target[128];
#ifdef RR_INDEXING
    sprintf(target,"rr_f[%d]", (reac_idx-dst_offset));
#else
    sprintf(target,"rr_f_%d", (reac_idx-dst_offset));
#endif
    bool first = true;
    for (std::map<Reaction*,Warp*>::const_iterator it = arrhenius_reactions.begin();
          it != arrhenius_reactions.end(); it++)
    {
      if (!unit->no_nondim)
        arrhenius(ost, it->second, first, false/*weird*/, reac_idx, target, 
                    (it->first->a*REACTION_RATE_REF), it->first->beta, it->first->e, second_pass);
      else
        arrhenius(ost, it->second, first, false/*weird*/, reac_idx, target, 
                    it->first->a, it->first->beta, it->first->e, second_pass);
      first = false;
    }
  }
  
  {
    char target[128];
#ifdef RR_INDEXING
    sprintf(target,"rr_r[%d]", (reac_idx-dst_offset));
#else
    sprintf(target,"rr_r_%d", (reac_idx-dst_offset));
#endif
    bool first = true;
    for (std::map<Reaction*,Warp*>::const_iterator it = arrhenius_reactions.begin();
          it != arrhenius_reactions.end(); it++)
    {
      // Not handling this case right now
      assert(!it->first->duplicate.enabled);
      if (!unit->no_nondim)
        arrhenius(ost, it->second, first, false/*weird*/, reac_idx, target, 
                  (it->first->rev.a*REACTION_RATE_REF), it->first->rev.beta, it->first->rev.e, second_pass);
      else
        arrhenius(ost, it->second, first, false/*weird*/, reac_idx, target, 
                  it->first->rev.a, it->first->rev.beta, it->first->rev.e, second_pass);
      first = false;
    }
  }
}

void ChemistryUnit::arrhenius(CodeOutStream &ost, Warp *warp, bool emit_code, bool weird, int reac_idx,
                              const char *target, double a, double beta, double e, bool second_pass, unsigned warp_size)
{
  bool emit_constants = true;
  int a_off = -1;
  int beta_off = -1;
  int e_off = -1;
  if (warp->can_add_constants(reac_idx, 3, second_pass))
  {
    emit_constants = false;
    beta_off = warp->add_constant(reac_idx, beta, second_pass);
    // Make it so we are always doing addition 
    // by negating the value of e
    e_off = warp->add_constant(reac_idx, -e, second_pass);
    if (!unit->no_nondim)
      a_off = warp->add_constant(reac_idx, a*pow(TEMPERATURE_REF,beta), second_pass);
    else
      a_off = warp->add_constant(reac_idx, a, second_pass);
  }
  else if (!weird)
    assert(false); // if we're not doing a weird reaction then this is really bad
  // If we're not emitting code, then we are done
  if (!emit_code)
    return;
  // Otherwise do the code generation
  if (emit_constants)
  {
    ost << target << " = ";
    if (!unit->no_nondim)
      ost << (a*pow(TEMPERATURE_REF,beta));
    else
      ost << a;
    ost << " * exp(" << beta << "*vlntemp";
    if (e < 0.)
      ost << " + " << -e;
    else
      ost << " - " << e;
    ost << "*ortc);\n";
  }
  else
  {
    PairDelim pair(ost);
    // figure out which thread in our warp owns these constants
    ost << INT << " hi_part, lo_part;\n";
    // Get the beta first
    assert(beta_off != -1);
    int location = reac_idx*MAX_REACTION_CONSTANTS + beta_off;
    int register_index = location/warp_size;
    int lane_index = location%warp_size;
    ost << "hi_part = __shfl(__double2hiint(reaction_constants[" << register_index << "]), " << lane_index << ", " << warp_size << ");\n";
    ost << "lo_part = __shfl(__double2loint(reaction_constants[" << register_index << "]), " << lane_index << ", " << warp_size << ");\n";
    ost << REAL << " arrhenius = __hiloint2double(hi_part,lo_part) * vlntemp;\n";
    // Now get the e value
    assert(e_off != -1);
    location = reac_idx*MAX_REACTION_CONSTANTS + e_off;
    register_index = location/warp_size;
    lane_index = location%warp_size;
    ost << "hi_part = __shfl(__double2hiint(reaction_constants[" << register_index << "]), " << lane_index << ", " << warp_size << ");\n";
    ost << "lo_part = __shfl(__double2loint(reaction_constants[" << register_index << "]), " << lane_index << ", " << warp_size << ");\n";
    ost << "arrhenius = __fma_rn(__hiloint2double(hi_part,lo_part),ortc,arrhenius);\n";
    ost << "arrhenius = exp(arrhenius);\n";
    // finally get the a value
    assert(a_off != -1);
    location = reac_idx*MAX_REACTION_CONSTANTS + a_off;
    register_index = location/warp_size;
    lane_index = location%warp_size;
    ost << "hi_part = __shfl(__double2hiint(reaction_constants[" << register_index << "]), " << lane_index << ", " << warp_size << ");\n";
    ost << "lo_part = __shfl(__double2loint(reaction_constants[" << register_index << "]), " << lane_index << ", " << warp_size << ");\n";
    ost << target << " = " << "__hiloint2double(hi_part,lo_part) * arrhenius;\n";
  }
}

void ChemistryUnit::emit_constant_declarations(CodeOutStream &ost, unsigned num_species, 
                            unsigned &constants_per_thread, unsigned &warp_constant_stride, unsigned warp_size /*= 32*/)
{
  // First go through all the species and emit the definitions for their code names
  {
    ost << "\n";
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      ost << "#define " << (*it)->code_name << "  " << spec_idx << "\n";
      spec_idx++;
    }
    ost << "#define NUM_SPECIES  " << num_species << "\n";
    ost << "\n\n";
  }
  // Now emit the molecular masses and the inverse molecular masses
  {
    unsigned spec_idx = 0;
    ost << "__constant__ double molecular_mass[" << num_species << "] = {";
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      if (spec_idx > 0)
        ost << ", ";
      ost << (*it)->molecular_mass;
      spec_idx++;
    }
    ost << "};\n\n";
  }
  {
    unsigned spec_idx = 0;
    ost << "__constant__ double recip_molecular_mass[" << num_species << "] = {";
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      if (spec_idx > 0)
        ost << ", ";
      ost << (1.0/((*it)->molecular_mass));
      spec_idx++;
    }
    ost << "};\n\n";
  }
  // Now emit the constants for the gibbs computations
  {
    ost << "__constant__ double gibbs_temperatures[" << gibbs_species.size() << "] = {";
    bool first = true;
    for (std::vector<Species*>::const_iterator it = gibbs_species.begin();
          it != gibbs_species.end(); it++)
    {
      if (!first)
        ost << ", ";
      ost << (*it)->get_common_temperature();
      first = false;
    }
    ost << "};\n\n";
  }
  {
    ost << "__constant__ double gibbs_high[" << gibbs_species.size() << "][7] = {";
    bool first = true;
    for (std::vector<Species*>::const_iterator it = gibbs_species.begin();
          it != gibbs_species.end(); it++)
    {
      if (!first)
        ost << ", {";
      else
        ost << " {";
      for (int idx = 0; idx < 7; idx++)
      {
        if (idx > 0)
          ost << ", ";
        ost << (*it)->get_high_coefficient(idx);
      }
      ost << "}";
      first = false;
    }
    ost << " };\n\n";
  }
  {
    ost << "__constant__ double gibbs_low[" << gibbs_species.size() << "][7] = {";
    bool first = true;
    for (std::vector<Species*>::const_iterator it = gibbs_species.begin();
          it != gibbs_species.end(); it++)
    {
      if (!first)
        ost << ", {";
      else
        ost << " {";
      for (int idx = 0; idx < 7; idx++)
      {
        if (idx > 0)
          ost << ", ";
        ost << (*it)->get_low_coefficient(idx);
      }
      ost << "}";
      first = false;
    }
    ost << " };\n\n";
  }

  {
    ost << "__constant__ " << INT << " diffusion_specs[" << stif_operations.size() << "] = {";
    bool first = true;
    for (std::vector<Stif*>::const_iterator it = stif_operations.begin();
          it != stif_operations.end(); it++)
    {
      if (!first)
        ost << ", ";
      ost << (*it)->k;
      first = false;
    }
    ost << "};\n\n";
  }

  // Now emit the array of reaction constants for each warp 
  {
    assert(!warps.empty());
    assert((32%warp_size) == 0);
    // first compute the warp stride
    warp_constant_stride = warps[0]->num_reactions()*MAX_REACTION_CONSTANTS*(32/warp_size);
    // round up to the nearest multiple of 32 which will be how many
    // constants are stored in each thread of a warp
    while ((warp_constant_stride%32) != 0)
      warp_constant_stride++;
    constants_per_thread = warp_constant_stride/32;
    ost << "__device__ const double warp_constants[" << (warps.size()*warp_constant_stride) << "] = {";
    if (warp_size == 32)
    {
      bool first = true;
      for (std::vector<Warp*>::const_iterator it = warps.begin();
            it != warps.end(); it++)
      {
        unsigned emitted_warp_constants = (*it)->emit_constants(ost,first);
        // Pad the rest of the entries with zeros
        for ( ; emitted_warp_constants < warp_constant_stride; emitted_warp_constants++)
          ost << ", 0.0";
        first = false;
      }
    }
    else
    {
      const int micro_warps = 32/warp_size;
      bool first = true;
      for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
      {
        unsigned emitted_warp_constants = 0;
        unsigned offset = 0;
        while (emitted_warp_constants < warp_constant_stride)
        {
          for (int i = 0; i < micro_warps; i++)
          {
            warps[idx*micro_warps+i]->emit_num_constants(ost,first,offset,warp_size);
            emitted_warp_constants += warp_size;
            first = false;
          }
          offset += warp_size;
        }
        assert(emitted_warp_constants == warp_constant_stride);
      }
    }
    ost << "};\n\n";
  }
}

void ChemistryUnit::emit_two_pass_getrates_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values,
                   const char *temp_file_name, int warps_per_stage, int reactions_per_warp, int second_pass_offset)
{
  unsigned constants_per_thread;
  unsigned warp_constant_stride;
  emit_constant_declarations(ost,num_species,constants_per_thread,warp_constant_stride);
  ost << "__global__ void\n";
  ost << "gpu_getrates(";
  ost << "const " << REAL << " *" << PRESSURE_ARRAY;
  ost << ", const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
  ost << ", const " << REAL << " *" << DIFFUSION_ARRAY;
  ost << ", const " << REAL << " recip_dt/*reciprocal of dt*/";
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << WDOT;
  ost << ")\n";
  PairDelim pair(ost);
  ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
  ost << "const " << INT << " wid = threadIdx.x >> 5;\n";
  // Compute the offset for this thread
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*row_stride) + "
        << "(blockIdx.x*" << POINTS << " + tid);\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << DIFFUSION_ARRAY << " += offset;\n";
    ost << WDOT << " += offset;\n";
  }
  // Decleare our shared memory arrays
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << num_species << "][" << POINTS << "];\n";
  ost << "volatile __shared__ " << REAL << " scratch[" << scratch_values << "][" << POINTS << "];\n";
  // check to see if need a third body value
  if (!unit->third_bodies.empty())
    ost << "volatile __shared__ " << REAL << " " << THIRD_BODY << "[" << POINTS << "];\n";

  // Now we can do the warp specialization
  ost << "if (__any(wid < " << unit->threads_per_point << "))\n";
  {
    PairDelim experiment_pair(ost);
    ost << REAL << " reaction_constants[" << constants_per_thread << "];\n";
    // emit the loads for the reaction constants
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char src_offset[128];
      sprintf(src_offset,"wid*%d+%d",warp_constant_stride,(idx*32));
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      emit_cuda_load(ost,"reaction_constants",dst_offset,"warp_constants",src_offset,false/*double2*/,".cg");
    }
    // Then copy over the code from the temporary file
    FILE *temp_file = fopen(temp_file_name,"r");
    assert(temp_file != NULL);
    char c = fgetc(temp_file);
    while (c != EOF)
    {
      ost << c;
      c = fgetc(temp_file);
    }
    assert(fclose(temp_file) == 0);
  }
  ost << "else\n";
  {
    PairDelim reduction_pair(ost);
    emit_two_pass_reducer_code(ost,num_species,scratch_values, warps_per_stage, reactions_per_warp, second_pass_offset);
  }
}

void ChemistryUnit::emit_two_pass_experiment_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values,
                                                  int warps_per_stage, int reactions_per_warp, int second_pass_offset)
{
  const int max_specs = (num_species+(unit->threads_per_point-1))/unit->threads_per_point;
  const bool uniform_specs = ((num_species%unit->threads_per_point) == 0);
  // Really don't want the compiler unrolling this loop since it would
  // be really bad for the instruction cache
  ost << "#pragma unroll 1\n";
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim step_loop_pair(ost);
  // First issue loads for our temperature and mass fractions
  ost << REAL << " " << TEMPERATURE << ";\n";
  ost << REAL << " " << MASS_FRAC << "[" << max_specs << "];\n";
  ost << "\n";
  // Cache at all levels since lots of people are all reading the same temperature
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,false/*double2*/,".ca");  
  for (int idx = 0; idx < max_specs; idx++)
  {
    char src_offset[128];
    sprintf(src_offset,"(wid+%d)*spec_stride",idx);
    char dst_offset[128];
    sprintf(dst_offset,"%d",idx);
    if (!uniform_specs && (idx == (max_specs-1)))
      ost << "if (__any((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << "))\n";
    emit_cuda_load(ost,MASS_FRAC,dst_offset,MASS_FRAC_ARRAY,src_offset,false/*double2*/,".cg");
  }
  ost << "\n";
  // Now we can compute a whole bunch of things that we'll need later 
  // for doing the reaction computations
  unit->emit_numerical_constants(ost); 
  ost << "\n";
  unit->emit_temperature_dependent(ost);
  ost << "\n";
  // Do a barrier here to make sure any stragglers from the previous loop have
  // finished reading from shared memory
  ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  // Now we compute our updated mass fractions and write them into shared memory
  // along with our partial value of sumyow
  {
    PairDelim mass_frac_pair(ost);
    ost << REAL << " sumyow = 0.0;\n";
    for (int idx = 0; idx < max_specs; idx++)
    {
      if (!uniform_specs && (idx == (max_specs-1)))
        ost << "if (__any((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << "))\n";
      PairDelim spec_pair(ost);   
      ost << REAL << " temp = " << MASS_FRAC << "[" << idx << "] * recip_molecular_mass[wid+" << (idx*unit->threads_per_point) << "];\n";
      ost << "sumyow = __fma_rn(temp," << TEMPERATURE << ",sumyow);\n";
      // Write the value into mole frac for the reducer to find
      ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid] = " << MASS_FRAC << "[" << idx << "];\n";
    }
    // This better be true or we are in a world of trouble
    assert(unit->threads_per_point <= scratch_values);
    // Write our partial sumyow into scratch space
    ost << "scratch[wid][tid] = sumyow;\n";
  }
  ost << "\n";
  // Now tell the reducer warp that we've written our values into shared
  ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  ost << "\n";
  
  // Allocate however much space we're going to need for holding the XQ values 
  {
    unsigned max_xq = 0;
    for (std::vector<Warp*>::const_iterator it = warps.begin();
          it != warps.end(); it++)
    {
      std::vector<int> needed_xqs;
      (*it)->compute_needed_xqs(connected_components,needed_xqs);
      if (needed_xqs.size() > max_xq)
        max_xq = needed_xqs.size();
    }
    assert(max_xq != 0);
    ost << REAL << " xq_values[" << max_xq << "];\n";
  }
  
  // Now we get to start handling experiments, do the first pass 
  {
    ost << "// First Pass Reactions\n";
    PairDelim first_pass_pair(ost);
    // Now we can start doing the experiment phases
    // Figure out the maximum number of reactions in any warp
    int max_reactions = -1;
    for (std::vector<Warp*>::const_iterator it = warps.begin();
          it != warps.end(); it++)
    {
      if (int((*it)->num_reactions()) > max_reactions)
        max_reactions = (*it)->num_reactions();
    }
    assert(max_reactions > 0);
    // Note even though it looks like we are materializing all the forward
    // and backward reaction rates, the compiler should be smart enough to
    // notice that most of them will not be live for the entire lifetime of
    // this array and will therefore reclaim the registers.
#ifdef RR_INDEXING
    ost << REAL << " rr_f[" << max_reactions << "];\n";
    ost << REAL << " rr_r[" << max_reactions << "];\n";
#endif

    // We assume for now that there are at least two phases and the the
    // first one is QSSA phase and the second phase is all stiff species
    assert(phases.size() >= 2);
    assert(phases[0]->stifs.empty());
    assert(phases[1]->components.empty());
    unsigned next_reaction = 0;

    // Do the first phase reactions
    emit_first_pass_reactions(ost, phases[0], next_reaction);
    
    // Global barrier to indicate that the molar fractions are in place
    // by the reducer thread
    ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";

    // Do each stage's writes of their reaction rates for the QSSA phase
    emit_first_pass_writes(ost, phases[0], next_reaction);
    next_reaction = phases[0]->max_reaction+1;
    
    // Do the second phase reactions
    emit_first_pass_reactions(ost, phases[1], next_reaction);

    // Global barrier to make sure that all the xq_values have been writen
    // by the reducer
    ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";

    // Read out the xq_values, then barrier across the experiment threads
    // to make sure everyone has read their xq_values
    {
      bool first = true;
      for (unsigned idx = 0; idx < warps.size(); idx++)
      {
        std::vector<int> needed_xqs;
        warps[idx]->compute_needed_xqs(connected_components,needed_xqs);
        if (needed_xqs.empty())
          continue;
        if (first)
          ost << "if (__any(wid == " << idx << "))\n";
        else
          ost << "else if (__any(wid == " << idx << "))\n";
        PairDelim xq_pair(ost);
        unsigned dst_idx = 0;
        for (std::vector<int>::const_iterator it = needed_xqs.begin();
              it != needed_xqs.end(); it++,dst_idx++)
        {
          ost << "xq_values[" << dst_idx << "] = scratch[" << (*it) << "][tid];\n";
        }
        first = false;
        ost << "asm volatile(\"bar.sync 1," << (unit->threads_per_point*32) << ";\" : : : \"memory\");\n";
        // Scale any of our reactions that need scaling by their xq_values
        phases[0]->emit_apply_qssa_factors(ost, warps[idx]);
      }
    }

    // Do each stage's writes of their reaction rates for the first Stif phase
    emit_first_pass_writes(ost, phases[1], next_reaction);
    next_reaction = phases[1]->max_reaction+1;

    // Do remaining Stif stages updating reaction rates with necessary QSSA values
    for (unsigned idx = 2; idx < phases.size(); idx++)
    {
      emit_first_pass_reactions(ost, phases[idx], next_reaction);
      emit_first_pass_writes(ost, phases[idx], next_reaction);
      next_reaction = phases[idx]->max_reaction+1;
    }
  }

  // Do the second pass of the reactions
  {
    unsigned num_passes = (warps[0]->reactions.size() + (reactions_per_warp-1))/reactions_per_warp;
    for (unsigned idx = 0; idx < num_passes; idx++)
    {
      unsigned start_reac = idx * reactions_per_warp;
      unsigned stop_reac = (idx+1)*reactions_per_warp-1;
      if (stop_reac >= (warps[0]->reactions.size()))
        stop_reac = warps[0]->reactions.size()-1;
      emit_second_pass_reactions(ost, start_reac, stop_reac, warps_per_stage, second_pass_offset);
    }
  }

  // finally update the pointers that we need
  {
    PairDelim update_pair(ost);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
  }
}

void ChemistryUnit::emit_two_pass_reducer_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values,
                                              int warps_per_stage, int reactions_per_warp, int second_pass_offset)
{
  // Really don't want the compiler unrolling this loop
  // since it would be really bad for the instruction cache
  ost << "#pragma unroll 1\n";
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim step_loop_pair(ost);
  // Issue the load for the pressure that we're going to need
  ost << REAL << " " << PRESSURE << ";\n";
  emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,false/*double2*/,".cg");
  // Do a barrier here to say that we've made it to this point in case we
  // were slow from the previous loop
  ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  // Now wait for the experiment warps to say they've written their values into shared
  ost << "\n";
  ost << "asm volatile(\"bar.sync 1," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  ost << "\n";
  // Do the sumyow reduction and then compute all the molar fractions
  {
    PairDelim sumyow_pair(ost);
    ost << REAL << " sumyow = scratch[0][tid];\n";
    for (unsigned idx = 1; idx < unit->threads_per_point; idx++)
      ost << "sumyow += scratch[" << idx << "][tid];\n";
    // Scale it and then invert it
    ost << "sumyow *= (";
    if (unit->no_nondim)
      ost << "8.314510e+07";
    else
      ost << (8.314510e+07 * TEMPERATURE_REF / PRESSURE_REF);
    ost << ");\n";
    ost << "sumyow = " << PRESSURE << " / sumyow;\n";
    {
      PairDelim spec_pair(ost);
      if (!unit->third_bodies.empty())
      {
        ost << REAL << " ctot = 0.0;\n";
        ost << REAL << " frac;\n";
      }
      ost << REAL << " " << MASS_FRAC << "[8];\n";
      // Pipepline shared memory loads into groups of 8
      for (unsigned idx = 0; idx < num_species; idx+=8)
      {
        // Issue up to 8 loads 
        for (unsigned spec_idx = idx; ((spec_idx-idx) < 8) && (spec_idx < num_species); spec_idx++)
        {
          ost << MASS_FRAC << "[" << (spec_idx-idx) << "] = " << MOLE_FRAC << "[" << spec_idx << "][tid];\n";
        }
        // Handle the 8 values
        for (unsigned spec_idx = idx; ((spec_idx-idx) < 8) && (spec_idx < num_species); spec_idx++)
        {
          if (!unit->third_bodies.empty())
          {
            ost << "frac = " << MASS_FRAC << "[" << (spec_idx-idx) << "] * sumyow * recip_molecular_mass[" << spec_idx << "];\n";
            ost << MOLE_FRAC << "[" << spec_idx << "][tid] = frac;\n";
            ost << "ctot += frac;\n";
          }
          else
          {
            ost << MOLE_FRAC << "[" << spec_idx << "][tid] = " << MASS_FRAC << "[" << (spec_idx-idx) << "] * sumyow * recip_molecular_mass["
                << spec_idx << "];\n";
          }
        }
      }
      if (!unit->third_bodies.empty())
        ost << THIRD_BODY << "[tid] = ctot;\n";
    }
  }
  // Indicate that we've finished writing out the mole fraction values
  ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  
  // emit code to do the first pass phases
  // we assume the first phase is all QSSA and the second phase is all Stiffness
  assert(phases.size() >= 2);
  assert(phases[0]->stifs.empty());
  assert(phases[1]->components.empty());
  
  // emit the first phase operations
  // First the QSSA values
  {
    PairDelim xq_pair(ost);
    unsigned num_qss_species = 0;
    for (std::set<Species*>::const_iterator it = unit->species.begin();
          it != unit->species.end(); it++)
    {
      if ((*it)->qss_species)
        num_qss_species++;
    }
    // We'll have this write the values into scratch directly
    phases[0]->emit_staged_operations(ost, num_species, "scratch", 0); 
    // Notify the reaction threads that the xq_values are ready
    ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  }
  // Now do the stiff values
  {
    unsigned stif_offset = 0;
    ost << REAL << " stif_values[" << stif_operations.size() << "];\n";
    for (unsigned idx = 1; idx < phases.size(); idx++)
    {
      stif_offset = phases[idx]->emit_staged_operations(ost, num_species, "stif_values", stif_offset);
    }
  }
  // Write out the stif species into shared memory and then signal the experiment threads
  // that the stif species are in memory
  for (unsigned idx = 0; idx < stif_operations.size(); idx++)
  {
    ost << "scratch[" << idx << "][tid] = stif_values[" << idx << "];\n";
  }
  ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  // Now we ge get to handle the outputs for each of the species
  {
    ost << REAL << " spec_wdot[" << num_species << "];\n";
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < " << num_species << "; i++)\n";
    ost << "  spec_wdot[i] = 0.0;\n";
    // Keep track of how many updates have been applied to each species, once the species
    // is done then we can write out its wdot
    std::map<Species*,unsigned> num_updates;
    unsigned performed_writes = 0;
    unsigned handled_reactions = 0;
    // See if there are any species which don't get touched by any reactions, if so we can emit their write
    // right now
    for (SpeciesSet::const_iterator it = unit->species.begin();
          it != unit->species.end(); it++)
    {
      if ((*it)->reaction_contributions.empty() && (strcmp((*it)->name,"M")!=0))
      {
        // Do some fancy math here to prevent the compiler from hoisting integer math and eating registers
        ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << WDOT << "+((" 
            << (*it)->code_name << "+step*" << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2)
            << ")*slice_stride), \"d\"(0.0) : \"memory\");\n";
        performed_writes++;
      }
    }
    const int num_phases = (warps[0]->reactions.size() + (reactions_per_warp-1))/reactions_per_warp;
    const int num_stages = (warps.size() + (warps_per_stage-1))/warps_per_stage;
    for (int phase_id = 0; phase_id < num_phases; phase_id++)
    {
      for (int stage_id = 0; stage_id < num_stages; stage_id++)
      {
        const int stage_warps = ((stage_id+1)*warps_per_stage > int(unit->threads_per_point)) ? 
                    unit->threads_per_point-(stage_id*warps_per_stage) : warps_per_stage;
        const int start_barrier = (stage_id+1)*2;
        const int finish_barrier = (stage_id+1)*2+1;
        ost << "asm volatile(\"bar.arrive " << start_barrier << "," << ((stage_warps+1)*32) << ";\" : : : \"memory\");\n";
        ost << "asm volatile(\"bar.sync " << finish_barrier << "," << ((stage_warps+1)*32) << ";\" : : : \"memory\");\n";
        for (int warp_off = 0; warp_off < warps_per_stage; warp_off++)
        {
          const int warp_id = stage_id*warps_per_stage + warp_off;
          if (warp_id >= int(warps.size()))
            break;
          for (int reac_off = 0; reac_off < reactions_per_warp; reac_off++)
          {
            const int reac_index = phase_id * reactions_per_warp + reac_off;
            const int location = reac_off*warps_per_stage + warp_off + second_pass_offset;
            assert(location < int(scratch_values));
            if (reac_index >= int(warps[warp_id]->reactions.size()))
              break;
            int reac_idx = warps[warp_id]->get_reaction_idx(reac_index);
            Reaction *reaction = warps[warp_id]->get_reaction(reac_idx);
            assert(reaction != NULL);
            handled_reactions++;
            ost << "// Handling reaction " << reac_idx << " from warp " << warp_id << " index " << reac_index << "\n";
            PairDelim reac_pair(ost);
            ost << REAL << " rate = scratch[" << location << "][tid];\n";
            // Go through each species in the stoichiometry and apply the updates
            for (SpeciesCoeffMap::const_iterator co_it = reaction->stoich.begin();
                  co_it != reaction->stoich.end(); co_it++)
            {
              Species *spec = co_it->first;
              // Skip any of the QSS species since we already handled them
              if (spec->qss_species)
                continue;
              // Also skip any third body species
              if (strcmp(spec->name,"M")==0)
                continue;
              ost << "spec_wdot[" << spec->code_name << "] = __fma_rn(" << double(co_it->second) 
                  << ",rate,spec_wdot[" << spec->code_name << "]);\n";
              // Update the species updates
              if (num_updates.find(spec) == num_updates.end())
                num_updates[spec] = 0;
              num_updates[spec]++;
            }
          }
        }
        // Check any of the species to see if they are finished
        for (std::set<Species*>::const_iterator it = unit->species.begin();
              it != unit->species.end(); it++)
        {
          if (strcmp((*it)->name,"M")==0)
            continue;
          if ((*it)->qss_species)
            continue;
          Species *spec = *it;
          if (num_updates.find(spec) == num_updates.end())
            continue;
          if (num_updates[spec] == spec->reaction_contributions.size())
          {
            // This species is done, multiply by the molecular mass and do the write
            // Do some fancy integer math here that reduces to shifts to prevent the compiler from hoisting integer
            // math and eating registers
            ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << WDOT << "+((" 
                << spec->code_name << "+step*" << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2)
                << ")*slice_stride), "
                << "\"d\"(molecular_mass[" << spec->code_name << "]*spec_wdot[" << spec->code_name << "]) : \"memory\");\n";
            performed_writes++;
            num_updates.erase(spec);
          }
        }
      }
    }
    assert(handled_reactions == unit->all_reactions.size());
    assert(performed_writes == num_species);
  }
  // Finally update the pointers that we need here
  {
    PairDelim update_pair(ost);
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << DIFFUSION_ARRAY << " += slice_stride;\n";
    ost << WDOT << " += slice_stride;\n";
  }
}

void ChemistryUnit::emit_first_pass_reactions(CodeOutStream &ost, Phase *phase, unsigned start_reac)
{
  for (unsigned reac_idx = start_reac; reac_idx <= phase->max_reaction; reac_idx++)
  {
    emit_phase_reaction(ost, reac_idx, false/*second pass*/, 0/*offset*/); 
  }
}

void ChemistryUnit::emit_first_pass_writes(CodeOutStream &ost, Phase *phase, unsigned start_reac)
{
  for (unsigned widx = 0; widx < warps.size(); widx++)
  {
    Warp *warp = warps[widx];
    if (widx == 0)
      ost << "if (__any(wid == 0))\n";
    else
      ost << "else if (__any(wid == " << widx << "))\n";
    PairDelim warp_pair(ost);
    // Emit the mole fraction updates to all the reaction rates computed in this phase
    emit_mole_fraction_updates(ost, warp, start_reac, phase->max_reaction, 0/*offset*/);  
    
    // Now figure out which stage this warp is in and how many other warps are in the same stage
    int stage_idx;
    int stage_warps = phase->find_warp_stage(warp, stage_idx);
    // Emit a barrier to wait to be told we can write into shared memory
    int start_barrier = (stage_idx+1)*2;
    int finish_barrier = (stage_idx+1)*2+1;
    ost << "asm volatile(\"bar.sync " << start_barrier << "," << ((stage_warps+1)*32) << ";\" : : : \"memory\");\n";
    // Now write our allocations into shared memory
    phase->emit_write_store_values(ost, warp);
    // Indicate that we've finished writing
    ost << "asm volatile(\"bar.arrive " << finish_barrier << "," << ((stage_warps+1)*32) << ";\" : : : \"memory\");\n";
  }
}

void ChemistryUnit::emit_second_pass_reactions(CodeOutStream &ost, unsigned start_reac, unsigned stop_reac,
                                                unsigned warps_per_stage, unsigned second_pass_offset)
{
  ost << "// Second pass phase for reactions " << start_reac << " to " << stop_reac << "\n"; 
  PairDelim pass_pair(ost);
#ifdef RR_INDEXING
  ost << REAL << " rr_f[" << ((stop_reac-start_reac)+1) << "];\n";
  ost << REAL << " rr_r[" << ((stop_reac-start_reac)+1) << "];\n";
#endif
  for (unsigned reac = start_reac; reac <= stop_reac; reac++)
  {
    emit_phase_reaction(ost, reac, true/*second pass*/, start_reac);
  }
  // If this is our first phase, then we need to wait for the stiff values to be ready
  if (start_reac == 0)
    ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  // Now emit the statements for each of the warps to scale their values by the right mole fractions
  // and QSSA and stif scaling factors
  for (unsigned widx = 0; widx < warps.size(); widx++)
  {
    Warp *warp = warps[widx];
    if (widx == 0)
      ost << "if (__any(wid == 0))\n";
    else
      ost << "else if (__any(wid == " << widx << "))\n";
    PairDelim warp_pair(ost);
    // Emit the mole fraction updates to all the reaction rates computed in this phase
    emit_mole_fraction_updates(ost, warp, start_reac, stop_reac, start_reac/*offset*/);  
    // Also for each reaction figure out if it needs any scaling
    std::vector<int> qss_locations;
    warp->compute_needed_xqs(connected_components, qss_locations);
    for (unsigned reac = start_reac; reac <= stop_reac; reac++)
    {
      if (reac < warp->reaction_order.size())
      {
        int qss_forward_update = -1;
        int qss_backward_update = -1;
        int reac_idx = warp->get_reaction_idx(reac);
        for (std::vector<ConnectedComponent*>::const_iterator cc = connected_components.begin();
               cc != connected_components.end(); cc++)
        {
          for (std::map<int,QSS*>::const_iterator spec_it = (*cc)->species.begin();
                spec_it != (*cc)->species.end(); spec_it++)
          {
            if (spec_it->second->forward_corrections.find(reac_idx) != spec_it->second->forward_corrections.end())
            {
              assert(qss_forward_update = -1);
              qss_forward_update = spec_it->second->qss_idx;
            }
            if (spec_it->second->backward_corrections.find(reac_idx) != spec_it->second->backward_corrections.end())
            {
              assert(qss_backward_update = -1);
              qss_backward_update = spec_it->second->qss_idx;
            }
          }
        }
        if (qss_forward_update != -1)
        {
          // find the index in the list of qss_locations
          int index = -1;
          for (unsigned idx = 0; idx < qss_locations.size(); idx++)
          {
            if (qss_locations[idx] == qss_forward_update)
            {
              index = idx;
              break;
            }
          }
          assert(index != -1);
#ifdef RR_INDEXING
          ost << "rr_f[" << (reac-start_reac) << "] *= xq_values[" << index << "];\n";
#else
          ost << "rr_f_" << (reac-start_reac) << " *= xq_values[" << index << "];\n";
#endif
        }
        if (qss_backward_update != -1)
        {
          // find the index in the list of qss_locations
          int index = -1;
          for (unsigned idx = 0; idx < qss_locations.size(); idx++)
          {
            if (qss_locations[idx] == qss_backward_update)
            {
              index = idx;
              break;
            }
          }
          assert(index != -1);
#ifdef RR_INDEXING
          ost << "rr_r[" << (reac-start_reac) << "] *= xq_values[" << index << "];\n";
#else
          ost << "rr_r_" << (reac-start_reac) << " *= xq_values[" << index << "];\n";
#endif
        }
        // Now look for stiffness updates
        for (unsigned idx = 0; idx < stif_operations.size(); idx++)
        {
          std::map<int,int>::const_iterator finder = stif_operations[idx]->forward_d.find(reac_idx);
          if (finder != stif_operations[idx]->forward_d.end())
          {
            for (int i = 0; i < finder->second; i++)
#ifdef RR_INDEXING
              ost << "rr_f[" << (reac-start_reac) << "] *= scratch[" << idx << "][tid];\n";
#else
              ost << "rr_f_" << (reac-start_reac) << " *= scratch[" << idx << "][tid];\n";
#endif
          }
          finder = stif_operations[idx]->backward_d.find(reac_idx);
          if (finder != stif_operations[idx]->backward_d.end())
          {
            for (int i = 0; i < finder->second; i++)
#ifdef RR_INDEXING
              ost << "rr_r[" << (reac-start_reac) << "] *= scratch[" << idx << "][tid];\n";
#else
              ost << "rr_r_" << (reac-start_reac) << " *= scratch[" << idx << "][tid];\n";
#endif
          }
        }
      }
    }
    int stage_idx = widx/warps_per_stage; 
    int stage_warps = ((stage_idx+1)*warps_per_stage > unit->threads_per_point) ? 
                    unit->threads_per_point-(stage_idx*warps_per_stage) : warps_per_stage;
    int start_barrier = (stage_idx+1)*2;
    int finish_barrier = (stage_idx+1)*2+1;
    ost << "asm volatile(\"bar.sync " << start_barrier << "," << ((stage_warps+1)*32) << ";\" : : : \"memory\");\n";
   for (unsigned reac = start_reac; reac <= stop_reac; reac++)
    {
      if (reac < warp->reaction_order.size())
      {
        const int location = warps_per_stage*(reac-start_reac) + (widx%warps_per_stage) + second_pass_offset;
#ifdef RR_INDEXING
        ost << "scratch[" << location 
            << "][tid] = rr_f[" << (reac-start_reac) << "] - rr_r[" << (reac-start_reac) << "];\n";
#else
        ost << "scratch[" << location
            << "][tid] = rr_f_" << (reac-start_reac) << " - rr_r_" << (reac-start_reac) << ";\n";
#endif
      }
    }
    // Indicate that we've finished writing
    ost << "asm volatile(\"bar.arrive " << finish_barrier << "," << ((stage_warps+1)*32) << ";\" : : : \"memory\");\n";
  }
}

void ChemistryUnit::emit_bulk_getrates_code(CodeOutStream &ost, unsigned num_species, const char *temp_file_name, 
                                            const char *fun_file_name, unsigned micro_warps, unsigned phases)
{
  // Assume we're doing two phases right now
  assert(phases == 2);
  assert((32%micro_warps) == 0);
  // Emit the constant declarations
  unsigned constants_per_thread;
  unsigned warp_constant_stride;
  emit_constant_declarations(ost, num_species, constants_per_thread, warp_constant_stride, 32/micro_warps);

  // Copy over any functions from the function file
  {
    // Then copy over the code from the temporary file
    FILE *temp_file = fopen(fun_file_name,"r");
    assert(temp_file != NULL);
    char c = fgetc(temp_file);
    while (c != EOF)
    {
      ost << c;
      c = fgetc(temp_file);
    }
    assert(fclose(temp_file) == 0);
  }
  ost << "\n";
  ost << "__global__ void\n";
  ost << "gpu_getrates(";
  ost << "const " << REAL << " *" << PRESSURE_ARRAY;
  ost << ", const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
  ost << ", const " << REAL << " *" << DIFFUSION_ARRAY;
  ost << ", const " << REAL << " recip_dt/*reciprocal of dt*/";
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << WDOT;
  ost << ")\n";
  PairDelim pair(ost);
  switch (micro_warps)
  {
    case 1:
      {
        ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
        ost << "const " << INT << " wid = threadIdx.x >> 5;\n";
        break;
      }
    case 2:
      {
        ost << "const " << INT << " tid = threadIdx.x & 0xf;\n";
        ost << "const " << INT << " wid = threadIdx.x >> 4;\n";
        break;
      }
    case 4:
      {
        ost << "const " << INT << " tid = threadIdx.x & 0x7;\n";
        ost << "const " << INT << " wid = threadIdx.x >> 3;\n";
        break;
      }
    case 8:
      {
        ost << "const " << INT << " tid = threadIdx.x & 0x3;\n";
        ost << "const " << INT << " wid = threadIdx.x >> 2;\n";
        break;
      }
    case 16:
      {
        ost << "const " << INT << " tid = threadIdx.x & 0x1;\n";
        ost << "const " << INT << " wid = threadIdx.x >> 1;\n";
        break;
      }
    default:
      assert(false); // unsupported number of micro warps
  }
#ifdef INSTR_INIT
  ost << INSTR_INIT;
#endif
  // Compute the offset for this thread
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*row_stride) + "
        << "(blockIdx.x*" << (2*POINTS_PER_CTA) << " + tid);\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << DIFFUSION_ARRAY << " += offset;\n";
    ost << WDOT << " += offset;\n";
  }
  // Declare the shared memory arrays
  unsigned total_shared_usage = 0;
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "_0[" << num_species << "][" << POINTS_PER_CTA << "];\n";
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "_1[" << num_species << "][" << POINTS_PER_CTA << "];\n";
  total_shared_usage += (2*num_species*POINTS_PER_CTA*sizeof(REAL_TYPE));
  ost << "volatile __shared__ " << REAL << " rr_f[" << unit->all_reactions.size() << "][" << POINTS_PER_CTA << "];\n";
  ost << "volatile __shared__ " << REAL << " rr_r[" << unit->all_reactions.size() << "][" << POINTS_PER_CTA << "];\n";
  total_shared_usage += (2*unit->all_reactions.size()*POINTS_PER_CTA*sizeof(REAL_TYPE));
  if (!unit->third_bodies.empty())
  {
    ost << "volatile __shared__ " << REAL << " " << THIRD_BODY << "_0[" << POINTS_PER_CTA << "];\n";
    ost << "volatile __shared__ " << REAL << " " << THIRD_BODY << "_1[" << POINTS_PER_CTA << "];\n";
    total_shared_usage += (2*POINTS_PER_CTA*sizeof(REAL_TYPE));
  }
  // Figure out how many qssa values we need
  {
    unsigned total_qssa_intermediates = 0;
    unsigned total_qssa_species = 0;
    for (unsigned idx = 0; idx < connected_components.size(); idx++)
    {
      ConnectedComponent *cc = connected_components[idx]; 
      for (std::map<int,QSS*>::const_iterator qit = cc->species.begin();
            qit != cc->species.end(); qit++)
      {
        total_qssa_species++;
        // Add 1 for the zero element
        total_qssa_intermediates++;
        std::set<int> other_elements;
        for (std::map<std::pair<int,int>,int>::const_iterator it = qit->second->forward_contributions.begin();
              it != qit->second->forward_contributions.end(); it++)
        {
          other_elements.insert(it->first.first);
        }
        for (std::map<std::pair<int,int>,int>::const_iterator it = qit->second->backward_contributions.begin();
              it != qit->second->backward_contributions.end(); it++)
        {
          other_elements.insert(it->first.first);
        }
        total_qssa_intermediates += other_elements.size();
      }
    }
    // Use the maximum of the two values
    ost << "volatile __shared__ " << REAL << " qssa_values[" 
        << (total_qssa_intermediates > total_qssa_species ? total_qssa_intermediates : total_qssa_species)
        << "][" << POINTS_PER_CTA << "];\n";
    total_shared_usage += ((total_qssa_intermediates > total_qssa_species ? total_qssa_intermediates : total_qssa_species)
                            * POINTS_PER_CTA * sizeof(REAL_TYPE));
  }
  ost << "volatile __shared__ " << REAL << " " << DIFFUSION << "_0[" << stif_operations.size() << "][" << POINTS_PER_CTA << "];\n";
  ost << "volatile __shared__ " << REAL << " " << DIFFUSION << "_1[" << stif_operations.size() << "][" << POINTS_PER_CTA << "];\n";
  total_shared_usage += (2*stif_operations.size()*POINTS_PER_CTA*sizeof(REAL_TYPE));
  fprintf(stdout,"  INFO: Total shared memory usage for getrates: %d bytes\n", total_shared_usage);
  ost << "if (__any(wid < " << (unit->threads_per_point*micro_warps) << "))\n";
  {
    PairDelim experiment_pair(ost);
    ost << REAL << " reaction_constants[" << constants_per_thread << "];\n";
    // emit the loads for the reaction constants
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char src_offset[128];
      assert((32 % micro_warps) == 0);
      sprintf(src_offset,"wid*%d+%d",warp_constant_stride,(idx*32/micro_warps));
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      emit_cuda_load(ost,"reaction_constants",dst_offset,"warp_constants",src_offset,false/*double2*/,".cg");
    }
    // Then copy over the code from the temporary file
    FILE *temp_file = fopen(temp_file_name,"r");
    assert(temp_file != NULL);
    char c = fgetc(temp_file);
    while (c != EOF)
    {
      ost << c;
      c = fgetc(temp_file);
    }
    assert(fclose(temp_file) == 0);
  }
  for (unsigned idx = 0; idx < phases; idx++)
  {
    if (idx < (phases-1))
      ost << "else if (__any(wid < " << ((unit->threads_per_point+(idx+1))*micro_warps) << "))\n";
    else
      ost << "else\n";
    PairDelim load_pair(ost);
    emit_bulk_loading_code(ost, num_species, micro_warps, idx/*offset*/, phases);
  }
}

void ChemistryUnit::emit_bulk_experiment_code(CodeOutStream &ost, CodeOutStream &fun_ost, unsigned num_species, unsigned micro_warps,
                                              unsigned offset, unsigned phases)
{
  const int start_spec_barrier = offset*4 + 0 + 2;
  const int finish_spec_barrier = offset*4 + 1 + 2;
  const int start_diff_barrier = offset*4 + 2 + 2;
  const int finish_diff_barrier = offset*4 + 3 + 2;
  ost << "// Phase " << offset << "\n"; 
  PairDelim phase_pair(ost);
  // First emit a load for our temperature
  ost << REAL << " " << TEMPERATURE << ";\n";
  if (offset == 0)
    emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,false/*double2*/,".cg");
  else
  {
    char src_offset[128];
    sprintf(src_offset,"%d",(offset*POINTS_PER_CTA));
    emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,src_offset,false/*double2*/,".cg");
  }
  unit->emit_numerical_constants(ost);
  ost << "\n";
  unit->emit_temperature_dependent(ost);
  ost << "\n";
  
  // Now emit the reaction contributions
  {
    const int max_reactions = warps[0]->reactions.size();
    unsigned start_offset = 0;
    for (int reac_index = 0; reac_index < max_reactions; reac_index++)
    {
      // Inside of here the first reaction will wait for the mole fractions
      // to be ready before continuing
      start_offset = emit_bulk_reaction(ost, fun_ost, reac_index, finish_spec_barrier, micro_warps, offset, start_offset); 
    }
  }
  // We're done reading the mole fractions, so tell the producer warp that they 
  // can start writing the next set of values
  ost << "asm volatile(\"bar.arrive " << start_spec_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n"; 

  // Now we need to barrier to make sure we have all the reaction rates present before
  // we can start computing the QSSA values
  ost << "asm volatile(\"bar.sync 1," << (unit->threads_per_point*32) << ";\" : : : \"memory\");\n";
  emit_bulk_qssa_code(ost, micro_warps);

  // Wait until everyone is done with the QSSA before starting stiffness
  // Also wait until all the diffusion values are present.  We can accomplish this
  // with a single barrier on the diffusion start condition since all the reaction warps
  // have to hit it before we can continue
  ost << "asm volatile(\"bar.sync " << finish_diff_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  emit_bulk_stiffness_code(ost, micro_warps);
  // Indicate that we are done reading the stiffness values
  ost << "asm volatile(\"bar.arrive " << start_diff_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";

  // Wait until all the reaction rates have been updated with their stiffness scaling
  ost << "asm volatile(\"bar.sync 1," << (unit->threads_per_point*32) << ";\" : : : \"memory\");\n";
  
  // Now emit the code to do the gather for each species and write up the result
  emit_bulk_species_output(ost, num_species, micro_warps, offset, phases);
}

void ChemistryUnit::emit_bulk_loading_code(CodeOutStream &ost, unsigned num_species, unsigned micro_warps,
                                            unsigned offset, unsigned phases)
{
  const bool needs_third_body = !unit->third_bodies.empty();
  // Figure out what are barrier id numbers are
  const int start_spec_barrier = offset*4 + 0 + 2;
  const int finish_spec_barrier = offset*4 + 1 + 2;
  const int start_diff_barrier = offset*4 + 2 + 2;
  const int finish_diff_barrier = offset*4 + 3 + 2;
  const int upper_bound = next_largest_power(2*unit->threads_per_point*micro_warps,2);
  unsigned log_upper_bound = 0;
  {
    unsigned value = upper_bound;
    while (value > 1)
    {
      value /= 2;
      log_upper_bound++;
    }
  }
  unsigned mod_mask = 0;
  for (unsigned i = 0; i < log_upper_bound; i++)
  {
    mod_mask |= (1 << i);
  }
  unsigned log_micro_warps = 0;
  {
    unsigned value = micro_warps;
    while (value > 1)
    {
      value /= 2;
      log_micro_warps++;
    }
  }
  unsigned mod_micro_mask = 0;
  for (unsigned i = 0; i < log_micro_warps; i++)
    mod_micro_mask |= (1 << i);
  // Emit updates for the strides if necessary
  // to account for our offset
  if (offset > 0)
  {
    ost << MASS_FRAC_ARRAY << " += " << (offset*POINTS_PER_CTA) << ";\n";
    ost << PRESSURE_ARRAY << " += " << (offset*POINTS_PER_CTA) << ";\n";
    ost << DIFFUSION_ARRAY << " += " << (offset*POINTS_PER_CTA) << ";\n";
    ost << PRESSURE_ARRAY << " += " << (offset*POINTS_PER_CTA) << ";\n";
  }
  // Num species
  const int species_per_thread = (num_species + (micro_warps-1))/micro_warps;
  const bool species_uniform = ((num_species % micro_warps) == 0);

  const int diffusion_per_thread = (stif_operations.size() + (micro_warps-1))/micro_warps;
  const bool diffusion_uniform = ((stif_operations.size() % micro_warps) == 0);

  ost << REAL << " mass_frac[" << species_per_thread << "];\n";
  ost << REAL << " diff_temp[" << diffusion_per_thread << "];\n";

  ost << "for (int step = 0; step < total_steps; step++)\n";
  {
  PairDelim step_loop_pair(ost);

  ost << "const " << INT << " offset = ((wid & " << mod_micro_mask << ") + step*" << upper_bound << ")%" << upper_bound << ";\n";
  // Issue a load for our temperature
  ost << REAL << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,false/*double2*/,".cg");

  // Issue loads for our species
  for (int idx = 0; idx < species_per_thread; idx++)
  {
    if (!species_uniform && (idx == (species_per_thread-1)))
      ost << "if ((wid+" << (idx*micro_warps) << ") < " << num_species << ")\n";
    char src_offset[128];
    sprintf(src_offset,"(offset+%d)*spec_stride",(idx*micro_warps));
    //sprintf(src_offset,"((wid+%d+%d*step)%%%d)*spec_stride",(idx*micro_warps),
    //    upper_bound,upper_bound);
    char dst_offset[128];
    sprintf(dst_offset,"%d",idx);
    emit_cuda_load(ost,"mass_frac",dst_offset,MASS_FRAC_ARRAY,src_offset,false/*double2*/,".cg");
  }

  // Issue loads for pressure
  ost << REAL << " " << PRESSURE << ";\n";
  emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,false/*double2*/,".cg");

  // Now compute the values of sumyow
  ost << REAL << " local_sumyow = 0.0;\n";
  for (int idx = 0; idx < species_per_thread; idx++)
  {
    if (!species_uniform && (idx == (species_per_thread-1)))
      ost << "if ((wid+" << (idx*micro_warps) << ") < " << num_species << ")\n";
    PairDelim spec_pair(ost);
    //ost << REAL << " mole = mass_frac[" << idx << "] * recip_molecular_mass[(wid+" << (idx*micro_warps) 
    //    << "+step*" << upper_bound << ") % " << upper_bound << "];\n";
    ost << REAL << " mole = mass_frac[" << idx << "] * recip_molecular_mass[offset+" << (idx*micro_warps) << "];\n";
    ost << "local_sumyow = __fma_rn(mole," << TEMPERATURE << ",local_sumyow);\n";
  }
  // Now gather our sumyow from the other micro-warps in this warp
  ost << REAL << " sumyow = 0.0;\n";
  {
    PairDelim sumyow_pair(ost);
    ost << INT << " hi_part, lo_part;\n";
    for (unsigned idx = 0; idx < micro_warps; idx++)
    {
      const char *mask = NULL;
      switch (micro_warps)
      {
        case 4:
          {
            switch (idx)
            {
              case 0:
                mask = "0x0";
                break;
              case 1:
                mask = "0x8";
                break;
              case 2:
                mask = "0x10";
                break;
              case 3:
                mask = "0x18";
                break;
              default:
                assert(false); // should never happen
            }
            break;
          }
        default:
          assert(false); // TODO; handle other cases
      }
      ost << "lo_part = __shfl_xor(__double2hiint(local_sumyow)," << mask << ");\n";
      ost << "hi_part = __shfl_xor(__double2loint(local_sumyow)," << mask << ");\n";
      ost << "sumyow += __hiloint2double(hi_part,lo_part);\n";
    }
  }
  // We've reduced the values of the sumyow value, now compute the rest of it
  ost << "sumyow *= (";
  if (unit->no_nondim)
    ost << "8.314510e+07 * " << TEMPERATURE;
  else
    ost << (8.314510e+07 * TEMPERATURE_REF / PRESSURE_REF) << " * " << TEMPERATURE;
  ost << ");\n";
  ost << "sumyow = " << PRESSURE << "/sumyow;\n";
  if (needs_third_body)
    ost << REAL << " local_ctot = 0.0;\n";
  for (int idx = 0; idx < species_per_thread; idx++)
  {
    if (!species_uniform && (idx == (species_per_thread-1)))
      ost << "if ((wid+" << (idx*micro_warps) << ") < " << num_species << ")\n";
    PairDelim spec_pair(ost);
    ost << "mass_frac[" << idx << "] *= (sumyow * recip_molecular_mass[offset+" << (idx*micro_warps) << "]);\n";
    //ost << "mass_frac[" << idx << "] *= (sumyow * recip_molecular_mass[(wid+" << (idx*micro_warps) 
    //    << "+step*" << upper_bound << ") % " << upper_bound << "]);\n";
    if (needs_third_body)
      ost << "local_ctot += mass_frac[" << idx << "];\n";
  }
  if (needs_third_body)
  {
    ost << REAL << " ctot = 0.0;\n";
    PairDelim ctot_pair(ost);
    ost << INT << " hi_part, lo_part;\n";
    for (unsigned idx = 0; idx < micro_warps; idx++)
    {
      const char *mask = NULL;
      switch (micro_warps)
      {
        case 4:
          {
            switch (idx)
            {
              case 0:
                mask = "0x0";
                break;
              case 1:
                mask = "0x8";
                break;
              case 2:
                mask = "0x10";
                break;
              case 3:
                mask = "0x18";
                break;
              default:
                assert(false); // should never happen
            }
            break;
          }
        default:
          assert(false); // TODO; handle other cases
      }
      ost << "lo_part = __shfl_xor(__double2hiint(local_ctot)," << mask << ");\n";
      ost << "hi_part = __shfl_xor(__double2loint(local_ctot)," << mask << ");\n";
      ost << "ctot += __hiloint2double(hi_part,lo_part);\n";
    }
  }
  // Wait to be told it is safe to write into shared memory
  ost << "asm volatile(\"bar.sync " << start_spec_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  // Write the values into shared
  for (int idx = 0; idx < species_per_thread; idx++)
  {
    if (!species_uniform && (idx == (species_per_thread-1)))
      ost << "if (((wid&" << mod_micro_mask << ")+" << (idx*micro_warps) << ") < " << num_species << ")\n";
    ost << MOLE_FRAC << "_" << offset << "[(wid&" << mod_micro_mask << ")+" << (idx*micro_warps) << "][tid] = mass_frac[" << idx << "];\n";
  }
  if (needs_third_body)
  {
    // Assuming micro warps is a power of 2 here
    unsigned log_micro_warps = 0;
    unsigned value = micro_warps;
    while (value > 1)
    {
      value /= 2;
      log_micro_warps++;
    }
    ost << "if ((wid & " << mod_micro_mask << ") == 0)\n";  
    ost << "  " << THIRD_BODY << "_" << offset << "[tid] = ctot;\n";
  }
  // Indicate that we're done writing
  ost << "asm volatile(\"bar.arrive " << finish_spec_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";

  // Now we get to load the diffusion values
  for (int idx = 0; idx < diffusion_per_thread; idx++)
  {
    if (!diffusion_uniform && (idx == (diffusion_per_thread-1)))
      ost << "if (((wid&" << mod_micro_mask << ")+" << (idx*micro_warps) << ") < " << stif_operations.size() << ")\n";
    char src_offset[128];
    sprintf(src_offset,"(diffusion_specs[offset+%d])*spec_stride", (idx*micro_warps));
    //sprintf(src_offset,"(diffusion_specs[(wid+%d+step*%d)%%%d])*spec_stride",(idx*micro_warps),
    //sprintf(src_offset,"((wid+%d+step*%d)%%%d)*spec_stride",(idx*micro_warps),
    //    upper_bound, upper_bound);
    char dst_offset[128];
    sprintf(dst_offset,"%d",idx);
    emit_cuda_load(ost,"diff_temp",dst_offset,DIFFUSION_ARRAY,src_offset,false/*double2*/,".cg");
  }
  
  // Wait to be told it is safe to write into shared memory
  ost << "asm volatile(\"bar.sync " << start_diff_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  // Write the values into shared memory
  for (int idx = 0; idx < diffusion_per_thread; idx++)
  {
    if (!diffusion_uniform && (idx == (diffusion_per_thread-1)))
      ost << "if (((wid&" << mod_micro_mask << ")+" << (idx*micro_warps) << ") < " << stif_operations.size() << ")\n";
    ost << DIFFUSION << "_" << offset << "[(wid&" << mod_micro_mask << ")+" << (idx*micro_warps) << "][tid] = diff_temp[" << idx << "];\n";
  }
  // Mark that we are done writing
  ost << "asm volatile(\"bar.arrive " << finish_diff_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";

  // Finally update our pointers for the next round
  ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
  ost << PRESSURE_ARRAY << " += slice_stride;\n";
  ost << DIFFUSION_ARRAY << " += slice_stride;\n";
  ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
  }
  // After we're done, emit two more barriers to match up with
  // the extra barriers issued by the reaction warps
  ost << "asm volatile(\"bar.sync " << start_spec_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
  ost << "asm volatile(\"bar.sync " << start_diff_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
}

unsigned ChemistryUnit::emit_bulk_reaction(CodeOutStream &ost, CodeOutStream &fun_ost, 
                                           int reac_index, int start_spec_barrier, unsigned micro_warps,
                                           unsigned offset, unsigned start_offset)
{
  // Get the set of reactions that we need to do for this index
  bool has_weird_reaction = false;
  std::vector<Reaction*> needed_reactions;
  std::vector<unsigned> warp_index;
  for (unsigned idx = 0; idx < warps.size(); idx++)
  {
    // Skip any warps for which we have exceeded the number of reactions that they own
    if (reac_index >= int(warps[idx]->reaction_order.size()))
      continue;
    int reac_idx = warps[idx]->get_reaction_idx(reac_index);
    Reaction *reac = warps[idx]->get_reaction(reac_idx);
    assert(reac != NULL);
    needed_reactions.push_back(reac);
    warp_index.push_back(idx);
    if (is_weird_reaction(reac))
    {
      // Check the invariant that all the other micro-warps in the same warp
      // have the same reaction and then skip ahead
      for (unsigned i = 1; i < micro_warps; i++)
      {
        int other_idx = warps[idx+i]->get_reaction_idx(reac_index);
        Reaction *other_reac = warps[idx+i]->get_reaction(other_idx);
        assert(other_reac != NULL);
        assert(other_reac == reac);
      }
      idx += (micro_warps-1);
      has_weird_reaction = true;
    }
  }
  ost << "// Reaction index " << reac_index << "\n";
  ost << "/* Reactions:";
  for (std::vector<Reaction*>::const_iterator it = needed_reactions.begin();
        it != needed_reactions.end(); it++)
  {
    ost << " " << (*it)->get_global_idx();
  }
  ost << "*/\n";
  if (has_weird_reaction)
  {
    ost << "// Has differing reactions\n";
    emit_bulk_weird_reaction(ost, fun_ost, needed_reactions, warp_index, reac_index, start_spec_barrier, micro_warps, offset, start_offset);
  }
  else
  {
    ost << "// All regular reactions\n";
    emit_bulk_normal_reaction(ost, fun_ost, needed_reactions, warp_index, reac_index, start_spec_barrier, micro_warps, offset, start_offset); 
  }
  return (start_offset + needed_reactions.size());
}

void ChemistryUnit::emit_bulk_qssa_code(CodeOutStream &ost, unsigned micro_warps)
{

}

void ChemistryUnit::emit_bulk_stiffness_code(CodeOutStream &ost, unsigned micro_warps)
{

}

void ChemistryUnit::emit_bulk_species_output(CodeOutStream &ost, unsigned num_species, unsigned micro_warps, unsigned offset, unsigned phases)
{
  // First get the set of species we need to do outputs for
  std::vector<Species*> output_species;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if ((*it)->qss_species)
      continue;
    output_species.push_back(*it);
  }
  unsigned log_micro_warps = 0;
  {
    unsigned value = micro_warps;
    while (value > 1)
    {
      value /= 2;
      log_micro_warps++;
    }
  }
  unsigned mod_micro_mask = 0;
  for (unsigned i = 0; i < log_micro_warps; i++)
    mod_micro_mask |= (1 << i);
  // Do this at the granularity of warps so that we can avoid divergence problems
  // There should be plenty of warps for this anyway since we're iterating over species
  // and not over reactions
  for (unsigned widx = 0; widx < unit->threads_per_point; widx++)
  {
    ost << "// Species output for warp " << widx << "\n";
    if (widx == 0)
      ost << "if (__any(wid < " << micro_warps << "))\n";
    else
      ost << "else if (__any(wid < " << ((widx+1)*micro_warps) << "))\n";
    PairDelim warp_pair(ost); 
    for (unsigned spec_idx = widx; spec_idx < output_species.size(); spec_idx += unit->threads_per_point)
    {
      Species *spec = output_species[spec_idx];
      ost << "// Species " << spec->name << "\n";
      PairDelim spec_pair(ost);
      ost << REAL << " wdot_" << spec->code_name << " = 0.0;\n"; 
      unsigned num_loads_in_flight = 0;
      for (std::map<Reaction*,int/*coeff*/>::const_iterator it = spec->reaction_contributions.begin();
            it != spec->reaction_contributions.end(); it++)
      {
        assert(reaction_map.find(it->first) != reaction_map.end());  
        assert(it->second != 0);
        int index = reaction_map[it->first];
        assert(index < int(unit->all_reactions.size()));
        ost << REAL << " rr_" << index << " = rr_f[" << index << "][tid] - rr_r[" << index << "][tid];\n";
        ost << "wdot_" << spec->code_name;
        if (it->second == 1)
          ost << " += rr_" << index << ";\n";
        else if (it->second == -1)
          ost << " -= rr_" << index << ";\n";
        else
          ost << " = __fma_rn(" << double(it->second) << ",rr_" << index << ",wdot_" << spec->code_name << ");\n";
        num_loads_in_flight++;
        if ((num_loads_in_flight%MAX_SHARED_LOADS) == 0)
          ost << INSTR_LIMITER;
      }
      if (!unit->no_nondim)
        ost << "wdot_" << spec->code_name << " *= " << (spec->molecular_mass/1000.0) << ";\n";
      // Only have the first micro-warp in each warp do the write
      ost << "if ((wid & " << mod_micro_mask << ") == 0)\n";
      {
        PairDelim if_pair(ost);
        ost << "asm volatile(\"st.global.cg.f64 [%0], %1;\" : : \"l\"(" << WDOT << "+"
            << spec->code_name << "*spec_stride";
        if (offset > 0)
          ost << "+" << (offset*POINTS_PER_CTA);
        ost << "), \"d\"(wdot_" << spec->code_name << ") : \"memory\");\n";
      }
    }
  }
}

void ChemistryUnit::emit_bulk_normal_reaction(CodeOutStream &ost, CodeOutStream &fun_ost, const std::vector<Reaction*> &needed_reactions,
                                              const std::vector<unsigned> &warp_index, unsigned reac_index, unsigned start_spec_barrier,
                                              unsigned micro_warps, unsigned offset, unsigned start_offset)
{
  assert(offset < 2); // haven't handled multiple buffer cases in arrhenius yet
  assert(needed_reactions.size() == warp_index.size());
  assert(needed_reactions.size() <= warps.size());
  assert((32%micro_warps) == 0);
  PairDelim normal_pair(ost);
  ost << REAL << " rr_forward, rr_backward;\n";
  // Forward rate first
  bool first = true;
  for (unsigned idx = 0; idx < warp_index.size(); idx++)
  {
    Reaction *reac = needed_reactions[idx];
    Warp *warp = warps[warp_index[idx]];
    if (!unit->no_nondim)
      arrhenius(ost, warp, first, false/*weird*/, reac_index, "rr_forward",
                (reac->a*REACTION_RATE_REF), reac->beta, reac->e, (offset > 0), 32/micro_warps);
    else
      arrhenius(ost, warp, first, false/*weird*/, reac_index, "rr_forward",
                reac->a, reac->beta, reac->e, (offset > 0), 32/micro_warps);
    first = false;
  }
  // Now do the reverse rate
  first = true;
  for (unsigned idx = 0; idx < warp_index.size(); idx++)
  {
    Reaction *reac = needed_reactions[idx];
    Warp *warp = warps[warp_index[idx]];
    // Not handling this case right now
    assert(!reac->duplicate.enabled);
    if (!unit->no_nondim)
      arrhenius(ost, warp, first, false/*weird*/, reac_index, "rr_backward",
                (reac->rev.a*REACTION_RATE_REF), reac->rev.beta, reac->rev.e, (offset > 0), 32/micro_warps);
    else
      arrhenius(ost, warp, first, false/*weird*/, reac_index, "rr_backward",
                reac->rev.a, reac->rev.beta, reac->rev.e, (offset > 0), 32/micro_warps);
    first = false;
  }
  // If this is the first reaction index, we need to wait for the mole fractions to be ready
  // so emit the barrier call
  if (reac_index == 0)
    ost << "asm volatile(\"bar.sync " << start_spec_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";

  // Now we can emit the code to do all the updates
  for (unsigned idx = 0; idx < warp_index.size(); idx+=micro_warps)
  {
    // First figure out the set of species needed for each reaction
    std::vector<std::set<Species*> > needed_mole_fracs(micro_warps);
    std::vector<unsigned> num_forward(micro_warps);
    std::vector<unsigned> num_backward(micro_warps);
    for (unsigned i = 0; i < micro_warps; i++)
    {
      num_forward[i] = 0;
      num_backward[i] = 0;
      if ((idx+i) >= warp_index.size())
        continue;
      Reaction *reac = needed_reactions[idx+i];
      const bool unimportant_f = (forward_unimportant.find(reac->get_global_idx()) != forward_unimportant.end());
      const bool unimportant_r = (backward_unimportant.find(reac->get_global_idx()) != backward_unimportant.end());
      if (!unimportant_f)
      {
        num_forward[i] = reac->forward.size();
        for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
              it != reac->forward.end(); it++)
        {
          if (!it->first->qss_species)
            needed_mole_fracs[i].insert(it->first);
        }
      }
      if (!unimportant_r && reac->reversible)
      {
        num_backward[i] = reac->backward.size();
        for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
              it != reac->backward.end(); it++)
        {
          if (!it->first->qss_species)
            needed_mole_fracs[i].insert(it->first);
        }
      }
      if (!unimportant_f || !unimportant_r)
      {
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          if (!unimportant_f)
            num_forward[i]++;
          if (!unimportant_r)
            num_backward[i]++;
          for (std::map<Species*,double>::const_iterator it = reac->thb->components.begin();
                it != reac->thb->components.end(); it++)
          {
            if (!it->first->qss_species)
              needed_mole_fracs[i].insert(it->first);
          }
        }
      }
    }
    unsigned max_forward = 0;
    unsigned max_backward = 0;
    for (unsigned i = 0; i < micro_warps; i++)
    {
      if (num_forward[i] > max_forward)
        max_forward = num_forward[i];
      if (num_backward[i] > max_backward)
        max_backward = num_backward[i];
    }
    // Now we can emit the code for handling the updates
    if (idx == 0)
      ost << "if (__any(wid < " << (warp_index[idx]+micro_warps) << "))\n";
    else
      ost << "else if (__any(wid < " << (warp_index[idx]+micro_warps) << "))\n";
    {
      PairDelim function_pair(ost);
      ost << "reaction_rate_" << warp_index[idx] << "_" << reac_index << "(wid, tid"
          << ", " << MOLE_FRAC << "_" << offset
          << ", " << THIRD_BODY << "_" << offset
          << ", rr_f, rr_r, rr_forward, rr_backward);\n";
    }
    if (offset == 0)
    {
      assert((max_forward > 0) || (max_backward > 0));
      fun_ost << "\n";
      fun_ost << "__device__ __forceinline__\n";
      fun_ost << "void reaction_rate_" << warp_index[idx] << "_" << reac_index << "("
              << "const int wid"
              << ", const int tid"
              << ", const volatile " << REAL << " " << MOLE_FRAC << "[NUM_SPECIES][" << POINTS_PER_CTA << "]"
              << ", const volatile " << REAL << " " << THIRD_BODY << "[" << POINTS_PER_CTA << "]"
              << ", volatile " << REAL << " rr_f[" << (unit->all_reactions.size()) << "][" << POINTS_PER_CTA << "]"
              << ", volatile " << REAL << " rr_r[" << (unit->all_reactions.size()) << "][" << POINTS_PER_CTA << "]"
              << ", " << REAL << " rr_forward"
              << ", " << REAL << " rr_backward"
              << ")\n";
      PairDelim case_pair(fun_ost);
      if (max_forward > 0)
        fun_ost << REAL << " forward_factor[" << max_forward << "];\n";
      if (max_backward > 0)
        fun_ost << REAL << " backward_factor[" << max_backward << "];\n";
      for (unsigned i = 0; i < micro_warps; i++)
      {
        if ((idx+i) >= warp_index.size())
          continue;
        if (i == 0)
          fun_ost << "if (wid == " << (warp_index[idx]+i) << ")\n";
        else
          fun_ost << "else if (wid == " << (warp_index[idx]+i) << ")\n";
        Reaction *reac = needed_reactions[idx+i];
        assert(reac != NULL);
        PairDelim reac_pair(fun_ost);
        for (std::set<Species*>::const_iterator it = needed_mole_fracs[i].begin();
              it != needed_mole_fracs[i].end(); it++)
        {
          fun_ost << REAL << " mole_frac_" << (*it)->code_name << " = " << MOLE_FRAC 
              /*<< "_" << offset*/ << "[" << (*it)->code_name << "][tid];\n";
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          fun_ost << REAL << " thb = " << THIRD_BODY << /*"_" << offset <<*/ "[tid]";
          for (std::map<Species*,double>::const_iterator it = reac->thb->components.begin();
                it != reac->thb->components.end(); it++)
          {
            if (it->first->qss_species)
              continue;
            fun_ost << " + " << it->second << "*mole_frac_" << it->first->code_name;
          }
          fun_ost << ";\n";
        }
        unsigned next_loc = 0;
        if (forward_unimportant.find(reac->get_global_idx()) != forward_unimportant.end())
          fun_ost << "rr_forward = 0.0;\n";
        else
        {
          for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
                it != reac->forward.end(); it++)
          {
            if (it->first->qss_species)
              continue;
            assert(next_loc < max_forward);
            fun_ost << "forward_factor[" << next_loc << "] = ";
            bool first_scale = true;
            for (int j = 0; j < it->second; j++)
            {
              if (!first_scale)
                fun_ost << " * ";
              fun_ost << "mole_frac_" << it->first->code_name;
              first_scale = false;
            }
            fun_ost << ";\n";
            next_loc++;
          }
          if ((reac->thb != NULL) && !reac->pressure_dep)
          {
            assert(next_loc < max_forward);
            fun_ost << "forward_factor[" << next_loc << "] = thb;\n";
            next_loc++;
          }
        }
        // fill everything else out with 1.0
        while (next_loc < max_forward)
        {
          fun_ost << "forward_factor[" << next_loc << "] = 1.0;\n";
          next_loc++;
        }
        // Now do the same thing for the reverse rate
        next_loc = 0;
        if (backward_unimportant.find(reac->get_global_idx()) != backward_unimportant.end())
          fun_ost << "rr_backward = 0.0;\n";
        else 
        {
          if (reac->reversible)
          {
            for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                  it != reac->backward.end(); it++)
            {
              if (it->first->qss_species)
                continue;
              assert(next_loc < max_backward);
              fun_ost << "backward_factor[" << next_loc << "] = ";
              bool first_scale = true;
              for (int j = 0; j < it->second; j++)
              {
                if (!first_scale)
                  fun_ost << " * ";
                fun_ost << "mole_frac_" << it->first->code_name;
                first_scale = false;
              }
              fun_ost << ";\n";
              next_loc++;
            }
          }
          if ((reac->thb != NULL) && !reac->pressure_dep)
          {
            assert(next_loc < max_backward);
            fun_ost << "backward_factor[" << next_loc << "] = thb;\n";
            next_loc++;
          }
        }
        // fill everything else out with 1.0
        while (next_loc < max_backward)
        {
          fun_ost << "backward_factor[" << next_loc << "] = 1.0;\n";
          next_loc++;
        }
      }
      // Now we can scale our reaction rate and write out our result
      for (unsigned i = 0; i < max_forward; i++)
        fun_ost << "rr_forward *= forward_factor[" << i << "];\n";
      for (unsigned i = 0; i < max_backward; i++)
        fun_ost << "rr_backward *= backward_factor[" << i << "];\n";
      {
        unsigned log_micro_warps = 0;
        unsigned value = micro_warps;
        while (value > 1)
        {
          value /= 2;
          log_micro_warps++;
        }
        unsigned mod_mask = 0;
        for (unsigned i = 0; i < log_micro_warps; i++)
          mod_mask |= (1 << i);
        fun_ost << "rr_f[(wid & " << mod_mask << ") + " << (start_offset+idx) << "][tid] = rr_forward;\n";
        fun_ost << "rr_r[(wid & " << mod_mask << ") + " << (start_offset+idx) << "][tid] = rr_backward;\n";
      }
    }
    for (unsigned i = 0; i < micro_warps; i++)
    {
      if ((idx+i) >= warp_index.size())
        continue;
      int location = start_offset + idx + i;
      assert(location < int(unit->all_reactions.size()));
      Reaction *reac = needed_reactions[idx+i]; 
      if (reaction_map.find(reac) == reaction_map.end())
      {
        // make sure no one else is writing to this location either
        for (std::map<Reaction*,int>::const_iterator it = reaction_map.begin();
              it != reaction_map.end(); it++)
        {
          assert(it->second != location);
        }
        reaction_map[reac] = location;
        reaction_index_map[reac->get_global_idx()] = location;
      }
      else
      {
        assert(reaction_index_map.find(reac->get_global_idx()) != reaction_index_map.end());
        assert(reaction_index_map[reac->get_global_idx()] == location);
        assert(reaction_map[reac] == location);
      }
    }
  }
}

void ChemistryUnit::emit_bulk_weird_reaction(CodeOutStream &ost, CodeOutStream &fun_ost, const std::vector<Reaction*> &needed_reactions,
                                             const std::vector<unsigned> &warp_index, unsigned reac_index, unsigned start_spec_barrier,
                                             unsigned micro_warps, unsigned offset, unsigned start_offset)
{
  assert(offset < 2); // haven't handled multiple buffer cases in arrhenius yet
  assert(needed_reactions.size() == warp_index.size());
  assert(needed_reactions.size() <= warps.size());
  assert((32%micro_warps) == 0);
  bool first = true;
  for (unsigned idx = 0; idx < needed_reactions.size(); idx++)
  {
    Reaction *reac = needed_reactions[idx];
    if (is_weird_reaction(reac))
    {
      if (first)
        ost << "if (__any(wid < " << (warp_index[idx]+micro_warps) << "))\n";
      else
        ost << "else if (__any(wid < " << (warp_index[idx]+micro_warps) << "))\n";
      PairDelim weird_pair(ost);
      emit_bulk_weird_helper(ost, reac, warp_index[idx], reac_index, start_spec_barrier, micro_warps, offset);
      // Now check to see if we need to emit the barrier to wait for the mole fractions to be ready
      if (reac_index == 0)
        ost << "asm volatile(\"bar.sync " <<start_spec_barrier << "," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      // Now figure out the set of mole fractions we need and load them 
      std::set<Species*> needed_mole_fractions;
      if (forward_unimportant.find(reac->get_global_idx()) == forward_unimportant.end())
      {
        for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
              it != reac->forward.end(); it++)
        {
          if (!it->first->qss_species)
            needed_mole_fractions.insert(it->first);
        }
      }
      if (reac->reversible && backward_unimportant.find(reac->get_global_idx()) == backward_unimportant.end());
      {
        for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
              it != reac->backward.end(); it++)
        {
          if (!it->first->qss_species)
            needed_mole_fractions.insert(it->first);
        }
      }
      if ((reac->thb != NULL) && !reac->pressure_dep)
      {
        for (std::map<Species*,double>::const_iterator it = reac->thb->components.begin();
              it != reac->thb->components.end(); it++)
        {
          if (!it->first->qss_species)
            needed_mole_fractions.insert(it->first);
        }
      }
      ost << "reaction_rate_" << warp_index[idx] << "_" << reac_index << "(wid, tid, "
          << MOLE_FRAC << "_" << offset << ", " << THIRD_BODY << "_" << offset
          << ", rr_f, rr_r, rr_forward, rr_backward);\n";
      if (offset == 0)
      {
        fun_ost << "\n";
        fun_ost << "__device__ __forceinline__\n";
        fun_ost << "void reaction_rate_" << warp_index[idx] << "_" << reac_index << "("
                << "const int wid"
                << ", const int tid"
                << ", const volatile " << REAL << " " << MOLE_FRAC << "[NUM_SPECIES][" << POINTS_PER_CTA << "]"
                << ", const volatile " << REAL << " " THIRD_BODY << "[" << POINTS_PER_CTA << "]"
                << ", volatile " << REAL << " rr_f[" << (unit->all_reactions.size()) << "][" << POINTS_PER_CTA << "]"
                << ", volatile " << REAL << " rr_r[" << (unit->all_reactions.size()) << "][" << POINTS_PER_CTA << "]"
                << ", " << REAL << " rr_forward"
                << ", " << REAL << " rr_backward)\n";
        PairDelim function_pair(fun_ost);
        // Emit the reads for the mole fractions
        for (std::set<Species*>::const_iterator it = needed_mole_fractions.begin();
              it != needed_mole_fractions.end(); it++)
        {
          fun_ost << REAL << " mole_frac_" << (*it)->code_name << " = " << MOLE_FRAC /*<< "_" << offset*/ 
                  << "[" << (*it)->code_name << "][tid];\n";
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          fun_ost << REAL << " thb = " << THIRD_BODY << /*"_" << offset <<*/ "[tid]";
          for (std::map<Species*,double>::const_iterator it = reac->thb->components.begin();
                it != reac->thb->components.end(); it++)
          {
            if (it->first->qss_species)
              continue;
            fun_ost << " + " << it->second << "*mole_frac_" << it->first->code_name;
          }
          fun_ost << ";\n";
        }
        if (forward_unimportant.find(reac->get_global_idx()) != forward_unimportant.end())
          fun_ost << "rr_forward = 0.0;\n";
        else
        {
          for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
                it != reac->forward.end(); it++)
          {
            if (it->first->qss_species)
              continue;
            for (int j = 0; j < it->second; j++)
              fun_ost << "rr_forward *= mole_frac_" << it->first->code_name << ";\n";
          }
          if ((reac->thb != NULL) && !reac->pressure_dep)
            fun_ost << "rr_forward *= thb;\n";
        }
        if (backward_unimportant.find(reac->get_global_idx()) != backward_unimportant.end())
          fun_ost << "rr_backward = 0.0;\n";
        else 
        {
          if (reac->reversible)
          {
            for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                  it != reac->backward.end(); it++)
            {
              if (it->first->qss_species)
                continue;
              for (int j = 0; j < it->second; j++)
                fun_ost << "rr_backward *= mole_frac_" << it->first->code_name << ";\n";
            }
          }
          if ((reac->thb != NULL) && !reac->pressure_dep)
            fun_ost << "rr_backward *= thb;\n";
        }
        // Now we can write the results into their destination
        unsigned log_micro_warps = 0;
        unsigned value = micro_warps;
        while (value > 1)
        {
          value /= 2;
          log_micro_warps++;
        }
        fun_ost << "if ((wid >> " << log_micro_warps << ") == 0)\n";
        {
          PairDelim out_pair(fun_ost);
          fun_ost << "rr_f[" << (start_offset+idx) << "][tid] = rr_forward;\n";
          fun_ost << "rr_r[" << (start_offset+idx) << "][tid] = rr_backward;\n";
          int location = start_offset+idx;
          assert(location < int(unit->all_reactions.size()));
          if (reaction_map.find(reac) == reaction_map.end())
          {
            // make sure no one else is writing to this location either
            for (std::map<Reaction*,int>::const_iterator it = reaction_map.begin();
                  it != reaction_map.end(); it++)
            {
              assert(it->second != location);
            }
            reaction_map[reac] = location;
            reaction_index_map[reac->get_global_idx()] = location;
          }
          else
          {
            assert(reaction_index_map.find(reac->get_global_idx()) != reaction_index_map.end());
            assert(reaction_index_map[reac->get_global_idx()] == location);
            assert(reaction_map[reac] == location);
          }
        }
      }
    }
    else
    {
      // All the reactions from here to the end should be normal reactions
      std::vector<Reaction*> normal_reactions;
      std::vector<unsigned> normal_index;
      for (unsigned norm_idx = idx; norm_idx < needed_reactions.size(); norm_idx++)
      {
        Reaction *reac = needed_reactions[norm_idx];
        assert(!is_weird_reaction(reac));
        normal_reactions.push_back(reac);
        normal_index.push_back(warp_index[norm_idx]);
      }
      ost << "else\n";
      PairDelim final_pair(ost);
      emit_bulk_normal_reaction(ost, fun_ost, normal_reactions, normal_index, reac_index, start_spec_barrier,
                                micro_warps, offset, start_offset+idx);
      // And we're done with all the reactions
      break;
    }
    first = false;
  }
}

void ChemistryUnit::emit_bulk_weird_helper(CodeOutStream &ost, Reaction *reac, unsigned warp_index, unsigned reac_index, 
                              unsigned start_spec_barrier, unsigned micro_warps, unsigned offset)
{
  assert(offset < 2);
  assert((32%micro_warps) == 0);
  // Not handling this case right now
  assert(!reac->duplicate.enabled);
  // Emit the forward part first
  if (reac->low.enabled)
  {
    ost << REAL << " rr_k0;\n"; // << low.a;
    bool first = true;
    for (unsigned idx = 0; idx < micro_warps; idx++)
    {
      if (unit->no_nondim)
        arrhenius(ost, warps[warp_index+idx], first/*code*/, true/*weird*/, reac_index, "rr_k0", 
                        reac->low.a, reac->low.beta, reac->low.e, (offset > 0), 32/micro_warps);
      else
        arrhenius(ost, warps[warp_index+idx], first/*code*/, true/*weird*/, reac_index, "rr_k0", 
                        (reac->low.a*REACTION_RATE_REF), reac->low.beta, reac->low.e, (offset > 0), 32/micro_warps);
      first = false;
    }
    ost << REAL << " rr_kinf;\n"; //<< a;
    first = true;
    for (unsigned idx = 0; idx < micro_warps; idx++)
    {
      if (unit->no_nondim)
        arrhenius(ost, warps[warp_index+idx], first/*code*/, true/*weird*/, reac_index, "rr_kinf", 
                        reac->a, reac->beta, reac->e, (offset > 0), 32/micro_warps);
      else
        arrhenius(ost, warps[warp_index+idx], first/*code*/, true/*weird*/, reac_index, "rr_kinf", 
                        (reac->a*REACTION_RATE_REF), reac->beta, reac->e, (offset > 0), 32/micro_warps);
      first = false;
    }
    /* calculate pr */
    ost << REAL << " pr = rr_k0 / rr_kinf * ";
    if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
    {
      ost << MOLE_FRAC << "[" << reac->pressure_species->code_name << "][tid]";
    }
    else
    {
      assert(reac->thb != NULL);
      ost << "(" << THIRD_BODY << "_" << offset << "[tid]";
      for (std::map<Species*,double>::const_iterator it = reac->thb->components.begin();
            it != reac->thb->components.end(); it++)
      {
        if (!it->first->qss_species)
          ost << " + " << it->second << "*" << MOLE_FRAC << "_" << offset << "[" << it->first->code_name << "][tid]";
      }
      ost << ")";
    }
    ost << ";\n";
    if (reac->troe.num > 0)
    {
      ost << REAL << " fcent = log10(" << (1.0-(reac->troe.a)) << " * exp(";
      if (!unit->no_nondim)
        ost << (-1.0*TEMPERATURE_REF/reac->troe.t3) << " * " << TEMPERATURE << ")";
      else
        ost << (-1.0/reac->troe.t3) << " * " << TEMPERATURE << ")";
      // Check for 0.0 here since in some cases
      // if we emit it we end up getting -inf for -1.0/troe.t1
      if (reac->troe.a != 0.0)
      {
        if (!unit->no_nondim)
          ost << " + " << reac->troe.a << " * exp(" << (-1.0*TEMPERATURE_REF/reac->troe.t1) << " * " << TEMPERATURE << ")";
        else
          ost << " + " << reac->troe.a << " * exp(" << (-1.0/reac->troe.t1) << " * " << TEMPERATURE << ")";
      }
      if (reac->troe.num == 4)
      {
        if (unit->no_nondim)
          ost << " + exp(" << (-(reac->troe.t2)) << " * otc)"; 
        else
          ost << " + exp(" << (-(reac->troe.t2)/TEMPERATURE_REF) << " * otc)";
      }
      ost << ");\n";
      ost << REAL << " flogpr = log10(pr) - 0.4 - 0.67 * fcent;\n";
      ost << REAL << " fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;\n";
      ost << REAL << " fquan = flogpr / fdenom;\n";
      ost << "fquan = fcent / (1.0 + fquan * fquan);\n";
      ost << REAL << " rr_forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);\n";
    }
    else if (reac->sri.num > 0)
    {
      ost << REAL << " fpexp = log10(pr);\n";
      ost << REAL << " fpexp = 1. / (1. + fpexp * fpexp);\n";
      ost << REAL << " fquan = (";
      ost << reac->sri.a;
      ost << " * exp(";
      if (unit->no_nondim)
        ost << (-(reac->sri.b));
      else
        ost << (-(reac->sri.b)/TEMPERATURE_REF);
      ost << "*otc) + exp(";
      ost << (-1./(reac->sri.c));
      assert(false); // need to handle power
      ost << "*tc)) ** fpexp;\n";
      if (reac->sri.num == 5) {
          ost << " fquan = fquan * ";
          ost << reac->sri.d;
          ost << " * pow(" << TEMPERATURE <<",";
          ost << reac->sri.e;
          ost << ");\n";
      }
      ost << REAL << " rr_forward = rr_kinf * pr/(1.0 + pr) * fquan;\n";
    }
    else
    {
      // plain old Lindemann
      ost << REAL << " rr_forward = rr_kinf * pr/(1.0 + pr);\n";
    }
  }
  else if (reac->lt.enabled)
  {
    /* Landau-Teller formulation */
    if (unit->no_nondim)
      ost << REAL << " ftroot = pow(otc,(1./3.));\n";
    else
      ost << REAL << " ftroot = pow(otc/" << TEMPERATURE_REF << ",(1./3.));\n";
    ost << REAL << " rr_forward = ";
    if (!unit->no_nondim)
      ost << (REACTION_RATE_REF * reac->a * pow(TEMPERATURE_REF,reac->beta));
    else
      ost << reac->a;
    ost << " * exp(";
    ost << reac->beta;
    ost << "*vlntemp- ";
    if (unit->no_nondim)
      ost << reac->e;
    else
      ost << (reac->e/TEMPERATURE_REF);
    ost << "*otc + ";
    ost << reac->lt.b;
    ost << "*ftroot + ";
    ost << reac->lt.c;
    ost << "*ftroot*ftroot);\n";
  }
  else
  {
    ost << REAL << " rr_forward;\n";
    bool first = true;
    for (unsigned idx = 0; idx < micro_warps; idx++)
    {
      if (!unit->no_nondim)
        arrhenius(ost, warps[warp_index+idx], first/*code*/, true/*weird*/, reac_index, "rr_forward", (reac->a*REACTION_RATE_REF), 
                  reac->beta, reac->e, (offset > 0), 32/micro_warps);
      else
        arrhenius(ost, warps[warp_index+idx], first/*code*/, true/*weird*/, reac_index, "rr_forward", reac->a, 
                  reac->beta, reac->e, (offset > 0), 32/micro_warps);
      first = false;
    }
  } 
  // Now do the reverse case
  if (!reac->reversible)
  {
    ost << REAL << " rr_backward = 0.0;\n";
    return;
  }
  if (reac->rev.enabled)
  {
    if (reac->rlt.enabled)
    {
      if (!reac->lt.enabled)
      {
        if (unit->no_nondim)
          ost << REAL << " ftroot = pow(otc,(1./3.));\n";
        else
          ost << REAL << " ftroot = pow(otc/" << TEMPERATURE_REF << ", (1./3.));\n";
      }
      ost << "rr_backward = ";
      if (!unit->no_nondim)
        ost << (reac->a*pow(TEMPERATURE_REF,reac->beta));
      else
        ost << reac->a;
      ost << " * exp(";
      ost << reac->beta;
      ost << "*vlntemp- ";
      if (unit->no_nondim)
        ost << reac->e;
      else
        ost << (reac->e / TEMPERATURE_REF); // scale by temperature ref for non-dimensionalized case
      ost << "*otc + ";
      ost << reac->rlt.b;
      ost << "*ftroot + ";
      ost << reac->rlt.c;
      ost << "*ftroot*ftroot);\n";
    }
    else
    {
      /* explicit Arrhenius rate given */
      ost << REAL << " rr_backward;\n";
      bool first = true;
      for (unsigned idx = 0; idx < micro_warps; idx++)
      {
        if (!unit->no_nondim)
          arrhenius(ost, warps[warp_index+idx], first/*code*/, true/*weird*/, reac_index, "rr_backward", 
                                (REACTION_RATE_REF*reac->rev.a), reac->rev.beta, reac->rev.e, (offset > 0), 32/micro_warps);
        else
          arrhenius(ost, warps[warp_index+idx], first/*code*/, true/*weird*/, reac_index, "rr_backward", 
                                reac->rev.a, reac->rev.beta, reac->rev.e, (offset > 0), 32/micro_warps);
        first = false;
      }
    }
  }
  else
  {
    /* use equilibrium relation */
    /* total nu */
    int nutot = 0;
    std::set<Species*> needed_gibbs;
    for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
          it != reac->stoich.end(); it++)
    {
      nutot += it->second;
      needed_gibbs.insert(it->first);
    }
    /* total stoich gibbs energy */
    emit_gibbs_computations(ost, needed_gibbs); 
    ost << REAL << " xik = ";
    bool first = true;
    for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
          it != reac->stoich.end(); it++)
    {
      if (first)
      {
        first = false;
        if (it->second > 1)
        {
          ost.print("%d.0 * ", it->second);
        }
        else if (it->second == -1)
        {
          ost << "-";
        }
        else if (it->second < -1)
        {
          ost.print("-%d.0 * ", -(it->second));
        }
      }
      else
      {
        if (it->second == 1)
        {
          ost << " + ";
        }
        else if (it->second > 1)
        {
          ost.print(" + %d.0 * ", it->second);
        }
        else if (it->second == -1)
          ost << " - ";
        else if (it->second < -1)
        {
          ost.print(" - %d.0 * ", -it->second);
        }
      }
      ost << "gibbs_" << it->first->code_name;
    }
    ost << ";\n";
    ost << REAL << " rr_backward = rr_forward";
    // This case of otc gets handled by folding it back through
    // all the gibbs free energy constants
    ost.print(" * exp(xik*otc");
    //if (unit->no_nondim) 
    {
      if (nutot == 0)
        ost << ")";
      else if (nutot == 1)
      {
        ost << ") * oprt";
      }
      else if (nutot == 2)
      {
        ost << ") * oprt * oprt";
      }
      else if (nutot == -1)
      {
        ost << ") * prt";
      }
      else if (nutot == -2)
      {
        ost << ") * prt * prt";
      }
      else if (nutot > 0)
      {
        if (unit->no_nondim)
          ost.print(" - %d.0*vlntemp)", nutot);
        else
          ost << " - " << nutot << ".0*(vlntemp + " << TEMPERATURE_REF << "))";
      }
      else 
      {
        if (unit->no_nondim)
          ost.print(" + %d.0*vlntemp)", -nutot);
        else
          ost << " + " << (-nutot) << ".0*(vlntemp + " << TEMPERATURE_REF << "))";
      }
    }
    ost << ";\n";
  }

}

void ChemistryUnit::emit_pipe_getrates_code(CodeOutStream &ost, unsigned non_qss_species, const char *temp_file_name)
{
  ost << "\n";
  emit_pipe_general_constants(ost, non_qss_species);
  unsigned pass0_per_thread, pass0_constant_stride;
  emit_pipe_warp_constants(ost, qssa_warps, 0/*pass*/, pass0_per_thread, pass0_constant_stride);
  unsigned pass1_per_thread, pass1_constant_stride;
  emit_pipe_warp_constants(ost, stif_warps, 1/*pass*/, pass1_per_thread, pass1_constant_stride);
  unsigned pass2_per_thread, pass2_constant_stride;
  emit_pipe_warp_constants(ost, last_warps, 2/*pass*/, pass2_per_thread, pass2_constant_stride);
  ost << "\n";
  ost << "__global__ void\n";
  ost << "gpu_getrates(";
  ost << "const " << REAL << " *" << PRESSURE_ARRAY;
  ost << ", const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
  ost << ", const " << REAL << " *" << DIFFUSION_ARRAY;
  ost << ", const " << REAL << " dt";
  ost << ", const " << REAL << " recip_dt/*reciprocal of dt*/";
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << WDOT;
  ost << ")\n";
  PairDelim pair(ost);
  // Compute the offset for this thread
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*2*row_stride) + "
        << "(blockIdx.x*32 + (threadIdx.x & 0x1f));\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MASS_FRAC_ARRAY << " += offset;\n";
    ost << DIFFUSION_ARRAY << " += offset;\n";
    ost << WDOT << " += offset;\n";
  }
  // Declare the shared memory allocations
  ost << "volatile __shared__ " << REAL << " pipe_0[" << unit->threads_per_point << "][2][32];\n";
  ost << "volatile __shared__ " << REAL << " pipe_1[" << unit->threads_per_point << "][2][32];\n";
  ost << "volatile __shared__ " << REAL << " pipe_2[" << unit->threads_per_point << "][2][32];\n";
  ost << "volatile __shared__ " << REAL << " pipe_3[" << unit->threads_per_point << "][2][32];\n";
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "_0[" << non_qss_species << "][32];\n";
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "_1[" << non_qss_species << "][32];\n";
  ost << "volatile __shared__ " << REAL << " " << THIRD_BODY << "_0[" << unit->third_bodies.size() << "][32];\n";
  ost << "volatile __shared__ " << REAL << " " << THIRD_BODY << "_1[" << unit->third_bodies.size() << "][32];\n";
  size_t total_shared = (4*unit->threads_per_point*2*32*sizeof(REAL_TYPE))
            + 2*((non_qss_species+unit->third_bodies.size())*32);
  fprintf(stdout,"  INFO: Total shared memory usage per CTA: %ld\n", total_shared);
  ost << "if (__any(threadIdx.x < " << (unit->threads_per_point*32) << "))\n";
  {
    PairDelim experiment_pair(ost);
    ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
    ost << "const " << INT << " wid = threadIdx.x >> 5;\n";
    ost << REAL << " pass_0_constants[" << pass0_per_thread << "];\n";
    ost << REAL << " pass_1_constants[" << pass1_per_thread << "];\n";
    ost << REAL << " pass_2_constants[" << pass2_per_thread << "];\n";
    // emit the loads for the reaction constants
    for (unsigned idx = 0; idx < pass0_per_thread; idx++)
    {
      char src_offset[128];
      sprintf(src_offset,"wid*%d+%d",pass0_constant_stride,(idx*32));
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      emit_cuda_load(ost,"pass_0_constants",dst_offset,"warp_constants_0",src_offset,false/*double2*/,".cg");
    }
    for (unsigned idx = 0; idx < pass1_per_thread; idx++)
    {
      char src_offset[128];
      sprintf(src_offset,"wid*%d+%d",pass1_constant_stride,(idx*32));
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      emit_cuda_load(ost,"pass_1_constants",dst_offset,"warp_constants_1",src_offset,false/*double2*/,".cg");
    }
    for (unsigned idx = 0; idx < pass2_per_thread; idx++)
    {
      char src_offset[128];
      sprintf(src_offset,"wid*%d+%d",pass2_constant_stride,(idx*32));
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      emit_cuda_load(ost,"pass_2_constants",dst_offset,"warp_constants_2",src_offset,false/*double2*/,".cg");
    }
    // Then copy over the code from the temporary file
    FILE *temp_file = fopen(temp_file_name,"r");
    assert(temp_file != NULL);
    char c = fgetc(temp_file);
    while (c != EOF)
    {
      ost << c;
      c = fgetc(temp_file);
    }
    assert(fclose(temp_file) == 0);
  }
  // Emit the code for each of the mole fraction warps
  ost << "else if (__any(threadIdx.x < " << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << "))\n";
  {
    PairDelim mole_frac_pair(ost);
    ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
    ost << "const " << INT << " offset = (threadIdx.x & " << MOLE_FRAC_WARP_MASK << ");\n";
    ost << "#pragma unroll 1\n"; // don't ever unroll this loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim loop_step_pair(ost);
    emit_pipe_fine_scaling_code(ost, qssa_warps, non_qss_species, 0, 0, true/*even*/, qssa_warps);
    emit_pipe_fine_scaling_code(ost, qssa_warps, non_qss_species, 0, 1, true/*even*/, stif_warps);
    emit_pipe_fine_scaling_code(ost, stif_warps, non_qss_species, 1, 0, true/*even*/, stif_warps);
    emit_pipe_fine_scaling_code(ost, stif_warps, non_qss_species, 1, 1, true/*even*/, last_warps);
    emit_pipe_fine_scaling_code(ost, last_warps, non_qss_species, 2, 0, true/*even*/, last_warps);
    emit_pipe_fine_scaling_code(ost, last_warps, non_qss_species, 2, 1, true/*even*/, qssa_warps);
  }
  ost << "else if (__any(threadIdx.x < " << ((unit->threads_per_point+2*MOLE_FRAC_WARPS)*32) << "))\n";
  {
    PairDelim mole_frac_pair(ost);
    ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
    ost << "const " << INT << " offset = (threadIdx.x & " << MOLE_FRAC_WARP_MASK << ");\n";
    ost << "#pragma unroll 1\n"; // don't ever unroll this loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim loop_step_pair(ost);
    emit_pipe_fine_scaling_code(ost, qssa_warps, non_qss_species, 0, 0, false/*even*/, qssa_warps);
    emit_pipe_fine_scaling_code(ost, qssa_warps, non_qss_species, 0, 1, false/*even*/, stif_warps);
    emit_pipe_fine_scaling_code(ost, stif_warps, non_qss_species, 1, 0, false/*even*/, stif_warps);
    emit_pipe_fine_scaling_code(ost, stif_warps, non_qss_species, 1, 1, false/*even*/, last_warps);
    emit_pipe_fine_scaling_code(ost, last_warps, non_qss_species, 2, 0, false/*even*/, last_warps);
    emit_pipe_fine_scaling_code(ost, last_warps, non_qss_species, 2, 1, false/*even*/, qssa_warps);
  }
  ost << "else\n";
  {
    PairDelim last_pair(ost);
    ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
    ost << "const " << INT << " offset = ((threadIdx.x >> 5) & 0x1);\n";
    // Step our pointers based on the offset
    ost << TEMPERATURE_ARRAY << " += (offset*row_stride);\n";
    ost << PRESSURE_ARRAY << " += (offset*row_stride);\n";
    ost << MASS_FRAC_ARRAY << " += (offset*row_stride);\n";
    ost << DIFFUSION_ARRAY << " += (offset*row_stride);\n";
    ost << WDOT << " += (offset*row_stride);\n";
    // Declare the arrays that we will use
    ost << REAL << " xq_values[" << ((unit->species.size()-1)-non_qss_species) << "];\n";
    ost << REAL << " stiffness[" << (stif_operations.size()) << "];\n";
    ost << "#pragma unroll 1\n"; // don't ever unroll this loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim loop_step_pair(ost);
    emit_pipe_fine_qssa_computation(ost, qssa_warps, non_qss_species);
    emit_pipe_fine_stiffness_computation(ost, stif_warps, non_qss_species);
    emit_pipe_fine_output_computation(ost, last_warps, non_qss_species);
    // Update our pointers before we go around the loop
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
    ost << DIFFUSION_ARRAY << " += slice_stride;\n";
    ost << WDOT << " += slice_stride;\n";
  }
#if 0
  // Now emit the code for the mole fraction warps
  ost << "else if (__any(threadIdx.x < " << ((unit->threads_per_point+2)*32) << "))\n";
  {
    PairDelim mole_frac_pair(ost);
    ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
    ost << "const " << INT << " offset = ((threadIdx.x >> 5) & 0x1);\n";
    // If step down our pointers based on our offset
    ost << TEMPERATURE_ARRAY << " += (offset*row_stride);\n";
    ost << PRESSURE_ARRAY << " += (offset*row_stride);\n";
    ost << MASS_FRAC_ARRAY << " += (offset*row_stride);\n";
    // Declare our data
    ost << REAL << " " << MOLE_FRAC << "[" << non_qss_species << "];\n";
    if (!unit->third_bodies.empty())
      ost << REAL << " " << THIRD_BODY << "[" << unit->third_bodies.size() << "];\n";
    // Do the loop
    ost << "#pragma unroll 1\n"; // don't ever unroll this loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim loop_step_pair(ost);
    emit_pipe_mole_fraction_code(ost, qssa_warps, non_qss_species, 0, stif_warps);
    emit_pipe_mole_fraction_code(ost, stif_warps, non_qss_species, 1, last_warps);
    emit_pipe_mole_fraction_code(ost, last_warps, non_qss_species, 2, qssa_warps);
    // Update our pointers before we go around the loop
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << MASS_FRAC_ARRAY << " += slice_stride;\n";
  }
  // finally emit the code for doing QSSA, stiffness, and output
  ost << "else\n";
  {
    PairDelim last_pair(ost);
    ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
    ost << "const " << INT << " offset = ((threadIdx.x >> 5) & 0x1);\n";
    // Step our pointers based on the offset
    ost << DIFFUSION_ARRAY << " += (offset*row_stride);\n";
    ost << WDOT << " += (offset*row_stride);\n";
    // Declare the arrays that we will use
    ost << REAL << " xq_values[" << ((unit->species.size()-1)-non_qss_species) << "];\n";
    ost << REAL << " stiffness[" << (stif_operations.size()) << "];\n";
    // Do the loop
    ost << "#pragma unroll 1\n"; // don't ever unroll this loop
    ost << "for (int step = 0; step < total_steps; step++)\n";
    PairDelim loop_step_pair(ost);
    emit_pipe_qssa_computation(ost, qssa_warps, non_qss_species);
    emit_pipe_stiffness_computation(ost, stif_warps, non_qss_species);
    emit_pipe_output_computation(ost, last_warps, non_qss_species);
    // Update our pointers before we go around the loop
    ost << DIFFUSION_ARRAY << " += slice_stride;\n";
    ost << WDOT << " += slice_stride;\n";
  }
#endif
}

void ChemistryUnit::emit_pipe_reaction_code(CodeOutStream &ost, const std::vector<Warp*> &target_warps,
                                            unsigned non_qss_species, int phase, int pass)
{
  assert(phase < 2);
  if ((pass == 0) && (phase == 0))
  {
    unit->emit_numerical_constants(ost);
    ost << REAL << " " << TEMPERATURE << "_0, " << TEMPERATURE << "_1;\n";
    char dst[128];
    sprintf(dst,"%s_%d",TEMPERATURE,0);
    emit_cuda_load(ost,dst,TEMPERATURE_ARRAY,false/*double2*/,".cg");
    sprintf(dst,"%s_%d",TEMPERATURE,1);
    emit_cuda_load(ost,dst,TEMPERATURE_ARRAY,"row_stride",false/*double2*/,".cg");
  }
  if (pass == 0)
  {
    ost << "const " << REAL << " otc_" << phase << " = 1.0 / " << TEMPERATURE << "_" << phase << ";\n";
    ost << "const " << REAL << " ortc_" << phase << " = 1.0 / (" << TEMPERATURE << "_" << phase << " * R0c);\n";
    ost << "const " << REAL << " vlntemp_" << phase << " = log(" << TEMPERATURE << "_" << phase << ");\n";
    //ost << "const " << REAL << " prt_" << phase << " = PA / (R0 * " << TEMPERATURE << "_" << phase << ");\n";
    //ost << "const " << REAL << " oprt_" << phase << " = 1.0 / prt_" << phase << ";\n";
  }
  assert(!target_warps.empty());
  unsigned total_gibbs = 0;
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    std::vector<Reaction*> weird_reactions;
    std::vector<Reaction*> normal_reactions;
    ost << "// Reaction stage " << stage_idx << " for phase " << phase << " of pass " << pass << "\n";
    PairDelim stage_pair(ost);
    ost << REAL << " rr_f, rr_r;\n";
    int last_weird_warp = -1;
    std::vector<Reaction*> low_reactions;
    std::vector<Reaction*> irreversible_reactions;
    unsigned low_mask = 0;
    std::vector<Warp*> low_warps;
    std::vector<Reaction*> troe_reactions;
    std::vector<Reaction*> lindermann_reactions;
    unsigned troe_mask = 0;
    std::vector<Warp*> troe_warps;
    // Do the arrhenius part for everyone
    for (unsigned widx = 0; widx < target_warps.size(); widx++)
    {
      if (stage_idx < target_warps[widx]->reactions.size())
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
        Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
        assert(reac != NULL);
        if (is_weird_reaction(reac))
        {
          weird_reactions.push_back(reac);
          assert((last_weird_warp+1) == int(widx)); // all weird warps should be contiguous
          last_weird_warp = widx;
          // Figure out what kind of arrhenius to do
          if (reac->low.enabled)
          {
            low_reactions.push_back(reac);
            assert(widx < 32); // 1-hot encoding doesn't work past 32 bits
            low_mask |= (1 << widx); 
            low_warps.push_back(target_warps[widx]);
            if (reac->troe.num > 0)
            {
              assert(reac->troe.num == 4); // assume four troe constants for now
              troe_reactions.push_back(reac);
              troe_mask |= (1 << widx);
              troe_warps.push_back(target_warps[widx]);
            }
            else if (reac->sri.num == 0)
            {
              lindermann_reactions.push_back(reac);
            }
            else
              assert(false); // not handling sri right now
            if (!unit->no_nondim)
            {
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                      (reac->low.a*REACTION_RATE_REF), reac->low.beta, reac->low.e, phase, pass);
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                      (reac->a*REACTION_RATE_REF), reac->beta, reac->e, phase, pass);
            }
            else
            {
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                      reac->low.a, reac->low.beta, reac->low.e, phase, pass);
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                      reac->a, reac->beta, reac->e, phase, pass); 
            }
            assert(!reac->rev.enabled); // hoping we never have to do a reverse reaction in this case 
            assert(reac->reversible);
          }
          else if (!reac->lt.enabled)
          {
            assert(!reac->rlt.enabled);
            assert(!reac->reversible);
            irreversible_reactions.push_back(reac);
            if (!unit->no_nondim)
            {
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                      (reac->a*REACTION_RATE_REF), reac->beta, reac->e, phase, pass);
              if (reac->rev.enabled)
              {
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        (reac->rev.a*REACTION_RATE_REF), reac->rev.beta, reac->rev.e, phase, pass);
              }
              else
              {
                // We're just going to write zero anyway, so what does it matter
                // Duplicate the computation so we don't diverge, but with coefficients that won't do anything weird
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        (reac->a*REACTION_RATE_REF), reac->beta, reac->e, phase, pass);
              }
            }
            else
            {
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                      reac->a, reac->beta, reac->e, phase, pass);
              if (reac->rev.enabled)
              {
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        reac->rev.a, reac->rev.beta, reac->rev.e, phase, pass);
              }
              else
              {
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                      reac->a, reac->beta, reac->e, phase, pass);
              }
            }
          }
          else
          {
            assert(false); // Not currently handling landau-teller right now since it doesn't use arrhenius
          }
        }
        else
        {
          normal_reactions.push_back(reac);
          // Normal arrhenius and we're done
          if (!unit->no_nondim)
          {
            emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                      (reac->a*REACTION_RATE_REF), reac->beta, reac->e, phase, pass);
            emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                      (reac->rev.a*REACTION_RATE_REF), reac->rev.beta, reac->rev.e, phase, pass);
          }
          else
          {
            emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                        reac->a, reac->beta, reac->e, phase, pass);
            emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        reac->rev.a, reac->rev.beta, reac->rev.e, phase, pass);
          }
        }
      }
    }
    if (!weird_reactions.empty())
    {
      if (!normal_reactions.empty())
      {
        assert(last_weird_warp != -1);
        ost << "if (__any(wid < " << (last_weird_warp+1) << "))\n";
      }
      PairDelim weird_pair(ost);
      // Now emit the code for the weird reactions
      // Emit the code for the different cases of weird reactions
      if (!low_reactions.empty())
      {
        if (!irreversible_reactions.empty())
          ost << "if (__any(((1 << wid) & " << low_mask << ")))\n";
        PairDelim low_pair(ost);
        ost << REAL << " rr_k0 = rr_f;\n";
        ost << REAL << " rr_kinf = rr_r;\n";
        ost << REAL << " pr = rr_k0 / rr_kinf;\n";
        // Figure out the maximum number of gibbs species we need to compute for any of the troe warps
        unsigned max_gibbs = 0;
        for (std::vector<Reaction*>::const_iterator it = low_reactions.begin();
              it != low_reactions.end(); it++)
        {
          if ((*it)->stoich.size() > max_gibbs)
            max_gibbs = (*it)->stoich.size();
        }
        // shouldn't be zero 
        assert(max_gibbs > 0);
        // emit the code to compute the gibbs values that we need
        ost << "// Gibbs computation\n";
        {
          unsigned old_total_gibbs = total_gibbs;
          PairDelim gibbs_pair(ost);
          ost << REAL << " xik = 0.0;\n";
          ost << "const " << REAL << " &tk1 = " << TEMPERATURE << "_" << phase << ";\n";
          ost << "const " << REAL << " tk2 = tk1*tk1;\n";
          ost << "const " << REAL << " tk3 = tk1*tk2;\n";
          ost << "const " << REAL << " tk4 = tk1*tk3;\n";
          ost << "const " << REAL << " tk5 = tk1*tk4;\n";
          ost << INT << " gibbs_idx;\n";
          for (unsigned idx = 0; idx < max_gibbs; idx++)
          {
            ost << REAL << " gibbs_" << idx << ";\n";
            // Do something to fool the compiler here
            unsigned upper_bound = next_largest_power(2*non_qss_species,2);
            ost << "gibbs_idx = (gibbs_index_" << pass << "[(" << total_gibbs 
                << "+(step+" << (2*pass+phase) << ")*" << upper_bound << ")%" << upper_bound << "][wid]"
                << "+(step+" << (2*pass+phase) << ")*" << upper_bound << ")%" << upper_bound << ";\n";
            ost << "if (tk1 > gibbs_values[gibbs_idx][0])\n";
            {
              PairDelim if_pair(ost);
              ost << "gibbs_" << idx << " = gibbs_values[gibbs_idx][1]*tk1*(";
              if (!unit->no_nondim)
                ost << (1.0-log(TEMPERATURE_REF));
              else
                ost << "1.0";
              ost << "- vlntemp_" << phase << ") + "
                  << "gibbs_values[gibbs_idx][2]*tk2 + "
                  << "gibbs_values[gibbs_idx][3]*tk3 + "
                  << "gibbs_values[gibbs_idx][4]*tk4 + "
                  << "gibbs_values[gibbs_idx][5]*tk5 + "
                  << "(gibbs_values[gibbs_idx][6] - tk1*"
                  << "gibbs_values[gibbs_idx][7]);\n";
            }
            ost << "else\n";
            {
              PairDelim else_pair(ost);
              ost << "gibbs_" << idx << " = gibbs_values[gibbs_idx][8]*tk1*(";
              if (!unit->no_nondim)
                ost << (1.0-log(TEMPERATURE_REF));
              else
                ost << "1.0";
              ost << " - vlntemp_" << phase << ") + "
                  << "gibbs_values[gibbs_idx][9]*tk2 + "
                  << "gibbs_values[gibbs_idx][10]*tk3 + "
                  << "gibbs_values[gibbs_idx][11]*tk4 + "
                  << "gibbs_values[gibbs_idx][12]*tk5 + "
                  << "(gibbs_values[gibbs_idx][13] - tk1*"
                  << "gibbs_values[gibbs_idx][14]);\n";
            }
            // Do something to fool the compiler here
            ost << "xik = __fma_rn(gibbs_scale_" << pass << "[(" << total_gibbs 
                << "+(step+" << (2*pass+phase) << ")*" << upper_bound << ")%" << upper_bound << "][wid],gibbs_" << idx << ",xik);\n";
            total_gibbs++;
          }
          ost << "rr_r = exp(xik*otc_" << phase << " - reverse_pass_" << pass << "[" << stage_idx << "][wid]*";
          if (!unit->no_nondim)
            ost << "(vlntemp_" << phase << " + " << TEMPERATURE_REF << ")";
          else
            ost << "vlntemp_" << phase;
          ost << ");\n";
          // Now update low warps with the information that they need to emit the constants
          assert(low_reactions.size() == low_warps.size());
          for (unsigned idx = 0; idx < low_warps.size(); idx++)
          {
            Warp *warp = low_warps[idx];
            Reaction *reac = low_reactions[idx];
            unsigned added_gibbs = 0;
            int nutot = 0;
            for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
                  it != reac->stoich.end(); it++)
            {
              int spec_idx = -1;
              for (unsigned gidx = 0; gidx < gibbs_species.size(); gidx++)
              {
                if (it->first == gibbs_species[gidx])
                {
                  spec_idx = gidx;
                  break;
                }
              }
              if (spec_idx == -1)
              {
                spec_idx = gibbs_species.size();
                gibbs_species.push_back(it->first);
              }
              warp->add_gibbs_species(old_total_gibbs+added_gibbs,spec_idx,double(it->second),(phase==0));
              added_gibbs++;
              nutot += it->second;
            }
            while (added_gibbs < max_gibbs)
            {
              warp->add_gibbs_species(old_total_gibbs+added_gibbs,0,0.0,(phase==0));
              added_gibbs++;
            }
            // Also update the reverse value
            warp->add_reverse_constant(stage_idx, double(nutot),(phase==0));
          }
        }

        // Now we need to wait until we get our information from the mole fraction warp
        // about our scaling value
        //if (phase == 0)
        if ((stage_idx%2)==0)
        {
          ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
          ost << "pr *= pipe_0[wid][0][tid];\n";
        }
        else
        {
          ost << "asm volatile(\"bar.sync 2," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
          ost << "pr *= pipe_1[wid][0][tid];\n";
        }
        if (!troe_reactions.empty())
        {
          if (!lindermann_reactions.empty())
            ost << "if (__any(((1 << wid) & " << troe_mask << ")))\n";
          PairDelim troe_pair(ost);
          // Do something to fool the compiler here
          assert(non_qss_species >= needed_troe_reactions.size());
          unsigned upper_bound = next_largest_power(2*non_qss_species,2);
          ost << INT << " troe_idx = (troe_index_" << pass << "[(" << stage_idx 
              << "+(step+" << (pass*2+phase) << ")*" << upper_bound << ")%" << upper_bound 
              << "][wid] + (step+" << (pass*2+phase) << ")*" << upper_bound << ")%" << upper_bound << ";\n";
          ost << REAL << " fcent = log10(troe_values[troe_idx][0] * exp("
              << "troe_values[troe_idx][1] * " << TEMPERATURE << "_" << phase  << ")"
              << " + troe_values[troe_idx][2] * exp("
              << "troe_values[troe_idx][3] * " << TEMPERATURE << "_" << phase << ")"
              << " + exp(troe_values[troe_idx][4] * otc_" << phase << "));\n";
          // add the troe constants
          assert(troe_reactions.size() == troe_warps.size());
          for (unsigned troe_idx = 0; troe_idx < troe_warps.size(); troe_idx++)
          {
            Warp *troe = troe_warps[troe_idx];
            Reaction *reac = troe_reactions[troe_idx];
            int tr_idx = -1;
            for (unsigned tidx = 0; tidx < needed_troe_reactions.size(); tidx++)
            {
              if (needed_troe_reactions[tidx] == reac)
              {
                tr_idx = tidx;
                break;
              }
            }
            if (tr_idx == -1)
            {
              tr_idx = needed_troe_reactions.size();
              needed_troe_reactions.push_back(reac);
            }
            troe->add_troe_reaction(stage_idx, tr_idx, (phase == 0));
          }
          ost << REAL << " flogpr = log10(pr) - 0.4 - 0.67 * fcent;\n";
          ost << REAL << " fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;\n";
          ost << REAL << " fquan = flogpr / fdenom;\n";
          ost << "fquan = fcent / (1.0 + fquan * fquan);\n";
          ost << "rr_f = rr_kinf * pr/(1.0 + pr) * exp(fquan * DLn10);\n";
        }
        if (!lindermann_reactions.empty())
        {
          if (!troe_reactions.empty())
            ost << "else\n";
          PairDelim linderman_pair(ost);
          ost << "rr_f = rr_kinf * pr/(1.0 + pr);\n";
        }
        // Now that we have rr_f, we can compute the final part of rr_r
        ost << "rr_r *= rr_f;\n";
        // Write our results out into shared memory, no need to wait since we already know the buffer is ready
        //if (phase == 0)
        if ((stage_idx%2)==0)
        {
          ost << "pipe_0[wid][0][tid] = rr_f;\n";
          ost << "pipe_0[wid][1][tid] = rr_r;\n";
          ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
        }
        else
        {
          ost << "pipe_1[wid][0][tid] = rr_f;\n";
          ost << "pipe_1[wid][1][tid] = rr_r;\n";
          ost << "asm volatile(\"bar.arrive 3," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
        }
      }
      if (!irreversible_reactions.empty())
      {
        if (!low_reactions.empty())
          ost << "else\n";
        PairDelim irrev_pair(ost);
        //if (phase == 0)
        if ((stage_idx%2)==0)
        {
          ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
          ost << "pipe_0[wid][0][tid] = rr_f;\n";
          ost << "pipe_0[wid][1][tid] = 0.0; /* irreversible reaction*/\n";
          ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
        }
        else
        {
          ost << "asm volatile(\"bar.sync 2," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
          ost << "pipe_1[wid][0][tid] = rr_f;\n";
          ost << "pipe_1[wid][1][tid] = 0.0; /*irreversible reaction*/\n";
          ost << "asm volatile(\"bar.arrive 3," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
        }
      }
    }
    // Now do all the normal reactions
    if (!normal_reactions.empty())
    {
      if (!weird_reactions.empty())
        ost << "else\n";
      PairDelim normal_pair(ost);
      // For the normal case we can just do the normal thing and write our values into
      // shared memory ass soon as the buffer is ready
      // Write our result into the pipe
      //if (phase == 0)
      if ((stage_idx%2)==0)
      {
        ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
        ost << "pipe_0[wid][0][tid] = rr_f;\n";
        ost << "pipe_0[wid][1][tid] = rr_r;\n";
        ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
      }
      else
      {
        ost << "asm volatile(\"bar.sync 2," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
        ost << "pipe_1[wid][0][tid] = rr_f;\n";
        ost << "pipe_1[wid][1][tid] = rr_r;\n";
        ost << "asm volatile(\"bar.arrive 3," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
      }
    }
  }
}

void ChemistryUnit::emit_pipe_mole_fraction_code(CodeOutStream &ost, const std::vector<Warp*> &target_warps, 
                              unsigned num_non_qss_species, int pass, const std::vector<Warp*> &next_warps)
{
  ost << "// Pass " << pass << "\n";
  if (pass == 0)
  {
    // Figure out if there are any mole fraction or third body values that need
    // to be sent to the reaction warps, if not, we can do the barrier now
    std::map<int,Reaction*> needed_values;
    for (unsigned widx = 0; widx < target_warps.size(); widx++)
    {
      if (!target_warps[widx]->reactions.empty())
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(0);
        Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
        assert(reac != NULL);
        // Check to see if it needs a mole frac value
        if (reac->low.enabled)
        {
          needed_values[widx] = reac;
        }
      }
    }
    if (needed_values.empty())
    {
      ost << "if (__any(offset == 0))\n";
      ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      ost << "else\n";
      ost << "asm volatile(\"bar.arrive 2," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
    }
    // emit the loads for the values that we need
    PairDelim load_pair(ost);
    ost << REAL << " " << TEMPERATURE << ", " << PRESSURE << ";\n";
    emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,false/*double2*/,".cg");
    std::set<Species*> non_qss_species;
    //unsigned upper_bound = next_largest_power(2*num_non_qss_species,2);
    for (std::set<Species*>::const_iterator it = unit->species.begin();
          it != unit->species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      char src_offset[128];
      //sprintf(src_offset,"((%s+step*%d)%%%d)*spec_stride",(*it)->code_name,upper_bound,upper_bound);
      sprintf(src_offset,"%s*spec_stride",(*it)->code_name);
      emit_cuda_load(ost,MOLE_FRAC,(*it)->code_name,MASS_FRAC_ARRAY,src_offset,false/*double2*/,".cg");
      non_qss_species.insert(*it);
    }
    emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,false/*double2*/,".cg");
    ost << REAL << " sumyow = 0.0;\n";
    for (std::set<Species*>::const_iterator it = non_qss_species.begin();
          it != non_qss_species.end(); it++)
    {
      ost << "sumyow = __fma_rn(" << MOLE_FRAC << "[" << (*it)->code_name 
          << "],recip_molecular_mass[" << (*it)->code_name << "],sumyow);\n";
    }
    ost << "sumyow *= (";
    if (unit->no_nondim)
      ost << "8.314510e+07 * " << TEMPERATURE;
    else
      ost << (8.314510e+07 * TEMPERATURE_REF / PRESSURE_REF) << " * " << TEMPERATURE;
    ost << ");\n";
    ost << "sumyow = " << PRESSURE << "/sumyow;\n";
    if (!unit->third_bodies.empty())
      ost << THIRD_BODY << "[0] = 0.0;\n";
    for (std::set<Species*>::const_iterator it = non_qss_species.begin();
          it != non_qss_species.end(); it++)
    {
      ost << MOLE_FRAC << "[" << (*it)->code_name << "] *= (sumyow * recip_molecular_mass[" << (*it)->code_name << "]);\n"; 
      if (!unit->third_bodies.empty())
        ost << THIRD_BODY << "[0] += " << MOLE_FRAC << "[" << (*it)->code_name << "];\n";
    }
    for (unsigned idx = 1; idx < unit->third_bodies.size(); idx++)
    {
      ost << THIRD_BODY << "[" << idx << "] = " << THIRD_BODY << "[0]"; 
      for (std::map<Species*,double>::const_iterator it = unit->third_bodies[idx]->components.begin();
            it != unit->third_bodies[idx]->components.end(); it++)
      {
        if (!it->first->qss_species)
          ost << " + " << it->second << "*" << MOLE_FRAC << "[" << it->first->code_name << "]";
      }
      ost << ";\n";
    }
    if (!needed_values.empty())
    {
      ost << "if (__any(offset == 0))\n";
      {
        PairDelim if_pair(ost);
        for (std::map<int,Reaction*>::const_iterator it = needed_values.begin();
              it != needed_values.end(); it++)
        {
          Reaction *reac = it->second;
          ost << "pipe_0[" << it->first << "][0][tid] = ";
          if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
            ost << MOLE_FRAC << "[" << reac->pressure_species->code_name << "]";
          else
            ost << THIRD_BODY << "[" << reac->thb->idx << "]";
          ost << ";\n";
        }
        ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        for (std::map<int,Reaction*>::const_iterator it = needed_values.begin();
              it != needed_values.end(); it++)
        {
          Reaction *reac = it->second;
          ost << "pipe_1[" << it->first << "][0][tid] = ";
          if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
            ost << MOLE_FRAC << "[" << reac->pressure_species->code_name << "]";
          else
            ost << THIRD_BODY << "[" << reac->thb->idx << "]";
          ost << ";\n";
        }
        ost << "asm volatile(\"bar.arrive 2," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
  }
  assert(!target_warps.empty());
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    ost << "// Stage " << stage_idx << " of pass " << pass << "\n";
    PairDelim stage_pair(ost);
    // Count how many values there are
    unsigned num_values = 0;
    for (std::vector<Warp*>::const_iterator it = target_warps.begin();
          it != target_warps.end(); it++)
    {
      int reac_idx = (*it)->get_reaction_idx(stage_idx);
      if (reac_idx != -1)
        num_values++;
    }
    // We also need to compute the set of needed mole fraction or third body values that need
    // to be sent back to the reaction warps
    std::map<int,Reaction*> needed_values;
    // If we're on our last stage use the next set of warps, otherwise, use target warps stage+1
    if (stage_idx < (target_warps[0]->reactions.size()-1))
    {
      for (unsigned idx = 0; idx < target_warps.size(); idx++)
      {
        int reac_idx = target_warps[idx]->get_reaction_idx(stage_idx+1);
        if (reac_idx != -1)
        {
          Reaction *reac = target_warps[idx]->get_reaction(reac_idx);
          assert(reac != NULL);
          if (reac->low.enabled)
            needed_values[idx] = reac;
        }
      }
    }
    else
    {
      for (unsigned idx = 0; idx < next_warps.size(); idx++)
      {
        int reac_idx = next_warps[idx]->get_reaction_idx(0);
        if (reac_idx != -1)
        {
          Reaction *reac = next_warps[idx]->get_reaction(reac_idx);
          assert(reac != NULL);
          if (reac->low.enabled)
            needed_values[idx] = reac;
        }
      }
    }
    ost << REAL << " rr_f[" << num_values << "];\n";
    ost << REAL << " rr_r[" << num_values << "];\n";
    ost << "if (__any(offset == 0))\n";
    {
      PairDelim if_pair(ost);
      ost << "asm volatile(\"bar.sync 1," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        // Check for unimportant reaction values in which case we don't even need to do the read
        int reac_idx = target_warps[idx]->get_reaction_idx(stage_idx);
        if (forward_unimportant.find(reac_idx) == forward_unimportant.end())
          ost << "rr_f[" << idx << "] = pipe_0[" << idx << "][0][tid];\n";
        else
          ost << "rr_f[" << idx << "] = 0.0; /* unimportant reaction rate */\n";
        if (backward_unimportant.find(reac_idx) == backward_unimportant.end())
          ost << "rr_r[" << idx << "] = pipe_0[" << idx << "][1][tid];\n";
        else
          ost << "rr_r[" << idx << "] = 0.0; /* unimportant reaction rate */\n";
      }
      if ((pass != 2) || (stage_idx != (target_warps[0]->reactions.size()-1)))
      {
        // Write in any needed values that we need, and then signal that we're done
        for (std::map<int,Reaction*>::const_iterator it = needed_values.begin();
              it != needed_values.end(); it++)
        {
         Reaction *reac = it->second;
          ost << "pipe_0[" << it->first << "][0][tid] = ";
          if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
            ost << MOLE_FRAC << "[" << reac->pressure_species->code_name << "]";
          else
            ost << THIRD_BODY << "[" << reac->thb->idx << "]";
          ost << ";\n"; 
        }
        ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "asm volatile(\"bar.sync 3," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        // Check for unimportant reaction values in which case we don't even need to do the read
        int reac_idx = target_warps[idx]->get_reaction_idx(stage_idx);
        if (forward_unimportant.find(reac_idx) == forward_unimportant.end())
          ost << "rr_f[" << idx << "] = pipe_1[" << idx << "][0][tid];\n";
        else
          ost << "rr_f[" << idx << "] = 0.0; /* unimportant reaction rate */\n";
        if (backward_unimportant.find(reac_idx) == backward_unimportant.end())
          ost << "rr_r[" << idx << "] = pipe_1[" << idx << "][1][tid];\n";
        else
          ost << "rr_r[" << idx << "] = 0.0; /* unimportant reaction rate */\n";
      }
      if ((pass != 2) || (stage_idx != (target_warps[0]->reactions.size()-1)))
      {
        // Write in any needed values that we need, and then signal that we're done
        for (std::map<int,Reaction*>::const_iterator it = needed_values.begin();
              it != needed_values.end(); it++)
        {
         Reaction *reac = it->second;
          ost << "pipe_1[" << it->first << "][0][tid] = ";
          if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
            ost << MOLE_FRAC << "[" << reac->pressure_species->code_name << "]";
          else
            ost << THIRD_BODY << "[" << reac->thb->idx << "]";
          ost << ";\n"; 
        }
        ost << "asm volatile(\"bar.arrive 2," << ((unit->threads_per_point+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
    // Now scale the values by their mole fractions
    for (unsigned widx = 0; widx < num_values; widx++)
    {
      int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
      Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
      assert(reac != NULL);
      for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
            it != reac->forward.end(); it++)
      {
        if (!it->first->qss_species)
        {
          for (int j = 0; j < it->second; j++)
            ost << "rr_f[" << widx << "] *= " << MOLE_FRAC << "[" << it->first->code_name << "];\n";
        }
      }
      if (reac->reversible)
      {
        for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
              it != reac->backward.end(); it++)
        {
          if (!it->first->qss_species)
          {
            for (int j = 0; j < it->second; j++)
              ost << "rr_r[" << widx << "] *= " << MOLE_FRAC << "[" << it->first->code_name << "];\n";
          }
        }
      }
      if ((reac->thb != NULL) && !reac->pressure_dep)
      {
        ost << "rr_f[" << widx << "] *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
        ost << "rr_r[" << widx << "] *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
      }
    }
    // Once we're done scaling, wait until we can write into our target buffer
    // then do the writes and indicate we're done
    ost << "if (__any(offset == 0))\n";
    {
      PairDelim if_pair(ost);
      ost << "asm volatile(\"bar.sync 4," << (2*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "pipe_2[" << idx << "][0][tid] = rr_f[" << idx << "];\n";
        ost << "pipe_2[" << idx << "][1][tid] = rr_r[" << idx << "];\n";
      }
      ost << "asm volatile(\"bar.arrive 5," << (2*32) << ";\" : : : \"memory\");\n";
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "asm volatile(\"bar.sync 6," << (2*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "pipe_3[" << idx << "][0][tid] = rr_f[" << idx << "];\n";
        ost << "pipe_3[" << idx << "][1][tid] = rr_r[" << idx << "];\n";
      }
      ost << "asm volatile(\"bar.arrive 7," << (2*32) << ";\" : : : \"memory\");\n";
    }
  }
  // If this is the stiffness pass, we have to send some additional
  // mole fractions to the reducer warp for the stiffness calculations
  if (pass == 1)
  {
    ost << "if (__any(offset == 0))\n";
    {
      PairDelim if_pair(ost);
      unsigned stif_idx = 0;
      while (stif_idx < stif_operations.size())
      {
        ost << "asm volatile(\"bar.sync 4," << (2*32) << ";\" : : : \"memory\");\n";
        for (unsigned pidx = 0; pidx < (2*unit->threads_per_point); pidx++)
        {
          unsigned index = pidx/2;
          unsigned second = pidx%2;
          ost << "pipe_2[" << index << "][" << second << "][tid] = "
              << MOLE_FRAC << "[" << stif_operations[stif_idx]->species->code_name << "];\n";
          stif_idx++;
          if (stif_idx == stif_operations.size())
            break;
        }
        ost << "asm volatile(\"bar.arrive 5," << (2*32) << ";\" : : : \"memory\");\n";
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      unsigned stif_idx = 0;
      while (stif_idx < stif_operations.size())
      {
        ost << "asm volatile(\"bar.sync 6," << (2*32) << ";\" : : : \"memory\");\n";
        for (unsigned pidx = 0; pidx < (2*unit->threads_per_point); pidx++)
        {
          unsigned index = pidx/2;
          unsigned second = pidx%2;
          ost << "pipe_3[" << index << "][" << second << "][tid] = "
              << MOLE_FRAC << "[" << stif_operations[stif_idx]->species->code_name << "];\n";
          stif_idx++;
          if (stif_idx == stif_operations.size())
            break;
        }
        ost << "asm volatile(\"bar.arrive 7," << (2*32) << ";\" : : : \"memory\");\n";
      }
    }
  }
}

void ChemistryUnit::emit_pipe_qssa_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species)
{
  ost << "// QSSA computation\n";
  PairDelim qssa_pair(ost);
  // Mark that we're ready to start consuming our buffer   
  ost << "if (__any(offset == 0))\n";
  ost << "asm volatile(\"bar.arrive 4," << (2*32) << ";\" : : : \"memory\");\n";
  ost << "else\n";
  ost << "asm volatile(\"bar.arrive 6," << (2*32) << ";\" : : : \"memory\");\n";

  // Emit declarations for all of the variables that we're going to need for this computation
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->emit_pipe_variable_declarations(ost);
  }
  
  // Start pulling values out of the pipe
  assert(!target_warps.empty());
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    // Count how many values are in this stage
    unsigned num_values = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (stage_idx < target_warps[idx]->reactions.size())
        num_values++;
    }
    ost << "// Stage " << stage_idx << "\n"; 
    PairDelim stage_pair(ost);
    ost << REAL << " rr_f[" << num_values << "];\n"; 
    ost << REAL << " rr_r[" << num_values << "];\n";
    // Wait until the values are ready and then read them out
    ost << "if (__any(offset == 0))\n";
    {
      PairDelim if_pair(ost);
      ost << "asm volatile(\"bar.sync 5," << (2*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_2[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_2[" << idx << "][1][tid];\n";
      }
      ost << "asm volatile(\"bar.arrive 4," << (2*32) << ";\" : : : \"memory\");\n";
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "asm volatile(\"bar.sync 7," << (2*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_3[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_3[" << idx << "][1][tid];\n";
      }
      ost << "asm volatile(\"bar.arrive 6," << (2*32) << ";\" : : : \"memory\");\n";
    }
    // Now that we've got our values reduce them out to the values we're computing
    for (unsigned widx = 0; widx < num_values; widx++)
    {
      int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
      assert(reac_idx != -1);
      // Tell each connected component to emit updates to this reaction
      for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
            it != connected_components.end(); it++)
      {
        (*it)->emit_pipe_updates(ost, widx, reac_idx);
      }
    }
  }
  // Now once we've got all the values that we need, do the actual QSSA computation
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->emit_pipe_qssa_computation(ost);
  }
}

void ChemistryUnit::emit_pipe_stiffness_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species)
{
  ost << "// Stiffness computation\n";
  PairDelim stif_pair(ost);
  // Issue the loads for the diffusion values that we'll need
  // Put a threadfence above these so the compiler doesn't do something dumb like try
  // to hoise them and add register pressure
  ost << "__threadfence_block();\n";
  //unsigned upper_bound = next_largest_power(2*non_qss_species,2);
  for (unsigned idx = 0; idx < stif_operations.size(); idx++)
  {
    char src_offset[128];
    //sprintf(src_offset,"((%s+%d*step)%%%d)*spec_stride",stif_operations[idx]->species->code_name,upper_bound,upper_bound);
    sprintf(src_offset,"%s*spec_stride",stif_operations[idx]->species->code_name);
    char dst_offset[128];
    sprintf(dst_offset,"%d",idx);
    emit_cuda_load(ost,"stiffness",dst_offset,DIFFUSION_ARRAY,src_offset,false/*double2*/,".cg");
  }
  // We've already marked previously that we're ready to start consuming the buffer
  // in the last part of the QSSA operation
  ost << REAL << " cdot[" << stif_operations.size() << "];\n";
  ost << REAL << " ddot[" << stif_operations.size() << "];\n";
  ost << "#pragma unroll\n";
  ost << "for (int i = 0; i < " << stif_operations.size() << "; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << " cdot[i] = 0.0;\n";
    ost << " ddot[i] = 0.0;\n";
  }
  // Start pulling values out of the pipe 
  assert(!target_warps.empty());
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    // Count how many values are in this stage
    unsigned num_values = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (stage_idx < target_warps[idx]->reactions.size())
        num_values++;
    }
    ost << "// Stage " << stage_idx << "\n";
    PairDelim stage_pair(ost);
    ost << REAL << " rr_f[" << num_values << "];\n";
    ost << REAL << " rr_r[" << num_values << "];\n";
    ost << "if (__any(offset == 0))\n";
    {
      PairDelim if_pair(ost);
      ost << "asm volatile(\"bar.sync 5," << (2*32) << ";\" : : : \"memory\");\n"; 
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_2[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_2[" << idx << "][1][tid];\n";
      }
      ost << "asm volatile(\"bar.sync 4," << (2*32) << ";\" : : : \"memory\");\n";
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "asm volatile(\"bar.sync 7," << (2*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_3[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_3[" << idx << "][1][tid];\n";
      }
      ost << "asm volatile(\"bar.arrive 6," << (2*32) << ";\" : : : \"memory\");\n";
    }
    for (unsigned widx = 0; widx < num_values; widx++)
    {
      int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
      assert(reac_idx != -1);
      // Update any reaction rate corrections for QSSA
      for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
            it != connected_components.end(); it++)
      {
        (*it)->emit_pipe_corrections(ost, widx, reac_idx); 
      }
      for (unsigned stif_idx = 0; stif_idx < stif_operations.size(); stif_idx++)
      {
        stif_operations[stif_idx]->emit_pipe_updates(ost, stif_idx, widx, reac_idx);
      }
    }
  }
  // Now that we've got all the values emit the computations
  // Do this in blocks of threads_per_point so we can be sent the mole fractions
  // needed for doing the comparison
  {
    unsigned stif_idx = 0;
    while (stif_idx < stif_operations.size())
    {
      // Wait for this set of values to be ready
      ost << "if (__any(offset == 0))\n";
      ost << "asm volatile(\"bar.sync 5," << (2*32) << ";\" : : : \"memory\");\n";
      ost << "else\n";
      ost << "asm volatile(\"bar.sync 7," << (2*32) << ";\" : : : \"memory\");\n";

      for (unsigned pidx = 0; pidx < (2*unit->threads_per_point); pidx++)
      {
        unsigned index = pidx/2;
        unsigned second = pidx%2;
        ost << "// Stiffness for " << stif_operations[stif_idx]->name << "\n";
        PairDelim stif_spec_pair(ost);
        ost << REAL << " " << MOLE_FRAC << ";\n";
        ost << "if (__any(offset == 0))\n";
        // Once we've read the last mole frac indicate that we're done
        if ((pidx == ((2*unit->threads_per_point)-1)) || (stif_idx == (stif_operations.size()-1)))
        {
          PairDelim if_pair(ost);
          ost << MOLE_FRAC << " = pipe_2[" << index << "][" << second << "][tid];\n";
          ost << "asm volatile(\"bar.arrive 4," << (2*32) << ";\" : : : \"memory\");\n";
        }
        else
          ost << MOLE_FRAC << " = pipe_2[" << index << "][" << second << "][tid];\n";
        ost << "else\n";
        if ((pidx == ((2*unit->threads_per_point)-1)) || (stif_idx == (stif_operations.size()-1)))
        {
          PairDelim else_pair(ost);
          ost << MOLE_FRAC << " = pipe_3[" << index << "][" << second << "][tid];\n";
          ost << "asm volatile(\"bar.arrive 6," << (2*32) << ";\" : : : \"memory\");\n";
        }
        else
          ost << MOLE_FRAC << " = pipe_3[" << index << "][" << second << "][tid];\n";

        // Now emit the code to do the stiffness computation
        stif_operations[stif_idx]->emit_pipe_statements(ost, stif_idx, unit); 

        // update the stif index
        stif_idx++;
        if (stif_idx == stif_operations.size())
          break;
      }
    }
  }
}

void ChemistryUnit::emit_pipe_output_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species)
{
  // Do the output computation for all the species
  ost << "// Species output computation\n";
  PairDelim output_pair(ost);
  ost << REAL << " spec_out[" << non_qss_species << "];\n";
  ost << "#pragma unroll\n";
  ost << "for (int i = 0; i < " << non_qss_species << "; i++)\n";
  ost << "spec_out[i] = 0.0;\n";
  // Build a map of the species and the number of updates they need to
  // see before they can be written
  std::map<Species*,int/*remaining updates*/> species_updates;
  for (std::set<Species*>::const_iterator it = unit->species.begin();
        it != unit->species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if ((*it)->qss_species)
      continue;
    species_updates[(*it)] = (*it)->reaction_contributions.size();
  }
  assert(species_updates.size() == non_qss_species);
  // Emit the writes for any species who are already done
  unsigned total_writes = 0;
  for (std::map<Species*,int>::const_iterator it = species_updates.begin();
        it != species_updates.end(); it++)
  {
    if (it->second == 0)
    {
      // No need to apply the non-dimensionalized scaling factor here since we know the value is zero
      ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << WDOT << "+"
          << it->first->code_name << "*spec_stride), \"d\"(spec_out[" << it->first->code_name << "]) : \"memory\");\n";
      total_writes++;
    }
  }
  assert(!target_warps.empty());
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    // Count how many values are in this stage
    unsigned num_values = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (stage_idx < target_warps[idx]->reactions.size())
        num_values++;
    }
    ost << "// Stage " << stage_idx << "\n";
    PairDelim stage_pair(ost);
    ost << REAL << " rr_f[" << num_values << "];\n";
    ost << REAL << " rr_r[" << num_values << "];\n";
    ost << "if (__any(offset == 0))\n";
    {
      PairDelim if_pair(ost);
      ost << "asm volatile(\"bar.sync 5," << (2*32) << ";\" : : : \"memory\");\n"; 
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_2[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_2[" << idx << "][1][tid];\n";
      }
      // No need to emit this on the last pass since we'll get it the next time around the loop
      if (stage_idx < (target_warps[0]->reactions.size()-1))
        ost << "asm volatile(\"bar.arrive 4," << (2*32) << ";\" : : : \"memory\");\n";
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "asm volatile(\"bar.sync 7," << (2*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_3[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_3[" << idx << "][1][tid];\n";
      }
      // No need to emit this on the last pass since we'll get it the next time around the loop
      if (stage_idx < (target_warps[0]->reactions.size()-1))
        ost << "asm volatile(\"bar.arrive 6," << (2*32) << ";\" : : : \"memory\");\n";
    }
    for (unsigned widx = 0; widx < num_values; widx++)
    {
      int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
      assert(reac_idx != -1);
      // Update any reaction rate corrections for QSSA
      for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
            it != connected_components.end(); it++)
      {
        (*it)->emit_pipe_corrections(ost, widx, reac_idx); 
      }
      // Update any reaction rate corrections for stiffness  
      for (unsigned stif_idx = 0; stif_idx < stif_operations.size(); stif_idx++)
      {
        stif_operations[stif_idx]->emit_pipe_corrections(ost, stif_idx, widx, reac_idx);
      }
      Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
      assert(reac != NULL);
      ost << REAL << " ropl_" << reac_idx << " = rr_f[" << widx << "] - rr_r[" << widx << "];\n";
      // Update all the species 
      for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
            it != reac->stoich.end(); it++)
      {
        if (it->first->qss_species)
          continue;
        if (it->second == 1)
          ost << "spec_out[" << it->first->code_name << "] += ropl_" << reac_idx << ";\n";
        else if (it->second == -1)
          ost << "spec_out[" << it->first->code_name << "] -= ropl_" << reac_idx << ";\n";
        else
          ost << "spec_out[" << it->first->code_name << "] = __fma_rn(" << double(it->second)
              << ",ropl_" << reac_idx << ",spec_out[" << it->first->code_name << "]);\n";
        assert(species_updates.find(it->first) != species_updates.end());
        assert(species_updates[it->first] > 0);
        species_updates[it->first]--;
        if (species_updates[it->first] == 0)
        {
          // This species has seen all it's updates, so we can write out it's value
          if (!unit->no_nondim)
            ost << "spec_out[" << it->first->code_name << "] *= " << (it->first->molecular_mass/1000.0) << ";\n";
          ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << WDOT << "+"
              << it->first->code_name << "*spec_stride), \"d\"(spec_out[" << it->first->code_name << "]) : \"memory\");\n";
          total_writes++;
        }
      }
    }
  }
  // make sure we wrote out all the species
  assert(total_writes == non_qss_species);
}

void ChemistryUnit::emit_pipe_arrhenius(CodeOutStream &ost, Warp *warp, bool emit_code, bool weird, unsigned stage_idx,
                                        const char *target, double a, double beta, double e, int phase, int pass)
{
  assert(phase < 2);
  const bool second_pass = (phase == 1);
  const int warp_size = 32;
  bool emit_constants = true;
  int a_off = -1;
  int beta_off = -1;
  int e_off = -1;
  if (warp->can_add_constants(stage_idx, 3, second_pass))
  {
    emit_constants = false;
    beta_off = warp->add_constant(stage_idx, beta, second_pass);
    // Make it so we are always doing addition 
    // by negating the value of e
    e_off = warp->add_constant(stage_idx, -e, second_pass);
    if (!unit->no_nondim)
      a_off = warp->add_constant(stage_idx, a*pow(TEMPERATURE_REF,beta), second_pass);
    else
      a_off = warp->add_constant(stage_idx, a, second_pass);
  }
  else if (!weird)
    assert(false); // if we're not doing a weird reaction then this is really bad
  // If we're not emitting code, then we are done
  if (!emit_code)
    return;
  // Otherwise do the code generation
  if (emit_constants)
  {
    ost << target << " = ";
    if (!unit->no_nondim)
      ost << (a*pow(TEMPERATURE_REF,beta));
    else
      ost << a;
    if (phase > -1)
      ost << " * exp(" << beta << "*vlntemp_" << phase;
    else
      ost << " * exp(" << beta << "*vlntemp";
    if (e < 0.)
      ost << " + " << -e;
    else
      ost << " - " << e;
    if (phase > -1)
      ost << "*ortc_" << phase << ");\n";
    else
      ost << "*ortc);\n";
  }
  else
  {
    PairDelim pair(ost);
#ifndef TARGET_FERMI
    // figure out which thread in our warp owns these constants
    ost << INT << " hi_part, lo_part;\n";
    // Get the beta first
    assert(beta_off != -1);
    int location = stage_idx*MAX_REACTION_CONSTANTS + beta_off;
    int register_index = location/warp_size;
    int lane_index = location%warp_size;
    ost << "hi_part = __shfl(__double2hiint(pass_" << pass << "_constants[" << register_index << "]), " 
        << lane_index << ", " << warp_size << ");\n";
    ost << "lo_part = __shfl(__double2loint(pass_" << pass << "_constants[" << register_index << "]), " 
        << lane_index << ", " << warp_size << ");\n";
    if (phase > -1)
      ost << REAL << " arrhenius = __hiloint2double(hi_part,lo_part) * vlntemp_" << phase << ";\n";
    else
      ost << REAL << " arrhenius = __hiloint2double(hi_part,lo_part) * vlntemp;\n";
    // Now get the e value
    assert(e_off != -1);
    location = stage_idx*MAX_REACTION_CONSTANTS + e_off;
    register_index = location/warp_size;
    lane_index = location%warp_size;
    ost << "hi_part = __shfl(__double2hiint(pass_" << pass << "_constants[" << register_index << "]), " 
        << lane_index << ", " << warp_size << ");\n";
    ost << "lo_part = __shfl(__double2loint(pass_" << pass << "_constants[" << register_index << "]), " 
        << lane_index << ", " << warp_size << ");\n";
    if (phase > -1)
      ost << "arrhenius = __fma_rn(__hiloint2double(hi_part,lo_part),ortc_" << phase << ",arrhenius);\n";
    else
      ost << "arrhenius = __fma_rn(__hiloint2double(hi_part,lo_part),ortc,arrhenius);\n";
    ost << "arrhenius = exp(arrhenius);\n";
    // finally get the a value
    assert(a_off != -1);
    location = stage_idx*MAX_REACTION_CONSTANTS + a_off;
    register_index = location/warp_size;
    lane_index = location%warp_size;
    ost << "hi_part = __shfl(__double2hiint(pass_" << pass << "_constants[" << register_index << "]), " 
        << lane_index << ", " << warp_size << ");\n";
    ost << "lo_part = __shfl(__double2loint(pass_" << pass << "_constants[" << register_index << "]), " 
        << lane_index << ", " << warp_size << ");\n";
    ost << target << " = " << "__hiloint2double(hi_part,lo_part) * arrhenius;\n";
#else
    // Beta first
    assert(beta_off != -1);
    int location = stage_idx*MAX_REACTION_CONSTANTS + beta_off;
    ost << REAL << " beta_val = warp_constants_" << pass << "[wid][" << location << "+step*step_stride];\n";
    if (phase > -1)
      ost << REAL << " arrhenius = beta_val * vlntemp_" << phase << ";\n";
    else
      ost << REAL << " arrhenius = beta_val * vlntemp;\n";
    // Now get the e value
    assert(e_off != -1);
    location = stage_idx*MAX_REACTION_CONSTANTS + e_off;
    ost << REAL << " e_val = warp_constants_" << pass << "[wid][" << location << "+step*step_stride];\n";
    if (phase > -1)
      ost << "arrhenius = __fma_rn(e_val,ortc_" << phase << ",arrhenius);\n";
    else
      ost << "arrhenius = __fma_rn(e_val,ortc,arrhenius);\n";
    ost << "arrhenius = exp(arrhenius);\n";
    // Finally do the a value
    assert(a_off != -1);
    location = stage_idx*MAX_REACTION_CONSTANTS + a_off;
    ost << REAL << " a_val = warp_constants_" << pass << "[wid][" << location << "+step*step_stride];\n";
    ost << target << " = a_val * arrhenius;\n";
#endif
  }

}

void ChemistryUnit::emit_pipe_general_constants(CodeOutStream &ost, unsigned num_species)
{
  // First go through all the species and emit the definitions for their code names
  {
    ost << "\n";
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      ost << "#define " << (*it)->code_name << "  " << spec_idx << "\n";
      spec_idx++;
    }
    ost << "#define NUM_SPECIES  " << num_species << "\n";
    ost << "#define NUM_QSS      " << (unit->species.size()-(1+num_species)) << "\n";
    ost << "#define NUM_STIF     " << stif_operations.size() << "\n";
    ost << "#define THB_OFFSET   (NUM_SPECIES)\n";
    ost << "#define TROE_OFFSET  (THB_OFFSET+" << unit->third_bodies.size() << ")\n";
    ost << "#define GIBBS_OFFSET (TROE_OFFSET+" << needed_troe_reactions.size() << ")\n";
    if (gibbs_species.size() <= gibbs_reactions.size())
      ost << "#define QSS_OFFSET   (GIBBS_OFFSET+" << gibbs_species.size() << ")\n";
    else
      ost << "#define QSS_OFFSET   (GIBBS_OFFSET+" << gibbs_reactions.size() << ")\n";
    ost << "#define AVAL_OFFSET  (QSS_OFFSET+NUM_QSS)\n";
    ost << "\n\n";
  }
  // Now emit the molecular masses and the inverse molecular masses
  {
    unsigned spec_idx = 0;
    ost << "__constant__ double molecular_mass[" << num_species << "] = {";
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      if (spec_idx > 0)
        ost << ", ";
      ost << (*it)->molecular_mass;
      spec_idx++;
    }
    ost << "};\n\n";
  }
  {
    unsigned spec_idx = 0;
    ost << "__constant__ double recip_molecular_mass[" << num_species << "] = {";
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      if (spec_idx > 0)
        ost << ", ";
      ost << (1.0/((*it)->molecular_mass));
      spec_idx++;
    }
    ost << "};\n\n";
  }
  // emit the gibbs values that we need
  {
    ost << "__constant__ " << REAL << " gibbs_values[" << gibbs_species.size() << "][15] = {";
    for (unsigned idx = 0; idx < gibbs_species.size(); idx++)
    {
      if (idx > 0)
        ost << ", ";
      ost << "{";
      Species *spec = gibbs_species[idx];
      ost << spec->get_common_temperature();
      for (int i = 0; i < 7; i++)
        ost << ", " << spec->get_high_coefficient(i);
      for (int i = 0; i < 7; i++)
        ost << ", " << spec->get_low_coefficient(i);
      ost << "}";
    }
    ost << "};\n\n";
  }
  // emit the troe constant values that we need
  {
    ost << "__constant__ " << REAL << " troe_values[" << needed_troe_reactions.size() << "][6] = {";
    for (unsigned idx = 0; idx < needed_troe_reactions.size(); idx++)
    {
      if (idx > 0)
        ost << ", ";
      ost << "{";
      Reaction *reac = needed_troe_reactions[idx];
      ost << (1.0 - reac->troe.a);
      if (!unit->no_nondim)
        ost << ", " << (-1.0*TEMPERATURE_REF/reac->troe.t3);
      else
        ost << ", " << (-1.0/reac->troe.t3);
      ost << ", " << reac->troe.a;
      if (reac->troe.a != 0.0)
      {
        if (!unit->no_nondim)
          ost << ", " << (-1.0*TEMPERATURE_REF/reac->troe.t1);
        else
          ost << ", " << (-1.0/reac->troe.t1);
      }
      else
        ost << ", 0.0";
      if (reac->troe.num == 4)
      {
        if (!unit->no_nondim)
          ost << ", " << (-(reac->troe.t2)/TEMPERATURE_REF);
        else
          ost << ", " << (-reac->troe.t2);
        ost << ", 0.0";
      }
      else
      {
        ost << ", 0.0, -1.0";
      }
      ost << "}";
    }
    ost << "};\n\n";
  }
}

void ChemistryUnit::emit_pipe_warp_constants(CodeOutStream &ost, const std::vector<Warp*> &target_warps, int pass,
                                              unsigned &constants_per_thread, unsigned &warp_constant_stride)
{
  ost << "\n// Constants for pass " << pass << "\n\n";
  // Emit the gibbs species for each of the warps
  {
    unsigned max_gibbs = 0;
    for (std::vector<Warp*>::const_iterator it = target_warps.begin();
          it != target_warps.end(); it++)
    {
      if ((*it)->gibbs_species.size() > max_gibbs)
        max_gibbs = (*it)->gibbs_species.size();
    }
    if (max_gibbs > 0)
    {
      ost << "__constant__ " << INT << " gibbs_index_" << pass << "[" << max_gibbs << "][" << target_warps.size() << "] = {";
      for (unsigned idx = 0; idx < max_gibbs; idx++)
      {
        if (idx > 0)
          ost << ", ";
        ost << "{";
        for (std::vector<Warp*>::const_iterator it = target_warps.begin();
              it != target_warps.end(); it++)
        {
          (*it)->emit_gibbs_index(ost, idx);
        }
        ost << "}";
      }
      ost << "};\n\n";

      ost << "__constant__ " << REAL << " gibbs_scale_" << pass << "[" << max_gibbs << "][" << target_warps.size() << "] = {";
      for (unsigned idx = 0; idx < max_gibbs; idx++)
      {
        if (idx > 0)
          ost << ", ";
        ost << "{";
        for (std::vector<Warp*>::const_iterator it = target_warps.begin();
              it != target_warps.end(); it++)
        {
          (*it)->emit_gibbs_scale(ost, idx);
        }
        ost << "}";
      }
      ost << "};\n\n";
    }
  }
  // Emit the array of index constants for the troe reactions
  {
    unsigned max_troe = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (target_warps[idx]->troe_reactions.size() > max_troe)
        max_troe = target_warps[idx]->troe_reactions.size();
    }
    if (max_troe > 0)
    {
      ost << "__constant__ " << INT << " troe_index_" << pass << "[" << max_troe << "][" << target_warps.size() << "] = {";
      for (unsigned idx = 0; idx < max_troe; idx++)
      {
        if (idx > 0)
          ost << ", ";
        ost << "{";
        for (std::vector<Warp*>::const_iterator it = target_warps.begin();
              it != target_warps.end(); it++)
        {
          (*it)->emit_troe_index(ost, idx);
        }
        ost << "}";
      }
      ost << "};\n\n";
    }
  }
  // Emit the array of reverse constants
  {
    unsigned max_reverse = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (target_warps[idx]->reverse_constants.size() > max_reverse)
        max_reverse = target_warps[idx]->reverse_constants.size();
    }
    if (max_reverse > 0)
    {
      ost << "__constant__ " << REAL << " reverse_pass_" << pass << "[" << max_reverse << "][" << target_warps.size() << "] = {";
      for (unsigned idx = 0; idx < max_reverse; idx++)
      {
        if (idx > 0)
          ost << ", ";
        ost << "{";
        for (std::vector<Warp*>::const_iterator it = target_warps.begin();
              it != target_warps.end(); it++)
        {
          (*it)->emit_reverse_constant(ost, idx);
        }
        ost << "}";
      }
      ost << "};\n\n";
    }
  }
  // emit the array of stif species index values
  {
    unsigned max_stif_species = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (target_warps[idx]->stif_species.size() > max_stif_species)
        max_stif_species = target_warps[idx]->stif_species.size();
    }
    if (max_stif_species > 0)
    {
      ost << "__constant__ " << INT << " stiff_index_" << pass << "[" << max_stif_species << "][" << target_warps.size() << "] = {";
      for (unsigned idx = 0; idx < max_stif_species; idx++)
      {
        if (idx > 0)
          ost << ", ";
        ost << "{";
        for (std::vector<Warp*>::const_iterator it = target_warps.begin();
              it != target_warps.end(); it++)
        {
          (*it)->emit_stiff_index(ost, idx);
        }
        ost << "}";
      }
      ost << "};\n\n";
    }
  }
  // Emit the array of reaction constants for each warp
#ifndef TARGET_FERMI
  {
    assert(!target_warps.empty());
    // Find the warp with the most constants
    warp_constant_stride = target_warps[0]->num_reactions()*MAX_REACTION_CONSTANTS;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if ((target_warps[idx]->num_reactions()*MAX_REACTION_CONSTANTS) > warp_constant_stride)
        warp_constant_stride = target_warps[idx]->num_reactions()*MAX_REACTION_CONSTANTS;
    }
    // round up to the nearest multiple of 32 which will be how many
    // constants are stored in each thread of a warp
    while ((warp_constant_stride%32) != 0)
      warp_constant_stride++;
    constants_per_thread = warp_constant_stride/32;
    ost << "__device__ const " << REAL << " warp_constants_" << pass << "[" << (target_warps.size()*warp_constant_stride) << "] = {";
    bool first = true;
    for (std::vector<Warp*>::const_iterator it = target_warps.begin();
          it != target_warps.end(); it++)
    {
      unsigned emitted_warp_constants = (*it)->emit_constants(ost,first);
      // Pad the rest of the entries with zeros
      for ( ; emitted_warp_constants < warp_constant_stride; emitted_warp_constants++)
        ost << ", 0.0";
      first = false;
    }
    ost << "};\n\n";
  }
#else
  {
    assert(!target_warps.empty());
    // Find the warp with the most constants
    warp_constant_stride = target_warps[0]->num_reactions()*MAX_REACTION_CONSTANTS;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if ((target_warps[idx]->num_reactions()*MAX_REACTION_CONSTANTS) > warp_constant_stride)
        warp_constant_stride = target_warps[idx]->num_reactions()*MAX_REACTION_CONSTANTS;
    }
    // round up to the nearest multiple of 32 which will be how many
    // constants are stored in each thread of a warp
    while ((warp_constant_stride%32) != 0)
      warp_constant_stride++;
    constants_per_thread = warp_constant_stride/32;
    ost << "__device__ const " << REAL << " warp_constants_" << pass << "[" << target_warps.size() << "]["
        << warp_constant_stride << "] = {";
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (idx == 0)
        ost << " {";
      else
        ost << ", {";
      unsigned emitted_warp_constants = target_warps[idx]->emit_constants(ost, true/*first*/);
      for ( ; emitted_warp_constants < warp_constant_stride; emitted_warp_constants++)
        ost << ", 0.0";
      ost << "}";
    }
    ost << "};\n\n";
  }
#endif
}

void ChemistryUnit::emit_pipe_fine_scaling_code(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species,
                                                int pass, int phase, bool even, const std::vector<Warp*> &next_warps)
{
  ost << "// Phase " << phase << " of pass " << pass << "\n";
  if (pass == 0)
  {
    if (phase == 0)
      ost << "asm volatile(\"bar.sync 12," << ((2*MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
    else
      ost << "asm volatile(\"bar.sync 13," << ((2*MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
    // Figure out if there are any mole fraction or third body values that need
    // to be sent to the reaction warps, if not, we can do the barrier now
    std::vector<std::map<int,Reaction*> > needed_values(MOLE_FRAC_WARPS);
    for (unsigned midx = 0; midx < MOLE_FRAC_WARPS; midx++)
    {
      for (unsigned widx = midx; widx < target_warps.size(); widx+=MOLE_FRAC_WARPS)
      {
        if (target_warps[widx]->reactions.size() > (even ? 0 : 1))
        {
          int reac_idx = target_warps[widx]->get_reaction_idx((even ? 0 : 1));
          Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
          assert(reac != NULL);
          // Check to see if it needs a mole frac value
          if (reac->low.enabled)
          {
            needed_values[midx][widx] = reac;
          }
        }
      }
    }
    // Emit the first set of writes
    bool first = true;
    for (unsigned midx = 0; midx < MOLE_FRAC_WARPS; midx++)
    {
      if (needed_values[midx].empty())
        continue;
      if (first)
        ost << "if (offset == " << midx << ")\n";
      else
        ost << "else if (offset == " << midx << ")\n";
      PairDelim write_pair(ost);
      for (std::map<int,Reaction*>::const_iterator it = needed_values[midx].begin();
            it != needed_values[midx].end(); it++)
      {
        Reaction *reac = it->second;
        if (even)
        {
          ost << "pipe_0[" << it->first << "][0][tid] = ";
          if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
            ost << MOLE_FRAC << "_" << phase << "[" << reac->pressure_species->code_name << "][tid]";
          else
            ost << THIRD_BODY << "_" << phase << "[" << reac->thb->idx << "][tid]";
          ost << ";\n";
        }
        else
        {
          ost << "pipe_1[" << it->first << "][0][tid] = ";
          if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
            ost << MOLE_FRAC << "_" << phase << "[" << reac->pressure_species->code_name << "][tid]";
          else
            ost << THIRD_BODY << "_" << phase << "[" << reac->thb->idx << "][tid]";
          ost << ";\n";
        }
      }
      first = false;
    }
    // Now we can mark that the values are ready
    if (even)
      ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
    else
      ost << "asm volatile(\"bar.arrive 2," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
  }
  // Before starting the second pass read in the xq_values and the stiffness values from
  // the warp that we're about to be reducing to
  if (pass == 2)
  {
    unsigned num_qssa_vars = ((unit->species.size()-1)-non_qss_species);
    // Emit the declaration for the arrays
    if (phase == 0)
    {
      ost << REAL << " xq_values[" << num_qssa_vars << "];\n";
      ost << REAL << " stiffness[" << stif_operations.size() << "];\n";
    }
    unsigned num_qssa_xfers = (num_qssa_vars + (2*unit->threads_per_point-1))/(2*unit->threads_per_point);
    ost << "// Transfering QSSA in " << num_qssa_xfers << " transfers\n";
    for (unsigned idx = 0; idx < num_qssa_xfers; idx++)
    {
      // Wait for the values to be ready then read them out
      if (even)
      {
        if (phase == 0)
          ost << "asm volatile(\"bar.sync 4," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.sync 8," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
      else
      {
        if (phase == 0)
          ost << "asm volatile(\"bar.sync 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.sync 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
      for (unsigned local_idx = 0; local_idx < (2*unit->threads_per_point); local_idx++)
      {
        unsigned qidx = idx*2*unit->threads_per_point + local_idx;
        if (qidx >= num_qssa_vars)
          break;
        unsigned first = local_idx/2;
        unsigned second = local_idx%2;
        ost << "xq_values[" << qidx << "] = ";
        if (phase == 0)
          ost << "pipe_2";
        else
          ost << "pipe_3";
        ost << "[" << first << "][" << second << "][tid];\n";
      }
      // Mark that we've read out all the values
      if (even)
      {
        if (phase == 0)
          ost << "asm volatile(\"bar.arrive 5," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.arrive 9," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
      else
      {
        if (phase == 0)
          ost << "asm volatile(\"bar.arrive 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.arrive 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
    // Now do the stiffness transfers
    unsigned num_stif_xfers = (stif_operations.size() + (2*unit->threads_per_point)-1)/(2*unit->threads_per_point);
    ost << "// Transfer Stiffness in " << num_stif_xfers << "\n";
    for (unsigned idx = 0; idx < num_stif_xfers; idx++)
    {
      if (even)
      {
        if (phase == 0)
          ost << "asm volatile(\"bar.sync 4," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.sync 8," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
      else
      {
        if (phase == 0)
          ost << "asm volatile(\"bar.sync 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.sync 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      } 
      for (unsigned local_idx = 0; local_idx < (2*unit->threads_per_point); local_idx++)
      {
        unsigned qidx = idx*2*unit->threads_per_point + local_idx;
        if (qidx >= stif_operations.size())
          break;
        unsigned first = local_idx/2;
        unsigned second = local_idx%2;
        ost << "stiffness[" << qidx << "] = ";
        if (phase == 0)
          ost << "pipe_2";
        else
          ost << "pipe_3";
        ost << "[" << first << "][" << second << "][tid];\n";
      }
      // Mark that we're done, unless it is the last transfer in which case we'll hold onto the buffer
      if (idx < (num_stif_xfers-1))
      {
        if (even)
        {
          if (phase == 0)
            ost << "asm volatile(\"bar.arrive 5," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
          else
            ost << "asm volatile(\"bar.arrive 9," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        }
        else
        {
          if (phase == 0)
            ost << "asm volatile(\"bar.arrive 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
          else
            ost << "asm volatile(\"bar.arrive 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        }
      }
    }
  }
  assert(!target_warps.empty());
  for (unsigned stage_idx = (even ? 0 : 1); stage_idx < target_warps[0]->reactions.size(); stage_idx+=2)
  {
    ost << "// Stage " << stage_idx << " of phase " << phase << " of pass " << pass << "\n";
    PairDelim stage_pair(ost);
    // Figure out any needed values that we need
    std::vector<std::map<int,Reaction*> > needed_values(MOLE_FRAC_WARPS);
    if (stage_idx < (target_warps[0]->reactions.size()-2))
    {
      for (unsigned midx = 0; midx < MOLE_FRAC_WARPS; midx++)
      {
        for (unsigned widx = midx; widx < target_warps.size(); widx+=MOLE_FRAC_WARPS)
        {
          if (target_warps[widx]->reactions.size() > (stage_idx+2))
          {
            int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx+2);
            Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
            assert(reac != NULL);
            // Check to see if it needs a mole frac value
            if (reac->low.enabled)
            {
              needed_values[midx][widx] = reac;
            }
          }
        }
      }
    }
    else
    {
      for (unsigned midx = 0; midx < MOLE_FRAC_WARPS; midx++)
      {
        for (unsigned widx = midx; widx < next_warps.size(); widx+=MOLE_FRAC_WARPS)
        {
          if (next_warps[widx]->reactions.size() > (even ? 0 : 1))
          {
            int reac_idx = next_warps[widx]->get_reaction_idx((even ? 0 : 1));
            Reaction *reac = next_warps[widx]->get_reaction(reac_idx);
            assert(reac != NULL);
            // Check to see if it needs a mole frac value
            if (reac->low.enabled)
            {
              needed_values[midx][widx] = reac;
            }
          }
        }
      }
    }
    // Count how many values there are
    unsigned num_values = 0;
    for (std::vector<Warp*>::const_iterator it = target_warps.begin();
          it != target_warps.end(); it++)
    {
      int reac_idx = (*it)->get_reaction_idx(stage_idx);
      if (reac_idx != -1)
        num_values++;
    }
    unsigned values_per_thread = (num_values + (MOLE_FRAC_WARPS-1))/MOLE_FRAC_WARPS;
    bool uniform_values = ((num_values%MOLE_FRAC_WARPS) == 0);
    ost << REAL << " rr_f[" << values_per_thread << "];\n";
    ost << REAL << " rr_r[" << values_per_thread << "];\n";
    if (even)
      ost << "asm volatile(\"bar.sync 1," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
    else
      ost << "asm volatile(\"bar.sync 3," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
    // Read out the values
    for (unsigned idx = 0; idx < values_per_thread; idx++)
    {
      if (!uniform_values && (idx == (values_per_thread-1)))
        ost << "if ((offset+" << (idx*MOLE_FRAC_WARPS) << ") < " << unit->threads_per_point << ")\n";
      PairDelim read_pair(ost);
      ost << "rr_f[" << idx << "] = ";
      if (even)
        ost << "pipe_0";
      else
        ost << "pipe_1";
      ost << "[offset+" << (idx*MOLE_FRAC_WARPS) << "][0][tid];\n";
      ost << "rr_r[" << idx << "] = ";
      if (even)
        ost << "pipe_0";
      else
        ost << "pipe_1";
      ost << "[offset+" << (idx*MOLE_FRAC_WARPS) << "][1][tid];\n";
    }
    // Emit any writes to send back to the next level
    if ((pass != 2) || (phase != 1) || (stage_idx < (target_warps[0]->reactions.size()-2)))
    {
      bool first = true;
      for (unsigned midx = 0; midx < MOLE_FRAC_WARPS; midx++)
      {
        if (needed_values[midx].empty())
          continue;
        if (first)
          ost << "if (offset == " << midx << ")\n";
        else
          ost << "else if (offset == " << midx << ")\n";
        PairDelim write_pair(ost);
        for (std::map<int,Reaction*>::const_iterator it = needed_values[midx].begin();
              it != needed_values[midx].end(); it++)
        {
          Reaction *reac = it->second;
          if (even)
          {
            ost << "pipe_0[" << it->first << "][0][tid] = ";
            if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
              ost << MOLE_FRAC << "_" << phase << "[" << reac->pressure_species->code_name << "][tid]";
            else
              ost << THIRD_BODY << "_" << phase << "[" << reac->thb->idx << "][tid]";
            ost << ";\n";
          }
          else
          {
            ost << "pipe_1[" << it->first << "][0][tid] = ";
            if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
              ost << MOLE_FRAC << "_" << phase << "[" << reac->pressure_species->code_name << "][tid]";
            else
              ost << THIRD_BODY << "_" << phase << "[" << reac->thb->idx << "][tid]";
            ost << ";\n";
          }
        }
        first = false;
      }
      // Now we can inidcate that the buffer is ready again
      if (even)
        ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
      else
        ost << "asm volatile(\"bar.arrive 2," << ((unit->threads_per_point+MOLE_FRAC_WARPS)*32) << ";\" : : : \"memory\");\n";
    }
    // Now handle the scaling of the things that we own
    bool first = true;
    for (unsigned midx = 0; midx < MOLE_FRAC_WARPS; midx++)
    {
      if (first)
        ost << "if (offset == " << midx << ")\n";
      else
        ost << "else if (offset == " << midx << ")\n";
      unsigned local_idx = 0;
      PairDelim scale_pair(ost);
      for (unsigned widx = midx; widx < num_values; widx += MOLE_FRAC_WARPS, local_idx++)
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
        Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
        assert(reac != NULL);
        // check for any unimportant values
        if (forward_unimportant.find(reac_idx) != forward_unimportant.end())
          ost << "rr_f[" << local_idx << "] = 0.0; /* unimportant rate */\n";
        else
        {
          for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
                it != reac->forward.end(); it++)
          {
            if (!it->first->qss_species)
            {
              for (int j = 0; j < it->second; j++)
                ost << "rr_f[" << local_idx << "] *= " << MOLE_FRAC << "_" << phase << "[" << it->first->code_name << "][tid];\n";
            }
          }
        }
        if (backward_unimportant.find(reac_idx) != backward_unimportant.end())
          ost << "rr_r[" << local_idx << "] = 0.0; /* unimportant rate */\n";
        else
        {
          if (reac->reversible)
          {
            for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                  it != reac->backward.end(); it++)
            {
              if (!it->first->qss_species)
              {
                for (int j = 0; j < it->second; j++)
                  ost << "rr_r[" << local_idx << "] *= " << MOLE_FRAC << "_" << phase << "[" << it->first->code_name << "][tid];\n";
              }
            }
          }
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          ost << "rr_f[" << local_idx << "] *= " << THIRD_BODY << "_" << phase << "[" << reac->thb->idx << "][tid];\n";
          ost << "rr_r[" << local_idx << "] *= " << THIRD_BODY << "_" << phase << "[" << reac->thb->idx << "][tid];\n";
        }
        // For the second pass also do the updates for qssa and stiffness
        // since we have these values now
        if (pass == 2)
        {
          for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
                it != connected_components.end(); it++)
          {
            (*it)->emit_pipe_corrections(ost, local_idx, reac_idx); 
          }
          for (unsigned idx = 0; idx < stif_operations.size(); idx++)
          {
            stif_operations[idx]->emit_pipe_corrections(ost, idx, local_idx, reac_idx);
          }
        }
      }
      first = false;
    }
    // Wait until we can write our values
    // Don't need to wait if this is the last pass and the first stage
    // since we already waited for getting the xq_values and stiffness values
    if ((pass != 2) || (stage_idx != (even ? 0 : 1)))
    {
      if (even)
      {
        if (phase == 0)
          ost << "asm volatile(\"bar.sync 4," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.sync 8," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
      else
      {
        if (phase == 0)
          ost << "asm volatile(\"bar.sync 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.sync 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
    // Write our values out
    for (unsigned idx = 0; idx < values_per_thread; idx++)
    {
      if (!uniform_values && (idx == (values_per_thread-1)))
        ost << "if ((offset+" << (idx*MOLE_FRAC_WARPS) << ") < " << unit->threads_per_point << ")\n";
      PairDelim write_pair(ost);
      if (phase == 0)
        ost << "pipe_2";
      else
        ost << "pipe_3";
      ost << "[offset+" << (idx*MOLE_FRAC_WARPS) << "][0][tid] = rr_f[" << idx << "];\n";
      if (phase == 0)
        ost << "pipe_2";
      else
        ost << "pipe_3";
      ost << "[offset+" << (idx*MOLE_FRAC_WARPS) << "][1][tid] = rr_r[" << idx << "];\n";
    }
    // Signal that we're done
    if (even)
    {
      if (phase == 0)
        ost << "asm volatile(\"bar.arrive 5," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      else
        ost << "asm volatile(\"bar.arrive 9," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
    }
    else
    {
      if (phase == 0)
        ost << "asm volatile(\"bar.arrive 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      else
        ost << "asm volatile(\"bar.arrive 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
    }
  }
}

void ChemistryUnit::emit_pipe_fine_qssa_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned num_non_qss_species)
{
  // First do the mole fraction computation and put it into shared memory
  {
    // emit the loads for the values that we need
    PairDelim load_pair(ost);
    ost << REAL << " " << TEMPERATURE << ", " << PRESSURE << ";\n";
    ost << REAL << " " << MOLE_FRAC << "[" << num_non_qss_species << "];\n";
    ost << REAL << " " << THIRD_BODY << "[" << unit->third_bodies.size() << "];\n";

    emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,false/*double2*/,".cg");
    std::set<Species*> non_qss_species;
    //unsigned upper_bound = next_largest_power(2*num_non_qss_species,2);
    for (std::set<Species*>::const_iterator it = unit->species.begin();
          it != unit->species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      char src_offset[128];
      //sprintf(src_offset,"((%s+step*%d)%%%d)*spec_stride",(*it)->code_name,upper_bound,upper_bound);
      sprintf(src_offset,"%s*spec_stride",(*it)->code_name);
      emit_cuda_load(ost,MOLE_FRAC,(*it)->code_name,MASS_FRAC_ARRAY,src_offset,false/*double2*/,".cg");
      non_qss_species.insert(*it);
    }
    emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,false/*double2*/,".cg");
    ost << REAL << " sumyow = 0.0;\n";
    for (std::set<Species*>::const_iterator it = non_qss_species.begin();
          it != non_qss_species.end(); it++)
    {
      ost << "sumyow = __fma_rn(" << MOLE_FRAC << "[" << (*it)->code_name 
          << "],recip_molecular_mass[" << (*it)->code_name << "],sumyow);\n";
    }
    ost << "sumyow *= (";
    if (unit->no_nondim)
      ost << "8.314510e+07 * " << TEMPERATURE;
    else
      ost << (8.314510e+07 * TEMPERATURE_REF / PRESSURE_REF) << " * " << TEMPERATURE;
    ost << ");\n";
    ost << "sumyow = " << PRESSURE << "/sumyow;\n";
    if (!unit->third_bodies.empty())
      ost << THIRD_BODY << "[0] = 0.0;\n";
    for (std::set<Species*>::const_iterator it = non_qss_species.begin();
          it != non_qss_species.end(); it++)
    {
      ost << MOLE_FRAC << "[" << (*it)->code_name << "] *= (sumyow * recip_molecular_mass[" << (*it)->code_name << "]);\n"; 
      if (!unit->third_bodies.empty())
        ost << THIRD_BODY << "[0] += " << MOLE_FRAC << "[" << (*it)->code_name << "];\n";
    }
    for (unsigned idx = 1; idx < unit->third_bodies.size(); idx++)
    {
      ost << THIRD_BODY << "[" << idx << "] = " << THIRD_BODY << "[0]"; 
      for (std::map<Species*,double>::const_iterator it = unit->third_bodies[idx]->components.begin();
            it != unit->third_bodies[idx]->components.end(); it++)
      {
        if (!it->first->qss_species)
          ost << " + " << it->second << "*" << MOLE_FRAC << "[" << it->first->code_name << "]";
      }
      ost << ";\n";
    }
    // Now do the writes
    ost << "if (__any(offset == 0))\n";
    {
      PairDelim if_pair(ost);
      for (std::set<Species*>::const_iterator it = non_qss_species.begin();
            it != non_qss_species.end(); it++)
      {
        ost << MOLE_FRAC << "_0[" << (*it)->code_name << "][tid] = " << MOLE_FRAC << "[" << (*it)->code_name << "];\n";
      }
      for (unsigned idx = 0; idx < unit->third_bodies.size(); idx++)
        ost << THIRD_BODY << "_0[" << idx << "][tid] = " << THIRD_BODY << "[" << idx << "];\n";
      // Mark that the buffer is ready
      ost << "asm volatile(\"bar.arrive 12," << ((2*MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      // Also mark that we're ready to start consuming our buffer
      ost << "asm volatile(\"bar.arrive 4," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      for (std::set<Species*>::const_iterator it = non_qss_species.begin();
            it != non_qss_species.end(); it++)
      {
        ost << MOLE_FRAC << "_1[" << (*it)->code_name << "][tid] = " << MOLE_FRAC << "[" << (*it)->code_name << "];\n";
      }
      for (unsigned idx = 0; idx < unit->third_bodies.size(); idx++)
        ost << THIRD_BODY << "_1[" << idx << "][tid] = " << THIRD_BODY << "[" << idx << "];\n";
      ost << "asm volatile(\"bar.arrive 13," << ((2*MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      ost << "asm volatile(\"bar.arrive 8," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
    }
  }
  // Emit declarations for all of the variables that we're going to need for this computation
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->emit_pipe_variable_declarations(ost);
  }
  // Start pulling values out of the pipe
  assert(!target_warps.empty());
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    // Count how many values are in this stage
    unsigned num_values = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (stage_idx < target_warps[idx]->reactions.size())
        num_values++;
    }
    ost << "// Stage " << stage_idx << "\n"; 
    PairDelim stage_pair(ost);
    ost << REAL << " rr_f[" << num_values << "];\n"; 
    ost << REAL << " rr_r[" << num_values << "];\n";
    // Wait until the values are ready and then read them out
    ost << "if (__any(offset == 0))\n";
    {
      PairDelim if_pair(ost);
      if ((stage_idx%2)==0)
        ost << "asm volatile(\"bar.sync 5," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      else
        ost << "asm volatile(\"bar.sync 7," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_2[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_2[" << idx << "][1][tid];\n";
      }
      // Trigger the other one unless its the last stage
      if (stage_idx < (target_warps[0]->reactions.size()-1))
      {
        if ((stage_idx%2)==0)
          ost << "asm volatile(\"bar.arrive 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.arrive 4," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      if ((stage_idx%2)==0)
        ost << "asm volatile(\"bar.sync 9," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      else
        ost << "asm volatile(\"bar.sync 11," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_3[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_3[" << idx << "][1][tid];\n";
      }
      if (stage_idx < (target_warps[0]->reactions.size()-1))
      {
        if ((stage_idx%2) == 0)
          ost << "asm volatile(\"bar.arrive 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.arrive 8," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
    // Now that we've got our values reduce them out to the values we're computing
    for (unsigned widx = 0; widx < num_values; widx++)
    {
      int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
      assert(reac_idx != -1);
      // Tell each connected component to emit updates to this reaction
      for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
            it != connected_components.end(); it++)
      {
        (*it)->emit_pipe_updates(ost, widx, reac_idx);
      }
    }
  }
  // Now once we've got all the values that we need, do the actual QSSA computation
  for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
        it != connected_components.end(); it++)
  {
    (*it)->emit_pipe_qssa_computation(ost);
  }

}

void ChemistryUnit::emit_pipe_fine_stiffness_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species)
{
  ost << "// Stiffness computation\n";
  PairDelim stif_pair(ost);
  // Mark that our buffer is ready for use before doing anything else
  ost << "if (offset == 0)\n";
  ost << "asm volatile(\"bar.arrive 4," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
  ost << "else\n";
  ost << "asm volatile(\"bar.arrive 8," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
  // Issue the loads for the diffusion values that we'll need
  // Put a threadfence above these so the compiler doesn't do something dumb like try
  // to hoise them and add register pressure
  ost << "__threadfence_block();\n";
  //unsigned upper_bound = next_largest_power(2*non_qss_species,2);
  for (unsigned idx = 0; idx < stif_operations.size(); idx++)
  {
    char src_offset[128];
    //sprintf(src_offset,"((%s+%d*step)%%%d)*spec_stride",stif_operations[idx]->species->code_name,upper_bound,upper_bound);
    sprintf(src_offset,"%s*spec_stride",stif_operations[idx]->species->code_name);
    char dst_offset[128];
    sprintf(dst_offset,"%d",idx);
    emit_cuda_load(ost,"stiffness",dst_offset,DIFFUSION_ARRAY,src_offset,false/*double2*/,".cg");
  }
  // We've already marked previously that we're ready to start consuming the buffer
  // in the last part of the QSSA operation
  ost << REAL << " cdot[" << stif_operations.size() << "];\n";
  ost << REAL << " ddot[" << stif_operations.size() << "];\n";
  ost << "#pragma unroll\n";
  ost << "for (int i = 0; i < " << stif_operations.size() << "; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << " cdot[i] = 0.0;\n";
    ost << " ddot[i] = 0.0;\n";
  }
  // Start pulling values out of the pipe 
  assert(!target_warps.empty());
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    // Count how many values are in this stage
    unsigned num_values = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (stage_idx < target_warps[idx]->reactions.size())
        num_values++;
    }
    ost << "// Stage " << stage_idx << "\n";
    PairDelim stage_pair(ost);
    ost << REAL << " rr_f[" << num_values << "];\n";
    ost << REAL << " rr_r[" << num_values << "];\n";
    ost << "if (__any(offset == 0))\n";
    {
      PairDelim if_pair(ost);
      if ((stage_idx%2)==0)
        ost << "asm volatile(\"bar.sync 5," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      else
        ost << "asm volatile(\"bar.sync 7," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_2[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_2[" << idx << "][1][tid];\n";
      }
      if (stage_idx < (target_warps[0]->reactions.size()-1))
      {
        if ((stage_idx%2)==0)
          ost << "asm volatile(\"bar.arrive 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.arrive 4," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      if ((stage_idx%2)==0)
        ost << "asm volatile(\"bar.sync 9," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      else
        ost << "asm volatile(\"bar.sync 11," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_3[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_3[" << idx << "][1][tid];\n";
      }
      if (stage_idx < (target_warps[0]->reactions.size()-1))
      {
        if ((stage_idx%2) == 0)
          ost << "asm volatile(\"bar.arrive 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.arrive 8," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
    for (unsigned widx = 0; widx < num_values; widx++)
    {
      int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
      assert(reac_idx != -1);
      // Update any reaction rate corrections for QSSA
      for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
            it != connected_components.end(); it++)
      {
        (*it)->emit_pipe_corrections(ost, widx, reac_idx); 
      }
      for (unsigned stif_idx = 0; stif_idx < stif_operations.size(); stif_idx++)
      {
        stif_operations[stif_idx]->emit_pipe_updates(ost, stif_idx, widx, reac_idx);
      }
    }
  }
  // Now do each of the stiffnes computations
  for (unsigned stif_idx = 0; stif_idx < stif_operations.size(); stif_idx++)
  {
    ost << "// Stiffness for " << stif_operations[stif_idx]->name << "\n";
    PairDelim stif_spec_pair(ost);
    ost << REAL << " " << MOLE_FRAC << ";\n";
    ost << "if (offset == 0)\n";
    ost << MOLE_FRAC << " = " << MOLE_FRAC << "_0[" << stif_operations[stif_idx]->species->code_name << "][tid];\n";
    ost << "else\n";
    ost << MOLE_FRAC << " = " << MOLE_FRAC << "_1[" << stif_operations[stif_idx]->species->code_name << "][tid];\n";
    // Now emit the code to do the stiffness computation
    stif_operations[stif_idx]->emit_pipe_statements(ost, stif_idx, unit);
  }
}

void ChemistryUnit::emit_pipe_fine_output_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species)
{
  // Do the output computation for all the species
  ost << "// Species output computation\n";
  PairDelim output_pair(ost);
  ost << REAL << " spec_out[" << non_qss_species << "];\n";
  ost << "#pragma unroll\n";
  ost << "for (int i = 0; i < " << non_qss_species << "; i++)\n";
  ost << "spec_out[i] = 0.0;\n";
  // Build a map of the species and the number of updates they need to
  // see before they can be written
  std::map<Species*,int/*remaining updates*/> species_updates;
  for (std::set<Species*>::const_iterator it = unit->species.begin();
        it != unit->species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if ((*it)->qss_species)
      continue;
    species_updates[(*it)] = (*it)->reaction_contributions.size();
  }
  assert(species_updates.size() == non_qss_species);
  // Emit the writes for any species who are already done
  unsigned total_writes = 0;
  for (std::map<Species*,int>::const_iterator it = species_updates.begin();
        it != species_updates.end(); it++)
  {
    if (it->second == 0)
    {
      // No need to apply the non-dimensionalized scaling factor here since we know the value is zero
      ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << WDOT << "+"
          << it->first->code_name << "*spec_stride), \"d\"(spec_out[" << it->first->code_name << "]) : \"memory\");\n";
      total_writes++;
    }
  }
  assert(!target_warps.empty());
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    // Count how many values are in this stage
    unsigned num_values = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (stage_idx < target_warps[idx]->reactions.size())
        num_values++;
    }
    ost << "// Stage " << stage_idx << "\n";
    PairDelim stage_pair(ost);
    ost << REAL << " rr_f[" << num_values << "];\n";
    ost << REAL << " rr_r[" << num_values << "];\n";
    ost << "if (__any(offset == 0))\n";
    {
      PairDelim if_pair(ost);
      // Do an exchange with the each of the mole fraction warps to give them qssa and stiffness values
      if (stage_idx < 2)
      {
        unsigned num_qssa_vars = ((unit->species.size()-1)-non_qss_species);
        unsigned num_qssa_xfers = (num_qssa_vars + (2*unit->threads_per_point-1))/(2*unit->threads_per_point);
        ost << "// Sending QSSA in " << num_qssa_xfers << " transfers\n";
        for (unsigned idx = 0; idx < num_qssa_xfers; idx++)
        {
          // Write the values, mark them ready, then wait
          for (unsigned local_idx = 0; local_idx < (2*unit->threads_per_point); local_idx++)
          {
            unsigned qidx = idx*2*unit->threads_per_point + local_idx;
            if (qidx >= num_qssa_vars)
              break;
            unsigned first = local_idx/2;
            unsigned second = local_idx%2;
            ost << "pipe_2[" << first << "][" << second << "][tid] = xq_values[" << qidx << "];\n";
          }
          if (stage_idx == 0)
          {
            ost << "asm volatile(\"bar.arrive 4," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
            ost << "asm volatile(\"bar.sync 5," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
          }
          else
          {
            ost << "asm volatile(\"bar.arrive 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
            ost << "asm volatile(\"bar.sync 7," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
          }
        }
        // Now do the same thing for stiffness
        unsigned num_stif_xfers = (stif_operations.size() + (2*unit->threads_per_point)-1)/(2*unit->threads_per_point);
        ost << "// Sending Stiffness in " << num_stif_xfers << "\n";
        for (unsigned idx = 0; idx < num_stif_xfers; idx++)
        {
          for (unsigned local_idx = 0; local_idx < (2*unit->threads_per_point); local_idx++)
          {
            unsigned qidx = idx*2*unit->threads_per_point + local_idx;
            if (qidx >= stif_operations.size())
              break;
            unsigned first = local_idx/2;
            unsigned second = local_idx%2;
            ost << "pipe_2[" << first << "][" << second << "][tid] = stiffness[" << qidx << "];\n";
          }
          if (stage_idx == 0)
          {
            ost << "asm volatile(\"bar.arrive 4," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
            ost << "asm volatile(\"bar.sync 5," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
          }
          else
          {
            ost << "asm volatile(\"bar.arrive 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
            ost << "asm volatile(\"bar.sync 7," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
          }
        }
      }
      else
      {
        if ((stage_idx%2) == 0)
          ost << "asm volatile(\"bar.sync 5," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.sync 7," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_2[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_2[" << idx << "][1][tid];\n";
      }
      // No need to emit this on the last pass since we'll get it the next time around the loop
      if (stage_idx < (target_warps[0]->reactions.size()-1))
      {
        if ((stage_idx%2)==0)
          ost << "asm volatile(\"bar.arrive 6," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.arrive 4," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      // Do an exchange with the each of the mole fraction warps to give them qssa and stiffness values
      if (stage_idx < 2)
      {
        unsigned num_qssa_vars = ((unit->species.size()-1)-non_qss_species);
        unsigned num_qssa_xfers = (num_qssa_vars + (2*unit->threads_per_point-1))/(2*unit->threads_per_point);
        ost << "// Sending QSSA in " << num_qssa_xfers << " transfers\n";
        for (unsigned idx = 0; idx < num_qssa_xfers; idx++)
        {
          // Write the values, mark them ready, then wait
          for (unsigned local_idx = 0; local_idx < (2*unit->threads_per_point); local_idx++)
          {
            unsigned qidx = idx*2*unit->threads_per_point + local_idx;
            if (qidx >= num_qssa_vars)
              break;
            unsigned first = local_idx/2;
            unsigned second = local_idx%2;
            ost << "pipe_3[" << first << "][" << second << "][tid] = xq_values[" << qidx << "];\n";
          }
          if (stage_idx == 0)
          {
            ost << "asm volatile(\"bar.arrive 8," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
            ost << "asm volatile(\"bar.sync 9," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
          }
          else
          {
            ost << "asm volatile(\"bar.arrive 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
            ost << "asm volatile(\"bar.sync 11," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
          }
        }
        // Now do the same thing for stiffness
        unsigned num_stif_xfers = (stif_operations.size() + (2*unit->threads_per_point)-1)/(2*unit->threads_per_point);
        ost << "// Sending Stiffness in " << num_stif_xfers << "\n";
        for (unsigned idx = 0; idx < num_stif_xfers; idx++)
        {
          for (unsigned local_idx = 0; local_idx < (2*unit->threads_per_point); local_idx++)
          {
            unsigned qidx = idx*2*unit->threads_per_point + local_idx;
            if (qidx >= stif_operations.size())
              break;
            unsigned first = local_idx/2;
            unsigned second = local_idx%2;
            ost << "pipe_3[" << first << "][" << second << "][tid] = stiffness[" << qidx << "];\n";
          }
          if (stage_idx == 0)
          {
            ost << "asm volatile(\"bar.arrive 8," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
            ost << "asm volatile(\"bar.sync 9," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
          }
          else
          {
            ost << "asm volatile(\"bar.arrive 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
            ost << "asm volatile(\"bar.sync 11," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
          }
        }
      }
      else
      {
        if ((stage_idx%2) == 0)
          ost << "asm volatile(\"bar.sync 9," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.sync 11," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe_3[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe_3[" << idx << "][1][tid];\n";
      }
      // No need to emit this on the last pass since we'll get it the next time around the loop
      if (stage_idx < (target_warps[0]->reactions.size()-1))
      {
        if ((stage_idx%2)==0)
          ost << "asm volatile(\"bar.arrive 10," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
        else
          ost << "asm volatile(\"bar.arrive 8," << ((MOLE_FRAC_WARPS+1)*32) << ";\" : : : \"memory\");\n";
      }
    }
    for (unsigned widx = 0; widx < num_values; widx++)
    {
      int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
      assert(reac_idx != -1);
      // Reaction rates have already been updated
      Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
      assert(reac != NULL);
      ost << REAL << " ropl_" << reac_idx << " = rr_f[" << widx << "] - rr_r[" << widx << "];\n";
      // Update all the species 
      for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
            it != reac->stoich.end(); it++)
      {
        if (it->first->qss_species)
          continue;
        if (it->second == 1)
          ost << "spec_out[" << it->first->code_name << "] += ropl_" << reac_idx << ";\n";
        else if (it->second == -1)
          ost << "spec_out[" << it->first->code_name << "] -= ropl_" << reac_idx << ";\n";
        else
          ost << "spec_out[" << it->first->code_name << "] = __fma_rn(" << double(it->second)
              << ",ropl_" << reac_idx << ",spec_out[" << it->first->code_name << "]);\n";
        assert(species_updates.find(it->first) != species_updates.end());
        assert(species_updates[it->first] > 0);
        species_updates[it->first]--;
        if (species_updates[it->first] == 0)
        {
          // This species has seen all it's updates, so we can write out it's value
          if (!unit->no_nondim)
            ost << "spec_out[" << it->first->code_name << "] *= " << (it->first->molecular_mass/1000.0) << ";\n";
          ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << WDOT << "+"
              << it->first->code_name << "*spec_stride), \"d\"(spec_out[" << it->first->code_name << "]) : \"memory\");\n";
          total_writes++;
        }
      }
    }
  }
  // make sure we wrote out all the species
  assert(total_writes == non_qss_species);
}

void ChemistryUnit::emit_multi_getrates_code(CodeOutStream &ost, unsigned non_qss_species, const char *temp_qssa,
                                            const char *temp_stif, const char *temp_last)
{ 
  ost << "\n";
  emit_pipe_general_constants(ost, non_qss_species);
  unsigned pass0_per_thread, pass0_constant_stride;
  emit_pipe_warp_constants(ost, qssa_warps, 0/*pass*/, pass0_per_thread, pass0_constant_stride);
  unsigned pass1_per_thread, pass1_constant_stride;
  emit_pipe_warp_constants(ost, stif_warps, 1/*pass*/, pass1_per_thread, pass1_constant_stride);
  unsigned pass2_per_thread, pass2_constant_stride;
  emit_pipe_warp_constants(ost, last_warps, 2/*pass*/, pass2_per_thread, pass2_constant_stride);
  // Emit the first kernel
  {
    ost << "\n";
    ost << "__global__ void\n";
    ost << "gpu_getrates(";
    ost << "const " << REAL << " *" << PRESSURE_ARRAY;
    ost << ", const " << REAL << " *" << TEMPERATURE_ARRAY;
    ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
    ost << ", const " << REAL << " *" << DIFFUSION_ARRAY;
    ost << ", const " << REAL << " dt";
    ost << ", const " << REAL << " recip_dt/*reciprocal of dt*/";
    ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
    ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
    ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
    ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
    ost << ", " << REAL << " *" << QSSA_ARRAY;
    ost << ")\n";
    PairDelim pair(ost);
    // Compute the offset for this thread
    {
      PairDelim offset_pair(ost);
      ost << "const " << INT << " offset = (blockIdx.y*row_stride) + "
          << "(blockIdx.x*32 + (threadIdx.x & 0x1f));\n";
      ost << PRESSURE_ARRAY << " += offset;\n";
      ost << TEMPERATURE_ARRAY << " += offset;\n";
      ost << MASS_FRAC_ARRAY << " += offset;\n";
      ost << QSSA_ARRAY << " += offset;\n";
    }
    // Declare the shared memory for this thread block
    ost << "volatile __shared__ " << REAL << " pipe[" << unit->threads_per_point << "][2][32];\n";
    ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << non_qss_species << "][32];\n";
    //ost << "volatile __shared__ " << REAL << " " << THIRD_BODY << "[" << unit->third_bodies.size() << "][32];\n";
    // Now we can split the warps
    //ost << "if (__any(threadIdx.x < " << (unit->threads_per_point*32) << "))\n";
    {
      PairDelim if_pair(ost);
      ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
      ost << "const " << INT << " wid = threadIdx.x >> 5;\n";
      ost << REAL << " pass_0_constants[" << pass0_per_thread << "];\n";
      for (unsigned idx = 0; idx < pass0_per_thread; idx++)
      {
        char src_offset[128];
        sprintf(src_offset,"wid*%d+%d",pass0_constant_stride,(idx*32));
        char dst_offset[128];
        sprintf(dst_offset,"%d",idx);
        emit_cuda_load(ost,"pass_0_constants",dst_offset,"warp_constants_0",src_offset,false/*double2*/,".cg");
      }
      // Then copy over the code from the temporary file
      FILE *temp_file = fopen(temp_qssa,"r");
      assert(temp_file != NULL);
      char c = fgetc(temp_file);
      while (c != EOF)
      {
        ost << c;
        c = fgetc(temp_file);
      }
      assert(fclose(temp_file) == 0);
      // Catch the last wake up from the qssa warps
      //ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+QSSA_WARPS)*32) << ";\" : : : \"memory\");\n";
    }
#if 0
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
      ost << "const " << INT << " wid = (threadIdx.x >> 5) - " << unit->threads_per_point << ";\n";
      // Signal that the buffer is ready
      ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+QSSA_WARPS)*32) << ";\" : : : \"memory\");\n";
      //ost << REAL << " xq_values[" << ((unit->species.size()-1)-non_qss_species) << "];\n";
      ost << "#pragma unroll 1\n"; // don't ever unroll this loop
      ost << "for (int step = 0; step < total_steps; step++)\n";
      PairDelim loop_step_pair(ost);
      emit_multi_qssa_computation(ost, qssa_warps, non_qss_species, qss_partition, max_qss_values, max_qss_denoms);
      // Update the qssa array for the next time around the loop
      ost << QSSA_ARRAY << " += slice_stride;\n";
    }
#endif
  }
}

void ChemistryUnit::emit_multi_reaction_code(CodeOutStream &ost, const std::vector<Warp*> &target_warps, 
                                              unsigned non_qss_species, int pass, int other_warps)
{
  // Emit the load for our temperature
  {
    unit->emit_numerical_constants(ost);
    ost << REAL << " " << TEMPERATURE << ";\n";
    emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,false/*double2*/,".cg");
  }
  // Emit loads for the species that we need
  {
    const unsigned num_mass_fracs = (non_qss_species+(unit->threads_per_point-1))/unit->threads_per_point;  
    const bool uniform_mass_fracs = ((non_qss_species%unit->threads_per_point) == 0);
    ost << REAL << " " << MASS_FRAC << "[" << num_mass_fracs << "];\n";
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx == (num_mass_fracs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride",idx*unit->threads_per_point);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      emit_cuda_load(ost,MASS_FRAC,dst_offset,MASS_FRAC_ARRAY,src_offset,false/*double2*/,".cs");
    }
    ost << REAL << " " << PRESSURE << ";\n";
    emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,false/*double2*/,".cg");
    // Compute our local sumyow value
    ost << REAL << " local_sumyow = 0.0;\n";
    const unsigned upper_bound = next_largest_power(2*non_qss_species,2);
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx*unit->threads_per_point))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      PairDelim mass_pair(ost);
      ost << MASS_FRAC << "[" << idx << "] *= recip_molecular_mass[(wid+" << (idx*unit->threads_per_point)
          << "+step*" << upper_bound << ")%" << upper_bound << "];\n";
      ost << "local_sumyow += " << MASS_FRAC << "[" << idx << "];\n";
    }
    // Wait until we can write into the pipe
    ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+other_warps)*32) << ";\" : : : \"memory\");\n";
    // Write our values into the pipe
    ost << "pipe[wid][0][tid] = local_sumyow;\n";
    // Wait until everyone has written into the pipe
    ost << "asm volatile(\"bar.sync 2," << (unit->threads_per_point*32) << ";\" : : : \"memory\");\n";
    // Everyone read out all the values compute the mole fractions and write them into shared memory
    ost << REAL << " sumyow = 0.0;\n";
    for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
      ost << "sumyow += pipe[" << idx << "][0][tid];\n";
    ost << "sumyow *= (";
    if (unit->no_nondim)
      ost << "8.314510e+07 * " << TEMPERATURE;
    else
      ost << (8.314510e+07 * TEMPERATURE_REF / PRESSURE_REF) << " * " << TEMPERATURE;
    ost << ");\n";
    ost << "sumyow = " << PRESSURE << "/sumyow;\n";
    // Scale our values and write them into shared memory
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx == (num_mass_fracs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid] = sumyow * " << MASS_FRAC << "[" << idx << "];\n";
    }
    // Indicate that we're done
    ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+other_warps)*32) << ";\" : : : \"memory\");\n";
  }
  {
    ost << "const " << REAL << " otc = 1.0 / " << TEMPERATURE << ";\n";
    ost << "const " << REAL << " ortc = 1.0 / (" << TEMPERATURE << " * R0c);\n";
    ost << "const " << REAL << " vlntemp = log(" << TEMPERATURE ");\n";
    //ost << "const " << REAL << " prt = PA / (R0 * " << TEMPERATURE << ");\n";
    //ost << "const " << REAL << " oprt = 1.0 / prt;\n";
  }
  assert(!target_warps.empty());
  unsigned total_gibbs = 0;
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    std::vector<Reaction*> weird_reactions;
    std::vector<Reaction*> normal_reactions;
    ost << "// Reaction stage " << stage_idx << "for pass " << pass << "\n";
    PairDelim stage_pair(ost);
    ost << REAL << " rr_f, rr_r;\n";
    int last_weird_warp = -1;
    std::vector<Reaction*> low_reactions;
    std::vector<Reaction*> irreversible_reactions;
    unsigned low_mask = 0;
    std::vector<Warp*> low_warps;
    std::vector<Reaction*> troe_reactions;
    std::vector<Reaction*> lindermann_reactions;
    unsigned troe_mask = 0;
    std::vector<Warp*> troe_warps;
    // Do the arrhenius part for everyone
    for (unsigned widx = 0; widx < target_warps.size(); widx++)
    {
      if (stage_idx < target_warps[widx]->reactions.size())
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
        Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
        assert(reac != NULL);
        if (is_weird_reaction(reac))
        {
          weird_reactions.push_back(reac);
          assert((last_weird_warp+1) == int(widx)); // all weird warps should be contiguous
          last_weird_warp = widx;
          // Figure out what kind of arrhenius to do
          if (reac->low.enabled)
          {
            low_reactions.push_back(reac);
            assert(widx < 32); // 1-hot encoding doesn't work past 32 bits
            low_mask |= (1 << widx); 
            low_warps.push_back(target_warps[widx]);
            if (reac->troe.num > 0)
            {
              assert(reac->troe.num == 4); // assume four troe constants for now
              troe_reactions.push_back(reac);
              troe_mask |= (1 << widx);
              troe_warps.push_back(target_warps[widx]);
            }
            else if (reac->sri.num == 0)
            {
              lindermann_reactions.push_back(reac);
            }
            else
              assert(false); // not handling sri right now
            if (!unit->no_nondim)
            {
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                      (reac->low.a*REACTION_RATE_REF), reac->low.beta, reac->low.e, -1, pass);
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                      (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
            }
            else
            {
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                      reac->low.a, reac->low.beta, reac->low.e, -1, pass);
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                      reac->a, reac->beta, reac->e, -1, pass); 
            }
            assert(!reac->rev.enabled); // hoping we never have to do a reverse reaction in this case 
            assert(reac->reversible);
          }
          else if (!reac->lt.enabled)
          {
            assert(!reac->rlt.enabled);
            assert(!reac->reversible);
            irreversible_reactions.push_back(reac);
            if (!unit->no_nondim)
            {
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                      (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
              if (reac->rev.enabled)
              {
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        (reac->rev.a*REACTION_RATE_REF), reac->rev.beta, reac->rev.e, -1, pass);
              }
              else
              {
                // We're just going to write zero anyway, so what does it matter
                // Duplicate the computation so we don't diverge, but with coefficients that won't do anything weird
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
              }
            }
            else
            {
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                      reac->a, reac->beta, reac->e, -1, pass);
              if (reac->rev.enabled)
              {
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        reac->rev.a, reac->rev.beta, reac->rev.e, -1, pass);
              }
              else
              {
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                      reac->a, reac->beta, reac->e, -1, pass);
              }
            }
          }
          else
          {
            assert(false); // Not currently handling landau-teller right now since it doesn't use arrhenius
          }
        }
        else
        {
          normal_reactions.push_back(reac);
          // Normal arrhenius and we're done
          if (!unit->no_nondim)
          {
            emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                      (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
            emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                      (reac->rev.a*REACTION_RATE_REF), reac->rev.beta, reac->rev.e, -1, pass);
          }
          else
          {
            emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                        reac->a, reac->beta, reac->e, -1, pass);
            emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        reac->rev.a, reac->rev.beta, reac->rev.e, -1, pass);
          }
        }
      }
    }
    if (!weird_reactions.empty())
    {
      if (!normal_reactions.empty())
      {
        assert(last_weird_warp != -1);
        ost << "if (__any(wid < " << (last_weird_warp+1) << "))\n";
      }
      PairDelim weird_pair(ost);
      // Now emit the code for the weird reactions
      // Emit the code for the different cases of weird reactions
      if (!low_reactions.empty())
      {
        if (!irreversible_reactions.empty())
          ost << "if (__any(((1 << wid) & " << low_mask << ")))\n";
        PairDelim low_pair(ost);
        ost << REAL << " rr_k0 = rr_f;\n";
        ost << REAL << " rr_kinf = rr_r;\n";
        ost << REAL << " pr = rr_k0 / rr_kinf;\n";
        // Figure out the maximum number of gibbs species we need to compute for any of the troe warps
        unsigned max_gibbs = 0;
        for (std::vector<Reaction*>::const_iterator it = low_reactions.begin();
              it != low_reactions.end(); it++)
        {
          if ((*it)->stoich.size() > max_gibbs)
            max_gibbs = (*it)->stoich.size();
        }
        // shouldn't be zero 
        assert(max_gibbs > 0);
        // emit the code to compute the gibbs values that we need
        ost << "// Gibbs computation\n";
        {
          unsigned old_total_gibbs = total_gibbs;
          PairDelim gibbs_pair(ost);
          ost << REAL << " xik = 0.0;\n";
          ost << "const " << REAL << " &tk1 = " << TEMPERATURE << ";\n";
          ost << "const " << REAL << " tk2 = tk1*tk1;\n";
          ost << "const " << REAL << " tk3 = tk1*tk2;\n";
          ost << "const " << REAL << " tk4 = tk1*tk3;\n";
          ost << "const " << REAL << " tk5 = tk1*tk4;\n";
          ost << INT << " gibbs_idx;\n";
          for (unsigned idx = 0; idx < max_gibbs; idx++)
          {
            ost << REAL << " gibbs_" << idx << ";\n";
            // Do something to fool the compiler here
            unsigned upper_bound = next_largest_power(2*non_qss_species,2);
            ost << "gibbs_idx = (gibbs_index_" << pass << "[(" << total_gibbs 
                << "+step*" << upper_bound << ")%" << upper_bound << "][wid]"
                << "+step*" << upper_bound << ")%" << upper_bound << ";\n";
            ost << "if (tk1 > gibbs_values[gibbs_idx][0])\n";
            {
              PairDelim if_pair(ost);
              ost << "gibbs_" << idx << " = gibbs_values[gibbs_idx][1]*tk1*(";
              if (!unit->no_nondim)
                ost << (1.0-log(TEMPERATURE_REF));
              else
                ost << "1.0";
              ost << "- vlntemp) + "
                  << "gibbs_values[gibbs_idx][2]*tk2 + "
                  << "gibbs_values[gibbs_idx][3]*tk3 + "
                  << "gibbs_values[gibbs_idx][4]*tk4 + "
                  << "gibbs_values[gibbs_idx][5]*tk5 + "
                  << "(gibbs_values[gibbs_idx][6] - tk1*"
                  << "gibbs_values[gibbs_idx][7]);\n";
            }
            ost << "else\n";
            {
              PairDelim else_pair(ost);
              ost << "gibbs_" << idx << " = gibbs_values[gibbs_idx][8]*tk1*(";
              if (!unit->no_nondim)
                ost << (1.0-log(TEMPERATURE_REF));
              else
                ost << "1.0";
              ost << " - vlntemp) + "
                  << "gibbs_values[gibbs_idx][9]*tk2 + "
                  << "gibbs_values[gibbs_idx][10]*tk3 + "
                  << "gibbs_values[gibbs_idx][11]*tk4 + "
                  << "gibbs_values[gibbs_idx][12]*tk5 + "
                  << "(gibbs_values[gibbs_idx][13] - tk1*"
                  << "gibbs_values[gibbs_idx][14]);\n";
            }
            // Do something to fool the compiler here
            ost << "xik = __fma_rn(gibbs_scale_" << pass << "[(" << total_gibbs 
                << "+step*" << upper_bound << ")%" << upper_bound << "][wid],gibbs_" << idx << ",xik);\n";
            total_gibbs++;
          }
          ost << "rr_r = exp(xik*otc - reverse_pass_" << pass << "[" << stage_idx << "][wid]*";
          if (!unit->no_nondim)
            ost << "(vlntemp + " << TEMPERATURE_REF << ")";
          else
            ost << "vlntemp";
          ost << ");\n";
          // Now update low warps with the information that they need to emit the constants
          assert(low_reactions.size() == low_warps.size());
          for (unsigned idx = 0; idx < low_warps.size(); idx++)
          {
            Warp *warp = low_warps[idx];
            Reaction *reac = low_reactions[idx];
            unsigned added_gibbs = 0;
            int nutot = 0;
            for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
                  it != reac->stoich.end(); it++)
            {
              int spec_idx = -1;
              for (unsigned gidx = 0; gidx < gibbs_species.size(); gidx++)
              {
                if (it->first == gibbs_species[gidx])
                {
                  spec_idx = gidx;
                  break;
                }
              }
              if (spec_idx == -1)
              {
                spec_idx = gibbs_species.size();
                gibbs_species.push_back(it->first);
              }
              warp->add_gibbs_species(old_total_gibbs+added_gibbs,spec_idx,double(it->second),true);
              added_gibbs++;
              nutot += it->second;
            }
            while (added_gibbs < max_gibbs)
            {
              warp->add_gibbs_species(old_total_gibbs+added_gibbs,0,0.0,true);
              added_gibbs++;
            }
            // Also update the reverse value
            warp->add_reverse_constant(stage_idx, double(nutot),true);
          }
        }

        // Now we need to wait until we get our information from the mole fraction warp
        // about our scaling value
        ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+other_warps)*32) << ";\" : : : \"memory\");\n";
        ost << "pr *= pipe[wid][0][tid];\n";
        if (!troe_reactions.empty())
        {
          if (!lindermann_reactions.empty())
            ost << "if (__any(((1 << wid) & " << troe_mask << ")))\n";
          PairDelim troe_pair(ost);
          // Do something to fool the compiler here
          assert(non_qss_species >= needed_troe_reactions.size());
          unsigned upper_bound = next_largest_power(2*non_qss_species,2);
          ost << INT << " troe_idx = (troe_index_" << pass << "[(" << stage_idx 
              << "+step*" << upper_bound << ")%" << upper_bound 
              << "][wid] + step*" << upper_bound << ")%" << upper_bound << ";\n";
          ost << REAL << " fcent = log10(troe_values[troe_idx][0] * exp("
              << "troe_values[troe_idx][1] * " << TEMPERATURE << ")"
              << " + troe_values[troe_idx][2] * exp("
              << "troe_values[troe_idx][3] * " << TEMPERATURE << ")"
              << " + exp(troe_values[troe_idx][4] * otc" << "));\n";
          // add the troe constants
          assert(troe_reactions.size() == troe_warps.size());
          for (unsigned troe_idx = 0; troe_idx < troe_warps.size(); troe_idx++)
          {
            Warp *troe = troe_warps[troe_idx];
            Reaction *reac = troe_reactions[troe_idx];
            int tr_idx = -1;
            for (unsigned tidx = 0; tidx < needed_troe_reactions.size(); tidx++)
            {
              if (needed_troe_reactions[tidx] == reac)
              {
                tr_idx = tidx;
                break;
              }
            }
            if (tr_idx == -1)
            {
              tr_idx = needed_troe_reactions.size();
              needed_troe_reactions.push_back(reac);
            }
            troe->add_troe_reaction(stage_idx, tr_idx, true);
          }
          ost << REAL << " flogpr = log10(pr) - 0.4 - 0.67 * fcent;\n";
          ost << REAL << " fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;\n";
          ost << REAL << " fquan = flogpr / fdenom;\n";
          ost << "fquan = fcent / (1.0 + fquan * fquan);\n";
          ost << "rr_f = rr_kinf * pr/(1.0 + pr) * exp(fquan * DLn10);\n";
        }
        if (!lindermann_reactions.empty())
        {
          if (!troe_reactions.empty())
            ost << "else\n";
          PairDelim linderman_pair(ost);
          ost << "rr_f = rr_kinf * pr/(1.0 + pr);\n";
        }
        // Now that we have rr_f, we can compute the final part of rr_r
        ost << "rr_r *= rr_f;\n";
        // Write our results out into shared memory, no need to wait since we already know the buffer is ready
        ost << "pipe[wid][0][tid] = rr_f;\n";
        ost << "pipe[wid][1][tid] = rr_r;\n";
        ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+other_warps)*32) << ";\" : : : \"memory\");\n";
      }
      if (!irreversible_reactions.empty())
      {
        if (!low_reactions.empty())
          ost << "else\n";
        PairDelim irrev_pair(ost);
        ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+other_warps)*32) << ";\" : : : \"memory\");\n";
        ost << "pipe[wid][0][tid] = rr_f;\n";
        ost << "pipe[wid][1][tid] = rr_r*0.0; /* irreversible reaction */\n";
        ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+other_warps)*32) << ";\" : : : \"memory\");\n";
      }
    }
    // Now do all the normal reactions
    if (!normal_reactions.empty())
    {
      if (!weird_reactions.empty())
        ost << "else\n";
      PairDelim normal_pair(ost);
      // For the normal case we can just do the normal thing and write our values into
      // shared memory ass soon as the buffer is ready
      // Write our result into the pipe
      ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+other_warps)*32) << ";\" : : : \"memory\");\n";
      ost << "pipe[wid][0][tid] = rr_f;\n";
      ost << "pipe[wid][1][tid] = rr_r;\n";
      ost << "asm volatile(\"bar.arrive 1," << ((unit->threads_per_point+other_warps)*32) << ";\" : : : \"memory\");\n";
    }
  }
}

void ChemistryUnit::emit_multi_qssa_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species,
                                                const std::vector<std::map<QSS*,int> > &qss_partition, int max_values, int max_denoms)
{
  ost << REAL << " avars[" << max_values << "];\n";
  for (int idx = 0; idx < max_values; idx++)
    ost << "avars[" << idx << "] = 0.0;\n";
  ost << REAL << " denom[" << max_denoms << "];\n";
  for (int idx = 0; idx < max_denoms; idx++)
    ost << "denom[" << idx << "] = 0.0;\n";
  {
    // Figure out if there are any mole fraction or third body values that need
    // to be sent to the reaction warps, if not, we can do the barrier now
    std::map<int,Reaction*> needed_values;
    for (unsigned widx = 0; widx < target_warps.size(); widx++)
    {
      if (!target_warps[widx]->reactions.empty())
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(0);
        Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
        assert(reac != NULL);
        // Check to see if it needs a mole frac value
        if (reac->low.enabled)
        {
          needed_values[widx] = reac;
        }
      }
    }
    // Wait until the mole fraction variables are ready
    ost << "asm volatile(\"bar.sync 1," << ((unit->threads_per_point+QSSA_WARPS)*32) << ";\" : : : \"memory\");\n";
    ost << REAL << " local_sum = 0.0;\n"; 
    const unsigned species_per_warp = (non_qss_species+(QSSA_WARPS-1))/QSSA_WARPS;
    const bool species_uniform = ((non_qss_species%QSSA_WARPS)==0);
    // Sum up our mole fractions
    for (unsigned idx = 0; idx < species_per_warp; idx++)
    {
      if (!species_uniform && (idx == (species_per_warp-1)))
        ost << "if ((wid+" << (idx*QSSA_WARPS) << ") < " << non_qss_species << ")\n";
      ost << "local_sum += " << MOLE_FRAC << "[wid+" << (idx*QSSA_WARPS) << "][tid];\n";
    }
    // Write our value into the pipe
    ost << "pipe[wid][0][tid] = local_sum;\n";
    // Wait until all the values are there
    ost << "asm volatile(\"bar.sync 3," << (QSSA_WARPS*32) << ";\" : : : \"memory\");\n";
    // Compute our third body values
    ost << REAL << " " << THIRD_BODY << "[" << unit->third_bodies.size() << "];\n";
    ost << THIRD_BODY << "[0] = 0.0;\n";
    // Sum up the mole fractions for the third body
    for (unsigned idx = 0; idx < QSSA_WARPS; idx++)
      ost << THIRD_BODY << "[0] += pipe[" << idx << "][0][tid];\n";
    // If we're done reading from the pipe we can mark it here
    if (needed_values.empty())
      ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+QSSA_WARPS)*32) << ";\" : : : \"memory\");\n";
    // Compute the other third body values
    for (unsigned idx = 1; idx < unit->third_bodies.size(); idx++)
    {
      ost << THIRD_BODY << "[" << idx << "] = " << THIRD_BODY << "[0]";
      for (std::map<Species*,double>::const_iterator it = unit->third_bodies[idx]->components.begin();
            it != unit->third_bodies[idx]->components.end(); it++)
      {
        if (!it->first->qss_species)
          ost << " + " << it->second << "*" << MOLE_FRAC << "[" << it->first->code_name << "][tid]"; 
      }
      ost << ";\n";
    }
    if (!needed_values.empty())
    {
      for (std::map<int,Reaction*>::const_iterator it = needed_values.begin();
            it != needed_values.end(); it++)
      {
        Reaction *reac = it->second;
        ost << "pipe[" << it->first << "][0][tid] = ";
        if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
          ost << MOLE_FRAC << "[" << reac->pressure_species->code_name << "][tid]";
        else
          ost << THIRD_BODY << "[" << reac->thb->idx << "]";
        ost << ";\n";
      }
      ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+QSSA_WARPS)*32) << ";\" : : : \"memory\");\n";
    }
  } 
  // Start pulling values out of the pipe
  assert(!target_warps.empty());
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    // Count how many values are in this stage
    unsigned num_values = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (stage_idx < target_warps[idx]->reactions.size())
        num_values++;
    }
    std::map<int,Reaction*> needed_values;
    for (unsigned widx = 0; widx < num_values; widx++)
    {
      int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
      Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
      assert(reac != NULL);
      // Check to see if it needs a mole frac value
      if (reac->low.enabled)
      {
        needed_values[widx] = reac;
      }
    }
    ost << "// Stage " << stage_idx << "\n"; 
    PairDelim stage_pair(ost);
    ost << REAL << " rr_f[" << num_values << "];\n"; 
    ost << REAL << " rr_r[" << num_values << "];\n";
    PairDelim if_pair(ost);
    ost << "asm volatile(\"bar.sync 1," << ((unit->threads_per_point+QSSA_WARPS)*32) << ";\" : : : \"memory\");\n";
    for (unsigned idx = 0; idx < num_values; idx++)
    {
      ost << "rr_f[" << idx << "] = pipe[" << idx << "][0][tid];\n";
      ost << "rr_r[" << idx << "] = pipe[" << idx << "][1][tid];\n";
    }
    // Now emit any writes for variables that are needed
    if (!needed_values.empty())
    {
      for (std::map<int,Reaction*>::const_iterator it = needed_values.begin();
            it != needed_values.end(); it++)
      {
        Reaction *reac = it->second;
        ost << "pipe[" << it->first << "][0][tid] = ";
        if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
          ost << MOLE_FRAC << "[" << reac->pressure_species->code_name << "][tid]";
        else
          ost << THIRD_BODY << "[" << reac->thb->idx << "]";
        ost << ";\n";
      }
    }
    // See if there are any needed values that we need to write
    ost << "asm volatile(\"bar.arrive 0," << ((unit->threads_per_point+QSSA_WARPS)*32) << ";\" : : : \"memory\");\n";
    // Forea each of the reactions we need to scale them by their mole fractions
    // Now scale the values by their mole fractions
    for (unsigned widx = 0; widx < num_values; widx++)
    {
      int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
      Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
      assert(reac != NULL);
      for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
            it != reac->forward.end(); it++)
      {
        if (!it->first->qss_species)
        {
          for (int j = 0; j < it->second; j++)
            ost << "rr_f[" << widx << "] *= " << MOLE_FRAC << "[" << it->first->code_name << "][tid];\n";
        }
      }
      if (reac->reversible)
      {
        for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
              it != reac->backward.end(); it++)
        {
          if (!it->first->qss_species)
          {
            for (int j = 0; j < it->second; j++)
              ost << "rr_r[" << widx << "] *= " << MOLE_FRAC << "[" << it->first->code_name << "][tid];\n";
          }
        }
      }
      if ((reac->thb != NULL) && !reac->pressure_dep)
      {
        ost << "rr_f[" << widx << "] *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
        ost << "rr_r[" << widx << "] *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
      }
    }
    for (unsigned qidx = 0; qidx < qss_partition.size(); qidx++)
    {
      if (qidx == 0)
        ost << "if ((wid == " << qidx << "))\n";
      else
        ost << "else if ((wid == " << qidx << "))\n";
      PairDelim qidx_pair(ost);
      for (unsigned widx = 0; widx < num_values; widx++)
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
        assert(reac_idx != -1);
        int avars_off = 0;
        int denom_off = 0;
        for (std::map<QSS*,int>::const_iterator it = qss_partition[qidx].begin();
              it != qss_partition[qidx].end(); it++)
        {
          it->first->emit_multi_updates(ost, avars_off, denom_off, widx, reac_idx); 
          avars_off += it->second;
          denom_off++;
        }
      }
    }
  }
  // Once we're done then emit all of the outputs
  unsigned write_idx = 0;
  for (unsigned qidx = 0; qidx < qss_partition.size(); qidx++)
  {
    if (qidx == 0)
      ost << "if ((wid == " << qidx << "))\n";
    else
      ost << "else if ((wid == " << qidx << "))\n"; 
    PairDelim qidx_pair(ost);
    int avars_off = 0;
    int denom_off = 0;
    for (std::map<QSS*,int>::const_iterator it = qss_partition[qidx].begin();
          it != qss_partition[qidx].end(); it++)
    {
      it->first->emit_multi_stores(ost, avars_off, denom_off, write_idx);
      avars_off += it->second;
      denom_off++;
      write_idx += it->second;
    }
  }
}

void ChemistryUnit::emit_multi_qssa_kernel(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species, 
                                            const std::vector<std::map<QSS*,int> > &qss_partition, int max_values, int max_denoms)
{
  const int pass = 0;
  // Emit the load for our temperature
  {
    unit->emit_numerical_constants(ost);
    ost << REAL << " " << TEMPERATURE << ";\n";
    emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,false/*double2*/,".cg");
  }
  // Emit loads for the species that we need
  {
    const unsigned num_mass_fracs = (non_qss_species+(unit->threads_per_point-1))/unit->threads_per_point;  
    const bool uniform_mass_fracs = ((non_qss_species%unit->threads_per_point) == 0);
    ost << REAL << " " << MASS_FRAC << "[" << num_mass_fracs << "];\n";
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx == (num_mass_fracs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride",idx*unit->threads_per_point);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      emit_cuda_load(ost,MASS_FRAC,dst_offset,MASS_FRAC_ARRAY,src_offset,false/*double2*/,".cs");
    }
    ost << REAL << " " << PRESSURE << ";\n";
    emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,false/*double2*/,".cg");
    // Compute our local sumyow value
    ost << REAL << " local_sumyow = 0.0;\n";
    const unsigned upper_bound = next_largest_power(2*non_qss_species,2);
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx*unit->threads_per_point))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      PairDelim mass_pair(ost);
      ost << MASS_FRAC << "[" << idx << "] *= recip_molecular_mass[(wid+" << (idx*unit->threads_per_point)
          << "+step*" << upper_bound << ")%" << upper_bound << "];\n";
      ost << "local_sumyow += " << MASS_FRAC << "[" << idx << "];\n";
    }
    emit_barrier(ost, 0, unit->threads_per_point, true/*blocking*/);  
    // Write our values into the pipe
    ost << "pipe[wid][0][tid] = local_sumyow;\n";
    emit_barrier(ost, 0, unit->threads_per_point, true);
    // Everyone read out all the values compute the mole fractions and write them into shared memory
    ost << REAL << " sumyow = 0.0;\n";
    for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
      ost << "sumyow += pipe[" << idx << "][0][tid];\n";
    ost << "sumyow *= (";
    if (unit->no_nondim)
      ost << "8.314510e+07 * " << TEMPERATURE;
    else
      ost << (8.314510e+07 * TEMPERATURE_REF / PRESSURE_REF) << " * " << TEMPERATURE;
    ost << ");\n";
    ost << "sumyow = " << PRESSURE << "/sumyow;\n";
    // Scale our values and write them into shared memory
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx == (num_mass_fracs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid] = sumyow * " << MASS_FRAC << "[" << idx << "];\n";
    }
    emit_barrier(ost, 0, unit->threads_per_point, true);
  }
  {
    // Figure out if there are any mole fraction or third body values that need
    // to be sent to the reaction warps, if not, we can do the barrier now
    std::map<int,Reaction*> needed_values;
    for (unsigned widx = 0; widx < target_warps.size(); widx++)
    {
      if (!target_warps[widx]->reactions.empty())
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(0);
        Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
        assert(reac != NULL);
        // Check to see if it needs a mole frac value
        if (reac->low.enabled)
        {
          needed_values[widx] = reac;
        }
      }
    }
    ost << REAL << " local_sum = 0.0;\n"; 
    const unsigned species_per_warp = (non_qss_species+(unit->threads_per_point-1))/unit->threads_per_point;
    const bool species_uniform = ((non_qss_species%unit->threads_per_point)==0);
    // Sum up our mole fractions
    for (unsigned idx = 0; idx < species_per_warp; idx++)
    {
      if (!species_uniform && (idx == (species_per_warp-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      ost << "local_sum += " << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid];\n";
    }
    // Write our value into the pipe
    ost << "pipe[wid][0][tid] = local_sum;\n";
    emit_barrier(ost, 0, unit->threads_per_point, true);
    // Compute our third body values
    ost << REAL << " " << THIRD_BODY << "[" << unit->third_bodies.size() << "];\n";
    ost << THIRD_BODY << "[0] = 0.0;\n";
    //ost << REAL << " ctot = 0.0;\n";
    // Sum up the mole fractions for the third body
    for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
      ost << THIRD_BODY << "[0] += pipe[" << idx << "][0][tid];\n";
      //ost << "ctot += pipe[" << idx << "][0][tid];\n";
    // If we're done reading from the pipe we can mark it here
    if (needed_values.empty())
      emit_barrier(ost, 0, unit->threads_per_point, true);
    // Compute the other third body values
    //ost << THIRD_BODY << "[0][tid] = ctot;\n";
    for (unsigned idx = 1; idx < unit->third_bodies.size(); idx++)
    {
      ost << THIRD_BODY << "[" << idx << "] = " << THIRD_BODY << "[0]";
      for (std::map<Species*,double>::const_iterator it = unit->third_bodies[idx]->components.begin();
            it != unit->third_bodies[idx]->components.end(); it++)
      {
        if (!it->first->qss_species)
          ost << " + " << it->second << "*" << MOLE_FRAC << "[" << it->first->code_name << "][tid]"; 
      }
      ost << ";\n";
    }
    if (!needed_values.empty())
    {
      for (std::map<int,Reaction*>::const_iterator it = needed_values.begin();
            it != needed_values.end(); it++)
      {
        Reaction *reac = it->second;
        ost << "pipe[" << it->first << "][0][tid] = ";
        if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
          ost << MOLE_FRAC << "[" << reac->pressure_species->code_name << "][tid]";
        else
          ost << THIRD_BODY << "[" << reac->thb->idx << "]";
        ost << ";\n";
      }
      emit_barrier(ost, 0, unit->threads_per_point, true);
    }
  } 
  {
    ost << "const " << REAL << " otc = 1.0 / " << TEMPERATURE << ";\n";
    ost << "const " << REAL << " ortc = 1.0 / (" << TEMPERATURE << " * R0c);\n";
    ost << "const " << REAL << " vlntemp = log(" << TEMPERATURE ");\n";
    //ost << "const " << REAL << " prt = PA / (R0 * " << TEMPERATURE << ");\n";
    //ost << "const " << REAL << " oprt = 1.0 / prt;\n";
  }
  ost << REAL << " avars[" << max_values << "];\n";
  for (int idx = 0; idx < max_values; idx++)
    ost << "avars[" << idx << "] = 0.0;\n";
  ost << REAL << " denom[" << max_denoms << "];\n";
  for (int idx = 0; idx < max_denoms; idx++)
    ost << "denom[" << idx << "] = 0.0;\n";
  assert(!target_warps.empty());
  unsigned total_gibbs = 0;
  for (unsigned stage_idx = 0; stage_idx < target_warps[0]->reactions.size(); stage_idx++)
  {
    std::vector<Reaction*> weird_reactions;
    std::vector<Reaction*> normal_reactions;
    ost << "// Reaction stage " << stage_idx << " for pass " << pass << "\n";
    {
      PairDelim stage_pair(ost);
      ost << REAL << " rr_f, rr_r;\n";
      int last_weird_warp = -1;
      std::vector<Reaction*> low_reactions;
      std::vector<Reaction*> irreversible_reactions;
      unsigned low_mask = 0;
      std::vector<Warp*> low_warps;
      std::vector<Reaction*> troe_reactions;
      std::vector<Reaction*> lindermann_reactions;
      unsigned troe_mask = 0;
      std::vector<Warp*> troe_warps;
      // Do the arrhenius part for everyone
      for (unsigned widx = 0; widx < target_warps.size(); widx++)
      {
        if (stage_idx < target_warps[widx]->reactions.size())
        {
          int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
          Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
          assert(reac != NULL);
          if (is_weird_reaction(reac))
          {
            weird_reactions.push_back(reac);
            assert((last_weird_warp+1) == int(widx)); // all weird warps should be contiguous
            last_weird_warp = widx;
            // Figure out what kind of arrhenius to do
            if (reac->low.enabled)
            {
              low_reactions.push_back(reac);
              assert(widx < 32); // 1-hot encoding doesn't work past 32 bits
              low_mask |= (1 << widx); 
              low_warps.push_back(target_warps[widx]);
              if (reac->troe.num > 0)
              {
                assert(reac->troe.num == 4); // assume four troe constants for now
                troe_reactions.push_back(reac);
                troe_mask |= (1 << widx);
                troe_warps.push_back(target_warps[widx]);
              }
              else if (reac->sri.num == 0)
              {
                lindermann_reactions.push_back(reac);
              }
              else
                assert(false); // not handling sri right now
              if (!unit->no_nondim)
              {
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                        (reac->low.a*REACTION_RATE_REF), reac->low.beta, reac->low.e, -1, pass);
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
              }
              else
              {
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                        reac->low.a, reac->low.beta, reac->low.e, -1, pass);
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        reac->a, reac->beta, reac->e, -1, pass); 
              }
              assert(!reac->rev.enabled); // hoping we never have to do a reverse reaction in this case 
              assert(reac->reversible);
            }
            else if (!reac->lt.enabled)
            {
              assert(!reac->rlt.enabled);
              assert(!reac->reversible);
              irreversible_reactions.push_back(reac);
              if (!unit->no_nondim)
              {
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                        (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
                if (reac->rev.enabled)
                {
                  emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                          (reac->rev.a*REACTION_RATE_REF), reac->rev.beta, reac->rev.e, -1, pass);
                }
                else
                {
                  // We're just going to write zero anyway, so what does it matter
                  // Duplicate the computation so we don't diverge, but with coefficients that won't do anything weird
                  emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                          (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
                }
              }
              else
              {
                emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                        reac->a, reac->beta, reac->e, -1, pass);
                if (reac->rev.enabled)
                {
                  emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                          reac->rev.a, reac->rev.beta, reac->rev.e, -1, pass);
                }
                else
                {
                  emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        reac->a, reac->beta, reac->e, -1, pass);
                }
              }
            }
            else
            {
              assert(false); // Not currently handling landau-teller right now since it doesn't use arrhenius
            }
          }
          else
          {
            normal_reactions.push_back(reac);
            // Normal arrhenius and we're done
            if (!unit->no_nondim)
            {
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                        (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                        (reac->rev.a*REACTION_RATE_REF), reac->rev.beta, reac->rev.e, -1, pass);
            }
            else
            {
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_f",
                          reac->a, reac->beta, reac->e, -1, pass);
              emit_pipe_arrhenius(ost, target_warps[widx], (widx==0), false/*weird*/, stage_idx, "rr_r",
                          reac->rev.a, reac->rev.beta, reac->rev.e, -1, pass);
            }
          }
        }
      }
      if (!weird_reactions.empty())
      {
        if (!normal_reactions.empty())
        {
          assert(last_weird_warp != -1);
          ost << "if (__any(wid < " << (last_weird_warp+1) << "))\n";
        }
        PairDelim weird_pair(ost);
        // Now emit the code for the weird reactions
        // Emit the code for the different cases of weird reactions
        if (!low_reactions.empty())
        {
          if (!irreversible_reactions.empty())
            ost << "if (__any(((1 << wid) & " << low_mask << ")))\n";
          PairDelim low_pair(ost);
          ost << REAL << " rr_k0 = rr_f;\n";
          ost << REAL << " rr_kinf = rr_r;\n";
          ost << REAL << " pr = rr_k0 / rr_kinf;\n";
          // Figure out the maximum number of gibbs species we need to compute for any of the troe warps
          unsigned max_gibbs = 0;
          for (std::vector<Reaction*>::const_iterator it = low_reactions.begin();
                it != low_reactions.end(); it++)
          {
            if ((*it)->stoich.size() > max_gibbs)
              max_gibbs = (*it)->stoich.size();
          }
          // shouldn't be zero 
          assert(max_gibbs > 0);
          // emit the code to compute the gibbs values that we need
          ost << "// Gibbs computation\n";
          {
            unsigned old_total_gibbs = total_gibbs;
            PairDelim gibbs_pair(ost);
            ost << REAL << " xik = 0.0;\n";
            ost << "const " << REAL << " &tk1 = " << TEMPERATURE << ";\n";
            ost << "const " << REAL << " tk2 = tk1*tk1;\n";
            ost << "const " << REAL << " tk3 = tk1*tk2;\n";
            ost << "const " << REAL << " tk4 = tk1*tk3;\n";
            ost << "const " << REAL << " tk5 = tk1*tk4;\n";
            ost << INT << " gibbs_idx;\n";
            for (unsigned idx = 0; idx < max_gibbs; idx++)
            {
              ost << REAL << " gibbs_" << idx << ";\n";
              // Do something to fool the compiler here
              unsigned upper_bound = next_largest_power(2*non_qss_species,2);
              ost << "gibbs_idx = (gibbs_index_" << pass << "[(" << total_gibbs 
                  << "+step*" << upper_bound << ")%" << upper_bound << "][wid]"
                  << "+step*" << upper_bound << ")%" << upper_bound << ";\n";
              ost << "if (tk1 > gibbs_values[gibbs_idx][0])\n";
              {
                PairDelim if_pair(ost);
                ost << "gibbs_" << idx << " = gibbs_values[gibbs_idx][1]*tk1*(";
                if (!unit->no_nondim)
                  ost << (1.0-log(TEMPERATURE_REF));
                else
                  ost << "1.0";
                ost << "- vlntemp) + "
                    << "gibbs_values[gibbs_idx][2]*tk2 + "
                    << "gibbs_values[gibbs_idx][3]*tk3 + "
                    << "gibbs_values[gibbs_idx][4]*tk4 + "
                    << "gibbs_values[gibbs_idx][5]*tk5 + "
                    << "(gibbs_values[gibbs_idx][6] - tk1*"
                    << "gibbs_values[gibbs_idx][7]);\n";
              }
              ost << "else\n";
              {
                PairDelim else_pair(ost);
                ost << "gibbs_" << idx << " = gibbs_values[gibbs_idx][8]*tk1*(";
                if (!unit->no_nondim)
                  ost << (1.0-log(TEMPERATURE_REF));
                else
                  ost << "1.0";
                ost << " - vlntemp) + "
                    << "gibbs_values[gibbs_idx][9]*tk2 + "
                    << "gibbs_values[gibbs_idx][10]*tk3 + "
                    << "gibbs_values[gibbs_idx][11]*tk4 + "
                    << "gibbs_values[gibbs_idx][12]*tk5 + "
                    << "(gibbs_values[gibbs_idx][13] - tk1*"
                    << "gibbs_values[gibbs_idx][14]);\n";
              }
              // Do something to fool the compiler here
              ost << "xik = __fma_rn(gibbs_scale_" << pass << "[(" << total_gibbs 
                  << "+step*" << upper_bound << ")%" << upper_bound << "][wid],gibbs_" << idx << ",xik);\n";
              total_gibbs++;
            }
            ost << "rr_r = exp(xik*otc - reverse_pass_" << pass << "[" << stage_idx << "][wid]*";
            if (!unit->no_nondim)
              ost << "(vlntemp + " << TEMPERATURE_REF << ")";
            else
              ost << "vlntemp";
            ost << ");\n";
            // Now update low warps with the information that they need to emit the constants
            assert(low_reactions.size() == low_warps.size());
            for (unsigned idx = 0; idx < low_warps.size(); idx++)
            {
              Warp *warp = low_warps[idx];
              Reaction *reac = low_reactions[idx];
              unsigned added_gibbs = 0;
              int nutot = 0;
              for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
                    it != reac->stoich.end(); it++)
              {
                int spec_idx = -1;
                for (unsigned gidx = 0; gidx < gibbs_species.size(); gidx++)
                {
                  if (it->first == gibbs_species[gidx])
                  {
                    spec_idx = gidx;
                    break;
                  }
                }
                if (spec_idx == -1)
                {
                  spec_idx = gibbs_species.size();
                  gibbs_species.push_back(it->first);
                }
                warp->add_gibbs_species(old_total_gibbs+added_gibbs,spec_idx,double(it->second),true);
                added_gibbs++;
                nutot += it->second;
              }
              while (added_gibbs < max_gibbs)
              {
                warp->add_gibbs_species(old_total_gibbs+added_gibbs,0,0.0,true);
                added_gibbs++;
              }
              // Also update the reverse value
              warp->add_reverse_constant(stage_idx, double(nutot),true);
            }
          }

          // Now we need to wait until we get our information from the mole fraction warp
          // about our scaling value
          ost << "pr *= pipe[wid][0][tid];\n";
          if (!troe_reactions.empty())
          {
            if (!lindermann_reactions.empty())
              ost << "if (__any(((1 << wid) & " << troe_mask << ")))\n";
            PairDelim troe_pair(ost);
            // Do something to fool the compiler here
            assert(non_qss_species >= needed_troe_reactions.size());
            unsigned upper_bound = next_largest_power(2*non_qss_species,2);
            ost << INT << " troe_idx = (troe_index_" << pass << "[(" << stage_idx 
                << "+step*" << upper_bound << ")%" << upper_bound 
                << "][wid] + step*" << upper_bound << ")%" << upper_bound << ";\n";
            ost << REAL << " fcent = log10(troe_values[troe_idx][0] * exp("
                << "troe_values[troe_idx][1] * " << TEMPERATURE << ")"
                << " + troe_values[troe_idx][2] * exp("
                << "troe_values[troe_idx][3] * " << TEMPERATURE << ")"
                << " + exp(troe_values[troe_idx][4] * otc" << "));\n";
            // add the troe constants
            assert(troe_reactions.size() == troe_warps.size());
            for (unsigned troe_idx = 0; troe_idx < troe_warps.size(); troe_idx++)
            {
              Warp *troe = troe_warps[troe_idx];
              Reaction *reac = troe_reactions[troe_idx];
              int tr_idx = -1;
              for (unsigned tidx = 0; tidx < needed_troe_reactions.size(); tidx++)
              {
                if (needed_troe_reactions[tidx] == reac)
                {
                  tr_idx = tidx;
                  break;
                }
              }
              if (tr_idx == -1)
              {
                tr_idx = needed_troe_reactions.size();
                needed_troe_reactions.push_back(reac);
              }
              troe->add_troe_reaction(stage_idx, tr_idx, true);
            }
            ost << REAL << " flogpr = log10(pr) - 0.4 - 0.67 * fcent;\n";
            ost << REAL << " fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;\n";
            ost << REAL << " fquan = flogpr / fdenom;\n";
            ost << "fquan = fcent / (1.0 + fquan * fquan);\n";
            ost << "rr_f = rr_kinf * pr/(1.0 + pr) * exp(fquan * DLn10);\n";
          }
          if (!lindermann_reactions.empty())
          {
            if (!troe_reactions.empty())
              ost << "else\n";
            PairDelim linderman_pair(ost);
            ost << "rr_f = rr_kinf * pr/(1.0 + pr);\n";
          }
          // Now that we have rr_f, we can compute the final part of rr_r
          ost << "rr_r *= rr_f;\n";
          // Write our results out into shared memory, no need to wait since we already know the buffer is ready
          //ost << "pipe[wid][0][tid] = rr_f;\n";
          //ost << "pipe[wid][1][tid] = rr_r;\n";
        }
        if (!irreversible_reactions.empty())
        {
          if (!low_reactions.empty())
            ost << "else\n";
          PairDelim irrev_pair(ost);
          ost << "rr_r = 0.0; /* irreversible reaction */\n";
          //ost << "pipe[wid][0][tid] = rr_f;\n";
          //ost << "pipe[wid][1][tid] = rr_r*0.0; /* irreversible reaction */\n";
        }
      }
#if 0
      // Now do all the normal reactions
      if (!normal_reactions.empty())
      {
        if (!weird_reactions.empty())
          ost << "else\n";
        PairDelim normal_pair(ost);
        // For the normal case we can just do the normal thing and write our values into
        // shared memory ass soon as the buffer is ready
        // Write our result into the pipe
        ost << "pipe[wid][0][tid] = rr_f;\n";
        ost << "pipe[wid][1][tid] = rr_r;\n";
      }
#endif
      // Now do the scaling for each of our reactions
      for (unsigned widx = 0; widx < target_warps.size(); widx++)
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
        if (reac_idx == -1)
          continue;
        if (widx == 0)
          ost << "if (wid == 0)\n";
        else
          ost << "else if (wid == " << widx << ")\n";
        PairDelim scale_pair(ost);
        Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
        assert(reac != NULL);
        for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
              it != reac->forward.end(); it++)
        {
          if (!it->first->qss_species)
          {
            for (int j = 0; j < it->second; j++)
              ost << "rr_f *= " << MOLE_FRAC << "[" << it->first->code_name << "][tid];\n";
          }
        }
        if (reac->reversible)
        {
          for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                it != reac->backward.end(); it++)
          {
            if (!it->first->qss_species)
            {
              for (int j = 0; j < it->second; j++)
                ost << "rr_r *= " << MOLE_FRAC << "[" << it->first->code_name << "][tid];\n";
            }
          }
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          ost << "rr_f *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
          ost << "rr_r *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
        }
      }
      // Now write our results and mark that we're done
      ost << "pipe[wid][0][tid] = rr_f;\n";
      ost << "pipe[wid][1][tid] = rr_r;\n";
    }
    emit_barrier(ost, 0, unit->threads_per_point, true);
    // Now we can pull the values out and scale them
    // Count how many values are in this stage
    unsigned num_values = 0;
    for (unsigned idx = 0; idx < target_warps.size(); idx++)
    {
      if (stage_idx < target_warps[idx]->reactions.size())
        num_values++;
    }
    std::map<int,Reaction*> needed_values;
    for (unsigned widx = 0; widx < num_values; widx++)
    {
      int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
      Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
      assert(reac != NULL);
      // Check to see if it needs a mole frac value
      if (reac->low.enabled)
      {
        needed_values[widx] = reac;
      }
    }
    ost << "// Handling results of stage " << stage_idx << "\n"; 
    {
      PairDelim result_pair(ost);
#if 0
      ost << REAL << " rr_f[" << num_values << "];\n"; 
      ost << REAL << " rr_r[" << num_values << "];\n";
      PairDelim if_pair(ost);
      for (unsigned idx = 0; idx < num_values; idx++)
      {
        ost << "rr_f[" << idx << "] = pipe[" << idx << "][0][tid];\n";
        ost << "rr_r[" << idx << "] = pipe[" << idx << "][1][tid];\n";
      }
#endif 
#if 0
      // For each of the reactions we need to scale them by their mole fractions
      // Now scale the values by their mole fractions
      for (unsigned widx = 0; widx < num_values; widx++)
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
        Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
        assert(reac != NULL);
        for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
              it != reac->forward.end(); it++)
        {
          if (!it->first->qss_species)
          {
            for (int j = 0; j < it->second; j++)
              ost << "rr_f[" << widx << "] *= " << MOLE_FRAC << "[" << it->first->code_name << "][tid];\n";
          }
        }
        if (reac->reversible)
        {
          for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                it != reac->backward.end(); it++)
          {
            if (!it->first->qss_species)
            {
              for (int j = 0; j < it->second; j++)
                ost << "rr_r[" << widx << "] *= " << MOLE_FRAC << "[" << it->first->code_name << "][tid];\n";
            }
          }
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          ost << "rr_f[" << widx << "] *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
          ost << "rr_r[" << widx << "] *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
        }
      }
#endif
      for (unsigned qidx = 0; qidx < qss_partition.size(); qidx++)
      {
        if (qidx == 0)
          ost << "if ((wid == " << qidx << "))\n";
        else
          ost << "else if ((wid == " << qidx << "))\n";
        PairDelim qidx_pair(ost);
        for (unsigned widx = 0; widx < num_values; widx++)
        {
          int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
          assert(reac_idx != -1);
          Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
          assert(reac != NULL);
          int avars_off = 0;
          int denom_off = 0;
          for (std::map<QSS*,int>::const_iterator it = qss_partition[qidx].begin();
                it != qss_partition[qidx].end(); it++)
          {
            it->first->emit_multi_scaled_updates(ost, avars_off, denom_off, widx, reac_idx, reac); 
            avars_off += it->second;
            denom_off++;
          }
        }
      }
      // Now emit any writes for variables that are needed
      if (!needed_values.empty())
      {
        for (std::map<int,Reaction*>::const_iterator it = needed_values.begin();
              it != needed_values.end(); it++)
        {
          Reaction *reac = it->second;
          ost << "pipe[" << it->first << "][0][tid] = ";
          if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
            ost << MOLE_FRAC << "[" << reac->pressure_species->code_name << "][tid]";
          else
            ost << THIRD_BODY << "[" << reac->thb->idx << "]";
          ost << ";\n";
        }
      }
    }
    emit_barrier(ost, 0, unit->threads_per_point, true);
  }
  // Once we're done then emit all of the outputs
  unsigned write_idx = 0;
  for (unsigned qidx = 0; qidx < qss_partition.size(); qidx++)
  {
    if (qidx == 0)
      ost << "if ((wid == " << qidx << "))\n";
    else
      ost << "else if ((wid == " << qidx << "))\n"; 
    PairDelim qidx_pair(ost);
    int avars_off = 0;
    int denom_off = 0;
    for (std::map<QSS*,int>::const_iterator it = qss_partition[qidx].begin();
          it != qss_partition[qidx].end(); it++)
    {
      it->first->emit_multi_stores(ost, avars_off, denom_off, write_idx);
      avars_off += it->second;
      denom_off++;
      write_idx += it->second;
    }
  }
}

void ChemistryUnit::emit_last_getrates_code(CodeOutStream &ost, unsigned non_qss_species, const char *temp_last)
{
  ost << "\n";
  emit_pipe_general_constants(ost, non_qss_species);
  unsigned pass0_per_thread, pass0_constant_stride;
  emit_pipe_warp_constants(ost, full_warps, 0/*pass*/, pass0_per_thread, pass0_constant_stride);
  // emit the kernel
  {
    ost << "\n";
    ost << "__global__ void\n";
    ost << "gpu_getrates(";
    ost << "const " << REAL << " *" << PRESSURE_ARRAY;
    ost << ", const " << REAL << " *" << TEMPERATURE_ARRAY;
    ost << ", const " << REAL << " *" << MASS_FRAC_ARRAY;
    ost << ", const " << REAL << " *" << DIFFUSION_ARRAY;
    ost << ", const " << REAL << " dt";
    ost << ", const " << REAL << " recip_dt/*reciprocal of dt*/";
    ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
    ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
    ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
    ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
    ost << ", const " << INT << " step_stride/*always zero*/";
    ost << ", " << REAL << " *" << WDOT;
    ost << ")\n";
    PairDelim pair(ost);
    // Compute the offset for this thread
#if 0
    {
      //ost << INT << " hi_temp, lo_temp, hi_press, lo_press, hi_mass, lo_mass, hi_diff, lo_diff, hi_wdot, lo_wdot;\n";
      PairDelim offset_pair(ost);
      ost << "const " << INT << " offset = (blockIdx.y*row_stride) + "
          << "(blockIdx.x*32 + (threadIdx.x & 0x1f));\n";
      ost << PRESSURE_ARRAY << " += offset;\n";
      ost << TEMPERATURE_ARRAY << " += offset;\n";
      ost << MASS_FRAC_ARRAY << " += offset;\n";
      ost << DIFFUSION_ARRAY << " += offset;\n";
      ost << WDOT << " += offset;\n";
#if 0
      ost << "long long int temp_long = (long long int)" << TEMPERATURE_ARRAY << ";\n";
      ost << "hi_temp = __double2hiint(__longlong_as_double(temp_long));\n";
      ost << "lo_temp = __double2loint(__longlong_as_double(temp_long));\n";
      ost << "long long int press_long = (long long int)" << PRESSURE_ARRAY << ";\n";
      ost << "hi_press = __double2hiint(__longlong_as_double(press_long));\n";
      ost << "lo_press = __double2loint(__longlong_as_double(press_long));\n";
      ost << "long long int mass_long = (long long int)" << MASS_FRAC_ARRAY << ";\n";
      ost << "hi_mass = __double2hiint(__longlong_as_double(mass_long));\n";
      ost << "lo_mass = __double2loint(__longlong_as_double(mass_long));\n";
      ost << "long long int diff_long = (long long int)" << DIFFUSION_ARRAY << ";\n";
      ost << "hi_diff = __double2hiint(__longlong_as_double(diff_long));\n";
      ost << "lo_diff = __double2loint(__longlong_as_double(diff_long));\n";
      ost << "long long int wdot_long = (long long int)" << WDOT << ";\n";
      ost << "hi_wdot = __double2hiint(__longlong_as_double(wdot_long));\n";
      ost << "lo_wdot = __double2loint(__longlong_as_double(wdot_long));\n";
#endif
    }
#endif
    // Declare the shared memory for this thread block
    ost << "volatile __shared__ " << REAL << " scratch[192][32];\n";
    //ost << "volatile __shared__ " << REAL << " " << THIRD_BODY << "[" << unit->third_bodies.size() << "][32];\n";
    // Now we can split the warps
    //ost << "if (__any(threadIdx.x < " << (unit->threads_per_point*32) << "))\n";
    {
      PairDelim if_pair(ost);
      ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
      ost << "const " << INT << " wid = threadIdx.x >> 5;\n";
#ifndef TARGET_FERMI
      ost << REAL << " pass_0_constants[" << pass0_per_thread << "];\n";
      for (unsigned idx = 0; idx < pass0_per_thread; idx++)
      {
        char src_offset[128];
        sprintf(src_offset,"wid*%d+%d",pass0_constant_stride,(idx*32));
        char dst_offset[128];
        sprintf(dst_offset,"%d",idx);
        emit_cuda_load(ost,"pass_0_constants",dst_offset,"warp_constants_0",src_offset,false/*double2*/,".cg");
      }
#endif
      ost << "if (wid == 0)\n";
      {
        PairDelim offset_pair(ost);
        ost << "const " << INT << " offset = (blockIdx.y*row_stride) + "
            << "(blockIdx.x*32 + tid);\n";
        ost << PRESSURE_ARRAY << " += offset;\n";
        ost << TEMPERATURE_ARRAY << " += offset;\n";
        ost << MASS_FRAC_ARRAY << " += offset;\n";
        ost << DIFFUSION_ARRAY << " += offset;\n";
        ost << WDOT << " += offset;\n"; 
        ost << "scratch[" << PRESSURE_IDX << "][tid] = __longlong_as_double((long long int)" << PRESSURE_ARRAY << ");\n";
        ost << "scratch[" << TEMPERATURE_IDX << "][tid] = __longlong_as_double((long long int)" << TEMPERATURE_ARRAY << ");\n";
        ost << "scratch[" << MASS_FRAC_IDX << "][tid] = __longlong_as_double((long long int)" << MASS_FRAC_ARRAY << ");\n";
        ost << "scratch[" << DIFFUSION_IDX << "][tid] = __longlong_as_double((long long int)" << DIFFUSION_ARRAY << ");\n";
        ost << "scratch[" << WDOT_IDX << "][tid] = __longlong_as_double((long long int)" << WDOT << ");\n";
      }
      // Wait for the pointers to be in place
      emit_barrier(ost, 0, unit->threads_per_point, true);
      // Then copy over the code from the temporary file
      FILE *temp_file = fopen(temp_last,"r");
      assert(temp_file != NULL);
      char c = fgetc(temp_file);
      while (c != EOF)
      {
        ost << c;
        c = fgetc(temp_file);
      }
      assert(fclose(temp_file) == 0);
      // Catch the last wake up from the qssa warps
      //ost << "asm volatile(\"bar.sync 0," << ((unit->threads_per_point+QSSA_WARPS)*32) << ";\" : : : \"memory\");\n";
    }
  }
}

void ChemistryUnit::emit_last_reaction_code(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species, 
                                            unsigned last_qss_idx, unsigned first_non_qss_idx, unsigned max_stages,
                                            const std::vector<std::map<QSS*,int> > &qss_partition, int max_values, int max_denoms)
{
  // Emit the load for our temperature
  {
    unit->emit_numerical_constants(ost);
    ost << REAL << " " << TEMPERATURE << ";\n";
    ost << REAL << " *temp_ptr = (" << REAL << "*)__double_as_longlong(scratch[" << TEMPERATURE_IDX << "][tid]);\n";
    //const char *temp_src = "((double*)__double_as_longlong(__hiloint2double(hi_temp,lo_temp)))";
    emit_cuda_load(ost,TEMPERATURE,"temp_ptr"/*TEMPERATURE_ARRAY*/,false/*double2*/,".cg");
  }
  // Emit loads for the species that we need
  {
    const unsigned num_mass_fracs = (non_qss_species+(unit->threads_per_point-1))/unit->threads_per_point;  
    const bool uniform_mass_fracs = ((non_qss_species%unit->threads_per_point) == 0);
    ost << REAL << " " << MASS_FRAC << "[" << num_mass_fracs << "];\n";
    ost << REAL << " *mass_frac_ptr = (" << REAL << "*)__double_as_longlong(scratch[" << MASS_FRAC_IDX << "][tid]);\n";
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx == (num_mass_fracs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride+step*step_stride",idx*unit->threads_per_point);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      //const char *mass_frac_src = "((double*)__double_as_longlong(__hiloint2double(hi_mass,lo_mass)))";
      emit_cuda_load(ost,MASS_FRAC,dst_offset,"mass_frac_ptr"/*MASS_FRAC_ARRAY*/,src_offset,false/*double2*/,".cs");
    }
    ost << REAL << " " << PRESSURE << ";\n";
    ost << REAL << " *press_ptr = (" << REAL << "*)__double_as_longlong(scratch[" << PRESSURE_IDX << "][tid]);\n";
    //const char *press_src = "((double*)__double_as_longlong(__hiloint2double(hi_press,lo_press)))";
    emit_cuda_load(ost,PRESSURE,"press_ptr"/*PRESSURE_ARRAY*/,false/*double2*/,".cg");
    {
      ost << "const " << REAL << " otc = 1.0 / " << TEMPERATURE << ";\n";
      ost << "const " << REAL << " ortc = 1.0 / (" << TEMPERATURE << " * R0c);\n";
      ost << "const " << REAL << " vlntemp = log(" << TEMPERATURE ");\n";
      //ost << "const " << REAL << " prt = PA / (R0 * " << TEMPERATURE << ");\n";
      //ost << "const " << REAL << " oprt = 1.0 / prt;\n";
    }
    // Figure out the set of gibbs species and troe reaction values that we need to compute
    // then emit the computations for the troe values and gibbs values and put them in shared memory
    {
      this->gibbs_species.clear();
      this->needed_troe_reactions.clear();
      for (std::vector<Reaction*>::const_iterator it = unit->all_reactions.begin();
            it != unit->all_reactions.end(); it++)
      {
        if ((*it)->low.enabled)
        {
          if ((*it)->troe.num > 0)
            needed_troe_reactions.push_back(*it);
        }
        if ((*it)->reversible)
        {
          gibbs_reactions.push_back(*it);
          // See what gibbs species are needed
          for (SpeciesCoeffMap::const_iterator spec_it = (*it)->stoich.begin();
                spec_it != (*it)->stoich.end(); spec_it++)
          {
            // check to see if it's already in the list
            bool added = false;
            for (unsigned idx = 0; idx < gibbs_species.size(); idx++)
            {
              if (gibbs_species[idx] == spec_it->first)
              {
                added = true;
                break;
              }
            }
            if (!added)
              gibbs_species.push_back(spec_it->first);
          }
        }
      }
      // Assign warps to compute the troe values and write them into shared memory
      unsigned troe_passes = (needed_troe_reactions.size() + (unit->threads_per_point-1))/unit->threads_per_point;
      bool troe_uniform = ((needed_troe_reactions.size()%unit->threads_per_point) == 0);
      for (unsigned pass_idx = 0; pass_idx < troe_passes; pass_idx++)
      {
        if (!troe_uniform && (pass_idx == (troe_passes-1)))
          ost << "if ((wid+" << (pass_idx*unit->threads_per_point) << ") < " << needed_troe_reactions.size() << ")\n";
        PairDelim troe_pair(ost);
        ost << INT << " troe_idx = wid+" << (pass_idx*unit->threads_per_point) << "+step*step_stride;\n";
        ost << REAL << " troe = log10(troe_values[troe_idx][0] * exp("
            << "troe_values[troe_idx][1] * " << TEMPERATURE << ")"
            << " + troe_values[troe_idx][2] * exp("
            << "troe_values[troe_idx][3] * " << TEMPERATURE << ")"
            << " + exp(troe_values[troe_idx][4] * otc" << ") + troe_values[troe_idx][5]);\n"; 
        ost << "scratch[TROE_OFFSET+wid+" << (pass_idx*unit->threads_per_point) << "][tid] = troe;\n";
      }
      // If we had remainder warps from the troe computation, use them to do the gibbs, otherwise use everyone
      {
        unsigned num_gibbs_warps = unit->threads_per_point;
        unsigned gibbs_offset = 0;
        if (!troe_uniform)
        {
          gibbs_offset = needed_troe_reactions.size()%unit->threads_per_point;
          num_gibbs_warps -= gibbs_offset;
        }
        assert(num_gibbs_warps > 0);
        unsigned gibbs_passes = (gibbs_species.size() + (num_gibbs_warps-1))/num_gibbs_warps;
        bool gibbs_uniform = ((gibbs_species.size()%num_gibbs_warps) == 0);
        if (gibbs_offset > 0)
          ost << "if (wid >= " << gibbs_offset << ")\n";
        PairDelim gibbs_pair(ost);
        // compute the temperature values that are needed for the gibbs computation
        ost << "const " << REAL << " &tk1 = " << TEMPERATURE << ";\n";
        ost << "const " << REAL << " tk2 = tk1*tk1;\n";
        ost << "const " << REAL << " tk3 = tk1*tk2;\n";
        ost << "const " << REAL << " tk4 = tk1*tk3;\n";
        ost << "const " << REAL << " tk5 = tk1*tk4;\n";
        for (unsigned pass_idx = 0; pass_idx < gibbs_passes; pass_idx++)
        {
          if (!gibbs_uniform && (pass_idx == (gibbs_passes-1)))
          {
            if ((pass_idx*num_gibbs_warps) >= gibbs_offset)
              ost << "if ((wid+" << (int(pass_idx*num_gibbs_warps)-gibbs_offset) << ") < " << gibbs_species.size() << ")\n";
            else
              ost << "if ((wid-" << (gibbs_offset-(pass_idx*num_gibbs_warps)) << ") < " << gibbs_species.size() << ")\n";
          }
          PairDelim gibbs_spec_pair(ost);
          if ((pass_idx*num_gibbs_warps) >= gibbs_offset)
            ost << INT << " gibbs_idx = wid+" << (int(pass_idx*num_gibbs_warps)-gibbs_offset) << " + step*step_stride;\n";
          else
            ost << INT << " gibbs_idx = wid-" << (gibbs_offset-(pass_idx*num_gibbs_warps)) << " + step*step_stride;\n";
          ost << REAL << " gibbs_" << pass_idx << ";\n";
          ost << "if (tk1 > gibbs_values[gibbs_idx][0])\n";
          {
            PairDelim if_pair(ost);
            ost << "gibbs_" << pass_idx << " = gibbs_values[gibbs_idx][1]*tk1*(";
            if (!unit->no_nondim)
              ost << (1.0-log(TEMPERATURE_REF));
            else
              ost << "1.0";
            ost << "- vlntemp) + "
                << "gibbs_values[gibbs_idx][2]*tk2 + "
                << "gibbs_values[gibbs_idx][3]*tk3 + "
                << "gibbs_values[gibbs_idx][4]*tk4 + "
                << "gibbs_values[gibbs_idx][5]*tk5 + "
                << "(gibbs_values[gibbs_idx][6] - tk1*"
                << "gibbs_values[gibbs_idx][7]);\n";
          }
          ost << "else\n";
          {
            PairDelim else_pair(ost);
            ost << "gibbs_" << pass_idx << " = gibbs_values[gibbs_idx][8]*tk1*(";
            if (!unit->no_nondim)
              ost << (1.0-log(TEMPERATURE_REF));
            else
              ost << "1.0";
            ost << " - vlntemp) + "
                << "gibbs_values[gibbs_idx][9]*tk2 + "
                << "gibbs_values[gibbs_idx][10]*tk3 + "
                << "gibbs_values[gibbs_idx][11]*tk4 + "
                << "gibbs_values[gibbs_idx][12]*tk5 + "
                << "(gibbs_values[gibbs_idx][13] - tk1*"
                << "gibbs_values[gibbs_idx][14]);\n";
          }
          ost << "scratch[GIBBS_OFFSET+gibbs_idx][tid] = gibbs_" << pass_idx << ";\n";
        }
      }
      // Check to see if we're going to compute the gibbs values for the reactions now
      if (gibbs_reactions.size() <= gibbs_species.size())
      {
        emit_barrier(ost, 0, unit->threads_per_point, true);
        unsigned max_reactions = (gibbs_reactions.size()+(unit->threads_per_point-1))/unit->threads_per_point;
        ost << REAL << " gibbs_reac_rates[" << max_reactions << "];\n";
        for (unsigned idx = 0; idx < max_reactions; idx++)
          ost << "gibbs_reac_rates[" << idx << "] = 0.0;\n";
        for (unsigned widx = 0; widx < unit->threads_per_point; widx++)
        {
          if (widx == 0)
            ost << "if (wid == 0)\n";
          else
            ost << "else if (wid == " << widx << ")\n";
          PairDelim warp_pair(ost);
          unsigned local_idx = 0;
          for (unsigned reac_idx = widx; reac_idx < gibbs_reactions.size(); reac_idx+=unit->threads_per_point, local_idx++)
          {
            Reaction *reac = gibbs_reactions[reac_idx]; 
            int nutot = 0;
            for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
                  it != reac->stoich.end(); it++)
            {
              nutot += it->second;
              // Find the index
              int gibbs_idx = -1;
              for (unsigned idx = 0; idx < gibbs_species.size(); idx++)
              {
                if (it->first == gibbs_species[idx])
                {
                  gibbs_idx = idx;
                  break;
                }
              }
              assert(gibbs_idx != -1);
              if (it->second == 1)
                ost << "gibbs_reac_rates[" << local_idx << "] += scratch[GIBBS_OFFSET+" << gibbs_idx << "][tid];\n";
              else if (it->second == -1)
                ost << "gibbs_reac_rates[" << local_idx << "] -= scratch[GIBBS_OFFSET+" << gibbs_idx << "][tid];\n";
              else
                ost << "gibbs_reac_rates[" << local_idx << "] = __fma_rn(" << double(it->second) << ","
                    << "scratch[GIBBS_OFFSET+" << gibbs_idx << "][tid],gibbs_reac_rates[" << local_idx << "]);\n";
            }
            ost << "gibbs_reac_rates[" << local_idx << "] = exp(gibbs_reac_rates[" << local_idx << "]*otc - "
                << double(nutot) << "*";
            if (!unit->no_nondim)
              ost << "(vlntemp + " << TEMPERATURE_REF << ")";
            else
              ost << "vlntemp";
            ost << ");\n";
          }
        }
        // Wait until we're done
        emit_barrier(ost, 0, unit->threads_per_point, true);
        // Write out our results
        for (unsigned idx = 0; idx < max_reactions; idx++)
          ost << "scratch[wid+GIBBS_OFFSET+" << (idx*unit->threads_per_point) << "][tid] = gibbs_reac_rates[" << idx << "];\n";
        // No need to barrier since there is another barrier before these values are needed
      }
    }

    // Compute our local sumyow value
    ost << REAL << " local_sumyow = 0.0;\n";
    //const unsigned upper_bound = next_largest_power(2*non_qss_species,2);
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx*unit->threads_per_point))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      PairDelim mass_pair(ost);
      //ost << MASS_FRAC << "[" << idx << "] *= recip_molecular_mass[(wid+" << (idx*unit->threads_per_point)
      //    << "+step*" << upper_bound << ")%" << upper_bound << "];\n";
      ost << MASS_FRAC << "[" << idx << "] *= recip_molecular_mass[wid+step*step_stride];\n";
      ost << "local_sumyow += " << MASS_FRAC << "[" << idx << "];\n";
    }
    emit_barrier(ost, 0, unit->threads_per_point, true/*blocking*/);  
    // Write our values into the pipe
    ost << "scratch[wid][tid] = local_sumyow;\n";
    emit_barrier(ost, 0, unit->threads_per_point, true);
    // After this barrier, it's safe to update the pointers for temperature, pressure and mass frac
    {
      ost << "if (wid == 0)\n";
      PairDelim update_pair(ost);
      ost << "scratch[" << PRESSURE_IDX << "][tid] = __longlong_as_double((long long int)(press_ptr+slice_stride));\n";
      ost << "scratch[" << TEMPERATURE_IDX << "][tid] = __longlong_as_double((long long int)(temp_ptr+slice_stride));\n";
      ost << "scratch[" << MASS_FRAC_IDX << "][tid] = __longlong_as_double((long long int)(mass_frac_ptr+slice_stride));\n";
    }
    // Everyone read out all the values compute the mole fractions and write them into shared memory
    ost << REAL << " sumyow = 0.0;\n";
    for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
      ost << "sumyow += scratch[" << idx << "][tid];\n";
    ost << "sumyow *= (";
    if (unit->no_nondim)
      ost << "8.314510e+07 * " << TEMPERATURE;
    else
      ost << (8.314510e+07 * TEMPERATURE_REF / PRESSURE_REF) << " * " << TEMPERATURE;
    ost << ");\n";
    ost << "sumyow = " << PRESSURE << "/sumyow;\n";
    // Scale our values and write them into shared memory
    for (unsigned idx = 0; idx < num_mass_fracs; idx++)
    {
      if (!uniform_mass_fracs && (idx == (num_mass_fracs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      ost << "scratch[wid+" << (idx*unit->threads_per_point) << "][tid] = sumyow * " << MASS_FRAC << "[" << idx << "];\n";
    }
    emit_barrier(ost, 0, unit->threads_per_point, true);
  }
  {
    // Figure out if there are any mole fraction or third body values that need
    // to be sent to the reaction warps, if not, we can do the barrier now
    ost << REAL << " local_sum = 0.0;\n"; 
    const unsigned species_per_warp = (non_qss_species+(unit->threads_per_point-1))/unit->threads_per_point;
    const bool species_uniform = ((non_qss_species%unit->threads_per_point)==0);
    // Sum up our mole fractions
    for (unsigned idx = 0; idx < species_per_warp; idx++)
    {
      if (!species_uniform && (idx == (species_per_warp-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      ost << "local_sum += scratch[wid+" << (idx*unit->threads_per_point) << "][tid];\n";
    }
    // Write our value into the pipe
    ost << "scratch[QSS_OFFSET+wid][tid] = local_sum;\n";
    emit_barrier(ost, 0, unit->threads_per_point, true);
#if 0
    // Compute our third body values
    ost << REAL << " " << THIRD_BODY << "[" << unit->third_bodies.size() << "];\n";
    ost << THIRD_BODY << "[0] = 0.0;\n";
    //ost << REAL << " ctot = 0.0;\n";
    // Sum up the mole fractions for the third body
    for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
      ost << THIRD_BODY << "[0] += scratch[QSS_OFFSET+" << idx << "][tid];\n";
      //ost << "ctot += pipe[" << idx << "][0][tid];\n";
    // If we're done reading from the pipe we can mark it here
    // Compute the other third body values
    //ost << THIRD_BODY << "[0][tid] = ctot;\n";
    for (unsigned idx = 1; idx < unit->third_bodies.size(); idx++)
    {
      ost << THIRD_BODY << "[" << idx << "] = " << THIRD_BODY << "[0]";
      for (std::map<Species*,double>::const_iterator it = unit->third_bodies[idx]->components.begin();
            it != unit->third_bodies[idx]->components.end(); it++)
      {
        if (!it->first->qss_species)
          ost << " + " << it->second << "*scratch[" << it->first->code_name << "][tid]"; 
      }
      ost << ";\n";
    }
#else
    {
      ost << "if (wid < " << unit->third_bodies.size() << ")\n";
      PairDelim thb_pair(ost);
      ost << REAL << " ctot = 0.0;\n";
      for (unsigned idx = 0; idx < unit->threads_per_point; idx++)
      {
        ost << "ctot += scratch[QSS_OFFSET+" << idx << "][tid];\n";
      }
      for (unsigned idx = 0; idx < unit->third_bodies.size(); idx++)
      {
        if (idx == 0)
        {
          ost << "if (wid == 0)\n";
          ost << "  scratch[THB_OFFSET][tid] = ctot;\n";
        }
        else
        {
          ost << "else if (wid == " << idx << ")\n";
          ost << "  scratch[THB_OFFSET+" << idx << "][tid] = ctot";
          for (std::map<Species*,double>::const_iterator it = unit->third_bodies[idx]->components.begin();
                it != unit->third_bodies[idx]->components.end(); it++)
          {
            if (!it->first->qss_species)
              ost << " + " << it->second << "*scratch[" << it->first->code_name << "][tid]"; 
          }
          ost << ";\n";
        }
      }
    }
#endif
    emit_barrier(ost, 0, unit->threads_per_point, true);
  } 
  assert(!target_warps.empty());
  unsigned total_gibbs = 0;
  unsigned total_troe = 0;
  unsigned total_reverse = 0;
  //ost << REAL << " rr_f[" << target_warps[0]->reactions.size() << "];\n";
  //ost << REAL << " rr_r[" << target_warps[0]->reactions.size() << "];\n";
  // Emit all the reactions needed for qssa
  emit_last_reaction_range(ost, target_warps, non_qss_species, 0, last_qss_idx, 0/*pass*/, 
                            total_gibbs, total_troe, total_reverse);

  // Do the QSSA computation once we have all the reactions ready for it 
#if 1
  {
    ost << "// QSSA computation\n"; 
    PairDelim qssa_pair(ost);
#if 0
    std::vector<std::vector<QSSValue*> > accum_values(target_warps.size());
    unsigned max_accum_values;
    // Do the partitioning of the QSSA values into specific threads
    {
      unsigned next_warp = 0;
      for (std::vector<ConnectedComponent*>::const_iterator cc = connected_components.begin();
            cc != connected_components.end(); cc++)
      {
        for (std::map<int,QSS*>::const_iterator it = (*cc)->species.begin();
              it != (*cc)->species.end(); it++)
        {
          it->second->partition_values(accum_values, next_warp); 
        }
      }
      max_accum_values = 0;
      for (unsigned idx = 0; idx < accum_values.size(); idx++)
      {
        if (accum_values[idx].size() > max_accum_values)
          max_accum_values = accum_values[idx].size();
      }
      ost << REAL << " accum_values[" << max_accum_values << "];\n";
      for (unsigned idx = 0; idx < max_accum_values; idx++)
        ost << "accum_values[" << idx << "] = 0.0;\n";
    }
#else
    ost << REAL << " avars[" << max_values << "];\n";
    for (int idx = 0; idx < max_values; idx++)
      ost << "avars[" << idx << "] = 0.0;\n";
    ost << REAL << " denom[" << max_denoms << "];\n";
    for (int idx = 0; idx < max_denoms; idx++)
      ost << "denom[" << idx << "] = 0.0;\n";
#endif
    // Figure out how many passes we need to do
    //unsigned total_qssa_reactions = (last_qss_idx+1)*unit->threads_per_point;
    unsigned total_taken_values = TOTAL_SHARED_POINTERS + non_qss_species + needed_troe_reactions.size() + unit->third_bodies.size();
    if (gibbs_species.size() < gibbs_reactions.size())
      total_taken_values += gibbs_species.size();
    else
      total_taken_values += gibbs_reactions.size();
    unsigned reactions_per_pass = (192-(total_taken_values))/2/*forward and backward values*/;
    assert(reactions_per_pass > 0);
    unsigned reactions_per_thread_per_pass = reactions_per_pass/unit->threads_per_point;
    unsigned needed_passes = (last_qss_idx+1+(reactions_per_thread_per_pass-1))/reactions_per_thread_per_pass;
    unsigned reverse_offset = (reactions_per_thread_per_pass*unit->threads_per_point);
    for (unsigned pass_idx = 0; pass_idx < needed_passes; pass_idx++)
    {
      // Write our values into shared memory 
      for (unsigned reac_idx = 0; reac_idx < reactions_per_thread_per_pass; reac_idx++)
      {
        unsigned stage_idx = pass_idx*reactions_per_thread_per_pass + reac_idx;
        if (stage_idx > last_qss_idx)
          continue;
        ost << "scratch[QSS_OFFSET+wid+" << (reac_idx*unit->threads_per_point) << "][tid] = rr_f_" << stage_idx << ";\n";
        //rr_f[" << stage_idx << "];\n";
        ost << "scratch[QSS_OFFSET+wid+" << (reverse_offset+reac_idx*unit->threads_per_point) << "][tid] = rr_r_" << stage_idx << ";\n";
        //rr_r[" << stage_idx << "];\n";
      }
      // Wait until everyone is done writing
      emit_barrier(ost, 0, unit->threads_per_point, true); 
#if 1
      for (unsigned qidx = 0; qidx < qss_partition.size(); qidx++)
      {
        if (qidx == 0)
          ost << "if ((wid == " << qidx << "))\n";
        else
          ost << "else if ((wid == " << qidx << "))\n";
        PairDelim qidx_pair(ost);
        unsigned num_loads = 0;
        for (unsigned widx = 0; widx < target_warps.size(); widx++)
        {
          for (unsigned reac_off = 0; reac_off < reactions_per_thread_per_pass; reac_off++)
          {
            unsigned stage_idx = pass_idx*reactions_per_thread_per_pass + reac_off;
            if (stage_idx > last_qss_idx)
              continue;
            int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx, 0);
            if (reac_idx == -1)
              continue;
            Reaction *reac = target_warps[widx]->get_reaction(reac_idx, 0);
            assert(reac != NULL);
            int avars_off = 0;
            int denom_off = 0;
            for (std::map<QSS*,int>::const_iterator it = qss_partition[qidx].begin();
                  it != qss_partition[qidx].end(); it++)
            {
              num_loads = it->first->emit_last_scaled_updates(ost, avars_off, denom_off, (widx+reac_off*unit->threads_per_point), 
                                                  reac_idx, reac, reverse_offset, num_loads);
              avars_off += it->second;
              denom_off++;
            }
          }
        }
      }
#else
      // Now do the reads
      for (unsigned widx = 0; widx < target_warps.size(); widx++)
      {
        if (widx == 0)
          ost << "if (wid == 0)\n";
        else
          ost << "else if (wid == " << widx << ")\n";
        PairDelim warp_pair(ost);
        // First emit the declarations for what we're about to compute
        assert(widx < accum_values.size());
        std::vector<QSSValue*> &values = accum_values[widx];
        assert(!values.empty());
        // For each reaction do the load and the update
        for (unsigned other_widx = 0; other_widx < target_warps.size(); other_widx++)
        {
          for (unsigned reac_off = 0; reac_off < reactions_per_thread_per_pass; reac_off++)
          {
            unsigned stage_idx = pass_idx*reactions_per_thread_per_pass + reac_off;
            if (stage_idx > last_qss_idx)
              continue;
            int reac_idx = target_warps[other_widx]->get_reaction_idx(stage_idx, 0);
            assert(reac_idx != -1);
            Reaction *reac = target_warps[other_widx]->get_reaction(reac_idx, 0);
            assert(reac != NULL);
            bool forward_loaded = false;
            bool backward_loaded = false;
            for (unsigned idx = 0; idx < values.size(); idx++)
            {
              values[idx]->emit_accum_updates(ost, widx, reac_idx, forward_loaded, backward_loaded, (widx==other_widx), 
                                    (reac_off*unit->threads_per_point+other_widx), stage_idx, reverse_offset);
            }
          }
        }
      }
#endif
      // Barrier before going on to make sure all the reads are finished
      emit_barrier(ost, 0, unit->threads_per_point, true);
    }
#if 0
    // Write our values into shared memory
    for (unsigned idx = 0; idx < max_accum_values; idx++)
    {
      ost << "scratch[NUM_SPECIES+wid+" << (idx*unit->threads_per_point) << "][tid] = accum_values[" << idx << "];\n";
    }
    emit_barrier(ost, 0, unit->threads_per_point, true);
#endif
    
    // Now figure out which our connected components are complex or simple
    std::vector<ConnectedComponent*> simple_components;
    std::vector<ConnectedComponent*> complex_components;
    for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
          it != connected_components.end(); it++)
    {
      if ((*it)->is_simple())
        simple_components.push_back(*it);
      else
        complex_components.push_back(*it);
    }
    // For each of the warps in the qss partition
    std::map<std::pair<int,int>,int/*location*/> amap;
    unsigned total_simple_emitted = 0;
#if 1
    for (unsigned qidx = 0; qidx < qss_partition.size(); qidx++)
    {
      if (qidx == 0)
        ost << "if ((wid == " << qidx << "))\n";
      else
        ost << "else if ((wid == " << qidx << "))\n";
      PairDelim qidx_pair(ost);
      const std::map<QSS*,int> &qss_map = qss_partition[qidx];
      int avars_off = 0;
      int denom_off = 0;
      for (std::map<QSS*,int>::const_iterator it = qss_map.begin();
            it != qss_map.end(); it++)
      {
        // Figure out whether this is a simple QSS, if so emit the
        // QSS and then the connected component
        bool emitted = false;
        for (std::vector<ConnectedComponent*>::const_iterator cc = simple_components.begin();
              cc != simple_components.end(); cc++)
        {
          if ((*cc)->has_qss(it->first))
          {
            assert(it->second == 1);
            it->first->emit_last_divide(ost, avars_off, denom_off);
            // Now emit the write into the correct xq value
            (*cc)->emit_last_simple_statements(ost, avars_off);
            emitted = true;
            total_simple_emitted++;
            break;
          }
        }
        if (!emitted)
        {
          it->first->emit_last_stores(ost, avars_off, denom_off, amap);
        }
        avars_off += it->second;
        denom_off++;
      }
    }
#else
    for (unsigned qidx = 0; qidx < qss_partition.size(); qidx++)
    {
      if (qidx == 0)
        ost << "if ((wid == " << qidx << "))\n";
      else
        ost << "else if ((wid == " << qidx << "))\n";
      PairDelim qidx_pair(ost);
      const std::map<QSS*,int> &qss_map = qss_partition[qidx];
      for (std::map<QSS*,int>::const_iterator it = qss_map.begin();
            it != qss_map.end(); it++)
      {
        it->first->emit_qssvalue_loads(ost);
        it->first->emit_qssvalue_operations(ost);
        bool emitted = false;
        for (std::vector<ConnectedComponent*>::const_iterator cc = simple_components.begin();
              cc != simple_components.end(); cc++)
        {
          if ((*cc)->has_qss(it->first))
          {
            assert(it->second == 1);
            (*cc)->emit_last_simple_qssvalues(ost);
            emitted = true;
            total_simple_emitted++;
            break;
          }
        }
        if (!emitted)
          it->first->emit_qssvalue_stores(ost, amap);
      }
    }
#endif
    assert(total_simple_emitted == simple_components.size());
    emit_barrier(ost, 0, unit->threads_per_point, true);
    // Emit the code for each of the complex connected components
    // Make the larger numbered warps do this map since they are more likeley to have 
    // fewer reactions to do
    unsigned complex_warp = unit->threads_per_point - complex_components.size();
    bool first_complex = true;
    for (unsigned idx = 0; idx < complex_components.size(); idx++)
    {
      assert(complex_warp < unit->threads_per_point);
      if (first_complex)
        ost << "if (wid == " << complex_warp << ")\n";
      else
        ost << "else if (wid == " << complex_warp << ")\n";
      PairDelim complex_pair(ost);
      ost << REAL << " den;\n";
      complex_components[idx]->emit_last_complex_statements(ost, amap);
      first_complex = false;
      complex_warp++;
    }
  }
#endif

  emit_last_reaction_range(ost, target_warps, non_qss_species, first_non_qss_idx, max_stages-1, 1/*pass*/, 
                            total_gibbs, total_troe, total_reverse);

  // Finish the QSSA computation 
  {
    // Now do a barrier so we can read out the QSSA values and update our reaction rates appropriately
    emit_barrier(ost, 0, unit->threads_per_point, true);
    // Update the QSSA valaues
    for (unsigned widx = 0; widx < unit->threads_per_point; widx++)
    {
      if (widx == 0)
        ost << "if (wid == 0)\n";
      else
        ost << "else if (wid == " << widx << ")\n";
      PairDelim qssa_pair(ost);
      std::set<int> loaded_qss_values;
      for (unsigned stage_idx = 0; stage_idx < target_warps[widx]->num_reactions(0/*pass*/); stage_idx++)
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx,0/*pass*/);
        assert(reac_idx != -1);
        for (std::vector<ConnectedComponent*>::const_iterator it = connected_components.begin();
              it != connected_components.end(); it++)
        {
          (*it)->emit_last_corrections(ost, stage_idx, reac_idx, loaded_qss_values);
        }
      }
    }
  }
  // Now do the stiffnes computation
  {
    // Issue the loads for shared diffusion values
    unsigned stiff_species_per_thread = (stif_operations.size() + (unit->threads_per_point-1))/unit->threads_per_point;
    //bool stiff_uniform = ((stif_operations.size()%unit->threads_per_point) == 0);
    ost << REAL << " stiff_diff[" << stiff_species_per_thread << "];\n";
    ost << REAL << " stiff_frac[" << stiff_species_per_thread << "];\n";
    ost << REAL << " *diff_ptr = (" << REAL << "*)__double_as_longlong(scratch[" << DIFFUSION_IDX << "][tid]);\n";
    // Put a thread fence here to prevent the compiler from hoisting these loads
    ost << "asm volatile(\"membar.cta;\" : : : \"memory\");\n";
    for (unsigned idx = 0; idx < stiff_species_per_thread; idx++)
    {
      char src_offset[128];
      sprintf(src_offset,"stiff_index_0[%d+step*step_stride][wid]*spec_stride",idx);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      //const char *diff_src = "((double*)__double_as_longlong(__hiloint2double(hi_diff,lo_diff)))\n";
      emit_cuda_load(ost,"stiff_diff",dst_offset,"diff_ptr"/*DIFFUSION_ARRAY*/,src_offset,false/*double2*/,".cs");
    }
    // Now issue the shared loads
    for (unsigned idx = 0; idx < stiff_species_per_thread; idx++)
    {
      ost << "stiff_frac[" << idx << "] = scratch[stiff_index_0[" << idx << "+step*step_stride][wid]][tid];\n";
    }
    for (unsigned widx = 0; widx < unit->threads_per_point; widx++)
    {
      unsigned stage_idx = 0;
      for (unsigned stif_idx = widx; stif_idx < stif_operations.size(); stif_idx+=unit->threads_per_point, stage_idx++)
      {
        target_warps[widx]->add_stiff_species(stage_idx, stif_idx, stif_operations[stif_idx], true);
      }
    }
    // Emit a barrier once we're done reading
    emit_barrier(ost, 0, unit->threads_per_point, true);
    // We can now update the pointer for the diffusion array
    {
      ost << "if (wid == 0)\n";
      PairDelim update_pair(ost);
      ost << "scratch[" << DIFFUSION_IDX << "][tid] = __longlong_as_double((long long int)(diff_ptr+slice_stride));\n";
    }
    // Do the send phases for each of the reactions so we can accumulate our stiff values
#if 1
    ost << REAL << " ddot[" << stiff_species_per_thread << "];\n";
    ost << REAL << " cdot[" << stiff_species_per_thread << "];\n";
    for (unsigned idx = 0; idx < stiff_species_per_thread; idx++)
    {
      ost << "ddot[" << idx << "] = 0.0;\n";
      ost << "cdot[" << idx << "] = 0.0;\n";
    }
#else
    std::vector<std::vector<StifValue*> > accum_values(target_warps.size());
    unsigned max_accum_values = 0;
    // Do the partitioning of the stif species
    {
      unsigned next_warp = 0;
      for (std::vector<Stif*>::const_iterator it = stif_operations.begin();
            it != stif_operations.end(); it++)
      {
        (*it)->partition_values(accum_values, next_warp);
      }
      for (unsigned idx = 0; idx < accum_values.size(); idx++)
      {
        if (accum_values[idx].size() > max_accum_values)
          max_accum_values = accum_values[idx].size();
      }
      ost << REAL << " accum_values[" << max_accum_values << "];\n";
      for (unsigned idx = 0; idx < max_accum_values; idx++)
        ost << "accum_values[" << idx << "] = 0.0;\n";
    }
#endif
    // Note we can now use all of the shared scratch since we've got our needed mole fractions and qss
    // values out and we have no more need of any of them
    unsigned reactions_per_pass = (192-TOTAL_SHARED_POINTERS)/2/*forward and backward rates*/;
    unsigned reactions_per_thread_per_pass = reactions_per_pass/unit->threads_per_point;
    assert(reactions_per_thread_per_pass > 0); // if this isn't true we're screwed anyway
    unsigned needed_passes = (max_stages+(reactions_per_thread_per_pass-1))/reactions_per_thread_per_pass;
    unsigned reverse_offset = (reactions_per_thread_per_pass*unit->threads_per_point);
    for (unsigned pass_idx = 0; pass_idx < needed_passes; pass_idx++)
    {
      // Write our values into shared memory
      for (unsigned reac_off = 0; reac_off < reactions_per_thread_per_pass; reac_off++)
      {
        unsigned stage_idx = pass_idx*reactions_per_thread_per_pass + reac_off;
        if (stage_idx >= max_stages)
          continue;
        ost << "scratch[wid+" << (reac_off*unit->threads_per_point) << "][tid] = rr_f_" << stage_idx << ";\n";
        ost << "scratch[wid+" << (reverse_offset+reac_off*unit->threads_per_point) << "][tid] = rr_r_" << stage_idx << ";\n";
      }
      // Wait until everything is done
      emit_barrier(ost, 0, unit->threads_per_point, true);
      // Do the reads and updates
      for (unsigned widx = 0; widx < target_warps.size(); widx++)
      {
        if (widx == 0)
          ost << "if (wid == 0)\n";
        else
          ost << "else if (wid == " << widx << ")\n";
        PairDelim stif_pair(ost);
#if 1
        std::set<int> emitted_forward;
        std::set<int> emitted_backward;
        for (unsigned other_widx = 0; other_widx < target_warps.size(); other_widx++)
        {
          for (unsigned reac_off = 0; reac_off < reactions_per_thread_per_pass; reac_off++)
          {
            unsigned stage_idx = pass_idx*reactions_per_thread_per_pass + reac_off;
            if (stage_idx >= target_warps[other_widx]->num_reactions())
              continue;
            int reac_idx = target_warps[other_widx]->get_reaction_idx(stage_idx);
            assert(reac_idx != -1);
            unsigned local_idx = 0;
            for (unsigned stif_idx = widx; stif_idx < stif_operations.size(); stif_idx+=unit->threads_per_point, local_idx++)
            {
              stif_operations[stif_idx]->emit_last_updates(ost, stif_idx, local_idx, (other_widx+reac_off*unit->threads_per_point),
                                                            reac_idx, reverse_offset, emitted_forward, emitted_backward);
            }
          }
        }
#else
        std::vector<StifValue*> &values = accum_values[widx];
        assert(!values.empty());
        for (unsigned other_widx = 0; other_widx < target_warps.size(); other_widx++)
        {
          for (unsigned reac_off = 0; reac_off < reactions_per_thread_per_pass; reac_off++)
          {
            unsigned stage_idx = pass_idx*reactions_per_thread_per_pass + reac_off;
            if (stage_idx >= target_warps[other_widx]->num_reactions())
              continue;
            int reac_idx = target_warps[other_widx]->get_reaction_idx(stage_idx);
            assert(reac_idx != -1);
            Reaction *reac = target_warps[other_widx]->get_reaction(reac_idx);
            assert(reac != NULL);
            bool forward_loaded = false;
            bool backward_loaded = false;
            for (unsigned idx = 0; idx < values.size(); idx++)
            {
              values[idx]->emit_accum_updates(ost, widx, reac_idx, forward_loaded, backward_loaded, (widx==other_widx),
                                    (reac_off*unit->threads_per_point+other_widx), stage_idx, reverse_offset);
            }
          }
        }
#endif
      }
      // Emit another barrier to indicate we're done
      emit_barrier(ost, 0, unit->threads_per_point, true);
    }
#if 0
    // Write our values into shared memory
    for (unsigned idx = 0; idx < max_accum_values; idx++)
    {
      ost << "scratch[wid+" << (idx*unit->threads_per_point) << "][tid] = accum_values[" << idx << "];\n";
    }
    emit_barrier(ost, 0, unit->threads_per_point, true);
    // Now load our values
    ost << REAL << " ddot[" << stiff_species_per_thread << "];\n";
    ost << REAL << " cdot[" << stiff_species_per_thread << "];\n";
    for (unsigned idx = 0; idx < stiff_species_per_thread; idx++)
    {
      ost << "ddot[" << idx << "] = 0.0;\n";
      ost << "cdot[" << idx << "] = 0.0;\n";
    }
    for (unsigned widx = 0; widx < unit->threads_per_point; widx++)
    {
      if (widx == 0)
        ost << "if ((wid == " << widx << "))\n";
      else
        ost << "else if ((wid == " << widx << "))\n";
      PairDelim warp_pair(ost);
      unsigned local_idx = 0;
      for (unsigned stif_idx = widx; stif_idx < stif_operations.size(); stif_idx+=unit->threads_per_point,local_idx++)
      {
        stif_operations[stif_idx]->emit_stif_value_loads(ost, local_idx);
      }
    }
    emit_barrier(ost, 0, unit->threads_per_point, true);
#endif

    // Once we've got all our cdot and ddot values, do the computations
    for (unsigned idx = 0; idx < stiff_species_per_thread; idx++)
    {
      ost << "if ((ddot[" << idx << "]*dt) > stiff_frac[" << idx << "])\n";
      {
        PairDelim if_pair(ost);
        ost << REAL << " recip_ddot = 1.0/ddot[" << idx << "];\n";
        ost << REAL << " part_sum = cdot[" << idx << "] + stiff_diff[" << idx << "]*recip_molecular_mass[stiff_index_0[" << idx
            << "+step*step_stride][wid]];\n";
        ost << REAL << " c0 = stiff_frac[" << idx << "] * part_sum * recip_ddot;\n";
        ost << "c0 = stiff_frac[" << idx << "] * (part_sum + (stiff_frac[" << idx << "] - c0) * recip_ddot);\n";
        ost << "stiff_diff[" << idx << "] = c0/stiff_frac[" << idx << "];\n";
      }
      ost << "else\n";
      {
        PairDelim else_pair(ost);
        ost << "stiff_diff[" << idx << "] = 1.0;\n";
      }
      ost << "scratch[wid+" << (idx*unit->threads_per_point) << "][tid] = stiff_diff[" << idx << "];\n";
    }
    // Barrier to make sure all the scaling factors are in shared
    emit_barrier(ost, 0, unit->threads_per_point, true);
    // Now we can scale all of our reaction rates and we're ready to do output
    for (unsigned widx = 0; widx < target_warps.size(); widx++)
    {
      if (widx == 0)
        ost << "if (wid == 0)\n";
      else
        ost << "else if (wid == " << widx << ")\n";
      PairDelim scale_pair(ost);
      std::set<int> loaded_stif_values;
      for (unsigned stif_idx = 0; stif_idx < stif_operations.size(); stif_idx++)
      {
        // go through all of our reactions and emit any updates
        for (unsigned idx = 0; idx < target_warps[widx]->num_reactions(); idx++)
        {
          int reac_idx = target_warps[widx]->get_reaction_idx(idx);
          assert(reac_idx);
          stif_operations[stif_idx]->emit_last_corrections(ost, stif_idx, idx, reac_idx, loaded_stif_values);
        }
      }
    }
  }
  // Now we need to compute the output for each of the species
  {
    // Figure our how many species each thread owns
    unsigned species_per_thread = (non_qss_species + (unit->threads_per_point-1))/unit->threads_per_point;
    bool species_uniform = ((non_qss_species%unit->threads_per_point) == 0);
    ost << REAL << " spec_out[" << species_per_thread << "];\n";
    for (unsigned idx = 0; idx < species_per_thread; idx++)
    {
      ost << "spec_out[" << idx << "] = 0.0;\n";
    }
    std::vector<Species*> output_species;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if ((*it)->qss_species)
        continue;
      output_species.push_back(*it);
    }
    unsigned available_values = 192-TOTAL_SHARED_POINTERS; // (49152/(32*8))
    while ((available_values%unit->threads_per_point) != 0)
      available_values--;
    // Figure out how many passes we need
    unsigned num_passes = (unit->all_reactions.size()+(available_values-1))/available_values;
    assert((available_values%unit->threads_per_point) == 0);
    unsigned reactions_per_thread = available_values/unit->threads_per_point;
    for (unsigned pass = 0; pass < num_passes; pass++)
    {
      emit_barrier(ost, 0, unit->threads_per_point, true);
      // Write our rates into shared
      for (unsigned idx = 0; idx < reactions_per_thread; idx++)
      {
        unsigned stage_idx = pass*reactions_per_thread+idx;
        int largest_warp = -1;
        for (unsigned widx = 0; widx < unit->threads_per_point; widx++)
        {
          int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
          if (reac_idx != -1)
            largest_warp = widx;
        }
        if (largest_warp == -1)
          continue;
        //if (largest_warp != int(unit->threads_per_point-1))
        //  ost << "if (wid < " << (largest_warp+1) << ")\n";
        ost << "scratch[wid+" << (idx*unit->threads_per_point) << "][tid] = "
            << "rr_f_" << (pass*reactions_per_thread+idx) << " - "
            << "rr_r_" << (pass*reactions_per_thread+idx) << ";\n";
            //<< "rr_f[" << (pass*reactions_per_thread+idx) << "] - "
            //<< "rr_r[" << (pass*reactions_per_thread+idx) << "];\n";
      }
      emit_barrier(ost, 0, unit->threads_per_point, true);
      // Now do the reads for each of the species and update the output values
      for (unsigned widx = 0; widx < unit->threads_per_point; widx++)
      {
        if (widx == 0)
          ost << "if (wid == 0)\n";
        else
          ost << "else if (wid == " << widx << ")\n";
        PairDelim out_pair(ost);
        // go over all the reactions in shared
        for (unsigned shared_idx = 0; shared_idx < available_values; shared_idx++)
        {
          unsigned warp_idx = shared_idx%unit->threads_per_point;
          unsigned stage_idx = pass*reactions_per_thread + (shared_idx/unit->threads_per_point);
          int reac_idx = target_warps[warp_idx]->get_reaction_idx(stage_idx);
          if (reac_idx != -1)
          {
            Reaction *reac = target_warps[warp_idx]->get_reaction(reac_idx);
            assert(reac != NULL);
            bool has_update = false;
            for (unsigned spec_idx = widx; spec_idx < output_species.size(); spec_idx+=unit->threads_per_point)
            {
              Species *spec = output_species[spec_idx];
              if (spec->reaction_contributions.find(reac) != spec->reaction_contributions.end())
              {
                has_update = true;
                break;
              }
            }
            if (has_update)
            {
              ost << REAL << " ropl_" << reac->get_global_idx() << " = scratch[" << shared_idx << "][tid];\n";
              unsigned local_idx = 0;
              for (unsigned spec_idx = widx; spec_idx < output_species.size(); spec_idx+=unit->threads_per_point,local_idx++)
              {
                Species *spec = output_species[spec_idx];
                std::map<Reaction*,int>::const_iterator finder = spec->reaction_contributions.find(reac);
                if (finder != spec->reaction_contributions.end())
                {
                  if (finder->second == 1)
                    ost << "spec_out[" << local_idx << "] += ropl_" << reac->get_global_idx() << ";\n";
                  else if (finder->second == -1)
                    ost << "spec_out[" << local_idx << "] -= ropl_" << reac->get_global_idx() << ";\n";
                  else
                    ost << "spec_out[" << local_idx << "] = __fma_rn(" << double(finder->second) << ",ropl_" << reac->get_global_idx()
                        << ",spec_out[" << local_idx << "]);\n";
                }
              }
            }
          }
        }
      }
    }
    ost << REAL << " *wdot_ptr = (" << REAL << "*)__double_as_longlong(scratch[" << WDOT_IDX << "][tid]);\n";
    // Now that we've got all our values write them out to memory
    for (unsigned idx = 0; idx < species_per_thread; idx++)
    {
      if (!species_uniform && (idx == (species_per_thread-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << non_qss_species << ")\n";
      PairDelim out_pair(ost);
      if (!unit->no_nondim)
        ost << "spec_out[" << idx << "] *= (0.001 * molecular_mass[wid+" << (idx*unit->threads_per_point) 
            << "+step*step_stride]);\n";
      //const char *wdot_dst = "((double*)__double_as_longlong(__hiloint2double(hi_wdot,lo_wdot)))";
      ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << "wdot_ptr"/*WDOT*/ << "+"
          << "(wid+" << (idx*unit->threads_per_point) << ")*spec_stride+step*step_stride), "
          << "\"d\"(spec_out[" << idx << "]) : \"memory\");\n";
    }
    ost << "if (wid == 0)\n";
    {
      PairDelim if_pair(ost);
      emit_barrier(ost, 1, unit->threads_per_point, true);
      ost << "scratch[" << WDOT_IDX << "][tid] = __longlong_as_double((long long int)(wdot_ptr+slice_stride));\n";
    }
    ost << "else\n";
    {
      PairDelim else_pair(ost);
      emit_barrier(ost, 1, unit->threads_per_point, false);
    }
  }
}

void ChemistryUnit::emit_last_reaction_range(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species,
                                             int start_idx, int stop_idx, int phase, unsigned &total_gibbs, unsigned &total_troe,
                                             unsigned &total_reverse)
{
  const int pass = 0;
  bool reached_full = false;
  for (unsigned stage_idx = start_idx; int(stage_idx) <= stop_idx; stage_idx++)
  {
    std::vector<Reaction*> weird_reactions;
    std::vector<Reaction*> normal_reactions;
    ost << "// Reaction stage " << stage_idx << "\n";
    {
      // Check to see if all the warps do this stage
      int last_warp_idx = -1;
      int last_warp_mask = 0;
      bool needs_mask = false;
      for (unsigned widx = 0; widx < target_warps.size(); widx++)
      {
        if (target_warps[widx]->has_index(stage_idx, phase))
        {
          //assert(int(widx) == (last_warp_idx+1)); // should be contiguous
          last_warp_mask |= (1 << widx);
          last_warp_idx = widx;
        }
        else
          needs_mask = true;
      }
      assert(last_warp_idx != -1); // there should be at least one warp doing something
      if (!needs_mask)
        reached_full = true;
      // Chain these so the compiler's register allocation algorithm doesn't do something dumb!
      // They're just going to get overwritten anyway
      if (stage_idx == 0)
        ost << REAL << " rr_f_" << stage_idx << " = 0.0, rr_r_" << stage_idx << " = 0.0;\n";
      else
      {
        if ((phase == 0) || reached_full)
        {
          ost << REAL << " rr_f_" << stage_idx << " = rr_f_" << (stage_idx-1) 
              << ", rr_r_" << stage_idx << " = rr_r_" << (stage_idx-1) << ";\n";
        }
      }
      // Emit the condition only if necessary
      if (needs_mask)
        ost << "if ((1 << wid) & " << last_warp_mask << ")\n";
      //if (last_warp_idx != int(target_warps.size()-1))
      //  ost << "if (wid < " << (last_warp_idx+1) << ")\n";
      PairDelim stage_pair(ost);
      char rr_f[128];
      sprintf(rr_f,"rr_f_%d", stage_idx);
      char rr_r[128];
      sprintf(rr_r,"rr_r_%d", stage_idx);
      //ost << REAL << " rr_f_" << stage_idx << ", rr_r_" << stage_idx << ";\n";
      int weird_warp_mask = 0;
      std::vector<Reaction*> low_reactions;
      std::vector<Reaction*> irreversible_reactions;
      unsigned low_mask = 0;
      std::vector<Warp*> low_warps;
      std::vector<Reaction*> troe_reactions;
      std::vector<Reaction*> lindermann_reactions;
      unsigned troe_mask = 0;
      std::vector<Warp*> troe_warps;
      std::map<int,Reaction*> needed_values;
      std::vector<Warp*> gibbs_warps;
      std::vector<Reaction*> needed_gibbs;
      unsigned gibbs_mask = 0;
      // Do the arrhenius part for everyone
      bool first_arrhenius = true;
      for (unsigned widx = 0; widx < target_warps.size(); widx++)
      {
        if (target_warps[widx]->has_index(stage_idx, phase))
        {
          int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx, phase);
          Reaction *reac = target_warps[widx]->get_reaction(reac_idx, phase);
          assert(reac != NULL);
          if (is_weird_reaction(reac))
          {
            weird_reactions.push_back(reac);
            assert(widx < 32); // one hot encoding only works up to 32 bits
            weird_warp_mask |= (1 << widx);
            // Figure out what kind of arrhenius to do
            if (reac->low.enabled)
            {
              low_reactions.push_back(reac);
              assert(widx < 32); // 1-hot encoding doesn't work past 32 bits
              low_mask |= (1 << widx); 
              low_warps.push_back(target_warps[widx]);
              needed_values[widx] = reac;
              gibbs_mask |= (1 << widx);
              gibbs_warps.push_back(target_warps[widx]);
              needed_gibbs.push_back(reac);
              if (reac->troe.num > 0)
              {
                troe_reactions.push_back(reac);
                troe_mask |= (1 << widx);
                troe_warps.push_back(target_warps[widx]);
              }
              else if (reac->sri.num == 0)
              {
                lindermann_reactions.push_back(reac);
              }
              else
                assert(false); // not handling sri right now
              if (!unit->no_nondim)
              {
                emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_f,
                        (reac->low.a*REACTION_RATE_REF), reac->low.beta, reac->low.e, -1, pass);
                emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_r,
                        (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
              }
              else
              {
                emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_f,
                        reac->low.a, reac->low.beta, reac->low.e, -1, pass);
                emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_r,
                        reac->a, reac->beta, reac->e, -1, pass); 
              }
              assert(!reac->rev.enabled); // hoping we never have to do a reverse reaction in this case 
              assert(reac->reversible);
            }
            else if (!reac->lt.enabled)
            {
              assert(!reac->rlt.enabled);
              if (reac->reversible)
              {
                gibbs_mask |= (1 << widx);
                gibbs_warps.push_back(target_warps[widx]);
                needed_gibbs.push_back(reac);
              }
              irreversible_reactions.push_back(reac);
              if (!unit->no_nondim)
              {
                emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_f,
                        (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
                if (reac->rev.enabled)
                {
                  emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_r,
                          (reac->rev.a*REACTION_RATE_REF), reac->rev.beta, reac->rev.e, -1, pass);
                }
                else
                {
                  // We're just going to write zero anyway, so what does it matter
                  // Duplicate the computation so we don't diverge, but with coefficients that won't do anything weird
                  emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_r,
                          (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
                }
              }
              else
              {
                emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_f,
                        reac->a, reac->beta, reac->e, -1, pass);
                if (reac->rev.enabled)
                {
                  emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_r,
                          reac->rev.a, reac->rev.beta, reac->rev.e, -1, pass);
                }
                else
                {
                  emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_r,
                        reac->a, reac->beta, reac->e, -1, pass);
                }
              }
            }
            else
            {
              assert(false); // Not currently handling landau-teller right now since it doesn't use arrhenius
            }
          }
          else
          {
            normal_reactions.push_back(reac);
            // Normal arrhenius and we're done
            if (!unit->no_nondim)
            {
              emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_f,
                        (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
              emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_r,
                        (reac->rev.a*REACTION_RATE_REF), reac->rev.beta, reac->rev.e, -1, pass);
            }
            else
            {
              emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_f,
                          reac->a, reac->beta, reac->e, -1, pass);
              emit_pipe_arrhenius(ost, target_warps[widx], first_arrhenius, false/*weird*/, stage_idx, rr_r,
                          reac->rev.a, reac->rev.beta, reac->rev.e, -1, pass);
            }
          }
          first_arrhenius = false;
        }
      }
      if (!weird_reactions.empty())
      {
        if (!normal_reactions.empty())
        {
          assert(weird_warp_mask > 0);
          ost << "if (((1 << wid) & " << weird_warp_mask << "))\n";
        }
        PairDelim weird_pair(ost);
        // Now emit the code for the weird reactions
        // Emit the code for the different cases of weird reactions
        if (!low_reactions.empty())
        {
          if (!irreversible_reactions.empty())
            ost << "if ((((1 << wid) & " << low_mask << ")))\n";
          PairDelim low_pair(ost);
          ost << REAL << " rr_k0 = " << rr_f << ";\n";
          ost << REAL << " rr_kinf = " << rr_r << ";\n";
          ost << REAL << " pr = rr_k0 / rr_kinf;\n";
          // Now we need to wait until we get our information from the mole fraction warp
          // about our scaling value
          {
            bool first_needed = true;
            for (std::map<int,Reaction*>::const_iterator it = needed_values.begin();
                  it != needed_values.end(); it++)
            {
              Reaction *reac = it->second;
              if (first_needed)
                ost << "if (wid == " << it->first << ")\n";
              else
                ost << "else if (wid == " << it->first << ")\n";
              ost << "pr *= ";
              if ((reac->pressure_species != NULL) && (reac->pressure_species != unit->find_species("M")))
                ost << "scratch[" << reac->pressure_species->code_name << "][tid]";
              else
                ost << "scratch[THB_OFFSET+" << reac->thb->idx << "][tid]";
                //ost << THIRD_BODY << "[" << reac->thb->idx << "]";
              ost << ";\n";
              first_needed = false;
            }
          }
          if (!troe_reactions.empty())
          {
            if (!lindermann_reactions.empty())
              ost << "if ((((1 << wid) & " << troe_mask << ")))\n";
            PairDelim troe_pair(ost);
            // Do something to fool the compiler here
            assert(non_qss_species >= needed_troe_reactions.size());
#if 0
            unsigned upper_bound = next_largest_power(2*non_qss_species,2);
            ost << INT << " troe_idx = (troe_index_" << pass << "[(" << stage_idx 
                << "+step*" << upper_bound << ")%" << upper_bound 
                << "][wid] + step*" << upper_bound << ")%" << upper_bound << ";\n";
#else
            ost << INT << " troe_idx = troe_index_" << pass << "[" << total_troe << "+step*step_stride][wid];\n";
#endif
            ost << REAL << " fcent = scratch[TROE_OFFSET+troe_idx][tid];\n";
            // add the troe constants
            assert(troe_reactions.size() == troe_warps.size());
            for (unsigned troe_idx = 0; troe_idx < troe_warps.size(); troe_idx++)
            {
              Warp *troe = troe_warps[troe_idx];
              Reaction *reac = troe_reactions[troe_idx];
              int tr_idx = -1;
              for (unsigned tidx = 0; tidx < needed_troe_reactions.size(); tidx++)
              {
                if (needed_troe_reactions[tidx] == reac)
                {
                  tr_idx = tidx;
                  break;
                }
              }
              if (tr_idx == -1)
              {
                tr_idx = needed_troe_reactions.size();
                needed_troe_reactions.push_back(reac);
              }
              troe->add_troe_reaction(total_troe, tr_idx, true);
            }
            total_troe++;
            ost << REAL << " flogpr = log10(pr) - 0.4 - 0.67 * fcent;\n";
            ost << REAL << " fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;\n";
            ost << REAL << " fquan = flogpr / fdenom;\n";
            ost << "fquan = fcent / (1.0 + fquan * fquan);\n";
            ost << rr_f << " = rr_kinf * pr/(1.0 + pr) * exp(fquan * DLn10);\n";
          }
          if (!lindermann_reactions.empty())
          {
            if (!troe_reactions.empty())
              ost << "else\n";
            PairDelim linderman_pair(ost);
            ost << rr_f << " = rr_kinf * pr/(1.0 + pr);\n";
          }
        }
        if (!irreversible_reactions.empty())
        {
          if (!low_reactions.empty())
            ost << "else\n";
          PairDelim irrev_pair(ost);
          ost << rr_r << " = 0.0; /* irreversible reaction */\n";
          //ost << "pipe[wid][0][tid] = rr_f;\n";
          //ost << "pipe[wid][1][tid] = rr_r*0.0; /* irreversible reaction */\n";
        }
      }
      // Finally handle any duplicate reactions
      {
        bool first_duplicate = true;
        for (unsigned widx = 0; widx < target_warps.size(); widx++)
        {
          if (target_warps[widx]->has_index(stage_idx, phase))
          {
            int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx, phase);
            Reaction *reac = target_warps[widx]->get_reaction(reac_idx, phase);
            assert(reac != NULL); 
            if (reac->duplicate.enabled)
            {
              assert(reac->duplicate.owner); // this better be the owner
              if (first_duplicate)
                ost << "if (wid == " << widx << ")\n";
              else
                ost << "else if (wid == " << widx << ")\n";
              PairDelim dup_pair(ost);
              ost << "// Handling duplicates for reaction " << reac->get_global_idx() << "\n";
              ost << REAL << " rr_f_prime;\n";
              for (std::list<Reaction*>::const_iterator it = reac->duplicate.ptr.duplicates->begin();
                    it != reac->duplicate.ptr.duplicates->end(); it++)
              {
                Reaction *dup_reac = (*it);
                // For right now we assume that duplicate reactions aren't weird
                assert(!dup_reac->low.enabled && !dup_reac->lt.enabled);
                if (!unit->no_nondim)
                {
                  emit_pipe_arrhenius(ost, target_warps[widx], true, true/*weird*/, stage_idx, "rr_f_prime",
                            (reac->a*REACTION_RATE_REF), reac->beta, reac->e, -1, pass);
                }
                else
                {
                  emit_pipe_arrhenius(ost, target_warps[widx], true, true/*weird*/, stage_idx, "rr_f_prime",
                              reac->a, reac->beta, reac->e, -1, pass);
                }
                ost << rr_f << " += rr_f_prime;\n";
              }
              first_duplicate = false;
            }
          }
        }
      }
      // Handle any gibbs computation
      if (!needed_gibbs.empty())
      {
          // Figure out the maximum number of gibbs species we need to compute for any of the troe warps
          unsigned max_gibbs = 0;
          for (std::vector<Reaction*>::const_iterator it = needed_gibbs.begin();
                it != needed_gibbs.end(); it++)
          {
            if ((*it)->stoich.size() > max_gibbs)
              max_gibbs = (*it)->stoich.size();
          }
          // shouldn't be zero 
          assert(max_gibbs > 0);
          // emit the code to compute the gibbs values that we need
          ost << "// Gibbs computation\n";
          if (gibbs_warps.size() < target_warps.size())
            ost << "if ((1 << wid) & " << gibbs_mask << ")\n";
          //unsigned upper_bound = next_largest_power(2*non_qss_species,2);
          if (gibbs_species.size() < gibbs_reactions.size())
          {
            unsigned old_total_gibbs = total_gibbs;
            PairDelim gibbs_pair(ost);
            ost << REAL << " xik = 0.0;\n";
            ost << INT << " gibbs_idx;\n";
            for (unsigned idx = 0; idx < max_gibbs; idx++)
            {
              // Do something to fool the compiler here
#if 0
              ost << "gibbs_idx = (gibbs_index_" << pass << "[(" << total_gibbs 
                  << "+step*" << upper_bound << ")%" << upper_bound << "][wid]"
                  << "+step*" << upper_bound << ")%" << upper_bound << ";\n";
#else
              ost << "gibbs_idx = gibbs_index_" << pass << "[" << total_gibbs
                  << "+step*step_stride][wid];\n";
#endif
              ost << REAL << " gibbs_" << idx << " = scratch[GIBBS_OFFSET+gibbs_idx][tid];\n";
#if 0
              ost << "xik = __fma_rn(gibbs_scale_" << pass << "[(" << total_gibbs 
                  << "+step*" << upper_bound << ")%" << upper_bound << "][wid],gibbs_" << idx << ",xik);\n";
#else
              ost << "xik = __fma_rn(gibbs_scale_" << pass << "[" << total_gibbs
                  << "+step*step_stride][wid],gibbs_" << idx << ",xik);\n";
#endif
              total_gibbs++;
            }
#if 0
            ost << rr_r << " = exp(xik*otc - reverse_pass_" << pass << "[(" << stage_idx  
                << "+step*" << upper_bound << ")%" << upper_bound << "][wid]*";
#else
            ost << rr_r << " = exp(xik*otc - reverse_pass_" << pass << "[" << total_reverse 
                << "+step*step_stride][wid]*";
#endif
            if (!unit->no_nondim)
              ost << "(vlntemp + " << TEMPERATURE_REF << ")";
            else
              ost << "vlntemp";
            ost << ");\n";
            // Now update low warps with the information that they need to emit the constants
            assert(low_reactions.size() == low_warps.size());
            assert(needed_gibbs.size() == gibbs_warps.size());
            for (unsigned idx = 0; idx < gibbs_warps.size(); idx++)
            {
              Warp *warp = gibbs_warps[idx];
              Reaction *reac = needed_gibbs[idx];
              unsigned added_gibbs = 0;
              int nutot = 0;
              for (SpeciesCoeffMap::const_iterator it = reac->stoich.begin();
                    it != reac->stoich.end(); it++)
              {
                int spec_idx = -1;
                for (unsigned gidx = 0; gidx < gibbs_species.size(); gidx++)
                {
                  if (it->first == gibbs_species[gidx])
                  {
                    spec_idx = gidx;
                    break;
                  }
                }
                if (spec_idx == -1)
                {
                  spec_idx = gibbs_species.size();
                  gibbs_species.push_back(it->first);
                }
                warp->add_gibbs_species(old_total_gibbs+added_gibbs,spec_idx,double(it->second),true);
                added_gibbs++;
                nutot += it->second;
              }
              while (added_gibbs < max_gibbs)
              {
                warp->add_gibbs_species(old_total_gibbs+added_gibbs,0,0.0,true);
                added_gibbs++;
              }
              // Also update the reverse value
              warp->add_reverse_constant(total_reverse, double(nutot),true);
            }
            total_reverse++;
          }
          else
          {
            PairDelim gibbs_pair(ost);
            ost << INT << " gibbs_idx = gibbs_index_" << pass << "[" << total_gibbs
                << "+step*step_stride][wid];\n";
            ost << rr_r << " = scratch[GIBBS_OFFSET+gibbs_idx][tid];\n";
            assert(needed_gibbs.size() == gibbs_warps.size());
            for (unsigned widx = 0; widx < gibbs_warps.size(); widx++)
            {
              Warp *warp = gibbs_warps[widx];
              Reaction *reac = needed_gibbs[widx];
              int reac_idx = -1;
              for (unsigned idx = 0; idx < gibbs_reactions.size(); idx++)
              {
                if (reac == gibbs_reactions[idx])
                {
                  reac_idx = idx;
                  break;
                }
              }
              assert(reac_idx != -1);
              warp->add_gibbs_species(total_gibbs,reac_idx,0.0,true);
            }
            total_gibbs++;
          }
          // Now that we have rr_f, we can compute the final part of rr_r
          ost << rr_r << " *= " << rr_f << ";\n";
          // Write our results out into shared memory, no need to wait since we already know the buffer is ready
          //ost << "pipe[wid][0][tid] = rr_f;\n";
          //ost << "pipe[wid][1][tid] = rr_r;\n";
      }
#if 0
      // Now do all the normal reactions
      if (!normal_reactions.empty())
      {
        if (!weird_reactions.empty())
          ost << "else\n";
        PairDelim normal_pair(ost);
        // For the normal case we can just do the normal thing and write our values into
        // shared memory ass soon as the buffer is ready
        // Write our result into the pipe
        ost << "pipe[wid][0][tid] = rr_f;\n";
        ost << "pipe[wid][1][tid] = rr_r;\n";
      }
#endif
      // Now do the scaling for each of our reactions
#if 0
      for (unsigned widx = 0; widx < target_warps.size(); widx++)
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx);
        if (reac_idx == -1)
          continue;
        if (widx == 0)
          ost << "if (wid == 0)\n";
        else
          ost << "else if (wid == " << widx << ")\n";
        PairDelim scale_pair(ost);
        Reaction *reac = target_warps[widx]->get_reaction(reac_idx);
        assert(reac != NULL);
        for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
              it != reac->forward.end(); it++)
        {
          if (!it->first->qss_species)
          {
            for (int j = 0; j < it->second; j++)
              ost << rr_f << " *= scratch[" << it->first->code_name << "][tid];\n";
          }
        }
        if (reac->reversible)
        {
          for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                it != reac->backward.end(); it++)
          {
            if (!it->first->qss_species)
            {
              for (int j = 0; j < it->second; j++)
                ost << rr_r << " *= scratch[" << it->first->code_name << "][tid];\n";
            }
          }
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          ost << rr_f << " *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
          ost << rr_r << " *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
        }
      }
#endif
      // Now write our results and mark that we're done
      //ost << "rr_f[" << stage_idx << "] = " << rr_f << ";\n";
      //ost << "rr_r[" << stage_idx << "] = " << rr_r << ";\n";
    }
  }
  // Now emit the scaling factors for each of the reaction rates
  for (unsigned widx = 0; widx < target_warps.size(); widx++)
  {
    if (widx == 0)
      ost << "if ((wid == 0))\n";
    else
      ost << "else if ((wid == " << widx << "))\n";
    PairDelim scale_pair(ost);
    std::set<Species*> emitted_species;
    std::set<int> emitted_thb;
    for (unsigned stage_idx = start_idx; int(stage_idx) <= stop_idx; stage_idx++)
    {
      if (target_warps[widx]->has_index(stage_idx, phase))
      {
        int reac_idx = target_warps[widx]->get_reaction_idx(stage_idx, phase);
        Reaction *reac = target_warps[widx]->get_reaction(reac_idx, phase);
        assert(reac != NULL);
        for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
              it != reac->forward.end(); it++)
        {
          if (!it->first->qss_species)
          {
            // See if we need to emit it
            if (emitted_species.find(it->first) == emitted_species.end())
            {
              ost << REAL << " " << MOLE_FRAC << "_" << it->first->code_name << " = scratch[" << it->first->code_name << "][tid];\n";
              emitted_species.insert(it->first);
            }
            for (int j = 0; j < it->second; j++)
              ost << "rr_f_" << stage_idx << " *= " << MOLE_FRAC << "_" << it->first->code_name << ";\n";
          }
        }
        if (!reac->reversible)
        {
          for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
                it != reac->backward.end(); it++)
          {
            if (!it->first->qss_species)
            {
              // See if we need to emit it
              if (emitted_species.find(it->first) == emitted_species.end())
              {
                ost << REAL << " " << MOLE_FRAC << "_" << it->first->code_name << " = scratch[" << it->first->code_name << "][tid];\n";
                emitted_species.insert(it->first);
              }
              for (int j = 0; j < it->second; j++)
                ost << "rr_r_" << stage_idx << " *= " << MOLE_FRAC << "_" << it->first->code_name << ";\n";  
            }
          }
        }
        if ((reac->thb != NULL) && !reac->pressure_dep)
        {
          //ost << "rr_f_" << stage_idx << " *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
          //ost << "rr_r_" << stage_idx << " *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
          if (emitted_thb.find(reac->thb->idx) == emitted_thb.end())
          {
            ost << REAL << " " << THIRD_BODY << "_" << reac->thb->idx << " = scratch[THB_OFFSET+" << reac->thb->idx << "][tid];\n";
            emitted_thb.insert(reac->thb->idx);
          }
          ost << "rr_f_" << stage_idx << " *= " << THIRD_BODY << "_" << reac->thb->idx << ";\n";
          ost << "rr_r_" << stage_idx << " *= " << THIRD_BODY << "_" << reac->thb->idx << ";\n";
        }
      }
    }
  }
}

void ChemistryUnit::emit_gpu_experiment(CodeOutStream &ost, int num_species, const char *expr_name, bool striding_z, bool multi_pass)
{
  ost << "#include <cstdio>\n";
  ost << "#include <cassert>\n";
  ost << "#include \"cuda.h\"\n";
  ost << "#include \"cuda_runtime.h\"\n";

  ost << "#define CUDA_SAFE_CALL(expr)  \\\n";
  ost << "  { \\\n";
  ost << "    cudaError_t err = (expr); \\\n";
  ost << "    if (err != cudaSuccess) \\\n";
  ost << "    { \\\n";
  ost << "      printf(\"Cuda error %s\\n\", cudaGetErrorString(err)); \\\n";
  ost << "      exit(1); \\\n";
  ost << "    } \\\n";
  ost << "  }\n";

  ost << "__host__\n";
  ost << "float run_" << expr_name << "(int num_steps, int nx, int ny, int nz)\n";
  PairDelim pair(ost);
  ost << "const " << REAL << " dt = 5.0e-9;\n";
  int num_input = 2*num_species/*mass fracs + diffusion*/ + 2/*temperature + pressure*/; 
  int num_output = num_species;
  ost << "// Allocate memory\n";
  ost << REAL << " *input_d;\n";
  // -1 for ignoring the third body species but +1 for temperature
  ost << "const int species_stride = nx * ny * nz;\n";
  ost << "printf(\"Allocating %ld MB for input\\n\", species_stride*" << num_input << "*sizeof(" << REAL << ") >> 20);\n";
  ost << "CUDA_SAFE_CALL(cudaMalloc((void**)&input_d, species_stride*" << num_input << "*sizeof(" << REAL << ")));\n";
  // Initialize the data with somewhat meaningful values
  ost << "// Initialize input data\n";
  {
    PairDelim init_pair(ost);
    ost << REAL << " *input_h = (" << REAL << "*)malloc(" << num_input << "*species_stride*sizeof(" << REAL << "));\n";
    // Initialize the temperatures to something reasonable
    {
      ost << "for (int i = 0; i < species_stride; i++)\n";
      PairDelim loop_pair(ost);
      ost << "input_h[i] = 400.0*drand48() + 100.0;\n";
    }
    // Initialize everything else to something small between 0 and 1
    {
      ost << "for (int i = species_stride; i < (" << num_input << "*species_stride); i++)\n";
      PairDelim loop_pair(ost);
      ost << "input_h[i] = drand48();\n";
    }
    // Copy down to the device
    ost << "CUDA_SAFE_CALL(cudaMemcpy(input_d,input_h," << num_input << "*species_stride*sizeof(" << REAL << "), cudaMemcpyHostToDevice));\n";
    ost << "free(input_h);\n";
  }
  ost << "\n";
  ost << "CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));\n";
  ost << "\n";
  ost << REAL << " *temperature_ptr = input_d;\n";
  ost << REAL << " *pressure_ptr = input_d + species_stride;\n";
  ost << REAL << " *mass_frac_ptr = input_d + 2*species_stride;\n";
  ost << REAL << " *diffusion_ptr = input_d + " << (2+num_species) << "*species_stride;\n";
  ost << REAL << " *output_d;\n";
  ost << "printf(\"Allocating %ld MB for output\\n\", species_stride*" << num_output << "*sizeof(" << REAL << ") >> 20);\n";
  ost << "CUDA_SAFE_CALL(cudaMalloc((void**)&output_d, species_stride*" << num_output << "*sizeof(" << REAL << ")));\n";
  ost << "cudaStream_t timing_stream;\n";
  ost << "CUDA_SAFE_CALL(cudaStreamCreate(&timing_stream));\n";
  ost << "\n";
  ost << "cudaEvent_t start, stop;\n";
  ost << "CUDA_SAFE_CALL(cudaEventCreate(&start));\n";
  ost << "CUDA_SAFE_CALL(cudaEventCreate(&stop));\n";
  ost << "\n";
  if (striding_z)
  {
    //ost << "dim3 grid(nx/" << (EXTRA_WARPS*POINTS_PER_CTA) << ",ny,1);\n";
    //ost << "dim3 block(" << ((unit->threads_per_point+EXTRA_WARPS)*32) << ",1,1);\n";
    ost << "dim3 grid(nx/32,ny,1);\n";
    //ost << "dim3 block(" << ((unit->threads_per_point+4)*32) << ",1,1);\n";
    //ost << "dim3 block(" << ((unit->threads_per_point+2*MOLE_FRAC_WARPS+2)*32) << ",1,1);\n";
    ost << "dim3 block(" << (unit->threads_per_point*32) << ",1,1);\n";
  }
  else
  {
    ost << "dim3 grid(nx/32,ny/4,nz);\n";
    ost << "dim3 block(32,4,1);\n";
  }
  ost << "assert(grid.x > 0);\n";
  ost << "assert(grid.y > 0);\n";
  ost << "\n";
  ost << "CUDA_SAFE_CALL(cudaEventRecord(start, timing_stream));\n";
  ost << "for (int i = 0; i < num_steps; i++)\n";
  {
    PairDelim loop_pair(ost);
    ost << expr_name << "<<<grid,block,0,timing_stream>>>(";
    ost << "temperature_ptr";
    ost << ", pressure_ptr";
    ost << ", mass_frac_ptr";
    ost << ", diffusion_ptr";
    ost << ", dt";
    if (striding_z) // do 1 over dt for these kernels
      ost << ", (1.0/dt)";
    ost << ", nx*ny";
    ost << ", nx";
    ost << ", nz";
    ost << ", species_stride";
    ost << ", 0";
    ost << ", output_d";
    ost << ");\n";
    ost << "#ifdef DEBUG\n";
    ost << "CUDA_SAFE_CALL(cudaGetLastError());\n";
    ost << "#endif\n";
  }
  ost << "CUDA_SAFE_CALL(cudaEventRecord(stop, timing_stream));\n";
  ost << "CUDA_SAFE_CALL(cudaStreamSynchronize(timing_stream));\n";
  ost << "float exec_time;\n";
  ost << "CUDA_SAFE_CALL(cudaEventElapsedTime(&exec_time,start,stop));\n";
  ost << "\n";
  ost << "CUDA_SAFE_CALL(cudaFree(input_d));\n";
  ost << "CUDA_SAFE_CALL(cudaFree(output_d));\n";
  ost << "CUDA_SAFE_CALL(cudaStreamDestroy(timing_stream));\n";
  ost << "\n";
#if 0
  {
    unsigned total_read_transfers = (unit->ordered_species.size()-1) + (pressure ? 2: 1);
    unsigned total_write_transfers = (full_out ? (unit->ordered_species.size()-1) : 1);
    ost << "double total_read_bytes = double(num_steps)*double(nx*ny*nz)*" << total_read_transfers << "*sizeof(" << REAL << ");\n";
    ost << "double total_write_bytes = double(num_steps)*double(nx*ny*nz)*" << total_write_transfers << "*sizeof(" << REAL << ");\n";
    ost << "double read_bandwidth = double(total_read_bytes) * 1e-6 / double(exec_time);\n";
    ost << "double write_bandwidth = double(total_write_bytes) * 1e-6 / double(exec_time);\n";
    ost << "fprintf(stdout,\"Read Bandwidth: %lf GB/s\\n\", read_bandwidth);\n";
    ost << "fprintf(stdout,\"Write Bandwidth: %lf GB/s\\n\", write_bandwidth);\n";
    ost << "fprintf(stdout,\"Total Bandwidth: %lf GB/s\\n\", read_bandwidth + write_bandwidth);\n";
  }
#endif
  ost << "\n";
  ost << "return exec_time;\n";
}

/////////////////////////////
//
// Operation 
//
/////////////////////////////

void Operation::sanity_check(void)
{
  assert(fread.size() >= fwrite.size());
  assert(bread.size() >= bwrite.size());
  for (std::set<int>::const_iterator it = fwrite.begin();
        it != fwrite.end(); it++)
  {
    assert(fread.find(*it) != fread.end());
  }
  for (std::set<int>::const_iterator it = bwrite.begin();
        it != bwrite.end(); it++)
  {
    assert(bread.find(*it) != bread.end());
  }
}

void Operation::get_read_values(std::set<int> &forward, std::set<int> &backward) const
{
  forward.insert(fread.begin(),fread.end());
  backward.insert(bread.begin(),bread.end());
}

void Operation::get_write_values(std::set<int> &forward, std::set<int> &backward) const
{
  forward.insert(fwrite.begin(),fwrite.end());
  backward.insert(bwrite.begin(),bwrite.end());
}

void Operation::remove_read_values(std::set<int> &forward, std::set<int> &backward) const
{
  for (std::set<int>::const_iterator it = fread.begin();
        it != fread.end(); it++)
  {
    forward.erase(*it);
  }
  for (std::set<int>::const_iterator it = bread.begin();
        it != bread.end(); it++)
  {
    backward.erase(*it);
  }
}

void Operation::remove_write_values(std::set<int> &forward, std::set<int> &backward) const
{
  for (std::set<int>::const_iterator it = fwrite.begin();
        it != fwrite.end(); it++)
  {
    forward.erase(*it);
  }
  for (std::set<int>::const_iterator it = bwrite.begin();
        it != bwrite.end(); it++)
  {
    backward.erase(*it);
  }
}

bool Operation::can_swap(const Operation *previous) const
{
  // Check to see if the write set for that operation overlaps
  // our read set
  for (std::set<int>::const_iterator it = previous->fwrite.begin();
        it != previous->fwrite.end(); it++)
  {
    if (fread.find(*it) != fread.end())
      return false;
  }
  for (std::set<int>::const_iterator it = previous->bwrite.begin();
        it != previous->bwrite.end(); it++)
  {
    if (bread.find(*it) != bread.end())
      return false;
  }
  return true;
}

void Operation::compute_liveness(Liveness &live_sets)
{
  // Our live variables are the ones that are read by somebody later
  // and we write so go through each of the variables in the live
  // sets and see if they are in our write set
  for (std::set<int>::const_iterator it = live_sets.fneeded.begin();
        it != live_sets.fneeded.end(); it++)
  {
    if (fwrite.find(*it) != fwrite.end())
      flive.insert(*it);
  }
  for (std::set<int>::const_iterator it = live_sets.bneeded.begin();
        it != live_sets.bneeded.end(); it++)
  {
    if (bwrite.find(*it) != bwrite.end())
      blive.insert(*it);
  }

  // Then put the variables that we need into the read set
  live_sets.fneeded.insert(fread.begin(),fread.end());
  live_sets.bneeded.insert(bread.begin(),bread.end());
  // We can also remove any variables that we are the first ones to write
  for (std::set<int>::const_iterator it = ffirst.begin();
        it != ffirst.end(); it++)
  {
    live_sets.fneeded.erase(*it);
  }
  for (std::set<int>::const_iterator it = bfirst.begin();
        it != bfirst.end(); it++)
  {
    live_sets.bneeded.erase(*it);
  }
}

void Operation::compute_first(Firstness &first)
{
  // Go through the list of variables that we read and see
  // if we are the first one to write it.  If so add it
  // to the list of first variables
  for (std::set<int>::const_iterator it = fread.begin();
        it != fread.end(); it++)
  {
    if (first.fmade.find(*it) == first.fmade.end())
    {
      ffirst.insert(*it);
      first.fmade.insert(*it);
    }
    if (first.reactions.find(*it) == first.reactions.end())
    {
      reactions.insert(*it);
      first.reactions.insert(*it);
    }
  }
  for (std::set<int>::const_iterator it = bread.begin();
        it != bread.end(); it++)
  {
    if (first.bmade.find(*it) == first.bmade.end())
    {
      bfirst.insert(*it);
      first.bmade.insert(*it);
    }
    if (first.reactions.find(*it) == first.reactions.end())
    {
      reactions.insert(*it);
      first.reactions.insert(*it);
    }
  }
}

/////////////////////////////
//
// ConnectedComponent 
//
/////////////////////////////

ConnectedComponent::ConnectedComponent(TranslationUnit *u, ConnectedComponent *o)
  : unit(u), current_den(NULL), current_x(NULL)  
{

}

void ConnectedComponent::add_forward_zero(int reac_idx, int qss_idx, int coeff)
{
  add_fread(reac_idx);
  get_qss(qss_idx)->add_forward_zero(reac_idx, coeff);
}

void ConnectedComponent::add_backward_zero(int reac_idx, int qss_idx, int coeff)
{
  add_bread(reac_idx);
  get_qss(qss_idx)->add_backward_zero(reac_idx, coeff);
}

void ConnectedComponent::add_forward_contribution(int reac_idx, int qss_idx, int other_idx, int coeff)
{
  add_fread(reac_idx);
  get_qss(qss_idx)->add_forward_contribution(reac_idx, other_idx, coeff);
}

void ConnectedComponent::add_backward_contribution(int reac_idx, int qss_idx, int other_idx, int coeff)
{
  add_bread(reac_idx);
  get_qss(qss_idx)->add_backward_contribution(reac_idx, other_idx, coeff);
}

void ConnectedComponent::add_forward_denom(int qss_idx, int reac_idx)
{
  add_fread(reac_idx);
  get_qss(qss_idx)->add_forward_denom(reac_idx);
}

void ConnectedComponent::add_backward_denom(int qss_idx, int reac_idx)
{
  add_bread(reac_idx);
  get_qss(qss_idx)->add_backward_denom(reac_idx);
}

void ConnectedComponent::add_forward_correction(int reac_idx, int qss_idx)
{
  add_fread(reac_idx);
  add_fwrite(reac_idx);
  get_qss(qss_idx)->add_forward_correction(reac_idx);
}

void ConnectedComponent::add_backward_correction(int reac_idx, int qss_idx)
{
  add_bread(reac_idx);
  add_bwrite(reac_idx);
  get_qss(qss_idx)->add_backward_correction(reac_idx);
}

void ConnectedComponent::add_zero_statement(int y, int x)
{
  statements.push_back(new Zero(y, x));
}

void ConnectedComponent::begin_den_statement(void)
{
  assert(current_den == NULL);
  current_den = new Den();
}

void ConnectedComponent::end_den_statement(void)
{
  assert(current_den != NULL);
  statements.push_back(current_den);
  current_den = NULL;
}

void ConnectedComponent::add_den_pair(int y, int x)
{
  assert(current_den != NULL);
  current_den->add_pair(y,x);
}

void ConnectedComponent::add_multiply_statement(int z, int y, int x)
{
  statements.push_back(new Multiply(z,y,x));
}

void ConnectedComponent::add_add_statement(int z, int y, int x)
{
  statements.push_back(new Add(z,y,x));
}

void ConnectedComponent::add_divide_statement(int z, int y)
{
  statements.push_back(new Divide(z,y));
}

void ConnectedComponent::start_x_statement(int x)
{
  assert(current_x == NULL);
  current_x = new Xstat(x);
}

void ConnectedComponent::finish_x_statement(void)
{
  assert(current_x != NULL);
  statements.push_back(current_x);
  current_x = NULL;
}

void ConnectedComponent::add_x_pair(int x, int y)
{
  assert(current_x != NULL);
  current_x->add_pair(x,y);
}

void ConnectedComponent::add_species(int id, int spec)
{
  assert(species.find(id) == species.end());
  species[id] = new QSS(id, spec, unit);
}

bool ConnectedComponent::has_species(int spec)
{
  return (species.find(spec) != species.end());
}

QSS* ConnectedComponent::get_qss(int spec)
{
  assert(species.find(spec) != species.end());
  return species[spec];
}

void ConnectedComponent::compute_allocations(std::vector<Allocation*> &needed_allocations,
                                             const std::vector<Warp*> &warps, int reaction_granularity)
{
  // For each QSS species do the computations for the needed allocations
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->compute_allocations(needed_allocations,warps,reaction_granularity);
  }
}

void ConnectedComponent::clear_allocations(void)
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->clear_allocations(); 
  }
}

bool ConnectedComponent::is_simple(void) const
{
  if (species.size() == 1)
  {
    // Better be an XQ statement
    assert(statements.size() == 1); 
    return true;
  }
  return false;
}

bool ConnectedComponent::has_qss(QSS *qss) const
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    if (it->second == qss)
      return true;
  }
  return false;
}

int ConnectedComponent::updates_warp(Warp *warp) const
{
  int num_updates = 0;
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    if (it->second->updates_warp(warp))
      num_updates++;
  }
  return num_updates;
}

int ConnectedComponent::emit_updates(CodeOutStream &ost, Warp *warp, int phase_id, int scaling_offset)
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    if (it->second->updates_warp(warp))
    {
      it->second->emit_updates(ost,warp,phase_id,scaling_offset);
      scaling_offset++;
    }
  }
  return scaling_offset;
}

int ConnectedComponent::emit_qssa_updates(CodeOutStream &ost, Warp *warp, int scaling_offset)
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    if (it->second->updates_warp(warp))
    {
      it->second->emit_qssa_updates(ost,warp,scaling_offset);
      scaling_offset++;
    }
  }
  return scaling_offset;
}

void ConnectedComponent::emit_scaling_reads(CodeOutStream &ost, Warp *warp, int phase_id, 
                                            int &src_offset, int &dst_offset)
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    if (it->second->updates_warp(warp))
    {
      it->second->emit_scaling_read(ost,warp,phase_id,src_offset,dst_offset);
      dst_offset++;
    }
    src_offset++;
  }
}

int ConnectedComponent::emit_statements(CodeOutStream &ost, int output_offset)
{
  std::set<std::pair<int,int> > already_emitted;
  // First tell all the species to emit their statements
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_statements(ost,already_emitted,false/*stage*/); 
  }
  // Emit forward declarations for all the X values
  PairDelim cc_pair(ost);
  ost << REAL << " den";
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    ost << ", xq_" << it->first;
  }
  ost << ";\n";
  // Now we can emit the statements for this connected component
  for (std::vector<Statement*>::const_iterator it = statements.begin();
        it != statements.end(); it++)
  {
    (*it)->emit_code(ost,already_emitted); 
  }
  // Now write the xq values into shared memory
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    ost << "scratch[" << output_offset << "][tid] = xq_" << it->first << ";\n";
    output_offset++;
  }
  return output_offset;
}

void ConnectedComponent::emit_cpp_statements(CodeOutStream &ost)
{
  ost << "// QSSA connected component\n";
  PairDelim qssa_pair(ost);
  std::set<std::pair<int,int> > already_emitted;
  // First tell all the species to emit their statements
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_cpp_statements(ost,already_emitted); 
  }
  // Emit forward declarations for all the X values
  ost << REAL << " den";
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    ost << ", xq_" << it->first;
  }
  ost << ";\n";
  // Now we can emit the statements for this connected component
  for (std::vector<Statement*>::const_iterator it = statements.begin();
        it != statements.end(); it++)
  {
    (*it)->emit_code(ost,already_emitted); 
  }
  // Finally update all the reactions 
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_cpp_updates(ost);
  }
}

void ConnectedComponent::emit_stage_loads(CodeOutStream &ost, const std::map<Warp*,unsigned> &stage, bool first)
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_stage_loads(ost, stage, first);
  }
}

unsigned ConnectedComponent::emit_stage_statements(CodeOutStream &ost, const char *target, unsigned target_offset)
{
  std::set<std::pair<int,int> > already_emitted;
  // First tell all the species to emit their statements
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_statements(ost,already_emitted,true/*stage*/); 
  }
  // Emit forward declarations for all the X values
  PairDelim cc_pair(ost);
  ost << REAL << " den";
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    ost << ", xq_" << it->first;
  }
  ost << ";\n";
  // Now we can emit the statements for this connected component
  for (std::vector<Statement*>::const_iterator it = statements.begin();
        it != statements.end(); it++)
  {
    (*it)->emit_code(ost,already_emitted); 
  }
  // Now write the xq values into shared memory
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    ost << target << "[" << target_offset << "][tid] = xq_" << it->first << ";\n";
    target_offset++;
  }
  return target_offset;
}

void ConnectedComponent::emit_pipe_variable_declarations(CodeOutStream &ost)
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_pipe_variable_declarations(ost);
  }
}

void ConnectedComponent::emit_pipe_updates(CodeOutStream &ost, unsigned local_idx, int reac_idx)
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_pipe_updates(ost, local_idx, reac_idx);
  }
}

void ConnectedComponent::emit_pipe_qssa_computation(CodeOutStream &ost)
{
  std::set<std::pair<int,int> > already_emitted;
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_pipe_qssa_computation(ost, already_emitted);
  }
  PairDelim cc_pair(ost);
  ost << REAL << " den";
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    ost << ", xq_" << it->first;
  }
  ost << ";\n";
  // Now we can emit the statements for this connected component
  for (std::vector<Statement*>::const_iterator it = statements.begin();
        it != statements.end(); it++)
  {
    (*it)->emit_code(ost,already_emitted); 
  }
  // Finally write the values into the right place
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    ost << "xq_values[" << (it->first-1) << "] = xq_" << it->first << ";\n";
  }
}

void ConnectedComponent::emit_pipe_corrections(CodeOutStream &ost, unsigned local_idx, int reac_idx)
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_pipe_corrections(ost, local_idx, reac_idx);
  }
}

void ConnectedComponent::emit_last_simple_statements(CodeOutStream &ost, int avars_off)
{
  // Better be a simple connected component
  assert(species.size() == 1);
  assert(statements.size() == 1);
  Xstat *statement = dynamic_cast<Xstat*>(statements[0]);
  // Check for xstat
  assert(statement != NULL);
  ost << "scratch[QSS_OFFSET+" << (statement->x-1) << "][tid] = avars[" << avars_off << "];\n";
}

void ConnectedComponent::emit_last_simple_qssvalues(CodeOutStream &ost)
{
  // Better be a simple connected component
  assert(species.size() == 1);
  assert(statements.size() == 1);
  Xstat *statement = dynamic_cast<Xstat*>(statements[0]);
  // Check for xstat
  assert(statement != NULL);
  QSS *spec = species.begin()->second;
  ost << "// Storing XQ value " << spec->qss_idx << "\n";
  ost << "scratch[QSS_OFFSET+" << (statement->x-1) << "][tid] = ";
  ost << "a" << spec->qss_idx << "_0;\n";
}

void ConnectedComponent::emit_last_complex_statements(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap)
{
  for (std::vector<Statement*>::const_iterator it = statements.begin();
        it != statements.end(); it++)
  {
    (*it)->emit_last_code(ost, amap);
  }
}

void ConnectedComponent::emit_last_corrections(CodeOutStream &ost, unsigned local_idx, int reac_idx, 
                                              std::set<int> &loaded_qss_values)
{
  for (std::map<int,QSS*>::const_iterator it = species.begin();
        it != species.end(); it++)
  {
    it->second->emit_last_corrections(ost, local_idx, reac_idx, loaded_qss_values);
  }
}

void ConnectedComponent::Statement::emit_last_var(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap,
                                                  std::pair<int,int> key)
{
  assert(amap.find(key) != amap.end());
  if (amap[key] == -1)
    ost << "a" << key.first << "_" << key.second;
  else
    ost << "scratch[AVAL_OFFSET+" << amap[key] << "][tid]";
}

void ConnectedComponent::Statement::emit_last_load(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap,
                                                   std::pair<int,int> key)
{
#if 0
  assert(amap.find(key) != amap.end());
  if (amap[key] != -1)
  {
    ost << REAL << " a" << key.first << "_" << key.second << " = scratch[NUM_SPECIES+NUM_QSS+" << amap[key] << "][tid];\n";
    amap[key] = -1;
  }
#endif
}

void ConnectedComponent::Zero::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted)
{
  std::pair<int,int> key(y,0);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << REAL << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_0 = a" << y << "_0 + a" << y << "_" << x << " * a" << x << "_0;\n";
}

void ConnectedComponent::Zero::emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap)
{
  std::pair<int,int> key(y,0);
  assert(amap.find(key) != amap.end());
  ost << "/* a" << y << "_0 = a" << y << "_0 + a" << y << "_" << x << " * a" << x << "_0; */\n";
  emit_last_load(ost, amap, key);
  emit_last_load(ost, amap, std::pair<int,int>(y,x));
  emit_last_load(ost, amap, std::pair<int,int>(x,0));
  emit_last_var(ost, amap, key);
  ost << " += (";
  emit_last_var(ost, amap, std::pair<int,int>(y,x));
  ost << " * ";
  emit_last_var(ost, amap, std::pair<int,int>(x,0));
  ost << ");\n";
}

void ConnectedComponent::Den::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted)
{
  ost << "den = 1";
  for (std::vector<std::pair<int,int> >::const_iterator it = pairs.begin();
        it != pairs.end(); it++)
  {
    ost << " - a" << it->second << "_" << it->first << "*a" << it->first << "_" << it->second;
  }
  ost << ";\n";
}

void ConnectedComponent::Den::emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap)
{
  ost << "/* den = 1";
  for (std::vector<std::pair<int,int> >::const_iterator it = pairs.begin();
        it != pairs.end(); it++)
  {
    ost << " - a" << it->second << "_" << it->first << "*a" << it->first << "_" << it->second;
  }
  ost << "; */\n";
  for (std::vector<std::pair<int,int> >::const_iterator it = pairs.begin();
        it != pairs.end(); it++)
  {
    emit_last_load(ost, amap, std::pair<int,int>(it->second,it->first));
    emit_last_load(ost, amap, *it);
  }
  ost << "den = 1";
  for (std::vector<std::pair<int,int> >::const_iterator it = pairs.begin();
        it != pairs.end(); it++)
  {
    ost << " - ";
    emit_last_var(ost, amap, std::pair<int,int>(it->second,it->first));
    ost << " * ";
    emit_last_var(ost, amap, (*it));
  }
  ost << ";\n";
}

void ConnectedComponent::Multiply::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted)
{
  std::pair<int,int> key(y,z);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << REAL << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_" << z << " = a" << y << "_" << x << " * a" << x << "_" << z << ";\n"; 
}

void ConnectedComponent::Multiply::emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap)
{
  ost << "/* a" << y << "_" << z << " = a" << y << "_" << x << " * a" << x << "_" << z << "; */\n";
  std::pair<int,int> key(y,z);
  emit_last_load(ost, amap, std::pair<int,int>(y,x));
  emit_last_load(ost, amap, std::pair<int,int>(x,z));
  if (amap.find(key) == amap.end())
  {
    ost << REAL << " ";
    amap[key] = -1;
  }
  emit_last_load(ost, amap, key);
  emit_last_var(ost, amap, key);
  ost << " = ";
  emit_last_var(ost, amap, std::pair<int,int>(y,x));
  ost << " * ";
  emit_last_var(ost, amap, std::pair<int,int>(x,z));
  ost << ";\n";
}

void ConnectedComponent::Add::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted)
{
  std::pair<int,int> key(y,z);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << REAL << " ";
    already_emitted.insert(key);
  }
  ost << "a" << y << "_" << z << " = a" << y << "_" << z << " + a" << y << "_" << x << " * a" << x << "_" << z << ";\n";
}

void ConnectedComponent::Add::emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap)
{
  ost << "/* a" << y << "_" << z << " = a" << y << "_" << z << " + a" << y << "_" << x << " * a" << x << "_" << z << "; */\n";
  std::pair<int,int> key(y,z);
  emit_last_load(ost, amap, std::pair<int,int>(y,z));
  emit_last_load(ost, amap, std::pair<int,int>(y,x));
  emit_last_load(ost, amap, std::pair<int,int>(x,z));
  if (amap.find(key) == amap.end())
  {
    ost << REAL << " ";
    amap[key] = -1;
  }
  emit_last_load(ost, amap, key);
  emit_last_var(ost, amap, key);
  ost << " = ";
  emit_last_var(ost, amap, std::pair<int,int>(y,z));
  ost << " + ";
  emit_last_var(ost, amap, std::pair<int,int>(y,x));
  ost << " * ";
  emit_last_var(ost, amap, std::pair<int,int>(x,z));
  ost << ";\n";
}

void ConnectedComponent::Divide::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted)
{
  std::pair<int,int> key(z,y);
  if (already_emitted.find(key) == already_emitted.end())
  {
    ost << REAL << " ";
    already_emitted.insert(key);
  }
  ost << "a" << z << "_" << y << " = a" << z << "_" << y << "/den;\n";
}

void ConnectedComponent::Divide::emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap)
{
  ost << "/* a" << z << "_" << y << " = a" << z << "_" << y << "/den; */\n";
  std::pair<int,int> key(z,y);
  if (amap.find(key) == amap.end())
  {
    //ost << REAL << " ";
    amap[key] = -1;
  }
  emit_last_load(ost, amap, key);
  emit_last_var(ost, amap, key);
  ost << " /= den;\n";
}

void ConnectedComponent::Xstat::emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted)
{
  ost << "xq_" << x << " = a" << x << "_0";
  for (std::set<int>::const_iterator it = y_vals.begin();
        it != y_vals.end(); it++)
  {
    ost << " + a" << x << "_" << (*it) << "*xq_" << (*it);
  }
  ost << ";\n";
}

void ConnectedComponent::Xstat::emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap)
{
  ost << "/* xq_" << x << " = a" << x << "_0";
  for (std::set<int>::const_iterator it = y_vals.begin();
        it != y_vals.end(); it++)
  {
    ost << " + a" << x << "_" << (*it) << "*xq_" << (*it);
  }
  ost << "; */\n";
  emit_last_load(ost, amap, std::pair<int,int>(x,0));
  for (std::set<int>::const_iterator it = y_vals.begin();
        it != y_vals.end(); it++)
  {
    emit_last_load(ost, amap, std::pair<int,int>(x,(*it)));
  }
  ost << "scratch[QSS_OFFSET+" << (x-1) << "][tid] = ";
  emit_last_var(ost, amap, std::pair<int,int>(x,0)); 
  for (std::set<int>::const_iterator it = y_vals.begin();
        it != y_vals.end(); it++)
  {
    std::pair<int,int> key(x,(*it));
    ost << " + ";
    emit_last_var(ost, amap, key);
    ost << "*scratch[QSS_OFFSET+" << ((*it)-1) << "][tid]";
  }
  ost << ";\n";
}

/////////////////////////////
//
// QSS
//
/////////////////////////////

QSS::QSS(int id, int spec, TranslationUnit *u)
  : qss_idx(id), species_idx(spec), unit(u)
{

}

int QSS::get_total_values(void) const
{
  std::set<int> others;
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  return (others.size()+1);
}

void QSS::add_forward_zero(int reac_idx, int coeff)
{
  assert(coeff != 0);
  assert(forward_zeros.find(reac_idx) == forward_zeros.end());
  forward_zeros[reac_idx] = coeff;
}

void QSS::add_backward_zero(int reac_idx, int coeff)
{
  assert(coeff != 0);
  assert(backward_zeros.find(reac_idx) == backward_zeros.end());
  backward_zeros[reac_idx] = coeff;
}

void QSS::add_forward_contribution(int reac_idx, int other_idx, int coeff)
{
  assert(coeff != 0);
  std::pair<int,int> key(other_idx,reac_idx);
  assert(forward_contributions.find(key) == forward_contributions.end());
  forward_contributions[key] = coeff;
}

void QSS::add_backward_contribution(int reac_idx, int other_idx, int coeff)
{
  assert(coeff != 0);
  std::pair<int,int> key(other_idx,reac_idx);
  assert(backward_contributions.find(key) == backward_contributions.end());
  backward_contributions[key] = coeff;
}

void QSS::add_forward_denom(int reac_idx)
{
  assert(forward_denom.find(reac_idx) == forward_denom.end());
  forward_denom.insert(reac_idx);
}

void QSS::add_backward_denom(int reac_idx)
{
  assert(backward_denom.find(reac_idx) == backward_denom.end());
  backward_denom.insert(reac_idx);
}

void QSS::add_forward_correction(int reac_idx)
{
  assert(forward_corrections.find(reac_idx) == forward_corrections.end());
  forward_corrections.insert(reac_idx);
}

void QSS::add_backward_correction(int reac_idx)
{
  assert(backward_corrections.find(reac_idx) == backward_corrections.end());
  backward_corrections.insert(reac_idx);
}

void QSS::compute_allocations(std::vector<Allocation*> &needed_allocations,
                              const std::vector<Warp*> &warps, int reaction_granularity)
{
  compute_zero_allocations(needed_allocations,warps,reaction_granularity);
  // Get the set of different outputs that we need
  {
    std::set<int> expressions;
    for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
          it != forward_contributions.end(); it++)
    {
      expressions.insert(it->first.first);
    }
    for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
          it != backward_contributions.end(); it++)
    {
      expressions.insert(it->first.first);
    }
    for (std::set<int>::const_iterator it = expressions.begin();
          it != expressions.end(); it++)
    {
      compute_contribution_allocations(*it, needed_allocations, warps, reaction_granularity);
    }
  }
  compute_denominator_allocations(needed_allocations,warps,reaction_granularity);
}

void QSS::compute_zero_allocations(std::vector<Allocation*> &needed_allocations,
                                   const std::vector<Warp*> &warps, int reaction_granularity)
{
  int target_warp = -1;
  int remaining_allocations = reaction_granularity;
  // The allocations done for this statement
  std::map<int,Allocation*> warp_allocations;
  help_compute_allocations(needed_allocations,warp_allocations,zero_allocations,
                      forward_zeros,warps,target_warp,remaining_allocations,true/*forwards*/,reaction_granularity);
  help_compute_allocations(needed_allocations,warp_allocations,zero_allocations,
                      backward_zeros,warps,target_warp,remaining_allocations,false/*forwards*/,reaction_granularity);
}

void QSS::compute_contribution_allocations(int other, std::vector<Allocation*> &needed_allocations,
                                           const std::vector<Warp*> &warps, int reaction_granularity)
{
  int target_warp = -1;
  int remaining_allocations = reaction_granularity;
  std::map<int,Allocation*> warp_allocations;
  contribution_allocations[other] = std::vector<Allocation*>();
  std::vector<Allocation*> &contrib_allocations = contribution_allocations[other];
  {
    std::map<int,int> forward_reactions;
    for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
          it != forward_contributions.end(); it++)
    {
      if (it->first.first == other)
        forward_reactions[it->first.second] = it->second;
    }
    help_compute_allocations(needed_allocations,warp_allocations,contrib_allocations,
                        forward_reactions,warps,target_warp,remaining_allocations,true/*forwards*/,reaction_granularity);
  }
  {
    std::map<int,int> backward_reactions;
    for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
          it != backward_contributions.end(); it++)
    {
      if (it->first.first == other)
        backward_reactions[it->first.second] = it->second;
    }
    help_compute_allocations(needed_allocations,warp_allocations,contrib_allocations,
                        backward_reactions,warps,target_warp,remaining_allocations,false/*forwards*/,reaction_granularity);
  }
}

void QSS::compute_denominator_allocations(std::vector<Allocation*> &needed_allocations,
                                          const std::vector<Warp*> &warps, int reaction_granularity)
{
  int target_warp = -1;
  int remaining_allocations = reaction_granularity;
  std::map<int,Allocation*> warp_allocations;
  {
    std::map<int,int> forward_denom_with_coeff;
    for (std::set<int>::const_iterator it = forward_denom.begin();
          it != forward_denom.end(); it++)
    {
      forward_denom_with_coeff[*it] = 1;
    }
    help_compute_allocations(needed_allocations,warp_allocations,denominator_allocations,
                        forward_denom_with_coeff,warps,target_warp,remaining_allocations,true/*forwards*/,reaction_granularity);
  }
  {
    std::map<int,int> backward_denom_with_coeff;
    for (std::set<int>::const_iterator it = backward_denom.begin();
          it != backward_denom.end(); it++)
    {
      backward_denom_with_coeff[*it] = 1;
    }
    help_compute_allocations(needed_allocations,warp_allocations,denominator_allocations,
                        backward_denom_with_coeff,warps,target_warp,remaining_allocations,false/*forwards*/,reaction_granularity);
  }
}

void QSS::clear_allocations(void)
{
  zero_allocations.clear();
  contribution_allocations.clear();
  denominator_allocations.clear();
}

bool QSS::updates_warp(Warp *warp) const
{
  for (std::set<int>::const_iterator it = forward_corrections.begin();
        it != forward_corrections.end(); it++)
  {
    int index = warp->get_reaction_index(*it);
    if (index != -1)
      return true;
  }
  for (std::set<int>::const_iterator it = backward_corrections.begin();
        it != backward_corrections.end(); it++)
  {
    int index = warp->get_reaction_index(*it);
    if (index != -1)
      return true;
  }
  return false;
}

void QSS::emit_updates(CodeOutStream &ost, Warp *warp, int phase_id, int scaling_offset)
{
  for (std::set<int>::const_iterator it = forward_corrections.begin();
        it != forward_corrections.end(); it++)
  {
    int index = warp->get_reaction_index(*it);
    if (index != -1)
    {
      ost << "rr_f[" << index << "] *= phase_" << phase_id << "_factor[" << scaling_offset << "];\n";
    }
  }
  for (std::set<int>::const_iterator it = backward_corrections.begin();
        it != backward_corrections.end(); it++)
  {
    int index = warp->get_reaction_index(*it);
    if (index != -1)
    {
      ost << "rr_f[" << index << "] *= phase_" << phase_id << "_factor[" << scaling_offset << "];\n";
    }
  }
}

void QSS::emit_qssa_updates(CodeOutStream &ost, Warp *warp, int scaling_offset)
{
  for (std::set<int>::const_iterator it = forward_corrections.begin();
        it != forward_corrections.end(); it++)
  {
    int index = warp->get_reaction_index(*it);
    if (index != -1)
    {
#ifdef RR_INDEXING
      ost << "rr_f[" << index << "] *= xq_values[" << scaling_offset << "];\n";
#else
      ost << "rr_f_" << index << " *= xq_values[" << scaling_offset << "];\n";
#endif
    }
  }
  for (std::set<int>::const_iterator it = backward_corrections.begin();
        it != backward_corrections.end(); it++)
  {
    int index = warp->get_reaction_index(*it);
    if (index != -1)
    {
#ifdef RR_INDEXING
      ost << "rr_r[" << index << "] *= xq_values[" << scaling_offset << "];\n";
#else
      ost << "rr_r_" << index << " *= xq_values[" << scaling_offset << "];\n";
#endif
    }
  }
}

void QSS::emit_scaling_read(CodeOutStream &ost, Warp *warp, int phase_id, int src_offset, int dst_offset)
{
  ost << "phase_" << phase_id << "_factor[" << dst_offset << "] = scratch[" << src_offset <<"][tid];\n";
}

void QSS::emit_statements(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, bool stage)
{
  // Emit declarations for this QSS species
  if (!stage)
    ost << REAL << " a" << qss_idx << "_0";
  already_emitted.insert(std::pair<int,int>(qss_idx,0));
  for (std::map<int,std::vector<Allocation*> >::const_iterator it = contribution_allocations.begin();
        it != contribution_allocations.end(); it++)
  {
    if (!stage)
      ost << ", a" << qss_idx << "_" << it->first;
    already_emitted.insert(std::pair<int,int>(qss_idx,it->first));
  }
  if (!stage)
    ost << ";\n";
  PairDelim spec_pair(ost);
  // First compute the denominator
  if (!stage)
  {
    ost << REAL << " den = ";
    bool first = true;
    for (std::vector<Allocation*>::const_iterator it = denominator_allocations.begin();
          it != denominator_allocations.end(); it++)
    {
      assert((*it)->location != -1);
      if (!first)
        ost << " + ";
      if (stage)
        ost << "scratch_" << (*it)->stage << "_" << (*it)->location;
      else
        ost << "scratch[" << (*it)->location << "][tid]";
      first = false;
    }
    ost << ";\n";
    // Now emit the code to compute each of our values
    ost << "a" << qss_idx << "_0 = (";
    first = true;
    if (!zero_allocations.empty())
    {
      for (std::vector<Allocation*>::const_iterator it = zero_allocations.begin();
            it != zero_allocations.end(); it++)
      {
        assert((*it)->location != -1);
        if (!first)
          ost << " + ";
        if (stage)
          ost << "scratch_" << (*it)->stage << "_" << (*it)->location;
        else
          ost << "scratch[" << (*it)->location << "][tid]";
        first = false;
      }
    }
    else
      ost << "0.0";
    ost << ")/den;\n";
    for (std::map<int,std::vector<Allocation*> >::const_iterator cit = contribution_allocations.begin();
          cit != contribution_allocations.end(); cit++)
    {
      ost << "a" << qss_idx << "_" << cit->first << " = (";
      const std::vector<Allocation*> &allocs = cit->second;
      first = true;
      for (std::vector<Allocation*>::const_iterator it = allocs.begin();
            it != allocs.end(); it++)
      {
        assert((*it)->location != -1);
        if (!first)
          ost << " + ";
        if (stage)
          ost << "scratch_" << (*it)->stage << "_" << (*it)->location;
        else
          ost << "scratch[" << (*it)->location << "][tid]";
        first = false;
      }
      ost << ")/den;\n";
    }
  }
  else
  {
    ost << "a" << qss_idx << "_0 /= den_" << qss_idx << ";\n";
    for (std::map<int,std::vector<Allocation*> >::const_iterator it = contribution_allocations.begin();
          it != contribution_allocations.end(); it++)
    {
      ost << "a" << qss_idx << "_" << it->first << " /= den_" << qss_idx << ";\n";
    }
  }
}

void QSS::emit_cpp_statements(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted)
{
  ost << REAL << " a" << qss_idx << "_0";
  already_emitted.insert(std::pair<int,int>(qss_idx,0));
  std::set<int> others;
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << ", a" << qss_idx << "_" << (*it);
    already_emitted.insert(std::pair<int,int>(qss_idx,*it));
  }
  ost << ";\n";
  PairDelim spec_pair(ost);
  // First compute the denominator
  ost << REAL << " den = ";
  bool first = true;
  for (std::set<int>::const_iterator it = forward_denom.begin();
        it != forward_denom.end(); it++)
  {
    if (!first)
      ost << " + ";
    ost << "forward_r[" << ((*it)-1) << "]";
    first = false;
  }
  for (std::set<int>::const_iterator it = backward_denom.begin();
        it != backward_denom.end(); it++)
  {
    if (!first)
      ost << " + ";
    ost << "reverse_r[" << ((*it)-1) << "]";
    first = false;
  }
  ost << ";\n";
  ost << "a" << qss_idx << "_0 = (";
  first = true;
  for (std::map<int,int>::const_iterator it = forward_zeros.begin();
        it != forward_zeros.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      if (!first)
        ost << " + ";
      ost << "forward_r[" << (it->first-1) << "]";
      first = false;
    }
  }
  for (std::map<int,int>::const_iterator it = backward_zeros.begin();
        it != backward_zeros.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      if (!first)
        ost << " + ";
      ost << "reverse_r[" << (it->first-1) << "]";
      first = false;
    }
  }
  if (forward_zeros.empty() && backward_zeros.empty())
    ost << "0.0";
  ost << ")/den;\n";
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << "a" << qss_idx << "_" << (*it) << " = (";
    first = true;
    for (std::map<std::pair<int,int>,int>::const_iterator pit = forward_contributions.begin();
          pit != forward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        for (int i = 0; i < pit->second; i++)
        {
          if (!first)
            ost << " + ";
          ost << "forward_r[" << (pit->first.second-1) << "]";
          first = false;
        }
      }
    }
    for (std::map<std::pair<int,int>,int>::const_iterator pit = backward_contributions.begin();
          pit != backward_contributions.end(); pit++)
    {
      if (pit->first.first == (*it))
      {
        for (int i = 0; i < pit->second; i++)
        {
          if (!first)
            ost << " + ";
          ost << "reverse_r[" << (pit->first.second-1) << "]";
          first = false;
        }
      }
    }
    ost << ")/den;\n";
  }
}

void QSS::emit_cpp_updates(CodeOutStream &ost)
{
  for (std::set<int>::const_iterator it = forward_corrections.begin();
        it != forward_corrections.end(); it++)
  {
    ost << "forward_r[" << ((*it)-1) << "] *= xq_" << qss_idx << ";\n";
  }
  for (std::set<int>::const_iterator it = backward_corrections.begin();
        it != backward_corrections.end(); it++)
  {
    ost << "reverse_r[" << ((*it)-1) << "] *= xq_" << qss_idx << ";\n";
  }
}

void QSS::emit_stage_loads(CodeOutStream &ost, const std::map<Warp*,unsigned> &stage, bool first)
{
  // If this is the first pass through then do allocations of our variables
  if (first)
  {
    ost << REAL << " a" << qss_idx << "_0 = 0.0";
    if (!forward_denom.empty() || !backward_denom.empty())
      ost << ", den_" << qss_idx << " = 0.0";
    for (std::map<int,std::vector<Allocation*> >::const_iterator it = contribution_allocations.begin();
          it != contribution_allocations.end(); it++)
    {
      ost << ", a" << qss_idx << "_" << it->first << " = 0.0";
    }
    ost << ";\n";
  }
  // a_0
  bool first_add = true;
  for (std::vector<Allocation*>::const_iterator it = zero_allocations.begin();
        it != zero_allocations.end(); it++)
  {
    bool in_stage = false;
    for (std::map<Warp*,unsigned>::const_iterator sit = stage.begin();
          sit != stage.end(); sit++)
    {
      if ((*it)->warp_id == sit->first->wid)
      {
        in_stage = true;
        break;
      }
    }
    if (!in_stage)
      continue;
    if (first_add)
      ost << "a" << qss_idx << "_0 = a" << qss_idx << "_0";
    ost << " + scratch[" << (*it)->location << "][tid]";
    first_add = false;
  }
  if (!first_add)
    ost << ";\n";
  // a_something
  for (std::map<int,std::vector<Allocation*> >::const_iterator cit = contribution_allocations.begin();
        cit != contribution_allocations.end(); cit++)
  {
    first_add = true; 
    for (std::vector<Allocation*>::const_iterator it = cit->second.begin();
          it != cit->second.end(); it++)
    {
      bool in_stage = false;
      for (std::map<Warp*,unsigned>::const_iterator sit = stage.begin();
            sit != stage.end(); sit++)
      {
        if ((*it)->warp_id == sit->first->wid)
        {
          in_stage = true;
          break;
        }
      }
      if (!in_stage)
        continue;
      if (first_add)
        ost << "a" << qss_idx << "_" << cit->first << " = a" << qss_idx << "_" << cit->first;
      ost << " + scratch[" << (*it)->location << "][tid]";
      first_add = false;
    }
    if (!first_add)
      ost << ";\n"; 
  }
  // denominator
  first_add = true;
  for (std::vector<Allocation*>::const_iterator it = denominator_allocations.begin();
        it != denominator_allocations.end(); it++)
  {
    bool in_stage = false;
    for (std::map<Warp*,unsigned>::const_iterator sit = stage.begin();
          sit != stage.end(); sit++)
    {
      if ((*it)->warp_id == sit->first->wid)
      {
        in_stage = true;
        break;
      }
    }
    if (!in_stage)
      continue;
    if (first_add)
      ost << "den_" << qss_idx << " = den_" << qss_idx;
    ost << " + scratch[" << (*it)->location << "][tid]";
    first_add = false;
  }
  if (!first_add)
    ost << ";\n";
}

void QSS::emit_pipe_variable_declarations(CodeOutStream &ost)
{
  ost << REAL << " a" << qss_idx << "_0 = 0.0";
  ost << ", den_" << qss_idx << " = 0.0";
  std::set<int> others;
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << ", a" << qss_idx << "_" << (*it) << " = 0.0";
  }
  ost << ";\n";
}

void QSS::emit_pipe_updates(CodeOutStream &ost, unsigned local_idx, int reac_idx)
{
  if (forward_zeros.find(reac_idx) != forward_zeros.end())
  {
    if (forward_zeros[reac_idx] == 1)
      ost << "a" << qss_idx << "_0 += rr_f[" << local_idx << "];\n";
    else if (forward_zeros[reac_idx] == -1)
      ost << "a" << qss_idx << "_0 -= rr_f[" << local_idx << "];\n";
    else
      ost << "a" << qss_idx << "_0 = __fma_rn(" << double(forward_zeros[reac_idx]) 
          << ",rr_f[" << local_idx << "],a" << qss_idx << "_0);\n";
  }
  if (backward_zeros.find(reac_idx) != backward_zeros.end())
  {
    if (backward_zeros[reac_idx] == 1)
      ost << "a" << qss_idx << "_0 += rr_r[" << local_idx << "];\n";
    else if (backward_zeros[reac_idx] == -1)
      ost << "a" << qss_idx << "_0 -= rr_r[" << local_idx << "];\n";
    else
      ost << "a" << qss_idx << "_0 = __fma_rn(" << double(backward_zeros[reac_idx])
          << ",rr_r[" << local_idx << "],a" << qss_idx << "_0);\n";
  }
  if (forward_denom.find(reac_idx) != forward_denom.end())
    ost << "den_" << qss_idx << " += rr_f[" << local_idx << "];\n";
  if (backward_denom.find(reac_idx) != backward_denom.end())
    ost << "den_" << qss_idx << " += rr_r[" << local_idx << "];\n";
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    if (it->first.second == reac_idx)
    {
      if (it->second == 1)
        ost << "a" << qss_idx << "_" << it->first.first << " += rr_f[" << local_idx << "];\n";
      else if (it->second == -1)
        ost << "a" << qss_idx << "_" << it->first.first << " -= rr_f[" << local_idx << "];\n";
      else
        ost << "a" << qss_idx << "_" << it->first.first << " = __fma_rn(" << double(it->second)
            << ",rr_f[" << local_idx << "],a" << qss_idx << "_" << it->first.first << ");\n";
    }
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    if (it->first.second == reac_idx)
    {
      if (it->second == 1)
        ost << "a" << qss_idx << "_" << it->first.first << " += rr_r[" << local_idx << "];\n";
      else if (it->second == -1)
        ost << "a" << qss_idx << "_" << it->first.first << " -= rr_r[" << local_idx << "];\n";
      else
        ost << "a" << qss_idx << "_" << it->first.first << " = __fma_rn(" << double(it->second)
            << ",rr_r[" << local_idx << "],a" << qss_idx << "_" << it->first.first << ");\n";
    }
  }
}

void QSS::emit_pipe_qssa_computation(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted)
{
  already_emitted.insert(std::pair<int,int>(qss_idx,0)); 
  ost << "a" << qss_idx << "_0 /= den_" << qss_idx << ";\n";
  std::set<int> others;
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    already_emitted.insert(std::pair<int,int>(qss_idx,(*it)));
    ost << "a" << qss_idx << "_" << (*it) << " /= den_" << qss_idx << ";\n";
  }
}

void QSS::emit_pipe_corrections(CodeOutStream &ost, unsigned local_idx, int reac_idx)
{
  if (forward_corrections.find(reac_idx) != forward_corrections.end())
    ost << "rr_f[" << local_idx << "] *= xq_values[" << (qss_idx-1) << "];\n";
  if (backward_corrections.find(reac_idx) != backward_corrections.end())
    ost << "rr_r[" << local_idx << "] *= xq_values[" << (qss_idx-1) << "];\n";
}

void QSS::emit_multi_updates(CodeOutStream &ost, int avars_off, int denom_off, int local_idx, int reac_idx)
{
  if (forward_zeros.find(reac_idx) != forward_zeros.end())
  {
    if (forward_zeros[reac_idx] == 1)
      ost << "avars[" << avars_off << "] += rr_f[" << local_idx << "];\n";
    else if (forward_zeros[reac_idx] == -1)
      ost << "avars[" << avars_off << "] -= rr_f[" << local_idx << "];\n";
    else
      ost << "avars[" << avars_off << "] = __fma_rn(" << double(forward_zeros[reac_idx]) 
          << ",rr_f[" << local_idx << "],avars[" << avars_off << "]);\n";
  }
  if (backward_zeros.find(reac_idx) != backward_zeros.end())
  {
    if (backward_zeros[reac_idx] == 1)
      ost << "avars[" << avars_off << "] += rr_r[" << local_idx << "];\n";
    else if (backward_zeros[reac_idx] == -1)
      ost << "avars[" << avars_off << "] -= rr_r[" << local_idx << "];\n";
    else
      ost << "avars[" << avars_off << "] = __fma_rn(" << double(backward_zeros[reac_idx])
          << ",rr_r[" << local_idx << "],avars[" << avars_off << "]);\n";
  }
  if (forward_denom.find(reac_idx) != forward_denom.end())
    ost << "denom[" << denom_off << "] += rr_f[" << local_idx << "];\n";
  if (backward_denom.find(reac_idx) != backward_denom.end())
    ost << "denom[" << denom_off << "] += rr_r[" << local_idx << "];\n";
  std::set<int> others;
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  unsigned other_off = avars_off+1; // already did the first a
  for (std::set<int>::const_iterator other_it = others.begin();
        other_it != others.end(); other_it++)
  {
    for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
          it != forward_contributions.end(); it++)
    {
      if (it->first.first != (*other_it))
        continue;
      if (it->first.second == reac_idx)
      {
        if (it->second == 1)
          ost << "avars[" << other_off << "] += rr_f[" << local_idx << "];\n";
        else if (it->second == -1)
          ost << "avars[" << other_off << "] -= rr_f[" << local_idx << "];\n";
        else
          ost << "avars[" << other_off << "] = __fma_rn(" << double(it->second)
              << ",rr_f[" << local_idx << "],avars[" << other_off << "]);\n";
      }
    }
    for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
          it != backward_contributions.end(); it++)
    {
      if (it->first.first != (*other_it))
        continue;
      if (it->first.second == reac_idx)
      {
        if (it->second == 1)
          ost << "avars[" << other_off << "] += rr_r[" << local_idx << "];\n";
        else if (it->second == -1)
          ost << "avars[" << other_off << "] -= rr_r[" << local_idx << "];\n";
        else
          ost << "avars[" << other_off << "] = __fma_rn(" << double(it->second)
              << ",rr_r[" << local_idx << "],avars[" << other_off << "]);\n";
      }
    }
    other_off++;
  }
}

void QSS::emit_multi_scaled_updates(CodeOutStream &ost, int avars_off, int denom_off, int local_idx, int reac_idx, Reaction *reac)
{
  if (forward_zeros.find(reac_idx) != forward_zeros.end())
  {
    emit_multi_declaration(ost, reac, local_idx, true);
    if (forward_zeros[reac_idx] == 1)
      ost << "avars[" << avars_off << "] += rr_f_" << local_idx << ";\n";
    else if (forward_zeros[reac_idx] == -1)
      ost << "avars[" << avars_off << "] -= rr_f_" << local_idx << ";\n";
    else
      ost << "avars[" << avars_off << "] = __fma_rn(" << double(forward_zeros[reac_idx]) 
          << ",rr_f_" << local_idx << ",avars[" << avars_off << "]);\n";
  }
  if (backward_zeros.find(reac_idx) != backward_zeros.end())
  {
    emit_multi_declaration(ost, reac, local_idx, false);
    if (backward_zeros[reac_idx] == 1)
      ost << "avars[" << avars_off << "] += rr_r_" << local_idx << ";\n";
    else if (backward_zeros[reac_idx] == -1)
      ost << "avars[" << avars_off << "] -= rr_r_" << local_idx << ";\n";
    else
      ost << "avars[" << avars_off << "] = __fma_rn(" << double(backward_zeros[reac_idx])
          << ",rr_r_" << local_idx << ",avars[" << avars_off << "]);\n";
  }
  if (forward_denom.find(reac_idx) != forward_denom.end())
  {
    emit_multi_declaration(ost, reac, local_idx, true);
    ost << "denom[" << denom_off << "] += rr_f_" << local_idx << ";\n";
  }
  if (backward_denom.find(reac_idx) != backward_denom.end())
  {
    emit_multi_declaration(ost, reac, local_idx, false);
    ost << "denom[" << denom_off << "] += rr_r_" << local_idx << ";\n";
  }
  std::set<int> others;
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  unsigned other_off = avars_off+1; // already did the first a
  for (std::set<int>::const_iterator other_it = others.begin();
        other_it != others.end(); other_it++)
  {
    for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
          it != forward_contributions.end(); it++)
    {
      if (it->first.first != (*other_it))
        continue;
      if (it->first.second == reac_idx)
      {
        emit_multi_declaration(ost, reac, local_idx, true);
        if (it->second == 1)
          ost << "avars[" << other_off << "] += rr_f_" << local_idx << ";\n";
        else if (it->second == -1)
          ost << "avars[" << other_off << "] -= rr_f_" << local_idx << ";\n";
        else
          ost << "avars[" << other_off << "] = __fma_rn(" << double(it->second)
              << ",rr_f_" << local_idx << ",avars[" << other_off << "]);\n";
      }
    }
    for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
          it != backward_contributions.end(); it++)
    {
      if (it->first.first != (*other_it))
        continue;
      if (it->first.second == reac_idx)
      {
        emit_multi_declaration(ost, reac, local_idx, false);
        if (it->second == 1)
          ost << "avars[" << other_off << "] += rr_r_" << local_idx << ";\n";
        else if (it->second == -1)
          ost << "avars[" << other_off << "] -= rr_r_" << local_idx << ";\n";
        else
          ost << "avars[" << other_off << "] = __fma_rn(" << double(it->second)
              << ",rr_r_" << local_idx << ",avars[" << other_off << "]);\n";
      }
    }
    other_off++;
  }
}

void QSS::emit_multi_declaration(CodeOutStream &ost, Reaction *reac, int local_idx, bool forward)
{
  ost << REAL << " ";
  if (forward)
    ost << "rr_f_";
  else
    ost << "rr_r_";
  ost << local_idx << " = pipe[" << local_idx << "][";
  if (forward)
    ost << "0";
  else
    ost << "1";
  ost << "][tid];\n";
#if 0
  if (forward)
  {
    for (SpeciesCoeffMap::const_iterator it = reac->forward.begin();
          it != reac->forward.end(); it++)
    {
      if (!it->first->qss_species)
      {
        for (int j = 0; j < it->second; j++)
          ost << "rr_f_" << local_idx << " *= " << MOLE_FRAC << "[" << it->first->code_name << "][tid];\n";
      }
    }
    if ((reac->thb != NULL) && !reac->pressure_dep)
      ost << "rr_f_" << local_idx << " *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
  }
  else
  {
    if (reac->reversible)
    {
      for (SpeciesCoeffMap::const_iterator it = reac->backward.begin();
            it != reac->backward.end(); it++)
      {
        if (!it->first->qss_species)
        {
          for (int j = 0; j < it->second; j++)
            ost << "rr_r_" << local_idx << " *= " << MOLE_FRAC << "[" << it->first->code_name << "][tid];\n";
        }
      }
    }
    if ((reac->thb != NULL) && !reac->pressure_dep)
      ost << "rr_r_" << local_idx << " *= " << THIRD_BODY << "[" << reac->thb->idx << "];\n";
  }
#endif
}

void QSS::emit_multi_stores(CodeOutStream &ost, int avars_off, int denom_off, int write_off)
{
  ost << "avars[" << avars_off << "] /= denom[" << denom_off << "];\n";
  ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << QSSA_ARRAY << "+"
      << write_off << "*spec_stride), \"d\"(avars[" << avars_off << "]) : \"memory\");\n";
  avars_off++;
  std::set<int> others;
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin(); it != others.end(); it++)
  {
    ost << "avars[" << avars_off << "] /= denom[" << denom_off << "];\n";
    ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(" << QSSA_ARRAY << "+"
        << write_off << "*spec_stride), \"d\"(avars[" << avars_off << "]) : \"memory\");\n";
    avars_off++;
  }
}

int QSS::emit_last_scaled_updates(CodeOutStream &ost, int avars_off, int denom_off, int local_idx, 
                                    int reac_idx, Reaction *reac, int reverse_off, int num_loads)
{
  if (forward_zeros.find(reac_idx) != forward_zeros.end())
  {
    emit_last_scaled_declaration(ost, reac, local_idx, true, reverse_off);
    if (forward_zeros[reac_idx] == 1)
      ost << "avars[" << avars_off << "] += rr_f_" << local_idx << ";\n";
    else if (forward_zeros[reac_idx] == -1)
      ost << "avars[" << avars_off << "] -= rr_f_" << local_idx << ";\n";
    else
      ost << "avars[" << avars_off << "] = __fma_rn(" << double(forward_zeros[reac_idx]) 
          << ",rr_f_" << local_idx << ",avars[" << avars_off << "]);\n";
    num_loads++;
#ifdef LOAD_FENCE
    if ((num_loads%LOAD_FENCE)==0)
      ost << "__threadfence_block();\n";
#endif
  }
  if (backward_zeros.find(reac_idx) != backward_zeros.end())
  {
    emit_last_scaled_declaration(ost, reac, local_idx, false, reverse_off);
    if (backward_zeros[reac_idx] == 1)
      ost << "avars[" << avars_off << "] += rr_r_" << local_idx << ";\n";
    else if (backward_zeros[reac_idx] == -1)
      ost << "avars[" << avars_off << "] -= rr_r_" << local_idx << ";\n";
    else
      ost << "avars[" << avars_off << "] = __fma_rn(" << double(backward_zeros[reac_idx])
          << ",rr_r_" << local_idx << ",avars[" << avars_off << "]);\n";
    num_loads++;
#ifdef LOAD_FENCE
    if ((num_loads%LOAD_FENCE)==0)
      ost << "__threadfence_block();\n";
#endif
  }
  if (forward_denom.find(reac_idx) != forward_denom.end())
  {
    emit_last_scaled_declaration(ost, reac, local_idx, true, reverse_off);
    ost << "denom[" << denom_off << "] += rr_f_" << local_idx << ";\n";
    num_loads++;
#ifdef LOAD_FENCE
    if ((num_loads%LOAD_FENCE)==0)
      ost << "__threadfence_block();\n";
#endif
  }
  if (backward_denom.find(reac_idx) != backward_denom.end())
  {
    emit_last_scaled_declaration(ost, reac, local_idx, false, reverse_off);
    ost << "denom[" << denom_off << "] += rr_r_" << local_idx << ";\n";
    num_loads++;
#ifdef LOAD_FENCE
    if ((num_loads%LOAD_FENCE)==0)
      ost << "__threadfence_block();\n";
#endif
  }
  std::set<int> others;
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  unsigned other_off = avars_off+1; // already did the first a
  for (std::set<int>::const_iterator other_it = others.begin();
        other_it != others.end(); other_it++)
  {
    for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
          it != forward_contributions.end(); it++)
    {
      if (it->first.first != (*other_it))
        continue;
      if (it->first.second == reac_idx)
      {
        emit_last_scaled_declaration(ost, reac, local_idx, true, reverse_off);
        if (it->second == 1)
          ost << "avars[" << other_off << "] += rr_f_" << local_idx << ";\n";
        else if (it->second == -1)
          ost << "avars[" << other_off << "] -= rr_f_" << local_idx << ";\n";
        else
          ost << "avars[" << other_off << "] = __fma_rn(" << double(it->second)
              << ",rr_f_" << local_idx << ",avars[" << other_off << "]);\n";
        num_loads++;
#ifdef LOAD_FENCE
        if ((num_loads%LOAD_FENCE)==0)
          ost << "__threadfence_block();\n";
#endif
      }
    }
    for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
          it != backward_contributions.end(); it++)
    {
      if (it->first.first != (*other_it))
        continue;
      if (it->first.second == reac_idx)
      {
        emit_last_scaled_declaration(ost, reac, local_idx, false, reverse_off);
        if (it->second == 1)
          ost << "avars[" << other_off << "] += rr_r_" << local_idx << ";\n";
        else if (it->second == -1)
          ost << "avars[" << other_off << "] -= rr_r_" << local_idx << ";\n";
        else
          ost << "avars[" << other_off << "] = __fma_rn(" << double(it->second)
              << ",rr_r_" << local_idx << ",avars[" << other_off << "]);\n";
        num_loads++;
#ifdef LOAD_FENCE
        if ((num_loads%LOAD_FENCE)==0)
          ost << "__threadfence_block();\n";
#endif
      }
    }
    other_off++;
  }
  return num_loads;
}

void QSS::emit_last_scaled_declaration(CodeOutStream &ost, Reaction *reac, int local_idx, bool forward, int reverse_off)
{
  ost << REAL << " ";
  if (forward)
    ost << "rr_f_";
  else
    ost << "rr_r_";
  ost << local_idx << " = scratch[QSS_OFFSET+";
  if (!forward)
    ost << reverse_off << "+";
  ost << local_idx << "][tid];\n";
}

void QSS::emit_last_divide(CodeOutStream &ost, int avars_off, int denom_off)
{
  assert(forward_contributions.empty());
  assert(backward_contributions.empty());
  ost << "avars[" << avars_off << "] /= denom[" << denom_off << "];\n";
}

void QSS::emit_last_stores(CodeOutStream &ost, int avars_off, int denom_off, std::map<std::pair<int,int>,int> &amap)
{
  int next_loc = amap.size();
  ost << "// Saving a" << qss_idx << "_0\n";
  // Invert the denominator so we can do multiplies
  ost << "denom[" << denom_off << "] = 1.0/denom[" << denom_off << "];\n";
  ost << "scratch[AVAL_OFFSET+" << next_loc << "][tid] = avars[" << avars_off << "]*denom[" << denom_off << "];\n";
  amap[std::pair<int,int>(qss_idx,0)] = next_loc;
  next_loc++;
  avars_off++;
  std::set<int> others;
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator it = others.begin();
        it != others.end(); it++)
  {
    ost << "// Saving a" << qss_idx << "_" << (*it) << "\n";
    ost << "scratch[AVAL_OFFSET+" << next_loc << "][tid] = avars[" << avars_off << "]*denom[" << denom_off << "];\n";
    amap[std::pair<int,int>(qss_idx,(*it))] = next_loc;
    next_loc++;
    avars_off++;
  }
}

void QSS::emit_last_corrections(CodeOutStream &ost, unsigned local_idx, int reac_idx, std::set<int> &loaded_qss_values)
{
  bool needs_correction = false;
  if (forward_corrections.find(reac_idx) != forward_corrections.end())
    needs_correction = true;
  if (backward_corrections.find(reac_idx) != backward_corrections.end())
    needs_correction = true;
  if (!needs_correction)
    return;
  // Check to see if we need to issue the load
  if (loaded_qss_values.find(qss_idx) == loaded_qss_values.end())
  {
    ost << REAL << " xq_" << qss_idx << " = scratch[QSS_OFFSET+" << (qss_idx-1) << "][tid];\n";
    loaded_qss_values.insert(qss_idx);
  }
  if (forward_corrections.find(reac_idx) != forward_corrections.end())
    ost << "rr_f_" << local_idx << " *= xq_" << qss_idx << ";\n";
  if (backward_corrections.find(reac_idx) != backward_corrections.end())
    ost << "rr_r_" << local_idx << " *= xq_" << qss_idx << ";\n";
}

void QSS::partition_values(std::vector<std::vector<QSSValue*> > &accum_values, unsigned &next_warp)
{
  // Do the zero value
  {
    QSSValue *zeroval = new QSSValue(this, 0, unit);
    avals.push_back(zeroval);
    int warp_index = -1;
    unsigned num_shared_loads = 0;
    bool first = true;
    for (std::map<int,int>::const_iterator it = forward_zeros.begin();
          it != forward_zeros.end(); it++)
    {
      if (first)
      {
        warp_index = accum_values[next_warp].size();
        accum_values[next_warp].push_back(zeroval);
        first = false;
      }
      zeroval->add_forward_value(next_warp, warp_index, it->first, it->second);
      num_shared_loads++;
      if (num_shared_loads == MAX_QSSA_LOADS)
      {
        num_shared_loads = 0;
        first = true;
        warp_index = -1;
        next_warp++;
        if (next_warp == accum_values.size())
          next_warp = 0;
      }
    }
    for (std::map<int,int>::const_iterator it = backward_zeros.begin();
          it != backward_zeros.end(); it++)
    {
      if (first)
      {
        warp_index = accum_values[next_warp].size();
        accum_values[next_warp].push_back(zeroval);
        first = false;
      }
      zeroval->add_backward_value(next_warp, warp_index, it->first, it->second);
      num_shared_loads++;
      if (num_shared_loads == MAX_QSSA_LOADS)
      {
        num_shared_loads = 0;
        first = true;
        warp_index = -1;
        next_warp++;
        if (next_warp == accum_values.size())
          next_warp = 0;
      }
    }
  }
  next_warp++;
  if (next_warp == accum_values.size())
    next_warp = 0;
  // Do the denominator
  {
    denom = new QSSValue(this, -1, unit);
    int warp_index = -1;
    unsigned num_shared_loads = 0;
    bool first = true;
    for (std::set<int>::const_iterator it = forward_denom.begin();
          it != forward_denom.end(); it++)
    {
      if (first)
      {
        warp_index = accum_values[next_warp].size();
        accum_values[next_warp].push_back(denom);
        first = false;
      }
      denom->add_forward_value(next_warp, warp_index, *it, 1);
      num_shared_loads++;
      if (num_shared_loads == MAX_QSSA_LOADS)
      {
        num_shared_loads = 0;
        first = true;
        warp_index = -1;
        next_warp++;
        if (next_warp == accum_values.size())
          next_warp = 0;
      }
    }
    for (std::set<int>::const_iterator it = backward_denom.begin();
          it != backward_denom.end(); it++)
    {
      if (first)
      {
        warp_index = accum_values[next_warp].size();
        accum_values[next_warp].push_back(denom);
        first = false;
      }
      denom->add_backward_value(next_warp, warp_index, *it, 1);
      num_shared_loads++;
      if (num_shared_loads == MAX_QSSA_LOADS)
      {
        num_shared_loads = 0;
        first = true;
        warp_index = -1;
        next_warp++;
        if (next_warp == accum_values.size())
          next_warp = 0;
      }
    }
  }
  next_warp++;
  if (next_warp == accum_values.size())
    next_warp = 0;
  // Do all the rest of the values
  std::set<int> others;
  for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
        it != forward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
        it != backward_contributions.end(); it++)
  {
    others.insert(it->first.first);
  }
  for (std::set<int>::const_iterator other_it = others.begin();
        other_it != others.end(); other_it++)
  {
    QSSValue *aval = new QSSValue(this, *other_it, unit);
    avals.push_back(aval);
    int warp_index = -1;
    unsigned num_shared_loads = 0;
    bool first = true;
    for (std::map<std::pair<int,int>,int>::const_iterator it = forward_contributions.begin();
          it != forward_contributions.end(); it++)
    {
      if (it->first.first != (*other_it))
        continue;
      if (first)
      {
        warp_index = accum_values[next_warp].size();
        accum_values[next_warp].push_back(aval);
        first = false;
      }
      aval->add_forward_value(next_warp, warp_index, it->first.second, it->second);
      num_shared_loads++;
      if (num_shared_loads == MAX_QSSA_LOADS)
      {
        num_shared_loads = 0;
        first = true;
        warp_index = -1;
        next_warp++;
        if (next_warp == accum_values.size())
          next_warp = 0;
      }
    }
    for (std::map<std::pair<int,int>,int>::const_iterator it = backward_contributions.begin();
          it != backward_contributions.end(); it++)
    {
      if (it->first.first != (*other_it))
        continue;
      if (first)
      {
        warp_index = accum_values[next_warp].size();
        accum_values[next_warp].push_back(aval);
        first = false;
      }
      aval->add_backward_value(next_warp, warp_index, it->first.second, it->second);
      num_shared_loads++;
      if (num_shared_loads == MAX_QSSA_LOADS)
      {
        num_shared_loads = 0;
        first = true;
        warp_index = -1;
        next_warp++;
        if (next_warp == accum_values.size())
          next_warp = 0;
      }
    }
    next_warp++;
    if (next_warp == accum_values.size())
      next_warp = 0;
  }
}

void QSS::emit_qssvalue_loads(CodeOutStream &ost)
{
  for (std::vector<QSSValue*>::const_iterator it = avals.begin();
        it != avals.end(); it++)
  {
    (*it)->emit_load(ost);
  }
  denom->emit_load(ost);
}

void QSS::emit_qssvalue_operations(CodeOutStream &ost)
{
  for (std::vector<QSSValue*>::const_iterator it = avals.begin();
        it != avals.end(); it++)
  {
    (*it)->emit_operation(ost);
  }
}

void QSS::emit_qssvalue_stores(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap)
{
  for (std::vector<QSSValue*>::const_iterator it = avals.begin();
        it != avals.end(); it++)
  {
    (*it)->emit_store(ost, amap);
  }
}

/////////////////////////////
//
// QSSValue 
//
/////////////////////////////

QSSValue::QSSValue(QSS *own, int other, TranslationUnit *u)
  : owner(own), other_idx(other), unit(u) { }

void QSSValue::add_forward_value(unsigned warp, int warp_idx, int reac_idx, int coeff)
{
  assert(warp >= 0);
  assert(warp_idx >= 0);
  if (warp_locations.find(warp) != warp_locations.end())
    assert(warp_locations[warp] == warp_idx); // if not wrap-around, really bad increase max share loads
  else
  {
    forward_values[warp] = std::map<int,int>();
    backward_values[warp] = std::map<int,int>();
  }
  warp_locations[warp] = warp_idx;
  forward_values[warp][reac_idx] = coeff;
}

void QSSValue::add_backward_value(unsigned warp, int warp_idx, int reac_idx, int coeff)
{
  assert(warp >= 0);
  assert(warp_idx >= 0);
  if (warp_locations.find(warp) != warp_locations.end())
    assert(warp_locations[warp] == warp_idx); // if not wrap-around, really bad increase max share loads
  else
  {
    forward_values[warp] = std::map<int,int>();
    backward_values[warp] = std::map<int,int>();
  }
  warp_locations[warp] = warp_idx;
  backward_values[warp][reac_idx] = coeff;
}

void QSSValue::emit_accum_updates(CodeOutStream &ost, unsigned widx, int reac_idx, bool &forward_loaded, bool &backward_loaded, 
                                  bool local, int scratch_idx, int stage_idx, int reverse_offset)
{
  assert(warp_locations.find(widx) != warp_locations.end());
  assert(forward_values.find(widx) != forward_values.end());
  assert(backward_values.find(widx) != backward_values.end());
  bool needs_forward = (forward_values[widx].find(reac_idx) != forward_values[widx].end());
  bool needs_backward = (backward_values[widx].find(reac_idx) != backward_values[widx].end());
  if (needs_forward)
  {
    // See if this reaction is already loaded
    if (!forward_loaded && !local)
    {
      ost << REAL << " forward_" << reac_idx << " = scratch[NUM_SPECIES+" << scratch_idx << "][tid];\n";
      forward_loaded = true;
    }
    ost << "accum_values[" << warp_locations[widx] << "]";
    int coeff = forward_values[widx][reac_idx];
    if (coeff == 1)
    {
      if (!local)
        ost << " += forward_" << reac_idx << ";\n";
      else
        ost << " += rr_f_" << stage_idx << ";\n";
    }
    else if (coeff == -1)
    {
      if (!local)
        ost << " -= forward_" << reac_idx << ";\n";
      else
        ost << " -= rr_f_" << stage_idx << ";\n";
    }
    else
    {
      ost << " = __fma_rn(" << double(coeff) << ",";
      if (!local)
        ost << "forward_" << reac_idx;
      else
        ost << "rr_f_" << stage_idx; 
      ost << ", accum_values[" << warp_locations[widx] << "]);\n";
    }
  }
  if (needs_backward)
  {
    // See if this reaction is already loaded
    if (!backward_loaded && !local)
    {
      ost << REAL << " backward_" << reac_idx << " = scratch[NUM_SPECIES+" << (reverse_offset+scratch_idx) << "][tid];\n";
      backward_loaded = true;
    }
    ost << "accum_values[" << warp_locations[widx] << "]";
    int coeff = backward_values[widx][reac_idx];
    if (coeff == 1)
    {
      if (!local)
        ost << " += backward_" << reac_idx << ";\n";
      else
        ost << " += rr_r_" << stage_idx << ";\n";
    }
    else if (coeff == -1)
    {
      if (!local)
        ost << " -= backward_" << reac_idx << ";\n";
      else
        ost << " -= rr_r_" << stage_idx << ";\n";
    }
    else
    {
      ost << " = __fma_rn(" << double(coeff) << ",";
      if (!local)
        ost << "backward_" << reac_idx;
      else
        ost << "rr_r_" << stage_idx; 
      ost << ", accum_values[" << warp_locations[widx] << "]);\n";
    }
  }
}

void QSSValue::emit_load(CodeOutStream &ost)
{
  if (other_idx == -1)
    ost << REAL << " den_" << owner->qss_idx << " = ";
  else
    ost << REAL << " a" << owner->qss_idx << "_" << other_idx << " = ";
  bool first = true;
  for (std::map<int,int>::const_iterator it = warp_locations.begin();
        it != warp_locations.end(); it++)
  {
    int scratch_location = it->second * unit->threads_per_point + it->first;
    assert(scratch_location < (192 - int(unit->species.size()-1)));
    if (!first)
      ost << " + ";
    ost << "scratch[NUM_SPECIES+" << scratch_location << "][tid]";
    first = false;
  }
  if (first)
    ost << "0.0";
  ost << ";\n";
}

void QSSValue::emit_operation(CodeOutStream &ost)
{
  ost << "a" << owner->qss_idx << "_" << other_idx << " /= den_" << owner->qss_idx << ";\n";
}

void QSSValue::emit_store(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap)
{
  int scratch_location = amap.size(); 
  std::pair<int,int> key(owner->qss_idx,other_idx);
  assert(amap.find(key) == amap.end());
  amap[key] = scratch_location;
  assert(scratch_location < (192 - int(unit->species.size()-1)));
  ost << "// Storing a" << owner->qss_idx << "_" << other_idx << "\n";
  ost << "scratch[NUM_SPECIES+NUM_QSS+" << scratch_location << "][tid] = a"
      << owner->qss_idx << "_" << other_idx << ";\n";
}

/////////////////////////////
//
// Stif 
//
/////////////////////////////

Stif::Stif(const char *n, int kp, int k2p, TranslationUnit *u)
  : species(NULL), name(n), k(kp), k2(k2p), unit(u)
{
  
}

void Stif::add_forward_d(int reac_idx, int coeff)
{
  assert(coeff != 0);
  assert(forward_d.find(reac_idx) == forward_d.end());
  forward_d[reac_idx] = coeff;
  add_fread(reac_idx);
  add_fwrite(reac_idx);
}

void Stif::add_forward_c(int reac_idx, int coeff)
{
  assert(coeff != 0);
  assert(forward_c.find(reac_idx) == forward_c.end());
  forward_c[reac_idx] = coeff;
  add_fread(reac_idx);
  add_fwrite(reac_idx);
}

void Stif::add_backward_d(int reac_idx, int coeff)
{
  assert(coeff != 0);
  assert(backward_d.find(reac_idx) == backward_d.end());
  backward_d[reac_idx] = coeff;
  add_bread(reac_idx);
  add_bwrite(reac_idx);
}

void Stif::add_backward_c(int reac_idx, int coeff)
{
  assert(coeff != 0);
  assert(backward_c.find(reac_idx) == backward_c.end());
  backward_c[reac_idx] = coeff;
  add_bread(reac_idx);
  add_bwrite(reac_idx);
}

void Stif::compute_allocations(std::vector<Allocation*> &needed_allocations,
                               const std::vector<Warp*> &warps, int reaction_granularity)
{
  // Do the D allocations first
  {
    int target_warp = -1;
    int remaining_allocations = reaction_granularity;
    std::map<int,Allocation*> warp_allocations;
    help_compute_allocations(needed_allocations,warp_allocations,d_allocations,
                        forward_d,warps,target_warp,remaining_allocations,true/*forwards*/,reaction_granularity);
    help_compute_allocations(needed_allocations,warp_allocations,d_allocations,
                        backward_d,warps,target_warp,remaining_allocations,false/*forwards*/,reaction_granularity);
  }
  // Then do the C allocations
  {
    int target_warp = -1;
    int remaining_allocations = reaction_granularity;
    std::map<int,Allocation*> warp_allocations;
    help_compute_allocations(needed_allocations,warp_allocations,c_allocations,
                        forward_c,warps,target_warp,remaining_allocations,true/*forwards*/,reaction_granularity);
    help_compute_allocations(needed_allocations,warp_allocations,c_allocations,
                        backward_c,warps,target_warp,remaining_allocations,false/*forwards*/,reaction_granularity);
  }
}

void Stif::clear_allocations(void)
{
  d_allocations.clear();
  c_allocations.clear();
}

bool Stif::updates_warp(Warp *warp) const
{
  for (std::map<int,int>::const_iterator it = forward_d.begin();
        it != forward_d.end(); it++)
  {
    int index = warp->get_reaction_index(it->first);
    if (index != -1)
      return true;
  }
  for (std::map<int,int>::const_iterator it = backward_d.begin();
        it != backward_d.end(); it++)
  {
    int index = warp->get_reaction_index(it->first);
    if (index != -1)
      return true;
  }
  return false;
}

void Stif::emit_updates(CodeOutStream &ost, Warp *warp, int phase_id, int scaling_offset)
{
  //ost << "if (__any(phase_" << phase_id << "_factor[" << scaling_offset << "] != 1.0))\n";
  //PairDelim if_pair(ost);
  for (std::map<int,int>::const_iterator it = forward_d.begin();
        it != forward_d.end(); it++)
  {
    int index = warp->get_reaction_index(it->first);
    int coeff = it->second;
    if (index != -1)
    {
      for (int idx = 0; idx < coeff; idx++)
        ost << "rr_f[" << index << "] *= phase_" << phase_id << "_factor[" << scaling_offset << "];\n";
    }
  }
  for (std::map<int,int>::const_iterator it = backward_d.begin();
        it != backward_d.end(); it++)
  {
    int index = warp->get_reaction_index(it->first);
    int coeff = it->second;
    if (index != -1)
    {
      for (int idx = 0; idx < coeff; idx++)
        ost << "rr_r[" << index << "] *= phase_" << phase_id << "_factor[" << scaling_offset << "];\n";
    }
  }
}

void Stif::emit_scaling_read(CodeOutStream &ost, Warp *warp, int phase_id, int src_offset, int dst_offset)
{
  ost << "phase_" << phase_id << "_factor[" << dst_offset << "] = scratch[" << src_offset << "][tid];\n";
}

void Stif::emit_statements(CodeOutStream &ost, const char *target, int output_offset, int num_species, bool stage)
{
  assert(species != NULL);
  ost << "// Stiff species " << name << "\n";
  PairDelim stif_pair(ost);
  // First compute the destruction rate for this species
  if (!stage)
  {
    ost << REAL << " ddot = ";
    bool first = true;
    for (std::vector<Allocation*>::const_iterator it = d_allocations.begin();
          it != d_allocations.end(); it++)
    {
      assert((*it)->location != -1);
      if (!first)
        ost << " + ";
      if (stage)
        ost << "scratch_" << (*it)->stage << "_" << (*it)->location;
      else
        ost << "scratch[" << (*it)->location << "][tid]";
      first = false;
    }
    ost << ";\n";
  }
  // Emit the condition
  ost << REAL << " local_mole_frac = " << MOLE_FRAC << "[" << species->code_name << "][tid];\n";
  if (!stage)
    ost << "if ((ddot * recip_dt) > local_mole_frac)\n";
  else
    ost << "if ((ddot_" << k << " * recip_dt) > local_mole_frac)\n";
  {
    PairDelim if_pair(ost);
    // Emit the load for the diffusion value
    ost << REAL << " " << DIFFUSION << ";\n";
    {
      // Do some fancy math here to prevent the compiler from hoisting integer math and eating registers
      char src_offset[128];
      sprintf(src_offset,"((%s+step*%d)%%%d)*spec_stride",species->code_name,
          next_largest_power(2*num_species,2),next_largest_power(2*num_species,2));
      emit_cuda_load(ost,DIFFUSION,DIFFUSION_ARRAY,src_offset,false/*double2*/,".cg");
    }
    // Compute the creation rate
    if (!stage)
    {
      ost << REAL << " cdot = ";
      bool first = true;
      for (std::vector<Allocation*>::const_iterator it = c_allocations.begin();
            it != c_allocations.end(); it++)
      {
        assert((*it)->location != -1);
        if (!first)
          ost << " + ";
        if (stage)
          ost << "scratch_" << (*it)->stage << "_" << (*it)->location;
        else
          ost << "scratch[" << (*it)->location << "][tid]";
        first = false;
      }
      ost << ";\n";
    }
    if (!stage)
      ost << REAL << " recip_ddot = 1.0/ddot;\n";
    else
      ost << REAL << " recip_ddot = 1.0/ddot_" << k << ";\n";
    if (!stage)
      ost << REAL << " part_sum = cdot + " << DIFFUSION << "*recip_molecular_mass[" << species->code_name << "];\n";
    else
      ost << REAL << " part_sum = cdot_" << k << " + " << DIFFUSION << "*recip_molecular_mass[" << species->code_name << "];\n";
    ost << REAL << " c0 = local_mole_frac * part_sum * recip_ddot;\n";
    ost << "c0 = local_mole_frac * (part_sum + (local_mole_frac - c0) * recip_dt) * recip_ddot;\n";
    if (stage)
      ost << target << "[" << output_offset << "] = c0/local_mole_frac;\n";
    else
      ost << "scratch[" << output_offset << "][tid] = c0/local_mole_frac;\n";
  }
  ost << "else\n";
  {
    PairDelim else_pair(ost);
    if (stage)
      ost << target << "[" << output_offset << "] = 1.0;\n";
    else
      ost << "scratch[" << output_offset << "][tid] = 1.0;\n";
  }
}

void Stif::emit_cpp_statements(CodeOutStream &ost, TranslationUnit *unit)
{
  assert(species != NULL);
  ost << "// Stiff species " << name << "\n";
  PairDelim stif_pair(ost);
  // First compute the destruction rate for this species
  ost << REAL << " ddot = ";
  bool first = true;
  for (std::map<int,int>::const_iterator it = forward_d.begin();
        it != forward_d.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      if (!first)
        ost << " + ";
      ost << "forward_r[" << (it->first-1) << "]";
      first = false;
    }
  }
  for (std::map<int,int>::const_iterator it = backward_d.begin();
        it != backward_d.end(); it++)
  {
    for (int i = 0; i < it->second; i++)
    {
      if (!first)
        ost << " + ";
      ost << "reverse_r[" << (it->first-1) << "]";
      first = false;
    }
  }
  if (first)
    ost << "0.0";
  ost << ";\n";
  ost << "if ((ddot * dt) > cspl[" << species->code_name << "])\n";
  {
    PairDelim if_pair(ost);
    ost << REAL << " cdot = ";
    first = true;
    for (std::map<int,int>::const_iterator it = forward_c.begin();
          it != forward_c.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        if (!first)
          ost << " + ";
        ost << "forward_r[" << (it->first-1) << "]";
        first = false;
      }
    }
    for (std::map<int,int>::const_iterator it = backward_c.begin();
          it != backward_c.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        if (!first)
          ost << " + ";
        ost << "reverse_r[" << (it->first-1) << "]";
        first = false;
      }
    }
    if (first)
      ost << "0.0";
    ost << ";\n";
    ost << REAL << " recip_ddot = 1.0/ddot;\n";
    if (!unit->no_nondim)
      ost << REAL << " part_sum = cdot + " << DIFFUSION << "[" << species->code_name << "] * " << (DIFF_REF * (1.0/species->molecular_mass)) << ";\n";
    else
      ost << REAL << " part_sum = cdot + " << DIFFUSION << "[" << species->code_name << "] * " << (1.0/species->molecular_mass) << ";\n";
    ost << REAL << " c0 = cspl[" << species->code_name << "] * part_sum * recip_ddot;\n";
    ost << "c0 = cspl[" << species->code_name << "] * (part_sum + (cspl[" << species->code_name << "]- c0) / dt) * recip_ddot;\n";
    ost << REAL << " scale_r = c0/cspl[" << species->code_name << "];\n";
    // appy the updates to all the destruction rates
    for (std::map<int,int>::const_iterator it = forward_d.begin();
          it != forward_d.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        ost << "forward_r[" << (it->first-1) << "] *= scale_r;\n";
      }
    }
    for (std::map<int,int>::const_iterator it = backward_d.begin();
          it != backward_d.end(); it++)
    {
      for (int i = 0; i < it->second; i++)
      {
        ost << "reverse_r[" << (it->first-1) << "] *= scale_r;\n";
      }
    }
  }
}

void Stif::emit_stage_loads(CodeOutStream &ost, const std::map<Warp*,unsigned> &stage, bool first)
{
  if (first)
  {
    ost << REAL << " ddot_" << k << " = 0.0, cdot_" << k << " = 0.0;\n";
  }

  bool first_add = true;
  for (std::vector<Allocation*>::const_iterator it = d_allocations.begin();
        it != d_allocations.end(); it++)
  {
    bool in_stage = false;
    for (std::map<Warp*,unsigned>::const_iterator sit = stage.begin();
          sit != stage.end(); sit++)
    {
      if ((*it)->warp_id == sit->first->wid)
      {
        in_stage = true;
        break;
      }
    }
    if (!in_stage)
      continue;
    if (first_add)
      ost << "ddot_" << k << " = ddot_" << k;
    ost << " + scratch[" << (*it)->location << "][tid]";
    first_add = false;
  }
  if (!first_add)
    ost << ";\n";

  first_add = true;
  for (std::vector<Allocation*>::const_iterator it = c_allocations.begin();
        it != c_allocations.end(); it++)
  {
    bool in_stage = false;
    for (std::map<Warp*,unsigned>::const_iterator sit = stage.begin();
          sit != stage.end(); sit++)
    {
      if ((*it)->warp_id == sit->first->wid)
      {
        in_stage = true;
        break;
      }
    }
    if (!in_stage)
      continue;
    if (first_add)
      ost << "cdot_" << k << " = cdot_" << k;
    ost << " + scratch[" << (*it)->location << "][tid]";
    first_add = false;
  }
  if (!first_add)
    ost << ";\n";
}

void Stif::emit_pipe_updates(CodeOutStream &ost, int stif_idx, unsigned local_idx, int reac_idx)
{
  if (forward_d.find(reac_idx) != forward_d.end())
  {
    if (forward_d[reac_idx] == 1)
      ost << "ddot[" << stif_idx << "] += rr_f[" << local_idx << "];\n";
    else if (forward_d[reac_idx] == -1)
      ost << "ddot[" << stif_idx << "] -= rr_f[" << local_idx << "];\n";
    else
      ost << "ddot[" << stif_idx << "] = __fma_rn(" << double(forward_d[reac_idx])
          << ",rr_f[" << local_idx << "],ddot[" << stif_idx << "]);\n";
  }
  if (forward_c.find(reac_idx) != forward_c.end())
  {
    if (forward_c[reac_idx] == 1)
      ost << "cdot[" << stif_idx << "] += rr_f[" << local_idx << "];\n";
    else if (forward_c[reac_idx] == -1)
      ost << "cdot[" << stif_idx << "] -= rr_f[" << local_idx << "];\n";
    else
      ost << "cdot[" << stif_idx << "] = __fma_rn(" << double(forward_c[reac_idx])
          << ",rr_f[" << local_idx << "],cdot[" << stif_idx << "]);\n";
  }
  if (backward_d.find(reac_idx) != backward_d.end())
  {
    if (backward_d[reac_idx] == 1)
      ost << "ddot[" << stif_idx << "] += rr_r[" << local_idx << "];\n";
    else if (backward_d[reac_idx] == -1)
      ost << "ddot[" << stif_idx << "] -= rr_r[" << local_idx << "];\n";
    else
      ost << "ddot[" << stif_idx << "] = __fma_rn(" << double(backward_d[reac_idx])
          << ",rr_r[" << local_idx << "],ddot[" << stif_idx << "]);\n";
  }
  if (backward_c.find(reac_idx) != backward_c.end())
  {
    if (backward_c[reac_idx] == 1)
      ost << "cdot[" << stif_idx << "] += rr_r[" << local_idx << "];\n";
    else if (backward_c[reac_idx] == -1)
      ost << "cdot[" << stif_idx << "] -= rr_r[" << local_idx << "];\n";
    else
      ost << "cdot[" << stif_idx << "] = __fma_rn(" << double(backward_c[reac_idx])
          << ",rr_r[" << local_idx << "],cdot[" << stif_idx << "]);\n";
  }
}

void Stif::emit_pipe_statements(CodeOutStream &ost, int stif_idx, TranslationUnit *unit)
{
  ost << "if ((ddot[" << stif_idx << "] * dt) > " << MOLE_FRAC << ")\n"; 
  {
    PairDelim if_pair(ost);
    ost << REAL << " recip_ddot = 1.0/ddot[" << stif_idx << "];\n";
    if (!unit->no_nondim)
      ost << REAL << " part_sum = cdot[" << stif_idx << "] + stiffness[" 
          << stif_idx << "] * " << (DIFF_REF * species->molecular_mass) << ";\n";
    else
      ost << REAL << " part_sum = cdot[" << stif_idx << "] + stiffness[" 
          << stif_idx << "] * molecular_mass[" << species->code_name << "];\n";
    ost << REAL << " c0 = " << MOLE_FRAC << " * part_sum * recip_ddot;\n";
    ost << "c0 = " << MOLE_FRAC << " * (part_sum + (" << MOLE_FRAC << " - c0) * recip_dt) * recip_ddot;\n";
    ost << "stiffness[" << stif_idx << "] = c0/" << MOLE_FRAC << ";\n";
  }
  ost << "else\n";
  {
    PairDelim else_pair(ost);
    ost << "stiffness[" << stif_idx << "] = 1.0;\n";
  }
}

void Stif::emit_pipe_corrections(CodeOutStream &ost, int stif_idx, unsigned local_idx, int reac_idx)
{
  if (forward_d.find(reac_idx) != forward_d.end())
  {
    for (int j = 0; j < forward_d[reac_idx]; j++)
      ost << "rr_f[" << local_idx << "] *= stiffness[" << stif_idx << "];\n";
  }
  if (backward_d.find(reac_idx) != backward_d.end())
  {
    for (int j = 0; j < backward_d[reac_idx]; j++)
      ost << "rr_r[" << local_idx << "] *= stiffness[" << stif_idx << "];\n";
  }
}

void Stif::emit_last_updates(CodeOutStream &ost, int stif_idx, unsigned local_idx, int scratch_loc, 
                              int reac_idx, int reverse_offset, std::set<int> &emitted_forward, std::set<int> &emitted_backward)
{
  bool needs_forward = ((forward_d.find(reac_idx) != forward_d.end()) || (forward_c.find(reac_idx) != forward_c.end()));
  bool needs_backward = ((backward_c.find(reac_idx) != backward_c.end()) || (backward_d.find(reac_idx) != backward_d.end()));
  if (needs_forward)
  {
    if (emitted_forward.find(reac_idx) == emitted_forward.end())
    {
      ost << REAL << " forward_" << reac_idx << " = scratch[" << scratch_loc << "][tid];\n";
      emitted_forward.insert(reac_idx);
    }
    for (std::map<int,int>::const_iterator it = forward_d.begin();
          it != forward_d.end(); it++)
    {
      if (it->first != reac_idx)
        continue;
      if (it->second == 1)
        ost << "ddot[" << local_idx << "] += forward_" << reac_idx << ";\n";
      else if (it->second == -1)
        ost << "ddot[" << local_idx << "] -= forward_" << reac_idx << ";\n";
      else
        ost << "ddot[" << local_idx << "] = __fma_rn(" << double(it->second) << ",forward_" << reac_idx
            << ",ddot[" << local_idx << "]);\n";
    }
    for (std::map<int,int>::const_iterator it = forward_c.begin();
          it != forward_c.end(); it++)
    {
      if (it->first != reac_idx)
        continue;
      if (it->second == 1)
        ost << "cdot[" << local_idx << "] += forward_" << reac_idx << ";\n";
      else if (it->second == -1)
        ost << "cdot[" << local_idx << "] -= forward_" << reac_idx << ";\n";
      else
        ost << "cdot[" << local_idx << "] = __fma_rn(" << double(it->second) << ",forward_" << reac_idx
            << ",cdot[" << local_idx << "]);\n";
    }
  }
  if (needs_backward)
  {
    if (emitted_backward.find(reac_idx) == emitted_backward.end())
    {
      ost << REAL << " backward_" << reac_idx << " = scratch[" << (scratch_loc+reverse_offset) << "][tid];\n";
      emitted_backward.insert(reac_idx);
    }
    for (std::map<int,int>::const_iterator it = backward_d.begin();
          it != backward_d.end(); it++)
    {
      if (it->first != reac_idx)
        continue;
      if (it->second == 1)
        ost << "ddot[" << local_idx << "] += backward_" << reac_idx << ";\n";
      else if (it->second == -1)
        ost << "ddot[" << local_idx << "] -= backward_" << reac_idx << ";\n";
      else
        ost << "ddot[" << local_idx << "] = __fma_rn(" << double(it->second) << ",backward_" << reac_idx
            << ",ddot[" << local_idx << "]);\n";
    }
    for (std::map<int,int>::const_iterator it = backward_c.begin();
          it != backward_c.end(); it++)
    {
      if (it->first != reac_idx)
        continue;
      if (it->second == 1)
        ost << "cdot[" << local_idx << "] += backward_" << reac_idx << ";\n";
      else if (it->second == -1)
        ost << "cdot[" << local_idx << "] -= backward_" << reac_idx << ";\n";
      else
        ost << "cdot[" << local_idx << "] = __fma_rn(" << double(it->second) << ",backward_" << reac_idx
            << ",cdot[" << local_idx << "]);\n";
    }
  }
}

void Stif::emit_last_corrections(CodeOutStream &ost, int stif_idx, int local_idx, int reac_idx, std::set<int> &loaded_stif_values)
{
  bool needs_update = (forward_d.find(reac_idx) != forward_d.end()) || (backward_d.find(reac_idx) != backward_d.end());
  if (needs_update)
  {
    if (loaded_stif_values.find(stif_idx) == loaded_stif_values.end())
    {
      ost << REAL << " scale_r_" << stif_idx << " = scratch[" << stif_idx << "][tid];\n";
      loaded_stif_values.insert(stif_idx);
    }
    if (forward_d.find(reac_idx) != forward_d.end())
    {
      for (int j = 0; j < forward_d[reac_idx]; j++)
        ost << "rr_f_" << local_idx << " *= scale_r_" << stif_idx << ";\n";
    }
    if (backward_d.find(reac_idx) != backward_d.end())
    {
      for (int j = 0; j < backward_d[reac_idx]; j++)
        ost << "rr_r_" << local_idx << " *= scale_r_" << stif_idx << ";\n";
    }
  }
}

void Stif::emit_stif_value_loads(CodeOutStream &ost, int local_idx)
{
  destruction_value->emit_load(ost, local_idx);
  construction_value->emit_load(ost, local_idx);
}

void Stif::partition_values(std::vector<std::vector<StifValue*> > &accum_values, unsigned &next_warp)
{
  // Do the destruction value
  {
    destruction_value = new StifValue(this, true, unit);
    int warp_index = -1;
    unsigned num_shared_loads = 0;
    bool first = true;
    for (std::map<int,int>::const_iterator it = forward_d.begin();
          it != forward_d.end(); it++)
    {
      if (first)
      {
        warp_index = accum_values[next_warp].size();
        accum_values[next_warp].push_back(destruction_value);
        first = false;
      }
      destruction_value->add_forward_value(next_warp, warp_index, it->first, it->second);
      num_shared_loads++;
      if (num_shared_loads == MAX_STIF_LOADS)
      {
        num_shared_loads = 0;
        first = true;
        warp_index = -1;
        next_warp++;
        if (next_warp == accum_values.size())
          next_warp = 0;
      }
    }
    for (std::map<int,int>::const_iterator it = backward_d.begin();
          it != backward_d.end(); it++)
    {
      if (first)
      {
        warp_index = accum_values[next_warp].size();
        accum_values[next_warp].push_back(destruction_value);
        first = false;
      }
      destruction_value->add_backward_value(next_warp, warp_index, it->first, it->second);
      num_shared_loads++;
      if (num_shared_loads == MAX_STIF_LOADS)
      {
        num_shared_loads = 0;
        first = true;
        warp_index = -1;
        next_warp++;
        if (next_warp == accum_values.size())
          next_warp = 0;
      }
    }
  }
  next_warp++;
  if (next_warp == accum_values.size())
    next_warp = 0;
  // Now do the construction value
  {
    construction_value = new StifValue(this, false, unit);
    int warp_index = -1;
    unsigned num_shared_loads = 0;
    bool first = true;
    for (std::map<int,int>::const_iterator it = forward_c.begin();
          it != forward_c.end(); it++)
    {
      if (first)
      {
        warp_index = accum_values[next_warp].size();
        accum_values[next_warp].push_back(construction_value);
        first = false;
      }
      construction_value->add_forward_value(next_warp, warp_index, it->first, it->second);
      num_shared_loads++;
      if (num_shared_loads == MAX_STIF_LOADS)
      {
        num_shared_loads = 0;
        first = true;
        warp_index = -1;
        next_warp++;
        if (next_warp == accum_values.size())
          next_warp = 0;
      }
    }
    for (std::map<int,int>::const_iterator it = backward_c.begin();
          it != backward_c.end(); it++)
    {
      if (first)
      {
        warp_index = accum_values[next_warp].size();
        accum_values[next_warp].push_back(construction_value);
        first = false;
      }
      construction_value->add_backward_value(next_warp, warp_index, it->first, it->second);
      num_shared_loads++;
      if (num_shared_loads == MAX_STIF_LOADS)
      {
        num_shared_loads = 0;
        first = true;
        warp_index = -1;
        next_warp++;
        if (next_warp == accum_values.size())
          next_warp = 0;
      }
    }
  }
  next_warp++;
  if (next_warp == accum_values.size())
    next_warp = 0;
}

/////////////////////////////
//
// StifValue 
//
/////////////////////////////

StifValue::StifValue(Stif *own, bool dest, TranslationUnit *u)
  : owner(own), destruction(dest), unit(u)
{
}

void StifValue::add_forward_value(unsigned warp, int warp_idx, int reac_idx, int coeff)
{
  assert(warp >= 0);
  assert(warp_idx >= 0);
  if (warp_locations.find(warp) != warp_locations.end())
    assert(warp_locations[warp] == warp_idx); // if not wrap-around, really bad increase max share loads
  else
  {
    forward_values[warp] = std::map<int,int>();
    backward_values[warp] = std::map<int,int>();
  }
  warp_locations[warp] = warp_idx;
  forward_values[warp][reac_idx] = coeff;
}

void StifValue::add_backward_value(unsigned warp, int warp_idx, int reac_idx, int coeff)
{
  assert(warp >= 0);
  assert(warp_idx >= 0);
  if (warp_locations.find(warp) != warp_locations.end())
    assert(warp_locations[warp] == warp_idx); // if not wrap-around, really bad increase max share loads
  else
  {
    forward_values[warp] = std::map<int,int>();
    backward_values[warp] = std::map<int,int>();
  }
  warp_locations[warp] = warp_idx;
  backward_values[warp][reac_idx] = coeff;
}

void StifValue::emit_accum_updates(CodeOutStream &ost, unsigned widx, int reac_idx, bool &forward_loaded, bool &backward_loaded, 
                          bool local, int scratch_idx, int stage_idx, int reverse_offset)
{
  assert(warp_locations.find(widx) != warp_locations.end());
  assert(forward_values.find(widx) != forward_values.end());
  assert(backward_values.find(widx) != backward_values.end());
  bool needs_forward = (forward_values[widx].find(reac_idx) != forward_values[widx].end());
  bool needs_backward = (backward_values[widx].find(reac_idx) != backward_values[widx].end());
  if (needs_forward)
  {
    // See if this reaction is already loaded
    if (!forward_loaded && !local)
    {
      ost << REAL << " forward_" << reac_idx << " = scratch[" << scratch_idx << "][tid];\n";
      forward_loaded = true;
    }
    ost << "accum_values[" << warp_locations[widx] << "]";
    int coeff = forward_values[widx][reac_idx];
    if (coeff == 1)
    {
      if (!local)
        ost << " += forward_" << reac_idx << ";\n";
      else
        ost << " += rr_f_" << stage_idx << ";\n";
    }
    else if (coeff == -1)
    {
      if (!local)
        ost << " -= forward_" << reac_idx << ";\n";
      else
        ost << " -= rr_f_" << stage_idx << ";\n";
    }
    else
    {
      ost << " = __fma_rn(" << double(coeff) << ",";
      if (!local)
        ost << "forward_" << reac_idx;
      else
        ost << "rr_f_" << stage_idx; 
      ost << ", accum_values[" << warp_locations[widx] << "]);\n";
    }
  }
  if (needs_backward)
  {
    // See if this reaction is already loaded
    if (!backward_loaded && !local)
    {
      ost << REAL << " backward_" << reac_idx << " = scratch[" << (reverse_offset+scratch_idx) << "][tid];\n";
      backward_loaded = true;
    }
    ost << "accum_values[" << warp_locations[widx] << "]";
    int coeff = backward_values[widx][reac_idx];
    if (coeff == 1)
    {
      if (!local)
        ost << " += backward_" << reac_idx << ";\n";
      else
        ost << " += rr_r_" << stage_idx << ";\n";
    }
    else if (coeff == -1)
    {
      if (!local)
        ost << " -= backward_" << reac_idx << ";\n";
      else
        ost << " -= rr_r_" << stage_idx << ";\n";
    }
    else
    {
      ost << " = __fma_rn(" << double(coeff) << ",";
      if (!local)
        ost << "backward_" << reac_idx;
      else
        ost << "rr_r_" << stage_idx; 
      ost << ", accum_values[" << warp_locations[widx] << "]);\n";
    }
  }
}

void StifValue::emit_load(CodeOutStream &ost, unsigned local_idx)
{
  if (destruction)
    ost << "ddot[" << local_idx << "]";
  else
    ost << "cdot[" << local_idx << "]";
  ost << " = ";
  bool first = true;
  for (std::map<int,int>::const_iterator it = warp_locations.begin();
        it != warp_locations.end(); it++)
  {
    int scratch_location = it->second * unit->threads_per_point + it->first;
    assert(scratch_location < 192);
    if (!first)
      ost << " + ";
    ost << "scratch[" << scratch_location << "][tid]";
    first = false;
  }
  if (first)
    ost << "0.0";
  ost << ";\n";
}

/////////////////////////////
//
// Warp 
//
/////////////////////////////

Warp::Warp(int id)
  : wid(id) { }

bool Warp::has_reaction(int reac_idx, int pass /*= -1*/) const
{
  if (reaction_indexes.find(reac_idx) != reaction_indexes.end())
  {
    if (pass != -1)
    {
      std::map<int,int>::const_iterator finder = pass_mask.find(reac_idx);
      assert(finder != pass_mask.end());
      if (pass == finder->second)
        return true;
      else
        return false;
    }
    else
      return true;
  }
  return false;
}

bool Warp::has_index(int index, int pass /*= -1*/) const
{
  if (index >= int(reaction_order.size()))
    return false;
  if (pass != -1)
  {
    std::map<int,int>::const_iterator finder = pass_mask.find(reaction_order[index]);
    assert(finder != pass_mask.end());
    if (finder->second == pass)
      return true;
    return false;
  }
  else
    return true;
}

void Warp::add_reaction(int reac_idx, int pass /*= -1*/)
{
  assert(reaction_indexes.find(reac_idx) == reaction_indexes.end());
  assert(pass_mask.find(reac_idx) == pass_mask.end());
  reaction_indexes.insert(reac_idx);
  reaction_order.push_back(reac_idx);
  pass_mask[reac_idx] = pass;
}

size_t Warp::num_reactions(int pass /*= -1*/) const
{
  if (pass == -1)
    return reaction_indexes.size();
  else
  {
    size_t num_reactions = 0;
    for (std::vector<int>::const_iterator it = reaction_order.begin();
          it != reaction_order.end(); it++)
    {
      std::map<int,int>::const_iterator finder = pass_mask.find(*it);
      assert(finder != pass_mask.end());
      if (finder->second == pass)
        num_reactions++;
    }
    return num_reactions;
  }
}

void Warp::set_reaction(int reac_idx, Reaction *reac, int pass /*= -1*/)
{
  assert(reaction_indexes.find(reac_idx) != reaction_indexes.end());
  assert(pass_mask.find(reac_idx) != pass_mask.end());
  reactions[reac_idx] = reac;
}

int Warp::get_reaction_index(int reac_idx, int pass /*= -1*/)
{
  for (int idx = 0; idx < int(reaction_order.size()); idx++)
  {
    if (reaction_order[idx] == reac_idx)
    {
      if (pass != -1)
      {
        if (pass_mask[reac_idx] == pass)  
          return idx;
      }
      else
        return idx;
    }
  }
  return -1;
}

int Warp::get_reaction_idx(int index, int pass /*= -1*/)
{
  assert(index >= 0);
  if (index >= int(reaction_order.size()))
    return -1;
  if (pass != -1)
  {
    if (pass_mask[reaction_order[index]] != pass)
      return -1;
  }
  return reaction_order[index];
}

Reaction* Warp::get_reaction(int reac_idx, int pass /*= -1*/)
{
  if (reactions.find(reac_idx) != reactions.end())
  {
    if (pass != -1)
    {
      if (pass_mask[reac_idx] == pass)
        return reactions[reac_idx];
      else
        return NULL;
    }
    else
      return reactions[reac_idx];
  }
  return NULL;
}

void Warp::sort_weird_reactions(int start_idx, int stop_idx)
{
  std::set<int> weird_reactions;
  std::set<int> normal_reactions;
  unsigned total_reactions = 0;
  for (int idx = start_idx; idx <= stop_idx; idx++)
  {
    if (idx < int(reaction_order.size()))
    {
      Reaction *reac = get_reaction(reaction_order[idx]);
      assert(reac != NULL);
      bool weird = is_weird_reaction(reac);
      if (weird)
        weird_reactions.insert(reaction_order[idx]);
      else
        normal_reactions.insert(reaction_order[idx]);
      total_reactions++;
    }
  }
  assert(total_reactions == (weird_reactions.size() + normal_reactions.size()));
  // Now reinsert the reactions with the weird reactions first
  // and the normal ones later
  int insert_idx = start_idx;
  for (std::set<int>::const_iterator it = weird_reactions.begin();
        it != weird_reactions.end(); it++)
  {
    assert(insert_idx <= stop_idx);
    reaction_order[insert_idx] = *it;
    insert_idx++;
  }
  for (std::set<int>::const_iterator it = normal_reactions.begin();
        it != normal_reactions.end(); it++)
  {
    assert(insert_idx <= stop_idx);
    reaction_order[insert_idx] = *it;
    insert_idx++;
  }
}

void Warp::compute_needed_xqs(const std::vector<ConnectedComponent*> &ccs, 
                              std::vector<int> &needed_xqs)
{
  for (std::vector<ConnectedComponent*>::const_iterator cc = ccs.begin();
        cc != ccs.end(); cc++)
  {
    for (std::map<int,QSS*>::const_iterator it = (*cc)->species.begin();
          it != (*cc)->species.end(); it++)
    {
      if (it->second->updates_warp(this))
      {
        needed_xqs.push_back(it->second->qss_idx);
      }
    }
  }
}

bool Warp::can_add_constants(unsigned reac_idx, unsigned cnt, bool second_pass)
{
  if (!second_pass)
  {
    if (reac_idx < reaction_constants.size())
      return ((reaction_constants[reac_idx].size()+cnt) <= MAX_REACTION_CONSTANTS);
    return true;
  }
  else
  {
    if (reac_idx < second_pass_constants.size())
      return ((second_pass_constants[reac_idx].size()+cnt) <= MAX_REACTION_CONSTANTS);
    return true;
  }
}

int Warp::add_constant(unsigned reac_idx, double constant, bool second_pass)
{
  if (!second_pass)
  {
    if (reac_idx >= reaction_constants.size())
      reaction_constants.resize(reac_idx+1);
    int offset = reaction_constants[reac_idx].size();
    assert(offset < MAX_REACTION_CONSTANTS);
    reaction_constants[reac_idx].push_back(constant);
    return offset;
  }
  else
  {
    if (reac_idx >= second_pass_constants.size())
      second_pass_constants.resize(reac_idx+1);
    int offset = second_pass_constants[reac_idx].size();
    assert(offset < MAX_REACTION_CONSTANTS);
    second_pass_constants[reac_idx].push_back(constant);
    return offset;
  }
}

unsigned Warp::emit_constants(CodeOutStream &ost, bool first)
{
  for (std::vector<std::vector<double> >::const_iterator it = reaction_constants.begin();
        it != reaction_constants.end(); it++)
  {
    const std::vector<double> &constants = *it;
    unsigned num_emitted = 0;
    for (std::vector<double>::const_iterator cit = constants.begin();
          cit != constants.end(); cit++)
    {
      if (!first)
        ost << ", ";
      ost << *cit;
      num_emitted++;
      first = false;
    }
    // Pad anything else out to zero
    for ( ; num_emitted < MAX_REACTION_CONSTANTS; num_emitted++)
    {
      if (!first)
        ost << ", ";
      ost << "0.0";
      first = false;
    }
  }
  return (reaction_constants.size() * MAX_REACTION_CONSTANTS);
}

void Warp::emit_num_constants(CodeOutStream &ost, bool first, int offset, int num_to_emit)
{
  while (num_to_emit > 0)
  {
    int reac_location = offset/MAX_REACTION_CONSTANTS;
    int reac_index = offset%MAX_REACTION_CONSTANTS;
    if (reac_location >= int(reaction_constants.size()))
    {
      if (!first)
        ost << ", ";
      ost << "0.0";
    }
    else
    {
      if (reac_index >= int(reaction_constants[reac_location].size()))
      {
        if (!first)
          ost << ", ";
        ost << "0.0";
      }
      else
      {
        if (!first)
          ost << ", ";
        ost << reaction_constants[reac_location][reac_index];
      }
    }
    offset++;
    num_to_emit--;
    first = false;
  }
}

void Warp::add_troe_reaction(int stage, int index, bool first)
{
  if (first)
  {
    while (stage > int(troe_reactions.size()))
      troe_reactions.push_back(0);
    if (stage == int(troe_reactions.size()))
      troe_reactions.push_back(index);
    else
      troe_reactions[stage] = index;
  }
  else
  {
    while (stage > int(second_pass_troe.size()))
      second_pass_troe.push_back(0);
    if (stage == int(second_pass_troe.size()))
      second_pass_troe.push_back(index);
    else
      second_pass_troe[stage] = index;
  }
}

void Warp::add_gibbs_species(int location, int index, double coefficient, bool first)
{
  if (first)
  {
    while (location > int(gibbs_species.size()))
      gibbs_species.push_back(std::pair<int,double>(0,0.0));
    if (location == int(gibbs_species.size()))
      gibbs_species.push_back(std::pair<int,double>(index,coefficient));
    else
      gibbs_species[location] = std::pair<int,double>(index,coefficient);
  }
  else
  {
    while (location > int(second_gibbs.size()))
      second_gibbs.push_back(std::pair<int,double>(0,0.0));
    if (location == int(gibbs_species.size()))
      gibbs_species.push_back(std::pair<int,double>(index,coefficient));
    else
      gibbs_species[location] = std::pair<int,double>(index,coefficient);
  }
}

void Warp::add_reverse_constant(int stage, double coefficient, bool first)
{
  if (first)
  {
    while (stage > int(reverse_constants.size()))
      reverse_constants.push_back(0.0);
    if (stage == int(reverse_constants.size()))
      reverse_constants.push_back(coefficient);
    else
      reverse_constants[stage] = coefficient;
  }
  else
  {
    while (stage > int(second_reverse.size()))
      second_reverse.push_back(0.0);
    if (stage == int(second_reverse.size()))
      second_reverse.push_back(coefficient);
    else
      second_reverse[stage] = coefficient;
  }
}

void Warp::add_stiff_species(int stage, int stif_idx, Stif *stif, bool first)
{
  if (first)
  {
    while (stage > int(stif_species.size()))
      stif_species.push_back(NULL);
    if (stage == int(stif_species.size()))
      stif_species.push_back(stif);
    else
      stif_species[stage] = stif;
  }
  else
  {
    while (stage > int(second_stif.size()))
      second_stif.push_back(NULL);
    if (stage == int(second_stif.size()))
      second_stif.push_back(stif);
    else
      second_stif[stage] = stif;
  }
}

void Warp::emit_troe_index(CodeOutStream &ost, int stage)
{
  if (wid > 0)
    ost << ", ";
  if (stage >= int(troe_reactions.size()))
    ost << "0";
  else
    ost << troe_reactions[stage];
}

void Warp::emit_gibbs_index(CodeOutStream &ost, int stage)
{
  if (wid > 0)
    ost << ", ";
  if (stage >= int(gibbs_species.size()))
    ost << "0";
  else
    ost << gibbs_species[stage].first;
}

void Warp::emit_gibbs_scale(CodeOutStream &ost, int stage)
{
  if (wid > 0)
    ost << ", ";
  if (stage >= int(gibbs_species.size()))
    ost << "0.0";
  else
    ost << gibbs_species[stage].second;
}

void Warp::emit_reverse_constant(CodeOutStream &ost, int stage)
{
  if (wid > 0)
    ost << ", ";
  if (stage >= int(reverse_constants.size()))
    ost << "0.0";
  else
    ost << reverse_constants[stage];
}

void Warp::emit_stiff_index(CodeOutStream &ost, int stage)
{
  if (wid > 0)
    ost << ", ";
  if (stage >= int(stif_species.size()))
    ost << "0";
  else if (stif_species[stage] == NULL)
    ost << "0";
  else
    ost << stif_species[stage]->species->code_name;
}

/////////////////////////////
//
// Phase 
//
/////////////////////////////

Phase::Phase(int id, int available_values)
  : phase_id(id), store(new SharedStore(available_values)) { }

bool Phase::add_connected_component(ConnectedComponent *cc, const std::vector<Warp*> &warps, int reaction_granularity)
{
  std::vector<Allocation*> needed_allocations; 
  cc->compute_allocations(needed_allocations, warps, reaction_granularity);
  if (store->has_space(needed_allocations))
  {
    store->commit_allocations(needed_allocations);
    components.push_back(cc);
    return true;
  }
  else
  {
    // Free up the memory for the allocations and return false
    for (std::vector<Allocation*>::const_iterator it = needed_allocations.begin();
          it != needed_allocations.end(); it++)
    {
      delete *it;
    }
    needed_allocations.clear();
    cc->clear_allocations();
    return false;
  }
}

int Phase::can_add_stif(Stif *op, const std::vector<Warp*> &warps, int reaction_granularity)
{
  std::vector<Allocation*> needed_allocations;
  op->compute_allocations(needed_allocations, warps, reaction_granularity);
  int result = -1;
  if (store->has_space(needed_allocations))
    result = needed_allocations.size();
  for (std::vector<Allocation*>::const_iterator it = needed_allocations.begin();
        it != needed_allocations.end(); it++)
  {
    delete *it;
  }
  needed_allocations.clear();
  op->clear_allocations();
  return result;
}

void Phase::add_stif(Stif *op, const std::vector<Warp*> &warps, int reaction_granularity)
{
  std::vector<Allocation*> needed_allocations;
  op->compute_allocations(needed_allocations, warps, reaction_granularity);
  assert(store->has_space(needed_allocations));
  store->commit_allocations(needed_allocations);
  stifs.push_back(op);
}

bool Phase::empty(void) const
{
  return (components.empty() && stifs.empty());
}

void Phase::finalize(const std::vector<Warp*> &warps)
{
  // Go through and find the largest number of reactions
  // that any of the warps has at this point, which represents
  // the total number of reactions that have to be done before
  // this phase can be performed
  max_reaction = 0;
  for (std::vector<Warp*>::const_iterator it = warps.begin();
        it != warps.end(); it++)
  {
    if ((*it)->num_reactions() > max_reaction)
      max_reaction = (*it)->num_reactions();
  }
  assert(max_reaction > 0);
}

void Phase::sort_into_stages(unsigned num_stages, unsigned max_values_per_stage,
                              const std::vector<Warp*> &warps)
{
  // For each warp first count how many allocations it needs
  std::map<Warp*,unsigned> needed_allocations;
  for (std::vector<Warp*>::const_iterator wit = warps.begin();
        wit != warps.end(); wit++)
  {
    needed_allocations[*wit] = 0;
    for (std::vector<Allocation*>::const_iterator it = store->allocations.begin();
          it != store->allocations.end(); it++)
    {
      if ((*it)->warp_id == ((*wit)->wid))
        needed_allocations[*wit]++;
    }
    //fprintf(stdout,"    Warp %d needs %d allocation\n",(*wit)->wid,needed_allocations[*wit]);
  }
  stages.resize(num_stages); 
  fprintf(stdout,"  INFO: Breaking phase %d into %d stages\n",phase_id,num_stages);
  for (unsigned idx = 0; idx < num_stages; idx++)
  {
    unsigned total_allocations = 0;
    // Keep putting warps into this stage until no more fit
    while (true)
    {
      Warp *next_warp = NULL;
      int max_allocations = -1;
      for (std::map<Warp*,unsigned>::const_iterator it = needed_allocations.begin();
            it != needed_allocations.end(); it++)
      {
        if ((total_allocations + it->second) <= max_values_per_stage)
        {
          if (int(it->second) > max_allocations)
          {
            next_warp = it->first;
            max_allocations = it->second;
          }
        }
      }
      // Break out when nothing more fits or we've used all our allocations
      if (next_warp == NULL)
        break;
      stages[idx][next_warp] = max_allocations;
      needed_allocations.erase(next_warp);
      total_allocations += max_allocations;
    }
    fprintf(stdout,"    Stage %d uses %d allocations for %ld warps:",idx,total_allocations,stages[idx].size());
    for (std::map<Warp*,unsigned>::const_iterator it = stages[idx].begin();
          it != stages[idx].end(); it++)
    {
      fprintf(stdout," %d", it->first->wid);
    }
    fprintf(stdout,"\n");
  }
  if (!needed_allocations.empty())
  {
    fprintf(stderr,"  FAILURE TO BREAK INTO STAGES.  TUNE THE HEURISTIC FOR CREATING STAGES\n");
    exit(1);
  }
  // Now reassign the locations for each of the stages
  for (unsigned idx = 0; idx < num_stages; idx++)
  {
    if (stages[idx].empty())
      continue;
    unsigned next_location = 0;
    for (std::vector<Allocation*>::const_iterator it = store->allocations.begin();
          it != store->allocations.end(); it++)
    {
      bool found = false;
      for (std::map<Warp*,unsigned>::const_iterator wit = stages[idx].begin();
            wit != stages[idx].end(); wit++)
      {
        if (wit->first->wid == (*it)->warp_id)
        {
          found = true;
          break;
        }
      }
      if (found)
      {
        assert(next_location < max_values_per_stage);
        (*it)->location = next_location;
        next_location++;
        (*it)->stage = idx;
      }
    }
  }
}

int Phase::find_warp_stage(Warp *warp, int &stage_idx)
{
  stage_idx = -1; 
  int stage_warps = 0;
  for (unsigned idx = 0; idx < stages.size(); idx++)
  {
    if (stages[idx].find(warp) != stages[idx].end())
    {
      stage_idx = idx;
      stage_warps = stages[idx].size();
      break;
    }
  }
  // Better have found it
  assert(stage_idx >= 0);
  assert(stage_warps > 0);
  return stage_warps;
}

void Phase::emit_write_store_values(CodeOutStream &ost, Warp *warp)
{
  bool need_warp = false;
  // Go through all the allocations and see if any of them are
  // associated with this warp
  for (std::vector<Allocation*>::const_iterator alloc_it = store->allocations.begin();
        alloc_it != store->allocations.end(); alloc_it++)
  {
    if ((*alloc_it)->warp_id == warp->wid)
    {
      need_warp = true;
      break;
    }
  }
  if (!need_warp)
    return;
  for (std::vector<Allocation*>::const_iterator alloc_it = store->allocations.begin();
        alloc_it != store->allocations.end(); alloc_it++)
  {
    if ((*alloc_it)->warp_id == warp->wid)
    {
      (*alloc_it)->emit_store_allocation(ost, warp); 
    }
  }
}

void Phase::emit_read_scaling_factors(CodeOutStream &ost, Warp *warp)
{
  assert(components.empty() || stifs.empty()); 
  if (!components.empty())
  {
    // Count how many QSS scaling factors there are here to read out
    int num_scaling_factors = 0;
    for (std::vector<ConnectedComponent*>::const_iterator cc = components.begin();
          cc != components.end(); cc++)
    {
      num_scaling_factors += (*cc)->updates_warp(warp);
    }
    if (num_scaling_factors > 0)
    {
      ost << REAL << " phase_" << phase_id << "_factor[" << num_scaling_factors << "];\n";
      int src_offset = 0;
      int dst_offset = 0;
      for (std::vector<ConnectedComponent*>::const_iterator cc = components.begin();
            cc != components.end(); cc++)
      {
        (*cc)->emit_scaling_reads(ost,warp,phase_id,src_offset,dst_offset); 
      }
      assert(dst_offset == num_scaling_factors);
    }
  }
  else
  {
    int num_scaling_factors = 0;
    for (std::vector<Stif*>::const_iterator it = stifs.begin();
          it != stifs.end(); it++)
    {
      if ((*it)->updates_warp(warp))
        num_scaling_factors++;
    }
    if (num_scaling_factors > 0)
    {
      ost << REAL << " phase_" << phase_id << "_factor[" << num_scaling_factors << "];\n";
      int src_offset = 0;
      int dst_offset = 0;
      for (std::vector<Stif*>::const_iterator it = stifs.begin();
            it != stifs.end(); it++)
      {
        if ((*it)->updates_warp(warp))
        {
          (*it)->emit_scaling_read(ost, warp, phase_id, src_offset, dst_offset);
          dst_offset++;
        }
        src_offset++;
      }
      assert(dst_offset == num_scaling_factors);
    }
  }
}

void Phase::emit_apply_scaling_factors(CodeOutStream &ost, Warp *warp)
{
  assert(components.empty() || stifs.empty());
  if (!components.empty())
  {
    bool need_warp = false;
    for (std::vector<ConnectedComponent*>::const_iterator cc = components.begin();
          cc != components.end(); cc++)
    {
      if ((*cc)->updates_warp(warp))
      {
        need_warp = true;
        break;
      }
    }
    if (need_warp)
    {
      int scaling_offset = 0;
      for (std::vector<ConnectedComponent*>::const_iterator cc = components.begin();
            cc != components.end(); cc++)
      {
        scaling_offset = (*cc)->emit_updates(ost,warp,phase_id,scaling_offset);
      }
    }
  }
  else
  {
    bool need_warp = false;
    for (std::vector<Stif*>::const_iterator st = stifs.begin();
          st != stifs.end(); st++)
    {
      if ((*st)->updates_warp(warp))
      {
        need_warp = true;
        break;
      }
    }
    if (need_warp)
    {
      int scaling_offset = 0;
      for (std::vector<Stif*>::const_iterator st = stifs.begin();
            st != stifs.end(); st++)
      {
        if ((*st)->updates_warp(warp))
        {
          (*st)->emit_updates(ost, warp, phase_id, scaling_offset);
          scaling_offset++;
        }
      }
    }
  }
}

void Phase::emit_apply_qssa_factors(CodeOutStream &ost, Warp *warp)
{
  assert(!components.empty());
  bool need_warp = false;
  for (std::vector<ConnectedComponent*>::const_iterator cc = components.begin();
        cc != components.end(); cc++)
  {
    if ((*cc)->updates_warp(warp))
    {
      need_warp = true;
      break;
    }
  }
  if (need_warp)
  {
    int scaling_offset = 0;
    for (std::vector<ConnectedComponent*>::const_iterator cc = components.begin();
          cc != components.end(); cc++)
    {
      scaling_offset = (*cc)->emit_qssa_updates(ost,warp,scaling_offset);
    }
  }
}

void Phase::emit_operations(CodeOutStream &ost, int num_species)
{
  assert(components.empty() || stifs.empty());
  ost << "// Phase " << phase_id << "\n";
  PairDelim phase_pair(ost);
  if (!components.empty())
  {
    unsigned output_offset = 0;
    for (std::vector<ConnectedComponent*>::const_iterator it = components.begin();
          it != components.end(); it++)
    {
      output_offset = (*it)->emit_statements(ost, output_offset);
    }
  }
  else
  {
    unsigned output_offset = 0;
    for (std::vector<Stif*>::const_iterator it = stifs.begin();
          it != stifs.end(); it++)
    {
      (*it)->emit_statements(ost, "scratch", output_offset, num_species, false/*stage*/);
      output_offset++;
    }
  }
}

unsigned Phase::emit_staged_operations(CodeOutStream &ost, int num_species, const char *target, unsigned target_offset)
{
  assert(components.empty() || stifs.empty());
  ost << "// Phase " << phase_id << "\n";
  PairDelim phase_pair(ost);
  if (!components.empty())
  {
    // for each stage, emit the code for loading values
    for (unsigned idx = 0; idx < stages.size(); idx++)
    {
      int stage_warps = stages[idx].size();
      int start_barrier = (idx+1)*2;
      int finish_barrier = (idx+1)*2+1;
      // Mark that we are ready for the data to be sent
      ost << "asm volatile(\"bar.arrive " << start_barrier << "," << ((stage_warps+1)*32) << ";\" : : : \"memory\");\n";
      // Wait for the data to be ready
      ost << "asm volatile(\"bar.sync " << finish_barrier << "," << ((stage_warps+1)*32) << ";\" : : : \"memory\");\n";
      for (std::vector<ConnectedComponent*>::const_iterator it = components.begin();
            it != components.end(); it++)
      {
        (*it)->emit_stage_loads(ost, stages[idx], (idx==0));
      }
    }
    // Now emit the statements for computing the values
    for (std::vector<ConnectedComponent*>::const_iterator it = components.begin();
          it != components.end(); it++)
    {
      target_offset = (*it)->emit_stage_statements(ost, target, target_offset);
    }
  }
  else
  {
    // for each stage, emit the code for loading values
    for (unsigned idx = 0; idx < stages.size(); idx++)
    {
      int stage_warps = stages[idx].size();
      int start_barrier = (idx+1)*2;
      int finish_barrier = (idx+1)*2+1;
      // Mark that we are ready for the data to be sent
      ost << "asm volatile(\"bar.arrive " << start_barrier << "," << ((stage_warps+1)*32) << ";\" : : : \"memory\");\n";
      // Wait for the data to be ready
      ost << "asm volatile(\"bar.sync " << finish_barrier << "," << ((stage_warps+1)*32) << ";\" : : : \"memory\");\n";
      for (std::vector<Stif*>::const_iterator it = stifs.begin();
            it != stifs.end(); it++)
      {
        (*it)->emit_stage_loads(ost, stages[idx], (idx==0));
      }
    }
    // Now emit the statements for computing the values
    for (std::vector<Stif*>::const_iterator it = stifs.begin();
          it != stifs.end(); it++)
    {
      (*it)->emit_statements(ost, target, target_offset, num_species, true/*stage*/);
      target_offset++;
    }
  }
  return target_offset;
}

/////////////////////////////
//
// Allocation 
//
/////////////////////////////

Allocation::Allocation(int wid)
  : warp_id(wid), location(-1), stage(-1) { }

void Allocation::add_forward_expression(int reac_idx, int coeff)
{
  assert(forward_expressions.find(reac_idx) == forward_expressions.end());
  forward_expressions[reac_idx] = coeff;
}

void Allocation::add_backward_expression(int reac_idx, int coeff)
{
  assert(backward_expressions.find(reac_idx) == backward_expressions.end());
  backward_expressions[reac_idx] = coeff;
}

void Allocation::set_location(int loc)
{
  // Should never have been set before
  assert(location == -1);
  location = loc;
}

void Allocation::emit_store_allocation(CodeOutStream &ost, Warp *warp)
{
  // Emit this allocation
  assert(location != -1);
  assert(!forward_expressions.empty() || !backward_expressions.empty());
  ost << "scratch[" << location << "][tid] = ";
  bool first = true;
  for (std::map<int,int>::const_iterator it = forward_expressions.begin();
        it != forward_expressions.end(); it++)
  {
    int register_loc = warp->get_reaction_index(it->first);
    assert(register_loc != -1);
    assert(it->second != 0);
    if (it->second == 1)
    {
#ifdef RR_INDEXING
      if (first)
        ost << "rr_f[" << register_loc << "]";
      else
        ost << " + rr_f[" << register_loc << "]";
#else
      if (first)
        ost << "rr_f_" << register_loc;
      else
        ost << " + rr_f_" << register_loc;
#endif
    }
    else if (it->second == -1)
    {
#ifdef RR_INDEXING
      if (first)
        ost << "-rr_f[" << register_loc << "]";
      else
        ost << " - rr_f[" << register_loc << "]";
#else
      if (first)
        ost << "-rr_f_" << register_loc;
      else
        ost << " - rr_f_" << register_loc;
#endif
    }
    else
    {
#ifdef RR_INDEXING
      if (first)
        ost << it->second << "*rr_f[" << register_loc << "]";
      else
        ost << " + " << it->second << "*rr_f[" << register_loc << "]";
#else
      if (first)
        ost << it->second << "*rr_f_" << register_loc;
      else
        ost << " + " << it->second << "*rr_f_" << register_loc;
#endif
    }
    first = false;
  }
  for (std::map<int,int>::const_iterator it = backward_expressions.begin();
        it != backward_expressions.end(); it++)
  {
    int register_loc = warp->get_reaction_index(it->first);
    assert(register_loc != -1);
    assert(it->second != 0);
    if (it->second == 1)
    {
#ifdef RR_INDEXING
      if (first)
        ost << "rr_r[" << register_loc << "]";
      else
        ost << " + rr_r[" << register_loc << "]";
#else
      if (first)
        ost << "rr_r_" << register_loc;
      else
        ost << " + rr_r_" << register_loc;
#endif
    }
    else if (it->second == -1)
    {
#ifdef RR_INDEXING
      if (first)
        ost << "-rr_r[" << register_loc << "]";
      else
        ost << " - rr_r[" << register_loc << "]";
#else
      if (first)
        ost << "-rr_r_" << register_loc;
      else
        ost << " - rr_r_" << register_loc;
#endif
    }
    else
    {
#ifdef RR_INDEXING
      if (first)
        ost << it->second << "*rr_r[" << register_loc << "]";
      else
        ost << " + " << it->second << "*rr_r[" << register_loc << "]";
#else
      if (first)
        ost << it->second << "*rr_r_" << register_loc;
      else
        ost << " + " << it->second << "*rr_r_" << register_loc;
#endif
    }
    first = false;
  }
  assert(!first);
  ost << ";\n";
}

/////////////////////////////
//
// Shared Store 
//
/////////////////////////////

SharedStore::SharedStore(int max_values)
  : available_values(max_values) { }

bool SharedStore::has_space(const std::vector<Allocation*> &needed_allocations) const
{
  if (int(allocations.size()+needed_allocations.size()) < available_values)
    return true;
  return false;
}

void SharedStore::commit_allocations(const std::vector<Allocation*> &needed_allocations)
{
  for (std::vector<Allocation*>::const_iterator it = needed_allocations.begin();
        it != needed_allocations.end(); it++)
  {
    int location = allocations.size();
    (*it)->set_location(location);
    allocations.push_back(*it);
  }
}

// EOF

