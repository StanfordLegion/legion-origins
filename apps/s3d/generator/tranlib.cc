
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>

#include "s3dgen_ast.h"
#include "code_gen.h"

#define VOID  "void"
#define REAL  "double"
#define INT   "int"
#define REAL2 "double2"
#define VECTOR2 "__m128d"
#define VECTOR4 "__m256d"
#define REAL_TYPE double // change this if you change REAL or REAL2

#define PRESSURE  "pressure"
#define TEMPERATURE "temperature"
#define MOLE_FRAC "mole_frac"
#define PRESSURE_ARRAY "pressure_array"
#define TEMPERATURE_ARRAY "temperature_array"
#define MOLE_FRAC_ARRAY "mole_frac_array"
#define SCRATCH "scratch"

// Defined as constant in tranlib
#define SMALL 1.0e-20

// Maximum amount of shared memory
// This version correct for both Fermi and Kepler
#define MAX_SHARED  49152
#define MAX_WARPS_PER_SM 64 // Kepler
//#define MAX_WARPS_PER_SM 48 // Fermi

// Do shuffle operations in conductivity kernel
//#define CONDUCTIVITY_SHUFFLE
// Switch to input and output arrays
#define SINGLE_ARRAYS
// Enable or disable generation of code for LDGs on Kepler
#define ENABLE_LDG
// Switch between math transform on warp-specialization for viscosity
#define VISC_MATH
// Turn on warp-specialization (warning i-cache thrashing!)
//#define WARP_SPECIALIZATION
// Control the buffering depth for constant loading
// Deeper is better at hiding latency, but uses more registers
// Only matters if things don't fit in the constant cache
#define CONSTANT_BUFFERING  1 
// Decide whether to cache mole fractions in registers or to
// reload them from shared memory.
#define RELOAD_DIFFUSION_FRACS
// Number of named barriers used in diffusion.  Only change
// this if you really understand what is going on.
#define NAMED_BARRIERS  4
// Have warps handle contiguous groups of species
#define CONTIGUOUS_SPECIES
// Switch between doing diffusion vertically versus across
#define DIFFUSION_ACROSS
// Buffering for diffusion
#define DIFFUSION_BUFFERING 1
// Use shuffles on Kepler
#define DIFFUSION_SHUFFLE
// Emit threadfences every so many species to stop the
// compiler from hoisting loads and oversubscribing registers
// in the thermal kernel
#define THERMAL_FENCES  -1 
// For doing large number of viscosity species on kepler
#define LARGE_VISC_KEPLER
// For doing large number of diffusion species on Kepler
#define LARGE_DIFF_KEPLER


#define MIN(a,b)  (((a) < (b)) ? (a) : (b))

static double factorial(unsigned num_stages)
{
  double result = 1.0;
  for (unsigned i = 2; i <= num_stages; i++)
    result *= double(i);
  return result;
}

static int next_largest_power(int value, int power)
{
  assert(value > 0);
  int result = 1;
  while (value > result)
  {
    result *= power;
  }
  return result;
}

// emit tunable implementation of exp function
static void emit_gpu_exp_taylor_series_expansion(CodeOutStream &ost, const char *var_name, unsigned num_stages, bool use_double2)
{
  ost << "// Taylor series expansion for exp(" << var_name << ") using " << num_stages << " stages\n";
  PairDelim taylor_pair(ost);
  if (use_double2)
  {
    // If we're doing this for double2, interleave operations to get more ILP
    // DAMN YOU CUDA COMPILER THAT I HAVE TO DO THIS MYSELF!
    ost << REAL << " scale1 = rint(1.4426950408889634e+0*" << var_name << ".x);\n";
    ost << REAL << " scale2 = rint(1.4426950408889634e+0*" << var_name << ".y);\n";
    ost << INT << " i1 = (int)scale1;\n";
    ost << INT << " i2 = (int)scale2;\n";
    ost << REAL2 << " remainder = make_double2( "
        << "__fma_rn(scale1,-6.9314718055994529e-1," << var_name << ".x), "
        << "__fma_rn(scale2,-6.9314718055994529e-1," << var_name << ".y));\n";
    ost << "remainder = make_double2( "
        << "__fma_rn(scale1,-2.3190468138462996e-17,remainder.x), "
        << "__fma_rn(scale2,-2.3190468138462996e-17,remainder.y));\n";
    for (unsigned stage = num_stages-1; stage >= 1; stage--)
    {
      ost << "// Stage " << stage << "\n";
      if (stage == (num_stages-1))
        ost << var_name << " = make_double2( "
            << "__fma_rn(" << (1.0/factorial(num_stages)) << ",remainder.x," << (1.0/factorial(stage)) << "), "
            << "__fma_rn(" << (1.0/factorial(num_stages)) << ",remainder.y," << (1.0/factorial(stage)) << "));\n";
      else
        ost << var_name << " = make_double2( "
            << "__fma_rn(" << var_name << ".x,remainder.x," << (1.0/factorial(stage)) << "), "
            << "__fma_rn(" << var_name << ".y,remainder.y," << (1.0/factorial(stage)) << "));\n";
    }
    ost << var_name << " = make_double2( "
        << "__fma_rn(" << var_name << ".x,remainder.x,1.0), "
        << "__fma_rn(" << var_name << ".y,remainder.y,1.0));\n";
    ost << INT << " k1 = (i1 + 1023) << 20;\n";
    ost << INT << " k2 = (i2 + 1023) << 20;\n";
    ost << var_name << " = make_double2( "
        << var_name << ".x * __hiloint2double(k1,0), "
        << var_name << ".y * __hiloint2double(k2,0));\n";
  }
  else
  {
    ost << REAL << " scale = rint(1.4426950408889634e+0*" << var_name << ");\n";
    ost << INT << " i = (int)scale;\n";
    ost << REAL << " remainder = __fma_rn(scale,-6.9314718055994529e-1," << var_name << ");\n";
    ost << "remainder = __fma_rn(scale,-2.3190468138462996e-17,remainder);\n";
    for (unsigned stage = num_stages-1; stage >= 1; stage--)
    {
      ost << "// Stage " << stage << "\n";
      if (stage == (num_stages-1))
        ost << var_name << " = __fma_rn(" << (1.0/factorial(num_stages)) << ",remainder," << (1.0/factorial(stage)) << ");\n";
      else
        ost << var_name << " = __fma_rn(" << var_name << ",remainder," << (1.0/factorial(stage)) << ");\n";
    }
    ost << var_name << " = __fma_rn(" << var_name << ",remainder,1.0);\n";
    ost << INT << " k = (i + 1023) << 20;\n";
    ost << var_name << " *= __hiloint2double(k, 0);\n";
  }
}

// Implementation of tranlib functions

TranLib::TranLib(int ord, int num_spec, int nli, double pat, TranslationUnit *u)
  : order(ord), num_species(num_spec), nlite(nli), patmos(pat), unit(u) { }

void TranLib::emit_code(const char *file_prefix)
{
  char header_name[128];
  char source_name[128];
  snprintf(header_name,127,"%s.h",file_prefix);
  printf("Writing coefficient header file %s...\n",header_name);
  emit_cpp_header_file(header_name);
  snprintf(source_name,127,"%s.cc",file_prefix);
  printf("Writing coefficient source file %s...\n",source_name);
  emit_cpp_source_file(source_name, header_name);
}

void TranLib::emit_cuda(const char *file_name, bool emit_experiment)
{
  char source_name[128];
  snprintf(source_name,127,"%s.cu",file_name);
  printf("Writing CUDA coefficients source file %s...\n",source_name);
  emit_cuda_source_file(source_name, emit_experiment);
}

void TranLib::emit_cpp_header_file(const char *file_name)
{
  CodeOutStream ost(file_name, true/*bound line length*/, 80);
  ost << "\n";
  ost << "#ifndef __LEGION_GET_COEFFS__\n";
  ost << "#define __LEGION_GET_COEFFS__\n";
  ost << "\n";

  emit_base_conductivity_declaration(ost);
  ost << ";\n";
  emit_base_viscosity_declaration(ost);
  ost << ";\n";
  emit_base_diffusion_declaration(ost);
  ost << ";\n";
  emit_base_thermal_declaration(ost);
  ost << ";\n";

  ost << "\n";
  ost << "#endif // __LEGION_GET_COEFFS__\n";
  ost << "\n";
}

void TranLib::emit_cpp_source_file(const char *file_name, const char *header_name)
{
  CodeOutStream ost(file_name, true/*bound line length*/, 80);

  ost << "\n";
  ost << "#include \"" << header_name << "\"\n";
  ost << "#include <cmath>\n";
  ost << "#include <cassert>\n";
  //ost << "#include <cstdio>\n";
  ost << "\n";

  emit_base_conductivity(ost);
  ost << "\n";
  emit_base_viscosity(ost);
  ost << "\n";
  emit_base_diffusion(ost);
  ost << "\n";
  emit_base_thermal(ost);

  ost << "\n";
}

void TranLib::emit_base_conductivity(CodeOutStream &ost)
{
  emit_base_conductivity_declaration(ost);
  ost << "\n";
  PairDelim pair(ost);
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  ost << REAL << " sum  = 0.0;\n";
  ost << REAL << " sumr = 0.0;\n";
  // For each species emit the code to compute the value for that species 
  unsigned spec_idx = 0;
  //unsigned offset = 0 * num_species;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Make sure the molecular masses are the same otherwise we're doing something wrong
    assert((*it)->molecular_mass == nwt[spec_idx]);
    ost << "// Species " << (*it)->name << "\n";
    PairDelim spec_pair(ost);
    ost << REAL " val = " << nlam[(spec_idx+1)*order-1] << ";\n";;
    for (int i = (order-1); i > 0; i--)
    {
      ost << "val = val * logt + " << nlam[(spec_idx)*order+i-1] << ";\n";
    }
    ost << "val = exp(val);\n";
    ost << "sum  += (" << MOLE_FRAC << "[" << spec_idx << "]*val);\n";
    ost << "sumr += (" << MOLE_FRAC << "[" << spec_idx << "]/val);\n";
    //ost << "printf(\"Coeffs for %s: %lf %lf %lf %lf\\n\", \"" << (*it)->name << "\","
    //    << "rmcwrk[" << (offset+spec_idx*order) << "],rmcwrk[" << (offset+spec_idx*order) << "+1],rmcwrk["
    //    << (offset+spec_idx*order) << "+2],rmcwrk[" << (offset+spec_idx*order) << "+3]);\n";
    // Update the species idx
    //ost << "printf(\"Mol weight for %s: %lf\\n\",\"" << (*it)->name << "\","
    //    << "rmcwrk[" << (offset+spec_idx) << "]);\n";
    spec_idx++;
  }
  ost << "return (0.5 * (sum + 1.0/sumr));\n";
}

void TranLib::emit_base_conductivity_declaration(CodeOutStream &ost)
{
  ost << REAL << " base_conductivity(" 
      << "const " << REAL << " " << TEMPERATURE << ", "
      << "const " << REAL << " *" << MOLE_FRAC << ")"; //", "
      //<< "const " << REAL << " *" << "rmcwrk)";
}

void TranLib::emit_base_viscosity(CodeOutStream &ost)
{
  emit_base_viscosity_declaration(ost);
  ost << "\n";
  PairDelim pair(ost);
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  ost << REAL << " spec_visc[" << num_species << "];\n";
  assert(num_species == int(unit->ordered_species.size()-1));
  unsigned spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Make sure the molecular masses are the same otherwise we're doing something wrong
    assert((*it)->molecular_mass == nwt[spec_idx]); 
    ost << "// Species " << (*it)->name << "\n";
    PairDelim spec_pair(ost);
    ost << REAL << " val = " << neta[(spec_idx+1)*order-1] << ";\n";
    for (int i = (order-1); i > 0; i--)
    {
      ost << "val = val * logt + " << neta[(spec_idx)*order+i-1] << ";\n";
    }
    ost << "spec_visc[" << spec_idx << "] = exp(val);\n";
    spec_idx++;
  }

  ost << REAL << " result = 0.0;\n";
  spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << "// Species " << (*it)->name << "\n";
    PairDelim spec_pair(ost);
    ost << REAL " spec_sum = 0.0;\n";
    for (int idx = 0; idx < num_species; idx++)
    {
      PairDelim inner_pair(ost);
      if (idx == int(spec_idx))
      {
        ost << "spec_sum += (" << MOLE_FRAC << "[" << idx << "]*4.0/"
            << (sqrt(2.0)) << ");\n";
      }
      else
      {
        ost << REAL << " numer = 1.0 + sqrt(spec_visc[" << spec_idx << "]/spec_visc[" << idx << "]"
            << "*" << (sqrt(nwt[idx]/nwt[spec_idx])) << ");\n";
        ost << "spec_sum += (" << MOLE_FRAC << "[" << idx << "]*numer*numer/"
            << (sqrt(1.0+(nwt[spec_idx]/nwt[idx]))) << ");\n";
      }
    }
    ost << "result += (" << MOLE_FRAC << "[" << spec_idx << "]*spec_visc[" << spec_idx << "]/spec_sum);\n";
    spec_idx++;
  }
  ost << "return (" << (sqrt(8.0)) << "*result);\n";
}

void TranLib::emit_base_viscosity_declaration(CodeOutStream &ost)
{
  ost << REAL << " base_viscosity("
      << "const " << REAL << " " << TEMPERATURE << ", "
      << "const " << REAL << " *" << MOLE_FRAC << ")";
}

void TranLib::emit_base_diffusion(CodeOutStream &ost)
{
  emit_base_diffusion_declaration(ost);
  ost << "\n";
  PairDelim pair(ost);
  // check to see if the djk matrix will be symmetric
  bool symmetric = is_diffusion_symmetric();
  // compute the clamped mole weights
  ost << REAL << " clamped[" << num_species << "];\n";
  ost << "for (int i = 0; i < " << num_species << "; i++)\n";
  ost << "  clamped[i] = ((" << MOLE_FRAC << "[i] > " << SMALL << ") ? " << MOLE_FRAC << "[i] : " << SMALL << ");\n";
  ost << REAL << " sumxod[" << num_species << "];\n";
  ost << "for (int i = 0; i < " << num_species << "; i++)\n";
  ost << "  sumxod[i] = 0.0;\n";
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  if (symmetric)
  {
    for (int k = 0; k < num_species; k++)
    {
      for (int j = k+1; j < num_species; j++)
      {
        ost << "// D_" << j << "_" << k << " and D_" << k << "_" << j << "\n";
        PairDelim d_pair(ost);
        int offset = j*order + k*order*num_species;
        ost << REAL << " val = " << ndif[offset+order-1] << ";\n";
        for (int i = (order-1); i > 0; i--)
          ost << "val = val * logt + " << ndif[offset+i-1] << ";\n";
        ost << "val = exp(-val);\n";
        ost << "sumxod[" << k << "] += (clamped[" << j << "]*val);\n";
        ost << "sumxod[" << j << "] += (clamped[" << k << "]*val);\n";
      }
    }
  }
  else
  {
    for (int k = 0; k < num_species; k++)
    {
      for (int j = 0; j < num_species; j++)
      {
        // Zeros on the diagonal
        if (k == j)
          continue;
        ost << "// D_" << j << "_" << k << "\n";
        PairDelim d_pair(ost);
        int offset = j*order + k*order*num_species;
        ost << REAL << " val = " << ndif[offset+order-1] << ";\n";
        for (int i = (order-1); i > 0; i--)
          ost << "val = val * logt + " << ndif[offset+i-1] << ";\n";
        ost << "val = exp(-val);\n";
        ost << "sumxod[" << k << "] += (clamped[" << j << "]*val);\n";
      }
    }
  }
  ost << REAL << " sumxw = 0.0;\n";
  ost << REAL << " wtm = 0.0;\n";
  unsigned spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Make sure the molecular masses are the same otherwise we're doing something wrong
    assert((*it)->molecular_mass == nwt[spec_idx]);
    ost << "sumxw += (clamped[" << spec_idx << "]*" << nwt[spec_idx] << ");\n";
    ost << "wtm += (" << MOLE_FRAC << "[" << spec_idx << "]*" << nwt[spec_idx] << ");\n";
    spec_idx++;
  }
  //if (unit->no_nondim)
    ost << REAL << " pfac = " << patmos << "/" << PRESSURE << ";\n";
  //else
  //  ost << REAL << " pfac = " << (patmos/PRESSURE_REF) << "/" << PRESSURE << ";\n";
  // Now compute the diffusion coefficients, and overwrite them in the sumxod
  spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    // Skip the third body species
    if (strcmp((*it)->name,"M") == 0)
      continue; 
    ost << "diffusion[" << spec_idx << "] = pfac * (sumxw - (" << nwt[spec_idx] << "*clamped[" << spec_idx << "]))"
        << " / (wtm * sumxod[" << spec_idx << "]);\n";
    spec_idx++;
  }
}

void TranLib::emit_base_diffusion_declaration(CodeOutStream &ost)
{
  ost << VOID << " base_diffusion("
      << "const " << REAL << " " << PRESSURE << ", "
      << "const " << REAL << " " << TEMPERATURE << ", "
      << "const " << REAL << " *" << MOLE_FRAC << ", "
      << REAL << " *" << "diffusion" << ")";
}

void TranLib::emit_base_thermal(CodeOutStream &ost)
{
  emit_base_thermal_declaration(ost);
  ost << "\n";
  PairDelim pair(ost);
  ost << "for (int i = 0; i < " << num_species << "; i++)\n";
  ost << "  thermal[i] = 0.0;\n";
  for (int i = 0; i < nlite; i++)
  {
    // Subtract one because of fortran
    int light_idx = iktdif[i] - 1;
    int offset = i*order*num_species;
    // Find the species name
    int search_idx = 0;
    Species *light_spec = NULL;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      assert((*it)->molecular_mass == nwt[search_idx]);
      if (search_idx == light_idx)
      {
        light_spec = *it;
        break;
      }
      search_idx++;
    }
    assert(light_spec != NULL);
    assert(light_spec->molecular_mass == nwt[light_idx]);
    ost << "// Light species " << light_spec->name << "\n";
    PairDelim light_pair(ost);
    // Now for the each of the other species, compute their interaction
    // with the light species
    unsigned other_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if (light_idx == int(other_idx))
      {
        other_idx++;
        continue;
      }
      assert((*it)->molecular_mass == nwt[other_idx]);
      ost << "// Interaction with " << (*it)->name << "\n";
      PairDelim inter_pair(ost);
      ost << REAL << " val = " << ntdif[offset+(other_idx+1)*order-1] << ";\n";
      for (int j = (order-1); j > 0; j--)
      {
        ost << "val = val * " << TEMPERATURE << " + " << ntdif[offset+other_idx*order+j-1] << ";\n";
      }
      ost << "thermal[" << light_idx << "] += (" << MOLE_FRAC << "[" << light_idx << "]"
          << "*" << MOLE_FRAC << "[" << other_idx << "]*val);\n";
      other_idx++;
    }
  }
}

void TranLib::emit_base_thermal_declaration(CodeOutStream &ost)
{
  ost << VOID << " base_thermal("
      << "const " << REAL << " " << TEMPERATURE << ", "
      << "const " << REAL << " *" << MOLE_FRAC << ", "
      << REAL << " *" << "thermal" << ")";
}

bool TranLib::is_diffusion_symmetric(void) const
{
  // Symmetric unless we find a counterexample
  bool result = true;
  for (int idx1 = 0; idx1 < num_species; idx1++)
  {
    for (int idx2 = 0; idx2 < num_species; idx2++)
    {
      for (int idx = 0; idx < order; idx++)
      {
        double one = ndif[idx + idx1*order + idx2*order*num_species];
        double two = ndif[idx + idx2*order + idx1*order*num_species];
        if (one != two)
          result = false;
      }
    }
  }
  return result;
}

void TranLib::emit_cuda_source_file(const char *file_name, bool emit_experiment)
{
  CodeOutStream ost(file_name, true/*bound line length*/, 80);

  ost << "\n";
  ost << "#include \"cuda.h\"\n";
  ost << "#include \"cuda_runtime.h\"\n";
  ost << "\n";
  ost << "#define POINTS_PER_THREAD " << unit->points_per_thread << "\n";
  ost << "#define THREADS_PER_BLOCK " << unit->threads_per_block << "\n";
  ost << "\n";

  std::vector<std::map<Species*,unsigned/*idx*/> > species_cuts;
  unsigned max_specs = 0;
  bool uniform = false;
  if (unit->threads_per_point > 0)
  {
    species_cuts.resize(unit->threads_per_point);
    // Round robin distribute species.  All work is the same
    // so it doesn't matter
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      unsigned index = spec_idx % unit->threads_per_point;
      species_cuts[index].insert(std::pair<Species*,unsigned>(*it,spec_idx));
      spec_idx++;
    }
    max_specs = species_cuts[0].size();
    uniform = (max_specs == (species_cuts.back().size()));
  }

  if (species_cuts.empty())
    emit_cuda_conductivity(ost);
  else
    emit_specialized_conductivity(ost, max_specs, uniform);
  ost << "\n";
  if (species_cuts.empty())
    emit_cuda_viscosity(ost);
  else
  {
    // Boundary for dropping under 2 CTAs because of shared memory
    if ((unit->species.size()-1) <= 48)
    {
      // For Kepler
      emit_sliced_viscosity(ost, species_cuts, max_specs, uniform);
      // For Fermi
      //emit_specialized_viscosity(ost, species_cuts, max_specs, uniform);
    }
    else
    {
      emit_large_sliced_viscosity(ost, max_specs, uniform);
    }
  }
  ost << "\n";
  this->diffusion_kernels = 1;
  if ((unit->threads_per_point == 0) && (unit->diff_block_h==0))
    emit_cuda_diffusion(ost);
  else if ((unit->species.size()-1) <= 48) //if (unit->diff_block_h == 0)
    emit_specialized_diffusion(ost);
  //else 
  //  emit_blocked_diffusion(ost); // this will set diffusion_kernels
  else
    emit_large_sliced_diffusion(ost, max_specs, uniform);
  ost << "\n";
  emit_cuda_thermal(ost);
  ost << "\n";

  if (emit_experiment)
    emit_cuda_experiments(ost);
}

static void emit_cuda_load(CodeOutStream &ost, const char *dst,
                           const char *src, bool double_two, const char *qualifier = ".cg")
{
#ifdef ENABLE_LDG
  ost << "#if __CUDA_ARCH__ >= 350\n";
  if (double_two)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << ".x), ";
    ost << "\"=d\"(" << dst << ".y) : \"l\"(" << src << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << ") : \"memory\");\n";
  }
  ost << "#else\n";
#endif
  if (double_two)
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << ".x), ";
    ost << "\"=d\"(" << dst << ".y) : \"l\"(" << src << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << ") : \"memory\");\n";
  }
#ifdef ENABLE_LDG
  ost << "#endif\n";
#endif
}

static void emit_cuda_load(CodeOutStream &ost, const char *dst, 
                           const char *src, const char *src_offset, bool double_two, const char *qualifier = ".cg")
{
#ifdef ENABLE_LDG
  ost << "#if __CUDA_ARCH__ >= 350\n";
  if (double_two)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << ".x), ";
    ost << "\"=d\"(" << dst << ".y) : \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  ost << "#else\n";
#endif
  if (double_two)
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << ".x), ";
    ost << "\"=d\"(" << dst << ".y) : \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << ") ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
#ifdef ENABLE_LDG
  ost << "#endif\n";
#endif
}

static void emit_cuda_load(CodeOutStream &ost, const char *dst, const char *dst_offset,
                           const char *src, const char *src_offset, bool double_two, const char *qualifier = ".cg")
{
  ost << "#if __CUDA_ARCH__ >= 350\n";
  if (double_two)
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << "[" << dst_offset << "].x), ";
    ost << "\"=d\"(" << dst << "[" << dst_offset << "].y) : \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global.nc" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << "[" << dst_offset << "]) ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  ost << "#else\n";
  if (double_two)
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst << "[" << dst_offset << "].x), ";
    ost << "\"=d\"(" << dst << "[" << dst_offset << "].y) : \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  else
  {
    ost << "asm volatile(\"ld.global" << qualifier << ".f64 %0, [%1];\" : \"=d\"(" << dst << "[" << dst_offset << "]) ";
    ost << ": \"l\"(" << src << "+" << src_offset << ") : \"memory\");\n";
  }
  ost << "#endif\n";
}

void TranLib::emit_cuda_conductivity(CodeOutStream &ost)
{
  emit_cuda_conductivity_declaration(ost, true/*put bounds*/);
  ost << "\n";
  PairDelim pair(ost);
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*blockDim.y+threadIdx.y)*row_stride + "
        << "(blockIdx.x*blockDim.x + threadIdx.x)*sizeof(" 
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ")"
        << "+blockIdx.z*slice_stride;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
#ifdef SINGLE_ARRAYS
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
#else
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      ost << MOLE_FRAC_ARRAY << "_" << (*it)->code_name << " += offset;\n";
    }
#endif
    ost << "conductivity += offset;\n";
  }
#if 0
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim outer_loop_pair(ost);
#endif
  // First load the temperature
  //ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << "[POINTS_PER_THREAD];\n";
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << ";\n";
  ost << "// Load the temperatures\n";
  emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,unit->use_double2);
#if 0
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    emit_cuda_load(ost, TEMPERATURE, "i", TEMPERATURE_ARRAY, "i*row_stride", unit->use_double2);   
  }
#endif
  // Issue the loads for the next set of species and overlap them with some math
  // The first load we'll overlap with computing the log of the temperatures
  std::vector<Species*>::const_iterator spec_it = unit->ordered_species.begin();
  unsigned spec_idx = 0;
  if (strcmp((*spec_it)->name,"M") == 0)
  {
    spec_it++;
  }
  ost << "// Loads for " << (*spec_it)->name << "\n";
  //ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "_" << (*spec_it)->code_name << "[POINTS_PER_THREAD];\n";
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "_" << (*spec_it)->code_name << ";\n";
  {
    //ost << "#pragma unroll\n";
    //ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    //PairDelim loop_pair(ost);
    char dst_name[128];
    strcpy(dst_name,MOLE_FRAC);
    strcat(dst_name,"_");
    strcat(dst_name,(*spec_it)->code_name);
#ifdef SINGLE_ARRAYS
    char src_offset[128];
    //sprintf(src_offset,"%d*spec_stride+i*row_stride",spec_idx);
    sprintf(src_offset,"%d*spec_stride",spec_idx);
    emit_cuda_load(ost, dst_name, MOLE_FRAC_ARRAY, src_offset, unit->use_double2);
#else
    char src_name[128];
    strcpy(src_name,MOLE_FRAC_ARRAY);
    strcat(src_name,"_");
    strcat(src_name,(*spec_it)->code_name);
    //emit_cuda_load(ost, dst_name, "i", src_name, "i*row_stride", unit->use_double2);
    emit_cuda_load(ost, dst_name, src_name, unit->use_double2);
#endif
  }
  // Now compute the logarithm of all the temperatures
  ost << "// Compute log(t)\n";
  //ost << (unit->use_double2 ? REAL2 : REAL) << " logt[POINTS_PER_THREAD];\n";
  ost << (unit->use_double2 ? REAL2 : REAL) << " logt;\n";
  {
    //ost << "#pragma unroll\n";
    //ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    //PairDelim loop_pair(ost);
    if (unit->use_double2)
      ost << "logt = make_double2(log(" << TEMPERATURE << ".x), log(" << TEMPERATURE << ".y));\n";
      //ost << "logt[i] = make_double2(log(" << TEMPERATURE << "[i].x),log(" << TEMPERATURE << "[i].y));\n";
    else
      ost << "logt = log(" << TEMPERATURE << ");\n";
      //ost << "logt[i] = log(" << TEMPERATURE << "[i]);\n";
  }
  ost << (unit->use_double2 ? REAL2 : REAL) << " sum;\n";
  ost << (unit->use_double2 ? REAL2 : REAL) << " sumr;\n";
  //ost << (unit->use_double2 ? REAL2 : REAL) << " sum[POINTS_PER_THREAD];\n";
  //ost << (unit->use_double2 ? REAL2 : REAL) << " sumr[POINTS_PER_THREAD];\n";
  {
    //ost << "#pragma unroll\n";
    //ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    //PairDelim loop_pair(ost);
    if (unit->use_double2)
    {
      ost << "sum = make_double2(0.0,0.0);\n";
      ost << "sumr = make_double2(0.0,0.0);\n";
      //ost << "sum[i] = make_double2(0.0,0.0);\n";
      //ost << "sumr[i] = make_double2(0.0,0.0);\n";
    }
    else
    {
      ost << "sum = 0.0;\n";
      ost << "sumr = 0.0;\n";
      //ost << "sum[i] = 0.0;\n";
      //ost << "sumr[i] = 0.0;\n";
    }
  }
  // Now do the rest of the species
  while (spec_it != unit->ordered_species.end())
  {
    if (strcmp((*spec_it)->name,"M") == 0)
    {
      spec_it++;
      continue;
    }
    // See if we need to load the next species
    std::vector<Species*>::const_iterator next_spec = spec_it+1;
    if (next_spec != unit->ordered_species.end())
    {
      // Issue loads for the next species that we need to do
      ost << "\n";
      ost << "// Loads for " << (*next_spec)->name << "\n";
      //ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "_" << (*next_spec)->code_name << "[POINTS_PER_THREAD];\n";
      ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "_" << (*next_spec)->code_name << ";\n";
      {
        //ost << "#pragma unroll\n";
        //ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
        PairDelim loop_pair(ost);
        char dst_name[128];
        strcpy(dst_name,MOLE_FRAC);
        strcat(dst_name,"_");
        strcat(dst_name,(*next_spec)->code_name);
#ifdef SINGLE_ARRAYS
        char src_offset[128];
        sprintf(src_offset,"%d*spec_stride",spec_idx+1);
        //sprintf(src_offset,"%d*spec_stride+i*row_stride",spec_idx);
        //emit_cuda_load(ost, dst_name, "i", MOLE_FRAC_ARRAY, src_offset, unit->use_double2);
        emit_cuda_load(ost, dst_name, MOLE_FRAC_ARRAY, src_offset, unit->use_double2);
#else
        char src_name[128];
        strcpy(src_name,MOLE_FRAC_ARRAY);
        strcat(src_name,"_");
        strcat(src_name,(*next_spec)->code_name);
        //emit_cuda_load(ost, dst_name, "i", src_name, "i*row_stride", unit->use_double2);
        emit_cuda_load(ost, dst_name, src_name, unit->use_double2);
#endif
      }
    }
    // Do the math for this species
    ost << "\n";
    ost << "// Math for " << (*spec_it)->name << "\n";
    {
      //ost << "#pragma unroll\n";
      //ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
      PairDelim loop_pair(ost);
      if (unit->use_double2)
      {
        for (int i = (order-1); i > 0; i--)
        {
          if (i == (order-1))
            ost << REAL2 << " val = make_double2( "
                << "__fma_rn(" << nlam[(spec_idx)*order+i] << ",logt.x," << nlam[(spec_idx)*order+i-1] << "), "
                << "__fma_rn(" << nlam[(spec_idx)*order+i] << ",logt.y," << nlam[(spec_idx)*order+i-1] << "));\n";
            //ost << REAL2 << " val = make_double2( "
            //    << "__fma_rn(" << nlam[(spec_idx)*order+i] << ",logt[i].x," << nlam[(spec_idx)*order+i-1] << "), "
            //    << "__fma_rn(" << nlam[(spec_idx)*order+i] << ",logt[i].y," << nlam[(spec_idx)*order+i-1] << "));\n";
          else
            ost << "val = make_double2( "
                << "__fma_rn(val.x,logt.x," << nlam[(spec_idx)*order+i-1] << "), "
                << "__fma_rn(val.y,logt.y," << nlam[(spec_idx)*order+i-1] << "));\n";
            //ost << "val = make_double2( "
            //    << "__fma_rn(val.x,logt[i].x," << nlam[(spec_idx)*order+i-1] << "), "
            //    << "__fma_rn(val.y,logt[i].y," << nlam[(spec_idx)*order+i-1] << "));\n";
        }
        if (unit->fast_math)
          emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, true/*double2*/);
        else
          ost << "val = make_double2(exp(val.x),exp(val.y));\n";
        ost << "sum = make_double2( "
            << "__fma_rn(" << MOLE_FRAC << "_" << (*spec_it)->code_name << ".x,val.x,sum.x), "
            << "__fma_rn(" << MOLE_FRAC << "_" << (*spec_it)->code_name << ".y,val.y,sum.y));\n";
        ost << "sumr.x += (" << MOLE_FRAC << "_" << (*spec_it)->code_name << ".x/val.x);\n";
        ost << "sumr.y += (" << MOLE_FRAC << "_" << (*spec_it)->code_name << ".y/val.y);\n";
        //ost << "sum[i] = make_double2( "
        //    << "__fma_rn(" << MOLE_FRAC << "_" << (*spec_it)->code_name << "[i].x,val.x,sum[i].x), "
        //    << "__fma_rn(" << MOLE_FRAC << "_" << (*spec_it)->code_name << "[i].y,val.y,sum[i].y));\n";
        //ost << "sumr[i].x += (" << MOLE_FRAC << "_" << (*spec_it)->code_name << "[i].x/val.x);\n";
        //ost << "sumr[i].y += (" << MOLE_FRAC << "_" << (*spec_it)->code_name << "[i].y/val.y);\n";
      }
      else
      {
        for (int i = (order-1); i > 0; i--)
        {
          if (i == (order-1))
            ost << REAL << " val = __fma_rn(" << nlam[(spec_idx)*order+i] << ",logt," << nlam[(spec_idx)*order+i-1] << ");\n";
            //ost << REAL << " val = __fma_rn(" << nlam[(spec_idx)*order+i] << ",logt[i]," << nlam[(spec_idx)*order+i-1] << ");\n"; 
          else
            ost << "val = __fma_rn(val,logt," << nlam[(spec_idx)*order+i-1] << ");\n";
            //ost << "val = __fma_rn(val,logt[i]," << nlam[(spec_idx)*order+i-1] << ");\n";
        }
        //ost << REAL << " neg_val = -val;\n";
        if (unit->fast_math)
        {
          emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, false/*doube2*/);
          //emit_gpu_exp_taylor_series_expansion(ost, "neg_val", unit->taylor_stages, false/*double2*/);
        }
        else
        {
          ost << "val = exp(val);\n";
          //ost << "neg_val = exp(neg_val);\n";
        }
        ost << "sum = __fma_rn(" << MOLE_FRAC << "_" << (*spec_it)->code_name << ",val,sum);\n";
        ost << "sumr += (" << MOLE_FRAC << "_" << (*spec_it)->code_name << "/val);\n";
        //ost << "sum[i] = __fma_rn(" << MOLE_FRAC << "_" << (*spec_it)->code_name << "[i],val,sum[i]);\n";
        //ost << "sumr[i] += (" << MOLE_FRAC << "_" << (*spec_it)->code_name << "[i]/val);\n";
        //ost << "sumr[i] = __fma_rn(" << MOLE_FRAC << "_" << (*spec_it)->code_name << "[i],neg_val,sumr[i]);\n";
      }
    }
    spec_it++;
    spec_idx++; 
  }
  ost << "\n";
  // Now issue the writes for the values
  ost << "// Write out the coefficients\n";
  {
    //ost << "#pragma unroll\n";
    //ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    {
      PairDelim loop_pair(ost);
      if (unit->use_double2)
        ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(conductivity), "
            << "\"d\"(0.5*(sum.x + 1.0/sumr.x)), "
            << "\"d\"(0.5*(sum.y + 1.0/sumr.y)) : \"memory\");\n";
        //ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(conductivity+i*row_stride), "
        //    << "\"d\"(0.5*(sum[i].x + 1.0/sumr[i].x)), "
        //    << "\"d\"(0.5*(sum[i].y + 1.0/sumr[i].y)) : \"memory\");\n";
      else
        ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(conductivity), "
            << "\"d\"(0.5*(sum + 1.0/sumr)) : \"memory\");\n";
        //ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(conductivity+i*row_stride), "
        //    << "\"d\"(0.5*(sum[i] + 1.0/sumr[i])) : \"memory\");\n";
    }
  }
  ost << "\n";
#if 0
  ost << "// Update the pointers\n";
  ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
#ifdef SINGLE_ARRAYS
  ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
#else
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << MOLE_FRAC_ARRAY << "_" << (*it)->code_name << " += slice_stride;\n";
  }
#endif
  ost << "conductivity += slice_stride;\n";
#endif
}

void TranLib::emit_cuda_conductivity_declaration(CodeOutStream &ost, bool bounds)
{
  ost << "__global__ void\n";
  if (bounds)
  {
    int warps_per_block = unit->threads_per_block/32;
    int blocks_per_sm = unit->num_warps/warps_per_block;
    ost << "__launch_bounds__(THREADS_PER_BLOCK," << blocks_per_sm << ")\n";
  }
  ost << "gpu_conductivity(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
#ifdef SINGLE_ARRAYS
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
#else
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY << "_" << (*it)->code_name;
  }
#endif
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
#ifdef SINGLE_ARRAYS
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
#endif
  ost << ", " << REAL << " *" << "conductivity" << ")";
}

void TranLib::emit_cuda_viscosity(CodeOutStream &ost)
{
  emit_cuda_viscosity_declaration(ost, true/*bounds*/);
  ost << "\n";
  PairDelim pair(ost);
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*POINTS_PER_THREAD)*row_stride + "
        << "(blockIdx.x*blockDim.x + threadIdx.x)*sizeof(" 
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ");\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
#ifdef SINGLE_ARRAYS
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
#else
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      ost << MOLE_FRAC_ARRAY << "_" << (*it)->code_name << " += offset;\n";
    }
#endif
    ost << "viscosity += offset;\n";
  }
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim outer_loop_pair(ost);
  // First load the temperature
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << "[POINTS_PER_THREAD];\n";
  ost << "// Load the temperatures\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    emit_cuda_load(ost, TEMPERATURE, "i", TEMPERATURE_ARRAY, "i*row_stride", unit->use_double2);   
  }
  // Compute the log for each of the temperatures
  ost << (unit->use_double2 ? REAL2 : REAL) << " logt[POINTS_PER_THREAD];\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    if (unit->use_double2)
      ost << "logt[i] = make_double2(log(" << TEMPERATURE << "[i].x),log(" << TEMPERATURE << "[i].y));\n";
    else
      ost << "logt[i] = log(" << TEMPERATURE << "[i]);\n";
  }
  // Now emit the computation for the per-species viscosity
  ost << (unit->use_double2 ? REAL2 : REAL) << " spec_visc[" << num_species << "][POINTS_PER_THREAD];\n"; 
  unsigned spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << "// Viscosity for " << (*it)->name << "\n"; 
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim spec_pair(ost);
    for (int i = (order-1); i > 0; i--)
    {
      if (i == (order-1))
      {
        if (unit->use_double2)
          ost << REAL2 << " val = make_double2( "
              << "__fma_rn(" << neta[(spec_idx)*order+i] << ",logt[i].x," << neta[spec_idx*order+i-1] << "), "
              << "__fma_rn(" << neta[(spec_idx)*order+i] << ",logt[i].y," << neta[spec_idx*order+i-1] << "));\n";
        else
          ost << REAL << " val = __fma_rn(" << neta[spec_idx*order+i] << ",logt[i]," << neta[spec_idx*order+i-1] << ");\n";
      }
      else
      {
        if (unit->use_double2)
          ost << "val = make_double2( "
              << "__fma_rn(val.x,logt[i].x," << neta[spec_idx*order+i-1] << "), "
              << "__fma_rn(val.y,logt[i].y," << neta[spec_idx*order+i-1] << "));\n";
        else
          ost << "val = __fma_rn(val,logt[i]," << neta[spec_idx*order+i-1] << ");\n";
      }
    }
    if (unit->fast_math)
    {
      emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, unit->use_double2);
      ost << "spec_visc[" << spec_idx << "][i] = val;\n";
    }
    else
    {
      if (unit->use_double2)
        ost << "spec_visc[" << spec_idx << "][i] = make_double2(exp(val.x),exp(val.y));\n";
      else
        ost << "spec_visc[" << spec_idx << "][i] = exp(val);\n";
    }
    spec_idx++;
  }
  // Now for each point compute its output
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "[" << num_species << "];\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    // First we need to issue the loads for the mole fractions for this point
    spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      char dst_offset[128];
      sprintf(dst_offset,"%d",spec_idx);
#ifdef SINGLE_ARRAYS
      char src_offset[128];
      sprintf(src_offset,"%d*spec_stride+i*row_stride",spec_idx);
      emit_cuda_load(ost,MOLE_FRAC,dst_offset,MOLE_FRAC_ARRAY,src_offset,unit->use_double2);
#else 
      char dst[128];
      strcpy(dst,MOLE_FRAC_ARRAY);
      strcat(dst,"_");
      strcat(dst,(*it)->code_name);
      emit_cuda_load(ost,MOLE_FRAC,dst_offset,dst,"i*row_stride",unit->use_double2); 
#endif
      spec_idx++;
    }
    // Now that we've got all the mole fractions loaded, do the math
    ost << (unit->use_double2 ? REAL2 : REAL) << " result = "
        << (unit->use_double2 ? "make_double2(0.0,0.0)" : "0.0") << ";\n";
    spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      ost << "// Species " << (*it)->name << "\n";
      PairDelim spec_pair(ost);
      ost << (unit->use_double2 ? REAL2 : REAL) << " spec_sum = "
          << (unit->use_double2 ? "make_double2(0.0,0.0)" : "0.0") << ";\n";
      for (int idx = 0; idx < num_species; idx++)
      {
        PairDelim inner_pair(ost);
        if (idx == int(spec_idx))
        {
          if (unit->use_double2)
            ost << "spec_sum = make_double2( "
                << "__fma_rn(" << MOLE_FRAC << "[" << idx << "].x," << (4.0/sqrt(2.0)) << ",spec_sum.x), "
                << "__fma_rn(" << MOLE_FRAC << "[" << idx << "].y," << (4.0/sqrt(2.0)) << ",spec_sum.y));\n";
          else
            ost << "spec_sum = __fma_rn(" << MOLE_FRAC << "[" << idx << "]," << (4.0/sqrt(2.0)) << ",spec_sum);\n";
        }
        else
        {
          if (unit->use_double2)
          {
            ost << REAL2 << " numer = make_double2( "
                << "1.0 + sqrt(spec_visc[" << spec_idx << "][i].x/spec_visc[" << idx << "][i].x"
                << "*" << (sqrt(nwt[idx]/nwt[spec_idx])) << "), "
                << "1.0 + sqrt(spec_visc[" << spec_idx << "][i].y/spec_visc[" << idx << "][i].y"
                << "*" << (sqrt(nwt[idx]/nwt[spec_idx])) << "));\n";
            ost << REAL2 << " factor = make_double2( " 
                << (1.0/sqrt(1.0+(nwt[spec_idx]/nwt[idx]))) << "*numer.x*numer.x, "
                << (1.0/sqrt(1.0+(nwt[spec_idx]/nwt[idx]))) << "*numer.y*numer.y);\n";
            ost << "spec_sum = make_double2( "
                << "__fma_rn(factor.x," << MOLE_FRAC << "[" << idx << "].x,spec_sum.x), "
                << "__fma_rn(factor.y," << MOLE_FRAC << "[" << idx << "].y,spec_sum.y));\n";
          }
          else
          {
            ost << REAL << " numer = 1.0 + sqrt(spec_visc[" << spec_idx << "][i]/spec_visc[" << idx << "][i]"
                << "*" << (sqrt(nwt[idx]/nwt[spec_idx])) << ");\n";
            ost << REAL << " factor = " << (1.0/sqrt(1.0+(nwt[spec_idx]/nwt[idx]))) << "*numer*numer;\n";
            ost << "spec_sum = __fma_rn(factor," << MOLE_FRAC << "[" << idx << "],spec_sum);\n";
          }
        }
      }
      if (unit->use_double2)
        ost << REAL2 << " ratio = make_double2( "
            << "spec_visc[" << spec_idx << "][i].x/spec_sum.x, "
            << "spec_visc[" << spec_idx << "][i].y/spec_sum.y);\n";
      else
        ost << REAL << " ratio = spec_visc[" << spec_idx << "][i]/spec_sum;\n";
      // Now update the result
      if (unit->use_double2)
        ost << "result = make_double2( "
            << "__fma_rn(" << MOLE_FRAC << "[" << spec_idx << "].x,ratio.x,result.x), "
            << "__fma_rn(" << MOLE_FRAC << "[" << spec_idx << "].y,ratio.y,result.y));\n";
      else
        ost << "result = __fma_rn(" << MOLE_FRAC << "[" << spec_idx << "],ratio,result);\n";
      spec_idx++;
    }
    if (unit->use_double2)
      ost << "result = make_double2(" << (sqrt(8.0)) << "*result.x," << (sqrt(8.0)) << "*result.y);\n";
    else
      ost << "result *= " << (sqrt(8.0)) << ";\n";
    // Now we need to write the result out
    if (unit->use_double2)
      ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(viscosity+i*row_stride), "
          << "\"d\"(result.x), "
          << "\"d\"(result.y) : \"memory\");\n";
    else
      ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(viscosity+i*row_stride), "
          << "\"d\"(result) : \"memory\");\n";
  }
  // Finally we need to update the pointers
  ost << "\n";
  ost << "// Update the pointers\n";
  ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
#ifdef SINGLE_ARRAYS
  ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
#else
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << MOLE_FRAC_ARRAY << "_" << (*it)->code_name << " += slice_stride;\n";
  }
#endif
  ost << "viscosity += slice_stride;\n";
}

void TranLib::emit_cuda_viscosity_declaration(CodeOutStream &ost, bool bounds)
{
  ost << "__global__ void\n";
  if (bounds)
  {
    int warps_per_block = unit->threads_per_block/32;
    int blocks_per_sm = unit->num_warps/warps_per_block;
    ost << "__launch_bounds__(THREADS_PER_BLOCK," << blocks_per_sm << ")\n";
  }
  ost << "gpu_viscosity(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
#ifdef SINGLE_ARRAYS
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
#else
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY << "_" << (*it)->code_name;
  }
#endif
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
#ifdef SINGLE_ARRAYS
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
#endif
  ost << ", " << REAL << " *" << "viscosity";
  //ost << ", const bool always_false";
  ost << ")";
}

void TranLib::emit_cuda_diffusion(CodeOutStream &ost)
{
  emit_cuda_diffusion_declaration(ost,true/*bounds*/);
  ost << "\n";
  PairDelim pair(ost);
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*POINTS_PER_THREAD)*row_stride + "
        << "(blockIdx.x*blockDim.x + threadIdx.x)*sizeof(" 
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ");\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
#ifdef SINGLE_ARRAYS
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
    ost << "diffusion += offset;\n";
#else
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      ost << MOLE_FRAC_ARRAY << "_" << (*it)->code_name << " += offset;\n";
      ost << "diffusion_" << (*it)->code_name << " += offset;\n";
    }
#endif
  }
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim outer_loop_pair(ost);
  // First load the temperature
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << "[POINTS_PER_THREAD];\n";
  ost << "// Load the temperatures\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    emit_cuda_load(ost, TEMPERATURE, "i", TEMPERATURE_ARRAY, "i*row_stride", unit->use_double2);   
  }
  // Now issue the loads for the molar fractions for each of the of the species
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "[" << num_species << "][POINTS_PER_THREAD];\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      char src_offset[128];
      sprintf(src_offset,"%d",spec_idx);
      strcat(src_offset,"][i");
#ifdef SINGLE_ARRAYS
      char dst_offset[128];
      sprintf(dst_offset,"%d*spec_stride+i*row_stride",spec_idx);
      emit_cuda_load(ost,MOLE_FRAC,src_offset,MOLE_FRAC_ARRAY,dst_offset,unit->use_double2);
#else
      char dst[128];
      strcpy(dst,MOLE_FRAC_ARRAY);
      strcat(dst,"_");
      strcat(dst,(*it)->code_name);
      emit_cuda_load(ost,MOLE_FRAC,src_offset,dst,"i*row_stride",unit->use_double2);
#endif
      spec_idx++;
    }
  }
  // finally issue any loads for the pressures
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << PRESSURE << "[POINTS_PER_THREAD];\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    emit_cuda_load(ost, PRESSURE, "i", PRESSURE_ARRAY, "i*row_stride", unit->use_double2);
  }
  // Compute the log for each of the temperatures
  ost << (unit->use_double2 ? REAL2 : REAL) << " logt[POINTS_PER_THREAD];\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    if (unit->use_double2)
      ost << "logt[i] = make_double2(log(" << TEMPERATURE << "[i].x),log(" << TEMPERATURE << "[i].y));\n";
    else
      ost << "logt[i] = log(" << TEMPERATURE << "[i]);\n";
  }
  ost << (unit->use_double2 ? REAL2 : REAL) << " sumxod[" << num_species << "][POINTS_PER_THREAD];\n";
  ost << (unit->use_double2 ? REAL2 : REAL) << " wtm[POINTS_PER_THREAD];\n";
  // Before clamping the mole fractions, compute wtm
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    if (unit->use_double2)
      ost << "wtm[i] = make_double2(0.0,0.0);\n";
    else
      ost << "wtm[i] = 0.0;\n";
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      assert((*it)->molecular_mass == nwt[spec_idx]);
      if (unit->use_double2)
        ost << "wtm[i] = make_double2( "
            << "__fma_rn(" << MOLE_FRAC << "[" << spec_idx << "][i].x," << nwt[spec_idx] << ",wtm[i].x), "
            << "__fma_rn(" << MOLE_FRAC << "[" << spec_idx << "][i].y," << nwt[spec_idx] << ",wtm[i].y));\n";
      else
        ost << "wtm[i] = __fma_rn(" << MOLE_FRAC << "[" << spec_idx << "][i]," << nwt[spec_idx] << ",wtm[i]);\n";
      spec_idx++;
    }
  }
  // Clamp all of the mole fractions
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < " << num_species << "; i++)\n";
    PairDelim pair_one(ost);
    ost << "#pragma unroll\n";
    ost << "for (int j = 0; j < POINTS_PER_THREAD; j++)\n";
    PairDelim pair_two(ost);
    if (unit->use_double2)
    {
      ost << MOLE_FRAC << "[i][j] = make_double2( "
          << "fmax(" << MOLE_FRAC << "[i][j].x," << SMALL << "), "
          << "fmax(" << MOLE_FRAC << "[i][j].y," << SMALL << "));\n";
      ost << "sumxod[i][j] = make_double2(0.0,0.0);\n";
    }
    else
    {
      ost << MOLE_FRAC << "[i][j] = fmax(" << MOLE_FRAC << "[i][j]," << SMALL << ");\n";
      ost << "sumxod[i][j] = 0.0;\n";
    }
  }

  // Figure out if we're symmetric or not
  bool symmetric = is_diffusion_symmetric();
  if (symmetric)
  {
    for (int k = 0; k < num_species; k++)
    {
      for (int j = k+1; j < num_species; j++)
      {
        ost << "// D_" << j << "_" << k << " and D_" << k << "_" << j << "\n";
        ost << "#pragma unroll\n";
        ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
        PairDelim d_pair(ost);
        int offset = j*order + k*order*num_species;
        for (int i = (order-1); i > 0; i--)
        {
          if (i == (order-1))
          {
            if (unit->use_double2)
              ost << REAL2 << " val = make_double2( "
                  << "__fma_rn(" << ndif[offset+i] << ",logt[i].x," << ndif[offset+i-1] << "), "
                  << "__fma_rn(" << ndif[offset+i] << ",logt[i].y," << ndif[offset+i-1] << "));\n";
            else
              ost << REAL << " val = __fma_rn(" << ndif[offset+i] << ",logt[i]," << ndif[offset+i-1] << ");\n";
          }
          else
          {
            if (unit->use_double2)
              ost << "val = make_double2( "
                  << "__fma_rn(val.x,logt[i].x," << ndif[offset+i-1] << "), "
                  << "__fma_rn(val.y,logt[i].y," << ndif[offset+i-1] << "));\n";
            else
              ost << "val = __fma_rn(val,logt[i]," << ndif[offset+i-1] << ");\n";
          }
        }
        if (unit->use_double2)
          ost << "val = make_double2(-val.x,-val.y);\n";
        else
          ost << "val = -val;\n";
        if (unit->fast_math)
          emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, unit->use_double2);
        else
        {
          if (unit->use_double2)
            ost << "val = make_double2(exp(val.x),exp(val.y));\n";
          else
            ost << "val = exp(val);\n";
        }
        // Update the output
        if (unit->use_double2)
        {
          ost << "sumxod[" << k << "][i] = make_double2( "
              << "__fma_rn(" << MOLE_FRAC << "[" << j << "][i].x,val.x,sumxod[" << k << "][i].x), "
              << "__fma_rn(" << MOLE_FRAC << "[" << j << "][i].y,val.y,sumxod[" << k << "][i].y));\n";
          ost << "sumxod[" << j << "][i] = make_double2( "
              << "__fma_rn(" << MOLE_FRAC << "[" << k << "][i].x,val.x,sumxod[" << j << "][i].x), "
              << "__fma_rn(" << MOLE_FRAC << "[" << k << "][i].y,val.y,sumxod[" << j << "][i].y));\n";
        }
        else
        {
          ost << "sumxod[" << k << "][i] = __fma_rn(" << MOLE_FRAC << "[" << j << "][i],val,sumxod[" << k << "][i]);\n";
          ost << "sumxod[" << j << "][i] = __fma_rn(" << MOLE_FRAC << "[" << k << "][i],val,sumxod[" << j << "][i]);\n";
        }
      }
    }
  }
  else
  {
    for (int k = 0; k < num_species; k++)
    {
      for (int j = 0; j < num_species; j++)
      {
        // Zeros on the diagonal
        if (k == j)
          continue;
        ost << "// D_" << j << "_" << k << " and D_" << k << "_" << j << "\n";
        ost << "#pragma unroll\n";
        ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
        PairDelim d_pair(ost);
        int offset = j*order + k*order*num_species;
        for (int i = (order-1); i > 0; i--)
        {
          if (i == (order-1))
          {
            if (unit->use_double2)
              ost << REAL2 << " val = make_double2( "
                  << "__fma_rn(" << ndif[offset+i] << ",logt[i].x," << ndif[offset+i-1] << "), "
                  << "__fma_rn(" << ndif[offset+i] << ",logt[i].y," << ndif[offset+i-1] << "));\n";
            else
              ost << REAL << " val = __fma_rn(" << ndif[offset+i] << ",logt[i]," << ndif[offset+i-1] << ");\n";
          }
          else
          {
            if (unit->use_double2)
              ost << "val = make_double2( "
                  << "__fma_rn(val.x,logt[i].x," << ndif[offset+i-1] << "), "
                  << "__fma_rn(val.y,logt[i].y," << ndif[offset+i-1] << "));\n";
            else
              ost << "val = __fma_rn(val,logt[i]," << ndif[offset+i-1] << ");\n";
          }
        }
        if (unit->use_double2)
          ost << "val = make_double2(-val.x,-val.y);\n";
        else
          ost << "val = -val;\n";
        if (unit->fast_math)
          emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, unit->use_double2);
        else
        {
          if (unit->use_double2)
            ost << "val = make_double2(exp(val.x),exp(val.y));\n";
          else
            ost << "val = exp(val);\n";
        }
        // Update the output
        if (unit->use_double2)
        {
          ost << "sumxod[" << k << "][i] = make_double2( "
              << "__fma_rn(" << MOLE_FRAC << "[" << j << "][i].x,val.x,sumxod[" << k << "][i].x), "
              << "__fma_rn(" << MOLE_FRAC << "[" << j << "][i].y,val.y,sumxod[" << k << "][i].y));\n";
        }
        else
        {
          ost << "sumxod[" << k << "][i] = __fma_rn(" << MOLE_FRAC << "[" << j << "][i],val,sumxod[" << k << "][i]);\n";
        }
      }
    }
  } 
  // Write out all the values
  {
    ost << "// Write out the results\n";
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    ost << (unit->use_double2 ? REAL2 : REAL) << " sumxw = "
        << (unit->use_double2 ? "make_double2(0.0,0.0)" : "0.0") << ";\n";
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      assert((*it)->molecular_mass == nwt[spec_idx]);  
      if (unit->use_double2)
        ost << "sumxw = make_double2( "
            << "__fma_rn(" << MOLE_FRAC << "[" << spec_idx << "][i].x," << nwt[spec_idx] << ",sumxw.x), "
            << "__fma_rn(" << MOLE_FRAC << "[" << spec_idx << "][i].y," << nwt[spec_idx] << ",sumxw.y));\n";
      else
        ost << "sumxw = __fma_rn(" << MOLE_FRAC << "[" << spec_idx << "][i]," << nwt[spec_idx] << ",sumxw);\n";
      spec_idx++;
    }
    if (unit->use_double2)
      ost << REAL2 << " pfac = make_double2( "
          << patmos << "/" << PRESSURE << "[i].x, "
          << patmos << "/" << PRESSURE << "[i].y);\n";
    else
      ost << REAL << " pfac = " << patmos << "/" << PRESSURE << "[i];\n";
    spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      assert((*it)->molecular_mass == nwt[spec_idx]);
      PairDelim result_pair(ost);
      if (unit->use_double2)
      {
        ost << REAL << " result0 = pfac.x*(sumxw.x - (" << nwt[spec_idx] << "*" << MOLE_FRAC << "[" << spec_idx << "][i].x))"
            << " / (wtm[i].x * sumxod[" << spec_idx << "][i].x);\n";
        ost << REAL << " result1 = pfac.y*(sumxw.y - (" << nwt[spec_idx] << "*" << MOLE_FRAC << "[" << spec_idx << "][i].y))"
            << " / (wtm[i].y * sumxod[" << spec_idx << "][i].y);\n";
#ifdef SINGLE_ARRAYS
        ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(diffusion+" << spec_idx 
            << "*spec_stride+i*row_stride), " << "\"d\"(result0), \"d\"(result1) : \"memory\");\n";
#else
        ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(diffusion_" << (*it)->code_name << "+i*row_stride), "
            << "\"d\"(result0), \"d\"(result1) : \"memory\");\n";
#endif
      }
      else
      {
        ost << REAL " result = pfac*(sumxw - (" << nwt[spec_idx] << "*" << MOLE_FRAC << "[" << spec_idx << "][i]))"
            << " / (wtm[i] * sumxod[" << spec_idx << "][i]);\n";
#ifdef SINGLE_ARRAYS
        ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(diffusion+" << spec_idx
            << "*spec_stride+i*row_stride), " << "\"d\"(result) : \"memory\");\n";
#else
        ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(diffusion_" << (*it)->code_name << "+i*row_stride), "
            << "\"d\"(result) : \"memory\");\n";
#endif
      }
      spec_idx++;
    }
  }
  // finally update the pointers
  ost << "\n";
  ost << "// Update the pointers\n";
  ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
  ost << PRESSURE_ARRAY << " += slice_stride;\n";
#ifdef SINGLE_ARRAYS
  ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
  ost << "diffusion += slice_stride;\n";
#else
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << MOLE_FRAC_ARRAY << "_" << (*it)->code_name << " += slice_stride;\n";
    ost << "diffusion_" << (*it)->code_name << " += slice_stride;\n";
  }
#endif
}

void TranLib::emit_cuda_diffusion_declaration(CodeOutStream &ost, bool bounds)
{
  ost << "__global__ void\n";
  if (bounds)
  {
    int warps_per_block = unit->threads_per_block/32;
    int blocks_per_sm = unit->num_warps/warps_per_block;
    ost << "__launch_bounds__(THREADS_PER_BLOCK," << blocks_per_sm << ")\n";
  }
  ost << "gpu_diffusion(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << PRESSURE_ARRAY;
#ifdef SINGLE_ARRAYS
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
#else
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY << "_" << (*it)->code_name;
  }
#endif
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
#ifdef SINGLE_ARRAYS
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "diffusion";
#else
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << ", " << REAL << " *" << "diffusion_" << (*it)->code_name;
  }
#endif
  ost << ")";
}

void TranLib::emit_cuda_thermal(CodeOutStream &ost)
{
  emit_cuda_thermal_declaration(ost, false/*bounds*/);
  ost << "\n";
  PairDelim pair(ost);
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*blockDim.y+threadIdx.y)*row_stride + "
        << "(blockIdx.x*blockDim.x + threadIdx.x)*sizeof(" 
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ")"
        << " + blockIdx.z*slice_stride;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
    ost << "thermal_out += offset;\n";
  }
  // First load the temperature
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, unit->use_double2);

  // Initialize the light species
  ost << (unit->use_double2 ? REAL2 : REAL) << " thermal[" << nlite << "];\n";
  for (int idx = 0; idx < nlite; idx++)
  {
    if (unit->use_double2)
      ost << "thermal[" << idx << "] = make_double2(0.0,0.0);\n";
    else
      ost << "thermal[" << idx << "] = 0.0;\n";
    ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "_light_" << (iktdif[idx]-1) << ";\n";
    // Also issue a load for the light species
    char dst[128];
    sprintf(dst,"%s_light_%d",MOLE_FRAC,(iktdif[idx]-1));
    char src_offset[128];
    sprintf(src_offset,"%d*spec_stride",(iktdif[idx]-1));
    emit_cuda_load(ost,dst,MOLE_FRAC_ARRAY,src_offset,unit->use_double2);
  }
  // Walk over all the species issuing loads for them and then computing their contribution
  // to each light species
  unsigned spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    if ((THERMAL_FENCES > 0) && (spec_idx>0) && (spec_idx%THERMAL_FENCES==0))
      ost << "__threadfence_block();\n";
    ost << "// Interactions for species " << (*it)->name << "\n";
    PairDelim spec_pair(ost);
    // First emit the load for this mole fraction if it is not a light
    // species, otherwise the mole fraction is just whichever light species
    // it is
    int spec_light_idx = -1;
    for (int i = 0; i < nlite; i++)
    {
      if (int(spec_idx) == (iktdif[i]-1))
      {
        spec_light_idx = iktdif[i]-1;
        assert(spec_light_idx >= 0);
        break;
      }
    }
    if (spec_light_idx == -1)
    {
      ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << ";\n";
      {
        char src_offset[128];
        sprintf(src_offset,"%d*spec_stride",spec_idx);
        emit_cuda_load(ost,MOLE_FRAC,MOLE_FRAC_ARRAY,src_offset,unit->use_double2);
      }
    }
    else
    {
      ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC
          << " = " << MOLE_FRAC << "_light_" << spec_light_idx << ";\n";
    }
    // Now compute the interactions for each of the lite species
    for (int i = 0; i < nlite; i++)
    {
      int light_idx = iktdif[i]-1; // subtract one because of fortran
      int offset = i*order*num_species;
      // don't need to do interactions with themselves
      if (spec_light_idx == light_idx)
      {
        ost << "// No interaction for species " << light_idx << " with itself\n";
        continue;
      }
      PairDelim light_pair(ost);
      for (int j = (order-1); j > 0; j--)
      {
        if (unit->use_double2)
        {
          if (j == (order-1))
            ost << REAL2 << " val = make_double2("
                << "__fma_rn(" << ntdif[offset+spec_idx*order+j] << "," << TEMPERATURE << ".x," 
                << ntdif[offset+spec_idx*order+j-1] << "), "
                << "__fma_rn(" << ntdif[offset+spec_idx*order+j] << "," << TEMPERATURE << ".y,"
                << ntdif[offset+spec_idx*order+j-1] << "));\n";
          else
            ost << "val = make_double2( "
                << "__fma_rn(val.x," << TEMPERATURE << ".x," << ntdif[offset+spec_idx*order+j-1] << "), "
                << "__fma_rn(val.y," << TEMPERATURE << ".y," << ntdif[offset+spec_idx*order+j-1] << "));\n";
        }
        else
        {
          if (j == (order-1))
            ost << REAL << " val = __fma_rn(" << ntdif[offset+spec_idx*order+j] << ","
                << TEMPERATURE << "," << ntdif[offset+spec_idx*order+j-1] << ");\n";
          else
            ost << "val = __fma_rn(val," << TEMPERATURE << "," << ntdif[offset+spec_idx*order+j-1] << ");\n";
        }
      }
      // Update the thermal values
      if (unit->use_double2)
      {
        ost << "val = make_double2(val.x*" << MOLE_FRAC << ".x,val.y*" << MOLE_FRAC << ".y);\n"; 
        ost << "thermal[" << i << "] = make_double2( "
            << "__fma_rn(" << MOLE_FRAC << "_light_" << light_idx << ".x,val.x,thermal[" << i << "].x), "
            << "__fma_rn(" << MOLE_FRAC << "_light_" << light_idx << ".y,val.y,thermal[" << i << "].y));\n";
      }
      else
      {
        ost << "val = val * " << MOLE_FRAC << ";\n";
        ost << "thermal[" << i << "] = __fma_rn(" << MOLE_FRAC << "_light_" << light_idx << ",val,thermal[" << i << "]);\n";
      }
    }
    // Update the species index
    spec_idx++;
  }
  // Now we get to do the outputs for all the species, if it is a light species then we know
  // its value, otherwise we just get to write zero.
  spec_idx = 0;
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    // Check to see if this is a light species
    int light_idx = -1;
    for (int i = 0; i < nlite; i++)
    {
      if (int(spec_idx) == (iktdif[i]-1))
      {
        light_idx = i;
        break;
      }
    }
    if (light_idx > -1)
    {
      if (unit->use_double2)
        ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(thermal_out+" << spec_idx
            << "*spec_stride), \"d\"(thermal[" << light_idx << "].x), \"d\"(thermal[" << light_idx << "].y) : \"memory\");\n";
      else
        ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(thermal_out+" << spec_idx
            << "*spec_stride), \"d\"(thermal[" << light_idx << "]) : \"memory\");\n";
    }
    else
    {
      // Not a light species, so just write out zero
      if (unit->use_double2)
        ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(thermal_out+" << spec_idx
            << "*spec_stride), \"d\"(0.0), \"d\"(0.0) : \"memory\");\n";
      else
        ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(thermal_out+" << spec_idx
            << "*spec_stride), \"d\"(0.0) : \"memory\");\n";
    }
    spec_idx++;
  }
}

// Old version of this function
#if 0
void TranLib::emit_cuda_thermal(CodeOutStream &ost)
{
  emit_cuda_thermal_declaration(ost, true/*bounds*/);
  PairDelim pair(ost);
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*POINTS_PER_THREAD)*row_stride + "
        << "(blockIdx.x*blockDim.x + threadIdx.x)*sizeof(" 
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ");\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
#ifdef SINGLE_ARRAYS
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
    ost << "thermal_out += offset;\n";
#else
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      ost << MOLE_FRAC_ARRAY << "_" << (*it)->code_name << " += offset;\n";
      ost << "thermal_" << (*it)->code_name << " += offset;\n";
    }
#endif
  }
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim outer_loop_pair(ost);
  // First load the temperature
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << "[POINTS_PER_THREAD];\n";
  ost << "// Load the temperatures\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    emit_cuda_load(ost, TEMPERATURE, "i", TEMPERATURE_ARRAY, "i*row_stride", unit->use_double2);   
  }
  // Now emit the loads for all the mole fractions
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "[" << num_species << "][POINTS_PER_THREAD];\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      char dst_offset[128];
      sprintf(dst_offset,"%d",spec_idx);
      strcat(dst_offset,"][i");
#ifdef SINGLE_ARRAYS
      char src_offset[128];
      sprintf(src_offset,"%d*spec_stride+i*row_stride",spec_idx);
      emit_cuda_load(ost,MOLE_FRAC,dst_offset,MOLE_FRAC_ARRAY,src_offset,unit->use_double2);
#else
      char dst[128];
      strcpy(dst,MOLE_FRAC_ARRAY);
      strcat(dst,"_");
      strcat(dst,(*it)->code_name);
      emit_cuda_load(ost,MOLE_FRAC,dst_offset,dst,"i*row_stride",unit->use_double2);
#endif
      spec_idx++;
    }
  }
  // Create and zero out the thermal array
  ost << (unit->use_double2 ? REAL2 : REAL) << " thermal[" << num_species << "][POINTS_PER_THREAD];\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < " << num_species << "; i++)\n";
    PairDelim first_pair(ost);
    ost << "#pragma unroll\n";
    ost << "for (int j = 0; j < POINTS_PER_THREAD; j++)\n";
    PairDelim snd_pair(ost);
    if (unit->use_double2)
      ost << "thermal[i][j] = make_double2(0.0,0.0);\n";
    else
      ost << "thermal[i][j] = 0.0;\n";
  }
  // for each light species 
  for (int i = 0; i < nlite; i++)
  {
    // Subtract one because of fortran
    int light_idx = iktdif[i] - 1;
    int offset = i*order*num_species;
    // Find the species name
    int search_idx = 0;
    Species *light_spec = NULL;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      assert((*it)->molecular_mass == nwt[search_idx]);
      if (search_idx == light_idx)
      {
        light_spec = *it;
        break;
      }
      search_idx++;
    }
    assert(light_spec != NULL);
    assert(light_spec->molecular_mass == nwt[light_idx]);
    ost << "// Light species " << light_spec->name << "\n";
    PairDelim light_pair(ost);
    // Now for the each of the other species, compute their interaction
    // with the light species
    unsigned other_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      if (light_idx == int(other_idx))
      {
        other_idx++;
        continue;
      }
      assert((*it)->molecular_mass == nwt[other_idx]);
      ost << "// Interaction with " << (*it)->name << "\n";
      ost << "#pragma unroll\n";
      ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
      PairDelim inter_pair(ost);
      for (int j = (order-1); j > 0; j--)
      {
        if (j == (order-1))
        {
          if (unit->use_double2)
            ost << REAL2 << " val = make_double2( "
                << "__fma_rn(" << ntdif[offset+other_idx*order+j] << "," << TEMPERATURE << "[i].x," 
                << ntdif[offset+other_idx*order+j-1] << "), "
                << "__fma_rn(" << ntdif[offset+other_idx*order+j] << "," << TEMPERATURE << "[i].y,"
                << ntdif[offset+other_idx*order+j-1] << "));\n";
          else
            ost << REAL << " val = __fma_rn(" << ntdif[offset+other_idx*order+j] << ","
                << TEMPERATURE << "[i]," << ntdif[offset+other_idx*order+j-1] << ");\n";
        }
        else
        {
          if (unit->use_double2)
            ost << "val = make_double2( "
                << "__fma_rn(val.x," << TEMPERATURE << "[i].x," << ntdif[offset+other_idx*order+j-1] << "), "
                << "__fma_rn(val.y," << TEMPERATURE << "[i].y," << ntdif[offset+other_idx*order+j-1] << "));\n";
          else
            ost << "val = __fma_rn(val," << TEMPERATURE << "[i]," << ntdif[offset+other_idx*order+j-1] << ");\n";
        }
      }
      if (unit->use_double2)
      {
        ost << "thermal[" << light_idx << "][i] = make_double2( "
            << "__fma_rn(" << MOLE_FRAC << "[" << light_idx << "][i].x * " << MOLE_FRAC << "[" << other_idx << "][i].x,"
            << "val.x,thermal[" << light_idx << "][i].x), "
            << "__fma_rn(" << MOLE_FRAC << "[" << light_idx << "][i].y * " << MOLE_FRAC << "[" << other_idx << "][i].y,"
            << "val.y,thermal[" << light_idx << "][i].y));\n";
      }
      else
      {
        ost << "thermal[" << light_idx << "][i] = "
            << "__fma_rn(" << MOLE_FRAC << "[" << light_idx << "][i] * " << MOLE_FRAC << "[" << other_idx << "][i], "
            << "val,thermal[" << light_idx << "][i]);\n";
      }
      other_idx++;
    }
  }
  ost << "\n";
  ost << "// Write-out the results\n";
  {
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < POINTS_PER_THREAD; i++)\n";
    PairDelim loop_pair(ost);
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
           it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue; 
#ifdef SINGLE_ARRAYS
      if (unit->use_double2)
        ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(thermal_out+" << spec_idx
            << "*spec_stride+i*row_stride), "
            << "\"d\"(thermal[" << spec_idx << "][i].x), \"d\"(thermal[" << spec_idx << "][i].y) : \"memory\");\n";
      else
        ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(thermal_out+" << spec_idx
            << "*spec_stride+i*row_stride), "
            << "\"d\"(thermal[" << spec_idx << "][i]) : \"memory\");\n";
#else
      if (unit->use_double2)
        ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(thermal_" << (*it)->code_name << "+i*row_stride), "
            << "\"d\"(thermal[" << spec_idx << "][i].x), \"d\"(thermal[" << spec_idx << "][i].y) : \"memory\");\n";
      else
        ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(thermal_" << (*it)->code_name << "+i*row_stride), "
            << "\"d\"(thermal[" << spec_idx << "][i]) : \"memory\");\n";
#endif
      spec_idx++;
    }
  }
  // Update the pointers
  ost << "\n";
  ost << "// Update the pointers\n";
  ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
#ifdef SINGLE_ARRAYS
  ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
  ost << "thermal_out += slice_stride;\n";
#else
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << MOLE_FRAC_ARRAY << "_" << (*it)->code_name << " += slice_stride;\n";
    ost << "thermal_" << (*it)->code_name << " += slice_stride;\n";
  }
#endif
}
#endif

void TranLib::emit_cuda_thermal_declaration(CodeOutStream &ost, bool bounds)
{
  ost << "__global__ void\n";
  if (bounds)
  {
    int warps_per_block = unit->threads_per_block/32;
    int blocks_per_sm = unit->num_warps/warps_per_block;
    ost << "__launch_bounds__(THREADS_PER_BLOCK," << blocks_per_sm << ")\n";
  }
  ost << "gpu_thermal(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
#ifdef SINGLE_ARRAYS
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
#else
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY << "_" << (*it)->code_name;
  }
#endif
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
#ifdef SINGLE_ARRAYS
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "thermal_out";
#else
  for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
        it != unit->ordered_species.end(); it++)
  {
    if (strcmp((*it)->name,"M") == 0)
      continue;
    ost << ", " << REAL << " *" << "thermal_" << (*it)->code_name;
  }
#endif
  ost << ")";
}

void TranLib::emit_cuda_experiments(CodeOutStream &ost)
{
  ost << "#include <cstdio>\n";
  ost << "#include <cassert>\n";
  ost << "#include \"cuda.h\"\n";
  ost << "#include \"cuda_runtime.h\"\n";

  ost << "#define CUDA_SAFE_CALL(expr)  \\\n";
  ost << "  { \\\n";
  ost << "    cudaError_t err = (expr); \\\n";
  ost << "    if (err != cudaSuccess) \\\n";
  ost << "    { \\\n";
  ost << "      printf(\"Cuda error %s\\n\", cudaGetErrorString(err)); \\\n";
  ost << "      exit(1); \\\n";
  ost << "    } \\\n";
  ost << "  }\n";

  emit_cuda_experiment(ost, "conductivity", false/*pressure*/, false/*full out*/, (unit->threads_per_point > 0));
  ost << "\n";
  emit_cuda_experiment(ost, "viscosity", false/*pressure*/, false/*full out*/, (unit->threads_per_point > 0));
  ost << "\n";
  emit_cuda_experiment(ost, "diffusion", true/*pressure*/, true/*full out*/, 
        (unit->threads_per_point > 0)/*warp specialized*/, diffusion_kernels);
  ost << "\n";
  emit_cuda_experiment(ost, "thermal", false/*pressure*/, true/*full out*/, false/*warp specialized*/);
  ost << "\n";
}

void TranLib::emit_cuda_experiment(CodeOutStream &ost, const char *name, bool pressure, 
                                   bool full_out, bool warp_specialized, unsigned num_kernels /*= 1*/)
{
#ifndef SINGLE_ARRAYS
  assert(!warp_specialized); // not supported
#endif
  ost << "__host__\n";
  ost << "float run_" << name << "_experiment(int num_steps, int nx, int ny, int nz)\n";
  PairDelim pair(ost);
  int num_input = num_species + 1/*temperature*/ + (pressure ? 1 : 0); 
  int num_output = (full_out ? num_species : 1);
  ost << "// Allocate memory\n";
  ost << REAL << " *input_d;\n";
  // -1 for ignoring the third body species but +1 for temperature
  ost << "const int species_stride = nx * ny * nz;\n";
  ost << "printf(\"Allocating %ld MB for input\\n\", species_stride*" << num_input << "*sizeof(" << REAL << ") >> 20);\n";
  ost << "CUDA_SAFE_CALL(cudaMalloc((void**)&input_d, species_stride*" << num_input << "*sizeof(" << REAL << ")));\n";
  // Initialize the data with somewhat meaningful values
  ost << "// Initialize input data\n";
  {
    PairDelim init_pair(ost);
    ost << REAL << " *input_h = (" << REAL << "*)malloc(" << num_input << "*species_stride*sizeof(" << REAL << "));\n";
    // Initialize the temperatures to something reasonable
    {
      ost << "for (int i = 0; i < species_stride; i++)\n";
      PairDelim loop_pair(ost);
      ost << "input_h[i] = 400.0*drand48() + 100.0;\n";
    }
    // Initialize everything else to something small between 0 and 1
    {
      ost << "for (int i = species_stride; i < (" << num_input << "*species_stride); i++)\n";
      PairDelim loop_pair(ost);
      ost << "input_h[i] = drand48();\n";
    }
    // Copy down to the device
    ost << "CUDA_SAFE_CALL(cudaMemcpy(input_d,input_h," << num_input << "*species_stride*sizeof(" << REAL << "), cudaMemcpyHostToDevice));\n";
    ost << "free(input_h);\n";
  }
  //ost << "CUDA_SAFE_CALL(cudaMemset(input_d, 0, " << num_input << "*sizeof(" << REAL << ")));\n";
  if ((strcmp(name,"diffusion") == 0) && (num_kernels > 1))
  {
    ost << REAL << " *wtm_d, *sumxw_d;\n";
    ost << "CUDA_SAFE_CALL(cudaMalloc((void**)&wtm_d, species_stride*sizeof(" << REAL << ")));\n";
    ost << "CUDA_SAFE_CALL(cudaMalloc((void**)&sumxw_d, species_stride*sizeof(" << REAL << ")));\n";
  }

  ost << REAL << " *temperature_ptr = input_d;\n";
  
  // Generate some code to make sure all the temperatures are the
  // same for each species so that all the branches go the same way
  // for the kernel which is in general the default behaviour for
  // kernel.
  //ost << "CUDA_SAFE_CALL(cudaMemset(temperature_ptr,0,species_stride*sizeof(" << REAL << ")));\n";
  if (pressure)
    ost << REAL << " *pressure_ptr = input_d + species_stride;\n";
  unsigned actual_idx = (pressure ? 2 : 1);
#ifdef SINGLE_ARRAYS
  ost << REAL << " *" << name << "_input_ptr = input_d + " << actual_idx << "*species_stride;\n";
#else
  for (unsigned idx = 0; idx < unit->ordered_species.size(); idx++)
  {
    Species *spec = unit->ordered_species[idx];
    if (strcmp(spec->name,"M") == 0)
      continue;
    ost << REAL << " *" << spec->code_name << "_ptr = input_d + " << actual_idx << "*species_stride;\n";
    actual_idx++;
  }
#endif
  ost << REAL << " *output_d;\n";
  ost << "printf(\"Allocating %ld MB for output\\n\", species_stride*" << num_output << "*sizeof(" << REAL << ") >> 20);\n";
  ost << "CUDA_SAFE_CALL(cudaMalloc((void**)&output_d, species_stride*" << num_output << "*sizeof(" << REAL << ")));\n";
  if (full_out)
  {
#ifndef SINGLE_ARRAYS
    actual_idx = 0;
    for (unsigned idx = 0; idx < unit->ordered_species.size(); idx++)
    {
      Species *spec = unit->ordered_species[idx];
      if (strcmp(spec->name,"M") == 0)
        continue;
      ost << REAL << " *" << spec->code_name << "_" << name << " = output_d + " << actual_idx << "*species_stride;\n";
      actual_idx++;
    }
#endif
  }
  ost << "\n";
  if (warp_specialized)
    ost << "CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));\n";
  ost << "\n";
  ost << "cudaStream_t timing_stream;\n";
  ost << "CUDA_SAFE_CALL(cudaStreamCreate(&timing_stream));\n";
  ost << "\n";
  ost << "cudaEvent_t start, stop;\n";
  ost << "CUDA_SAFE_CALL(cudaEventCreate(&start));\n";
  ost << "CUDA_SAFE_CALL(cudaEventCreate(&stop));\n";
  ost << "\n";
  if (!warp_specialized)
  {
    if ((strcmp(name,"diffusion")==0) && (num_kernels > 1))
    {
      ost << "dim3 grid(nx/(THREADS_PER_BLOCK*sizeof(" << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof("
          << REAL << ")),ny,nz);\n";
    }
    else
    {
      ost << "dim3 grid(nx/(THREADS_PER_BLOCK*sizeof(" << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" 
          << REAL << ")),ny/POINTS_PER_THREAD,1);\n";
    }
    ost << "dim3 block(THREADS_PER_BLOCK,1,1);\n";
  }
  else
  {
    unsigned threads_per_cta = unit->threads_per_point * 32;
    ost << "dim3 grid(nx/(32*sizeof(" << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ")),"
        << "ny,nz);\n";
    ost << "dim3 block(" << threads_per_cta << ",1,1);\n";
  }
  ost << "assert(grid.x > 0);\n";
  ost << "assert(grid.y > 0);\n";
  ost << "\n";
  ost << "CUDA_SAFE_CALL(cudaEventRecord(start, timing_stream));\n";
  ost << "for (int i = 0; i < num_steps; i++)\n";
  {
    PairDelim loop_pair(ost);
    for (unsigned idx = 0; idx < num_kernels; idx++)
    {
      if (num_kernels > 1)
        ost << "gpu_" << name << "_" << idx << "<<<grid,block,0,timing_stream>>>(";
      else
        ost << "gpu_" << name << "<<<grid,block,0,timing_stream>>>(";
      ost << "temperature_ptr";      
      if (pressure)
        ost << ", pressure_ptr";
#ifdef SINGLE_ARRAYS
      ost << ", " << name << "_input_ptr";
#else
      for (unsigned idx = 0; idx < unit->ordered_species.size(); idx++)
      {
        Species *spec = unit->ordered_species[idx];
        if (strcmp(spec->name,"M") == 0)
          continue;
        ost << ", " << spec->code_name << "_ptr";
      }
#endif
      ost << ", nx*ny";
      ost << ", nx";
      ost << ", nz";
#ifdef SINGLE_ARRAYS
      ost << ", species_stride";
#endif
      if ((strcmp(name,"diffusion")==0) && (num_kernels > 1))
      {
        ost << ", wtm_d";
        ost << ", sumxw_d";
      }
      if (full_out)
      {
#ifdef SINGLE_ARRAYS
        ost << ", output_d";
#else
        for (unsigned idx = 0; idx < unit->ordered_species.size(); idx++)
        {
          Species *spec = unit->ordered_species[idx];
          if (strcmp(spec->name,"M") == 0)
            continue;
          ost << ", " << spec->code_name << "_" << name;
        }
#endif
      }
      else
      {
        ost << ", output_d";
      }
      // for always_false
      //if (warp_specialized || (strcmp(name,"viscosity")==0))
      //  ost << ", (num_steps > 1000)";
      ost << ");\n";
      ost << "#ifdef DEBUG\n";
      ost << "CUDA_SAFE_CALL(cudaGetLastError());\n";
      ost << "#endif\n";
    }
  }
  ost << "CUDA_SAFE_CALL(cudaEventRecord(stop, timing_stream));\n";
  ost << "CUDA_SAFE_CALL(cudaStreamSynchronize(timing_stream));\n";
  ost << "float exec_time;\n";
  ost << "CUDA_SAFE_CALL(cudaEventElapsedTime(&exec_time,start,stop));\n";
  ost << "\n";
  ost << "CUDA_SAFE_CALL(cudaFree(input_d));\n";
  ost << "CUDA_SAFE_CALL(cudaFree(output_d));\n";
  if ((strcmp(name,"diffusion")==0) && (num_kernels > 1))
  {
    ost << "CUDA_SAFE_CALL(cudaFree(wtm_d));\n";
    ost << "CUDA_SAFE_CALL(cudaFree(sumxw_d));\n";
  }
  ost << "CUDA_SAFE_CALL(cudaStreamDestroy(timing_stream));\n";
  ost << "\n";
  {
    unsigned total_read_transfers = (unit->ordered_species.size()-1) + (pressure ? 2: 1);
    unsigned total_write_transfers = (full_out ? (unit->ordered_species.size()-1) : 1);
    ost << "double total_read_bytes = double(num_steps)*double(nx*ny*nz)*" << total_read_transfers << "*sizeof(" << REAL << ");\n";
    ost << "double total_write_bytes = double(num_steps)*double(nx*ny*nz)*" << total_write_transfers << "*sizeof(" << REAL << ");\n";
    ost << "double read_bandwidth = double(total_read_bytes) * 1e-6 / double(exec_time);\n";
    ost << "double write_bandwidth = double(total_write_bytes) * 1e-6 / double(exec_time);\n";
    ost << "fprintf(stdout,\"Read Bandwidth: %lf GB/s\\n\", read_bandwidth);\n";
    ost << "fprintf(stdout,\"Write Bandwidth: %lf GB/s\\n\", write_bandwidth);\n";
    ost << "fprintf(stdout,\"Total Bandwidth: %lf GB/s\\n\", read_bandwidth + write_bandwidth);\n";
  }
  ost << "\n";
  ost << "return exec_time;\n";
}

void TranLib::emit_specialized_conductivity(CodeOutStream &ost, unsigned max_specs, bool uniform)
{
  const unsigned num_species = (unit->species.size()-1);
  // Emit the constants
#ifndef CONDUCTIVITY_SHUFFLE
  ost << "__constant__ " << REAL << " conductivity_constants[" << num_species << "][" << order << "] = {";
  for (unsigned idx = 0; idx < num_species; idx++)
  {
    if (idx == 0)
      ost << " {";
    else
      ost << ", {";
    for (int i = order; i > 0; i--)
    {
      if (i == (order))
        ost << nlam[idx*order+i-1];
      else
        ost << ", " << nlam[idx*order+i-1];
    }
    ost << "}";
  }
  ost << " };\n\n";
#else
  bool has_remainder = ((num_species%unit->threads_per_point) != 0);
  unsigned num_columns = (num_species/unit->threads_per_point);
  unsigned max_points_per_thread = (num_columns + (has_remainder ? 1 : 0))*(order);
  unsigned warp_constant_stride = (max_points_per_thread);
  // Make everything fit nicely by padding with zeros
  // Also guarantees aligned loads for the threads
  while ((warp_constant_stride%32) != 0)
    warp_constant_stride++;
  ost << "__device__ const " << REAL << " conductivity_constants[" << (unit->threads_per_point*warp_constant_stride) << "] = {"; 
  bool first = true;
  for (unsigned wid = 0; wid < unit->threads_per_point; wid++)
  {
    unsigned num_created = 0;
    for (unsigned col_idx = 0; col_idx < num_columns; col_idx++)
    {
      unsigned column = col_idx*unit->threads_per_point + wid;
      for (int i = order; i > 0; i--)
      {
        if (first)
        {
          first = false;
          ost << (nlam[column*order+i-1]);
        }
        else
          ost << ", " << (nlam[column*order+i-1]);
        num_created++;
      }
    }
    assert(!first);
    if (has_remainder)
    {
      unsigned column = (wid+(num_columns*unit->threads_per_point))%num_species;
      for (int i = order; i > 0; i--)
      {
        ost << ", " << (nlam[column*order+i-1]);
        num_created++;
      }
    }
    assert(num_created == max_points_per_thread);
    // Pad everything else with zeros
    while (num_created < warp_constant_stride)
    {
      ost << ", 0.0";
      num_created++;
    }
  }
  ost << "};\n\n";
#endif

  emit_specialized_conductivity_declaration(ost);
  ost << "\n";
  PairDelim pair(ost);
  // emit declarations for tid and wid
  ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
  ost << "const " << INT << " wid = threadIdx.x >> 5;\n";
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*1)*row_stride + "
        << "(blockIdx.x*32 + tid)*sizeof("
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ")"
        << "+blockIdx.z*slice_stride;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
    ost << "conductivity += offset;\n";
  }
  
  // Emit loads for our constants
#ifdef CONDUCTIVITY_SHUFFLE
  const unsigned constants_per_thread = ((max_specs*order)+31)/32;
  assert(warp_constant_stride == (32*constants_per_thread));
  ost << REAL << " conduc_constants[" << constants_per_thread << "];\n";
  {
    PairDelim load_pair(ost);
    ost << "const " << INT << " offset = wid*" << warp_constant_stride << "+tid;\n";
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"offset+%d",(idx*32));
      emit_cuda_load(ost,"conduc_constants",dst_offset,"conductivity_constants",src_offset,false/*double2*/);
    }
  }
#endif
  // Forward declarations for double buffering in registers
  if (unit->use_double2)
  {
    ost << "volatile __shared__ " << REAL << " reduc_buffer[2][" << unit->threads_per_point << "][2][32];\n"; 
    ost << REAL2 << " temperature[2];\n";
    ost << REAL2 << " " << MOLE_FRAC << "[" << max_specs << "][2];\n";
  }
  else
  {
    ost << "volatile __shared__ " << REAL << " reduc_buffer[2][" << unit->threads_per_point << "][32];\n";
    ost << REAL << " temperature[2];\n";
    ost << REAL << " " << MOLE_FRAC << "[" << max_specs << "][2];\n";
  }
  // Issue the first batch of loads
  emit_specialized_conductivity_loads(ost, 0, max_specs, uniform);
  // Issue the next batch of loads
  emit_specialized_conductivity_loads(ost, 1, max_specs, uniform);
  // emit the loop
  {
    ost << "for (int step = 0; step < (total_steps-2); step+=2)\n";
    PairDelim step_loop_pair(ost);
    emit_specialized_conductivity_step(ost, 0, max_specs, uniform, true/*next loads*/);
    emit_specialized_conductivity_step(ost, 1, max_specs, uniform, true/*next loads*/);
  }
  // emit the postamble
  emit_specialized_conductivity_step(ost, 0, max_specs, uniform, false/*next loads*/);
  emit_specialized_conductivity_step(ost, 1, max_specs, uniform, false/*next loads*/);
}

void TranLib::emit_specialized_conductivity_loads(CodeOutStream &ost, unsigned index, unsigned max_specs, bool uniform)
{
  // Temperature load
  {
    char dst[128];
    sprintf(dst,"%s[%d]",TEMPERATURE,index);
    emit_cuda_load(ost,dst,TEMPERATURE_ARRAY,unit->use_double2,".ca");
  }
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    char dst_offset[128];
    sprintf(dst_offset,"%d][%d",idx,index);
    char src_offset[128];
    sprintf(src_offset,"(wid+%d)*spec_stride",(idx*unit->threads_per_point));
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
    emit_cuda_load(ost,MOLE_FRAC,dst_offset,MOLE_FRAC_ARRAY,src_offset,unit->use_double2,".cg");
  }
  ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
  ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
}

void TranLib::emit_specialized_conductivity_step(CodeOutStream &ost, unsigned index, unsigned max_specs, bool uniform, bool next_loads)
{
  const unsigned num_species = (unit->species.size()-1);
  ost << "// Iteration step + " << index << "\n"; 
  PairDelim step_pair(ost);
  if (unit->use_double2)
  {
    ost << REAL2 << " logt = make_double2(log(" << TEMPERATURE << "[" << index << "].x), log("
        << TEMPERATURE << "[" << index << "].y));\n";
    ost << REAL2 << " sum = make_double2(0.0,0.0);\n";
    ost << REAL2 << " sumr = make_double2(0.0,0.0);\n";
  }
  else
  {
    ost << REAL << " logt = log(" << TEMPERATURE << "[" << index << "]);\n";
    ost << REAL << " sum = 0.0;\n";
    ost << REAL << " sumr = 0.0;\n";
  }
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
    PairDelim spec_pair(ost);
#ifdef CONDUCTIVITY_SHUFFLE
    ost << INT << " hi_part, lo_part;\n";
    unsigned coeff_index = idx*order + 0;
    unsigned lane_index = coeff_index % 32;
    unsigned thread_index = coeff_index / 32;
    ost << "lo_part = __shfl(__double2loint(conduc_constants[" << thread_index << "]), " << lane_index << ");\n";
    ost << "hi_part = __shfl(__double2hiint(conduc_constants[" << thread_index << "]), " << lane_index << ");\n";
#endif
    if (unit->use_double2)
    {
#ifdef CONDUCTIVITY_SHUFFLE
      ost << REAL << " coeff0 = __hiloint2double(hi_part,lo_part);\n";  
#else
      ost << REAL << " coeff0 = conductivity_constants[wid+" << (idx*unit->threads_per_point) << "][0];\n";
#endif
      ost << REAL2 << " val = make_double2(coeff0,coeff0);\n";
      for (int i = 1; i < order; i++)
      {
#ifdef CONDUCTIVITY_SHUFFLE
        coeff_index = idx*order + i;
        lane_index = coeff_index % 32;
        thread_index = coeff_index / 32;   
        ost << "lo_part = __shfl(__double2loint(conduc_constants[" << thread_index << "]), " << lane_index << ");\n";
        ost << "hi_part = __shfl(__double2hiint(conduc_constants[" << thread_index << "]), " << lane_index << ");\n";   
        ost << REAL << " coeff" << i << " = __hiloint2double(hi_part,lo_part);\n";
#else
        ost << REAL << " coeff" << i << " = conductivity_constants[wid+" << (idx*unit->threads_per_point) << "][" << i << "];\n";
#endif
        ost << "val = make_double2( "
            << "__fma_rn(val.x,logt.x,coeff" << i << "), "
            << "__fma_rn(val.y,logt.y,coeff" << i << "));\n";
      }
      ost << REAL2 << " sum_val = val;\n";
      ost << REAL2 << " sumr_val = make_double2(-val.x,-val.y);\n";
    }
    else
    {
#ifdef CONDUCTIVITY_SHUFFLE
      ost << REAL << " val = __hiloint2double(hi_part,lo_part);\n";
#else
      ost << REAL << " val = conductivity_constants[wid+" << (idx*unit->threads_per_point) << "][0];\n";
#endif
      for (int i = 1; i < order; i++)
      {
#ifdef CONDUCTIVITY_SHUFFLE
        coeff_index = idx*order + i;
        lane_index = coeff_index % 32;
        thread_index = coeff_index / 32;   
        ost << "lo_part = __shfl(__double2loint(conduc_constants[" << thread_index << "]), " << lane_index << ");\n";
        ost << "hi_part = __shfl(__double2hiint(conduc_constants[" << thread_index << "]), " << lane_index << ");\n";
        ost << "val = __fma_rn(val,logt,__hiloint2double(hi_part,lo_part));\n";
#else
        ost << "val = __fma_rn(val,logt,conductivity_constants[wid+" << (idx*unit->threads_per_point) << "][" << i << "]);\n";
#endif
      }
      ost << REAL << " sum_val = val;\n";
      ost << REAL << " sumr_val = -val;\n";
    }
    // Note we do 2 exponentials here rather than an exponential and a divide.
    // This costs us 5 additional FMAs but we're memory latency limited so it
    // shouldn't matter. More importantly we avoid the CAL instruction issued
    // by the compiler to perform the divide which is death for having
    // lots of outstanding LDGs in flight to hide the memory latency.
    if (unit->fast_math)
    {
      emit_gpu_exp_taylor_series_expansion(ost, "sum_val", unit->taylor_stages, unit->use_double2);
      emit_gpu_exp_taylor_series_expansion(ost, "sumr_val", unit->taylor_stages, unit->use_double2);
    }
    else
    {
      if (unit->use_double2)
      {
        ost << "sum_val = make_double2(exp(sum_val.x),exp(sum_val.y));\n";
        ost << "sumr_val = make_double2(exp(sumr_val.x),exp(sumr_val.y));\n";
      }
      else
      {
        ost << "sum_val = exp(sum_val);\n";
        ost << "sumr_val = exp(sumr_val);\n";
      }
    }
    if (unit->use_double2)
    {
      ost << "sum = make_double2( "
          << "__fma_rn(" << MOLE_FRAC << "[" << idx << "][" << index << "].x,sum_val.x,sum.x), "
          << "__fma_rn(" << MOLE_FRAC << "[" << idx << "][" << index << "].y,sum_val.y,sum.y));\n";
      ost << "sumr = make_double2( "
          << "__fma_rn(" << MOLE_FRAC << "[" << idx << "][" << index << "].x,sumr_val.x,sumr.x), "
          << "__fma_rn(" << MOLE_FRAC << "[" << idx << "][" << index << "].y,sumr_val.y,sumr.y));\n";
    }
    else
    {
      ost << "sum = __fma_rn(" << MOLE_FRAC << "[" << idx << "][" << index << "],sum_val,sum);\n";
      ost << "sumr = __fma_rn(" << MOLE_FRAC << "[" << idx << "][" << index << "],sumr_val,sumr);\n";
    }
  }
  // Now issue the reduction after everyone has finished their elements
  if (unit->use_double2)
  {
    ost << "reduc_buffer[0][wid][0][tid] = sum.x;\n";
    ost << "reduc_buffer[0][wid][1][tid] = sum.y;\n";
    ost << "reduc_buffer[1][wid][0][tid] = sumr.x;\n";
    ost << "reduc_buffer[1][wid][1][tid] = sumr.y;\n";
  }
  else
  {
    ost << "reduc_buffer[0][wid][tid] = sum;\n";
    ost << "reduc_buffer[1][wid][tid] = sumr;\n";
  }
  // synchronize
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  {
    ost << "if (wid == 0)\n";
    PairDelim if_pair(ost);
    {
      ost << "#pragma unroll\n";
      ost << "for (int i = 1; i < " << unit->threads_per_point << "; i++)\n";
      PairDelim loop_pair(ost);
      if (unit->use_double2)
      {
        ost << "sum.x += reduc_buffer[0][wid][0][tid];\n";
        ost << "sum.y += reduc_buffer[0][wid][1][tid];\n";
        ost << "sumr.x += reduc_buffer[1][wid][0][tid];\n";
        ost << "sumr.y += reduc_buffer[1][wid][1][tid];\n";
      }
      else
      {
        ost << "sum += reduc_buffer[0][i][tid];\n";
        ost << "sumr += reduc_buffer[1][i][tid];\n";
      }
    }
    // Kind of tricky here, do this in log space to avoid any calls that might 
    // screw up the LDGs, so rather than do a divide, we'll use exp(-log(x)) 
    // to approximate 1/x
    if (unit->use_double2)
    {
      ost << "sumr = make_double2(log(sumr.x),log(sumr.y));\n";
      ost << "sumr = make_double2(-sumr.x,-sumr.y);\n";
    }
    else
    {
      ost << "sumr = log(sumr);\n";
      ost << "sumr = -sumr;\n";
    }
    if (unit->fast_math)
      emit_gpu_exp_taylor_series_expansion(ost, "sumr", unit->taylor_stages, unit->use_double2);
    else
    {
      if (unit->use_double2)
        ost << "sumr = make_double2(exp(sumr.x),exp(sumr.y));\n";
      else
        ost << "sumr = exp(sumr);\n";
    }
    // Do the final output
    if (unit->use_double2)
    {
      ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(conductivity), "
          << "\"d\"(0.5*(sum.x + sumr.x)), "
          << "\"d\"(0.5*(sum.y + sumr.y)) : \"memory\");\n";
    }
    else
    {
      ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(conductivity), "
          << "\"d\"(0.5*(sum + sumr)) : \"memory\");\n";
    }
  }
  // update our output address
  ost << "conductivity += slice_stride;\n";
  // emit our next batch of loads if necessary
  if (next_loads)
  {
    emit_specialized_conductivity_loads(ost, index, max_specs, uniform);
  }
  // final synchronization to say where' done with this step
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
}

void TranLib::emit_specialized_conductivity_declaration(CodeOutStream &ost)
{
  ost << "__global__ void\n";
  ost << "gpu_conductivity(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "conductivity" << ")";
}

void TranLib::emit_specialized_viscosity(CodeOutStream &ost, std::vector<std::map<Species*,unsigned> > &cuts,
                                         unsigned max_specs, bool uniform)
{
#ifndef WARP_SPECIALIZATION
  // Emit constant declarations for all the constants that we need
  // neta values first
  bool fits_in_constant_cache = true;
  { 
    // Figure out whether it actually fits in constant cache
    if (fits_in_constant_cache)
    {
      size_t total_constant_bytes = (unit->species.size()-1)*order*sizeof(REAL_TYPE);
      total_constant_bytes += (unit->species.size()-1)*(unit->species.size()-2)*sizeof(REAL_TYPE);
      total_constant_bytes += (unit->species.size()-1)*(unit->species.size()-2)*sizeof(REAL_TYPE);
      if (total_constant_bytes > 65536)
      {
        fprintf(stderr,"INFO: total constant size for viscosity is %ld, which no longer fits "
                        "in constant cache.  Moving to global memory.", total_constant_bytes);
        fits_in_constant_cache = false;
      }
    }

    ost << (fits_in_constant_cache ? "__constant__ " : "__device__ const ")
        << REAL << " viscosity_neta[" << (unit->species.size()-1)
        << "][" << order << "] = {";
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      ost << "{";
      for (int i = order; i > 0; i--)
      {
        if (i == order)
          ost << (neta[spec_idx*order+i-1]);
        else
          ost << ", " << (neta[spec_idx*order+i-1]);
      }
      ost << "},";
      spec_idx++;
    }
    ost << "};\n\n";
    ost << (fits_in_constant_cache ? "__constant__ " : "__device__ const ")
        << REAL << " viscosity_mass_ratios[" << (unit->species.size()-1)
        << "][" << (unit->species.size()-2) << "] = {";
    for (unsigned i = 0; i < (unit->species.size()-1); i++)
    {
      ost << "{";
      bool first = true;
      for (unsigned j = 0; j < (unit->species.size()-1); j++)
      {
        if (i == j)
          continue;
        if (first)
        {
          first = false;
#ifdef VISC_MATH
          ost << (log(sqrt(nwt[j]/nwt[i])));
#else
          ost << (sqrt(nwt[j]/nwt[i]));
#endif
        }
        else
        {
#ifdef VISC_MATH
          ost << ", " << (log(sqrt(nwt[j]/nwt[i])));
#else
          ost << ", " << (sqrt(nwt[j]/nwt[i]));
#endif
        }
      }
      ost << "},";
    }
    ost << "};\n\n";
    ost << (fits_in_constant_cache ? "__constant__ " : "__device__ const ")
        << REAL << " viscosity_factor_prefix[" << (unit->species.size()-1)
        << "][" << (unit->species.size()-2) << "] = {";
    for (unsigned i = 0; i < (unit->species.size()-1); i++)
    {
      ost << "{";
      bool first = true;
      for (unsigned j = 0; j < (unit->species.size()-1); j++)
      {
        if (i == j)
          continue;
        if (first)
        {
          first = false;
          ost << (1.0/sqrt(1.0+(nwt[i]/nwt[j])));
        }
        else
        {
          ost << ", " << (1.0/sqrt(1.0+(nwt[i]/nwt[j])));
        }
      }
      ost << "},";
    }
    ost << "};\n\n";
  }
#endif
  assert(!cuts.empty());
  emit_specialized_viscosity_declaration(ost, false/*bounds*/);
  ost << "\n";
  PairDelim pair(ost);
  // POINTS_PER_THREAD==32 everywhere throughout this kernel
  // since we are doing warp-specialization
  // compute the offset for this block
  // Compute our warp-id and thread-id within the warp
  ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
  ost << "const " << INT << " wid = threadIdx.x >> 5;\n";
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*1)*row_stride + "
        << "(blockIdx.x*32 + tid)*sizeof("
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ")"
        << "+blockIdx.z*slice_stride;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
    ost << "viscosity += offset;\n";
  }
  if (unit->use_double2)
    ost << "volatile __shared__ " << REAL << " spec_visc[" << (unit->species.size()-1) << "][2][32];\n";
  else
    ost << "volatile __shared__ " << REAL << " spec_visc[" << (unit->species.size()-1) << "][32];\n";
  if (unit->visc_shared)
  {
    // Mark these as volatile so the compiler doesn't do
    // something stupid like try to cache them in registers
    if (unit->use_double2)
      ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << (unit->species.size()-1) << "][2][32];\n";
    else
      ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << (unit->species.size()-1) << "][32];\n";
  }
  // First load the temperature
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, "0", unit->use_double2, ".cs"); 
#ifndef WARP_SPECIALIZATION
  // Start prefetching some of our constants if they are in global memory
  int buffered_depth = 0;
  if (!fits_in_constant_cache)
  {
    // Prefetch to the buffering depth
    ost << REAL << " neta_prefetch[" << (MIN(CONSTANT_BUFFERING,max_specs)) << "][" << order << "];\n";
    for (unsigned idx = 0; idx < (MIN(CONSTANT_BUFFERING,max_specs)); idx++)
    {
      // Emit a check if necessary
      if (!uniform && (buffered_depth == int(max_specs-1)))
        ost << "if ((wid+" << (buffered_depth*unit->threads_per_point) << ") < "
            << (unit->species.size()-1) << ")\n";
      PairDelim prefetch_pair(ost);
      for (int i = 0; i < order; i++)
      {
        char dst_offset[128];
        sprintf(dst_offset,"%d][%d",buffered_depth,i);
        char src_offset[128];
        sprintf(src_offset,"(wid+%d)*%d+%d",(buffered_depth*unit->threads_per_point),
                order, i);
        emit_cuda_load(ost,"neta_prefetch",dst_offset,"viscosity_neta",src_offset,false/*double2*/,".ca");
      }
      buffered_depth++;
    }
  }
#endif
  // Emit the loads for the mole fractions
  // Won't need them for a while
  if (!unit->visc_shared)
  {
    ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "[" << (unit->species.size()-1) << "];\n";
    unsigned spec_idx = 0;
    for (std::vector<Species*>::const_iterator it = unit->ordered_species.begin();
          it != unit->ordered_species.end(); it++)
    {
      if (strcmp((*it)->name,"M") == 0)
        continue;
      char dst_offset[128];
      sprintf(dst_offset,"%d",spec_idx);
      char src_offset[128];
      sprintf(src_offset,"%d*spec_stride",spec_idx);
      // Since we're trying to cache constants in L2 mark this as streaming so it doesn't pollute
      // the L2 cache too much
      emit_cuda_load(ost,MOLE_FRAC,dst_offset,MOLE_FRAC_ARRAY,src_offset,unit->use_double2,".cs");
      spec_idx++;
    }
  }
  // Otherwise we'll do the loads in the warp specialized part
  // Compute logt 
  
  if (unit->use_double2)
    ost << REAL2 << " logt = make_double2(log(" << TEMPERATURE << ".x),log(" << TEMPERATURE << ".y));\n";
  else
    ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
#ifdef WARP_SPECIALIZATION
  ost << "// Warp-specialization\n";
  for (unsigned cut_idx = 0; cut_idx < cuts.size(); cut_idx++)
#else
  const unsigned cut_idx = 0;
#endif
  {
#ifdef WARP_SPECIALIZATION
    if (cut_idx == 0)
      ost << "if (wid == 0)\n";
    else
      ost << "else if (wid == " << cut_idx << ")\n";
    PairDelim cut_pair(ost);
#endif
    // If we put the mole fractions in shared memory move our loads here
    if (unit->visc_shared)
    {
      ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "_temp[" << (cuts[cut_idx].size()) << "];\n";
      unsigned spec_idx = 0;
      char dst[128];
      sprintf(dst,"%s_temp",MOLE_FRAC);
      for (std::map<Species*,unsigned>::const_iterator it = cuts[cut_idx].begin();
            it != cuts[cut_idx].end(); /*upgrade it in the loop*/)
      {
        char dst_offset[128];
        sprintf(dst_offset,"%d",spec_idx);
        char src_offset[128];
#ifdef WARP_SPECIALIZATION
        sprintf(src_offset,"%d*spec_stride",it->second);
        it++;
#else
        sprintf(src_offset,"(%d+wid)*spec_stride",it->second);
        // If we're not doing warp-specialization, check to see if
        // we need a condition on the last load
        unsigned last_spec = it->second;
        it++;
        if ((it == cuts[cut_idx].end()) && !uniform)
          ost << "if ((wid+" << last_spec << ") < " << (unit->species.size()-1) << ")\n";
#endif
        // Emit this with cache all since it is likely to be reused
        emit_cuda_load(ost,dst,dst_offset,MOLE_FRAC_ARRAY,src_offset,unit->use_double2,".ca");
        spec_idx++;
      }
    }
#ifndef WARP_SPECIALIZATION
    unsigned last_species_check = 0;
#endif
    // For each species in the cut  
    for (std::map<Species*,unsigned>::const_iterator it = cuts[cut_idx].begin();
          it != cuts[cut_idx].end(); it++)
    {
#ifdef WARP_SPECIALIZATION
      ost << "// Viscosity for " << it->first->name << "\n";
#else
      // If not uniform, check to see if we have to put a check
      // on this species (only needs to be done on last species)
      if (!uniform)
      {
        if (last_species_check == (cuts[cut_idx].size()-1))
          ost << "if ((wid+" << it->second << ") < " << (unit->species.size()-1) << ")\n";
        else
          last_species_check++;
      }
#endif
      PairDelim spec_pair(ost);
#ifdef WARP_SPECIALIZATION
      if (unit->use_double2)
        ost << REAL2 << " val = make_double2(" << neta[(it->second+1)*order-1] << "," << neta[(it->second+1)*order-1] << ");\n";
      else
        ost << REAL << " val = " << neta[(it->second+1)*order-1] << ";\n";
#else
      if (fits_in_constant_cache)
      {
        if (unit->use_double2)
          ost << REAL2 << " val = make_double2(viscosity_neta[wid+" << it->second << "][0], "
              << "viscosity_neta[wid+" << it->second << "][0]);\n";
        else
          ost << REAL << " val = viscosity_neta[wid+" << it->second << "][0];\n";
      }
      else
      {
        if (unit->use_double2)
          ost << REAL2 << " val = make_double2(neta_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,max_specs)) << "][0], "
              << "neta_prefetch[" << (buffered_depth%CONSTANT_BUFFERING) << "][0]);\n";
        else
          ost << REAL << " val = neta_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,max_specs)) << "][0];\n";
      }
#endif
      for (int i = (order-1); i > 0; i--)
      {
#ifdef WARP_SPECIALIZATION
        if (unit->use_double2)
          ost << "val = make_double2( "
              << "__fma_rn(val.x,logt.x," << neta[it->second*order+i-1] << "), "
              << "__fma_rn(val.y,logt.y," << neta[it->second*order+i-1] << "));\n";
        else
          ost << "val = __fma_rn(val,logt," << neta[it->second*order+i-1] << ");\n";
#else
        if (fits_in_constant_cache)
        {
          if (unit->use_double2)
            ost << "val = make_double2( "
                << "__fma_rn(val.x,logt.x,viscosity_neta[wid+" << it->second << "][" << (order-i) << "]), "
                << "__fma_rn(val.y,logt.y,viscosity_neta[wid+" << it->second << "][" << (order-i) << "]));\n";
          else
            ost << "val = __fma_rn(val,logt,viscosity_neta[wid+" << it->second << "][" << (order-i) << "]);\n";
        }
        else
        {
          if (unit->use_double2)
            ost << "val = make_double2( "
                << "__fma_rn(val.x,logt.x,neta_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,max_specs)) << "][" << (order-i) << "]), "
                << "__fma_rn(val.y,logt.y,neta_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,max_specs)) << "][" << (order-i) << "]));\n";
          else
            ost << "val = __fma_rn(val,logt,neta_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,max_specs)) << "][" << (order-i) << "]);\n";
        }
#endif
      }
      // Hold off on the exponentials for now, they'll get done later,
      // we'll do O(n^2) of them instead of O(n) but it means we'll
      // get rid of O(n^2) sqrt and divisions. Woot!
#ifdef VISC_MATH
      if (unit->use_double2)
      {
        ost << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
            << "wid+"
#endif
            << it->second << "][0][tid] = val.x;\n";
        ost << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
            << "wid+"
#endif
            << it->second << "][1][tid] = val.y;\n";
      }
      else
        ost << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
            << "wid+"
#endif
            << it->second << "][tid] = val;\n";
#else
      if (unit->fast_math)
      {
        emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, unit->use_double2);
        if (unit->use_double2)
        {
          ost << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][0][tid] = val.x;\n";
          ost << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][1][tid] = val.y;\n";
        }
        else
          ost << "spec_visc[" << it->second << "][tid] = val;\n";
      }
      else
      {
        if (unit->use_double2)
        { 
          ost << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][0][tid] = exp(val.x);\n";
          ost << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][1][tid] = exp(val.y);\n";
        }
        else
          ost << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][tid] = exp(val);\n";
      }
#endif
#ifndef WARP_SPECIALIZATION
      // See if we need to issue any more prefetches for constant values
      if (!fits_in_constant_cache && (buffered_depth < int(max_specs)))
      {
        // Check to see if it is the last one
        if (buffered_depth == int(max_specs-1))
          ost << "if ((wid+" << (buffered_depth*unit->threads_per_point) << ") < "
              << (unit->species.size()-1) << ")\n";
        PairDelim prefetch_pair(ost);
        for (int i = 0; i < order; i++)
        {
          char dst_offset[128];
          sprintf(dst_offset,"%d][%d",(buffered_depth%CONSTANT_BUFFERING),i);
          char src_offset[128];
          sprintf(src_offset,"(wid+%d)*%d+%d",(buffered_depth*unit->threads_per_point),
                  order, i);
          emit_cuda_load(ost,"neta_prefetch",dst_offset,"viscosity_neta",src_offset,false/*double2*/,".ca"); 
        }
      }
      // Keep this outside the if-condition, so we can get
      // the loads from the buffer correct
      buffered_depth++;
#endif
    }
    // If we loaded our species into temporary registers, write them into shared
    if (unit->visc_shared)
    {
      unsigned spec_idx = 0;
#ifndef WARP_SPECIALIZATION
      last_species_check = 0;
#endif
      for (std::map<Species*,unsigned>::const_iterator it = cuts[cut_idx].begin();
            it != cuts[cut_idx].end(); it++)
      {
#ifndef WARP_SPECIALIZATION
        if (!uniform)
        {
          if (last_species_check == (cuts[cut_idx].size()-1))
            ost << "if ((wid+" << it->second << ") < " << (unit->species.size()-1) << ")\n";
          else
            last_species_check++;
        }
#endif
        if (unit->use_double2)
        {
          PairDelim two_pair(ost);
          ost << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][0][tid] = " << MOLE_FRAC << "_temp[" << spec_idx << "].x;\n";
          ost << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][1][tid] = " << MOLE_FRAC << "_temp[" << spec_idx << "].y;\n";
        }
        else
          ost << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][tid] = " << MOLE_FRAC << "_temp[" << spec_idx << "];\n";
        spec_idx++;
      }
    }
    // Emit a syncthreads to say that we're done writing into shared
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    ost << (unit->use_double2 ? REAL2 : REAL) << " result = "
        << (unit->use_double2 ? "make_double2(0.0,0.0)" : "0.0") << ";\n";
#ifndef WARP_SPECIALIZATION
    last_species_check = 0; // for checking if we're the last species
#endif
    // For each species in the cut
    for (std::map<Species*,unsigned>::const_iterator it = cuts[cut_idx].begin();
          it != cuts[cut_idx].end(); it++)
    {
#ifdef WARP_SPECIALIZATION
      ost << "// Species " << it->first->name << "\n";    
#else
      // If we're not doing warp specialization, check to see if this is
      // the last species and we need to emit a condition
      if (!uniform)
      {
        if (last_species_check == (cuts[cut_idx].size()-1))
          ost << "if ((wid+" << it->second << ") < " << (unit->species.size()-1) << ")\n";
        else
          last_species_check++;
      }
#endif
      PairDelim spec_pair(ost);
      ost << (unit->use_double2 ? REAL2 : REAL) << " spec_sum = "
          << (unit->use_double2 ? "make_double2(0.0,0.0)" : "0.0") << ";\n";
#ifndef WARP_SPECIALIZATION
      ost << "const " << INT << " point = wid + " << it->second << ";\n";
      ost << "unsigned offset = 0;\n";
      // Also check to see if we need to do prefetching into registers
      // for our constant values
      if (!fits_in_constant_cache)
      {
        buffered_depth = 0; // keep track of our register buffer depth for prefetching constants into registers
        // Prefetch to the buffering depth
        ost << REAL << " ratio_prefetch[" << (MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "];\n";
        ost << REAL << " factor_prefetch[" << (MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "];\n";
        for (unsigned idx = 0; idx < (MIN(CONSTANT_BUFFERING,(unit->species.size()-2))); idx++)
        {
          // No need for a check since we already know we're supposed to be doing this species
          PairDelim prefetch_pair(ost);
          char dst_offset[128];
          sprintf(dst_offset,"%d",buffered_depth);
          char src_offset[128];
          sprintf(src_offset,"(wid+%d)*%ld+%d",it->second,
                  (unit->species.size()-2), buffered_depth);
          emit_cuda_load(ost,"ratio_prefetch",dst_offset,"viscosity_mass_ratios",src_offset,false/*double2*/,".ca");
          emit_cuda_load(ost,"factor_prefetch",dst_offset,"viscosity_factor_prefix",src_offset,false/*double2*/,".ca");
          buffered_depth++;
        }
      }
#endif
      // If our viscosity is in shared memory, read it out now since
      // we can cache this locally in registers to avoid doing lots of reads from shared
      if (unit->visc_shared)
      {
        // Emit two or more shared memory loads back-to-back so
        // we get shared memory pipelining
        if (unit->use_double2)
        {
          ost << REAL2 << " local_visc = make_double2("
              << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][0][tid], "
              << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][1][tid]);\n"; 
        }
        else
        {
          ost << REAL << " local_visc = spec_visc[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "][tid];\n"; 
        }
      }
      for (int idx = 0; idx < num_species; idx++)
      {
        // We'll do our own contribution after the loop in
        // case we want to cache our mole fraction after
        // reading it out of shared memory
        if (idx == int(it->second))
          continue;
        PairDelim inner_pair(ost);
#ifndef WARP_SPECIALIZATION
        // Need to update our offset
        ost << "if (point == " << (idx > int(it->second) ? idx-1 : idx) << ") offset = 1;\n";
#endif
        // Since we didn't do the exponentials before, we can do them now
        // Here is the transformation
        // sqrt(c*exp(x)/exp(y)) = exp(1/2*(log(c) + x - y))
        // where c = sqrt(nwt[idx]/nwt[it->second])
        if (unit->use_double2)
        {
          // Hoist shared memory loads so we can wait as long
          // as possible before needing them
          if (unit->visc_shared)
          {
            ost << REAL2 << " visc_" << idx << " = make_double2("
                << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
                << "offset + " << (idx > int(it->second) ? idx-1 : idx)
#else
                << idx
#endif
                << "][0][tid], "
                << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
                << "offset + " << (idx > int(it->second) ? idx-1 : idx)
#else
                << idx 
#endif
                << "][1][tid]);\n";
            ost << REAL2 << " mole_" << idx << " = make_double2("
                << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "offset + " << (idx > int(it->second) ? idx-1 : idx)
#else
                << idx 
#endif
                << "][0][tid], "
                << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "offset + " << (idx > int(it->second) ? idx-1 : idx)
#else
                << idx 
#endif
                << "][1][tid]);\n";
          }
#ifdef VISC_MATH
          if (unit->visc_shared)
          {
            ost << REAL2 << " numer = make_double2( ";
#ifdef WARP_SPECIALIZATION
            ost << "0.5 * (" << (log(sqrt(nwt[idx]/nwt[it->second])));
#else
            if (fits_in_constant_cache)
              ost << "0.5 * (viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]";
            else
              ost << "0.5 * (ratio_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]";
#endif
            ost << " + local_visc.x - visc_" << idx << ".x), ";
#ifdef WARP_SPECIALIZATION
            ost << "0.5 * (" << (log(sqrt(nwt[idx]/nwt[it->second])));
#else
            if (fits_in_constant_cache)
              ost << "0.5 * (viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]";
            else
              ost << "0.5 * (ratio_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]";
#endif
            ost << " + local_visc.y - visc_" << idx << ".y));\n";
          }
          else
          {
            ost << REAL2 << " numer = make_double2( ";
#ifdef WARP_SPECIALIZATION
            ost << "0.5 * (" << (log(sqrt(nwt[idx]/nwt[it->second])));
#else
            if (fits_in_constant_cache)
              ost << "0.5 * (viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]";
            else
              ost << "0.5 * (ratio_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]";
#endif
            ost << " + local_visc.x - spec_visc[";
#ifndef WARP_SPECIALIZATION
            ost << "offset + " << (idx > int(it->second) ? idx-1 : idx);
#else
            ost << idx;
#endif
            ost << "].x), ";
#ifdef WARP_SPECIALIZATION
            ost << "0.5 * (" << (log(sqrt(nwt[idx]/nwt[it->second])));
#else
            if (fits_in_constant_cache)
              ost << "0.5 * (viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]";
            else
              ost << "0.5 * (ratio_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]";
#endif
            ost << " + local_visc.y - spec_visc[";
#ifndef WARP_SPECIALIZATION
            ost << "offset + " << (idx > int(it->second) ? idx-1 : idx);
#else
            ost << idx;
#endif
            ost << "].y));\n";
          }
          if (unit->fast_math)
            emit_gpu_exp_taylor_series_expansion(ost, "numer", unit->taylor_stages, true/*double2*/);
          else
            ost << "numer = make_double2(exp(numer.x), exp(numer.y));\n";
          ost << "numer = make_double2(numer.x+1.0,numer.y+1.0);\n";
#else
          if (unit->visc_shared)
          {
            ost << REAL2 << " numer = make_double2( "
                << "1.0 + sqrt(local_visc.x/visc_" << idx << ".x"
#ifdef WARP_SPECIALIZATION
                << "*" << (sqrt(nwt[idx]/nwt[it->second])) << "), "
#else
                << "*viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]"
#endif
                << "1.0 + sqrt(local_visc.y/visc_" << idx << ".y"
#ifdef WARP_SPECIALIZATION
                << "*" << (sqrt(nwt[idx]/nwt[it->second])) << "));\n"; 
#else
                << "*viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]));\n";
#endif
          }
          else
          {
            ost << REAL2 << " numer = make_double2( "
                << "1.0 + sqrt(local_visc.x/spec_visc[";
#ifndef WARP_SPECIALIZATION
            ost << "offset + " << (idx > int(it->second) ? idx-1 : idx);
#else
            ost << idx;
#endif
            ost << "].x";
#ifdef WARP_SPECIALIZATION
            ost << "*" << (sqrt(nwt[idx]/nwt[it->second])) << "), ";
#else
            if (fits_in_constant_cache)
              ost << "*viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]";
            else
              ost << "*ratio_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]";
#endif
            ost << "1.0 + sqrt(local_visc.y/spec_visc["; 
#ifndef WARP_SPECIALIZATION
            ost << "offset + " << (idx > int(it->second) ? idx-1 : idx);
#else
            ost << idx;
#endif
            ost << "].y";
#ifdef WARP_SPECIALIZATION
            ost << "*" << (sqrt(nwt[idx]/nwt[it->second])) << "));\n";
#else
            if (fits_in_constant_cache)
              ost << "*viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]));\n";
            else
              ost << "*ratio_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]";
#endif
          }
#endif
          ost << REAL2 << " factor = make_double2( "; 
#ifdef WARP_SPECIALIZATION
          ost << (1.0/sqrt(1.0+(nwt[it->second]/nwt[idx]))) << "*numer.x*numer.x, "
              << (1.0/sqrt(1.0+(nwt[it->second]/nwt[idx]))) << "*numer.y*numer.y);\n";
#else
          if (fits_in_constant_cache)
            ost << "viscosity_factor_prefix[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]*numer.x*numer.x, "
                << "viscosity_factor_prefix[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]*numer.y*numer.y);\n";
          else
            ost << "factor_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]*numer.x*numer.x, "
                << "factor_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]*numer.y*numer.y);\n";
#endif
          if (unit->visc_shared)
            ost << "spec_sum = make_double2( "
                << "__fma_rn(factor.x," << "mole_" << idx << ".x,spec_sum.x), "
                << "__fma_rn(factor.y," << "mole_" << idx << ".y,spec_sum.y));\n";
          else
            ost << "spec_sum = make_double2( "
                << "__fma_rn(factor.x," << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "offset + " << (idx > int(it->second) ? idx-1 : idx)
#else
                << idx 
#endif
                << "].x,spec_sum.x), "
                << "__fma_rn(factor.y," << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "offset + " << (idx > int(it->second) ? idx-1 : idx)
#else
                << idx 
#endif
                << "].y,spec_sum.y));\n";
        }
        else
        { 
          if (unit->visc_shared)
          {
            ost << REAL << " visc_" << idx << " = spec_visc[" 
#ifndef WARP_SPECIALIZATION
                << "offset + " << (idx > int(it->second) ? idx-1 : idx)
#else
                << idx 
#endif
                << "][tid];\n";
            ost << REAL << " mole_" << idx << " = " << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "offset + " << (idx > int(it->second) ? idx-1 : idx)
#else
                << idx 
#endif
                << "][tid];\n";
          }
#ifdef VISC_MATH
          if (unit->visc_shared)
          {
            ost << REAL << " numer = 0.5 * ("; 
#ifdef WARP_SPECIALIZATION
            ost << (log(sqrt(nwt[idx]/nwt[it->second]))) << " + ";
#else
            if (fits_in_constant_cache)
              ost << "viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "] + ";
            else
              ost << "ratio_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "] + ";
#endif
            ost << "local_visc - visc_" << idx << ");\n";
          }
          else
          {
            ost << REAL << " numer = 0.5 * ("; 
#ifdef WARP_SPECIALIZATION
            ost << (log(sqrt(nwt[idx]/nwt[it->second]))) << " + ";
#else
            if (fits_in_constant_cache)
              ost << "viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << " + ";
            else
              ost << "ratio_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "] + ";
#endif
            ost << "local_visc - spec_visc["; 
#ifndef WARP_SPECIALIZATION
            ost << "offset + " << (idx > int(it->second) ? idx-1 : idx);
#else
            ost << idx;
#endif
            ost << "]);\n";
          }
          if (unit->fast_math)
            emit_gpu_exp_taylor_series_expansion(ost, "numer", unit->taylor_stages, false/*double2*/);
          else
            ost << "numer = exp(numer);\n";
          ost << "numer += 1.0;\n";
#else
          if (unit->visc_shared)
          {
            ost << REAL << " numer = 1.0 + sqrt(local_visc/visc_" << idx; 
#ifdef WARP_SPECIALIZATION
            ost << "*" << (sqrt(nwt[idx]/nwt[it->second])) << ");\n";
#else
            if (fits_in_constant_cache)
              ost << "*viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]);\n";
            else
              ost << "*ratio_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]);\n";
#endif
          }
          else
          {
            ost << REAL << " numer = 1.0 + sqrt(local_visc/spec_visc["; 
#ifndef WARP_SPECIALIZATION
            ost << "offset + " << (idx > int(it->second) ? idx-1 : idx);
#else
            ost << idx; 
#endif
            ost << "]";
#ifdef WARP_SPECIALIZATION
            ost << "*" << (sqrt(nwt[idx]/nwt[it->second])) << ");\n";
#else
            if (fits_in_constant_cache)
              ost << "*viscosity_mass_ratios[wid+" << it->second << "][" << (idx > int(it->second) ? idx-1 : idx) << "]);\n";
            else
              ost << "*ratio_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]);\n";
#endif

          }
#endif
#ifdef WARP_SPECIALIZATION
          ost << REAL << " factor = " << (1.0/sqrt(1.0+(nwt[it->second]/nwt[idx]))) << "*numer*numer;\n";
#else
          if (fits_in_constant_cache)
            ost << REAL << " factor = viscosity_factor_prefix[wid+" << it->second << "][" 
                << (idx > int(it->second) ? idx-1 : idx) << "]*numer*numer;\n";
          else
            ost << REAL << " factor = factor_prefetch[" << (buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))) << "]"
                << "*numer*numer;\n";
#endif
          if (unit->visc_shared)
            ost << "spec_sum = __fma_rn(factor,mole_" << idx << ",spec_sum);\n";
          else
            ost << "spec_sum = __fma_rn(factor," << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "offset + " << (idx > int(it->second) ? idx-1 : idx)
#else
                << idx 
#endif
                << "],spec_sum);\n";
        }
        // If we're doing prefetching, issue the next set of loads here
#ifndef WARP_SPECIALIZATION
        // See if we need to issue any more prefetches for constant values
        if (!fits_in_constant_cache && (buffered_depth < int(unit->species.size()-2)))
        {
          // Check to see if it is the last one
          if (!uniform && buffered_depth == int(unit->species.size()-2))
            ost << "if ((wid+" << (buffered_depth*unit->threads_per_point) << ") < "
                << (unit->species.size()-1) << ")\n";
          PairDelim prefetch_pair(ost);
          char dst_offset[128];
          sprintf(dst_offset,"%ld",(buffered_depth%MIN(CONSTANT_BUFFERING,(unit->species.size()-2))));
          char src_offset[128];
          sprintf(src_offset,"(wid+%d)*%ld+%d",it->second,
                  (unit->species.size()-2), buffered_depth);
          emit_cuda_load(ost,"ratio_prefetch",dst_offset,"viscosity_mass_ratios",src_offset,false/*double2*/,".ca"); 
          emit_cuda_load(ost,"factor_prefetch",dst_offset,"viscosity_factor_prefix",src_offset,false/*double2*/,".ca");
        }
        // Keep this outside the if-condition, so we can get
        // the loads from the buffer correct
        buffered_depth++;
#endif
      }
      // Do our own contribution
      {
        if (unit->visc_shared)
        {
          if (unit->use_double2)
            ost << REAL2 << " local_mole = make_double2("
                << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "wid+"
#endif
                << it->second << "][0][tid], "
                << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "wid+"
#endif
                << it->second << "][1][tid]);\n";
          else
            ost << REAL << " local_mole = " << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "wid+"
#endif
                << it->second << "][tid];\n";
        }
        PairDelim own_pair(ost);
        // Skip this until the end so we can cache our mole-fraction in registers
        if (unit->use_double2)
        {
          if (unit->visc_shared)
            ost << "spec_sum = make_double2( "
                << "__fma_rn(local_mole.x," << (4.0/sqrt(2.0)) << ",spec_sum.x), "
                << "__fma_rn(local_mole.y," << (4.0/sqrt(2.0)) << ",spec_sum.y));\n";
          else
            ost << "spec_sum = make_double2( "
                << "__fma_rn(" << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "wid+"
#endif
                << it->second << "].x," << (4.0/sqrt(2.0)) << ",spec_sum.x), "
                << "__fma_rn(" << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "wid+"
#endif
                << it->second << "].y," << (4.0/sqrt(2.0)) << ",spec_sum.y));\n";
        }
        else
        {
          if (unit->visc_shared)
            ost << "spec_sum = __fma_rn(local_mole," << (4.0/sqrt(2.0)) << ",spec_sum);\n";
          else
            ost << "spec_sum = __fma_rn(" << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
                << "wid+"
#endif
                << it->second << "]," << (4.0/sqrt(2.0)) << ",spec_sum);\n";
        }
      }
#ifdef VISC_MATH
      if (unit->use_double2)
      {
        if (!unit->visc_shared)
          ost << REAL2 << " local_visc = spec_visc[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "];\n"; 
        if (unit->fast_math)
          emit_gpu_exp_taylor_series_expansion(ost, "local_visc", unit->taylor_stages, true/*double2*/);
        else
          ost << "local_visc = make_double2(exp(local_visc.x),exp(local_visc.y));\n";
        ost << REAL2 << " ratio = make_double2( "
            << "local_visc.x/spec_sum.x, local_visc.y/spec_sum.y);\n";
      }
      else
      {
        if (!unit->visc_shared)
          ost << REAL << " local_visc = spec_visc[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "];\n";
        if (unit->fast_math)
          emit_gpu_exp_taylor_series_expansion(ost, "local_visc", unit->taylor_stages, false/*double2*/);
        else
          ost << "local_visc = exp(local_visc);\n";
        ost << REAL << " ratio = local_visc/spec_sum;\n";
      }
#else
      if (unit->use_double2)
        ost << REAL2 << " ratio = make_double2( "
            << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
            << "wid+"
#endif
            << it->second << "][0][tid]/spec_sum.x, "
            << "spec_visc[" 
#ifndef WARP_SPECIALIZATION
            << "wid+"
#endif
            << it->second << "][1][tid]/spec_sum.y);\n";
      else
        ost << REAL << " ratio = spec_visc[" 
#ifndef WARP_SPECIALIZATION
            << "wid+"
#endif
            << it->second << "][tid]/spec_sum;\n";
#endif
      // Now update the result
      if (unit->use_double2)
      {
        if (unit->visc_shared)
          ost << "result = make_double2( "
              << "__fma_rn(local_mole.x,ratio.x,result.x), "
              << "__fma_rn(local_mole.y,ratio.y,result.y));\n";
        else
          ost << "result = make_double2( "
              << "__fma_rn(" << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "].x,ratio.x,result.x), "
              << "__fma_rn(" << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "].y,ratio.y,result.y));\n";
      }
      else
      {
        if (unit->visc_shared)
          ost << "result = __fma_rn(local_mole,ratio,result);\n";
        else
          ost << "result = __fma_rn(" << MOLE_FRAC << "[" 
#ifndef WARP_SPECIALIZATION
              << "wid+"
#endif
              << it->second << "],ratio,result);\n";
      }
    }
    // Do another syncthreads to say that we're all done reading
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    // Write our results into shared memory
    if (unit->use_double2)
    {
      ost << "spec_visc[wid][0][tid] = result.x;\n";
      ost << "spec_visc[wid][1][tid] = result.y;\n";
    }
    else
      ost << "spec_visc[wid][tid] = result;\n";
    // Mark that we are done writing
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    // Now if we're in the first cut then emit the code to do the last reduction and write out the result
    ost << "if (wid == 0)\n";
    {
      PairDelim if_pair(ost);
      if (unit->use_double2)
        ost << REAL2 << " accum = make_double2(0.0,0.0);\n";
      else
        ost << REAL << " accum = 0.0;\n";
      {
        // Note threads per point is the same as the number of warps
        ost << "#pragma unroll\n";
        ost << "for (int i = 0; i < " << unit->threads_per_point << "; i++)\n";
        PairDelim loop_pair(ost);
        if (unit->use_double2)
        {
          ost << "accum.x += spec_visc[i][0][tid];\n";
          ost << "accum.y += spec_visc[i][1][tid];\n";
        }
        else
          ost << "accum += spec_visc[i][tid];\n";
      }
      // Write out the result
      if (unit->use_double2)
      {
        ost << "accum = make_double2(" << (sqrt(8.0)) << "*accum.x," << (sqrt(8.0)) << "*accum.y);\n";
        ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(viscosity), "
            << "\"d\"(accum.x), "
            << "\"d\"(accum.y) : \"memory\");\n";
      }
      else
      {
        ost << "accum *= " << (sqrt(8.0)) << ";\n";
        ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(viscosity), "
            << "\"d\"(accum) : \"memory\");\n";
      }
    }
  }
  // Update the pointers
  // Need a final synchthreads to make sure that the first warp finished reading
  // before starting the next iteration
  // No need to update the pointers if we're not looping
#if 0
  ost << "__syncthreads();\n";
  ost << "// Update the pointers\n";
  ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
  ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
  ost << "viscosity += slice_stride;\n";
#endif
}

void TranLib::emit_specialized_viscosity_declaration(CodeOutStream &ost, bool bounds)
{
  ost << "__global__ void\n";
  {
    int warps_per_cta = unit->threads_per_point;
    // Figure out how much shared memory this kernel needs
    int shared_memory = 32/*min points*/ * (unit->use_double2 ? 16 : 8)/*bytes per element*/
                        * (unit->species.size()-1)/*number of species*/;
    // If we're putting mole fraction in shared too then double
    // the amount of shared memory we're using
    if (unit->visc_shared)
      shared_memory *= 2;
    if (shared_memory > MAX_SHARED)
    {
      fprintf(stderr,"Too many species to fit in shared memory for warp-specialized viscosity: needed %d bytes\n", shared_memory);
      exit(1);
    }
    // Figure out how many CTAs we can get on an SM
    int ctas_per_sm = MAX_SHARED/shared_memory;
    // Now compute how many warps this will put on an SM
    int total_warps = ctas_per_sm * warps_per_cta;
    fprintf(stdout,"    Warp-Specialized Viscosity Kernel:\n");
    fprintf(stdout,"      Warps-per-CTA:         %d\n", warps_per_cta);
    fprintf(stdout,"      Shared Memory per CTA: %d\n", shared_memory);
    fprintf(stdout,"      CTAs-per-SM:           %d\n", ctas_per_sm);
    fprintf(stdout,"      Warps-per-SM:          %d\n", total_warps);
    if (bounds)
      ost << "__launch_bounds__(" << (32*warps_per_cta) << "," << ctas_per_sm << ")\n";
  }
  ost << "gpu_viscosity(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "viscosity";
  //ost << ", const bool always_false";
  ost << ")";
}

void TranLib::emit_sliced_viscosity(CodeOutStream &ost, std::vector<std::map<Species*,unsigned> > &cuts, unsigned max_specs, bool uniform)
{
  const unsigned num_species = (unit->species.size()-1);
  unsigned warp_constant_stride;
  // Emit the constants for this kernel
  {
    // Always put the neta values in constant cache since they should always fit easily
    ost << "__constant__ " << REAL << " viscosity_neta[" << num_species << "][" << order << "] = {";
    for (unsigned idx = 0; idx < num_species; idx++)
    {
      if (idx == 0)
        ost << " {";
      else
        ost << ", {";
      for (int i = order ; i > 0; i--)
      {
        if (i == order)
          ost << (neta[idx*order+i-1]);
        else
          ost << ", " << (neta[idx*order+i-1]);
      }
      ost << "} ";
    }
    ost << "};\n\n";
    // Figure out parameters of specifying constants
    bool has_remainder = ((num_species%unit->threads_per_point) != 0);
    unsigned num_columns = (num_species/unit->threads_per_point);
    unsigned max_points_per_thread = (num_columns + (has_remainder ? 1 : 0))*(num_species-1);
    warp_constant_stride = (max_points_per_thread);
    // Make everything fit nicely by padding with zeros
    // Also guarantees aligned loads for the threads
    while ((warp_constant_stride%32) != 0)
      warp_constant_stride++;
    // Now do the viscosity mass ratios
    ost << "__device__ const " << REAL << " viscosity_mass_ratios[" << (unit->threads_per_point*warp_constant_stride) << "] = {";
    bool first = true;
    for (unsigned wid = 0; wid < unit->threads_per_point; wid++)
    {
      unsigned num_created = 0;
      for (unsigned col_idx = 0; col_idx < num_columns; col_idx++)
      {
        unsigned column = col_idx*unit->threads_per_point + wid;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (row == column)
            continue;
          if (first)
          {
            first = false;
            ost << (log(sqrt(nwt[row]/nwt[column])));
          }
          else
            ost << ", " << (log(sqrt(nwt[row]/nwt[column]))); 
          num_created++;
        }
      }
      assert(!first);
      if (has_remainder)
      {
        unsigned column = (wid+(num_columns*unit->threads_per_point))%num_species;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (row == column)
            continue;
          ost << ", " << (log(sqrt(nwt[row]/nwt[column])));
          num_created++;
        }
      }
      assert(num_created == max_points_per_thread);
      // Pad everything else with zeros
      while (num_created < warp_constant_stride)
      {
        ost << ", 0.0";
        num_created++;
      }
    }
    ost << "};\n\n";
    // Finally do the viscosity factor prefixes
    first = true;
    ost << "__device__ const " << REAL << " viscosity_factor_prefix[" << (unit->threads_per_point*warp_constant_stride) << "] = {";
    for (unsigned wid = 0; wid < unit->threads_per_point; wid++)
    {
      unsigned num_created = 0;
      for (unsigned col_idx = 0; col_idx < num_columns; col_idx++)
      {
        unsigned column = col_idx*unit->threads_per_point + wid;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (row == column)
            continue;
          if (first)
          {
            first = false;
            ost << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
          }
          else
            ost << ", " << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
          num_created++;
        }
      }
      assert(!first);
      if (has_remainder)
      {
        unsigned column = (wid+(num_columns*unit->threads_per_point))%num_species;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (row == column)
            continue;
          ost << ", " << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
          num_created++;
        }
      }
      assert(num_created == max_points_per_thread);
      // Pad everything else with zeros
      while (num_created < warp_constant_stride)
      {
        ost << ", 0.0";
        num_created++;
      }
    }
    ost << "};\n\n";
  }
  emit_sliced_viscosity(ost, false/*bounds*/);
  ost << "\n";
  PairDelim pair(ost);
  // POINTS_PER_THREAD == 32 everywhere throughout this kernel
  ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
  ost << "const " << INT << " wid = threadIdx.x >> 5;\n";
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*1)*row_stride + "
        << "(blockIdx.x*32 + tid)*sizeof("
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ")"
        << "+blockIdx.z*slice_stride;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
    ost << "viscosity += offset;\n";
  }
  if (unit->use_double2)
    ost << "volatile __shared__ " << REAL << " spec_visc[" << num_species << "][2][32];\n";
  else
    ost << "volatile __shared__ " << REAL << " spec_visc[" << num_species << "][32];\n";
  // Mark these as volatile so the compiler doesn't do
  // something stupid like try to cache them in registers
  if (unit->use_double2)
    ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << num_species << "][2][32];\n";
  else
    ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << num_species << "][32];\n";

  // Emit loads for our constants
  const unsigned constants_per_thread = ((num_species-1)*max_specs+31)/32;
  assert(warp_constant_stride == (32*constants_per_thread));
  ost << REAL << " mass_ratios[" << constants_per_thread << "];\n";
  ost << REAL << " factor_prefix[" << constants_per_thread << "];\n";
  {
    PairDelim load_pair(ost);
    ost << "const " << INT << " offset = wid*" << warp_constant_stride << "+tid;\n";
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"offset+%d",(idx*32));
      emit_cuda_load(ost,"mass_ratios",dst_offset,"viscosity_mass_ratios",src_offset,false/*double2*/);
      emit_cuda_load(ost,"factor_prefix",dst_offset,"viscosity_factor_prefix",src_offset,false/*double2*/);
    }
  }
  // Forward declarations
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "_temp[" << max_specs << "];\n";
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << ";\n";
  // Emit the set of loads for the first loop
#if 0
  emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, "0", unit->use_double2);
  // Issue loads for our mole fractions
  {
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      char dst[128];
      sprintf(dst,"%s_temp",MOLE_FRAC);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride",(idx*unit->threads_per_point));
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      emit_cuda_load(ost,dst,dst_offset,MOLE_FRAC_ARRAY,src_offset,unit->use_double2,".cg");
    }
  }
  // Step the pointers
  ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
  ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
#endif

  // emit the main loop
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim step_loop_pair(ost);

  // Emit the set of loads for the first loop
  emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, "0", unit->use_double2);
  // Issue loads for our mole fractions
  {
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      char dst[128];
      sprintf(dst,"%s_temp",MOLE_FRAC);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride",(idx*unit->threads_per_point));
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      emit_cuda_load(ost,dst,dst_offset,MOLE_FRAC_ARRAY,src_offset,unit->use_double2,".cg");
    }
  }

  // Compute logt
  if (unit->use_double2)
    ost << REAL2 << " logt = make_double2(log(" << TEMPERATURE << ".x),log(" << TEMPERATURE << ".y));\n";
  else
    ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  // Compute the species specific viscosities
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
    PairDelim spec_pair(ost);
    // Do some fancy math to make sure the compiler reloads this value and doesn't hoist it
    // out of the loop to consume registers
    ost << "const " << INT << " neta_index = (wid+" << (idx*unit->threads_per_point) << "+step*"
        << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2) << ";\n";
    if (unit->use_double2)
    {
      //ost << REAL << " coeff0 = viscosity_neta[wid+" << (idx*unit->threads_per_point) << "][0];\n";
      ost << REAL << " coeff0 = viscosity_neta[neta_index][0];\n";
      ost << REAL2 << " val = make_double2(coeff0,coeff0);\n";
      for (int i = (order-1); i > 0; i--)
      {
        //ost << REAL << " coeff" << i << " = viscosity_neta[wid+" << (idx*unit->threads_per_point) << "][" << (order-i) << "];\n";
        ost << REAL << " coeff" << i << " = viscosity_neta[neta_index][" << (order-i) << "];\n";
        ost << "val = make_double2( "
            << "__fma_rn(val.x,logt.x,coeff" << i << "), __fma_rn(val.y,logt.y,coeff" << i << "));\n";
      }
      ost << "spec_visc[wid+" << (idx*unit->threads_per_point) << "][0][tid] = val.x;\n";
      ost << "spec_visc[wid+" << (idx*unit->threads_per_point) << "][1][tid] = val.y;\n";
    }
    else
    {
      //ost << REAL << " val = viscosity_neta[wid+" << (idx*unit->threads_per_point) << "][0];\n";
      ost << REAL << " val = viscosity_neta[neta_index][0];\n";
      for (int i = (order-1); i > 0; i--)
      {
        //ost << "val = __fma_rn(val,logt,viscosity_neta[wid+" << (idx*unit->threads_per_point) << "][" << (order-i) << "]);\n";
        ost << "val = __fma_rn(val,logt,viscosity_neta[neta_index][" << (order-i) << "]);\n";
      }
      ost << "spec_visc[wid+" << (idx*unit->threads_per_point) << "][tid] = val;\n";
    }
  }
  // Write out our species mole fractions into shared memory
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
    if (unit->use_double2)
    {
      // need a pair delim here
      PairDelim spec_pair(ost);
      ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][0][tid] = " << MOLE_FRAC << "_temp[" << idx << "].x;\n";
      ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][1][tid] = " << MOLE_FRAC << "_temp[" << idx << "].y;\n";
    }
    else
    {
      ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid] = " << MOLE_FRAC << "_temp[" << idx << "];\n";
    }
  }
  // Emit code to issue the next batch of loads
#if 0
  {
    ost << "if (step < (total_steps-1))\n";
    PairDelim load_pair(ost);
    emit_cuda_load(ost,TEMPERATURE,TEMPERATURE_ARRAY,"0",unit->use_double2);
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      char dst[128];
      sprintf(dst,"%s_temp",MOLE_FRAC);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride",(idx*unit->threads_per_point));
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      emit_cuda_load(ost,dst,dst_offset,MOLE_FRAC_ARRAY,src_offset,unit->use_double2,".cg");
    }
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
  }
#endif
  // Synchronize to say that everything important is in shared memory
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  // Now do the code to compute the output coefficient for each point
  if (unit->use_double2)
    ost << REAL2 << " result = make_double2(0.0,0.0);\n";
  else
    ost << REAL << " result = 0.0;\n";
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
    PairDelim idx_pair(ost);
    if (unit->use_double2)
      ost << REAL2 << " spec_sum = make_double2(0.0,0.0);\n";
    else
      ost << REAL << " spec_sum = 0.0;\n";
    ost << "const " << INT << " point = wid + " << (idx*unit->threads_per_point) << ";\n";
    ost << "unsigned offset = 0;\n";
    if (unit->use_double2)
      ost << REAL2 << " local_visc = make_double2( "
          << "spec_visc[point][0][tid], "
          << "spec_visc[point][1][tid]);\n";
    else
      ost << REAL << " local_visc = spec_visc[point][tid];\n";
    for (unsigned spec_idx = 0; spec_idx < (num_species-1); spec_idx++)
    {
      PairDelim spec_pair(ost);
      ost << "if (point == " << spec_idx << ") offset = 1;\n";
      const unsigned coeff_index = idx*(num_species-1) + spec_idx;
      const unsigned lane_index = coeff_index % 32;
      const unsigned thread_index = coeff_index / 32;
      if (unit->use_double2)
      {
        ost << REAL2 << " visc_" << spec_idx << " = make_double2("
            << "spec_visc[offset + " << spec_idx << "][0][tid], spec_visc[offset + " << spec_idx << "][1][tid]);\n";
        ost << REAL2 << " mole_" << spec_idx << " = make_double2("
            << MOLE_FRAC << "[offset + " << spec_idx << "][0][tid], "
            << MOLE_FRAC << "[offset + " << spec_idx << "][1][tid]);\n";
        ost << INT << " hi_part, lo_part;\n";
        ost << "lo_part = __shfl(__double2loint(mass_ratios[" << thread_index << "]), " << lane_index << ");\n";
        ost << "hi_part = __shfl(__double2hiint(mass_ratios[" << thread_index << "]), " << lane_index << ");\n";
        ost << REAL2 << " numer = make_double2( "
            << "0.5 * (__hiloint2double(hi_part,lo_part) + local_visc.x - visc_" << spec_idx << ".x), "
            << "0.5 * (__hiloint2double(hi_part,lo_part) + local_visc.y - visc_" << spec_idx << ".y));\n";
        ost << "lo_part = __shfl(__double2loint(factor_prefix[" << thread_index << "]), " << lane_index << ");\n";
        ost << "hi_part = __shfl(__double2hiint(factor_prefix[" << thread_index << "]), " << lane_index << ");\n";
      }
      else
      {
        ost << REAL << " visc_" << spec_idx << " = spec_visc[offset + " << spec_idx << "][tid];\n";
        ost << REAL << " mole_" << spec_idx << " = " << MOLE_FRAC << "[offset + " << spec_idx << "][tid];\n";
        ost << INT << " hi_part, lo_part;\n";
        ost << "lo_part = __shfl(__double2loint(mass_ratios[" << thread_index << "]), " << lane_index << ");\n";
        ost << "hi_part = __shfl(__double2hiint(mass_ratios[" << thread_index << "]), " << lane_index << ");\n";
        ost << REAL << " numer = 0.5 * (__hiloint2double(hi_part,lo_part) + local_visc - visc_" << spec_idx << ");\n";
        ost << "lo_part = __shfl(__double2loint(factor_prefix[" << thread_index << "]), " << lane_index << ");\n";
        ost << "hi_part = __shfl(__double2hiint(factor_prefix[" << thread_index << "]), " << lane_index << ");\n";
      }
      if (unit->fast_math)
        emit_gpu_exp_taylor_series_expansion(ost, "numer", unit->taylor_stages, unit->use_double2);
      else
      {
        if (unit->use_double2)
          ost << "numer = make_double2(exp(numer.x), exp(numer.y));\n";
        else
          ost << "numer = exp(numer);\n";
      }
      if (unit->use_double2)
      {
        ost << "numer = make_double2(numer.x+1.0,numer.y+1.0);\n"; 
        ost << REAL2 << " factor = make_double2( "
            << "__hiloint2double(hi_part,lo_part)*numer.x*numer.x, "
            << "__hiloint2double(hi_part,lo_part)*numer.y*numer.y);\n";
        ost << "spec_sum = make_double2( "
            << "__fma_rn(factor.x,mole_" << spec_idx << ".x,spec_sum.x), "
            << "__fma_rn(factor.y,mole_" << spec_idx << ".y,spec_sum.y));\n";
      }
      else
      {
        ost << "numer += 1.0;\n";
        ost << REAL << " factor = __hiloint2double(hi_part,lo_part)*numer*numer;\n";
        ost << "spec_sum = __fma_rn(factor,mole_" << spec_idx << ",spec_sum);\n";
      }
    }
    // Now compute our own contribution and write it out
    if (unit->use_double2)
    {
      ost << REAL2 << " local_mole = make_double2("
          << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][0][tid], "
          << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][1][tid]);\n";
      ost << "spec_sum = make_double2( "
          << "__fma_rn(local_mole.x," << (4.0/sqrt(2.0)) << ",spec_sum.x), "
          << "__fma_rn(local_mole.y," << (4.0/sqrt(2.0)) << ",spec_sum.y));\n";
    }
    else
    {
      ost << REAL << " local_mole = " << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid];\n";
      ost << "spec_sum = __fma_rn(local_mole," << (4.0/sqrt(2.0)) << ",spec_sum);\n";
    }
    if (unit->fast_math)
      emit_gpu_exp_taylor_series_expansion(ost, "local_visc", unit->taylor_stages, unit->use_double2);
    else
    {
      if (unit->use_double2)
        ost << "local_visc = make_double2(exp(local_visc.x),exp(local_visc.y));\n";
      else
        ost << "local_visc = exp(local_visc);\n";
    }
    if (unit->use_double2)
    {
      ost << REAL2 << " ratio = make_double2(local_visc.x/spec_sum.x,local_visc.y/spec_sum.y);\n";
      ost << "result = make_double2( "
          << "__fma_rn(local_mole.x,ratio.x,result.x), "
          << "__fma_rn(local_mole.y,ratio.y,result.y));\n";
    }
    else
    {
      ost << REAL << " ratio = local_visc/spec_sum;\n";
      ost << "result = __fma_rn(local_mole,ratio,result);\n";
    } 
  }
  // Write our results into shared memory, then do the reduction and have warp 0 write out the results
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  // Must be true for how we are going to do this reduction
  assert(unit->threads_per_point <= num_species);
  if (unit->use_double2)
  {
    ost << "spec_visc[wid][0][tid] = result.x;\n";
    ost << "spec_visc[wid][1][tid] = result.y;\n";
  }
  else
    ost << "spec_visc[wid][tid] = result;\n";
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  // Do the final reduction
  {
    ost << "if (wid == 0)\n";
    PairDelim reduc_pair(ost);
    if (unit->use_double2)
    {
      ost << REAL2 << " accum = make_double2(0.0,0.0);\n";
      ost << "#pragma unroll\n";
      ost << "for (int i = 0; i < " << unit->threads_per_point << "; i++)\n";
      {
        PairDelim loop_pair(ost);
        ost << "accum.x += spec_visc[i][0][tid];\n";
        ost << "accum.y += spec_visc[i][1][tid];\n";
      }
      ost << "accum = make_double2(" << (sqrt(8.0)) << "*accum.x," << (sqrt(8.0)) << "*accum.y);\n";
      ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(viscosity), "
          << "\"d\"(accum.x), "
          << "\"d\"(accum.y) : \"memory\");\n";
    }
    else
    {
      ost << REAL << " accum = 0.0;\n";
      ost << "#pragma unroll\n";
      ost << "for (int i = 0; i < " << unit->threads_per_point << "; i++)\n";
      {
        PairDelim loop_pair(ost);
        ost << "accum += spec_visc[i][tid];\n";
      }
      ost << "accum *= " << sqrt(8.0) << ";\n";
      ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(viscosity), "
            << "\"d\"(accum) : \"memory\");\n";
    }
  }
  // Update the pointers, then synchronize before going around the loop again
  {
    PairDelim pointer_pair(ost);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
    ost << "viscosity += slice_stride;\n";
  }
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
}

void TranLib::emit_sliced_viscosity(CodeOutStream &ost, bool bounds)
{
  ost << "__global__ void\n";
  {
    int warps_per_cta = unit->threads_per_point;
    int shared_memory = 32/*min points*/ * (unit->use_double2 ? 16 : 8)/*bytes per element*/
                        * (unit->species.size()-1)/*number of species*/ * 2/*number of arrays*/;
    if (shared_memory > MAX_SHARED)
    {
      fprintf(stderr,"Too many species to fit in shared memory for sliced visocisty kernel: needed %d bytes\n", shared_memory);
      exit(1);
    }
    // Figure out how many CTAs we can get on an SM, assuming shared memory limited
    int ctas_per_sm = MAX_SHARED/shared_memory;
    int total_warps = ctas_per_sm * warps_per_cta;
    fprintf(stdout,"    Warp-Specialized Viscosity Kernel:\n");
    fprintf(stdout,"      Warps-per-CTA:         %d\n", warps_per_cta);
    fprintf(stdout,"      Shared Memory per CTA: %d\n", shared_memory);
    fprintf(stdout,"      CTAs-per-SM:           %d\n", ctas_per_sm);
    fprintf(stdout,"      Warps-per-SM:          %d\n", total_warps);
    if (bounds)
      ost << "__launch_bounds__(" << (32*warps_per_cta) << "," << ctas_per_sm << ")\n";
  }
  ost << "gpu_viscosity(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "viscosity";
  //ost << ", const bool always_false";
  ost << ")";
}

void TranLib::emit_large_sliced_viscosity(CodeOutStream &ost, unsigned max_specs, bool uniform)
{
  // Be careful making this less than 16, you stop getting coalesced loads
  const unsigned warp_size = 16;
  const char *thread_mask = "0xf";
  const unsigned warp_shift = 4;
  const unsigned num_species = (unit->species.size()-1);
#ifdef LARGE_VISC_KEPLER
  unsigned warp_constant_stride = 0;
#endif
  // Emit the constants for this kernel
  {
    // Always put the neta values in constant cache since they should always fit easily
    ost << "__constant__ " << REAL << " viscosity_neta[" << num_species << "][" << order << "] = {";
    for (unsigned idx = 0; idx < num_species; idx++)
    {
      if (idx == 0)
        ost << " {";
      else
        ost << ", {";
      for (int i = order ; i > 0; i--)
      {
        if (i == order)
          ost << (neta[idx*order+i-1]);
        else
          ost << ", " << (neta[idx*order+i-1]);
      }
      ost << "} ";
    }
    ost << "};\n\n";
#ifdef LARGE_VISC_KEPLER
    // Figure out parameters of specifying constants
    bool has_remainder = ((num_species%unit->threads_per_point) != 0);
    unsigned num_columns = (num_species/unit->threads_per_point);
    unsigned max_points_per_thread = (num_columns + (has_remainder ? 1 : 0))*(num_species);
    warp_constant_stride = (max_points_per_thread);
    // Make everything fit nicely by padding with zeros
    // Also guarantees aligned loads for the threads
    while ((warp_constant_stride%warp_size) != 0)
      warp_constant_stride++;
    // Now do the viscosity mass ratios
    ost << "__device__ const " << REAL << " viscosity_mass_ratios[" << (unit->threads_per_point*warp_constant_stride) << "] = {";
    bool first = true;
    for (unsigned wid = 0; wid < unit->threads_per_point; wid++)
    {
      unsigned num_created = 0;
      for (unsigned col_idx = 0; col_idx < num_columns; col_idx++)
      {
        unsigned column = col_idx*unit->threads_per_point + wid;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (first)
          {
            first = false;
            if (row == column)
              ost << "0.0";
            else
              ost << (log(sqrt(nwt[row]/nwt[column])));
          }
          else
          {
            if (row == column)
              ost << ", " << "0.0";
            else
              ost << ", " << (log(sqrt(nwt[row]/nwt[column]))); 
          }
          num_created++;
        }
      }
      assert(!first);
      if (has_remainder)
      {
        unsigned column = (wid+(num_columns*unit->threads_per_point))%num_species;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (row == column)
            ost << ", " << "0.0";
          else
            ost << ", " << (log(sqrt(nwt[row]/nwt[column])));
          num_created++;
        }
      }
      assert(num_created == max_points_per_thread);
      // Pad everything else with zeros
      while (num_created < warp_constant_stride)
      {
        ost << ", 0.0";
        num_created++;
      }
    }
    ost << "};\n\n";
    // Finally do the viscosity factor prefixes
    first = true;
    ost << "__device__ const " << REAL << " viscosity_factor_prefix[" << (unit->threads_per_point*warp_constant_stride) << "] = {";
    for (unsigned wid = 0; wid < unit->threads_per_point; wid++)
    {
      unsigned num_created = 0;
      for (unsigned col_idx = 0; col_idx < num_columns; col_idx++)
      {
        unsigned column = col_idx*unit->threads_per_point + wid;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (first)
          {
            first = false;
            if (row == column)
              ost << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
            else
              ost << (1.0/sqrt(2.0));
          }
          else
          {
            if (row == column)
              ost << ", " << (1.0/sqrt(2.0));
            else
              ost << ", " << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
          }
          num_created++;
        }
      }
      assert(!first);
      if (has_remainder)
      {
        unsigned column = (wid+(num_columns*unit->threads_per_point))%num_species;
        for (unsigned row = 0; row < num_species; row++)
        {
          if (row == column)
            ost << ", " << (1.0/sqrt(2.0));
          else
            ost << ", " << (1.0/sqrt(1.0+(nwt[column]/nwt[row])));
          num_created++;
        }
      }
      assert(num_created == max_points_per_thread);
      // Pad everything else with zeros
      while (num_created < warp_constant_stride)
      {
        ost << ", 0.0";
        num_created++;
      }
    }
    ost << "};\n\n";
#else
    bool fits_in_constant_cache = true;
    if (fits_in_constant_cache)
    {
      size_t total_constant_bytes = (unit->species.size()-1)*order*sizeof(REAL_TYPE);
      total_constant_bytes += (unit->species.size()-1)*(unit->species.size()-2)*sizeof(REAL_TYPE);
      total_constant_bytes += (unit->species.size()-1)*(unit->species.size()-2)*sizeof(REAL_TYPE);
      if (total_constant_bytes > 65536)
      {
        fprintf(stderr,"INFO: total constant size for viscosity is %ld, which no longer fits "
                        "in constant cache.  Moving to global memory.", total_constant_bytes);
        fits_in_constant_cache = false;
      }
    }
    ost << (fits_in_constant_cache ? "__constant__ " : "__device__ const ")
        << REAL << " viscosity_mass_ratios[" << (unit->species.size()-1)
        << "][" << (unit->species.size()-1) << "] = {";
    for (unsigned i = 0; i < (unit->species.size()-1); i++)
    {
      ost << "{";
      bool first = true;
      for (unsigned j = 0; j < (unit->species.size()-1); j++)
      {
        if (first)
        {
          first = false;
          if (i == j)
            ost << "0.0";
          else
            ost << (log(sqrt(nwt[j]/nwt[i])));
        }
        else
        {
          if (i == j)
            ost << ", 0.0";
          else
            ost << ", " << (log(sqrt(nwt[j]/nwt[i])));
        }
      }
      ost << "},";
    }
    ost << "};\n\n";
    ost << (fits_in_constant_cache ? "__constant__ " : "__device__ const ")
        << REAL << " viscosity_factor_prefix[" << (unit->species.size()-1)
        << "][" << (unit->species.size()-1) << "] = {";
    for (unsigned i = 0; i < (unit->species.size()-1); i++)
    {
      ost << "{";
      bool first = true;
      for (unsigned j = 0; j < (unit->species.size()-1); j++)
      {
        if (first)
        {
          first = false;
          if (i == j)
            ost << (1.0/sqrt(2.0));
          else
            ost << (1.0/sqrt(1.0+(nwt[i]/nwt[j])));
        }
        else
        {
          if (i == j)
            ost << ", " << (1.0/sqrt(2.0));
          else
            ost << ", " << (1.0/sqrt(1.0+(nwt[i]/nwt[j])));
        }
      }
      ost << "},";
    }
    ost << "};\n\n";
#endif
  }
  emit_large_sliced_viscosity_declaration(ost);
  ost << "\n";
  PairDelim pair(ost);
  ost << "const " << INT << " tid = threadIdx.x & " << thread_mask << ";\n";
  ost << "const " << INT << " wid = threadIdx.x >> " << warp_shift << ";\n";
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*1)*row_stride + "
        << "(blockIdx.x*" << warp_size << " + tid)*sizeof("
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ")"
#ifdef LARGE_VISC_KEPLER
        << ";\n";
#else
        << "+blockIdx.z*slice_stride;\n";
#endif
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
    ost << "viscosity += offset;\n";
  }
  ost << "volatile __shared__ " << REAL << " spec_visc[" << num_species << "][" << warp_size << "];\n";
  // Mark these as volatile so the compiler doesn't do
  // something stupid like try to cache them in registers
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << num_species << "][" << warp_size << "];\n";
#ifdef LARGE_VISC_KEPLER
  // Emit loads for our constants
  const unsigned constants_per_thread = ((num_species)*max_specs+(warp_size-1))/warp_size;
  assert(warp_constant_stride == (warp_size*constants_per_thread));
  ost << REAL << " mass_ratios[" << constants_per_thread << "];\n";
  ost << REAL << " factor_prefix[" << constants_per_thread << "];\n";
  {
    PairDelim load_pair(ost);
    ost << "const " << INT << " offset = wid*" << warp_constant_stride << "+tid;\n";
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"offset+%d",(idx*warp_size));
      emit_cuda_load(ost,"mass_ratios",dst_offset,"viscosity_mass_ratios",src_offset,false/*double2*/);
      emit_cuda_load(ost,"factor_prefix",dst_offset,"viscosity_factor_prefix",src_offset,false/*double2*/);
    }
  }
#endif
  ost << REAL << " " << MOLE_FRAC << "_temp[" << max_specs << "];\n";
  ost << REAL << " " << TEMPERATURE << ";\n";
#ifdef LARGE_VISC_KEPLER
  // emit the main loop
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim step_loop_pair(ost);
#endif
  // Emit the set of loads for the first loop
  emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, "0", false);
  // Issue loads for our mole fractions
  {
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      char dst[128];
      sprintf(dst,"%s_temp",MOLE_FRAC);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride",(idx*unit->threads_per_point));
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      emit_cuda_load(ost,dst,dst_offset,MOLE_FRAC_ARRAY,src_offset,false,".cg");
    }
  }
#ifdef LARGE_VISC_KEPLER
#if 0
  {
    PairDelim load_pair(ost);
    unsigned total_constants = warp_constant_stride*unit->threads_per_point;
    ost << "const " << INT << " offset = (wid*" << warp_constant_stride << "+tid+step*"
        << next_largest_power(2*total_constants,2) << ")%" << next_largest_power(2*total_constants,2) << ";\n";
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"offset+%d",(idx*warp_size));
      //emit_cuda_load(ost,"mass_ratios",dst_offset,"viscosity_mass_ratios",src_offset,false/*double2*/);
      emit_cuda_load(ost,"factor_prefix",dst_offset,"viscosity_factor_prefix",src_offset,false/*double2*/,".ca");
    }
  }
#endif
#endif
  // Compute logt
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  // Compute the species specific viscosities
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
    PairDelim spec_pair(ost);
    // For right now we'll take replays on the reads from viscosity_neta here.  We'll come
    // back and fix it later
#ifdef LARGE_VISC_KEPLER
    // Do some fancy math to make sure the compiler reloads this value and doesn't hoist it
    // out of the loop to consume registers
    ost << "const " << INT << " neta_index = (wid+" << (idx*unit->threads_per_point) << "+step*"
        << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2) << ";\n";
    ost << REAL << " val = viscosity_neta[neta_index][0];\n";
#else
    ost << REAL << " val = viscosity_neta[wid+" << (idx*unit->threads_per_point) << "][0];\n";
#endif
    for (int i = (order-1); i > 0; i--)
    {
#ifdef LARGE_VISC_KEPLER
      ost << "val = __fma_rn(val,logt,viscosity_neta[neta_index][" << (order-i) << "]);\n";
#else
      ost << "val = __fma_rn(val,logt,viscosity_neta[wid+" << (idx*unit->threads_per_point) << "][" << (order-i) << "]);\n";
#endif
    }
    ost << "spec_visc[wid+" << (idx*unit->threads_per_point) << "][tid] = val;\n";
  }
  // Write out our species mole fractions into shared memory
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
    ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid] = " << MOLE_FRAC << "_temp[" << idx << "];\n";
  }
  // Synchronize to say that everything important is in shared memory
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  // Now do the code to compute the output coefficient for each point
  ost << REAL << " result = 0.0;\n";
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
    PairDelim idx_pair(ost);
    ost << REAL << " spec_sum = 0.0;\n";
    ost << "const " << INT << " point = wid + " << (idx*unit->threads_per_point) << ";\n";
    ost << REAL << " local_visc = spec_visc[point][tid];\n";
    // Do all the species here including ourselves so we can avoid bank conflicts even though
    // it will cost us an extra step
    for (unsigned spec_idx = 0; spec_idx < num_species; spec_idx++)
    {
      PairDelim spec_pair(ost);
      ost << REAL << " visc_" << spec_idx << " = spec_visc[" << spec_idx << "][tid];\n";
      ost << REAL << " mole_" << spec_idx << " = " << MOLE_FRAC << "[" << spec_idx << "][tid];\n";
#ifdef LARGE_VISC_KEPLER
      const unsigned coeff_index = idx*num_species + spec_idx;
      const unsigned lane_index = coeff_index % warp_size;
      const unsigned thread_index = coeff_index / warp_size;
      ost << INT << " hi_part, lo_part;\n";
      ost << "lo_part = __shfl(__double2loint(mass_ratios[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
      ost << "hi_part = __shfl(__double2hiint(mass_ratios[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
      ost << REAL << " numer = 0.5 * (__hiloint2double(hi_part,lo_part) + local_visc - visc_" << spec_idx << ");\n";
      ost << "lo_part = __shfl(__double2loint(factor_prefix[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
      ost << "hi_part = __shfl(__double2hiint(factor_prefix[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
#else
      ost << REAL << " numer = 0.5 * (viscosity_mass_ratios[wid+" << (idx*unit->threads_per_point) << "][" << spec_idx << "]"
          << " + local_visc - visc_" << spec_idx << ");\n";
#endif
      if (unit->fast_math)
        emit_gpu_exp_taylor_series_expansion(ost, "numer", unit->taylor_stages, false);
      else
        ost << "numer = exp(numer);\n";
      ost << "numer += 1.0;\n";
#ifdef LARGE_VISC_KEPLER
      ost << REAL << " factor = __hiloint2double(hi_part,lo_part)*numer*numer;\n";
#else
      ost << REAL << " factor = viscosity_factor_prefix[wid+" << (idx*unit->threads_per_point) << "][" << spec_idx << "]*numer*numer;\n";
#endif
      ost << "spec_sum = __fma_rn(factor,mole_" << spec_idx << ",spec_sum);\n";
    }
    // Update the contribution to result
    ost << REAL << " local_mole = " << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid];\n";
    if (unit->fast_math)
      emit_gpu_exp_taylor_series_expansion(ost, "local_visc", unit->taylor_stages, false);
    else
      ost << "local_visc = exp(local_visc);\n";
    ost << REAL << " ratio = local_visc/spec_sum;\n";
    ost << "result = __fma_rn(local_mole,ratio,result);\n";
  }
  // Write our results into shared memory, then do the reduction and have warp 0 write out the results
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  // Must be true for how we are going to do this reduction
  assert(unit->threads_per_point <= num_species);
  ost << "spec_visc[wid][tid] = result;\n";
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  {
    ost << "if (wid == 0)\n";
    PairDelim reduc_pair(ost);
    ost << REAL << " accum = 0.0;\n";
    ost << "#pragma unroll\n";
    ost << "for (int i = 0; i < " << unit->threads_per_point << "; i++)\n";
    {
      PairDelim loop_pair(ost);
      ost << "accum += spec_visc[i][tid];\n";
    }
    ost << "accum *= " << sqrt(8.0) << ";\n";
    ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(viscosity), "
          << "\"d\"(accum) : \"memory\");\n";
  }
  // Update the pointers, then synchronize before going around the loop again
#ifdef LARGE_VISC_KEPLER
  {
    PairDelim pointer_pair(ost);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
    ost << "viscosity += slice_stride;\n";
  }
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
#endif
}

void TranLib::emit_large_sliced_viscosity_declaration(CodeOutStream &ost)
{
  ost << "__global__ void\n";
  {
    int warps_per_cta = unit->threads_per_point/2;
    int shared_memory = 16/*min points*/ * (unit->use_double2 ? 16 : 8)/*bytes per element*/
                        * (unit->species.size()-1)/*number of species*/ * 2/*number of arrays*/;
    if (shared_memory > MAX_SHARED)
    {
      fprintf(stderr,"Too many species to fit in shared memory for sliced visocisty kernel: needed %d bytes\n", shared_memory);
      exit(1);
    }
    // Figure out how many CTAs we can get on an SM, assuming shared memory limited
    int ctas_per_sm = MAX_SHARED/shared_memory;
    int total_warps = ctas_per_sm * warps_per_cta;
    fprintf(stdout,"    Warp-Specialized Viscosity Kernel:\n");
    fprintf(stdout,"      Warps-per-CTA:         %d\n", warps_per_cta);
    fprintf(stdout,"      Shared Memory per CTA: %d\n", shared_memory);
    fprintf(stdout,"      CTAs-per-SM:           %d\n", ctas_per_sm);
    fprintf(stdout,"      Warps-per-SM:          %d\n", total_warps);
  }
  ost << "gpu_viscosity(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "viscosity";
  //ost << ", const bool always_false";
  ost << ")";
}

void TranLib::emit_specialized_diffusion(CodeOutStream &ost)
{
  // Let's compute how many of each kind of load we're going to need
  // as warps walk along their group of species.
  unsigned full_specs, partial_specs;
  bool full_uniform, partial_uniform;
  const unsigned num_species = unit->species.size()-1;
  const unsigned full_count = num_species/2;
  const unsigned partial_count = full_count - 1;
  unsigned total_species;
  if ((num_species%2) == 0)
  {
    // For an even number of species, then
    // half of the species will be full species and
    // half of the species will be partial species
    // Get the number of full species per thread (round up)
    full_specs = ((num_species/2)+(unit->threads_per_point-1))/unit->threads_per_point;
    partial_specs = full_specs;
    full_uniform = (((num_species/2) % unit->threads_per_point) == 0);
    partial_uniform = full_uniform;
    total_species = num_species/2;
  }
  else
  {
    // For an odd number of species, then 
    // all the species will be full species of N/2 (truncated) elements
    // Get the number of full species per thread (round up)
    full_specs = (num_species+(unit->threads_per_point-1))/unit->threads_per_point;
    partial_specs = 0;
    full_uniform = ((num_species % unit->threads_per_point) == 0);
    partial_uniform = true;
    total_species = num_species;
  }
  if (unit->threads_per_point >= num_species)
  {
    fprintf(stderr,"The number of threads per point (%d) must be less than or equal to "
                    "the number of species (%d) for the warp-specialized diffusion kernel\n",
                    unit->threads_per_point, num_species);
    exit(1);
  }
#if 0
  if ((unit->threads_per_point % unit->named_barriers) != 0)
  {
    fprintf(stderr,"The number of threads per point (%d) must be divisible by the number of "
                    "named barriers (%d) for the warp-specialized diffusion kernel\n",
                    unit->threads_per_point, unit->named_barriers);
    exit(1);
  }
#endif

  // Now let's emit all the constants 
  bool fits_in_constant_cache = false;
#ifdef DIFFUSION_ACROSS
  unsigned max_prefetch = 1;
  unsigned max_points_per_thread = 1;
  unsigned warp_constant_stride = 0;
#endif
  {
    size_t total_constant_bytes = 0;
    total_constant_bytes += (num_species * sizeof(REAL_TYPE));
#ifndef DIFFUSION_SHUFFLE
    if ((num_species%2) == 0)
    {
      total_constant_bytes += ((num_species/2)*full_count*order*sizeof(REAL_TYPE));
      total_constant_bytes += ((num_species/2)*partial_count*order*sizeof(REAL_TYPE));
    }
    else
    {
      total_constant_bytes += (num_species*full_count*order*sizeof(REAL_TYPE));
    }
#endif
    if (total_constant_bytes > 65536)
    {
      fprintf(stderr,"INFO: total constant size for diffusion is %ld, which no longer fits "
                        "in constant cache.  Moving to global memory.", total_constant_bytes);
      fits_in_constant_cache = false;
    }
    ost << (fits_in_constant_cache ? "__constant__ " : "__device__ const ")
        << REAL << " diffusion_masses[" << num_species << "] = {";
    for (unsigned idx = 0; idx < num_species; idx++)
    {
      if (idx == 0)
        ost << nwt[idx];
      else
        ost << ", " << nwt[idx];
    }
    ost << "};\n\n";
#ifdef DIFFUSION_ACROSS
    {
      bool has_remainder = ((num_species%unit->threads_per_point) != 0);
      unsigned num_columns = (num_species/unit->threads_per_point);
      max_points_per_thread = (num_columns + (has_remainder ? 1 : 0))*(num_species/2); 
#ifdef DIFFUSION_SHUFFLE
      warp_constant_stride = (max_points_per_thread*order);
      while ((warp_constant_stride%32) != 0)
        warp_constant_stride++;
#endif
      max_prefetch = max_points_per_thread;
#ifndef DIFFUSION_SHUFFLE
      ost << (fits_in_constant_cache ? "__constant__ " : "__device__ const ")
          << REAL << " across_coeffs[" << unit->threads_per_point << "][" << max_points_per_thread << "][" << order << "] = {";
#else
      ost << "__device__ const "
          << REAL << " across_coeffs[" << (unit->threads_per_point*warp_constant_stride) << "] = {";
#endif
      for (unsigned wid = 0; wid < unit->threads_per_point; wid++)
      {
#ifndef DIFFUSION_SHUFFLE
        if (wid == 0)
          ost << " {";
        else
          ost << ", {";
        bool first = true;
#endif
        unsigned num_created = 0;
        for (unsigned point_idx = 0; point_idx < ((num_species/2)+num_columns-1); point_idx++)
        {
          unsigned start_across = (point_idx < (num_species/2) ? 0 : point_idx-(num_species/2)+1);
          unsigned stop_across = (point_idx < num_columns ? point_idx+1 : num_columns);
          for (unsigned across_idx = start_across; across_idx < stop_across; across_idx++)
          {
            unsigned x_loc = wid*num_columns+across_idx;
            unsigned y_loc = (wid*num_columns+1+point_idx)%num_species;
#ifndef DIFFUSION_SHUFFLE
            if (first)
            {
              ost << " {";
              first = false;
            }
            else
              ost << ", {";
#endif
            for (int k = order; k > 0; k--)
            {
              // We assume they are symmetric, so check it
              assert(ndif[(k-1)+y_loc*order+x_loc*order*num_species] == ndif[(k-1)+x_loc*order+y_loc*order*num_species]);
#ifndef DIFFUSION_SHUFFLE
              if (k < order)
#else
              if ((k < order) || (wid > 0) || (across_idx > 0) || (point_idx > 0))
#endif
                ost << ", ";
              ost << ndif[(k-1)+y_loc*order+x_loc*order*num_species];
            }
#ifndef DIFFUSION_SHUFFLE
            ost << "}";
#endif
            num_created++;
          }
        }
#ifndef DIFFUSION_SHUFFLE
        assert(!first);
#endif
        // Handle the case where we have a remainder
        if (has_remainder)
        {
          unsigned x_loc = (wid+(num_columns*unit->threads_per_point))%num_species;
          for (unsigned point_idx = 0; point_idx < (num_species/2); point_idx++)
          {
            unsigned y_loc = (x_loc+1+point_idx)%num_species;
#ifndef DIFFUSION_SHUFFLE
            ost << ", {";
#endif
            for (int k = order; k > 0; k--)
            {
              // We assume they are symmetric, so check it
              assert(ndif[(k-1)+y_loc*order+x_loc*order*num_species] == ndif[(k-1)+x_loc*order+y_loc*order*num_species]);
#ifndef DIFFUSION_SHUFFLE
              if (k < order)
#endif
                ost << ", ";
              ost << ndif[(k-1)+y_loc*order+x_loc*order*num_species];
            }
#ifndef DIFFUSION_SHUFFLE
            ost << "}";
#endif
            num_created++;
          }
        }
        assert(num_created == max_points_per_thread);
#ifndef DIFFUSION_SHUFFLE
        ost << "}";
#else
        // Otherwise if we're doing diffusion shuffle, pad it out with zeros
        for (int i = 0; i < (int(warp_constant_stride)-int(num_created*order)); i++)
          ost << ", 0.0";
#endif
      }
      ost << "};\n\n";
    }
#else
    if ((num_species%2) == 0)
    {
      ost << (fits_in_constant_cache ? "__constant__ " : "__device__ const ")
          << REAL << " diffusion_coeffs[" << (num_species/2) << "][" << full_count << "][" << order << "] = {";
      for (unsigned i = 0; i < (num_species/2); i++)
      {
        if (i == 0)
          ost << " {";
        else
          ost << ", {";
        for (unsigned j = 0; j < full_count; j++)
        {
          unsigned act_j = (i+1+j)%num_species;
          if (j == 0)
            ost << " {";
          else
            ost << ", {";
          for (int k = order; k > 0; k--)
          {
            // We assume that they are symmetric so check it
            assert(ndif[(k-1)+act_j*order+i*order*num_species] == ndif[(k-1)+i*order+act_j*order*num_species]);
            if (k < order)
              ost << ", ";
            ost << ndif[(k-1)+act_j*order+i*order*num_species];
          }
          ost << "}";
        }
        ost << "}";
      }
      ost << "};\n\n";
      ost << (fits_in_constant_cache ? "__constant__ " : "__device__ const ")
          << REAL << " diffusion_coeffs2[" << (num_species/2) << "][" << partial_count << "][" << order << "] = {";
      for (unsigned i = 0; i < (num_species/2); i++)
      {
        unsigned act_i = (num_species/2)+i;
        if (i == 0)
          ost << " {";
        else
          ost << ", {";
        for (unsigned j = 0; j < partial_count; j++)
        {
          unsigned act_j = (act_i+1+j)%num_species;
          if (j == 0)
            ost << " {";
          else
            ost << ", {";
          for (int k = order; k > 0; k--)
          {
            // We assume that they are symmetric so check it
            assert(ndif[(k-1)+act_j*order+act_i*order*num_species] == ndif[(k-1)+act_i*order+act_j*order*num_species]);
            if (k < order)
              ost << ", ";
            ost << ndif[(k-1)+act_j*order+act_i*order*num_species];
          }
          ost << "}";
        }
        ost << "}";
      }
      ost << "};\n\n";
    }
    else
    {
      ost << (fits_in_constant_cache ? "__constant__ " : "__device__ const ")
          << REAL << " diffusion_coeffs[" << num_species << "][" << full_count << "][" << order << "] = {";
      for (unsigned i = 0; i < num_species; i++)
      {
        if (i == 0)
          ost << " {";
        else
          ost << ", {";
        for (unsigned j = 0; j < full_count; j++)
        {
          unsigned act_j = (i+1+j)%num_species;
          if (j == 0)
            ost << " {";
          else
            ost << ", {";
          for (int k = order; k > 0; k--)
          {
            // We're assuming here that they are symmetric so check it 
            assert(ndif[(k-1)+act_j*order+i*order*num_species] == ndif[(k-1)+i*order+act_j*order*num_species]);
            if (k < order)
              ost << ", ";
            ost << ndif[(k-1)+act_j*order+i*order*num_species];
          }
          ost << "}";
        }
        ost << "}";
      }
      ost << "};\n\n";
    }
#endif
  }
  
  emit_specialized_diffusion_declaration(ost,false/*bounds*/);
  ost << "\n";
  PairDelim pair(ost);
  ost << "const " << INT << " tid = threadIdx.x & 0x1f;\n";
  ost << "const " << INT << " wid = threadIdx.x >> 5;\n";
  // Offset all the named barriers by 1 to avoid clashing with basic syncthreads
  // Note we need to have these wrap to avoid having the warp with highest warp id
  // run too far ahead and accidentally register with an aliased barrier too soon.
#if 0
  ost << "const " << INT << " pbar0 = ((wid & 0x1)*2)+1;\n";
  ost << "const " << INT << " cbar0 = ((wid+1) & 0x1)*2+1;\n";
  ost << "const " << INT << " pbar1 = ((wid & 0x1)*2)+2;\n";
  ost << "const " << INT << " cbar1 = ((wid+1) & 0x1)*2+2;\n";
#endif
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y*1)*row_stride + "
        << "(blockIdx.x*32 + tid)*sizeof("
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ")"
#ifdef DIFFUSION_SHUFFLE
        << ";\n"; 
#else
        << "+blockIdx.z*slice_stride;\n";
#endif
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
    ost << "diffusion += offset;\n";
  }
  if (unit->use_double2)
    ost << "volatile __shared__ " << REAL << " sumxod[" << (unit->species.size()-1) << "][2][32];\n";
  else
    ost << "volatile __shared__ " << REAL << " sumxod[" << (unit->species.size()-1) << "][32];\n";
  // Mark these volatile so the compiler doesn't cache them in registers
  if (unit->use_double2)
    ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << (unit->species.size()-1) << "][2][32];\n";
  else
    ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << (unit->species.size()-1) << "][32];\n";

   
#ifdef DIFFUSION_SHUFFLE
  // Emit loads for storing our constants
  {
    unsigned constants_per_thread = (max_points_per_thread*order+31)/32;
    assert(warp_constant_stride == (32*constants_per_thread));
    ost << REAL << " thread_coeffs[" << (constants_per_thread) << "];\n";
    PairDelim constant_pair(ost);
    ost << "const " << INT << " offset = wid*" << (warp_constant_stride) << "+tid;\n";
    // Issue loads for our constants
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"offset+%d",(idx*32));
      emit_cuda_load(ost,"thread_coeffs",dst_offset,"across_coeffs",src_offset,false/*double2*/);
    }
  }
  // Emit the global loop
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim step_loop_pair(ost);
#endif
  // Issue a load for the temperature
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << ";\n";
  emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, "0", unit->use_double2);
  // Emit loads for the mole fractions
  {
    // Figure out how many species we are loading ourself
    const unsigned max_specs = (num_species+(unit->threads_per_point-1))/(unit->threads_per_point);
    const bool uniform = ((num_species % unit->threads_per_point) == 0);
    // Otherwise, only need to load the species that we own
    ost << (unit->use_double2 ? REAL2 : REAL) << " local_" << MOLE_FRAC << "[" << max_specs << "];\n";
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      // Check to see if we need to emit a conditional load in this case
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride",idx*unit->threads_per_point);
      char dst[128];
      sprintf(dst,"local_%s",MOLE_FRAC);
      emit_cuda_load(ost,dst,dst_offset,MOLE_FRAC_ARRAY,src_offset,unit->use_double2);
    }
  }

#ifdef DIFFUSION_ACROSS
#ifndef DIFFUSION_SHUFFLE
  if (!fits_in_constant_cache)
  {
    // Emit the initial loads for however deep our buffering is for constants
    ost << REAL << " coeff_prefetch[" << (MIN(DIFFUSION_BUFFERING,max_prefetch)) << "][" << order << "];\n";
    this->diffusion_depth = 0;
    for (unsigned idx = 0; idx < (MIN(DIFFUSION_BUFFERING,max_prefetch)); idx++)
    {
      PairDelim prefetch_pair(ost);
      for (int i = 0; i < order; i++)
      {
        char dst[128];
        sprintf(dst,"coeff_prefetch[%d][%d]",idx,i);
        char src[128];
        sprintf(src,"&(across_coeffs[wid][%d][%d])",idx,i);
        emit_cuda_load(ost,dst,src,false/*double2*/,".ca");
        //char src_offset[128];
        //sprintf(src_offset,"(wid*%d)+%d", (max_prefetch*order), (idx*order+i)); 
        //char src[128];
        //sprintf(src,"((%s*)across_coeffs)", REAL);
        //emit_cuda_load(ost,"coeff_prefetch",dst_offset,src,src_offset,false/*double2*/,".ca");
      }
      this->diffusion_depth++;
    }
  }
#endif
#endif
  // Compute our value of logt
  if (unit->use_double2)
    ost << REAL2 << " logt = make_double2(log(" << TEMPERATURE << ".x),log(" << TEMPERATURE << ".y));\n";
  else
    ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  
  // Initialize all of our sumxod values to 0.0
  {
    const unsigned max_specs = (num_species+(unit->threads_per_point-1))/(unit->threads_per_point);
    const bool uniform = ((num_species % unit->threads_per_point) == 0);
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      if (unit->use_double2)
      {
        PairDelim init_pair(ost);
        ost << "sumxod[wid+" << (idx*unit->threads_per_point) << "][0][tid] = 0.0;\n";
        ost << "sumxod[wid+" << (idx*unit->threads_per_point) << "][1][tid] = 0.0;\n";
      }
      else
        ost << "sumxod[wid+" << (idx*unit->threads_per_point) << "][tid] = 0.0;\n";
    }
    // If we're putting our mole fractions in shared memory, then write the values out now
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      if (unit->use_double2)
      {
        PairDelim init_pair(ost);
        ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][0][tid] = local_" << MOLE_FRAC << "[" << idx << "].x;\n";
        ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][1][tid] = local_" << MOLE_FRAC << "[" << idx << "].y;\n";
      }
      else
        ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid] = local_" << MOLE_FRAC << "[" << idx << "];\n";
    }
  }
  // As we're going along, let's compute our local values of sumxw and wtm
  if (unit->use_double2)
  {
    ost << REAL2 << " local_sumxw = make_double2(0.0,0.0);\n";
    ost << REAL2 << " local_wtm = make_double2(0.0,0.0);\n";
  }
  else
  {
    ost << REAL << " local_sumxw = 0.0;\n";
    ost << REAL << " local_wtm = 0.0;\n";
  }
  // Need a barrier here before starting to read anything
  ost << "asm volatile(\"bar.sync 0;\");\n";
#ifdef DIFFUSION_ACROSS
  emit_specialized_diffusion_across(ost, full_specs, partial_specs, full_count, partial_count,
                                    num_species, "across_coeffs", max_prefetch, fits_in_constant_cache);
#else
  // Emit code for computing all the D entries for both full and partial cases
  emit_specialized_diffusion_points(ost, full_specs, full_count, full_uniform, 0/*offset*/, total_species, "diffusion_coeffs");
  if (partial_specs > 0)
    emit_specialized_diffusion_points(ost, partial_specs, partial_count, 
                                      partial_uniform, num_species/2/*offset*/, total_species, "diffusion_coeffs2");
#endif

  // Emit the load for our pressure
  ost << (unit->use_double2 ? REAL2 : REAL) << " " << PRESSURE << ";\n";
  emit_cuda_load(ost, PRESSURE, PRESSURE_ARRAY, "0", unit->use_double2);

  // If we decided to reload our mole fractions, then we have to do that now
  // since we're about to reuse the mole fraction part of shared memory to 
  // do the reductions for sumxw and wtm
#ifdef RELOAD_DIFFUSION_FRACS
  {
    const unsigned max_specs = (num_species+(unit->threads_per_point-1))/(unit->threads_per_point);
    const bool uniform = ((num_species % unit->threads_per_point) == 0);
    if (unit->use_double2)
      ost << REAL2 << " output_fracs[" << max_specs << "];\n";
    else
      ost << REAL << " output_fracs[" << max_specs << "];\n";
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      if (unit->use_double2)
        ost << "output_fracs[" << idx << "] = make_double2( "
            << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][0][tid], "
            << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][1][tid]);\n";
      else
        ost << "output_fracs[" << idx << "] = " << MOLE_FRAC 
            << "[wid+" << (idx*unit->threads_per_point) << "][tid];\n";
    }
    // Emit a syncthreads before the next phase
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  }
#endif
  // Now we need to compute the final sumxw and wtm values, write our local values into memory, and the read
  // out all the intermediate values and sum them up.  To do this we need to have twice as many species
  // as there are warps.  This shouldn't be a problem for most mechanisms, only smaller mechanisms.
  if ((2*unit->threads_per_point) > num_species)
  {
    fprintf(stderr,"Warp-specialized diffusion kernel needs at least twice as many points as species. "
            "Decrease number of warps to meet this constraint.  Species %d, warps %d\n", num_species, unit->threads_per_point);
    exit(1);
  }
  if (unit->use_double2)
  {
    ost << MOLE_FRAC << "[wid][0][tid] = local_sumxw.x;\n";
    ost << MOLE_FRAC << "[wid][1][tid] = local_sumxw.y;\n";
    ost << MOLE_FRAC << "[wid+" << unit->threads_per_point << "][0][tid] = local_wtm.x;\n";
    ost << MOLE_FRAC << "[wid+" << unit->threads_per_point << "][1][tid] = local_wtm.y;\n";
  }
  else
  {
    ost << MOLE_FRAC << "[wid][tid] = local_sumxw;\n";
    ost << MOLE_FRAC << "[wid+" << unit->threads_per_point << "][tid] = local_wtm;\n";
  }
  // Need a sync threads here
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  // Do the reductions.  Note we reload our own
  // sumxw and wtm values, but it will allow us to get
  // a bunch of pipelined shared memory loads without
  // any integer math or comparisons which should be faster.
  if (unit->use_double2)
  {
    ost << REAL2 << " sumxw = make_double2(0.0,0.0);\n";
    ost << REAL2 << " wtm = make_double2(0.0,0.0);\n";
    PairDelim reduc_pair(ost);
    ost << REAL2 << " reduc_sumxw[" << unit->threads_per_point << "];\n";
    ost << REAL2 << " reduc_wtm[" << unit->threads_per_point << "];\n";
    for (unsigned i = 0; i < unit->threads_per_point; i++)
    {
      ost << "reduc_sumxw[" << i << "].x = " << MOLE_FRAC << "[" << i << "][0][tid];\n";
      ost << "reduc_sumxw[" << i << "].y = " << MOLE_FRAC << "[" << i << "][1][tid];\n";
      ost << "reduc_wtm[" << i << "].x = " << MOLE_FRAC << "[" << i << "][0][tid];\n";
      ost << "reduc_wtm[" << i << "].y = " << MOLE_FRAC << "[" << i << "][1][tid];\n";
    }
    for (unsigned i = 0; i < unit->threads_per_point; i++)
    {
      ost << "sumxw.x += reduc_sumxw[" << i << "].x;\n";
      ost << "sumxw.y += reduc_sumxw[" << i << "].y;\n";
      ost << "wtm.x += reduc_wtm[" << i << "].x;\n";
      ost << "wtm.y += reduc_wtm[" << i << "].y;\n";
    }
  }
  else
  {
    ost << REAL << " sumxw = 0.0;\n";
    ost << REAL << " wtm = 0.0;\n";
    PairDelim reduc_pair(ost);
    ost << REAL << " reduc_sumxw[" << unit->threads_per_point << "];\n";
    ost << REAL << " reduc_wtm[" << unit->threads_per_point << "];\n";
    for (unsigned i = 0; i < unit->threads_per_point; i++)
    {
      ost << "reduc_sumxw[" << i << "] = " << MOLE_FRAC << "[" << i << "][tid];\n";
      ost << "reduc_wtm[" << i << "] = " << MOLE_FRAC << "[" << (unit->threads_per_point+i) << "][tid];\n";
    }
    for (unsigned i = 0; i < unit->threads_per_point; i++)
    {
      ost << "sumxw += reduc_sumxw[" << i << "];\n";
      ost << "wtm += reduc_wtm[" << i << "];\n";
    }
  }
  // Once we've got our values of sumxw and wtm, then we can do our output computation
  if (unit->use_double2)
  {
    const unsigned max_specs = (num_species+(unit->threads_per_point-1))/(unit->threads_per_point);
    const bool uniform = ((num_species % unit->threads_per_point) == 0);
    ost << REAL2 << " pfac = make_double2("
        << patmos << "/" << PRESSURE << ".x, "
        << patmos << "/" << PRESSURE << ".y);\n";
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      PairDelim output_pair(ost);
#ifdef RELOAD_DIFFUSION_FRACS
      ost << REAL2 << " clamped = make_double2( "
          << "(output_fracs[" << idx << "].x > " << SMALL << " ? output_fracs[" << idx << "].x : " << SMALL << "), "
          << "(output_fracs[" << idx << "].y > " << SMALL << " ? output_fracs[" << idx << "].y : " << SMALL << "));\n";
#else
      ost << REAL2 << " clamped = make_double2( "
          << "(local_" << MOLE_FRAC << "[" << idx << "].x > " << SMALL << " ? local_" << MOLE_FRAC << "[" << idx << "].x : " << SMALL << "), "
          << "(local_" << MOLE_FRAC << "[" << idx << "].y > " << SMALL << " ? local_" << MOLE_FRAC << "[" << idx << "].y : " << SMALL << "));\n";
#endif
      ost << REAL << " diff_mass = diffusion_masses[wid+" << (idx*unit->threads_per_point) << "];\n";
      ost << REAL2 << " result = make_double2( "
          << "pfac.x * (sumxw.x - (diff_mass*clamped.x)) / (wtm.x * sumxod[wid+" << (idx*unit->threads_per_point) << "][0][tid]), "
          << "pfac.y * (sumxw.y - (diff_mass*clamped.y)) / (wtm.y * sumxod[wid+" << (idx*unit->threads_per_point) << "][1][tid]));\n";
      // Write out the result
      ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(diffusion+(wid+"
          << (idx*unit->threads_per_point) << ")*spec_stride), \"d\"(result.x), \"d\"(result.y) : \"memory\");\n";
    }
  }
  else
  {
    const unsigned max_specs = (num_species+(unit->threads_per_point-1))/(unit->threads_per_point);
    const bool uniform = ((num_species % unit->threads_per_point) == 0);
    ost << REAL << " pfac = " << patmos << "/" << PRESSURE << ";\n";
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      PairDelim output_pair(ost);
#ifdef RELOAD_DIFFUSION_FRACS
      ost << REAL << " clamped = (output_fracs[" << idx << "] > " << SMALL 
          << " ? output_fracs[" << idx << "] : " << SMALL << ");\n";
#else
      ost << REAL << " clamped = (local_" << MOLE_FRAC << "[" << idx << "] > " << SMALL
          << " ? local_" << MOLE_FRAC << "[" << idx << "] : " << SMALL << ");\n";
#endif
#ifdef DIFFUSION_SHUFFLE 
      ost << REAL << " result = pfac * (sumxw - (diffusion_masses[(wid+" << (idx*unit->threads_per_point) 
          << "+step*" << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2) << "]"
#else
      ost << REAL << " result = pfac * (sumxw - (diffusion_masses[wid+" << (idx*unit->threads_per_point) << "]"
#endif
          << "*clamped)) / (wtm * sumxod[wid+" << (idx*unit->threads_per_point) << "][tid]);\n";
      // Write out the result
      ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(diffusion+(wid+" 
          << (idx*unit->threads_per_point) << ")*spec_stride), " << "\"d\"(result) : \"memory\");\n";
    }
  }
#if 1
#ifdef DIFFUSION_SHUFFLE
  // We need a barrier at the end of the loop
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  ost << "// Update pointers for the next iteration\n";
  // Need to update all our pointers
  {
    PairDelim pointer_pair(ost);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
    ost << "diffusion += slice_stride;\n";
  }
#endif
#endif
}

void TranLib::emit_specialized_diffusion_points(CodeOutStream &ost, unsigned max_specs, unsigned num_points, 
                                                bool uniform, unsigned offset, unsigned total_species, const char *coeff_matrix)
{
  unsigned predicated_threads = 0;
  if (!uniform)
    predicated_threads = ((unit->species.size()-1) % unit->threads_per_point) * 32;
  // Compute the number of threads that are going to register at every barrier
  // Need x2 because half the threads are producers while the other half are consumers
  unsigned num_named_barrier_threads = (unit->threads_per_point/(NAMED_BARRIERS/2))*32*2;
  assert(num_named_barrier_threads > 0);
#ifdef CONTIGUOUS_SPECIES
  unsigned owned_species = total_species/unit->threads_per_point;
#endif

  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (offset+idx*unit->threads_per_point) << ") < " << (unit->species.size()-1) << ")\n";
    
    {
      PairDelim spec_pair(ost);
      // Read the mole fraction for this species, and then update the 
      // the local values of sumxw and wtm
      if (unit->use_double2)
      {
#ifdef CONTIGUOUS_SPECIES
        if (!uniform && (idx == (max_specs-1)))
        {
          ost << REAL2 << " local_frac = make_double2( "
            << MOLE_FRAC << "[wid+" << (offset+idx*unit->threads_per_point) << "][0][tid], "
            << MOLE_FRAC << "[wid+" << (offset+idx*unit->threads_per_point) << "][1][tid]);\n";
          ost << REAL << " local_weight = diffusion_masses[wid+" << (offset+idx*unit->threads_per_point) << "];\n";
        }
        else
        {
          ost << REAL2 << " local_frac = make_double2( "
              << MOLE_FRAC << "[wid*" << owned_species << "+" << (offset+idx) << "][0][tid], "
              << MOLE_FRAC << "[wid*" << owned_species << "+" << (offset+idx) << "][1][tid]);\n";
          ost << REAL << " local_weight = diffusion_masses[wid*" << owned_species << "+" << (offset+idx) << "];\n";
        }
#else
        ost << REAL2 << " local_frac = make_double2( "
            << MOLE_FRAC << "[wid+" << (offset+idx*unit->threads_per_point) << "][0][tid], "
            << MOLE_FRAC << "[wid+" << (offset+idx*unit->threads_per_point) << "][1][tid]);\n";
        ost << REAL << " local_weight = diffusion_masses[wid+" << (offset+idx*unit->threads_per_point) << "];\n";
#endif
        ost << REAL2 << " local_clamped = make_double2( "
            << "(local_frac.x > " << SMALL << " ? local_frac.x : " << SMALL << "), "
            << "(local_frac.y > " << SMALL << " ? local_frac.y : " << SMALL << "));\n";
        ost << "local_wtm.x = __fma_rn(local_frac.x,local_weight,local_wtm.x);\n";
        ost << "local_wtm.y = __fma_rn(local_frac.y,local_weight,local_wtm.y);\n";
        ost << "local_sumxw.x = __fma_rn(local_clamped.x,local_weight,local_sumxw.x);\n";
        ost << "local_sumxw.y = __fma_rn(local_clamped.y,local_weight,local_sumxw.y);\n";
        // Emit variables to keep our local species summation
        ost << REAL2 << " local_dsum = make_double2(0.0,0.0);\n";
      }
      else
      {
#ifdef CONTIGUOUS_SPECIES
        if (!uniform && (idx == (max_specs-1)))
        {
          ost << REAL << " local_frac = " << MOLE_FRAC << "[wid+" << (offset+idx*unit->threads_per_point) << "][tid];\n";
          ost << REAL << " local_weight = diffusion_masses[wid+" << (offset+idx*unit->threads_per_point) << "];\n";
        }
        else
        {
          ost << REAL << " local_frac = " << MOLE_FRAC << "[wid*" << owned_species << "+" << (offset+idx) << "][tid];\n";
          ost << REAL << " local_weight = diffusion_masses[wid*" << owned_species << "+" << (offset+idx) << "];\n";
        }
#else
        ost << REAL << " local_frac = " << MOLE_FRAC << "[wid+" << (offset+idx*unit->threads_per_point) << "][tid];\n";
        ost << REAL << " local_weight = diffusion_masses[wid+" << (offset+idx*unit->threads_per_point) << "];\n";
#endif
        ost << REAL << " local_clamped = (local_frac > " << SMALL << " ? local_frac : " << SMALL << ");\n";
        ost << "local_wtm = __fma_rn(local_frac,local_weight,local_wtm);\n";
        ost << "local_sumxw = __fma_rn(local_clamped,local_weight,local_sumxw);\n";
        // Keep our local dsum here
        ost << REAL << " local_dsum = 0.0;\n";
      }
      // No need for a syncthreads here since we're just reading mole fractions

      // For this species, do as many points in the D matrix as are specified by num_points  
      for (unsigned point_idx = 0; point_idx < num_points; point_idx++)
      {
        PairDelim d_pair(ost);
        // Compute the other index and then make sure it is in the right place
#ifdef CONTIGUOUS_SPECIES
        // Revert back to non-contiguous species if we have left over species to handle
        if (!uniform && (idx == (max_specs-1)))
          ost << "unsigned index = wid + " << (offset+idx*unit->threads_per_point+1+point_idx) << ";\n";
        else
          ost << "unsigned index = wid*" << owned_species << "+" << (offset+idx+1+point_idx) << ";\n";
#else
        ost << "unsigned index = wid + " << (offset+idx*unit->threads_per_point+1+point_idx) << ";\n";
#endif
        // Avoid doing a modulus here
        ost << "if (index >= " << (unit->species.size()-1) << ") index -= " << (unit->species.size()-1) << ";\n"; 
        // Note we can safely load the sumxod here since we know each warp is targeting
        // different species for each point and points are separated by synchronization points
        // By issuing all the shared memory loads at once hopefully they will get pipelined
        if (unit->use_double2)
        {
          ost << REAL2 << " index_frac = make_double2( "
              << MOLE_FRAC << "[index][0][tid], " << MOLE_FRAC << "[index][1][tid]);\n";
          ost << REAL2 << " index_sumxod = make_double2( "
              << "sumxod[index][0][tid], sumxod[index][1][tid]);\n";
          ost << "const " << REAL << " coeff0 = " << coeff_matrix;
#ifdef CONTIGOUS_SPECIES
          if (!uniform && (idx == (max_specs-1)))
            ost << "[wid+" << (idx*unit->threads_per_point) << "][" << point_idx << "][0];\n";
          else
            ost << "[wid*" << owned_species << "+" << idx << "][" << point_idx << "][0];\n";
#else
          ost << "[wid+" << (idx*unit->threads_per_point) << "][" << point_idx << "][0];\n"; 
#endif
          ost << REAL2 << " val = make_double2(coeff0,coeff0);\n";
          // Do the computation for this point
          for (int i = 1; i < order; i++)
          {
#ifdef CONTIGUOUS_SPECIES
            if (!uniform && (idx == (max_specs-1)))
              ost << REAL << " coeff" << i << " = " << coeff_matrix << "[wid+" << (idx*unit->threads_per_point)
                  << "][" << point_idx << "][" << i << "];\n";
            else
              ost << REAL << " coeff" << i << " = " << coeff_matrix << "[wid*" << owned_species << "+" << idx
                  << "][" << point_idx << "][" << i << "];\n";
#else
            ost << REAL << " coeff" << i << " = " << coeff_matrix << "[wid+" << (idx*unit->threads_per_point)
                << "][" << point_idx << "][" << i << "];\n";
#endif
            ost << "val.x = __fma_rn(val.x,logt.x,coeff" << i << ");\n";
            ost << "val.y = __fma_rn(val.y,logt.y,coeff" << i << ");\n";
          }
          ost << "val = make_double2(-val.x,-val.y);\n";
        }
        else
        { 
          ost << REAL << " index_frac = " << MOLE_FRAC << "[index][tid];\n";
          ost << REAL << " index_sumxod = sumxod[index][tid];\n";
#ifdef CONTIGUOUS_SPECIES
          if (!uniform && (idx == (max_specs-1)))
            ost << REAL << " val = " << coeff_matrix << "[wid+" << (idx*unit->threads_per_point)
                << "][" << point_idx << "][0];\n";
          else
            ost << REAL << " val = " << coeff_matrix << "[wid*" << owned_species << "+" << idx
                << "][" << point_idx << "][0];\n";
#else
          ost << REAL << " val = " << coeff_matrix << "[wid+" << (idx*unit->threads_per_point)
              << "][" << point_idx << "][0];\n";
#endif
          // Now we do the computation for this point
          for (int i = 1; i < order; i++)
          {
#ifdef CONTIGUOUS_SPECIES
            if (!uniform && (idx == (max_specs-1)))
              ost << "val = __fma_rn(val,logt," << coeff_matrix << "[wid+" << (idx*unit->threads_per_point)
                  << "][" << point_idx << "][" << i << "]);\n";
            else
              ost << "val = __fma_rn(val,logt," << coeff_matrix << "[wid*" << owned_species << "+" << idx
                  << "][" << point_idx << "][" << i << "]);\n";
#else
            ost << "val = __fma_rn(val,logt," << coeff_matrix << "[wid+" << (idx*unit->threads_per_point)
                << "][" << point_idx << "][" << i << "]);\n";
#endif
          }
          ost << "val = -val;\n";
        }
        // Do the exponentiation
        if (unit->fast_math)
          emit_gpu_exp_taylor_series_expansion(ost,"val",unit->taylor_stages,unit->use_double2);
        else
        {
          if (unit->use_double2)
            ost << "val = make_double2(exp(val.x),exp(val.y));\n";
          else
            ost << "val = exp(val);\n";
        }
        // If this isn't our first time through and it's not our last in a non-uniform
        // case then emit a consumer barrier.
#if 0
        if ((uniform || (idx != (max_specs-1))) && (point_idx > 0))
          ost << "asm volatile(\"bar.sync %0, " << num_named_barrier_threads << ";\" : : \"r\"(cbar"
              << (point_idx%2) << ") : \"memory\");\n";
#endif
        // Now update the values of sumxod, we only need to write back the index species
        // we'll keep updating our local species in registers
        if (unit->use_double2)
        {
#if 0
          ost << REAL2 << " index_frac = make_double2( "
              << MOLE_FRAC << "[index][0][tid], " << MOLE_FRAC << "[index][1][tid]);\n";
          ost << REAL2 << " index_sumxod = make_double2( "
              << "sumxod[index][0][tid], sumxod[index][1][tid]);\n";
#endif
          ost << REAL2 << " index_clamped = make_double2( "
              << "(index_frac.x > " << SMALL << " ? index_frac.x : " << SMALL << "), "
              << "(index_frac.y > " << SMALL << " ? index_frac.y : " << SMALL << "));\n";
          ost << "index_sumxod = make_double2( "
              << "__fma_rn(local_clamped.x,val.x,index_sumxod.x), "
              << "__fma_rn(local_clamped.y,val.y,index_sumxod.y));\n";
          // Issue back to back writes, so that they get pipelined
          ost << "sumxod[index][0][tid] = index_sumxod.x;\n";
          ost << "sumxod[index][1][tid] = index_sumxod.y;\n";
          ost << "local_dsum = make_double2( "
              << "__fma_rn(index_clamped.x,val.x,local_dsum.x), "
              << "__fma_rn(index_clamped.y,val.y,local_dsum.y));\n";
        }
        else
        {
#if 0
          ost << REAL << " index_frac = " << MOLE_FRAC << "[index][tid];\n";
          ost << REAL << " index_sumxod = sumxod[index][tid];\n";
#endif
          ost << REAL << " index_clamped = (index_frac > " << SMALL << " ? index_frac : " << SMALL << ");\n";
          ost << "index_sumxod = __fma_rn(local_clamped,val,index_sumxod);\n";
          ost << "sumxod[index][tid] = index_sumxod;\n";
          ost << "local_dsum = __fma_rn(index_clamped,val,local_dsum);\n";
        }
        // If this isn't our last time through and it's not our last point or non-uniform
        // case then emit an arrival barrier saying that we're done
#if 0
        if ((uniform || (idx != (max_specs-1))) && (point_idx < (num_points-1)))
          ost << "asm volatile(\"bar.arrive %0, " << num_named_barrier_threads << ";\" : : \"r\"(pbar"
              << ((point_idx+1)%2) << ") : \"memory\");\n";
#else
        // Only need to do this 
        // Need a syncthreads before starting the next phase
        if (!uniform && (idx == (max_specs-1)))
        {
          assert(predicated_threads > 0);
          assert((predicated_threads%32) == 0);
          // If the number of predicated threads is 32 then we can skip
          // the barrier since this is warp-synchronous
          if (predicated_threads > 32)
            ost << "asm volatile(\"bar.sync 1, " << predicated_threads << ";\" : : : \"memory\");\n";
        }
        else
        {
#ifdef CONTIGUOUS_SPECIES
          if (((point_idx+1)%owned_species) == 0)
            ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
#else
          ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
#endif
        }
#endif
      }
      // If we didn't just end with a barrier, we need one now
#ifdef CONTIGUOUS_SPECIES
      if ((num_points%owned_species) != 0)
        ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
#endif
      // Now that we're done all the points for this species we need to reduce our local
      // value back into shared memory before starting the next species, so read our the value
      // of sumxod, update it and then write it back.  Note we can do this all in one phase
      // because we know all the warps are handling different species
      if (unit->use_double2)
      {
#ifdef CONTIGUOUS_SPECIES
        if (!uniform && (idx == (max_specs-1)))
        {
          ost << REAL2 << " local_sumxod = make_double2( "
            << "sumxod[wid+" << (offset+idx*unit->threads_per_point) << "][0][tid], "
            << "sumxod[wid+" << (offset+idx*unit->threads_per_point) << "][1][tid]);\n";
        }
        else
        {
          ost << REAL2 << " local_sumxod = make_double2( "
              << "sumxod[wid*" << owned_species << "+" << (offset+idx) << "][0][tid], "
              << "sumxod[wid*" << owned_species << "+" << (offset+idx) << "][1][tid]);\n";
        }
#else
        ost << REAL2 << " local_sumxod = make_double2( "
            << "sumxod[wid+" << (offset+idx*unit->threads_per_point) << "][0][tid], "
            << "sumxod[wid+" << (offset+idx*unit->threads_per_point) << "][1][tid]);\n";
#endif
        ost << "local_sumxod = make_double2( "
            << "local_sumxod.x + local_dsum.x, local_sumxod.y + local_dsum.y);\n";
#ifdef CONTIGUOUS_SPECIES
        if (!uniform && (idx == (max_specs-1)))
        {
          ost << "sumxod[wid*" << owned_species << "+" << (offset+idx) << "][0][tid] = "
              << "local_sumxod.x;\n";
          ost << "sumxod[wid*" << owned_species << "+" << (offset+idx) << "][1][tid] = "
              << "local_sumxod.y;\n";
        }
        else
        {
          ost << "sumxod[wid*" << owned_species << "+" << (offset+idx) << "][0][tid] = "
              << "local_sumxod.x;\n";
          ost << "sumxod[wid*" << owned_species << "+" << (offset+idx) << "][1][tid] = "
              << "local_sumxod.y;\n";
        }
#else
        ost << "sumxod[wid+" << (offset+idx*unit->threads_per_point) << "][0][tid] = "
            << "local_sumxod.x;\n";
        ost << "sumxod[wid+" << (offset+idx*unit->threads_per_point) << "][1][tid] = "
            << "local_sumxod.y;\n";
#endif
      }
      else
      {
#ifdef CONTIGUOUS_SPECIES
        if (!uniform && (idx == (max_specs-1)))
          ost << REAL << " local_sumxod = sumxod[wid+" << (offset+idx*unit->threads_per_point) << "][tid];\n";
        else
          ost << REAL << " local_sumxod = sumxod[wid*"<< owned_species << "+" << (offset+idx) << "][tid];\n";
#else
        ost << REAL << " local_sumxod = sumxod[wid+" << (offset+idx*unit->threads_per_point) << "][tid];\n";
#endif
        ost << "local_sumxod += local_dsum;\n";
#ifdef CONTIGUOUS_SPECIES
        if (!uniform && (idx == (max_specs-1)))
          ost << "sumxod[wid+" << (offset+idx*unit->threads_per_point) << "][tid] = local_sumxod;\n";
        else
          ost << "sumxod[wid*" << owned_species << "+" << (offset+idx) << "][tid] = local_sumxod;\n";
#else
        ost << "sumxod[wid+" << (offset+idx*unit->threads_per_point) << "][tid] = local_sumxod;\n";
#endif
      }
    }
    // Syncthreads at the end of this to make sure everyone is coordinated
    // This is outside the conditional so it will do the right thing
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  }
}

void TranLib::emit_specialized_diffusion_across(CodeOutStream &ost, unsigned full_specs, unsigned partial_specs,
                                                unsigned num_full_points, unsigned num_partial_points,
                                                unsigned total_species, const char *coeff_matrix,
                                                unsigned max_prefetch, bool fits_in_cache)
{
  bool needs_horizontal_check = (partial_specs > 0);
  unsigned num_columns = total_species/unit->threads_per_point;
  bool has_remainder = ((total_species%unit->threads_per_point) != 0);
  // For indexing into the coeff matrix
  unsigned coeff_offset = 0;

  // First we need to compute sumxw and wtm for our species that we
  // own and build our array of mole fractions
  if (unit->use_double2)
  {
    ost << REAL2 << " local_fracs[" << num_columns << "];\n";
    for (unsigned idx = 0; idx < num_columns; idx++)
    {
      ost << "local_fracs[" << idx << "].x = " << MOLE_FRAC << "[wid*" << num_columns << "+" << idx << "][0][tid];\n";
      ost << "local_fracs[" << idx << "].y = " << MOLE_FRAC << "[wid*" << num_columns << "+" << idx << "][1][tid];\n";
    }
    for (unsigned idx = 0; idx < num_columns; idx++)
    {
      PairDelim spec_pair(ost);
      ost << "const " << REAL << " local_mass = diffusion_masses[wid*" << num_columns << "+" << idx << "];\n";
      ost << "local_wtm.x = __fma_rn(local_fracs[" << idx << "].x,local_mass,local_wtm.x);\n";
      ost << "local_fracs[" << idx << "].x = (local_fracs[" << idx << "].x > " << SMALL << " ? "
          << "local_fracs[" << idx << "].x : " << SMALL << ");\n";
      ost << "local_wtm.y = __fma_rn(local_fracs[" << idx << "].y,local_mass,local_wtm.y);\n";
      ost << "local_fracs[" << idx << "].y = (local_fracs[" << idx << "].y > " << SMALL << " ? "
          << "local_fracs[" << idx << "].y : " << SMALL << ");\n";
      ost << "local_sumxw.x = __fma_rn(local_fracs[" << idx << "].x,local_mass,local_sumxw.x);\n";
      ost << "local_sumxw.y = __fma_rn(local_fracs[" << idx << "].y,local_mass,local_sumxw.y);\n";
    }
  }
  else
  {
    ost << REAL << " local_fracs[" << num_columns << "];\n";
    for (unsigned idx = 0; idx < num_columns; idx++)
      ost << "local_fracs[" << idx << "] = " << MOLE_FRAC << "[wid*" << num_columns << "+" << idx << "][tid];\n";
    for (unsigned idx = 0; idx < num_columns; idx++)
    {
      PairDelim spec_pair(ost);
#ifdef DIFFUSION_SHUFFLE
      ost << "const " << REAL << " local_mass = diffusion_masses[(wid*" << num_columns << "+" << idx 
          << "+step*" << num_species << ")%" << num_species << "];\n";
#else
      ost << "const " << REAL << " local_mass = diffusion_masses[wid*" << num_columns << "+" << idx << "];\n";
#endif
      ost << "local_wtm = __fma_rn(local_fracs[" << idx << "],local_mass,local_wtm);\n";
      ost << "local_fracs[" << idx << "] = (local_fracs[" << idx << "] > " << SMALL << " ? "
          << "local_fracs[" << idx << "] : " << SMALL << ");\n";
      ost << "local_sumxw = __fma_rn(local_fracs[" << idx << "],local_mass,local_sumxw);\n";
    }
  }
  // Now we can emit code for storing our local sumxod values
  if (unit->use_double2)
  {
    ost << REAL2 << " local_sumxod[" << num_columns << "];\n";
    for (unsigned idx = 0; idx < num_columns; idx++)
      ost << "local_sumxod[" << idx << "] = make_double2(0.0,0.0);\n";
  }
  else
  {
    ost << REAL << " local_sumxod[" << num_columns << "];\n";
    for (unsigned idx = 0; idx < num_columns; idx++)
      ost << "local_sumxod[" << idx << "] = 0.0;\n";
  }
  // Now what we want to do is iterate over the number of full points that we have, and for
  // each of them see how many we have to cut across
  for (unsigned point_idx = 0; point_idx < (num_full_points+num_columns-1); point_idx++)
  {
    unsigned start_across = (point_idx < num_full_points ? 0 : point_idx-num_full_points+1);
    unsigned stop_across = (point_idx < num_columns ? point_idx+1 : num_columns);
    bool needs_check = needs_horizontal_check && (point_idx >= (num_partial_points));
    ost << "// Starting point " << point_idx << "\n";
    PairDelim across_pair(ost);
    // First emit code to get the mole fractions and sumxod for this species
    //ost << "unsigned index = (wid*" << num_columns << "+" << (point_idx+1) << ")%" << total_species << ";\n";
#ifdef DIFFUSION_SHUFFLE
    //ost << "unsigned index = (wid*" << num_columns << "+" << (point_idx+1) << "+step*" << total_species << ")%" << total_species << ";\n";
    ost << "unsigned index = (wid*" << num_columns << "+" << (point_idx+1) << "+step*" << next_largest_power(2*total_species,2) 
        << ")%" << next_largest_power(2*total_species,2) << ";\n";
#else
    ost << "unsigned index = wid*" << num_columns << "+" << (point_idx+1) << ";\n";
#endif
    // Handle the case of wrapped index
    ost << "if (index >= " << total_species << ") index -= " << total_species << ";\n";
    if (unit->use_double2)
    {
      ost << REAL2 << " index_frac = make_double2("
          << MOLE_FRAC << "[index][0][tid], " << MOLE_FRAC << "[index][1][tid]);\n";
      ost << REAL2 << " index_sumxod = make_double2( "
          << "sumxod[index][0][tid], sumxod[index][1][tid]);\n";
      ost << REAL2 << " index_clamped = make_double2( "
            << "(index_frac.x > " << SMALL << " ? index_frac.x : " << SMALL << "), "
            << "(index_frac.y > " << SMALL << " ? index_frac.y : " << SMALL << "));\n";
    }
    else
    {
      ost << REAL << " index_frac = " << MOLE_FRAC << "[index][tid];\n";
      ost << REAL << " index_sumxod = sumxod[index][tid];\n";
      ost << REAL << " index_clamped = (index_frac > " << SMALL << " ? index_frac : " << SMALL << ");\n";
    }
    // Now across this index point
    for (unsigned across_idx = start_across; across_idx < stop_across; across_idx++)
    {
      ost << "// Across point " << across_idx << " for point " << point_idx << "\n";
      if (needs_check)
        ost << "if ((wid*" << num_columns << "+" << across_idx << ") < " << partial_specs << ")\n";
      PairDelim index_pair(ost);
      assert(point_idx >= across_idx); // better always be true
#if 0
      if ((point_idx != (num_full_points+num_columns-2)) || (across_idx != (stop_across-1)))
      {
        ost << "asm volatile(\"prefetchu.L1 [%0];\" : : \"l\"(const_ptr+wid*" << (order*num_species) << "+"
            << ((coeff_offset+1)*order) << ") : \"memory\");\n";
      }
#endif
#ifndef DIFFUSION_SHUFFLE
      if (unit->use_double2)
      {
        if (fits_in_cache)
          ost << REAL << " coeff0 = " << coeff_matrix << "[wid][" << coeff_offset << "][0];\n";
        else
          ost << REAL << " coeff0 = coeff_prefetch[" << (coeff_offset%MIN(DIFFUSION_BUFFERING,max_prefetch)) << "][0];\n";
        ost << REAL2 << " val = make_double2(coeff0,coeff0);\n";
      }
      else
      {
        if (fits_in_cache)
          ost << REAL << " val = " << coeff_matrix << "[wid][" << coeff_offset << "][0];\n";
        else
          ost << REAL << " val = coeff_prefetch[" << (coeff_offset%MIN(DIFFUSION_BUFFERING,max_prefetch)) << "][0];\n";
      }
      // Emit the load for the next prefetch
      if (!fits_in_cache && (diffusion_depth < max_prefetch))
      {
        char dst[128];
        sprintf(dst,"coeff_prefetch[%d][0]",(coeff_offset%MIN(DIFFUSION_BUFFERING,max_prefetch)));
        char src[128];
        sprintf(src,"&(across_coeffs[wid][%d][0])",diffusion_depth);
        emit_cuda_load(ost,dst,src,false/*double2*/,".ca");
      }
#else
      // Figure out the index and then compute the index and lane of the thread that owns the constant
      unsigned constant_index = coeff_offset*order+0;
      unsigned lane_index = constant_index%32;
      unsigned thread_index = constant_index/32;
      ost << INT << " lo_part, hi_part;\n";
      ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n";
      ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n";
      if (unit->use_double2)
      {
        ost << REAL2 << " val = make_double2(__hiloint2double(hi_part,lo_part),__hiloint2double(hi_part,lo_part));\n";
      }
      else
      {
        ost << REAL << " val = __hiloint2double(hi_part,lo_part);\n";
      }
#endif
      for (int i = 1; i < order; i++)
      {
#ifndef DIFFUSION_SHUFFLE
        if (unit->use_double2)
        {
          if (fits_in_cache)
            ost << REAL << " coeff" << i << " = " << coeff_matrix << "[wid][" << coeff_offset << "][" << i << "];\n";
          else
            ost << REAL << " coeff" << i << " = coeff_prefetch[" << (coeff_offset%MIN(DIFFUSION_BUFFERING,max_prefetch)) << "][" << i << "];\n";
          ost << "val = make_double2( "
              << "__fma_rn(val.x,logt.x,coeff" << i << "), "
              << "__fma_rn(val.y,logt.y,coeff" << i << "));\n";
        }
        else
        {
          if (fits_in_cache)
            ost << "val = __fma_rn(val,logt," << coeff_matrix << "[wid][" << coeff_offset << "][" << i << "]);\n";
          else
            ost << "val = __fma_rn(val,logt,coeff_prefetch[" << (coeff_offset%MIN(DIFFUSION_BUFFERING,max_prefetch)) << "][" << i << "]);\n";
        }
        // Issue the next load if we can
        if (!fits_in_cache && (diffusion_depth < max_prefetch))
        {
          char dst[128];
          sprintf(dst,"coeff_prefetch[%d][%d]",(coeff_offset%MIN(DIFFUSION_BUFFERING,max_prefetch)),i);
          char src[128];
          sprintf(src,"&(across_coeffs[wid][%d][%d])",diffusion_depth,i);
          emit_cuda_load(ost,dst,src,false/*double2*/,".ca");
        }
#else
        // compute the locations
        constant_index = coeff_offset*order+i;
        lane_index = constant_index%32;
        thread_index = constant_index/32;
        ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n";
        ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n";
        if (unit->use_double2)
        {
          ost << REAL << " coeff" << i << " = __hiloint2double(hi_part,lo_part);\n";
          ost << "val = make_double2( "
              << "__fma_rn(val.x,logt.x,coeff" << i << "), "
              << "__fma_rn(val.y,logt.y,coeff" << i << "));\n";
        }
        else
        {
          ost << "val = __fma_rn(val,logt,__hiloint2double(hi_part,lo_part));\n"; 
        }
#endif
      }
      // Update the coeff offset
      coeff_offset++;
      if (!fits_in_cache)
        diffusion_depth++;

      if (unit->use_double2)
        ost << "val = make_double2(-val.x,-val.y);\n";
      else
        ost << "val = -val;\n";
      if (unit->fast_math)
        emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, unit->use_double2);
      else
      {
        if (unit->use_double2)
          ost << "val = make_double2(exp(val.x),exp(val.y));\n";
        else
          ost << "val = exp(val);\n";
      }
      // Now update both the index species and our local value
      if (unit->use_double2)
      { 
        ost << "index_sumxod.x = __fma_rn(local_fracs[" << across_idx << "].x,val.x,index_sumxod.x);\n";
        ost << "index_sumxod.y = __fma_rn(local_fracs[" << across_idx << "].y,val.y,index_sumxod.y);\n";
        ost << "local_sumxod[" << across_idx << "].x = __fma_rn(index_clamped.x,val.x,local_sumxod[" << across_idx << "].x);\n";
        ost << "local_sumxod[" << across_idx << "].y = __fma_rn(index_clamped.y,val.y,local_sumxod[" << across_idx << "].y);\n";
      }
      else
      { 
        ost << "index_sumxod = __fma_rn(local_fracs[" << across_idx << "],val,index_sumxod);\n";
        ost << "local_sumxod[" << across_idx << "] = __fma_rn(index_clamped,val,local_sumxod[" << across_idx << "]);\n";
      }
    }
    // We've finished this across point, so write it's result back to shared
    if (unit->use_double2)
    {
      ost << "sumxod[index][0][tid] = index_sumxod.x;\n";
      ost << "sumxod[index][1][tid] = index_sumxod.y;\n";
    }
    else
      ost << "sumxod[index][tid] = index_sumxod;\n";
    // We only need barriers after the every num_column number of points (Yay group theory!)
    // or it is the last iteration through
    if ((((point_idx+1)%num_columns) == 0) || (point_idx == (num_full_points+num_columns-2)))
      ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  }
  // Now that we've finished all of our points, we need to reduce them out to shared
  // We can do this in one synchronization block since we know each warp has a uniqely
  // different set of species.
  // Try to do all shared memory operations in batches to get pipelining
  if (unit->use_double2)
  {
    PairDelim reduc_pair(ost);
    ost << REAL2 << " current_sumxod[" << num_columns << "];\n";
    for (unsigned idx = 0; idx < num_columns; idx++)
      ost << "current_sumxod[" << idx << "] = make_double2( "
          << "sumxod[wid*" << num_columns << "+" << idx << "][0][tid], "
          << "sumxod[wid*" << num_columns << "+" << idx << "][1][tid]);\n";
    for (unsigned idx = 0; idx < num_columns; idx++)
    {
      ost << "current_sumxod[" << idx << "].x += local_sumxod[" << idx << "].x;\n";
      ost << "current_sumxod[" << idx << "].y += local_sumxod[" << idx << "].y;\n";
    }
    for (unsigned idx = 0; idx < num_columns; idx++)
    {
      ost << "sumxod[wid*" << num_columns << "+" << idx << "][0][tid] = current_sumxod[" << idx << "].x;\n";
      ost << "sumxod[wid*" << num_columns << "+" << idx << "][1][tid] = current_sumxod[" << idx << "].y;\n";
    }
  }
  else
  {
    PairDelim reduc_pair(ost);
    // Read everything out of shared first
    ost << REAL << " current_sumxod[" << num_columns << "];\n";
    for (unsigned idx = 0; idx < num_columns; idx++)
      ost << "current_sumxod[" << idx << "] = sumxod[wid*" << num_columns << "+" << idx << "][tid];\n";
    // Do the reductions
    for (unsigned idx = 0; idx < num_columns; idx++)
      ost << "current_sumxod[" << idx << "] += local_sumxod[" << idx << "];\n";
    // Then write it back
    for (unsigned idx = 0; idx < num_columns; idx++)
      ost << "sumxod[wid*" << num_columns << "+" << idx << "][tid] = current_sumxod[" << idx << "];\n";
  }
  // Put a barrier after we're done so we know we're done
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  // Check to see if we have leftover rows that we need to handle
  if (has_remainder)
  {
    // Handle the remainder species
    unsigned offset = unit->threads_per_point*num_columns;
    assert(offset < total_species);
    unsigned predicated_threads = (total_species-offset)*32;
    ost << "// Handling remainder species\n";
    ost << "if ((wid+" << offset << ") < " << total_species << ")\n";
    PairDelim remainder_pair(ost);
    // First emit the code to update our local values of wtm and sumxw
    if (unit->use_double2)
    {
      ost << REAL2 << " local_frac = make_double2( "
          << MOLE_FRAC << "[wid+" << (offset) << "][0][tid], "
          << MOLE_FRAC << "[wid+" << (offset) << "][1][tid]);\n";
      ost << REAL << " local_mass = diffusion_masses[wid+" << offset << "];\n";
      ost << "local_wtm.x = __fma_rn(local_frac.x,local_mass,local_wtm.x);\n";
      ost << "local_wtm.y = __fma_rn(local_frac.y,local_mass,local_wtm.y);\n";
      ost << REAL2 << " local_clamped = make_double2( "
          << "(local_frac.x > " << SMALL << " ? local_frac.x : " << SMALL << "), "
          << "(local_frac.y > " << SMALL << " ? local_frac.y : " << SMALL << "));\n";
      ost << "local_sumxw.x = __fma_rn(local_clamped.x,local_mass,local_sumxw.x);\n";
      ost << "local_sumxw.y = __fma_rn(local_clamped.y,local_mass,local_sumxw.y);\n";
      ost << REAL2 << " local_dsum = make_double2(0.0,0.0);\n";
    }
    else
    {
      ost << REAL << " local_frac = " << MOLE_FRAC << "[wid+" << (offset) << "][tid];\n";
      ost << REAL << " local_mass = diffusion_masses[wid+" << offset << "];\n";
      ost << "local_wtm = __fma_rn(local_frac,local_mass,local_wtm);\n";
      ost << REAL << " local_clamped = (local_frac > " << SMALL << " ? local_frac : " << SMALL << ");\n";
      ost << "local_sumxw = __fma_rn(local_frac,local_mass,local_sumxw);\n";
      ost << REAL << " local_dsum = 0.0;\n";
    }
    // Now iterate over the points for this species
    unsigned num_remainder_points = (((total_species%2)==1) ? num_full_points : num_partial_points);
    // A check to make sure that we are either handling all partial points
    // and not a combination of partial points.  This will only fail in mechanisms
    // with very few species and large numbers of threads per points which should
    // be uncommon so we're going to ignore it for now
    if (num_remainder_points == num_partial_points)
      assert(unit->threads_per_point <= num_partial_points);
    for (unsigned point_idx = 0; point_idx < num_remainder_points; point_idx++)
    {
      PairDelim point_pair(ost);
#ifndef DIFFUSION_SHUFFLE
      ost << "unsigned index = wid + " << (offset+1+point_idx) << ";\n";
      //ost << "if (index >= " << total_species << ") index -= " << total_species << ";\n";
#else
      //ost << "unsigned index = (wid + " << (offset+1+point_idx) << "+ step*" << total_species << ")%" << total_species << ";\n";
      ost << "unsigned index = (wid + " << (offset+1+point_idx) << " + step*" << next_largest_power(2*total_species,2)
          << ")%" << next_largest_power(2*total_species,2) << ";\n";
#endif
      ost << "if (index >= " << total_species << ") index -= " << total_species << ";\n";
      if (unit->use_double2)
      {
        ost << REAL2 << " index_frac = make_double2( "
            << MOLE_FRAC << "[index][0][tid], " << MOLE_FRAC << "[index][1][tid]);\n";
        ost << REAL2 << " index_sumxod = make_double2( "
            << "sumxod[index][0][tid], sumxod[index][1][tid]);\n";
#ifdef DIFFUSION_SHUFFLE
        unsigned constant_index = coeff_offset*order+0;
        unsigned lane_index = constant_index%32;
        unsigned thread_index = constant_index/32;
        ost << INT << " hi_part, lo_part;\n";
        ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n"; 
        ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n";
        ost << REAL2 << " val = make_double2(__hiloint2double(hi_part,lo_part), __hiloint2double(hi_part,lo_part));\n";
        for (int i = 1; i < order; i++)
        {
          constant_index = coeff_offset*order+i;
          lane_index = constant_index%32;
          thread_index = constant_index/32;
          ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n"; 
          ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n";
          ost << REAL << " coeff" << i << " = __hiloint2double(hi_part,lo_part);\n";
          ost << "val.x = __fma_rn(val.x,logt.x,coeff" << i << ");\n";
          ost << "val.y = __fma_rn(val.y,logt.y,coeff" << i << ");\n";
        }
#else
        ost << REAL << " coeff0 = " << coeff_matrix << "[wid][" << coeff_offset << "][0];\n";
        ost << REAL2 << " val = make_double2(coeff0,coeff0);\n";
        for (int i = 1; i < order; i++)
        {
          ost << REAL << " coeff" << i << " = " << coeff_matrix << "[wid][" << coeff_offset << "][" << i << "];\n";
          ost << "val.x = __fma_rn(val.x,logt.x,coeff" << i << ");\n";
          ost << "val.y = __fma_rn(val.y,logt.y,coeff" << i << ");\n";
        }
#endif
        ost << "val = make_double2(-val.x,-val.y);\n";
      }
      else
      {
        ost << REAL << " index_frac = " << MOLE_FRAC << "[index][tid];\n";
        ost << REAL << " index_sumxod = sumxod[index][tid];\n";
#ifdef DIFFUSION_SHUFFLE
        unsigned constant_index = coeff_offset*order+0;
        unsigned lane_index = constant_index%32;
        unsigned thread_index = constant_index/32;
        ost << INT << " hi_part, lo_part;\n";
        ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n";
        ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n";
        ost << REAL << " val = __hiloint2double(hi_part,lo_part);\n";
        for (int i = 1; i < order; i++)
        {
          constant_index = coeff_offset*order+i;
          lane_index = constant_index%32;
          thread_index = constant_index/32;
          ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n";
          ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ");\n";
          ost << "val = __fma_rn(val,logt,__hiloint2double(hi_part,lo_part));\n";
        }
#else
        ost << REAL << " val = " << coeff_matrix << "[wid][" << coeff_offset << "][0];\n";
        for (int i = 1; i < order; i++)
        {
          ost << "val = __fma_rn(val,logt," << coeff_matrix << "[wid][" << coeff_offset << "][" << i << "]);\n";
        }
#endif
        ost << "val = -val;\n";
      }
      // Update the coeff offset
      coeff_offset++;

      if (unit->fast_math)
        emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, unit->use_double2);
      else
      {
        if (unit->use_double2)
          ost << "val = make_double2(exp(val.x),exp(val.y));\n";
        else
          ost << "val = exp(val);\n";
      }
      if (unit->use_double2)
      {
        ost << REAL2 << " index_clamped = make_double2( "
            << "(index_frac.x > " << SMALL << " ? index_frac.x : " << SMALL << "), "
            << "(index_frac.y > " << SMALL << " ? index_frac.y : " << SMALL << "));\n";
        ost << "index_sumxod.x = __fma_rn(local_clamped.x,val.x,index_sumxod.x);\n";
        ost << "index_sumxod.y = __fma_rn(local_clamped.y,val.y,index_sumxod.y);\n";
        ost << "sumxod[index][0][tid] = index_sumxod.x;\n";
        ost << "sumxod[index][1][tid] = index_sumxod.y;\n";
        ost << "local_dsum.x = __fma_rn(index_clamped.x,val.x,local_dsum.x);\n";
        ost << "local_dsum.y = __fma_rn(index_clamped.y,val.y,local_dsum.y);\n";
      }
      else
      {
        ost << REAL << " index_clamped = (index_frac > " << SMALL << " ? index_frac : " << SMALL << ");\n";
        ost << "index_sumxod = __fma_rn(local_clamped,val,index_sumxod);\n";
        ost << "sumxod[index][tid] = index_sumxod;\n";
        ost << "local_dsum = __fma_rn(index_clamped,val,local_dsum);\n";
      }
      // Need a barrier here, unless it is warp-synchronous
      if (predicated_threads > 32)
        ost << "asm volatile(\"bar.sync 1, " << predicated_threads << ";\" : : : \"memory\");\n";
    }
    // When we're done with all our points, then we get to write our result back
    if (unit->use_double2)
    {
      ost << REAL2 << " local_sumxod = make_double2( "
          << "sumxod[wid+" << offset << "][0][tid], sumxod[wid+" << offset << "][1][tid]);\n";
      ost << "local_sumxod.x += local_dsum.x;\n";
      ost << "local_sumxod.y += local_dsum.y;\n";
      ost << "sumxod[wid+" << offset << "][0][tid] = local_sumxod.x;\n";
      ost << "sumxod[wid+" << offset << "][1][tid] = local_sumxod.y;\n";
    }
    else
    {
      ost << REAL << " local_sumxod = sumxod[wid+" << offset << "][tid];\n";
      ost << "local_sumxod += local_dsum;\n";
      ost << "sumxod[wid+" << offset << "][tid] = local_sumxod;\n";
    }
  }
  // If we had a remainder, then we need an extra barrier here
  if (has_remainder)
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
}

void TranLib::emit_specialized_diffusion_declaration(CodeOutStream &ost, bool bounds)
{
  ost << "__global__ void\n";
  {
    int warps_per_cta = unit->threads_per_point;
    int shared_memory = 32/*min points*/ * (unit->use_double2 ? 16 : 8)/*bytes per element*/
                        * (unit->species.size()-1)/*number of species*/;
    // If we're putting mole fractions in shared too then double
    // the amount of shared memory we're using
    shared_memory *= 2;
    if (shared_memory > MAX_SHARED)
    {
      fprintf(stderr,"Too many species to fit in shared memory for warp-specialized diffusion: needed %d bytes\n", shared_memory);
      exit(1);
    }
    // Figure out how many CTAs we can get on an SM
    // Assuming we are shared memory bound
    int ctas_per_sm = MAX_SHARED/shared_memory;
    // Now compute how many warps this will put on an SM
    int total_warps = ctas_per_sm * warps_per_cta;
    fprintf(stdout,"    Warp-Specialized Diffusion Kernel:\n");
    fprintf(stdout,"      Warps-per-CTA:         %d\n", warps_per_cta);
    fprintf(stdout,"      Shared Memory per CTA: %d\n", shared_memory);
    fprintf(stdout,"      CTAs-per-SM:           %d\n", ctas_per_sm);
    fprintf(stdout,"      Warps-per-SM:          %d\n", total_warps);
    if (bounds)
      ost << "__launch_bounds__(" << (32*warps_per_cta) << "," << ctas_per_sm << ")\n";
  }
  ost << "gpu_diffusion(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << PRESSURE_ARRAY;
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "diffusion";
  ost << ")";
}

void TranLib::emit_blocked_diffusion(CodeOutStream &ost)
{
  const unsigned num_species = unit->species.size()-1; 
  this->diffusion_kernels = 0;
  unsigned total_memory_ops = 0;
#if 0
  unsigned diff_block = unit->diff_block_h;
  unsigned steps = (num_species+(diff_block-1))/diff_block;
  for (unsigned h = 0; h < steps; h++)
  {
    for (unsigned v = 0; v <= h; v++)
    {
      unsigned h_start = h*diff_block;
      unsigned h_stop = (h+1)*diff_block;
      // clamp the stop species if necessary
      if (h_stop > num_species) h_stop = num_species;

      unsigned v_start = v*diff_block;
      unsigned v_stop = (v+1)*diff_block;
      // clamp the stop species if necessary
      if (v_stop > h_stop) v_stop = h_stop;
      // We only need to emit the kernel if it has work to do
      if ((h_start == v_start) && ((h_stop-h_start)==1)
          && (v > 0) && (h < (steps-1)))
      {
        assert((v_stop-v_start)==1);
        continue;
      }
      total_memory_ops += emit_blocked_diffusion_kernel(ost, h_start, h_stop, this->diffusion_kernels, 
                                                        v_start, v_stop, (v==0), (h==(steps-1)),
                                                        (v==0), (h==0));
      ost << "\n";
      this->diffusion_kernels++;
    }
  }
#else
  unsigned h_steps = (num_species+(unit->diff_block_h-1))/unit->diff_block_h;
  for (unsigned h = 0; h < h_steps; h++)
  {
    unsigned h_start = h*unit->diff_block_h;
    unsigned h_stop = (h+1)*unit->diff_block_h;
    // clamp the stop species if necessary
    if (h_stop > num_species) h_stop = num_species;
    // For doing the v's, we have to start at the top to make sure we get a square corner first
    if ((h_stop < unit->diff_block_v) || (h==0))
    {
      // Only one block here
      total_memory_ops += emit_blocked_diffusion_kernel(ost, h_start, h_stop, this->diffusion_kernels,
                                                        0/*v_start*/, h_stop, true, (h==(h_steps-1)), true, true);
      ost << "\n";
      this->diffusion_kernels++;
    }
    else
    {
      
      unsigned v_steps = (h_start+(unit->diff_block_v-1))/unit->diff_block_v;
      for (unsigned v = 0; v < v_steps; v++)
      {
        unsigned v_start = v*unit->diff_block_v;
        unsigned v_stop = (v+1)*unit->diff_block_v;
        // clamp the stop species if necessary
        if (v_stop > h_start) v_stop = h_start;
        total_memory_ops += emit_blocked_diffusion_kernel(ost, h_start, h_stop, this->diffusion_kernels,
                                                        v_start, v_stop, (v==0), (h==(h_steps-1)),
                                                        (v==0), false);
        ost << "\n";
        this->diffusion_kernels++;
      }
      total_memory_ops += emit_blocked_diffusion_kernel(ost, h_start, h_stop, this->diffusion_kernels,
                                                        h_start, h_stop, false, (h==(h_steps-1)), false, false);
      ost << "\n";
      this->diffusion_kernels++;
    }
  }
#endif
  fprintf(stdout,"  INFO: emitted %d blocked diffusion kernels\n", this->diffusion_kernels);
  fprintf(stdout,"  INFO: total memory ops per point for diffusion kernels: %d\n", total_memory_ops);
}

unsigned TranLib::emit_blocked_diffusion_kernel(CodeOutStream &ost, unsigned start_h_spec, unsigned stop_h_spec,
                                           unsigned kernel_idx, unsigned start_v_spec, unsigned stop_v_spec,
                                           bool compute_sums, bool do_output, 
                                           bool initialize_h, bool initialize_v)
{
  ost << "// Diffusion kernel info:\n";
  ost << "//  h_start = " << start_h_spec << "\n";
  ost << "//  h_stop  = " << stop_h_spec << "\n";
  ost << "//  v_start = " << start_v_spec << "\n";
  ost << "//  v_stop  = " << stop_v_spec << "\n";
  unsigned num_memory_ops = 0;
  assert(start_h_spec < stop_h_spec);
  assert(start_v_spec < stop_v_spec);
  emit_blocked_diffusion_declaration(ost, kernel_idx);
  ost << "\n";
  PairDelim pair(ost);
  // compute the offset for this block
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = blockIdx.y*row_stride + "
        << "(blockIdx.x*blockDim.x + threadIdx.x)*sizeof(" 
        << (unit->use_double2 ? REAL2 : REAL) << ")/sizeof(" << REAL << ")"
        << "+blockIdx.z*slice_stride;\n";
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
    ost << "temp_wtm += offset;\n";
    ost << "temp_sumxw += offset;\n";
    ost << "diffusion += offset;\n";
  }
  if (start_h_spec == start_v_spec)
  {
    bool has_d_elements = ((stop_h_spec-start_h_spec) > 1);
    // Better be doing at least one of these things or why bother having a kernel
    assert(has_d_elements || compute_sums || do_output);
    // Handle the diagonal case where the horizontal and vertical species are the same
    assert(stop_h_spec == stop_v_spec);
    ost << (unit->use_double2 ? REAL2 : REAL) << " " << MOLE_FRAC << "[" << (stop_h_spec-start_h_spec) << "];\n";
    ost << (unit->use_double2 ? REAL2 : REAL) << " sumxod[" << (stop_h_spec-start_h_spec) << "];\n";

    // Emit the temperature load first
    if (has_d_elements)
    {
      ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << ";\n";
      emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, "0", unit->use_double2);
      num_memory_ops++;
    }
    // If we're computing sums or doing output in this kernel emit the code to either load or initialize it
    if (compute_sums || do_output)
    {
      ost << (unit->use_double2 ? REAL2 : REAL) << " wtm;\n";
      ost << (unit->use_double2 ? REAL2 : REAL) << " sumxw;\n";
      if (kernel_idx > 0)
      {
        emit_cuda_load(ost, "wtm", "temp_wtm", "0", unit->use_double2);
        emit_cuda_load(ost, "sumxw", "temp_sumxw", "0", unit->use_double2);
        num_memory_ops+=2;
      }
      else
      {
        if (unit->use_double2)
        {
          ost << "wtm = make_double2(0.0,0.0);\n";
          ost << "sumxw = make_double2(0.0,0.0);\n";
        }
        else
        {
          ost << "wtm = 0.0;\n";
          ost << "sumxw = 0.0;\n";
        }
      }
    }
    emit_blocked_species_load(ost,start_h_spec,stop_h_spec,MOLE_FRAC_ARRAY,MOLE_FRAC);
    num_memory_ops += (stop_v_spec-start_v_spec);
    // should be the same for the square parts
    assert(initialize_v == initialize_h);
    if (!initialize_v)
    {
      emit_blocked_species_load(ost,start_h_spec,stop_h_spec,"diffusion","sumxod");
      num_memory_ops += (stop_h_spec-start_h_spec);
    }
    else
    {
      for (unsigned idx = 0; idx < (stop_h_spec-start_h_spec); idx++)
      {
        if (unit->use_double2)
          ost << "sumxod[" << idx << "] = make_double2(0.0,0.0);\n";
        else
          ost << "sumxod[" << idx << "] = 0.0;\n";
      }
    }
    // Emit pressure load if we're going to need it
    if (do_output)
    {
      ost << (unit->use_double2 ? REAL2 : REAL) << " " << PRESSURE << ";\n";
      emit_cuda_load(ost, PRESSURE, PRESSURE_ARRAY, "0", unit->use_double2);
      num_memory_ops++;
    }
    if (has_d_elements)
    {
      if (unit->use_double2)
        ost << REAL2 << " logt = make_double2(log(" << TEMPERATURE << ".x), log(" << TEMPERATURE << ".y));\n";
      else
        ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
    }

    if (compute_sums)
    {
      // Update the sums for our horizontal species
      for (unsigned spec_idx = start_h_spec; spec_idx < stop_h_spec; spec_idx++)
      {
        unsigned local_idx = spec_idx-start_h_spec;
        if (unit->use_double2)
        {
          ost << "wtm = make_double2( "
              << "__fma_rn(mole_frac[" << local_idx << "].x, "
              << nwt[spec_idx] << ",wtm.x), "
              << "__fma_rn(mole_frac[" << local_idx << "].y, "
              << nwt[spec_idx] << ",wtm.y));\n";
          // Now clamp and then redo into sumxw
          ost << "mole_frac[" << local_idx << "] = make_double2( "
              << "(mole_frac[" << local_idx << "].x > " << SMALL << " ? "
              << "mole_frac[" << local_idx << "].x : " << SMALL << "), "
              << "(mole_frac[" << local_idx << "].y > " << SMALL << " ? "
              << "mole_frac[" << local_idx << "].y : " << SMALL << "));\n";
          ost << "sumxw = make_double2( "
              << "__fma_rn(mole_frac[" << local_idx << "].x, "
              << nwt[spec_idx] << ",sumxw.x), "
              << "__fma_rn(mole_frac[" << local_idx << "].y, "
              << nwt[spec_idx] << ",sumxw.y));\n";
        }
        else
        {
          ost << "wtm = "
              << "__fma_rn(mole_frac[" << local_idx << "]," 
              << nwt[spec_idx] << ",wtm);\n";
          ost << "mole_frac[" << local_idx << "] = "
              << "(mole_frac[" << local_idx << "] > " << SMALL << " ? "
              << "mole_frac[" << local_idx << "] : " << SMALL << ");\n";
          ost << "sumxw = "
              << "__fma_rn(mole_frac[" << local_idx << "]," 
              << nwt[spec_idx] << ",sumxw);\n";
        }
      }
      // Write these values out no matter what
      // Mark all these cache global since they are likely to be reused
      if (unit->use_double2)
      {
        ost << "asm volatile(\"st.global.cg.v2.f64 [%0], {%1,%2};\" : : \"l\"(temp_wtm), "
            << "\"d\"(wtm.x), \"d\"(wtm.y) : \"memory\");\n";
        ost << "asm volatile(\"st.global.cg.v2.f64 [%0], {%1,%2};\" : : \"l\"(temp_sumxw), "
            << "\"d\"(sumxw.x), \"d\"(sumxw.y) : \"memory\");\n";
      }
      else
      {
        ost << "asm volatile(\"st.global.cg.f64 [%0], %1;\" : : \"l\"(temp_wtm), "
            << "\"d\"(wtm) : \"memory\");\n";
        ost << "asm volatile(\"st.global.cg.f64 [%0], %1;\" : : \"l\"(temp_sumxw), "
            << "\"d\"(sumxw) : \"memory\");\n";
      }
      num_memory_ops+=2;
    }
    else
    {
      // Just clamp the horizontal species
      for (unsigned spec_idx = start_h_spec; spec_idx < stop_h_spec; spec_idx++)
      {
        unsigned local_idx = spec_idx-start_h_spec;
        if (unit->use_double2)
          ost << "mole_frac[" << local_idx << "] = make_double2( "
              << "(mole_frac[" << local_idx << "].x > " << SMALL << " ? "
              << "mole_frac[" << local_idx << "].x : " << SMALL << "), "
              << "(mole_frac[" << local_idx << "].y > " << SMALL << " ? "
              << "mole_frac[" << local_idx << "].y : " << SMALL << "));\n";
        else
          ost << "mole_frac[" << local_idx << "] = "
              << "(mole_frac[" << local_idx << "] > " << SMALL << " ? "
              << "mole_frac[" << local_idx << "] : " << SMALL << ");\n";
      }
    }

    // Now do the computations for the sumxod
    if (has_d_elements)
    {
      const unsigned num_species = (unit->species.size()-1);
      for (unsigned hidx = start_h_spec; hidx < stop_h_spec; hidx++)
      {
        for (unsigned vidx = start_v_spec; vidx <= hidx; vidx++)
        {
          if (hidx == vidx)
            continue;
          ost << "// D_" << hidx << "_" << vidx << " and D_" << vidx << "_" << hidx << "\n";
          PairDelim dpair(ost);
          if (unit->use_double2)
          {
            ost << REAL << " coeff0 = " << ndif[(order-1)+hidx*order+vidx*order*num_species] << ";\n";
            ost << REAL2 << " val = make_double2(coeff0,coeff0);\n";
            for (int k = order-1; k > 0; k--)
            {
              ost << REAL << " coeff" << k << " = " << ndif[(k-1)+hidx*order+vidx*order*num_species] << ";\n";
              ost << "val = make_double2( "
                  << "__fma_rn(val.x,logt.x,coeff" << k << "), "
                  << "__fma_rn(val.y,logt.y,coeff" << k << "));\n";
            }
            ost << "val = make_double2(-val.x,-val.y);\n";
          }
          else
          {
            ost << REAL << " val = " << ndif[(order-1)+hidx*order+vidx*order*num_species] << ";\n";
            for (int k = order-1; k > 0; k--)
            {
              ost << "val = __fma_rn(val,logt," << ndif[(k-1)+hidx*order+vidx*order*num_species] << ");\n";
            }
            ost << "val = -val;\n";
          }
          // do the exponent
          if (unit->fast_math)
            emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, unit->use_double2);
          else
          {
            if (unit->use_double2)
              ost << "val = make_double2(exp(val.x),exp(val.y));\n";
            else
              ost << "val = exp(val);\n";
          }
          // Now distribute out our result
          if (unit->use_double2)
          {
            ost << "sumxod[" << (hidx-start_h_spec) << "].x = "
                << "__fma_rn(mole_frac[" << (vidx-start_v_spec) << "].x,val.x,sumxod[" << (hidx-start_h_spec) << "].x);\n";
            ost << "sumxod[" << (vidx-start_v_spec) << "].x = "
                << "__fma_rn(mole_frac[" << (hidx-start_h_spec) << "].x,val.x,sumxod[" << (vidx-start_v_spec) << "].x);\n";
            ost << "sumxod[" << (hidx-start_h_spec) << "].y = "
                << "__fma_rn(mole_frac[" << (vidx-start_v_spec) << "].y,val.y,sumxod[" << (hidx-start_h_spec) << "].y);\n";
            ost << "sumxod[" << (vidx-start_v_spec) << "].y = "
                << "__fma_rn(mole_frac[" << (hidx-start_h_spec) << "].y,val.y,sumxod[" << (vidx-start_v_spec) << "].y);\n";
          }
          else
          {
            ost << "sumxod[" << (hidx-start_h_spec) << "] = "
                << "__fma_rn(mole_frac[" << (vidx-start_v_spec) << "],val,sumxod[" << (hidx-start_h_spec) << "]);\n";
            ost << "sumxod[" << (vidx-start_v_spec) << "] = "
                << "__fma_rn(mole_frac[" << (hidx-start_h_spec) << "],val,sumxod[" << (vidx-start_v_spec) << "]);\n";
          }
        }
      }
    }
    // Now figur out what to do with the results
    if (do_output)
    {
      if (unit->use_double2)
        ost << REAL2 << " pfac = make_double2(" << patmos << "/" << PRESSURE << ".x, "
            << patmos << "/" << PRESSURE << ".y);\n";
      else
        ost << REAL << " pfac = " << patmos << "/" << PRESSURE << ";\n";
      // Do the output computation for the vertical species
      for (unsigned spec_idx = start_v_spec; spec_idx < stop_v_spec; spec_idx++)
      {
        PairDelim spec_pair(ost);
        if (unit->use_double2)
        {
          ost << REAL2 << " result = make_double2( "
              << "pfac.x * (sumxw.x - (" << nwt[spec_idx] << "*mole_frac[" << (spec_idx-start_v_spec) << "].x))"
              << " / (wtm.x * sumxod[" << (spec_idx-start_v_spec) << "].x), "
              << "pfac.y * (sumxw.y - (" << nwt[spec_idx] << "*mole_frac[" << (spec_idx-start_v_spec) << "].y))"
              << " / (wtm.y * sumxod[" << (spec_idx-start_v_spec) << "].y));\n";
          ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(diffusion+" << spec_idx << "*spec_stride), "
              << "\"d\"(result.x), \"d\"(result.y) : \"memory\");\n";
        }
        else
        {
          ost << REAL << " result = pfac * (sumxw - (" << nwt[spec_idx] << "*mole_frac["
              << (spec_idx-start_v_spec) << "])) / (wtm * sumxod[" << (spec_idx-start_v_spec) << "]);\n";
          // Write these out cache streaming since they will never be used again
          ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(diffusion+" << spec_idx << "*spec_stride), "
              << "\"d\"(result) : \"memory\");\n";
        }
        num_memory_ops++;
      }
    }
    else
    {
      // Just write out species results
      emit_blocked_species_store(ost, start_h_spec, stop_h_spec, "sumxod", "diffusion");
      num_memory_ops += (stop_h_spec-start_h_spec);
    }
  }
  else
  {
    // Different sets of species
    // Emit the arrays for storing our information
    ost << (unit->use_double2 ? REAL2 : REAL) << " h_mole_frac[" << (stop_h_spec-start_h_spec) << "];\n";
    ost << (unit->use_double2 ? REAL2 : REAL) << " v_mole_frac[" << (stop_v_spec-start_v_spec) << "];\n";
    ost << (unit->use_double2 ? REAL2 : REAL) << " h_sumxod[" << (stop_h_spec-start_h_spec) << "];\n";
    ost << (unit->use_double2 ? REAL2 : REAL) << " v_sumxod[" << (stop_v_spec-start_v_spec) << "];\n";

    // Emit the temperature load first
    ost << (unit->use_double2 ? REAL2 : REAL) << " " << TEMPERATURE << ";\n";
    emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, "0", unit->use_double2);
    num_memory_ops++;
    // If we're computing sums or doing output in this kernel emit the code to either load or initialize it
    if (compute_sums || do_output)
    {
      ost << (unit->use_double2 ? REAL2 : REAL) << " wtm;\n";
      ost << (unit->use_double2 ? REAL2 : REAL) << " sumxw;\n";
      if (kernel_idx > 0)
      {
        emit_cuda_load(ost, "wtm", "temp_wtm", "0", unit->use_double2);
        emit_cuda_load(ost, "sumxw", "temp_sumxw", "0", unit->use_double2);
        num_memory_ops+=2;
      }
      else
      {
        if (unit->use_double2)
        {
          ost << "wtm = make_double2(0.0,0.0);\n";
          ost << "sumxw = make_double2(0.0,0.0);\n";
        }
        else
        {
          ost << "wtm = 0.0;\n";
          ost << "sumxw = 0.0;\n";
        }
      }
    }

    emit_blocked_species_load(ost,start_h_spec,stop_h_spec,MOLE_FRAC_ARRAY,"h_mole_frac");
    num_memory_ops += (stop_h_spec-start_h_spec);
    emit_blocked_species_load(ost,start_v_spec,stop_v_spec,MOLE_FRAC_ARRAY,"v_mole_frac");
    num_memory_ops += (stop_v_spec-start_v_spec);
    if (!initialize_h)
    {
      emit_blocked_species_load(ost,start_h_spec,stop_h_spec,"diffusion","h_sumxod");
      num_memory_ops += (stop_h_spec-start_h_spec);
    }
    else
    {
      // This is the first one ever for any species so we can just set it all to zero here
      for (unsigned spec_idx = 0; spec_idx < (stop_h_spec-start_h_spec); spec_idx++)
      {
        if (unit->use_double2)
          ost << "h_sumxod[" << spec_idx << "] = make_double2(0.0,0.0);\n";
        else
          ost << "h_sumxod[" << spec_idx << "] = 0.0;\n";
      }
    }
    if (!initialize_v)
    {
      emit_blocked_species_load(ost,start_v_spec,stop_v_spec,"diffusion","v_sumxod");
      num_memory_ops += (stop_v_spec-start_v_spec);
    }
    else
    {
      for (unsigned spec_idx = 0; spec_idx < (stop_v_spec-start_v_spec); spec_idx++)
      {
        if (unit->use_double2)
          ost << "v_sumxod[" << spec_idx << "] = make_double2(0.0,0.0);\n";
        else
          ost << "v_sumxod[" << spec_idx << "] = 0.0;\n";
      }
    }
    
    if (do_output)
    {
      ost << (unit->use_double2 ? REAL2 : REAL) << " " << PRESSURE << ";\n";
      emit_cuda_load(ost, PRESSURE, PRESSURE_ARRAY, "0", unit->use_double2);
      num_memory_ops++;
    }

    // Compute the log on our temperature
    if (unit->use_double2)
      ost << REAL2 << " logt = make_double2(log(" << TEMPERATURE << ".x), log(" << TEMPERATURE << ".y));\n";
    else
      ost << REAL << " logt = log(" << TEMPERATURE << ");\n";

    if (compute_sums)
    {
      // Update the sums for our horizontal species
      for (unsigned spec_idx = start_h_spec; spec_idx < stop_h_spec; spec_idx++)
      {
        unsigned local_idx = spec_idx-start_h_spec;
        if (unit->use_double2)
        {
          ost << "wtm = make_double2( "
              << "__fma_rn(h_mole_frac[" << local_idx << "].x, "
              << nwt[spec_idx] << ",wtm.x), "
              << "__fma_rn(h_mole_frac[" << local_idx << "].y, "
              << nwt[spec_idx] << ",wtm.y));\n";
          // Now clamp and then redo into sumxw
          ost << "h_mole_frac[" << local_idx << "] = make_double2( "
              << "(h_mole_frac[" << local_idx << "].x > " << SMALL << " ? "
              << "h_mole_frac[" << local_idx << "].x : " << SMALL << "), "
              << "(h_mole_frac[" << local_idx << "].y > " << SMALL << " ? "
              << "h_mole_frac[" << local_idx << "].y : " << SMALL << "));\n";
          ost << "sumxw = make_double2( "
              << "__fma_rn(h_mole_frac[" << local_idx << "].x, "
              << nwt[spec_idx] << ",sumxw.x), "
              << "__fma_rn(h_mole_frac[" << local_idx << "].y, "
              << nwt[spec_idx] << ",sumxw.y));\n";
        }
        else
        {
          ost << "wtm = "
              << "__fma_rn(h_mole_frac[" << local_idx << "]," 
              << nwt[spec_idx] << ",wtm);\n";
          ost << "h_mole_frac[" << local_idx << "] = "
              << "(h_mole_frac[" << local_idx << "] > " << SMALL << " ? "
              << "h_mole_frac[" << local_idx << "] : " << SMALL << ");\n";
          ost << "sumxw = "
              << "__fma_rn(h_mole_frac[" << local_idx << "]," 
              << nwt[spec_idx] << ",sumxw);\n";
        }
      }
      // Write these values out no matter what
      // Mark all these cache global since they are likely to be reused
      if (unit->use_double2)
      {
        ost << "asm volatile(\"st.global.cg.v2.f64 [%0], {%1,%2};\" : : \"l\"(temp_wtm), "
            << "\"d\"(wtm.x), \"d\"(wtm.y) : \"memory\");\n";
        ost << "asm volatile(\"st.global.cg.v2.f64 [%0], {%1,%2};\" : : \"l\"(temp_sumxw), "
            << "\"d\"(sumxw.x), \"d\"(sumxw.y) : \"memory\");\n";
      }
      else
      {
        ost << "asm volatile(\"st.global.cg.f64 [%0], %1;\" : : \"l\"(temp_wtm), "
            << "\"d\"(wtm) : \"memory\");\n";
        ost << "asm volatile(\"st.global.cg.f64 [%0], %1;\" : : \"l\"(temp_sumxw), "
            << "\"d\"(sumxw) : \"memory\");\n";
      }
      num_memory_ops+=2;
    }
    else
    {
      // Just clamp the horizontal species
      for (unsigned spec_idx = start_h_spec; spec_idx < stop_h_spec; spec_idx++)
      {
        unsigned local_idx = spec_idx-start_h_spec;
        if (unit->use_double2)
          ost << "h_mole_frac[" << local_idx << "] = make_double2( "
              << "(h_mole_frac[" << local_idx << "].x > " << SMALL << " ? "
              << "h_mole_frac[" << local_idx << "].x : " << SMALL << "), "
              << "(h_mole_frac[" << local_idx << "].y > " << SMALL << " ? "
              << "h_mole_frac[" << local_idx << "].y : " << SMALL << "));\n";
        else
          ost << "h_mole_frac[" << local_idx << "] = "
              << "(h_mole_frac[" << local_idx << "] > " << SMALL << " ? "
              << "h_mole_frac[" << local_idx << "] : " << SMALL << ");\n";
      }
    }
    // Now clamp the vertical species
    for (unsigned spec_idx = start_v_spec; spec_idx < stop_v_spec; spec_idx++)
    {
      unsigned local_idx = spec_idx - start_v_spec;
      if (unit->use_double2)
        ost << "v_mole_frac[" << local_idx << "] = make_double2( "
            << "(v_mole_frac[" << local_idx << "].x > " << SMALL << " ? "
            << "v_mole_frac[" << local_idx << "].x : " << SMALL << "), "
            << "(v_mole_frac[" << local_idx << "].y > " << SMALL << " ? "
            << "v_mole_frac[" << local_idx << "].y : " << SMALL << "));\n";
      else
        ost << "v_mole_frac[" << local_idx << "] = "
            << "(v_mole_frac[" << local_idx << "] > " << SMALL << " ? "
            << "v_mole_frac[" << local_idx << "] : " << SMALL << ");\n";
    }

    // Now iterate over all the species that we own and do the computation
    const unsigned num_species = (unit->species.size()-1);
    for (unsigned hidx = start_h_spec; hidx < stop_h_spec; hidx++)
    {
      for (unsigned vidx = start_v_spec; vidx < stop_v_spec; vidx++)
      {
        ost << "// D_" << hidx << "_" << vidx << " and D_" << vidx << "_" << hidx << "\n";
        PairDelim dpair(ost);
        if (unit->use_double2)
        {
          ost << REAL << " coeff0 = " << ndif[(order-1)+hidx*order+vidx*order*num_species] << ";\n";
          ost << REAL2 << " val = make_double2(coeff0,coeff0);\n";
          for (int k = order-1; k > 0; k--)
          {
            ost << REAL << " coeff" << k << " = " << ndif[(k-1)+hidx*order+vidx*order*num_species] << ";\n";
            ost << "val = make_double2( "
                << "__fma_rn(val.x,logt.x,coeff" << k << "), "
                << "__fma_rn(val.y,logt.y,coeff" << k << "));\n";
          }
          ost << "val = make_double2(-val.x,-val.y);\n";
        }
        else
        {
          ost << REAL << " val = " << ndif[(order-1)+hidx*order+vidx*order*num_species] << ";\n";
          for (int k = order-1; k > 0; k--)
          {
            ost << "val = __fma_rn(val,logt," << ndif[(k-1)+hidx*order+vidx*order*num_species] << ");\n";
          }
          ost << "val = -val;\n";
        }
        // do the exponent
        if (unit->fast_math)
          emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, unit->use_double2);
        else
        {
          if (unit->use_double2)
            ost << "val = make_double2(exp(val.x),exp(val.y));\n";
          else
            ost << "val = exp(val);\n";
        }
        // distribute out the results
        if (unit->use_double2)
        {
          ost << "h_sumxod[" << (hidx-start_h_spec) << "].x = "
              << "__fma_rn(v_mole_frac[" << (vidx-start_v_spec) << "].x,val.x,h_sumxod[" << (hidx-start_h_spec) << "].x);\n";
          ost << "v_sumxod[" << (vidx-start_v_spec) << "].x = "
              << "__fma_rn(h_mole_frac[" << (hidx-start_h_spec) << "].x,val.x,v_sumxod[" << (vidx-start_v_spec) << "].x);\n";
          ost << "h_sumxod[" << (hidx-start_h_spec) << "].y = "
              << "__fma_rn(v_mole_frac[" << (vidx-start_v_spec) << "].y,val.y,h_sumxod[" << (hidx-start_h_spec) << "].y);\n";
          ost << "v_sumxod[" << (vidx-start_v_spec) << "].y = "
              << "__fma_rn(h_mole_frac[" << (hidx-start_h_spec) << "].y,val.y,v_sumxod[" << (vidx-start_v_spec) << "].y);\n";
        }
        else
        {
          ost << "h_sumxod[" << (hidx-start_h_spec) << "] = "
              << "__fma_rn(v_mole_frac[" << (vidx-start_v_spec) << "],val,h_sumxod[" << (hidx-start_h_spec) << "]);\n";
          ost << "v_sumxod[" << (vidx-start_v_spec) << "] = "
              << "__fma_rn(h_mole_frac[" << (hidx-start_h_spec) << "],val,v_sumxod[" << (vidx-start_v_spec) << "]);\n";
        }
      }
    }
    // Now we get to figure out what to do with the results
    if (do_output)
    {
      if (unit->use_double2)
        ost << REAL2 << " pfac = make_double2(" << patmos << "/" << PRESSURE << ".x, "
            << patmos << "/" << PRESSURE << ".y);\n";
      else
        ost << REAL << " pfac = " << patmos << "/" << PRESSURE << ";\n";
      // Do the output computation for the vertical species
      for (unsigned spec_idx = start_v_spec; spec_idx < stop_v_spec; spec_idx++)
      {
        PairDelim spec_pair(ost);
        if (unit->use_double2)
        {
          ost << REAL2 << " result = make_double2( "
              << "pfac.x * (sumxw.x - (" << nwt[spec_idx] << "*v_mole_frac[" << (spec_idx-start_v_spec) << "].x))"
              << " / (wtm.x * v_sumxod[" << (spec_idx-start_v_spec) << "].x), "
              << "pfac.y * (sumxw.y - (" << nwt[spec_idx] << "*v_mole_frac[" << (spec_idx-start_v_spec) << "].y))"
              << " / (wtm.y * v_sumxod[" << (spec_idx-start_v_spec) << "].y));\n";
          ost << "asm volatile(\"st.global.cs.v2.f64 [%0], {%1,%2};\" : : \"l\"(diffusion+" << spec_idx << "*spec_stride), "
              << "\"d\"(result.x), \"d\"(result.y) : \"memory\");\n";
        }
        else
        {
          ost << REAL << " result = pfac * (sumxw - (" << nwt[spec_idx] << "*v_mole_frac["
              << (spec_idx-start_v_spec) << "])) / (wtm * v_sumxod[" << (spec_idx-start_v_spec) << "]);\n";
          // Write these out cache streaming since they will never be used again
          ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(diffusion+" << spec_idx << "*spec_stride), "
              << "\"d\"(result) : \"memory\");\n";
        }
        num_memory_ops++;
      }
    }
    else
    {
      // Otherwise, just write out the vertical species 
      emit_blocked_species_store(ost, start_v_spec, stop_v_spec, "v_sumxod", "diffusion");
      num_memory_ops += (stop_v_spec-start_v_spec);
    }
    // Write out the results for the horizontal species
    emit_blocked_species_store(ost, start_h_spec, stop_h_spec, "h_sumxod", "diffusion");
    num_memory_ops += (stop_h_spec-start_h_spec);
  }
  return num_memory_ops;
}

void TranLib::emit_blocked_species_load(CodeOutStream &ost, unsigned start_spec, unsigned stop_spec,
                                                            const char *src_name, const char *dst_name)
{
  for (unsigned idx = start_spec; idx < stop_spec; idx++)
  {
    unsigned local = idx - start_spec;
#ifdef ENABLE_LDG
    ost << "#if __CUDA_ARCH__ >= 350\n";
    if (unit->use_double2)
    {
      ost << "asm volatile(\"ld.global.nc.cg.v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst_name << "[" << local << "].x), "
          << "\"=d\"(" << dst_name << "[" << local << "].y) : \"l\"(" << src_name << "+" << idx << "*spec_stride) : \"memory\");\n";
    }
    else
    {
      ost << "asm volatile(\"ld.global.nc.cg.f64 %0, [%1];\" : \"=d\"(" << dst_name << "[" << local << "]) : "
          << "\"l\"(" << src_name << "+" << idx << "*spec_stride) : \"memory\");\n";
    }
    ost << "#else\n";
#endif
    if (unit->use_double2)
    {
      ost << "asm volatile(\"ld.global.cg.v2.f64 {%0,%1}, [%2];\" : \"=d\"(" << dst_name << "[" << local << "].x), "
          << "\"=d\"(" << dst_name << "[" << local << "].y) : \"l\"(" << src_name << "+" << idx << "*spec_stride) : \"memory\");\n";
    }
    else
    {
      ost << "asm volatile(\"ld.global.cg.f64 %0, [%1];\" : \"=d\"(" << dst_name << "[" << local << "]) : "
          << "\"l\"(" << src_name << "+" << idx << "*spec_stride) : \"memory\");\n";
    }
#ifdef ENABLE_LDG
    ost << "#endif\n";
#endif
  }
}

void TranLib::emit_blocked_species_store(CodeOutStream &ost, unsigned start_spec, unsigned stop_spec,
                                                             const char *src_name, const char *dst_name)
{
  for (unsigned idx = start_spec; idx < stop_spec; idx++)
  {
    unsigned local = idx - start_spec;
    if (unit->use_double2)
    {
      ost << "asm volatile(\"st.global.cg.v2.f64 [%0], {%1,%2};\" : : \"l\"(" << dst_name << "+" << idx << "*spec_stride), "
          << "\"d\"(" << src_name << "[" << local << "].x), \"d\"(" << src_name << "[" << local << "].y) : \"memory\");\n";
    }
    else
    {
      ost << "asm volatile(\"st.global.cg.f64 [%0], %1;\" : : \"l\"(" << dst_name << "+" << idx << "*spec_stride), "
          << "\"d\"(" << src_name << "[" << local << "]) : \"memory\");\n";
    }
  }
}

void TranLib::emit_blocked_diffusion_declaration(CodeOutStream &ost, unsigned kernel_idx)
{
  ost << "__global__ void\n";
  ost << "#ifdef DIFFUSION_" << kernel_idx << "_BOUNDS\n";
  ost << "DIFFUSION_" << kernel_idx << "_BOUNDS\n";
  ost << "#endif\n";
  ost << "gpu_diffusion_" << kernel_idx << "(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << PRESSURE_ARRAY;
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "temp_wtm";
  ost << ", " << REAL << " *" << "temp_sumxw";
  ost << ", " << REAL << " *" << "diffusion";
  ost << ")";
}

void TranLib::emit_large_sliced_diffusion(CodeOutStream &ost, unsigned max_specs, bool uniform)
{
  // Be careful making this less than 16 as it could lead to divergent loads
  const unsigned warp_size = 16;
  const char *warp_mask = "0xf";
  const unsigned warp_shift = 4;
  assert((32%warp_size) == 0);
  const unsigned threads_per_full_warp = 32/warp_size;
  assert(threads_per_full_warp > 0);
  const char *local_mask = "0x1";
  const unsigned num_species = (unit->species.size()-1);
#ifdef LARGE_DIFF_KEPLER
  unsigned warp_constant_stride = 0;
#endif
  unsigned max_points_per_thread = 0;
  // Emit the constants for this kernel
  {
    // Diffusion masses should always fit in the constant cache
    ost << "__constant__ " << REAL << " diffusion_masses[" << num_species << "] = {";
    for (unsigned idx = 0; idx < num_species; idx++)
    {
      if (idx == 0)
        ost << nwt[idx];
      else
        ost << ", " << nwt[idx];
    }
    ost << "};\n\n";
  }
  {
    bool has_remainder = ((num_species%unit->threads_per_point) != 0);
    unsigned warp_columns = (num_species/(unit->threads_per_point/threads_per_full_warp));
    //unsigned thread_columns = (num_species/unit->threads_per_point);
    unsigned num_full_points = (num_species/2);
    // Compute the maximum number of points per thread
    {
      max_points_per_thread = 0;
      for (unsigned point_idx = 0; point_idx < ((num_species/2)+warp_columns-1); point_idx++)
      {
        const unsigned start_across = (point_idx < num_full_points ? 0 : point_idx-num_full_points+1);
        const unsigned stop_across = (point_idx < warp_columns ? point_idx+1 : warp_columns);
        unsigned loop_start_across = start_across;
        unsigned loop_stop_across = stop_across;
        while ((loop_start_across%threads_per_full_warp) != 0)
        {
          assert(loop_start_across > 0);
          loop_start_across--;
        }
        while ((loop_stop_across%threads_per_full_warp) != 0)
        {
          loop_stop_across++;
        }
        for (unsigned across_idx = loop_start_across; across_idx < loop_stop_across; across_idx+=threads_per_full_warp)
        {
          max_points_per_thread++;
        }
      }
      if (has_remainder)
      {
        for (unsigned point_idx = 0; point_idx < (num_species/2); point_idx++)
          max_points_per_thread++;
      }
    }
#ifdef LARGE_DIFF_KEPLER
    warp_constant_stride = max_points_per_thread*order;
    while ((warp_constant_stride%warp_size) != 0)
      warp_constant_stride++;
    ost << "__device__ const " << REAL << " across_coeffs[" << (unit->threads_per_point*warp_constant_stride) << "] = {";
#else
    ost << "__device__ const " << REAL << " across_coeffs[" << unit->threads_per_point << "][" 
        << max_points_per_thread << "][" << order << "] = {";
#endif
    bool first = true;
    for (unsigned wid = 0; wid < unit->threads_per_point; wid++)
    {
#ifndef LARGE_DIFF_KEPLER
      if (wid == 0)
        ost << "{";
      else
        ost << ", {";
      first = true;
#endif
      unsigned thread_offset = wid%threads_per_full_warp;
      unsigned num_created = 0;
      for (unsigned point_idx = 0; point_idx < ((num_species/2)+warp_columns-1); point_idx++)
      {
        const unsigned start_across = (point_idx < num_full_points ? 0 : point_idx-num_full_points+1);
        const unsigned stop_across = (point_idx < warp_columns ? point_idx+1 : warp_columns);
        unsigned loop_start_across = start_across;
        unsigned loop_stop_across = stop_across;
        while ((loop_start_across%threads_per_full_warp) != 0)
        {
          assert(loop_start_across > 0);
          loop_start_across--;
        }
        while ((loop_stop_across%threads_per_full_warp) != 0)
        {
          loop_stop_across++;
        }
        for (unsigned across_idx = loop_start_across; across_idx < loop_stop_across; across_idx+=threads_per_full_warp)
        {
          assert((across_idx%threads_per_full_warp)==0);
          //const unsigned thread_across_idx = across_idx%threads_per_full_warp;
          unsigned x_loc = ((wid/threads_per_full_warp)*warp_columns+thread_offset+across_idx)%num_species;
          unsigned y_loc = ((wid/threads_per_full_warp)*warp_columns+1+point_idx)%num_species;
          assert((x_loc < num_species) && (y_loc < num_species));
          //ost << "/*(" << wid << "," << x_loc << "," << y_loc << ")*/";
#ifndef LARGE_DIFF_KEPLER
          if (first)
          {
            ost << " {";
            first = false;
          }
          else
            ost << ", {";
#endif
          for (int k = order; k > 0; k--)
          {
#ifdef LARGE_DIFF_KEPLER
            if (first)
              first = false;
            else
              ost << ", ";
#else
            if (k < order)
              ost << ", ";
#endif
            ost << ndif[(k-1)+y_loc*order+x_loc*order*num_species];
          }
#ifndef LARGE_DIFF_KEPLER
          ost << "}";
#endif
          num_created++;
        }
      }
      assert(!first);
      if (has_remainder)
      {
        unsigned x_loc = (wid+(warp_columns*(unit->threads_per_point/threads_per_full_warp)))%num_species;
        for (unsigned point_idx = 0; point_idx < (num_species/2); point_idx++)
        {
          unsigned y_loc = (x_loc+1+point_idx)%num_species;
#ifndef LARGE_DIFF_KEPLER
          ost << ", {";
#endif
          for (int k = order; k > 0; k--)
          {
#ifdef LARGE_DIFF_KEPLER
            ost << ", ";
#else
            if (k < order)
              ost << ", ";
#endif
            ost << ndif[(k-1)+y_loc*order+x_loc*order*num_species];
          }
#ifndef LARGE_DIFF_KEPLER
          ost << "}";
#endif
          num_created++;
        }
      }
      assert(num_created == max_points_per_thread);
#ifndef LARGE_DIFF_KEPLER
      ost << "}";
#else
      // Otherwise, pad with zeros
      assert(num_created <= warp_constant_stride);
      for (int i = 0; i < (int(warp_constant_stride)-int(num_created*order)); i++)
        ost << ", 0.0";
#endif
    }
    ost << "};\n\n";
  }
  emit_large_sliced_diffusion_declaration(ost);
  ost << "\n";
  PairDelim pair(ost);
  ost << "const " << INT << " tid = threadIdx.x & " << warp_mask << ";\n";
  ost << "const " << INT << " wid = threadIdx.x >> " << warp_shift << ";\n";
  {
    PairDelim offset_pair(ost);
    ost << "const " << INT << " offset = (blockIdx.y)*row_stride + "
        << "(blockIdx.x*" << warp_size << " + tid)"
#ifdef LARGE_DIFF_KEPLER
        << ";\n";
#else
        << "+blockIdx.z*slice_stride;\n";
#endif
    ost << TEMPERATURE_ARRAY << " += offset;\n";
    ost << PRESSURE_ARRAY << " += offset;\n";
    ost << MOLE_FRAC_ARRAY << " += offset;\n";
    ost << "diffusion += offset;\n";
  }
  ost << "volatile __shared__ " << REAL << " sumxod[" << num_species << "][" << warp_size << "];\n";
  ost << "volatile __shared__ " << REAL << " " << MOLE_FRAC << "[" << num_species << "][" << warp_size << "];\n";

  ost << REAL << " " << TEMPERATURE << ";\n";
  ost << REAL << " " << MOLE_FRAC << "_temp[" << max_specs << "];\n";

#ifdef LARGE_DIFF_KEPLER
  // Emit loads for our constants  
  assert(warp_constant_stride > 0);
  assert(max_points_per_thread > 0);
  const unsigned constants_per_thread = (max_points_per_thread*order+(warp_size-1))/warp_size; 
  assert(warp_constant_stride == (warp_size*constants_per_thread));
  ost << REAL << " thread_coeffs[" << (constants_per_thread) << "];\n";
  {
    PairDelim constant_pair(ost);
    ost << "const " << INT << " offset = wid*" << (warp_constant_stride) << "+tid;\n";
    // Issue loads for our constants
    for (unsigned idx = 0; idx < constants_per_thread; idx++)
    {
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"offset+%d",(idx*warp_size));
      emit_cuda_load(ost,"thread_coeffs",dst_offset,"across_coeffs",src_offset,false/*double2*/);
    }
  }
  // Emit the major loop
  ost << "for (int step = 0; step < total_steps; step++)\n";
  PairDelim step_loop_pair(ost);
#endif
  // Issue the load for the temperature
  emit_cuda_load(ost, TEMPERATURE, TEMPERATURE_ARRAY, false/*double2*/);
  // Emit the loads for the mole fractions
  {
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      char dst[128];
      sprintf(dst,"%s_temp",MOLE_FRAC);
      char dst_offset[128];
      sprintf(dst_offset,"%d",idx);
      char src_offset[128];
      sprintf(src_offset,"(wid+%d)*spec_stride",(idx*unit->threads_per_point));
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      emit_cuda_load(ost,dst,dst_offset,MOLE_FRAC_ARRAY,src_offset,false,".cg");
    }
  }
  // Compute the log of the temperature
  ost << REAL << " logt = log(" << TEMPERATURE << ");\n";
  // Initialize all sumxod values to 0.0
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
    ost << "sumxod[wid+" << (idx*unit->threads_per_point) << "][tid] = 0.0;\n";
  }
  // Write our mole fractions into shared memory
  for (unsigned idx = 0; idx < max_specs; idx++)
  {
    if (!uniform && (idx == (max_specs-1)))
      ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
    ost << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid] = " << MOLE_FRAC << "_temp[" << idx << "];\n";
  }
  // Compute our local values of sumxw and wtm
  ost << REAL << " local_sumxw = 0.0;\n";
  ost << REAL << " local_wtm = 0.0;\n";
  // Barrier to say everything is written into shared memory
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  {
    // Figure out what we're doing for this across instance
    const bool needs_horizontal_check = ((num_species%2) == 0);
    assert((unit->threads_per_point%threads_per_full_warp) == 0);
    const unsigned warp_columns = num_species/(unit->threads_per_point/threads_per_full_warp);
    const unsigned thread_columns = (warp_columns+(threads_per_full_warp-1))/threads_per_full_warp;
    const bool has_remainder = ((num_species%(unit->threads_per_point/threads_per_full_warp))!=0);
    const bool local_uniform = ((thread_columns%threads_per_full_warp)==0);
    const unsigned num_full_points = (num_species/2);
    const unsigned num_partial_points = num_full_points-1;
    // Emit code for storing our local sumxod values 
    ost << REAL << " local_fracs[" << thread_columns << "];\n";
//#ifdef LARGE_DIFF_KEPLER
//    ost << "const " << INT << " local_wid = ((wid >> " << (threads_per_full_warp-1) << ")"
//        << "+step*" << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2) << ";\n";
//    ost << "const " << INT << " local_offset = (wid+step*" << next_largest_power(2*num_species,2) << ") & " << local_mask << ";\n";
//#else
    ost << "const " << INT << " local_wid = wid >> " << (threads_per_full_warp-1) << ";\n";
    ost << "const " << INT << " local_offset = wid & " << local_mask << ";\n";
//#endif
    // Note we group threads based on which full warp they fall into
    for (unsigned idx = 0; idx < thread_columns; idx++)
    {
      if (!local_uniform && (idx == (thread_columns-1)))
        ost << "if ((local_offset+" << (idx*threads_per_full_warp) << ") < " << warp_columns << ")\n";
      ost << "local_fracs[" << idx << "] = " << MOLE_FRAC << "[local_wid*" << warp_columns 
          << "+" << (idx*threads_per_full_warp) << "+local_offset][tid];\n";
    }
    for (unsigned idx = 0; idx < thread_columns; idx++)
    {
      if (!local_uniform && (idx == (thread_columns-1)))
        ost << "if ((local_offset+" << (idx*threads_per_full_warp) << ") < " << warp_columns << ")\n";
      PairDelim spec_pair(ost);
#ifdef LARGE_DIFF_KEPLER
      ost << "const " << REAL << " local_mass = diffusion_masses[(local_wid*" << warp_columns << "+"
          << (idx*threads_per_full_warp) << "+local_offset+step*" << (2*num_species) << ")%" << (2*num_species) << "];\n";
#else
      ost << "const " << REAL << " local_mass = diffusion_masses[local_wid*" << warp_columns << "+"
          << (idx*threads_per_full_warp) << "+local_offset];\n";
#endif
      ost << "local_wtm = __fma_rn(local_fracs[" << idx << "],local_mass,local_wtm);\n";
      ost << "local_fracs[" << idx << "] = (local_fracs[" << idx << "] > " << SMALL << " ? "
          << "local_fracs[" << idx << "] : " << SMALL << ");\n";
      ost << "local_sumxw = __fma_rn(local_fracs[" << idx << "],local_mass,local_sumxw);\n";
    }
    // Now we can emit code for storing our sumxod values
    ost << REAL << " local_sumxod[" << thread_columns << "];\n";
    for (unsigned idx = 0; idx < thread_columns; idx++)
      ost << "local_sumxod[" << idx << "] = 0.0;\n";
    // Now iterate over the number of full points that we have and for each of them do our
    // cut across points
    unsigned coeff_offset = 0;
    for (unsigned point_idx = 0; point_idx < (num_full_points+warp_columns-1); point_idx++)
    {
      bool needs_start_check = false;
      bool needs_stop_check = false;
      // This is the range for all the threads in the full warp
      const unsigned start_across = (point_idx < num_full_points ? 0 : point_idx-num_full_points+1);
      const unsigned stop_across = (point_idx < warp_columns ? point_idx+1 : warp_columns);
      unsigned loop_start_across = start_across;
      unsigned loop_stop_across = stop_across;
      while ((loop_start_across%threads_per_full_warp) != 0)
      {
        assert(loop_start_across > 0);
        loop_start_across--;
        needs_start_check = true;
      }
      while ((loop_stop_across%threads_per_full_warp) != 0)
      {
        loop_stop_across++;
        needs_stop_check = true;
      }
      // Also see if we need a bottom check
      bool needs_bottom_check = needs_horizontal_check && (point_idx >= num_partial_points);
      ost << "// Starting point " << point_idx << "\n";
      PairDelim across_pair(ost);
      // Get the index for the species we are handling
#ifdef LARGE_DIFF_KEPLER
      ost << "unsigned index = (local_wid*" << warp_columns << "+" << (point_idx+1) 
          << "+step*" << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2) << ";\n";
#else
      ost << "unsigned index = local_wid*" << warp_columns << "+" << (point_idx+1) << ";\n";
#endif
      // Handle the case of wrapped index
      ost << "if (index >= " << num_species << ") index -= " << num_species << ";\n";
      ost << REAL << " index_frac = " << MOLE_FRAC << "[index][tid];\n";
      ost << REAL << " index_sumxod = 0.0;\n";
      //ost << REAL << " index_sumxod = sumxod[index][tid];\n";
      ost << REAL << " index_clamped = (index_frac > " << SMALL << " ? index_frac : " << SMALL << ");\n";
      // Now go across for this point
      for (unsigned across_idx = loop_start_across; across_idx < loop_stop_across; across_idx+=threads_per_full_warp)
      {
        assert((across_idx%threads_per_full_warp)==0);
        const unsigned thread_across_idx = across_idx/threads_per_full_warp;
        ost << "// Across point (offset+" << (thread_across_idx) << ")\n";
        // Three possible checks here, one to see if we should even be doing the row (bottom)
        // one for the left side and one for the right side
        if (needs_bottom_check || (needs_start_check && (across_idx==loop_start_across)) 
                               || (needs_stop_check && (across_idx==(loop_stop_across-threads_per_full_warp))))
        {
          ost << "if (";
          if (needs_bottom_check)
          {
            ost << "((local_wid*" << warp_columns << "+" << (across_idx)
                << "+local_offset) < " << num_partial_points << ")";
            // If we need the second check put an && operator
            if ((needs_start_check && (across_idx==loop_start_across)) 
                  || (needs_stop_check && (across_idx==(loop_stop_across-threads_per_full_warp))))
              ost << " && ";
          }
          if (needs_start_check && (across_idx==loop_start_across))
          {
            ost << "(" << (start_across%threads_per_full_warp) << " <= local_offset)"; 
            if (needs_stop_check && (across_idx==(loop_stop_across-threads_per_full_warp)))
              ost << " && ";
          }
          if (needs_stop_check && (across_idx==(loop_stop_across-threads_per_full_warp)))
          {
            ost << "((local_offset+" << (across_idx) << ") < " << stop_across << ")";
          }
          ost << ")\n";
        }
        PairDelim index_pair(ost);
        assert(point_idx >= across_idx);
#ifdef LARGE_DIFF_KEPLER
        unsigned constant_index = coeff_offset*order+0;
        unsigned lane_index = constant_index%warp_size;
        unsigned thread_index = constant_index/warp_size;
        ost << INT << " lo_part, hi_part;\n";
        ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
        ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
        ost << REAL << " val = __hiloint2double(hi_part,lo_part);\n";
        for (int i = 1; i < order; i++)
        {
          constant_index = coeff_offset*order+i;
          lane_index = constant_index%warp_size;
          thread_index = constant_index/warp_size;
          ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << "val = __fma_rn(val,logt,__hiloint2double(hi_part,lo_part));\n";
        }
#else
        ost << REAL << " val = across_coeffs[wid][" << coeff_offset << "][0];\n";
        for (int i = 1; i < order; i++)
        {
          ost << "val = __fma_rn(val,logt,across_coeffs[wid][" << coeff_offset << "][" << i << "]);\n";
        }
#endif
        // Update the coeff offset
        coeff_offset++;
        ost << "val = -val;\n";
        if (unit->fast_math)
          emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, false/*double2*/);
        else
          ost << "val = exp(val);\n";
        // Now update both the index species and our local value
        ost << "index_sumxod = __fma_rn(local_fracs[" << thread_across_idx << "],val,index_sumxod);\n";
        ost << "local_sumxod[" << thread_across_idx << "] = __fma_rn(index_clamped,val,local_sumxod[" << thread_across_idx << "]);\n";
      }
      // We've finished across this point, we need to reduce our temporaries out to shared memory
#ifdef LARGE_DIFF_KEPLER
      // If we're on Kepler, do the warp reduction with shuffles and then have
      // the first thread write out the result
      // For right now we'll only suppor the case of warp_size 16
      // To do more smaller warp sizes we'll need to emit as many shuffles as there are lanes
      assert(warp_size==16);
      {
        PairDelim reduc_pair(ost);
        ost << INT << " lo_part, hi_part;\n";
        ost << "lo_part = __shfl_down(__double2loint(index_sumxod), " << warp_size << ");\n";
        ost << "hi_part = __shfl_down(__double2hiint(index_sumxod), " << warp_size << ");\n";
        ost << "index_sumxod += __hiloint2double(hi_part,lo_part);\n";
        // Only the first thread in the warp needs to write the result back
        ost << "if (local_offset==0)\n";
        ost << "  sumxod[index][tid] += index_sumxod;\n";
      }
#else
      // If we're on Fermi, we'll do this with warp synchronous programming
      for (unsigned mod = 0; mod < threads_per_full_warp; mod++)
      {
        {
          ost << "if ((wid%" << threads_per_full_warp<< ")==" << mod << ")\n";
          ost << "  sumxod[index][tid] += index_sumxod;\n";
          // Don't need this barrier since we're warp-synchronous
          //ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
        }
      }
#endif
      // We only need barriers after the every num_column number of points (Yay group theory!)
      // or it is the last iteration through
      if ((((point_idx+1)%warp_columns) == 0) || (point_idx == (num_full_points+warp_columns-2)))
        ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";

    }
    // Now that we've finished all our points, we need to reduce them out to shared
    for (unsigned idx = 0; idx < thread_columns; idx++)
    {
      if (!local_uniform && (idx == (thread_columns-1)))
       ost << "if ((local_offset+" << (idx*threads_per_full_warp) << ") < " << warp_columns << ")\n";       
      ost << "sumxod[local_wid*" << warp_columns << "+" << (idx*threads_per_full_warp) 
          << "+local_offset][tid] += local_sumxod[" << idx << "];\n";
    }
    // Put a barrier after we're done so we know we're done
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
    if (has_remainder)
    {
      unsigned offset = (unit->threads_per_point/threads_per_full_warp)*warp_columns;
      assert(offset < num_species);
      unsigned predicated_threads = (num_species-offset)*warp_size;
      // Round up to a multiple of 32 to get a full warp
      while ((predicated_threads%32)!=0)
        predicated_threads++;
      ost << "// Handling remainders\n";
      ost << "if ((wid+" << offset << ") < " << num_species << ")\n";
      PairDelim remainder_pair(ost);
      // First emit the code to update our local values of wtm and sumxw
      ost << REAL << " local_frac = " << MOLE_FRAC << "[wid+" << (offset) << "][tid];\n";
      ost << REAL << " local_mass = diffusion_masses[wid+" << offset << "];\n";
      ost << "local_wtm = __fma_rn(local_frac,local_mass,local_wtm);\n";
      ost << REAL << " local_clamped = (local_frac > " << SMALL << " ? local_frac : " << SMALL << ");\n";
      ost << "local_sumxw = __fma_rn(local_frac,local_mass,local_sumxw);\n";
      ost << REAL << " local_dsum = 0.0;\n";
      // Now iterate over the points for this species
      unsigned num_remainder_points = (((num_species%2)==1) ? num_full_points : num_partial_points);
      // A check to make sure that we are either handling all partial points
      // and not a combination of partial points.  This will only fail in mechanisms
      // with very few species and large numbers of threads per points which should
      // be uncommon so we're going to ignore it for now
      if (num_remainder_points == num_partial_points)
        assert(unit->threads_per_point <= num_partial_points);
      for (unsigned point_idx = 0; point_idx < num_remainder_points; point_idx++)
      {
        PairDelim point_pair(ost);
#ifndef LARGE_DIFF_KEPLER
        ost << "unsigned index = wid + " << (offset+1+point_idx) << ";\n";
        //ost << "if (index >= " << total_species << ") index -= " << total_species << ";\n";
#else
        //ost << "unsigned index = (wid + " << (offset+1+point_idx) << "+ step*" << total_species << ")%" << total_species << ";\n";
        ost << "unsigned index = (wid + " << (offset+1+point_idx) << " + step*" << next_largest_power(2*num_species,2)
            << ")%" << next_largest_power(2*num_species,2) << ";\n";
#endif
        ost << "if (index >= " << num_species << ") index -= " << num_species<< ";\n";
        ost << REAL << " index_frac = " << MOLE_FRAC << "[index][tid];\n";
        ost << REAL << " index_sumxod = sumxod[index][tid];\n";
#ifdef LARGE_DIFF_KEPLER
        unsigned constant_index = coeff_offset*order+0;
        unsigned lane_index = constant_index%warp_size;
        unsigned thread_index = constant_index/warp_size;
        ost << INT << " hi_part, lo_part;\n";
        ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
        ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
        ost << REAL << " val = __hiloint2double(hi_part,lo_part);\n";
        for (int i = 1; i < order; i++)
        {
          constant_index = coeff_offset*order+i;
          lane_index = constant_index%warp_size;
          thread_index = constant_index/warp_size;
          ost << "lo_part = __shfl(__double2loint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << "hi_part = __shfl(__double2hiint(thread_coeffs[" << thread_index << "]), " << lane_index << ", " << warp_size << ");\n";
          ost << "val = __fma_rn(val,logt,__hiloint2double(hi_part,lo_part));\n";
        }
#else
        ost << REAL << " val = across_coeffs[wid][" << coeff_offset << "][0];\n";
        for (int i = 1; i < order; i++)
        {
          ost << "val = __fma_rn(val,logt,across_coeffs[wid][" << coeff_offset << "][" << i << "]);\n";
        }
#endif
        ost << "val = -val;\n";
        // Update the coeff offset
        coeff_offset++;

        if (unit->fast_math)
          emit_gpu_exp_taylor_series_expansion(ost, "val", unit->taylor_stages, unit->use_double2);
        else
        {
          ost << "val = exp(val);\n";
        }
        ost << REAL << " index_clamped = (index_frac > " << SMALL << " ? index_frac : " << SMALL << ");\n";
        ost << "index_sumxod = __fma_rn(local_clamped,val,index_sumxod);\n";
        ost << "sumxod[index][tid] = index_sumxod;\n";
        ost << "local_dsum = __fma_rn(index_clamped,val,local_dsum);\n";
        // Need a barrier here, unless it is warp-synchronous
        if (predicated_threads > 32)
          ost << "asm volatile(\"bar.sync 1, " << predicated_threads << ";\" : : : \"memory\");\n";
      }
      // When we're done with all our points, then we get to write our result back
      ost << REAL << " local_sumxod = sumxod[wid+" << offset << "][tid];\n";
      ost << "local_sumxod += local_dsum;\n";
      ost << "sumxod[wid+" << offset << "][tid] = local_sumxod;\n";
    }
    // If we had a remainder, then we need an extra barrier here
    if (has_remainder)
      ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  }
  // Now we're ready to do the output computation
  // Emit the load for our pressure
  ost << REAL << " " << PRESSURE << ";\n";
  emit_cuda_load(ost,PRESSURE,PRESSURE_ARRAY,false/*double2*/);
  // Reload the mole fractions
  {
    ost << REAL << " output_fracs[" << max_specs << "];\n";
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      ost << "output_fracs[" << idx << "] = " << MOLE_FRAC << "[wid+" << (idx*unit->threads_per_point) << "][tid];\n";
    }
    // Emit a syncthreads before the next phase
    ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  }
  // Compute the final sumxw and wtm values
#ifdef LARGE_DIFF_KEPLER
  if ((2*(unit->threads_per_point/threads_per_full_warp)) > num_species)
  {
    fprintf(stderr,"Warp-specialized diffusion kernel needs at least twice as many points as species. "
            "Decrease number of warps to meet this constraint.  Species %d, warps %d\n", 
            num_species, (unit->threads_per_point/threads_per_full_warp));
    exit(1);
  }
  // Assume this is of size 16 right now
  assert(warp_size==16);
  {
    PairDelim reduc_pair(ost);
    ost << INT << " lo_part, hi_part;\n";
    ost << "lo_part = __shfl_down(__double2loint(local_sumxw), " << warp_size << ");\n";
    ost << "hi_part = __shfl_down(__double2hiint(local_sumxw), " << warp_size << ");\n";
    ost << "local_sumxw += __hiloint2double(hi_part,lo_part);\n";
    ost << "lo_part = __shfl_down(__double2loint(local_wtm), " << warp_size << ");\n";
    ost << "hi_part = __shfl_down(__double2hiint(local_wtm), " << warp_size << ");\n";
    ost << "local_wtm += __hiloint2double(hi_part,lo_part);\n";
  }
  // Now write out the value for local warp 0 inside our larger warp
  {
    ost << "if (local_offset==0)\n";
    PairDelim write_pair(ost);
    ost << MOLE_FRAC << "[local_wid][tid] = local_sumxw;\n";
    ost << MOLE_FRAC << "[local_wid+" << (unit->threads_per_point/threads_per_full_warp) << "][tid] = local_wtm;\n";
  }
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  {
    ost << REAL << " sumxw = 0.0;\n";
    ost << REAL << " wtm = 0.0;\n";
    PairDelim reduc_pair(ost); 
    ost << REAL << " reduc_sumxw[" << (unit->threads_per_point/threads_per_full_warp) << "];\n";
    ost << REAL << " reduc_wtm[" << (unit->threads_per_point/threads_per_full_warp) << "];\n";
    for (unsigned i = 0; i < (unit->threads_per_point/threads_per_full_warp); i++)
    {
      ost << "reduc_sumxw[" << i << "] = " << MOLE_FRAC << "[" << i << "][tid];\n";
      ost << "reduc_wtm[" << i << "] = " << MOLE_FRAC << "[" << ((unit->threads_per_point/threads_per_full_warp)+i) << "][tid];\n";
    }
    for (unsigned i = 0; i < (unit->threads_per_point/threads_per_full_warp); i++)
    {
      ost << "sumxw += reduc_sumxw[" << i << "];\n";
      ost << "wtm += reduc_wtm[" << i << "];\n";
    }
  }
#else
  if ((2*unit->threads_per_point) > num_species)
  {
    fprintf(stderr,"Warp-specialized diffusion kernel needs at least twice as many points as species. "
            "Decrease number of warps to meet this constraint.  Species %d, warps %d\n", num_species, unit->threads_per_point);
    exit(1);
  }
  ost << MOLE_FRAC << "[wid][tid] = local_sumxw;\n";
  ost << MOLE_FRAC << "[wid+" << unit->threads_per_point << "][tid] = local_wtm;\n";
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  {
    ost << REAL << " sumxw = 0.0;\n";
    ost << REAL << " wtm = 0.0;\n";
    PairDelim reduc_pair(ost);
    ost << REAL << " reduc_sumxw[" << unit->threads_per_point << "];\n";
    ost << REAL << " reduc_wtm[" << unit->threads_per_point << "];\n";
    for (unsigned i = 0; i < unit->threads_per_point; i++)
    {
      ost << "reduc_sumxw[" << i << "] = " << MOLE_FRAC << "[" << i << "][tid];\n";
      ost << "reduc_wtm[" << i << "] = " << MOLE_FRAC << "[" << (unit->threads_per_point+i) << "][tid];\n";
    }
    for (unsigned i = 0; i < unit->threads_per_point; i++)
    {
      ost << "sumxw += reduc_sumxw[" << i << "];\n";
      ost << "wtm += reduc_wtm[" << i << "];\n";
    }
  }
#endif
  // Do the output computation
  {
    ost << REAL << " pfac = " << patmos << "/" << PRESSURE << ";\n";
    for (unsigned idx = 0; idx < max_specs; idx++)
    {
      if (!uniform && (idx == (max_specs-1)))
        ost << "if ((wid+" << (idx*unit->threads_per_point) << ") < " << num_species << ")\n";
      PairDelim output_pair(ost);
      ost << REAL << " clamped = (output_fracs[" << idx << "] > " << SMALL 
          << " ? output_fracs[" << idx << "] : " << SMALL << ");\n";
#ifdef LARGE_DIFF_KEPLER
      ost << REAL << " result = pfac * (sumxw - (diffusion_masses[(wid+" << (idx*unit->threads_per_point) 
          << "+step*" << next_largest_power(2*num_species,2) << ")%" << next_largest_power(2*num_species,2) << "]"
#else
      ost << REAL << " result = pfac * (sumxw - (diffusion_masses[wid+" << (idx*unit->threads_per_point) << "]"
#endif
          << "*clamped)) / (wtm * sumxod[wid+" << (idx*unit->threads_per_point) << "][tid]);\n";
      // Write out the result
      ost << "asm volatile(\"st.global.cs.f64 [%0], %1;\" : : \"l\"(diffusion+(wid+" 
          << (idx*unit->threads_per_point) << ")*spec_stride), " << "\"d\"(result) : \"memory\");\n";
    }
  }
#ifdef LARGE_DIFF_KEPLER
  // We need a barrier at the end of the loop
  ost << "asm volatile(\"bar.sync 0;\" : : : \"memory\");\n";
  ost << "// Update pointers for the next iteration\n";
  // Need to update all our pointers
  {
    PairDelim pointer_pair(ost);
    ost << TEMPERATURE_ARRAY << " += slice_stride;\n";
    ost << PRESSURE_ARRAY << " += slice_stride;\n";
    ost << MOLE_FRAC_ARRAY << " += slice_stride;\n";
    ost << "diffusion += slice_stride;\n";
  }
#endif
}

void TranLib::emit_large_sliced_diffusion_declaration(CodeOutStream &ost)
{
  ost << "__global__ void\n";
  ost << "gpu_diffusion(";
  ost << "const " << REAL << " *" << TEMPERATURE_ARRAY;
  ost << ", const " << REAL << " *" << PRESSURE_ARRAY;
  ost << ", const " << REAL << " *" << MOLE_FRAC_ARRAY;
  ost << ", const " << INT << " slice_stride/*NX*NY in number of doubles*/";
  ost << ", const " << INT << " row_stride/*NX in number of doubles*/";
  ost << ", const " << INT << " total_steps/*NZ in number of doubles*/";
  ost << ", const " << INT << " spec_stride/*NX*NY*NZ in number of doubles*/";
  ost << ", " << REAL << " *" << "diffusion";
  ost << ")";
}

// EOF

