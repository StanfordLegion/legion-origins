#!/usr/bin/python

import sys, re

lmem_pattern    = re.compile("ptxas info    : Used (?P<reg>[0-9]+) registers, [0-9]+ bytes cmem\[0\], [0-9]+ bytes cmem\[2\], (?P<lmem>[0-9]+)")
no_lmem_pattern  = re.compile("ptxas info    : Used (?P<reg>[0-9]+) registers")
name_pattern    = re.compile("ptxas info    : Compiling entry function '_Z[0-9]+gpu_getrates_chunk_(?P<chunk>[0-9]+)")

def filter():
    for l in sys.stdin:
        m = lmem_pattern.match(l)
        if m <> None:
            print m.group('reg')+" "+m.group('lmem')
            continue
        m = no_lmem_pattern.match(l)
        if m <> None:
            print m.group('reg')+" 0"
            continue
        m = name_pattern.match(l)
        if m <> None:
            print m.group('chunk')
            continue

if __name__ == "__main__":
    filter()

