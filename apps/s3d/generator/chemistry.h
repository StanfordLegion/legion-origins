
#ifndef __S3DGEN_CHEMISTRY__
#define __S3DGEN_CHEMISTRY__

#include "s3dgen_ast.h"

#include <map>
#include <set>
#include <list>
#include <vector>

#include <cstdlib>
#include <cstdio>
#include <cassert>

class ChemistryUnit;
class Operation;
class Liveness;
class Firstness;
class ConnectedComponent;
class QSS;
class QSSValue;
class Stif;
class StifValue;
class Warp;
class Phase;
class Allocation;
class SharedStore;

class ChemistryUnit {
public:
  ChemistryUnit(TranslationUnit *unit);
public:
  void add_component(ConnectedComponent *comp);
  void add_unimportant(int backward, int idx);
  // For the Aq_0
  void add_forward_zero(int reac_idx, int qss_idx, int coeff);
  void add_backward_zero(int reac_idx, int qss_idx, int coeff);
  // For Aq_other
  void add_forward_contribution(int reac_idx, int qss_idx, int other_idx, int coeff);
  void add_backward_contribution(int reac_idx, int qss_idx, int other_idx, int coeff);
  // For denom
  void add_forward_denom(int qss_idx, int reac_idx);
  void add_backward_denom(int qss_idx, int reac_idx);
  // For final correction
  void add_forward_correction(int reac_idx, int qss_idx);
  void add_backward_correction(int reac_idx, int qss_idx);
public:
  void add_stif(Stif *stif);
public:
  void analyze_working_set(void);
  void compute_heuristic_ordering(const std::vector<Operation*> &original,
                                 std::vector<Operation*> &result);
  void emit_code(const char *outname);
  void emit_cuda(const char *outname, bool emit_experiment);
private:
  // CPU code generation methods
  void emit_cpp_header_file(const char *file_name);
  void emit_cpp_source_file(const char *header_name, const char *file_name);
  void emit_cpp_declaration(CodeOutStream &ost); 
private:
  // Baseline GPU code generation
  void emit_baseline_cuda(CodeOutStream &ost, int num_species);
private:
  // GPU code generation methods
  void emit_getrates_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values, const char *temp_file_name);
  void emit_experiment_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values);
  void emit_reducer_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values);
  void emit_phase_reactions(CodeOutStream &ost, unsigned start_reac, unsigned term_reac, bool first, 
                            Phase *current_phase, Phase *prev_phase);
  void emit_phase_reaction(CodeOutStream &ost, unsigned reac_idx, bool second_pass, int dst_offset);
  void emit_weird_reaction(CodeOutStream &ost, Reaction *reac, Warp *warp, int reac_idx, bool second_pass, int dst_offset);
  void emit_gibbs_computations(CodeOutStream &ost, const std::set<Species*> &needed_gibbs);
  void emit_normal_reaction(CodeOutStream &ost, const std::map<Reaction*,Warp*> &arrhenius_reactions, 
                              int reac_idx, bool second_pass, int dst_offset);
  void arrhenius(CodeOutStream &ost, Warp *warp, bool emit_code, bool weird, int reac_idx, 
                 const char *target, double a, double beta, double e, bool second_pass, unsigned warp_size = 32);
  void emit_constant_declarations(CodeOutStream &ost, unsigned num_species, unsigned &constants_per_thread, 
                                  unsigned &warp_constant_stride, unsigned warp_size = 32);
  void emit_mole_fraction_updates(CodeOutStream &ost, Warp *warp, unsigned start_reac, unsigned term_reac, unsigned offset); 
private:
  void emit_two_pass_getrates_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values, const char *temp_file_name,
                                     int warps_per_stage, int reactions_per_warp, int second_pass_offset);
  void emit_two_pass_experiment_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values,
                                     int warps_per_stage, int reactions_per_warp, int second_pass_offset);
  void emit_two_pass_reducer_code(CodeOutStream &ost, unsigned num_species, unsigned scratch_values, 
                                     int warps_per_stage, int reactions_per_warp, int second_pass_offset);
  void emit_first_pass_reactions(CodeOutStream &ost, Phase *phase, unsigned start_reac);
  void emit_first_pass_writes(CodeOutStream &ost, Phase *phase, unsigned start_reac);
  void emit_second_pass_reactions(CodeOutStream &ost, unsigned start_reac, unsigned stop_reac, 
                                  unsigned warps_per_stage, unsigned second_pass_offset);
private:
  void emit_bulk_getrates_code(CodeOutStream &ost, unsigned num_species, const char *temp_file, const char *fun_file,
                                unsigned micro_warps, unsigned phases);
  void emit_bulk_experiment_code(CodeOutStream &ost, CodeOutStream &func_ost,
                                 unsigned num_species, unsigned micro_warps, unsigned offset, unsigned phases);
  void emit_bulk_loading_code(CodeOutStream &ost, unsigned num_species, unsigned micro_warps, unsigned offset, unsigned phases);
  unsigned emit_bulk_reaction(CodeOutStream &ost, CodeOutStream &func_ost, int reac_index, int start_spec_barrier, unsigned micro_warps,
                                              unsigned offset, unsigned start_offset);
  void emit_bulk_qssa_code(CodeOutStream &ost, unsigned micro_warps);
  void emit_bulk_stiffness_code(CodeOutStream &ost, unsigned micro_warps);
  void emit_bulk_species_output(CodeOutStream &ost, unsigned num_species, unsigned micro_warps, unsigned offset, unsigned phases);
  void emit_bulk_normal_reaction(CodeOutStream &ost, CodeOutStream &func_ost, const std::vector<Reaction*> &needed_reactions,
                                 const std::vector<unsigned> &warp_index, unsigned reac_index, unsigned start_spec_barrier, 
                                 unsigned micro_warps, unsigned offset, unsigned start_offset);
  void emit_bulk_weird_reaction(CodeOutStream &ost, CodeOutStream &fun_ost, const std::vector<Reaction*> &needed_reactions,
                                const std::vector<unsigned> &warp_index, unsigned reac_index, unsigned start_spec_barrier,
                                unsigned micro_warps, unsigned offset, unsigned start_offset);
  void emit_bulk_weird_helper(CodeOutStream &ost, Reaction *reac, unsigned warp_index, unsigned reac_index, unsigned start_spec_barrier,
                              unsigned micro_warps, unsigned offset);
private:
  void emit_pipe_getrates_code(CodeOutStream &ost, unsigned non_qss_species, const char *temp_file);
  void emit_pipe_reaction_code(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species, int phase, int pass);
  void emit_pipe_mole_fraction_code(CodeOutStream &ost, const std::vector<Warp*> &target_warps, 
                            unsigned non_qss_species, int pass, const std::vector<Warp*> &next_warps);
  void emit_pipe_qssa_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species);
  void emit_pipe_stiffness_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species);
  void emit_pipe_output_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species);
  void emit_pipe_arrhenius(CodeOutStream &ost, Warp *warp, bool emit_code, bool weird, unsigned stage_idx,
                          const char *target, double a, double beta, double e, int phase, int pass);
  void emit_pipe_general_constants(CodeOutStream &ost, unsigned num_species);
  void emit_pipe_warp_constants(CodeOutStream &ost, const std::vector<Warp*> &target_warps, int pass,
                                unsigned &constants_per_thread, unsigned &warp_constant_stride);
  void emit_pipe_fine_scaling_code(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species,
                                    int pass, int phase, bool even, const std::vector<Warp*> &next_warps);
  void emit_pipe_fine_qssa_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species);
  void emit_pipe_fine_stiffness_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species);
  void emit_pipe_fine_output_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species);
private:
  void emit_multi_getrates_code(CodeOutStream &ost, unsigned non_qss_species, const char *temp_qssa, const char *temp_stif, const char *temp_last);
  void emit_multi_reaction_code(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species, int pass, int other_warps);
  void emit_multi_qssa_computation(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species, 
                                  const std::vector<std::map<QSS*,int> > &qss_partition, int max_values, int max_denom);
  void emit_multi_qssa_kernel(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species, 
                                  const std::vector<std::map<QSS*,int> > &qss_partition, int max_values, int max_denom);
private:
  void emit_last_getrates_code(CodeOutStream &ost, unsigned non_qss_species, const char *temp_last);
  void emit_last_reaction_code(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species, 
                                unsigned last_qss_idx, unsigned first_non_qss_idx, unsigned max_stages,
                                const std::vector<std::map<QSS*,int> > &qss_partition, int max_values, int max_denom);
  void emit_last_reaction_range(CodeOutStream &ost, const std::vector<Warp*> &target_warps, unsigned non_qss_species,
                                int start_idx, int stop_idx, int pass, unsigned &total_gibbs, unsigned &total_troe, unsigned &total_reverse);
private:
  // Generate experiment code
  void emit_gpu_experiment(CodeOutStream &ost, int num_species, const char *expr_name, bool striding_z, bool multi_pass = false);
private:
  ConnectedComponent *get_qss(int spec);
public:
  int reaction_granularity;
private:
  TranslationUnit *const unit;
  std::vector<ConnectedComponent*> connected_components;
  std::set<int> forward_unimportant;
  std::set<int> backward_unimportant;
  std::vector<Stif*> stif_operations;
private:
  std::vector<Warp*> warps;
  std::vector<Phase*> phases;
  std::map<Reaction*,int/*location*/> reaction_map;
  std::map<int/*reac idx*/,int/*location*/> reaction_index_map;
private:
  std::vector<Warp*> qssa_warps;
  std::vector<Warp*> stif_warps;
  std::vector<Warp*> last_warps;
  std::vector<Warp*> full_warps;
private:
  std::vector<Species*> gibbs_species;
  std::vector<Reaction*> gibbs_reactions;
  std::vector<Reaction*> needed_troe_reactions;
};

// For doing analysis to determine working set size
class Liveness {
public:
  Liveness(void) { }
public:
  std::set<int> fneeded;
  std::set<int> bneeded;
};

class Firstness {
public:
  Firstness(void) { }
public:
  std::set<int> fmade;
  std::set<int> bmade;
  std::set<int> reactions;
};

class Operation {
public:
  Operation(void) { }
public:
  void sanity_check(void);
  void get_read_values(std::set<int> &forward, std::set<int> &backward) const;
  void get_write_values(std::set<int> &forward, std::set<int> &backward) const;
  void remove_read_values(std::set<int> &forward, std::set<int> &backward) const;
  void remove_write_values(std::set<int> &forward, std::set<int> &backward) const;
  bool can_swap(const Operation *previous) const;
public:
  void compute_liveness(Liveness &live);
  void clear_liveness(void) { flive.clear(); blive.clear(); }
public:
  void compute_first(Firstness &first);
  void clear_firstness(void) { ffirst.clear(); bfirst.clear(); }
public:
public:
  virtual const char* get_name(void) const = 0;
  virtual bool is_stif(void) const = 0;
public:
  size_t ffirst_size(void) const { return ffirst.size(); }
  size_t bfirst_size(void) const { return bfirst.size(); }
  size_t flive_size(void) const { return flive.size(); }
  size_t blive_size(void) const { return blive.size(); }
  size_t first_reac_size(void) const { return reactions.size(); }
  size_t fread_size(void) const { return fread.size(); }
  size_t bread_size(void) const { return bread.size(); }
  size_t fwrite_size(void) const { return fwrite.size(); }
  size_t bwrite_size(void) const { return bwrite.size(); }
protected:
  void add_fread(int x) { fread.insert(x); }
  void add_bread(int x) { bread.insert(x); }
  void add_fwrite(int x) { fwrite.insert(x); }
  void add_bwrite(int x) { bwrite.insert(x); }
protected:
  friend class ChemistryUnit;
  std::set<int> fread; // reading forward reaction rates
  std::set<int> bread; // reading backward reaction rates
  std::set<int> fwrite; // writing forward reaction rates
  std::set<int> bwrite; // writing backward reaction rates
protected:
  std::set<int> flive; // live forward reaction rates
  std::set<int> blive; // live backward reaction rates
  std::set<int> ffirst; // first forward reaction rates
  std::set<int> bfirst; // first backward reaction rates
  std::set<int> reactions; // first reactions
};

class ConnectedComponent : public Operation {
public:
  ConnectedComponent(TranslationUnit *unit, ConnectedComponent *orig = NULL);
public:
  void add_forward_zero(int reac_idx, int qss_idx, int coeff);
  void add_backward_zero(int reac_idx, int qss_idx, int coeff);
  void add_forward_contribution(int reac_idx, int qss_idx, int other_idx, int coeff);
  void add_backward_contribution(int reac_idx, int qss_idx, int other_idx, int coeff);
  void add_forward_denom(int qss_idx, int reac_idx);
  void add_backward_denom(int qss_idx, int reac_idx);
  void add_forward_correction(int reac_idx, int qss_idx);
  void add_backward_correction(int reac_idx, int qss_idx);
public:
  // Ay_0 = Ay_0 + Ay_x * Ax_0
  void add_zero_statement(int y, int x);
  void begin_den_statement(void);
  void end_den_statement(void);
  // Add DEN pair for 1 - Ay_x*Ax_y for all pairs
  void add_den_pair(int y, int x);
  // Ay_z = Ay_x * Ax_z
  void add_multiply_statement(int z, int y, int x);
  // Ay_z = Ay_z + Ay_x * Ax_z
  void add_add_statement(int z, int y, int x);
  // Ay_z = Ay_z/DEN
  void add_divide_statement(int z, int y);
  // XQ(x) = Ax_0
  void start_x_statement(int x);
  void finish_x_statement(void);
  // +Ax_y*XQ(y)
  void add_x_pair(int x, int y);
public:
  void add_species(int id, int spec);
  bool has_species(int id);
  size_t num_species(void) const { return species.size(); }
public:
  virtual const char* get_name(void) const { return "Connected Component"; }
  virtual bool is_stif(void) const { return false; }
public:
  void compute_allocations(std::vector<Allocation*> &needed_allocations,
                           const std::vector<Warp*> &warps, int reaction_granularity);
  void clear_allocations(void);
public:
  bool is_simple(void) const;
  bool has_qss(QSS *qss) const;
public:
  int updates_warp(Warp *warp) const;
  int emit_updates(CodeOutStream &ost, Warp *warp, int phase_id, int scaling_offset);
  int emit_qssa_updates(CodeOutStream &ost, Warp *warp, int scaling_offset);
  void emit_scaling_reads(CodeOutStream &ost, Warp *warp, int phase_id, int &src_offset, int &dst_offset);
public:
  void emit_cpp_statements(CodeOutStream &ost);
  int emit_statements(CodeOutStream &ost, int output_offset);
  void emit_stage_loads(CodeOutStream &ost, const std::map<Warp*,unsigned> &stage, bool first);
  unsigned emit_stage_statements(CodeOutStream &ost, const char *target, unsigned target_offset);
public:
  void emit_pipe_variable_declarations(CodeOutStream &ost);
  void emit_pipe_updates(CodeOutStream &ost, unsigned local_idx, int reac_idx);
  void emit_pipe_qssa_computation(CodeOutStream &ost);
  void emit_pipe_corrections(CodeOutStream &ost, unsigned local_idx, int reac_idx);
public:
  void emit_last_simple_statements(CodeOutStream &ost, int avars_off);
  void emit_last_complex_statements(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap);
  void emit_last_corrections(CodeOutStream &ost, unsigned local_idx, int reac_idx, std::set<int> &loaded_qss_values);
  void emit_last_simple_qssvalues(CodeOutStream &ost);
private:
  QSS* get_qss(int spec);
private:
  class Statement { 
  public:
    virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted) = 0;
    virtual void emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap) = 0;
    void emit_last_var(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap, std::pair<int,int> key);
    void emit_last_load(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap, std::pair<int,int> key);
  };
  class Zero : public Statement {
  public:
    Zero(int yp, int xp) : y(yp), x(xp) { }
  public:
    int y, x;
  public:
    virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted);
    virtual void emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap);
  };
  class Den : public Statement {
  public:
    Den(void) { }
  public:
    void add_pair(int y, int x) { pairs.push_back(std::pair<int,int>(y,x)); }
  public:
    virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted);
    virtual void emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap);
  public:
    std::vector<std::pair<int,int> > pairs;
  };
  class Multiply : public Statement {
  public:
    Multiply(int zp, int yp, int xp) : z(zp), y(yp), x(xp) { }
  public:
    virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted);
    virtual void emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap);
  public:
    int z, y, x;
  };
  class Add : public Statement {
  public:
    Add(int zp, int yp, int xp) : z(zp), y(yp), x(xp) { }
  public:
    virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted);
    virtual void emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap);
  public:
    int z, y, x;
  };
  class Divide : public Statement {
  public:
    Divide(int zp, int yp) : z(zp), y(yp) { }
  public:
    virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted);
    virtual void emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap);
  public:
    int z, y;
  };
  class Xstat : public Statement {
  public:
    Xstat(int xp) : x(xp) { }
    public:
    virtual void emit_code(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted);
    virtual void emit_last_code(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap);
  public:
    void add_pair(int xp, int y) { assert(x == xp); assert(y_vals.find(y) == y_vals.end()); y_vals.insert(y);  }
  public:
    int x;
    std::set<int> y_vals;
  };
public:
  std::map<int,QSS*> species;
  std::vector<Statement*> statements;
  TranslationUnit *unit;
private:
  // For parsing only
  Den *current_den;
  Xstat *current_x;
};

class QSS {
public:
  QSS(int id, int spec, TranslationUnit *unit);
public:
  int get_total_values(void) const;
public:
  void add_forward_zero(int reac_idx, int coeff);
  void add_backward_zero(int reac_idx, int coeff);
  void add_forward_contribution(int reac_idx, int other_idx, int coeff);
  void add_backward_contribution(int reac_idx, int other_idx, int coeff);
  void add_forward_denom(int reac_idx);
  void add_backward_denom(int reac_idx);
  void add_forward_correction(int reac_idx);
  void add_backward_correction(int reac_idx);
public:
  void compute_allocations(std::vector<Allocation*> &needed_allocations,
                           const std::vector<Warp*> &warps, int reaction_granularity);
  void compute_zero_allocations(std::vector<Allocation*> &needed_allocations,
                                const std::vector<Warp*> &warps, int reaction_granularity);
  void compute_contribution_allocations(int other, std::vector<Allocation*> &needed_allocations,
                                        const std::vector<Warp*> &warps, int reaction_granularity);
  void compute_denominator_allocations(std::vector<Allocation*> &needed_allocations,
                                       const std::vector<Warp*> &warps, int reaction_granularity);
  void clear_allocations(void);
public:
  bool updates_warp(Warp *warp) const;
  void emit_updates(CodeOutStream &ost, Warp *warp, int phase_id, int scaling_offset);
  void emit_qssa_updates(CodeOutStream &ost, Warp *warp, int scaling_offset);
  void emit_scaling_read(CodeOutStream &ost, Warp *warp, int phase_id, int src_offset, int dst_offset);
public:
  void emit_statements(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted, bool stage);
public:
  void emit_cpp_statements(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted);
  void emit_cpp_updates(CodeOutStream &ost);
  void emit_stage_loads(CodeOutStream &ost, const std::map<Warp*,unsigned> &stage, bool first);
public:
  void emit_pipe_variable_declarations(CodeOutStream &ost);
  void emit_pipe_updates(CodeOutStream &ost, unsigned local_idx, int reac_idx);
  void emit_pipe_qssa_computation(CodeOutStream &ost, std::set<std::pair<int,int> > &already_emitted);
  void emit_pipe_corrections(CodeOutStream &ost, unsigned local_idx, int reac_idx);
public:
  void emit_multi_updates(CodeOutStream &ost, int avars_off, int denom_off, int local_off, int reac_idx);
  void emit_multi_scaled_updates(CodeOutStream &ost, int avars_off, int denom_off, int local_off, int reac_idx, Reaction *reac);
  void emit_multi_declaration(CodeOutStream &ost, Reaction *reac, int local_idx, bool forward);
  void emit_multi_stores(CodeOutStream &ost, int avars_off, int denom_off, int write_off);
public:
  int emit_last_scaled_updates(CodeOutStream &ost, int avars_off, int denom_off, int local_off, 
                                int reac_idx, Reaction *reac, int reverse_off, int num_loads);
  void emit_last_scaled_declaration(CodeOutStream &ost, Reaction *reac, int local_idx, bool forward, int reverse_off);
  void emit_last_divide(CodeOutStream &ost, int avars_off, int denom_off);
  void emit_last_stores(CodeOutStream &ost, int avars_off, int denom_off, std::map<std::pair<int,int>,int> &amap);
  void emit_last_corrections(CodeOutStream &ost, unsigned local_idx, int reac_idx, std::set<int> &loaded_qss_values);
public:
  void partition_values(std::vector<std::vector<QSSValue*> > &accum_values, unsigned &next_warp);
  void emit_qssvalue_loads(CodeOutStream &ost);
  void emit_qssvalue_operations(CodeOutStream &ost);
  void emit_qssvalue_stores(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap);
public:
  const int qss_idx;
  const int species_idx;
public:
  std::set<int> member_species;
  std::map<int/*reac*/,int/*coeff*/> forward_zeros;
  std::map<int/*reac*/,int/*coeff*/> backward_zeros;
  std::map<std::pair<int/*other idx*/,int/*reac*/>,int/*coeff*/> forward_contributions;
  std::map<std::pair<int/*other idx*/,int/*reac*/>,int/*coeff*/> backward_contributions;
  std::set<int/*reac*/> forward_denom;
  std::set<int/*reac*/> backward_denom;
  std::set<int/*reac*/> forward_corrections;
  std::set<int/*reac*/> backward_corrections;
public:
  std::vector<QSSValue*> avals;
  QSSValue *denom;
  TranslationUnit *unit;
private:
  std::vector<Allocation*> zero_allocations;
  std::map<int,std::vector<Allocation*> > contribution_allocations;
  std::vector<Allocation*> denominator_allocations;
};

class QSSValue {
public:
  QSSValue(QSS *owner, int other_idx, TranslationUnit *unit);
public:
  void add_forward_value(unsigned warp, int warp_idx, int reac_idx, int coeff);
  void add_backward_value(unsigned warp, int warp_idx, int reac_idx, int coeff);
public:
  void emit_accum_updates(CodeOutStream &ost, unsigned widx, int reac_idx, bool &forward_loaded, bool &backward_loaded, 
                          bool local, int scratch_idx, int stage_idx, int reverse_offset);
  void emit_load(CodeOutStream &ost);
  void emit_operation(CodeOutStream &ost);
  void emit_store(CodeOutStream &ost, std::map<std::pair<int,int>,int> &amap);
public:
  QSS *owner;
  int other_idx;
  TranslationUnit *unit;
  std::map<int/*warp*/,std::map<int/*reac*/,int/*coeff*/> > forward_values;
  std::map<int/*warp*/,std::map<int/*reac*/,int/*coeff*/> > backward_values;
  std::map<int/*warp*/,int/*warp index*/> warp_locations;
};

class Stif : public Operation {
public:
  Stif(const char *name, int k, int k2, TranslationUnit *unit);
public:
  void add_forward_d(int reac_idx, int coeff);
  void add_forward_c(int reac_idx, int coeff);
  void add_backward_d(int reac_idx, int coeff);
  void add_backward_c(int reac_idx, int coeff);
public:
  virtual const char* get_name(void) const { return "Stiff Species"; }
  virtual bool is_stif(void) const { return true; }
public:
  void compute_allocations(std::vector<Allocation*> &needed_allocations,
                           const std::vector<Warp*> &warps, int reaction_granularity);
  void clear_allocations(void);
public:
  bool updates_warp(Warp *warp) const;
  void emit_updates(CodeOutStream &ost, Warp *warp, int phase_id, int scaling_offset);
  void emit_scaling_read(CodeOutStream &ost, Warp *warp, int phase_id, int src_offset, int dst_offset);
public:
  void emit_statements(CodeOutStream &ost, const char *target, int output_offset, int num_species, bool stage);
  void emit_cpp_statements(CodeOutStream &ost, TranslationUnit *unit);
  void emit_stage_loads(CodeOutStream &ost, const std::map<Warp*,unsigned> &stage, bool first);
public:
  void emit_pipe_updates(CodeOutStream &ost, int stif_idx, unsigned local_idx, int reac_idx);
  void emit_pipe_statements(CodeOutStream &ost, int stif_idx, TranslationUnit *unit);
  void emit_pipe_corrections(CodeOutStream &ost, int stif_idx, unsigned local_idx, int reac_idx);
public:
  void emit_last_updates(CodeOutStream &ost, int stif_idx, unsigned local_idx, int scratch_loc, int reac_idx, int reverse_offset,
                        std::set<int> &emitted_forward, std::set<int> &emitted_backward);
  void emit_last_corrections(CodeOutStream &ost, int stif_idx, int local_idx, int reac_idx, std::set<int> &loaded_stif_values);
public:
  void emit_stif_value_loads(CodeOutStream &ost, int local_idx);
  void partition_values(std::vector<std::vector<StifValue*> > &accum_values, unsigned &next_warp);
public:
  Species *species;
public:
  const char *const name;
  const int k;
  const int k2;
  TranslationUnit *unit;
  // Creation and deletion
  std::map<int/*reac*/,int/*coeff*/> forward_d;
  std::map<int/*reac*/,int/*coeff*/> forward_c;
  std::map<int/*reac*/,int/*coeff*/> backward_d;
  std::map<int/*reac*/,int/*coeff*/> backward_c;
public:
  StifValue *construction_value;
  StifValue *destruction_value;
private:
  std::vector<Allocation*> d_allocations;
  std::vector<Allocation*> c_allocations;
};

class StifValue {
public:
  StifValue(Stif *owner, bool destruction, TranslationUnit *unit);
public:
  void add_forward_value(unsigned warp, int warp_idx, int reac_idx, int coeff);
  void add_backward_value(unsigned warp, int warp_idx, int reac_idx, int coeff);
public:
  void emit_accum_updates(CodeOutStream &ost, unsigned widx, int reac_idx, bool &forward_loaded, bool &backward_loaded, 
                          bool local, int scratch_idx, int stage_idx, int reverse_offset);
  void emit_load(CodeOutStream &ost, unsigned local_idx);
public:
  Stif *owner;
  bool destruction;
  TranslationUnit *unit;
  std::map<int/*warp*/,std::map<int/*reac*/,int/*coeff*/> > forward_values;
  std::map<int/*warp*/,std::map<int/*reac*/,int/*coeff*/> > backward_values;
  std::map<int/*warp*/,int/*warp index*/> warp_locations;
};

class Warp {
public:
  Warp(int id);
public:
  bool has_reaction(int reac_idx, int pass = -1) const;
  bool has_index(int index, int pass = -1) const;
  void add_reaction(int reac_idx, int pass = -1);
  size_t num_reactions(int pass = -1) const;
  void set_reaction(int reac_idx, Reaction *reac, int pass = -1);
  int get_reaction_index(int reac_idx, int pass = -1);
  int get_reaction_idx(int index, int pass = -1);
  Reaction* get_reaction(int reac_idx, int pass = -1);
public:
  void sort_weird_reactions(int start_idx, int stop_idx);
  void compute_needed_xqs(const std::vector<ConnectedComponent*> &ccs, std::vector<int> &needed_xqs);
public:
  bool can_add_constants(unsigned reac_idx, unsigned cnt, bool second_pass);
  int add_constant(unsigned reac_idx, double constant, bool second_pass);
  unsigned emit_constants(CodeOutStream &ost, bool first);
  void emit_num_constants(CodeOutStream &ost, bool first, int offset, int num_to_emit);
public:
  void add_troe_reaction(int stage, int index, bool first);
  void add_gibbs_species(int location, int index, double coefficient, bool first);
  void add_reverse_constant(int stage, double coefficient, bool first);
  void add_stiff_species(int stage, int stif_idx, Stif *stif, bool first);
public:
  void emit_troe_index(CodeOutStream &ost, int stage);
  void emit_gibbs_index(CodeOutStream &ost, int stage);
  void emit_gibbs_scale(CodeOutStream &ost, int stage);
  void emit_reverse_constant(CodeOutStream &ost, int stage);
  void emit_stiff_index(CodeOutStream &ost, int stage);
public:
  int wid;
  std::set<int> reaction_indexes;
  std::vector<int> reaction_order;
  std::map<int,Reaction*> reactions;
  std::map<int/*reac idx*/,int/*pass*/> pass_mask;
public:
  std::vector<std::vector<double> > reaction_constants;
  std::vector<std::vector<double> > second_pass_constants; // should be identical
public:
  std::vector<int> troe_reactions;
  std::vector<int> second_pass_troe;
public:
  std::vector<std::pair<int/*spec idx*/,double> > gibbs_species;
  std::vector<std::pair<int/*spec idx*/,double> > second_gibbs;
public:
  std::vector<double> reverse_constants;
  std::vector<double> second_reverse;
public:
  std::vector<Stif*> stif_species;
  std::vector<Stif*> second_stif;
};

class Phase {
public:
  Phase(int id, int available_values);
public:
  bool add_connected_component(ConnectedComponent *cc, const std::vector<Warp*> &warps, int granularity);
  // Return number of values required in the store or -1 if it doesn't fit
  int can_add_stif(Stif *op, const std::vector<Warp*> &warps, int reaction_granularity);
  void add_stif(Stif *op, const std::vector<Warp*> &warps, int reaction_granularity);
  bool empty(void) const;
  void finalize(const std::vector<Warp*> &warps);
public:
  void sort_into_stages(unsigned num_stages, unsigned max_values_per_stage,
                        const std::vector<Warp*> &warps);
  int find_warp_stage(Warp *warp, int &stage_idx);
public:
  void emit_write_store_values(CodeOutStream &ost, Warp *warp);
  void emit_read_scaling_factors(CodeOutStream &ost, Warp *warp);
  void emit_apply_scaling_factors(CodeOutStream &ost, Warp *warp);
  void emit_apply_qssa_factors(CodeOutStream &ost, Warp *warp);
public:
  void emit_operations(CodeOutStream &ost, int num_species);
  unsigned emit_staged_operations(CodeOutStream &ost, int num_species, const char *target, unsigned target_offset);
public:
  const int phase_id;
  std::vector<ConnectedComponent*> components;
  std::vector<Stif*> stifs;
  SharedStore *store;
  unsigned max_reaction; // the largest reaction idx in any warp that this phase requires
  // The number of stages in this phase
  std::vector<std::map<Warp*,unsigned> > stages;
};

class Allocation {
public:
  Allocation(int warp_id);
public:
  void add_forward_expression(int reac_idx, int coeff);
  void add_backward_expression(int reac_idx, int coeff);
  void set_location(int location);
public:
  void emit_store_allocation(CodeOutStream &ost, Warp *warp);
public:
  const int warp_id;
  int location; // -1 if not set
  int stage; // -1 if not set
  std::map<int,int> forward_expressions;
  std::map<int,int> backward_expressions;
};

class SharedStore {
public:
  SharedStore(int available_values);
public:
  bool has_space(const std::vector<Allocation*> &needed_allocations) const;
  void commit_allocations(const std::vector<Allocation*> &needed_allocations);
public:
  const int available_values;
  std::vector<Allocation*> allocations;
};

#endif // __S3DGEN_CHEMISTRY__
