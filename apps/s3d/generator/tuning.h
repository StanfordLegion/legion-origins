
#ifndef __S3D_TUNING__
#define __S3D_TUNING__

#include "s3dgen_ast.h"

#include <set>
#include <vector>

class ReactionGraph;
class ReactionNode;
class SpeciesEdge;

class ReactionGraph {
public:
  ReactionGraph(const SpeciesSet &species, const ReactionVec &reactions);
  ~ReactionGraph(void);
public:
  void color_node(ReactionNode *node, unsigned color);
  ReactionNode* find_largest_uncolored(void);
  void find_adjacent(unsigned color, const std::set<Species*> &color_set, std::map<ReactionNode*,unsigned> &adjacent);
  void clean_graph(void);
  bool has_uncolored(void) const;
  void find_uncolored(std::set<ReactionNode*> &uncolored);
private:
  std::vector<ReactionNode*> nodes;
  std::vector<SpeciesEdge*> edges;
};

class ReactionNode {
public:
  ReactionNode(Reaction *reac);
public:
  void add_edge(SpeciesEdge *edge);
  bool has_species(Species *spec) const;
  unsigned get_num_species(void) const;
  SpeciesEdge* find_edge(ReactionNode *node) const;
public:
  Reaction *const reaction;
private:
  friend class ReactionGraph;
  int color; // -1 is uncolored
  std::vector<SpeciesEdge*> edges;
};

class SpeciesEdge {
public:
  SpeciesEdge(ReactionNode *left, ReactionNode *right, Species *init);
public:
  void add_weight(Species *spec);
  const std::set<Species*>& get_species(void) const;
  unsigned get_weight(void) const;
private:
  friend class ReactionGraph;
  friend class ReactionNode;
  ReactionNode *left, *right;
  // Total weight of the edge is the number of species
  std::set<Species*> weights;
};

#endif // __S3D_TUNING__

// EOF

