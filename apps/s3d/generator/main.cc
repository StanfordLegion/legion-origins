
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <unistd.h>

#include "s3dgen_ast.h"
#include "s3dgen_parse.h"
#include "chemistry.h"

static void usage(const char *progname);

int main(int argc, char **argv)
{
  bool dimensional = false;
  bool print_cuda = false;
  bool temp_array = false;
  bool backwards = false;
  bool fast_math = false;
  bool use_shared = false;
  bool emit_experiment = false;
  bool seperate_streams = false;
  bool uber_kernel = false;
  bool use_atomics = false;
  bool use_double2 = false;
  bool load_balance = false;
  bool hybrid = false;
  bool mac_dumbness = false;
  bool visc_shared = true;
  bool emit_chemistry = true;
  bool emit_physics = true;
  unsigned int chunks = 0;
  unsigned int points_per_thread = 4;
  unsigned int threads_per_block = 128;
  unsigned int max_tuning_iters = 0;
  unsigned int num_warps = 8;
  unsigned int vector_width = 0;
  unsigned int taylor_stages = 0;
  unsigned int socket_config = 0;
  unsigned int threads_per_point = 0;
  unsigned int diff_block_v = 0;
  unsigned int diff_block_h = 0;
  int reaction_granularity = -1;
  int prefetch_level = -1;
  const char *out_name = "getrates";
  const char *thermos_name = NULL;
  const char *slicing_config = NULL;
  const char *tran_name = NULL;
  const char *dir_name = NULL;
  const char *chem_name = NULL;
#if 0
  int i;
  while ((i=getopt(argc, argv, "bc:no:h:i:gf:sp:t:m:e:w:uadv:lyxq:k:")) != EOF)
  {
    switch (i)
    {
      case 'b':
        backwards = true;
        break;
      case 'c':
        if (('0' <= optarg[0]) && (optarg[0] <= '9'))
          chunks = atoi(optarg);
        else
          slicing_config = optarg;
        break;
      case 'n':
        dimensional = true;
        break;
      case 'o':
        out_name = optarg;
        break;
      case 'h':
        thermos_name = optarg;
        break;
      case 'i':
        tran_name = optarg;
        break;
      case 'g':
        print_cuda = true;
        break;
      case 'f':
        fast_math = true;
        taylor_stages = atoi(optarg);
        break;
      case 's':
        use_shared = true;
        break;
      case 'p':
        points_per_thread = atoi(optarg);
        break;
      case 't':
        threads_per_block = atoi(optarg);
        break;
      case 'm':
        max_tuning_iters = atoi(optarg);
        break;
      case 'e':
        emit_experiment = true;
        seperate_streams = (atoi(optarg) > 0);
        break;
      case 'w':
        num_warps = atoi(optarg);
        break;
      case 'u':
        uber_kernel = true;
        break;
      case 'a':
        use_atomics = true;
        break;
      case 'd':
        use_double2 = true;
        break;
      case 'v':
        vector_width = atoi(optarg);
        if ((vector_width != 2) && (vector_width != 4))
        {
          fprintf(stderr,"Vector width must be one of 2 (SSE) or 4 (AVX)\n");
          return 1;
        }
        break;
      case 'l':
        load_balance = true;
        break;
      case 'y':
        hybrid = true;
        break;
      case 'x':
        mac_dumbness = true;
        break;
      case 'q':
        prefetch_level = atoi(optarg);
        break;
      case 'k':
        socket_config = atoi(optarg);
        break;
      default:
        fprintf(stderr,"Bad command line argument!\n");
        usage(argv[0]);
        return 1;
    }
  }
  if (optind != (argc-1))
  {
    usage(argv[0]);
    return 1;
  }
#else
#define MATCH(str) (strcmp(argv[idx],str) == 0)
  for (int idx = 1; idx < argc; idx++)
  {
    if (MATCH("-nondim") || MATCH("-n"))
    {
      dimensional = true;
      continue;
    }
    if (MATCH("-therm") || MATCH("-h"))
    {
      thermos_name = argv[++idx];
      continue;
    }
    if (MATCH("-tran") || MATCH("-i"))
    {
      tran_name = argv[++idx];
      continue;
    }
    if (MATCH("-dir"))
    {
      dir_name = argv[++idx];
      char *tran = (char*)malloc(128*sizeof(char));
      strcpy(tran,argv[idx]);
      strcat(tran,"/tran.dat");
      tran_name = tran;
      char *therm = (char*)malloc(128*sizeof(char));
      strcpy(therm,argv[idx]);
      strcat(therm,"/therm.dat");
      thermos_name = therm;
      continue;
    }
    if (MATCH("-o"))
    {
      out_name = argv[++idx];
      continue;
    }
    if (MATCH("-backward") || MATCH("-b"))
    {
      backwards = true;
      continue;
    }
    if (MATCH("-chunks") || MATCH("-c"))
    {
      idx++;
      if (('0' <= argv[idx][0]) && (argv[idx][0] <= '9'))
        chunks = atoi(argv[idx]);
      else
        slicing_config = argv[idx];
      continue;
    }
    if (MATCH("-gpu") || MATCH("-g"))
    {
      print_cuda = true;
      continue;
    }
    if (MATCH("-fastmath") || MATCH("-f"))
    {
      fast_math = true;
      taylor_stages = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("-shared") || MATCH("-shared"))
    {
      use_shared = true;
      continue;
    }
    if (MATCH("-points") || MATCH("-p"))
    {
      points_per_thread = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("-threads") || MATCH("-t"))
    {
      threads_per_block = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("-magic") || MATCH("-m"))
    {
      max_tuning_iters = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("-experiment") || MATCH("-e"))
    {
      emit_experiment = true;
      seperate_streams = (atoi(argv[++idx]) > 0);
      continue;
    }
    if (MATCH("-warps") || MATCH("-w"))
    {
      num_warps = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("-uber") || MATCH("-u"))
    {
      uber_kernel = true;
      continue;
    }
    if (MATCH("-atomic") || MATCH("-a"))
    {
      use_atomics = true;
      continue;
    }
    if (MATCH("-double") || MATCH("-d"))
    {
      use_double2 = true;
      continue;
    }
    if (MATCH("-vector") || MATCH("-v"))
    {
      vector_width = atoi(argv[++idx]);
      if ((vector_width != 2) && (vector_width != 4))
      {
        fprintf(stderr,"Vector width must be one of 2 (SSE) or 4 (AVX)\n");
        return 1;
      }
      continue;
    }
    if (MATCH("-load") || MATCH("-l"))
    {
      load_balance = true;
      continue;
    }
    if (MATCH("-hybrid") || MATCH("-y"))
    {
      hybrid = true;
      continue;
    }
    if (MATCH("-mac") || MATCH("-x"))
    {
      mac_dumbness = true;
      continue;
    }
    if (MATCH("-prefetch") || MATCH("-q"))
    {
      prefetch_level = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("-socket") || MATCH("-k"))
    {
      socket_config = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("-tpp"))
    {
      threads_per_point = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("-visc"))
    {
      visc_shared = true;
      continue;
    }
    if (MATCH("-diff"))
    {
      diff_block_h = atoi(argv[++idx]);
      diff_block_v = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("-chem"))
    {
      chem_name = argv[++idx];
      continue;
    }
    if (MATCH("-granularity"))
    {
      reaction_granularity = atoi(argv[++idx]);
      continue;
    }
    if (MATCH("-nochem"))
    {
      emit_chemistry = false;
      continue;
    }
    if (MATCH("-nophys"))
    {
      emit_physics = false;
      continue;
    }
    if (idx != (argc-1))
    {
      usage(argv[0]);
      return 1;
    }
  }
#undef MATCH
#endif
  if (argc == 1)
  {
    usage(argv[0]);
    return 1;
  }
  // parse the input file
  TranslationUnit *unit = NULL;
  if (dir_name == NULL)
    unit = parse_source_file(argv[argc-1]);
  else
  {
    char source[128];
    strcpy(source,dir_name);
    strcat(source,"/chem.inp");
    unit = parse_source_file(source);
  }
  assert(unit != NULL);
  unit->post_parse();
  if (thermos_name == NULL)
  {
    if (!unit->has_thermos)
    {
      fprintf(stderr,"ERROR: Input file %s has no thermodynamic data.\nPlease "
        "provide a thermodynamic data file to reference using the '-t' option.\n", argv[argc-1]);
      exit(1);
    }
    parse_therm_file(unit, argv[argc-1]);
  }
  else
    parse_therm_file(unit, thermos_name);
  if (tran_name != NULL)
  {
    if (strstr(tran_name,"asc") != NULL)
      unit->tran = parse_tran_file(unit, tran_name);
    else
      unit->tran = parse_tran_dat_file(unit, tran_name);
  }
  if (!dimensional)
  {
    fprintf(stdout,"Non-dimensionalizing coefficients...\n");
    unit->non_dimensionalize();
  }
  if (chunks > 0)
    unit->chunkify(chunks);
  else if (slicing_config != NULL)
    unit->load_config(slicing_config);

  if (temp_array)
    unit->multi_temp = temp_array;
  if (backwards)
    unit->iterate_forwards = false;
  unit->print_summary();
  ChemistryUnit *chem = NULL;
  if (chem_name != NULL)
  {
    chem = parse_qss_stif_file(unit, chem_name);
    assert(chem != NULL);
    chem->reaction_granularity = reaction_granularity;
    //chem->analyze_working_set();
  }
  if (emit_chemistry)
  {
    unit->emit_code(out_name);
    if (chem != NULL)
      chem->emit_code(out_name);
  }
  if (emit_physics)
  {
    if (unit->tran != NULL)
      unit->tran->emit_code("getcoeffs");
  }
  if (print_cuda)
  {
    unit->fast_math = fast_math;
    unit->taylor_stages = taylor_stages;
    unit->use_shared = use_shared;
    unit->threads_per_block = (use_double2 ? threads_per_block/2 : threads_per_block);
    assert(unit->threads_per_block %32 == 0);
    unit->points_per_thread = points_per_thread;
    unit->num_warps = num_warps;
    unit->use_atomics = use_atomics;
    unit->use_double2 = use_double2;
    unit->threads_per_point = threads_per_point;
    unit->visc_shared = visc_shared;
    unit->diff_block_h = diff_block_h;
    unit->diff_block_v = diff_block_v;
    // The old way of generating GPU code prior to QSSA and Stiffness
#if 0
    if (max_tuning_iters > 0)
    {
      if ((chunks > 0) || (slicing_config != NULL))
        fprintf(stderr,"WARNING: Overwriting previous chunking config during tuning!\n");
      unit->tune_gpu(max_tuning_iters);
      return 0;
    }
    if ((chunks > 0) || (max_tuning_iters > 0) || (slicing_config != NULL))
      unit->emit_cuda(out_name, emit_experiment, seperate_streams, uber_kernel);
    else
      fprintf(stderr,"WARNING: GPU code generation specified without specifying\n"
                     "         chunking factor, slicing config, or tuning. No GPU\n"
                     "         code will be generated.\n");
#else
    if (chem != NULL)
    {
      chem->emit_cuda(out_name, emit_experiment);
    }
    else
      fprintf(stderr,"WARNING: no chem file was specified, no GPU getrates will be generated\n");
#endif
    //if (unit->tran != NULL)
    //  unit->tran->emit_cuda("getcoeffs",emit_experiment);
  }
  if (vector_width > 0)
  {
    unit->fast_math = fast_math;
    unit->taylor_stages = taylor_stages;
    unit->use_shared = false;
    unit->threads_per_block = threads_per_block; // However many chunks there are, that is how many threads to make
    unit->points_per_thread = points_per_thread;
    unit->num_warps = 0;
    unit->use_atomics = use_atomics;
    unit->use_double2 = false;
    unit->load_balance = load_balance;
    unit->hybrid_layout = hybrid;
    unit->prefetch = (prefetch_level > -1);
    unit->prefetch_level = unsigned(prefetch_level > -1 ? prefetch_level : 0);
    if (vector_width == 2)
      unit->use_sse = true;
    else
      unit->use_sse = false; // avx instead
    if (max_tuning_iters > 0)
    {
      if (slicing_config != NULL)
        fprintf(stderr,"WARNING: Overwriting previous chunking config during tuning!\n");
      if (chunks == 0)
      {
        fprintf(stderr,"Did not specify max chunks for tuning!\n");
        return 1;
      }
      if (socket_config > 3)
      {
        fprintf(stderr,"ERROR: invalid socket config %d\n", socket_config);
        return 1;
      }
      unit->socket_config = socket_config; 
      unit->tune_cpu(max_tuning_iters, chunks);
      return 0;
    }
    if ((chunks > 0) || (slicing_config != NULL))
      unit->emit_vectorized_code(out_name, emit_experiment, mac_dumbness);
    else
      fprintf(stderr,"WARNING: Vectorized code generation specified without specifiying\n"
                     "         chunking factor, slicing config, or tuning.  No vectorized\n"
                     "         code will be generated.\n");
  }
  return 0;
}

#if 0
static void usage(const char *progname)
{
  fprintf(stderr,"Usage: %s [options] <input_file>\n", progname);
  fprintf(stderr,"  -n : produce dimensionalized code\n");
  fprintf(stderr,"  -h <therm> : specify thermodynamic input file\n");
  fprintf(stderr,"  -i <tran> : specify tran.asc input file\n");
  fprintf(stderr,"  -o <name> : give output files a name\n");
  fprintf(stderr,"  -b : iterate backwards (C++ style)\n");
  fprintf(stderr,"  -c <chunks> : number of reaction chunks\n");
  fprintf(stderr,"  -c <config_file> : alternatively provide a slicing config file\n");
  fprintf(stderr,"  -g : emit gpu code\n");
  fprintf(stderr,"  -f <stages> : fast gpu math or taylor series on cpu (specify number of stages)\n");
  fprintf(stdout,"  -s : use shared memory as scratch space\n");
  fprintf(stdout,"  -p <points> : number of points per thread for GPU code\n");
  fprintf(stdout,"  -t <threads> : threads per block for GPU code\n");
  fprintf(stdout,"  -m <iters> : perform magic tuning for gpu for maximum number of iterations\n");
  fprintf(stdout,"  -e <0-1> : emit experiment at the bottom of the cuda file for testing performance\n");
  fprintf(stdout,"             0 for single stream, 1 for seperate streams\n");
  fprintf(stdout,"  -w <warps> : target number of warps per SM on the GPU\n");
  fprintf(stdout,"  -u : generate an uber-kernel for the chunks\n");
  fprintf(stdout,"  -a : use atomics for the outputs\n");
  fprintf(stdout,"  -d : use 2X points for GPU threads to get 128-byte vector loads\n");
  fprintf(stdout,"  -l : load balance on CPU threads\n");
  fprintf(stdout,"  -y : do hybrid layout of data for better cache performance on the cpu\n");
  fprintf(stdout,"  -x : generate code for Mac OSX which is dumb because it doesn't support basic libraries\n");
  fprintf(stdout,"  -q <level> : do prefetching on the cpu threads to the specified cache level [0-2]\n");
  fprintf(stdout,"  -k <socket_config> : 0=one socket,one thread, 1=one socket,two threads, 2=two sockets,one thread, 3=two sockets, two threads\n");
}
#else
static void usage(const char *progname)
{
  fprintf(stderr,"Usage: %s [options] <input_file>\n", progname);
  fprintf(stderr,"  -nondim : produce dimensionalized code\n");
  fprintf(stderr,"  -therm <therm> : specify thermodynamic input file\n");
  fprintf(stderr,"  -tran <tran> : specify tran.asc or tran.dat input file\n");
  fprintf(stderr,"  -dir <directory> : target directory to find chem.inp, tran.dat, and therm.dat\n");
  fprintf(stderr,"  -o <name> : give output files a name\n");
  fprintf(stderr,"  -backward : iterate backwards (C++ style)\n");
  fprintf(stderr,"  -chunks <chunks> : number of reaction chunks\n");
  fprintf(stderr,"  -chunks <config_file> : alternatively provide a slicing config file\n");
  fprintf(stderr,"  -gpu : emit gpu code\n");
  fprintf(stderr,"  -fastmath <stages> : fast gpu math or taylor series on cpu (specify number of stages)\n");
  fprintf(stdout,"  -shared : use shared memory as scratch space\n");
  fprintf(stdout,"  -points <points> : number of points per thread for GPU code\n");
  fprintf(stdout,"  -threads <threads> : threads per block for GPU code\n");
  fprintf(stdout,"  -magic <iters> : perform magic tuning for gpu for maximum number of iterations\n");
  fprintf(stdout,"  -experiment <0-1> : emit experiment at the bottom of the cuda file for testing performance\n");
  fprintf(stdout,"             0 for single stream, 1 for seperate streams\n");
  fprintf(stdout,"  -warps <warps> : target number of warps per SM on the GPU\n");
  fprintf(stdout,"  -uber : generate an uber-kernel for the chunks\n");
  fprintf(stdout,"  -atomic : use atomics for the outputs\n");
  fprintf(stdout,"  -double : use 2X points for GPU threads to get 128-byte vector loads\n");
  fprintf(stdout,"  -vector <2,4> : vector width of 2 or 4 for SSE or AVX instructions\n");
  fprintf(stdout,"  -load : load balance on CPU threads\n");
  fprintf(stdout,"  -hybrid : do hybrid layout of data for better cache performance on the cpu\n");
  fprintf(stdout,"  -mac : generate code for Mac OSX which is dumb because it doesn't support basic libraries\n");
  fprintf(stdout,"  -prefetch <level> : do prefetching on the cpu threads to the specified cache level [0-2]\n");
  fprintf(stdout,"  -socket <socket_config> : 0=one socket,one thread, 1=one socket,two threads, 2=two sockets,one thread, 3=two sockets, two threads\n");
  fprintf(stdout,"  -tpp <threads_per_point> : for doing warp-specialized kernels for getcoeffs, specify the threads per point (suggested >= 8)\n");
  fprintf(stdout,"  -visc : put mole fractions in shared memory for viscosity (only works up to 96 species on Fermi and Kepler)\n");
  fprintf(stdout,"  -diff <block_h> <block_v> : blocking factors for fissioned diffusion kernel (have already been tuned for Kepler)\n");
  fprintf(stdout,"  -chem <file_name> : chemistry file input from the matlab code generator\n");
  fprintf(stdout,"  -granularity <gran> : reaction allocation granularity for bin-packing heuristic\n");
  fprintf(stdout,"  -nochem : don't emit chemistry\n");
  fprintf(stdout,"  -nophys : don't emit physics\n");
}
#endif
