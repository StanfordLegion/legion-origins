
#ifndef __S3DGEN_AST__
#define __S3DGEN_AST__

#include <cstdio>
#include <cassert>
#include <cstdlib>

#include <map>
#include <set>
#include <list>
#include <vector>

#include "code_gen.h"

// Helper stuff for non-dimensionalization
/*
 * Physical quantities (units).  Do not change.
 *   R (gas constant):  J/(kg-mol K)
 *   cal/gmole --> K :  J/cal * g/kg / (J / (kg-mol K)) == gmole K / cal
 *   kg-mol --> g-mol:  g-mol/kg-mol
 *   cm3 --> m3      :  m^3/cm^3
 */
#define R_GAS_CONST	(8314.51)
#define CAL_PER_GMOLE__TO__KELVIN  (4.184 * 1000. / R_GAS_CONST)
#define GMOL__TO__KGMOL  (1.0e-3)
#define KGMOL__TO__GMOL  (1.0e+3)
#define CM3__TO__M3      (1.0e-6)
#define M3__TO__CM3      (1.0e+6)

/*
 * Reference quantities.  Ensure consistency.
 *   temperature:   kelvin
 *   length:        meters
 *   mol. wt.:      kg/kg-mol
 *   pressure:      N/m^2  (atmospheric)
 *   density:       kg/m^3
 *   concentration: kg-mol/m^3
 *   reaction rate: kg/(m^3 s)
 */
#if 0
// These are the old reference temperatures migrated over from
// the cgetrates code generator
#define TEMPERATURE_REF	  (300.0)
#define LENGTH_REF	  (1.0e-2)
#define MOLAR_REF	  (28.0)
#define PRESSURE_REF	  (101325.0)
#define RGAS_REF	  (R_GAS_CONST / MOLAR_REF)
#define DENSITY_REF       (PRESSURE_REF / (RGAS_REF * TEMPERATURE_REF))
/* #define CONCENTRATION_REF (DENSITY_REF / MOLAR_REF) */
/* equivalent formulation, gets rid of MOLAR_REF cancellation: */
#define CONCENTRATION_REF (PRESSURE_REF / (R_GAS_CONST * TEMPERATURE_REF))
#define REACTION_RATE_REF (500.0)
#else
// These are the reference values from observed runs of S3D
#define MACH_NO           (0.001)
#define RE_REAL           (100.0)
#define PARAM_PR          (0.708)
#define G_REF             (1.4)
#define A_REF             (347.2) // m/s
#define T_0               (300.0) // K
#define RHO_REF           (1.1766)  // Kg/m3
#define LAMBDA_REF        (26.14e-3)
#define PARAM_RE          (RE_REAL / MACH_NO)
#define T_REF             (T_0 * (G_REF - 1))
#define CP_REF            ((A_REF * A_REF) / T_REF)
#define L_REF             (PARAM_PR * LAMBDA_REF / CP_REF * PARAM_RE / RHO_REF / A_REF)
#define MU_REF            (RHO_REF * L_REF * A_REF)
#define P_REF             ((A_REF * A_REF) * RHO_REF * 10.0)
#define TIME_REF          (L_REF / A_REF)
#define RR_REF            (L_REF * 1e6 / (RHO_REF * A_REF))
#define TEMPERATURE_REF   (T_REF)
#define LENGTH_REF        (L_REF)
#define PRESSURE_REF      (P_REF)
#define REACTION_RATE_REF (RR_REF)
#define GASCONST          (8.31451)
#define CONCENTRATION_REF (PRESSURE_REF / (GASCONST * TEMPERATURE_REF))
//#define MOLAR_REF         (R_GAS_CONST * T_REF * A_REF * TIME_REF * TIME_REF * TIME_REF / (RHO_REF * 1000.0))
#define MOLAR_REF         (1.0)
#define TCONV             ((G_REF - 1.0)*T_0)
#define DIFF_REF          (RHO_REF/TCONV/1.0e3)
#endif

class TranslationUnit;
class Element;
class Species;
class Reactant;
class Reaction;
class ThirdBody;
class Slicing;
class ReactionChunk;
class TranLib;

typedef std::set<Element*> ElementSet;
typedef std::set<Species*> SpeciesSet;
typedef std::vector<Species*> SpeciesVec;
typedef std::vector<Reactant*> ReactantVec;
typedef std::vector<Reaction*> ReactionVec;
typedef std::map<Species*,int> SpeciesCoeffMap;
typedef std::vector<ThirdBody*> ThirdBodyVec;

class Slicing {
public:
  Slicing(TranslationUnit *unit);
  ~Slicing(void);
public:
  unsigned get_next_chunk_color(void) const;
  void add_chunk(ReactionChunk *chunk);
  unsigned compute_cost(void) const;
  bool empty(void) const { return chunks.empty(); }
public:
  void emit_getrates_declaration(CodeOutStream &ost);
  void emit_vec_getrates_declaration(CodeOutStream &ost);
  void emit_macros(CodeOutStream &ost);
  void emit_cpp_getrates(CodeOutStream &ost);
  void emit_vec_getrates(CodeOutStream &ost);
  void emit_gpu_getrates(CodeOutStream &ost, bool uber_kernel);
  void emit_uber_getrates_declaration(CodeOutStream &ost);
  void emit_vector_getrates(CodeOutStream &ost);
public:
  void dump_slicing(const char *file_name);
  void load_slicing(const char *file_name);
private:
  friend class TranslationUnit;
  TranslationUnit *unit;
  std::vector<ReactionChunk*> chunks;
};

class TranslationUnit {
public:
  TranslationUnit(void);
  ~TranslationUnit(void);
public:
  Element* add_element(const char *name);
  Species* add_species(const char *name);
  Species* find_or_add_species(const char *name);
  Species* find_species(const char *name, bool must = true);
  Reactant* add_reactant(Species *spec, int coeff);
  Reaction* add_reaction(ReactantVec *lhs, bool reversible, ReactantVec *rhs, double a, double beta, double e);
  ThirdBody* add_third_body(void);
  ThirdBody* find_or_add_third_body(const std::map<Species*,double> &coeffs);
public:
  void set_pressure_species(const char *spec_name);
  void add_thb(Species *spec, double coef);
  void add_duplicate(void);
  void add_low(double a, double beta, double e);
  void add_troe3(double a, double t3, double t1);
  void add_troe4(double a, double t3, double t1, double t2);
  void add_sri3(double a, double b, double c);
  void add_sri5(double a, double b, double c, double d, double e);
  void add_hv(double wavelength);
  void add_rev(double a, double beta, double e);
  void add_lt(double b, double c);
  void add_rlt(double b, double c);
public:
  void check_species_coefficients(void) const;
  void check_species_masses(void) const;
  inline size_t get_num_species(void) const { return (species.size()-1/*ignore third body species*/); }
public:
  // Call this when parsing is finished so we can clean up the translation unit
  void post_parse(void);
  void non_dimensionalize(void);
  void emit_code(const char *file_name);
  void emit_cuda(const char *file_name, bool emit_experiment, 
                 bool seperate_streams, bool uber_kernel);
  void emit_vectorized_code(const char *file_name, bool emit_experiment, bool macosx);
  void print_summary(void) const;
  void chunkify(unsigned int chunks);
  void tune_gpu(unsigned int max_iters);
  void tune_cpu(unsigned int max_iters, unsigned int max_chunks);
  void load_config(const char *config_file_name);
public:
  void emit_numerical_constants(CodeOutStream &ost, bool needs_dln10 = true);
  void emit_temperature_dependent(CodeOutStream &ost);
private:
  // CPP code generation methods
  void emit_cpp_header_file(const char *header_name);
  void emit_cpp_source_file(const char *source_name, const char *header_name);
  void emit_cpp_macros(CodeOutStream &ost);
  void emit_global_variable_declarations(CodeOutStream &ost, bool external);
  void emit_cpp_chemkin_functions(CodeOutStream &ost);
  void emit_cpp_getrates(CodeOutStream &ost);
  void emit_vec_getrates(CodeOutStream &ost);
  void emit_cpp_getrates_declaration(CodeOutStream &ost);
  void emit_vec_getrates_declaration(CodeOutStream &ost);
  void emit_vec_getrates_impl(CodeOutStream &ost);
  void emit_chunked_getrates(CodeOutStream &ost);
  void emit_prefix_computation(CodeOutStream &ost);
  void emit_postfix_computation(CodeOutStream &ost);
  void emit_vec_prefix_computation(CodeOutStream &ost);
  void emit_vec_prefix_impl(CodeOutStream &ost);
  void emit_vec_postfix_computation(CodeOutStream &ost);
  void emit_vec_postfix_impl(CodeOutStream &ost);
  void emit_chunked_prefix_declaration(CodeOutStream &ost);
  void emit_chunked_postfix_declaration(CodeOutStream &ost);
  void emit_vec_chunked_prefix_declaration(CodeOutStream &ost);
  void emit_vec_chunked_postfix_declaration(CodeOutStream &ost);
  void emit_point_type(CodeOutStream &ost, unsigned dim);
  void emit_array_type(CodeOutStream &ost, unsigned dim);
private:
  // CUDA code generation methods
  void emit_cuda_source_file(const char *source_name, bool emit_experiment, 
                             bool seperate_streams, bool uber_kernel);
  void emit_gpu_assumptions(CodeOutStream &ost);
  void emit_gpu_atomic_operations(CodeOutStream &ost);
  void emit_cuda_experiment(CodeOutStream &ost, bool seperate_streams, bool uber_kernel);
private:
  // Vectorizable code generation
  void emit_vector_source_file(const char *source_name, bool emit_experiment, bool macosx);
  void emit_vector_atomic_operations(CodeOutStream &ost);
  void emit_vector_hybrid_type(CodeOutStream &ost);
  void emit_vector_experiment(CodeOutStream &ost, int num_threads, bool load_balance, bool macosx, Slicing *slice = NULL);
public:
  bool no_nondim;
  bool multi_temp;
  bool has_thermos;
  bool iterate_forwards;
public:
  // GPU options
  bool fast_math;
  bool use_shared;
  bool use_atomics;
  bool use_double2;
  // CPU options
  bool use_sse;
  bool load_balance;
  bool hybrid_layout;
  bool prefetch;
  unsigned threads_per_block;
  unsigned points_per_thread;
  unsigned num_warps;
  unsigned taylor_stages;
  unsigned prefetch_level;
  // Tunning options
  unsigned socket_config;
public:
  // Getcoeffs options for GPU
  unsigned threads_per_point; // NOT reciprocal of points_per_threads, for different problems
  unsigned visc_shared; // put the mole fractions in shared memory for viscosity too
  unsigned diff_block_v; // blocking factor for diffusion
  unsigned diff_block_h;
public:
  TranLib *tran; 
private:
  friend class Slicing;
  friend class ReactionChunk;
  friend class TranLib;
  ElementSet elements;
public:
  SpeciesSet species;
  SpeciesVec ordered_species; // screw it
public:
  ReactantVec all_reactants;
  ReactionVec all_reactions;
  ThirdBodyVec third_bodies;
  Slicing reaction_slicing;
private:
  // Used for parsing only
  bool pressure_dep;
  Species *pressure_spec;
  Reaction *current_reac;
};

class Element {
public:
  Element(const char *name, unsigned idx);
public:
  const unsigned idx;
  const char *name;
};

class Species {
public:
  Species(TranslationUnit *unit, const char *name, unsigned idx);
  ~Species(void);
public:
  void notify(Reaction* reac, unsigned reac_idx, int coeff);
  void emit_contributions(CodeOutStream &ost, IntensityCounter &cnt, bool vector);
  void emit_contributions(CodeOutStream &ost, bool vector, const std::vector<Reaction*> &filter, unsigned actual_idx);
public:
  void set_coefficients(double low, double high, double common, double *coeffs);
  bool coefficients_set(void) const;
public:
  void update_mass(const char *element_name, int element_count);
  bool is_mass_set(void) const { return mass_set; }
public:
  double get_common_temperature(void) const;
  // Sometimes we want the original coefficients (see s3dgen_parse.cc)
  // but most times we want the scaled versions, so if you want the 
  // original ones say so.
  double get_high_coefficient(unsigned index, bool orig = false) const;
  double get_low_coefficient(unsigned index, bool orig = false) const;
private:
  TranslationUnit *const unit;
public:
  const unsigned idx;
  const char *name;
  char *code_name;
  bool qss_species;
  double low_temp, high_temp, common_temp;
private:
  double *thermo_coefficients;
public:
  double mass_set;
  double molecular_mass;
  std::map<Reaction*,int/*coeff*/> reaction_contributions;
};

class Reactant {
public:
  Reactant(Species *spec, int coeff);
public:

public:
  Species *spec;
  int coeff;
};

class Reaction {
public:
  Reaction(unsigned id, TranslationUnit *unit, ReactantVec *lhs, bool reversible, ReactantVec *rhs, 
           double a, double beta, double e,
           bool pressure_dep, Species *pressure_species);
public:
  void add_thb(Species *spec, double coeff);
  void set_duplicate(void);
  void add_low(double a, double beta, double e);
  void add_troe3(double a, double t3, double t1);
  void add_troe4(double a, double t3, double t1, double t2);
  void add_sri3(double a, double b, double c);
  void add_sri5(double a, double b, double c, double d, double e);
  void add_hv(double wavelength);
  void add_rev(double a, double beta, double e);
  void add_lt(double b, double c);
  void add_rlt(double b, double c);
public:
  bool need_default_third_body(void) const;
  void find_third_body(void);
  bool is_duplicate(void) const;
  void check_duplicate_and_reverse(void);
  bool is_owned(void) const;
  bool matches(const Reaction *other) const;
  void add_to_duplicates(Reaction *other);
  void set_owner(Reaction *owner);
  void non_dimensionalize(void);
  void notify_species(unsigned reaction_idx);
public:
  void emit_cpp_reaction(CodeOutStream &ost, IntensityCounter &cnt, unsigned local_idx, bool indexing = true, bool vector = false);
  void emit_gpu_reaction(CodeOutStream &ost, unsigned local_idx, bool spec, bool twice, bool first,
                         const std::vector<Species*> &ordered_species);
  void emit_vector_reaction(CodeOutStream &ost, unsigned local_idx, bool species_centric,
                         const std::vector<Species*> &ordered_species);
  void emit_single_forward(CodeOutStream &ost, IntensityCounter &cnt, const char *prefix, bool first, bool vector);
  void arrhenius(CodeOutStream &ost, IntensityCounter &cnt, double prefix, double beta, double e);
public:
  void emit_reaction_comment(CodeOutStream &ost);
  void emit_forward_reaction_rate(CodeOutStream &ost, IntensityCounter &cnt, bool vector);
  void emit_reverse_reaction_rate(CodeOutStream &ost, IntensityCounter &cnt, bool indexing);
private:
  void emit_gpu_forward_reaction(CodeOutStream &ost, bool spec, bool twice, bool first);
  void emit_gpu_reverse_reaction(CodeOutStream &ost, unsigned local_idx, bool spec, bool twice, bool first,
                                 const std::vector<Species*> &ordered_species);
  void emit_gpu_single_forward(CodeOutStream &ost, bool first_invocation, const char *prefix, bool spec, bool twice, bool first);
  void gpu_arrhenius(CodeOutStream &ost, double prefix, double beta, double e, bool twice, bool first);
private:
 void emit_vector_forward_reaction(CodeOutStream &ost, bool species_centric);
 void emit_vector_reverse_reaction(CodeOutStream &ost, unsigned local_idx, bool species_centric,
                                   const std::vector<Species*> &ordered_species);
 void emit_vector_single_forward(CodeOutStream &ost, bool first_invocation, bool species_centric);
 void vector_arrhenius(CodeOutStream &ost, const char *var_name, bool needs_type, double prefix, double beta, double e);
private:
  // The index of this reaction in the translation unit's data structures
  const unsigned int idx;
public:
  unsigned int get_global_idx(void) const { return idx; }
  // The index of the reaction in emitted code, always holds that (reaction_idx <= idx)
  int reaction_idx;
  const SpeciesSet& get_species(void) const { return all_species; }
  bool has_species_coeff(Species *spec) const;
  void emit_species_coeff(CodeOutStream &ost, Species *spec, bool first) const;
  int get_species_coeff(Species *spec) const;
  bool needs_dln10(void) const { return (low.enabled && (troe.num > 0)); }
public:
  friend class ReactionChunk;
  TranslationUnit *const unit;
  double a, beta, e;
  bool third_body, reversible, pressure_dep;
  Species *pressure_species;
  std::map<Species*,double> thb_values; // Used only in parsing, will go away when we common sub-expression eliminate
  ThirdBody       *thb;
  SpeciesCoeffMap stoich; /* stoichometry */
  SpeciesCoeffMap forward; /* forward atomic rate */
  SpeciesCoeffMap backward; /* backward atomic rate */
  SpeciesSet      all_species; /* list of species mentioned in the reaction at all */
  struct {
    bool enabled;
    bool owner;
    // Either this reaction is an owner in which case we keep a list of the duplicates
    // or it is one of the duplicates in which case we remember the owner of this duplicate
    union {
      Reaction *owner;
      std::list<Reaction*> *duplicates;
    } ptr;
  } duplicate;
  struct {
    bool enabled;
    double a, beta, e;
  } low;
  struct {
    int num;
    double a, t3, t1, t2;
  } troe;
  struct {
    int num;
    double a, b, c, d, e;
  } sri;
  struct {
    bool enabled;
    double wavelength;
  } hv;
  struct {
    bool enabled;
    double a, beta, e;
  } rev;
  struct {
    bool enabled;
    double b, c;
  } lt;
  struct {
    bool enabled;
    double b, c;
  } rlt;
};

class ThirdBody {
public:
  ThirdBody(unsigned idx);
public:
  void initialize(const std::map<Species*,double> &coeffs);
  bool matches(const std::map<Species*,double> &coeffs) const;
public:
  void emit_concentration(CodeOutStream &ost, IntensityCounter &cnt, const char *target, 
                    const char *molar_conc, const char *total_conc, bool vector = false) const;
public:
  const unsigned idx;
public:
  std::map<Species*,double> components;
};

class ReactionChunk {
public:
  ReactionChunk(unsigned idx, TranslationUnit *unit);
public:
  void add_reaction(Reaction *reaction);
  void compute_ordered_species(void);
  ReactionChunk* clone(void);
  bool compiles_to_registers(void);
public:
  // C++ code generation methods
  void emit_cpp_getrates(CodeOutStream &ost);
  void emit_vec_getrates(CodeOutStream &ost);
  void emit_vec_getrates_impl(CodeOutStream &ost);
  void emit_chunked_getrates_declaration(CodeOutStream &ost);
  void emit_vec_chunked_getrates_declaration(CodeOutStream &ost);
  void emit_gibbs_computation(CodeOutStream &ost);
public:
  // CUDA code generation methods
  void emit_gpu_getrates(CodeOutStream &ost, bool emit_header);
  void emit_gpu_getrates_declaration(CodeOutStream &ost); 
  void emit_gpu_gibbs(CodeOutStream &ost, bool spec, bool twice);
  void emit_gpu_reactions(CodeOutStream &ost, bool spec, bool twice);
  void emit_gpu_contributions(CodeOutStream &ost, bool spec, bool twice);
  void emit_gpu_arguments(CodeOutStream &ost);
public:
  // Vectorized code generation methods
  void emit_vector_getrates(CodeOutStream &ost);
  void emit_vector_getrates_declaration(CodeOutStream &ost);
  void emit_vector_getrates_impl(CodeOutStream &ost);
  void emit_vector_gibbs(CodeOutStream &ost, bool species_centric);
  void emit_vector_reactions(CodeOutStream &ost, bool species_centric);
  void emit_vector_contributions(CodeOutStream &ost, bool species_centric);
  void emit_vector_arguments(CodeOutStream &ost, bool needs_prefix = false);
  void emit_vector_gibbs_recurse(CodeOutStream &ost, Species *spec, int offset, int mask);
  void emit_vector_gibbs_sse_impl(CodeOutStream &ost, Species *spec, int mask);
  void emit_vector_gibbs_avx_impl(CodeOutStream &ost, Species *spec, int mask);
public:
  // Helper methods for tuning
  unsigned get_total_species_count(void) const { return species.size(); }
  const std::vector<Reaction*>& get_reactions(void) const { return reactions; }
  const std::set<Species*>& get_species(void) const { return species; }
private:
  friend class TranslationUnit;
  friend class Slicing;
  friend class TranLib;
  const unsigned int idx;
  TranslationUnit *const unit;
  std::set<Species*> species;
  std::vector<Reaction*> reactions;
  std::vector<Species*> ordered_species;
  std::set<unsigned> third_bodies;
};

class TranLib {
public:
  TranLib(int order, int num_species, int nlite, double patmos, TranslationUnit *unit);
public:
  void emit_code(const char *out_name);
  void emit_cuda(const char *out_name, bool emit_experiment);
  void emit_cpp_header_file(const char *file_name);
  void emit_cpp_source_file(const char *file_name, const char *header_file);
public:
  // Baseline code generation
  void emit_base_conductivity(CodeOutStream &ost);
  void emit_base_conductivity_declaration(CodeOutStream &ost);
  void emit_base_viscosity(CodeOutStream &ost);
  void emit_base_viscosity_declaration(CodeOutStream &ost);
  void emit_base_diffusion(CodeOutStream &ost);
  void emit_base_diffusion_declaration(CodeOutStream &ost);
  void emit_base_thermal(CodeOutStream &ost);
  void emit_base_thermal_declaration(CodeOutStream &ost);
public:
  // CUDA code generation
  void emit_cuda_source_file(const char *file_name, bool emit_experiment);
  void emit_cuda_conductivity(CodeOutStream &ost);
  void emit_cuda_conductivity_declaration(CodeOutStream &ost, bool bounds);
  void emit_cuda_viscosity(CodeOutStream &ost);
  void emit_cuda_viscosity_declaration(CodeOutStream &ost, bool bounds);
  void emit_cuda_diffusion(CodeOutStream &ost);
  void emit_cuda_diffusion_declaration(CodeOutStream &ost, bool bounds);
  void emit_cuda_thermal(CodeOutStream &ost);
  void emit_cuda_thermal_declaration(CodeOutStream &ost, bool bounds);
  void emit_cuda_experiments(CodeOutStream &ost);
  void emit_cuda_experiment(CodeOutStream &ost, const char *name, bool pressure, 
                            bool full_out, bool warp_specialized, unsigned num_kernels = 1);
public:
  // Warp-specialized versions of CUDA code
  void emit_specialized_conductivity(CodeOutStream &ost, unsigned max_specs, bool uniform);
  void emit_specialized_conductivity_declaration(CodeOutStream &ost);
  void emit_specialized_conductivity_loads(CodeOutStream &ost, unsigned index, unsigned max_specs, bool uniform);
  void emit_specialized_conductivity_step(CodeOutStream &ost, unsigned index, unsigned max_specs, bool uniform, bool next_loads);
  void emit_specialized_viscosity(CodeOutStream &ost, std::vector<std::map<Species*,unsigned> > &cuts, unsigned max_specs, bool uniform);
  void emit_specialized_viscosity_declaration(CodeOutStream &ost, bool bounds);
  void emit_sliced_viscosity(CodeOutStream &ost, std::vector<std::map<Species*,unsigned> > &cuts, unsigned max_specs, bool uniform);
  void emit_sliced_viscosity(CodeOutStream &ost, bool bounds);
  void emit_large_sliced_viscosity(CodeOutStream &ost, unsigned max_specs, bool uniform);
  void emit_large_sliced_viscosity_declaration(CodeOutStream &ost);
  void emit_specialized_diffusion(CodeOutStream &ost);
  void emit_specialized_diffusion_points(CodeOutStream &ost, unsigned max_specs, unsigned num_points, 
                                         bool uniform, unsigned offset, unsigned total_species, const char *coeff_matrix);
  void emit_specialized_diffusion_across(CodeOutStream &ost, unsigned full_specs, unsigned partial_specs,
                                         unsigned num_full_points, unsigned num_partial_points,
                                         unsigned total_species, const char *coeff_matrix,
                                         unsigned max_prefetch, bool fits_in_cache);
  void emit_specialized_diffusion_declaration(CodeOutStream &ost, bool bounds);
  void emit_large_sliced_diffusion(CodeOutStream &ost, unsigned max_specs, bool uniform);
  void emit_large_sliced_diffusion_declaration(CodeOutStream &ost);
public:
  void emit_blocked_diffusion(CodeOutStream &ost);
  unsigned emit_blocked_diffusion_kernel(CodeOutStream &ost, unsigned start_horz_spec, unsigned stop_horz_spec,
                                    unsigned kernel_idx, unsigned start_vert_spec, unsigned stop_vert_spec,
                                    bool compute_sums, bool do_output, bool initialize_h, bool initialize_v);
  void emit_blocked_species_load(CodeOutStream &ost, unsigned start_spec, unsigned stop_spec, 
                                                     const char *src_name, const char *dst_name);
  void emit_blocked_species_store(CodeOutStream &ost, unsigned start_spec, unsigned stop_spec,
                                                      const char *src_name, const char *dst_name);
  void emit_blocked_diffusion_declaration(CodeOutStream &ost, unsigned kernel_idx);
public:
  bool is_diffusion_symmetric(void) const;
public:
  int order, num_species, nlite;
  double patmos;
  TranslationUnit *const unit;
  double *nwt; // molecular weights for the species (num_species)
  double *neps; // epsilon/k well depth for the species (num_species)
  double *nsig; // collision diameter for the species (num_species)
  double *ndip; // dipole moments for the species (num_species)
  double *npol; // polarizabilities for the species (num_species)
  double *nzrot; // rotational relaxation collision numbers (num_species)
  int    *inlin; // indicators for molecule linearity
  double *nlam; // fit coefficients for conductivity (num_speces * order)
  double *neta; // fit coefficients for viscosity (num_species * order)
  double *ndif; // fit coefficients for diffusion (num_species * num_species * order)
  int    *iktdif; // species indicies for the light species (nlite)
  double *ntdif; // fit coefficients for thermal diffiusion ratio (num_species * order * nlite)
public:
  unsigned diffusion_kernels; // number of diffusion kernels generated
private:
  unsigned diffusion_depth; // for code generation only
};

#endif // __S3DGEN_AST__

