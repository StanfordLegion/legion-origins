
#ifndef __CODE_GEN_H__
#define __CODE_GEN_H__

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <cstdarg>

#define DEFAULT_LINE_LENGTH 1024

class CodeOutStream {
public:
  CodeOutStream(const char *file_name, bool bound_line_length = false, 
                unsigned max_chars = 80, unsigned indent_length = 2);
  ~CodeOutStream(void);
public:
  inline void up(void) { assert(depth > 0); depth--; print_single_indent(false); }
  inline void down(void) { depth++; print_single_indent(true); } 
public:
  // C style of doing things
  void print(const char *fmt, ...)
  {
    char buffer[1000];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, 999, fmt, args);
    va_end(args);
    print_while_inserting_indentation(buffer);
  }
  void println(const char *fmt, ...)
  {
    char buffer[1000];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, 998, fmt, args);
    va_end(args);
    strcat(buffer,"\n");
    print_while_inserting_indentation(buffer);
  }
public:
  // C++ style of doing things
  virtual CodeOutStream& operator<<(const char *message);
  virtual CodeOutStream& operator<<(char val);
  virtual CodeOutStream& operator<<(bool val);
  virtual CodeOutStream& operator<<(int val);
  virtual CodeOutStream& operator<<(unsigned int val);
  virtual CodeOutStream& operator<<(long val);
  virtual CodeOutStream& operator<<(unsigned long val);
  virtual CodeOutStream& operator<<(float val);
  virtual CodeOutStream& operator<<(double val);
private:
  void print_single_indent(bool add = true);
  void print_while_inserting_indentation(const char *message);
  void flush_line_buffer(void);
  void resize_buffer(void);
private:
  void clean_floating_point_buffer(char *buffer);
private:
  const char *file_name;
  FILE *target;
  unsigned int buffer_length;
  char *line_buffer;
  unsigned depth;
  unsigned line_index;
  const unsigned indent_length;
  const bool bound_lines;
  const unsigned max_line_length; // in terms of number of chars
};

class PairDelim {
public:
  PairDelim(CodeOutStream &ost);
  PairDelim(CodeOutStream &ost, const char *message);
  PairDelim(CodeOutStream &ost, const char *message, const char *open, const char *close);
  ~PairDelim(void);
private:
  CodeOutStream &ost;
  const char *close;
};

class IntensityCounter {
public:
  IntensityCounter(size_t data_size = 8);
public:
  void summarize(CodeOutStream &ost, const char *name);
public:
  void add_load(unsigned cnt = 1);
  void add_flop(unsigned cnt = 1);
private:
  size_t elmt_size;
  unsigned num_loads;
  unsigned num_flops;
};

#endif // __CODE_GEN_H__

