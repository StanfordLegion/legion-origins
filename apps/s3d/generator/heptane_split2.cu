
        if (wid < 14)
        {
          asm volatile("bar.arrive 1,512;" : : : "memory");
        }
        else if (wid == 14)
        {
          double den;
          asm volatile("bar.sync 1,512;" : : : "memory");
          // 7 -> 14
          double a14_15;
          {
            /* a14_0 = a14_0 + a14_7 * a7_0; */
            scratch[AVAL_OFFSET+21][tid] += 
              (scratch[AVAL_OFFSET+23][tid] * 
              scratch[AVAL_OFFSET+15][tid]); 
            /* den = 1 - a7_14*a14_7; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+16][tid] * 
              scratch[AVAL_OFFSET+23][tid]); 
            /* a14_15 = a14_7 * a7_15; */
            a14_15 = scratch[AVAL_OFFSET+23][tid] * 
              scratch[AVAL_OFFSET+17][tid]; 
            /* a14_0 = a14_0/den; */
            scratch[AVAL_OFFSET+21][tid] *= den;
            /* a14_2 = a14_2/den; */
            scratch[AVAL_OFFSET+22][tid] *= den;
            /* a14_15 = a14_15/den; */
            a14_15 *= den;
          }

          // 7 -> 15
          double a15_14;
          {
            /* a15_0 = a15_0 + a15_7 * a7_0; */
            scratch[AVAL_OFFSET+24][tid] += 
              (scratch[AVAL_OFFSET+26][tid] * 
              scratch[AVAL_OFFSET+15][tid]); 
            /* den = 1 - a7_15*a15_7; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+17][tid] * 
              scratch[AVAL_OFFSET+26][tid]); 
            /* a15_14 = a15_7 * a7_14; */
            a15_14 = scratch[AVAL_OFFSET+26][tid] * 
              scratch[AVAL_OFFSET+16][tid]; 
            /* a15_0 = a15_0/den; */
            scratch[AVAL_OFFSET+24][tid] *= den;
            /* a15_2 = a15_2/den; */
            scratch[AVAL_OFFSET+25][tid] *= den;
            /* a15_14 = a15_14/den; */
            a15_14 *= den;
          }

          // 1 -> 2
          double a2_3;
          {
            /* a2_0 = a2_0 + a2_1 * a1_0; */
            scratch[AVAL_OFFSET+5][tid] += 
              (scratch[AVAL_OFFSET+6][tid] * 
              scratch[AVAL_OFFSET+9][tid]); 
            /* den = 1 - a1_2*a2_1; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+10][tid] * 
              scratch[AVAL_OFFSET+6][tid]); 
            /* a2_3 = a2_1 * a1_3; */
            a2_3 = scratch[AVAL_OFFSET+6][tid] * 
              scratch[AVAL_OFFSET+11][tid]; 
            /* a2_0 = a2_0/den; */
            scratch[AVAL_OFFSET+5][tid] *= den;
            /* a2_3 = a2_3/den; */
            a2_3 *= den;
            /* a2_14 = a2_14/den; */
            scratch[AVAL_OFFSET+7][tid] *= den;
            /* a2_15 = a2_15/den; */
            scratch[AVAL_OFFSET+8][tid] *= den;
          }

          // 15 -> 2 
          {
            /* a2_0 = a2_0 + a2_15 * a15_0; */
            scratch[AVAL_OFFSET+5][tid] += 
              (scratch[AVAL_OFFSET+8][tid] * 
              scratch[AVAL_OFFSET+24][tid]); 
            /* den = 1 - a15_2*a2_15; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+25][tid] * 
              scratch[AVAL_OFFSET+8][tid]); 
            /* a2_14 = a2_14 + a2_15 * a15_14; */
        scratch[AVAL_OFFSET+7][tid] = scratch[AVAL_OFFSET+7][tid] 
              + scratch[AVAL_OFFSET+8][tid] * a15_14; 
            /* a2_0 = a2_0/den; */
            scratch[AVAL_OFFSET+5][tid] *= den;
            /* a2_3 = a2_3/den; */
            a2_3 *= den;
            /* a2_14 = a2_14/den; */
            scratch[AVAL_OFFSET+7][tid] *= den;
          }

          // 15 -> 14
          {
            /* a14_0 = a14_0 + a14_15 * a15_0; */
            scratch[AVAL_OFFSET+21][tid] += (a14_15 * 
              scratch[AVAL_OFFSET+24][tid]); 
            /* den = 1 - a15_14*a14_15; */
            den = 1.0/(1.0 - a15_14 * a14_15);
            /* a14_2 = a14_2 + a14_15 * a15_2; */
            scratch[AVAL_OFFSET+22][tid] = 
              scratch[AVAL_OFFSET+22][tid] + a14_15 * 
              scratch[AVAL_OFFSET+25][tid]; 
            /* a14_0 = a14_0/den; */
            scratch[AVAL_OFFSET+21][tid] *= den;
            /* a14_2 = a14_2/den; */
            scratch[AVAL_OFFSET+22][tid] *= den;
          }

          // 14 -> 2
          {
            /* a2_0 = a2_0 + a2_14 * a14_0; */
            scratch[AVAL_OFFSET+5][tid] += 
              (scratch[AVAL_OFFSET+7][tid] * 
              scratch[AVAL_OFFSET+21][tid]); 
            /* den = 1 - a14_2*a2_14; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+22][tid] * 
              scratch[AVAL_OFFSET+7][tid]); 
            /* a2_0 = a2_0/den; */
            scratch[AVAL_OFFSET+5][tid] *= den;
            /* a2_3 = a2_3/den; */
            a2_3 *= den;
            scratch[AVAL_OFFSET+33][tid] = a2_3;
          }

          // Indicate finished with 2
          asm volatile("bar.arrive 2,64;" : : : "memory");

          // Wait for xq_3
          asm volatile("bar.sync 3,64;" : : : "memory");

          // xq_2
          /* xq_2 = a2_0 + a2_3*xq_3; */
          scratch[NUM_SPECIES+1][tid] = scratch[AVAL_OFFSET+5][tid] + 
            scratch[AVAL_OFFSET+33][tid] * scratch[NUM_SPECIES+2][tid];

          // xq_14
          /* xq_14 = a14_0 + a14_2*xq_2; */
          scratch[NUM_SPECIES+13][tid] = scratch[AVAL_OFFSET+21][tid] + 
            scratch[AVAL_OFFSET+22][tid]*scratch[NUM_SPECIES+1][tid];

          // xq_15
          /* xq_15 = a15_0 + a15_2*xq_2 + a15_14*xq_14; */
          scratch[NUM_SPECIES+14][tid] = scratch[AVAL_OFFSET+24][tid] + 
            scratch[AVAL_OFFSET+25][tid]*scratch[NUM_SPECIES+1][tid] + 
            a15_14*scratch[NUM_SPECIES+13][tid];

          // xq_1
          /* xq_1 = a1_0 + a1_2*xq_2 + a1_3*xq_3; */
          scratch[NUM_SPECIES+0][tid] = scratch[AVAL_OFFSET+9][tid] + 
            scratch[AVAL_OFFSET+10][tid]*scratch[NUM_SPECIES+1][tid] + 
            scratch[AVAL_OFFSET+11][tid]*scratch[NUM_SPECIES+2][tid];

          // xq_7
          /* xq_7 = a7_0 + a7_14*xq_14 + a7_15*xq_15; */
          scratch[NUM_SPECIES+6][tid] = scratch[AVAL_OFFSET+15][tid] + 
            scratch[AVAL_OFFSET+16][tid]*scratch[NUM_SPECIES+13][tid] + 
            scratch[AVAL_OFFSET+17][tid]*scratch[NUM_SPECIES+14][tid];
        }
        else // wid == 15
        {
          double den;
          asm volatile("bar.sync 1,512;" : : : "memory");
          // 11 -> 3
          {
            /* a3_0 = a3_0 + a3_11 * a11_0; */
            scratch[AVAL_OFFSET+0][tid] += 
              (scratch[AVAL_OFFSET+4][tid] * 
              scratch[AVAL_OFFSET+31][tid]); 
            /* den = 1 - a11_3*a3_11; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+32][tid] * 
              scratch[AVAL_OFFSET+4][tid]); 
            /* a3_0 = a3_0/den; */
            scratch[AVAL_OFFSET+0][tid] *= den;
            /* a3_6 = a3_6/den; */
            scratch[AVAL_OFFSET+2][tid] *= den;
            /* a3_1 = a3_1/den; */
            scratch[AVAL_OFFSET+1][tid] *= den;
            /* a3_10 = a3_10/den; */
            scratch[AVAL_OFFSET+3][tid] *= den;
          }

          // 10 -> 3
          {
            /* a3_0 = a3_0 + a3_10 * a10_0; */
            scratch[AVAL_OFFSET+0][tid] += 
              (scratch[AVAL_OFFSET+3][tid] * 
              scratch[AVAL_OFFSET+29][tid]); 
            /* den = 1 - a10_3*a3_10; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+30][tid] * 
              scratch[AVAL_OFFSET+3][tid]); 
            /* a3_0 = a3_0/den; */
            scratch[AVAL_OFFSET+0][tid] *= den;
            /* a3_6 = a3_6/den; */
            scratch[AVAL_OFFSET+2][tid] *= den;
            /* a3_1 = a3_1/den; */
            scratch[AVAL_OFFSET+1][tid] *= den;
          }

          // 5 -> 9
          {
            /* a9_0 = a9_0 + a9_5 * a5_0; */
            scratch[AVAL_OFFSET+18][tid] += 
              (scratch[AVAL_OFFSET+19][tid] * 
              scratch[AVAL_OFFSET+27][tid]); 
            /* den = 1 - a5_9*a9_5; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+28][tid] * 
              scratch[AVAL_OFFSET+19][tid]); 
            /* a9_0 = a9_0/den; */
            scratch[AVAL_OFFSET+18][tid] *= den;
            /* a9_6 = a9_6/den; */
            scratch[AVAL_OFFSET+20][tid] *= den;
          }

          // 1 -> 3
          double a3_2;
          {
            /* a3_0 = a3_0 + a3_1 * a1_0; */
            scratch[AVAL_OFFSET+0][tid] += 
              (scratch[AVAL_OFFSET+1][tid] * 
              scratch[AVAL_OFFSET+9][tid]); 
            /* den = 1 - a1_3*a3_1; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+11][tid] * 
              scratch[AVAL_OFFSET+1][tid]); 
            /* a3_2 = a3_1 * a1_2; */
            a3_2 = scratch[AVAL_OFFSET+1][tid] * 
              scratch[AVAL_OFFSET+10][tid]; 
            /* a3_0 = a3_0/den; */
            scratch[AVAL_OFFSET+0][tid] *= den;
            /* a3_2 = a3_2/den; */
            a3_2 *= den;
            /* a3_6 = a3_6/den; */
            scratch[AVAL_OFFSET+2][tid] *= den;
          }

          // 6 -> 3
          double a3_9;
          {
            /* a3_0 = a3_0 + a3_6 * a6_0; */
            scratch[AVAL_OFFSET+0][tid] += 
              (scratch[AVAL_OFFSET+2][tid] * 
              scratch[AVAL_OFFSET+12][tid]); 
            /* den = 1 - a6_3*a3_6; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+13][tid] * 
              scratch[AVAL_OFFSET+2][tid]); 
            /* a3_9 = a3_6 * a6_9; */
            a3_9 = scratch[AVAL_OFFSET+2][tid] * 
              scratch[AVAL_OFFSET+14][tid]; 
            /* a3_0 = a3_0/den; */
            scratch[AVAL_OFFSET+0][tid] *= den;
            /* a3_2 = a3_2/den; */
            a3_2 *= den;
            /* a3_9 = a3_9/den; */
            a3_9 *= den;
          }

          // 6 -> 9
          double a9_3;
          {
            /* a9_0 = a9_0 + a9_6 * a6_0; */
            scratch[AVAL_OFFSET+18][tid] += 
              (scratch[AVAL_OFFSET+20][tid] * 
              scratch[AVAL_OFFSET+12][tid]); 
            /* den = 1 - a6_9*a9_6; */
            den = 1.0/(1.0 - scratch[AVAL_OFFSET+14][tid] * 
              scratch[AVAL_OFFSET+20][tid]); 
            /* a9_3 = a9_6 * a6_3; */
            a9_3 = scratch[AVAL_OFFSET+20][tid] * 
              scratch[AVAL_OFFSET+13][tid]; 
            /* a9_0 = a9_0/den; */
            scratch[AVAL_OFFSET+18][tid] *= den;
            /* a9_3 = a9_3/den; */
            a9_3 *= den;
          }

          // 9 -> 3
          {
            /* a3_0 = a3_0 + a3_9 * a9_0; */
            scratch[AVAL_OFFSET+0][tid] += (a3_9 * 
              scratch[AVAL_OFFSET+18][tid]); 
            /* den = 1 - a9_3*a3_9; */
            den = 1.0/(1.0 - a9_3 * a3_9);
            /* a3_0 = a3_0/den; */
            scratch[AVAL_OFFSET+0][tid] *= den;
            /* a3_2 = a3_2/den; */
            a3_2 *= den;
          }

          // Wait for 2
          asm volatile("bar.sync 2,64;" : : : "memory");

          // 2 -> 3
          double a3_0;
          {
            /* a3_0 = a3_0 + a3_2 * a2_0; */
            a3_0 = scratch[AVAL_OFFSET+0][tid] + (a3_2 * 
              scratch[AVAL_OFFSET+5][tid]); 
            /* den = 1 - a2_3*a3_2; */
            den = 1.0 - scratch[AVAL_OFFSET+33][tid] * a3_2;
            /* a3_0 = a3_0/den; */
            a3_0 /= den;
          }

          // xq_3
          /* xq_3 = a3_0; */
          scratch[NUM_SPECIES+2][tid] = a3_0;
          
          // Signal xq_3
          asm volatile("bar.arrive 3,64;" : : : "memory");

          // xq_9
          /* xq_9 = a9_0 + a9_3*xq_3; */
          scratch[NUM_SPECIES+8][tid] = scratch[AVAL_OFFSET+18][tid] + 
            a9_3*scratch[NUM_SPECIES+2][tid];

          // xq_6
          /* xq_6 = a6_0 + a6_3*xq_3 + a6_9*xq_9; */
          scratch[NUM_SPECIES+5][tid] = scratch[AVAL_OFFSET+12][tid] + 
            scratch[AVAL_OFFSET+13][tid]*scratch[NUM_SPECIES+2][tid] + 
            scratch[AVAL_OFFSET+14][tid]*scratch[NUM_SPECIES+8][tid];

          // xq_5
          /* xq_5 = a5_0 + a5_9*xq_9; */
          scratch[NUM_SPECIES+4][tid] = scratch[AVAL_OFFSET+27][tid] + 
            scratch[AVAL_OFFSET+28][tid]*scratch[NUM_SPECIES+8][tid];

          // xq_10
          /* xq_10 = a10_0 + a10_3*xq_3; */
          scratch[NUM_SPECIES+9][tid] = scratch[AVAL_OFFSET+29][tid] + 
            scratch[AVAL_OFFSET+30][tid]*scratch[NUM_SPECIES+2][tid];

          // xq_11
          /* xq_11 = a11_0 + a11_3*xq_3; */
          scratch[NUM_SPECIES+10][tid] = scratch[AVAL_OFFSET+31][tid] + 
            scratch[AVAL_OFFSET+32][tid]*scratch[NUM_SPECIES+2][tid];
        }
