
/*
 * Fermi tuned thermal kernel for PRF.
 * Gets 4 CTAs with 4 warps per CTA.
 * Completely memory bandwidth limited.
 *
 * Can tune number of threads for each CTA
 * Probably want at least 4 warps per CTA
 * since max number of CTAs/SM is 8 on Fermi.
 *
 * Launch with (64-byte version):
 *  dim3 grid((nx*ny*nz)/128,1,1);
 *  dim3 block(128,1,1);
 *
 * gpu_thermal<<<grid,block>>>(temperature_ptr,  
 *                 mole_frac_input_ptr, nx*ny, nx, nz, nx*ny*nz, output_d);
 *
 * Performance numbers on C2070 with 14 SMs with 6 memory partitions:
 * 
 * 32x32x32 (threads=128) 
 *   Latency: 1.682 ms
 *   Throughput: 19.487 Mpoints/s
 *
 * 64x64x64 (threads=128)
 *   Latency: 10.760 ms
 *   Throughput: 24.364 Mpoints/s
 *
 * 128x128x128 (threads=128)
 *   Latency: 89.084 ms
 *   Throughput: 23.541 Mpoints/
 *
 * Generation command:
 *   ./singe --dir inputs/PRF/ --cuda
 *
 */

__constant__ double molecular_masses[116] = {1.00797, 2.01594, 15.9994, 31.9988, 
  17.00737, 18.01534, 33.00677, 34.01474, 28.01055, 44.00995, 30.02649, 
  29.01852, 46.02589, 32.04243, 48.04183, 47.03386, 16.04303, 15.03506, 
  30.07012, 29.06215, 28.05418, 27.04621, 26.03824, 44.05358, 43.04561, 
  42.03764, 41.02967, 75.04441, 62.06892000000001, 61.06095000000001, 58.08067, 
  89.0715, 90.07947, 56.06473, 55.05676, 58.08067, 42.08127, 41.0733, 40.06533, 
  40.06533, 39.05736, 75.08804000000001, 75.08804000000001, 56.06473, 56.10836, 
  57.11633, 55.10039, 54.09242, 89.11513000000001, 71.09979, 72.10776, 56.10836, 
  55.10039, 89.11513000000001, 89.11513000000001, 72.10776, 71.09979, 121.11393, 
  104.10656, 71.09979, 70.09182, 69.08385, 88.10716000000001, 103.09859, 
  70.13545000000001, 69.12748000000001, 69.12748000000001, 117.16931, 86.13485, 
  100.20557, 98.18963000000001, 98.18963000000001, 98.18963000000001, 
  97.18166000000001, 131.1964, 131.1964, 131.1964, 131.1964, 163.1952, 163.1952, 
  163.1952, 163.1952, 163.1952, 114.18903, 114.18903, 114.18903, 146.18783, 
  88.10716000000001, 57.0727, 55.10039, 98.18963000000001, 98.18963000000001, 
  97.18166000000001, 97.18166000000001, 131.1964, 82.14660000000001, 
  81.13863000000001, 81.13863000000001, 71.14342000000001, 86.13485, 114.23266, 
  112.21672, 112.21672, 145.22349, 145.22349, 128.21612, 128.21612, 160.21492, 
  177.22229, 177.22229, 145.22349, 160.21492, 163.1952, 163.1952, 146.18783, 
  28.0134}; 

__constant__ double recip_molecular_masses[116] = {0.9920930186414277, 
  0.4960465093207139, 0.06250234383789392, 0.03125117191894696, 
  0.05879803873262004, 0.05550825019122593, 0.03029681486555637, 
  0.02939901936631002, 0.03570083414998991, 0.02272213442641948, 
  0.03330392596670473, 0.03446075127194632, 0.02172690196756651, 
  0.03120861932131863, 0.02081519375927187, 0.021261278576753, 
  0.06233236489615739, 0.06651120780362699, 0.03325560390181349, 
  0.0344090165386938, 0.03564531203549703, 0.0369737571363973, 
  0.0384050534905585, 0.02269963076780593, 0.02323117270262868, 
  0.02378820504671527, 0.02437260645771706, 0.01332544289441412, 
  0.01611112292593459, 0.01637707896781822, 0.01721743223692151, 
  0.01122693566404518, 0.01110130865556824, 0.01783652574443861, 
  0.01816307388956415, 0.01721743223692151, 0.02376354135699802, 
  0.02434671672351625, 0.02495923532889907, 0.02495923532889907, 
  0.02560336899370566, 0.01331770012907515, 0.01331770012907515, 
  0.01783652574443861, 0.01782265601774851, 0.01750812771058645, 
  0.01814869186951308, 0.01848687856819865, 0.01122143905305418, 
  0.01406473915042506, 0.01386813291662368, 0.01782265601774851, 
  0.01814869186951308, 0.01122143905305418, 0.01122143905305418, 
  0.01386813291662368, 0.01406473915042506, 8.256688557625038e-03, 
  9.605542628629742e-03, 0.01406473915042506, 0.01426700005792402, 
  0.01447516315318269, 0.01134981538390296, 9.699453697669386e-03, 
  0.01425812481419881, 0.01446602711396394, 0.01446602711396394, 
  8.534658094342281e-03, 0.01160970269292859, 9.979485172331238e-03, 
  0.01018437486728486, 0.01018437486728486, 0.01018437486728486, 
  0.01029000739439931, 7.622160364156333e-03, 7.622160364156333e-03, 
  7.622160364156333e-03, 7.622160364156333e-03, 6.127631204839358e-03, 
  6.127631204839358e-03, 6.127631204839358e-03, 6.127631204839358e-03, 
  6.127631204839358e-03, 8.757408658257278e-03, 8.757408658257278e-03, 
  8.757408658257278e-03, 6.840514699479429e-03, 0.01134981538390296, 
  0.01752151203640269, 0.01814869186951308, 0.01018437486728486, 
  0.01018437486728486, 0.01029000739439931, 0.01029000739439931, 
  7.622160364156333e-03, 0.01217335836175812, 0.01232458571213243, 
  0.01232458571213243, 0.01405611369259448, 0.01160970269292859, 
  8.754063855293223e-03, 8.911328008874257e-03, 8.911328008874257e-03, 
  6.885938356115805e-03, 6.885938356115805e-03, 7.799331316530245e-03, 
  7.799331316530245e-03, 6.241615949376e-03, 5.642631070843289e-03, 
  5.642631070843289e-03, 6.885938356115805e-03, 6.241615949376e-03, 
  6.127631204839358e-03, 6.127631204839358e-03, 6.840514699479429e-03, 
  0.03569720205330306}; 

__global__ void
__launch_bounds__(128,4)
gpu_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *mixmw_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *thermal_out) 
{
  {
    const int offset = (blockIdx.x*blockDim.x+threadIdx.x);
    temperature_array += offset;
    mass_frac_array += offset;
    mixmw_array += offset;
    thermal_out += offset;
  }
  // Load the temperatures
  double temperature;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  // Load the average mole weight
  double mixmw;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mixmw) : "l"(mixmw_array) : 
    "memory"); 
  double thermal[2];
  thermal[0] = 0.0;
  double mass_frac_light_0;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac_light_0) : 
    "l"(mass_frac_array+0*spec_stride) : "memory"); 
  thermal[1] = 0.0;
  double mass_frac_light_1;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac_light_1) : 
    "l"(mass_frac_array+1*spec_stride) : "memory"); 
  temperature *= 120.0;
  double mole_frac_light_0 = mass_frac_light_0 * recip_molecular_masses[0] * 1e3 
    * mixmw; 
  double mole_frac_light_1 = mass_frac_light_1 * recip_molecular_masses[1] * 1e3 
    * mixmw; 
  {
    double mole_frac = mole_frac_light_0;
    // No interaction for species 0 with itself
    {
      double val = 
        __fma_rn(-4.870919137829885e-12,temperature,2.934124701213393e-08); 
      val = __fma_rn(val,temperature,-5.464040220781073e-05);
      val = __fma_rn(val,temperature,-0.1525347421818075);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mole_frac = mole_frac_light_1;
    {
      double val = 
        __fma_rn(4.870919137829885e-12,temperature,-2.934124701213393e-08); 
      val = __fma_rn(val,temperature,5.464040220781073e-05);
      val = __fma_rn(val,temperature,0.1525347421818075);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    // No interaction for species 1 with itself
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+2*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[2] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(2.753212482584989e-11,temperature,-1.807447515293367e-07); 
      val = __fma_rn(val,temperature,3.615550927540215e-04);
      val = __fma_rn(val,temperature,0.2700101503763491);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.144144427154902e-12,temperature,-3.960219633370026e-09); 
      val = __fma_rn(val,temperature,1.097383985461379e-05);
      val = __fma_rn(val,temperature,0.4155833371210522);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+3*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[3] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(3.464704359017023e-11,temperature,-2.329279443546144e-07); 
      val = __fma_rn(val,temperature,4.801642880313139e-04);
      val = __fma_rn(val,temperature,0.2204828425102964);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(6.863234368775513e-12,temperature,-3.847680615982657e-08); 
      val = __fma_rn(val,temperature,7.117708177718757e-05);
      val = __fma_rn(val,temperature,0.442739083893085);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+4*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[4] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(2.773927219139813e-11,temperature,-1.821046465375425e-07); 
      val = __fma_rn(val,temperature,3.642753762569578e-04);
      val = __fma_rn(val,temperature,0.2720416641688923);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.1616241814762e-12,temperature,-4.020722192843019e-09); 
      val = __fma_rn(val,temperature,1.114149353544928e-05);
      val = __fma_rn(val,temperature,0.4219324434580685);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+5*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[5] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.029595022301048e-11,temperature,-3.06550003452475e-07); 
      val = __fma_rn(val,temperature,7.665588101246594e-04);
      val = __fma_rn(val,temperature,-0.1418837440685931);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.633898010835056e-11,temperature,-2.553727569929836e-07); 
      val = __fma_rn(val,temperature,5.615615459953126e-04);
      val = __fma_rn(val,temperature,0.06020313090654345);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+6*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[6] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(3.471383045051124e-11,temperature,-2.33376944456128e-07); 
      val = __fma_rn(val,temperature,4.810898696083402e-04);
      val = __fma_rn(val,temperature,0.2209078529955919);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(6.889796396703081e-12,temperature,-3.862571874897412e-08); 
      val = __fma_rn(val,temperature,7.145255067893005e-05);
      val = __fma_rn(val,temperature,0.4444525687136661);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+7*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[7] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(3.477677299659916e-11,temperature,-2.338000996911352e-07); 
      val = __fma_rn(val,temperature,4.819621738426258e-04);
      val = __fma_rn(val,temperature,0.2213083994791666);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(6.914872261877633e-12,temperature,-3.876629958182471e-08); 
      val = __fma_rn(val,temperature,7.171260691630459e-05);
      val = __fma_rn(val,temperature,0.4460701829431508);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+8*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[8] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(3.279735103970086e-11,temperature,-2.189517023536565e-07); 
      val = __fma_rn(val,temperature,4.471971790476808e-04);
      val = __fma_rn(val,temperature,0.2394099391222855);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.012897588533783e-12,temperature,-2.698208997043916e-08); 
      val = __fma_rn(val,temperature,5.06631704213602e-05);
      val = __fma_rn(val,temperature,0.4446536171810983);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+9*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[9] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.488286850043913e-11,temperature,-3.19718503863343e-07); 
      val = __fma_rn(val,temperature,7.182424984463018e-04);
      val = __fma_rn(val,temperature,0.02443693852824372);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.414664356171263e-11,temperature,-1.55290329600105e-07); 
      val = __fma_rn(val,temperature,3.03633410691677e-04);
      val = __fma_rn(val,temperature,0.325742449763764);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+10*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[10] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.335277618092613e-11,temperature,-3.257584801931525e-07); 
      val = __fma_rn(val,temperature,7.983875255094847e-04);
      val = __fma_rn(val,temperature,-0.1249393610711058);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.509853095463745e-11,temperature,-2.393931455067419e-07); 
      val = __fma_rn(val,temperature,5.034377422345768e-04);
      val = __fma_rn(val,temperature,0.1621503530989792);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+11*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[11] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.325167358752768e-11,temperature,-3.249987819668701e-07); 
      val = __fma_rn(val,temperature,7.965256136210947e-04);
      val = __fma_rn(val,temperature,-0.1246479911858392);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.493444239067435e-11,temperature,-2.382739625551938e-07); 
      val = __fma_rn(val,temperature,5.010841287379074e-04);
      val = __fma_rn(val,temperature,0.1613922866539616);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+12*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[12] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.520982659134082e-11,temperature,-3.360087012297871e-07); 
      val = __fma_rn(val,temperature,8.08852395097942e-04);
      val = __fma_rn(val,temperature,-0.1028743096173313);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.479416197849631e-11,temperature,-2.350128245613158e-07); 
      val = __fma_rn(val,temperature,4.875390950586673e-04);
      val = __fma_rn(val,temperature,0.2008740337688601);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+13*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[13] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.377077577885603e-11,temperature,-3.279817895510682e-07); 
      val = __fma_rn(val,temperature,8.001720724106142e-04);
      val = __fma_rn(val,temperature,-0.1195425287462414);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.493977141606181e-11,temperature,-2.377400529084644e-07); 
      val = __fma_rn(val,temperature,4.982675022704415e-04);
      val = __fma_rn(val,temperature,0.1709789533066371);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+14*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[14] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.46982121178071e-11,temperature,-3.34931226126757e-07); 
      val = __fma_rn(val,temperature,8.171265047724426e-04);
      val = __fma_rn(val,temperature,-0.1220754535856252);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.64393018603243e-11,temperature,-2.479432807118648e-07); 
      val = __fma_rn(val,temperature,5.196519377936141e-04);
      val = __fma_rn(val,temperature,0.1783169562591571);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+15*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[15] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.465801589946938e-11,temperature,-3.346300290977121e-07); 
      val = __fma_rn(val,temperature,8.163916790637816e-04);
      val = __fma_rn(val,temperature,-0.1219656735440195);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.637370458269565e-11,temperature,-2.474969383455001e-07); 
      val = __fma_rn(val,temperature,5.187164711218219e-04);
      val = __fma_rn(val,temperature,0.1779959537621703);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+16*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[16] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(3.646154536025681e-11,temperature,-2.501196515107643e-07); 
      val = __fma_rn(val,temperature,5.3034959983897e-04);
      val = __fma_rn(val,temperature,0.1454336980342498);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.090025974274926e-11,temperature,-6.537575117820058e-08); 
      val = __fma_rn(val,temperature,1.215250104692813e-04);
      val = __fma_rn(val,temperature,0.3586579089020671);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+17*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[17] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(3.638243462261528e-11,temperature,-2.498993298490946e-07); 
      val = __fma_rn(val,temperature,5.308790954638053e-04);
      val = __fma_rn(val,temperature,0.1401104863854205);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.103560161376326e-11,temperature,-6.6397517972414e-08); 
      val = __fma_rn(val,temperature,1.235850543398597e-04);
      val = __fma_rn(val,temperature,0.3502756491976494);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+18*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[18] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.401522158129584e-11,temperature,-3.138463983827954e-07); 
      val = __fma_rn(val,temperature,7.061593176529384e-04);
      val = __fma_rn(val,temperature,0.02061862296971059);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.341246542944883e-11,temperature,-1.50778930519446e-07); 
      val = __fma_rn(val,temperature,2.952383782939097e-04);
      val = __fma_rn(val,temperature,0.3094369822364394);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+19*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[19] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.39128769183801e-11,temperature,-3.131166393881604e-07); 
      val = __fma_rn(val,temperature,7.045173484719697e-04);
      val = __fma_rn(val,temperature,0.02057068032755605);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.330333384661689e-11,temperature,-1.500761107589693e-07); 
      val = __fma_rn(val,temperature,2.938621955235372e-04);
      val = __fma_rn(val,temperature,0.3079946160849567);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+20*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[20] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.359697990261612e-11,temperature,-3.100619307754527e-07); 
      val = __fma_rn(val,temperature,6.947741997709393e-04);
      val = __fma_rn(val,temperature,0.02918725115574974);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.24737607112918e-11,temperature,-1.441992162602128e-07); 
      val = __fma_rn(val,temperature,2.812854689610914e-04);
      val = __fma_rn(val,temperature,0.3135046618706612);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+21*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[21] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.400635795990381e-11,temperature,-3.15298242275639e-07); 
      val = __fma_rn(val,temperature,7.149289365551509e-04);
      val = __fma_rn(val,temperature,4.602791312288912e-03);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.43600420156161e-11,temperature,-1.579233381633694e-07); 
      val = __fma_rn(val,temperature,3.114176405405652e-04);
      val = __fma_rn(val,temperature,0.2915526392882199);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+22*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[22] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.387938168142103e-11,temperature,-3.14388478339873e-07); 
      val = __fma_rn(val,temperature,7.128660751880239e-04);
      val = __fma_rn(val,temperature,4.589510383383261e-03);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.421905791965572e-11,temperature,-1.570093545566196e-07); 
      val = __fma_rn(val,temperature,3.096153064358219e-04);
      val = __fma_rn(val,temperature,0.2898652741659205);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+23*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[23] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.512121427917628e-11,temperature,-3.353501163563588e-07); 
      val = __fma_rn(val,temperature,8.072670255813252e-04);
      val = __fma_rn(val,temperature,-0.102672673576567);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.465769593735491e-11,temperature,-2.340910817182158e-07); 
      val = __fma_rn(val,temperature,4.856269199574121e-04);
      val = __fma_rn(val,temperature,0.2000861865382392);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+24*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[24] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.507286434204571e-11,temperature,-3.349907697097407e-07); 
      val = __fma_rn(val,temperature,8.064019932332827e-04);
      val = __fma_rn(val,temperature,-0.102562653990619);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.458334089273447e-11,temperature,-2.335888598492875e-07); 
      val = __fma_rn(val,temperature,4.845850500256201e-04);
      val = __fma_rn(val,temperature,0.1996569191872046);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+25*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[25] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.502225004921751e-11,temperature,-3.346145939072854e-07); 
      val = __fma_rn(val,temperature,8.054964491277873e-04);
      val = __fma_rn(val,temperature,-0.1024474818958766);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.450558327995375e-11,temperature,-2.330636557583819e-07); 
      val = __fma_rn(val,temperature,4.834955029863076e-04);
      val = __fma_rn(val,temperature,0.1992080080927188);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+26*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[26] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.015630420200985e-11,temperature,-2.766155182856125e-07); 
      val = __fma_rn(val,temperature,5.901179147926529e-04);
      val = __fma_rn(val,temperature,0.1425531835039741);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.395295820937912e-11,temperature,-8.452040383421748e-08); 
      val = __fma_rn(val,temperature,1.578073119571342e-04);
      val = __fma_rn(val,temperature,0.4095753483425218);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+27*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[27] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.598230698897722e-11,temperature,-3.417499339374789e-07); 
      val = __fma_rn(val,temperature,8.22672899773669e-04);
      val = __fma_rn(val,temperature,-0.1046320776423694);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.599451056446156e-11,temperature,-2.431204292744346e-07); 
      val = __fma_rn(val,temperature,5.043584932013247e-04);
      val = __fma_rn(val,temperature,0.2078038992601058);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+28*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[28] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.528309056404535e-11,temperature,-3.386498705397744e-07); 
      val = __fma_rn(val,temperature,8.235535927654216e-04);
      val = __fma_rn(val,temperature,-0.1188180249186659);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.678413279743343e-11,temperature,-2.498605372651732e-07); 
      val = __fma_rn(val,temperature,5.224090609124337e-04);
      val = __fma_rn(val,temperature,0.1873509486424837);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+29*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[29] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.525881197059819e-11,temperature,-3.384683029297603e-07); 
      val = __fma_rn(val,temperature,8.231120433346818e-04);
      val = __fma_rn(val,temperature,-0.1187543204655188);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.674466790798495e-11,temperature,-2.495924673738696e-07); 
      val = __fma_rn(val,temperature,5.218485796867561e-04);
      val = __fma_rn(val,temperature,0.1871499439180563);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+30*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[30] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.562861733266612e-11,temperature,-3.390901461307104e-07); 
      val = __fma_rn(val,temperature,8.161473562678685e-04);
      val = __fma_rn(val,temperature,-0.1035879709046901);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.541524504956121e-11,temperature,-2.391873354578209e-07); 
      val = __fma_rn(val,temperature,4.961409768733495e-04);
      val = __fma_rn(val,temperature,0.204835323020833);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+31*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[31] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.526286972518531e-11,temperature,-3.403502659526861e-07); 
      val = __fma_rn(val,temperature,8.351082720429574e-04);
      val = __fma_rn(val,temperature,-0.132144036457982);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.849487319876821e-11,temperature,-2.627140628790276e-07); 
      val = __fma_rn(val,temperature,5.529481611640254e-04);
      val = __fma_rn(val,temperature,0.175267630067599);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+32*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[32] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.522258070508474e-11,temperature,-3.402376536221525e-07); 
      val = __fma_rn(val,temperature,8.35595363117477e-04);
      val = __fma_rn(val,temperature,-0.1333740238279703);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.861015681537012e-11,temperature,-2.636246487581637e-07); 
      val = __fma_rn(val,temperature,5.552368980457714e-04);
      val = __fma_rn(val,temperature,0.1737586269222836);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+33*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[33] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.564696073032282e-11,temperature,-3.388079384934631e-07); 
      val = __fma_rn(val,temperature,8.138176942748241e-04);
      val = __fma_rn(val,temperature,-0.1003931457666353);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.696602709119429e-11,temperature,-2.516334647829666e-07); 
      val = __fma_rn(val,temperature,5.277002617632793e-04);
      val = __fma_rn(val,temperature,0.1790958906990665);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+34*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[34] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.545108225360678e-11,temperature,-3.382458846404453e-07); 
      val = __fma_rn(val,temperature,8.159949038123061e-04);
      val = __fma_rn(val,temperature,-0.106823844419147);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.555747194480747e-11,temperature,-2.40461978905484e-07); 
      val = __fma_rn(val,temperature,4.996799923975804e-04);
      val = __fma_rn(val,temperature,0.1999436033230719);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+35*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[35] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.563206548322578e-11,temperature,-3.390971051300635e-07); 
      val = __fma_rn(val,temperature,8.160903905048536e-04);
      val = __fma_rn(val,temperature,-0.1034520728959348);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.733746863667858e-11,temperature,-2.545125312985756e-07); 
      val = __fma_rn(val,temperature,5.347808508376394e-04);
      val = __fma_rn(val,temperature,0.1750114498215574);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+36*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[36] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.561605967620619e-11,temperature,-3.302722739669981e-07); 
      val = __fma_rn(val,temperature,7.616900972942965e-04);
      val = __fma_rn(val,temperature,-0.02924248273991606);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.853273617119277e-11,temperature,-1.874470217448901e-07); 
      val = __fma_rn(val,temperature,3.752788651966644e-04);
      val = __fma_rn(val,temperature,0.2765479501123343);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+37*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[37] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.559584748441448e-11,temperature,-3.30748651258885e-07); 
      val = __fma_rn(val,temperature,7.651339800697156e-04);
      val = __fma_rn(val,temperature,-0.03506726825304948);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.895076682490655e-11,temperature,-1.906201975886112e-07); 
      val = __fma_rn(val,temperature,3.826571187059876e-04);
      val = __fma_rn(val,temperature,0.2702797104568991);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+38*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[38] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.556106005956227e-11,temperature,-3.311516853157343e-07); 
      val = __fma_rn(val,temperature,7.685461104044838e-04);
      val = __fma_rn(val,temperature,-0.04108604408667313);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.937943806303212e-11,temperature,-1.938904277835856e-07); 
      val = __fma_rn(val,temperature,3.903146060188393e-04);
      val = __fma_rn(val,temperature,0.2637157772638316);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+39*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[39] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.556106005956227e-11,temperature,-3.311516853157343e-07); 
      val = __fma_rn(val,temperature,7.685461104044838e-04);
      val = __fma_rn(val,temperature,-0.04108604408667313);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.937943806303212e-11,temperature,-1.938904277835856e-07); 
      val = __fma_rn(val,temperature,3.903146060188393e-04);
      val = __fma_rn(val,temperature,0.2637157772638316);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+40*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[40] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.55018974366283e-11,temperature,-3.307216733215738e-07); 
      val = __fma_rn(val,temperature,7.675481265191711e-04);
      val = __fma_rn(val,temperature,-0.04103269242780111);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.93030384087735e-11,temperature,-1.933862261165918e-07); 
      val = __fma_rn(val,temperature,3.892996138025842e-04);
      val = __fma_rn(val,temperature,0.263029998517408);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+41*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[41] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.538351026613339e-11,temperature,-3.400485414423541e-07); 
      val = __fma_rn(val,temperature,8.295403167181099e-04);
      val = __fma_rn(val,temperature,-0.1238182141997003);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.813120504270874e-11,temperature,-2.601803589274413e-07); 
      val = __fma_rn(val,temperature,5.474599659441624e-04);
      val = __fma_rn(val,temperature,0.174465567384375);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+42*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[42] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.568991924255432e-11,temperature,-3.410216187271481e-07); 
      val = __fma_rn(val,temperature,8.266556977512041e-04);
      val = __fma_rn(val,temperature,-0.1149139293342275);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.743456061112879e-11,temperature,-2.545627663852558e-07); 
      val = __fma_rn(val,temperature,5.330754599419471e-04);
      val = __fma_rn(val,temperature,0.1857841679435356);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+43*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[43] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.548102247745546e-11,temperature,-3.384686990818159e-07); 
      val = __fma_rn(val,temperature,8.165324283082732e-04);
      val = __fma_rn(val,temperature,-0.1068942130364758);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.560437965498766e-11,temperature,-2.407791983307953e-07); 
      val = __fma_rn(val,temperature,5.003391743636832e-04);
      val = __fma_rn(val,temperature,0.200207370573219);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+44*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[44] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.619638795512581e-11,temperature,-3.379624168621418e-07); 
      val = __fma_rn(val,temperature,7.927487617245049e-04);
      val = __fma_rn(val,temperature,-0.06119693186523686);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.186402182633129e-11,temperature,-2.118256241031627e-07); 
      val = __fma_rn(val,temperature,4.303070647531414e-04);
      val = __fma_rn(val,temperature,0.2515053501004754);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+45*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[45] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.623447639277388e-11,temperature,-3.380285677190069e-07); 
      val = __fma_rn(val,temperature,7.920881497879646e-04);
      val = __fma_rn(val,temperature,-0.0593988764249655);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.175202114176142e-11,temperature,-2.109368266734933e-07); 
      val = __fma_rn(val,temperature,4.28130461581054e-04);
      val = __fma_rn(val,temperature,0.2537467266443551);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+46*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[46] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.616602451457661e-11,temperature,-3.377402847386903e-07); 
      val = __fma_rn(val,temperature,7.922277127645631e-04);
      val = __fma_rn(val,temperature,-0.06115670903646728);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.182210782821013e-11,temperature,-2.115469882530159e-07); 
      val = __fma_rn(val,temperature,4.297410379784335e-04);
      val = __fma_rn(val,temperature,0.2511745194592898);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+47*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[47] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.612800833678035e-11,temperature,-3.376028818355302e-07); 
      val = __fma_rn(val,temperature,7.924464823482466e-04);
      val = __fma_rn(val,temperature,-0.06232533359647535);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.18788902752517e-11,temperature,-2.120201063368895e-07); 
      val = __fma_rn(val,temperature,4.309492324039214e-04);
      val = __fma_rn(val,temperature,0.2495626655207719);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+48*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[48] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.535824442554248e-11,temperature,-3.407111093833764e-07); 
      val = __fma_rn(val,temperature,8.345669245467522e-04);
      val = __fma_rn(val,temperature,-0.1298861512542501);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.831460977664718e-11,temperature,-2.612528304137385e-07); 
      val = __fma_rn(val,temperature,5.491811503499105e-04);
      val = __fma_rn(val,temperature,0.1782675329250339);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+49*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[49] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.569302062213313e-11,temperature,-3.407097364419278e-07); 
      val = __fma_rn(val,temperature,8.245700459056968e-04);
      val = __fma_rn(val,temperature,-0.1124125697822017);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.831119685796979e-11,temperature,-2.617922769320333e-07); 
      val = __fma_rn(val,temperature,5.520076510227504e-04);
      val = __fma_rn(val,temperature,0.1690038311159024);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+50*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[50] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.557627186054617e-11,temperature,-3.404575241557945e-07); 
      val = __fma_rn(val,temperature,8.264183754430336e-04);
      val = __fma_rn(val,temperature,-0.1167426162680249);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.828212287106097e-11,temperature,-2.615158404207491e-07); 
      val = __fma_rn(val,temperature,5.511900420322915e-04);
      val = __fma_rn(val,temperature,0.1701416330069884);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+51*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[51] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.622136967918545e-11,temperature,-3.373965848903454e-07); 
      val = __fma_rn(val,temperature,7.885546448386373e-04);
      val = __fma_rn(val,temperature,-0.05467626738587272);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.139159892698979e-11,temperature,-2.082432192996593e-07); 
      val = __fma_rn(val,temperature,4.218990940091604e-04);
      val = __fma_rn(val,temperature,0.2574227626771194);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+52*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[52] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.616602451457661e-11,temperature,-3.377402847386903e-07); 
      val = __fma_rn(val,temperature,7.922277127645631e-04);
      val = __fma_rn(val,temperature,-0.06115670903646728);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.182210782821013e-11,temperature,-2.115469882530159e-07); 
      val = __fma_rn(val,temperature,4.297410379784335e-04);
      val = __fma_rn(val,temperature,0.2511745194592898);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+53*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[53] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.526122544242423e-11,temperature,-3.403458487781187e-07); 
      val = __fma_rn(val,temperature,8.351292893484326e-04);
      val = __fma_rn(val,temperature,-0.1321956012368501);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.849973825853257e-11,temperature,-2.627524280124728e-07); 
      val = __fma_rn(val,temperature,5.530444078194158e-04);
      val = __fma_rn(val,temperature,0.1752047792163409);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+54*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[54] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.526122544242423e-11,temperature,-3.403458487781187e-07); 
      val = __fma_rn(val,temperature,8.351292893484326e-04);
      val = __fma_rn(val,temperature,-0.1321956012368501);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.849973825853257e-11,temperature,-2.627524280124728e-07); 
      val = __fma_rn(val,temperature,5.530444078194158e-04);
      val = __fma_rn(val,temperature,0.1752047792163409);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+55*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[55] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.592736460896348e-11,temperature,-3.413666162087228e-07); 
      val = __fma_rn(val,temperature,8.218490212598976e-04);
      val = __fma_rn(val,temperature,-0.1046995313966866);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.593060650610343e-11,temperature,-2.42705428864045e-07); 
      val = __fma_rn(val,temperature,5.035447921451897e-04);
      val = __fma_rn(val,temperature,0.2071307046174108);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+56*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[56] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.590916146467253e-11,temperature,-3.412313167892184e-07); 
      val = __fma_rn(val,temperature,8.215232843828365e-04);
      val = __fma_rn(val,temperature,-0.1046580341173753);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.590211328192655e-11,temperature,-2.425129617485124e-07); 
      val = __fma_rn(val,temperature,5.031454775763345e-04);
      val = __fma_rn(val,temperature,0.2069664485089155);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+57*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[57] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.381007305023083e-11,temperature,-3.347642543627598e-07); 
      val = __fma_rn(val,temperature,8.431319689027048e-04);
      val = __fma_rn(val,temperature,-0.1638797841060047);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.140557323493241e-11,temperature,-2.861663830588287e-07); 
      val = __fma_rn(val,temperature,6.134916377125089e-04);
      val = __fma_rn(val,temperature,0.1330410154083669);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+58*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[58] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.461657115653286e-11,temperature,-3.381653086882606e-07); 
      val = __fma_rn(val,temperature,8.40554308594672e-04);
      val = __fma_rn(val,temperature,-0.1487458973633961);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.00348119786285e-11,temperature,-2.749927605332868e-07); 
      val = __fma_rn(val,temperature,5.842067614676778e-04);
      val = __fma_rn(val,temperature,0.154076944202763);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+59*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[59] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.500330159218058e-11,temperature,-3.384033681819751e-07); 
      val = __fma_rn(val,temperature,8.303508734732157e-04);
      val = __fma_rn(val,temperature,-0.1314210060731693);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.855938562019549e-11,temperature,-2.638178862054273e-07); 
      val = __fma_rn(val,temperature,5.572785036890575e-04);
      val = __fma_rn(val,temperature,0.1647733776857318);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+60*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[60] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.589044219443097e-11,temperature,-3.410921811345892e-07); 
      val = __fma_rn(val,temperature,8.21188311669777e-04);
      val = __fma_rn(val,temperature,-0.1046153602378904);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.587282346345321e-11,temperature,-2.423151137674962e-07); 
      val = __fma_rn(val,temperature,5.027349992407511e-04);
      val = __fma_rn(val,temperature,0.2067976002391966);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+61*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[61] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.587118453146898e-11,temperature,-3.409490437415035e-07); 
      val = __fma_rn(val,temperature,8.208437046671366e-04);
      val = __fma_rn(val,temperature,-0.1045714589972013);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.584270317112543e-11,temperature,-2.421116560700681e-07); 
      val = __fma_rn(val,temperature,5.023128823377989e-04);
      val = __fma_rn(val,temperature,0.2066239645013176);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+62*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[62] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.530098817287206e-11,temperature,-3.404528545054026e-07); 
      val = __fma_rn(val,temperature,8.346226856955816e-04);
      val = __fma_rn(val,temperature,-0.1309475120716851);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.838257874986276e-11,temperature,-2.618282922822298e-07); 
      val = __fma_rn(val,temperature,5.507259444294193e-04);
      val = __fma_rn(val,temperature,0.1767269240452876);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+63*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[63] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.631501115716321e-11,temperature,-3.442478960637856e-07); 
      val = __fma_rn(val,temperature,8.287857775694584e-04);
      val = __fma_rn(val,temperature,-0.1055832401025883);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.653996836002756e-11,temperature,-2.468215695160243e-07); 
      val = __fma_rn(val,temperature,5.120846142610037e-04);
      val = __fma_rn(val,temperature,0.210643518968286);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+64*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[64] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.636781513214472e-11,temperature,-3.413706011653432e-07); 
      val = __fma_rn(val,temperature,8.090986854571634e-04);
      val = __fma_rn(val,temperature,-0.0796555715226468);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.385582974226769e-11,temperature,-2.266032595741289e-07); 
      val = __fma_rn(val,temperature,4.643899371200188e-04);
      val = __fma_rn(val,temperature,0.2352076403720557);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+65*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[65] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.62676299731947e-11,temperature,-3.413400941323498e-07); 
      val = __fma_rn(val,temperature,8.117846373328121e-04);
      val = __fma_rn(val,temperature,-0.08527555792453631);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.426249340131386e-11,temperature,-2.297736437212846e-07); 
      val = __fma_rn(val,temperature,4.721027068087945e-04);
      val = __fma_rn(val,temperature,0.2291049941252589);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+66*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[66] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.62676299731947e-11,temperature,-3.413400941323498e-07); 
      val = __fma_rn(val,temperature,8.117846373328121e-04);
      val = __fma_rn(val,temperature,-0.08527555792453631);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.426249340131386e-11,temperature,-2.297736437212846e-07); 
      val = __fma_rn(val,temperature,4.721027068087945e-04);
      val = __fma_rn(val,temperature,0.2291049941252589);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+67*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[67] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.400170543316899e-11,temperature,-3.356167627223228e-07); 
      val = __fma_rn(val,temperature,8.427969966107827e-04);
      val = __fma_rn(val,temperature,-0.1606515536836302);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.111608558279537e-11,temperature,-2.837843785265333e-07); 
      val = __fma_rn(val,temperature,6.071730281172348e-04);
      val = __fma_rn(val,temperature,0.137681352315085);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+68*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[68] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.5619574012919e-11,temperature,-3.414895866071521e-07); 
      val = __fma_rn(val,temperature,8.31748649990881e-04);
      val = __fma_rn(val,temperature,-0.1220733036299917);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.852377952754338e-11,temperature,-2.630254544924468e-07); 
      val = __fma_rn(val,temperature,5.539465488827452e-04);
      val = __fma_rn(val,temperature,0.1735243414217434);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+69*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[69] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.599714018931597e-11,temperature,-3.433207776151939e-07); 
      val = __fma_rn(val,temperature,8.322532861920912e-04);
      val = __fma_rn(val,temperature,-0.1157322522800855);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.733419947753365e-11,temperature,-2.53157090666833e-07); 
      val = __fma_rn(val,temperature,5.280206106070026e-04);
      val = __fma_rn(val,temperature,0.1978172705125204);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+70*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[70] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.600207787725811e-11,temperature,-3.432474568559233e-07); 
      val = __fma_rn(val,temperature,8.316379483085679e-04);
      val = __fma_rn(val,temperature,-0.1149215636691549);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.725175451766335e-11,temperature,-2.525374429044239e-07); 
      val = __fma_rn(val,temperature,5.265525120926886e-04);
      val = __fma_rn(val,temperature,0.1984471960727259);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+71*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[71] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.600207787725811e-11,temperature,-3.432474568559233e-07); 
      val = __fma_rn(val,temperature,8.316379483085679e-04);
      val = __fma_rn(val,temperature,-0.1149215636691549);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.725175451766335e-11,temperature,-2.525374429044239e-07); 
      val = __fma_rn(val,temperature,5.265525120926886e-04);
      val = __fma_rn(val,temperature,0.1984471960727259);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+72*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[72] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.600207787725811e-11,temperature,-3.432474568559233e-07); 
      val = __fma_rn(val,temperature,8.316379483085679e-04);
      val = __fma_rn(val,temperature,-0.1149215636691549);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.725175451766335e-11,temperature,-2.525374429044239e-07); 
      val = __fma_rn(val,temperature,5.265525120926886e-04);
      val = __fma_rn(val,temperature,0.1984471960727259);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+73*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[73] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.599228178828788e-11,temperature,-3.431743627092821e-07); 
      val = __fma_rn(val,temperature,8.314608519749113e-04);
      val = __fma_rn(val,temperature,-0.1148970912558586);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.72358857028377e-11,temperature,-2.52429864886425e-07); 
      val = __fma_rn(val,temperature,5.263282068373103e-04);
      val = __fma_rn(val,temperature,0.1983626598717356);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+74*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[74] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.459269846479454e-11,temperature,-3.386111587810343e-07); 
      val = __fma_rn(val,temperature,8.441984049113488e-04);
      val = __fma_rn(val,temperature,-0.1528653426813261);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.087521147941881e-11,temperature,-2.815270165742931e-07); 
      val = __fma_rn(val,temperature,6.004681089942864e-04);
      val = __fma_rn(val,temperature,0.1457157109617758);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+75*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[75] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.459269846479454e-11,temperature,-3.386111587810343e-07); 
      val = __fma_rn(val,temperature,8.441984049113488e-04);
      val = __fma_rn(val,temperature,-0.1528653426813261);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.087521147941881e-11,temperature,-2.815270165742931e-07); 
      val = __fma_rn(val,temperature,6.004681089942864e-04);
      val = __fma_rn(val,temperature,0.1457157109617758);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+76*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[76] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.459269846479454e-11,temperature,-3.386111587810343e-07); 
      val = __fma_rn(val,temperature,8.441984049113488e-04);
      val = __fma_rn(val,temperature,-0.1528653426813261);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.087521147941881e-11,temperature,-2.815270165742931e-07); 
      val = __fma_rn(val,temperature,6.004681089942864e-04);
      val = __fma_rn(val,temperature,0.1457157109617758);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+77*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[77] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.459269846479454e-11,temperature,-3.386111587810343e-07); 
      val = __fma_rn(val,temperature,8.441984049113488e-04);
      val = __fma_rn(val,temperature,-0.1528653426813261);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.087521147941881e-11,temperature,-2.815270165742931e-07); 
      val = __fma_rn(val,temperature,6.004681089942864e-04);
      val = __fma_rn(val,temperature,0.1457157109617758);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+78*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[78] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.398852262727423e-11,temperature,-3.361557149210406e-07); 
      val = __fma_rn(val,temperature,8.467497672787769e-04);
      val = __fma_rn(val,temperature,-0.1647260826228332);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.193680476295577e-11,temperature,-2.901186828193292e-07); 
      val = __fma_rn(val,temperature,6.228630624609403e-04);
      val = __fma_rn(val,temperature,0.1306919011985047);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+79*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[79] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.398852262727423e-11,temperature,-3.361557149210406e-07); 
      val = __fma_rn(val,temperature,8.467497672787769e-04);
      val = __fma_rn(val,temperature,-0.1647260826228332);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.193680476295577e-11,temperature,-2.901186828193292e-07); 
      val = __fma_rn(val,temperature,6.228630624609403e-04);
      val = __fma_rn(val,temperature,0.1306919011985047);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+80*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[80] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.398852262727423e-11,temperature,-3.361557149210406e-07); 
      val = __fma_rn(val,temperature,8.467497672787769e-04);
      val = __fma_rn(val,temperature,-0.1647260826228332);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.193680476295577e-11,temperature,-2.901186828193292e-07); 
      val = __fma_rn(val,temperature,6.228630624609403e-04);
      val = __fma_rn(val,temperature,0.1306919011985047);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+81*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[81] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.398852262727423e-11,temperature,-3.361557149210406e-07); 
      val = __fma_rn(val,temperature,8.467497672787769e-04);
      val = __fma_rn(val,temperature,-0.1647260826228332);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.193680476295577e-11,temperature,-2.901186828193292e-07); 
      val = __fma_rn(val,temperature,6.228630624609403e-04);
      val = __fma_rn(val,temperature,0.1306919011985047);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+82*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[82] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.398852262727423e-11,temperature,-3.361557149210406e-07); 
      val = __fma_rn(val,temperature,8.467497672787769e-04);
      val = __fma_rn(val,temperature,-0.1647260826228332);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.193680476295577e-11,temperature,-2.901186828193292e-07); 
      val = __fma_rn(val,temperature,6.228630624609403e-04);
      val = __fma_rn(val,temperature,0.1306919011985047);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+83*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[83] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.533905913535488e-11,temperature,-3.414666322989833e-07); 
      val = __fma_rn(val,temperature,8.400295151879666e-04);
      val = __fma_rn(val,temperature,-0.1362011204084067);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.915315077639392e-11,temperature,-2.675624913293333e-07); 
      val = __fma_rn(val,temperature,5.642255720374605e-04);
      val = __fma_rn(val,temperature,0.1724436229983005);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+84*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[84] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.533905913535488e-11,temperature,-3.414666322989833e-07); 
      val = __fma_rn(val,temperature,8.400295151879666e-04);
      val = __fma_rn(val,temperature,-0.1362011204084067);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.915315077639392e-11,temperature,-2.675624913293333e-07); 
      val = __fma_rn(val,temperature,5.642255720374605e-04);
      val = __fma_rn(val,temperature,0.1724436229983005);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+85*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[85] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.533905913535488e-11,temperature,-3.414666322989833e-07); 
      val = __fma_rn(val,temperature,8.400295151879666e-04);
      val = __fma_rn(val,temperature,-0.1362011204084067);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.915315077639392e-11,temperature,-2.675624913293333e-07); 
      val = __fma_rn(val,temperature,5.642255720374605e-04);
      val = __fma_rn(val,temperature,0.1724436229983005);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+86*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[86] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.429012509884483e-11,temperature,-3.374188011743558e-07); 
      val = __fma_rn(val,temperature,8.457093065560648e-04);
      val = __fma_rn(val,temperature,-0.1591162405485481);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.152374351576052e-11,temperature,-2.867949675116273e-07); 
      val = __fma_rn(val,temperature,6.142400407456487e-04);
      val = __fma_rn(val,temperature,0.1361599814793505);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+87*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[87] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.491163293144878e-11,temperature,-3.389136663234355e-07); 
      val = __fma_rn(val,temperature,8.36429059227314e-04);
      val = __fma_rn(val,temperature,-0.1395626623254423);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.943224522105039e-11,temperature,-2.704033308251574e-07); 
      val = __fma_rn(val,temperature,5.730671329970126e-04);
      val = __fma_rn(val,temperature,0.1587674988067104);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+88*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[88] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.605191204729365e-11,temperature,-3.391557773161557e-07); 
      val = __fma_rn(val,temperature,8.042809015946806e-04);
      val = __fma_rn(val,temperature,-0.08002764657078171);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.345488119022815e-11,temperature,-2.239623516344361e-07); 
      val = __fma_rn(val,temperature,4.590928302784055e-04);
      val = __fma_rn(val,temperature,0.2315640470601582);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+89*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[89] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.604658854769384e-11,temperature,-3.385904459214842e-07); 
      val = __fma_rn(val,temperature,8.008970373892375e-04);
      val = __fma_rn(val,temperature,-0.07565912639074657);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.301963819907232e-11,temperature,-2.206970105483841e-07); 
      val = __fma_rn(val,temperature,4.514571475624785e-04);
      val = __fma_rn(val,temperature,0.2356669287270006);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+90*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[90] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.623659108416477e-11,temperature,-3.438410635928887e-07); 
      val = __fma_rn(val,temperature,8.285021962217045e-04);
      val = __fma_rn(val,temperature,-0.1067555933068318);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.657326981394235e-11,temperature,-2.471644899937468e-07); 
      val = __fma_rn(val,temperature,5.13131654027187e-04);
      val = __fma_rn(val,temperature,0.2086832809807505);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+91*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[91] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.623659108416477e-11,temperature,-3.438410635928887e-07); 
      val = __fma_rn(val,temperature,8.285021962217045e-04);
      val = __fma_rn(val,temperature,-0.1067555933068318);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.657326981394235e-11,temperature,-2.471644899937468e-07); 
      val = __fma_rn(val,temperature,5.13131654027187e-04);
      val = __fma_rn(val,temperature,0.2086832809807505);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+92*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[92] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.469728764425059e-11,temperature,-3.383230704438438e-07); 
      val = __fma_rn(val,temperature,8.391149486882217e-04);
      val = __fma_rn(val,temperature,-0.1459428470818797);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.010520537672479e-11,temperature,-2.757365489322036e-07); 
      val = __fma_rn(val,temperature,5.865958928851192e-04);
      val = __fma_rn(val,temperature,0.1503507068022064);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+93*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[93] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.469728764425059e-11,temperature,-3.383230704438438e-07); 
      val = __fma_rn(val,temperature,8.391149486882217e-04);
      val = __fma_rn(val,temperature,-0.1459428470818797);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.010520537672479e-11,temperature,-2.757365489322036e-07); 
      val = __fma_rn(val,temperature,5.865958928851192e-04);
      val = __fma_rn(val,temperature,0.1503507068022064);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+94*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[94] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.459269846479454e-11,temperature,-3.386111587810343e-07); 
      val = __fma_rn(val,temperature,8.441984049113488e-04);
      val = __fma_rn(val,temperature,-0.1528653426813261);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.087521147941881e-11,temperature,-2.815270165742931e-07); 
      val = __fma_rn(val,temperature,6.004681089942864e-04);
      val = __fma_rn(val,temperature,0.1457157109617758);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+95*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[95] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.615670439639393e-11,temperature,-3.426738044280574e-07); 
      val = __fma_rn(val,temperature,8.234283249154151e-04);
      val = __fma_rn(val,temperature,-0.102153503441574);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.593932429521948e-11,temperature,-2.425001093799645e-07); 
      val = __fma_rn(val,temperature,5.023711758087253e-04);
      val = __fma_rn(val,temperature,0.2120399075588693);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+96*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[96] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.614263282048984e-11,temperature,-3.425693350879602e-07); 
      val = __fma_rn(val,temperature,8.231772902211674e-04);
      val = __fma_rn(val,temperature,-0.1021223603867062);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.59174043093474e-11,temperature,-2.423522045688466e-07); 
      val = __fma_rn(val,temperature,5.020647713536551e-04);
      val = __fma_rn(val,temperature,0.2119105809265757);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+97*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[97] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.614263282048984e-11,temperature,-3.425693350879602e-07); 
      val = __fma_rn(val,temperature,8.231772902211674e-04);
      val = __fma_rn(val,temperature,-0.1021223603867062);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.59174043093474e-11,temperature,-2.423522045688466e-07); 
      val = __fma_rn(val,temperature,5.020647713536551e-04);
      val = __fma_rn(val,temperature,0.2119105809265757);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+98*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[98] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.585877223612714e-11,temperature,-3.411268858753558e-07); 
      val = __fma_rn(val,temperature,8.223398944808384e-04);
      val = __fma_rn(val,temperature,-0.1066140615489455);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.606248410869454e-11,temperature,-2.437759561079196e-07); 
      val = __fma_rn(val,temperature,5.062774699853959e-04);
      val = __fma_rn(val,temperature,0.2046167696567734);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+99*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[99] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.5619574012919e-11,temperature,-3.414895866071521e-07); 
      val = __fma_rn(val,temperature,8.31748649990881e-04);
      val = __fma_rn(val,temperature,-0.1220733036299917);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.852377952754338e-11,temperature,-2.630254544924468e-07); 
      val = __fma_rn(val,temperature,5.539465488827452e-04);
      val = __fma_rn(val,temperature,0.1735243414217434);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+100*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[100] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.612561486903246e-11,temperature,-3.442122202574722e-07); 
      val = __fma_rn(val,temperature,8.341461700160089e-04);
      val = __fma_rn(val,temperature,-0.1155515350422395);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.748101347717267e-11,temperature,-2.541077625152081e-07); 
      val = __fma_rn(val,temperature,5.298734307471563e-04);
      val = __fma_rn(val,temperature,0.1993843985947297);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+101*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[101] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.572782926774026e-11,temperature,-3.4287232708705e-07); 
      val = __fma_rn(val,temperature,8.374033086713351e-04);
      val = __fma_rn(val,temperature,-0.1265252110902139);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.836148763634291e-11,temperature,-2.611822284453702e-07); 
      val = __fma_rn(val,temperature,5.478720979557639e-04);
      val = __fma_rn(val,temperature,0.1850184764261043);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+102*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[102] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.572782926774026e-11,temperature,-3.4287232708705e-07); 
      val = __fma_rn(val,temperature,8.374033086713351e-04);
      val = __fma_rn(val,temperature,-0.1265252110902139);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.836148763634291e-11,temperature,-2.611822284453702e-07); 
      val = __fma_rn(val,temperature,5.478720979557639e-04);
      val = __fma_rn(val,temperature,0.1850184764261043);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+103*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[103] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.428606938317187e-11,temperature,-3.373879032096916e-07); 
      val = __fma_rn(val,temperature,8.456318636388975e-04);
      val = __fma_rn(val,temperature,-0.1591016700268083);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.151613798443206e-11,temperature,-2.867424378520742e-07); 
      val = __fma_rn(val,temperature,6.141275359116055e-04);
      val = __fma_rn(val,temperature,0.1361350422778927);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+104*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[104] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.428606938317187e-11,temperature,-3.373879032096916e-07); 
      val = __fma_rn(val,temperature,8.456318636388975e-04);
      val = __fma_rn(val,temperature,-0.1591016700268083);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.151613798443206e-11,temperature,-2.867424378520742e-07); 
      val = __fma_rn(val,temperature,6.141275359116055e-04);
      val = __fma_rn(val,temperature,0.1361350422778927);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+105*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[105] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.420459399926626e-11,temperature,-3.367671931461888e-07); 
      val = __fma_rn(val,temperature,8.440761107420514e-04);
      val = __fma_rn(val,temperature,-0.1588089624140987);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.136349463771079e-11,temperature,-2.856881652851709e-07); 
      val = __fma_rn(val,temperature,6.118695589670645e-04);
      val = __fma_rn(val,temperature,0.1356345114128318);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+106*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[106] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.420459399926626e-11,temperature,-3.367671931461888e-07); 
      val = __fma_rn(val,temperature,8.440761107420514e-04);
      val = __fma_rn(val,temperature,-0.1588089624140987);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.136349463771079e-11,temperature,-2.856881652851709e-07); 
      val = __fma_rn(val,temperature,6.118695589670645e-04);
      val = __fma_rn(val,temperature,0.1356345114128318);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+107*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[107] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.434363313569837e-11,temperature,-3.378264454880267e-07); 
      val = __fma_rn(val,temperature,8.467310296747424e-04);
      val = __fma_rn(val,temperature,-0.1593084729625293);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.162414904251256e-11,temperature,-2.874884454436426e-07); 
      val = __fma_rn(val,temperature,6.157252896567891e-04);
      val = __fma_rn(val,temperature,0.1364892199705233);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+108*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[108] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.439721311872035e-11,temperature,-3.3823463791462e-07); 
      val = __fma_rn(val,temperature,8.477541265882343e-04);
      val = __fma_rn(val,temperature,-0.159500963849563);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.172480878156449e-11,temperature,-2.88183679161676e-07); 
      val = __fma_rn(val,temperature,6.172142990037726e-04);
      val = __fma_rn(val,temperature,0.1368192920460296);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+109*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[109] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.439721311872035e-11,temperature,-3.3823463791462e-07); 
      val = __fma_rn(val,temperature,8.477541265882343e-04);
      val = __fma_rn(val,temperature,-0.159500963849563);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.172480878156449e-11,temperature,-2.88183679161676e-07); 
      val = __fma_rn(val,temperature,6.172142990037726e-04);
      val = __fma_rn(val,temperature,0.1368192920460296);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+110*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[110] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.428606938317187e-11,temperature,-3.373879032096916e-07); 
      val = __fma_rn(val,temperature,8.456318636388975e-04);
      val = __fma_rn(val,temperature,-0.1591016700268083);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.151613798443206e-11,temperature,-2.867424378520742e-07); 
      val = __fma_rn(val,temperature,6.141275359116055e-04);
      val = __fma_rn(val,temperature,0.1361350422778927);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+111*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[111] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.471698312329224e-11,temperature,-3.39554904588789e-07); 
      val = __fma_rn(val,temperature,8.465512768852567e-04);
      val = __fma_rn(val,temperature,-0.1532913948729487);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.110340986737132e-11,temperature,-2.830987273746074e-07); 
      val = __fma_rn(val,temperature,6.038204061330641e-04);
      val = __fma_rn(val,temperature,0.1465292135501986);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+112*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[112] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.398852262727423e-11,temperature,-3.361557149210406e-07); 
      val = __fma_rn(val,temperature,8.467497672787769e-04);
      val = __fma_rn(val,temperature,-0.1647260826228332);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.193680476295577e-11,temperature,-2.901186828193292e-07); 
      val = __fma_rn(val,temperature,6.228630624609403e-04);
      val = __fma_rn(val,temperature,0.1306919011985047);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+113*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[113] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.472726005822209e-11,temperature,-3.396329416882495e-07); 
      val = __fma_rn(val,temperature,8.467458327738623e-04);
      val = __fma_rn(val,temperature,-0.1533266245682541);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.112230715663233e-11,temperature,-2.832288819909248e-07); 
      val = __fma_rn(val,temperature,6.040980125144623e-04);
      val = __fma_rn(val,temperature,0.1465965803439168);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+114*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[114] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(4.429012509884483e-11,temperature,-3.374188011743558e-07); 
      val = __fma_rn(val,temperature,8.457093065560648e-04);
      val = __fma_rn(val,temperature,-0.1591162405485481);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.152374351576052e-11,temperature,-2.867949675116273e-07); 
      val = __fma_rn(val,temperature,6.142400407456487e-04);
      val = __fma_rn(val,temperature,0.1361599814793505);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+115*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[115] * 1e3 * mixmw;
    {
      double val = 
        __fma_rn(3.269585062280131e-11,temperature,-2.18173873565895e-07); 
      val = __fma_rn(val,temperature,4.453434507868577e-04);
      val = __fma_rn(val,temperature,0.2407444208104047);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.903062174600694e-12,temperature,-2.630234415935383e-08); 
      val = __fma_rn(val,temperature,4.946971739853044e-05);
      val = __fma_rn(val,temperature,0.4452619662404717);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+0*spec_stride) , 
    "d"(thermal[0]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+1*spec_stride) , 
    "d"(thermal[1]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+2*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+3*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+4*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+5*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+6*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+7*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+8*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+9*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+10*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+11*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+12*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+13*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+14*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+15*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+16*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+17*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+18*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+19*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+20*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+21*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+22*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+23*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+24*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+25*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+26*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+27*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+28*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+29*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+30*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+31*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+32*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+33*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+34*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+35*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+36*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+37*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+38*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+39*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+40*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+41*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+42*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+43*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+44*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+45*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+46*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+47*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+48*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+49*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+50*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+51*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+52*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+53*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+54*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+55*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+56*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+57*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+58*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+59*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+60*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+61*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+62*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+63*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+64*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+65*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+66*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+67*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+68*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+69*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+70*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+71*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+72*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+73*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+74*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+75*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+76*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+77*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+78*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+79*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+80*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+81*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+82*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+83*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+84*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+85*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+86*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+87*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+88*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+89*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+90*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+91*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+92*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+93*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+94*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+95*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+96*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+97*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+98*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+99*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+100*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+101*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+102*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+103*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+104*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+105*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+106*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+107*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+108*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+109*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+110*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+111*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+112*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+113*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+114*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+115*spec_stride) 
    , "d"(0.0) : "memory"); 
}

