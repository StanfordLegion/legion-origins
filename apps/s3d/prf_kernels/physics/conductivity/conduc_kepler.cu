
/*
 * Kepler tuned conductivity kernel for PRF.
 * Gets 4 CTAs with 4 warps per CTA.
 * Should compile to 34 registers.
 * Math throughput limited:
 * 23 DMFAs per 8-byte load
 *
 * Can tune number of threads for each CTA
 * Probably want at least 4 warps per CTA
 * since max number of CTAs/SM is 16 on Kepler.
 *
 * Launch with:
 *  dim3 grid((nx*ny*nz)/128,1,1);
 *  dim3 block(128,1,1);
 *
 * gpu_conductivity<<<grid,block>>>(temperature_ptr,  
 *                 mole_frac_input_ptr, nx*ny, nx, nz, nx*ny*nz, output_d);
 *
 * Performance numbers on K20c with 13 SMs:
 * 
 * 32x32x32 (threads=128)
 *   Latency: 10.715 ms
 *   Throughput: 61.164 Mpoints/s
 *
 * 64x64x64 (threads=128)
 *   Latency: 70.675 ms
 *   Throughput: 74.183 Mpoints/s
 *
 * 128x128x128 (threads=128)
 *   Latency: 535.941 ms
 *   Throughput: 78.261 Mpoints/s
 *
 * Generation command:
 *   ./singe --dir inputs/PRF/ --cuda --k20
 *
 */

__constant__ double molecular_masses[116] = {1.00797, 2.01594, 15.9994, 31.9988, 
  17.00737, 18.01534, 33.00677, 34.01474, 28.01055, 44.00995, 30.02649, 
  29.01852, 46.02589, 32.04243, 48.04183, 47.03386, 16.04303, 15.03506, 
  30.07012, 29.06215, 28.05418, 27.04621, 26.03824, 44.05358, 43.04561, 
  42.03764, 41.02967, 75.04441, 62.06892000000001, 61.06095000000001, 58.08067, 
  89.0715, 90.07947, 56.06473, 55.05676, 58.08067, 42.08127, 41.0733, 40.06533, 
  40.06533, 39.05736, 75.08804000000001, 75.08804000000001, 56.06473, 56.10836, 
  57.11633, 55.10039, 54.09242, 89.11513000000001, 71.09979, 72.10776, 56.10836, 
  55.10039, 89.11513000000001, 89.11513000000001, 72.10776, 71.09979, 121.11393, 
  104.10656, 71.09979, 70.09182, 69.08385, 88.10716000000001, 103.09859, 
  70.13545000000001, 69.12748000000001, 69.12748000000001, 117.16931, 86.13485, 
  100.20557, 98.18963000000001, 98.18963000000001, 98.18963000000001, 
  97.18166000000001, 131.1964, 131.1964, 131.1964, 131.1964, 163.1952, 163.1952, 
  163.1952, 163.1952, 163.1952, 114.18903, 114.18903, 114.18903, 146.18783, 
  88.10716000000001, 57.0727, 55.10039, 98.18963000000001, 98.18963000000001, 
  97.18166000000001, 97.18166000000001, 131.1964, 82.14660000000001, 
  81.13863000000001, 81.13863000000001, 71.14342000000001, 86.13485, 114.23266, 
  112.21672, 112.21672, 145.22349, 145.22349, 128.21612, 128.21612, 160.21492, 
  177.22229, 177.22229, 145.22349, 160.21492, 163.1952, 163.1952, 146.18783, 
  28.0134}; 

__constant__ double recip_molecular_masses[116] = {0.9920930186414277, 
  0.4960465093207139, 0.06250234383789392, 0.03125117191894696, 
  0.05879803873262004, 0.05550825019122593, 0.03029681486555637, 
  0.02939901936631002, 0.03570083414998991, 0.02272213442641948, 
  0.03330392596670473, 0.03446075127194632, 0.02172690196756651, 
  0.03120861932131863, 0.02081519375927187, 0.021261278576753, 
  0.06233236489615739, 0.06651120780362699, 0.03325560390181349, 
  0.0344090165386938, 0.03564531203549703, 0.0369737571363973, 
  0.0384050534905585, 0.02269963076780593, 0.02323117270262868, 
  0.02378820504671527, 0.02437260645771706, 0.01332544289441412, 
  0.01611112292593459, 0.01637707896781822, 0.01721743223692151, 
  0.01122693566404518, 0.01110130865556824, 0.01783652574443861, 
  0.01816307388956415, 0.01721743223692151, 0.02376354135699802, 
  0.02434671672351625, 0.02495923532889907, 0.02495923532889907, 
  0.02560336899370566, 0.01331770012907515, 0.01331770012907515, 
  0.01783652574443861, 0.01782265601774851, 0.01750812771058645, 
  0.01814869186951308, 0.01848687856819865, 0.01122143905305418, 
  0.01406473915042506, 0.01386813291662368, 0.01782265601774851, 
  0.01814869186951308, 0.01122143905305418, 0.01122143905305418, 
  0.01386813291662368, 0.01406473915042506, 8.256688557625038e-03, 
  9.605542628629742e-03, 0.01406473915042506, 0.01426700005792402, 
  0.01447516315318269, 0.01134981538390296, 9.699453697669386e-03, 
  0.01425812481419881, 0.01446602711396394, 0.01446602711396394, 
  8.534658094342281e-03, 0.01160970269292859, 9.979485172331238e-03, 
  0.01018437486728486, 0.01018437486728486, 0.01018437486728486, 
  0.01029000739439931, 7.622160364156333e-03, 7.622160364156333e-03, 
  7.622160364156333e-03, 7.622160364156333e-03, 6.127631204839358e-03, 
  6.127631204839358e-03, 6.127631204839358e-03, 6.127631204839358e-03, 
  6.127631204839358e-03, 8.757408658257278e-03, 8.757408658257278e-03, 
  8.757408658257278e-03, 6.840514699479429e-03, 0.01134981538390296, 
  0.01752151203640269, 0.01814869186951308, 0.01018437486728486, 
  0.01018437486728486, 0.01029000739439931, 0.01029000739439931, 
  7.622160364156333e-03, 0.01217335836175812, 0.01232458571213243, 
  0.01232458571213243, 0.01405611369259448, 0.01160970269292859, 
  8.754063855293223e-03, 8.911328008874257e-03, 8.911328008874257e-03, 
  6.885938356115805e-03, 6.885938356115805e-03, 7.799331316530245e-03, 
  7.799331316530245e-03, 6.241615949376e-03, 5.642631070843289e-03, 
  5.642631070843289e-03, 6.885938356115805e-03, 6.241615949376e-03, 
  6.127631204839358e-03, 6.127631204839358e-03, 6.840514699479429e-03, 
  0.03569720205330306}; 

__global__ void
__launch_bounds__(128,4)
gpu_conductivity(const double *temperature_array, const double *mass_frac_array, 
  const double *mixmw_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *conductivity) 
{
  {
    const int offset = (blockIdx.x*blockDim.x+threadIdx.x);
    temperature_array += offset;
    mass_frac_array += offset;
    mixmw_array += offset;
    conductivity += offset;
  }
  // Load the temperatures
  double temperature;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  // Load the average mole weight
  double mixmw;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mixmw) : "l"(mixmw_array) 
    : "memory"); 
  temperature *= 120.0;
  // Compute log(t)
  double logt = log(temperature);
  double sum = 0.0;
  double sumr = 0.0;
  // Species H
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+0*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[0] * 1e3 * mixmw;
    double val = __fma_rn(0.01585988965532566,logt,-0.3631110078153958);
    val = __fma_rn(val,logt,3.416401604464596);
    val = __fma_rn(val,logt,-0.3253871703970487);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+1*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[1] * 1e3 * mixmw;
    double val = __fma_rn(-8.971812440098507e-03,logt,0.2434840522735617);
    val = __fma_rn(val,logt,-1.31498166381121);
    val = __fma_rn(val,logt,11.09465938678583);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species O
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+2*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[2] * 1e3 * mixmw;
    double val = __fma_rn(6.908732177853375e-03,logt,-0.1549096547430882);
    val = __fma_rn(val,logt,1.801392496839076);
    val = __fma_rn(val,logt,1.969675461941004);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+3*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[3] * 1e3 * mixmw;
    double val = __fma_rn(0.01344785152870204,logt,-0.3099605229855862);
    val = __fma_rn(val,logt,3.151649687562876);
    val = __fma_rn(val,logt,-2.513869215844388);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species OH
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+4*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[4] * 1e3 * mixmw;
    double val = __fma_rn(-0.02107341977422975,logt,0.4662153180577253);
    val = __fma_rn(val,logt,-2.636264291632148);
    val = __fma_rn(val,logt,12.45224890063577);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2O
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+5*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[5] * 1e3 * mixmw;
    double val = __fma_rn(-0.08578545401194727,logt,1.736766798678847);
    val = __fma_rn(val,logt,-10.38691498752045);
    val = __fma_rn(val,logt,26.45538168928002);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HO2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+6*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[6] * 1e3 * mixmw;
    double val = __fma_rn(4.072437253732364e-04,logt,-0.05282466276444234);
    val = __fma_rn(val,logt,1.591058710589947);
    val = __fma_rn(val,logt,0.5546382791947876);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+7*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[7] * 1e3 * mixmw;
    double val = __fma_rn(-6.382550767548769e-03,logt,0.05716835124727214);
    val = __fma_rn(val,logt,1.062275726777448);
    val = __fma_rn(val,logt,1.48625779925865);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+8*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[8] * 1e3 * mixmw;
    double val = __fma_rn(-0.01479369308174673,logt,0.2876388960195676);
    val = __fma_rn(val,logt,-1.04069697699083);
    val = __fma_rn(val,logt,7.156323549823808);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CO2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+9*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[9] * 1e3 * mixmw;
    double val = __fma_rn(0.01991705093639912,logt,-0.559384259218221);
    val = __fma_rn(val,logt,5.807104611907367);
    val = __fma_rn(val,logt,-11.1476090840516);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH2O
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+10*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[10] * 1e3 * mixmw;
    double val = __fma_rn(-0.03107422780363027,logt,0.4441066419283235);
    val = __fma_rn(val,logt,-0.4377550672718165);
    val = __fma_rn(val,logt,1.115022738076616);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HCO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+11*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[11] * 1e3 * mixmw;
    double val = __fma_rn(-0.04398311519656636,logt,0.7818064435225804);
    val = __fma_rn(val,logt,-3.358927837668578);
    val = __fma_rn(val,logt,9.175401890258234);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HOCHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+12*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[12] * 1e3 * mixmw;
    double val = __fma_rn(0.01887544717884202,logt,-0.6268681960831423);
    val = __fma_rn(val,logt,7.108176745665713);
    val = __fma_rn(val,logt,-16.44432971987534);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3OH
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+13*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[13] * 1e3 * mixmw;
    double val = __fma_rn(-0.01631010667239554,logt,0.1044498840523253);
    val = __fma_rn(val,logt,2.205308860566742);
    val = __fma_rn(val,logt,-5.48679190929971);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3O2H
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+14*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[14] * 1e3 * mixmw;
    double val = __fma_rn(1.376954896583777e-03,logt,-0.258554112470858);
    val = __fma_rn(val,logt,4.575833828203221);
    val = __fma_rn(val,logt,-10.30449946964937);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+15*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[15] * 1e3 * mixmw;
    double val = __fma_rn(-0.0255666174707711,logt,0.3297736094821814);
    val = __fma_rn(val,logt,0.3386703243203927);
    val = __fma_rn(val,logt,-0.4087522269886092);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH4
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+16*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[16] * 1e3 * mixmw;
    double val = __fma_rn(-0.04502788383046752,logt,0.7549857295730629);
    val = __fma_rn(val,logt,-2.797253149512245);
    val = __fma_rn(val,logt,7.896812647532657);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+17*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[17] * 1e3 * mixmw;
    double val = __fma_rn(-0.03904773097108359,logt,0.7310707289223419);
    val = __fma_rn(val,logt,-3.456311287501603);
    val = __fma_rn(val,logt,11.37039088293264);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H6
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+18*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[18] * 1e3 * mixmw;
    double val = __fma_rn(0.01260281555077185,logt,-0.5099200532130985);
    val = __fma_rn(val,logt,6.473223815242712);
    val = __fma_rn(val,logt,-15.00422507348189);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H5
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+19*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[19] * 1e3 * mixmw;
    double val = __fma_rn(7.819435141734036e-03,logt,-0.3813773454338885);
    val = __fma_rn(val,logt,5.342093798436855);
    val = __fma_rn(val,logt,-11.86823220137246);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H4
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+20*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[20] * 1e3 * mixmw;
    double val = __fma_rn(0.0176087520096732,logt,-0.5938378336921354);
    val = __fma_rn(val,logt,6.863336990851373);
    val = __fma_rn(val,logt,-15.1314653038555);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H3
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+21*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[21] * 1e3 * mixmw;
    double val = __fma_rn(0.01051349943687131,logt,-0.4142259269284922);
    val = __fma_rn(val,logt,5.347170227554369);
    val = __fma_rn(val,logt,-11.18991687405521);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+22*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[22] * 1e3 * mixmw;
    double val = __fma_rn(0.01387631358501832,logt,-0.4196201017822119);
    val = __fma_rn(val,logt,4.818818644316385);
    val = __fma_rn(val,logt,-8.526087565954128);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+23*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[23] * 1e3 * mixmw;
    double val = __fma_rn(3.527392371612507e-03,logt,-0.3145985149223144);
    val = __fma_rn(val,logt,5.086298812863912);
    val = __fma_rn(val,logt,-12.00733393925903);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH2CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+24*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[24] * 1e3 * mixmw;
    double val = __fma_rn(0.01656394365079438,logt,-0.5723207318583379);
    val = __fma_rn(val,logt,6.699595400659183);
    val = __fma_rn(val,logt,-15.24596397842156);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH2CO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+25*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[25] * 1e3 * mixmw;
    double val = __fma_rn(6.641652331760933e-03,logt,-0.3225192524259651);
    val = __fma_rn(val,logt,4.627373018741903);
    val = __fma_rn(val,logt,-9.737564567420446);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HCCO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+26*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[26] * 1e3 * mixmw;
    double val = __fma_rn(-8.100916401978992e-04,logt,-0.06892721478583427);
    val = __fma_rn(val,logt,1.957147102615334);
    val = __fma_rn(val,logt,0.06246198770665277);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3CO3
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+27*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[27] * 1e3 * mixmw;
    double val = __fma_rn(0.01199122555097848,logt,-0.450711904345662);
    val = __fma_rn(val,logt,5.629069607610618);
    val = __fma_rn(val,logt,-12.09912116959681);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H5O2H
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+28*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[28] * 1e3 * mixmw;
    double val = __fma_rn(0.01647252511963946,logt,-0.5719089661969301);
    val = __fma_rn(val,logt,6.753648782336318);
    val = __fma_rn(val,logt,-15.55536255931696);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H5O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+29*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[29] * 1e3 * mixmw;
    double val = __fma_rn(-9.96352071012982e-04,logt,-0.1894247362093991);
    val = __fma_rn(val,logt,3.995871024500834);
    val = __fma_rn(val,logt,-9.124796954615482);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3COCH3
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+30*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[30] * 1e3 * mixmw;
    double val = __fma_rn(0.01009556611387441,logt,-0.467057044224039);
    val = __fma_rn(val,logt,6.273786901710239);
    val = __fma_rn(val,logt,-15.28606854033995);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3COCH2O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+31*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[31] * 1e3 * mixmw;
    double val = __fma_rn(-0.01580520724315912,logt,0.129238204789877);
    val = __fma_rn(val,logt,1.691262202361948);
    val = __fma_rn(val,logt,-3.903036192069683);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3COCH2O2H
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+32*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[32] * 1e3 * mixmw;
    double val = __fma_rn(-1.568928859919537e-03,logt,-0.1832224709138104);
    val = __fma_rn(val,logt,3.954401822204027);
    val = __fma_rn(val,logt,-9.228354294106142);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H3CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+33*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[33] * 1e3 * mixmw;
    double val = __fma_rn(0.0289807017341314,logt,-0.851204596016817);
    val = __fma_rn(val,logt,8.807053869559107);
    val = __fma_rn(val,logt,-20.87557672332276);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H3CO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+34*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[34] * 1e3 * mixmw;
    double val = __fma_rn(0.02159432157714404,logt,-0.6562777213055159);
    val = __fma_rn(val,logt,7.108417637135006);
    val = __fma_rn(val,logt,-15.72632593917792);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H5CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+35*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[35] * 1e3 * mixmw;
    double val = __fma_rn(-0.05163739518117463,logt,0.7923255142436797);
    val = __fma_rn(val,logt,-2.190718442391858);
    val = __fma_rn(val,logt,3.56649101808727);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C3H6
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+36*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[36] * 1e3 * mixmw;
    double val = __fma_rn(0.01803624818448422,logt,-0.6228352228731804);
    val = __fma_rn(val,logt,7.245919336711522);
    val = __fma_rn(val,logt,-16.71746863811853);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C3H5-A
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+37*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[37] * 1e3 * mixmw;
    double val = __fma_rn(0.03183266210590843,logt,-0.9081929746505697);
    val = __fma_rn(val,logt,9.168646779264737);
    val = __fma_rn(val,logt,-21.03284122282515);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C3H4-P
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+38*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[38] * 1e3 * mixmw;
    double val = __fma_rn(5.773959691329358e-03,logt,-0.3317637850330909);
    val = __fma_rn(val,logt,4.927555807098175);
    val = __fma_rn(val,logt,-10.82242156255568);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C3H4-A
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+39*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[39] * 1e3 * mixmw;
    double val = __fma_rn(0.01381401024084079,logt,-0.5078450429358684);
    val = __fma_rn(val,logt,6.209924964440014);
    val = __fma_rn(val,logt,-13.91842346433606);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C3H3
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+40*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[40] * 1e3 * mixmw;
    double val = __fma_rn(9.32976701412958e-03,logt,-0.35834058099721);
    val = __fma_rn(val,logt,4.674351358199873);
    val = __fma_rn(val,logt,-9.065845004033033);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NC3H7O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+41*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[41] * 1e3 * mixmw;
    double val = __fma_rn(-2.516449197346529e-03,logt,-0.1856052159607741);
    val = __fma_rn(val,logt,4.181154449533788);
    val = __fma_rn(val,logt,-10.11063955892072);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC3H7O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+42*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[42] * 1e3 * mixmw;
    double val = __fma_rn(0.01005285206428737,logt,-0.4480298179281424);
    val = __fma_rn(val,logt,5.977298472898844);
    val = __fma_rn(val,logt,-14.12587248080143);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3CHCO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+43*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[43] * 1e3 * mixmw;
    double val = __fma_rn(0.01828891069970763,logt,-0.6006874744847212);
    val = __fma_rn(val,logt,6.845090127730683);
    val = __fma_rn(val,logt,-15.37214845602959);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C4H8-1
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+44*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[44] * 1e3 * mixmw;
    double val = __fma_rn(0.02658260151485194,logt,-0.8165562200117548);
    val = __fma_rn(val,logt,8.710835456723588);
    val = __fma_rn(val,logt,-20.51677443831626);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species SC4H9
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+45*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[45] * 1e3 * mixmw;
    double val = __fma_rn(0.01882784174690226,logt,-0.6535529861880186);
    val = __fma_rn(val,logt,7.586808400860657);
    val = __fma_rn(val,logt,-18.16374538746288);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C4H71-3
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+46*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[46] * 1e3 * mixmw;
    double val = __fma_rn(0.03175300932242021,logt,-0.9299010293892567);
    val = __fma_rn(val,logt,9.53020815905924);
    val = __fma_rn(val,logt,-22.53265618711692);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C4H6
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+47*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[47] * 1e3 * mixmw;
    double val = __fma_rn(0.03565543284446677,logt,-0.9909520065974204);
    val = __fma_rn(val,logt,9.763007561438142);
    val = __fma_rn(val,logt,-22.60597953657753);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species PC4H9O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+48*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[48] * 1e3 * mixmw;
    double val = __fma_rn(4.219991633284178e-03,logt,-0.3358253275765659);
    val = __fma_rn(val,logt,5.299690653107541);
    val = __fma_rn(val,logt,-12.83675709600429);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H5COCH2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+49*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[49] * 1e3 * mixmw;
    double val = __fma_rn(9.751379271427016e-03,logt,-0.450752207540612);
    val = __fma_rn(val,logt,6.076443615485423);
    val = __fma_rn(val,logt,-14.71269182893659);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NC3H7CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+50*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[50] * 1e3 * mixmw;
    double val = __fma_rn(6.213305393280698e-03,logt,-0.3762555653468311);
    val = __fma_rn(val,logt,5.576312503425919);
    val = __fma_rn(val,logt,-13.4403187257456);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC4H8
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+51*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[51] * 1e3 * mixmw;
    double val = __fma_rn(0.01689389272114111,logt,-0.5992966938972845);
    val = __fma_rn(val,logt,7.091258341882717);
    val = __fma_rn(val,logt,-16.68024695626224);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC4H7
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+52*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[52] * 1e3 * mixmw;
    double val = __fma_rn(0.02479459195092844,logt,-0.76131364992173);
    val = __fma_rn(val,logt,8.167119126706725);
    val = __fma_rn(val,logt,-18.85312202228574);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species TC4H9O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+53*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[53] * 1e3 * mixmw;
    double val = __fma_rn(9.776893611781421e-03,logt,-0.4503779787906871);
    val = __fma_rn(val,logt,6.069878853592057);
    val = __fma_rn(val,logt,-14.60734651711046);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC4H9O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+54*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[54] * 1e3 * mixmw;
    double val = __fma_rn(7.617775275498861e-03,logt,-0.4108147038639501);
    val = __fma_rn(val,logt,5.849789296728462);
    val = __fma_rn(val,logt,-14.27451142768949);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC3H7CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+55*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[55] * 1e3 * mixmw;
    double val = __fma_rn(0.02411573710647881,logt,-0.7695952304058176);
    val = __fma_rn(val,logt,8.426947843671645);
    val = __fma_rn(val,logt,-20.35401885646375);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species TC3H6CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+56*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[56] * 1e3 * mixmw;
    double val = __fma_rn(0.01165905178810435,logt,-0.4816096106932554);
    val = __fma_rn(val,logt,6.209099299349878);
    val = __fma_rn(val,logt,-14.73008850495938);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC4H8OOH-IO2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+57*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[57] * 1e3 * mixmw;
    double val = __fma_rn(4.782391417607518e-03,logt,-0.3404865883052784);
    val = __fma_rn(val,logt,5.263970657638567);
    val = __fma_rn(val,logt,-12.90453409874579);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC4KETII
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+58*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[58] * 1e3 * mixmw;
    double val = __fma_rn(0.01630221184862269,logt,-0.5929965450844354);
    val = __fma_rn(val,logt,7.088601950893604);
    val = __fma_rn(val,logt,-17.17328587403431);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC4H6OH
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+59*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[59] * 1e3 * mixmw;
    double val = __fma_rn(0.0157506959714032,logt,-0.5766095630000811);
    val = __fma_rn(val,logt,6.945966630965411);
    val = __fma_rn(val,logt,-16.67806413860254);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC3H5CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+60*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[60] * 1e3 * mixmw;
    double val = __fma_rn(0.02612140391873639,logt,-0.7830075161384275);
    val = __fma_rn(val,logt,8.261875941869135);
    val = __fma_rn(val,logt,-19.34752628825325);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC3H5CO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+61*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[61] * 1e3 * mixmw;
    double val = __fma_rn(0.02025106621813004,logt,-0.6395838737419519);
    val = __fma_rn(val,logt,7.0875210382371);
    val = __fma_rn(val,logt,-16.22412234858855);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC4H7OOH
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+62*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[62] * 1e3 * mixmw;
    double val = __fma_rn(8.26738039839249e-03,logt,-0.4033943620589743);
    val = __fma_rn(val,logt,5.604726130549488);
    val = __fma_rn(val,logt,-13.1806786904122);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species TC3H6O2CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+63*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[63] * 1e3 * mixmw;
    double val = __fma_rn(0.02169029103624929,logt,-0.6938651864294556);
    val = __fma_rn(val,logt,7.657428517016783);
    val = __fma_rn(val,logt,-17.90202173684519);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C5H10-1
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+64*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[64] * 1e3 * mixmw;
    double val = __fma_rn(0.02700052384527702,logt,-0.8298997940004281);
    val = __fma_rn(val,logt,8.84857421611536);
    val = __fma_rn(val,logt,-21.2096289016483);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C5H91-3
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+65*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[65] * 1e3 * mixmw;
    double val = __fma_rn(0.03003803755113019,logt,-0.8975481388714498);
    val = __fma_rn(val,logt,9.344707766236015);
    val = __fma_rn(val,logt,-22.45033364812575);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C5H91-4
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+66*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[66] * 1e3 * mixmw;
    double val = __fma_rn(0.0243205613760384,logt,-0.7707163679544478);
    val = __fma_rn(val,logt,8.414347311386305);
    val = __fma_rn(val,logt,-20.21481648632124);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C6H13O2-1
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+67*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[67] * 1e3 * mixmw;
    double val = __fma_rn(9.2390268764101e-04,logt,-0.2753173086639342);
    val = __fma_rn(val,logt,4.976204050211469);
    val = __fma_rn(val,logt,-12.5326119985007);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NC4H9CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+68*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[68] * 1e3 * mixmw;
    double val = __fma_rn(8.523137708473969e-03,logt,-0.4290682153873266);
    val = __fma_rn(val,logt,5.978554490645703);
    val = __fma_rn(val,logt,-14.62808787421757);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NC7H16
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+69*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[69] * 1e3 * mixmw;
    double val = __fma_rn(0.02055394865717284,logt,-0.7015984091163054);
    val = __fma_rn(val,logt,8.034170612133611);
    val = __fma_rn(val,logt,-19.6438961707497);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14-1
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+70*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[70] * 1e3 * mixmw;
    double val = __fma_rn(0.02416871673852385,logt,-0.7764416485443779);
    val = __fma_rn(val,logt,8.540784139021099);
    val = __fma_rn(val,logt,-20.81081371359468);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14-2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+71*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[71] * 1e3 * mixmw;
    double val = __fma_rn(0.02142097802843824,logt,-0.7191035276110006);
    val = __fma_rn(val,logt,8.149046445418623);
    val = __fma_rn(val,logt,-19.94009981684197);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14-3
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+72*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[72] * 1e3 * mixmw;
    double val = __fma_rn(0.02424719179021531,logt,-0.7816181612679974);
    val = __fma_rn(val,logt,8.607585198056242);
    val = __fma_rn(val,logt,-21.0537826829585);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H132-4
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+73*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[73] * 1e3 * mixmw;
    double val = __fma_rn(0.024226998925573,logt,-0.780548194880213);
    val = __fma_rn(val,logt,8.592532133828009);
    val = __fma_rn(val,logt,-21.02969917917719);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H15O2-1
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+74*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[74] * 1e3 * mixmw;
    double val = __fma_rn(3.938836467911271e-03,logt,-0.3391801876026227);
    val = __fma_rn(val,logt,5.418749078678753);
    val = __fma_rn(val,logt,-13.55675292408297);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H15O2-2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+75*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[75] * 1e3 * mixmw;
    double val = __fma_rn(7.836810931422212e-03,logt,-0.4205908413894774);
    val = __fma_rn(val,logt,5.975422620311131);
    val = __fma_rn(val,logt,-14.79497253945762);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H15O2-3
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+76*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[76] * 1e3 * mixmw;
    double val = __fma_rn(7.836810931422212e-03,logt,-0.4205908413894774);
    val = __fma_rn(val,logt,5.975422620311131);
    val = __fma_rn(val,logt,-14.79497253945762);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H15O2-4
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+77*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[77] * 1e3 * mixmw;
    double val = __fma_rn(7.836810931422212e-03,logt,-0.4205908413894774);
    val = __fma_rn(val,logt,5.975422620311131);
    val = __fma_rn(val,logt,-14.79497253945762);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14OOH1-3O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+78*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[78] * 1e3 * mixmw;
    double val = __fma_rn(7.259950377737335e-03,logt,-0.3992847791663398);
    val = __fma_rn(val,logt,5.739388890086436);
    val = __fma_rn(val,logt,-14.28990284362691);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14OOH2-3O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+79*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[79] * 1e3 * mixmw;
    double val = __fma_rn(9.965704424897302e-03,logt,-0.4550067335800911);
    val = __fma_rn(val,logt,6.112817886167695);
    val = __fma_rn(val,logt,-15.09584378763585);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14OOH2-4O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+80*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[80] * 1e3 * mixmw;
    double val = __fma_rn(9.965704424897302e-03,logt,-0.4550067335800911);
    val = __fma_rn(val,logt,6.112817886167695);
    val = __fma_rn(val,logt,-15.09584378763585);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14OOH4-2O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+81*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[81] * 1e3 * mixmw;
    double val = __fma_rn(9.965704424897302e-03,logt,-0.4550067335800911);
    val = __fma_rn(val,logt,6.112817886167695);
    val = __fma_rn(val,logt,-15.09584378763585);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14OOH4-3O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+82*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[82] * 1e3 * mixmw;
    double val = __fma_rn(9.965704424897302e-03,logt,-0.4550067335800911);
    val = __fma_rn(val,logt,6.112817886167695);
    val = __fma_rn(val,logt,-15.09584378763585);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14O1-3
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+83*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[83] * 1e3 * mixmw;
    double val = __fma_rn(0.03231832325094881,logt,-0.969363169304333);
    val = __fma_rn(val,logt,10.06448883314449);
    val = __fma_rn(val,logt,-24.89976790972475);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14O2-4
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+84*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[84] * 1e3 * mixmw;
    double val = __fma_rn(0.03337634000477292,logt,-0.9869943441737321);
    val = __fma_rn(val,logt,10.14645496348521);
    val = __fma_rn(val,logt,-24.96926880591032);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C7H14O3-5
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+85*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[85] * 1e3 * mixmw;
    double val = __fma_rn(0.03337634000477292,logt,-0.9869943441737321);
    val = __fma_rn(val,logt,10.14645496348521);
    val = __fma_rn(val,logt,-24.96926880591032);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NC7KET13
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+86*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[86] * 1e3 * mixmw;
    double val = __fma_rn(4.648391547151486e-03,logt,-0.3421950584558243);
    val = __fma_rn(val,logt,5.32371956923878);
    val = __fma_rn(val,logt,-13.08884948991513);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C4H7OOH1-4
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+87*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[87] * 1e3 * mixmw;
    double val = __fma_rn(0.0121212816451202,logt,-0.4975946076112122);
    val = __fma_rn(val,logt,6.37695532021381);
    val = __fma_rn(val,logt,-15.39504060068674);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3CHCHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+88*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[88] * 1e3 * mixmw;
    double val = __fma_rn(0.02673404770319818,logt,-0.9415047291001111);
    val = __fma_rn(val,logt,10.47689794377387);
    val = __fma_rn(val,logt,-27.03759503823155);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC4H7-I1
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+89*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[89] * 1e3 * mixmw;
    double val = __fma_rn(9.775710775415968e-03,logt,-0.4364849076546956);
    val = __fma_rn(val,logt,5.847332881701009);
    val = __fma_rn(val,logt,-13.42615553695792);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species XC7H14
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+90*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[90] * 1e3 * mixmw;
    double val = __fma_rn(0.02215616083001668,logt,-0.7305456167329407);
    val = __fma_rn(val,logt,8.184478008432436);
    val = __fma_rn(val,logt,-19.8491460981378);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species YC7H14
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+91*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[91] * 1e3 * mixmw;
    double val = __fma_rn(0.01942603841868861,logt,-0.6729738204733515);
    val = __fma_rn(val,logt,7.786346178672924);
    val = __fma_rn(val,logt,-18.95136148448935);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species XC7H13-Z
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+92*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[92] * 1e3 * mixmw;
    double val = __fma_rn(0.01634339008796666,logt,-0.6149907256768178);
    val = __fma_rn(val,logt,7.459414952508743);
    val = __fma_rn(val,logt,-18.38296820939395);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species YC7H13-Y2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+93*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[93] * 1e3 * mixmw;
    double val = __fma_rn(9.757733465966112e-03,logt,-0.4767360943853465);
    val = __fma_rn(val,logt,6.507457112041088);
    val = __fma_rn(val,logt,-16.24476500775072);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species YC7H15O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+94*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[94] * 1e3 * mixmw;
    double val = __fma_rn(9.899239440023602e-03,logt,-0.464377551571614);
    val = __fma_rn(val,logt,6.281284646576459);
    val = __fma_rn(val,logt,-15.49520929911947);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species ACC6H10
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+95*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[95] * 1e3 * mixmw;
    double val = __fma_rn(0.02330776435629068,logt,-0.738976221278165);
    val = __fma_rn(val,logt,8.102021413539479);
    val = __fma_rn(val,logt,-19.31173062296646);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species ACC6H9-A
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+96*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[96] * 1e3 * mixmw;
    double val = __fma_rn(0.03087332458063054,logt,-0.8948374652812969);
    val = __fma_rn(val,logt,9.145759602792779);
    val = __fma_rn(val,logt,-21.60883047057677);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species ACC6H9-D
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+97*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[97] * 1e3 * mixmw;
    double val = __fma_rn(0.03087332458063054,logt,-0.8948374652812969);
    val = __fma_rn(val,logt,9.145759602792779);
    val = __fma_rn(val,logt,-21.60883047057677);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species NEOC5H11
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+98*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[98] * 1e3 * mixmw;
    double val = __fma_rn(0.02111360550792578,logt,-0.7446538730715193);
    val = __fma_rn(val,logt,8.535578571595789);
    val = __fma_rn(val,logt,-20.93527157054831);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species TC4H9CHO
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+99*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[99] * 1e3 * mixmw;
    double val = __fma_rn(0.0289441292664402,logt,-0.8760059021167133);
    val = __fma_rn(val,logt,9.211422301205955);
    val = __fma_rn(val,logt,-22.33763604856136);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC8H18
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+100*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[100] * 1e3 * mixmw;
    double val = __fma_rn(0.0316891200790793,logt,-0.9408572175485437);
    val = __fma_rn(val,logt,9.726962986962668);
    val = __fma_rn(val,logt,-23.56385848069751);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC8H16
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+101*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[101] * 1e3 * mixmw;
    double val = __fma_rn(0.02702825501346872,logt,-0.8372605718195116);
    val = __fma_rn(val,logt,8.967692479301984);
    val = __fma_rn(val,logt,-21.82417538530061);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species JC8H16
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+102*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[102] * 1e3 * mixmw;
    double val = __fma_rn(0.02886775661171496,logt,-0.8757028478924672);
    val = __fma_rn(val,logt,9.230369369511903);
    val = __fma_rn(val,logt,-22.40632548038474);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species BC8H17O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+103*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[103] * 1e3 * mixmw;
    double val = __fma_rn(0.01618137547577202,logt,-0.6013916253522533);
    val = __fma_rn(val,logt,7.272991679794454);
    val = __fma_rn(val,logt,-17.87270448973075);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CC8H17O2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+104*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[104] * 1e3 * mixmw;
    double val = __fma_rn(0.01323026061249098,logt,-0.5357874569611339);
    val = __fma_rn(val,logt,6.788072367839177);
    val = __fma_rn(val,logt,-16.68116087869208);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC8ETERAB
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+105*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[105] * 1e3 * mixmw;
    double val = __fma_rn(0.03268188427313398,logt,-0.9756829219505625);
    val = __fma_rn(val,logt,10.10736636373876);
    val = __fma_rn(val,logt,-25.04456870561197);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC8ETERBD
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+106*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[106] * 1e3 * mixmw;
    double val = __fma_rn(0.03268188427313398,logt,-0.9756829219505625);
    val = __fma_rn(val,logt,10.10736636373876);
    val = __fma_rn(val,logt,-25.04456870561197);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species IC8KETDB
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+107*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[107] * 1e3 * mixmw;
    double val = __fma_rn(0.0223131063666595,logt,-0.7299205516220164);
    val = __fma_rn(val,logt,8.142183555648428);
    val = __fma_rn(val,logt,-19.79038266803494);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species ISO002
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+108*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[108] * 1e3 * mixmw;
    double val = __fma_rn(0.01778532949076689,logt,-0.62382183677644);
    val = __fma_rn(val,logt,7.313927346334085);
    val = __fma_rn(val,logt,-17.61530546072392);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species ISO003
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+109*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[109] * 1e3 * mixmw;
    double val = __fma_rn(0.01663428220883804,logt,-0.6021673696913632);
    val = __fma_rn(val,logt,7.187830678271776);
    val = __fma_rn(val,logt,-17.40377217695461);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species ISO004
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+110*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[110] * 1e3 * mixmw;
    double val = __fma_rn(0.01271578693695843,logt,-0.5284209002971252);
    val = __fma_rn(val,logt,6.76896894151142);
    val = __fma_rn(val,logt,-16.73729300224702);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species ISO008
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+111*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[111] * 1e3 * mixmw;
    double val = __fma_rn(0.02493705029224658,logt,-0.7862406372736697);
    val = __fma_rn(val,logt,8.537739198010968);
    val = __fma_rn(val,logt,-20.62753518087346);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species ISO010
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+112*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[112] * 1e3 * mixmw;
    double val = __fma_rn(9.965704424897302e-03,logt,-0.4550067335800911);
    val = __fma_rn(val,logt,6.112817886167695);
    val = __fma_rn(val,logt,-15.09584378763585);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species ISO011
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+113*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[113] * 1e3 * mixmw;
    double val = __fma_rn(9.897714408640337e-03,logt,-0.4539516762014743);
    val = __fma_rn(val,logt,6.103270591269875);
    val = __fma_rn(val,logt,-14.76882709164238);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species ISO014
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+114*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[114] * 1e3 * mixmw;
    double val = __fma_rn(0.0200127057733755,logt,-0.6753688857787693);
    val = __fma_rn(val,logt,7.717097529383314);
    val = __fma_rn(val,logt,-18.78187430681722);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species N2
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+115*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[115] * 1e3 * mixmw;
    double val = __fma_rn(-0.02750102626098995,logt,0.5546581094878873);
    val = __fma_rn(val,logt,-2.911524100094236);
    val = __fma_rn(val,logt,11.54289501420304);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Write out the coefficients
  double result = 0.5 * (sum + (1.0/sumr));
  result *= 5.403325855130351e-09;
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(conductivity), "d"(result) : 
    "memory"); 
}

