
#ifndef __AVX_CPU_GET_COEFFS__
#define __AVX_CPU_GET_COEFFS__

#ifndef __SINGE_MOLE_MASSES__
#define __SINGE_MOLE_MASSES__
const double molecular_masses[116] = {1.00797, 2.01594, 15.9994, 31.9988, 
  17.00737, 18.01534, 33.00677, 34.01474, 28.01055, 44.00995, 30.02649, 
  29.01852, 46.02589, 32.04243, 48.04183, 47.03386, 16.04303, 15.03506, 
  30.07012, 29.06215, 28.05418, 27.04621, 26.03824, 44.05358, 43.04561, 
  42.03764, 41.02967, 75.04441, 62.06892000000001, 61.06095000000001, 58.08067, 
  89.0715, 90.07947, 56.06473, 55.05676, 58.08067, 42.08127, 41.0733, 40.06533, 
  40.06533, 39.05736, 75.08804000000001, 75.08804000000001, 56.06473, 56.10836, 
  57.11633, 55.10039, 54.09242, 89.11513000000001, 71.09979, 72.10776, 56.10836, 
  55.10039, 89.11513000000001, 89.11513000000001, 72.10776, 71.09979, 121.11393, 
  104.10656, 71.09979, 70.09182, 69.08385, 88.10716000000001, 103.09859, 
  70.13545000000001, 69.12748000000001, 69.12748000000001, 117.16931, 86.13485, 
  100.20557, 98.18963000000001, 98.18963000000001, 98.18963000000001, 
  97.18166000000001, 131.1964, 131.1964, 131.1964, 131.1964, 163.1952, 163.1952, 
  163.1952, 163.1952, 163.1952, 114.18903, 114.18903, 114.18903, 146.18783, 
  88.10716000000001, 57.0727, 55.10039, 98.18963000000001, 98.18963000000001, 
  97.18166000000001, 97.18166000000001, 131.1964, 82.14660000000001, 
  81.13863000000001, 81.13863000000001, 71.14342000000001, 86.13485, 114.23266, 
  112.21672, 112.21672, 145.22349, 145.22349, 128.21612, 128.21612, 160.21492, 
  177.22229, 177.22229, 145.22349, 160.21492, 163.1952, 163.1952, 146.18783, 
  28.0134}; 
#endif


#ifndef __SINGE_RECIP_MOLE_MASSES__
#define __SINGE_RECIP_MOLE_MASSES__
const double recip_molecular_masses[116] = {0.9920930186414277, 
  0.4960465093207139, 0.06250234383789392, 0.03125117191894696, 
  0.05879803873262004, 0.05550825019122593, 0.03029681486555637, 
  0.02939901936631002, 0.03570083414998991, 0.02272213442641948, 
  0.03330392596670473, 0.03446075127194632, 0.02172690196756651, 
  0.03120861932131863, 0.02081519375927187, 0.021261278576753, 
  0.06233236489615739, 0.06651120780362699, 0.03325560390181349, 
  0.0344090165386938, 0.03564531203549703, 0.0369737571363973, 
  0.0384050534905585, 0.02269963076780593, 0.02323117270262868, 
  0.02378820504671527, 0.02437260645771706, 0.01332544289441412, 
  0.01611112292593459, 0.01637707896781822, 0.01721743223692151, 
  0.01122693566404518, 0.01110130865556824, 0.01783652574443861, 
  0.01816307388956415, 0.01721743223692151, 0.02376354135699802, 
  0.02434671672351625, 0.02495923532889907, 0.02495923532889907, 
  0.02560336899370566, 0.01331770012907515, 0.01331770012907515, 
  0.01783652574443861, 0.01782265601774851, 0.01750812771058645, 
  0.01814869186951308, 0.01848687856819865, 0.01122143905305418, 
  0.01406473915042506, 0.01386813291662368, 0.01782265601774851, 
  0.01814869186951308, 0.01122143905305418, 0.01122143905305418, 
  0.01386813291662368, 0.01406473915042506, 8.256688557625038e-03, 
  9.605542628629742e-03, 0.01406473915042506, 0.01426700005792402, 
  0.01447516315318269, 0.01134981538390296, 9.699453697669386e-03, 
  0.01425812481419881, 0.01446602711396394, 0.01446602711396394, 
  8.534658094342281e-03, 0.01160970269292859, 9.979485172331238e-03, 
  0.01018437486728486, 0.01018437486728486, 0.01018437486728486, 
  0.01029000739439931, 7.622160364156333e-03, 7.622160364156333e-03, 
  7.622160364156333e-03, 7.622160364156333e-03, 6.127631204839358e-03, 
  6.127631204839358e-03, 6.127631204839358e-03, 6.127631204839358e-03, 
  6.127631204839358e-03, 8.757408658257278e-03, 8.757408658257278e-03, 
  8.757408658257278e-03, 6.840514699479429e-03, 0.01134981538390296, 
  0.01752151203640269, 0.01814869186951308, 0.01018437486728486, 
  0.01018437486728486, 0.01029000739439931, 0.01029000739439931, 
  7.622160364156333e-03, 0.01217335836175812, 0.01232458571213243, 
  0.01232458571213243, 0.01405611369259448, 0.01160970269292859, 
  8.754063855293223e-03, 8.911328008874257e-03, 8.911328008874257e-03, 
  6.885938356115805e-03, 6.885938356115805e-03, 7.799331316530245e-03, 
  7.799331316530245e-03, 6.241615949376e-03, 5.642631070843289e-03, 
  5.642631070843289e-03, 6.885938356115805e-03, 6.241615949376e-03, 
  6.127631204839358e-03, 6.127631204839358e-03, 6.840514699479429e-03, 
  0.03569720205330306}; 
#endif


void avx_conductivity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *lambda); 
void avx_viscosity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *viscosity); 
void avx_diffusion(const double *temperature_array, const double 
  *pressure_array, const double *mass_frac_array, const double *mixmw_array, 
  const int num_elmts, const int spec_stride, double *diffusion); 
void avx_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *mixmw_array, const int num_elmts, const int spec_stride, double 
  *thermal); 

#endif // __AVX_CPU_GET_COEFFS__

