
#ifndef __RHSF_GPU__
#define __RHSF_GPU__

void run_gpu_conductivity(const double *temp_array, const double *mole_frac_array,
                          const double *mole_avg_array, const int nx, const int ny, 
                          const int nz, double *conductivity);

void run_gpu_viscosity(const double *temp_array, const double *mole_frac_array,
                       const double *mole_avg_array, const int nx, const int ny, 
                       const int nz, double *viscosity);

void run_gpu_diffusion(const double *temp_array, const double *pres_array,
                       const double *mole_frac_array, const double *mole_avg_array,
                       const int nx, const int ny, const int nz, double *diffusion);

void run_gpu_thermal(const double *temp_array, const double *mole_frac_array,
                     const double *mole_avg_array, const int nx, const int ny, 
                     const int nz, double *thermal);

void run_gpu_getrates(const double *temp_array, const double *pres_array,
                  const double *mass_frac_array, const double *avmolwt_array,
                  const double *diffusion_array, const int nx, const int ny, 
                  const int nz, const double dt, double *wdot);

#endif // __RHSF_GPU__

// EOF

