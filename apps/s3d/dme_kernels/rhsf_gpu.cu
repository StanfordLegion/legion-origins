
#include "rhsf_gpu.h"

#include "cuda.h"
#include "cuda_runtime.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>

//#define DEBUG_CUDA

#ifdef DEBUG_CUDA
#define CUDA_SAFE_CALL(expr)                        \
  {                                                 \
    cudaError_t err = (expr);                       \
    if (err != cudaSuccess)                         \
    {                                               \
      printf("Cuda error: %s\n", cudaGetErrorString(err));  \
      exit(1);                                      \
    }                                               \
  }
#else
#define CUDA_SAFE_CALL(expr)  expr
#endif

extern __global__ void
gpu_conductivity(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *conductivity);

__host__
void run_gpu_conductivity(const double *temp_array, const double *mole_frac_array,
                          const double *mole_avg_array, const int nx, const int ny, 
                          const int nz, double *conductivity)
{
#ifdef K20_ARCH
  assert(((nx*ny*nz) % 128) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));
  gpu_conductivity<<<grid,block>>>(temp_array, mole_frac_array,
                                                 mole_avg_array,
                                                 nx*ny, nx, nz, nx*ny*nz,
                                                 conductivity);
#else
  assert(((nx*ny*nz) % 256) == 0);
  dim3 grid((nx*ny*nz)/256,1,1);
  dim3 block(256,1,1);
  gpu_conductivity<<<grid,block>>>(temp_array, mole_frac_array,
                                                 mole_avg_array,
                                                 nx*ny, nx, nz, nx*ny*nz,
                                                 conductivity);
#endif
}

extern __global__ void
gpu_viscosity(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *viscosity);

__host__
void run_gpu_viscosity(const double *temp_array, const double *mole_frac_array,
                       const double *mole_avg_array, const int nx, const int ny, 
                       const int nz, double *viscosity)
{
#ifdef K20_ARCH
  dim3 grid((nx*ny)/32,1,1);
  dim3 block(15*32,1,1);
  CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));
  gpu_viscosity<<<grid,block>>>(temp_array, mole_frac_array,
                                              mole_avg_array,
                                              nx*ny, nx, nz, nx*ny*nz,
                                              viscosity);
#else
  assert(((nx*ny*nz) % 32) == 0);
  dim3 grid((nx*ny*nz)/32,1,1);
  dim3 block(12*32,1,1);
  gpu_viscosity<<<grid,block>>>(temp_array, mole_frac_array,
                                              mole_avg_array,
                                              nx*ny, nx, nz, nx*ny*nz,
                                              viscosity);
#endif
}

extern __global__ void
gpu_diffusion(const double *temperature_array, const double *pressure_array, 
  const double *mass_frac_array, const double *avmolwt_array, const int 
  slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX in number 
  of doubles*/, const int total_steps/*NZ in number of doubles*/, const int 
  spec_stride/*NX*NY*NZ in number of doubles*/, double *diffusion);

__host__
void run_gpu_diffusion(const double *temp_array, const double *pres_array,
                       const double *mole_frac_array, const double *mole_avg_array,
                       const int nx, const int ny, const int nz, double *diffusion)
{
#ifdef K20_ARCH
  assert(((nx*ny) % 32) == 0);
  dim3 grid((nx*ny)/32,1,1);
  dim3 block(10*32,1,1);
  CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));
  gpu_diffusion<<<grid,block>>>(temp_array, pres_array,
                                              mole_frac_array, mole_avg_array,
                                              nx*ny, nx, nz, nx*ny*nz,
                                              diffusion);
#else
  assert(((nx*ny) % 32) == 0);
  dim3 grid((nx*ny)/32,1,1);
  dim3 block(10*32,1,1);
  gpu_diffusion<<<grid,block>>>(temp_array, pres_array,
                                              mole_frac_array, mole_avg_array,
                                              nx*ny, nx, nz, nx*ny*nz,
                                              diffusion);
#endif
}

extern __global__ void
gpu_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *thermal_out);

__host__
void run_gpu_thermal(const double *temp_array, const double *mole_frac_array,
                     const double *mole_avg_array, const int nx, const int ny, 
                     const int nz, double *thermal)
{
  assert(((nx*ny*nz) % 128) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  gpu_thermal<<<grid,block>>>(temp_array, mole_frac_array,
                                            mole_avg_array,
                                            nx*ny, nx, nz, nx*ny*nz,
                                            thermal);
}

extern __global__ void
gpu_getrates(const double *temperature_array, const double *pressure_array, 
  const double *avmolwt_array, const double *mass_frac_array, const double 
  *diffusion_array, const double dt, const double recip_dt, const int 
  slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX in number 
  of doubles*/, const int total_steps/*NZ in number of doubles*/, const int 
  spec_stride/*NX*NY*NZ in number of doubles*/, const int step_stride/*always 
  zero*/, double *wdot_array);

__host__
void run_gpu_getrates(const double *temp_array, const double *pres_array,
                  const double *mass_frac_array, const double *avmolwt_array,
                  const double *diffusion_array, const int nx, const int ny, 
                  const int nz, const double dt, double *wdot)
{
#ifndef FAST_GPU_CHEMISTRY 
  assert(((nx*ny*nz) % 128) == 0);
  dim3 grid((nx*ny*nz)/128,1,1);
  dim3 block(128,1,1);
  gpu_getrates<<<grid,block>>>(temp_array, pres_array,
                                             avmolwt_array, mass_frac_array, diffusion_array,
                                             dt, (1.0/dt), nx*ny, nx, nz, nx*ny*nz, 0, wdot);
#else
  assert(((nx*ny) % 32) == 0);
  dim3 grid((nx*ny)/32,1,1);
#ifdef K20_ARCH
  dim3 block(8*32,1,1);
  CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));
#else
  dim3 block(16*32,1,1);
#endif
  gpu_getrates<<<grid,block>>>(temp_array, pres_array,
                                             avmolwt_array, mass_frac_array, diffusion_array,
                                             dt, (1.0/dt), nx*ny, nx, nz, nx*ny*nz, 0,
                                             wdot);
#endif
}


