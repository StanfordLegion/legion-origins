
/*
 * Baseline chemistry kernel for DME on Fermi.
 * Best version runs with 4 CTAs with 4 warps each.
 * Should compile to 63 registers and spill 5732 bytes.
 * Completely memory latency limited.
 *
 * Launch with:
 * dim3 grid((nx*ny*nz)/128,1,1);
 * dim3 block(128,1,1);
 *
 * gpu_getrates<<<grid,block>>>(temperature_ptr,
 *                      pressure_ptr, avmolwt_ptr, mass_frac_ptr,
 *                      diffusion_ptr, dt, nx*ny, nx, nz, nx*ny*nz,
 *                      0, wdot_ptr);
 *
 * Performance numbers on C2070 with 14 SMs:
 *
 * 32x32x32
 *   Latency: 13.706 ms
 *   Throughput: 2.391 Mpoints/s
 *
 * 64x64x64
 *   Latency: 100.989 ms
 *   Throughput: 2.596 Mpoints/s
 *
 * 128x128x128
 *   Latency: 791.364 ms
 *   Throughput: 2.650 Mpoints/s
 *
 * Generation command:
 *  ./singe --dir inputs/DME --cuda
 *
 */

#ifndef MAX
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

__constant__ double molecular_masses[30] = {1.00797, 2.01594, 15.03506, 15.9994, 
  16.04303, 17.00737, 18.01534, 26.03824, 28.01055, 28.05418, 29.06215, 
  30.02649, 30.07012, 31.03446, 31.9988, 33.00677, 34.01474, 44.00995, 44.05358, 
  46.02589, 46.06952, 59.04501, 60.05298000000001, 62.06892000000001, 75.04441, 
  75.04441, 77.06035, 92.05178000000001, 109.05915, 28.0134}; 


__constant__ double recip_molecular_masses[30] = {0.9920930186414277, 
  0.4960465093207139, 0.06651120780362699, 0.06250234383789392, 
  0.06233236489615739, 0.05879803873262004, 0.05550825019122593, 
  0.0384050534905585, 0.03570083414998991, 0.03564531203549703, 
  0.0344090165386938, 0.03330392596670473, 0.03325560390181349, 
  0.03222224585186918, 0.03125117191894696, 0.03029681486555637, 
  0.02939901936631002, 0.02272213442641948, 0.02269963076780593, 
  0.02172690196756651, 0.02170632557057247, 0.01693623220658274, 
  0.01665196298335236, 0.01611112292593459, 0.01332544289441412, 
  0.01332544289441412, 0.01297684217629429, 0.01086345098378326, 
  9.169336089635763e-03, 0.03569720205330306}; 


__global__ void
gpu_getrates(const double *temperature_array, const double *pressure_array, 
  const double *avmolwt_array, const double *mass_frac_array, const double 
  *diffusion_array, const double dt, const double recip_dt, const int 
  slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX in number 
  of doubles*/, const int total_steps/*NZ in number of doubles*/, const int 
  spec_stride/*NX*NY*NZ in number of doubles*/, const int step_stride/*always 
  zero*/, double *wdot_array) 
{
  
  const double PA = 1.013250e+06;
  // Scaled R0 for non-dimensionalization
  const double R0 = 9.977411999999998e+09;
  // Scaled R0c for non-dimensionalization
  const double R0c = 238.4658699839999;
  const double DLn10 = 2.3025850929940459e0;
  
  {
    const int offset = (blockIdx.x*blockDim.x + threadIdx.x);
    temperature_array += offset;
    pressure_array += offset;
    avmolwt_array += offset;
    mass_frac_array += offset;
    diffusion_array += offset;
    wdot_array += offset;
  }
  double temperature;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  const double otc     = 1.0 / temperature;
  const double ortc    = 1.0 / (temperature * R0c);
  const double vlntemp = log(temperature);
  const double prt     = PA / (R0 * temperature);
  const double oprt    = 1.0 / prt;
  
  double mass_frac[30];
  double avmolwt;
  double pressure;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[0]) : 
    "l"(mass_frac_array+0*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[1]) : 
    "l"(mass_frac_array+1*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[2]) : 
    "l"(mass_frac_array+2*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[3]) : 
    "l"(mass_frac_array+3*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[4]) : 
    "l"(mass_frac_array+4*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[5]) : 
    "l"(mass_frac_array+5*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[6]) : 
    "l"(mass_frac_array+6*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[7]) : 
    "l"(mass_frac_array+7*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[8]) : 
    "l"(mass_frac_array+8*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[9]) : 
    "l"(mass_frac_array+9*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[10]) : 
    "l"(mass_frac_array+10*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[11]) : 
    "l"(mass_frac_array+11*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[12]) : 
    "l"(mass_frac_array+12*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[13]) : 
    "l"(mass_frac_array+13*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[14]) : 
    "l"(mass_frac_array+14*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[15]) : 
    "l"(mass_frac_array+15*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[16]) : 
    "l"(mass_frac_array+16*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[17]) : 
    "l"(mass_frac_array+17*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[18]) : 
    "l"(mass_frac_array+18*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[19]) : 
    "l"(mass_frac_array+19*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[20]) : 
    "l"(mass_frac_array+20*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[21]) : 
    "l"(mass_frac_array+21*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[22]) : 
    "l"(mass_frac_array+22*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[23]) : 
    "l"(mass_frac_array+23*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[24]) : 
    "l"(mass_frac_array+24*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[25]) : 
    "l"(mass_frac_array+25*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[26]) : 
    "l"(mass_frac_array+26*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[27]) : 
    "l"(mass_frac_array+27*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[28]) : 
    "l"(mass_frac_array+28*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac[29]) : 
    "l"(mass_frac_array+29*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(avmolwt) : "l"(avmolwt_array) 
    : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(pressure) : 
    "l"(pressure_array) : "memory"); 
  double cgspl[39];
  // Gibbs computation
  {
    const double &tk1 = temperature;
    double tklog = log(tk1);
    double tk2 = tk1 * tk1;
    double tk3 = tk1 * tk2;
    double tk4 = tk1 * tk3;
    double tk5 = tk1 * tk4;
    
    // Species H
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[0] = 2.5*tk1*(-3.787491742782046-tklog) + -0.0*tk2 + -0.0*tk3 + 
          -0.0*tk4 + -0.0*tk5 + (212.2635833333334 - tk1*-0.4601176); 
      }
      else
      {
        cgspl[0] = 2.5*tk1*(-3.787491742782046-tklog) + -0.0*tk2 + -0.0*tk3 + 
          -0.0*tk4 + -0.0*tk5 + (212.2635833333334 - tk1*-0.4601176); 
      }
    }
    // Species H2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[1] = 2.991423*tk1*(-3.787491742782046-tklog) + 
          -0.04200386399999999*tk2 + 1.352118959999999e-04*tk3 + 
          1.329347231999999e-06*tk4 + -1.640997273599998e-08*tk5 + 
          (-6.958616666666668 - tk1*-1.35511); 
      }
      else
      {
        cgspl[1] = 3.298124*tk1*(-3.787491742782046-tklog) + 
          -0.04949665199999999*tk2 + 1.954323599999999e-03*tk3 + 
          1.364462495999999e-05*tk4 + -4.287035289599994e-06*tk5 + (-8.437675 - 
          tk1*-3.294094); 
      }
    }
    // Species CH2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[2] = 2.87410113*tk1*(-3.787491742782046-tklog) + 
          -0.2193835751999999*tk2 + 3.381470327999999e-03*tk3 + 
          -3.746585505599996e-05*tk4 + 1.946359414655998e-07*tk5 + 
          (385.5300333333334 - tk1*6.17119324); 
      }
      else
      {
        cgspl[2] = 3.76267867*tk1*(-3.787491742782046-tklog) + 
          -0.05813232857999999*tk2 + -6.707756183999998e-03*tk3 + 
          5.545312603199995e-04*tk4 + -1.749514142591998e-05*tk5 + 
          (383.3670008333334 - tk1*1.56253185); 
      }
    }
    // Species CH2(S)
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[3] = 2.29203842*tk1*(-3.787491742782046-tklog) + 
          -0.2793531821999999*tk2 + 4.828606727999998e-03*tk3 + 
          -6.017846399999994e-05*tk4 + 3.522179272319995e-07*tk5 + 
          (424.3833308333334 - tk1*8.62650169); 
      }
      else
      {
        cgspl[3] = 4.19860411*tk1*(-3.787491742782046-tklog) + 0.1419968514*tk2 
          + -0.01975910927999999*tk3 + 9.63095012639999e-04*tk4 + 
          -2.014655193215998e-05*tk5 + (420.8068025000001 - tk1*-0.769118967); 
      }
    }
    // Species CH3
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[4] = 2.9781206*tk1*(-3.787491742782046-tklog) + 
          -0.3478711199999999*tk2 + 4.741391999999999e-03*tk3 + 
          -4.425089759999996e-05*tk4 + 1.857677690879998e-07*tk5 + (137.579275 - 
          tk1*4.7224799); 
      }
      else
      {
        cgspl[4] = 3.6571797*tk1*(-3.787491742782046-tklog) + -0.127595874*tk2 + 
          -0.01310013191999999*tk3 + 9.53006443199999e-04*tk4 + 
          -2.556445432319997e-05*tk5 + (136.8559666666667 - tk1*1.6735354); 
      }
    }
    // Species O
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[5] = 2.54206*tk1*(-3.787491742782046-tklog) + 1.6530372e-03*tk2 + 
          7.446727199999996e-06*tk3 + -6.553536479999994e-07*tk4 + 
          4.528796313599995e-09*tk5 + (243.59 - tk1*4.920308); 
      }
      else
      {
        cgspl[5] = 2.946429*tk1*(-3.787491742782046-tklog) + 
          0.09828995999999997*tk2 + -5.810476799999997e-03*tk3 + 
          2.308093919999998e-04*tk4 + -4.033873612799995e-06*tk5 + (242.897 - 
          tk1*2.963995); 
      }
    }
    // Species CH4
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[6] = 1.683479*tk1*(-3.787491742782046-tklog) + 
          -0.6142343999999998*tk2 + 9.300309599999996e-03*tk3 + 
          -9.771242399999991e-05*tk4 + 4.669148966399994e-07*tk5 + 
          (-84.00658333333335 - tk1*9.623395); 
      }
      else
      {
        cgspl[6] = 0.7787415*tk1*(-3.787491742782046-tklog) + -1.0486008*tk2 + 
          0.06680181599999997*tk3 + -4.391579519999995e-03*tk4 + 
          1.268971660799999e-04*tk5 + (-81.87690833333335 - tk1*13.72219); 
      }
    }
    // Species OH
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[7] = 2.86472886*tk1*(-3.787491742782046-tklog) + 
          -0.06339026879999998*tk2 + 6.217986191999998e-04*tk3 + 
          -4.395148905599995e-06*tk4 + 1.380974842367998e-08*tk5 + (30.69690625 
          - tk1*5.70164073); 
      }
      else
      {
        cgspl[7] = 4.12530561*tk1*(-3.787491742782046-tklog) + 0.1935269634*tk2 
          + -0.015666352584*tk3 + 8.349892459199992e-04*tk4 + 
          -2.138269145471997e-05*tk5 + (27.88590941666667 - tk1*-0.69043296); 
      }
    }
    // Species H2O
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[8] = 2.672146*tk1*(-3.787491742782046-tklog) + -0.18337758*tk2 + 
          2.095262399999999e-03*tk3 + -1.729434239999998e-05*tk4 + 
          6.626829542399991e-08*tk5 + (-249.1600833333334 - tk1*6.862817); 
      }
      else
      {
        cgspl[8] = 3.386842*tk1*(-3.787491742782046-tklog) + 
          -0.2084989199999999*tk2 + 0.01525127039999999*tk3 + 
          -1.003475663999999e-03*tk4 + 2.598830438399997e-05*tk5 + 
          (-251.7342500000001 - tk1*2.590233); 
      }
    }
    // Species C2H2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[9] = 4.14756964*tk1*(-3.787491742782046-tklog) + 
          -0.3576999983999999*tk2 + 5.695076447999998e-03*tk3 + 
          -6.730735262399994e-05*tk4 + 3.745286688383996e-07*tk5 + 
          (216.1333266666667 - tk1*-1.23028121); 
      }
      else
      {
        cgspl[9] = 0.808681094*tk1*(-3.787491742782046-tklog) + -1.401693774*tk2 
          + 0.08524123559999996*tk3 + -4.034195092799996e-03*tk4 + 
          8.813556594431989e-05*tk5 + (220.2415058333334 - tk1*13.9397051); 
      }
    }
    // Species C2H3
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[10] = 3.016724*tk1*(-3.787491742782046-tklog) + 
          -0.6198137519999999*tk2 + 0.01123397637599999*tk3 + 
          -1.465391347199998e-04*tk4 + 8.943509801087989e-07*tk5 + 
          (288.4406158333334 - tk1*7.78732378); 
      }
      else
      {
        cgspl[10] = 3.21246645*tk1*(-3.787491742782046-tklog) + 
          -0.09088749719999999*tk2 + -0.06221025887999997*tk3 + 
          5.150272996799995e-03*tk4 + -1.525660251263998e-04*tk5 + 
          (290.4987233333334 - tk1*8.51054025); 
      }
    }
    // Species CO
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[11] = 3.025078*tk1*(-3.787491742782046-tklog) + 
          -0.08656133999999999*tk2 + 1.351398719999999e-03*tk3 + 
          -1.466756639999998e-05*tk4 + 7.165275033599991e-08*tk5 + 
          (-118.9029166666667 - tk1*6.108218); 
      }
      else
      {
        cgspl[11] = 3.262452*tk1*(-3.787491742782046-tklog) + 
          -0.09071645999999997*tk2 + 9.316211999999997e-03*tk3 + 
          -8.037999359999992e-04*tk4 + 2.566029196799997e-05*tk5 + (-119.2545 - 
          tk1*4.848897); 
      }
    }
    // Species C2H4
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[12] = 2.03611116*tk1*(-3.787491742782046-tklog) + 
          -0.8787249059999998*tk2 + 0.01610586995999999*tk3 + 
          -2.120010091199998e-04*tk4 + 1.303320440447998e-06*tk5 + 
          (41.16571783333334 - tk1*10.3053693); 
      }
      else
      {
        cgspl[12] = 3.95920148*tk1*(-3.787491742782046-tklog) + 
          0.4542313481999999*tk2 + -0.13703767008*tk3 + 9.95887804319999e-03*tk4 
          + -2.798161179263997e-04*tk5 + (42.41479941666667 - tk1*4.09733096); 
      }
    }
    // Species HCO
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[13] = 3.557271*tk1*(-3.787491742782046-tklog) + -0.20073438*tk2 + 
          3.204014399999999e-03*tk3 + -3.557625119999996e-05*tk4 + 
          1.776920716799998e-07*tk5 + (32.63603333333334 - tk1*5.552299); 
      }
      else
      {
        cgspl[13] = 2.89833*tk1*(-3.787491742782046-tklog) + -0.37194882*tk2 + 
          0.02309540159999999*tk3 + -1.569347999999998e-03*tk4 + 
          4.743240767999994e-05*tk5 + (34.66601666666667 - 
          tk1*8.983613999999999); 
      }
    }
    // Species C2H5
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[14] = 4.2878814*tk1*(-3.787491742782046-tklog) + 
          -0.7460335799999998*tk2 + 0.01059338855999999*tk3 + 
          -1.017419068799999e-04*tk4 + 4.358202900479995e-07*tk5 + 
          (100.4704583333334 - tk1*0.84602583); 
      }
      else
      {
        cgspl[14] = 4.305858*tk1*(-3.787491742782046-tklog) + 
          0.2510018279999999*tk2 + -0.119297448*tk3 + 8.626445855999991e-03*tk4 
          + -2.389666199039997e-04*tk5 + (107.0142833333334 - tk1*4.7100236); 
      }
    }
    // Species CH2O
    {
      if (tk1 > 10.0)
      {
        cgspl[15] = 5.1481905*tk1*(-3.787491742782046-tklog) + 
          -0.1720680959999999*tk2 + 5.707831919999998e-04*tk3 + 
          2.320027631999998e-05*tk4 + -2.961799084799996e-07*tk5 + 
          (-135.2514416666667 - tk1*-5.1213813); 
      }
      else
      {
        cgspl[15] = 2.6962612*tk1*(-3.787491742782046-tklog) + 
          -0.2955685379999999*tk2 + -1.987835855999999e-03*tk3 + 
          7.925500223999992e-05*tk4 + 4.106798599679995e-06*tk5 + 
          (-124.7566083333334 - tk1*9.4697599); 
      }
    }
    // Species C2H6
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[16] = 4.825938*tk1*(-3.787491742782046-tklog) + 
          -0.8304257999999999*tk2 + 0.0109374216*tk3 + -9.68395247999999e-05*tk4 
          + 3.730573324799995e-07*tk5 + (-105.9815833333334 - tk1*-5.239507); 
      }
      else
      {
        cgspl[16] = 1.462539*tk1*(-3.787491742782046-tklog) + 
          -0.9296801999999998*tk2 + -0.01387321679999999*tk3 + 
          1.811278079999998e-03*tk4 + -4.755041625599994e-05*tk5 + 
          (-93.65983333333335 - tk1*14.43229); 
      }
    }
    // Species CH2OH
    {
      if (tk1 > 6.250000000000002)
      {
        cgspl[17] = 3.7469103*tk1*(-3.787491742782046-tklog) + 
          -0.5318767259999999*tk2 + 0.01021937328*tk3 + 
          -1.452677759999998e-04*tk4 + 9.797921844479988e-07*tk5 + (-30.55402 - 
          tk1*5.4281095); 
      }
      else
      {
        cgspl[17] = 4.6119792*tk1*(-3.787491742782046-tklog) + 0.18722256*tk2 + 
          -0.08527603199999997*tk3 + 7.110633311999993e-03*tk4 + 
          -2.283784968959997e-04*tk5 + (-30.03394500000001 - tk1*2.8351399); 
      }
    }
    // Species CH3O
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[18] = 3.7708*tk1*(-3.787491742782046-tklog) + 
          -0.4722898199999999*tk2 + 6.375321599999997e-03*tk3 + 
          -5.679980639999994e-05*tk4 + 2.190360268799997e-07*tk5 + 
          (1.065270833333334 - tk1*2.929575); 
      }
      else
      {
        cgspl[18] = 2.106204*tk1*(-3.787491742782046-tklog) + 
          -0.4329956999999999*tk2 + -0.01281233279999999*tk3 + 
          1.062379583999999e-03*tk4 + -2.151993484799997e-05*tk5 + 
          (8.155009166666668 - tk1*13.15218); 
      }
    }
    // Species O2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[19] = 3.697578*tk1*(-3.787491742782046-tklog) + 
          -0.03681118199999999*tk2 + 3.021220799999999e-04*tk3 + 
          -2.556404639999998e-06*tk4 + 1.178255807999999e-08*tk5 + (-10.28275 - 
          tk1*3.189166); 
      }
      else
      {
        cgspl[19] = 3.212936*tk1*(-3.787491742782046-tklog) + 
          -0.06764915999999999*tk2 + 1.381476e-03*tk3 + 
          -1.891982879999998e-04*tk4 + 9.091236787199989e-06*tk5 + 
          (-8.377075000000001 - tk1*6.034738); 
      }
    }
    // Species HO2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[20] = 4.0172109*tk1*(-3.787491742782046-tklog) + -0.1343892078*tk2 
          + 1.520779559999999e-03*tk3 + -1.645147727999999e-05*tk4 + 
          1.118795690879999e-07*tk5 + (0.9321392750000002 - tk1*3.78510215); 
      }
      else
      {
        cgspl[20] = 4.30179801*tk1*(-3.787491742782046-tklog) + 
          0.2849472305999999*tk2 + -0.05077989383999998*tk3 + 
          3.495800073599997e-03*tk4 + -9.634206085631988e-05*tk5 + 
          (2.456733666666667 - tk1*3.71666245); 
      }
    }
    // Species H2O2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[21] = 4.573167*tk1*(-3.787491742782046-tklog) + 
          -0.2601681599999999*tk2 + 3.539253599999998e-03*tk3 + 
          -3.382421759999996e-05*tk4 + 1.484338867199998e-07*tk5 + (-150.058 - 
          tk1*0.5011370000000001); 
      }
      else
      {
        cgspl[21] = 3.388754*tk1*(-3.787491742782046-tklog) + 
          -0.3941535599999999*tk2 + 3.564031199999998e-04*tk3 + 
          6.661160639999993e-04*tk4 + -2.562466751999997e-05*tk5 + 
          (-147.1929166666667 - tk1*6.785363); 
      }
    }
    // Species CO2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[22] = 4.453623*tk1*(-3.787491742782046-tklog) + 
          -0.1884101399999999*tk2 + 3.068186399999999e-03*tk3 + 
          -3.447355679999996e-05*tk4 + 1.730453414399998e-07*tk5 + (-408.058 - 
          tk1*-0.9553959); 
      }
      else
      {
        cgspl[22] = 2.275725*tk1*(-3.787491742782046-tklog) + 
          -0.5953243199999999*tk2 + 0.02498186399999999*tk3 + 
          -9.888029279999989e-04*tk4 + 2.195195903999997e-05*tk5 + 
          (-403.1095000000001 - tk1*10.18849); 
      }
    }
    // Species CH3HCO
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[23] = 5.4041108*tk1*(-3.787491742782046-tklog) + 
          -0.7033835399999998*tk2 + 0.01014315288*tk3 + 
          -9.84563294399999e-05*tk4 + 4.249310595839995e-07*tk5 + 
          (-188.2760166666667 - tk1*-3.4807917); 
      }
      else
      {
        cgspl[23] = 4.7294595*tk1*(-3.787491742782046-tklog) + 
          0.1915971479999999*tk2 + -0.1140838104*tk3 + 8.274039983999991e-03*tk4 
          + -2.273817692159997e-04*tk5 + (-179.7739833333334 - tk1*4.1030159); 
      }
    }
    // Species CH3OCH2
    {
      if (tk1 > 11.46666666666667)
      {
        cgspl[24] = 8.17137842*tk1*(-3.787491742782046-tklog) + 
          -0.6605170859999999*tk2 + 9.176454647999996e-03*tk3 + 
          -8.634775708799992e-05*tk4 + 3.632091974783995e-07*tk5 + (-28.49513375 
          - tk1*-17.8650856); 
      }
      else
      {
        cgspl[24] = 2.91327415*tk1*(-3.787491742782046-tklog) + -1.220187954*tk2 
          + 0.02303309620799999*tk3 + -2.987690759999997e-04*tk4 + 
          1.776487977215998e-06*tk5 + (-9.903686666666667 - tk1*11.6066817); 
      }
    }
    // Species HCOOH
    {
      if (tk1 > 11.46666666666667)
      {
        cgspl[25] = 6.68733013*tk1*(-3.787491742782046-tklog) + 
          -0.3085736207999999*tk2 + 4.373724311999999e-03*tk3 + 
          -4.171955947199996e-05*tk4 + 1.771810319231998e-07*tk5 + 
          (-403.3295000000001 - tk1*-11.3104798); 
      }
      else
      {
        cgspl[25] = 1.43548185*tk1*(-3.787491742782046-tklog) + 
          -0.9801780959999997*tk2 + 0.02550178103999999*tk3 + 
          -4.782714868799995e-04*tk4 + 4.169761835903994e-06*tk5 + 
          (-387.1804200000001 - tk1*17.2885798); 
      }
    }
    // Species CH3OCH3
    {
      if (tk1 > 5.916666666666668)
      {
        cgspl[26] = 0.830815546*tk1*(-3.787491742782046-tklog) + 
          -1.615039578*tk2 + 0.03332994647999999*tk3 + 
          -5.004217137599995e-04*tk4 + 3.542815936511995e-06*tk5 + (-195.1008125 
          - tk1*20.217436); 
      }
      else
      {
        cgspl[26] = 5.68097447*tk1*(-3.787491742782046-tklog) + 
          0.3236608505999999*tk2 + -0.1558734599999999*tk3 + 
          0.01159294057919999*tk4 + -3.395250618623996e-04*tk5 + 
          (-199.7962125000001 - tk1*-0.636955496); 
      }
    }
    // Species HOCH2O
    {
      if (tk1 > 12.1)
      {
        cgspl[27] = 6.39521515*tk1*(-3.787491742782046-tklog) + 
          -0.4462038257999999*tk2 + 6.010136495999997e-03*tk3 + 
          -5.542267852799995e-05*tk4 + 2.299401447551997e-07*tk5 + 
          (-206.2503208333334 - tk1*-7.29290847); 
      }
      else
      {
        cgspl[27] = 4.11183145*tk1*(-3.787491742782046-tklog) + 
          -0.4523104181999999*tk2 + -9.056096879999996e-03*tk3 + 
          7.757942471999992e-04*tk4 + -1.509745516415998e-05*tk5 + (-195.345455 
          - tk1*6.81381989); 
      }
    }
    // Species CH3OCO
    {
      if (tk1 > 11.35)
      {
        cgspl[28] = 13.08776*tk1*(-3.787491742782046-tklog) + -0.27212697*tk2 + 
          3.962312735999999e-03*tk3 + -3.847640788799996e-05*tk4 + 
          1.654492915583998e-07*tk5 + (-205.5136666666667 - tk1*-32.7914051); 
      }
      else
      {
        cgspl[28] = 3.94199159*tk1*(-3.787491742782046-tklog) + -1.460609304*tk2 
          + 0.03974293439999999*tk3 + -6.602938718399993e-04*tk4 + 
          3.440057900543996e-06*tk5 + (-178.6706908333334 - tk1*16.6954362); 
      }
    }
    // Species CH3OCHO
    {
      if (tk1 > 14.05)
      {
        cgspl[29] = 8.69123518*tk1*(-3.787491742782046-tklog) + 
          -0.6930187319999997*tk2 + 0.010266779664*tk3 + 
          -1.011647604959999e-04*tk4 + 4.399490267135994e-07*tk5 + 
          (-386.9706408333334 - tk1*-18.9301478); 
      }
      else
      {
        cgspl[29] = 3.08839783*tk1*(-3.787491742782046-tklog) + -1.222560288*tk2 
          + 0.01643464895999999*tk3 + 1.048588132319999e-04*tk4 + 
          -5.828166079487992e-06*tk5 + (-368.2126391666667 - tk1*12.5364719); 
      }
    }
    // Species CH3OCH2O
    {
      if (tk1 > 16.76666666666667)
      {
        cgspl[30] = 8.60261845*tk1*(-3.787491742782046-tklog) + 
          -0.8146331699999998*tk2 + 0.011631878448*tk3 + 
          -1.119983317919999e-04*tk4 + 4.796585413631994e-07*tk5 + (-178.13537 - 
          tk1*-17.5775023); 
      }
      else
      {
        cgspl[30] = 3.25889339*tk1*(-3.787491742782046-tklog) + -1.332878154*tk2 
          + 0.01868535215999999*tk3 + 3.477371875199997e-05*tk4 + 
          -4.685449494527994e-06*tk5 + (-160.3143433333334 - tk1*12.3680069); 
      }
    }
    // Species CH3OCH2OH
    {
      if (tk1 > 16.78333333333334)
      {
        cgspl[31] = 8.7098157*tk1*(-3.787491742782046-tklog) + 
          -0.9216142319999998*tk2 + 0.01298409091199999*tk3 + 
          -1.239225762239999e-04*tk4 + 5.275443188735994e-07*tk5 + 
          (-397.1725958333334 - tk1*-18.0226702); 
      }
      else
      {
        cgspl[31] = 3.15851876*tk1*(-3.787491742782046-tklog) + -1.465954506*tk2 
          + 0.02080763481599999*tk3 + 8.543798323199991e-06*tk4 + 
          -4.524595231103994e-06*tk5 + (-378.7407491666668 - tk1*13.0511235); 
      }
    }
    // Species OCH2OCHO
    {
      if (tk1 > 12.29166666666667)
      {
        cgspl[32] = 12.0233916*tk1*(-3.787491742782046-tklog) + 
          -0.4867575953999999*tk2 + 6.992555087999997e-03*tk3 + 
          -6.729701529599993e-05*tk4 + 2.875829443199996e-07*tk5 + 
          (-361.3726925000001 - tk1*-33.3691809); 
      }
      else
      {
        cgspl[32] = 5.19690837*tk1*(-3.787491742782046-tklog) + 
          -0.9530383379999998*tk2 + -8.484973127999997e-04*tk3 + 
          8.790579691199991e-04*tk4 + -2.018253552767998e-05*tk5 + 
          (-335.2023266666667 - tk1*6.11645828); 
      }
    }
    // Species HOCH2OCO
    {
      if (tk1 > 13.35833333333334)
      {
        cgspl[33] = 11.3737391*tk1*(-3.787491742782046-tklog) + 
          -0.4905983387999999*tk2 + 7.008816503999998e-03*tk3 + 
          -6.720416870399994e-05*tk4 + 2.864438100863996e-07*tk5 + 
          (-387.9797858333334 - tk1*-28.6035265); 
      }
      else
      {
        cgspl[33] = 6.08180801*tk1*(-3.787491742782046-tklog) + 
          -0.7726101539999998*tk2 + -4.906066031999998e-03*tk3 + 
          8.786230862399991e-04*tk4 + -1.864379555711998e-05*tk5 + 
          (-366.2718191666668 - tk1*2.54054449); 
      }
    }
    // Species CH3OCH2O2
    {
      if (tk1 > 11.575)
      {
        cgspl[34] = 12.4249729*tk1*(-3.787491742782046-tklog) + 
          -0.7122359159999999*tk2 + 9.789756767999997e-03*tk3 + 
          -9.148475649599991e-05*tk4 + 3.830228125055995e-07*tk5 + (-191.399365 
          - tk1*-35.3740145); 
      }
      else
      {
        cgspl[34] = 2.21029612*tk1*(-3.787491742782046-tklog) + 
          -2.213264723999999*tk2 + 0.06781477319999997*tk3 + 
          -1.666519675199998e-03*tk4 + 2.043848712959998e-05*tk5 + 
          (-162.4507833333334 - tk1*19.1463601); 
      }
    }
    // Species CH2OCH2O2H
    {
      if (tk1 > 11.60833333333334)
      {
        cgspl[35] = 15.1191783*tk1*(-3.787491742782046-tklog) + 
          -0.5542313297999999*tk2 + 7.659060119999997e-03*tk3 + 
          -7.187251363199993e-05*tk4 + 3.018772675583996e-07*tk5 + 
          (-153.4290558333334 - tk1*-48.5706618); 
      }
      else
      {
        cgspl[35] = 2.52895507*tk1*(-3.787491742782046-tklog) + -2.54476974*tk2 
          + 0.08961753263999997*tk3 + -2.399606395199998e-03*tk4 + 
          3.073524258815996e-05*tk5 + (-120.2444216666667 - tk1*17.6899251); 
      }
    }
    // Species HO2CH2OCHO
    {
      if (tk1 > 11.55833333333334)
      {
        cgspl[36] = 16.4584298*tk1*(-3.787491742782046-tklog) + 
          -0.5116101065999999*tk2 + 7.298723999999997e-03*tk3 + 
          -6.992595475199992e-05*tk4 + 2.978895750911996e-07*tk5 + 
          (-519.9663400000001 - tk1*-53.8924139); 
      }
      else
      {
        cgspl[36] = 3.47935703*tk1*(-3.787491742782046-tklog) + -2.417714352*tk2 
          + 0.07922623103999997*tk3 + -1.934785684799998e-03*tk4 + 
          2.266461181439997e-05*tk5 + (-483.8582783333334 - tk1*15.2521392); 
      }
    }
    // Species O2CH2OCH2O2H
    {
      if (tk1 > 11.68333333333334)
      {
        cgspl[37] = 19.2038046*tk1*(-3.787491742782046-tklog) + 
          -0.6263690459999999*tk2 + 8.653990535999996e-03*tk3 + 
          -8.118616939199993e-05*tk4 + 3.409073194751995e-07*tk5 + 
          (-316.0058791666667 - tk1*-65.18472730000001); 
      }
      else
      {
        cgspl[37] = 1.99640551*tk1*(-3.787491742782046-tklog) + 
          -3.499357391999999*tk2 + 0.1327823467199999*tk3 + 
          -3.741271775999996e-03*tk4 + 4.946997939839994e-05*tk5 + 
          (-273.0239516666667 - tk1*24.4215005); 
      }
    }
    // Species N2
    {
      if (tk1 > 8.333333333333336)
      {
        cgspl[38] = 2.92664*tk1*(-3.787491742782046-tklog) + 
          -0.08927861999999998*tk2 + 1.364342639999999e-03*tk3 + 
          -1.453973759999998e-05*tk4 + 7.001874316799991e-08*tk5 + 
          (-7.689980833333334 - tk1*5.980528); 
      }
      else
      {
        cgspl[38] = 3.298677*tk1*(-3.787491742782046-tklog) + 
          -0.08449439999999998*tk2 + 9.511732799999995e-03*tk3 + 
          -8.123781599999992e-04*tk4 + 2.534825663999997e-05*tk5 + 
          (-8.507500000000002 - tk1*3.950372); 
      }
    }
  }
  
  double mole_frac[30];
  // Compute mole fractions
  {
    double sumyow = temperature * avmolwt * 7.03444160806564;
    sumyow = pressure/sumyow;
    mole_frac[0] = mass_frac[0] * recip_molecular_masses[0];
    mole_frac[0] = (mole_frac[0] > 1e-200) ? mole_frac[0] : 1e-200;
    mole_frac[0] *= sumyow;
    mole_frac[1] = mass_frac[1] * recip_molecular_masses[1];
    mole_frac[1] = (mole_frac[1] > 1e-200) ? mole_frac[1] : 1e-200;
    mole_frac[1] *= sumyow;
    mole_frac[2] = mass_frac[2] * recip_molecular_masses[2];
    mole_frac[2] = (mole_frac[2] > 1e-200) ? mole_frac[2] : 1e-200;
    mole_frac[2] *= sumyow;
    mole_frac[3] = mass_frac[3] * recip_molecular_masses[3];
    mole_frac[3] = (mole_frac[3] > 1e-200) ? mole_frac[3] : 1e-200;
    mole_frac[3] *= sumyow;
    mole_frac[4] = mass_frac[4] * recip_molecular_masses[4];
    mole_frac[4] = (mole_frac[4] > 1e-200) ? mole_frac[4] : 1e-200;
    mole_frac[4] *= sumyow;
    mole_frac[5] = mass_frac[5] * recip_molecular_masses[5];
    mole_frac[5] = (mole_frac[5] > 1e-200) ? mole_frac[5] : 1e-200;
    mole_frac[5] *= sumyow;
    mole_frac[6] = mass_frac[6] * recip_molecular_masses[6];
    mole_frac[6] = (mole_frac[6] > 1e-200) ? mole_frac[6] : 1e-200;
    mole_frac[6] *= sumyow;
    mole_frac[7] = mass_frac[7] * recip_molecular_masses[7];
    mole_frac[7] = (mole_frac[7] > 1e-200) ? mole_frac[7] : 1e-200;
    mole_frac[7] *= sumyow;
    mole_frac[8] = mass_frac[8] * recip_molecular_masses[8];
    mole_frac[8] = (mole_frac[8] > 1e-200) ? mole_frac[8] : 1e-200;
    mole_frac[8] *= sumyow;
    mole_frac[9] = mass_frac[9] * recip_molecular_masses[9];
    mole_frac[9] = (mole_frac[9] > 1e-200) ? mole_frac[9] : 1e-200;
    mole_frac[9] *= sumyow;
    mole_frac[10] = mass_frac[10] * recip_molecular_masses[10];
    mole_frac[10] = (mole_frac[10] > 1e-200) ? mole_frac[10] : 1e-200;
    mole_frac[10] *= sumyow;
    mole_frac[11] = mass_frac[11] * recip_molecular_masses[11];
    mole_frac[11] = (mole_frac[11] > 1e-200) ? mole_frac[11] : 1e-200;
    mole_frac[11] *= sumyow;
    mole_frac[12] = mass_frac[12] * recip_molecular_masses[12];
    mole_frac[12] = (mole_frac[12] > 1e-200) ? mole_frac[12] : 1e-200;
    mole_frac[12] *= sumyow;
    mole_frac[13] = mass_frac[13] * recip_molecular_masses[13];
    mole_frac[13] = (mole_frac[13] > 1e-200) ? mole_frac[13] : 1e-200;
    mole_frac[13] *= sumyow;
    mole_frac[14] = mass_frac[14] * recip_molecular_masses[14];
    mole_frac[14] = (mole_frac[14] > 1e-200) ? mole_frac[14] : 1e-200;
    mole_frac[14] *= sumyow;
    mole_frac[15] = mass_frac[15] * recip_molecular_masses[15];
    mole_frac[15] = (mole_frac[15] > 1e-200) ? mole_frac[15] : 1e-200;
    mole_frac[15] *= sumyow;
    mole_frac[16] = mass_frac[16] * recip_molecular_masses[16];
    mole_frac[16] = (mole_frac[16] > 1e-200) ? mole_frac[16] : 1e-200;
    mole_frac[16] *= sumyow;
    mole_frac[17] = mass_frac[17] * recip_molecular_masses[17];
    mole_frac[17] = (mole_frac[17] > 1e-200) ? mole_frac[17] : 1e-200;
    mole_frac[17] *= sumyow;
    mole_frac[18] = mass_frac[18] * recip_molecular_masses[18];
    mole_frac[18] = (mole_frac[18] > 1e-200) ? mole_frac[18] : 1e-200;
    mole_frac[18] *= sumyow;
    mole_frac[19] = mass_frac[19] * recip_molecular_masses[19];
    mole_frac[19] = (mole_frac[19] > 1e-200) ? mole_frac[19] : 1e-200;
    mole_frac[19] *= sumyow;
    mole_frac[20] = mass_frac[20] * recip_molecular_masses[20];
    mole_frac[20] = (mole_frac[20] > 1e-200) ? mole_frac[20] : 1e-200;
    mole_frac[20] *= sumyow;
    mole_frac[21] = mass_frac[21] * recip_molecular_masses[21];
    mole_frac[21] = (mole_frac[21] > 1e-200) ? mole_frac[21] : 1e-200;
    mole_frac[21] *= sumyow;
    mole_frac[22] = mass_frac[22] * recip_molecular_masses[22];
    mole_frac[22] = (mole_frac[22] > 1e-200) ? mole_frac[22] : 1e-200;
    mole_frac[22] *= sumyow;
    mole_frac[23] = mass_frac[23] * recip_molecular_masses[23];
    mole_frac[23] = (mole_frac[23] > 1e-200) ? mole_frac[23] : 1e-200;
    mole_frac[23] *= sumyow;
    mole_frac[24] = mass_frac[24] * recip_molecular_masses[24];
    mole_frac[24] = (mole_frac[24] > 1e-200) ? mole_frac[24] : 1e-200;
    mole_frac[24] *= sumyow;
    mole_frac[25] = mass_frac[25] * recip_molecular_masses[25];
    mole_frac[25] = (mole_frac[25] > 1e-200) ? mole_frac[25] : 1e-200;
    mole_frac[25] *= sumyow;
    mole_frac[26] = mass_frac[26] * recip_molecular_masses[26];
    mole_frac[26] = (mole_frac[26] > 1e-200) ? mole_frac[26] : 1e-200;
    mole_frac[26] *= sumyow;
    mole_frac[27] = mass_frac[27] * recip_molecular_masses[27];
    mole_frac[27] = (mole_frac[27] > 1e-200) ? mole_frac[27] : 1e-200;
    mole_frac[27] *= sumyow;
    mole_frac[28] = mass_frac[28] * recip_molecular_masses[28];
    mole_frac[28] = (mole_frac[28] > 1e-200) ? mole_frac[28] : 1e-200;
    mole_frac[28] *= sumyow;
    mole_frac[29] = mass_frac[29] * recip_molecular_masses[29];
    mole_frac[29] = (mole_frac[29] > 1e-200) ? mole_frac[29] : 1e-200;
    mole_frac[29] *= sumyow;
  }
  
  double thbctemp[7];
  // Computing third body values
  {
    double ctot = 0.0;
    ctot += mole_frac[0];
    ctot += mole_frac[1];
    ctot += mole_frac[2];
    ctot += mole_frac[3];
    ctot += mole_frac[4];
    ctot += mole_frac[5];
    ctot += mole_frac[6];
    ctot += mole_frac[7];
    ctot += mole_frac[8];
    ctot += mole_frac[9];
    ctot += mole_frac[10];
    ctot += mole_frac[11];
    ctot += mole_frac[12];
    ctot += mole_frac[13];
    ctot += mole_frac[14];
    ctot += mole_frac[15];
    ctot += mole_frac[16];
    ctot += mole_frac[17];
    ctot += mole_frac[18];
    ctot += mole_frac[19];
    ctot += mole_frac[20];
    ctot += mole_frac[21];
    ctot += mole_frac[22];
    ctot += mole_frac[23];
    ctot += mole_frac[24];
    ctot += mole_frac[25];
    ctot += mole_frac[26];
    ctot += mole_frac[27];
    ctot += mole_frac[28];
    ctot += mole_frac[29];
    thbctemp[0] = ctot;
    thbctemp[1] = ctot + 1.5*mole_frac[1] + 11.0*mole_frac[6] + 
      0.8999999999999999*mole_frac[8] + 2.8*mole_frac[17]; 
    thbctemp[2] = ctot + mole_frac[1] + 10.0*mole_frac[6] + 
      0.8999999999999999*mole_frac[8] - 0.22*mole_frac[14] + 2.8*mole_frac[17]; 
    thbctemp[3] = ctot + 1.5*mole_frac[1] + 5.0*mole_frac[6] + 
      0.8999999999999999*mole_frac[8] + 2.8*mole_frac[17]; 
    thbctemp[4] = ctot + 4.0*mole_frac[6] + mole_frac[8] + 2.0*mole_frac[17];
    thbctemp[5] = ctot + mole_frac[1] + mole_frac[4] + 5.0*mole_frac[6] + 
      0.5*mole_frac[8] + 2.0*mole_frac[12] + mole_frac[17]; 
    thbctemp[6] = ctot - mole_frac[6] - mole_frac[8] - mole_frac[17];
  }
  
  double rr_f[175];
  double rr_r[175];
  //   0)  H + O2 <=> O + OH
  {
    double forward = 5.606052175734568e+15 * exp(-0.406*vlntemp - 
      1.6599e+04*ortc); 
    double xik = -cgspl[0] + cgspl[5] + cgspl[7] - cgspl[19];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[0] = forward * mole_frac[0] * mole_frac[14];
    rr_r[0] = reverse * mole_frac[3] * mole_frac[5];
  }
  //   1)  H2 + O <=> H + OH
  {
    double forward = 1.996278696128123e+11 * exp(2.67*vlntemp - 6.29e+03*ortc);
    double xik = cgspl[0] - cgspl[1] - cgspl[5] + cgspl[7];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[1] = forward * mole_frac[1] * mole_frac[3];
    rr_r[1] = reverse * mole_frac[0] * mole_frac[5];
  }
  //   2)  H2 + OH <=> H + H2O
  {
    double forward = 3.288220749682686e+12 * exp(1.51*vlntemp - 3.43e+03*ortc);
    double xik = cgspl[0] - cgspl[1] - cgspl[7] + cgspl[8];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[2] = forward * mole_frac[1] * mole_frac[5];
    rr_r[2] = reverse * mole_frac[0] * mole_frac[6];
  }
  //   3)  O + H2O <=> 2 OH
  {
    double forward = 5.195724313044046e+11 * exp(2.02*vlntemp - 1.34e+04*ortc);
    double xik = -cgspl[5] + 2.0 * cgspl[7] - cgspl[8];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[3] = forward * mole_frac[3] * mole_frac[6];
    rr_r[3] = reverse * mole_frac[5] * mole_frac[5];
  }
  //   4)  H2 + M <=> 2 H + M
  {
    double forward = 6.203984453113697e+17 * exp(-1.4*vlntemp - 
      1.0438e+05*ortc); 
    double xik = 2.0 * cgspl[0] - cgspl[1];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[4] = forward * mole_frac[1];
    rr_r[4] = reverse * mole_frac[0] * mole_frac[0];
    rr_f[4] *= thbctemp[1];
    rr_r[4] *= thbctemp[1];
  }
  //   5)  2 O + M <=> O2 + M
  {
    double forward = 6.21278022363831e+15 * exp(-0.5 * vlntemp);
    double xik = -2.0 * cgspl[5] + cgspl[19];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[5] = forward * mole_frac[3] * mole_frac[3];
    rr_r[5] = reverse * mole_frac[14];
    rr_f[5] *= thbctemp[1];
    rr_r[5] *= thbctemp[1];
  }
  //   6)  H + O + M <=> OH + M
  {
    double forward = 4.336624958847576e+17 * otc;
    double xik = -cgspl[0] - cgspl[5] + cgspl[7];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[6] = forward * mole_frac[0] * mole_frac[3];
    rr_r[6] = reverse * mole_frac[5];
    rr_f[6] *= thbctemp[1];
    rr_r[6] *= thbctemp[1];
  }
  //   7)  H + OH + M <=> H2O + M
  {
    double forward = 2.913162007428368e+19 * otc * otc;
    double xik = -cgspl[0] - cgspl[7] + cgspl[8];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[7] = forward * mole_frac[0] * mole_frac[5];
    rr_r[7] = reverse * mole_frac[6];
    rr_f[7] *= thbctemp[1];
    rr_r[7] *= thbctemp[1];
  }
  //   8)  H + O2 (+M) <=> HO2 (+M)
  {
    double rr_k0 = 1.864740588650248e+18 * exp(-1.72*vlntemp - 524.8*ortc);
    double rr_kinf = 2.879017473981534e+14 * exp(0.6 * vlntemp);
    double pr = rr_k0 / rr_kinf * thbctemp[2];
    double fcent = log10(MAX(0.2 * exp(-1.2e+32 * temperature) + 0.8 * 
      exp(-1.2e-28 * temperature),1e-200)); 
    double flogpr = log10(MAX(pr,1e-200)) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = -cgspl[0] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[8] = forward * mole_frac[0] * mole_frac[14];
    rr_r[8] = reverse * mole_frac[15];
  }
  //   9)  H + HO2 <=> H2 + O2
  {
    double forward = 1.832532226988623e+14 * exp(-823.0*ortc);
    double xik = -cgspl[0] + cgspl[1] + cgspl[19] - cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[9] = forward * mole_frac[0] * mole_frac[15];
    rr_r[9] = reverse * mole_frac[1] * mole_frac[14];
  }
  //  10)  H + HO2 <=> 2 OH
  {
    double forward = 7.814756406537628e+14 * exp(-295.0*ortc);
    double xik = -cgspl[0] + 2.0 * cgspl[7] - cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[10] = forward * mole_frac[0] * mole_frac[15];
    rr_r[10] = reverse * mole_frac[5] * mole_frac[5];
  }
  //  11)  O + HO2 <=> OH + O2
  {
    double forward = 3.587788998622304e+14;
    double xik = -cgspl[5] + cgspl[7] + cgspl[19] - cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[11] = forward * mole_frac[3] * mole_frac[15];
    rr_r[11] = reverse * mole_frac[5] * mole_frac[14];
  }
  //  12)  OH + HO2 <=> H2O + O2
  {
    double forward = 3.190372371082602e+14 * exp(497.0*ortc);
    double xik = -cgspl[7] + cgspl[8] + cgspl[19] - cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[12] = forward * mole_frac[5] * mole_frac[15];
    rr_r[12] = reverse * mole_frac[6] * mole_frac[14];
  }
  //  13)  2 HO2 <=> O2 + H2O2
  {
    double forward = 4.636527321296516e+15 * exp(-1.1982e+04*ortc);
    double xik = cgspl[19] - 2.0 * cgspl[20] + cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[13] = forward * mole_frac[15] * mole_frac[15];
    rr_r[13] = reverse * mole_frac[14] * mole_frac[16];
  }
  //  14)  2 HO2 <=> O2 + H2O2
  {
    double forward = 1.435115599448922e+12 * exp(1.6293e+03*ortc);
    double xik = cgspl[19] - 2.0 * cgspl[20] + cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[14] = forward * mole_frac[15] * mole_frac[15];
    rr_r[14] = reverse * mole_frac[14] * mole_frac[16];
  }
  //  15)  H2O2 (+M) <=> 2 OH (+M)
  {
    double rr_k0 = 1.326929961952003e+18 * exp(-4.55e+04*ortc);
    double rr_kinf = 3.257712410749052e+15 * exp(-4.843e+04*ortc);
    double pr = rr_k0 / rr_kinf * thbctemp[1];
    double fcent = log10(MAX(0.5 * exp(-1.2e+32 * temperature) + 0.5 * 
      exp(-1.2e-28 * temperature),1e-200)); 
    double flogpr = log10(MAX(pr,1e-200)) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = 2.0 * cgspl[7] - cgspl[21];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[15] = forward * mole_frac[16];
    rr_r[15] = reverse * mole_frac[5] * mole_frac[5];
  }
  //  16)  H + H2O2 <=> OH + H2O
  {
    double forward = 2.660483534363001e+14 * exp(-3.97e+03*ortc);
    double xik = -cgspl[0] + cgspl[7] + cgspl[8] - cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[16] = forward * mole_frac[0] * mole_frac[16];
    rr_r[16] = reverse * mole_frac[5] * mole_frac[6];
  }
  //  17)  H + H2O2 <=> H2 + HO2
  {
    double forward = 5.320967068726001e+14 * exp(-7.95e+03*ortc);
    double xik = -cgspl[0] + cgspl[1] + cgspl[20] - cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[17] = forward * mole_frac[0] * mole_frac[16];
    rr_r[17] = reverse * mole_frac[1] * mole_frac[15];
  }
  //  18)  O + H2O2 <=> OH + HO2
  {
    double forward = 1.518131517201658e+12 * temperature * temperature * 
      exp(-3.97e+03*ortc); 
    double xik = -cgspl[5] + cgspl[7] + cgspl[20] - cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[18] = forward * mole_frac[3] * mole_frac[16];
    rr_r[18] = reverse * mole_frac[5] * mole_frac[15];
  }
  //  19)  OH + H2O2 <=> H2O + HO2
  {
    double forward = 1.103935076499171e+13;
    double xik = -cgspl[7] + cgspl[8] + cgspl[20] - cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[19] = forward * mole_frac[5] * mole_frac[16];
    rr_r[19] = reverse * mole_frac[6] * mole_frac[15];
  }
  //  20)  OH + H2O2 <=> H2O + HO2
  {
    double forward = 6.402823443695189e+15 * exp(-9.557e+03*ortc);
    double xik = -cgspl[7] + cgspl[8] + cgspl[20] - cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[20] = forward * mole_frac[5] * mole_frac[16];
    rr_r[20] = reverse * mole_frac[6] * mole_frac[15];
  }
  //  21)  O + CO (+M) <=> CO2 (+M)
  {
    double rr_k0 = 2.706197911607109e+19 * exp(-2.79*vlntemp - 4.191e+03*ortc);
    double rr_kinf = 1.987083137698507e+11 * exp(-2.384e+03*ortc);
    double pr = rr_k0 / rr_kinf * thbctemp[1];
    double forward = rr_kinf * pr/(1.0 + pr);
    double xik = -cgspl[5] - cgspl[11] + cgspl[22];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[21] = forward * mole_frac[3] * mole_frac[8];
    rr_r[21] = reverse * mole_frac[17];
  }
  //  22)  CO + O2 <=> O + CO2
  {
    double forward = 2.792955743542901e+13 * exp(-4.77e+04*ortc);
    double xik = cgspl[5] - cgspl[11] - cgspl[19] + cgspl[22];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[22] = forward * mole_frac[8] * mole_frac[14];
    rr_r[22] = reverse * mole_frac[3] * mole_frac[17];
  }
  //  23)  CO + HO2 <=> OH + CO2
  {
    double forward = 3.322844580262503e+14 * exp(-2.3e+04*ortc);
    double xik = cgspl[7] - cgspl[11] - cgspl[20] + cgspl[22];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[23] = forward * mole_frac[8] * mole_frac[15];
    rr_r[23] = reverse * mole_frac[5] * mole_frac[17];
  }
  //  24)  OH + CO <=> H + CO2
  {
    double forward = 2.09269605867099e+10 * exp(1.89*vlntemp + 1.1587e+03*ortc);
    double xik = cgspl[0] - cgspl[7] - cgspl[11] + cgspl[22];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[24] = forward * mole_frac[5] * mole_frac[8];
    rr_r[24] = reverse * mole_frac[0] * mole_frac[17];
  }
  //  25)  HCO + M <=> H + CO + M
  {
    double forward = 1.229363787723021e+14 * exp(0.659*vlntemp - 
      1.4874e+04*ortc); 
    double xik = cgspl[0] + cgspl[11] - cgspl[13];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[25] = forward;
    rr_r[25] = reverse * mole_frac[0] * mole_frac[8];
    rr_f[25] *= thbctemp[3];
    rr_r[25] *= thbctemp[3];
  }
  //  26)  HCO + O2 <=> CO + HO2
  {
    double forward = 8.367827879863712e+13 * exp(-410.0*ortc);
    double xik = cgspl[11] - cgspl[13] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[26] = forward * mole_frac[14];
    rr_r[26] = reverse * mole_frac[8] * mole_frac[15];
  }
  //  27)  H + HCO <=> H2 + CO
  {
    double forward = 7.981450603089002e+14;
    double xik = -cgspl[0] + cgspl[1] + cgspl[11] - cgspl[13];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[27] = forward * mole_frac[0];
    rr_r[27] = reverse * mole_frac[1] * mole_frac[8];
  }
  //  28)  O + HCO <=> OH + CO
  {
    double forward = 3.333883931027495e+14;
    double xik = -cgspl[5] + cgspl[7] + cgspl[11] - cgspl[13];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[28] = forward * mole_frac[3];
    rr_r[28] = reverse * mole_frac[5] * mole_frac[8];
  }
  //  29)  OH + HCO <=> H2O + CO
  {
    double forward = 3.333883931027495e+14;
    double xik = -cgspl[7] + cgspl[8] + cgspl[11] - cgspl[13];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[29] = forward * mole_frac[5];
    rr_r[29] = reverse * mole_frac[6] * mole_frac[8];
  }
  //  30)  O + HCO <=> H + CO2
  {
    double forward = 3.311805229497511e+14;
    double xik = cgspl[0] - cgspl[5] - cgspl[13] + cgspl[22];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[30] = forward * mole_frac[3];
    rr_r[30] = reverse * mole_frac[0] * mole_frac[17];
  }
  //  31)  HCO + HO2 <=> H + OH + CO2
  {
    double forward = 3.311805229497511e+14;
    double xik = cgspl[0] + cgspl[7] - cgspl[13] - cgspl[20] + cgspl[22];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[31] = forward * mole_frac[15];
    rr_r[31] = reverse * mole_frac[0] * mole_frac[5] * mole_frac[17];
  }
  //  32)  2 HCO <=> H2 + 2 CO
  {
    double forward = 3.311805229497511e+13;
    double xik = cgspl[1] + 2.0 * cgspl[11] - 2.0 * cgspl[13];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[32] = forward;
    rr_r[32] = reverse * mole_frac[1] * mole_frac[8] * mole_frac[8];
  }
  //  33)  CH3 + HCO <=> CH4 + CO
  {
    double forward = 2.925427952722802e+14;
    double xik = -cgspl[4] + cgspl[6] + cgspl[11] - cgspl[13];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[33] = forward * mole_frac[2];
    rr_r[33] = reverse * mole_frac[4] * mole_frac[8];
  }
  //  34)  2 HCO <=> CO + CH2O
  {
    double forward = 3.311805229497511e+14;
    double xik = cgspl[11] - 2.0 * cgspl[13] + cgspl[15];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[34] = forward;
    rr_r[34] = reverse * mole_frac[8] * mole_frac[11];
  }
  //  35)  CH2O + M <=> H + HCO + M
  {
    double forward = 2.901453552656132e+27 * exp(-6.3*vlntemp - 9.99e+04*ortc);
    double xik = cgspl[0] + cgspl[13] - cgspl[15];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[35] = forward * mole_frac[11];
    rr_r[35] = reverse * mole_frac[0];
    rr_f[35] *= thbctemp[1];
    rr_r[35] *= thbctemp[1];
  }
  //  36)  CH2O + M <=> H2 + CO + M
  {
    double forward = 7.958940506033622e+29 * exp(-8.0*vlntemp - 9.751e+04*ortc);
    double xik = cgspl[1] + cgspl[11] - cgspl[15];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[36] = forward * mole_frac[11];
    rr_r[36] = reverse * mole_frac[1] * mole_frac[8];
    rr_f[36] *= thbctemp[1];
    rr_r[36] *= thbctemp[1];
  }
  //  37)  H + CH2O <=> H2 + HCO
  {
    double forward = 5.653270844854574e+12 * exp(1.9*vlntemp - 2.7486e+03*ortc);
    double xik = -cgspl[0] + cgspl[1] + cgspl[13] - cgspl[15];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[37] = forward * mole_frac[0] * mole_frac[11];
    rr_r[37] = reverse * mole_frac[1];
  }
  //  38)  O + CH2O <=> OH + HCO
  {
    double forward = 1.998122488463498e+14 * exp(-3.08e+03*ortc);
    double xik = -cgspl[5] + cgspl[7] + cgspl[13] - cgspl[15];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[38] = forward * mole_frac[3] * mole_frac[11];
    rr_r[38] = reverse * mole_frac[5];
  }
  //  39)  OH + CH2O <=> H2O + HCO
  {
    double forward = 1.075651341627646e+13 * exp(1.18*vlntemp + 447.0*ortc);
    double xik = -cgspl[7] + cgspl[8] + cgspl[13] - cgspl[15];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[39] = forward * mole_frac[5] * mole_frac[11];
    rr_r[39] = reverse * mole_frac[6];
  }
  //  40)  CH2O + O2 <=> HCO + HO2
  {
    double forward = 2.346347768994395e+13 * exp(3.0*vlntemp - 5.2e+04*ortc);
    double xik = cgspl[13] - cgspl[15] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[40] = forward * mole_frac[11] * mole_frac[14];
    rr_r[40] = reverse * mole_frac[15];
  }
  //  41)  CH2O + HO2 <=> HCO + H2O2
  {
    double forward = 7.157122817631328e+10 * exp(2.5*vlntemp - 1.021e+04*ortc);
    double xik = cgspl[13] - cgspl[15] - cgspl[20] + cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[41] = forward * mole_frac[11] * mole_frac[15];
    rr_r[41] = reverse * mole_frac[16];
  }
  //  42)  CH3 + CH2O <=> CH4 + HCO
  {
    double forward = 7.45984426405874e+06 * exp(5.42*vlntemp - 998.0*ortc);
    double xik = -cgspl[4] + cgspl[6] + cgspl[13] - cgspl[15];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[42] = forward * mole_frac[2] * mole_frac[11];
    rr_r[42] = reverse * mole_frac[4];
  }
  //  43)  CH3 + O <=> H + CH2O
  {
    double forward = 9.306172694888006e+14;
    double xik = cgspl[0] - cgspl[4] - cgspl[5] + cgspl[15];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[43] = forward * mole_frac[2] * mole_frac[3];
    rr_r[43] = reverse * mole_frac[0] * mole_frac[11];
  }
  //  44)  CH3 + O2 <=> O + CH3O
  {
    double forward = 1.195314128835182e+16 * exp(-1.57*vlntemp - 
      2.923e+04*ortc); 
    double xik = -cgspl[4] + cgspl[5] + cgspl[18] - cgspl[19];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[44] = forward * mole_frac[2] * mole_frac[14];
    rr_r[44] = reverse * mole_frac[3] * mole_frac[13];
  }
  //  45)  CH3 + O2 <=> OH + CH2O
  {
    double forward = 4.128717186106897e+12 * exp(-1.464e+04*ortc);
    double xik = -cgspl[4] + cgspl[7] + cgspl[15] - cgspl[19];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[45] = forward * mole_frac[2] * mole_frac[14];
    rr_r[45] = reverse * mole_frac[5] * mole_frac[11];
  }
  //  46)  CH3 + HO2 <=> OH + CH3O
  {
    double forward = 1.011901692790769e+13 * exp(0.76*vlntemp + 2.325e+03*ortc);
    double xik = -cgspl[4] + cgspl[7] + cgspl[18] - cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[46] = forward * mole_frac[2] * mole_frac[15];
    rr_r[46] = reverse * mole_frac[5] * mole_frac[13];
  }
  //  47)  2 CH3 (+M) <=> C2H6 (+M)
  {
    double rr_k0 = 1.419140797871381e+25 * exp(-3.75*vlntemp - 981.6*ortc);
    double rr_kinf = 9.23999005317581e+14 * exp(-0.6899999999999999*vlntemp - 
      174.86*ortc); 
    double pr = rr_k0 / rr_kinf * thbctemp[4];
    double fcent = log10(MAX(1.0 * exp(-0.2105263157894736 * temperature) + 
      exp(-8.333333333333336e+27 * otc),1e-200)); 
    double flogpr = log10(MAX(pr,1e-200)) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = -2.0 * cgspl[4] + cgspl[16];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[47] = forward * mole_frac[2] * mole_frac[2];
    rr_r[47] = reverse * mole_frac[12];
  }
  //  48)  H + CH3 (+M) <=> CH4 (+M)
  {
    double rr_k0 = 3.467103514755617e+24 * exp(-4.76*vlntemp - 2.44e+03*ortc);
    double rr_kinf = 6.868518444211025e+15 * exp(-0.63*vlntemp - 383.0*ortc);
    double pr = rr_k0 / rr_kinf * thbctemp[5];
    double fcent = log10(MAX(0.217 * exp(-1.621621621621621 * temperature) + 
      0.783 * exp(-0.0408024481468888 * temperature) + exp(-58.03333333333335 * 
      otc),1e-200)); 
    double flogpr = log10(MAX(pr,1e-200)) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = -cgspl[0] - cgspl[4] + cgspl[6];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[48] = forward * mole_frac[0] * mole_frac[2];
    rr_r[48] = reverse * mole_frac[4];
  }
  //  49)  H + CH4 <=> H2 + CH3
  {
    double forward = 7.532131852958465e+12 * exp(1.97*vlntemp - 1.121e+04*ortc);
    double xik = -cgspl[0] + cgspl[1] + cgspl[4] - cgspl[6];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[49] = forward * mole_frac[0] * mole_frac[4];
    rr_r[49] = reverse * mole_frac[1] * mole_frac[2];
  }
  //  50)  O + CH4 <=> CH3 + OH
  {
    double forward = 3.809295903544656e+14 * exp(0.5*vlntemp - 1.029e+04*ortc);
    double xik = cgspl[4] - cgspl[5] - cgspl[6] + cgspl[7];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[50] = forward * mole_frac[3] * mole_frac[4];
    rr_r[50] = reverse * mole_frac[2] * mole_frac[5];
  }
  //  51)  CH4 + OH <=> CH3 + H2O
  {
    double forward = 7.508182214995878e+11 * exp(1.96*vlntemp - 2.639e+03*ortc);
    double xik = cgspl[4] - cgspl[6] - cgspl[7] + cgspl[8];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[51] = forward * mole_frac[4] * mole_frac[5];
    rr_r[51] = reverse * mole_frac[2] * mole_frac[6];
  }
  //  52)  CH3 + HO2 <=> CH4 + O2
  {
    double forward = 3.488434841737379e+13;
    double xik = -cgspl[4] + cgspl[6] + cgspl[19] - cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[52] = forward * mole_frac[2] * mole_frac[15];
    rr_r[52] = reverse * mole_frac[4] * mole_frac[14];
  }
  //  53)  CH4 + HO2 <=> CH3 + H2O2
  {
    double forward = 1.998122488463499e+12 * exp(-1.858e+04*ortc);
    double xik = cgspl[4] - cgspl[6] - cgspl[20] + cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[53] = forward * mole_frac[4] * mole_frac[15];
    rr_r[53] = reverse * mole_frac[2] * mole_frac[16];
  }
  //  54)  CH2OH + M <=> H + CH2O + M
  {
    double forward = 1.10393507649917e+15 * exp(-2.51e+04*ortc);
    double xik = cgspl[0] + cgspl[15] - cgspl[17];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[54] = forward;
    rr_r[54] = reverse * mole_frac[0] * mole_frac[11];
    rr_f[54] *= thbctemp[0];
    rr_r[54] *= thbctemp[0];
  }
  //  55)  H + CH2OH <=> H2 + CH2O
  {
    double forward = 6.623610458995023e+13;
    double xik = -cgspl[0] + cgspl[1] + cgspl[15] - cgspl[17];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[55] = forward * mole_frac[0];
    rr_r[55] = reverse * mole_frac[1] * mole_frac[11];
  }
  //  56)  H + CH2OH <=> CH3 + OH
  {
    double forward = 1.063641446206951e+15;
    double xik = -cgspl[0] + cgspl[4] + cgspl[7] - cgspl[17];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[56] = forward * mole_frac[0];
    rr_r[56] = reverse * mole_frac[2] * mole_frac[5];
  }
  //  57)  O + CH2OH <=> OH + CH2O
  {
    double forward = 4.636527321296516e+14;
    double xik = -cgspl[5] + cgspl[7] + cgspl[15] - cgspl[17];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[57] = forward * mole_frac[3];
    rr_r[57] = reverse * mole_frac[5] * mole_frac[11];
  }
  //  58)  OH + CH2OH <=> H2O + CH2O
  {
    double forward = 2.649444183598009e+14;
    double xik = -cgspl[7] + cgspl[8] + cgspl[15] - cgspl[17];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[58] = forward * mole_frac[5];
    rr_r[58] = reverse * mole_frac[6] * mole_frac[11];
  }
  //  59)  CH2OH + O2 <=> CH2O + HO2
  {
    double forward = 2.660483534363001e+15 * exp(-5.017e+03*ortc);
    double xik = cgspl[15] - cgspl[17] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[59] = forward * mole_frac[14];
    rr_r[59] = reverse * mole_frac[11] * mole_frac[15];
  }
  //  60)  CH2OH + O2 <=> CH2O + HO2
  {
    double forward = 1.38911830459479e+14 * otc;
    double xik = cgspl[15] - cgspl[17] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[60] = forward * mole_frac[14];
    rr_r[60] = reverse * mole_frac[11] * mole_frac[15];
  }
  //  61)  CH2OH + HO2 <=> CH2O + H2O2
  {
    double forward = 1.324722091799005e+14;
    double xik = cgspl[15] - cgspl[17] - cgspl[20] + cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[61] = forward * mole_frac[15];
    rr_r[61] = reverse * mole_frac[11] * mole_frac[16];
  }
  //  62)  HCO + CH2OH <=> 2 CH2O
  {
    double forward = 1.655902614748756e+14;
    double xik = -cgspl[13] + 2.0 * cgspl[15] - cgspl[17];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[62] = forward;
    rr_r[62] = reverse * mole_frac[11] * mole_frac[11];
  }
  //  63)  CH3O + M <=> H + CH2O + M
  {
    double forward = 2.930921117095452e+16 * exp(-1.2*vlntemp - 1.55e+04*ortc);
    double xik = cgspl[0] + cgspl[15] - cgspl[18];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[63] = forward * mole_frac[13];
    rr_r[63] = reverse * mole_frac[0] * mole_frac[11];
    rr_f[63] *= thbctemp[0];
    rr_r[63] *= thbctemp[0];
  }
  //  64)  H + CH3O <=> CH3 + OH
  {
    double forward = 3.532592244797346e+14;
    double xik = -cgspl[0] + cgspl[4] + cgspl[7] - cgspl[18];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[64] = forward * mole_frac[0] * mole_frac[13];
    rr_r[64] = reverse * mole_frac[2] * mole_frac[5];
  }
  //  65)  O + CH3O <=> OH + CH2O
  {
    double forward = 6.623610458995023e+13;
    double xik = -cgspl[5] + cgspl[7] + cgspl[15] - cgspl[18];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[65] = forward * mole_frac[3] * mole_frac[13];
    rr_r[65] = reverse * mole_frac[5] * mole_frac[11];
  }
  //  66)  OH + CH3O <=> H2O + CH2O
  {
    double forward = 1.987083137698507e+14;
    double xik = -cgspl[7] + cgspl[8] + cgspl[15] - cgspl[18];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[66] = forward * mole_frac[5] * mole_frac[13];
    rr_r[66] = reverse * mole_frac[6] * mole_frac[11];
  }
  //  67)  CH3O + O2 <=> CH2O + HO2
  {
    double forward = 9.971845546017006e+14 * exp(-1.198e+04*ortc);
    double xik = cgspl[15] - cgspl[18] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[67] = forward * mole_frac[13] * mole_frac[14];
    rr_r[67] = reverse * mole_frac[11] * mole_frac[15];
  }
  //  68)  CH3O + O2 <=> CH2O + HO2
  {
    double forward = 2.428657168298175e+11 * exp(-1.748e+03*ortc);
    double xik = cgspl[15] - cgspl[18] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[68] = forward * mole_frac[13] * mole_frac[14];
    rr_r[68] = reverse * mole_frac[11] * mole_frac[15];
  }
  //  69)  CH3O + HO2 <=> CH2O + H2O2
  {
    double forward = 3.311805229497511e+12;
    double xik = cgspl[15] - cgspl[18] - cgspl[20] + cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[69] = forward * mole_frac[13] * mole_frac[15];
    rr_r[69] = reverse * mole_frac[11] * mole_frac[16];
  }
  //  70)  CO + CH3O <=> CH3 + CO2
  {
    double forward = 1.766296122398673e+14 * exp(-1.18e+04*ortc);
    double xik = cgspl[4] - cgspl[11] - cgspl[18] + cgspl[22];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[70] = forward * mole_frac[8] * mole_frac[13];
    rr_r[70] = reverse * mole_frac[2] * mole_frac[17];
  }
  //  71)  2 CH3 <=> H + C2H5
  {
    double forward = 8.891237335160038e+13 * exp(0.1*vlntemp - 1.06e+04*ortc);
    double xik = cgspl[0] - 2.0 * cgspl[4] + cgspl[14];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[71] = forward * mole_frac[2] * mole_frac[2];
    rr_r[71] = reverse * mole_frac[0] * mole_frac[10];
  }
  //  72)  CH2 + CH4 <=> 2 CH3
  {
    double forward = 3.91057961499066e+11 * temperature * temperature * 
      exp(-8.27e+03*ortc); 
    double xik = -cgspl[2] + 2.0 * cgspl[4] - cgspl[6];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[72] = forward * mole_frac[4];
    rr_r[72] = reverse * mole_frac[2] * mole_frac[2];
  }
  //  73)  CH2(S) + CH4 <=> 2 CH3
  {
    double forward = 1.766296122398673e+14 * exp(570.0*ortc);
    double xik = -cgspl[3] + 2.0 * cgspl[4] - cgspl[6];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[73] = forward * mole_frac[4];
    rr_r[73] = reverse * mole_frac[2] * mole_frac[2];
  }
  //  74)  CH3 + OH <=> CH2 + H2O
  {
    double forward = 1.311660842383452e+12 * exp(1.6*vlntemp - 5.42e+03*ortc);
    double xik = cgspl[2] - cgspl[4] - cgspl[7] + cgspl[8];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[74] = forward * mole_frac[2] * mole_frac[5];
    rr_r[74] = reverse * mole_frac[6];
  }
  //  75)  CH3 + OH <=> CH2(S) + H2O
  {
    double forward = 2.760941626324425e+14;
    double xik = cgspl[3] - cgspl[4] - cgspl[7] + cgspl[8];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[75] = forward * mole_frac[2] * mole_frac[5];
    rr_r[75] = reverse * mole_frac[6];
  }
  //  76)  CH2 + CH3 <=> H + C2H4
  {
    double forward = 4.415740305996682e+14;
    double xik = cgspl[0] - cgspl[2] - cgspl[4] + cgspl[12];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[76] = forward * mole_frac[2];
    rr_r[76] = reverse * mole_frac[0] * mole_frac[9];
  }
  //  77)  CH2(S) + CH3 <=> H + C2H4
  {
    double forward = 1.324722091799005e+14 * exp(570.0*ortc);
    double xik = cgspl[0] - cgspl[3] - cgspl[4] + cgspl[12];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[77] = forward * mole_frac[2];
    rr_r[77] = reverse * mole_frac[0] * mole_frac[9];
  }
  //  78)  H + CH3O <=> CH2(S) + H2O
  {
    double forward = 1.766296122398673e+14;
    double xik = -cgspl[0] + cgspl[3] + cgspl[8] - cgspl[18];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[78] = forward * mole_frac[0] * mole_frac[13];
    rr_r[78] = reverse * mole_frac[6];
  }
  //  79)  H + C2H6 <=> H2 + C2H5
  {
    double forward = 1.132623949753094e+13 * exp(1.9*vlntemp - 7.53e+03*ortc);
    double xik = -cgspl[0] + cgspl[1] + cgspl[14] - cgspl[16];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[79] = forward * mole_frac[0] * mole_frac[12];
    rr_r[79] = reverse * mole_frac[1] * mole_frac[10];
  }
  //  80)  O + C2H6 <=> OH + C2H5
  {
    double forward = 9.733025516474088e+12 * exp(1.92*vlntemp - 5.69e+03*ortc);
    double xik = -cgspl[5] + cgspl[7] + cgspl[14] - cgspl[16];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[80] = forward * mole_frac[3] * mole_frac[12];
    rr_r[80] = reverse * mole_frac[5] * mole_frac[10];
  }
  //  81)  OH + C2H6 <=> H2O + C2H5
  {
    double forward = 9.99564990124443e+11 * exp(2.12*vlntemp - 870.0*ortc);
    double xik = -cgspl[7] + cgspl[8] + cgspl[14] - cgspl[16];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[81] = forward * mole_frac[5] * mole_frac[12];
    rr_r[81] = reverse * mole_frac[6] * mole_frac[10];
  }
  //  82)  C2H6 + O2 <=> C2H5 + HO2
  {
    double forward = 4.415740305996682e+14 * exp(-5.09e+04*ortc);
    double xik = cgspl[14] - cgspl[16] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[82] = forward * mole_frac[12] * mole_frac[14];
    rr_r[82] = reverse * mole_frac[10] * mole_frac[15];
  }
  //  83)  C2H6 + HO2 <=> C2H5 + H2O2
  {
    double forward = 3.245569124907561e+12 * exp(-1.494e+04*ortc);
    double xik = cgspl[14] - cgspl[16] - cgspl[20] + cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[83] = forward * mole_frac[12] * mole_frac[15];
    rr_r[83] = reverse * mole_frac[10] * mole_frac[16];
  }
  //  84)  CH3 + C2H6 <=> CH4 + C2H5
  {
    double forward = 2.811171052603441e+11 * exp(1.74*vlntemp - 1.045e+04*ortc);
    double xik = -cgspl[4] + cgspl[6] + cgspl[14] - cgspl[16];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[84] = forward * mole_frac[2] * mole_frac[12];
    rr_r[84] = reverse * mole_frac[4] * mole_frac[10];
  }
  //  85)  H + C2H5 (+M) <=> C2H6 (+M)
  {
    double rr_k0 = 4.18016464015134e+27 * exp(-7.08*vlntemp - 6.685e+03*ortc);
    double rr_kinf = 5.027960099347838e+16 * exp(-0.99*vlntemp - 1.58e+03*ortc);
    double pr = rr_k0 / rr_kinf * thbctemp[5];
    double fcent = log10(MAX(0.1578000000000001 * exp(-0.9599999999999997 * 
      temperature) + 0.8421999999999999 * exp(-0.05407841369986479 * 
      temperature) + exp(-57.35000000000002 * otc),1e-200)); 
    double flogpr = log10(MAX(pr,1e-200)) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = -cgspl[0] - cgspl[14] + cgspl[16];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[85] = forward * mole_frac[0] * mole_frac[10];
    rr_r[85] = reverse * mole_frac[12];
  }
  //  86)  H + C2H5 <=> H2 + C2H4
  {
    double forward = 2.207870152998341e+13;
    double xik = -cgspl[0] + cgspl[1] + cgspl[12] - cgspl[14];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[86] = forward * mole_frac[0] * mole_frac[10];
    rr_r[86] = reverse * mole_frac[1] * mole_frac[9];
  }
  //  87)  O + C2H5 <=> CH3 + CH2O
  {
    double forward = 1.457194300978905e+15;
    double xik = cgspl[4] - cgspl[5] - cgspl[14] + cgspl[15];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[87] = forward * mole_frac[3] * mole_frac[10];
    rr_r[87] = reverse * mole_frac[2] * mole_frac[11];
  }
  //  88)  C2H5 + O2 <=> C2H4 + HO2
  {
    double forward = 2.207870152998341e+11;
    double xik = cgspl[12] - cgspl[14] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[88] = forward * mole_frac[10] * mole_frac[14];
    rr_r[88] = reverse * mole_frac[9] * mole_frac[15];
  }
  //  89)  2 C2H5 <=> C2H4 + C2H6
  {
    double forward = 1.545509107098839e+13;
    double xik = cgspl[12] - 2.0 * cgspl[14] + cgspl[16];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[89] = forward * mole_frac[10] * mole_frac[10];
    rr_r[89] = reverse * mole_frac[9] * mole_frac[12];
  }
  //  90)  HCO + C2H5 <=> CO + C2H6
  {
    double forward = 1.324722091799004e+15;
    double xik = cgspl[11] - cgspl[13] - cgspl[14] + cgspl[16];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[90] = forward * mole_frac[10];
    rr_r[90] = reverse * mole_frac[8] * mole_frac[12];
  }
  //  91)  O + C2H5 <=> H + CH3HCO
  {
    double forward = 8.853559313523348e+14;
    double xik = cgspl[0] - cgspl[5] - cgspl[14] + cgspl[23];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[91] = forward * mole_frac[3] * mole_frac[10];
    rr_r[91] = reverse * mole_frac[0] * mole_frac[18];
  }
  //  92)  C2H4 (+M) <=> H2 + C2H2 (+M)
  {
    double rr_k0 = 3.395190696521737e+32 * exp(-9.31*vlntemp - 9.986e+04*ortc);
    double rr_kinf = 7.258941026773331e+14 * exp(0.44*vlntemp - 8.877e+04*ortc);
    double pr = rr_k0 / rr_kinf * thbctemp[5];
    double fcent = log10(MAX(0.2655 * exp(-0.6666666666666665 * temperature) + 
      0.7345 * exp(-0.1159420289855072 * temperature) + exp(-45.14166666666668 * 
      otc),1e-200)); 
    double flogpr = log10(MAX(pr,1e-200)) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = cgspl[1] + cgspl[9] - cgspl[12];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[92] = forward * mole_frac[9];
    rr_r[92] = reverse * mole_frac[1] * mole_frac[7];
  }
  //  93)  H + C2H4 (+M) <=> C2H5 (+M)
  {
    double rr_k0 = 1.900044113066513e+27 * exp(-7.62*vlntemp - 6.97e+03*ortc);
    double rr_kinf = 1.047889705976806e+14 * exp(0.454*vlntemp - 1.82e+03*ortc);
    double pr = rr_k0 / rr_kinf * thbctemp[5];
    double fcent = log10(MAX(0.02470000000000006 * exp(-0.5714285714285713 * 
      temperature) + 0.9752999999999999 * exp(-0.1219512195121951 * temperature) 
      + exp(-36.45000000000001 * otc),1e-200)); 
    double flogpr = log10(MAX(pr,1e-200)) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = -cgspl[0] - cgspl[12] + cgspl[14];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[93] = forward * mole_frac[0] * mole_frac[9];
    rr_r[93] = reverse * mole_frac[10];
  }
  //  94)  H + C2H3 (+M) <=> C2H4 (+M)
  {
    double rr_k0 = 1.456907432195987e+23 * exp(-3.86*vlntemp - 3.32e+03*ortc);
    double rr_kinf = 2.444702679710093e+14 * exp(0.27*vlntemp - 280.0*ortc);
    double pr = rr_k0 / rr_kinf * thbctemp[5];
    double fcent = log10(MAX(0.218 * exp(-0.5783132530120481 * temperature) + 
      0.782 * exp(-0.04506196019526848 * temperature) + exp(-50.79166666666668 * 
      otc),1e-200)); 
    double flogpr = log10(MAX(pr,1e-200)) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = -cgspl[0] - cgspl[10] + cgspl[12];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[94] = forward * mole_frac[0];
    rr_r[94] = reverse * mole_frac[9];
  }
  //  95)  H + C2H4 <=> H2 + C2H3
  {
    double forward = 2.663716271498442e+12 * exp(2.53*vlntemp - 1.224e+04*ortc);
    double xik = -cgspl[0] + cgspl[1] + cgspl[10] - cgspl[12];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[95] = forward * mole_frac[0] * mole_frac[9];
    rr_r[95] = reverse * mole_frac[1];
  }
  //  96)  OH + C2H4 <=> H2O + C2H3
  {
    double forward = 2.861399718285849e+11 * temperature * temperature * 
      exp(-2.5e+03*ortc); 
    double xik = -cgspl[7] + cgspl[8] + cgspl[10] - cgspl[12];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[96] = forward * mole_frac[5] * mole_frac[9];
    rr_r[96] = reverse * mole_frac[6];
  }
  //  97)  CH3 + C2H4 <=> CH4 + C2H3
  {
    double forward = 3.608542978060487e+10 * temperature * temperature * 
      exp(-9.2e+03*ortc); 
    double xik = -cgspl[4] + cgspl[6] + cgspl[10] - cgspl[12];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[97] = forward * mole_frac[2] * mole_frac[9];
    rr_r[97] = reverse * mole_frac[4];
  }
  //  98)  O + C2H4 <=> CH3 + HCO
  {
    double forward = 1.352528551877918e+12 * exp(1.83*vlntemp - 220.0*ortc);
    double xik = cgspl[4] - cgspl[5] - cgspl[12] + cgspl[13];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[98] = forward * mole_frac[3] * mole_frac[9];
    rr_r[98] = reverse * mole_frac[2];
  }
  //  99)  OH + C2H3 <=> H2O + C2H2
  {
    double forward = 5.519675382495852e+13;
    double xik = -cgspl[7] + cgspl[8] + cgspl[9] - cgspl[10];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[99] = forward * mole_frac[5];
    rr_r[99] = reverse * mole_frac[6] * mole_frac[7];
  }
  // 100)  O + C2H4 <=> OH + C2H3
  {
    double forward = 1.560115171110356e+12 * exp(1.91*vlntemp - 3.74e+03*ortc);
    double xik = -cgspl[5] + cgspl[7] + cgspl[10] - cgspl[12];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[100] = forward * mole_frac[3] * mole_frac[9];
    rr_r[100] = reverse * mole_frac[5];
  }
  // 101)  C2H4 + O2 <=> C2H3 + HO2
  {
    double forward = 4.653086347444003e+14 * exp(-5.76e+04*ortc);
    double xik = cgspl[10] - cgspl[12] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[101] = forward * mole_frac[9] * mole_frac[14];
    rr_r[101] = reverse * mole_frac[15];
  }
  // 102)  H + C2H3 <=> H2 + C2H2
  {
    double forward = 1.0641934137452e+15;
    double xik = -cgspl[0] + cgspl[1] + cgspl[9] - cgspl[10];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[102] = forward * mole_frac[0];
    rr_r[102] = reverse * mole_frac[1] * mole_frac[7];
  }
  // 103)  C2H3 + H2O2 <=> C2H4 + HO2
  {
    double forward = 1.335761442563996e+11 * exp(596.0*ortc);
    double xik = -cgspl[10] + cgspl[12] + cgspl[20] - cgspl[21];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[103] = forward * mole_frac[16];
    rr_r[103] = reverse * mole_frac[9] * mole_frac[15];
  }
  // 104)  CH3 + C2H3 <=> CH4 + C2H2
  {
    double forward = 4.305346798346765e+12;
    double xik = -cgspl[4] + cgspl[6] + cgspl[9] - cgspl[10];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[104] = forward * mole_frac[2];
    rr_r[104] = reverse * mole_frac[4] * mole_frac[7];
  }
  // 105)  2 C2H3 <=> C2H2 + C2H4
  {
    double forward = 1.059777673439204e+13;
    double xik = cgspl[9] - 2.0 * cgspl[10] + cgspl[12];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[105] = forward;
    rr_r[105] = reverse * mole_frac[7] * mole_frac[9];
  }
  // 106)  C2H3 + O2 <=> HCO + CH2O
  {
    double forward = 6.512490141179874e+14 * exp(-1.39*vlntemp - 
      1.015e+03*ortc); 
    double xik = -cgspl[10] + cgspl[13] + cgspl[15] - cgspl[19];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[106] = forward * mole_frac[14];
    rr_r[106] = reverse * mole_frac[11];
  }
  // 107)  C2H3 + O2 <=> C2H2 + HO2
  {
    double forward = 3.285161664369637e+10 * exp(1.61*vlntemp + 384.0*ortc);
    double xik = cgspl[9] - cgspl[10] - cgspl[19] + cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[107] = forward * mole_frac[14];
    rr_r[107] = reverse * mole_frac[7] * mole_frac[15];
  }
  // 108)  H + C2H2 (+M) <=> C2H3 (+M)
  {
    double rr_k0 = 3.21424865400783e+26 * exp(-7.27*vlntemp - 7.22e+03*ortc);
    double rr_kinf = 6.182036428395355e+13 * exp(-2.4e+03*ortc);
    double pr = rr_k0 / rr_kinf * thbctemp[5];
    double fcent = log10(MAX(0.2493 * exp(-1.218274111675127 * temperature) + 
      0.7507 * exp(-0.0921658986175115 * temperature) + exp(-34.72500000000001 * 
      otc),1e-200)); 
    double flogpr = log10(MAX(pr,1e-200)) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = -cgspl[0] - cgspl[9] + cgspl[10];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[108] = forward * mole_frac[0] * mole_frac[7];
    rr_r[108] = reverse;
  }
  // 109)  O + C2H2 <=> CH2 + CO
  {
    double forward = 6.485839361447922e+11 * temperature * temperature * 
      exp(-1.9e+03*ortc); 
    double xik = cgspl[2] - cgspl[5] - cgspl[9] + cgspl[11];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[109] = forward * mole_frac[3] * mole_frac[7];
    rr_r[109] = reverse * mole_frac[8];
  }
  // 110)  OH + C2H2 <=> CH3 + CO
  {
    double forward = 1.105644851145651e+06 * exp(4.0*vlntemp + 2.0e+03*ortc);
    double xik = cgspl[4] - cgspl[7] - cgspl[9] + cgspl[11];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[110] = forward * mole_frac[5] * mole_frac[7];
    rr_r[110] = reverse * mole_frac[2] * mole_frac[8];
  }
  // 111)  H + CH2 (+M) <=> CH3 (+M)
  {
    double rr_k0 = 1.045837954098072e+22 * exp(-3.14*vlntemp - 1.23e+03*ortc);
    double rr_kinf = 5.991541126414163e+15 * exp(-0.8 * vlntemp);
    double pr = rr_k0 / rr_kinf * thbctemp[5];
    double fcent = log10(MAX(0.32 * exp(-1.538461538461538 * temperature) + 0.68 
      * exp(-0.06015037593984961 * temperature) + exp(-46.58333333333334 * 
      otc),1e-200)); 
    double flogpr = log10(MAX(pr,1e-200)) - 0.4 - 0.67 * fcent;
    double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
    double fquan = flogpr / fdenom;
    fquan = fcent / (1.0 + fquan * fquan);
    double forward = rr_kinf * pr/(1.0 + pr) * exp(fquan*DLn10);
    double xik = -cgspl[0] - cgspl[2] + cgspl[4];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[111] = forward * mole_frac[0];
    rr_r[111] = reverse * mole_frac[2];
  }
  // 112)  CH2 + O <=> H + HCO
  {
    double forward = 8.831480611993364e+14;
    double xik = cgspl[0] - cgspl[2] - cgspl[5] + cgspl[13];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[112] = forward * mole_frac[3];
    rr_r[112] = reverse * mole_frac[0];
  }
  // 113)  CH2 + OH <=> H + CH2O
  {
    double forward = 2.207870152998341e+14;
    double xik = cgspl[0] - cgspl[2] - cgspl[7] + cgspl[15];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[113] = forward * mole_frac[5];
    rr_r[113] = reverse * mole_frac[0] * mole_frac[11];
  }
  // 114)  H2 + CH2 <=> H + CH3
  {
    double forward = 7.948332550794025e+10 * temperature * temperature * 
      exp(-7.23e+03*ortc); 
    double xik = cgspl[0] - cgspl[1] - cgspl[2] + cgspl[4];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[114] = forward * mole_frac[1];
    rr_r[114] = reverse * mole_frac[0] * mole_frac[2];
  }
  // 115)  CH2 + O2 <=> OH + HCO
  {
    double forward = 1.457194300978905e+14 * exp(-1.5e+03*ortc);
    double xik = -cgspl[2] + cgspl[7] + cgspl[13] - cgspl[19];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[115] = forward * mole_frac[14];
    rr_r[115] = reverse * mole_frac[5];
  }
  // 116)  CH2 + HO2 <=> OH + CH2O
  {
    double forward = 2.207870152998341e+14;
    double xik = -cgspl[2] + cgspl[7] + cgspl[15] - cgspl[20];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[116] = forward * mole_frac[15];
    rr_r[116] = reverse * mole_frac[5] * mole_frac[11];
  }
  // 117)  2 CH2 <=> H2 + C2H2
  {
    double forward = 3.532592244797346e+14;
    double xik = cgspl[1] - 2.0 * cgspl[2] + cgspl[9];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[117] = forward;
    rr_r[117] = reverse * mole_frac[1] * mole_frac[7];
  }
  // 118)  CH2(S) + M <=> CH2 + M
  {
    double forward = 9.935415688492534e+13 * exp(-600.0*ortc);
    double xik = cgspl[2] - cgspl[3];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[118] = forward;
    rr_r[118] = reverse;
    rr_f[118] *= thbctemp[6];
    rr_r[118] *= thbctemp[6];
  }
  // 119)  CH2(S) + H2O <=> CH2 + H2O
  {
    double forward = 3.311805229497511e+14;
    double xik = cgspl[2] - cgspl[3];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[119] = forward * mole_frac[6];
    rr_r[119] = reverse * mole_frac[6];
  }
  // 120)  CH2(S) + CO <=> CH2 + CO
  {
    double forward = 9.935415688492534e+13;
    double xik = cgspl[2] - cgspl[3];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[120] = forward * mole_frac[8];
    rr_r[120] = reverse * mole_frac[8];
  }
  // 121)  CH2(S) + CO2 <=> CH2 + CO2
  {
    double forward = 7.727545535494194e+13;
    double xik = cgspl[2] - cgspl[3];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[121] = forward * mole_frac[17];
    rr_r[121] = reverse * mole_frac[17];
  }
  // 122)  CH2(S) + O <=> H2 + CO
  {
    double forward = 1.655902614748756e+14;
    double xik = cgspl[1] - cgspl[3] - cgspl[5] + cgspl[11];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[122] = forward * mole_frac[3];
    rr_r[122] = reverse * mole_frac[1] * mole_frac[8];
  }
  // 123)  CH2(S) + O <=> H + HCO
  {
    double forward = 1.655902614748756e+14;
    double xik = cgspl[0] - cgspl[3] - cgspl[5] + cgspl[13];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[123] = forward * mole_frac[3];
    rr_r[123] = reverse * mole_frac[0];
  }
  // 124)  CH2(S) + OH <=> H + CH2O
  {
    double forward = 3.311805229497511e+14;
    double xik = cgspl[0] - cgspl[3] - cgspl[7] + cgspl[15];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[124] = forward * mole_frac[5];
    rr_r[124] = reverse * mole_frac[0] * mole_frac[11];
  }
  // 125)  H2 + CH2(S) <=> H + CH3
  {
    double forward = 7.727545535494192e+14;
    double xik = cgspl[0] - cgspl[1] - cgspl[3] + cgspl[4];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[125] = forward * mole_frac[1];
    rr_r[125] = reverse * mole_frac[0] * mole_frac[2];
  }
  // 126)  CH2(S) + O2 <=> H + OH + CO
  {
    double forward = 3.091018214197678e+14;
    double xik = cgspl[0] - cgspl[3] + cgspl[7] + cgspl[11] - cgspl[19];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[126] = forward * mole_frac[14];
    rr_r[126] = reverse * mole_frac[0] * mole_frac[5] * mole_frac[8];
  }
  // 127)  CH2(S) + O2 <=> H2O + CO
  {
    double forward = 1.324722091799005e+14;
    double xik = -cgspl[3] + cgspl[8] + cgspl[11] - cgspl[19];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[127] = forward * mole_frac[14];
    rr_r[127] = reverse * mole_frac[6] * mole_frac[8];
  }
  // 128)  CH2(S) + CO2 <=> CO + CH2O
  {
    double forward = 1.545509107098839e+14;
    double xik = -cgspl[3] + cgspl[11] + cgspl[15] - cgspl[22];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[128] = forward * mole_frac[17];
    rr_r[128] = reverse * mole_frac[8] * mole_frac[11];
  }
  // 129)  CH3HCO <=> CH3 + HCO
  {
    double forward = 7.727545535494194e+16 * exp(-8.1674e+04*ortc);
    double xik = cgspl[4] + cgspl[13] - cgspl[23];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[129] = forward * mole_frac[18];
    rr_r[129] = reverse * mole_frac[2];
  }
  // 130)  CH3OCH3 <=> CH3 + CH3O
  {
    double forward = 5.447209472326553e+26 * exp(-7.95359*vlntemp - 
      9.180660000000001e+04*ortc); 
    double xik = cgspl[4] + cgspl[18] - cgspl[26];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[130] = forward * mole_frac[20];
    rr_r[130] = reverse * mole_frac[2] * mole_frac[13];
  }
  // 131)  OH + CH3OCH3 <=> H2O + CH3OCH2
  {
    double forward = 1.066666228316558e+12 * temperature * temperature * 
      exp(629.88*ortc); 
    double xik = -cgspl[7] + cgspl[8] + cgspl[24] - cgspl[26];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[131] = forward * mole_frac[5] * mole_frac[20];
    rr_r[131] = reverse * mole_frac[6];
  }
  // 132)  H + CH3OCH3 <=> H2 + CH3OCH2
  {
    double forward = 4.72130953517165e+12 * temperature * temperature * 
      exp(-4.03361e+03*ortc); 
    double xik = -cgspl[0] + cgspl[1] + cgspl[24] - cgspl[26];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[132] = forward * mole_frac[0] * mole_frac[20];
    rr_r[132] = reverse * mole_frac[1];
  }
  // 133)  CH3 + CH3OCH3 <=> CH4 + CH3OCH2
  {
    double forward = 2.118444820464827e+10 * exp(3.7779*vlntemp - 
      9.631299999999999e+03*ortc); 
    double xik = -cgspl[4] + cgspl[6] + cgspl[24] - cgspl[26];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[133] = forward * mole_frac[2] * mole_frac[20];
    rr_r[133] = reverse * mole_frac[4];
  }
  // 134)  O + CH3OCH3 <=> OH + CH3OCH2
  {
    double forward = 2.042472596567349e+09 * exp(5.29*vlntemp + 109.0*ortc);
    double xik = -cgspl[5] + cgspl[7] + cgspl[24] - cgspl[26];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[134] = forward * mole_frac[3] * mole_frac[20];
    rr_r[134] = reverse * mole_frac[5];
  }
  // 135)  HO2 + CH3OCH3 <=> H2O2 + CH3OCH2
  {
    double forward = 2.207870152998341e+14 * exp(-1.65e+04*ortc);
    double xik = -cgspl[20] + cgspl[21] + cgspl[24] - cgspl[26];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[135] = forward * mole_frac[15] * mole_frac[20];
    rr_r[135] = reverse * mole_frac[16];
  }
  // 136)  O2 + CH3OCH3 <=> HO2 + CH3OCH2
  {
    double forward = 4.526133813646599e+14 * exp(-4.491e+04*ortc);
    double xik = -cgspl[19] + cgspl[20] + cgspl[24] - cgspl[26];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[136] = forward * mole_frac[14] * mole_frac[20];
    rr_r[136] = reverse * mole_frac[15];
  }
  // 137)  CH3OCH2 <=> CH3 + CH2O
  {
    double forward = 1.324722091799005e+14 * exp(-2.575e+04*ortc);
    double xik = cgspl[4] + cgspl[15] - cgspl[24];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[137] = forward;
    rr_r[137] = reverse * mole_frac[2] * mole_frac[11];
  }
  // 138)  CH3O + CH3OCH2 <=> CH2O + CH3OCH3
  {
    double forward = 2.660483534363001e+14;
    double xik = cgspl[15] - cgspl[18] - cgspl[24] + cgspl[26];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[138] = forward * mole_frac[13];
    rr_r[138] = reverse * mole_frac[11] * mole_frac[20];
  }
  // 139)  CH2O + CH3OCH2 <=> HCO + CH3OCH3
  {
    double forward = 4.019975129634464e+10 * exp(2.8*vlntemp - 5.862e+03*ortc);
    double xik = cgspl[13] - cgspl[15] - cgspl[24] + cgspl[26];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[139] = forward * mole_frac[11];
    rr_r[139] = reverse * mole_frac[20];
  }
  // 140)  HO2 + CH3OCH2 <=> OH + CH3OCH2O
  {
    double forward = 9.935415688492534e+13;
    double xik = cgspl[7] - cgspl[20] - cgspl[24] + cgspl[30];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[140] = forward * mole_frac[15];
    rr_r[140] = reverse * mole_frac[5];
  }
  // 141)  CH3OCH2O <=> H + CH3OCHO
  {
    double forward = 8.174841514690048e+15 * exp(-0.66*vlntemp - 
      1.172e+04*ortc); 
    double xik = cgspl[0] + cgspl[29] - cgspl[30];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[141] = forward;
    rr_r[141] = reverse * mole_frac[0] * mole_frac[22];
  }
  // 142)  O2 + CH3OCHO <=> HO2 + CH3OCO
  {
    double forward = 1.10393507649917e+14 * exp(-4.97e+04*ortc);
    double xik = -cgspl[19] + cgspl[20] + cgspl[28] - cgspl[29];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[142] = forward * mole_frac[14] * mole_frac[22];
    rr_r[142] = reverse * mole_frac[15] * mole_frac[21];
  }
  // 143)  OH + CH3OCHO <=> H2O + CH3OCO
  {
    double forward = 5.749647191192931e+11 * exp(1.61*vlntemp + 35.0*ortc);
    double xik = -cgspl[7] + cgspl[8] + cgspl[28] - cgspl[29];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[143] = forward * mole_frac[5] * mole_frac[22];
    rr_r[143] = reverse * mole_frac[6] * mole_frac[21];
  }
  // 144)  HO2 + CH3OCHO <=> H2O2 + CH3OCO
  {
    double forward = 1.346800793328988e+13 * exp(-1.7e+04*ortc);
    double xik = -cgspl[20] + cgspl[21] + cgspl[28] - cgspl[29];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[144] = forward * mole_frac[15] * mole_frac[22];
    rr_r[144] = reverse * mole_frac[16] * mole_frac[21];
  }
  // 145)  O + CH3OCHO <=> OH + CH3OCO
  {
    double forward = 4.092272170665114e+11 * exp(2.5*vlntemp - 2.23e+03*ortc);
    double xik = -cgspl[5] + cgspl[7] + cgspl[28] - cgspl[29];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[145] = forward * mole_frac[3] * mole_frac[22];
    rr_r[145] = reverse * mole_frac[5] * mole_frac[21];
  }
  // 146)  H + CH3OCHO <=> H2 + CH3OCO
  {
    double forward = 7.232982621222562e+11 * temperature * temperature * 
      exp(-5.0e+03*ortc); 
    double xik = -cgspl[0] + cgspl[1] + cgspl[28] - cgspl[29];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[146] = forward * mole_frac[0] * mole_frac[22];
    rr_r[146] = reverse * mole_frac[1] * mole_frac[21];
  }
  // 147)  CH3 + CH3OCHO <=> CH4 + CH3OCO
  {
    double forward = 1.302739543042372e+08 * exp(3.46*vlntemp - 5.481e+03*ortc);
    double xik = -cgspl[4] + cgspl[6] + cgspl[28] - cgspl[29];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[147] = forward * mole_frac[2] * mole_frac[22];
    rr_r[147] = reverse * mole_frac[4] * mole_frac[21];
  }
  // 148)  CH3OCO <=> CO + CH3O
  {
    double forward = 1.802183890287902e+10 * exp(-1.76*vlntemp - 
      1.715e+04*ortc); 
    double xik = cgspl[11] + cgspl[18] - cgspl[28];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[148] = forward * mole_frac[21];
    rr_r[148] = reverse * mole_frac[8] * mole_frac[13];
  }
  // 149)  CH3OCO <=> CH3 + CO2
  {
    double forward = 3.327566857330973e+09 * exp(-1.78*vlntemp - 
      1.382e+04*ortc); 
    double xik = cgspl[4] + cgspl[22] - cgspl[28];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[149] = forward * mole_frac[21];
    rr_r[149] = reverse * mole_frac[2] * mole_frac[17];
  }
  // 150)  O2 + CH3OCH2 <=> CH3OCH2O2
  {
    double forward = 2.207870152998341e+13;
    double xik = -cgspl[19] - cgspl[24] + cgspl[34];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[150] = forward * mole_frac[14];
    rr_r[150] = reverse * mole_frac[26];
  }
  // 151)  2 CH3OCH2O2 <=> O2 + 2 CH3OCH2O
  {
    double forward = 7.761270889928952e+14 * exp(-4.5 * vlntemp);
    double xik = cgspl[19] + 2.0 * cgspl[30] - 2.0 * cgspl[34];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[151] = forward * mole_frac[26] * mole_frac[26];
    rr_r[151] = reverse * mole_frac[14];
  }
  // 152)  2 CH3OCH2O2 <=> O2 + CH3OCHO + CH3OCH2OH
  {
    double forward = 3.326120098351519e+14 * exp(-4.5 * vlntemp);
    double xik = cgspl[19] + cgspl[29] + cgspl[31] - 2.0 * cgspl[34];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[152] = forward * mole_frac[26] * mole_frac[26];
    rr_r[152] = reverse * mole_frac[14] * mole_frac[22] * mole_frac[23];
  }
  // 153)  CH3OCH2O <=> CH2O + CH3O
  {
    double forward = 5.541148369313399e+14 * exp(-1.1*vlntemp - 2.064e+04*ortc);
    double xik = cgspl[15] + cgspl[18] - cgspl[30];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[153] = forward;
    rr_r[153] = reverse * mole_frac[11] * mole_frac[13];
  }
  // 154)  O2 + CH3OCH2O <=> HO2 + CH3OCHO
  {
    double forward = 5.519675382495852e+11 * exp(-500.0*ortc);
    double xik = -cgspl[19] + cgspl[20] + cgspl[29] - cgspl[30];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[154] = forward * mole_frac[14];
    rr_r[154] = reverse * mole_frac[15] * mole_frac[22];
  }
  // 155)  CH3OCH2O2 <=> CH2OCH2O2H
  {
    double forward = 6.623610458995023e+11 * exp(-2.15e+04*ortc);
    double xik = -cgspl[34] + cgspl[35];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[155] = forward * mole_frac[26];
    rr_r[155] = reverse;
  }
  // 156)  CH2OCH2O2H <=> OH + 2 CH2O
  {
    double forward = 1.655902614748756e+14 * exp(-2.05e+04*ortc);
    double xik = cgspl[7] + 2.0 * cgspl[15] - cgspl[35];
    double reverse = forward * MIN(exp(xik*otc) * oprt * oprt,1e200);
    rr_f[156] = forward;
    rr_r[156] = reverse * mole_frac[5] * mole_frac[11] * mole_frac[11];
  }
  // 157)  O2 + CH2OCH2O2H <=> O2CH2OCH2O2H
  {
    double forward = 7.727545535494193e+12;
    double xik = -cgspl[19] - cgspl[35] + cgspl[37];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[157] = forward * mole_frac[14];
    rr_r[157] = reverse * mole_frac[28];
  }
  // 158)  O2CH2OCH2O2H <=> OH + HO2CH2OCHO
  {
    double forward = 4.415740305996682e+11 * exp(-1.85e+04*ortc);
    double xik = cgspl[7] + cgspl[36] - cgspl[37];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[158] = forward * mole_frac[28];
    rr_r[158] = reverse * mole_frac[5] * mole_frac[27];
  }
  // 159)  HO2CH2OCHO <=> OH + OCH2OCHO
  {
    double forward = 3.311805229497511e+17 * exp(-4.0e+04*ortc);
    double xik = cgspl[7] + cgspl[32] - cgspl[36];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[159] = forward * mole_frac[27];
    rr_r[159] = reverse * mole_frac[5] * mole_frac[24];
  }
  // 160)  OCH2OCHO <=> HOCH2OCO
  {
    double forward = 1.10393507649917e+12 * exp(-1.4e+04*ortc);
    double xik = -cgspl[32] + cgspl[33];
    double reverse = forward * MIN(exp(xik*otc),1e200);
    rr_f[160] = forward * mole_frac[24];
    rr_r[160] = reverse * mole_frac[25];
  }
  // 161)  HOCH2OCO <=> CO + HOCH2O
  {
    double forward = 6.134856519837184e+11 * exp(-2.69*vlntemp - 1.72e+04*ortc);
    double xik = cgspl[11] + cgspl[27] - cgspl[33];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[161] = forward * mole_frac[25];
    rr_r[161] = reverse * mole_frac[8];
  }
  // 162)  HOCH2OCO <=> CH2OH + CO2
  {
    double forward = 2.195112496257389e+11 * exp(-2.61*vlntemp - 
      2.081e+04*ortc); 
    double xik = cgspl[17] + cgspl[22] - cgspl[33];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[162] = forward * mole_frac[25];
    rr_r[162] = reverse * mole_frac[17];
  }
  // 163)  HOCH2O <=> H + HCOOH
  {
    double forward = 1.10393507649917e+15 * exp(-1.49e+04*ortc);
    double xik = cgspl[0] + cgspl[25] - cgspl[27];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[163] = forward;
    rr_r[163] = reverse * mole_frac[0] * mole_frac[19];
  }
  // 164)  OH + CH2O <=> HOCH2O
  {
    double forward = 2.444921184143431e+14 * exp(-1.11 * vlntemp);
    double xik = -cgspl[7] - cgspl[15] + cgspl[27];
    double reverse = forward * MIN(exp(xik*otc) * prt,1e200);
    rr_f[164] = forward * mole_frac[5] * mole_frac[11];
    rr_r[164] = reverse;
  }
  // 165)  HCOOH + M <=> H2O + CO + M
  {
    double forward = 2.539050675948092e+14 * exp(-5.0e+04*ortc);
    double xik = cgspl[8] + cgspl[11] - cgspl[25];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[165] = forward * mole_frac[19];
    rr_r[165] = reverse * mole_frac[6] * mole_frac[8];
    rr_f[165] *= thbctemp[0];
    rr_r[165] *= thbctemp[0];
  }
  // 166)  HCOOH + M <=> H2 + CO2 + M
  {
    double forward = 1.655902614748756e+17 * exp(-5.7e+04*ortc);
    double xik = cgspl[1] + cgspl[22] - cgspl[25];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[166] = forward * mole_frac[19];
    rr_r[166] = reverse * mole_frac[1] * mole_frac[17];
    rr_f[166] *= thbctemp[0];
    rr_r[166] *= thbctemp[0];
  }
  // 167)  HCOOH <=> OH + HCO
  {
    double forward = 5.605529017804934e+18 * exp(-0.46*vlntemp - 
      1.083e+05*ortc); 
    double xik = cgspl[7] + cgspl[13] - cgspl[25];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[167] = forward * mole_frac[19];
    rr_r[167] = reverse * mole_frac[5];
  }
  // 168)  OH + HCOOH <=> H + H2O + CO2
  {
    double forward = 5.550833377171349e+11 * exp(2.06*vlntemp - 916.0*ortc);
    double xik = cgspl[0] - cgspl[7] + cgspl[8] + cgspl[22] - cgspl[25];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[168] = forward * mole_frac[5] * mole_frac[19];
    rr_r[168] = reverse * mole_frac[0] * mole_frac[6] * mole_frac[17];
  }
  // 169)  OH + HCOOH <=> OH + H2O + CO
  {
    double forward = 2.816300179126374e+11 * exp(1.51*vlntemp + 962.0*ortc);
    double xik = cgspl[8] + cgspl[11] - cgspl[25];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[169] = forward * mole_frac[5] * mole_frac[19];
    rr_r[169] = reverse * mole_frac[5] * mole_frac[6] * mole_frac[8];
  }
  // 170)  H + HCOOH <=> H + H2 + CO2
  {
    double forward = 1.087902578628319e+12 * exp(2.1*vlntemp - 4.868e+03*ortc);
    double xik = cgspl[1] + cgspl[22] - cgspl[25];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[170] = forward * mole_frac[0] * mole_frac[19];
    rr_r[170] = reverse * mole_frac[0] * mole_frac[1] * mole_frac[17];
  }
  // 171)  H + HCOOH <=> H2 + OH + CO
  {
    double forward = 1.246084256131252e+14 * exp(-0.35*vlntemp - 
      2.988e+03*ortc); 
    double xik = -cgspl[0] + cgspl[1] + cgspl[7] + cgspl[11] - cgspl[25];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[171] = forward * mole_frac[0] * mole_frac[19];
    rr_r[171] = reverse * mole_frac[1] * mole_frac[5] * mole_frac[8];
  }
  // 172)  CH3 + HCOOH <=> CH4 + OH + CO
  {
    double forward = 4.934684224705385e+06 * exp(5.8*vlntemp - 2.2e+03*ortc);
    double xik = -cgspl[4] + cgspl[6] + cgspl[7] + cgspl[11] - cgspl[25];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[172] = forward * mole_frac[2] * mole_frac[19];
    rr_r[172] = reverse * mole_frac[4] * mole_frac[5] * mole_frac[8];
  }
  // 173)  HO2 + HCOOH <=> OH + CO + H2O2
  {
    double forward = 1.103935076499171e+13 * exp(-1.192e+04*ortc);
    double xik = cgspl[7] + cgspl[11] - cgspl[20] + cgspl[21] - cgspl[25];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[173] = forward * mole_frac[15] * mole_frac[19];
    rr_r[173] = reverse * mole_frac[5] * mole_frac[8] * mole_frac[16];
  }
  // 174)  O + HCOOH <=> 2 OH + CO
  {
    double forward = 2.190142797154487e+15 * exp(-1.9*vlntemp - 2.975e+03*ortc);
    double xik = -cgspl[5] + 2.0 * cgspl[7] + cgspl[11] - cgspl[25];
    double reverse = forward * MIN(exp(xik*otc) * oprt,1e200);
    rr_f[174] = forward * mole_frac[3] * mole_frac[19];
    rr_r[174] = reverse * mole_frac[5] * mole_frac[5] * mole_frac[8];
  }
  // Unimportant reaction rates
  rr_f[32] = 0.0;
  rr_f[34] = 0.0;
  rr_f[62] = 0.0;
  rr_f[105] = 0.0;
  rr_f[117] = 0.0;
  rr_r[151] = 0.0;
  // QSSA connected component
  {
    double a1_0, a1_2, a1_4;
    {
      double den = rr_f[72] + rr_f[76] + rr_f[111] + rr_f[112] + rr_f[113] + 
        rr_f[114] + rr_f[115] + rr_f[116] + rr_r[74] + rr_r[109] + rr_r[118] + 
        rr_r[119] + rr_r[120] + rr_r[121]; 
      a1_0 = (rr_f[74] + rr_f[109] + rr_r[72] + rr_r[76] + rr_r[111] + rr_r[113] 
        + rr_r[114] + rr_r[116] + rr_r[117] + rr_r[117])/den; 
      a1_2 = (rr_f[118] + rr_f[119] + rr_f[120] + rr_f[121])/den;
      a1_4 = (rr_r[112] + rr_r[115])/den;
    }
    double a2_0, a2_1, a2_4;
    {
      double den = rr_f[73] + rr_f[77] + rr_f[118] + rr_f[119] + rr_f[120] + 
        rr_f[121] + rr_f[122] + rr_f[123] + rr_f[124] + rr_f[125] + rr_f[126] + 
        rr_f[127] + rr_f[128] + rr_r[75] + rr_r[78]; 
      a2_0 = (rr_f[75] + rr_f[78] + rr_r[73] + rr_r[77] + rr_r[122] + rr_r[124] 
        + rr_r[125] + rr_r[126] + rr_r[127] + rr_r[128])/den; 
      a2_1 = (rr_r[118] + rr_r[119] + rr_r[120] + rr_r[121])/den;
      a2_4 = (rr_r[123])/den;
    }
    double a3_0, a3_4;
    {
      double den = rr_f[94] + rr_f[99] + rr_f[102] + rr_f[103] + rr_f[104] + 
        rr_f[106] + rr_f[107] + rr_r[95] + rr_r[96] + rr_r[97] + rr_r[100] + 
        rr_r[101] + rr_r[108]; 
      a3_0 = (rr_f[95] + rr_f[96] + rr_f[97] + rr_f[100] + rr_f[101] + rr_f[108] 
        + rr_r[94] + rr_r[99] + rr_r[102] + rr_r[103] + rr_r[104] + rr_r[105] + 
        rr_r[105] + rr_r[107])/den; 
      a3_4 = (rr_r[106])/den;
    }
    double a4_0, a4_1, a4_2, a4_3, a4_6;
    {
      double den = rr_f[25] + rr_f[26] + rr_f[27] + rr_f[28] + rr_f[29] + 
        rr_f[30] + rr_f[31] + rr_f[33] + rr_f[90] + rr_r[35] + rr_r[37] + 
        rr_r[38] + rr_r[39] + rr_r[40] + rr_r[41] + rr_r[42] + rr_r[98] + 
        rr_r[106] + rr_r[112] + rr_r[115] + rr_r[123] + rr_r[129] + rr_r[139] + 
        rr_r[167]; 
      a4_0 = (rr_f[35] + rr_f[37] + rr_f[38] + rr_f[39] + rr_f[40] + rr_f[41] + 
        rr_f[42] + rr_f[98] + rr_f[129] + rr_f[167] + rr_r[25] + rr_r[26] + 
        rr_r[27] + rr_r[28] + rr_r[29] + rr_r[30] + rr_r[31] + rr_r[32] + 
        rr_r[32] + rr_r[33] + rr_r[34] + rr_r[34] + rr_r[62] + rr_r[90])/den; 
      a4_1 = (rr_f[112] + rr_f[115])/den;
      a4_2 = (rr_f[123])/den;
      a4_3 = (rr_f[106])/den;
      a4_6 = (rr_f[139])/den;
    }
    double a6_0, a6_4, a6_8;
    {
      double den = rr_f[137] + rr_f[138] + rr_f[139] + rr_f[140] + rr_f[150] + 
        rr_r[131] + rr_r[132] + rr_r[133] + rr_r[134] + rr_r[135] + rr_r[136]; 
      a6_0 = (rr_f[131] + rr_f[132] + rr_f[133] + rr_f[134] + rr_f[135] + 
        rr_f[136] + rr_r[137] + rr_r[138] + rr_r[150])/den; 
      a6_4 = (rr_r[139])/den;
      a6_8 = (rr_r[140])/den;
    }
    double a8_0, a8_6;
    {
      double den = rr_f[141] + rr_f[153] + rr_f[154] + rr_r[140];
      a8_0 = (rr_f[151] + rr_f[151] + rr_r[141] + rr_r[153] + rr_r[154])/den;
      a8_6 = (rr_f[140])/den;
    }
    double den, xq_1, xq_2, xq_3, xq_4, xq_6, xq_8;
    a6_0 = a6_0 + a6_8 * a8_0;
    den = 1.0/(1.0 - a8_6*a6_8);
    a6_0 = a6_0*den;
    a6_4 = a6_4*den;
    a4_0 = a4_0 + a4_3 * a3_0;
    den = 1.0/(1.0 - a3_4*a4_3);
    a4_0 = a4_0*den;
    a4_1 = a4_1*den;
    a4_2 = a4_2*den;
    a4_6 = a4_6*den;
    a4_0 = a4_0 + a4_6 * a6_0;
    den = 1.0/(1.0 - a6_4*a4_6);
    a4_0 = a4_0*den;
    a4_1 = a4_1*den;
    a4_2 = a4_2*den;
    a4_0 = a4_0 + a4_2 * a2_0;
    den = 1.0/(1.0 - a2_4*a4_2);
    a4_1 = a4_1 + a4_2 * a2_1;
    a4_0 = a4_0*den;
    a4_1 = a4_1*den;
    a1_0 = a1_0 + a1_2 * a2_0;
    den = 1.0/(1.0 - a2_1*a1_2);
    a1_4 = a1_4 + a1_2 * a2_4;
    a1_0 = a1_0*den;
    a1_4 = a1_4*den;
    a4_0 = a4_0 + a4_1 * a1_0;
    den = 1.0/(1.0 - a1_4*a4_1);
    a4_0 = a4_0*den;
    xq_4 = a4_0;
    xq_1 = a1_0 + a1_4*xq_4;
    xq_2 = a2_0 + a2_1*xq_1 + a2_4*xq_4;
    xq_6 = a6_0 + a6_4*xq_4;
    xq_3 = a3_0 + a3_4*xq_4;
    xq_8 = a8_0 + a8_6*xq_6;
    rr_f[72] *= xq_1;
    rr_f[76] *= xq_1;
    rr_f[111] *= xq_1;
    rr_f[112] *= xq_1;
    rr_f[113] *= xq_1;
    rr_f[114] *= xq_1;
    rr_f[115] *= xq_1;
    rr_f[116] *= xq_1;
    rr_r[74] *= xq_1;
    rr_r[109] *= xq_1;
    rr_r[118] *= xq_1;
    rr_r[119] *= xq_1;
    rr_r[120] *= xq_1;
    rr_r[121] *= xq_1;
    rr_f[73] *= xq_2;
    rr_f[77] *= xq_2;
    rr_f[118] *= xq_2;
    rr_f[119] *= xq_2;
    rr_f[120] *= xq_2;
    rr_f[121] *= xq_2;
    rr_f[122] *= xq_2;
    rr_f[123] *= xq_2;
    rr_f[124] *= xq_2;
    rr_f[125] *= xq_2;
    rr_f[126] *= xq_2;
    rr_f[127] *= xq_2;
    rr_f[128] *= xq_2;
    rr_r[75] *= xq_2;
    rr_r[78] *= xq_2;
    rr_f[94] *= xq_3;
    rr_f[99] *= xq_3;
    rr_f[102] *= xq_3;
    rr_f[103] *= xq_3;
    rr_f[104] *= xq_3;
    rr_f[106] *= xq_3;
    rr_f[107] *= xq_3;
    rr_r[95] *= xq_3;
    rr_r[96] *= xq_3;
    rr_r[97] *= xq_3;
    rr_r[100] *= xq_3;
    rr_r[101] *= xq_3;
    rr_r[108] *= xq_3;
    rr_f[25] *= xq_4;
    rr_f[26] *= xq_4;
    rr_f[27] *= xq_4;
    rr_f[28] *= xq_4;
    rr_f[29] *= xq_4;
    rr_f[30] *= xq_4;
    rr_f[31] *= xq_4;
    rr_f[33] *= xq_4;
    rr_f[90] *= xq_4;
    rr_r[35] *= xq_4;
    rr_r[37] *= xq_4;
    rr_r[38] *= xq_4;
    rr_r[39] *= xq_4;
    rr_r[40] *= xq_4;
    rr_r[41] *= xq_4;
    rr_r[42] *= xq_4;
    rr_r[98] *= xq_4;
    rr_r[106] *= xq_4;
    rr_r[112] *= xq_4;
    rr_r[115] *= xq_4;
    rr_r[123] *= xq_4;
    rr_r[129] *= xq_4;
    rr_r[139] *= xq_4;
    rr_r[167] *= xq_4;
    rr_f[137] *= xq_6;
    rr_f[138] *= xq_6;
    rr_f[139] *= xq_6;
    rr_f[140] *= xq_6;
    rr_f[150] *= xq_6;
    rr_r[131] *= xq_6;
    rr_r[132] *= xq_6;
    rr_r[133] *= xq_6;
    rr_r[134] *= xq_6;
    rr_r[135] *= xq_6;
    rr_r[136] *= xq_6;
    rr_f[141] *= xq_8;
    rr_f[153] *= xq_8;
    rr_f[154] *= xq_8;
    rr_r[140] *= xq_8;
  }
  // QSSA connected component
  {
    double a5_0;
    {
      double den = rr_f[54] + rr_f[55] + rr_f[56] + rr_f[57] + rr_f[58] + 
        rr_f[59] + rr_f[60] + rr_f[61] + rr_r[162]; 
      a5_0 = (rr_f[162] + rr_r[54] + rr_r[55] + rr_r[56] + rr_r[57] + rr_r[58] + 
        rr_r[59] + rr_r[60] + rr_r[61] + rr_r[62])/den; 
    }
    double den, xq_5;
    xq_5 = a5_0;
    rr_f[54] *= xq_5;
    rr_f[55] *= xq_5;
    rr_f[56] *= xq_5;
    rr_f[57] *= xq_5;
    rr_f[58] *= xq_5;
    rr_f[59] *= xq_5;
    rr_f[60] *= xq_5;
    rr_f[61] *= xq_5;
    rr_r[162] *= xq_5;
  }
  // QSSA connected component
  {
    double a7_0;
    {
      double den = rr_f[163] + rr_r[161] + rr_r[164];
      a7_0 = (rr_f[161] + rr_f[164] + rr_r[163])/den;
    }
    double den, xq_7;
    xq_7 = a7_0;
    rr_f[163] *= xq_7;
    rr_r[161] *= xq_7;
    rr_r[164] *= xq_7;
  }
  // QSSA connected component
  {
    double a9_0;
    {
      double den = rr_f[156] + rr_f[157] + rr_r[155];
      a9_0 = (rr_f[155] + rr_r[156] + rr_r[157])/den;
    }
    double den, xq_9;
    xq_9 = a9_0;
    rr_f[156] *= xq_9;
    rr_f[157] *= xq_9;
    rr_r[155] *= xq_9;
  }
  double diffusion[30];
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[0]) : 
    "l"(diffusion_array+0*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[1]) : 
    "l"(diffusion_array+1*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[2]) : 
    "l"(diffusion_array+2*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[3]) : 
    "l"(diffusion_array+3*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[4]) : 
    "l"(diffusion_array+4*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[5]) : 
    "l"(diffusion_array+5*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[6]) : 
    "l"(diffusion_array+6*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[7]) : 
    "l"(diffusion_array+7*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[8]) : 
    "l"(diffusion_array+8*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[9]) : 
    "l"(diffusion_array+9*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[10]) : 
    "l"(diffusion_array+10*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[11]) : 
    "l"(diffusion_array+11*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[12]) : 
    "l"(diffusion_array+12*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[13]) : 
    "l"(diffusion_array+13*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[14]) : 
    "l"(diffusion_array+14*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[15]) : 
    "l"(diffusion_array+15*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[16]) : 
    "l"(diffusion_array+16*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[17]) : 
    "l"(diffusion_array+17*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[18]) : 
    "l"(diffusion_array+18*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[19]) : 
    "l"(diffusion_array+19*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[20]) : 
    "l"(diffusion_array+20*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[21]) : 
    "l"(diffusion_array+21*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[22]) : 
    "l"(diffusion_array+22*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[23]) : 
    "l"(diffusion_array+23*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[24]) : 
    "l"(diffusion_array+24*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[25]) : 
    "l"(diffusion_array+25*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[26]) : 
    "l"(diffusion_array+26*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[27]) : 
    "l"(diffusion_array+27*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[28]) : 
    "l"(diffusion_array+28*spec_stride) : "memory"); 
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(diffusion[29]) : 
    "l"(diffusion_array+29*spec_stride) : "memory"); 
  // Stiff species H
  {
    double ddot = rr_f[0] + rr_f[6] + rr_f[7] + rr_f[8] + rr_f[9] + rr_f[10] + 
      rr_f[16] + rr_f[17] + rr_f[27] + rr_f[37] + rr_f[48] + rr_f[49] + rr_f[55] 
      + rr_f[56] + rr_f[64] + rr_f[78] + rr_f[79] + rr_f[85] + rr_f[86] + 
      rr_f[93] + rr_f[94] + rr_f[95] + rr_f[102] + rr_f[108] + rr_f[111] + 
      rr_f[132] + rr_f[146] + rr_f[170] + rr_f[171] + rr_r[1] + rr_r[2] + 
      rr_r[4] + rr_r[4] + rr_r[24] + rr_r[25] + rr_r[30] + rr_r[31] + rr_r[35] + 
      rr_r[43] + rr_r[54] + rr_r[63] + rr_r[71] + rr_r[76] + rr_r[77] + rr_r[91] 
      + rr_r[112] + rr_r[113] + rr_r[114] + rr_r[123] + rr_r[124] + rr_r[125] + 
      rr_r[126] + rr_r[141] + rr_r[163] + rr_r[168] + rr_r[170]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[0])
    {
      double cdot = rr_f[1] + rr_f[2] + rr_f[4] + rr_f[4] + rr_f[24] + rr_f[25] 
        + rr_f[30] + rr_f[31] + rr_f[35] + rr_f[43] + rr_f[54] + rr_f[63] + 
        rr_f[71] + rr_f[76] + rr_f[77] + rr_f[91] + rr_f[112] + rr_f[113] + 
        rr_f[114] + rr_f[123] + rr_f[124] + rr_f[125] + rr_f[126] + rr_f[141] + 
        rr_f[163] + rr_f[168] + rr_f[170] + rr_r[0] + rr_r[6] + rr_r[7] + 
        rr_r[8] + rr_r[9] + rr_r[10] + rr_r[16] + rr_r[17] + rr_r[27] + rr_r[37] 
        + rr_r[48] + rr_r[49] + rr_r[55] + rr_r[56] + rr_r[64] + rr_r[78] + 
        rr_r[79] + rr_r[85] + rr_r[86] + rr_r[93] + rr_r[94] + rr_r[95] + 
        rr_r[102] + rr_r[108] + rr_r[111] + rr_r[132] + rr_r[146] + rr_r[170] + 
        rr_r[171]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[0] * 9.727472047779202e-06;
      double c0 = mole_frac[0] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[0]- c0) / dt) * 
        recip_ddot; 
      rr_f[0] *= scale_r;
      rr_f[6] *= scale_r;
      rr_f[7] *= scale_r;
      rr_f[8] *= scale_r;
      rr_f[9] *= scale_r;
      rr_f[10] *= scale_r;
      rr_f[16] *= scale_r;
      rr_f[17] *= scale_r;
      rr_f[27] *= scale_r;
      rr_f[37] *= scale_r;
      rr_f[48] *= scale_r;
      rr_f[49] *= scale_r;
      rr_f[55] *= scale_r;
      rr_f[56] *= scale_r;
      rr_f[64] *= scale_r;
      rr_f[78] *= scale_r;
      rr_f[79] *= scale_r;
      rr_f[85] *= scale_r;
      rr_f[86] *= scale_r;
      rr_f[93] *= scale_r;
      rr_f[94] *= scale_r;
      rr_f[95] *= scale_r;
      rr_f[102] *= scale_r;
      rr_f[108] *= scale_r;
      rr_f[111] *= scale_r;
      rr_f[132] *= scale_r;
      rr_f[146] *= scale_r;
      rr_f[170] *= scale_r;
      rr_f[171] *= scale_r;
      rr_r[1] *= scale_r;
      rr_r[2] *= scale_r;
      rr_r[4] *= scale_r;
      rr_r[4] *= scale_r;
      rr_r[24] *= scale_r;
      rr_r[25] *= scale_r;
      rr_r[30] *= scale_r;
      rr_r[31] *= scale_r;
      rr_r[35] *= scale_r;
      rr_r[43] *= scale_r;
      rr_r[54] *= scale_r;
      rr_r[63] *= scale_r;
      rr_r[71] *= scale_r;
      rr_r[76] *= scale_r;
      rr_r[77] *= scale_r;
      rr_r[91] *= scale_r;
      rr_r[112] *= scale_r;
      rr_r[113] *= scale_r;
      rr_r[114] *= scale_r;
      rr_r[123] *= scale_r;
      rr_r[124] *= scale_r;
      rr_r[125] *= scale_r;
      rr_r[126] *= scale_r;
      rr_r[141] *= scale_r;
      rr_r[163] *= scale_r;
      rr_r[168] *= scale_r;
      rr_r[170] *= scale_r;
    }
  }
  // Stiff species H2
  {
    double ddot = rr_f[1] + rr_f[2] + rr_f[4] + rr_f[114] + rr_f[125] + rr_r[9] 
      + rr_r[17] + rr_r[27] + rr_r[32] + rr_r[36] + rr_r[37] + rr_r[49] + 
      rr_r[55] + rr_r[79] + rr_r[86] + rr_r[92] + rr_r[95] + rr_r[102] + 
      rr_r[117] + rr_r[122] + rr_r[132] + rr_r[146] + rr_r[166] + rr_r[170] + 
      rr_r[171]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[1])
    {
      double cdot = rr_f[9] + rr_f[17] + rr_f[27] + rr_f[32] + rr_f[36] + 
        rr_f[37] + rr_f[49] + rr_f[55] + rr_f[79] + rr_f[86] + rr_f[92] + 
        rr_f[95] + rr_f[102] + rr_f[117] + rr_f[122] + rr_f[132] + rr_f[146] + 
        rr_f[166] + rr_f[170] + rr_f[171] + rr_r[1] + rr_r[2] + rr_r[4] + 
        rr_r[114] + rr_r[125]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[1] * 4.863736023889601e-06;
      double c0 = mole_frac[1] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[1]- c0) / dt) * 
        recip_ddot; 
      rr_f[1] *= scale_r;
      rr_f[2] *= scale_r;
      rr_f[4] *= scale_r;
      rr_f[114] *= scale_r;
      rr_f[125] *= scale_r;
      rr_r[9] *= scale_r;
      rr_r[17] *= scale_r;
      rr_r[27] *= scale_r;
      rr_r[32] *= scale_r;
      rr_r[36] *= scale_r;
      rr_r[37] *= scale_r;
      rr_r[49] *= scale_r;
      rr_r[55] *= scale_r;
      rr_r[79] *= scale_r;
      rr_r[86] *= scale_r;
      rr_r[92] *= scale_r;
      rr_r[95] *= scale_r;
      rr_r[102] *= scale_r;
      rr_r[117] *= scale_r;
      rr_r[122] *= scale_r;
      rr_r[132] *= scale_r;
      rr_r[146] *= scale_r;
      rr_r[166] *= scale_r;
      rr_r[170] *= scale_r;
      rr_r[171] *= scale_r;
    }
  }
  // Stiff species CH3
  {
    double ddot = rr_f[33] + rr_f[42] + rr_f[43] + rr_f[44] + rr_f[45] + 
      rr_f[46] + rr_f[47] + rr_f[47] + rr_f[48] + rr_f[52] + rr_f[71] + rr_f[71] 
      + rr_f[74] + rr_f[75] + rr_f[76] + rr_f[77] + rr_f[84] + rr_f[97] + 
      rr_f[104] + rr_f[133] + rr_f[147] + rr_f[172] + rr_r[49] + rr_r[50] + 
      rr_r[51] + rr_r[53] + rr_r[56] + rr_r[64] + rr_r[70] + rr_r[72] + rr_r[72] 
      + rr_r[73] + rr_r[73] + rr_r[87] + rr_r[98] + rr_r[110] + rr_r[111] + 
      rr_r[114] + rr_r[125] + rr_r[129] + rr_r[130] + rr_r[137] + rr_r[149]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[2])
    {
      double cdot = rr_f[49] + rr_f[50] + rr_f[51] + rr_f[53] + rr_f[56] + 
        rr_f[64] + rr_f[70] + rr_f[72] + rr_f[72] + rr_f[73] + rr_f[73] + 
        rr_f[87] + rr_f[98] + rr_f[110] + rr_f[111] + rr_f[114] + rr_f[125] + 
        rr_f[129] + rr_f[130] + rr_f[137] + rr_f[149] + rr_r[33] + rr_r[42] + 
        rr_r[43] + rr_r[44] + rr_r[45] + rr_r[46] + rr_r[47] + rr_r[47] + 
        rr_r[48] + rr_r[52] + rr_r[71] + rr_r[71] + rr_r[74] + rr_r[75] + 
        rr_r[76] + rr_r[77] + rr_r[84] + rr_r[97] + rr_r[104] + rr_r[133] + 
        rr_r[147] + rr_r[172]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[2] * 6.521423925145629e-07;
      double c0 = mole_frac[2] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[2]- c0) / dt) * 
        recip_ddot; 
      rr_f[33] *= scale_r;
      rr_f[42] *= scale_r;
      rr_f[43] *= scale_r;
      rr_f[44] *= scale_r;
      rr_f[45] *= scale_r;
      rr_f[46] *= scale_r;
      rr_f[47] *= scale_r;
      rr_f[47] *= scale_r;
      rr_f[48] *= scale_r;
      rr_f[52] *= scale_r;
      rr_f[71] *= scale_r;
      rr_f[71] *= scale_r;
      rr_f[74] *= scale_r;
      rr_f[75] *= scale_r;
      rr_f[76] *= scale_r;
      rr_f[77] *= scale_r;
      rr_f[84] *= scale_r;
      rr_f[97] *= scale_r;
      rr_f[104] *= scale_r;
      rr_f[133] *= scale_r;
      rr_f[147] *= scale_r;
      rr_f[172] *= scale_r;
      rr_r[49] *= scale_r;
      rr_r[50] *= scale_r;
      rr_r[51] *= scale_r;
      rr_r[53] *= scale_r;
      rr_r[56] *= scale_r;
      rr_r[64] *= scale_r;
      rr_r[70] *= scale_r;
      rr_r[72] *= scale_r;
      rr_r[72] *= scale_r;
      rr_r[73] *= scale_r;
      rr_r[73] *= scale_r;
      rr_r[87] *= scale_r;
      rr_r[98] *= scale_r;
      rr_r[110] *= scale_r;
      rr_r[111] *= scale_r;
      rr_r[114] *= scale_r;
      rr_r[125] *= scale_r;
      rr_r[129] *= scale_r;
      rr_r[130] *= scale_r;
      rr_r[137] *= scale_r;
      rr_r[149] *= scale_r;
    }
  }
  // Stiff species O
  {
    double ddot = rr_f[1] + rr_f[3] + rr_f[5] + rr_f[5] + rr_f[6] + rr_f[11] + 
      rr_f[18] + rr_f[21] + rr_f[28] + rr_f[30] + rr_f[38] + rr_f[43] + rr_f[50] 
      + rr_f[57] + rr_f[65] + rr_f[80] + rr_f[87] + rr_f[91] + rr_f[98] + 
      rr_f[100] + rr_f[109] + rr_f[112] + rr_f[122] + rr_f[123] + rr_f[134] + 
      rr_f[145] + rr_f[174] + rr_r[0] + rr_r[22] + rr_r[44]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[3])
    {
      double cdot = rr_f[0] + rr_f[22] + rr_f[44] + rr_r[1] + rr_r[3] + rr_r[5] 
        + rr_r[5] + rr_r[6] + rr_r[11] + rr_r[18] + rr_r[21] + rr_r[28] + 
        rr_r[30] + rr_r[38] + rr_r[43] + rr_r[50] + rr_r[57] + rr_r[65] + 
        rr_r[80] + rr_r[87] + rr_r[91] + rr_r[98] + rr_r[100] + rr_r[109] + 
        rr_r[112] + rr_r[122] + rr_r[123] + rr_r[134] + rr_r[145] + rr_r[174]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[3] * 6.128354813305501e-07;
      double c0 = mole_frac[3] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[3]- c0) / dt) * 
        recip_ddot; 
      rr_f[1] *= scale_r;
      rr_f[3] *= scale_r;
      rr_f[5] *= scale_r;
      rr_f[5] *= scale_r;
      rr_f[6] *= scale_r;
      rr_f[11] *= scale_r;
      rr_f[18] *= scale_r;
      rr_f[21] *= scale_r;
      rr_f[28] *= scale_r;
      rr_f[30] *= scale_r;
      rr_f[38] *= scale_r;
      rr_f[43] *= scale_r;
      rr_f[50] *= scale_r;
      rr_f[57] *= scale_r;
      rr_f[65] *= scale_r;
      rr_f[80] *= scale_r;
      rr_f[87] *= scale_r;
      rr_f[91] *= scale_r;
      rr_f[98] *= scale_r;
      rr_f[100] *= scale_r;
      rr_f[109] *= scale_r;
      rr_f[112] *= scale_r;
      rr_f[122] *= scale_r;
      rr_f[123] *= scale_r;
      rr_f[134] *= scale_r;
      rr_f[145] *= scale_r;
      rr_f[174] *= scale_r;
      rr_r[0] *= scale_r;
      rr_r[22] *= scale_r;
      rr_r[44] *= scale_r;
    }
  }
  // Stiff species CH4
  {
    double ddot = rr_f[49] + rr_f[50] + rr_f[51] + rr_f[53] + rr_f[72] + 
      rr_f[73] + rr_r[33] + rr_r[42] + rr_r[48] + rr_r[52] + rr_r[84] + rr_r[97] 
      + rr_r[104] + rr_r[133] + rr_r[147] + rr_r[172]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[4])
    {
      double cdot = rr_f[33] + rr_f[42] + rr_f[48] + rr_f[52] + rr_f[84] + 
        rr_f[97] + rr_f[104] + rr_f[133] + rr_f[147] + rr_f[172] + rr_r[49] + 
        rr_r[50] + rr_r[51] + rr_r[53] + rr_r[72] + rr_r[73]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[4] * 6.111688378068234e-07;
      double c0 = mole_frac[4] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[4]- c0) / dt) * 
        recip_ddot; 
      rr_f[49] *= scale_r;
      rr_f[50] *= scale_r;
      rr_f[51] *= scale_r;
      rr_f[53] *= scale_r;
      rr_f[72] *= scale_r;
      rr_f[73] *= scale_r;
      rr_r[33] *= scale_r;
      rr_r[42] *= scale_r;
      rr_r[48] *= scale_r;
      rr_r[52] *= scale_r;
      rr_r[84] *= scale_r;
      rr_r[97] *= scale_r;
      rr_r[104] *= scale_r;
      rr_r[133] *= scale_r;
      rr_r[147] *= scale_r;
      rr_r[172] *= scale_r;
    }
  }
  // Stiff species OH
  {
    double ddot = rr_f[2] + rr_f[7] + rr_f[12] + rr_f[19] + rr_f[20] + rr_f[24] 
      + rr_f[29] + rr_f[39] + rr_f[51] + rr_f[58] + rr_f[66] + rr_f[74] + 
      rr_f[75] + rr_f[81] + rr_f[96] + rr_f[99] + rr_f[110] + rr_f[113] + 
      rr_f[124] + rr_f[131] + rr_f[143] + rr_f[164] + rr_f[168] + rr_f[169] + 
      rr_r[0] + rr_r[1] + rr_r[3] + rr_r[3] + rr_r[6] + rr_r[10] + rr_r[10] + 
      rr_r[11] + rr_r[15] + rr_r[15] + rr_r[16] + rr_r[18] + rr_r[23] + rr_r[28] 
      + rr_r[31] + rr_r[38] + rr_r[45] + rr_r[46] + rr_r[50] + rr_r[56] + 
      rr_r[57] + rr_r[64] + rr_r[65] + rr_r[80] + rr_r[100] + rr_r[115] + 
      rr_r[116] + rr_r[126] + rr_r[134] + rr_r[140] + rr_r[145] + rr_r[156] + 
      rr_r[158] + rr_r[159] + rr_r[167] + rr_r[169] + rr_r[171] + rr_r[172] + 
      rr_r[173] + rr_r[174] + rr_r[174]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[5])
    {
      double cdot = rr_f[0] + rr_f[1] + rr_f[3] + rr_f[3] + rr_f[6] + rr_f[10] + 
        rr_f[10] + rr_f[11] + rr_f[15] + rr_f[15] + rr_f[16] + rr_f[18] + 
        rr_f[23] + rr_f[28] + rr_f[31] + rr_f[38] + rr_f[45] + rr_f[46] + 
        rr_f[50] + rr_f[56] + rr_f[57] + rr_f[64] + rr_f[65] + rr_f[80] + 
        rr_f[100] + rr_f[115] + rr_f[116] + rr_f[126] + rr_f[134] + rr_f[140] + 
        rr_f[145] + rr_f[156] + rr_f[158] + rr_f[159] + rr_f[167] + rr_f[169] + 
        rr_f[171] + rr_f[172] + rr_f[173] + rr_f[174] + rr_f[174] + rr_r[2] + 
        rr_r[7] + rr_r[12] + rr_r[19] + rr_r[20] + rr_r[24] + rr_r[29] + 
        rr_r[39] + rr_r[51] + rr_r[58] + rr_r[66] + rr_r[74] + rr_r[75] + 
        rr_r[81] + rr_r[96] + rr_r[99] + rr_r[110] + rr_r[113] + rr_r[124] + 
        rr_r[131] + rr_r[143] + rr_r[164] + rr_r[168] + rr_r[169]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[5] * 5.765147697733397e-07;
      double c0 = mole_frac[5] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[5]- c0) / dt) * 
        recip_ddot; 
      rr_f[2] *= scale_r;
      rr_f[7] *= scale_r;
      rr_f[12] *= scale_r;
      rr_f[19] *= scale_r;
      rr_f[20] *= scale_r;
      rr_f[24] *= scale_r;
      rr_f[29] *= scale_r;
      rr_f[39] *= scale_r;
      rr_f[51] *= scale_r;
      rr_f[58] *= scale_r;
      rr_f[66] *= scale_r;
      rr_f[74] *= scale_r;
      rr_f[75] *= scale_r;
      rr_f[81] *= scale_r;
      rr_f[96] *= scale_r;
      rr_f[99] *= scale_r;
      rr_f[110] *= scale_r;
      rr_f[113] *= scale_r;
      rr_f[124] *= scale_r;
      rr_f[131] *= scale_r;
      rr_f[143] *= scale_r;
      rr_f[164] *= scale_r;
      rr_f[168] *= scale_r;
      rr_f[169] *= scale_r;
      rr_r[0] *= scale_r;
      rr_r[1] *= scale_r;
      rr_r[3] *= scale_r;
      rr_r[3] *= scale_r;
      rr_r[6] *= scale_r;
      rr_r[10] *= scale_r;
      rr_r[10] *= scale_r;
      rr_r[11] *= scale_r;
      rr_r[15] *= scale_r;
      rr_r[15] *= scale_r;
      rr_r[16] *= scale_r;
      rr_r[18] *= scale_r;
      rr_r[23] *= scale_r;
      rr_r[28] *= scale_r;
      rr_r[31] *= scale_r;
      rr_r[38] *= scale_r;
      rr_r[45] *= scale_r;
      rr_r[46] *= scale_r;
      rr_r[50] *= scale_r;
      rr_r[56] *= scale_r;
      rr_r[57] *= scale_r;
      rr_r[64] *= scale_r;
      rr_r[65] *= scale_r;
      rr_r[80] *= scale_r;
      rr_r[100] *= scale_r;
      rr_r[115] *= scale_r;
      rr_r[116] *= scale_r;
      rr_r[126] *= scale_r;
      rr_r[134] *= scale_r;
      rr_r[140] *= scale_r;
      rr_r[145] *= scale_r;
      rr_r[156] *= scale_r;
      rr_r[158] *= scale_r;
      rr_r[159] *= scale_r;
      rr_r[167] *= scale_r;
      rr_r[169] *= scale_r;
      rr_r[171] *= scale_r;
      rr_r[172] *= scale_r;
      rr_r[173] *= scale_r;
      rr_r[174] *= scale_r;
      rr_r[174] *= scale_r;
    }
  }
  // Stiff species C2H4
  {
    double ddot = rr_f[92] + rr_f[93] + rr_f[95] + rr_f[96] + rr_f[97] + 
      rr_f[98] + rr_f[100] + rr_f[101] + rr_r[76] + rr_r[77] + rr_r[86] + 
      rr_r[88] + rr_r[89] + rr_r[94] + rr_r[103] + rr_r[105]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[9])
    {
      double cdot = rr_f[76] + rr_f[77] + rr_f[86] + rr_f[88] + rr_f[89] + 
        rr_f[94] + rr_f[103] + rr_f[105] + rr_r[92] + rr_r[93] + rr_r[95] + 
        rr_r[96] + rr_r[97] + rr_r[98] + rr_r[100] + rr_r[101]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[9] * 3.495022845080485e-07;
      double c0 = mole_frac[9] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[9]- c0) / dt) * 
        recip_ddot; 
      rr_f[92] *= scale_r;
      rr_f[93] *= scale_r;
      rr_f[95] *= scale_r;
      rr_f[96] *= scale_r;
      rr_f[97] *= scale_r;
      rr_f[98] *= scale_r;
      rr_f[100] *= scale_r;
      rr_f[101] *= scale_r;
      rr_r[76] *= scale_r;
      rr_r[77] *= scale_r;
      rr_r[86] *= scale_r;
      rr_r[88] *= scale_r;
      rr_r[89] *= scale_r;
      rr_r[94] *= scale_r;
      rr_r[103] *= scale_r;
      rr_r[105] *= scale_r;
    }
  }
  // Stiff species C2H5
  {
    double ddot = rr_f[85] + rr_f[86] + rr_f[87] + rr_f[88] + rr_f[89] + 
      rr_f[89] + rr_f[90] + rr_f[91] + rr_r[71] + rr_r[79] + rr_r[80] + rr_r[81] 
      + rr_r[82] + rr_r[83] + rr_r[84] + rr_r[93]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[10])
    {
      double cdot = rr_f[71] + rr_f[79] + rr_f[80] + rr_f[81] + rr_f[82] + 
        rr_f[83] + rr_f[84] + rr_f[93] + rr_r[85] + rr_r[86] + rr_r[87] + 
        rr_r[88] + rr_r[89] + rr_r[89] + rr_r[90] + rr_r[91]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[10] * 3.373804071618928e-07;
      double c0 = mole_frac[10] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[10]- c0) / dt) 
        * recip_ddot; 
      rr_f[85] *= scale_r;
      rr_f[86] *= scale_r;
      rr_f[87] *= scale_r;
      rr_f[88] *= scale_r;
      rr_f[89] *= scale_r;
      rr_f[89] *= scale_r;
      rr_f[90] *= scale_r;
      rr_f[91] *= scale_r;
      rr_r[71] *= scale_r;
      rr_r[79] *= scale_r;
      rr_r[80] *= scale_r;
      rr_r[81] *= scale_r;
      rr_r[82] *= scale_r;
      rr_r[83] *= scale_r;
      rr_r[84] *= scale_r;
      rr_r[93] *= scale_r;
    }
  }
  // Stiff species CH2O
  {
    double ddot = rr_f[35] + rr_f[36] + rr_f[37] + rr_f[38] + rr_f[39] + 
      rr_f[40] + rr_f[41] + rr_f[42] + rr_f[139] + rr_f[164] + rr_r[34] + 
      rr_r[43] + rr_r[45] + rr_r[54] + rr_r[55] + rr_r[57] + rr_r[58] + rr_r[59] 
      + rr_r[60] + rr_r[61] + rr_r[62] + rr_r[62] + rr_r[63] + rr_r[65] + 
      rr_r[66] + rr_r[67] + rr_r[68] + rr_r[69] + rr_r[87] + rr_r[106] + 
      rr_r[113] + rr_r[116] + rr_r[124] + rr_r[128] + rr_r[137] + rr_r[138] + 
      rr_r[153] + rr_r[156] + rr_r[156]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[11])
    {
      double cdot = rr_f[34] + rr_f[43] + rr_f[45] + rr_f[54] + rr_f[55] + 
        rr_f[57] + rr_f[58] + rr_f[59] + rr_f[60] + rr_f[61] + rr_f[62] + 
        rr_f[62] + rr_f[63] + rr_f[65] + rr_f[66] + rr_f[67] + rr_f[68] + 
        rr_f[69] + rr_f[87] + rr_f[106] + rr_f[113] + rr_f[116] + rr_f[124] + 
        rr_f[128] + rr_f[137] + rr_f[138] + rr_f[153] + rr_f[156] + rr_f[156] + 
        rr_r[35] + rr_r[36] + rr_r[37] + rr_r[38] + rr_r[39] + rr_r[40] + 
        rr_r[41] + rr_r[42] + rr_r[139] + rr_r[164]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[11] * 3.2654499410354e-07;
      double c0 = mole_frac[11] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[11]- c0) / dt) 
        * recip_ddot; 
      rr_f[35] *= scale_r;
      rr_f[36] *= scale_r;
      rr_f[37] *= scale_r;
      rr_f[38] *= scale_r;
      rr_f[39] *= scale_r;
      rr_f[40] *= scale_r;
      rr_f[41] *= scale_r;
      rr_f[42] *= scale_r;
      rr_f[139] *= scale_r;
      rr_f[164] *= scale_r;
      rr_r[34] *= scale_r;
      rr_r[43] *= scale_r;
      rr_r[45] *= scale_r;
      rr_r[54] *= scale_r;
      rr_r[55] *= scale_r;
      rr_r[57] *= scale_r;
      rr_r[58] *= scale_r;
      rr_r[59] *= scale_r;
      rr_r[60] *= scale_r;
      rr_r[61] *= scale_r;
      rr_r[62] *= scale_r;
      rr_r[62] *= scale_r;
      rr_r[63] *= scale_r;
      rr_r[65] *= scale_r;
      rr_r[66] *= scale_r;
      rr_r[67] *= scale_r;
      rr_r[68] *= scale_r;
      rr_r[69] *= scale_r;
      rr_r[87] *= scale_r;
      rr_r[106] *= scale_r;
      rr_r[113] *= scale_r;
      rr_r[116] *= scale_r;
      rr_r[124] *= scale_r;
      rr_r[128] *= scale_r;
      rr_r[137] *= scale_r;
      rr_r[138] *= scale_r;
      rr_r[153] *= scale_r;
      rr_r[156] *= scale_r;
      rr_r[156] *= scale_r;
    }
  }
  // Stiff species C2H6
  {
    double ddot = rr_f[79] + rr_f[80] + rr_f[81] + rr_f[82] + rr_f[83] + 
      rr_f[84] + rr_r[47] + rr_r[85] + rr_r[89] + rr_r[90]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[12])
    {
      double cdot = rr_f[47] + rr_f[85] + rr_f[89] + rr_f[90] + rr_r[79] + 
        rr_r[80] + rr_r[81] + rr_r[82] + rr_r[83] + rr_r[84]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[12] * 3.260711962572814e-07;
      double c0 = mole_frac[12] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[12]- c0) / dt) 
        * recip_ddot; 
      rr_f[79] *= scale_r;
      rr_f[80] *= scale_r;
      rr_f[81] *= scale_r;
      rr_f[82] *= scale_r;
      rr_f[83] *= scale_r;
      rr_f[84] *= scale_r;
      rr_r[47] *= scale_r;
      rr_r[85] *= scale_r;
      rr_r[89] *= scale_r;
      rr_r[90] *= scale_r;
    }
  }
  // Stiff species CH3O
  {
    double ddot = rr_f[63] + rr_f[64] + rr_f[65] + rr_f[66] + rr_f[67] + 
      rr_f[68] + rr_f[69] + rr_f[70] + rr_f[78] + rr_f[138] + rr_r[44] + 
      rr_r[46] + rr_r[130] + rr_r[148] + rr_r[153]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[13])
    {
      double cdot = rr_f[44] + rr_f[46] + rr_f[130] + rr_f[148] + rr_f[153] + 
        rr_r[63] + rr_r[64] + rr_r[65] + rr_r[66] + rr_r[67] + rr_r[68] + 
        rr_r[69] + rr_r[70] + rr_r[78] + rr_r[138]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[13] * 3.159391205775774e-07;
      double c0 = mole_frac[13] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[13]- c0) / dt) 
        * recip_ddot; 
      rr_f[63] *= scale_r;
      rr_f[64] *= scale_r;
      rr_f[65] *= scale_r;
      rr_f[66] *= scale_r;
      rr_f[67] *= scale_r;
      rr_f[68] *= scale_r;
      rr_f[69] *= scale_r;
      rr_f[70] *= scale_r;
      rr_f[78] *= scale_r;
      rr_f[138] *= scale_r;
      rr_r[44] *= scale_r;
      rr_r[46] *= scale_r;
      rr_r[130] *= scale_r;
      rr_r[148] *= scale_r;
      rr_r[153] *= scale_r;
    }
  }
  // Stiff species HO2
  {
    double ddot = rr_f[9] + rr_f[10] + rr_f[11] + rr_f[12] + rr_f[13] + rr_f[13] 
      + rr_f[14] + rr_f[14] + rr_f[23] + rr_f[31] + rr_f[41] + rr_f[46] + 
      rr_f[52] + rr_f[53] + rr_f[61] + rr_f[69] + rr_f[83] + rr_f[116] + 
      rr_f[135] + rr_f[140] + rr_f[144] + rr_f[173] + rr_r[8] + rr_r[17] + 
      rr_r[18] + rr_r[19] + rr_r[20] + rr_r[26] + rr_r[40] + rr_r[59] + rr_r[60] 
      + rr_r[67] + rr_r[68] + rr_r[82] + rr_r[88] + rr_r[101] + rr_r[103] + 
      rr_r[107] + rr_r[136] + rr_r[142] + rr_r[154]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[15])
    {
      double cdot = rr_f[8] + rr_f[17] + rr_f[18] + rr_f[19] + rr_f[20] + 
        rr_f[26] + rr_f[40] + rr_f[59] + rr_f[60] + rr_f[67] + rr_f[68] + 
        rr_f[82] + rr_f[88] + rr_f[101] + rr_f[103] + rr_f[107] + rr_f[136] + 
        rr_f[142] + rr_f[154] + rr_r[9] + rr_r[10] + rr_r[11] + rr_r[12] + 
        rr_r[13] + rr_r[13] + rr_r[14] + rr_r[14] + rr_r[23] + rr_r[31] + 
        rr_r[41] + rr_r[46] + rr_r[52] + rr_r[53] + rr_r[61] + rr_r[69] + 
        rr_r[83] + rr_r[116] + rr_r[135] + rr_r[140] + rr_r[144] + rr_r[173]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[15] * 2.970602697567803e-07;
      double c0 = mole_frac[15] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[15]- c0) / dt) 
        * recip_ddot; 
      rr_f[9] *= scale_r;
      rr_f[10] *= scale_r;
      rr_f[11] *= scale_r;
      rr_f[12] *= scale_r;
      rr_f[13] *= scale_r;
      rr_f[13] *= scale_r;
      rr_f[14] *= scale_r;
      rr_f[14] *= scale_r;
      rr_f[23] *= scale_r;
      rr_f[31] *= scale_r;
      rr_f[41] *= scale_r;
      rr_f[46] *= scale_r;
      rr_f[52] *= scale_r;
      rr_f[53] *= scale_r;
      rr_f[61] *= scale_r;
      rr_f[69] *= scale_r;
      rr_f[83] *= scale_r;
      rr_f[116] *= scale_r;
      rr_f[135] *= scale_r;
      rr_f[140] *= scale_r;
      rr_f[144] *= scale_r;
      rr_f[173] *= scale_r;
      rr_r[8] *= scale_r;
      rr_r[17] *= scale_r;
      rr_r[18] *= scale_r;
      rr_r[19] *= scale_r;
      rr_r[20] *= scale_r;
      rr_r[26] *= scale_r;
      rr_r[40] *= scale_r;
      rr_r[59] *= scale_r;
      rr_r[60] *= scale_r;
      rr_r[67] *= scale_r;
      rr_r[68] *= scale_r;
      rr_r[82] *= scale_r;
      rr_r[88] *= scale_r;
      rr_r[101] *= scale_r;
      rr_r[103] *= scale_r;
      rr_r[107] *= scale_r;
      rr_r[136] *= scale_r;
      rr_r[142] *= scale_r;
      rr_r[154] *= scale_r;
    }
  }
  // Stiff species H2O2
  {
    double ddot = rr_f[15] + rr_f[16] + rr_f[17] + rr_f[18] + rr_f[19] + 
      rr_f[20] + rr_f[103] + rr_r[13] + rr_r[14] + rr_r[41] + rr_r[53] + 
      rr_r[61] + rr_r[69] + rr_r[83] + rr_r[135] + rr_r[144] + rr_r[173]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[16])
    {
      double cdot = rr_f[13] + rr_f[14] + rr_f[41] + rr_f[53] + rr_f[61] + 
        rr_f[69] + rr_f[83] + rr_f[135] + rr_f[144] + rr_f[173] + rr_r[15] + 
        rr_r[16] + rr_r[17] + rr_r[18] + rr_r[19] + rr_r[20] + rr_r[103]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[16] * 2.882573848866699e-07;
      double c0 = mole_frac[16] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[16]- c0) / dt) 
        * recip_ddot; 
      rr_f[15] *= scale_r;
      rr_f[16] *= scale_r;
      rr_f[17] *= scale_r;
      rr_f[18] *= scale_r;
      rr_f[19] *= scale_r;
      rr_f[20] *= scale_r;
      rr_f[103] *= scale_r;
      rr_r[13] *= scale_r;
      rr_r[14] *= scale_r;
      rr_r[41] *= scale_r;
      rr_r[53] *= scale_r;
      rr_r[61] *= scale_r;
      rr_r[69] *= scale_r;
      rr_r[83] *= scale_r;
      rr_r[135] *= scale_r;
      rr_r[144] *= scale_r;
      rr_r[173] *= scale_r;
    }
  }
  // Stiff species CH3HCO
  {
    double ddot = rr_f[129] + rr_r[91];
    if ((ddot * dt * 0.09058503722621332) > mole_frac[18])
    {
      double cdot = rr_f[91] + rr_r[129];
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[18] * 2.225698796783372e-07;
      double c0 = mole_frac[18] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[18]- c0) / dt) 
        * recip_ddot; 
      rr_f[129] *= scale_r;
      rr_r[91] *= scale_r;
    }
  }
  // Stiff species HCOOH
  {
    double ddot = rr_f[165] + rr_f[166] + rr_f[167] + rr_f[168] + rr_f[169] + 
      rr_f[170] + rr_f[171] + rr_f[172] + rr_f[173] + rr_f[174] + rr_r[163]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[19])
    {
      double cdot = rr_f[163] + rr_r[165] + rr_r[166] + rr_r[167] + rr_r[168] + 
        rr_r[169] + rr_r[170] + rr_r[171] + rr_r[172] + rr_r[173] + rr_r[174]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[19] * 2.130322737919897e-07;
      double c0 = mole_frac[19] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[19]- c0) / dt) 
        * recip_ddot; 
      rr_f[165] *= scale_r;
      rr_f[166] *= scale_r;
      rr_f[167] *= scale_r;
      rr_f[168] *= scale_r;
      rr_f[169] *= scale_r;
      rr_f[170] *= scale_r;
      rr_f[171] *= scale_r;
      rr_f[172] *= scale_r;
      rr_f[173] *= scale_r;
      rr_f[174] *= scale_r;
      rr_r[163] *= scale_r;
    }
  }
  // Stiff species CH3OCH3
  {
    double ddot = rr_f[130] + rr_f[131] + rr_f[132] + rr_f[133] + rr_f[134] + 
      rr_f[135] + rr_f[136] + rr_r[138] + rr_r[139]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[20])
    {
      double cdot = rr_f[138] + rr_f[139] + rr_r[130] + rr_r[131] + rr_r[132] + 
        rr_r[133] + rr_r[134] + rr_r[135] + rr_r[136]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[20] * 2.128305222194632e-07;
      double c0 = mole_frac[20] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[20]- c0) / dt) 
        * recip_ddot; 
      rr_f[130] *= scale_r;
      rr_f[131] *= scale_r;
      rr_f[132] *= scale_r;
      rr_f[133] *= scale_r;
      rr_f[134] *= scale_r;
      rr_f[135] *= scale_r;
      rr_f[136] *= scale_r;
      rr_r[138] *= scale_r;
      rr_r[139] *= scale_r;
    }
  }
  // Stiff species CH3OCHO
  {
    double ddot = rr_f[142] + rr_f[143] + rr_f[144] + rr_f[145] + rr_f[146] + 
      rr_f[147] + rr_r[141] + rr_r[152] + rr_r[154]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[22])
    {
      double cdot = rr_f[141] + rr_f[152] + rr_f[154] + rr_r[142] + rr_r[143] + 
        rr_r[144] + rr_r[145] + rr_r[146] + rr_r[147]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[22] * 1.6327249705177e-07;
      double c0 = mole_frac[22] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[22]- c0) / dt) 
        * recip_ddot; 
      rr_f[142] *= scale_r;
      rr_f[143] *= scale_r;
      rr_f[144] *= scale_r;
      rr_f[145] *= scale_r;
      rr_f[146] *= scale_r;
      rr_f[147] *= scale_r;
      rr_r[141] *= scale_r;
      rr_r[152] *= scale_r;
      rr_r[154] *= scale_r;
    }
  }
  // Stiff species OCH2OCHO
  {
    double ddot = rr_f[160] + rr_r[159];
    if ((ddot * dt * 0.09058503722621332) > mole_frac[24])
    {
      double cdot = rr_f[159] + rr_r[160];
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[24] * 1.306559675797305e-07;
      double c0 = mole_frac[24] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[24]- c0) / dt) 
        * recip_ddot; 
      rr_f[160] *= scale_r;
      rr_r[159] *= scale_r;
    }
  }
  // Stiff species HOCH2OCO
  {
    double ddot = rr_f[161] + rr_f[162] + rr_r[160];
    if ((ddot * dt * 0.09058503722621332) > mole_frac[25])
    {
      double cdot = rr_f[160] + rr_r[161] + rr_r[162];
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[25] * 1.306559675797305e-07;
      double c0 = mole_frac[25] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[25]- c0) / dt) 
        * recip_ddot; 
      rr_f[161] *= scale_r;
      rr_f[162] *= scale_r;
      rr_r[160] *= scale_r;
    }
  }
  // Stiff species CH3OCH2O2
  {
    double ddot = rr_f[151] + rr_f[151] + rr_f[152] + rr_f[152] + rr_f[155] + 
      rr_r[150]; 
    if ((ddot * dt * 0.09058503722621332) > mole_frac[26])
    {
      double cdot = rr_f[150] + rr_r[151] + rr_r[151] + rr_r[152] + rr_r[152] + 
        rr_r[155]; 
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[26] * 1.272379375385656e-07;
      double c0 = mole_frac[26] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[26]- c0) / dt) 
        * recip_ddot; 
      rr_f[151] *= scale_r;
      rr_f[151] *= scale_r;
      rr_f[152] *= scale_r;
      rr_f[152] *= scale_r;
      rr_f[155] *= scale_r;
      rr_r[150] *= scale_r;
    }
  }
  // Stiff species HO2CH2OCHO
  {
    double ddot = rr_f[159] + rr_r[158];
    if ((ddot * dt * 0.09058503722621332) > mole_frac[27])
    {
      double cdot = rr_f[158] + rr_r[159];
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[27] * 1.065161368959949e-07;
      double c0 = mole_frac[27] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[27]- c0) / dt) 
        * recip_ddot; 
      rr_f[159] *= scale_r;
      rr_r[158] *= scale_r;
    }
  }
  // Stiff species O2CH2OCH2O2H
  {
    double ddot = rr_f[158] + rr_r[157];
    if ((ddot * dt * 0.09058503722621332) > mole_frac[28])
    {
      double cdot = rr_f[157] + rr_r[158];
      double recip_ddot = 1.0/ddot;
      double part_sum = cdot + diffusion[28] * 8.990534035887869e-08;
      double c0 = mole_frac[28] * part_sum * recip_ddot;
      double scale_r = (part_sum + 11.0393507649917 * (mole_frac[28]- c0) / dt) 
        * recip_ddot; 
      rr_f[158] *= scale_r;
      rr_r[157] *= scale_r;
    }
  }
  double wdot[30];
  double ropl[175];
  for (int i = 0; i < 175; i++)
  {
    ropl[i] = rr_f[i] - rr_r[i];
  }
  // 0. H
  wdot[0] = 1.00797e-03 * (-ropl[0] + ropl[1] + ropl[2] + 2.0*ropl[4] - ropl[6] 
    - ropl[7] - ropl[8] - ropl[9] - ropl[10] - ropl[16] - ropl[17] + ropl[24] + 
    ropl[25] - ropl[27] + ropl[30] + ropl[31] + ropl[35] - ropl[37] + ropl[43] - 
    ropl[48] - ropl[49] + ropl[54] - ropl[55] - ropl[56] + ropl[63] - ropl[64] + 
    ropl[71] + ropl[76] + ropl[77] - ropl[78] - ropl[79] - ropl[85] - ropl[86] + 
    ropl[91] - ropl[93] - ropl[94] - ropl[95] - ropl[102] - ropl[108] - 
    ropl[111] + ropl[112] + ropl[113] + ropl[114] + ropl[123] + ropl[124] + 
    ropl[125] + ropl[126] - ropl[132] + ropl[141] - ropl[146] + ropl[163] + 
    ropl[168] - ropl[171]); 
  // 1. H2
  wdot[1] = 2.01594e-03 * (-ropl[1] - ropl[2] - ropl[4] + ropl[9] + ropl[17] + 
    ropl[27] + ropl[32] + ropl[36] + ropl[37] + ropl[49] + ropl[55] + ropl[79] + 
    ropl[86] + ropl[92] + ropl[95] + ropl[102] - ropl[114] + ropl[117] + 
    ropl[122] - ropl[125] + ropl[132] + ropl[146] + ropl[166] + ropl[170] + 
    ropl[171]); 
  // 2. CH3
  wdot[2] = 0.01503506 * (-ropl[33] - ropl[42] - ropl[43] - ropl[44] - ropl[45] 
    - ropl[46] - 2.0*ropl[47] - ropl[48] + ropl[49] + ropl[50] + ropl[51] - 
    ropl[52] + ropl[53] + ropl[56] + ropl[64] + ropl[70] - 2.0*ropl[71] + 
    2.0*ropl[72] + 2.0*ropl[73] - ropl[74] - ropl[75] - ropl[76] - ropl[77] - 
    ropl[84] + ropl[87] - ropl[97] + ropl[98] - ropl[104] + ropl[110] + 
    ropl[111] + ropl[114] + ropl[125] + ropl[129] + ropl[130] - ropl[133] + 
    ropl[137] - ropl[147] + ropl[149] - ropl[172]); 
  // 3. O
  wdot[3] = 0.0159994 * (ropl[0] - ropl[1] - ropl[3] - 2.0*ropl[5] - ropl[6] - 
    ropl[11] - ropl[18] - ropl[21] + ropl[22] - ropl[28] - ropl[30] - ropl[38] - 
    ropl[43] + ropl[44] - ropl[50] - ropl[57] - ropl[65] - ropl[80] - ropl[87] - 
    ropl[91] - ropl[98] - ropl[100] - ropl[109] - ropl[112] - ropl[122] - 
    ropl[123] - ropl[134] - ropl[145] - ropl[174]); 
  // 4. CH4
  wdot[4] = 0.01604303 * (ropl[33] + ropl[42] + ropl[48] - ropl[49] - ropl[50] - 
    ropl[51] + ropl[52] - ropl[53] - ropl[72] - ropl[73] + ropl[84] + ropl[97] + 
    ropl[104] + ropl[133] + ropl[147] + ropl[172]); 
  // 5. OH
  wdot[5] = 0.01700737 * (ropl[0] + ropl[1] - ropl[2] + 2.0*ropl[3] + ropl[6] - 
    ropl[7] + 2.0*ropl[10] + ropl[11] - ropl[12] + 2.0*ropl[15] + ropl[16] + 
    ropl[18] - ropl[19] - ropl[20] + ropl[23] - ropl[24] + ropl[28] - ropl[29] + 
    ropl[31] + ropl[38] - ropl[39] + ropl[45] + ropl[46] + ropl[50] - ropl[51] + 
    ropl[56] + ropl[57] - ropl[58] + ropl[64] + ropl[65] - ropl[66] - ropl[74] - 
    ropl[75] + ropl[80] - ropl[81] - ropl[96] - ropl[99] + ropl[100] - ropl[110] 
    - ropl[113] + ropl[115] + ropl[116] - ropl[124] + ropl[126] - ropl[131] + 
    ropl[134] + ropl[140] - ropl[143] + ropl[145] + ropl[156] + ropl[158] + 
    ropl[159] - ropl[164] + ropl[167] - ropl[168] + ropl[171] + ropl[172] + 
    ropl[173] + 2.0*ropl[174]); 
  // 6. H2O
  wdot[6] = 0.01801534 * (ropl[2] - ropl[3] + ropl[7] + ropl[12] + ropl[16] + 
    ropl[19] + ropl[20] + ropl[29] + ropl[39] + ropl[51] + ropl[58] + ropl[66] + 
    ropl[74] + ropl[75] + ropl[78] + ropl[81] + ropl[96] + ropl[99] + ropl[127] 
    + ropl[131] + ropl[143] + ropl[165] + ropl[168] + ropl[169]); 
  // 7. C2H2
  wdot[7] = 0.02603824 * (ropl[92] + ropl[99] + ropl[102] + ropl[104] + 
    ropl[105] + ropl[107] - ropl[108] - ropl[109] - ropl[110] + ropl[117]); 
  // 8. CO
  wdot[8] = 0.02801055 * (-ropl[21] - ropl[22] - ropl[23] - ropl[24] + ropl[25] 
    + ropl[26] + ropl[27] + ropl[28] + ropl[29] + 2.0*ropl[32] + ropl[33] + 
    ropl[34] + ropl[36] - ropl[70] + ropl[90] + ropl[109] + ropl[110] + 
    ropl[122] + ropl[126] + ropl[127] + ropl[128] + ropl[148] + ropl[161] + 
    ropl[165] + ropl[169] + ropl[171] + ropl[172] + ropl[173] + ropl[174]); 
  // 9. C2H4
  wdot[9] = 0.02805418 * (ropl[76] + ropl[77] + ropl[86] + ropl[88] + ropl[89] - 
    ropl[92] - ropl[93] + ropl[94] - ropl[95] - ropl[96] - ropl[97] - ropl[98] - 
    ropl[100] - ropl[101] + ropl[103] + ropl[105]); 
  // 10. C2H5
  wdot[10] = 0.02906215 * (ropl[71] + ropl[79] + ropl[80] + ropl[81] + ropl[82] 
    + ropl[83] + ropl[84] - ropl[85] - ropl[86] - ropl[87] - ropl[88] - 
    2.0*ropl[89] - ropl[90] - ropl[91] + ropl[93]); 
  // 11. CH2O
  wdot[11] = 0.03002649 * (ropl[34] - ropl[35] - ropl[36] - ropl[37] - ropl[38] 
    - ropl[39] - ropl[40] - ropl[41] - ropl[42] + ropl[43] + ropl[45] + ropl[54] 
    + ropl[55] + ropl[57] + ropl[58] + ropl[59] + ropl[60] + ropl[61] + 
    2.0*ropl[62] + ropl[63] + ropl[65] + ropl[66] + ropl[67] + ropl[68] + 
    ropl[69] + ropl[87] + ropl[106] + ropl[113] + ropl[116] + ropl[124] + 
    ropl[128] + ropl[137] + ropl[138] - ropl[139] + ropl[153] + 2.0*ropl[156] - 
    ropl[164]); 
  // 12. C2H6
  wdot[12] = 0.03007012 * (ropl[47] - ropl[79] - ropl[80] - ropl[81] - ropl[82] 
    - ropl[83] - ropl[84] + ropl[85] + ropl[89] + ropl[90]); 
  // 13. CH3O
  wdot[13] = 0.03103446 * (ropl[44] + ropl[46] - ropl[63] - ropl[64] - ropl[65] 
    - ropl[66] - ropl[67] - ropl[68] - ropl[69] - ropl[70] - ropl[78] + 
    ropl[130] - ropl[138] + ropl[148] + ropl[153]); 
  // 14. O2
  wdot[14] = 0.0319988 * (-ropl[0] + ropl[5] - ropl[8] + ropl[9] + ropl[11] + 
    ropl[12] + ropl[13] + ropl[14] - ropl[22] - ropl[26] - ropl[40] - ropl[44] - 
    ropl[45] + ropl[52] - ropl[59] - ropl[60] - ropl[67] - ropl[68] - ropl[82] - 
    ropl[88] - ropl[101] - ropl[106] - ropl[107] - ropl[115] - ropl[126] - 
    ropl[127] - ropl[136] - ropl[142] - ropl[150] + ropl[151] + ropl[152] - 
    ropl[154] - ropl[157]); 
  // 15. HO2
  wdot[15] = 0.03300677 * (ropl[8] - ropl[9] - ropl[10] - ropl[11] - ropl[12] - 
    2.0*ropl[13] - 2.0*ropl[14] + ropl[17] + ropl[18] + ropl[19] + ropl[20] - 
    ropl[23] + ropl[26] - ropl[31] + ropl[40] - ropl[41] - ropl[46] - ropl[52] - 
    ropl[53] + ropl[59] + ropl[60] - ropl[61] + ropl[67] + ropl[68] - ropl[69] + 
    ropl[82] - ropl[83] + ropl[88] + ropl[101] + ropl[103] + ropl[107] - 
    ropl[116] - ropl[135] + ropl[136] - ropl[140] + ropl[142] - ropl[144] + 
    ropl[154] - ropl[173]); 
  // 16. H2O2
  wdot[16] = 0.03401473999999999 * (ropl[13] + ropl[14] - ropl[15] - ropl[16] - 
    ropl[17] - ropl[18] - ropl[19] - ropl[20] + ropl[41] + ropl[53] + ropl[61] + 
    ropl[69] + ropl[83] - ropl[103] + ropl[135] + ropl[144] + ropl[173]); 
  // 17. CO2
  wdot[17] = 0.04400995000000001 * (ropl[21] + ropl[22] + ropl[23] + ropl[24] + 
    ropl[30] + ropl[31] + ropl[70] - ropl[128] + ropl[149] + ropl[162] + 
    ropl[166] + ropl[168] + ropl[170]); 
  // 18. CH3HCO
  wdot[18] = 0.04405358 * (ropl[91] - ropl[129]);
  // 19. HCOOH
  wdot[19] = 0.04602589000000001 * (ropl[163] - ropl[165] - ropl[166] - 
    ropl[167] - ropl[168] - ropl[169] - ropl[170] - ropl[171] - ropl[172] - 
    ropl[173] - ropl[174]); 
  // 20. CH3OCH3
  wdot[20] = 0.04606952 * (-ropl[130] - ropl[131] - ropl[132] - ropl[133] - 
    ropl[134] - ropl[135] - ropl[136] + ropl[138] + ropl[139]); 
  // 21. CH3OCO
  wdot[21] = 0.05904501 * (ropl[142] + ropl[143] + ropl[144] + ropl[145] + 
    ropl[146] + ropl[147] - ropl[148] - ropl[149]); 
  // 22. CH3OCHO
  wdot[22] = 0.06005298000000001 * (ropl[141] - ropl[142] - ropl[143] - 
    ropl[144] - ropl[145] - ropl[146] - ropl[147] + ropl[152] + ropl[154]); 
  // 23. CH3OCH2OH
  wdot[23] = 0.06206892000000001 * (ropl[152]);
  // 24. OCH2OCHO
  wdot[24] = 0.07504441000000001 * (ropl[159] - ropl[160]);
  // 25. HOCH2OCO
  wdot[25] = 0.07504441000000001 * (ropl[160] - ropl[161] - ropl[162]);
  // 26. CH3OCH2O2
  wdot[26] = 0.07706035 * (ropl[150] - 2.0*ropl[151] - 2.0*ropl[152] - 
    ropl[155]); 
  // 27. HO2CH2OCHO
  wdot[27] = 0.09205178000000001 * (ropl[158] - ropl[159]);
  // 28. O2CH2OCH2O2H
  wdot[28] = 0.10905915 * (ropl[157] - ropl[158]);
  // 29. N2
  wdot[29] = 0.0;
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+0*spec_stride) , 
    "d"(wdot[0]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+1*spec_stride) , 
    "d"(wdot[1]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+2*spec_stride) , 
    "d"(wdot[2]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+3*spec_stride) , 
    "d"(wdot[3]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+4*spec_stride) , 
    "d"(wdot[4]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+5*spec_stride) , 
    "d"(wdot[5]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+6*spec_stride) , 
    "d"(wdot[6]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+7*spec_stride) , 
    "d"(wdot[7]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+8*spec_stride) , 
    "d"(wdot[8]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+9*spec_stride) , 
    "d"(wdot[9]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+10*spec_stride) , 
    "d"(wdot[10]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+11*spec_stride) , 
    "d"(wdot[11]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+12*spec_stride) , 
    "d"(wdot[12]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+13*spec_stride) , 
    "d"(wdot[13]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+14*spec_stride) , 
    "d"(wdot[14]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+15*spec_stride) , 
    "d"(wdot[15]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+16*spec_stride) , 
    "d"(wdot[16]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+17*spec_stride) , 
    "d"(wdot[17]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+18*spec_stride) , 
    "d"(wdot[18]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+19*spec_stride) , 
    "d"(wdot[19]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+20*spec_stride) , 
    "d"(wdot[20]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+21*spec_stride) , 
    "d"(wdot[21]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+22*spec_stride) , 
    "d"(wdot[22]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+23*spec_stride) , 
    "d"(wdot[23]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+24*spec_stride) , 
    "d"(wdot[24]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+25*spec_stride) , 
    "d"(wdot[25]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+26*spec_stride) , 
    "d"(wdot[26]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+27*spec_stride) , 
    "d"(wdot[27]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+28*spec_stride) , 
    "d"(wdot[28]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(wdot_array+29*spec_stride) , 
    "d"(wdot[29]) : "memory"); 
}

