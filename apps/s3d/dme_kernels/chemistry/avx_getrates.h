#ifndef __AVX_CPU_GET_RATES__
#define __AVX_CPU_GET_RATES__

void avx_getrates(const double *pressure_array, const double *temperature_array, 
  const double *avmolwt_array, const double *mass_frac_array, const int 
  num_elmts, const int spec_stride, const double *diffusion_array, const double 
  dt, double *wdot_array); 
#endif // __AVX_CPU_GET_RATES__
