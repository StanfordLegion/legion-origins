
/*
 * Kepler tuned conductivity kernel for DME.
 * Gets 12 CTAs with 4 warps per CTA.
 * Should compile to 37 registers.
 * Math throughput limited:
 * 23 DMFAs per 8-byte load
 *
 * Can tune DIMX and DIMY for each CTA
 * Probably want at least 4 warps per CTA
 * since max number of CTAs/SM is 16 on Kepler.
 *
 * Launch with:
 *  dim3 grid(nx/DIMX,ny/DIMY,nz);
 *  dim3 block(DIMX,DIMY,1);
 *
 * gpu_conductivity<<<grid,block>>>(temperature_ptr,  
 *                 mole_frac_input_ptr, nx*ny, nx, nz, nx*ny*nz, output_d);
 *
 * Performance numbers on K20c with 13 SMs:
 * 
 * 32x32x32 (DIMX=32, DIMY=4)
 *   Latency: 0.113 ms
 *   Throughput: 288.756 Mpoints/s
 *   Perf: 517.9 GFLOPS
 *
 * 64x64x64 (DIMX=32, DIMY=4)
 *   Latency: 0.728 ms
 *   Throughput: 359.987 Mpoints/s
 *   Perf: 643.1 GFLOPS
 *
 * 128x128x128 (DIMX=32, DIMY=4)
 *   Latency: 5.644 ms
 *   Throughput: 371.602 Mpoints/s
 *   Perf: 663.6 GFLOPS
 *
 * Generation command:
 *   May need to disable diffusion kernel generation to avoid warnings
 *   ./s3dgen -dir inputs/DME_sk39/ -gpu -p 1 -c 1 -t 32  
 *
 */

#define DIMX 32
#define DIMY 4

__global__ void
__launch_bounds__(DIMX*DIMY,48/(DIMX*DIMY/32))
gpu_conductivity(const double *temperature_array, const double *mole_frac_array, 
  const int slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX 
  in number of doubles*/, const int total_steps/*NZ in number of doubles*/, 
  const int spec_stride/*NX*NY*NZ in number of doubles*/, double *conductivity) 
{
  {
    const int offset = (blockIdx.y*blockDim.y+threadIdx.y)*row_stride + 
      (blockIdx.x*blockDim.x + 
      threadIdx.x)*sizeof(double)/sizeof(double)+blockIdx.z*slice_stride; 
    temperature_array += offset;
    mole_frac_array += offset;
    conductivity += offset;
  }
  double temperature;
  // Load the temperatures
  #if __CUDA_ARCH__ >= 350
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  #else
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  #endif
  // Loads for H
  double mole_frac_NC_H;
  #if __CUDA_ARCH__ >= 350
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_H) : 
    "l"(mole_frac_array+0*spec_stride) : "memory"); 
  #else
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_H) : 
    "l"(mole_frac_array+0*spec_stride) : "memory"); 
  #endif
  // Compute log(t)
  double logt;
  logt = log(temperature);
  double sum;
  double sumr;
  sum = 0.0;
  sumr = 0.0;
  
  // Loads for H2
  double mole_frac_NC_H2;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_H2) : 
      "l"(mole_frac_array+1*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_H2) : 
      "l"(mole_frac_array+1*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for H
  {
    double val = __fma_rn(0.01757076575883899,logt,-0.3980310503714907);
    val = __fma_rn(val,logt,3.652696513182315);
    val = __fma_rn(val,logt,-0.855439325379628);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_H,val,sum);
    sumr += (mole_frac_NC_H/val);
  }
  
  // Loads for CH3
  double mole_frac_NC_CH3;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3) : 
      "l"(mole_frac_array+2*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3) : 
      "l"(mole_frac_array+2*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for H2
  {
    double val = __fma_rn(-2.531812506496628e-03,logt,0.1126756313865689);
    val = __fma_rn(val,logt,-0.4346974875280237);
    val = __fma_rn(val,logt,9.13234704571685);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_H2,val,sum);
    sumr += (mole_frac_NC_H2/val);
  }
  
  // Loads for O
  double mole_frac_NC_O;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_O) : 
      "l"(mole_frac_array+3*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_O) : 
      "l"(mole_frac_array+3*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CH3
  {
    double val = __fma_rn(-0.04516105496053591,logt,0.85747982235779);
    val = __fma_rn(val,logt,-4.317036929586961);
    val = __fma_rn(val,logt,13.29540333966261);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CH3,val,sum);
    sumr += (mole_frac_NC_CH3/val);
  }
  
  // Loads for CH4
  double mole_frac_NC_CH4;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH4) : 
      "l"(mole_frac_array+4*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH4) : 
      "l"(mole_frac_array+4*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for O
  {
    double val = __fma_rn(7.841447484966579e-03,logt,-0.1738651327164223);
    val = __fma_rn(val,logt,1.929020467011312);
    val = __fma_rn(val,logt,1.68503497702711);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_O,val,sum);
    sumr += (mole_frac_NC_O/val);
  }
  
  // Loads for OH
  double mole_frac_NC_OH;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_OH) : 
      "l"(mole_frac_array+5*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_OH) : 
      "l"(mole_frac_array+5*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CH4
  {
    double val = __fma_rn(-0.05698710698012796,logt,1.029040836403109);
    val = __fma_rn(val,logt,-4.856399189122637);
    val = __fma_rn(val,logt,12.95934911704963);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CH4,val,sum);
    sumr += (mole_frac_NC_CH4/val);
  }
  
  // Loads for H2O
  double mole_frac_NC_H2O;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_H2O) : 
      "l"(mole_frac_array+6*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_H2O) : 
      "l"(mole_frac_array+6*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for OH
  {
    double val = __fma_rn(-0.02628247820183795,logt,0.5926154238598758);
    val = __fma_rn(val,logt,-3.630971796795533);
    val = __fma_rn(val,logt,15.00752282630259);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_OH,val,sum);
    sumr += (mole_frac_NC_OH/val);
  }
  
  // Loads for C2H2
  double mole_frac_NC_C2H2;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_C2H2) : 
      "l"(mole_frac_array+7*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_C2H2) : 
      "l"(mole_frac_array+7*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for H2O
  {
    double val = __fma_rn(-0.09540405626785535,logt,1.95608794076996);
    val = __fma_rn(val,logt,-12.04606585246879);
    val = __fma_rn(val,logt,30.61822927962006);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_H2O,val,sum);
    sumr += (mole_frac_NC_H2O/val);
  }
  
  // Loads for CO
  double mole_frac_NC_CO;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CO) : 
      "l"(mole_frac_array+8*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CO) : 
      "l"(mole_frac_array+8*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for C2H2
  {
    double val = __fma_rn(0.01590925343656362,logt,-0.4576174623960179);
    val = __fma_rn(val,logt,5.065459744313331);
    val = __fma_rn(val,logt,-9.069307106082508);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_C2H2,val,sum);
    sumr += (mole_frac_NC_C2H2/val);
  }
  
  // Loads for C2H4
  double mole_frac_NC_C2H4;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_C2H4) : 
      "l"(mole_frac_array+9*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_C2H4) : 
      "l"(mole_frac_array+9*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CO
  {
    double val = __fma_rn(-0.02934674200838898,logt,0.582168039279237);
    val = __fma_rn(val,logt,-3.021054191345565);
    val = __fma_rn(val,logt,11.57880053161914);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CO,val,sum);
    sumr += (mole_frac_NC_CO/val);
  }
  
  // Loads for C2H5
  double mole_frac_NC_C2H5;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_C2H5) : 
      "l"(mole_frac_array+10*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_C2H5) : 
      "l"(mole_frac_array+10*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for C2H4
  {
    double val = __fma_rn(0.01178955170353108,logt,-0.4766243659164148);
    val = __fma_rn(val,logt,6.083701125827112);
    val = __fma_rn(val,logt,-13.42414777442611);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_C2H4,val,sum);
    sumr += (mole_frac_NC_C2H4/val);
  }
  
  // Loads for CH2O
  double mole_frac_NC_CH2O;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH2O) : 
      "l"(mole_frac_array+11*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH2O) : 
      "l"(mole_frac_array+11*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for C2H5
  {
    double val = __fma_rn(1.377143158556522e-03,logt,-0.2468067039334131);
    val = __fma_rn(val,logt,4.413381543169306);
    val = __fma_rn(val,logt,-9.751830830827839);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_C2H5,val,sum);
    sumr += (mole_frac_NC_C2H5/val);
  }
  
  // Loads for C2H6
  double mole_frac_NC_C2H6;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_C2H6) : 
      "l"(mole_frac_array+12*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_C2H6) : 
      "l"(mole_frac_array+12*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CH2O
  {
    double val = __fma_rn(-0.04004666591975085,logt,0.6203934945229457);
    val = __fma_rn(val,logt,-1.5813012183647);
    val = __fma_rn(val,logt,3.562399309905459);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CH2O,val,sum);
    sumr += (mole_frac_NC_CH2O/val);
  }
  
  // Loads for CH3O
  double mole_frac_NC_CH3O;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3O) : 
      "l"(mole_frac_array+13*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3O) : 
      "l"(mole_frac_array+13*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for C2H6
  {
    double val = __fma_rn(0.01122438780571428,logt,-0.4671891488760542);
    val = __fma_rn(val,logt,6.085570573784379);
    val = __fma_rn(val,logt,-13.92569539920283);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_C2H6,val,sum);
    sumr += (mole_frac_NC_C2H6/val);
  }
  
  // Loads for O2
  double mole_frac_NC_O2;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_O2) : 
      "l"(mole_frac_array+14*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_O2) : 
      "l"(mole_frac_array+14*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CH3O
  {
    double val = __fma_rn(-0.02700966360245718,logt,0.3302503247033797);
    val = __fma_rn(val,logt,0.5901734642841732);
    val = __fma_rn(val,logt,-1.689589350425731);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CH3O,val,sum);
    sumr += (mole_frac_NC_CH3O/val);
  }
  
  // Loads for HO2
  double mole_frac_NC_HO2;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_HO2) : 
      "l"(mole_frac_array+15*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_HO2) : 
      "l"(mole_frac_array+15*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for O2
  {
    double val = __fma_rn(0.0115252152240115,logt,-0.2711197315686871);
    val = __fma_rn(val,logt,2.891810356017463);
    val = __fma_rn(val,logt,-1.938378170116771);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_O2,val,sum);
    sumr += (mole_frac_NC_O2/val);
  }
  
  // Loads for H2O2
  double mole_frac_NC_H2O2;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_H2O2) : 
      "l"(mole_frac_array+16*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_H2O2) : 
      "l"(mole_frac_array+16*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for HO2
  {
    double val = __fma_rn(5.799969718755228e-03,logt,-0.1632053830988643);
    val = __fma_rn(val,logt,2.340065203947785);
    val = __fma_rn(val,logt,-1.130093365014655);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_HO2,val,sum);
    sumr += (mole_frac_NC_HO2/val);
  }
  
  // Loads for CO2
  double mole_frac_NC_CO2;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CO2) : 
      "l"(mole_frac_array+17*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CO2) : 
      "l"(mole_frac_array+17*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for H2O2
  {
    double val = __fma_rn(-4.467536916305526e-03,logt,0.01818530793145573);
    val = __fma_rn(val,logt,1.325299407573309);
    val = __fma_rn(val,logt,0.898128844523025);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_H2O2,val,sum);
    sumr += (mole_frac_NC_H2O2/val);
  }
  
  // Loads for CH3HCO
  double mole_frac_NC_CH3HCO;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3HCO) : 
      "l"(mole_frac_array+18*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3HCO) : 
      "l"(mole_frac_array+18*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CO2
  {
    double val = __fma_rn(0.02109965793388982,logt,-0.582640562453916);
    val = __fma_rn(val,logt,5.968339795463443);
    val = __fma_rn(val,logt,-11.54098453117943);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CO2,val,sum);
    sumr += (mole_frac_NC_CO2/val);
  }
  
  // Loads for HCOOH
  double mole_frac_NC_HCOOH;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_HCOOH) : 
      "l"(mole_frac_array+19*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_HCOOH) : 
      "l"(mole_frac_array+19*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CH3HCO
  {
    double val = __fma_rn(-3.280842418595923e-03,logt,-0.174962798081539);
    val = __fma_rn(val,logt,4.139056863623322);
    val = __fma_rn(val,logt,-9.884150350795055);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CH3HCO,val,sum);
    sumr += (mole_frac_NC_CH3HCO/val);
  }
  
  // Loads for CH3OCH3
  double mole_frac_NC_CH3OCH3;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3OCH3) : 
      "l"(mole_frac_array+20*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3OCH3) : 
      "l"(mole_frac_array+20*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for HCOOH
  {
    double val = __fma_rn(8.355378131029852e-03,logt,-0.4144369280866382);
    val = __fma_rn(val,logt,5.702536484362833);
    val = __fma_rn(val,logt,-13.64058709322757);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_HCOOH,val,sum);
    sumr += (mole_frac_NC_HCOOH/val);
  }
  
  // Loads for CH3OCO
  double mole_frac_NC_CH3OCO;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3OCO) : 
      "l"(mole_frac_array+21*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3OCO) : 
      "l"(mole_frac_array+21*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CH3OCH3
  {
    double val = __fma_rn(-8.016865668546173e-03,logt,-0.07676194103215409);
    val = __fma_rn(val,logt,3.455143630954375);
    val = __fma_rn(val,logt,-8.307841747009206);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CH3OCH3,val,sum);
    sumr += (mole_frac_NC_CH3OCH3/val);
  }
  
  // Loads for CH3OCHO
  double mole_frac_NC_CH3OCHO;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3OCHO) : 
      "l"(mole_frac_array+22*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3OCHO) : 
      "l"(mole_frac_array+22*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CH3OCO
  {
    double val = __fma_rn(0.02806490464177051,logt,-0.8013681701684446);
    val = __fma_rn(val,logt,8.090042284050424);
    val = __fma_rn(val,logt,-17.92570219976034);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CH3OCO,val,sum);
    sumr += (mole_frac_NC_CH3OCO/val);
  }
  
  // Loads for CH3OCH2OH
  double mole_frac_NC_CH3OCH2OH;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3OCH2OH) 
      : "l"(mole_frac_array+23*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3OCH2OH) : 
      "l"(mole_frac_array+23*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CH3OCHO
  {
    double val = __fma_rn(-6.85619957188065e-03,logt,-0.1011227818498091);
    val = __fma_rn(val,logt,3.577780313568882);
    val = __fma_rn(val,logt,-8.639124901940727);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CH3OCHO,val,sum);
    sumr += (mole_frac_NC_CH3OCHO/val);
  }
  
  // Loads for OCH2OCHO
  double mole_frac_NC_OCH2OCHO;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_OCH2OCHO) : 
      "l"(mole_frac_array+24*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_OCH2OCHO) : 
      "l"(mole_frac_array+24*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CH3OCH2OH
  {
    double val = __fma_rn(-4.811640316628474e-03,logt,-0.1246285105106686);
    val = __fma_rn(val,logt,3.61405693688714);
    val = __fma_rn(val,logt,-8.201968404580237);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CH3OCH2OH,val,sum);
    sumr += (mole_frac_NC_CH3OCH2OH/val);
  }
  
  // Loads for HOCH2OCO
  double mole_frac_NC_HOCH2OCO;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_HOCH2OCO) : 
      "l"(mole_frac_array+25*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_HOCH2OCO) : 
      "l"(mole_frac_array+25*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for OCH2OCHO
  {
    double val = __fma_rn(1.809547240084334e-04,logt,-0.2208904202224758);
    val = __fma_rn(val,logt,4.130184222890766);
    val = __fma_rn(val,logt,-9.028889222172253);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_OCH2OCHO,val,sum);
    sumr += (mole_frac_NC_OCH2OCHO/val);
  }
  
  // Loads for CH3OCH2O2
  double mole_frac_NC_CH3OCH2O2;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3OCH2O2) 
      : "l"(mole_frac_array+26*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_CH3OCH2O2) : 
      "l"(mole_frac_array+26*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for HOCH2OCO
  {
    double val = __fma_rn(-0.01010838735996976,logt,0.0113110750868739);
    val = __fma_rn(val,logt,2.392748262187772);
    val = __fma_rn(val,logt,-4.747760530379912);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_HOCH2OCO,val,sum);
    sumr += (mole_frac_NC_HOCH2OCO/val);
  }
  
  // Loads for HO2CH2OCHO
  double mole_frac_NC_HO2CH2OCHO;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_HO2CH2OCHO) 
      : "l"(mole_frac_array+27*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_HO2CH2OCHO) : 
      "l"(mole_frac_array+27*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for CH3OCH2O2
  {
    double val = __fma_rn(0.01859842769304785,logt,-0.6002254714412125);
    val = __fma_rn(val,logt,6.751988586949599);
    val = __fma_rn(val,logt,-14.9470545599887);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_CH3OCH2O2,val,sum);
    sumr += (mole_frac_NC_CH3OCH2O2/val);
  }
  
  // Loads for O2CH2OCH2O2H
  double mole_frac_NC_O2CH2OCH2O2H;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : 
      "=d"(mole_frac_NC_O2CH2OCH2O2H) : "l"(mole_frac_array+28*spec_stride) : 
      "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_O2CH2OCH2O2H) 
      : "l"(mole_frac_array+28*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for HO2CH2OCHO
  {
    double val = __fma_rn(0.03119495987148689,logt,-0.8585687031446342);
    val = __fma_rn(val,logt,8.432843826770814);
    val = __fma_rn(val,logt,-18.39997336105232);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_HO2CH2OCHO,val,sum);
    sumr += (mole_frac_NC_HO2CH2OCHO/val);
  }
  
  // Loads for N2
  double mole_frac_NC_N2;
  {
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_N2) : 
      "l"(mole_frac_array+29*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_NC_N2) : 
      "l"(mole_frac_array+29*spec_stride) : "memory"); 
    #endif
  }
  
  // Math for O2CH2OCH2O2H
  {
    double val = __fma_rn(0.04418819272298635,logt,-1.126822251130392);
    val = __fma_rn(val,logt,10.27211025085665);
    val = __fma_rn(val,logt,-22.49644487312027);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_O2CH2OCH2O2H,val,sum);
    sumr += (mole_frac_NC_O2CH2OCH2O2H/val);
  }
  
  // Math for N2
  {
    double val = __fma_rn(-0.03194429885386962,logt,0.6455863299907513);
    val = __fma_rn(val,logt,-3.528398528983377);
    val = __fma_rn(val,logt,12.93009748943811);
    val = exp(val);
    sum = __fma_rn(mole_frac_NC_N2,val,sum);
    sumr += (mole_frac_NC_N2/val);
  }
  
  // Write out the coefficients
  {
    asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(conductivity), 
      "d"(0.5*(sum + 1.0/sumr)) : "memory"); 
  }
  
}


