
/*
 * Fermi tuned conductivity kernel for DME.
 * Gets 4 CTAs with 8 warps per CTA.
 * Should compile to 32 registers.
 * Math throughput limited:
 * 23 DMFAs per 8-byte load
 *
 * Can tune number of threads for each CTA
 * Probably want at least 8 warps per CTA
 * since max number of CTAs/SM is 8 on Fermi.
 *
 * Launch with:
 *  dim3 grid((nx*ny*nz)/256,1,1);
 *  dim3 block(256,1,1);
 *
 * gpu_conductivity<<<grid,block>>>(temperature_ptr,  
 *                 mole_frac_input_ptr, nx*ny, nx, nz, nx*ny*nz, output_d);
 *
 * Performance numbers on C2070 with 14 SMs:
 * 
 * 32x32x32 (threads=256)
 *   Latency: 0.281 ms
 *   Throughput: 116.617 Mpoints/s
 *   Perf: 208.3 GFLOPS
 *
 * 64x64x64 (threads=256)
 *   Latency: 2.002 ms
 *   Throughput: 130.924 Mpoints/s
 *   Perf: 233.9 GFLOPS
 *
 * 128x128x128 (threads=256)
 *   Latency: 15.773 ms
 *   Throughput: 132.959 Mpoints/
 *   Perf: 237.5 GFLOPS
 *
 * Generation command:
 *   ./singe --dir inputs/DME/ --cuda
 *
 */

__constant__ double recip_molecular_masses[30] = {0.9920930186414277, 
  0.4960465093207139, 0.06651120780362699, 0.06250234383789392, 
  0.06233236489615739, 0.05879803873262004, 0.05550825019122593, 
  0.0384050534905585, 0.03570083414998991, 0.03564531203549703, 
  0.0344090165386938, 0.03330392596670473, 0.03325560390181349, 
  0.03222224585186918, 0.03125117191894696, 0.03029681486555637, 
  0.02939901936631002, 0.02272213442641948, 0.02269963076780593, 
  0.02172690196756651, 0.02170632557057247, 0.01693623220658274, 
  0.01665196298335236, 0.01611112292593459, 0.01332544289441412, 
  0.01332544289441412, 0.01297684217629429, 0.01086345098378326, 
  9.169336089635763e-03, 0.03569720205330306}; 

__global__ void
__launch_bounds__(256,4)
gpu_conductivity(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *conductivity) 
{
  {
    const int offset = (blockIdx.x*blockDim.x+threadIdx.x);
    temperature_array += offset;
    mass_frac_array += offset;
    avmolwt_array += offset;
    conductivity += offset;
  }
  // Load the temperatures
  double temperature;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  // Load the average mole weight
  double avmolwt;
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(avmolwt) : "l"(avmolwt_array) 
    : "memory"); 
  temperature *= 120.0;
  // Compute log(t)
  double logt = log(temperature);
  double sum = 0.0;
  double sumr = 0.0;
  // Species H
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+0*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[0] * 1e3 * avmolwt;
    double val = __fma_rn(0.01757076575883899,logt,-0.3980310503714907);
    val = __fma_rn(val,logt,3.652696513182315);
    val = __fma_rn(val,logt,-0.855439325379628);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+1*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[1] * 1e3 * avmolwt;
    double val = __fma_rn(-2.531812506496628e-03,logt,0.1126756313865689);
    val = __fma_rn(val,logt,-0.4346974875280237);
    val = __fma_rn(val,logt,9.13234704571685);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+2*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[2] * 1e3 * avmolwt;
    double val = __fma_rn(-0.04516105496053591,logt,0.85747982235779);
    val = __fma_rn(val,logt,-4.317036929586961);
    val = __fma_rn(val,logt,13.29540333966261);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species O
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+3*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[3] * 1e3 * avmolwt;
    double val = __fma_rn(7.841447484966579e-03,logt,-0.1738651327164223);
    val = __fma_rn(val,logt,1.929020467011312);
    val = __fma_rn(val,logt,1.68503497702711);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH4
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+4*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[4] * 1e3 * avmolwt;
    double val = __fma_rn(-0.05698710698012796,logt,1.029040836403109);
    val = __fma_rn(val,logt,-4.856399189122637);
    val = __fma_rn(val,logt,12.95934911704963);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species OH
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+5*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[5] * 1e3 * avmolwt;
    double val = __fma_rn(-0.02628247820183795,logt,0.5926154238598758);
    val = __fma_rn(val,logt,-3.630971796795533);
    val = __fma_rn(val,logt,15.00752282630259);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2O
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+6*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[6] * 1e3 * avmolwt;
    double val = __fma_rn(-0.09540405626785535,logt,1.95608794076996);
    val = __fma_rn(val,logt,-12.04606585246879);
    val = __fma_rn(val,logt,30.61822927962006);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+7*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[7] * 1e3 * avmolwt;
    double val = __fma_rn(0.01590925343656362,logt,-0.4576174623960179);
    val = __fma_rn(val,logt,5.065459744313331);
    val = __fma_rn(val,logt,-9.069307106082508);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+8*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[8] * 1e3 * avmolwt;
    double val = __fma_rn(-0.02934674200838898,logt,0.582168039279237);
    val = __fma_rn(val,logt,-3.021054191345565);
    val = __fma_rn(val,logt,11.57880053161914);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H4
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+9*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[9] * 1e3 * avmolwt;
    double val = __fma_rn(0.01178955170353108,logt,-0.4766243659164148);
    val = __fma_rn(val,logt,6.083701125827112);
    val = __fma_rn(val,logt,-13.42414777442611);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H5
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+10*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[10] * 1e3 * avmolwt;
    double val = __fma_rn(1.377143158556522e-03,logt,-0.2468067039334131);
    val = __fma_rn(val,logt,4.413381543169306);
    val = __fma_rn(val,logt,-9.751830830827839);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH2O
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+11*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[11] * 1e3 * avmolwt;
    double val = __fma_rn(-0.04004666591975085,logt,0.6203934945229457);
    val = __fma_rn(val,logt,-1.5813012183647);
    val = __fma_rn(val,logt,3.562399309905459);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species C2H6
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+12*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[12] * 1e3 * avmolwt;
    double val = __fma_rn(0.01122438780571428,logt,-0.4671891488760542);
    val = __fma_rn(val,logt,6.085570573784379);
    val = __fma_rn(val,logt,-13.92569539920283);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3O
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+13*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[13] * 1e3 * avmolwt;
    double val = __fma_rn(-0.02700966360245718,logt,0.3302503247033797);
    val = __fma_rn(val,logt,0.5901734642841732);
    val = __fma_rn(val,logt,-1.689589350425731);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species O2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+14*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[14] * 1e3 * avmolwt;
    double val = __fma_rn(0.0115252152240115,logt,-0.2711197315686871);
    val = __fma_rn(val,logt,2.891810356017463);
    val = __fma_rn(val,logt,-1.938378170116771);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HO2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+15*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[15] * 1e3 * avmolwt;
    double val = __fma_rn(5.799969718755228e-03,logt,-0.1632053830988643);
    val = __fma_rn(val,logt,2.340065203947785);
    val = __fma_rn(val,logt,-1.130093365014655);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species H2O2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+16*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[16] * 1e3 * avmolwt;
    double val = __fma_rn(-4.467536916305526e-03,logt,0.01818530793145573);
    val = __fma_rn(val,logt,1.325299407573309);
    val = __fma_rn(val,logt,0.898128844523025);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CO2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+17*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[17] * 1e3 * avmolwt;
    double val = __fma_rn(0.02109965793388982,logt,-0.582640562453916);
    val = __fma_rn(val,logt,5.968339795463443);
    val = __fma_rn(val,logt,-11.54098453117943);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3HCO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+18*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[18] * 1e3 * avmolwt;
    double val = __fma_rn(-3.280842418595923e-03,logt,-0.174962798081539);
    val = __fma_rn(val,logt,4.139056863623322);
    val = __fma_rn(val,logt,-9.884150350795055);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HCOOH
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+19*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[19] * 1e3 * avmolwt;
    double val = __fma_rn(8.355378131029852e-03,logt,-0.4144369280866382);
    val = __fma_rn(val,logt,5.702536484362833);
    val = __fma_rn(val,logt,-13.64058709322757);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3OCH3
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+20*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[20] * 1e3 * avmolwt;
    double val = __fma_rn(-8.016865668546173e-03,logt,-0.07676194103215409);
    val = __fma_rn(val,logt,3.455143630954375);
    val = __fma_rn(val,logt,-8.307841747009206);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3OCO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+21*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[21] * 1e3 * avmolwt;
    double val = __fma_rn(0.02806490464177051,logt,-0.8013681701684446);
    val = __fma_rn(val,logt,8.090042284050424);
    val = __fma_rn(val,logt,-17.92570219976034);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3OCHO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+22*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[22] * 1e3 * avmolwt;
    double val = __fma_rn(-6.85619957188065e-03,logt,-0.1011227818498091);
    val = __fma_rn(val,logt,3.577780313568882);
    val = __fma_rn(val,logt,-8.639124901940727);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3OCH2OH
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+23*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[23] * 1e3 * avmolwt;
    double val = __fma_rn(-4.811640316628474e-03,logt,-0.1246285105106686);
    val = __fma_rn(val,logt,3.61405693688714);
    val = __fma_rn(val,logt,-8.201968404580237);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species OCH2OCHO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+24*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[24] * 1e3 * avmolwt;
    double val = __fma_rn(1.809547240084334e-04,logt,-0.2208904202224758);
    val = __fma_rn(val,logt,4.130184222890766);
    val = __fma_rn(val,logt,-9.028889222172253);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HOCH2OCO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+25*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[25] * 1e3 * avmolwt;
    double val = __fma_rn(-0.01010838735996976,logt,0.0113110750868739);
    val = __fma_rn(val,logt,2.392748262187772);
    val = __fma_rn(val,logt,-4.747760530379912);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species CH3OCH2O2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+26*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[26] * 1e3 * avmolwt;
    double val = __fma_rn(0.01859842769304785,logt,-0.6002254714412125);
    val = __fma_rn(val,logt,6.751988586949599);
    val = __fma_rn(val,logt,-14.9470545599887);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species HO2CH2OCHO
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+27*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[27] * 1e3 * avmolwt;
    double val = __fma_rn(0.03119495987148689,logt,-0.8585687031446342);
    val = __fma_rn(val,logt,8.432843826770814);
    val = __fma_rn(val,logt,-18.39997336105232);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species O2CH2OCH2O2H
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+28*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[28] * 1e3 * avmolwt;
    double val = __fma_rn(0.04418819272298635,logt,-1.126822251130392);
    val = __fma_rn(val,logt,10.27211025085665);
    val = __fma_rn(val,logt,-22.49644487312027);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Species N2
  {
    double mass_frac;
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+29*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[29] * 1e3 * avmolwt;
    double val = __fma_rn(-0.03194429885386962,logt,0.6455863299907513);
    val = __fma_rn(val,logt,-3.528398528983377);
    val = __fma_rn(val,logt,12.93009748943811);
    val = exp(val);
    sum = __fma_rn(mole_frac,val,sum);
    sumr = __fma_rn(mole_frac,1.0/val,sumr);
  }
  // Write out the coefficients
  double result = 0.5 * (sum + (1.0/sumr));
  result *= 5.403325855130351e-09;
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(conductivity), "d"(result) : 
    "memory"); 
}


