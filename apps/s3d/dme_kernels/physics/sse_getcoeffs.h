
#ifndef __SSE_CPU_GET_COEFFS__
#define __SSE_CPU_GET_COEFFS__

#ifndef __SINGE_MOLE_MASSES__
#define __SINGE_MOLE_MASSES__
const double molecular_masses[30] = {1.00797, 2.01594, 15.03506, 15.9994, 
  16.04303, 17.00737, 18.01534, 26.03824, 28.01055, 28.05418, 29.06215, 
  30.02649, 30.07012, 31.03446, 31.9988, 33.00677, 34.01474, 44.00995, 44.05358, 
  46.02589, 46.06952, 59.04501, 60.05298000000001, 62.06892000000001, 75.04441, 
  75.04441, 77.06035, 92.05178000000001, 109.05915, 28.0134}; 
#endif


#ifndef __SINGE_RECIP_MOLE_MASSES__
#define __SINGE_RECIP_MOLE_MASSES__
const double recip_molecular_masses[30] = {0.9920930186414277, 
  0.4960465093207139, 0.06651120780362699, 0.06250234383789392, 
  0.06233236489615739, 0.05879803873262004, 0.05550825019122593, 
  0.0384050534905585, 0.03570083414998991, 0.03564531203549703, 
  0.0344090165386938, 0.03330392596670473, 0.03325560390181349, 
  0.03222224585186918, 0.03125117191894696, 0.03029681486555637, 
  0.02939901936631002, 0.02272213442641948, 0.02269963076780593, 
  0.02172690196756651, 0.02170632557057247, 0.01693623220658274, 
  0.01665196298335236, 0.01611112292593459, 0.01332544289441412, 
  0.01332544289441412, 0.01297684217629429, 0.01086345098378326, 
  9.169336089635763e-03, 0.03569720205330306}; 
#endif


void sse_conductivity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *lambda); 
void sse_viscosity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *viscosity); 
void sse_diffusion(const double *temperature_array, const double 
  *pressure_array, const double *mass_frac_array, const double *mixmw_array, 
  const int num_elmts, const int spec_stride, double *diffusion); 
void sse_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *mixmw_array, const int num_elmts, const int spec_stride, double 
  *thermal); 

#endif // __SSE_CPU_GET_COEFFS__

