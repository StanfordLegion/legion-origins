
#include "avx_getcoeffs.h"
#include <cmath>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <emmintrin.h>
#include <smmintrin.h>
#include <immintrin.h>
#include <malloc.h>

template<unsigned BOUNDARY>
static inline bool aligned(const void *ptr)
{
  return ((((unsigned long)ptr) & (BOUNDARY-1)) == 0);
}

void avx_conductivity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *lambda) 
{
  __m256d *sum = (__m256d*)memalign(32,1024*sizeof(__m256d));
  __m256d *sumr = (__m256d*)memalign(32,1024*sizeof(__m256d));
  __m256d *logt = (__m256d*)memalign(32,1024*sizeof(__m256d));
  __m256d *mixmw = (__m256d*)memalign(32,1024*sizeof(__m256d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 4096)
  {
    if (aligned<32>(temperature_array) && aligned<32>(mixmw_array))
    {
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        logt[idx] = _mm256_load_pd(temperature_array);
        temperature_array += 4;
        mixmw[idx] = _mm256_load_pd(mixmw_array);
        mixmw_array += 4;
        sum[idx] = _mm256_set1_pd(0.0);
        sumr[idx] = _mm256_set1_pd(0.0);
        logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(120.0));
        __m128d lower = _mm256_extractf128_pd(logt[idx],0);
        __m128d upper = _mm256_extractf128_pd(logt[idx],1);
        logt[idx] = 
          _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      }
    }
    else
    {
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        logt[idx] = _mm256_loadu_pd(temperature_array);
        temperature_array += 4;
        mixmw[idx] = _mm256_loadu_pd(mixmw_array);
        mixmw_array += 4;
        sum[idx] = _mm256_set1_pd(0.0);
        sumr[idx] = _mm256_set1_pd(0.0);
        logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(120.0));
        __m128d lower = _mm256_extractf128_pd(logt[idx],0);
        __m128d upper = _mm256_extractf128_pd(logt[idx],1);
        logt[idx] = 
          _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      }
    }
    if (aligned<32>(mass_frac_array))
    {
      // Species H
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.01757076575883899);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.3980310503714907)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.652696513182315)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.855439325379628)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-2.531812506496628e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.1126756313865689)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4346974875280237)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(9.13234704571685)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.04516105496053591);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.85747982235779)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.317036929586961)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(13.29540333966261)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(7.841447484966579e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1738651327164223)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.929020467011312)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.68503497702711)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH4
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.05698710698012796);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.029040836403109)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.856399189122637)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(12.95934911704963)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species OH
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02628247820183795);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5926154238598758)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.630971796795533)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(15.00752282630259)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2O
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.09540405626785535);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.95608794076996)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-12.04606585246879)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(30.61822927962006)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.01590925343656362);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4576174623960179)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.065459744313331)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.069307106082508)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02934674200838898);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.582168039279237)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.021054191345565)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(11.57880053161914)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H4
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.01178955170353108);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4766243659164148)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.083701125827112)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.42414777442611)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(9*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[9])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H5
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(1.377143158556522e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2468067039334131)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.413381543169306)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.751830830827839)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[10])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH2O
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.04004666591975085);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.6203934945229457)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.5813012183647)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.562399309905459)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[11])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H6
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.01122438780571428);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4671891488760542)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.085570573784379)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.92569539920283)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[12])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3O
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02700966360245718);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.3302503247033797)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5901734642841732)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.689589350425731)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[13])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.0115252152240115);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2711197315686871)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.891810356017463)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.938378170116771)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[14])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HO2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(5.799969718755228e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1632053830988643)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.340065203947785)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.130093365014655)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[15])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2O2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-4.467536916305526e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.01818530793145573)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.325299407573309)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.898128844523025)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[16])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CO2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.02109965793388982);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.582640562453916)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.968339795463443)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-11.54098453117943)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[17])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3HCO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-3.280842418595923e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.174962798081539)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.139056863623322)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.884150350795055)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[18])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HCOOH
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(8.355378131029852e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4144369280866382)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.702536484362833)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.64058709322757)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[19])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH3
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-8.016865668546173e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.07676194103215409)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.455143630954375)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.307841747009206)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[20])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.02806490464177051);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.8013681701684446)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(8.090042284050424)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-17.92570219976034)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[21])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCHO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-6.85619957188065e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1011227818498091)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.577780313568882)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.639124901940727)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[22])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH2OH
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-4.811640316628474e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1246285105106686)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.61405693688714)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.201968404580237)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[23])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species OCH2OCHO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(1.809547240084334e-04);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2208904202224758)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.130184222890766)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.028889222172253)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[24])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HOCH2OCO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.01010838735996976);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.0113110750868739)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.392748262187772)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.747760530379912)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[25])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH2O2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.01859842769304785);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.6002254714412125)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.751988586949599)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-14.9470545599887)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[26])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HO2CH2OCHO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.03119495987148689);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.8585687031446342)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(8.432843826770814)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-18.39997336105232)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[27])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O2CH2OCH2O2H
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.04418819272298635);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.126822251130392)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(10.27211025085665)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-22.49644487312027)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[28])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species N2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.03194429885386962);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.6455863299907513)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.528398528983377)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(12.93009748943811)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[29])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
    }
    else
    {
      // Species H
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.01757076575883899);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.3980310503714907)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.652696513182315)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.855439325379628)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-2.531812506496628e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.1126756313865689)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4346974875280237)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(9.13234704571685)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.04516105496053591);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.85747982235779)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.317036929586961)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(13.29540333966261)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(7.841447484966579e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1738651327164223)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.929020467011312)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.68503497702711)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH4
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.05698710698012796);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.029040836403109)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.856399189122637)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(12.95934911704963)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species OH
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02628247820183795);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5926154238598758)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.630971796795533)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(15.00752282630259)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2O
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.09540405626785535);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.95608794076996)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-12.04606585246879)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(30.61822927962006)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.01590925343656362);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4576174623960179)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.065459744313331)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.069307106082508)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02934674200838898);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.582168039279237)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.021054191345565)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(11.57880053161914)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H4
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.01178955170353108);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4766243659164148)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.083701125827112)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.42414777442611)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[9])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H5
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(1.377143158556522e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2468067039334131)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.413381543169306)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.751830830827839)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[10])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH2O
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.04004666591975085);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.6203934945229457)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.5813012183647)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.562399309905459)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[11])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H6
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.01122438780571428);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4671891488760542)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.085570573784379)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.92569539920283)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[12])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3O
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02700966360245718);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.3302503247033797)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5901734642841732)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.689589350425731)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[13])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.0115252152240115);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2711197315686871)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.891810356017463)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.938378170116771)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[14])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HO2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(5.799969718755228e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1632053830988643)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.340065203947785)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.130093365014655)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[15])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2O2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-4.467536916305526e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.01818530793145573)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.325299407573309)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.898128844523025)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[16])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CO2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.02109965793388982);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.582640562453916)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.968339795463443)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-11.54098453117943)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[17])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3HCO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-3.280842418595923e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.174962798081539)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.139056863623322)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.884150350795055)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[18])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HCOOH
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(8.355378131029852e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4144369280866382)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.702536484362833)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.64058709322757)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[19])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH3
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-8.016865668546173e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.07676194103215409)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.455143630954375)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.307841747009206)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[20])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.02806490464177051);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.8013681701684446)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(8.090042284050424)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-17.92570219976034)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[21])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCHO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-6.85619957188065e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1011227818498091)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.577780313568882)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.639124901940727)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[22])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH2OH
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-4.811640316628474e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1246285105106686)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.61405693688714)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.201968404580237)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[23])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species OCH2OCHO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(1.809547240084334e-04);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2208904202224758)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.130184222890766)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.028889222172253)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[24])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HOCH2OCO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.01010838735996976);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.0113110750868739)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.392748262187772)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.747760530379912)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[25])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH2O2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.01859842769304785);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.6002254714412125)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.751988586949599)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-14.9470545599887)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[26])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HO2CH2OCHO
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.03119495987148689);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.8585687031446342)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(8.432843826770814)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-18.39997336105232)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[27])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O2CH2OCH2O2H
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(0.04418819272298635);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.126822251130392)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(10.27211025085665)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-22.49644487312027)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[28])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species N2
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d val = _mm256_set1_pd(-0.03194429885386962);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.6455863299907513)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.528398528983377)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(12.93009748943811)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[29])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
    }
    if (aligned<32>(lambda))
    {
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d result = 
          _mm256_add_pd(sum[idx],_mm256_div_pd(_mm256_set1_pd(1.0),sumr[idx])); 
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.5));
        result = _mm256_mul_pd(result,_mm256_set1_pd(5.403325855130351e-09));
        _mm256_stream_pd(lambda,result);
        lambda += 4;
      }
    }
    else
    {
      for (unsigned idx = 0; idx < 1024; idx++)
      {
        __m256d result = 
          _mm256_add_pd(sum[idx],_mm256_div_pd(_mm256_set1_pd(1.0),sumr[idx])); 
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.5));
        result = _mm256_mul_pd(result,_mm256_set1_pd(5.403325855130351e-09));
        _mm256_storeu_pd(lambda,result);
        lambda += 4;
      }
    }
    remaining_elmts -= 4096;
    mass_frac_array += 4096;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 4) == 0);
    if (aligned<32>(temperature_array) && aligned<32>(mixmw_array))
    {
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        logt[idx] = _mm256_load_pd(temperature_array);
        temperature_array += 4;
        mixmw[idx] = _mm256_load_pd(mixmw_array);
        mixmw_array += 4;
        sum[idx] = _mm256_set1_pd(0.0);
        sumr[idx] = _mm256_set1_pd(0.0);
        logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(120.0));
        __m128d lower = _mm256_extractf128_pd(logt[idx],0);
        __m128d upper = _mm256_extractf128_pd(logt[idx],1);
        logt[idx] = 
          _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      }
    }
    else
    {
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        logt[idx] = _mm256_loadu_pd(temperature_array);
        temperature_array += 4;
        mixmw[idx] = _mm256_loadu_pd(mixmw_array);
        mixmw_array += 4;
        sum[idx] = _mm256_set1_pd(0.0);
        sumr[idx] = _mm256_set1_pd(0.0);
        logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(120.0));
        __m128d lower = _mm256_extractf128_pd(logt[idx],0);
        __m128d upper = _mm256_extractf128_pd(logt[idx],1);
        logt[idx] = 
          _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      }
    }
    if (aligned<32>(mass_frac_array))
    {
      // Species H
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.01757076575883899);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.3980310503714907)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.652696513182315)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.855439325379628)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-2.531812506496628e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.1126756313865689)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4346974875280237)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(9.13234704571685)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.04516105496053591);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.85747982235779)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.317036929586961)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(13.29540333966261)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(7.841447484966579e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1738651327164223)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.929020467011312)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.68503497702711)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH4
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.05698710698012796);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.029040836403109)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.856399189122637)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(12.95934911704963)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species OH
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02628247820183795);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5926154238598758)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.630971796795533)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(15.00752282630259)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2O
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.09540405626785535);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.95608794076996)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-12.04606585246879)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(30.61822927962006)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.01590925343656362);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4576174623960179)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.065459744313331)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.069307106082508)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02934674200838898);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.582168039279237)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.021054191345565)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(11.57880053161914)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H4
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.01178955170353108);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4766243659164148)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.083701125827112)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.42414777442611)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = _mm256_load_pd(mass_frac_array+(9*spec_stride)+(idx<<2));
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[9])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H5
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(1.377143158556522e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2468067039334131)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.413381543169306)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.751830830827839)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[10])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH2O
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.04004666591975085);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.6203934945229457)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.5813012183647)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.562399309905459)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[11])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H6
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.01122438780571428);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4671891488760542)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.085570573784379)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.92569539920283)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[12])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3O
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02700966360245718);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.3302503247033797)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5901734642841732)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.689589350425731)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[13])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.0115252152240115);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2711197315686871)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.891810356017463)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.938378170116771)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[14])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HO2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(5.799969718755228e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1632053830988643)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.340065203947785)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.130093365014655)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[15])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2O2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-4.467536916305526e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.01818530793145573)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.325299407573309)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.898128844523025)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[16])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CO2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.02109965793388982);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.582640562453916)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.968339795463443)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-11.54098453117943)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[17])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3HCO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-3.280842418595923e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.174962798081539)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.139056863623322)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.884150350795055)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[18])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HCOOH
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(8.355378131029852e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4144369280866382)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.702536484362833)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.64058709322757)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[19])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH3
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-8.016865668546173e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.07676194103215409)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.455143630954375)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.307841747009206)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[20])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.02806490464177051);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.8013681701684446)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(8.090042284050424)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-17.92570219976034)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[21])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCHO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-6.85619957188065e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1011227818498091)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.577780313568882)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.639124901940727)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[22])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH2OH
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-4.811640316628474e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1246285105106686)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.61405693688714)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.201968404580237)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[23])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species OCH2OCHO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(1.809547240084334e-04);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2208904202224758)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.130184222890766)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.028889222172253)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[24])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HOCH2OCO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.01010838735996976);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.0113110750868739)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.392748262187772)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.747760530379912)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[25])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH2O2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.01859842769304785);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.6002254714412125)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.751988586949599)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-14.9470545599887)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[26])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HO2CH2OCHO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.03119495987148689);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.8585687031446342)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(8.432843826770814)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-18.39997336105232)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[27])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O2CH2OCH2O2H
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.04418819272298635);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.126822251130392)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(10.27211025085665)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-22.49644487312027)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[28])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species N2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.03194429885386962);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.6455863299907513)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.528398528983377)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(12.93009748943811)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_load_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[29])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
    }
    else
    {
      // Species H
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.01757076575883899);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.3980310503714907)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.652696513182315)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.855439325379628)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-2.531812506496628e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.1126756313865689)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4346974875280237)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(9.13234704571685)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.04516105496053591);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.85747982235779)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.317036929586961)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(13.29540333966261)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(7.841447484966579e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1738651327164223)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.929020467011312)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.68503497702711)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH4
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.05698710698012796);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.029040836403109)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.856399189122637)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(12.95934911704963)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species OH
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02628247820183795);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5926154238598758)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.630971796795533)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(15.00752282630259)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2O
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.09540405626785535);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.95608794076996)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-12.04606585246879)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(30.61822927962006)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.01590925343656362);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4576174623960179)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.065459744313331)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.069307106082508)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02934674200838898);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.582168039279237)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.021054191345565)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(11.57880053161914)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H4
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.01178955170353108);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4766243659164148)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.083701125827112)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.42414777442611)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[9])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H5
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(1.377143158556522e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2468067039334131)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.413381543169306)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.751830830827839)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[10])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH2O
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.04004666591975085);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.6203934945229457)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.5813012183647)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.562399309905459)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[11])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species C2H6
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.01122438780571428);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4671891488760542)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.085570573784379)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.92569539920283)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[12])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3O
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.02700966360245718);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.3302503247033797)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.5901734642841732)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.689589350425731)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[13])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.0115252152240115);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2711197315686871)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.891810356017463)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.938378170116771)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[14])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HO2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(5.799969718755228e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1632053830988643)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.340065203947785)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.130093365014655)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[15])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species H2O2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-4.467536916305526e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.01818530793145573)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(1.325299407573309)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.898128844523025)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[16])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CO2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.02109965793388982);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.582640562453916)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.968339795463443)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-11.54098453117943)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[17])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3HCO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-3.280842418595923e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.174962798081539)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.139056863623322)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.884150350795055)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[18])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HCOOH
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(8.355378131029852e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.4144369280866382)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(5.702536484362833)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-13.64058709322757)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[19])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH3
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-8.016865668546173e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.07676194103215409)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.455143630954375)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.307841747009206)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[20])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.02806490464177051);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.8013681701684446)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(8.090042284050424)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-17.92570219976034)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[21])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCHO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-6.85619957188065e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1011227818498091)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.577780313568882)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.639124901940727)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[22])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH2OH
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-4.811640316628474e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.1246285105106686)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(3.61405693688714)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-8.201968404580237)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[23])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species OCH2OCHO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(1.809547240084334e-04);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.2208904202224758)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(4.130184222890766)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-9.028889222172253)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[24])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HOCH2OCO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.01010838735996976);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.0113110750868739)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(2.392748262187772)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-4.747760530379912)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[25])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species CH3OCH2O2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.01859842769304785);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.6002254714412125)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(6.751988586949599)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-14.9470545599887)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[26])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species HO2CH2OCHO
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.03119495987148689);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-0.8585687031446342)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(8.432843826770814)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-18.39997336105232)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[27])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species O2CH2OCH2O2H
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(0.04418819272298635);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-1.126822251130392)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(10.27211025085665)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-22.49644487312027)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[28])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
      // Species N2
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d val = _mm256_set1_pd(-0.03194429885386962);
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(0.6455863299907513)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(-3.528398528983377)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(logt[idx],val),_mm256_set1_pd(12.93009748943811)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        __m256d frac = 
          _mm256_loadu_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
        __m256d mole = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(frac,_mm256_set1_pd(recip_molecular_masses[29])),_mm256_set1_pd(1e3)),mixmw[idx]); 
        sum[idx] = _mm256_add_pd(_mm256_mul_pd(mole,val),sum[idx]);
        sumr[idx] = _mm256_add_pd(_mm256_div_pd(mole,val),sumr[idx]);
      }
    }
    if (aligned<32>(lambda))
    {
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d result = 
          _mm256_add_pd(sum[idx],_mm256_div_pd(_mm256_set1_pd(1.0),sumr[idx])); 
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.5));
        result = _mm256_mul_pd(result,_mm256_set1_pd(5.403325855130351e-09));
        _mm256_stream_pd(lambda,result);
        lambda += 4;
      }
    }
    else
    {
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        __m256d result = 
          _mm256_add_pd(sum[idx],_mm256_div_pd(_mm256_set1_pd(1.0),sumr[idx])); 
        result = _mm256_mul_pd(result,_mm256_set1_pd(0.5));
        result = _mm256_mul_pd(result,_mm256_set1_pd(5.403325855130351e-09));
        _mm256_storeu_pd(lambda,result);
        lambda += 4;
      }
    }
  }
  free(sum);
  free(sumr);
  free(logt);
  free(mixmw);
}

const double visc_constants[1740] = {1.414213562373095, 
  0.8164965809277261,3.862146820209582, 0.9680758473154983,3.98407994930469, 
  0.9699140894422974,3.989508498656819, 0.9699922639292418,4.107662722577361, 
  0.971622019642798,4.227634449955632, 0.9731464190357054,5.082554094322063, 
  0.9811888513529012,5.271534037005418, 0.9824788020819667,5.275637982434918, 
  0.9825053402396818,5.369576903417063, 0.9830968156469072,5.457936524301712, 
  0.9836264244359701,5.461900413016515, 0.9836496020649622,5.548790057598741, 
  0.9841456436842519,5.634339897885405, 0.9846124717471159,5.722393387814518, 
  0.9850719113086823,5.809112331923296, 0.9855047090477966,6.607720041418092, 
  0.9887414136306164,6.61099456694389, 0.9887523765479622,6.757363697904554, 
  0.9892266014584273,6.760565742906553, 0.9892366402378487,7.653635881501895, 
  0.991572146074944,7.718687855238952, 0.9917118511507302,7.847174154217128, 
  0.9919778119620978,8.628501332738203, 0.993351081633508,8.628501332738203, 
  0.993351081633508,8.743628265718124, 0.9935233347736726,9.556355387464231, 
  0.9945695409596425,10.40177010580258, 0.9954105807514206,5.271802212565431, 
  0.9824805380660118,0.7071067811865476, 0.5773502691896258,2.730950206508258, 
  0.9390260827927792,2.817169948942702, 0.9423898864639306,2.821008513001599, 
  0.9425333143890049,2.904556165961648, 0.9455304890716288,2.989388987941487, 
  0.9483461395154609,3.593908465842583, 0.9634008457828757,3.727537464822228, 
  0.9658474431744803,3.730439392465047, 0.9658978713457121,3.796864240508869, 
  0.9670227915024118,3.859343927619476, 0.9680316606213769,3.862146820209582, 
  0.9680758473154983,3.923587077108564, 0.9690222218899796,3.98407994930469, 
  0.9699140894422974,4.046343169140707, 0.9707930073454198,4.107662722577361, 
  0.971622019642798,4.672363649468988, 0.9778547280897629,4.674679088673448, 
  0.9778759379539206,4.778177693732116, 0.9787940632701722,4.780441881466693, 
  0.9788135125650554,5.411937832542669, 0.9833538485334853,5.457936524301712, 
  0.9836264244359701,5.548790057598741, 0.9841456436842519,6.101271803856346, 
  0.9868330480801305,6.101271803856346, 0.9868330480801305,6.182678838863658, 
  0.9871709027483913,6.757363697904554, 0.9892266014584273,7.355162178156518, 
  0.9908837753057731,3.727727093579261, 0.9658507417986416,0.2589233518434015, 
  0.2506574432255698,0.3661729157920391, 0.3438459187410087,1.031571334486059, 
  0.7180087745162979,1.03297691267996, 0.7184822406842919,1.063569800371923, 
  0.7285441208237253,1.094633282151147, 0.7383000051379629,1.315991942027273, 
  0.7962070417039337,1.364923262218021, 0.8066708898588171,1.365985869524409, 
  0.8068900730770983,1.390308849813658, 0.8118173216724673,1.413187219020724, 
  0.8162989176295664,1.414213562373095, 0.8164965809277261,1.436711320388807, 
  0.8207582425214556,1.458862171785498, 0.8248244667332583,1.481661276539453, 
  0.8288801559439831,1.504114836216418, 0.8327506059785745,1.710893021166792, 
  0.8633439248643011,1.711740872291629, 0.8634528083374381,1.74963925828637, 
  0.8681990757275172,1.750468342511041, 0.8683003218550683,1.981705056227398, 
  0.8927727137098141,1.998548531311425, 0.8942972545509392,2.031816634508808, 
  0.8972194841334854,2.234120486457833, 0.9127382748000721,2.234120486457833, 
  0.9127382748000721,2.263929537832444, 0.9147375258144468,2.474363568328986, 
  0.9271458181639928,2.693261180898898, 0.9374655018854918,1.364992699132902, 
  0.8066852227333896,0.2509989791180074, 0.2434474462821885,0.3549661604104874, 
  0.3345165196077766,0.9693949090764638, 0.6960340506883298,1.001362560345476, 
  0.7075880270542277,1.031019149928013, 0.7178224991180043,1.061131931022983, 
  0.7277591150584508,1.275715888986887, 0.7870215957491786,1.323149661674189, 
  0.7977841076744662,1.324179747787349, 0.7980097289466876,1.347758321053314, 
  0.8030836919542321,1.369936495690615, 0.8077020519103046,1.370931427711368, 
  0.8079058204185576,1.392740639797434, 0.8123006214075538,1.414213562373095, 
  0.8164965809277261,1.43631489845308, 0.8206843133241595,1.458081264894575, 
  0.8246832525414386,1.658530984693539, 0.8563789353178104,1.65935288725761, 
  0.8564920218216583,1.696091389703127, 0.8614234613548143,1.696895099729718, 
  0.8615286993819776,1.921054792797927, 0.8870179868272744,1.937382771795539, 
  0.8886093464112432,1.969632701669713, 0.891660806854676,2.165745025835656, 
  0.9078915246105729,2.165745025835656, 0.9078915246105729,2.194641768482604, 
  0.9099856585455582,2.398635446342392, 0.9229992292972317,2.610833677576657, 
  0.9338440581183054,1.323216973465976, 0.7977988613181562,0.2506574432255699, 
  0.2431357808250859,0.354483155719365, 0.3341121836552468,0.9680758473154985, 
  0.6955453039315838,0.9986392936989714, 0.7066252075672828,1.029616235674224, 
  0.7173485420944357,1.059688042118217, 0.7272928228428442,1.27398001433842, 
  0.7866134966445973,1.321349243592345, 0.7973889531484409,1.32237792806084, 
  0.7976148538422746,1.345924417813594, 0.8026951886966347,1.36807241446892, 
  0.8073194954177303,1.369065992679404, 0.8075235295471489,1.390845528833163, 
  0.8119241262794462,1.412289233067774, 0.8161257391261402,1.434360495720494, 
  0.8203192310747778,1.456097244530021, 0.8243237801367711,1.656274211132215, 
  0.8560677883784604,1.657094995328289, 0.8561810597575654,1.693783507462038, 
  0.861120648669827,1.694586123875332, 0.8612260624725449,1.918440801436746, 
  0.8867602479240054,1.934746562850453, 0.8883545657425779,1.966952610041839, 
  0.8914117502446957,2.16279808293258, 0.9076740629511328,2.16279808293258, 
  0.9076740629511328,2.191655505599729, 0.9097724153382329,2.395371607976684, 
  0.9228129367233473,2.607281099740641, 0.9336812025659105,1.321416463792553, 
  0.7974037250536872,0.2434474462821884, 0.2365388994335815,0.3442866802573664, 
  0.3255335531645952,0.9402297805468971, 0.6849988788408234,0.9699140894422975, 
  0.6962261556132335,0.9712356559190902, 0.6967144818036923,1.029206810656303, 
  0.7172100349425605,1.237334814853787, 0.7777524351054738,1.283341499298605, 
  0.7888021863940915,1.284340594333098, 0.7890340160044326,1.307209784752705, 
  0.7942496393960862,1.328720708811535, 0.7990003558022288,1.329685707396501, 
  0.7992100394194569,1.350838769478411, 0.8037339722286053,1.371665659626048, 
  0.8080560102661852,1.393102056885525, 0.8123723078052557,1.414213562373095, 
  0.8164965809277261,1.608632569830871, 0.8492760427787966,1.609429744707912, 
  0.8493932884565886,1.645062935854829, 0.8545081577418637,1.645842465533446, 
  0.8546173541554862,1.863258110125362, 0.8811202766432499,1.879094847007265, 
  0.8827785926698482,1.910374508375752, 0.8859596678205917,2.100586614697575, 
  0.9029071212710276,2.100586614697575, 0.9029071212710276,2.128613972529838, 
  0.9050971131795992,2.3264703148432, 0.9187239566209273,2.532284369072048, 
  0.9301032715906857,1.283406785953845, 0.7888173457727758,0.2365388994335815, 
  0.2301869829464367,0.3345165196077766, 0.3172374499741829,0.9135479583032811, 
  0.6744724623090879,0.9423898864639306, 0.6858328298130242,0.9436739495532042, 
  0.6863272906138056,0.9716220196427982, 0.6968570626589725,1.202221751782585, 
  0.7688032478102023,1.246922859439927, 0.7801169616843492,1.247893602175156, 
  0.7803545121900962,1.270113811158251, 0.7857008447863071,1.291014298636674, 
  0.7905739733009424,1.291951912510751, 0.7907891339056414,1.312504693412406, 
  0.7954327647479643,1.332740558480532, 0.7998719059670416,1.35356863407965, 
  0.8043078475514793,1.374081037679183, 0.8085489752128718,1.562982826362255, 
  0.8423470045732541,1.563757379026295, 0.8424681849823727,1.59837937217478, 
  0.8477567674083175,1.59913678037549, 0.8478697164057328,1.810382608075828, 
  0.8753386636426602,1.825769930349574, 0.8770611312561573,1.856161938102165, 
  0.8803665053035069,2.040976209007086, 0.8980039211317182,2.040976209007086, 
  0.8980039211317182,2.068208207029321, 0.9002865161377931,2.260449785946967, 
  0.9145075472102803,2.460423252987672, 0.9264070823423055,1.246986293391747, 
  0.7801324949225127,0.1967514720831289, 0.1930503508952376,0.2782486002368323, 
  0.2680649368060655,0.7598830722774107, 0.6050242530188936,0.7838735949225752, 
  0.6169254475416105,0.7849416699988826, 0.6174457116998708,0.8081886874880889, 
  0.6285707197185578,0.8317933014581195, 0.6394853916677729,1.037182081917135, 
  0.7198928005974746,1.037989539221738, 0.7201626337087944,1.056472160210876, 
  0.7262506491670556,1.073857045692638, 0.7318252946572212,1.074636946632449, 
  0.7320719945281474,1.091732612112782, 0.7374080718525245,1.10856466912566, 
  0.7425303656419568,1.12588932289127, 0.7476699279155669,1.142951402802166, 
  0.7526036083944555,1.300078645262203, 0.7926418136878975,1.300722912979778, 
  0.7927877574810048,1.329521254963816, 0.7991743263189174,1.330151262031636, 
  0.7993110951094403,1.505864126473754, 0.8330471154383486,1.518663198068433, 
  0.835194932242619,1.543943066534902, 0.8393275779326226,1.697670339087481, 
  0.8616301043446239,1.697670339087481, 0.8616301043446239,1.720321732627696, 
  0.8645480303321482,1.880226990233127, 0.8828958945168079,2.046563580586942, 
  0.8984782634602946,1.037234845853345, 0.719910443030877,0.1896981017252554, 
  0.186374363740252,0.2682736282162872, 0.2591113978838421,0.7326419203779889, 
  0.5910009098591849,0.7557724035047513, 0.6029432125350246,0.756802189011972, 
  0.6034657052367048,0.7792158210005197, 0.6146471432780769,0.8019742299448775, 
  0.6256336996137434,0.96415086360882, 0.694085265401827,1.00077851065756, 
  0.7073818656207655,1.018598545645992, 0.713591487497392,1.035360197996973, 
  0.7192827689648849,1.036112140161621, 0.7195347479143021,1.052594940798452, 
  0.7249874626267021,1.06882358310373, 0.7302261555763849,1.085527162993567, 
  0.7354869124545533,1.101977582074621, 0.7405411157484487,1.253471948588938, 
  0.7817130668172513,1.254093119865233, 0.7818636624477916,1.281859066159646, 
  0.788457581802559,1.282466488018164, 0.7885988722165204,1.451880198017211, 
  0.8235563942357124,1.464220433948612, 0.8257892720351042,1.488594040962475, 
  0.8300880910106908,1.636810323554273, 0.8533451494485161,1.636810323554273, 
  0.8533451494485161,1.658649684198015, 0.85639527493489,1.812822476413882, 
  0.8756140302351951,1.973196043653259, 0.8919906932856912,1.000050872394664, 
  0.7071247666079385,0.1895505346139122, 0.1862344125034554,0.2680649368060655, 
  0.2589233518434015,0.7320719945281474, 0.5907016251625139,0.7551844843352723, 
  0.6026445656491346,0.7562134687671463, 0.6031670953642669,0.778609665078177, 
  0.6143495109365001,0.8013503701412827, 0.6253373771849537,0.9634008457828757, 
  0.6938052904162757,0.9992220949498121, 0.7068315896950879,1.017806172693219, 
  0.7133188864345454,1.034554786070187, 0.7190125568358325,1.035306143295228, 
  0.71926464406382,1.051776121878202, 0.7247197542809828,1.067992139840674, 
  0.7299608435468957,1.084682725931358, 0.7352241010485261,1.101120348148331, 
  0.7402807963426474,1.252496866429861, 0.7814763935589044,1.253117554493884, 
  0.7816270868471196,1.280861901518451, 0.7882253636710244,1.281468850860438, 
  0.7883667492025335,1.450750773078906, 0.8233500991692995,1.463081409478455, 
  0.8255847844123686,1.487436056140331, 0.8298871373831255,1.635537040537381, 
  0.853164567984347,1.635537040537381, 0.853164567984347,1.657359412232185, 
  0.8562175240383979,1.811412272654385, 0.8754549768867886,1.97166108448589, 
  0.8918487645236579,0.9992729277705827, 0.7068495821084,0.1862344125034554, 
  0.1830864578960195,0.2633752319429721, 0.2546898520060881,0.71926464406382, 
  0.5839114969175909,0.7419727887255553, 0.5958662464992963,0.7429837714248951, 
  0.5963894986024435,0.764988153901539, 0.6075915653785749,0.7873310180668559, 
  0.618606646021592,0.9465464757731015, 0.6874299924970035,0.9817410443736727, 
  0.7005620521918522,0.9825053402396817, 0.700839615215764,1.016455602084481, 
  0.712853500621518,1.017193814570511, 0.7131079940714454,1.033375656481916, 
  0.7186163751581033,1.049307980727467, 0.7239108194214406,1.065706570693294, 
  0.7292298189583651,1.081856622302313, 0.7343421245935389,1.230584859900806, 
  0.7760677622540173,1.231194689238331, 0.7762206519186073,1.258453658351431, 
  0.7829169823128111,1.259049989321188, 0.7830605093193033,1.425370381906871, 
  0.8186268764465924,1.437485298017982, 0.8209024637703809,1.461413868422926, 
  0.8252848251106604,1.606923876487781, 0.8490243101910381,1.606923876487781, 
  0.8490243101910381,1.628364473214622, 0.8521415707855909,1.779722231258632, 
  0.8718043809450704,1.937167544650149, 0.8885885762186616,0.9817909878915394, 
  0.7005801994846593,0.1832194265263904, 0.1802194694013624,0.2591113978838421, 
  0.2508280367794221,0.7076203255595218, 0.5776297058469191,0.7299608435468958, 
  0.5895908711470045,0.7309554592460631, 0.590114592531367,0.7526036083944555, 
  0.6013305508852113,0.7745847594840828, 0.6123665509636861,0.931222646451046, 
  0.6814922876305141,0.9658474431744805, 0.6947174233241945,0.9665993657025689, 
  0.694997081369698,0.9838108009334254, 0.7013129733946517,1.000726261417141, 
  0.7073634135610164,1.016646132268578, 0.7129192115124937,1.032320524945325, 
  0.7182612619757396,1.048453634873784, 0.7236301093593548,1.064342230082369, 
  0.7287922497867374,1.210662676635559, 0.7709969742638532,1.211262633324542, 
  0.7711518618381339,1.238080301560321, 0.7779374748238393,1.238666978409299, 
  0.7780829536509317,1.402294777050582, 0.8141833506974361,1.414213562373095, 
  0.8164965809277261,1.437754748388375, 0.8209526357161643,1.580909065966489, 
  0.8451191595254188,1.580909065966489, 0.8451191595254188,1.602002556604812, 
  0.8482960347806956,1.750909953773577, 0.868354206291716,1.9058063536445, 
  0.8855027364619978,0.9658965781467943, 0.6947357074285243,0.1830864578960196, 
  0.1800929214529031,0.2589233518434015, 0.2506574432255698,0.7071067811865476, 
  0.5773502691896258,0.7294310858927489, 0.5893116198869808,0.730424979765144, 
  0.5898353577293538,0.7520574181082091, 0.6010518387719567,0.7740226167215636, 
  0.6120886747006234,0.9305468261943384, 0.6812272710539267,0.9651464945136267, 
  0.6944564396302347,0.9658978713457123, 0.694736188635475,0.9830968156469072, 
  0.7010541981839915,0.9992742656557126, 0.7068500556379029,1.015908317254404, 
  0.7126646461465801,1.031571334486059, 0.7180087745162979,1.047692736062563, 
  0.7233798002278359,1.063569800371923, 0.7285441208237253,1.209784057151778, 
  0.7707698986659839,1.210383578431587, 0.7709248743281107,1.237181784164493, 
  0.7777144258194417,1.237768035241933, 0.777859990790196,1.401277083570061, 
  0.8139840177190056,1.413187219020724, 0.8162989176295664,1.436711320388807, 
  0.8207582425214556,1.579761745962122, 0.8449437490664348,1.579761745962122, 
  0.8449437490664348,1.600839928329848, 0.8481232784908196,1.74963925828637, 
  0.8681990757275172,1.904423244520099, 0.8853638817345015,0.9651955938270036, 
  0.6944747296742978,0.1802194694013623, 0.1773622057184381,0.2548688178310896, 
  0.246973548145155,0.6960340506883296, 0.5712756841780432,0.7180087745162977, 
  0.5832389737156648,0.7189871048001576, 0.5837629768710566,0.7402807963426474, 
  0.5949888250090313,0.7619020373939243, 0.606041844071356,0.915975202082444, 
  0.6754475076323416,0.9500330670803382, 0.6887620627140379,0.9507726779480946, 
  0.6890437415396152,0.9677023004436333, 0.6954067193769616,0.9836264244359701, 
  0.7012461749317455,0.984340794356918, 0.7015048838980167,1.015417746823834, 
  0.71249523060695,1.031286700057797, 0.717912774444376,1.046915142152127, 
  0.7231237039799582,1.190839799817117, 0.7658015901726901,1.191429933069917, 
  0.7659584636598922,1.217808500188386, 0.7728329124419338,1.218385571039646, 
  0.7729803352256907,1.3793341975555, 0.8096152670133596,1.391057829745903, 
  0.8119663538364404,1.414213562373095, 0.8164965809277261,1.555023931915027, 
  0.8410943804666712,1.555023931915027, 0.8410943804666712,1.575772046690475, 
  0.8443316463605945,1.722241297339654, 0.8647913616518677,1.874601489302694, 
  0.8823114007467934,0.95008139753747, 0.6887804787718768,0.1774830802052437, 
  0.1747520542941766,0.2509989791180074, 0.2434474462821885,0.6854657138556842, 
  0.565388891894947,0.7071067811865476, 0.5773502691896258,0.7080702568466096, 
  0.5778743617221752,0.7290406324472875, 0.5891056647772915,0.7503335841599268, 
  0.6001707540730823,0.9020673559700526, 0.6698124036621164,0.9356080982945049, 
  0.6832055047437288,0.9363364791703267, 0.683488966178902,0.9530090482173947, 
  0.6898935610111014,0.9686913858977697, 0.6957734972999603,0.9693949090764638, 
  0.6960340506883298,0.9848163508348563, 0.701676952993576,1.01562800461544, 
  0.7125678571310604,1.031019149928013, 0.7178224991180043,1.172758506084803, 
  0.7609281352078161,1.173339678961333, 0.761086815717388,1.199317723171196, 
  0.7680421800463454,1.199886031981107, 0.7681913752193938,1.358390871018332, 
  0.8053161124176151,1.369936495690615, 0.8077020519103046,1.392740639797434, 
  0.8123006214075538,1.531412994089427, 0.8372971475129369,1.531412994089427, 
  0.8372971475129369,1.551846076769286, 0.8405904165138709,1.696091389703127, 
  0.8614234613548143,1.846138197964666, 0.8792902758466925,0.9356556949189316, 
  0.6832240371937915,0.1747520542941766, 0.1721433401286832,0.247136725235384, 
  0.2399186047167572,0.6749180908173465, 0.5594262123660976,0.6962261556132336, 
  0.5713818844377662,0.6971748057643555, 0.5719059005893237,0.7178224991180044, 
  0.5831391202030293,0.7387878049345782, 0.5942128291842127,0.888186769044059, 
  0.6640705375867318,0.9212114022483711, 0.6775389299575841,0.9219285751428873, 
  0.6778241078903778,0.9383445945626954, 0.6842688588135148,0.9537856198289424, 
  0.6901879923821975,0.9544783175248478, 0.6904503346529252,0.9696624614124828, 
  0.6961330679471983,0.984612471747116, 0.7016031990973591,1.015154313629231, 
  0.7124042040882749,1.154712651438613, 0.7559323444988001,1.155284881501085, 
  0.7560928216566766,1.180863187821715, 0.763128719376609,1.181422751763757, 
  0.7632796803223634,1.337488593112078, 0.8008948570018367,1.348856559158519, 
  0.8033158590005929,1.371309803853611, 0.8079832383229113,1.507848333376042, 
  0.8333825770601405,1.507848333376042, 0.8333825770601405,1.527967001418871, 
  0.836732594676151,1.669992735524596, 0.8579449079413927,1.817730694284756, 
  0.876165402858484,0.9212582664783947, 0.6775575744738503,0.1721433401286832, 
  0.1696480723280338,0.2434474462821884, 0.2365388994335815,0.6648428536982504, 
  0.5536482892977428,0.6858328298130242, 0.5655948487899036,0.6867673184305531, 
  0.5661186320030672,0.7071067811865476, 0.5773502691896258,0.7277591150584509, 
  0.5884288866823369,0.8749278381813145, 0.6584738481000175,0.9074594767321543, 
  0.672011053395733,0.9081659436060942, 0.6722978079439913,0.924336903232045, 
  0.6787795253596322,0.9395474235036325, 0.6847348805565449,0.9402297805468971, 
  0.6849988788408234,0.9551872541878759, 0.6907185452427825,0.9699140894422975, 
  0.6962261556132335,0.9850719113086823, 0.7017693709455775,1.137474998564951, 
  0.7510336771306878,1.138038686326299, 0.751195861570777,1.1632351574216, 
  0.7583083679057505,1.163786368143487, 0.7584610102935874,1.317522444770475, 
  0.7965456302852552,1.328720708811535, 0.7990003558022288,1.350838769478411, 
  0.8037339722286053,1.485339039722349, 0.8295224029153299,1.485339039722349, 
  0.8295224029153299,1.505157374504284, 0.8329274054011169,1.645062935854829, 
  0.8545081577418637,1.790595449263543, 0.8730736108758221,0.9075056413687956, 
  0.6720298008362261,0.1513381308124229, 0.1496342773956902,0.2140244370991221, 
  0.2092848077441266,0.5844900806936612, 0.5046159603103175,0.6029432125350246, 
  0.5163478664078446,0.6037647590469988, 0.5168635619782184,0.6216459984427261, 
  0.5279490535667124,0.6398022954144912, 0.5389355470614888,0.7691842363877284, 
  0.6096875881905095,0.7977841076744662, 0.6236382614682711,0.7984051911047227, 
  0.6239348093432265,0.8126217318166965, 0.6306495289699677,0.8259939116806735, 
  0.6368388066661637,0.8265937991895203, 0.6371136188392371,0.8397435155875503, 
  0.6430769195741511,0.8526904685078353, 0.6488361681111461,0.8660163190852177, 
  0.6546497464603095,0.8791402019926672, 0.6602638986159685,1.000495560572372, 
  0.7072819232039818,1.022646791260597, 0.7149791664698106,1.023131382765977, 
  0.7151447134209092,1.158286948225388, 0.7569324965326423,1.168131792336413, 
  0.7596599957278316,1.187576668658776, 0.7649316873386622,1.3058212634091, 
  0.7939378765229638,1.3058212634091, 0.7939378765229638,1.323244358252448, 
  0.7978048632052999,1.446240961718064, 0.8225230951698341,1.574184444952702, 
  0.8440871521167191,0.7978246928624479, 0.6236576478512198,0.1512631707489478, 
  0.1495618195622024,0.2139184275607192, 0.2091856829965662,0.5842005739228679, 
  0.5044296261860434,0.6026445656491346, 0.5161602624726624,0.6034657052367048, 
  0.5166759070369086,0.6213380877843072, 0.5277604016264412,0.6394853916677729, 
  0.5387460972410903,0.7688032478102022, 0.6094978027755634,0.7973889531484408, 
  0.6234494473040505,0.7980097289466875, 0.6237460197122588,0.8122192279911816, 
  0.630461338652143,0.8255847844123686, 0.6366512436048324,0.8261843747878657, 
  0.636926085305184,0.8393275779326225, 0.64289006210065,0.8522681180314492, 
  0.6486500280900068,0.8655873681136373, 0.6544643955474164,0.878704750563532, 
  0.6600793721659071,0.9995046848862693, 0.7069315957777504,1.022140258848878, 
  0.7148059953842244,1.022624610328761, 0.7149715861035167,1.157713231193895, 
  0.7567723175258367,1.16755319900484, 0.7595007961590622,1.186988443986076, 
  0.7647744285081944,1.305174470401503, 0.7937924387932112,1.305174470401503, 
  0.7937924387932112,1.322588935322647, 0.7976611499199272,1.445524616711629, 
  0.8223912504024188,1.573404727605317, 0.8439668807769256,0.7974295182339657, 
  0.6234688352858434,0.1479867067551948, 0.1463923869844662,0.2092848077441266, 
  0.2048467273525904,0.5715463889278635, 0.4962160466025711,0.5895908711470045, 
  0.5078874090066529,0.5903942242880842, 0.5084006573898743,0.607879478775301, 
  0.5194379735373668,0.6256336996137434, 0.5303852027662534,0.7521504423238539, 
  0.6010993230346416,0.7801169616843491, 0.6150891331328014,0.7807242910531638, 
  0.6153866882421827,0.7946260026054478, 0.6221261920271492,0.8077020519103045, 
  0.6283416946731358,0.8082886547471523, 0.6286177470230505,0.8211471671000055, 
  0.6346095566933405,0.8338074062274617, 0.6403992580177285,0.8468381522203727, 
  0.6462465146231868,0.8596714031723187, 0.6518960186748475,0.9778547280897628, 
  0.6991457584181819,0.9783393143385115, 0.6993228074092593,1.000473860094728, 
  0.7072742565027537,1.132636368806858, 0.7496358955113952,1.14226319616813, 
  0.7524070205547019,1.161277460417074, 0.7577652287536132,1.276903496464736, 
  0.7873002049300376,1.276903496464736, 0.7873002049300376,1.293940752135261, 
  0.7912445578998659,1.414213562373095, 0.8164965809277261,1.539323702382358, 
  0.8385830908303361,0.7801566481023076, 0.6151085851374556,0.1479166149740114, 
  0.1463245352322465,0.2091856829965661, 0.2047537731521891,0.5712756841780432, 
  0.4960388604397693,0.5893116198869807, 0.5077088734119169,0.590114592531367, 
  0.5082220669333795,0.6075915653785749, 0.5192582960110279,0.6253373771849536, 
  0.5302046246517115,0.751794197054422, 0.6009174429444923,0.7797474704741265, 
  0.6149079758295807,0.7803545121900961, 0.6152055500008348,0.7942496393960862, 
  0.6219455271521722,0.8073194954177303, 0.6281615375346074,0.8079058204185576, 
  0.628437614030125,0.8207582425214557, 0.6344299814434835,0.8334124853082262, 
  0.6402202832139392,0.8464370594751888, 0.6460682081692233,0.8592642321418797, 
  0.6517184176194737,0.9773915812224986, 0.6989764222533731,0.9778759379539207, 
  0.6991535103713786,0.9995263643423098, 0.7069392661951075,1.132099911835394, 
  0.7494803032791987,1.141722179587958, 0.7522523366135777,1.160727438003349, 
  0.7576123477227482,1.276298709437381, 0.7871583839105838,1.276298709437381, 
  0.7871583839105838,1.293327895656111, 0.791104360160902,1.413543740402366, 
  0.8163676125517652,1.538594623788182, 0.8384651566239493,0.7797871380951823, 
  0.6149274290756328,0.1306568558372243, 0.1295556989419211,0.1847766975420289, 
  0.1817008766472619,0.5046159603103174, 0.4505073602675259,0.5205473595802786, 
  0.4617348709431521,0.521256636770385, 0.4622296644545398,0.5366942961717304, 
  0.4728922267156894,0.5523694248603359, 0.4835103141943113,0.6640705375867318, 
  0.5532020457842204,0.6887620627140378, 0.5672344008551246,0.6892982713203838, 
  0.5675338000488647,0.7015720353766522, 0.5743257239226671,0.7131168256244096, 
  0.580607846525601,0.7136347348607744, 0.5808872686658104,0.7249874626267021, 
  0.5869609181358555,0.7361651357759343, 0.5928456452404612,0.7476699279155667, 
  0.5988050000025114,0.7590003524943438, 0.6045784141643379,0.8633439248643012, 
  0.6534930724138255,0.8637717640738607, 0.6536785597115559,0.8828958945168079, 
  0.6618504545294417,0.8833142636490184, 0.6620266422105084,1.00849948635449, 
  0.7100927288087108,1.025287102197139, 0.7158796976784185,1.127372854722873, 
  0.7481038979875578,1.127372854722873, 0.7481038979875578,1.142414977808212, 
  0.7524503946274755,1.248603348189195, 0.7805279806531389,1.359062577165797, 
  0.8054560052104229,0.6887971016895215, 0.5672539722043541,0.1295556989419211, 
  0.1284819220248193,0.1832194265263904, 0.1802194694013624,0.5003631307085703, 
  0.4474733740711871,0.5161602624726624, 0.4586648334792883,0.5168635619782183, 
  0.459158105149322,0.5321711150411844, 0.4697892679956007,0.5477141360349459, 
  0.4803787797557983,0.6584738481000175, 0.5499540209474307,0.6829572766603638, 
  0.563978792324439,0.683488966178902, 0.5642780907910415,0.6956592887445937, 
  0.5710684240751878,0.7071067811865476, 0.5773502691896258,0.7076203255595218, 
  0.5776297058469191,0.7188773741941874, 0.5837042403799687,0.7299608435468958, 
  0.5895908711470045,0.7413686749789373, 0.5955532139768365,0.7526036083944555, 
  0.6013305508852113,0.8560677883784604, 0.6503204524623155,0.8564920218216584, 
  0.6505063724774344,0.8754549768867887, 0.658698470789174,0.8758698200650663, 
  0.65887511871326,0.9915721460749441, 0.7041081710170666,1.016646132268578, 
  0.7129192115124937,1.117871520984195, 0.7453078480727765,1.117871520984195, 
  0.7453078480727765,1.132786871253449, 0.749679524438323,1.238080301560321, 
  0.7779374748238393,1.347608596290434, 0.8030520118454051,0.6829920203324803, 
  0.5639983570106363,0.1274344089155448, 0.1264121061247254,0.1802194694013623, 
  0.1773622057184381,0.492170397178459, 0.4415848698622297,0.5077088734119168, 
  0.4527041037137483,0.5084006573898744, 0.4531943198294617,0.5234575710760636, 
  0.463762295788723,0.5387460972410903, 0.474294018874042,0.6476922767912143, 
  0.5436259899248357,0.6717748240839613, 0.557632281312898,0.6722978079439912, 
  0.5579313033035891,0.6842688588135148, 0.5647167054745827,0.6955289148729517, 
  0.5709962958817534,0.6960340506883296, 0.5712756841780432,0.7071067811865476, 
  0.5773502691896258,0.7180087745162977, 0.5832389737156648,0.7292298189583651, 
  0.5892054706036101,0.7402807963426474, 0.5949888250090313,0.8420508977575138, 
  0.6441114140466905,0.8424681849823727, 0.6442981247062299,0.861120648669827, 
  0.6525272853237511,0.8615286993819777, 0.6527047805693058,0.9753365646139989, 
  0.6982236450105769,0.9836264244359701, 0.7012461749317455,1.099567967164484, 
  0.739808530184733,1.099567967164484, 0.739808530184733,1.114239099819039, 
  0.7442283941135424,1.217808500188386, 0.7728329124419338,1.325543425108336, 
  0.7983079018932795,0.671808998877937, 0.5576518277744518,0.1158949812299161, 
  0.1151244049606323,0.1639002542663226, 0.1617421874987436,0.4476034332353696, 
  0.4085447854458406,0.4617348709431521, 0.4192051759464445,0.4623640125684228, 
  0.419675821850369,0.4760574941319293, 0.4298357015861594,0.4899616152245546, 
  0.4399874516756801,0.5890425113614887, 0.5075367605278188,0.6109443382716068, 
  0.5213463876471091,0.6114199649501272, 0.5216418502536799,0.6223070144341114, 
  0.5283537836569659,0.6325474510380203, 0.5345779701811978,0.6330068458462199, 
  0.5348551775140238,0.6430769195741511, 0.540888383261636,0.6529917167083964, 
  0.546748101749516,0.6631966742709595, 0.5526965535016468,0.6732469646707244, 
  0.5584734398891116,0.76580159017269, 0.6079988883396146,0.7661810912470394, 
  0.6081887569782313,0.7831445389323647, 0.6165698559912905,0.7835156398777687, 
  0.6167509048548513,0.8870179868272744, 0.6635816135305601,0.8945571841024996, 
  0.6667204898614765,0.9094481013108767, 0.6728174631100958,1.0, 
  0.7071067811865475,1.013342633736766, 0.7117772223868714,1.107533628256574, 
  0.7422202886471023,1.205512951169893, 0.7696618511911792,0.6109754184731008, 
  0.5213657006137662,0.1158949812299161, 0.1151244049606323,0.1639002542663226, 
  0.1617421874987436,0.4476034332353696, 0.4085447854458406,0.4617348709431521, 
  0.4192051759464445,0.4623640125684228, 0.419675821850369,0.4760574941319293, 
  0.4298357015861594,0.4899616152245546, 0.4399874516756801,0.5890425113614887, 
  0.5075367605278188,0.6109443382716068, 0.5213463876471091,0.6114199649501272, 
  0.5216418502536799,0.6223070144341114, 0.5283537836569659,0.6325474510380203, 
  0.5345779701811978,0.6330068458462199, 0.5348551775140238,0.6430769195741511, 
  0.540888383261636,0.6529917167083964, 0.546748101749516,0.6631966742709595, 
  0.5526965535016468,0.6732469646707244, 0.5584734398891116,0.76580159017269, 
  0.6079988883396146,0.7661810912470394, 0.6081887569782313,0.7831445389323647, 
  0.6165698559912905,0.7835156398777687, 0.6167509048548513,0.8870179868272744, 
  0.6635816135305601,0.8945571841024996, 0.6667204898614765,0.9094481013108767, 
  0.6728174631100958,1.0, 0.7071067811865475,1.013342633736766, 
  0.7117772223868714,1.107533628256574, 0.7422202886471023,1.205512951169893, 
  0.7696618511911792,0.6109754184731008, 0.5213657006137662,0.1143689975843076, 
  0.1136282678746834,0.1617421874987436, 0.1596671812456343,0.441709860350791, 
  0.4040485847851274,0.4556552300977165, 0.4146397246301983,0.4562760878454564, 
  0.4151073985002407,0.4697892679956006, 0.4252049102655752,0.4835103141943113, 
  0.4352978162826863,0.5812866169356329, 0.5025501997301394,0.602900063543868, 
  0.5163207656769135,0.6033694276687807, 0.5166154774390286,0.61411312789566, 
  0.5233113254450602,0.6242187291631667, 0.5295222727849593,0.6246720751420145, 
  0.529798928351109,0.6346095566933406, 0.5358209317990553,0.6443938061704239, 
  0.5416712579277553,0.6544643955474164, 0.5476116918095486,0.664382354256707, 
  0.5533822705253246,0.7557183174547267, 0.6029157488787076,0.7560928216566766, 
  0.6031058695688671,0.7728329124419339, 0.6114998361355838,0.7731991271190324, 
  0.61168120073647,0.8753386636426603, 0.6586489228906067,0.8827785926698482, 
  0.6618010355370638,0.8974734418872998, 0.6679252184153387,0.9868330480801305, 
  0.7024052859220453,0.9868330480801305, 0.7024052859220453,1.092950786223682, 
  0.7377831216950063,1.189640020103059, 0.7654822340333597,0.6029307345138433, 
  0.5163400294548546,0.1046424038720633, 0.1040741475839515,0.1479867067551948, 
  0.1463923869844662,0.4041443273735761, 0.3747007230591108,0.4169037031137309, 
  0.3848017966651355,0.4174717595674758, 0.3852483404455255,0.4298357015861594, 
  0.3949003564581685,0.4423898315357054, 0.4045688397484871,0.5318506782396583, 
  0.4695687803137741,0.5516259937256494, 0.4830114595486103,0.5520554404407519, 
  0.4832996828512844,0.5618854349494713, 0.4898541837781755,0.5711315980840652, 
  0.4959445255424078,0.5715463889278635, 0.4962160466025711,0.580638730208537, 
  0.5021313581248521,0.5895908711470045, 0.5078874090066529,0.5988050000025114, 
  0.5137417006020002,0.607879478775301, 0.5194379735373668,0.6914477092475988, 
  0.5687317099584266,0.6917903634701588, 0.5689223420305677,0.7071067811865476, 
  0.5773502691896258,0.7074418508728633, 0.5775326148162813,0.8008948570018366, 
  0.6251208454511281,0.8077020519103045, 0.6283416946731358,0.8211471671000055, 
  0.6346095566933405,0.9029071212710276, 0.6701559841713063,0.9029071212710276, 
  0.6701559841713063,0.9149542802884645, 0.675037825119431,1.088466228395748, 
  0.7363990457986389,0.5516540562609088, 0.4830302985252535,0.09613748331566704, 
  0.09569626809922814,0.1359589327574335, 0.1347195005772294,0.3712970754905552, 
  0.3480781992233687,0.3830194196545632, 0.3576804091883355,0.3835413067273318, 
  0.358105308498876,0.3949003564581685, 0.3672981134940705,0.4064341363973488, 
  0.3765234624641826,0.4886239594438626, 0.4390180065662151,0.5067920155305794, 
  0.4520537612847743,0.5071865585158363, 0.4523337055953546,0.516217609964449, 
  0.4587050710573101,0.5247122815430255, 0.4646341611615674,0.5250933598282101, 
  0.4648986953305155,0.5334467115845382, 0.4706661153219247,0.5416712579277553, 
  0.4762862698015209,0.5501364988467016, 0.4820105671391762,0.5584734398891116, 
  0.487588422742228,0.6352495752364306, 0.5362060048446742,0.6355643798795337, 
  0.5363952872198511,0.6496359397651932, 0.5447737142827658,0.6499437763131489, 
  0.5449552102031654,0.7358012918620791, 0.5926555691718987,0.7420552249018766, 
  0.5959089412578464,0.7544075743261828, 0.6022495278327332,0.8295224029153299, 
  0.6384517482323681,0.8295224029153299, 0.6384517482323681,0.840590416513871, 
  0.6434570299400704,0.9187239566209273, 0.6765474450080009,0.5068177972540058, 
  0.4520720585482646,0.1896884518194713, 0.1863652122085029,0.2682599811886517, 
  0.2590991018259489,0.7326046510250497, 0.5909813460876593,0.7557339575086043, 
  0.6029236907598284,0.7567636906308354, 0.6034461858944041,0.7791761824422541, 
  0.614627688123463,0.801933433670746, 0.6256143303713364,0.9641018174406674, 
  0.6940669665205846,0.9999491301932054, 0.7070887953076815,1.000727601248079, 
  0.707363886746546,1.018546729734773, 0.7135736711020348,1.035307529423738, 
  0.7192651088602782,1.036059433337233, 0.7195170948933813,1.05254139549718, 
  0.7249699663177671,1.06876921225456, 0.7302088160249892,1.085471942436515, 
  0.7354697364767042,1.101921524687929, 0.7405241027731794,1.253408184713091, 
  0.7816976002756334,1.254029324390523, 0.7818482022920522,1.281793858236612, 
  0.788442406577803,1.282401249195698, 0.7885837032112905,1.451806341152049, 
  0.8235429138899696,1.464145949338032, 0.8257759098504285,1.488518316471216, 
  0.8300749598559236,1.636727059329355, 0.8533333500007583,1.636727059329355, 
  0.8533333500007583,1.65856530900904, 0.8563836605065277,1.812730258484754, 
  0.8756036379016505,1.97309566755175, 0.8919814201427821}; 

void avx_viscosity(const double *temperature_array, const double 
  *mass_frac_array, const double *mixmw_array, const int num_elmts, const int 
  spec_stride, double *viscosity) 
{
  __m256d *mole_frac = (__m256d*)memalign(32,66*30*sizeof(__m256d));
  __m256d *spec_visc = (__m256d*)memalign(32,66*30*sizeof(__m256d));
  __m256d *result_vec = (__m256d*)memalign(32,66*sizeof(__m256d));
  __m256d *spec_sum = (__m256d*)memalign(32,66*sizeof(__m256d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 264)
  {
    for (unsigned idx = 0; idx < 66; idx++)
    {
      __m256d temp;
      if (aligned<32>(temperature_array))
        temp = _mm256_load_pd(temperature_array);
      else
        temp = _mm256_loadu_pd(temperature_array);
      temperature_array += 4;
      if (aligned<32>(mass_frac_array))
      {
        mole_frac[0+idx] = 
          _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        mole_frac[66+idx] = 
          _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        mole_frac[132+idx] = 
          _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        mole_frac[198+idx] = 
          _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        mole_frac[264+idx] = 
          _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        mole_frac[330+idx] = 
          _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        mole_frac[396+idx] = 
          _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        mole_frac[462+idx] = 
          _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        mole_frac[528+idx] = 
          _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        mole_frac[594+idx] = 
          _mm256_load_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        mole_frac[660+idx] = 
          _mm256_load_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        mole_frac[726+idx] = 
          _mm256_load_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        mole_frac[792+idx] = 
          _mm256_load_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        mole_frac[858+idx] = 
          _mm256_load_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        mole_frac[924+idx] = 
          _mm256_load_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        mole_frac[990+idx] = 
          _mm256_load_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        mole_frac[1056+idx] = 
          _mm256_load_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        mole_frac[1122+idx] = 
          _mm256_load_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        mole_frac[1188+idx] = 
          _mm256_load_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        mole_frac[1254+idx] = 
          _mm256_load_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        mole_frac[1320+idx] = 
          _mm256_load_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        mole_frac[1386+idx] = 
          _mm256_load_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        mole_frac[1452+idx] = 
          _mm256_load_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        mole_frac[1518+idx] = 
          _mm256_load_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        mole_frac[1584+idx] = 
          _mm256_load_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        mole_frac[1650+idx] = 
          _mm256_load_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        mole_frac[1716+idx] = 
          _mm256_load_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        mole_frac[1782+idx] = 
          _mm256_load_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        mole_frac[1848+idx] = 
          _mm256_load_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        mole_frac[1914+idx] = 
          _mm256_load_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
      }
      else
      {
        mole_frac[1980+idx] = 
          _mm256_loadu_pd(mass_frac_array+(30*spec_stride)+(idx<<2)); 
        mole_frac[2046+idx] = 
          _mm256_loadu_pd(mass_frac_array+(31*spec_stride)+(idx<<2)); 
        mole_frac[2112+idx] = 
          _mm256_loadu_pd(mass_frac_array+(32*spec_stride)+(idx<<2)); 
        mole_frac[2178+idx] = 
          _mm256_loadu_pd(mass_frac_array+(33*spec_stride)+(idx<<2)); 
        mole_frac[2244+idx] = 
          _mm256_loadu_pd(mass_frac_array+(34*spec_stride)+(idx<<2)); 
        mole_frac[2310+idx] = 
          _mm256_loadu_pd(mass_frac_array+(35*spec_stride)+(idx<<2)); 
        mole_frac[2376+idx] = 
          _mm256_loadu_pd(mass_frac_array+(36*spec_stride)+(idx<<2)); 
        mole_frac[2442+idx] = 
          _mm256_loadu_pd(mass_frac_array+(37*spec_stride)+(idx<<2)); 
        mole_frac[2508+idx] = 
          _mm256_loadu_pd(mass_frac_array+(38*spec_stride)+(idx<<2)); 
        mole_frac[2574+idx] = 
          _mm256_loadu_pd(mass_frac_array+(39*spec_stride)+(idx<<2)); 
        mole_frac[2640+idx] = 
          _mm256_loadu_pd(mass_frac_array+(40*spec_stride)+(idx<<2)); 
        mole_frac[2706+idx] = 
          _mm256_loadu_pd(mass_frac_array+(41*spec_stride)+(idx<<2)); 
        mole_frac[2772+idx] = 
          _mm256_loadu_pd(mass_frac_array+(42*spec_stride)+(idx<<2)); 
        mole_frac[2838+idx] = 
          _mm256_loadu_pd(mass_frac_array+(43*spec_stride)+(idx<<2)); 
        mole_frac[2904+idx] = 
          _mm256_loadu_pd(mass_frac_array+(44*spec_stride)+(idx<<2)); 
        mole_frac[2970+idx] = 
          _mm256_loadu_pd(mass_frac_array+(45*spec_stride)+(idx<<2)); 
        mole_frac[3036+idx] = 
          _mm256_loadu_pd(mass_frac_array+(46*spec_stride)+(idx<<2)); 
        mole_frac[3102+idx] = 
          _mm256_loadu_pd(mass_frac_array+(47*spec_stride)+(idx<<2)); 
        mole_frac[3168+idx] = 
          _mm256_loadu_pd(mass_frac_array+(48*spec_stride)+(idx<<2)); 
        mole_frac[3234+idx] = 
          _mm256_loadu_pd(mass_frac_array+(49*spec_stride)+(idx<<2)); 
        mole_frac[3300+idx] = 
          _mm256_loadu_pd(mass_frac_array+(50*spec_stride)+(idx<<2)); 
        mole_frac[3366+idx] = 
          _mm256_loadu_pd(mass_frac_array+(51*spec_stride)+(idx<<2)); 
        mole_frac[3432+idx] = 
          _mm256_loadu_pd(mass_frac_array+(52*spec_stride)+(idx<<2)); 
        mole_frac[3498+idx] = 
          _mm256_loadu_pd(mass_frac_array+(53*spec_stride)+(idx<<2)); 
        mole_frac[3564+idx] = 
          _mm256_loadu_pd(mass_frac_array+(54*spec_stride)+(idx<<2)); 
        mole_frac[3630+idx] = 
          _mm256_loadu_pd(mass_frac_array+(55*spec_stride)+(idx<<2)); 
        mole_frac[3696+idx] = 
          _mm256_loadu_pd(mass_frac_array+(56*spec_stride)+(idx<<2)); 
        mole_frac[3762+idx] = 
          _mm256_loadu_pd(mass_frac_array+(57*spec_stride)+(idx<<2)); 
        mole_frac[3828+idx] = 
          _mm256_loadu_pd(mass_frac_array+(58*spec_stride)+(idx<<2)); 
        mole_frac[3894+idx] = 
          _mm256_loadu_pd(mass_frac_array+(59*spec_stride)+(idx<<2)); 
      }
      __m256d mixmw;
      if (aligned<32>(mixmw_array))
        mixmw = _mm256_load_pd(mixmw_array);
      else
        mixmw = _mm256_loadu_pd(mixmw_array);
      mixmw_array += 4;
      result_vec[idx] = _mm256_set1_pd(0.0);
      temp = _mm256_mul_pd(temp,_mm256_set1_pd(120.0));
      __m128d lower = _mm256_extractf128_pd(temp,0);
      __m128d upper = _mm256_extractf128_pd(temp,1);
      __m256d logt = 
        _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      // Species H
      {
        __m256d val = _mm256_set1_pd(0.01757076575883849);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.3980310503714805)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.652696513182246)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-20.40535459028184)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[0+idx] = val;
      }
      // Species H2
      {
        __m256d val = _mm256_set1_pd(2.33100139953291e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.05016056447174558)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.003492096616715)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-13.84035618317203)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[66+idx] = val;
      }
      // Species CH3
      {
        __m256d val = _mm256_set1_pd(0.01745292662633083);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.3952264471530641)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.630413752559152)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-20.22834639799518)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[132+idx] = val;
      }
      // Species O
      {
        __m256d val = _mm256_set1_pd(7.841447484966627e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.173865132716423)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.929020467011314)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-15.10026747364014)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[198+idx] = val;
      }
      // Species CH4
      {
        __m256d val = _mm256_set1_pd(0.01712465091640445);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.3874927847672007)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.569547094857978)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-20.00458513330756)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[264+idx] = val;
      }
      // Species OH
      {
        __m256d val = _mm256_set1_pd(7.841447484966898e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.1738651327164285)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.929020467011351)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-15.06971969464791)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[330+idx] = val;
      }
      // Species H2O
      {
        __m256d val = _mm256_set1_pd(-0.05045537005542979);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.031781480707904)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-5.987902336937527)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(0.7009388163509245)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[396+idx] = val;
      }
      // Species C2H2
      {
        __m256d val = _mm256_set1_pd(0.02486206221740027);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.587852927349057)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.290034479082859)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.74505541054281)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[462+idx] = val;
      }
      // Species CO
      {
        __m256d val = _mm256_set1_pd(0.01054816060102988);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.235770758410507)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.400968097030088)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.61559645415615)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[528+idx] = val;
      }
      // Species C2H4
      {
        __m256d val = _mm256_set1_pd(0.02436925155378749);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5700815021202054)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.100704137488538)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-23.93711926625748)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[594+idx] = val;
      }
      // Species C2H5
      {
        __m256d val = _mm256_set1_pd(0.02441371059443401);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5734517595536173)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.145721907003817)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.529504601817)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[660+idx] = val;
      }
      // Species CH2O
      {
        __m256d val = _mm256_set1_pd(3.212240006873408e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.167235142784101)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.703808872626219)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-19.85295314006834)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[726+idx] = val;
      }
      // Species C2H6
      {
        __m256d val = _mm256_set1_pd(0.02441371059443357);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5734517595536081)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.145721907003752)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.51245692810857)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[792+idx] = val;
      }
      // Species CH3O
      {
        __m256d val = _mm256_set1_pd(-0.023618697767246);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(0.4069654657503018)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-1.384947591294291)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-10.10494407684299)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[858+idx] = val;
      }
      // Species O2
      {
        __m256d val = _mm256_set1_pd(0.01214171417829216);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.272158843610373)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.678085479588637)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-17.15808369877082)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[924+idx] = val;
      }
      // Species HO2
      {
        __m256d val = _mm256_set1_pd(0.01214171417829254);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.272158843610381)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.678085479588692)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-17.14257655384877)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[990+idx] = val;
      }
      // Species H2O2
      {
        __m256d val = _mm256_set1_pd(0.01214171417829194);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.272158843610368)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.6780854795886)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-17.12753591977842)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1056+idx] = val;
      }
      // Species CO2
      {
        __m256d val = _mm256_set1_pd(0.02440890122032096);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5724287559128219)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.130428117775916)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-23.97057723451212)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1122+idx] = val;
      }
      // Species CH3HCO
      {
        __m256d val = _mm256_set1_pd(0.01173153385928102);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.3419575514452081)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.866549994604684)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-22.31996918351742)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1188+idx] = val;
      }
      // Species HCOOH
      {
        __m256d val = _mm256_set1_pd(7.235675392407266e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.2503679239288357)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.262693232699687)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-21.25097866061817)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1254+idx] = val;
      }
      // Species CH3OCH3
      {
        __m256d val = _mm256_set1_pd(0.02237582504898801);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107528)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254609)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-25.02944655668421)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1320+idx] = val;
      }
      // Species CH3OCO
      {
        __m256d val = _mm256_set1_pd(0.01536289690465918);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.4150999855999652)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(4.341454112797969)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-23.48003905216325)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1386+idx] = val;
      }
      // Species CH3OCHO
      {
        __m256d val = _mm256_set1_pd(0.0153628969046591);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.4150999855999636)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(4.341454112797959)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-23.47157548306755)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1452+idx] = val;
      }
      // Species CH3OCH2OH
      {
        __m256d val = _mm256_set1_pd(0.02237582504898794);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107515)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254599)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.880399646385)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1518+idx] = val;
      }
      // Species OCH2OCHO
      {
        __m256d val = _mm256_set1_pd(0.02237582504898696);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107304)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254451)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.78548230085281)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1584+idx] = val;
      }
      // Species HOCH2OCO
      {
        __m256d val = _mm256_set1_pd(0.02237582504898696);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107304)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254451)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.78548230085281)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1650+idx] = val;
      }
      // Species CH3OCH2O2
      {
        __m256d val = _mm256_set1_pd(0.02237582504898747);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107413)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254526)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.77222789611538)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1716+idx] = val;
      }
      // Species HO2CH2OCHO
      {
        __m256d val = _mm256_set1_pd(0.02237582504898723);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.550061453310736)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254486)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.68334671426243)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1782+idx] = val;
      }
      // Species O2CH2OCH2O2H
      {
        __m256d val = _mm256_set1_pd(0.02237582504898733);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107383)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254504)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.59857713887057)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1848+idx] = val;
      }
      // Species N2
      {
        __m256d val = _mm256_set1_pd(0.01047722299820579);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.2341198015382627)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.388160005867027)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.56562055017989)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1914+idx] = val;
      }
      mole_frac[0+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[66+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[66+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[132+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[132+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[198+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[198+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[264+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[264+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[330+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[330+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[396+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[396+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[462+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[462+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[528+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[528+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[594+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[594+idx],_mm256_set1_pd(recip_molecular_masses[9])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[660+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[660+idx],_mm256_set1_pd(recip_molecular_masses[10])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[726+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[726+idx],_mm256_set1_pd(recip_molecular_masses[11])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[792+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[792+idx],_mm256_set1_pd(recip_molecular_masses[12])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[858+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[858+idx],_mm256_set1_pd(recip_molecular_masses[13])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[924+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[924+idx],_mm256_set1_pd(recip_molecular_masses[14])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[990+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[990+idx],_mm256_set1_pd(recip_molecular_masses[15])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1056+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1056+idx],_mm256_set1_pd(recip_molecular_masses[16])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1122+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1122+idx],_mm256_set1_pd(recip_molecular_masses[17])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1188+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1188+idx],_mm256_set1_pd(recip_molecular_masses[18])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1254+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1254+idx],_mm256_set1_pd(recip_molecular_masses[19])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1320+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1320+idx],_mm256_set1_pd(recip_molecular_masses[20])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1386+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1386+idx],_mm256_set1_pd(recip_molecular_masses[21])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1452+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1452+idx],_mm256_set1_pd(recip_molecular_masses[22])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1518+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1518+idx],_mm256_set1_pd(recip_molecular_masses[23])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1584+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1584+idx],_mm256_set1_pd(recip_molecular_masses[24])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1650+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1650+idx],_mm256_set1_pd(recip_molecular_masses[25])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1716+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1716+idx],_mm256_set1_pd(recip_molecular_masses[26])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1782+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1782+idx],_mm256_set1_pd(recip_molecular_masses[27])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1848+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1848+idx],_mm256_set1_pd(recip_molecular_masses[28])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1914+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1914+idx],_mm256_set1_pd(recip_molecular_masses[29])),_mm256_set1_pd(1e3)),mixmw); 
    }
    int visc_idx = 0;
    for (int spec_idx = 0; spec_idx < 30; spec_idx++)
    {
      spec_sum[0] = _mm256_set1_pd(0.0);
      spec_sum[1] = _mm256_set1_pd(0.0);
      spec_sum[2] = _mm256_set1_pd(0.0);
      spec_sum[3] = _mm256_set1_pd(0.0);
      spec_sum[4] = _mm256_set1_pd(0.0);
      spec_sum[5] = _mm256_set1_pd(0.0);
      spec_sum[6] = _mm256_set1_pd(0.0);
      spec_sum[7] = _mm256_set1_pd(0.0);
      spec_sum[8] = _mm256_set1_pd(0.0);
      spec_sum[9] = _mm256_set1_pd(0.0);
      spec_sum[10] = _mm256_set1_pd(0.0);
      spec_sum[11] = _mm256_set1_pd(0.0);
      spec_sum[12] = _mm256_set1_pd(0.0);
      spec_sum[13] = _mm256_set1_pd(0.0);
      spec_sum[14] = _mm256_set1_pd(0.0);
      spec_sum[15] = _mm256_set1_pd(0.0);
      spec_sum[16] = _mm256_set1_pd(0.0);
      spec_sum[17] = _mm256_set1_pd(0.0);
      spec_sum[18] = _mm256_set1_pd(0.0);
      spec_sum[19] = _mm256_set1_pd(0.0);
      spec_sum[20] = _mm256_set1_pd(0.0);
      spec_sum[21] = _mm256_set1_pd(0.0);
      spec_sum[22] = _mm256_set1_pd(0.0);
      spec_sum[23] = _mm256_set1_pd(0.0);
      spec_sum[24] = _mm256_set1_pd(0.0);
      spec_sum[25] = _mm256_set1_pd(0.0);
      spec_sum[26] = _mm256_set1_pd(0.0);
      spec_sum[27] = _mm256_set1_pd(0.0);
      spec_sum[28] = _mm256_set1_pd(0.0);
      spec_sum[29] = _mm256_set1_pd(0.0);
      spec_sum[30] = _mm256_set1_pd(0.0);
      spec_sum[31] = _mm256_set1_pd(0.0);
      spec_sum[32] = _mm256_set1_pd(0.0);
      spec_sum[33] = _mm256_set1_pd(0.0);
      spec_sum[34] = _mm256_set1_pd(0.0);
      spec_sum[35] = _mm256_set1_pd(0.0);
      spec_sum[36] = _mm256_set1_pd(0.0);
      spec_sum[37] = _mm256_set1_pd(0.0);
      spec_sum[38] = _mm256_set1_pd(0.0);
      spec_sum[39] = _mm256_set1_pd(0.0);
      spec_sum[40] = _mm256_set1_pd(0.0);
      spec_sum[41] = _mm256_set1_pd(0.0);
      spec_sum[42] = _mm256_set1_pd(0.0);
      spec_sum[43] = _mm256_set1_pd(0.0);
      spec_sum[44] = _mm256_set1_pd(0.0);
      spec_sum[45] = _mm256_set1_pd(0.0);
      spec_sum[46] = _mm256_set1_pd(0.0);
      spec_sum[47] = _mm256_set1_pd(0.0);
      spec_sum[48] = _mm256_set1_pd(0.0);
      spec_sum[49] = _mm256_set1_pd(0.0);
      spec_sum[50] = _mm256_set1_pd(0.0);
      spec_sum[51] = _mm256_set1_pd(0.0);
      spec_sum[52] = _mm256_set1_pd(0.0);
      spec_sum[53] = _mm256_set1_pd(0.0);
      spec_sum[54] = _mm256_set1_pd(0.0);
      spec_sum[55] = _mm256_set1_pd(0.0);
      spec_sum[56] = _mm256_set1_pd(0.0);
      spec_sum[57] = _mm256_set1_pd(0.0);
      spec_sum[58] = _mm256_set1_pd(0.0);
      spec_sum[59] = _mm256_set1_pd(0.0);
      spec_sum[60] = _mm256_set1_pd(0.0);
      spec_sum[61] = _mm256_set1_pd(0.0);
      spec_sum[62] = _mm256_set1_pd(0.0);
      spec_sum[63] = _mm256_set1_pd(0.0);
      spec_sum[64] = _mm256_set1_pd(0.0);
      spec_sum[65] = _mm256_set1_pd(0.0);
      for (int idx = 0; idx < 30; idx++)
      {
        if (idx == spec_idx)
        {
          spec_sum[0] = 
            _mm256_add_pd(spec_sum[0],_mm256_mul_pd(mole_frac[idx*66+0],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[1] = 
            _mm256_add_pd(spec_sum[1],_mm256_mul_pd(mole_frac[idx*66+1],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[2] = 
            _mm256_add_pd(spec_sum[2],_mm256_mul_pd(mole_frac[idx*66+2],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[3] = 
            _mm256_add_pd(spec_sum[3],_mm256_mul_pd(mole_frac[idx*66+3],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[4] = 
            _mm256_add_pd(spec_sum[4],_mm256_mul_pd(mole_frac[idx*66+4],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[5] = 
            _mm256_add_pd(spec_sum[5],_mm256_mul_pd(mole_frac[idx*66+5],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[6] = 
            _mm256_add_pd(spec_sum[6],_mm256_mul_pd(mole_frac[idx*66+6],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[7] = 
            _mm256_add_pd(spec_sum[7],_mm256_mul_pd(mole_frac[idx*66+7],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[8] = 
            _mm256_add_pd(spec_sum[8],_mm256_mul_pd(mole_frac[idx*66+8],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[9] = 
            _mm256_add_pd(spec_sum[9],_mm256_mul_pd(mole_frac[idx*66+9],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[10] = 
            _mm256_add_pd(spec_sum[10],_mm256_mul_pd(mole_frac[idx*66+10],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[11] = 
            _mm256_add_pd(spec_sum[11],_mm256_mul_pd(mole_frac[idx*66+11],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[12] = 
            _mm256_add_pd(spec_sum[12],_mm256_mul_pd(mole_frac[idx*66+12],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[13] = 
            _mm256_add_pd(spec_sum[13],_mm256_mul_pd(mole_frac[idx*66+13],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[14] = 
            _mm256_add_pd(spec_sum[14],_mm256_mul_pd(mole_frac[idx*66+14],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[15] = 
            _mm256_add_pd(spec_sum[15],_mm256_mul_pd(mole_frac[idx*66+15],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[16] = 
            _mm256_add_pd(spec_sum[16],_mm256_mul_pd(mole_frac[idx*66+16],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[17] = 
            _mm256_add_pd(spec_sum[17],_mm256_mul_pd(mole_frac[idx*66+17],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[18] = 
            _mm256_add_pd(spec_sum[18],_mm256_mul_pd(mole_frac[idx*66+18],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[19] = 
            _mm256_add_pd(spec_sum[19],_mm256_mul_pd(mole_frac[idx*66+19],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[20] = 
            _mm256_add_pd(spec_sum[20],_mm256_mul_pd(mole_frac[idx*66+20],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[21] = 
            _mm256_add_pd(spec_sum[21],_mm256_mul_pd(mole_frac[idx*66+21],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[22] = 
            _mm256_add_pd(spec_sum[22],_mm256_mul_pd(mole_frac[idx*66+22],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[23] = 
            _mm256_add_pd(spec_sum[23],_mm256_mul_pd(mole_frac[idx*66+23],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[24] = 
            _mm256_add_pd(spec_sum[24],_mm256_mul_pd(mole_frac[idx*66+24],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[25] = 
            _mm256_add_pd(spec_sum[25],_mm256_mul_pd(mole_frac[idx*66+25],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[26] = 
            _mm256_add_pd(spec_sum[26],_mm256_mul_pd(mole_frac[idx*66+26],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[27] = 
            _mm256_add_pd(spec_sum[27],_mm256_mul_pd(mole_frac[idx*66+27],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[28] = 
            _mm256_add_pd(spec_sum[28],_mm256_mul_pd(mole_frac[idx*66+28],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[29] = 
            _mm256_add_pd(spec_sum[29],_mm256_mul_pd(mole_frac[idx*66+29],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[30] = 
            _mm256_add_pd(spec_sum[30],_mm256_mul_pd(mole_frac[idx*66+30],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[31] = 
            _mm256_add_pd(spec_sum[31],_mm256_mul_pd(mole_frac[idx*66+31],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[32] = 
            _mm256_add_pd(spec_sum[32],_mm256_mul_pd(mole_frac[idx*66+32],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[33] = 
            _mm256_add_pd(spec_sum[33],_mm256_mul_pd(mole_frac[idx*66+33],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[34] = 
            _mm256_add_pd(spec_sum[34],_mm256_mul_pd(mole_frac[idx*66+34],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[35] = 
            _mm256_add_pd(spec_sum[35],_mm256_mul_pd(mole_frac[idx*66+35],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[36] = 
            _mm256_add_pd(spec_sum[36],_mm256_mul_pd(mole_frac[idx*66+36],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[37] = 
            _mm256_add_pd(spec_sum[37],_mm256_mul_pd(mole_frac[idx*66+37],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[38] = 
            _mm256_add_pd(spec_sum[38],_mm256_mul_pd(mole_frac[idx*66+38],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[39] = 
            _mm256_add_pd(spec_sum[39],_mm256_mul_pd(mole_frac[idx*66+39],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[40] = 
            _mm256_add_pd(spec_sum[40],_mm256_mul_pd(mole_frac[idx*66+40],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[41] = 
            _mm256_add_pd(spec_sum[41],_mm256_mul_pd(mole_frac[idx*66+41],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[42] = 
            _mm256_add_pd(spec_sum[42],_mm256_mul_pd(mole_frac[idx*66+42],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[43] = 
            _mm256_add_pd(spec_sum[43],_mm256_mul_pd(mole_frac[idx*66+43],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[44] = 
            _mm256_add_pd(spec_sum[44],_mm256_mul_pd(mole_frac[idx*66+44],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[45] = 
            _mm256_add_pd(spec_sum[45],_mm256_mul_pd(mole_frac[idx*66+45],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[46] = 
            _mm256_add_pd(spec_sum[46],_mm256_mul_pd(mole_frac[idx*66+46],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[47] = 
            _mm256_add_pd(spec_sum[47],_mm256_mul_pd(mole_frac[idx*66+47],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[48] = 
            _mm256_add_pd(spec_sum[48],_mm256_mul_pd(mole_frac[idx*66+48],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[49] = 
            _mm256_add_pd(spec_sum[49],_mm256_mul_pd(mole_frac[idx*66+49],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[50] = 
            _mm256_add_pd(spec_sum[50],_mm256_mul_pd(mole_frac[idx*66+50],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[51] = 
            _mm256_add_pd(spec_sum[51],_mm256_mul_pd(mole_frac[idx*66+51],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[52] = 
            _mm256_add_pd(spec_sum[52],_mm256_mul_pd(mole_frac[idx*66+52],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[53] = 
            _mm256_add_pd(spec_sum[53],_mm256_mul_pd(mole_frac[idx*66+53],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[54] = 
            _mm256_add_pd(spec_sum[54],_mm256_mul_pd(mole_frac[idx*66+54],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[55] = 
            _mm256_add_pd(spec_sum[55],_mm256_mul_pd(mole_frac[idx*66+55],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[56] = 
            _mm256_add_pd(spec_sum[56],_mm256_mul_pd(mole_frac[idx*66+56],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[57] = 
            _mm256_add_pd(spec_sum[57],_mm256_mul_pd(mole_frac[idx*66+57],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[58] = 
            _mm256_add_pd(spec_sum[58],_mm256_mul_pd(mole_frac[idx*66+58],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[59] = 
            _mm256_add_pd(spec_sum[59],_mm256_mul_pd(mole_frac[idx*66+59],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[60] = 
            _mm256_add_pd(spec_sum[60],_mm256_mul_pd(mole_frac[idx*66+60],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[61] = 
            _mm256_add_pd(spec_sum[61],_mm256_mul_pd(mole_frac[idx*66+61],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[62] = 
            _mm256_add_pd(spec_sum[62],_mm256_mul_pd(mole_frac[idx*66+62],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[63] = 
            _mm256_add_pd(spec_sum[63],_mm256_mul_pd(mole_frac[idx*66+63],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[64] = 
            _mm256_add_pd(spec_sum[64],_mm256_mul_pd(mole_frac[idx*66+64],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[65] = 
            _mm256_add_pd(spec_sum[65],_mm256_mul_pd(mole_frac[idx*66+65],_mm256_set1_pd(2.82842712474619))); 
        }
        else
        {
          __m256d c1 = _mm256_set1_pd(visc_constants[visc_idx++]);
          __m256d c2 = _mm256_set1_pd(visc_constants[visc_idx++]);
          __m256d numer;
          numer = _mm256_div_pd(spec_visc[spec_idx*66+0],spec_visc[idx*66+0]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[0] = 
            _mm256_add_pd(spec_sum[0],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+0],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+1],spec_visc[idx*66+1]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[1] = 
            _mm256_add_pd(spec_sum[1],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+1],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+2],spec_visc[idx*66+2]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[2] = 
            _mm256_add_pd(spec_sum[2],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+2],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+3],spec_visc[idx*66+3]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[3] = 
            _mm256_add_pd(spec_sum[3],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+3],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+4],spec_visc[idx*66+4]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[4] = 
            _mm256_add_pd(spec_sum[4],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+4],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+5],spec_visc[idx*66+5]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[5] = 
            _mm256_add_pd(spec_sum[5],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+5],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+6],spec_visc[idx*66+6]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[6] = 
            _mm256_add_pd(spec_sum[6],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+6],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+7],spec_visc[idx*66+7]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[7] = 
            _mm256_add_pd(spec_sum[7],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+7],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+8],spec_visc[idx*66+8]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[8] = 
            _mm256_add_pd(spec_sum[8],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+8],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+9],spec_visc[idx*66+9]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[9] = 
            _mm256_add_pd(spec_sum[9],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+9],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+10],spec_visc[idx*66+10]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[10] = 
            _mm256_add_pd(spec_sum[10],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+10],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+11],spec_visc[idx*66+11]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[11] = 
            _mm256_add_pd(spec_sum[11],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+11],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+12],spec_visc[idx*66+12]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[12] = 
            _mm256_add_pd(spec_sum[12],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+12],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+13],spec_visc[idx*66+13]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[13] = 
            _mm256_add_pd(spec_sum[13],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+13],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+14],spec_visc[idx*66+14]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[14] = 
            _mm256_add_pd(spec_sum[14],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+14],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+15],spec_visc[idx*66+15]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[15] = 
            _mm256_add_pd(spec_sum[15],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+15],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+16],spec_visc[idx*66+16]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[16] = 
            _mm256_add_pd(spec_sum[16],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+16],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+17],spec_visc[idx*66+17]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[17] = 
            _mm256_add_pd(spec_sum[17],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+17],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+18],spec_visc[idx*66+18]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[18] = 
            _mm256_add_pd(spec_sum[18],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+18],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+19],spec_visc[idx*66+19]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[19] = 
            _mm256_add_pd(spec_sum[19],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+19],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+20],spec_visc[idx*66+20]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[20] = 
            _mm256_add_pd(spec_sum[20],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+20],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+21],spec_visc[idx*66+21]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[21] = 
            _mm256_add_pd(spec_sum[21],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+21],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+22],spec_visc[idx*66+22]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[22] = 
            _mm256_add_pd(spec_sum[22],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+22],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+23],spec_visc[idx*66+23]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[23] = 
            _mm256_add_pd(spec_sum[23],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+23],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+24],spec_visc[idx*66+24]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[24] = 
            _mm256_add_pd(spec_sum[24],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+24],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+25],spec_visc[idx*66+25]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[25] = 
            _mm256_add_pd(spec_sum[25],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+25],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+26],spec_visc[idx*66+26]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[26] = 
            _mm256_add_pd(spec_sum[26],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+26],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+27],spec_visc[idx*66+27]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[27] = 
            _mm256_add_pd(spec_sum[27],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+27],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+28],spec_visc[idx*66+28]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[28] = 
            _mm256_add_pd(spec_sum[28],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+28],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+29],spec_visc[idx*66+29]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[29] = 
            _mm256_add_pd(spec_sum[29],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+29],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+30],spec_visc[idx*66+30]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[30] = 
            _mm256_add_pd(spec_sum[30],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+30],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+31],spec_visc[idx*66+31]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[31] = 
            _mm256_add_pd(spec_sum[31],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+31],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+32],spec_visc[idx*66+32]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[32] = 
            _mm256_add_pd(spec_sum[32],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+32],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+33],spec_visc[idx*66+33]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[33] = 
            _mm256_add_pd(spec_sum[33],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+33],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+34],spec_visc[idx*66+34]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[34] = 
            _mm256_add_pd(spec_sum[34],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+34],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+35],spec_visc[idx*66+35]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[35] = 
            _mm256_add_pd(spec_sum[35],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+35],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+36],spec_visc[idx*66+36]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[36] = 
            _mm256_add_pd(spec_sum[36],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+36],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+37],spec_visc[idx*66+37]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[37] = 
            _mm256_add_pd(spec_sum[37],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+37],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+38],spec_visc[idx*66+38]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[38] = 
            _mm256_add_pd(spec_sum[38],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+38],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+39],spec_visc[idx*66+39]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[39] = 
            _mm256_add_pd(spec_sum[39],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+39],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+40],spec_visc[idx*66+40]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[40] = 
            _mm256_add_pd(spec_sum[40],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+40],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+41],spec_visc[idx*66+41]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[41] = 
            _mm256_add_pd(spec_sum[41],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+41],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+42],spec_visc[idx*66+42]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[42] = 
            _mm256_add_pd(spec_sum[42],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+42],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+43],spec_visc[idx*66+43]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[43] = 
            _mm256_add_pd(spec_sum[43],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+43],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+44],spec_visc[idx*66+44]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[44] = 
            _mm256_add_pd(spec_sum[44],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+44],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+45],spec_visc[idx*66+45]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[45] = 
            _mm256_add_pd(spec_sum[45],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+45],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+46],spec_visc[idx*66+46]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[46] = 
            _mm256_add_pd(spec_sum[46],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+46],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+47],spec_visc[idx*66+47]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[47] = 
            _mm256_add_pd(spec_sum[47],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+47],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+48],spec_visc[idx*66+48]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[48] = 
            _mm256_add_pd(spec_sum[48],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+48],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+49],spec_visc[idx*66+49]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[49] = 
            _mm256_add_pd(spec_sum[49],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+49],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+50],spec_visc[idx*66+50]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[50] = 
            _mm256_add_pd(spec_sum[50],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+50],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+51],spec_visc[idx*66+51]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[51] = 
            _mm256_add_pd(spec_sum[51],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+51],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+52],spec_visc[idx*66+52]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[52] = 
            _mm256_add_pd(spec_sum[52],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+52],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+53],spec_visc[idx*66+53]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[53] = 
            _mm256_add_pd(spec_sum[53],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+53],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+54],spec_visc[idx*66+54]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[54] = 
            _mm256_add_pd(spec_sum[54],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+54],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+55],spec_visc[idx*66+55]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[55] = 
            _mm256_add_pd(spec_sum[55],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+55],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+56],spec_visc[idx*66+56]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[56] = 
            _mm256_add_pd(spec_sum[56],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+56],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+57],spec_visc[idx*66+57]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[57] = 
            _mm256_add_pd(spec_sum[57],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+57],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+58],spec_visc[idx*66+58]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[58] = 
            _mm256_add_pd(spec_sum[58],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+58],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+59],spec_visc[idx*66+59]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[59] = 
            _mm256_add_pd(spec_sum[59],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+59],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+60],spec_visc[idx*66+60]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[60] = 
            _mm256_add_pd(spec_sum[60],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+60],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+61],spec_visc[idx*66+61]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[61] = 
            _mm256_add_pd(spec_sum[61],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+61],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+62],spec_visc[idx*66+62]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[62] = 
            _mm256_add_pd(spec_sum[62],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+62],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+63],spec_visc[idx*66+63]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[63] = 
            _mm256_add_pd(spec_sum[63],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+63],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+64],spec_visc[idx*66+64]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[64] = 
            _mm256_add_pd(spec_sum[64],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+64],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+65],spec_visc[idx*66+65]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[65] = 
            _mm256_add_pd(spec_sum[65],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+65],numer),numer),c2)); 
        }
      }
      result_vec[0] = 
        _mm256_add_pd(result_vec[0],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+0],spec_visc[spec_idx*66+0]),spec_sum[0])); 
      result_vec[1] = 
        _mm256_add_pd(result_vec[1],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+1],spec_visc[spec_idx*66+1]),spec_sum[1])); 
      result_vec[2] = 
        _mm256_add_pd(result_vec[2],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+2],spec_visc[spec_idx*66+2]),spec_sum[2])); 
      result_vec[3] = 
        _mm256_add_pd(result_vec[3],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+3],spec_visc[spec_idx*66+3]),spec_sum[3])); 
      result_vec[4] = 
        _mm256_add_pd(result_vec[4],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+4],spec_visc[spec_idx*66+4]),spec_sum[4])); 
      result_vec[5] = 
        _mm256_add_pd(result_vec[5],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+5],spec_visc[spec_idx*66+5]),spec_sum[5])); 
      result_vec[6] = 
        _mm256_add_pd(result_vec[6],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+6],spec_visc[spec_idx*66+6]),spec_sum[6])); 
      result_vec[7] = 
        _mm256_add_pd(result_vec[7],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+7],spec_visc[spec_idx*66+7]),spec_sum[7])); 
      result_vec[8] = 
        _mm256_add_pd(result_vec[8],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+8],spec_visc[spec_idx*66+8]),spec_sum[8])); 
      result_vec[9] = 
        _mm256_add_pd(result_vec[9],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+9],spec_visc[spec_idx*66+9]),spec_sum[9])); 
      result_vec[10] = 
        _mm256_add_pd(result_vec[10],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+10],spec_visc[spec_idx*66+10]),spec_sum[10])); 
      result_vec[11] = 
        _mm256_add_pd(result_vec[11],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+11],spec_visc[spec_idx*66+11]),spec_sum[11])); 
      result_vec[12] = 
        _mm256_add_pd(result_vec[12],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+12],spec_visc[spec_idx*66+12]),spec_sum[12])); 
      result_vec[13] = 
        _mm256_add_pd(result_vec[13],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+13],spec_visc[spec_idx*66+13]),spec_sum[13])); 
      result_vec[14] = 
        _mm256_add_pd(result_vec[14],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+14],spec_visc[spec_idx*66+14]),spec_sum[14])); 
      result_vec[15] = 
        _mm256_add_pd(result_vec[15],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+15],spec_visc[spec_idx*66+15]),spec_sum[15])); 
      result_vec[16] = 
        _mm256_add_pd(result_vec[16],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+16],spec_visc[spec_idx*66+16]),spec_sum[16])); 
      result_vec[17] = 
        _mm256_add_pd(result_vec[17],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+17],spec_visc[spec_idx*66+17]),spec_sum[17])); 
      result_vec[18] = 
        _mm256_add_pd(result_vec[18],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+18],spec_visc[spec_idx*66+18]),spec_sum[18])); 
      result_vec[19] = 
        _mm256_add_pd(result_vec[19],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+19],spec_visc[spec_idx*66+19]),spec_sum[19])); 
      result_vec[20] = 
        _mm256_add_pd(result_vec[20],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+20],spec_visc[spec_idx*66+20]),spec_sum[20])); 
      result_vec[21] = 
        _mm256_add_pd(result_vec[21],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+21],spec_visc[spec_idx*66+21]),spec_sum[21])); 
      result_vec[22] = 
        _mm256_add_pd(result_vec[22],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+22],spec_visc[spec_idx*66+22]),spec_sum[22])); 
      result_vec[23] = 
        _mm256_add_pd(result_vec[23],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+23],spec_visc[spec_idx*66+23]),spec_sum[23])); 
      result_vec[24] = 
        _mm256_add_pd(result_vec[24],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+24],spec_visc[spec_idx*66+24]),spec_sum[24])); 
      result_vec[25] = 
        _mm256_add_pd(result_vec[25],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+25],spec_visc[spec_idx*66+25]),spec_sum[25])); 
      result_vec[26] = 
        _mm256_add_pd(result_vec[26],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+26],spec_visc[spec_idx*66+26]),spec_sum[26])); 
      result_vec[27] = 
        _mm256_add_pd(result_vec[27],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+27],spec_visc[spec_idx*66+27]),spec_sum[27])); 
      result_vec[28] = 
        _mm256_add_pd(result_vec[28],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+28],spec_visc[spec_idx*66+28]),spec_sum[28])); 
      result_vec[29] = 
        _mm256_add_pd(result_vec[29],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+29],spec_visc[spec_idx*66+29]),spec_sum[29])); 
      result_vec[30] = 
        _mm256_add_pd(result_vec[30],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+30],spec_visc[spec_idx*66+30]),spec_sum[30])); 
      result_vec[31] = 
        _mm256_add_pd(result_vec[31],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+31],spec_visc[spec_idx*66+31]),spec_sum[31])); 
      result_vec[32] = 
        _mm256_add_pd(result_vec[32],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+32],spec_visc[spec_idx*66+32]),spec_sum[32])); 
      result_vec[33] = 
        _mm256_add_pd(result_vec[33],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+33],spec_visc[spec_idx*66+33]),spec_sum[33])); 
      result_vec[34] = 
        _mm256_add_pd(result_vec[34],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+34],spec_visc[spec_idx*66+34]),spec_sum[34])); 
      result_vec[35] = 
        _mm256_add_pd(result_vec[35],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+35],spec_visc[spec_idx*66+35]),spec_sum[35])); 
      result_vec[36] = 
        _mm256_add_pd(result_vec[36],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+36],spec_visc[spec_idx*66+36]),spec_sum[36])); 
      result_vec[37] = 
        _mm256_add_pd(result_vec[37],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+37],spec_visc[spec_idx*66+37]),spec_sum[37])); 
      result_vec[38] = 
        _mm256_add_pd(result_vec[38],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+38],spec_visc[spec_idx*66+38]),spec_sum[38])); 
      result_vec[39] = 
        _mm256_add_pd(result_vec[39],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+39],spec_visc[spec_idx*66+39]),spec_sum[39])); 
      result_vec[40] = 
        _mm256_add_pd(result_vec[40],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+40],spec_visc[spec_idx*66+40]),spec_sum[40])); 
      result_vec[41] = 
        _mm256_add_pd(result_vec[41],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+41],spec_visc[spec_idx*66+41]),spec_sum[41])); 
      result_vec[42] = 
        _mm256_add_pd(result_vec[42],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+42],spec_visc[spec_idx*66+42]),spec_sum[42])); 
      result_vec[43] = 
        _mm256_add_pd(result_vec[43],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+43],spec_visc[spec_idx*66+43]),spec_sum[43])); 
      result_vec[44] = 
        _mm256_add_pd(result_vec[44],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+44],spec_visc[spec_idx*66+44]),spec_sum[44])); 
      result_vec[45] = 
        _mm256_add_pd(result_vec[45],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+45],spec_visc[spec_idx*66+45]),spec_sum[45])); 
      result_vec[46] = 
        _mm256_add_pd(result_vec[46],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+46],spec_visc[spec_idx*66+46]),spec_sum[46])); 
      result_vec[47] = 
        _mm256_add_pd(result_vec[47],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+47],spec_visc[spec_idx*66+47]),spec_sum[47])); 
      result_vec[48] = 
        _mm256_add_pd(result_vec[48],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+48],spec_visc[spec_idx*66+48]),spec_sum[48])); 
      result_vec[49] = 
        _mm256_add_pd(result_vec[49],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+49],spec_visc[spec_idx*66+49]),spec_sum[49])); 
      result_vec[50] = 
        _mm256_add_pd(result_vec[50],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+50],spec_visc[spec_idx*66+50]),spec_sum[50])); 
      result_vec[51] = 
        _mm256_add_pd(result_vec[51],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+51],spec_visc[spec_idx*66+51]),spec_sum[51])); 
      result_vec[52] = 
        _mm256_add_pd(result_vec[52],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+52],spec_visc[spec_idx*66+52]),spec_sum[52])); 
      result_vec[53] = 
        _mm256_add_pd(result_vec[53],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+53],spec_visc[spec_idx*66+53]),spec_sum[53])); 
      result_vec[54] = 
        _mm256_add_pd(result_vec[54],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+54],spec_visc[spec_idx*66+54]),spec_sum[54])); 
      result_vec[55] = 
        _mm256_add_pd(result_vec[55],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+55],spec_visc[spec_idx*66+55]),spec_sum[55])); 
      result_vec[56] = 
        _mm256_add_pd(result_vec[56],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+56],spec_visc[spec_idx*66+56]),spec_sum[56])); 
      result_vec[57] = 
        _mm256_add_pd(result_vec[57],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+57],spec_visc[spec_idx*66+57]),spec_sum[57])); 
      result_vec[58] = 
        _mm256_add_pd(result_vec[58],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+58],spec_visc[spec_idx*66+58]),spec_sum[58])); 
      result_vec[59] = 
        _mm256_add_pd(result_vec[59],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+59],spec_visc[spec_idx*66+59]),spec_sum[59])); 
      result_vec[60] = 
        _mm256_add_pd(result_vec[60],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+60],spec_visc[spec_idx*66+60]),spec_sum[60])); 
      result_vec[61] = 
        _mm256_add_pd(result_vec[61],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+61],spec_visc[spec_idx*66+61]),spec_sum[61])); 
      result_vec[62] = 
        _mm256_add_pd(result_vec[62],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+62],spec_visc[spec_idx*66+62]),spec_sum[62])); 
      result_vec[63] = 
        _mm256_add_pd(result_vec[63],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+63],spec_visc[spec_idx*66+63]),spec_sum[63])); 
      result_vec[64] = 
        _mm256_add_pd(result_vec[64],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+64],spec_visc[spec_idx*66+64]),spec_sum[64])); 
      result_vec[65] = 
        _mm256_add_pd(result_vec[65],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+65],spec_visc[spec_idx*66+65]),spec_sum[65])); 
    }
    if (aligned<32>(viscosity))
    {
      for (unsigned idx = 0; idx < 66; idx++)
      {
        result_vec[idx] = 
          _mm256_mul_pd(_mm256_set1_pd(0.1535268500652559),result_vec[idx]); 
        _mm256_stream_pd(viscosity,result_vec[idx]);
        viscosity += 4;
      }
    }
    else
    {
      for (unsigned idx = 0; idx < 66; idx++)
      {
        result_vec[idx] = 
          _mm256_mul_pd(_mm256_set1_pd(0.1535268500652559),result_vec[idx]); 
        _mm256_storeu_pd(viscosity,result_vec[idx]);
        viscosity += 4;
      }
    }
    remaining_elmts -= 264;
    mass_frac_array += 264;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 4) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d temp;
      if (aligned<32>(temperature_array))
        temp = _mm256_load_pd(temperature_array);
      else
        temp = _mm256_loadu_pd(temperature_array);
      temperature_array += 4;
      if (aligned<32>(mass_frac_array))
      {
        mole_frac[0+idx] = 
          _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        mole_frac[66+idx] = 
          _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        mole_frac[132+idx] = 
          _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        mole_frac[198+idx] = 
          _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        mole_frac[264+idx] = 
          _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        mole_frac[330+idx] = 
          _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        mole_frac[396+idx] = 
          _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        mole_frac[462+idx] = 
          _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        mole_frac[528+idx] = 
          _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        mole_frac[594+idx] = 
          _mm256_load_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        mole_frac[660+idx] = 
          _mm256_load_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        mole_frac[726+idx] = 
          _mm256_load_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        mole_frac[792+idx] = 
          _mm256_load_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        mole_frac[858+idx] = 
          _mm256_load_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        mole_frac[924+idx] = 
          _mm256_load_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        mole_frac[990+idx] = 
          _mm256_load_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        mole_frac[1056+idx] = 
          _mm256_load_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        mole_frac[1122+idx] = 
          _mm256_load_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        mole_frac[1188+idx] = 
          _mm256_load_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        mole_frac[1254+idx] = 
          _mm256_load_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        mole_frac[1320+idx] = 
          _mm256_load_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        mole_frac[1386+idx] = 
          _mm256_load_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        mole_frac[1452+idx] = 
          _mm256_load_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        mole_frac[1518+idx] = 
          _mm256_load_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        mole_frac[1584+idx] = 
          _mm256_load_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        mole_frac[1650+idx] = 
          _mm256_load_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        mole_frac[1716+idx] = 
          _mm256_load_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        mole_frac[1782+idx] = 
          _mm256_load_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        mole_frac[1848+idx] = 
          _mm256_load_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        mole_frac[1914+idx] = 
          _mm256_load_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
      }
      else
      {
        mole_frac[1980+idx] = 
          _mm256_loadu_pd(mass_frac_array+(30*spec_stride)+(idx<<2)); 
        mole_frac[2046+idx] = 
          _mm256_loadu_pd(mass_frac_array+(31*spec_stride)+(idx<<2)); 
        mole_frac[2112+idx] = 
          _mm256_loadu_pd(mass_frac_array+(32*spec_stride)+(idx<<2)); 
        mole_frac[2178+idx] = 
          _mm256_loadu_pd(mass_frac_array+(33*spec_stride)+(idx<<2)); 
        mole_frac[2244+idx] = 
          _mm256_loadu_pd(mass_frac_array+(34*spec_stride)+(idx<<2)); 
        mole_frac[2310+idx] = 
          _mm256_loadu_pd(mass_frac_array+(35*spec_stride)+(idx<<2)); 
        mole_frac[2376+idx] = 
          _mm256_loadu_pd(mass_frac_array+(36*spec_stride)+(idx<<2)); 
        mole_frac[2442+idx] = 
          _mm256_loadu_pd(mass_frac_array+(37*spec_stride)+(idx<<2)); 
        mole_frac[2508+idx] = 
          _mm256_loadu_pd(mass_frac_array+(38*spec_stride)+(idx<<2)); 
        mole_frac[2574+idx] = 
          _mm256_loadu_pd(mass_frac_array+(39*spec_stride)+(idx<<2)); 
        mole_frac[2640+idx] = 
          _mm256_loadu_pd(mass_frac_array+(40*spec_stride)+(idx<<2)); 
        mole_frac[2706+idx] = 
          _mm256_loadu_pd(mass_frac_array+(41*spec_stride)+(idx<<2)); 
        mole_frac[2772+idx] = 
          _mm256_loadu_pd(mass_frac_array+(42*spec_stride)+(idx<<2)); 
        mole_frac[2838+idx] = 
          _mm256_loadu_pd(mass_frac_array+(43*spec_stride)+(idx<<2)); 
        mole_frac[2904+idx] = 
          _mm256_loadu_pd(mass_frac_array+(44*spec_stride)+(idx<<2)); 
        mole_frac[2970+idx] = 
          _mm256_loadu_pd(mass_frac_array+(45*spec_stride)+(idx<<2)); 
        mole_frac[3036+idx] = 
          _mm256_loadu_pd(mass_frac_array+(46*spec_stride)+(idx<<2)); 
        mole_frac[3102+idx] = 
          _mm256_loadu_pd(mass_frac_array+(47*spec_stride)+(idx<<2)); 
        mole_frac[3168+idx] = 
          _mm256_loadu_pd(mass_frac_array+(48*spec_stride)+(idx<<2)); 
        mole_frac[3234+idx] = 
          _mm256_loadu_pd(mass_frac_array+(49*spec_stride)+(idx<<2)); 
        mole_frac[3300+idx] = 
          _mm256_loadu_pd(mass_frac_array+(50*spec_stride)+(idx<<2)); 
        mole_frac[3366+idx] = 
          _mm256_loadu_pd(mass_frac_array+(51*spec_stride)+(idx<<2)); 
        mole_frac[3432+idx] = 
          _mm256_loadu_pd(mass_frac_array+(52*spec_stride)+(idx<<2)); 
        mole_frac[3498+idx] = 
          _mm256_loadu_pd(mass_frac_array+(53*spec_stride)+(idx<<2)); 
        mole_frac[3564+idx] = 
          _mm256_loadu_pd(mass_frac_array+(54*spec_stride)+(idx<<2)); 
        mole_frac[3630+idx] = 
          _mm256_loadu_pd(mass_frac_array+(55*spec_stride)+(idx<<2)); 
        mole_frac[3696+idx] = 
          _mm256_loadu_pd(mass_frac_array+(56*spec_stride)+(idx<<2)); 
        mole_frac[3762+idx] = 
          _mm256_loadu_pd(mass_frac_array+(57*spec_stride)+(idx<<2)); 
        mole_frac[3828+idx] = 
          _mm256_loadu_pd(mass_frac_array+(58*spec_stride)+(idx<<2)); 
        mole_frac[3894+idx] = 
          _mm256_loadu_pd(mass_frac_array+(59*spec_stride)+(idx<<2)); 
      }
      __m256d mixmw;
      if (aligned<32>(mixmw_array))
        mixmw = _mm256_load_pd(mixmw_array);
      else
        mixmw = _mm256_loadu_pd(mixmw_array);
      mixmw_array += 4;
      result_vec[idx] = _mm256_set1_pd(0.0);
      temp = _mm256_mul_pd(temp,_mm256_set1_pd(120.0));
      __m128d lower = _mm256_extractf128_pd(temp,0);
      __m128d upper = _mm256_extractf128_pd(temp,1);
      __m256d logt = 
        _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      // Species H
      {
        __m256d val = _mm256_set1_pd(0.01757076575883849);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.3980310503714805)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.652696513182246)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-20.40535459028184)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[0+idx] = val;
      }
      // Species H2
      {
        __m256d val = _mm256_set1_pd(2.33100139953291e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.05016056447174558)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.003492096616715)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-13.84035618317203)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[66+idx] = val;
      }
      // Species CH3
      {
        __m256d val = _mm256_set1_pd(0.01745292662633083);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.3952264471530641)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.630413752559152)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-20.22834639799518)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[132+idx] = val;
      }
      // Species O
      {
        __m256d val = _mm256_set1_pd(7.841447484966627e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.173865132716423)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.929020467011314)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-15.10026747364014)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[198+idx] = val;
      }
      // Species CH4
      {
        __m256d val = _mm256_set1_pd(0.01712465091640445);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.3874927847672007)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.569547094857978)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-20.00458513330756)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[264+idx] = val;
      }
      // Species OH
      {
        __m256d val = _mm256_set1_pd(7.841447484966898e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.1738651327164285)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.929020467011351)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-15.06971969464791)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[330+idx] = val;
      }
      // Species H2O
      {
        __m256d val = _mm256_set1_pd(-0.05045537005542979);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(1.031781480707904)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-5.987902336937527)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(0.7009388163509245)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[396+idx] = val;
      }
      // Species C2H2
      {
        __m256d val = _mm256_set1_pd(0.02486206221740027);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.587852927349057)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.290034479082859)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.74505541054281)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[462+idx] = val;
      }
      // Species CO
      {
        __m256d val = _mm256_set1_pd(0.01054816060102988);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.235770758410507)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.400968097030088)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.61559645415615)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[528+idx] = val;
      }
      // Species C2H4
      {
        __m256d val = _mm256_set1_pd(0.02436925155378749);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5700815021202054)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.100704137488538)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-23.93711926625748)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[594+idx] = val;
      }
      // Species C2H5
      {
        __m256d val = _mm256_set1_pd(0.02441371059443401);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5734517595536173)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.145721907003817)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.529504601817)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[660+idx] = val;
      }
      // Species CH2O
      {
        __m256d val = _mm256_set1_pd(3.212240006873408e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.167235142784101)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.703808872626219)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-19.85295314006834)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[726+idx] = val;
      }
      // Species C2H6
      {
        __m256d val = _mm256_set1_pd(0.02441371059443357);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5734517595536081)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.145721907003752)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.51245692810857)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[792+idx] = val;
      }
      // Species CH3O
      {
        __m256d val = _mm256_set1_pd(-0.023618697767246);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(0.4069654657503018)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-1.384947591294291)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-10.10494407684299)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[858+idx] = val;
      }
      // Species O2
      {
        __m256d val = _mm256_set1_pd(0.01214171417829216);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.272158843610373)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.678085479588637)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-17.15808369877082)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[924+idx] = val;
      }
      // Species HO2
      {
        __m256d val = _mm256_set1_pd(0.01214171417829254);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.272158843610381)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.678085479588692)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-17.14257655384877)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[990+idx] = val;
      }
      // Species H2O2
      {
        __m256d val = _mm256_set1_pd(0.01214171417829194);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.272158843610368)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.6780854795886)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-17.12753591977842)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1056+idx] = val;
      }
      // Species CO2
      {
        __m256d val = _mm256_set1_pd(0.02440890122032096);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5724287559128219)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.130428117775916)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-23.97057723451212)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1122+idx] = val;
      }
      // Species CH3HCO
      {
        __m256d val = _mm256_set1_pd(0.01173153385928102);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.3419575514452081)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.866549994604684)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-22.31996918351742)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1188+idx] = val;
      }
      // Species HCOOH
      {
        __m256d val = _mm256_set1_pd(7.235675392407266e-03);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.2503679239288357)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(3.262693232699687)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-21.25097866061817)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1254+idx] = val;
      }
      // Species CH3OCH3
      {
        __m256d val = _mm256_set1_pd(0.02237582504898801);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107528)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254609)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-25.02944655668421)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1320+idx] = val;
      }
      // Species CH3OCO
      {
        __m256d val = _mm256_set1_pd(0.01536289690465918);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.4150999855999652)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(4.341454112797969)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-23.48003905216325)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1386+idx] = val;
      }
      // Species CH3OCHO
      {
        __m256d val = _mm256_set1_pd(0.0153628969046591);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.4150999855999636)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(4.341454112797959)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-23.47157548306755)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1452+idx] = val;
      }
      // Species CH3OCH2OH
      {
        __m256d val = _mm256_set1_pd(0.02237582504898794);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107515)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254599)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.880399646385)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1518+idx] = val;
      }
      // Species OCH2OCHO
      {
        __m256d val = _mm256_set1_pd(0.02237582504898696);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107304)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254451)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.78548230085281)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1584+idx] = val;
      }
      // Species HOCH2OCO
      {
        __m256d val = _mm256_set1_pd(0.02237582504898696);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107304)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254451)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.78548230085281)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1650+idx] = val;
      }
      // Species CH3OCH2O2
      {
        __m256d val = _mm256_set1_pd(0.02237582504898747);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107413)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254526)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.77222789611538)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1716+idx] = val;
      }
      // Species HO2CH2OCHO
      {
        __m256d val = _mm256_set1_pd(0.02237582504898723);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.550061453310736)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254486)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.68334671426243)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1782+idx] = val;
      }
      // Species O2CH2OCH2O2H
      {
        __m256d val = _mm256_set1_pd(0.02237582504898733);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.5500614533107383)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(5.159442761254504)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-24.59857713887057)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1848+idx] = val;
      }
      // Species N2
      {
        __m256d val = _mm256_set1_pd(0.01047722299820579);
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-0.2341198015382627)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(2.388160005867027)); 
        val = 
          _mm256_add_pd(_mm256_mul_pd(val,logt),_mm256_set1_pd(-16.56562055017989)); 
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        spec_visc[1914+idx] = val;
      }
      mole_frac[0+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[66+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[66+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[132+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[132+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[198+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[198+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[264+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[264+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[330+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[330+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[396+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[396+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[462+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[462+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[528+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[528+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[594+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[594+idx],_mm256_set1_pd(recip_molecular_masses[9])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[660+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[660+idx],_mm256_set1_pd(recip_molecular_masses[10])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[726+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[726+idx],_mm256_set1_pd(recip_molecular_masses[11])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[792+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[792+idx],_mm256_set1_pd(recip_molecular_masses[12])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[858+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[858+idx],_mm256_set1_pd(recip_molecular_masses[13])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[924+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[924+idx],_mm256_set1_pd(recip_molecular_masses[14])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[990+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[990+idx],_mm256_set1_pd(recip_molecular_masses[15])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1056+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1056+idx],_mm256_set1_pd(recip_molecular_masses[16])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1122+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1122+idx],_mm256_set1_pd(recip_molecular_masses[17])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1188+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1188+idx],_mm256_set1_pd(recip_molecular_masses[18])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1254+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1254+idx],_mm256_set1_pd(recip_molecular_masses[19])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1320+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1320+idx],_mm256_set1_pd(recip_molecular_masses[20])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1386+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1386+idx],_mm256_set1_pd(recip_molecular_masses[21])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1452+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1452+idx],_mm256_set1_pd(recip_molecular_masses[22])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1518+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1518+idx],_mm256_set1_pd(recip_molecular_masses[23])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1584+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1584+idx],_mm256_set1_pd(recip_molecular_masses[24])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1650+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1650+idx],_mm256_set1_pd(recip_molecular_masses[25])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1716+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1716+idx],_mm256_set1_pd(recip_molecular_masses[26])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1782+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1782+idx],_mm256_set1_pd(recip_molecular_masses[27])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1848+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1848+idx],_mm256_set1_pd(recip_molecular_masses[28])),_mm256_set1_pd(1e3)),mixmw); 
      mole_frac[1914+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1914+idx],_mm256_set1_pd(recip_molecular_masses[29])),_mm256_set1_pd(1e3)),mixmw); 
    }
    int visc_idx = 0;
    for (int spec_idx = 0; spec_idx < 30; spec_idx++)
    {
      spec_sum[0] = _mm256_set1_pd(0.0);
      spec_sum[1] = _mm256_set1_pd(0.0);
      spec_sum[2] = _mm256_set1_pd(0.0);
      spec_sum[3] = _mm256_set1_pd(0.0);
      spec_sum[4] = _mm256_set1_pd(0.0);
      spec_sum[5] = _mm256_set1_pd(0.0);
      spec_sum[6] = _mm256_set1_pd(0.0);
      spec_sum[7] = _mm256_set1_pd(0.0);
      spec_sum[8] = _mm256_set1_pd(0.0);
      spec_sum[9] = _mm256_set1_pd(0.0);
      spec_sum[10] = _mm256_set1_pd(0.0);
      spec_sum[11] = _mm256_set1_pd(0.0);
      spec_sum[12] = _mm256_set1_pd(0.0);
      spec_sum[13] = _mm256_set1_pd(0.0);
      spec_sum[14] = _mm256_set1_pd(0.0);
      spec_sum[15] = _mm256_set1_pd(0.0);
      spec_sum[16] = _mm256_set1_pd(0.0);
      spec_sum[17] = _mm256_set1_pd(0.0);
      spec_sum[18] = _mm256_set1_pd(0.0);
      spec_sum[19] = _mm256_set1_pd(0.0);
      spec_sum[20] = _mm256_set1_pd(0.0);
      spec_sum[21] = _mm256_set1_pd(0.0);
      spec_sum[22] = _mm256_set1_pd(0.0);
      spec_sum[23] = _mm256_set1_pd(0.0);
      spec_sum[24] = _mm256_set1_pd(0.0);
      spec_sum[25] = _mm256_set1_pd(0.0);
      spec_sum[26] = _mm256_set1_pd(0.0);
      spec_sum[27] = _mm256_set1_pd(0.0);
      spec_sum[28] = _mm256_set1_pd(0.0);
      spec_sum[29] = _mm256_set1_pd(0.0);
      spec_sum[30] = _mm256_set1_pd(0.0);
      spec_sum[31] = _mm256_set1_pd(0.0);
      spec_sum[32] = _mm256_set1_pd(0.0);
      spec_sum[33] = _mm256_set1_pd(0.0);
      spec_sum[34] = _mm256_set1_pd(0.0);
      spec_sum[35] = _mm256_set1_pd(0.0);
      spec_sum[36] = _mm256_set1_pd(0.0);
      spec_sum[37] = _mm256_set1_pd(0.0);
      spec_sum[38] = _mm256_set1_pd(0.0);
      spec_sum[39] = _mm256_set1_pd(0.0);
      spec_sum[40] = _mm256_set1_pd(0.0);
      spec_sum[41] = _mm256_set1_pd(0.0);
      spec_sum[42] = _mm256_set1_pd(0.0);
      spec_sum[43] = _mm256_set1_pd(0.0);
      spec_sum[44] = _mm256_set1_pd(0.0);
      spec_sum[45] = _mm256_set1_pd(0.0);
      spec_sum[46] = _mm256_set1_pd(0.0);
      spec_sum[47] = _mm256_set1_pd(0.0);
      spec_sum[48] = _mm256_set1_pd(0.0);
      spec_sum[49] = _mm256_set1_pd(0.0);
      spec_sum[50] = _mm256_set1_pd(0.0);
      spec_sum[51] = _mm256_set1_pd(0.0);
      spec_sum[52] = _mm256_set1_pd(0.0);
      spec_sum[53] = _mm256_set1_pd(0.0);
      spec_sum[54] = _mm256_set1_pd(0.0);
      spec_sum[55] = _mm256_set1_pd(0.0);
      spec_sum[56] = _mm256_set1_pd(0.0);
      spec_sum[57] = _mm256_set1_pd(0.0);
      spec_sum[58] = _mm256_set1_pd(0.0);
      spec_sum[59] = _mm256_set1_pd(0.0);
      spec_sum[60] = _mm256_set1_pd(0.0);
      spec_sum[61] = _mm256_set1_pd(0.0);
      spec_sum[62] = _mm256_set1_pd(0.0);
      spec_sum[63] = _mm256_set1_pd(0.0);
      spec_sum[64] = _mm256_set1_pd(0.0);
      spec_sum[65] = _mm256_set1_pd(0.0);
      for (int idx = 0; idx < 30; idx++)
      {
        if (idx == spec_idx)
        {
          spec_sum[0] = 
            _mm256_add_pd(spec_sum[0],_mm256_mul_pd(mole_frac[idx*66+0],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[1] = 
            _mm256_add_pd(spec_sum[1],_mm256_mul_pd(mole_frac[idx*66+1],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[2] = 
            _mm256_add_pd(spec_sum[2],_mm256_mul_pd(mole_frac[idx*66+2],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[3] = 
            _mm256_add_pd(spec_sum[3],_mm256_mul_pd(mole_frac[idx*66+3],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[4] = 
            _mm256_add_pd(spec_sum[4],_mm256_mul_pd(mole_frac[idx*66+4],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[5] = 
            _mm256_add_pd(spec_sum[5],_mm256_mul_pd(mole_frac[idx*66+5],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[6] = 
            _mm256_add_pd(spec_sum[6],_mm256_mul_pd(mole_frac[idx*66+6],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[7] = 
            _mm256_add_pd(spec_sum[7],_mm256_mul_pd(mole_frac[idx*66+7],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[8] = 
            _mm256_add_pd(spec_sum[8],_mm256_mul_pd(mole_frac[idx*66+8],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[9] = 
            _mm256_add_pd(spec_sum[9],_mm256_mul_pd(mole_frac[idx*66+9],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[10] = 
            _mm256_add_pd(spec_sum[10],_mm256_mul_pd(mole_frac[idx*66+10],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[11] = 
            _mm256_add_pd(spec_sum[11],_mm256_mul_pd(mole_frac[idx*66+11],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[12] = 
            _mm256_add_pd(spec_sum[12],_mm256_mul_pd(mole_frac[idx*66+12],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[13] = 
            _mm256_add_pd(spec_sum[13],_mm256_mul_pd(mole_frac[idx*66+13],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[14] = 
            _mm256_add_pd(spec_sum[14],_mm256_mul_pd(mole_frac[idx*66+14],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[15] = 
            _mm256_add_pd(spec_sum[15],_mm256_mul_pd(mole_frac[idx*66+15],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[16] = 
            _mm256_add_pd(spec_sum[16],_mm256_mul_pd(mole_frac[idx*66+16],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[17] = 
            _mm256_add_pd(spec_sum[17],_mm256_mul_pd(mole_frac[idx*66+17],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[18] = 
            _mm256_add_pd(spec_sum[18],_mm256_mul_pd(mole_frac[idx*66+18],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[19] = 
            _mm256_add_pd(spec_sum[19],_mm256_mul_pd(mole_frac[idx*66+19],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[20] = 
            _mm256_add_pd(spec_sum[20],_mm256_mul_pd(mole_frac[idx*66+20],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[21] = 
            _mm256_add_pd(spec_sum[21],_mm256_mul_pd(mole_frac[idx*66+21],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[22] = 
            _mm256_add_pd(spec_sum[22],_mm256_mul_pd(mole_frac[idx*66+22],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[23] = 
            _mm256_add_pd(spec_sum[23],_mm256_mul_pd(mole_frac[idx*66+23],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[24] = 
            _mm256_add_pd(spec_sum[24],_mm256_mul_pd(mole_frac[idx*66+24],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[25] = 
            _mm256_add_pd(spec_sum[25],_mm256_mul_pd(mole_frac[idx*66+25],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[26] = 
            _mm256_add_pd(spec_sum[26],_mm256_mul_pd(mole_frac[idx*66+26],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[27] = 
            _mm256_add_pd(spec_sum[27],_mm256_mul_pd(mole_frac[idx*66+27],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[28] = 
            _mm256_add_pd(spec_sum[28],_mm256_mul_pd(mole_frac[idx*66+28],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[29] = 
            _mm256_add_pd(spec_sum[29],_mm256_mul_pd(mole_frac[idx*66+29],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[30] = 
            _mm256_add_pd(spec_sum[30],_mm256_mul_pd(mole_frac[idx*66+30],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[31] = 
            _mm256_add_pd(spec_sum[31],_mm256_mul_pd(mole_frac[idx*66+31],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[32] = 
            _mm256_add_pd(spec_sum[32],_mm256_mul_pd(mole_frac[idx*66+32],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[33] = 
            _mm256_add_pd(spec_sum[33],_mm256_mul_pd(mole_frac[idx*66+33],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[34] = 
            _mm256_add_pd(spec_sum[34],_mm256_mul_pd(mole_frac[idx*66+34],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[35] = 
            _mm256_add_pd(spec_sum[35],_mm256_mul_pd(mole_frac[idx*66+35],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[36] = 
            _mm256_add_pd(spec_sum[36],_mm256_mul_pd(mole_frac[idx*66+36],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[37] = 
            _mm256_add_pd(spec_sum[37],_mm256_mul_pd(mole_frac[idx*66+37],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[38] = 
            _mm256_add_pd(spec_sum[38],_mm256_mul_pd(mole_frac[idx*66+38],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[39] = 
            _mm256_add_pd(spec_sum[39],_mm256_mul_pd(mole_frac[idx*66+39],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[40] = 
            _mm256_add_pd(spec_sum[40],_mm256_mul_pd(mole_frac[idx*66+40],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[41] = 
            _mm256_add_pd(spec_sum[41],_mm256_mul_pd(mole_frac[idx*66+41],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[42] = 
            _mm256_add_pd(spec_sum[42],_mm256_mul_pd(mole_frac[idx*66+42],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[43] = 
            _mm256_add_pd(spec_sum[43],_mm256_mul_pd(mole_frac[idx*66+43],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[44] = 
            _mm256_add_pd(spec_sum[44],_mm256_mul_pd(mole_frac[idx*66+44],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[45] = 
            _mm256_add_pd(spec_sum[45],_mm256_mul_pd(mole_frac[idx*66+45],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[46] = 
            _mm256_add_pd(spec_sum[46],_mm256_mul_pd(mole_frac[idx*66+46],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[47] = 
            _mm256_add_pd(spec_sum[47],_mm256_mul_pd(mole_frac[idx*66+47],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[48] = 
            _mm256_add_pd(spec_sum[48],_mm256_mul_pd(mole_frac[idx*66+48],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[49] = 
            _mm256_add_pd(spec_sum[49],_mm256_mul_pd(mole_frac[idx*66+49],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[50] = 
            _mm256_add_pd(spec_sum[50],_mm256_mul_pd(mole_frac[idx*66+50],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[51] = 
            _mm256_add_pd(spec_sum[51],_mm256_mul_pd(mole_frac[idx*66+51],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[52] = 
            _mm256_add_pd(spec_sum[52],_mm256_mul_pd(mole_frac[idx*66+52],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[53] = 
            _mm256_add_pd(spec_sum[53],_mm256_mul_pd(mole_frac[idx*66+53],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[54] = 
            _mm256_add_pd(spec_sum[54],_mm256_mul_pd(mole_frac[idx*66+54],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[55] = 
            _mm256_add_pd(spec_sum[55],_mm256_mul_pd(mole_frac[idx*66+55],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[56] = 
            _mm256_add_pd(spec_sum[56],_mm256_mul_pd(mole_frac[idx*66+56],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[57] = 
            _mm256_add_pd(spec_sum[57],_mm256_mul_pd(mole_frac[idx*66+57],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[58] = 
            _mm256_add_pd(spec_sum[58],_mm256_mul_pd(mole_frac[idx*66+58],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[59] = 
            _mm256_add_pd(spec_sum[59],_mm256_mul_pd(mole_frac[idx*66+59],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[60] = 
            _mm256_add_pd(spec_sum[60],_mm256_mul_pd(mole_frac[idx*66+60],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[61] = 
            _mm256_add_pd(spec_sum[61],_mm256_mul_pd(mole_frac[idx*66+61],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[62] = 
            _mm256_add_pd(spec_sum[62],_mm256_mul_pd(mole_frac[idx*66+62],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[63] = 
            _mm256_add_pd(spec_sum[63],_mm256_mul_pd(mole_frac[idx*66+63],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[64] = 
            _mm256_add_pd(spec_sum[64],_mm256_mul_pd(mole_frac[idx*66+64],_mm256_set1_pd(2.82842712474619))); 
          spec_sum[65] = 
            _mm256_add_pd(spec_sum[65],_mm256_mul_pd(mole_frac[idx*66+65],_mm256_set1_pd(2.82842712474619))); 
        }
        else
        {
          __m256d c1 = _mm256_set1_pd(visc_constants[visc_idx++]);
          __m256d c2 = _mm256_set1_pd(visc_constants[visc_idx++]);
          __m256d numer;
          numer = _mm256_div_pd(spec_visc[spec_idx*66+0],spec_visc[idx*66+0]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[0] = 
            _mm256_add_pd(spec_sum[0],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+0],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+1],spec_visc[idx*66+1]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[1] = 
            _mm256_add_pd(spec_sum[1],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+1],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+2],spec_visc[idx*66+2]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[2] = 
            _mm256_add_pd(spec_sum[2],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+2],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+3],spec_visc[idx*66+3]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[3] = 
            _mm256_add_pd(spec_sum[3],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+3],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+4],spec_visc[idx*66+4]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[4] = 
            _mm256_add_pd(spec_sum[4],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+4],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+5],spec_visc[idx*66+5]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[5] = 
            _mm256_add_pd(spec_sum[5],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+5],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+6],spec_visc[idx*66+6]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[6] = 
            _mm256_add_pd(spec_sum[6],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+6],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+7],spec_visc[idx*66+7]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[7] = 
            _mm256_add_pd(spec_sum[7],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+7],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+8],spec_visc[idx*66+8]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[8] = 
            _mm256_add_pd(spec_sum[8],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+8],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+9],spec_visc[idx*66+9]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[9] = 
            _mm256_add_pd(spec_sum[9],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+9],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+10],spec_visc[idx*66+10]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[10] = 
            _mm256_add_pd(spec_sum[10],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+10],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+11],spec_visc[idx*66+11]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[11] = 
            _mm256_add_pd(spec_sum[11],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+11],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+12],spec_visc[idx*66+12]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[12] = 
            _mm256_add_pd(spec_sum[12],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+12],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+13],spec_visc[idx*66+13]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[13] = 
            _mm256_add_pd(spec_sum[13],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+13],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+14],spec_visc[idx*66+14]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[14] = 
            _mm256_add_pd(spec_sum[14],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+14],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+15],spec_visc[idx*66+15]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[15] = 
            _mm256_add_pd(spec_sum[15],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+15],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+16],spec_visc[idx*66+16]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[16] = 
            _mm256_add_pd(spec_sum[16],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+16],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+17],spec_visc[idx*66+17]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[17] = 
            _mm256_add_pd(spec_sum[17],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+17],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+18],spec_visc[idx*66+18]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[18] = 
            _mm256_add_pd(spec_sum[18],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+18],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+19],spec_visc[idx*66+19]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[19] = 
            _mm256_add_pd(spec_sum[19],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+19],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+20],spec_visc[idx*66+20]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[20] = 
            _mm256_add_pd(spec_sum[20],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+20],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+21],spec_visc[idx*66+21]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[21] = 
            _mm256_add_pd(spec_sum[21],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+21],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+22],spec_visc[idx*66+22]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[22] = 
            _mm256_add_pd(spec_sum[22],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+22],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+23],spec_visc[idx*66+23]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[23] = 
            _mm256_add_pd(spec_sum[23],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+23],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+24],spec_visc[idx*66+24]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[24] = 
            _mm256_add_pd(spec_sum[24],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+24],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+25],spec_visc[idx*66+25]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[25] = 
            _mm256_add_pd(spec_sum[25],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+25],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+26],spec_visc[idx*66+26]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[26] = 
            _mm256_add_pd(spec_sum[26],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+26],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+27],spec_visc[idx*66+27]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[27] = 
            _mm256_add_pd(spec_sum[27],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+27],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+28],spec_visc[idx*66+28]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[28] = 
            _mm256_add_pd(spec_sum[28],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+28],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+29],spec_visc[idx*66+29]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[29] = 
            _mm256_add_pd(spec_sum[29],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+29],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+30],spec_visc[idx*66+30]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[30] = 
            _mm256_add_pd(spec_sum[30],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+30],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+31],spec_visc[idx*66+31]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[31] = 
            _mm256_add_pd(spec_sum[31],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+31],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+32],spec_visc[idx*66+32]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[32] = 
            _mm256_add_pd(spec_sum[32],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+32],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+33],spec_visc[idx*66+33]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[33] = 
            _mm256_add_pd(spec_sum[33],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+33],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+34],spec_visc[idx*66+34]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[34] = 
            _mm256_add_pd(spec_sum[34],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+34],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+35],spec_visc[idx*66+35]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[35] = 
            _mm256_add_pd(spec_sum[35],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+35],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+36],spec_visc[idx*66+36]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[36] = 
            _mm256_add_pd(spec_sum[36],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+36],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+37],spec_visc[idx*66+37]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[37] = 
            _mm256_add_pd(spec_sum[37],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+37],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+38],spec_visc[idx*66+38]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[38] = 
            _mm256_add_pd(spec_sum[38],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+38],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+39],spec_visc[idx*66+39]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[39] = 
            _mm256_add_pd(spec_sum[39],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+39],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+40],spec_visc[idx*66+40]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[40] = 
            _mm256_add_pd(spec_sum[40],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+40],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+41],spec_visc[idx*66+41]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[41] = 
            _mm256_add_pd(spec_sum[41],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+41],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+42],spec_visc[idx*66+42]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[42] = 
            _mm256_add_pd(spec_sum[42],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+42],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+43],spec_visc[idx*66+43]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[43] = 
            _mm256_add_pd(spec_sum[43],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+43],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+44],spec_visc[idx*66+44]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[44] = 
            _mm256_add_pd(spec_sum[44],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+44],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+45],spec_visc[idx*66+45]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[45] = 
            _mm256_add_pd(spec_sum[45],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+45],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+46],spec_visc[idx*66+46]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[46] = 
            _mm256_add_pd(spec_sum[46],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+46],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+47],spec_visc[idx*66+47]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[47] = 
            _mm256_add_pd(spec_sum[47],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+47],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+48],spec_visc[idx*66+48]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[48] = 
            _mm256_add_pd(spec_sum[48],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+48],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+49],spec_visc[idx*66+49]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[49] = 
            _mm256_add_pd(spec_sum[49],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+49],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+50],spec_visc[idx*66+50]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[50] = 
            _mm256_add_pd(spec_sum[50],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+50],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+51],spec_visc[idx*66+51]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[51] = 
            _mm256_add_pd(spec_sum[51],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+51],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+52],spec_visc[idx*66+52]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[52] = 
            _mm256_add_pd(spec_sum[52],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+52],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+53],spec_visc[idx*66+53]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[53] = 
            _mm256_add_pd(spec_sum[53],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+53],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+54],spec_visc[idx*66+54]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[54] = 
            _mm256_add_pd(spec_sum[54],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+54],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+55],spec_visc[idx*66+55]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[55] = 
            _mm256_add_pd(spec_sum[55],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+55],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+56],spec_visc[idx*66+56]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[56] = 
            _mm256_add_pd(spec_sum[56],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+56],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+57],spec_visc[idx*66+57]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[57] = 
            _mm256_add_pd(spec_sum[57],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+57],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+58],spec_visc[idx*66+58]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[58] = 
            _mm256_add_pd(spec_sum[58],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+58],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+59],spec_visc[idx*66+59]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[59] = 
            _mm256_add_pd(spec_sum[59],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+59],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+60],spec_visc[idx*66+60]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[60] = 
            _mm256_add_pd(spec_sum[60],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+60],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+61],spec_visc[idx*66+61]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[61] = 
            _mm256_add_pd(spec_sum[61],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+61],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+62],spec_visc[idx*66+62]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[62] = 
            _mm256_add_pd(spec_sum[62],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+62],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+63],spec_visc[idx*66+63]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[63] = 
            _mm256_add_pd(spec_sum[63],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+63],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+64],spec_visc[idx*66+64]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[64] = 
            _mm256_add_pd(spec_sum[64],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+64],numer),numer),c2)); 
          numer = _mm256_div_pd(spec_visc[spec_idx*66+65],spec_visc[idx*66+65]);
          numer = _mm256_mul_pd(c1,numer);
          numer = _mm256_add_pd(_mm256_set1_pd(1.0),_mm256_sqrt_pd(numer));
          spec_sum[65] = 
            _mm256_add_pd(spec_sum[65],_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[idx*66+65],numer),numer),c2)); 
        }
      }
      result_vec[0] = 
        _mm256_add_pd(result_vec[0],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+0],spec_visc[spec_idx*66+0]),spec_sum[0])); 
      result_vec[1] = 
        _mm256_add_pd(result_vec[1],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+1],spec_visc[spec_idx*66+1]),spec_sum[1])); 
      result_vec[2] = 
        _mm256_add_pd(result_vec[2],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+2],spec_visc[spec_idx*66+2]),spec_sum[2])); 
      result_vec[3] = 
        _mm256_add_pd(result_vec[3],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+3],spec_visc[spec_idx*66+3]),spec_sum[3])); 
      result_vec[4] = 
        _mm256_add_pd(result_vec[4],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+4],spec_visc[spec_idx*66+4]),spec_sum[4])); 
      result_vec[5] = 
        _mm256_add_pd(result_vec[5],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+5],spec_visc[spec_idx*66+5]),spec_sum[5])); 
      result_vec[6] = 
        _mm256_add_pd(result_vec[6],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+6],spec_visc[spec_idx*66+6]),spec_sum[6])); 
      result_vec[7] = 
        _mm256_add_pd(result_vec[7],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+7],spec_visc[spec_idx*66+7]),spec_sum[7])); 
      result_vec[8] = 
        _mm256_add_pd(result_vec[8],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+8],spec_visc[spec_idx*66+8]),spec_sum[8])); 
      result_vec[9] = 
        _mm256_add_pd(result_vec[9],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+9],spec_visc[spec_idx*66+9]),spec_sum[9])); 
      result_vec[10] = 
        _mm256_add_pd(result_vec[10],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+10],spec_visc[spec_idx*66+10]),spec_sum[10])); 
      result_vec[11] = 
        _mm256_add_pd(result_vec[11],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+11],spec_visc[spec_idx*66+11]),spec_sum[11])); 
      result_vec[12] = 
        _mm256_add_pd(result_vec[12],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+12],spec_visc[spec_idx*66+12]),spec_sum[12])); 
      result_vec[13] = 
        _mm256_add_pd(result_vec[13],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+13],spec_visc[spec_idx*66+13]),spec_sum[13])); 
      result_vec[14] = 
        _mm256_add_pd(result_vec[14],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+14],spec_visc[spec_idx*66+14]),spec_sum[14])); 
      result_vec[15] = 
        _mm256_add_pd(result_vec[15],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+15],spec_visc[spec_idx*66+15]),spec_sum[15])); 
      result_vec[16] = 
        _mm256_add_pd(result_vec[16],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+16],spec_visc[spec_idx*66+16]),spec_sum[16])); 
      result_vec[17] = 
        _mm256_add_pd(result_vec[17],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+17],spec_visc[spec_idx*66+17]),spec_sum[17])); 
      result_vec[18] = 
        _mm256_add_pd(result_vec[18],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+18],spec_visc[spec_idx*66+18]),spec_sum[18])); 
      result_vec[19] = 
        _mm256_add_pd(result_vec[19],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+19],spec_visc[spec_idx*66+19]),spec_sum[19])); 
      result_vec[20] = 
        _mm256_add_pd(result_vec[20],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+20],spec_visc[spec_idx*66+20]),spec_sum[20])); 
      result_vec[21] = 
        _mm256_add_pd(result_vec[21],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+21],spec_visc[spec_idx*66+21]),spec_sum[21])); 
      result_vec[22] = 
        _mm256_add_pd(result_vec[22],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+22],spec_visc[spec_idx*66+22]),spec_sum[22])); 
      result_vec[23] = 
        _mm256_add_pd(result_vec[23],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+23],spec_visc[spec_idx*66+23]),spec_sum[23])); 
      result_vec[24] = 
        _mm256_add_pd(result_vec[24],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+24],spec_visc[spec_idx*66+24]),spec_sum[24])); 
      result_vec[25] = 
        _mm256_add_pd(result_vec[25],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+25],spec_visc[spec_idx*66+25]),spec_sum[25])); 
      result_vec[26] = 
        _mm256_add_pd(result_vec[26],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+26],spec_visc[spec_idx*66+26]),spec_sum[26])); 
      result_vec[27] = 
        _mm256_add_pd(result_vec[27],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+27],spec_visc[spec_idx*66+27]),spec_sum[27])); 
      result_vec[28] = 
        _mm256_add_pd(result_vec[28],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+28],spec_visc[spec_idx*66+28]),spec_sum[28])); 
      result_vec[29] = 
        _mm256_add_pd(result_vec[29],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+29],spec_visc[spec_idx*66+29]),spec_sum[29])); 
      result_vec[30] = 
        _mm256_add_pd(result_vec[30],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+30],spec_visc[spec_idx*66+30]),spec_sum[30])); 
      result_vec[31] = 
        _mm256_add_pd(result_vec[31],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+31],spec_visc[spec_idx*66+31]),spec_sum[31])); 
      result_vec[32] = 
        _mm256_add_pd(result_vec[32],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+32],spec_visc[spec_idx*66+32]),spec_sum[32])); 
      result_vec[33] = 
        _mm256_add_pd(result_vec[33],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+33],spec_visc[spec_idx*66+33]),spec_sum[33])); 
      result_vec[34] = 
        _mm256_add_pd(result_vec[34],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+34],spec_visc[spec_idx*66+34]),spec_sum[34])); 
      result_vec[35] = 
        _mm256_add_pd(result_vec[35],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+35],spec_visc[spec_idx*66+35]),spec_sum[35])); 
      result_vec[36] = 
        _mm256_add_pd(result_vec[36],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+36],spec_visc[spec_idx*66+36]),spec_sum[36])); 
      result_vec[37] = 
        _mm256_add_pd(result_vec[37],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+37],spec_visc[spec_idx*66+37]),spec_sum[37])); 
      result_vec[38] = 
        _mm256_add_pd(result_vec[38],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+38],spec_visc[spec_idx*66+38]),spec_sum[38])); 
      result_vec[39] = 
        _mm256_add_pd(result_vec[39],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+39],spec_visc[spec_idx*66+39]),spec_sum[39])); 
      result_vec[40] = 
        _mm256_add_pd(result_vec[40],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+40],spec_visc[spec_idx*66+40]),spec_sum[40])); 
      result_vec[41] = 
        _mm256_add_pd(result_vec[41],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+41],spec_visc[spec_idx*66+41]),spec_sum[41])); 
      result_vec[42] = 
        _mm256_add_pd(result_vec[42],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+42],spec_visc[spec_idx*66+42]),spec_sum[42])); 
      result_vec[43] = 
        _mm256_add_pd(result_vec[43],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+43],spec_visc[spec_idx*66+43]),spec_sum[43])); 
      result_vec[44] = 
        _mm256_add_pd(result_vec[44],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+44],spec_visc[spec_idx*66+44]),spec_sum[44])); 
      result_vec[45] = 
        _mm256_add_pd(result_vec[45],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+45],spec_visc[spec_idx*66+45]),spec_sum[45])); 
      result_vec[46] = 
        _mm256_add_pd(result_vec[46],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+46],spec_visc[spec_idx*66+46]),spec_sum[46])); 
      result_vec[47] = 
        _mm256_add_pd(result_vec[47],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+47],spec_visc[spec_idx*66+47]),spec_sum[47])); 
      result_vec[48] = 
        _mm256_add_pd(result_vec[48],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+48],spec_visc[spec_idx*66+48]),spec_sum[48])); 
      result_vec[49] = 
        _mm256_add_pd(result_vec[49],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+49],spec_visc[spec_idx*66+49]),spec_sum[49])); 
      result_vec[50] = 
        _mm256_add_pd(result_vec[50],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+50],spec_visc[spec_idx*66+50]),spec_sum[50])); 
      result_vec[51] = 
        _mm256_add_pd(result_vec[51],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+51],spec_visc[spec_idx*66+51]),spec_sum[51])); 
      result_vec[52] = 
        _mm256_add_pd(result_vec[52],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+52],spec_visc[spec_idx*66+52]),spec_sum[52])); 
      result_vec[53] = 
        _mm256_add_pd(result_vec[53],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+53],spec_visc[spec_idx*66+53]),spec_sum[53])); 
      result_vec[54] = 
        _mm256_add_pd(result_vec[54],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+54],spec_visc[spec_idx*66+54]),spec_sum[54])); 
      result_vec[55] = 
        _mm256_add_pd(result_vec[55],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+55],spec_visc[spec_idx*66+55]),spec_sum[55])); 
      result_vec[56] = 
        _mm256_add_pd(result_vec[56],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+56],spec_visc[spec_idx*66+56]),spec_sum[56])); 
      result_vec[57] = 
        _mm256_add_pd(result_vec[57],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+57],spec_visc[spec_idx*66+57]),spec_sum[57])); 
      result_vec[58] = 
        _mm256_add_pd(result_vec[58],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+58],spec_visc[spec_idx*66+58]),spec_sum[58])); 
      result_vec[59] = 
        _mm256_add_pd(result_vec[59],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+59],spec_visc[spec_idx*66+59]),spec_sum[59])); 
      result_vec[60] = 
        _mm256_add_pd(result_vec[60],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+60],spec_visc[spec_idx*66+60]),spec_sum[60])); 
      result_vec[61] = 
        _mm256_add_pd(result_vec[61],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+61],spec_visc[spec_idx*66+61]),spec_sum[61])); 
      result_vec[62] = 
        _mm256_add_pd(result_vec[62],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+62],spec_visc[spec_idx*66+62]),spec_sum[62])); 
      result_vec[63] = 
        _mm256_add_pd(result_vec[63],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+63],spec_visc[spec_idx*66+63]),spec_sum[63])); 
      result_vec[64] = 
        _mm256_add_pd(result_vec[64],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+64],spec_visc[spec_idx*66+64]),spec_sum[64])); 
      result_vec[65] = 
        _mm256_add_pd(result_vec[65],_mm256_div_pd(_mm256_mul_pd(mole_frac[spec_idx*66+65],spec_visc[spec_idx*66+65]),spec_sum[65])); 
    }
    if (aligned<32>(viscosity))
    {
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        result_vec[idx] = 
          _mm256_mul_pd(_mm256_set1_pd(0.1535268500652559),result_vec[idx]); 
        _mm256_stream_pd(viscosity,result_vec[idx]);
        viscosity += 4;
      }
    }
    else
    {
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        result_vec[idx] = 
          _mm256_mul_pd(_mm256_set1_pd(0.1535268500652559),result_vec[idx]); 
        _mm256_storeu_pd(viscosity,result_vec[idx]);
        viscosity += 4;
      }
    }
  }
  free(mole_frac);
  free(spec_visc);
  free(result_vec);
  free(spec_sum);
}

const double diff_constants[1740] = {8.361686290887316e-03, -0.1860162057484011, 
  3.033147089100501, -12.0191229465797, 0.01929139676017443, 
  -0.4457548701621225, 5.090086348908469, -18.01636401228401, 0.015450973050297, 
  -0.3482328328762868, 4.270888004936923, -15.31287558472519, 
  0.01910839802389039, -0.4413160055384065, 5.054239009523916, 
  -17.90236107824279, 0.01549202608989689, -0.3491406935690256, 
  4.277567665477716, -15.33072459328188, 0.01018064676069338, 
  -0.2825946969132601, 4.220700982568849, -16.51314932188011, 
  0.01310172053619389, -0.3323549429812104, 4.4471030347451, -16.99414771483977, 
  0.01870695312438664, -0.4240579014711124, 4.859366081385168, 
  -17.19132402412691, 0.01462628008567279, -0.3605874816786186, 
  4.609955031206802, -17.18717776794484, 0.01334268108291999, 
  -0.339872975605966, 4.517483207515941, -17.39068703770995, 
  5.536895739234286e-03, -0.1867583089190002, 3.565008159636777, 
  -15.39084361466359, 0.01332456327434385, -0.3395706503024595, 
  4.51602164115593, -17.38939006797174, 6.792229447080372e-03, 
  -0.2103085359487538, 3.698147584577465, -15.61126528748085, 
  0.0198133054853778, -0.4490716598832116, 5.048252383884035, 
  -17.61107473004078, 0.01983694950759395, -0.4495998310963165, 
  5.052178473243545, -17.62110951921104, 0.01985957569699271, 
  -0.4501052540237288, 5.055935363576013, -17.63070445637847, 
  0.01402682860856762, -0.3509187795957734, 4.566918361563354, 
  -17.25033892804664, 4.517948517158758e-03, -0.1645512845004072, 
  3.400144334222293, -15.08764355297302, 1.198342438267866e-03, 
  -0.09821904357550085, 2.972261954495235, -14.34857260742947, 
  6.236610592995685e-03, -0.1996384148079854, 3.625576538368873, 
  -15.6965222017684, 9.946430396957434e-04, -0.0945153047093119, 
  2.944308618231648, -14.33144198720684, 9.675828355619525e-04, 
  -0.09397468017373527, 2.940790588799131, -14.32408304776669, 
  5.885373675076967e-03, -0.1928145452184591, 3.582790325381286, 
  -15.61208985457336, 5.682892938320809e-03, -0.1888801510502674, 
  3.558116185352223, -15.56317356418745, 5.682892938320809e-03, 
  -0.1888801510502674, 3.558116185352223, -15.56317356418745, 
  5.655846538805275e-03, -0.1883545789275397, 3.554819803472873, 
  -15.5566262932005, 5.481883524736946e-03, -0.1849738808346717, 
  3.533614252024805, -15.51444018497138, 5.327436577901751e-03, 
  -0.181972131773661, 3.514782853160168, -15.47688066504096, 
  0.01855344618681919, -0.4205906575434817, 4.833263530567264, 
  -17.11566826957102, 9.715282317873395e-03, -0.2150511936847471, 
  3.239853677599016, -13.58416798775506, 3.897545152990703e-03, 
  -0.08736463553908934, 2.306135807245676, -10.93372903923921, 
  9.644706316049781e-03, -0.2134256737108184, 3.227360672737047, 
  -13.53826022418841, 3.897467312077582e-03, -0.08736274608812922, 
  2.306120327843064, -10.93673502135609, 0.01887643503750303, 
  -0.4377066088511249, 5.04362412310166, -18.18497779640277, 0.0147372149722128, 
  -0.3299413515244374, 4.116350814298772, -15.86051294474001, 
  7.771157267814767e-03, -0.1707676302238381, 2.903434292626104, 
  -12.68298231849476, 0.01334851538330884, -0.2989248768485377, 
  3.885327569302682, -15.21342866871342, 0.014981080885595, -0.3347664000209726, 
  4.147161703679751, -16.09619381109583, 0.01711384774032581, 
  -0.3935105495781183, 4.673156936930092, -17.4777138062095, 
  0.01501090353608677, -0.335418860179776, 4.151908403958016, -16.1085711288622, 
  0.01891300902883566, -0.429308012851489, 4.905017521955911, 
  -17.97853779811946, 7.901250163854623e-03, -0.1739680837285394, 
  2.929626522788437, -12.70709387778168, 7.91063674756423e-03, 
  -0.1741690060676314, 2.931054697752604, -12.71125832383727, 
  7.919537539234882e-03, -0.174359531735583, 2.932408989082864, 
  -12.71520205606033, 0.01422137871784259, -0.3181198249953437, 
  4.025827521562348, -15.64739655500348, 0.01778644607069595, 
  -0.4058989294964501, 4.743684212887176, -17.70170519863036, 
  0.01947276925128896, -0.4442636336023223, 5.034794328287674, 
  -18.56440021052348, 0.01856825753836317, -0.4169336661378619, 
  4.774819760507476, -17.7979941825457, 0.02005641636469029, 
  -0.4538461036761604, 5.07838172139493, -18.66727920854099, 
  0.02006829944074763, -0.4541122322887322, 5.08036558601627, 
  -18.67240359007563, 0.01879928558568076, -0.4220337025094115, 
  4.812277032281624, -17.89359697664186, 0.01892735043336728, 
  -0.424862995650768, 4.833074714641758, -17.94643128562877, 
  0.01892735043336728, -0.424862995650768, 4.833074714641758, 
  -17.94643128562877, 0.01894418056968953, -0.4252349548753668, 
  4.835810033457547, -17.95336628632041, 0.01905091044713881, 
  -0.4275945908565789, 4.853168998813324, -17.99730068626329, 
  0.01914350583060496, -0.429642995453191, 4.868248534707257, 
  -18.03535628485514, 7.738735186575013e-03, -0.1700462638479564, 
  2.898081865116513, -12.66067773360676, 0.01197280363715555, 
  -0.271240959763248, 3.703886270090693, -15.61687140161808, 
  0.01527022787579618, -0.3506765555415377, 4.343240120686327, 
  -17.66458833773856, 0.01194857826622421, -0.2707080833280793, 
  3.699988722155256, -15.62210643661616, 0.01751957999989958, 
  -0.4324987296990057, 5.222435482812253, -20.42453456473173, 
  0.01718272806693526, -0.406068533902639, 4.860641368293829, 
  -19.38214525305954, 0.01314285350732774, -0.2994308129782687, 
  3.930380704942472, -16.61214440642024, 0.01690529184503588, 
  -0.3975468468661103, 4.777587841158367, -19.06497074712664, 
  0.01688978898805172, -0.398782693428465, 4.799452728417728, 
  -19.38164785335095, 0.01704934461227353, -0.4199389693147162, 
  5.108642883195372, -20.45023975022425, 0.01686860403443959, 
  -0.3984316068670234, 4.797776439372183, -19.38533027530205, 
  0.01694648548163187, -0.4131419577900753, 5.01825068725196, -20.1268971113818, 
  0.01423183077529593, -0.3242841480029669, 4.119808689722729, -17.070944000896, 
  0.01426685069202663, -0.3250667198791441, 4.125626065677508, 
  -17.09002710504034, 0.01430141955243729, -0.3258392062302946, 
  4.131368460333886, -17.10868108206892, 0.01667281076360975, 
  -0.3944963416937638, 4.771649901922499, -19.23135414690418, 
  0.01589906970550712, -0.3929903033822995, 4.895182518344389, 
  -20.02013694597282, 0.01551840200013507, -0.3874275960261506, 
  4.879130020329604, -20.17297636396011, 0.01603138507467473, 
  -0.3895192522242649, 4.812636189871089, -19.7919104234836, 
  0.01481937311659824, -0.3701544206643765, 4.733573887115632, 
  -19.84048696731194, 0.01477860059136737, -0.3693407338613307, 
  4.728284622271691, -19.83092928344765, 0.01550280947485409, 
  -0.3792561469877729, 4.748313100043996, -19.69362175667486, 
  0.01519737529087586, -0.3733234851752495, 4.711114707698918, 
  -19.63494393702734, 0.01519737529087586, -0.3733234851752495, 
  4.711114707698918, -19.63494393702734, 0.01515701643004046, 
  -0.3725394574619092, 4.706197987870115, -19.62710762666573, 
  0.01490153586209278, -0.3675758272249446, 4.675066542043249, 
  -19.57710032281691, 0.01468246421822933, -0.363318835868341, 
  4.648361846635586, -19.53370264172737, 0.01309926016016264, 
  -0.2983957396801308, 3.922183617690015, -16.58229727389163, 
  0.01186253353791213, -0.268655647344621, 3.683629037447621, 
  -15.56218013907301, 8.457418012014975e-03, -0.189039657678284, 
  3.062679110774865, -13.5846845098064, 0.01761870766105015, 
  -0.4192832736941898, 4.989055712509337, -19.06508683685098, 0.016019461533698, 
  -0.3684297830246653, 4.48333144504821, -17.8466966887303, 0.01025849789757969, 
  -0.2298575526170802, 3.37109785521221, -14.78200095138266, 
  0.01532051709368729, -0.3515346256464287, 4.34682118116802, 
  -17.41286111571464, 0.01594204216011421, -0.3662483329469599, 
  4.462875336261959, -17.97995444008053, 0.01697257048056616, 
  -0.4039216933855035, 4.86489740789333, -19.14986287807598, 
  0.01598941534962163, -0.367358462503614, 4.471517397007869, 
  -18.00785616028524, 0.0166365765552174, -0.3924367983867684, 
  4.745967813973647, -18.78354675448943, 0.01111627671020413, 
  -0.2491203680148213, 3.515444700971034, -15.11165822172473, 
  0.01116669950736782, -0.2502123502720225, 3.523301396417855, 
  -15.13525216798087, 0.01121620107196187, -0.2512844002655299, 
  3.531014868535837, -15.15825477973445, 0.01609251728515954, 
  -0.3697552089183042, 4.489873138391907, -17.93673259258161, 
  0.01639339319066143, -0.3901993432640227, 4.754404091430307, 
  -19.00626201897158, 0.01596102041363236, -0.383989885167513, 
  4.736214212367928, -19.17250405677622, 0.0166917388200961, 
  -0.3908198819509124, 4.708475230601932, -18.91691701064183, 
  0.01657369092412961, -0.3951542962143418, 4.796857262563063, 
  -19.35098481047572, 0.01656733968623798, -0.395096717719429, 4.79707158017407, 
  -19.35469686949993, 0.01681441552105499, -0.3946441793121106, 
  4.74515056823209, -19.05867249006957, 0.0168844084620502, -0.3968433319937477, 
  4.766339275560844, -19.13891627340647, 0.0168844084620502, 
  -0.3968433319937477, 4.766339275560844, -19.13891627340647, 
  0.01689363642025551, -0.3971342216680241, 4.769147389221604, 
  -19.14947331545991, 0.01695203857536147, -0.3989806947551502, 
  4.78700340578782, -19.21620476611159, 0.01700221862475968, 
  -0.4005755230154533, 4.802472654131053, -19.27347508617585, 
  0.01019994586293929, -0.2285473221102691, 3.361316901848032, 
  -14.74821144369962, 0.01183177254102251, -0.2679777607122745, 
  3.678661496181423, -15.56533029767279, 0.01661901022407684, 
  -0.4197153934542214, 5.190807260648161, -20.52802326149737, 
  0.01714935570776451, -0.4047676849196055, 4.846411603587371, 
  -19.3392262841592, 0.01299730148779137, -0.2959724763391836, 
  3.902989297942163, -16.54504014910531, 0.01686704317359211, 
  -0.3961873618110683, 4.76334419828346, -19.02331942112334, 
  0.01684654523282157, -0.3972351814453051, 4.78322302991886, -19.3354808205356, 
  0.01721821288373084, -0.422870986435835, 5.123416927402899, 
  -20.47278723428071, 0.0168271651765628, -0.3969191119628537, 
  4.781766672581613, -19.33986177626137, 0.01724911938009028, 
  -0.4211895731638367, 5.089565275349292, -20.34322303295647, 
  0.01396610989156723, -0.3182573739011489, 4.074165936822435, 
  -16.96131607891197, 0.01399974884308092, -0.319009104054729, 4.07975412231456, 
  -16.98005835597672, 0.014033128813055, -0.3197550390977301, 4.08529920451891, 
  -16.99844022429313, 0.01668921486599597, -0.3942400992627107, 
  4.764710227065308, -19.20917400756508, 0.01609670331359669, 
  -0.3965435253955804, 4.914483173131496, -20.05722347460514, 
  0.01544707727641764, -0.3856330904913191, 4.863182182371991, 
  -20.13383074207051, 0.01610776665596586, -0.3904696845273812, 
  4.813523329043252, -19.78759388906466, 0.01504183917288854, 
  -0.374232656347211, 4.756597486676818, -19.8893274039839, 0.01500060767434612, 
  -0.3734109451244153, 4.751266251329464, -19.87979635557892, 
  0.01557944902435037, -0.3802355605296093, 4.74960024909389, 
  -19.69237447240075, 0.01527044851018354, -0.3742473849184965, 
  4.712179350216234, -19.63449986794348, 0.01527044851018354, 
  -0.3742473849184965, 4.712179350216234, -19.63449986794348, 
  0.01522944736638463, -0.3734526926135804, 4.707212311207702, 
  -19.62672516079136, 0.01496907327072761, -0.3684054320912689, 
  4.675660977248493, -19.57689217600287, 0.01474476807762328, 
  -0.3640565131267177, 4.648469059406899, -19.53337286596797, 
  0.01296169192693592, -0.2951168666407831, 3.896131714663515, 
  -16.51845374267443, 0.01762633021574163, -0.4194071513631389, 
  4.989633579791411, -19.0817111553006, 0.0159545772153037, -0.366909592210842, 
  4.471502079973733, -17.83539062741878, 0.01017307352583319, 
  -0.228005450510195, 3.3577564736272, -14.76986416716022, 0.01526172524244872, 
  -0.350156415304678, 4.336089185778999, -17.40488975953586, 0.0158588821517522, 
  -0.3643001920591742, 4.447714255340937, -17.96100445430084, 
  0.01704104851254479, -0.405159917212821, 4.871870985671556, 
  -19.18176588166162, 0.01590499646282739, -0.3653803846358016, 
  4.456119901668306, -17.98853893616841, 0.01667069852061068, 
  -0.3929509375495965, 4.747893974778329, -18.80408513074376, 
  0.01101912705698624, -0.2470165007803154, 3.500308159504729, 
  -15.09621639507836, 0.01106805265226408, -0.2480760211505555, 
  3.507930920796229, -15.11947081451499, 0.01111627671020429, 
  -0.2491203680148245, 3.515444700971055, -15.14220600071709, 
  0.01601858518571832, -0.3680187024810189, 4.476324204916731, 
  -17.92453424748907, 0.01644141870947844, -0.3909681420938221, 
  4.757794935034931, -19.03172055435256, 0.0160441906117713, 
  -0.3854712226807655, 4.744334130135219, -19.20854067629937, 
  0.01666500998084409, -0.3899919756904059, 4.700566007749391, 
  -18.91630557107337, 0.01659681899665369, -0.3953627029194812, 
  4.796058076589331, -19.36780531053828, 0.01659038086724507, 
  -0.3953048910133678, 4.796283536542286, -19.37166032271827, 
  0.01679046890483364, -0.3938946423059972, 4.737945086452727, 
  -19.06168007452957, 0.01686262715349251, -0.3961576198462414, 
  4.759724787772213, -19.14452730116448, 0.01686262715349251, 
  -0.3961576198462414, 4.759724787772213, -19.14452730116448, 
  0.01687216564626523, -0.3964577547063053, 4.762619067972215, 
  -19.15545097541556, 0.01693264584232135, -0.3983664798204694, 
  4.781057630953784, -19.22460719579998, 0.01698474101367319, 
  -0.4000190964554385, 4.797070217225778, -19.28407860467514, 
  0.01011604018635441, -0.2267279585032462, 3.3482099613877, -14.73662675035525, 
  0.01276715517710311, -0.3467558983432538, 4.769182061492662, 
  -20.00930506299005, 0.01759976282858629, -0.431374579075224, 
  5.186314414861248, -20.3123444050489, 0.01393104706159106, 
  -0.3689614203481394, 4.899867871269459, -20.16247345324624, 
  0.01251273318909122, -0.3398284593956539, 4.706359866108304, 
  -20.02166250825629, 2.961995181951187e-03, -0.1496107047376196, 
  3.509439623576637, -17.57470889906346, 0.01240312218810322, 
  -0.3375016305986661, 4.690224285295936, -19.99140738276309, 
  -1.722854872784397e-03, -0.0506481953929987, 2.840074298036828, 
  -16.1879122279262, 0.01725358851966463, -0.426062980261422, 5.167992585222853, 
  -20.29800744458825, 0.01660123780373732, -0.4062735870564639, 
  4.975026876334818, -19.69227647126568, 0.01652519649257162, 
  -0.4047376014201799, 4.964899372240972, -19.67562585464342, 
  9.647092764604213e-03, -0.2835828359206056, 4.356727595492845, 
  -19.24814729175656, 5.458543694842264e-03, -0.1987081305704537, 
  3.820063074259181, -18.35811956138697, 4.220579372562574e-03, 
  -0.1727114141289421, 3.646823568376245, -18.14292159439013, 
  8.501857059348713e-03, -0.2588518679238623, 4.192212007663292, 
  -19.20855554427839, 6.308850965281988e-03, -0.2138632789225292, 
  3.905547006697611, -18.7428108929955, 6.294067997107787e-03, 
  -0.2134885372340211, 3.902622999559342, -18.73753542577968, 
  7.811866370889864e-03, -0.2435512042497034, 4.082521310659742, 
  -18.98993522059879, 7.39892309205497e-03, -0.2343946259688733, 
  4.016883342936556, -18.85645979161424, 7.39892309205497e-03, 
  -0.2343946259688733, 4.016883342936556, -18.85645979161424, 
  7.343465478205244e-03, -0.2331649476339979, 4.008068871074578, 
  -18.83841122761047, 6.986875316322941e-03, -0.2252584591284294, 
  3.951396684538771, -18.72172539380137, 6.672311235276643e-03, 
  -0.2182844020130469, 3.901412364432089, -18.61792437389283, 
  0.01752041063984414, -0.4292621352288661, 5.167061062943796, 
  -20.24331802125277, 0.01608218846344772, -0.3730497341488359, 
  4.544732900243092, -18.50939564665117, 0.0177470251422625, 
  -0.4303118620542329, 5.141705878828438, -20.50229160525336, 
  0.01769058946061219, -0.4303609922918875, 5.153159982358243, 
  -20.79667851668607, 0.01487810846600316, -0.388421946402578, 
  5.028238023247621, -20.9108858688434, 0.01765710195666046, 
  -0.4296855118108462, 5.148709356637934, -20.79503636026955, 
  0.01603486837534695, -0.4085738237685885, 5.126564302650014, 
  -21.01914748570987, 0.01662299306317555, -0.3866746528753355, 
  4.658928838069516, -18.81310913052763, 0.01662154762449842, 
  -0.3866620330866413, 4.658986052816886, -18.8204774717565, 
  0.01662031168714402, -0.3866568678234443, 4.659119682480987, 
  -18.82775721520052, 0.01727468125584479, -0.4215261145035135, 
  5.090756014595465, -20.58395629908095, 0.01527464578286526, 
  -0.3937614207118246, 5.035855423845857, -21.00065693515901, 
  0.01427855212817547, -0.3746195297877447, 4.9213716101658, -20.92465637093389, 
  0.01613326247817586, -0.40528110508291, 5.051779331170613, -20.97636201445358, 
  0.01428958335181497, -0.3717174561188799, 4.870280509250251, 
  -20.80529118742287, 0.0142334532279584, -0.3705508916713781, 
  4.862343376405767, -20.79011807164681, 0.01518990166966528, 
  -0.3859045055915796, 4.921490200084995, -20.73724733814467, 
  0.01455661433378379, -0.3728954562376406, 4.834009893132143, 
  -20.57011055891577, 0.01455661433378379, -0.3728954562376406, 
  4.834009893132143, -20.57011055891577, 0.01446863424693232, 
  -0.371088079933474, 4.821855665982699, -20.5466256222119, 0.01389052432734886, 
  -0.3592114791206485, 4.741985734589082, -20.39107748587153, 
  0.0133683785054096, -0.3484838936408337, 4.66983962339939, -20.24909175349189, 
  0.01605610967565784, -0.3723671521293567, 4.538825284308942, 
  -18.48414610794849, 0.01574444975162647, -0.3635632171037952, 
  4.458090368873123, -18.19896210816889, 0.01603614256888279, 
  -0.3706447271251463, 4.51545905276349, -18.59114802045471, 
  0.01776547635070906, -0.4240364357896437, 5.037182907598198, 
  -20.09778392705388, 0.01604490332863651, -0.3708580653156919, 
  4.517176561882548, -18.60391748015955, 0.01797007519685268, 
  -0.4259383525601978, 5.0286249616893, -20.03301698033892, 0.01143096775398266, 
  -0.2581899062898847, 3.599212287457288, -15.82244530724902, 
  0.01143956871960472, -0.2583774171294352, 3.600570645003726, 
  -15.83284728213591, 0.01145011749170804, -0.2586074419215186, 
  3.602237408062738, -15.84365217293951, 0.01604367644406037, 
  -0.3708426164400376, 4.516961186217023, -18.53079415235116, 
  0.01740205764262819, -0.4135431552074462, 4.938189988363134, 
  -19.96442004198294, 0.01708637022100646, -0.4091613599751163, 
  4.927762994179491, -20.12557094589089, 0.01692140297359912, 
  -0.3970169107027955, 4.765732873001947, -19.54574682885651, 
  0.01685556990331726, -0.4017709158779321, 4.852493620481751, 
  -19.97996549790109, 0.0168351907444687, -0.4014000128382058, 
  4.850382996032597, -19.97892312858702, 0.01684679371909519, 
  -0.3963819676295641, 4.768858707658768, -19.62169697463377, 
  0.01679180208398761, -0.395866083530237, 4.77044846207417, -19.66499973830335, 
  0.01679180208398761, -0.395866083530237, 4.77044846207417, -19.66499973830335, 
  0.01678393578994712, -0.3957903381737837, 4.770646845789735, 
  -19.67070707170019, 0.01673109000569553, -0.3952718911518128, 4.7718371375473, 
  -19.70677159783628, 0.01668183728044754, -0.3947763368120913, 
  4.772763191717539, -19.73758535862802, 0.01067562975746162, 
  -0.2408858507243121, 3.466861174547758, -15.49018246615967, 
  0.01770786483461071, -0.4279145838383313, 5.110293120668989, 
  -20.57314095446369, 0.01567910370932323, -0.4028192731367471, 
  5.103555624065509, -20.94984906818039, 0.01768074206972333, 
  -0.4273724895313247, 5.106762162919154, -20.57390455941299, 
  0.01692223939700398, -0.4246119478607712, 5.212545652269392, 
  -21.08393040216978, 0.0160122624542402, -0.3712220098742469, 
  4.528923919785043, -18.39743682612787, 0.01601165461959638, 
  -0.3712235497928291, 4.529051633294823, -18.40518687534747, 
  0.01601136059376211, -0.3712347917780965, 4.529271889920254, 
  -18.4128766690898, 0.01743629493798964, -0.4221580294036591, 
  5.069651213972101, -20.41078339354567, 0.01581327605008552, 
  -0.4026975385768839, 5.073822719997091, -20.96286228487243, 
  0.01483107529775175, -0.3839103898530722, 4.962015890672341, 
  -20.89621281744701, 0.01651244139193281, -0.4110387930714625, 
  5.069554525819001, -20.90423129483277, 0.01464948423593474, 
  -0.3771685378292694, 4.88612476442801, -20.72976384860511, 
  0.01458647906015063, -0.3758681294827105, 4.877333817784148, 
  -20.7129352923631, 0.01551999421200606, -0.3907995530443317, 
  4.934466776029828, -20.65920818821595, 0.01483785885135674, 
  -0.3768869342451547, 4.841596996190879, -20.48311873316177, 
  0.01483785885135674, -0.3768869342451547, 4.841596996190879, 
  -20.48311873316177, 0.01474225968413833, -0.3749370292167276, 
  4.828580453150343, -20.45813206744282, 0.01410955736917457, 
  -0.3620314518721119, 4.74242675185444, -20.29132239773672, 
  0.01353185635921411, -0.3502469557301395, 4.663752995734938, 
  -20.13725064581071, 0.01570386697431676, -0.3625777153439032, 
  4.450097797101236, -18.16875230767832, 0.01543768817634423, 
  -0.3985688522410473, 5.082524602390438, -21.16981144671941, 
  0.0177907936334924, -0.4303513116901599, 5.133992166243674, -20.8766851485934, 
  0.01659698821680079, -0.4187528258546066, 5.181118607350389, 
  -21.27807800651694, 0.01621138246238222, -0.3763258990701221, 
  4.572319887381138, -18.76367495662019, 0.01620851192523879, 
  -0.376254622994634, 4.571736935996036, -18.76945956866688, 
  0.01620601406889396, -0.3761953302339939, 4.571270809742595, 
  -18.77525482385897, 0.0175761502202725, -0.4258236989664007, 
  5.101860069775093, -20.74266187026316, 0.01608354416452579, 
  -0.4090117090600894, 5.124133879216219, -21.33138000082258, 
  0.01542541303751555, -0.3970692402724553, 5.059981968898624, 
  -21.36315850637574, 0.01719123713176045, -0.4256066007744396, 
  5.17441885465442, -21.37421469355437, 0.01564529517530011, 
  -0.3984882382224751, 5.038360563664739, -21.30884737441428, 
  0.01559906571966491, -0.397531507826562, 5.03187774149355, -21.29718418410792, 
  0.01646975887591035, -0.4108520141344763, 5.075664912433869, 
  -21.20949558107147, 0.0159435533040175, -0.4000889455055683, 
  5.003614870231607, -21.07933553026604, 0.0159435533040175, 
  -0.4000889455055683, 5.003614870231607, -21.07933553026604, 
  0.01586861434643187, -0.3985560344691689, 4.993352685847322, 
  -21.06042109609795, 0.01536752838941727, -0.3883055054812352, 
  4.924726507640238, -20.93223286510378, 0.01490475581683615, 
  -0.3788378891915134, 4.861337194426643, -20.81181538659878, 
  0.01600311167154659, -0.3698233154242626, 4.508649662313163, 
  -18.5646066809692, 0.01542313357114251, -0.3982610348305203, 
  5.080395939874243, -21.17356928612397, 9.37396187600255e-03, 
  -0.2814553224653157, 4.379424310484298, -19.82650548257712, 
  0.01803192489534054, -0.4320140228914988, 5.114224090626984, -20.326898812041, 
  0.01802858183516278, -0.4319404969468079, 5.11369276226905, 
  -20.33308645046955, 0.01802243384229506, -0.4318116028053467, 
  5.112806455068219, -20.33819160831386, 0.01536584788667469, 
  -0.396684198037172, 5.06619744471396, -21.06202603900746, 
  8.519324047002199e-03, -0.263840202805529, 4.263553119181736, 
  -19.74614429255437, 7.076029778068207e-03, -0.234226191115042, 
  4.068998658183347, -19.47442130545115, 0.01222478491057199, 
  -0.3368448263965969, 4.716303610294374, -20.74010752889676, 
  9.208443004764429e-03, -0.2764811454324513, 4.335079963781424, 
  -20.09226308738432, 9.188677431838989e-03, -0.27603021570242, 
  4.33178400671293, -20.08729291509497, 0.01167636386407826, 
  -0.3249856451788373, 4.63288733096908, -20.6017949146601, 0.01128276953889692, 
  -0.3164756849052876, 4.573038427781504, -20.49311353348583, 
  0.01128276953889692, -0.3164756849052876, 4.573038427781504, 
  -20.49311353348583, 0.0112268653608535, -0.3152669823413235, 
  4.564538073057616, -20.47730958084015, 0.01085325715695665, 
  -0.3071891014341762, 4.507729748503372, -20.36998824125493, 
  0.01050767519013051, -0.2997168674281591, 4.455180463695319, 
  -20.26865294469341, 0.01774763201206144, -0.4235011164744815, 
  5.032034577127299, -20.07324021623124, 0.01658788139904375, 
  -0.4185621852453963, 5.179811733119763, -21.28386476635458, 
  0.0162148859114813, -0.376416221370762, 4.573081574637201, -18.77458898751097, 
  0.01621168406702842, -0.3763335451682168, 4.572383506694558, 
  -18.78015861559497, 0.01620886652068069, -0.3762632660238456, 
  4.571806511084312, -18.78574761348539, 0.01760952666922614, 
  -0.4264914778811474, 5.106217050160734, -20.76217989449102, 
  0.01613182942241974, -0.4100138212804433, 5.130943281320124, 
  -21.35678986760308, 0.01548877078258531, -0.398390424367975, 
  5.069000737507449, -21.39377018190699, 0.01725984955592346, 
  -0.427009517936241, 5.183807132904281, -21.40524771579032, 
  0.01573709878029865, -0.400388089698128, 5.05123370244048, -21.34887894835055, 
  0.01569175074940746, -0.3994496322406266, 5.044874890045692, 
  -21.33755632791134, 0.01656068325953119, -0.4127116533463194, 
  5.088112871478407, -21.24842870757958, 0.01603967488855693, 
  -0.4020551218816306, 5.016777381117836, -21.12052575391307, 
  0.01603967488855693, -0.4020551218816306, 5.016777381117836, 
  -21.12052575391307, 0.0159649722102323, -0.4005270747650523, 5.00654793356848, 
  -21.1017747851548, 0.01546290892894862, -0.3902567454920981, 
  4.937790266689601, -20.97388198176454, 0.01499593834194696, 
  -0.3807034057629116, 4.873827876156825, -20.85271355370843, 
  0.01601210918223789, -0.3700424020243533, 4.510413399877581, 
  -18.57749676303557, 0.01797181525421944, -0.4281517584207457, 
  5.063567543310374, -20.1601358357579, 0.01768642894877551, 
  -0.4200959404692323, 4.989293154728844, -19.94316497820088, 
  0.01768418607937717, -0.4200448104870494, 4.988909576066858, 
  -19.9494567123894, 0.01598594961361961, -0.4073078273122722, 
  5.115002894749336, -21.11193315112983, 0.01111635351457155, 
  -0.3155841990403139, 4.589561842347139, -20.39176246821974, 
  9.894147069085996e-03, -0.2910492891214793, 4.433391294454064, 
  -20.21372071039943, 0.01402556738272536, -0.3711906261020791, 
  4.917134617563886, -21.08916515696886, 0.01125100881328648, 
  -0.3167735568466945, 4.582949146323461, -20.56103033615217, 
  0.01121727463116988, -0.3160434942731526, 4.577808759944576, 
  -20.55208856663434, 0.01335168574963909, -0.3569223449053559, 
  4.818451151767042, -20.92000535772784, 0.01285777815506309, 
  -0.346465109951805, 4.746130739320086, -20.78559266129254, 
  0.01285777815506309, -0.346465109951805, 4.746130739320086, 
  -20.78559266129254, 0.0127871621439714, -0.3449699770046651, 
  4.735790710383812, -20.76597312346908, 0.01231294467242273, 
  -0.3349292693683298, 4.666350913822045, -20.63236134885959, 
  0.01187147930576884, -0.3255815643573291, 4.601702675185011, -20.505735333898, 
  0.01795666463244159, -0.4253479492459609, 5.021789068381895, 
  -20.00088354859287, 0.01187611033178163, -0.2691183051571773, 
  3.688399522537898, -16.0588777374304, 0.01187918561469026, 
  -0.2691862126248796, 3.688898128818053, -16.06742344100748, 
  0.01617305905066158, -0.3754539076071414, 4.565512563284903, 
  -18.68514283240182, 0.0173997357504221, -0.415636708201812, 4.972105854634792, 
  -20.09700958225115, 0.0174104690464549, -0.4183963036350818, 
  5.013530846509195, -20.38668178000486, 0.01693107525007737, 
  -0.3991631789660171, 4.798015411450207, -19.67282954642232, 
  0.01690406096954081, -0.4047490479403464, 4.891097281921525, 
  -20.12863924357937, 0.01687689732661909, -0.4042348392899182, 
  4.887987398257996, -20.12555076593007, 0.01674300452558515, 
  -0.3961226148106777, 4.784274271296734, -19.71434723257281, 
  0.01660195894251777, -0.3938193673950928, 4.773629836259699, 
  -19.73273198013653, 0.01660195894251777, -0.3938193673950928, 
  4.773629836259699, -19.73273198013653, 0.01658162331264033, 
  -0.3934863342901689, 4.772081091442831, -19.7348891861111, 
  0.01644408121217673, -0.3912290413999133, 4.761535901311994, 
  -19.74716619789997, 0.01631460272759323, -0.389097814855518, 
  4.751517515319503, -19.75588741535798, 0.01137272204757106, 
  -0.2568719643311959, 3.589258796402727, -15.78874095180036, 
  0.01187604700964037, -0.2691169068977695, 3.688389255917086, -16.074130510967, 
  0.01616757044132469, -0.3752914424287733, 4.564003286855589, 
  -18.68972760873219, 0.01743114230290766, -0.4162402310021223, 
  4.975836243162726, -20.11340234267186, 0.01745806103616812, 
  -0.4193218983446132, 5.019351664986814, -20.40768667902062, 
  0.01694706252051249, -0.3994185034848948, 4.799136859803416, 
  -19.68297531287536, 0.01695310951393357, -0.4056773377950208, 
  4.89670979958589, -20.14954970593606, 0.01692645907413683, 
  -0.4051729863119613, 4.89366072793903, -20.14664254889118, 
  0.01676490537133993, -0.3964789630238127, 4.785908097736856, 
  -19.72631017971463, 0.01662561265359033, -0.3942064736081308, 
  4.775427372888558, -19.74559607023413, 0.01662561265359033, 
  -0.3942064736081308, 4.775427372888558, -19.74559607023413, 
  0.0166054051538909, -0.3938757848085727, 4.773891994789771, 
  -19.74786325277953, 0.01646808523668161, -0.3916235297490276, 
  4.763384188521981, -19.76078463829237, 0.01633797483140773, 
  -0.3894829104295383, 4.753331579309927, -19.76998541567083, 
  0.01138172293572765, -0.2570682099052526, 3.590680573100387, 
  -15.79929387864297, 0.01616247951262514, -0.3751419311812992, 
  4.562621216407553, -18.69430895597448, 0.0174595932067848, 
  -0.4167864186469481, 4.979207742162901, -20.12868410872482, 
  0.01750205583130883, -0.4201771086364999, 5.024726415101656, 
  -20.42738469266181, 0.01696178101495658, -0.3996527780187634, 
  4.800157612023575, -19.69261114433477, 0.01699971626290854, 
  -0.4065591528582583, 4.902039121154386, -20.16949743077555, 
  0.01697361220173926, -0.4060652884493906, 4.899054690201257, 
  -20.16677779869221, 0.01678573318906437, -0.3968174423184244, 
  4.787455823171905, -19.73775415108052, 0.0166483789106802, 
  -0.3945787673734945, 4.777153227698085, -19.75794568246729, 
  0.0166483789106802, -0.3945787673734945, 4.777153227698085, 
  -19.75794568246729, 0.01662832613480617, -0.3942508620888461, 4.7756332995235, 
  -19.76032378278003, 0.01649140036787046, -0.3920064908270851, 
  4.765176429178698, -19.77389666642353, 0.01636080179691941, 
  -0.3898588654932456, 4.755101032068557, -19.78358391083434, 
  0.01139264370810637, -0.2573063628166208, 3.59240637555027, 
  -15.81023928701771, 0.01652352773512009, -0.4178141411162211, 
  5.180859915501394, -21.41485098970703, 0.01594432386395693, 
  -0.4075460772871262, 5.128237538960962, -21.48234196390234, 
  0.01769834254227584, -0.4356664314039516, 5.238739937408947, 
  -21.48607931647734, 0.01642252923649986, -0.414226021125283, 5.1417295140154, 
  -21.52238818021212, 0.0163866247106693, -0.4134836134716063, 5.13670422066241, 
  -21.51480512520413, 0.01723100326528765, -0.426117645295681, 
  5.174902312075997, -21.41313404755354, 0.01676520503182691, 
  -0.416597451234906, 5.111234764957458, -21.31043129953129, 
  0.01676520503182691, -0.416597451234906, 5.111234764957458, 
  -21.31043129953129, 0.01669228222435651, -0.4151069016606618, 
  5.101265721056969, -21.29337952253727, 0.01616778687629563, 
  -0.4043854597150124, 5.029554599460577, -21.16627173996594, 
  0.01563174795793794, -0.3934269765918735, 4.956251784065518, 
  -21.03101953140062, 0.01600568321247894, -0.3699248651570135, 
  4.509545427950659, -18.5024429944941, 9.575579807752718e-03, 
  -0.2854876997921853, 4.40514914469213, -20.35193456269646, 0.0139491523715259, 
  -0.3707312407207418, 4.924194321920059, -21.30546447332933, 
  0.01134131140988099, -0.3198653608990882, 4.614415577740197, 
  -20.84204172148407, 0.01132350456277812, -0.3194764112215697, 
  4.611658028139982, -20.83925421880993, 0.01362116176750284, 
  -0.3637689719188354, 4.875906488091211, -21.26291206885387, 
  0.01326783732845294, -0.3562736235057223, 4.823950767424825, 
  -21.18197618919406, 0.01326783732845294, -0.3562736235057223, 
  4.823950767424825, -21.18197618919406, 0.01321166126035343, 
  -0.3550820315982385, 4.815691762073211, -21.16805408022431, 
  0.01280372388689691, -0.3464293358677495, 4.755722270170422, 
  -21.06220103413366, 0.0123823026739862, -0.3374908314250546, 
  4.693774452640856, -20.94722048709242, 0.01738192517655597, 
  -0.4129816902240582, 4.933052551153176, -19.9408309519916, 
  0.01345541269767703, -0.3619122509759409, 4.879355828634515, 
  -21.37643592844271, 0.01053713663610162, -0.3043843984770711, 
  4.522131478315447, -20.80210760915337, 0.01052766355833828, 
  -0.3041724438249637, 4.520602981674404, -20.80221248336058, 
  0.01330002694361484, -0.3585707121706969, 4.855946692831879, 
  -21.39169084658453, 0.01307425394597843, -0.3537260960515838, 
  4.822071494950798, -21.35234588189431, 0.01307425394597843, 
  -0.3537260960515838, 4.822071494950798, -21.35234588189431, 
  0.01303642415323102, -0.3529145651524986, 4.816398359142701, 
  -21.34435355380187, 0.01275239305626985, -0.3468222688206015, 
  4.773814478300335, -21.27827621649272, 0.01244736118076681, 
  -0.3402801779457003, 4.72809173977682, -21.20029994790442, 
  0.01712216798754908, -0.4099899344319641, 4.933748560778064, 
  -20.13110614783666, 0.01458807786682745, -0.3826370390268865, 
  4.991331861562553, -21.63568659298432, 0.01456979693058862, 
  -0.3822520897812069, 4.988680641368549, -21.63338236360901, 
  0.0161192661908447, -0.4098684967504068, 5.13106890369355, -21.76867116744413, 
  0.01581454158324943, -0.4035325779556495, 5.087934817373613, 
  -21.71041313105005, 0.01581454158324943, -0.4035325779556495, 
  5.087934817373613, -21.71041313105005, 0.01576296083391036, 
  -0.4024600695996186, 5.080633206702339, -21.6990754832046, 
  0.01537335700609398, -0.3943588594626115, 5.025479260685903, 
  -21.60709639073198, 0.01495221078008985, -0.3856012650628667, 
  4.965854365178214, -21.50038308074001, 0.01689789413388545, 
  -0.3964018470322728, 4.760385853389059, -19.52276535167255, 
  0.01265785079449114, -0.346456428444864, 4.785842218222558, 
  -21.42678747389941, 0.0147306249366153, -0.3856352290690706, 
  5.011958719531333, -21.76049603388678, 0.0146379355925999, 
  -0.3836778285216462, 4.998441697245595, -21.77387972550602, 
  0.0146379355925999, -0.3836778285216462, 4.998441697245595, 
  -21.77387972550602, 0.01461414068796094, -0.3831757858468835, 
  4.994977714898364, -21.77182547378199, 0.01439360220033007, 
  -0.3785240254015475, 4.962890332793519, -21.73578748628346, 
  0.01410083021273559, -0.3723498163165907, 4.920309299196179, 
  -21.67080309713513, 0.01683645173346144, -0.4012450988474866, 
  4.847727062563259, -19.95820513461937, 0.01473199307576863, 
  -0.3856644122458766, 5.012162109711452, -21.76528061565813, 
  0.01465179430003966, -0.3839702598726567, 5.000459602666496, 
  -21.78317063907969, 0.01465179430003966, -0.3839702598726567, 
  5.000459602666496, -21.78317063907969, 0.01462962714861432, 
  -0.3835025242209622, 4.997232094240242, -21.78170686253426, 
  0.01441881906488604, -0.3790558550794542, 4.966558415746743, 
  -21.74923340086429, 0.01413297189707328, -0.3730276230930598, 
  4.924983677632214, -21.68686200523289, 0.01681618320911379, 
  -0.4008769247507338, 4.845638770971929, -19.95722192764029, 
  0.01623367035226293, -0.4122471495028369, 5.147262147654125, 
  -21.93017076163049, 0.01623367035226293, -0.4122471495028369, 
  5.147262147654125, -21.93017076163049, 0.01621005830169684, 
  -0.4117562188467453, 5.143920034136893, -21.92864921131436, 
  0.0159802881951061, -0.4069788614848662, 5.111396800906318, 
  -21.89359155718993, 0.01566226038374453, -0.4003662023243093, 
  5.066378111092643, -21.82528955758129, 0.01682594288190327, 
  -0.395829483551514, 4.76400205277652, -19.59996862327433, 0.01631381353256323, 
  -0.4139134366782187, 5.15860569476018, -22.00525993798245, 
  0.01631223411790557, -0.4138805986519443, 5.158382144825793, 
  -22.0113430228176, 0.01622117472735086, -0.4119873465466709, 
  5.145493485362663, -22.02438533180022, 0.01601239764671065, 
  -0.4076464873459705, 5.115941896228071, -21.99456726163089, 
  0.0167728239314106, -0.3953568796163239, 4.765925108535543, 
  -19.64411077239263, 0.01631223411790557, -0.4138805986519443, 
  5.158382144825793, -22.0113430228176, 0.01622117472735086, 
  -0.4119873465466709, 5.145493485362663, -22.02438533180022, 
  0.01601239764671065, -0.4076464873459705, 5.115941896228071, 
  -21.99456726163089, 0.0167728239314106, -0.3953568796163239, 
  4.765925108535543, -19.64411077239263, 0.01624344631200003, 
  -0.4124504061849023, 5.148645858123513, -22.03869711373971, 
  0.01605218054382352, -0.4084736567519756, 5.121573121509476, 
  -22.0149766380066, 0.01676522140443546, -0.3952872044638014, 
  4.766170050356854, -19.64993494875462, 0.01624975139793536, 
  -0.4125814981116693, 5.149538291961584, -22.12769739348005, 
  0.01671412406740528, -0.3948089119599393, 4.767667716880863, 
  -19.68676952794811, 0.01666646519297767, -0.3943499224517551, 
  4.768873390738809, -19.71828315609303}; 

void avx_diffusion(const double *temperature_array, const double 
  *pressure_array, const double *mass_frac_array, const double *mixmw_array, 
  const int num_elmts, const int spec_stride, double *diffusion) 
{
  __m256d *mole_frac = (__m256d*)memalign(32,45*30*sizeof(__m256d));
  __m256d *clamped = (__m256d*)memalign(32,45*30*sizeof(__m256d));
  __m256d *logt = (__m256d*)memalign(32,45*sizeof(__m256d));
  __m256d *sumxod = (__m256d*)memalign(32,45*30*sizeof(__m256d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 180)
  {
    for (unsigned idx = 0; idx < 45; idx++)
    {
      if (aligned<32>(temperature_array))
        logt[idx] = _mm256_load_pd(temperature_array);
      else
        logt[idx] = _mm256_loadu_pd(temperature_array);
      temperature_array += 4;
      if (aligned<32>(mass_frac_array))
      {
        mole_frac[0+idx] = 
          _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        mole_frac[45+idx] = 
          _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        mole_frac[90+idx] = 
          _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        mole_frac[135+idx] = 
          _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        mole_frac[180+idx] = 
          _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        mole_frac[225+idx] = 
          _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        mole_frac[270+idx] = 
          _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        mole_frac[315+idx] = 
          _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        mole_frac[360+idx] = 
          _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        mole_frac[405+idx] = 
          _mm256_load_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        mole_frac[450+idx] = 
          _mm256_load_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        mole_frac[495+idx] = 
          _mm256_load_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        mole_frac[540+idx] = 
          _mm256_load_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        mole_frac[585+idx] = 
          _mm256_load_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        mole_frac[630+idx] = 
          _mm256_load_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        mole_frac[675+idx] = 
          _mm256_load_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        mole_frac[720+idx] = 
          _mm256_load_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        mole_frac[765+idx] = 
          _mm256_load_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        mole_frac[810+idx] = 
          _mm256_load_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        mole_frac[855+idx] = 
          _mm256_load_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        mole_frac[900+idx] = 
          _mm256_load_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        mole_frac[945+idx] = 
          _mm256_load_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        mole_frac[990+idx] = 
          _mm256_load_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        mole_frac[1035+idx] = 
          _mm256_load_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        mole_frac[1080+idx] = 
          _mm256_load_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        mole_frac[1125+idx] = 
          _mm256_load_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        mole_frac[1170+idx] = 
          _mm256_load_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        mole_frac[1215+idx] = 
          _mm256_load_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        mole_frac[1260+idx] = 
          _mm256_load_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        mole_frac[1305+idx] = 
          _mm256_load_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
      }
      else
      {
        mole_frac[0+idx] = 
          _mm256_loadu_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        mole_frac[45+idx] = 
          _mm256_loadu_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        mole_frac[90+idx] = 
          _mm256_loadu_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        mole_frac[135+idx] = 
          _mm256_loadu_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        mole_frac[180+idx] = 
          _mm256_loadu_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        mole_frac[225+idx] = 
          _mm256_loadu_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        mole_frac[270+idx] = 
          _mm256_loadu_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        mole_frac[315+idx] = 
          _mm256_loadu_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        mole_frac[360+idx] = 
          _mm256_loadu_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        mole_frac[405+idx] = 
          _mm256_loadu_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        mole_frac[450+idx] = 
          _mm256_loadu_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        mole_frac[495+idx] = 
          _mm256_loadu_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        mole_frac[540+idx] = 
          _mm256_loadu_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        mole_frac[585+idx] = 
          _mm256_loadu_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        mole_frac[630+idx] = 
          _mm256_loadu_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        mole_frac[675+idx] = 
          _mm256_loadu_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        mole_frac[720+idx] = 
          _mm256_loadu_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        mole_frac[765+idx] = 
          _mm256_loadu_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        mole_frac[810+idx] = 
          _mm256_loadu_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        mole_frac[855+idx] = 
          _mm256_loadu_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        mole_frac[900+idx] = 
          _mm256_loadu_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        mole_frac[945+idx] = 
          _mm256_loadu_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        mole_frac[990+idx] = 
          _mm256_loadu_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        mole_frac[1035+idx] = 
          _mm256_loadu_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        mole_frac[1080+idx] = 
          _mm256_loadu_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        mole_frac[1125+idx] = 
          _mm256_loadu_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        mole_frac[1170+idx] = 
          _mm256_loadu_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        mole_frac[1215+idx] = 
          _mm256_loadu_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        mole_frac[1260+idx] = 
          _mm256_loadu_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        mole_frac[1305+idx] = 
          _mm256_loadu_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
      }
      __m256d mixmw;
      if (aligned<32>(mixmw_array))
        mixmw = _mm256_load_pd(mixmw_array);
      else
        mixmw = _mm256_loadu_pd(mixmw_array);
      mixmw_array += 4;
      sumxod[0+idx] = _mm256_set1_pd(0.0);
      sumxod[45+idx] = _mm256_set1_pd(0.0);
      sumxod[90+idx] = _mm256_set1_pd(0.0);
      sumxod[135+idx] = _mm256_set1_pd(0.0);
      sumxod[180+idx] = _mm256_set1_pd(0.0);
      sumxod[225+idx] = _mm256_set1_pd(0.0);
      sumxod[270+idx] = _mm256_set1_pd(0.0);
      sumxod[315+idx] = _mm256_set1_pd(0.0);
      sumxod[360+idx] = _mm256_set1_pd(0.0);
      sumxod[405+idx] = _mm256_set1_pd(0.0);
      sumxod[450+idx] = _mm256_set1_pd(0.0);
      sumxod[495+idx] = _mm256_set1_pd(0.0);
      sumxod[540+idx] = _mm256_set1_pd(0.0);
      sumxod[585+idx] = _mm256_set1_pd(0.0);
      sumxod[630+idx] = _mm256_set1_pd(0.0);
      sumxod[675+idx] = _mm256_set1_pd(0.0);
      sumxod[720+idx] = _mm256_set1_pd(0.0);
      sumxod[765+idx] = _mm256_set1_pd(0.0);
      sumxod[810+idx] = _mm256_set1_pd(0.0);
      sumxod[855+idx] = _mm256_set1_pd(0.0);
      sumxod[900+idx] = _mm256_set1_pd(0.0);
      sumxod[945+idx] = _mm256_set1_pd(0.0);
      sumxod[990+idx] = _mm256_set1_pd(0.0);
      sumxod[1035+idx] = _mm256_set1_pd(0.0);
      sumxod[1080+idx] = _mm256_set1_pd(0.0);
      sumxod[1125+idx] = _mm256_set1_pd(0.0);
      sumxod[1170+idx] = _mm256_set1_pd(0.0);
      sumxod[1215+idx] = _mm256_set1_pd(0.0);
      sumxod[1260+idx] = _mm256_set1_pd(0.0);
      sumxod[1305+idx] = _mm256_set1_pd(0.0);
      logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(120.0));
      __m128d lower = _mm256_extractf128_pd(logt[idx],0);
      __m128d upper = _mm256_extractf128_pd(logt[idx],1);
      logt[idx] = 
        _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      {
        mole_frac[0+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[0+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[0+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[0+idx] = 
          _mm256_add_pd(clamped[0+idx],_mm256_andnot_pd(mask,mole_frac[0+idx])); 
      }
      {
        mole_frac[45+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[45+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[45+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[45+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[45+idx] = 
          _mm256_add_pd(clamped[45+idx],_mm256_andnot_pd(mask,mole_frac[45+idx])); 
      }
      {
        mole_frac[90+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[90+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[90+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[90+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[90+idx] = 
          _mm256_add_pd(clamped[90+idx],_mm256_andnot_pd(mask,mole_frac[90+idx])); 
      }
      {
        mole_frac[135+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[135+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[135+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[135+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[135+idx] = 
          _mm256_add_pd(clamped[135+idx],_mm256_andnot_pd(mask,mole_frac[135+idx])); 
      }
      {
        mole_frac[180+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[180+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[180+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[180+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[180+idx] = 
          _mm256_add_pd(clamped[180+idx],_mm256_andnot_pd(mask,mole_frac[180+idx])); 
      }
      {
        mole_frac[225+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[225+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[225+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[225+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[225+idx] = 
          _mm256_add_pd(clamped[225+idx],_mm256_andnot_pd(mask,mole_frac[225+idx])); 
      }
      {
        mole_frac[270+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[270+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[270+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[270+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[270+idx] = 
          _mm256_add_pd(clamped[270+idx],_mm256_andnot_pd(mask,mole_frac[270+idx])); 
      }
      {
        mole_frac[315+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[315+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[315+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[315+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[315+idx] = 
          _mm256_add_pd(clamped[315+idx],_mm256_andnot_pd(mask,mole_frac[315+idx])); 
      }
      {
        mole_frac[360+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[360+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[360+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[360+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[360+idx] = 
          _mm256_add_pd(clamped[360+idx],_mm256_andnot_pd(mask,mole_frac[360+idx])); 
      }
      {
        mole_frac[405+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[405+idx],_mm256_set1_pd(recip_molecular_masses[9])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[405+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[405+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[405+idx] = 
          _mm256_add_pd(clamped[405+idx],_mm256_andnot_pd(mask,mole_frac[405+idx])); 
      }
      {
        mole_frac[450+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[450+idx],_mm256_set1_pd(recip_molecular_masses[10])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[450+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[450+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[450+idx] = 
          _mm256_add_pd(clamped[450+idx],_mm256_andnot_pd(mask,mole_frac[450+idx])); 
      }
      {
        mole_frac[495+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[495+idx],_mm256_set1_pd(recip_molecular_masses[11])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[495+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[495+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[495+idx] = 
          _mm256_add_pd(clamped[495+idx],_mm256_andnot_pd(mask,mole_frac[495+idx])); 
      }
      {
        mole_frac[540+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[540+idx],_mm256_set1_pd(recip_molecular_masses[12])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[540+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[540+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[540+idx] = 
          _mm256_add_pd(clamped[540+idx],_mm256_andnot_pd(mask,mole_frac[540+idx])); 
      }
      {
        mole_frac[585+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[585+idx],_mm256_set1_pd(recip_molecular_masses[13])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[585+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[585+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[585+idx] = 
          _mm256_add_pd(clamped[585+idx],_mm256_andnot_pd(mask,mole_frac[585+idx])); 
      }
      {
        mole_frac[630+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[630+idx],_mm256_set1_pd(recip_molecular_masses[14])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[630+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[630+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[630+idx] = 
          _mm256_add_pd(clamped[630+idx],_mm256_andnot_pd(mask,mole_frac[630+idx])); 
      }
      {
        mole_frac[675+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[675+idx],_mm256_set1_pd(recip_molecular_masses[15])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[675+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[675+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[675+idx] = 
          _mm256_add_pd(clamped[675+idx],_mm256_andnot_pd(mask,mole_frac[675+idx])); 
      }
      {
        mole_frac[720+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[720+idx],_mm256_set1_pd(recip_molecular_masses[16])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[720+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[720+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[720+idx] = 
          _mm256_add_pd(clamped[720+idx],_mm256_andnot_pd(mask,mole_frac[720+idx])); 
      }
      {
        mole_frac[765+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[765+idx],_mm256_set1_pd(recip_molecular_masses[17])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[765+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[765+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[765+idx] = 
          _mm256_add_pd(clamped[765+idx],_mm256_andnot_pd(mask,mole_frac[765+idx])); 
      }
      {
        mole_frac[810+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[810+idx],_mm256_set1_pd(recip_molecular_masses[18])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[810+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[810+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[810+idx] = 
          _mm256_add_pd(clamped[810+idx],_mm256_andnot_pd(mask,mole_frac[810+idx])); 
      }
      {
        mole_frac[855+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[855+idx],_mm256_set1_pd(recip_molecular_masses[19])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[855+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[855+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[855+idx] = 
          _mm256_add_pd(clamped[855+idx],_mm256_andnot_pd(mask,mole_frac[855+idx])); 
      }
      {
        mole_frac[900+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[900+idx],_mm256_set1_pd(recip_molecular_masses[20])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[900+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[900+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[900+idx] = 
          _mm256_add_pd(clamped[900+idx],_mm256_andnot_pd(mask,mole_frac[900+idx])); 
      }
      {
        mole_frac[945+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[945+idx],_mm256_set1_pd(recip_molecular_masses[21])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[945+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[945+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[945+idx] = 
          _mm256_add_pd(clamped[945+idx],_mm256_andnot_pd(mask,mole_frac[945+idx])); 
      }
      {
        mole_frac[990+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[990+idx],_mm256_set1_pd(recip_molecular_masses[22])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[990+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[990+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[990+idx] = 
          _mm256_add_pd(clamped[990+idx],_mm256_andnot_pd(mask,mole_frac[990+idx])); 
      }
      {
        mole_frac[1035+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1035+idx],_mm256_set1_pd(recip_molecular_masses[23])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1035+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1035+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1035+idx] = 
          _mm256_add_pd(clamped[1035+idx],_mm256_andnot_pd(mask,mole_frac[1035+idx])); 
      }
      {
        mole_frac[1080+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1080+idx],_mm256_set1_pd(recip_molecular_masses[24])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1080+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1080+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1080+idx] = 
          _mm256_add_pd(clamped[1080+idx],_mm256_andnot_pd(mask,mole_frac[1080+idx])); 
      }
      {
        mole_frac[1125+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1125+idx],_mm256_set1_pd(recip_molecular_masses[25])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1125+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1125+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1125+idx] = 
          _mm256_add_pd(clamped[1125+idx],_mm256_andnot_pd(mask,mole_frac[1125+idx])); 
      }
      {
        mole_frac[1170+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1170+idx],_mm256_set1_pd(recip_molecular_masses[26])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1170+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1170+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1170+idx] = 
          _mm256_add_pd(clamped[1170+idx],_mm256_andnot_pd(mask,mole_frac[1170+idx])); 
      }
      {
        mole_frac[1215+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1215+idx],_mm256_set1_pd(recip_molecular_masses[27])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1215+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1215+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1215+idx] = 
          _mm256_add_pd(clamped[1215+idx],_mm256_andnot_pd(mask,mole_frac[1215+idx])); 
      }
      {
        mole_frac[1260+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1260+idx],_mm256_set1_pd(recip_molecular_masses[28])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1260+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1260+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1260+idx] = 
          _mm256_add_pd(clamped[1260+idx],_mm256_andnot_pd(mask,mole_frac[1260+idx])); 
      }
      {
        mole_frac[1305+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1305+idx],_mm256_set1_pd(recip_molecular_masses[29])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1305+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1305+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1305+idx] = 
          _mm256_add_pd(clamped[1305+idx],_mm256_andnot_pd(mask,mole_frac[1305+idx])); 
      }
    }
    int diff_off = 0;
    for (int k = 0; k < 30; k++)
    {
      for (int j = k+1; j < 30; j++)
      {
        __m256d c0 = _mm256_set1_pd(diff_constants[diff_off+0]);
        __m256d c1 = _mm256_set1_pd(diff_constants[diff_off+1]);
        __m256d c2 = _mm256_set1_pd(diff_constants[diff_off+2]);
        __m256d c3 = _mm256_set1_pd(diff_constants[diff_off+3]);
        diff_off += 4;
        __m256d val;
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[0]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[0]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[0]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+0] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+0],val),sumxod[k*45+0]); 
        sumxod[j*45+0] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+0],val),sumxod[j*45+0]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[1]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[1]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[1]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+1] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+1],val),sumxod[k*45+1]); 
        sumxod[j*45+1] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+1],val),sumxod[j*45+1]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[2]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[2]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[2]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+2] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+2],val),sumxod[k*45+2]); 
        sumxod[j*45+2] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+2],val),sumxod[j*45+2]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[3]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[3]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[3]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+3] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+3],val),sumxod[k*45+3]); 
        sumxod[j*45+3] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+3],val),sumxod[j*45+3]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[4]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[4]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[4]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+4] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+4],val),sumxod[k*45+4]); 
        sumxod[j*45+4] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+4],val),sumxod[j*45+4]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[5]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[5]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[5]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+5] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+5],val),sumxod[k*45+5]); 
        sumxod[j*45+5] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+5],val),sumxod[j*45+5]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[6]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[6]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[6]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+6] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+6],val),sumxod[k*45+6]); 
        sumxod[j*45+6] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+6],val),sumxod[j*45+6]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[7]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[7]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[7]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+7] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+7],val),sumxod[k*45+7]); 
        sumxod[j*45+7] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+7],val),sumxod[j*45+7]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[8]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[8]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[8]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+8] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+8],val),sumxod[k*45+8]); 
        sumxod[j*45+8] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+8],val),sumxod[j*45+8]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[9]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[9]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[9]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+9] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+9],val),sumxod[k*45+9]); 
        sumxod[j*45+9] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+9],val),sumxod[j*45+9]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[10]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[10]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[10]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+10] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+10],val),sumxod[k*45+10]); 
        sumxod[j*45+10] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+10],val),sumxod[j*45+10]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[11]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[11]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[11]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+11] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+11],val),sumxod[k*45+11]); 
        sumxod[j*45+11] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+11],val),sumxod[j*45+11]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[12]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[12]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[12]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+12] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+12],val),sumxod[k*45+12]); 
        sumxod[j*45+12] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+12],val),sumxod[j*45+12]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[13]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[13]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[13]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+13] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+13],val),sumxod[k*45+13]); 
        sumxod[j*45+13] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+13],val),sumxod[j*45+13]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[14]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[14]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[14]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+14] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+14],val),sumxod[k*45+14]); 
        sumxod[j*45+14] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+14],val),sumxod[j*45+14]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[15]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[15]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[15]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+15] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+15],val),sumxod[k*45+15]); 
        sumxod[j*45+15] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+15],val),sumxod[j*45+15]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[16]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[16]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[16]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+16] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+16],val),sumxod[k*45+16]); 
        sumxod[j*45+16] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+16],val),sumxod[j*45+16]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[17]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[17]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[17]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+17] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+17],val),sumxod[k*45+17]); 
        sumxod[j*45+17] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+17],val),sumxod[j*45+17]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[18]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[18]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[18]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+18] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+18],val),sumxod[k*45+18]); 
        sumxod[j*45+18] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+18],val),sumxod[j*45+18]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[19]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[19]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[19]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+19] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+19],val),sumxod[k*45+19]); 
        sumxod[j*45+19] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+19],val),sumxod[j*45+19]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[20]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[20]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[20]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+20] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+20],val),sumxod[k*45+20]); 
        sumxod[j*45+20] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+20],val),sumxod[j*45+20]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[21]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[21]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[21]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+21] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+21],val),sumxod[k*45+21]); 
        sumxod[j*45+21] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+21],val),sumxod[j*45+21]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[22]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[22]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[22]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+22] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+22],val),sumxod[k*45+22]); 
        sumxod[j*45+22] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+22],val),sumxod[j*45+22]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[23]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[23]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[23]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+23] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+23],val),sumxod[k*45+23]); 
        sumxod[j*45+23] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+23],val),sumxod[j*45+23]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[24]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[24]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[24]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+24] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+24],val),sumxod[k*45+24]); 
        sumxod[j*45+24] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+24],val),sumxod[j*45+24]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[25]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[25]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[25]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+25] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+25],val),sumxod[k*45+25]); 
        sumxod[j*45+25] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+25],val),sumxod[j*45+25]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[26]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[26]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[26]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+26] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+26],val),sumxod[k*45+26]); 
        sumxod[j*45+26] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+26],val),sumxod[j*45+26]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[27]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[27]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[27]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+27] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+27],val),sumxod[k*45+27]); 
        sumxod[j*45+27] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+27],val),sumxod[j*45+27]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[28]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[28]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[28]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+28] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+28],val),sumxod[k*45+28]); 
        sumxod[j*45+28] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+28],val),sumxod[j*45+28]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[29]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[29]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[29]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+29] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+29],val),sumxod[k*45+29]); 
        sumxod[j*45+29] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+29],val),sumxod[j*45+29]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[30]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[30]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[30]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+30] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+30],val),sumxod[k*45+30]); 
        sumxod[j*45+30] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+30],val),sumxod[j*45+30]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[31]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[31]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[31]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+31] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+31],val),sumxod[k*45+31]); 
        sumxod[j*45+31] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+31],val),sumxod[j*45+31]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[32]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[32]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[32]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+32] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+32],val),sumxod[k*45+32]); 
        sumxod[j*45+32] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+32],val),sumxod[j*45+32]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[33]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[33]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[33]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+33] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+33],val),sumxod[k*45+33]); 
        sumxod[j*45+33] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+33],val),sumxod[j*45+33]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[34]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[34]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[34]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+34] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+34],val),sumxod[k*45+34]); 
        sumxod[j*45+34] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+34],val),sumxod[j*45+34]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[35]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[35]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[35]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+35] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+35],val),sumxod[k*45+35]); 
        sumxod[j*45+35] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+35],val),sumxod[j*45+35]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[36]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[36]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[36]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+36] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+36],val),sumxod[k*45+36]); 
        sumxod[j*45+36] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+36],val),sumxod[j*45+36]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[37]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[37]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[37]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+37] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+37],val),sumxod[k*45+37]); 
        sumxod[j*45+37] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+37],val),sumxod[j*45+37]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[38]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[38]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[38]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+38] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+38],val),sumxod[k*45+38]); 
        sumxod[j*45+38] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+38],val),sumxod[j*45+38]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[39]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[39]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[39]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+39] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+39],val),sumxod[k*45+39]); 
        sumxod[j*45+39] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+39],val),sumxod[j*45+39]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[40]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[40]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[40]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+40] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+40],val),sumxod[k*45+40]); 
        sumxod[j*45+40] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+40],val),sumxod[j*45+40]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[41]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[41]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[41]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+41] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+41],val),sumxod[k*45+41]); 
        sumxod[j*45+41] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+41],val),sumxod[j*45+41]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[42]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[42]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[42]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+42] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+42],val),sumxod[k*45+42]); 
        sumxod[j*45+42] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+42],val),sumxod[j*45+42]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[43]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[43]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[43]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+43] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+43],val),sumxod[k*45+43]); 
        sumxod[j*45+43] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+43],val),sumxod[j*45+43]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[44]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[44]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[44]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+44] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+44],val),sumxod[k*45+44]); 
        sumxod[j*45+44] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+44],val),sumxod[j*45+44]); 
      }
    }
    for (unsigned idx = 0; idx < 45; idx++)
    {
      __m256d sumxw = _mm256_set1_pd(0.0);
      __m256d wtm = _mm256_set1_pd(0.0);
      __m256d pressure;
      if (aligned<32>(pressure_array))
        pressure = _mm256_load_pd(pressure_array);
      else
        pressure = _mm256_loadu_pd(pressure_array);
      pressure_array += 4;
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],_mm256_set1_pd(1.00797)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(1.00797)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[45+idx],_mm256_set1_pd(2.01594)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[45+idx],_mm256_set1_pd(2.01594)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[90+idx],_mm256_set1_pd(15.03506)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[90+idx],_mm256_set1_pd(15.03506)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[135+idx],_mm256_set1_pd(15.9994)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[135+idx],_mm256_set1_pd(15.9994)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[180+idx],_mm256_set1_pd(16.04303)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[180+idx],_mm256_set1_pd(16.04303)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[225+idx],_mm256_set1_pd(17.00737)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[225+idx],_mm256_set1_pd(17.00737)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[270+idx],_mm256_set1_pd(18.01534)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[270+idx],_mm256_set1_pd(18.01534)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[315+idx],_mm256_set1_pd(26.03824)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[315+idx],_mm256_set1_pd(26.03824)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[360+idx],_mm256_set1_pd(28.01055)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[360+idx],_mm256_set1_pd(28.01055)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[405+idx],_mm256_set1_pd(28.05418)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[405+idx],_mm256_set1_pd(28.05418)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[450+idx],_mm256_set1_pd(29.06215)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[450+idx],_mm256_set1_pd(29.06215)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[495+idx],_mm256_set1_pd(30.02649)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[495+idx],_mm256_set1_pd(30.02649)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[540+idx],_mm256_set1_pd(30.07012)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[540+idx],_mm256_set1_pd(30.07012)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[585+idx],_mm256_set1_pd(31.03446)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[585+idx],_mm256_set1_pd(31.03446)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[630+idx],_mm256_set1_pd(31.9988)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[630+idx],_mm256_set1_pd(31.9988)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[675+idx],_mm256_set1_pd(33.00677)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[675+idx],_mm256_set1_pd(33.00677)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[720+idx],_mm256_set1_pd(34.01474)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[720+idx],_mm256_set1_pd(34.01474)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[765+idx],_mm256_set1_pd(44.00995)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[765+idx],_mm256_set1_pd(44.00995)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[810+idx],_mm256_set1_pd(44.05358)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[810+idx],_mm256_set1_pd(44.05358)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[855+idx],_mm256_set1_pd(46.02589)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[855+idx],_mm256_set1_pd(46.02589)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[900+idx],_mm256_set1_pd(46.06952)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[900+idx],_mm256_set1_pd(46.06952)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[945+idx],_mm256_set1_pd(59.04501)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[945+idx],_mm256_set1_pd(59.04501)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[990+idx],_mm256_set1_pd(60.05298000000001)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[990+idx],_mm256_set1_pd(60.05298000000001)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1035+idx],_mm256_set1_pd(62.06892000000001)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1035+idx],_mm256_set1_pd(62.06892000000001)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1080+idx],_mm256_set1_pd(75.04441)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1080+idx],_mm256_set1_pd(75.04441)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1125+idx],_mm256_set1_pd(75.04441)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1125+idx],_mm256_set1_pd(75.04441)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1170+idx],_mm256_set1_pd(77.06035)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1170+idx],_mm256_set1_pd(77.06035)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1215+idx],_mm256_set1_pd(92.05178000000001)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1215+idx],_mm256_set1_pd(92.05178000000001)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1260+idx],_mm256_set1_pd(109.05915)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1260+idx],_mm256_set1_pd(109.05915)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1305+idx],_mm256_set1_pd(28.0134)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1305+idx],_mm256_set1_pd(28.0134)),wtm); 
      pressure = _mm256_mul_pd(pressure,_mm256_set1_pd(1.41836588544e+06));
      __m256d pfac = _mm256_div_pd(_mm256_set1_pd(1.01325e+06),pressure);
      if (aligned<32>(diffusion))
      {
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(1.00797),clamped[0+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[0+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(0*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(2.01594),clamped[45+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[45+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(1*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(15.03506),clamped[90+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[90+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(2*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(15.9994),clamped[135+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[135+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(3*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(16.04303),clamped[180+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[180+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(4*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(17.00737),clamped[225+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[225+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(5*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(18.01534),clamped[270+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[270+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(6*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(26.03824),clamped[315+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[315+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(7*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(28.01055),clamped[360+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[360+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(8*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(28.05418),clamped[405+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[405+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(9*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(29.06215),clamped[450+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[450+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(10*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(30.02649),clamped[495+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[495+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(11*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(30.07012),clamped[540+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[540+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(12*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(31.03446),clamped[585+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[585+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(13*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(31.9988),clamped[630+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[630+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(14*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(33.00677),clamped[675+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[675+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(15*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(34.01474),clamped[720+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[720+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(16*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(44.00995),clamped[765+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[765+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(17*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(44.05358),clamped[810+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[810+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(18*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(46.02589),clamped[855+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[855+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(19*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(46.06952),clamped[900+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[900+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(20*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(59.04501),clamped[945+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[945+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(21*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(60.05298000000001),clamped[990+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[990+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(22*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(62.06892000000001),clamped[1035+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1035+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(23*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(75.04441),clamped[1080+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1080+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(24*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(75.04441),clamped[1125+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1125+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(25*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(77.06035),clamped[1170+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1170+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(26*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(92.05178000000001),clamped[1215+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1215+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(27*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(109.05915),clamped[1260+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1260+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(28*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(28.0134),clamped[1305+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1305+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(29*spec_stride)+(idx<<2),result);
        }
      }
      else
      {
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(145.0),clamped[1350+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1350+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(30*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(38.0),clamped[1395+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1395+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(31*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(144.0),clamped[1440+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1440+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(32*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(80.0),clamped[1485+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1485+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(33*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(141.4),clamped[1530+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1530+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(34*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(80.0),clamped[1575+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1575+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(35*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(572.4),clamped[1620+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1620+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(36*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(265.3),clamped[1665+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1665+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(37*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(98.09999999999999),clamped[1710+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1710+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(38*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(238.4),clamped[1755+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1755+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(39*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(247.5),clamped[1800+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1800+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(40*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(498.0),clamped[1845+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1845+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(41*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(247.5),clamped[1890+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1890+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(42*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(417.0),clamped[1935+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1935+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(43*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(107.4),clamped[1980+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1980+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(44*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(107.4),clamped[2025+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2025+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(45*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(107.4),clamped[2070+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2070+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(46*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(244.0),clamped[2115+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2115+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(47*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(436.0),clamped[2160+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2160+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(48*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(470.6),clamped[2205+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2205+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(49*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2250+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2250+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(50*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(406.5),clamped[2295+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2295+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(51*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(406.5),clamped[2340+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2340+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(52*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2385+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2385+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(53*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2430+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2430+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(54*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2475+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2475+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(55*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2520+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2520+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(56*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2565+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2565+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(57*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2610+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2610+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(58*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(97.53),clamped[2655+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2655+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(59*spec_stride)+(idx<<2),result);
        }
      }
    }
    remaining_elmts -= 180;
    mass_frac_array += 180;
    diffusion += 180;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 4) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      if (aligned<32>(temperature_array))
        logt[idx] = _mm256_load_pd(temperature_array);
      else
        logt[idx] = _mm256_loadu_pd(temperature_array);
      temperature_array += 4;
      if (aligned<32>(mass_frac_array))
      {
        mole_frac[0+idx] = 
          _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        mole_frac[45+idx] = 
          _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        mole_frac[90+idx] = 
          _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        mole_frac[135+idx] = 
          _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        mole_frac[180+idx] = 
          _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        mole_frac[225+idx] = 
          _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        mole_frac[270+idx] = 
          _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        mole_frac[315+idx] = 
          _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        mole_frac[360+idx] = 
          _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        mole_frac[405+idx] = 
          _mm256_load_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        mole_frac[450+idx] = 
          _mm256_load_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        mole_frac[495+idx] = 
          _mm256_load_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        mole_frac[540+idx] = 
          _mm256_load_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        mole_frac[585+idx] = 
          _mm256_load_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        mole_frac[630+idx] = 
          _mm256_load_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        mole_frac[675+idx] = 
          _mm256_load_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        mole_frac[720+idx] = 
          _mm256_load_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        mole_frac[765+idx] = 
          _mm256_load_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        mole_frac[810+idx] = 
          _mm256_load_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        mole_frac[855+idx] = 
          _mm256_load_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        mole_frac[900+idx] = 
          _mm256_load_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        mole_frac[945+idx] = 
          _mm256_load_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        mole_frac[990+idx] = 
          _mm256_load_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        mole_frac[1035+idx] = 
          _mm256_load_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        mole_frac[1080+idx] = 
          _mm256_load_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        mole_frac[1125+idx] = 
          _mm256_load_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        mole_frac[1170+idx] = 
          _mm256_load_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        mole_frac[1215+idx] = 
          _mm256_load_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        mole_frac[1260+idx] = 
          _mm256_load_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        mole_frac[1305+idx] = 
          _mm256_load_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
      }
      else
      {
        mole_frac[0+idx] = 
          _mm256_loadu_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        mole_frac[45+idx] = 
          _mm256_loadu_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        mole_frac[90+idx] = 
          _mm256_loadu_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        mole_frac[135+idx] = 
          _mm256_loadu_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        mole_frac[180+idx] = 
          _mm256_loadu_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        mole_frac[225+idx] = 
          _mm256_loadu_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        mole_frac[270+idx] = 
          _mm256_loadu_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        mole_frac[315+idx] = 
          _mm256_loadu_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        mole_frac[360+idx] = 
          _mm256_loadu_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        mole_frac[405+idx] = 
          _mm256_loadu_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        mole_frac[450+idx] = 
          _mm256_loadu_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        mole_frac[495+idx] = 
          _mm256_loadu_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        mole_frac[540+idx] = 
          _mm256_loadu_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        mole_frac[585+idx] = 
          _mm256_loadu_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        mole_frac[630+idx] = 
          _mm256_loadu_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        mole_frac[675+idx] = 
          _mm256_loadu_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        mole_frac[720+idx] = 
          _mm256_loadu_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        mole_frac[765+idx] = 
          _mm256_loadu_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        mole_frac[810+idx] = 
          _mm256_loadu_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        mole_frac[855+idx] = 
          _mm256_loadu_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        mole_frac[900+idx] = 
          _mm256_loadu_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        mole_frac[945+idx] = 
          _mm256_loadu_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        mole_frac[990+idx] = 
          _mm256_loadu_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        mole_frac[1035+idx] = 
          _mm256_loadu_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        mole_frac[1080+idx] = 
          _mm256_loadu_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        mole_frac[1125+idx] = 
          _mm256_loadu_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        mole_frac[1170+idx] = 
          _mm256_loadu_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        mole_frac[1215+idx] = 
          _mm256_loadu_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        mole_frac[1260+idx] = 
          _mm256_loadu_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        mole_frac[1305+idx] = 
          _mm256_loadu_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
      }
      __m256d mixmw;
      if (aligned<32>(mixmw_array))
        mixmw = _mm256_load_pd(mixmw_array);
      else
        mixmw = _mm256_loadu_pd(mixmw_array);
      mixmw_array += 4;
      sumxod[0+idx] = _mm256_set1_pd(0.0);
      sumxod[45+idx] = _mm256_set1_pd(0.0);
      sumxod[90+idx] = _mm256_set1_pd(0.0);
      sumxod[135+idx] = _mm256_set1_pd(0.0);
      sumxod[180+idx] = _mm256_set1_pd(0.0);
      sumxod[225+idx] = _mm256_set1_pd(0.0);
      sumxod[270+idx] = _mm256_set1_pd(0.0);
      sumxod[315+idx] = _mm256_set1_pd(0.0);
      sumxod[360+idx] = _mm256_set1_pd(0.0);
      sumxod[405+idx] = _mm256_set1_pd(0.0);
      sumxod[450+idx] = _mm256_set1_pd(0.0);
      sumxod[495+idx] = _mm256_set1_pd(0.0);
      sumxod[540+idx] = _mm256_set1_pd(0.0);
      sumxod[585+idx] = _mm256_set1_pd(0.0);
      sumxod[630+idx] = _mm256_set1_pd(0.0);
      sumxod[675+idx] = _mm256_set1_pd(0.0);
      sumxod[720+idx] = _mm256_set1_pd(0.0);
      sumxod[765+idx] = _mm256_set1_pd(0.0);
      sumxod[810+idx] = _mm256_set1_pd(0.0);
      sumxod[855+idx] = _mm256_set1_pd(0.0);
      sumxod[900+idx] = _mm256_set1_pd(0.0);
      sumxod[945+idx] = _mm256_set1_pd(0.0);
      sumxod[990+idx] = _mm256_set1_pd(0.0);
      sumxod[1035+idx] = _mm256_set1_pd(0.0);
      sumxod[1080+idx] = _mm256_set1_pd(0.0);
      sumxod[1125+idx] = _mm256_set1_pd(0.0);
      sumxod[1170+idx] = _mm256_set1_pd(0.0);
      sumxod[1215+idx] = _mm256_set1_pd(0.0);
      sumxod[1260+idx] = _mm256_set1_pd(0.0);
      sumxod[1305+idx] = _mm256_set1_pd(0.0);
      logt[idx] = _mm256_mul_pd(logt[idx],_mm256_set1_pd(120.0));
      __m128d lower = _mm256_extractf128_pd(logt[idx],0);
      __m128d upper = _mm256_extractf128_pd(logt[idx],1);
      logt[idx] = 
        _mm256_set_pd(log(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),log(_mm_cvtsd_f64(upper)),log(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),log(_mm_cvtsd_f64(lower))); 
      {
        mole_frac[0+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[0+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[0+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[0+idx] = 
          _mm256_add_pd(clamped[0+idx],_mm256_andnot_pd(mask,mole_frac[0+idx])); 
      }
      {
        mole_frac[45+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[45+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[45+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[45+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[45+idx] = 
          _mm256_add_pd(clamped[45+idx],_mm256_andnot_pd(mask,mole_frac[45+idx])); 
      }
      {
        mole_frac[90+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[90+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[90+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[90+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[90+idx] = 
          _mm256_add_pd(clamped[90+idx],_mm256_andnot_pd(mask,mole_frac[90+idx])); 
      }
      {
        mole_frac[135+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[135+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[135+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[135+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[135+idx] = 
          _mm256_add_pd(clamped[135+idx],_mm256_andnot_pd(mask,mole_frac[135+idx])); 
      }
      {
        mole_frac[180+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[180+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[180+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[180+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[180+idx] = 
          _mm256_add_pd(clamped[180+idx],_mm256_andnot_pd(mask,mole_frac[180+idx])); 
      }
      {
        mole_frac[225+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[225+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[225+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[225+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[225+idx] = 
          _mm256_add_pd(clamped[225+idx],_mm256_andnot_pd(mask,mole_frac[225+idx])); 
      }
      {
        mole_frac[270+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[270+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[270+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[270+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[270+idx] = 
          _mm256_add_pd(clamped[270+idx],_mm256_andnot_pd(mask,mole_frac[270+idx])); 
      }
      {
        mole_frac[315+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[315+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[315+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[315+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[315+idx] = 
          _mm256_add_pd(clamped[315+idx],_mm256_andnot_pd(mask,mole_frac[315+idx])); 
      }
      {
        mole_frac[360+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[360+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[360+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[360+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[360+idx] = 
          _mm256_add_pd(clamped[360+idx],_mm256_andnot_pd(mask,mole_frac[360+idx])); 
      }
      {
        mole_frac[405+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[405+idx],_mm256_set1_pd(recip_molecular_masses[9])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[405+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[405+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[405+idx] = 
          _mm256_add_pd(clamped[405+idx],_mm256_andnot_pd(mask,mole_frac[405+idx])); 
      }
      {
        mole_frac[450+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[450+idx],_mm256_set1_pd(recip_molecular_masses[10])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[450+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[450+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[450+idx] = 
          _mm256_add_pd(clamped[450+idx],_mm256_andnot_pd(mask,mole_frac[450+idx])); 
      }
      {
        mole_frac[495+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[495+idx],_mm256_set1_pd(recip_molecular_masses[11])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[495+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[495+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[495+idx] = 
          _mm256_add_pd(clamped[495+idx],_mm256_andnot_pd(mask,mole_frac[495+idx])); 
      }
      {
        mole_frac[540+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[540+idx],_mm256_set1_pd(recip_molecular_masses[12])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[540+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[540+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[540+idx] = 
          _mm256_add_pd(clamped[540+idx],_mm256_andnot_pd(mask,mole_frac[540+idx])); 
      }
      {
        mole_frac[585+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[585+idx],_mm256_set1_pd(recip_molecular_masses[13])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[585+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[585+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[585+idx] = 
          _mm256_add_pd(clamped[585+idx],_mm256_andnot_pd(mask,mole_frac[585+idx])); 
      }
      {
        mole_frac[630+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[630+idx],_mm256_set1_pd(recip_molecular_masses[14])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[630+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[630+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[630+idx] = 
          _mm256_add_pd(clamped[630+idx],_mm256_andnot_pd(mask,mole_frac[630+idx])); 
      }
      {
        mole_frac[675+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[675+idx],_mm256_set1_pd(recip_molecular_masses[15])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[675+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[675+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[675+idx] = 
          _mm256_add_pd(clamped[675+idx],_mm256_andnot_pd(mask,mole_frac[675+idx])); 
      }
      {
        mole_frac[720+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[720+idx],_mm256_set1_pd(recip_molecular_masses[16])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[720+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[720+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[720+idx] = 
          _mm256_add_pd(clamped[720+idx],_mm256_andnot_pd(mask,mole_frac[720+idx])); 
      }
      {
        mole_frac[765+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[765+idx],_mm256_set1_pd(recip_molecular_masses[17])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[765+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[765+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[765+idx] = 
          _mm256_add_pd(clamped[765+idx],_mm256_andnot_pd(mask,mole_frac[765+idx])); 
      }
      {
        mole_frac[810+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[810+idx],_mm256_set1_pd(recip_molecular_masses[18])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[810+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[810+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[810+idx] = 
          _mm256_add_pd(clamped[810+idx],_mm256_andnot_pd(mask,mole_frac[810+idx])); 
      }
      {
        mole_frac[855+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[855+idx],_mm256_set1_pd(recip_molecular_masses[19])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[855+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[855+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[855+idx] = 
          _mm256_add_pd(clamped[855+idx],_mm256_andnot_pd(mask,mole_frac[855+idx])); 
      }
      {
        mole_frac[900+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[900+idx],_mm256_set1_pd(recip_molecular_masses[20])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[900+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[900+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[900+idx] = 
          _mm256_add_pd(clamped[900+idx],_mm256_andnot_pd(mask,mole_frac[900+idx])); 
      }
      {
        mole_frac[945+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[945+idx],_mm256_set1_pd(recip_molecular_masses[21])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[945+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[945+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[945+idx] = 
          _mm256_add_pd(clamped[945+idx],_mm256_andnot_pd(mask,mole_frac[945+idx])); 
      }
      {
        mole_frac[990+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[990+idx],_mm256_set1_pd(recip_molecular_masses[22])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[990+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[990+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[990+idx] = 
          _mm256_add_pd(clamped[990+idx],_mm256_andnot_pd(mask,mole_frac[990+idx])); 
      }
      {
        mole_frac[1035+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1035+idx],_mm256_set1_pd(recip_molecular_masses[23])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1035+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1035+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1035+idx] = 
          _mm256_add_pd(clamped[1035+idx],_mm256_andnot_pd(mask,mole_frac[1035+idx])); 
      }
      {
        mole_frac[1080+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1080+idx],_mm256_set1_pd(recip_molecular_masses[24])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1080+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1080+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1080+idx] = 
          _mm256_add_pd(clamped[1080+idx],_mm256_andnot_pd(mask,mole_frac[1080+idx])); 
      }
      {
        mole_frac[1125+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1125+idx],_mm256_set1_pd(recip_molecular_masses[25])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1125+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1125+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1125+idx] = 
          _mm256_add_pd(clamped[1125+idx],_mm256_andnot_pd(mask,mole_frac[1125+idx])); 
      }
      {
        mole_frac[1170+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1170+idx],_mm256_set1_pd(recip_molecular_masses[26])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1170+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1170+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1170+idx] = 
          _mm256_add_pd(clamped[1170+idx],_mm256_andnot_pd(mask,mole_frac[1170+idx])); 
      }
      {
        mole_frac[1215+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1215+idx],_mm256_set1_pd(recip_molecular_masses[27])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1215+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1215+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1215+idx] = 
          _mm256_add_pd(clamped[1215+idx],_mm256_andnot_pd(mask,mole_frac[1215+idx])); 
      }
      {
        mole_frac[1260+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1260+idx],_mm256_set1_pd(recip_molecular_masses[28])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1260+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1260+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1260+idx] = 
          _mm256_add_pd(clamped[1260+idx],_mm256_andnot_pd(mask,mole_frac[1260+idx])); 
      }
      {
        mole_frac[1305+idx] = 
          _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1305+idx],_mm256_set1_pd(recip_molecular_masses[29])),_mm256_set1_pd(1e3)),mixmw); 
        __m256d mask = 
          _mm256_cmp_pd(mole_frac[1305+idx],_mm256_set1_pd(9.999999999999999e-21),_CMP_LT_OQ); 
        clamped[1305+idx] = 
          _mm256_and_pd(mask,_mm256_set1_pd(9.999999999999999e-21)); 
        clamped[1305+idx] = 
          _mm256_add_pd(clamped[1305+idx],_mm256_andnot_pd(mask,mole_frac[1305+idx])); 
      }
    }
    int diff_off = 0;
    for (int k = 0; k < 30; k++)
    {
      for (int j = k+1; j < 30; j++)
      {
        __m256d c0 = _mm256_set1_pd(diff_constants[diff_off+0]);
        __m256d c1 = _mm256_set1_pd(diff_constants[diff_off+1]);
        __m256d c2 = _mm256_set1_pd(diff_constants[diff_off+2]);
        __m256d c3 = _mm256_set1_pd(diff_constants[diff_off+3]);
        diff_off += 4;
        __m256d val;
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[0]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[0]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[0]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+0] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+0],val),sumxod[k*45+0]); 
        sumxod[j*45+0] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+0],val),sumxod[j*45+0]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[1]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[1]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[1]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+1] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+1],val),sumxod[k*45+1]); 
        sumxod[j*45+1] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+1],val),sumxod[j*45+1]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[2]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[2]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[2]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+2] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+2],val),sumxod[k*45+2]); 
        sumxod[j*45+2] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+2],val),sumxod[j*45+2]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[3]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[3]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[3]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+3] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+3],val),sumxod[k*45+3]); 
        sumxod[j*45+3] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+3],val),sumxod[j*45+3]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[4]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[4]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[4]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+4] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+4],val),sumxod[k*45+4]); 
        sumxod[j*45+4] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+4],val),sumxod[j*45+4]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[5]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[5]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[5]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+5] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+5],val),sumxod[k*45+5]); 
        sumxod[j*45+5] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+5],val),sumxod[j*45+5]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[6]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[6]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[6]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+6] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+6],val),sumxod[k*45+6]); 
        sumxod[j*45+6] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+6],val),sumxod[j*45+6]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[7]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[7]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[7]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+7] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+7],val),sumxod[k*45+7]); 
        sumxod[j*45+7] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+7],val),sumxod[j*45+7]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[8]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[8]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[8]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+8] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+8],val),sumxod[k*45+8]); 
        sumxod[j*45+8] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+8],val),sumxod[j*45+8]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[9]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[9]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[9]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+9] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+9],val),sumxod[k*45+9]); 
        sumxod[j*45+9] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+9],val),sumxod[j*45+9]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[10]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[10]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[10]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+10] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+10],val),sumxod[k*45+10]); 
        sumxod[j*45+10] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+10],val),sumxod[j*45+10]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[11]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[11]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[11]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+11] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+11],val),sumxod[k*45+11]); 
        sumxod[j*45+11] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+11],val),sumxod[j*45+11]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[12]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[12]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[12]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+12] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+12],val),sumxod[k*45+12]); 
        sumxod[j*45+12] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+12],val),sumxod[j*45+12]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[13]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[13]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[13]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+13] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+13],val),sumxod[k*45+13]); 
        sumxod[j*45+13] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+13],val),sumxod[j*45+13]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[14]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[14]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[14]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+14] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+14],val),sumxod[k*45+14]); 
        sumxod[j*45+14] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+14],val),sumxod[j*45+14]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[15]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[15]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[15]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+15] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+15],val),sumxod[k*45+15]); 
        sumxod[j*45+15] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+15],val),sumxod[j*45+15]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[16]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[16]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[16]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+16] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+16],val),sumxod[k*45+16]); 
        sumxod[j*45+16] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+16],val),sumxod[j*45+16]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[17]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[17]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[17]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+17] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+17],val),sumxod[k*45+17]); 
        sumxod[j*45+17] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+17],val),sumxod[j*45+17]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[18]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[18]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[18]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+18] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+18],val),sumxod[k*45+18]); 
        sumxod[j*45+18] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+18],val),sumxod[j*45+18]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[19]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[19]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[19]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+19] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+19],val),sumxod[k*45+19]); 
        sumxod[j*45+19] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+19],val),sumxod[j*45+19]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[20]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[20]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[20]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+20] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+20],val),sumxod[k*45+20]); 
        sumxod[j*45+20] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+20],val),sumxod[j*45+20]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[21]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[21]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[21]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+21] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+21],val),sumxod[k*45+21]); 
        sumxod[j*45+21] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+21],val),sumxod[j*45+21]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[22]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[22]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[22]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+22] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+22],val),sumxod[k*45+22]); 
        sumxod[j*45+22] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+22],val),sumxod[j*45+22]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[23]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[23]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[23]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+23] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+23],val),sumxod[k*45+23]); 
        sumxod[j*45+23] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+23],val),sumxod[j*45+23]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[24]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[24]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[24]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+24] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+24],val),sumxod[k*45+24]); 
        sumxod[j*45+24] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+24],val),sumxod[j*45+24]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[25]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[25]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[25]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+25] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+25],val),sumxod[k*45+25]); 
        sumxod[j*45+25] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+25],val),sumxod[j*45+25]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[26]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[26]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[26]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+26] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+26],val),sumxod[k*45+26]); 
        sumxod[j*45+26] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+26],val),sumxod[j*45+26]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[27]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[27]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[27]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+27] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+27],val),sumxod[k*45+27]); 
        sumxod[j*45+27] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+27],val),sumxod[j*45+27]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[28]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[28]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[28]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+28] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+28],val),sumxod[k*45+28]); 
        sumxod[j*45+28] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+28],val),sumxod[j*45+28]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[29]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[29]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[29]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+29] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+29],val),sumxod[k*45+29]); 
        sumxod[j*45+29] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+29],val),sumxod[j*45+29]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[30]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[30]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[30]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+30] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+30],val),sumxod[k*45+30]); 
        sumxod[j*45+30] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+30],val),sumxod[j*45+30]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[31]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[31]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[31]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+31] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+31],val),sumxod[k*45+31]); 
        sumxod[j*45+31] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+31],val),sumxod[j*45+31]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[32]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[32]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[32]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+32] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+32],val),sumxod[k*45+32]); 
        sumxod[j*45+32] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+32],val),sumxod[j*45+32]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[33]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[33]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[33]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+33] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+33],val),sumxod[k*45+33]); 
        sumxod[j*45+33] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+33],val),sumxod[j*45+33]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[34]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[34]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[34]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+34] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+34],val),sumxod[k*45+34]); 
        sumxod[j*45+34] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+34],val),sumxod[j*45+34]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[35]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[35]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[35]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+35] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+35],val),sumxod[k*45+35]); 
        sumxod[j*45+35] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+35],val),sumxod[j*45+35]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[36]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[36]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[36]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+36] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+36],val),sumxod[k*45+36]); 
        sumxod[j*45+36] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+36],val),sumxod[j*45+36]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[37]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[37]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[37]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+37] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+37],val),sumxod[k*45+37]); 
        sumxod[j*45+37] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+37],val),sumxod[j*45+37]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[38]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[38]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[38]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+38] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+38],val),sumxod[k*45+38]); 
        sumxod[j*45+38] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+38],val),sumxod[j*45+38]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[39]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[39]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[39]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+39] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+39],val),sumxod[k*45+39]); 
        sumxod[j*45+39] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+39],val),sumxod[j*45+39]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[40]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[40]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[40]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+40] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+40],val),sumxod[k*45+40]); 
        sumxod[j*45+40] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+40],val),sumxod[j*45+40]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[41]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[41]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[41]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+41] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+41],val),sumxod[k*45+41]); 
        sumxod[j*45+41] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+41],val),sumxod[j*45+41]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[42]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[42]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[42]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+42] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+42],val),sumxod[k*45+42]); 
        sumxod[j*45+42] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+42],val),sumxod[j*45+42]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[43]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[43]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[43]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+43] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+43],val),sumxod[k*45+43]); 
        sumxod[j*45+43] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+43],val),sumxod[j*45+43]); 
        val = c0;
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[44]),c1);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[44]),c2);
        val = _mm256_add_pd(_mm256_mul_pd(val,logt[44]),c3);
        val = _mm256_xor_pd(val,_mm256_set1_pd(-0.0));
        {
          __m128d lower = _mm256_extractf128_pd(val,0);
          __m128d upper = _mm256_extractf128_pd(val,1);
          val = 
            _mm256_set_pd(exp(_mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1))),exp(_mm_cvtsd_f64(upper)),exp(_mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1))),exp(_mm_cvtsd_f64(lower))); 
        }
        sumxod[k*45+44] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[j*45+44],val),sumxod[k*45+44]); 
        sumxod[j*45+44] = 
          _mm256_add_pd(_mm256_mul_pd(clamped[k*45+44],val),sumxod[j*45+44]); 
      }
    }
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      __m256d sumxw = _mm256_set1_pd(0.0);
      __m256d wtm = _mm256_set1_pd(0.0);
      __m256d pressure;
      if (aligned<32>(pressure_array))
        pressure = _mm256_load_pd(pressure_array);
      else
        pressure = _mm256_loadu_pd(pressure_array);
      pressure_array += 4;
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[0+idx],_mm256_set1_pd(1.00797)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(1.00797)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[45+idx],_mm256_set1_pd(2.01594)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[45+idx],_mm256_set1_pd(2.01594)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[90+idx],_mm256_set1_pd(15.03506)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[90+idx],_mm256_set1_pd(15.03506)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[135+idx],_mm256_set1_pd(15.9994)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[135+idx],_mm256_set1_pd(15.9994)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[180+idx],_mm256_set1_pd(16.04303)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[180+idx],_mm256_set1_pd(16.04303)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[225+idx],_mm256_set1_pd(17.00737)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[225+idx],_mm256_set1_pd(17.00737)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[270+idx],_mm256_set1_pd(18.01534)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[270+idx],_mm256_set1_pd(18.01534)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[315+idx],_mm256_set1_pd(26.03824)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[315+idx],_mm256_set1_pd(26.03824)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[360+idx],_mm256_set1_pd(28.01055)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[360+idx],_mm256_set1_pd(28.01055)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[405+idx],_mm256_set1_pd(28.05418)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[405+idx],_mm256_set1_pd(28.05418)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[450+idx],_mm256_set1_pd(29.06215)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[450+idx],_mm256_set1_pd(29.06215)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[495+idx],_mm256_set1_pd(30.02649)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[495+idx],_mm256_set1_pd(30.02649)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[540+idx],_mm256_set1_pd(30.07012)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[540+idx],_mm256_set1_pd(30.07012)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[585+idx],_mm256_set1_pd(31.03446)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[585+idx],_mm256_set1_pd(31.03446)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[630+idx],_mm256_set1_pd(31.9988)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[630+idx],_mm256_set1_pd(31.9988)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[675+idx],_mm256_set1_pd(33.00677)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[675+idx],_mm256_set1_pd(33.00677)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[720+idx],_mm256_set1_pd(34.01474)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[720+idx],_mm256_set1_pd(34.01474)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[765+idx],_mm256_set1_pd(44.00995)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[765+idx],_mm256_set1_pd(44.00995)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[810+idx],_mm256_set1_pd(44.05358)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[810+idx],_mm256_set1_pd(44.05358)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[855+idx],_mm256_set1_pd(46.02589)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[855+idx],_mm256_set1_pd(46.02589)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[900+idx],_mm256_set1_pd(46.06952)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[900+idx],_mm256_set1_pd(46.06952)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[945+idx],_mm256_set1_pd(59.04501)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[945+idx],_mm256_set1_pd(59.04501)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[990+idx],_mm256_set1_pd(60.05298000000001)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[990+idx],_mm256_set1_pd(60.05298000000001)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1035+idx],_mm256_set1_pd(62.06892000000001)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1035+idx],_mm256_set1_pd(62.06892000000001)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1080+idx],_mm256_set1_pd(75.04441)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1080+idx],_mm256_set1_pd(75.04441)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1125+idx],_mm256_set1_pd(75.04441)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1125+idx],_mm256_set1_pd(75.04441)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1170+idx],_mm256_set1_pd(77.06035)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1170+idx],_mm256_set1_pd(77.06035)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1215+idx],_mm256_set1_pd(92.05178000000001)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1215+idx],_mm256_set1_pd(92.05178000000001)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1260+idx],_mm256_set1_pd(109.05915)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1260+idx],_mm256_set1_pd(109.05915)),wtm); 
      sumxw = 
        _mm256_add_pd(_mm256_mul_pd(clamped[1305+idx],_mm256_set1_pd(28.0134)),sumxw); 
      wtm = 
        _mm256_add_pd(_mm256_mul_pd(mole_frac[1305+idx],_mm256_set1_pd(28.0134)),wtm); 
      pressure = _mm256_mul_pd(pressure,_mm256_set1_pd(1.41836588544e+06));
      __m256d pfac = _mm256_div_pd(_mm256_set1_pd(1.01325e+06),pressure);
      if (aligned<32>(diffusion))
      {
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(1.00797),clamped[0+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[0+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(0*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(2.01594),clamped[45+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[45+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(1*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(15.03506),clamped[90+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[90+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(2*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(15.9994),clamped[135+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[135+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(3*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(16.04303),clamped[180+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[180+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(4*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(17.00737),clamped[225+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[225+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(5*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(18.01534),clamped[270+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[270+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(6*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(26.03824),clamped[315+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[315+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(7*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(28.01055),clamped[360+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[360+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(8*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(28.05418),clamped[405+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[405+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(9*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(29.06215),clamped[450+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[450+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(10*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(30.02649),clamped[495+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[495+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(11*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(30.07012),clamped[540+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[540+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(12*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(31.03446),clamped[585+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[585+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(13*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(31.9988),clamped[630+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[630+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(14*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(33.00677),clamped[675+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[675+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(15*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(34.01474),clamped[720+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[720+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(16*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(44.00995),clamped[765+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[765+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(17*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(44.05358),clamped[810+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[810+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(18*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(46.02589),clamped[855+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[855+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(19*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(46.06952),clamped[900+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[900+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(20*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(59.04501),clamped[945+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[945+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(21*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(60.05298000000001),clamped[990+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[990+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(22*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(62.06892000000001),clamped[1035+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1035+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(23*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(75.04441),clamped[1080+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1080+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(24*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(75.04441),clamped[1125+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1125+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(25*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(77.06035),clamped[1170+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1170+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(26*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(92.05178000000001),clamped[1215+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1215+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(27*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(109.05915),clamped[1260+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1260+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(28*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(28.0134),clamped[1305+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1305+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_stream_pd(diffusion+(29*spec_stride)+(idx<<2),result);
        }
      }
      else
      {
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(145.0),clamped[1350+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1350+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(30*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(38.0),clamped[1395+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1395+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(31*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(144.0),clamped[1440+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1440+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(32*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(80.0),clamped[1485+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1485+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(33*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(141.4),clamped[1530+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1530+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(34*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(80.0),clamped[1575+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1575+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(35*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(572.4),clamped[1620+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1620+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(36*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(265.3),clamped[1665+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1665+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(37*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(98.09999999999999),clamped[1710+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1710+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(38*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(238.4),clamped[1755+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1755+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(39*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(247.5),clamped[1800+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1800+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(40*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(498.0),clamped[1845+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1845+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(41*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(247.5),clamped[1890+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1890+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(42*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(417.0),clamped[1935+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1935+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(43*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(107.4),clamped[1980+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[1980+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(44*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(107.4),clamped[2025+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2025+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(45*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(107.4),clamped[2070+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2070+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(46*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(244.0),clamped[2115+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2115+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(47*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(436.0),clamped[2160+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2160+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(48*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(470.6),clamped[2205+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2205+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(49*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2250+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2250+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(50*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(406.5),clamped[2295+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2295+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(51*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(406.5),clamped[2340+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2340+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(52*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2385+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2385+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(53*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2430+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2430+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(54*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2475+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2475+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(55*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2520+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2520+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(56*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2565+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2565+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(57*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(329.4),clamped[2610+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2610+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(58*spec_stride)+(idx<<2),result);
        }
        {
          __m256d result = 
            _mm256_mul_pd(pfac,_mm256_sub_pd(sumxw,_mm256_mul_pd(_mm256_set1_pd(97.53),clamped[2655+idx]))); 
          result = _mm256_div_pd(result,_mm256_mul_pd(wtm,sumxod[2655+idx]));
          result = _mm256_mul_pd(result,_mm256_set1_pd(6.386577550694006e-05));
          _mm256_storeu_pd(diffusion+(59*spec_stride)+(idx<<2),result);
        }
      }
    }
  }
  free(mole_frac);
  free(clamped);
  free(logt);
  free(sumxod);
}

void avx_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *mixmw_array, const int num_elmts, const int spec_stride, double 
  *thermal) 
{
  __m256d *mole_frac = (__m256d*)memalign(32,67*30*sizeof(__m256d));
  __m256d *temperature = (__m256d*)memalign(32,67*sizeof(__m256d));
  __m256d *thermal_vec = (__m256d*)memalign(32,67*30*sizeof(__m256d));
  size_t remaining_elmts = num_elmts;
  while (remaining_elmts >= 268)
  {
    for (unsigned idx = 0; idx < 67; idx++)
    {
      if (aligned<32>(temperature_array))
        temperature[idx] = _mm256_load_pd(temperature_array);
      else
        temperature[idx] = _mm256_loadu_pd(temperature_array);
      temperature_array += 4;
      __m256d mixmw;
      if (aligned<32>(mixmw_array))
        mixmw = _mm256_load_pd(mixmw_array);
      else
        mixmw = _mm256_loadu_pd(mixmw_array);
      mixmw_array += 4;
      if (aligned<32>(mass_frac_array))
      {
        mole_frac[0+idx] = 
          _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        mole_frac[67+idx] = 
          _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        mole_frac[134+idx] = 
          _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        mole_frac[201+idx] = 
          _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        mole_frac[268+idx] = 
          _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        mole_frac[335+idx] = 
          _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        mole_frac[402+idx] = 
          _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        mole_frac[469+idx] = 
          _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        mole_frac[536+idx] = 
          _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        mole_frac[603+idx] = 
          _mm256_load_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        mole_frac[670+idx] = 
          _mm256_load_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        mole_frac[737+idx] = 
          _mm256_load_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        mole_frac[804+idx] = 
          _mm256_load_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        mole_frac[871+idx] = 
          _mm256_load_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        mole_frac[938+idx] = 
          _mm256_load_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        mole_frac[1005+idx] = 
          _mm256_load_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        mole_frac[1072+idx] = 
          _mm256_load_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        mole_frac[1139+idx] = 
          _mm256_load_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        mole_frac[1206+idx] = 
          _mm256_load_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        mole_frac[1273+idx] = 
          _mm256_load_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        mole_frac[1340+idx] = 
          _mm256_load_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        mole_frac[1407+idx] = 
          _mm256_load_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        mole_frac[1474+idx] = 
          _mm256_load_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        mole_frac[1541+idx] = 
          _mm256_load_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        mole_frac[1608+idx] = 
          _mm256_load_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        mole_frac[1675+idx] = 
          _mm256_load_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        mole_frac[1742+idx] = 
          _mm256_load_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        mole_frac[1809+idx] = 
          _mm256_load_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        mole_frac[1876+idx] = 
          _mm256_load_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        mole_frac[1943+idx] = 
          _mm256_load_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
      }
      else
      {
        mole_frac[0+idx] = 
          _mm256_loadu_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        mole_frac[67+idx] = 
          _mm256_loadu_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        mole_frac[134+idx] = 
          _mm256_loadu_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        mole_frac[201+idx] = 
          _mm256_loadu_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        mole_frac[268+idx] = 
          _mm256_loadu_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        mole_frac[335+idx] = 
          _mm256_loadu_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        mole_frac[402+idx] = 
          _mm256_loadu_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        mole_frac[469+idx] = 
          _mm256_loadu_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        mole_frac[536+idx] = 
          _mm256_loadu_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        mole_frac[603+idx] = 
          _mm256_loadu_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        mole_frac[670+idx] = 
          _mm256_loadu_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        mole_frac[737+idx] = 
          _mm256_loadu_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        mole_frac[804+idx] = 
          _mm256_loadu_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        mole_frac[871+idx] = 
          _mm256_loadu_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        mole_frac[938+idx] = 
          _mm256_loadu_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        mole_frac[1005+idx] = 
          _mm256_loadu_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        mole_frac[1072+idx] = 
          _mm256_loadu_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        mole_frac[1139+idx] = 
          _mm256_loadu_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        mole_frac[1206+idx] = 
          _mm256_loadu_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        mole_frac[1273+idx] = 
          _mm256_loadu_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        mole_frac[1340+idx] = 
          _mm256_loadu_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        mole_frac[1407+idx] = 
          _mm256_loadu_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        mole_frac[1474+idx] = 
          _mm256_loadu_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        mole_frac[1541+idx] = 
          _mm256_loadu_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        mole_frac[1608+idx] = 
          _mm256_loadu_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        mole_frac[1675+idx] = 
          _mm256_loadu_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        mole_frac[1742+idx] = 
          _mm256_loadu_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        mole_frac[1809+idx] = 
          _mm256_loadu_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        mole_frac[1876+idx] = 
          _mm256_loadu_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        mole_frac[1943+idx] = 
          _mm256_loadu_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
      }
      temperature[idx] = _mm256_mul_pd(temperature[idx],_mm256_set1_pd(120.0));
      mole_frac[0+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[0+idx] = _mm256_set1_pd(0.0);
      mole_frac[67+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[67+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[67+idx] = _mm256_set1_pd(0.0);
      mole_frac[134+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[134+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[134+idx] = _mm256_set1_pd(0.0);
      mole_frac[201+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[201+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[201+idx] = _mm256_set1_pd(0.0);
      mole_frac[268+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[268+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[268+idx] = _mm256_set1_pd(0.0);
      mole_frac[335+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[335+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[335+idx] = _mm256_set1_pd(0.0);
      mole_frac[402+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[402+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[402+idx] = _mm256_set1_pd(0.0);
      mole_frac[469+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[469+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[469+idx] = _mm256_set1_pd(0.0);
      mole_frac[536+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[536+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[536+idx] = _mm256_set1_pd(0.0);
      mole_frac[603+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[603+idx],_mm256_set1_pd(recip_molecular_masses[9])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[603+idx] = _mm256_set1_pd(0.0);
      mole_frac[670+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[670+idx],_mm256_set1_pd(recip_molecular_masses[10])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[670+idx] = _mm256_set1_pd(0.0);
      mole_frac[737+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[737+idx],_mm256_set1_pd(recip_molecular_masses[11])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[737+idx] = _mm256_set1_pd(0.0);
      mole_frac[804+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[804+idx],_mm256_set1_pd(recip_molecular_masses[12])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[804+idx] = _mm256_set1_pd(0.0);
      mole_frac[871+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[871+idx],_mm256_set1_pd(recip_molecular_masses[13])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[871+idx] = _mm256_set1_pd(0.0);
      mole_frac[938+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[938+idx],_mm256_set1_pd(recip_molecular_masses[14])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[938+idx] = _mm256_set1_pd(0.0);
      mole_frac[1005+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1005+idx],_mm256_set1_pd(recip_molecular_masses[15])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1005+idx] = _mm256_set1_pd(0.0);
      mole_frac[1072+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1072+idx],_mm256_set1_pd(recip_molecular_masses[16])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1072+idx] = _mm256_set1_pd(0.0);
      mole_frac[1139+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1139+idx],_mm256_set1_pd(recip_molecular_masses[17])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1139+idx] = _mm256_set1_pd(0.0);
      mole_frac[1206+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1206+idx],_mm256_set1_pd(recip_molecular_masses[18])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1206+idx] = _mm256_set1_pd(0.0);
      mole_frac[1273+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1273+idx],_mm256_set1_pd(recip_molecular_masses[19])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1273+idx] = _mm256_set1_pd(0.0);
      mole_frac[1340+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1340+idx],_mm256_set1_pd(recip_molecular_masses[20])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1340+idx] = _mm256_set1_pd(0.0);
      mole_frac[1407+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1407+idx],_mm256_set1_pd(recip_molecular_masses[21])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1407+idx] = _mm256_set1_pd(0.0);
      mole_frac[1474+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1474+idx],_mm256_set1_pd(recip_molecular_masses[22])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1474+idx] = _mm256_set1_pd(0.0);
      mole_frac[1541+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1541+idx],_mm256_set1_pd(recip_molecular_masses[23])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1541+idx] = _mm256_set1_pd(0.0);
      mole_frac[1608+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1608+idx],_mm256_set1_pd(recip_molecular_masses[24])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1608+idx] = _mm256_set1_pd(0.0);
      mole_frac[1675+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1675+idx],_mm256_set1_pd(recip_molecular_masses[25])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1675+idx] = _mm256_set1_pd(0.0);
      mole_frac[1742+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1742+idx],_mm256_set1_pd(recip_molecular_masses[26])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1742+idx] = _mm256_set1_pd(0.0);
      mole_frac[1809+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1809+idx],_mm256_set1_pd(recip_molecular_masses[27])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1809+idx] = _mm256_set1_pd(0.0);
      mole_frac[1876+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1876+idx],_mm256_set1_pd(recip_molecular_masses[28])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1876+idx] = _mm256_set1_pd(0.0);
      mole_frac[1943+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1943+idx],_mm256_set1_pd(recip_molecular_masses[29])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1943+idx] = _mm256_set1_pd(0.0);
    }
    for (unsigned idx = 0; idx < 67; idx++)
    {
      // Light species H
      {
        // Interaction with H2
        {
          __m256d val = _mm256_set1_pd(9.142772693139689e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.897074419931439e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(7.999935843937031e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1441521904321288)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[67+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3
        {
          __m256d val = _mm256_set1_pd(5.627791322321943e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.417789994615262e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.504686600507828e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.10003911024761)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[134+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O
        {
          __m256d val = _mm256_set1_pd(4.492718219288103e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.609398238358899e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.656705986862719e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2352831187271017)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[201+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH4
        {
          __m256d val = _mm256_set1_pd(5.64804120066121e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.425645382492365e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.506659567041789e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.10512412221484)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[268+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with OH
        {
          __m256d val = _mm256_set1_pd(4.526520722696853e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.629030939218756e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.691742308374871e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2370533518097488)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[335+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O
        {
          __m256d val = _mm256_set1_pd(5.60262093242529e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.795454892019831e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.622468727790503e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1743526975317052)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[402+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H2
        {
          __m256d val = _mm256_set1_pd(6.491249524287789e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.116889146585897e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.398334896024519e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.03814707647703083)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[469+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CO
        {
          __m256d val = _mm256_set1_pd(5.241393353126546e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.094454842930398e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.647937041424646e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2001198973224177)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[536+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H4
        {
          __m256d val = _mm256_set1_pd(6.498993101254459e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.089955152261598e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.238121023303065e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.01421003964199077)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[603+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H5
        {
          __m256d val = _mm256_set1_pd(6.528593713906523e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.119691123141584e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.334704025823057e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.02281059439920743)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[670+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH2O
        {
          __m256d val = _mm256_set1_pd(6.106262901787082e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.078791525156344e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.059202595899134e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1613575642024698)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[737+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H6
        {
          __m256d val = _mm256_set1_pd(6.543809449468678e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.129292598355758e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.354129136038698e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.02286375745201874)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[804+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3O
        {
          __m256d val = _mm256_set1_pd(6.357256669996396e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.178315068673857e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.039013843496612e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1312445187253599)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[871+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O2
        {
          __m256d val = _mm256_set1_pd(5.491123022431369e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.264338944017707e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.017229023529065e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1798402987276865)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[938+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HO2
        {
          __m256d val = _mm256_set1_pd(5.501707904384143e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.270631398627651e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.02882804592151e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1801869652155083)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1005+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O2
        {
          __m256d val = _mm256_set1_pd(5.511683510614852e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.276561653654913e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.039759417718045e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1805136772555028)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1072+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CO2
        {
          __m256d val = _mm256_set1_pd(6.679597097339202e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.210644676604187e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.504401145902722e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.0200309448373045)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1139+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3HCO
        {
          __m256d val = _mm256_set1_pd(6.4281019561444e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.241403755265837e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.234296792961776e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1419518484883568)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1206+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HCOOH
        {
          __m256d val = _mm256_set1_pd(6.33759628795356e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.210744316207838e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.271591784927813e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1556825544201592)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1273+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OCH3
        {
          __m256d val = _mm256_set1_pd(6.678968159099615e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.305714758783602e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.023015056603979e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08712275158999067)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1340+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OCO
        {
          __m256d val = _mm256_set1_pd(6.584178790541647e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.317978670798312e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.307618485510858e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1305226225166914)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1407+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OCHO
        {
          __m256d val = _mm256_set1_pd(6.587954147434553e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.32045459240628e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.312955458081581e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1305974639659101)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1474+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OCH2OH
        {
          __m256d val = _mm256_set1_pd(6.754758571972398e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.354574386724192e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.125404830025142e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08811138773203048)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1541+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with OCH2OCHO
        {
          __m256d val = _mm256_set1_pd(6.792806834910504e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.379102871270937e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.176806489860936e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08860770232455792)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1608+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HOCH2OCO
        {
          __m256d val = _mm256_set1_pd(6.792806834910504e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.379102871270937e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.176806489860936e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08860770232455792)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1675+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OCH2O2
        {
          __m256d val = _mm256_set1_pd(6.797583052545046e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.382181944305923e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.18325896614223e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08867000494565155)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1742+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HO2CH2OCHO
        {
          __m256d val = _mm256_set1_pd(6.826609941725254e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.400894640963193e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.222473115975025e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08904864164450052)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1809+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O2CH2OCH2O2H
        {
          __m256d val = _mm256_set1_pd(6.849966660104515e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.415951961891624e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.254027095061118e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08935331469052193)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1876+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with N2
        {
          __m256d val = _mm256_set1_pd(5.228059863164392e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.085192390840869e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.627440892870838e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2015216426512595)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1943+idx]),thermal_vec[0+idx]); 
        }
      }
      // Light species H2
      {
        // Interaction with H
        {
          __m256d val = _mm256_set1_pd(-9.142772693139689e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.897074419931439e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.999935843937031e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1441521904321288)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[0+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3
        {
          __m256d val = _mm256_set1_pd(2.076359594171054e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-1.110963906990921e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.813267135865227e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.3311911853008514)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[134+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with O
        {
          __m256d val = _mm256_set1_pd(5.863023540803317e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.548468678114864e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.847052476570652e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4066824922592918)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[201+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH4
        {
          __m256d val = _mm256_set1_pd(2.064272390123945e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-1.101357049801305e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.793350357124907e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.3395572425759529)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[268+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with OH
        {
          __m256d val = _mm256_set1_pd(5.952596333049684e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.587403103989587e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.905826116117993e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4128956152556185)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[335+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with H2O
        {
          __m256d val = _mm256_set1_pd(5.484991023772154e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.409357392384156e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.730785866652221e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.0227472235431365)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[402+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with C2H2
        {
          __m256d val = _mm256_set1_pd(4.018638410036789e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.305874430998064e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.050725927395682e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2580668321401118)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[469+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CO
        {
          __m256d val = _mm256_set1_pd(1.231151699236785e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-6.03983623275141e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.359619018176707e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4306055468568625)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[536+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with C2H4
        {
          __m256d val = _mm256_set1_pd(3.783551545571742e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.149591614672117e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.730329487625099e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2829743924415595)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[603+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with C2H5
        {
          __m256d val = _mm256_set1_pd(3.902511428547676e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.225045809889136e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.877928182492332e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2767259633905499)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[670+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH2O
        {
          __m256d val = _mm256_set1_pd(5.471615475380025e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.299652077563944e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.212781427974868e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1226933815442866)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[737+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with C2H6
        {
          __m256d val = _mm256_set1_pd(3.92078723629348e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.235465897105197e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.896088864712714e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2780218956633432)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[804+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3O
        {
          __m256d val = _mm256_set1_pd(5.36678967039148e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.219150637684619e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.012659551129757e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1403143195346041)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[871+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with O2
        {
          __m256d val = _mm256_set1_pd(1.520903355214672e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.672987567046268e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.204072736871709e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4265799425063761)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[938+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with HO2
        {
          __m256d val = _mm256_set1_pd(1.526789541701338e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.702683494519006e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.208732728348772e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4282308883632945)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1005+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with H2O2
        {
          __m256d val = _mm256_set1_pd(1.532346392222491e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.73071791551923e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.213131990269295e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4297894627248423)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1072+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CO2
        {
          __m256d val = _mm256_set1_pd(4.051765859784344e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.307057627129864e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.014300062227435e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2931915230505652)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1139+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3HCO
        {
          __m256d val = _mm256_set1_pd(5.46306750914904e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.262695732605872e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.054913027593865e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1599911857471234)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1206+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with HCOOH
        {
          __m256d val = _mm256_set1_pd(5.631821141955933e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.382377920782449e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.329413609152518e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1422358613007126)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1273+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3OCH3
        {
          __m256d val = _mm256_set1_pd(4.859640883800891e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.840231652270688e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.111593704013715e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2270092692143592)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1340+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3OCO
        {
          __m256d val = _mm256_set1_pd(5.444656143389017e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.234737004489394e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.957384170040745e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1808704574400636)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1407+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3OCHO
        {
          __m256d val = _mm256_set1_pd(5.450907225208022e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.238450848880206e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.96422392169694e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1810781172074247)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1474+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3OCH2OH
        {
          __m256d val = _mm256_set1_pd(4.97068072303713e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.905129218490674e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.228390511989111e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2321962929804006)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1541+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with OCH2OCHO
        {
          __m256d val = _mm256_set1_pd(5.026873655050304e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.937971345708288e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.287496821355728e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2348212434111704)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1608+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with HOCH2OCO
        {
          __m256d val = _mm256_set1_pd(5.026873655050304e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.937971345708288e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.287496821355728e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2348212434111704)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1675+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3OCH2O2
        {
          __m256d val = _mm256_set1_pd(5.03394895217157e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.942106524276735e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.294938944155305e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2351517530244069)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1742+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with HO2CH2OCHO
        {
          __m256d val = _mm256_set1_pd(5.07705125768645e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.967297795676495e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.340275930728274e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2371651986905267)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1809+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with O2CH2OCH2O2H
        {
          __m256d val = _mm256_set1_pd(5.111862875856663e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.987643550055875e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.376892391187019e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2387913600036666)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1876+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with N2
        {
          __m256d val = _mm256_set1_pd(1.214379926045129e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-5.945096155210955e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.205368004195816e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4313312690639463)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1943+idx]),thermal_vec[67+idx]); 
        }
      }
    }
    if (aligned<32>(thermal))
    {
      for (unsigned idx = 0; idx < 67; idx++)
      {
        _mm256_stream_pd(thermal+(0*spec_stride)+(idx<<2),thermal_vec[0+idx]);
        _mm256_stream_pd(thermal+(1*spec_stride)+(idx<<2),thermal_vec[67+idx]);
        _mm256_stream_pd(thermal+(2*spec_stride)+(idx<<2),thermal_vec[134+idx]);
        _mm256_stream_pd(thermal+(3*spec_stride)+(idx<<2),thermal_vec[201+idx]);
        _mm256_stream_pd(thermal+(4*spec_stride)+(idx<<2),thermal_vec[268+idx]);
        _mm256_stream_pd(thermal+(5*spec_stride)+(idx<<2),thermal_vec[335+idx]);
        _mm256_stream_pd(thermal+(6*spec_stride)+(idx<<2),thermal_vec[402+idx]);
        _mm256_stream_pd(thermal+(7*spec_stride)+(idx<<2),thermal_vec[469+idx]);
        _mm256_stream_pd(thermal+(8*spec_stride)+(idx<<2),thermal_vec[536+idx]);
        _mm256_stream_pd(thermal+(9*spec_stride)+(idx<<2),thermal_vec[603+idx]);
        
          _mm256_stream_pd(thermal+(10*spec_stride)+(idx<<2),thermal_vec[670+idx]); 
        
          _mm256_stream_pd(thermal+(11*spec_stride)+(idx<<2),thermal_vec[737+idx]); 
        
          _mm256_stream_pd(thermal+(12*spec_stride)+(idx<<2),thermal_vec[804+idx]); 
        
          _mm256_stream_pd(thermal+(13*spec_stride)+(idx<<2),thermal_vec[871+idx]); 
        
          _mm256_stream_pd(thermal+(14*spec_stride)+(idx<<2),thermal_vec[938+idx]); 
        
          _mm256_stream_pd(thermal+(15*spec_stride)+(idx<<2),thermal_vec[1005+idx]); 
        
          _mm256_stream_pd(thermal+(16*spec_stride)+(idx<<2),thermal_vec[1072+idx]); 
        
          _mm256_stream_pd(thermal+(17*spec_stride)+(idx<<2),thermal_vec[1139+idx]); 
        
          _mm256_stream_pd(thermal+(18*spec_stride)+(idx<<2),thermal_vec[1206+idx]); 
        
          _mm256_stream_pd(thermal+(19*spec_stride)+(idx<<2),thermal_vec[1273+idx]); 
        
          _mm256_stream_pd(thermal+(20*spec_stride)+(idx<<2),thermal_vec[1340+idx]); 
        
          _mm256_stream_pd(thermal+(21*spec_stride)+(idx<<2),thermal_vec[1407+idx]); 
        
          _mm256_stream_pd(thermal+(22*spec_stride)+(idx<<2),thermal_vec[1474+idx]); 
        
          _mm256_stream_pd(thermal+(23*spec_stride)+(idx<<2),thermal_vec[1541+idx]); 
        
          _mm256_stream_pd(thermal+(24*spec_stride)+(idx<<2),thermal_vec[1608+idx]); 
        
          _mm256_stream_pd(thermal+(25*spec_stride)+(idx<<2),thermal_vec[1675+idx]); 
        
          _mm256_stream_pd(thermal+(26*spec_stride)+(idx<<2),thermal_vec[1742+idx]); 
        
          _mm256_stream_pd(thermal+(27*spec_stride)+(idx<<2),thermal_vec[1809+idx]); 
        
          _mm256_stream_pd(thermal+(28*spec_stride)+(idx<<2),thermal_vec[1876+idx]); 
        
          _mm256_stream_pd(thermal+(29*spec_stride)+(idx<<2),thermal_vec[1943+idx]); 
      }
    }
    else
    {
      for (unsigned idx = 0; idx < 67; idx++)
      {
        _mm256_storeu_pd(thermal+(0*spec_stride)+(idx<<2),thermal_vec[0+idx]);
        _mm256_storeu_pd(thermal+(1*spec_stride)+(idx<<2),thermal_vec[67+idx]);
        _mm256_storeu_pd(thermal+(2*spec_stride)+(idx<<2),thermal_vec[134+idx]);
        _mm256_storeu_pd(thermal+(3*spec_stride)+(idx<<2),thermal_vec[201+idx]);
        _mm256_storeu_pd(thermal+(4*spec_stride)+(idx<<2),thermal_vec[268+idx]);
        _mm256_storeu_pd(thermal+(5*spec_stride)+(idx<<2),thermal_vec[335+idx]);
        _mm256_storeu_pd(thermal+(6*spec_stride)+(idx<<2),thermal_vec[402+idx]);
        _mm256_storeu_pd(thermal+(7*spec_stride)+(idx<<2),thermal_vec[469+idx]);
        _mm256_storeu_pd(thermal+(8*spec_stride)+(idx<<2),thermal_vec[536+idx]);
        _mm256_storeu_pd(thermal+(9*spec_stride)+(idx<<2),thermal_vec[603+idx]);
        
          _mm256_storeu_pd(thermal+(10*spec_stride)+(idx<<2),thermal_vec[670+idx]); 
        
          _mm256_storeu_pd(thermal+(11*spec_stride)+(idx<<2),thermal_vec[737+idx]); 
        
          _mm256_storeu_pd(thermal+(12*spec_stride)+(idx<<2),thermal_vec[804+idx]); 
        
          _mm256_storeu_pd(thermal+(13*spec_stride)+(idx<<2),thermal_vec[871+idx]); 
        
          _mm256_storeu_pd(thermal+(14*spec_stride)+(idx<<2),thermal_vec[938+idx]); 
        
          _mm256_storeu_pd(thermal+(15*spec_stride)+(idx<<2),thermal_vec[1005+idx]); 
        
          _mm256_storeu_pd(thermal+(16*spec_stride)+(idx<<2),thermal_vec[1072+idx]); 
        
          _mm256_storeu_pd(thermal+(17*spec_stride)+(idx<<2),thermal_vec[1139+idx]); 
        
          _mm256_storeu_pd(thermal+(18*spec_stride)+(idx<<2),thermal_vec[1206+idx]); 
        
          _mm256_storeu_pd(thermal+(19*spec_stride)+(idx<<2),thermal_vec[1273+idx]); 
        
          _mm256_storeu_pd(thermal+(20*spec_stride)+(idx<<2),thermal_vec[1340+idx]); 
        
          _mm256_storeu_pd(thermal+(21*spec_stride)+(idx<<2),thermal_vec[1407+idx]); 
        
          _mm256_storeu_pd(thermal+(22*spec_stride)+(idx<<2),thermal_vec[1474+idx]); 
        
          _mm256_storeu_pd(thermal+(23*spec_stride)+(idx<<2),thermal_vec[1541+idx]); 
        
          _mm256_storeu_pd(thermal+(24*spec_stride)+(idx<<2),thermal_vec[1608+idx]); 
        
          _mm256_storeu_pd(thermal+(25*spec_stride)+(idx<<2),thermal_vec[1675+idx]); 
        
          _mm256_storeu_pd(thermal+(26*spec_stride)+(idx<<2),thermal_vec[1742+idx]); 
        
          _mm256_storeu_pd(thermal+(27*spec_stride)+(idx<<2),thermal_vec[1809+idx]); 
        
          _mm256_storeu_pd(thermal+(28*spec_stride)+(idx<<2),thermal_vec[1876+idx]); 
        
          _mm256_storeu_pd(thermal+(29*spec_stride)+(idx<<2),thermal_vec[1943+idx]); 
      }
    }
    remaining_elmts -= 268;
    mass_frac_array += 268;
    thermal += 268;
  }
  if (remaining_elmts > 0)
  {
    assert((remaining_elmts % 4) == 0);
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      if (aligned<32>(temperature_array))
        temperature[idx] = _mm256_load_pd(temperature_array);
      else
        temperature[idx] = _mm256_loadu_pd(temperature_array);
      temperature_array += 4;
      __m256d mixmw;
      if (aligned<32>(mixmw_array))
        mixmw = _mm256_load_pd(mixmw_array);
      else
        mixmw = _mm256_loadu_pd(mixmw_array);
      mixmw_array += 4;
      if (aligned<32>(mass_frac_array))
      {
        mole_frac[0+idx] = 
          _mm256_load_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        mole_frac[67+idx] = 
          _mm256_load_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        mole_frac[134+idx] = 
          _mm256_load_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        mole_frac[201+idx] = 
          _mm256_load_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        mole_frac[268+idx] = 
          _mm256_load_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        mole_frac[335+idx] = 
          _mm256_load_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        mole_frac[402+idx] = 
          _mm256_load_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        mole_frac[469+idx] = 
          _mm256_load_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        mole_frac[536+idx] = 
          _mm256_load_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        mole_frac[603+idx] = 
          _mm256_load_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        mole_frac[670+idx] = 
          _mm256_load_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        mole_frac[737+idx] = 
          _mm256_load_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        mole_frac[804+idx] = 
          _mm256_load_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        mole_frac[871+idx] = 
          _mm256_load_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        mole_frac[938+idx] = 
          _mm256_load_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        mole_frac[1005+idx] = 
          _mm256_load_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        mole_frac[1072+idx] = 
          _mm256_load_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        mole_frac[1139+idx] = 
          _mm256_load_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        mole_frac[1206+idx] = 
          _mm256_load_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        mole_frac[1273+idx] = 
          _mm256_load_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        mole_frac[1340+idx] = 
          _mm256_load_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        mole_frac[1407+idx] = 
          _mm256_load_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        mole_frac[1474+idx] = 
          _mm256_load_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        mole_frac[1541+idx] = 
          _mm256_load_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        mole_frac[1608+idx] = 
          _mm256_load_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        mole_frac[1675+idx] = 
          _mm256_load_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        mole_frac[1742+idx] = 
          _mm256_load_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        mole_frac[1809+idx] = 
          _mm256_load_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        mole_frac[1876+idx] = 
          _mm256_load_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        mole_frac[1943+idx] = 
          _mm256_load_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
      }
      else
      {
        mole_frac[0+idx] = 
          _mm256_loadu_pd(mass_frac_array+(0*spec_stride)+(idx<<2)); 
        mole_frac[67+idx] = 
          _mm256_loadu_pd(mass_frac_array+(1*spec_stride)+(idx<<2)); 
        mole_frac[134+idx] = 
          _mm256_loadu_pd(mass_frac_array+(2*spec_stride)+(idx<<2)); 
        mole_frac[201+idx] = 
          _mm256_loadu_pd(mass_frac_array+(3*spec_stride)+(idx<<2)); 
        mole_frac[268+idx] = 
          _mm256_loadu_pd(mass_frac_array+(4*spec_stride)+(idx<<2)); 
        mole_frac[335+idx] = 
          _mm256_loadu_pd(mass_frac_array+(5*spec_stride)+(idx<<2)); 
        mole_frac[402+idx] = 
          _mm256_loadu_pd(mass_frac_array+(6*spec_stride)+(idx<<2)); 
        mole_frac[469+idx] = 
          _mm256_loadu_pd(mass_frac_array+(7*spec_stride)+(idx<<2)); 
        mole_frac[536+idx] = 
          _mm256_loadu_pd(mass_frac_array+(8*spec_stride)+(idx<<2)); 
        mole_frac[603+idx] = 
          _mm256_loadu_pd(mass_frac_array+(9*spec_stride)+(idx<<2)); 
        mole_frac[670+idx] = 
          _mm256_loadu_pd(mass_frac_array+(10*spec_stride)+(idx<<2)); 
        mole_frac[737+idx] = 
          _mm256_loadu_pd(mass_frac_array+(11*spec_stride)+(idx<<2)); 
        mole_frac[804+idx] = 
          _mm256_loadu_pd(mass_frac_array+(12*spec_stride)+(idx<<2)); 
        mole_frac[871+idx] = 
          _mm256_loadu_pd(mass_frac_array+(13*spec_stride)+(idx<<2)); 
        mole_frac[938+idx] = 
          _mm256_loadu_pd(mass_frac_array+(14*spec_stride)+(idx<<2)); 
        mole_frac[1005+idx] = 
          _mm256_loadu_pd(mass_frac_array+(15*spec_stride)+(idx<<2)); 
        mole_frac[1072+idx] = 
          _mm256_loadu_pd(mass_frac_array+(16*spec_stride)+(idx<<2)); 
        mole_frac[1139+idx] = 
          _mm256_loadu_pd(mass_frac_array+(17*spec_stride)+(idx<<2)); 
        mole_frac[1206+idx] = 
          _mm256_loadu_pd(mass_frac_array+(18*spec_stride)+(idx<<2)); 
        mole_frac[1273+idx] = 
          _mm256_loadu_pd(mass_frac_array+(19*spec_stride)+(idx<<2)); 
        mole_frac[1340+idx] = 
          _mm256_loadu_pd(mass_frac_array+(20*spec_stride)+(idx<<2)); 
        mole_frac[1407+idx] = 
          _mm256_loadu_pd(mass_frac_array+(21*spec_stride)+(idx<<2)); 
        mole_frac[1474+idx] = 
          _mm256_loadu_pd(mass_frac_array+(22*spec_stride)+(idx<<2)); 
        mole_frac[1541+idx] = 
          _mm256_loadu_pd(mass_frac_array+(23*spec_stride)+(idx<<2)); 
        mole_frac[1608+idx] = 
          _mm256_loadu_pd(mass_frac_array+(24*spec_stride)+(idx<<2)); 
        mole_frac[1675+idx] = 
          _mm256_loadu_pd(mass_frac_array+(25*spec_stride)+(idx<<2)); 
        mole_frac[1742+idx] = 
          _mm256_loadu_pd(mass_frac_array+(26*spec_stride)+(idx<<2)); 
        mole_frac[1809+idx] = 
          _mm256_loadu_pd(mass_frac_array+(27*spec_stride)+(idx<<2)); 
        mole_frac[1876+idx] = 
          _mm256_loadu_pd(mass_frac_array+(28*spec_stride)+(idx<<2)); 
        mole_frac[1943+idx] = 
          _mm256_loadu_pd(mass_frac_array+(29*spec_stride)+(idx<<2)); 
      }
      temperature[idx] = _mm256_mul_pd(temperature[idx],_mm256_set1_pd(120.0));
      mole_frac[0+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[0+idx],_mm256_set1_pd(recip_molecular_masses[0])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[0+idx] = _mm256_set1_pd(0.0);
      mole_frac[67+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[67+idx],_mm256_set1_pd(recip_molecular_masses[1])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[67+idx] = _mm256_set1_pd(0.0);
      mole_frac[134+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[134+idx],_mm256_set1_pd(recip_molecular_masses[2])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[134+idx] = _mm256_set1_pd(0.0);
      mole_frac[201+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[201+idx],_mm256_set1_pd(recip_molecular_masses[3])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[201+idx] = _mm256_set1_pd(0.0);
      mole_frac[268+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[268+idx],_mm256_set1_pd(recip_molecular_masses[4])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[268+idx] = _mm256_set1_pd(0.0);
      mole_frac[335+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[335+idx],_mm256_set1_pd(recip_molecular_masses[5])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[335+idx] = _mm256_set1_pd(0.0);
      mole_frac[402+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[402+idx],_mm256_set1_pd(recip_molecular_masses[6])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[402+idx] = _mm256_set1_pd(0.0);
      mole_frac[469+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[469+idx],_mm256_set1_pd(recip_molecular_masses[7])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[469+idx] = _mm256_set1_pd(0.0);
      mole_frac[536+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[536+idx],_mm256_set1_pd(recip_molecular_masses[8])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[536+idx] = _mm256_set1_pd(0.0);
      mole_frac[603+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[603+idx],_mm256_set1_pd(recip_molecular_masses[9])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[603+idx] = _mm256_set1_pd(0.0);
      mole_frac[670+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[670+idx],_mm256_set1_pd(recip_molecular_masses[10])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[670+idx] = _mm256_set1_pd(0.0);
      mole_frac[737+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[737+idx],_mm256_set1_pd(recip_molecular_masses[11])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[737+idx] = _mm256_set1_pd(0.0);
      mole_frac[804+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[804+idx],_mm256_set1_pd(recip_molecular_masses[12])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[804+idx] = _mm256_set1_pd(0.0);
      mole_frac[871+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[871+idx],_mm256_set1_pd(recip_molecular_masses[13])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[871+idx] = _mm256_set1_pd(0.0);
      mole_frac[938+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[938+idx],_mm256_set1_pd(recip_molecular_masses[14])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[938+idx] = _mm256_set1_pd(0.0);
      mole_frac[1005+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1005+idx],_mm256_set1_pd(recip_molecular_masses[15])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1005+idx] = _mm256_set1_pd(0.0);
      mole_frac[1072+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1072+idx],_mm256_set1_pd(recip_molecular_masses[16])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1072+idx] = _mm256_set1_pd(0.0);
      mole_frac[1139+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1139+idx],_mm256_set1_pd(recip_molecular_masses[17])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1139+idx] = _mm256_set1_pd(0.0);
      mole_frac[1206+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1206+idx],_mm256_set1_pd(recip_molecular_masses[18])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1206+idx] = _mm256_set1_pd(0.0);
      mole_frac[1273+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1273+idx],_mm256_set1_pd(recip_molecular_masses[19])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1273+idx] = _mm256_set1_pd(0.0);
      mole_frac[1340+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1340+idx],_mm256_set1_pd(recip_molecular_masses[20])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1340+idx] = _mm256_set1_pd(0.0);
      mole_frac[1407+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1407+idx],_mm256_set1_pd(recip_molecular_masses[21])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1407+idx] = _mm256_set1_pd(0.0);
      mole_frac[1474+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1474+idx],_mm256_set1_pd(recip_molecular_masses[22])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1474+idx] = _mm256_set1_pd(0.0);
      mole_frac[1541+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1541+idx],_mm256_set1_pd(recip_molecular_masses[23])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1541+idx] = _mm256_set1_pd(0.0);
      mole_frac[1608+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1608+idx],_mm256_set1_pd(recip_molecular_masses[24])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1608+idx] = _mm256_set1_pd(0.0);
      mole_frac[1675+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1675+idx],_mm256_set1_pd(recip_molecular_masses[25])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1675+idx] = _mm256_set1_pd(0.0);
      mole_frac[1742+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1742+idx],_mm256_set1_pd(recip_molecular_masses[26])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1742+idx] = _mm256_set1_pd(0.0);
      mole_frac[1809+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1809+idx],_mm256_set1_pd(recip_molecular_masses[27])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1809+idx] = _mm256_set1_pd(0.0);
      mole_frac[1876+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1876+idx],_mm256_set1_pd(recip_molecular_masses[28])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1876+idx] = _mm256_set1_pd(0.0);
      mole_frac[1943+idx] = 
        _mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(mole_frac[1943+idx],_mm256_set1_pd(recip_molecular_masses[29])),_mm256_set1_pd(1e3)),mixmw); 
      thermal_vec[1943+idx] = _mm256_set1_pd(0.0);
    }
    for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
    {
      // Light species H
      {
        // Interaction with H2
        {
          __m256d val = _mm256_set1_pd(9.142772693139689e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.897074419931439e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(7.999935843937031e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1441521904321288)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[67+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3
        {
          __m256d val = _mm256_set1_pd(5.627791322321943e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.417789994615262e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.504686600507828e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.10003911024761)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[134+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O
        {
          __m256d val = _mm256_set1_pd(4.492718219288103e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.609398238358899e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.656705986862719e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2352831187271017)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[201+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH4
        {
          __m256d val = _mm256_set1_pd(5.64804120066121e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.425645382492365e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.506659567041789e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.10512412221484)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[268+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with OH
        {
          __m256d val = _mm256_set1_pd(4.526520722696853e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.629030939218756e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.691742308374871e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2370533518097488)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[335+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O
        {
          __m256d val = _mm256_set1_pd(5.60262093242529e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.795454892019831e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.622468727790503e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1743526975317052)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[402+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H2
        {
          __m256d val = _mm256_set1_pd(6.491249524287789e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.116889146585897e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.398334896024519e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.03814707647703083)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[469+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CO
        {
          __m256d val = _mm256_set1_pd(5.241393353126546e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.094454842930398e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.647937041424646e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2001198973224177)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[536+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H4
        {
          __m256d val = _mm256_set1_pd(6.498993101254459e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.089955152261598e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.238121023303065e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.01421003964199077)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[603+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H5
        {
          __m256d val = _mm256_set1_pd(6.528593713906523e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.119691123141584e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.334704025823057e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.02281059439920743)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[670+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH2O
        {
          __m256d val = _mm256_set1_pd(6.106262901787082e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.078791525156344e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.059202595899134e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1613575642024698)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[737+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with C2H6
        {
          __m256d val = _mm256_set1_pd(6.543809449468678e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.129292598355758e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.354129136038698e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.02286375745201874)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[804+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3O
        {
          __m256d val = _mm256_set1_pd(6.357256669996396e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.178315068673857e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.039013843496612e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1312445187253599)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[871+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O2
        {
          __m256d val = _mm256_set1_pd(5.491123022431369e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.264338944017707e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.017229023529065e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1798402987276865)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[938+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HO2
        {
          __m256d val = _mm256_set1_pd(5.501707904384143e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.270631398627651e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.02882804592151e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1801869652155083)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1005+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with H2O2
        {
          __m256d val = _mm256_set1_pd(5.511683510614852e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.276561653654913e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.039759417718045e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1805136772555028)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1072+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CO2
        {
          __m256d val = _mm256_set1_pd(6.679597097339202e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.210644676604187e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(8.504401145902722e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.0200309448373045)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1139+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3HCO
        {
          __m256d val = _mm256_set1_pd(6.4281019561444e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.241403755265837e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.234296792961776e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1419518484883568)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1206+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HCOOH
        {
          __m256d val = _mm256_set1_pd(6.33759628795356e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.210744316207838e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.271591784927813e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1556825544201592)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1273+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OCH3
        {
          __m256d val = _mm256_set1_pd(6.678968159099615e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.305714758783602e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.023015056603979e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08712275158999067)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1340+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OCO
        {
          __m256d val = _mm256_set1_pd(6.584178790541647e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.317978670798312e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.307618485510858e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1305226225166914)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1407+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OCHO
        {
          __m256d val = _mm256_set1_pd(6.587954147434553e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.32045459240628e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.312955458081581e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1305974639659101)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1474+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OCH2OH
        {
          __m256d val = _mm256_set1_pd(6.754758571972398e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.354574386724192e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.125404830025142e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08811138773203048)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1541+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with OCH2OCHO
        {
          __m256d val = _mm256_set1_pd(6.792806834910504e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.379102871270937e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.176806489860936e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08860770232455792)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1608+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HOCH2OCO
        {
          __m256d val = _mm256_set1_pd(6.792806834910504e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.379102871270937e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.176806489860936e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08860770232455792)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1675+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with CH3OCH2O2
        {
          __m256d val = _mm256_set1_pd(6.797583052545046e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.382181944305923e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.18325896614223e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08867000494565155)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1742+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with HO2CH2OCHO
        {
          __m256d val = _mm256_set1_pd(6.826609941725254e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.400894640963193e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.222473115975025e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08904864164450052)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1809+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with O2CH2OCH2O2H
        {
          __m256d val = _mm256_set1_pd(6.849966660104515e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-4.415951961891624e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.254027095061118e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.08935331469052193)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1876+idx]),thermal_vec[0+idx]); 
        }
        // Interaction with N2
        {
          __m256d val = _mm256_set1_pd(5.228059863164392e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.085192390840869e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.627440892870838e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2015216426512595)); 
          thermal_vec[0+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[0+idx],mole_frac[1943+idx]),thermal_vec[0+idx]); 
        }
      }
      // Light species H2
      {
        // Interaction with H
        {
          __m256d val = _mm256_set1_pd(-9.142772693139689e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.897074419931439e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.999935843937031e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-0.1441521904321288)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[0+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3
        {
          __m256d val = _mm256_set1_pd(2.076359594171054e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-1.110963906990921e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.813267135865227e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.3311911853008514)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[134+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with O
        {
          __m256d val = _mm256_set1_pd(5.863023540803317e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.548468678114864e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.847052476570652e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4066824922592918)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[201+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH4
        {
          __m256d val = _mm256_set1_pd(2.064272390123945e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-1.101357049801305e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.793350357124907e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.3395572425759529)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[268+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with OH
        {
          __m256d val = _mm256_set1_pd(5.952596333049684e-12);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.587403103989587e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.905826116117993e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4128956152556185)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[335+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with H2O
        {
          __m256d val = _mm256_set1_pd(5.484991023772154e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.409357392384156e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.730785866652221e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.0227472235431365)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[402+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with C2H2
        {
          __m256d val = _mm256_set1_pd(4.018638410036789e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.305874430998064e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.050725927395682e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2580668321401118)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[469+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CO
        {
          __m256d val = _mm256_set1_pd(1.231151699236785e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-6.03983623275141e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.359619018176707e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4306055468568625)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[536+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with C2H4
        {
          __m256d val = _mm256_set1_pd(3.783551545571742e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.149591614672117e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.730329487625099e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2829743924415595)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[603+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with C2H5
        {
          __m256d val = _mm256_set1_pd(3.902511428547676e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.225045809889136e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.877928182492332e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2767259633905499)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[670+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH2O
        {
          __m256d val = _mm256_set1_pd(5.471615475380025e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.299652077563944e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.212781427974868e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1226933815442866)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[737+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with C2H6
        {
          __m256d val = _mm256_set1_pd(3.92078723629348e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.235465897105197e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(3.896088864712714e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2780218956633432)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[804+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3O
        {
          __m256d val = _mm256_set1_pd(5.36678967039148e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.219150637684619e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.012659551129757e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1403143195346041)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[871+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with O2
        {
          __m256d val = _mm256_set1_pd(1.520903355214672e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.672987567046268e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.204072736871709e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4265799425063761)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[938+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with HO2
        {
          __m256d val = _mm256_set1_pd(1.526789541701338e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.702683494519006e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.208732728348772e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4282308883632945)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1005+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with H2O2
        {
          __m256d val = _mm256_set1_pd(1.532346392222491e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-7.73071791551923e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(1.213131990269295e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4297894627248423)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1072+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CO2
        {
          __m256d val = _mm256_set1_pd(4.051765859784344e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.307057627129864e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(4.014300062227435e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2931915230505652)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1139+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3HCO
        {
          __m256d val = _mm256_set1_pd(5.46306750914904e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.262695732605872e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.054913027593865e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1599911857471234)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1206+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with HCOOH
        {
          __m256d val = _mm256_set1_pd(5.631821141955933e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.382377920782449e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(6.329413609152518e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1422358613007126)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1273+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3OCH3
        {
          __m256d val = _mm256_set1_pd(4.859640883800891e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.840231652270688e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.111593704013715e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2270092692143592)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1340+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3OCO
        {
          __m256d val = _mm256_set1_pd(5.444656143389017e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.234737004489394e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.957384170040745e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1808704574400636)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1407+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3OCHO
        {
          __m256d val = _mm256_set1_pd(5.450907225208022e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-3.238450848880206e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.96422392169694e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.1810781172074247)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1474+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3OCH2OH
        {
          __m256d val = _mm256_set1_pd(4.97068072303713e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.905129218490674e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.228390511989111e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2321962929804006)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1541+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with OCH2OCHO
        {
          __m256d val = _mm256_set1_pd(5.026873655050304e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.937971345708288e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.287496821355728e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2348212434111704)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1608+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with HOCH2OCO
        {
          __m256d val = _mm256_set1_pd(5.026873655050304e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.937971345708288e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.287496821355728e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2348212434111704)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1675+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with CH3OCH2O2
        {
          __m256d val = _mm256_set1_pd(5.03394895217157e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.942106524276735e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.294938944155305e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2351517530244069)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1742+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with HO2CH2OCHO
        {
          __m256d val = _mm256_set1_pd(5.07705125768645e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.967297795676495e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.340275930728274e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2371651986905267)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1809+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with O2CH2OCH2O2H
        {
          __m256d val = _mm256_set1_pd(5.111862875856663e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-2.987643550055875e-07)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(5.376892391187019e-04)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.2387913600036666)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1876+idx]),thermal_vec[67+idx]); 
        }
        // Interaction with N2
        {
          __m256d val = _mm256_set1_pd(1.214379926045129e-11);
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(-5.945096155210955e-08)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(9.205368004195816e-05)); 
          val = 
            _mm256_add_pd(_mm256_mul_pd(val,temperature[idx]),_mm256_set1_pd(0.4313312690639463)); 
          thermal_vec[67+idx] = 
            _mm256_add_pd(_mm256_mul_pd(mole_frac[67+idx],mole_frac[1943+idx]),thermal_vec[67+idx]); 
        }
      }
    }
    if (aligned<32>(thermal))
    {
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        _mm256_stream_pd(thermal+(0*spec_stride)+(idx<<2),thermal_vec[0+idx]);
        _mm256_stream_pd(thermal+(1*spec_stride)+(idx<<2),thermal_vec[67+idx]);
        _mm256_stream_pd(thermal+(2*spec_stride)+(idx<<2),thermal_vec[134+idx]);
        _mm256_stream_pd(thermal+(3*spec_stride)+(idx<<2),thermal_vec[201+idx]);
        _mm256_stream_pd(thermal+(4*spec_stride)+(idx<<2),thermal_vec[268+idx]);
        _mm256_stream_pd(thermal+(5*spec_stride)+(idx<<2),thermal_vec[335+idx]);
        _mm256_stream_pd(thermal+(6*spec_stride)+(idx<<2),thermal_vec[402+idx]);
        _mm256_stream_pd(thermal+(7*spec_stride)+(idx<<2),thermal_vec[469+idx]);
        _mm256_stream_pd(thermal+(8*spec_stride)+(idx<<2),thermal_vec[536+idx]);
        _mm256_stream_pd(thermal+(9*spec_stride)+(idx<<2),thermal_vec[603+idx]);
        
          _mm256_stream_pd(thermal+(10*spec_stride)+(idx<<2),thermal_vec[670+idx]); 
        
          _mm256_stream_pd(thermal+(11*spec_stride)+(idx<<2),thermal_vec[737+idx]); 
        
          _mm256_stream_pd(thermal+(12*spec_stride)+(idx<<2),thermal_vec[804+idx]); 
        
          _mm256_stream_pd(thermal+(13*spec_stride)+(idx<<2),thermal_vec[871+idx]); 
        
          _mm256_stream_pd(thermal+(14*spec_stride)+(idx<<2),thermal_vec[938+idx]); 
        
          _mm256_stream_pd(thermal+(15*spec_stride)+(idx<<2),thermal_vec[1005+idx]); 
        
          _mm256_stream_pd(thermal+(16*spec_stride)+(idx<<2),thermal_vec[1072+idx]); 
        
          _mm256_stream_pd(thermal+(17*spec_stride)+(idx<<2),thermal_vec[1139+idx]); 
        
          _mm256_stream_pd(thermal+(18*spec_stride)+(idx<<2),thermal_vec[1206+idx]); 
        
          _mm256_stream_pd(thermal+(19*spec_stride)+(idx<<2),thermal_vec[1273+idx]); 
        
          _mm256_stream_pd(thermal+(20*spec_stride)+(idx<<2),thermal_vec[1340+idx]); 
        
          _mm256_stream_pd(thermal+(21*spec_stride)+(idx<<2),thermal_vec[1407+idx]); 
        
          _mm256_stream_pd(thermal+(22*spec_stride)+(idx<<2),thermal_vec[1474+idx]); 
        
          _mm256_stream_pd(thermal+(23*spec_stride)+(idx<<2),thermal_vec[1541+idx]); 
        
          _mm256_stream_pd(thermal+(24*spec_stride)+(idx<<2),thermal_vec[1608+idx]); 
        
          _mm256_stream_pd(thermal+(25*spec_stride)+(idx<<2),thermal_vec[1675+idx]); 
        
          _mm256_stream_pd(thermal+(26*spec_stride)+(idx<<2),thermal_vec[1742+idx]); 
        
          _mm256_stream_pd(thermal+(27*spec_stride)+(idx<<2),thermal_vec[1809+idx]); 
        
          _mm256_stream_pd(thermal+(28*spec_stride)+(idx<<2),thermal_vec[1876+idx]); 
        
          _mm256_stream_pd(thermal+(29*spec_stride)+(idx<<2),thermal_vec[1943+idx]); 
      }
    }
    else
    {
      for (unsigned idx = 0; idx < (remaining_elmts/4); idx++)
      {
        _mm256_storeu_pd(thermal+(0*spec_stride)+(idx<<2),thermal_vec[0+idx]);
        _mm256_storeu_pd(thermal+(1*spec_stride)+(idx<<2),thermal_vec[67+idx]);
        _mm256_storeu_pd(thermal+(2*spec_stride)+(idx<<2),thermal_vec[134+idx]);
        _mm256_storeu_pd(thermal+(3*spec_stride)+(idx<<2),thermal_vec[201+idx]);
        _mm256_storeu_pd(thermal+(4*spec_stride)+(idx<<2),thermal_vec[268+idx]);
        _mm256_storeu_pd(thermal+(5*spec_stride)+(idx<<2),thermal_vec[335+idx]);
        _mm256_storeu_pd(thermal+(6*spec_stride)+(idx<<2),thermal_vec[402+idx]);
        _mm256_storeu_pd(thermal+(7*spec_stride)+(idx<<2),thermal_vec[469+idx]);
        _mm256_storeu_pd(thermal+(8*spec_stride)+(idx<<2),thermal_vec[536+idx]);
        _mm256_storeu_pd(thermal+(9*spec_stride)+(idx<<2),thermal_vec[603+idx]);
        
          _mm256_storeu_pd(thermal+(10*spec_stride)+(idx<<2),thermal_vec[670+idx]); 
        
          _mm256_storeu_pd(thermal+(11*spec_stride)+(idx<<2),thermal_vec[737+idx]); 
        
          _mm256_storeu_pd(thermal+(12*spec_stride)+(idx<<2),thermal_vec[804+idx]); 
        
          _mm256_storeu_pd(thermal+(13*spec_stride)+(idx<<2),thermal_vec[871+idx]); 
        
          _mm256_storeu_pd(thermal+(14*spec_stride)+(idx<<2),thermal_vec[938+idx]); 
        
          _mm256_storeu_pd(thermal+(15*spec_stride)+(idx<<2),thermal_vec[1005+idx]); 
        
          _mm256_storeu_pd(thermal+(16*spec_stride)+(idx<<2),thermal_vec[1072+idx]); 
        
          _mm256_storeu_pd(thermal+(17*spec_stride)+(idx<<2),thermal_vec[1139+idx]); 
        
          _mm256_storeu_pd(thermal+(18*spec_stride)+(idx<<2),thermal_vec[1206+idx]); 
        
          _mm256_storeu_pd(thermal+(19*spec_stride)+(idx<<2),thermal_vec[1273+idx]); 
        
          _mm256_storeu_pd(thermal+(20*spec_stride)+(idx<<2),thermal_vec[1340+idx]); 
        
          _mm256_storeu_pd(thermal+(21*spec_stride)+(idx<<2),thermal_vec[1407+idx]); 
        
          _mm256_storeu_pd(thermal+(22*spec_stride)+(idx<<2),thermal_vec[1474+idx]); 
        
          _mm256_storeu_pd(thermal+(23*spec_stride)+(idx<<2),thermal_vec[1541+idx]); 
        
          _mm256_storeu_pd(thermal+(24*spec_stride)+(idx<<2),thermal_vec[1608+idx]); 
        
          _mm256_storeu_pd(thermal+(25*spec_stride)+(idx<<2),thermal_vec[1675+idx]); 
        
          _mm256_storeu_pd(thermal+(26*spec_stride)+(idx<<2),thermal_vec[1742+idx]); 
        
          _mm256_storeu_pd(thermal+(27*spec_stride)+(idx<<2),thermal_vec[1809+idx]); 
        
          _mm256_storeu_pd(thermal+(28*spec_stride)+(idx<<2),thermal_vec[1876+idx]); 
        
          _mm256_storeu_pd(thermal+(29*spec_stride)+(idx<<2),thermal_vec[1943+idx]); 
      }
    }
  }
  free(mole_frac);
  free(temperature);
  free(thermal_vec);
}

