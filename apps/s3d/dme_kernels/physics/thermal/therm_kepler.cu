
/*
 * Kepler tuned thermal kernel for DME.
 * Gets 4 CTAs with 4 warps per CTA.
 * Two versions, one which uses 64-byte
 * loads and stores, and one which
 * uses 128-byte loads and stores.
 * Should compile to 83 and 101 registers.
 * Completely memory bandwidth limited.
 *
 * Can tune number of threads for each CTA
 * Probably want at least 4 warps per CTA
 * since max number of CTAs/SM is 16 on Kepler.
 *
 * Launch with (64-byte version):
 *  dim3 grid((nx*ny*nz)/128,1,1);
 *  dim3 block(128,1,1);
 *
 * Launch with (128-byte version):
 *  dim3 grid((nx*ny*nz)/128,1,1);
 *  dim3 grid(128,1,1);
 *
 * gpu_thermal<<<grid,block>>>(temperature_ptr,  
 *                 mole_frac_input_ptr, nx*ny, nx, nz, nx*ny*nz, output_d);
 *
 * Performance numbers on K20c with 13 SMs and 5 memory partitions:
 * 
 * 32x32x32 (threads=128) 64-byte loads/stores
 *   Latency: 0.104 ms
 *   Throughput: 313.610 Mpoints/s
 *   Perf: 153.0 GB/s (77.8 GB/s read, 75.2 GB/s write)
 *
 * 64x64x64 (threads=128) 128-byte loads/stores
 *   Latency: 0.784 ms
 *   Throughput: 334.410 Mpoints/s
 *   Perf: 163.2 GB/s (80.9 GB/s read, 80.3 GB/s write)
 *
 * 128x128x128 (threads=128) 128-byte loads/stores
 *   Latency: 6.228 ms
 *   Throughput: 336.743 Mpoints/
 *   Perf: 164.3 GB/s (83.5 GB/s read, 80.8 GB/s write) 
 *
 * Generation command:
 *   ./singe --dir inputs/DME/ --cuda --k20
 *
 */

__constant__ double recip_molecular_masses[30] = {0.9920930186414277, 
  0.4960465093207139, 0.06651120780362699, 0.06250234383789392, 
  0.06233236489615739, 0.05879803873262004, 0.05550825019122593, 
  0.0384050534905585, 0.03570083414998991, 0.03564531203549703, 
  0.0344090165386938, 0.03330392596670473, 0.03325560390181349, 
  0.03222224585186918, 0.03125117191894696, 0.03029681486555637, 
  0.02939901936631002, 0.02272213442641948, 0.02269963076780593, 
  0.02172690196756651, 0.02170632557057247, 0.01693623220658274, 
  0.01665196298335236, 0.01611112292593459, 0.01332544289441412, 
  0.01332544289441412, 0.01297684217629429, 0.01086345098378326, 
  9.169336089635763e-03, 0.03569720205330306}; 

__global__ void
__launch_bounds__(128,4)
gpu_thermal(const double *temperature_array, const double *mass_frac_array, 
  const double *avmolwt_array, const int slice_stride/*NX*NY in number of 
  doubles*/, const int row_stride/*NX in number of doubles*/, const int 
  total_steps/*NZ in number of doubles*/, const int spec_stride/*NX*NY*NZ in 
  number of doubles*/, double *thermal_out) 
{
  {
    const int offset = (blockIdx.x*blockDim.x+threadIdx.x);
    temperature_array += offset;
    mass_frac_array += offset;
    avmolwt_array += offset;
    thermal_out += offset;
  }
  // Load the temperatures
  double temperature;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  // Load the average mole weight
  double avmolwt;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(avmolwt) : 
    "l"(avmolwt_array) : "memory"); 
  double thermal[2];
  thermal[0] = 0.0;
  double mass_frac_light_0;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac_light_0) : 
    "l"(mass_frac_array+0*spec_stride) : "memory"); 
  thermal[1] = 0.0;
  double mass_frac_light_1;
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac_light_1) : 
    "l"(mass_frac_array+1*spec_stride) : "memory"); 
  temperature *= 120.0;
  double mole_frac_light_0 = mass_frac_light_0 * recip_molecular_masses[0] * 1e3 
    * avmolwt; 
  double mole_frac_light_1 = mass_frac_light_1 * recip_molecular_masses[1] * 1e3 
    * avmolwt; 
  {
    double mole_frac = mole_frac_light_0;
    // No interaction for species 0 with itself
    {
      double val = 
        __fma_rn(-9.142772693139689e-12,temperature,4.897074419931439e-08); 
      val = __fma_rn(val,temperature,-7.999935843937031e-05);
      val = __fma_rn(val,temperature,-0.1441521904321288);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mole_frac = mole_frac_light_1;
    {
      double val = 
        __fma_rn(9.142772693139689e-12,temperature,-4.897074419931439e-08); 
      val = __fma_rn(val,temperature,7.999935843937031e-05);
      val = __fma_rn(val,temperature,0.1441521904321288);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    // No interaction for species 1 with itself
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+2*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[2] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(5.627791322321943e-11,temperature,-3.417789994615262e-07); 
      val = __fma_rn(val,temperature,6.504686600507828e-04);
      val = __fma_rn(val,temperature,0.10003911024761);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.076359594171054e-11,temperature,-1.110963906990921e-07); 
      val = __fma_rn(val,temperature,1.813267135865227e-04);
      val = __fma_rn(val,temperature,0.3311911853008514);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+3*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[3] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(4.492718219288103e-11,temperature,-2.609398238358899e-07); 
      val = __fma_rn(val,temperature,4.656705986862719e-04);
      val = __fma_rn(val,temperature,0.2352831187271017);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.863023540803317e-12,temperature,-2.548468678114864e-08); 
      val = __fma_rn(val,temperature,3.847052476570652e-05);
      val = __fma_rn(val,temperature,0.4066824922592918);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+4*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[4] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(5.64804120066121e-11,temperature,-3.425645382492365e-07); 
      val = __fma_rn(val,temperature,6.506659567041789e-04);
      val = __fma_rn(val,temperature,0.10512412221484);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.064272390123945e-11,temperature,-1.101357049801305e-07); 
      val = __fma_rn(val,temperature,1.793350357124907e-04);
      val = __fma_rn(val,temperature,0.3395572425759529);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+5*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[5] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(4.526520722696853e-11,temperature,-2.629030939218756e-07); 
      val = __fma_rn(val,temperature,4.691742308374871e-04);
      val = __fma_rn(val,temperature,0.2370533518097488);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.952596333049684e-12,temperature,-2.587403103989587e-08); 
      val = __fma_rn(val,temperature,3.905826116117993e-05);
      val = __fma_rn(val,temperature,0.4128956152556185);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+6*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[6] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(5.60262093242529e-11,temperature,-3.795454892019831e-07); 
      val = __fma_rn(val,temperature,8.622468727790503e-04);
      val = __fma_rn(val,temperature,-0.1743526975317052);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.484991023772154e-11,temperature,-3.409357392384156e-07); 
      val = __fma_rn(val,temperature,6.730785866652221e-04);
      val = __fma_rn(val,temperature,0.0227472235431365);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+7*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[7] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.491249524287789e-11,temperature,-4.116889146585897e-07); 
      val = __fma_rn(val,temperature,8.398334896024519e-04);
      val = __fma_rn(val,temperature,-0.03814707647703083);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.018638410036789e-11,temperature,-2.305874430998064e-07); 
      val = __fma_rn(val,temperature,4.050725927395682e-04);
      val = __fma_rn(val,temperature,0.2580668321401118);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+8*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[8] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(5.241393353126546e-11,temperature,-3.094454842930398e-07); 
      val = __fma_rn(val,temperature,5.647937041424646e-04);
      val = __fma_rn(val,temperature,0.2001198973224177);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.231151699236785e-11,temperature,-6.03983623275141e-08); 
      val = __fma_rn(val,temperature,9.359619018176707e-05);
      val = __fma_rn(val,temperature,0.4306055468568625);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+9*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[9] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.498993101254459e-11,temperature,-4.089955152261598e-07); 
      val = __fma_rn(val,temperature,8.238121023303065e-04);
      val = __fma_rn(val,temperature,-0.01421003964199077);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.783551545571742e-11,temperature,-2.149591614672117e-07); 
      val = __fma_rn(val,temperature,3.730329487625099e-04);
      val = __fma_rn(val,temperature,0.2829743924415595);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+10*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[10] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.528593713906523e-11,temperature,-4.119691123141584e-07); 
      val = __fma_rn(val,temperature,8.334704025823057e-04);
      val = __fma_rn(val,temperature,-0.02281059439920743);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.902511428547676e-11,temperature,-2.225045809889136e-07); 
      val = __fma_rn(val,temperature,3.877928182492332e-04);
      val = __fma_rn(val,temperature,0.2767259633905499);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+11*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[11] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.106262901787082e-11,temperature,-4.078791525156344e-07); 
      val = __fma_rn(val,temperature,9.059202595899134e-04);
      val = __fma_rn(val,temperature,-0.1613575642024698);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.471615475380025e-11,temperature,-3.299652077563944e-07); 
      val = __fma_rn(val,temperature,6.212781427974868e-04);
      val = __fma_rn(val,temperature,0.1226933815442866);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+12*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[12] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.543809449468678e-11,temperature,-4.129292598355758e-07); 
      val = __fma_rn(val,temperature,8.354129136038698e-04);
      val = __fma_rn(val,temperature,-0.02286375745201874);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.92078723629348e-11,temperature,-2.235465897105197e-07); 
      val = __fma_rn(val,temperature,3.896088864712714e-04);
      val = __fma_rn(val,temperature,0.2780218956633432);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+13*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[13] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.357256669996396e-11,temperature,-4.178315068673857e-07); 
      val = __fma_rn(val,temperature,9.039013843496612e-04);
      val = __fma_rn(val,temperature,-0.1312445187253599);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.36678967039148e-11,temperature,-3.219150637684619e-07); 
      val = __fma_rn(val,temperature,6.012659551129757e-04);
      val = __fma_rn(val,temperature,0.1403143195346041);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+14*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[14] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(5.491123022431369e-11,temperature,-3.264338944017707e-07); 
      val = __fma_rn(val,temperature,6.017229023529065e-04);
      val = __fma_rn(val,temperature,0.1798402987276865);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.520903355214672e-11,temperature,-7.672987567046268e-08); 
      val = __fma_rn(val,temperature,1.204072736871709e-04);
      val = __fma_rn(val,temperature,0.4265799425063761);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+15*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[15] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(5.501707904384143e-11,temperature,-3.270631398627651e-07); 
      val = __fma_rn(val,temperature,6.02882804592151e-04);
      val = __fma_rn(val,temperature,0.1801869652155083);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.526789541701338e-11,temperature,-7.702683494519006e-08); 
      val = __fma_rn(val,temperature,1.208732728348772e-04);
      val = __fma_rn(val,temperature,0.4282308883632945);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+16*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[16] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(5.511683510614852e-11,temperature,-3.276561653654913e-07); 
      val = __fma_rn(val,temperature,6.039759417718045e-04);
      val = __fma_rn(val,temperature,0.1805136772555028);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.532346392222491e-11,temperature,-7.73071791551923e-08); 
      val = __fma_rn(val,temperature,1.213131990269295e-04);
      val = __fma_rn(val,temperature,0.4297894627248423);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+17*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[17] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.679597097339202e-11,temperature,-4.210644676604187e-07); 
      val = __fma_rn(val,temperature,8.504401145902722e-04);
      val = __fma_rn(val,temperature,-0.0200309448373045);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.051765859784344e-11,temperature,-2.307057627129864e-07); 
      val = __fma_rn(val,temperature,4.014300062227435e-04);
      val = __fma_rn(val,temperature,0.2931915230505652);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+18*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[18] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.4281019561444e-11,temperature,-4.241403755265837e-07); 
      val = __fma_rn(val,temperature,9.234296792961776e-04);
      val = __fma_rn(val,temperature,-0.1419518484883568);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.46306750914904e-11,temperature,-3.262695732605872e-07); 
      val = __fma_rn(val,temperature,6.054913027593865e-04);
      val = __fma_rn(val,temperature,0.1599911857471234);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+19*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[19] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.33759628795356e-11,temperature,-4.210744316207838e-07); 
      val = __fma_rn(val,temperature,9.271591784927813e-04);
      val = __fma_rn(val,temperature,-0.1556825544201592);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.631821141955933e-11,temperature,-3.382377920782449e-07); 
      val = __fma_rn(val,temperature,6.329413609152518e-04);
      val = __fma_rn(val,temperature,0.1422358613007126);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+20*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[20] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.678968159099615e-11,temperature,-4.305714758783602e-07); 
      val = __fma_rn(val,temperature,9.023015056603979e-04);
      val = __fma_rn(val,temperature,-0.08712275158999067);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.859640883800891e-11,temperature,-2.840231652270688e-07); 
      val = __fma_rn(val,temperature,5.111593704013715e-04);
      val = __fma_rn(val,temperature,0.2270092692143592);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+21*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[21] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.584178790541647e-11,temperature,-4.317978670798312e-07); 
      val = __fma_rn(val,temperature,9.307618485510858e-04);
      val = __fma_rn(val,temperature,-0.1305226225166914);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.444656143389017e-11,temperature,-3.234737004489394e-07); 
      val = __fma_rn(val,temperature,5.957384170040745e-04);
      val = __fma_rn(val,temperature,0.1808704574400636);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+22*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[22] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.587954147434553e-11,temperature,-4.32045459240628e-07); 
      val = __fma_rn(val,temperature,9.312955458081581e-04);
      val = __fma_rn(val,temperature,-0.1305974639659101);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.450907225208022e-11,temperature,-3.238450848880206e-07); 
      val = __fma_rn(val,temperature,5.96422392169694e-04);
      val = __fma_rn(val,temperature,0.1810781172074247);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+23*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[23] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.754758571972398e-11,temperature,-4.354574386724192e-07); 
      val = __fma_rn(val,temperature,9.125404830025142e-04);
      val = __fma_rn(val,temperature,-0.08811138773203048);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.97068072303713e-11,temperature,-2.905129218490674e-07); 
      val = __fma_rn(val,temperature,5.228390511989111e-04);
      val = __fma_rn(val,temperature,0.2321962929804006);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+24*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[24] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.792806834910504e-11,temperature,-4.379102871270937e-07); 
      val = __fma_rn(val,temperature,9.176806489860936e-04);
      val = __fma_rn(val,temperature,-0.08860770232455792);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.026873655050304e-11,temperature,-2.937971345708288e-07); 
      val = __fma_rn(val,temperature,5.287496821355728e-04);
      val = __fma_rn(val,temperature,0.2348212434111704);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+25*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[25] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.792806834910504e-11,temperature,-4.379102871270937e-07); 
      val = __fma_rn(val,temperature,9.176806489860936e-04);
      val = __fma_rn(val,temperature,-0.08860770232455792);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.026873655050304e-11,temperature,-2.937971345708288e-07); 
      val = __fma_rn(val,temperature,5.287496821355728e-04);
      val = __fma_rn(val,temperature,0.2348212434111704);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+26*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[26] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.797583052545046e-11,temperature,-4.382181944305923e-07); 
      val = __fma_rn(val,temperature,9.18325896614223e-04);
      val = __fma_rn(val,temperature,-0.08867000494565155);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.03394895217157e-11,temperature,-2.942106524276735e-07); 
      val = __fma_rn(val,temperature,5.294938944155305e-04);
      val = __fma_rn(val,temperature,0.2351517530244069);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+27*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[27] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.826609941725254e-11,temperature,-4.400894640963193e-07); 
      val = __fma_rn(val,temperature,9.222473115975025e-04);
      val = __fma_rn(val,temperature,-0.08904864164450052);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.07705125768645e-11,temperature,-2.967297795676495e-07); 
      val = __fma_rn(val,temperature,5.340275930728274e-04);
      val = __fma_rn(val,temperature,0.2371651986905267);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+28*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[28] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(6.849966660104515e-11,temperature,-4.415951961891624e-07); 
      val = __fma_rn(val,temperature,9.254027095061118e-04);
      val = __fma_rn(val,temperature,-0.08935331469052193);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.111862875856663e-11,temperature,-2.987643550055875e-07); 
      val = __fma_rn(val,temperature,5.376892391187019e-04);
      val = __fma_rn(val,temperature,0.2387913600036666);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  {
    double mass_frac;
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mass_frac) : 
      "l"(mass_frac_array+29*spec_stride) : "memory"); 
    double mole_frac = mass_frac * recip_molecular_masses[29] * 1e3 * avmolwt;
    {
      double val = 
        __fma_rn(5.228059863164392e-11,temperature,-3.085192390840869e-07); 
      val = __fma_rn(val,temperature,5.627440892870838e-04);
      val = __fma_rn(val,temperature,0.2015216426512595);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.214379926045129e-11,temperature,-5.945096155210955e-08); 
      val = __fma_rn(val,temperature,9.205368004195816e-05);
      val = __fma_rn(val,temperature,0.4313312690639463);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+0*spec_stride) , 
    "d"(thermal[0]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+1*spec_stride) , 
    "d"(thermal[1]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+2*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+3*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+4*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+5*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+6*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+7*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+8*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+9*spec_stride) , 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+10*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+11*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+12*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+13*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+14*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+15*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+16*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+17*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+18*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+19*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+20*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+21*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+22*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+23*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+24*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+25*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+26*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+27*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+28*spec_stride) 
    , "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+29*spec_stride) 
    , "d"(0.0) : "memory"); 
}

