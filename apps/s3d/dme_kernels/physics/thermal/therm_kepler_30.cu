
/*
 * Kepler tuned thermal kernel for DME.
 * Gets 4 CTAs with 4 warps per CTA.
 * Two versions, one which uses 64-byte
 * loads and stores, and one which
 * uses 128-byte loads and stores.
 * Should compile to 83 and 101 registers.
 * Completely memory bandwidth limited.
 *
 * Can tune DIMX and DIMY for each CTA
 * Probably want at least 4 warps per CTA
 * since max number of CTAs/SM is 16 on Kepler.
 *
 * Launch with (64-byte version):
 *  dim3 grid(nx/DIMX,ny/DIMY,nz);
 *  dim3 block(DIMX,DIMY,1);
 *
 * Launch with (128-byte version):
 *  dim3 grid(nx/(2*DIMX),ny/DIMY,nz);
 *  dim3 grid(DIMX,DIMY,1);
 *
 * gpu_thermal<<<grid,block>>>(temperature_ptr,  
 *                 mole_frac_input_ptr, nx*ny, nx, nz, nx*ny*nz, output_d);
 *
 * Performance numbers on K20c with 13 SMs and 5 memory partitions:
 * 
 * 32x32x32 (DIMX=32, DIMY=4) 64-byte loads/stores
 *   Latency: 0.104 ms
 *   Throughput: 313.610 Mpoints/s
 *   Perf: 153.0 GB/s (77.8 GB/s read, 75.2 GB/s write)
 *
 * 64x64x64 (DIMX=32, DIMY=4) 128-byte loads/stores
 *   Latency: 0.784 ms
 *   Throughput: 334.410 Mpoints/s
 *   Perf: 163.2 GB/s (80.9 GB/s read, 80.3 GB/s write)
 *
 * 128x128x128 (DIMX=32, DIMY=4) 128-byte loads/stores
 *   Latency: 6.228 ms
 *   Throughput: 336.743 Mpoints/
 *   Perf: 164.3 GB/s (83.5 GB/s read, 80.8 GB/s write) 
 *
 * Generation command:
 *   May need to disable diffusion kernel generation to avoid warnings
 *   ./s3dgen -dir inputs/DME_sk39/ -gpu -p 1 -c 1 -t 32  
 *
 */

__global__ void
gpu_thermal_64(const double *temperature_array, const double *mole_frac_array, 
  const int slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX 
  in number of doubles*/, const int total_steps/*NZ in number of doubles*/, 
  const int spec_stride/*NX*NY*NZ in number of doubles*/, double *thermal_out) 
{
  {
    const int offset = (blockIdx.y*blockDim.y+threadIdx.y)*row_stride + 
      (blockIdx.x*blockDim.x + threadIdx.x)*sizeof(double)/sizeof(double) + 
      blockIdx.z*slice_stride; 
    temperature_array += offset;
    mole_frac_array += offset;
    thermal_out += offset;
  }
  double temperature;
  #if __CUDA_ARCH__ >= 350
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  #else
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(temperature) : 
    "l"(temperature_array) : "memory"); 
  #endif
  double thermal[2];
  thermal[0] = 0.0;
  double mole_frac_light_0;
  #if __CUDA_ARCH__ >= 350
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_light_0) : 
    "l"(mole_frac_array+0*spec_stride) : "memory"); 
  #else
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_light_0) : 
    "l"(mole_frac_array+0*spec_stride) : "memory"); 
  #endif
  thermal[1] = 0.0;
  double mole_frac_light_1;
  #if __CUDA_ARCH__ >= 350
  asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac_light_1) : 
    "l"(mole_frac_array+1*spec_stride) : "memory"); 
  #else
  asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac_light_1) : 
    "l"(mole_frac_array+1*spec_stride) : "memory"); 
  #endif
  // Interactions for species H
  {
    double mole_frac = mole_frac_light_0;
    // No interaction for species 0 with itself
    {
      double val = 
        __fma_rn(-9.142772693139689e-12,temperature,4.897074419931439e-08); 
      val = __fma_rn(val,temperature,-7.999935843937031e-05);
      val = __fma_rn(val,temperature,-0.1441521904321288);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species H2
  {
    double mole_frac = mole_frac_light_1;
    {
      double val = 
        __fma_rn(9.142772693139689e-12,temperature,-4.897074419931439e-08); 
      val = __fma_rn(val,temperature,7.999935843937031e-05);
      val = __fma_rn(val,temperature,0.1441521904321288);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    // No interaction for species 1 with itself
  }
  // Interactions for species CH3
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+2*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+2*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(5.627791322321943e-11,temperature,-3.417789994615262e-07); 
      val = __fma_rn(val,temperature,6.504686600507828e-04);
      val = __fma_rn(val,temperature,0.10003911024761);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.076359594171054e-11,temperature,-1.110963906990921e-07); 
      val = __fma_rn(val,temperature,1.813267135865227e-04);
      val = __fma_rn(val,temperature,0.3311911853008514);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species O
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+3*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+3*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(4.492718219288103e-11,temperature,-2.609398238358899e-07); 
      val = __fma_rn(val,temperature,4.656705986862719e-04);
      val = __fma_rn(val,temperature,0.2352831187271017);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.863023540803317e-12,temperature,-2.548468678114864e-08); 
      val = __fma_rn(val,temperature,3.847052476570652e-05);
      val = __fma_rn(val,temperature,0.4066824922592918);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CH4
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+4*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+4*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(5.64804120066121e-11,temperature,-3.425645382492365e-07); 
      val = __fma_rn(val,temperature,6.506659567041789e-04);
      val = __fma_rn(val,temperature,0.10512412221484);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(2.064272390123945e-11,temperature,-1.101357049801305e-07); 
      val = __fma_rn(val,temperature,1.793350357124907e-04);
      val = __fma_rn(val,temperature,0.3395572425759529);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species OH
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+5*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+5*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(4.526520722696853e-11,temperature,-2.629030939218756e-07); 
      val = __fma_rn(val,temperature,4.691742308374871e-04);
      val = __fma_rn(val,temperature,0.2370533518097488);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.952596333049684e-12,temperature,-2.587403103989587e-08); 
      val = __fma_rn(val,temperature,3.905826116117993e-05);
      val = __fma_rn(val,temperature,0.4128956152556185);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species H2O
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+6*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+6*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(5.60262093242529e-11,temperature,-3.795454892019831e-07); 
      val = __fma_rn(val,temperature,8.622468727790503e-04);
      val = __fma_rn(val,temperature,-0.1743526975317052);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.484991023772154e-11,temperature,-3.409357392384156e-07); 
      val = __fma_rn(val,temperature,6.730785866652221e-04);
      val = __fma_rn(val,temperature,0.0227472235431365);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species C2H2
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+7*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+7*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.491249524287789e-11,temperature,-4.116889146585897e-07); 
      val = __fma_rn(val,temperature,8.398334896024519e-04);
      val = __fma_rn(val,temperature,-0.03814707647703083);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.018638410036789e-11,temperature,-2.305874430998064e-07); 
      val = __fma_rn(val,temperature,4.050725927395682e-04);
      val = __fma_rn(val,temperature,0.2580668321401118);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CO
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+8*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+8*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(5.241393353126546e-11,temperature,-3.094454842930398e-07); 
      val = __fma_rn(val,temperature,5.647937041424646e-04);
      val = __fma_rn(val,temperature,0.2001198973224177);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.231151699236785e-11,temperature,-6.03983623275141e-08); 
      val = __fma_rn(val,temperature,9.359619018176707e-05);
      val = __fma_rn(val,temperature,0.4306055468568625);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species C2H4
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+9*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+9*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.498993101254459e-11,temperature,-4.089955152261598e-07); 
      val = __fma_rn(val,temperature,8.238121023303065e-04);
      val = __fma_rn(val,temperature,-0.01421003964199077);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.783551545571742e-11,temperature,-2.149591614672117e-07); 
      val = __fma_rn(val,temperature,3.730329487625099e-04);
      val = __fma_rn(val,temperature,0.2829743924415595);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species C2H5
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+10*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+10*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.528593713906523e-11,temperature,-4.119691123141584e-07); 
      val = __fma_rn(val,temperature,8.334704025823057e-04);
      val = __fma_rn(val,temperature,-0.02281059439920743);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.902511428547676e-11,temperature,-2.225045809889136e-07); 
      val = __fma_rn(val,temperature,3.877928182492332e-04);
      val = __fma_rn(val,temperature,0.2767259633905499);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CH2O
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+11*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+11*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.106262901787082e-11,temperature,-4.078791525156344e-07); 
      val = __fma_rn(val,temperature,9.059202595899134e-04);
      val = __fma_rn(val,temperature,-0.1613575642024698);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.471615475380025e-11,temperature,-3.299652077563944e-07); 
      val = __fma_rn(val,temperature,6.212781427974868e-04);
      val = __fma_rn(val,temperature,0.1226933815442866);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species C2H6
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+12*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+12*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.543809449468678e-11,temperature,-4.129292598355758e-07); 
      val = __fma_rn(val,temperature,8.354129136038698e-04);
      val = __fma_rn(val,temperature,-0.02286375745201874);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(3.92078723629348e-11,temperature,-2.235465897105197e-07); 
      val = __fma_rn(val,temperature,3.896088864712714e-04);
      val = __fma_rn(val,temperature,0.2780218956633432);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CH3O
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+13*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+13*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.357256669996396e-11,temperature,-4.178315068673857e-07); 
      val = __fma_rn(val,temperature,9.039013843496612e-04);
      val = __fma_rn(val,temperature,-0.1312445187253599);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.36678967039148e-11,temperature,-3.219150637684619e-07); 
      val = __fma_rn(val,temperature,6.012659551129757e-04);
      val = __fma_rn(val,temperature,0.1403143195346041);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species O2
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+14*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+14*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(5.491123022431369e-11,temperature,-3.264338944017707e-07); 
      val = __fma_rn(val,temperature,6.017229023529065e-04);
      val = __fma_rn(val,temperature,0.1798402987276865);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.520903355214672e-11,temperature,-7.672987567046268e-08); 
      val = __fma_rn(val,temperature,1.204072736871709e-04);
      val = __fma_rn(val,temperature,0.4265799425063761);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species HO2
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+15*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+15*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(5.501707904384143e-11,temperature,-3.270631398627651e-07); 
      val = __fma_rn(val,temperature,6.02882804592151e-04);
      val = __fma_rn(val,temperature,0.1801869652155083);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.526789541701338e-11,temperature,-7.702683494519006e-08); 
      val = __fma_rn(val,temperature,1.208732728348772e-04);
      val = __fma_rn(val,temperature,0.4282308883632945);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species H2O2
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+16*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+16*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(5.511683510614852e-11,temperature,-3.276561653654913e-07); 
      val = __fma_rn(val,temperature,6.039759417718045e-04);
      val = __fma_rn(val,temperature,0.1805136772555028);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.532346392222491e-11,temperature,-7.73071791551923e-08); 
      val = __fma_rn(val,temperature,1.213131990269295e-04);
      val = __fma_rn(val,temperature,0.4297894627248423);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CO2
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+17*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+17*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.679597097339202e-11,temperature,-4.210644676604187e-07); 
      val = __fma_rn(val,temperature,8.504401145902722e-04);
      val = __fma_rn(val,temperature,-0.0200309448373045);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.051765859784344e-11,temperature,-2.307057627129864e-07); 
      val = __fma_rn(val,temperature,4.014300062227435e-04);
      val = __fma_rn(val,temperature,0.2931915230505652);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CH3HCO
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+18*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+18*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.4281019561444e-11,temperature,-4.241403755265837e-07); 
      val = __fma_rn(val,temperature,9.234296792961776e-04);
      val = __fma_rn(val,temperature,-0.1419518484883568);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.46306750914904e-11,temperature,-3.262695732605872e-07); 
      val = __fma_rn(val,temperature,6.054913027593865e-04);
      val = __fma_rn(val,temperature,0.1599911857471234);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species HCOOH
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+19*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+19*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.33759628795356e-11,temperature,-4.210744316207838e-07); 
      val = __fma_rn(val,temperature,9.271591784927813e-04);
      val = __fma_rn(val,temperature,-0.1556825544201592);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.631821141955933e-11,temperature,-3.382377920782449e-07); 
      val = __fma_rn(val,temperature,6.329413609152518e-04);
      val = __fma_rn(val,temperature,0.1422358613007126);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CH3OCH3
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+20*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+20*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.678968159099615e-11,temperature,-4.305714758783602e-07); 
      val = __fma_rn(val,temperature,9.023015056603979e-04);
      val = __fma_rn(val,temperature,-0.08712275158999067);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.859640883800891e-11,temperature,-2.840231652270688e-07); 
      val = __fma_rn(val,temperature,5.111593704013715e-04);
      val = __fma_rn(val,temperature,0.2270092692143592);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CH3OCO
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+21*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+21*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.584178790541647e-11,temperature,-4.317978670798312e-07); 
      val = __fma_rn(val,temperature,9.307618485510858e-04);
      val = __fma_rn(val,temperature,-0.1305226225166914);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.444656143389017e-11,temperature,-3.234737004489394e-07); 
      val = __fma_rn(val,temperature,5.957384170040745e-04);
      val = __fma_rn(val,temperature,0.1808704574400636);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CH3OCHO
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+22*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+22*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.587954147434553e-11,temperature,-4.32045459240628e-07); 
      val = __fma_rn(val,temperature,9.312955458081581e-04);
      val = __fma_rn(val,temperature,-0.1305974639659101);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.450907225208022e-11,temperature,-3.238450848880206e-07); 
      val = __fma_rn(val,temperature,5.96422392169694e-04);
      val = __fma_rn(val,temperature,0.1810781172074247);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CH3OCH2OH
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+23*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+23*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.754758571972398e-11,temperature,-4.354574386724192e-07); 
      val = __fma_rn(val,temperature,9.125404830025142e-04);
      val = __fma_rn(val,temperature,-0.08811138773203048);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(4.97068072303713e-11,temperature,-2.905129218490674e-07); 
      val = __fma_rn(val,temperature,5.228390511989111e-04);
      val = __fma_rn(val,temperature,0.2321962929804006);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species OCH2OCHO
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+24*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+24*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.792806834910504e-11,temperature,-4.379102871270937e-07); 
      val = __fma_rn(val,temperature,9.176806489860936e-04);
      val = __fma_rn(val,temperature,-0.08860770232455792);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.026873655050304e-11,temperature,-2.937971345708288e-07); 
      val = __fma_rn(val,temperature,5.287496821355728e-04);
      val = __fma_rn(val,temperature,0.2348212434111704);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species HOCH2OCO
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+25*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+25*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.792806834910504e-11,temperature,-4.379102871270937e-07); 
      val = __fma_rn(val,temperature,9.176806489860936e-04);
      val = __fma_rn(val,temperature,-0.08860770232455792);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.026873655050304e-11,temperature,-2.937971345708288e-07); 
      val = __fma_rn(val,temperature,5.287496821355728e-04);
      val = __fma_rn(val,temperature,0.2348212434111704);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species CH3OCH2O2
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+26*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+26*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.797583052545046e-11,temperature,-4.382181944305923e-07); 
      val = __fma_rn(val,temperature,9.18325896614223e-04);
      val = __fma_rn(val,temperature,-0.08867000494565155);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.03394895217157e-11,temperature,-2.942106524276735e-07); 
      val = __fma_rn(val,temperature,5.294938944155305e-04);
      val = __fma_rn(val,temperature,0.2351517530244069);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species HO2CH2OCHO
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+27*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+27*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.826609941725254e-11,temperature,-4.400894640963193e-07); 
      val = __fma_rn(val,temperature,9.222473115975025e-04);
      val = __fma_rn(val,temperature,-0.08904864164450052);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.07705125768645e-11,temperature,-2.967297795676495e-07); 
      val = __fma_rn(val,temperature,5.340275930728274e-04);
      val = __fma_rn(val,temperature,0.2371651986905267);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species O2CH2OCH2O2H
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+28*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+28*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(6.849966660104515e-11,temperature,-4.415951961891624e-07); 
      val = __fma_rn(val,temperature,9.254027095061118e-04);
      val = __fma_rn(val,temperature,-0.08935331469052193);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(5.111862875856663e-11,temperature,-2.987643550055875e-07); 
      val = __fma_rn(val,temperature,5.376892391187019e-04);
      val = __fma_rn(val,temperature,0.2387913600036666);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  // Interactions for species N2
  {
    double mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+29*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.f64 %0, [%1];" : "=d"(mole_frac) : 
      "l"(mole_frac_array+29*spec_stride) : "memory"); 
    #endif
    {
      double val = 
        __fma_rn(5.228059863164392e-11,temperature,-3.085192390840869e-07); 
      val = __fma_rn(val,temperature,5.627440892870838e-04);
      val = __fma_rn(val,temperature,0.2015216426512595);
      val = val * mole_frac;
      thermal[0] = __fma_rn(mole_frac_light_0,val,thermal[0]);
    }
    {
      double val = 
        __fma_rn(1.214379926045129e-11,temperature,-5.945096155210955e-08); 
      val = __fma_rn(val,temperature,9.205368004195816e-05);
      val = __fma_rn(val,temperature,0.4313312690639463);
      val = val * mole_frac;
      thermal[1] = __fma_rn(mole_frac_light_1,val,thermal[1]);
    }
  }
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+0*spec_stride), 
    "d"(thermal[0]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+1*spec_stride), 
    "d"(thermal[1]) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+2*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+3*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+4*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+5*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+6*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+7*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+8*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+9*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+10*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+11*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+12*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+13*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+14*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+15*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+16*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+17*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+18*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+19*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+20*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+21*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+22*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+23*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+24*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+25*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+26*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+27*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+28*spec_stride), 
    "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(thermal_out+29*spec_stride), 
    "d"(0.0) : "memory"); 
}

__global__ void
gpu_thermal_128(const double *temperature_array, const double *mole_frac_array, 
  const int slice_stride/*NX*NY in number of doubles*/, const int row_stride/*NX 
  in number of doubles*/, const int total_steps/*NZ in number of doubles*/, 
  const int spec_stride/*NX*NY*NZ in number of doubles*/, double *thermal_out) 
{
  {
    const int offset = (blockIdx.y*blockDim.y+threadIdx.y)*row_stride + 
      (blockIdx.x*blockDim.x + threadIdx.x)*sizeof(double2)/sizeof(double) + 
      blockIdx.z*slice_stride; 
    temperature_array += offset;
    mole_frac_array += offset;
    thermal_out += offset;
  }
  double2 temperature;
  #if __CUDA_ARCH__ >= 350
  asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(temperature.x), 
    "=d"(temperature.y) : "l"(temperature_array) : "memory"); 
  #else
  asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(temperature.x), 
    "=d"(temperature.y) : "l"(temperature_array) : "memory"); 
  #endif
  double2 thermal[2];
  thermal[0] = make_double2(0.0,0.0);
  double2 mole_frac_light_0;
  #if __CUDA_ARCH__ >= 350
  asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : 
    "=d"(mole_frac_light_0.x), "=d"(mole_frac_light_0.y) : 
    "l"(mole_frac_array+0*spec_stride) : "memory"); 
  #else
  asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac_light_0.x), 
    "=d"(mole_frac_light_0.y) : "l"(mole_frac_array+0*spec_stride) : "memory"); 
  #endif
  thermal[1] = make_double2(0.0,0.0);
  double2 mole_frac_light_1;
  #if __CUDA_ARCH__ >= 350
  asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : 
    "=d"(mole_frac_light_1.x), "=d"(mole_frac_light_1.y) : 
    "l"(mole_frac_array+1*spec_stride) : "memory"); 
  #else
  asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac_light_1.x), 
    "=d"(mole_frac_light_1.y) : "l"(mole_frac_array+1*spec_stride) : "memory"); 
  #endif
  // Interactions for species H
  {
    double2 mole_frac = mole_frac_light_0;
    // No interaction for species 0 with itself
    {
      double2 val = 
        make_double2(__fma_rn(-9.142772693139689e-12,temperature.x,4.897074419931439e-08), 
        __fma_rn(-9.142772693139689e-12,temperature.y,4.897074419931439e-08)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-7.999935843937031e-05), 
        __fma_rn(val.y,temperature.y,-7.999935843937031e-05)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.1441521904321288), 
        __fma_rn(val.y,temperature.y,-0.1441521904321288)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species H2
  {
    double2 mole_frac = mole_frac_light_1;
    {
      double2 val = 
        make_double2(__fma_rn(9.142772693139689e-12,temperature.x,-4.897074419931439e-08), 
        __fma_rn(9.142772693139689e-12,temperature.y,-4.897074419931439e-08)); 
      val = make_double2( __fma_rn(val.x,temperature.x,7.999935843937031e-05), 
        __fma_rn(val.y,temperature.y,7.999935843937031e-05)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.1441521904321288), 
        __fma_rn(val.y,temperature.y,0.1441521904321288)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    // No interaction for species 1 with itself
  }
  // Interactions for species CH3
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+2*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+2*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(5.627791322321943e-11,temperature.x,-3.417789994615262e-07), 
        __fma_rn(5.627791322321943e-11,temperature.y,-3.417789994615262e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,6.504686600507828e-04), 
        __fma_rn(val.y,temperature.y,6.504686600507828e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.10003911024761), 
        __fma_rn(val.y,temperature.y,0.10003911024761)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(2.076359594171054e-11,temperature.x,-1.110963906990921e-07), 
        __fma_rn(2.076359594171054e-11,temperature.y,-1.110963906990921e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,1.813267135865227e-04), 
        __fma_rn(val.y,temperature.y,1.813267135865227e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.3311911853008514), 
        __fma_rn(val.y,temperature.y,0.3311911853008514)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species O
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+3*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+3*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(4.492718219288103e-11,temperature.x,-2.609398238358899e-07), 
        __fma_rn(4.492718219288103e-11,temperature.y,-2.609398238358899e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,4.656705986862719e-04), 
        __fma_rn(val.y,temperature.y,4.656705986862719e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2352831187271017), 
        __fma_rn(val.y,temperature.y,0.2352831187271017)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.863023540803317e-12,temperature.x,-2.548468678114864e-08), 
        __fma_rn(5.863023540803317e-12,temperature.y,-2.548468678114864e-08)); 
      val = make_double2( __fma_rn(val.x,temperature.x,3.847052476570652e-05), 
        __fma_rn(val.y,temperature.y,3.847052476570652e-05)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.4066824922592918), 
        __fma_rn(val.y,temperature.y,0.4066824922592918)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CH4
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+4*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+4*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(5.64804120066121e-11,temperature.x,-3.425645382492365e-07), 
        __fma_rn(5.64804120066121e-11,temperature.y,-3.425645382492365e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,6.506659567041789e-04), 
        __fma_rn(val.y,temperature.y,6.506659567041789e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.10512412221484), 
        __fma_rn(val.y,temperature.y,0.10512412221484)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(2.064272390123945e-11,temperature.x,-1.101357049801305e-07), 
        __fma_rn(2.064272390123945e-11,temperature.y,-1.101357049801305e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,1.793350357124907e-04), 
        __fma_rn(val.y,temperature.y,1.793350357124907e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.3395572425759529), 
        __fma_rn(val.y,temperature.y,0.3395572425759529)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species OH
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+5*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+5*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(4.526520722696853e-11,temperature.x,-2.629030939218756e-07), 
        __fma_rn(4.526520722696853e-11,temperature.y,-2.629030939218756e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,4.691742308374871e-04), 
        __fma_rn(val.y,temperature.y,4.691742308374871e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2370533518097488), 
        __fma_rn(val.y,temperature.y,0.2370533518097488)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.952596333049684e-12,temperature.x,-2.587403103989587e-08), 
        __fma_rn(5.952596333049684e-12,temperature.y,-2.587403103989587e-08)); 
      val = make_double2( __fma_rn(val.x,temperature.x,3.905826116117993e-05), 
        __fma_rn(val.y,temperature.y,3.905826116117993e-05)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.4128956152556185), 
        __fma_rn(val.y,temperature.y,0.4128956152556185)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species H2O
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+6*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+6*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(5.60262093242529e-11,temperature.x,-3.795454892019831e-07), 
        __fma_rn(5.60262093242529e-11,temperature.y,-3.795454892019831e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,8.622468727790503e-04), 
        __fma_rn(val.y,temperature.y,8.622468727790503e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.1743526975317052), 
        __fma_rn(val.y,temperature.y,-0.1743526975317052)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.484991023772154e-11,temperature.x,-3.409357392384156e-07), 
        __fma_rn(5.484991023772154e-11,temperature.y,-3.409357392384156e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,6.730785866652221e-04), 
        __fma_rn(val.y,temperature.y,6.730785866652221e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.0227472235431365), 
        __fma_rn(val.y,temperature.y,0.0227472235431365)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species C2H2
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+7*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+7*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.491249524287789e-11,temperature.x,-4.116889146585897e-07), 
        __fma_rn(6.491249524287789e-11,temperature.y,-4.116889146585897e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,8.398334896024519e-04), 
        __fma_rn(val.y,temperature.y,8.398334896024519e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.03814707647703083), 
        __fma_rn(val.y,temperature.y,-0.03814707647703083)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(4.018638410036789e-11,temperature.x,-2.305874430998064e-07), 
        __fma_rn(4.018638410036789e-11,temperature.y,-2.305874430998064e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,4.050725927395682e-04), 
        __fma_rn(val.y,temperature.y,4.050725927395682e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2580668321401118), 
        __fma_rn(val.y,temperature.y,0.2580668321401118)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CO
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+8*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+8*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(5.241393353126546e-11,temperature.x,-3.094454842930398e-07), 
        __fma_rn(5.241393353126546e-11,temperature.y,-3.094454842930398e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.647937041424646e-04), 
        __fma_rn(val.y,temperature.y,5.647937041424646e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2001198973224177), 
        __fma_rn(val.y,temperature.y,0.2001198973224177)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(1.231151699236785e-11,temperature.x,-6.03983623275141e-08), 
        __fma_rn(1.231151699236785e-11,temperature.y,-6.03983623275141e-08)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.359619018176707e-05), 
        __fma_rn(val.y,temperature.y,9.359619018176707e-05)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.4306055468568625), 
        __fma_rn(val.y,temperature.y,0.4306055468568625)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species C2H4
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+9*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+9*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.498993101254459e-11,temperature.x,-4.089955152261598e-07), 
        __fma_rn(6.498993101254459e-11,temperature.y,-4.089955152261598e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,8.238121023303065e-04), 
        __fma_rn(val.y,temperature.y,8.238121023303065e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.01421003964199077), 
        __fma_rn(val.y,temperature.y,-0.01421003964199077)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(3.783551545571742e-11,temperature.x,-2.149591614672117e-07), 
        __fma_rn(3.783551545571742e-11,temperature.y,-2.149591614672117e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,3.730329487625099e-04), 
        __fma_rn(val.y,temperature.y,3.730329487625099e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2829743924415595), 
        __fma_rn(val.y,temperature.y,0.2829743924415595)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species C2H5
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+10*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+10*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.528593713906523e-11,temperature.x,-4.119691123141584e-07), 
        __fma_rn(6.528593713906523e-11,temperature.y,-4.119691123141584e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,8.334704025823057e-04), 
        __fma_rn(val.y,temperature.y,8.334704025823057e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.02281059439920743), 
        __fma_rn(val.y,temperature.y,-0.02281059439920743)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(3.902511428547676e-11,temperature.x,-2.225045809889136e-07), 
        __fma_rn(3.902511428547676e-11,temperature.y,-2.225045809889136e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,3.877928182492332e-04), 
        __fma_rn(val.y,temperature.y,3.877928182492332e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2767259633905499), 
        __fma_rn(val.y,temperature.y,0.2767259633905499)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CH2O
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+11*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+11*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.106262901787082e-11,temperature.x,-4.078791525156344e-07), 
        __fma_rn(6.106262901787082e-11,temperature.y,-4.078791525156344e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.059202595899134e-04), 
        __fma_rn(val.y,temperature.y,9.059202595899134e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.1613575642024698), 
        __fma_rn(val.y,temperature.y,-0.1613575642024698)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.471615475380025e-11,temperature.x,-3.299652077563944e-07), 
        __fma_rn(5.471615475380025e-11,temperature.y,-3.299652077563944e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,6.212781427974868e-04), 
        __fma_rn(val.y,temperature.y,6.212781427974868e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.1226933815442866), 
        __fma_rn(val.y,temperature.y,0.1226933815442866)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species C2H6
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+12*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+12*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.543809449468678e-11,temperature.x,-4.129292598355758e-07), 
        __fma_rn(6.543809449468678e-11,temperature.y,-4.129292598355758e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,8.354129136038698e-04), 
        __fma_rn(val.y,temperature.y,8.354129136038698e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.02286375745201874), 
        __fma_rn(val.y,temperature.y,-0.02286375745201874)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(3.92078723629348e-11,temperature.x,-2.235465897105197e-07), 
        __fma_rn(3.92078723629348e-11,temperature.y,-2.235465897105197e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,3.896088864712714e-04), 
        __fma_rn(val.y,temperature.y,3.896088864712714e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2780218956633432), 
        __fma_rn(val.y,temperature.y,0.2780218956633432)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CH3O
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+13*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+13*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.357256669996396e-11,temperature.x,-4.178315068673857e-07), 
        __fma_rn(6.357256669996396e-11,temperature.y,-4.178315068673857e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.039013843496612e-04), 
        __fma_rn(val.y,temperature.y,9.039013843496612e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.1312445187253599), 
        __fma_rn(val.y,temperature.y,-0.1312445187253599)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.36678967039148e-11,temperature.x,-3.219150637684619e-07), 
        __fma_rn(5.36678967039148e-11,temperature.y,-3.219150637684619e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,6.012659551129757e-04), 
        __fma_rn(val.y,temperature.y,6.012659551129757e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.1403143195346041), 
        __fma_rn(val.y,temperature.y,0.1403143195346041)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species O2
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+14*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+14*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(5.491123022431369e-11,temperature.x,-3.264338944017707e-07), 
        __fma_rn(5.491123022431369e-11,temperature.y,-3.264338944017707e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,6.017229023529065e-04), 
        __fma_rn(val.y,temperature.y,6.017229023529065e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.1798402987276865), 
        __fma_rn(val.y,temperature.y,0.1798402987276865)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(1.520903355214672e-11,temperature.x,-7.672987567046268e-08), 
        __fma_rn(1.520903355214672e-11,temperature.y,-7.672987567046268e-08)); 
      val = make_double2( __fma_rn(val.x,temperature.x,1.204072736871709e-04), 
        __fma_rn(val.y,temperature.y,1.204072736871709e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.4265799425063761), 
        __fma_rn(val.y,temperature.y,0.4265799425063761)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species HO2
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+15*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+15*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(5.501707904384143e-11,temperature.x,-3.270631398627651e-07), 
        __fma_rn(5.501707904384143e-11,temperature.y,-3.270631398627651e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,6.02882804592151e-04), 
        __fma_rn(val.y,temperature.y,6.02882804592151e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.1801869652155083), 
        __fma_rn(val.y,temperature.y,0.1801869652155083)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(1.526789541701338e-11,temperature.x,-7.702683494519006e-08), 
        __fma_rn(1.526789541701338e-11,temperature.y,-7.702683494519006e-08)); 
      val = make_double2( __fma_rn(val.x,temperature.x,1.208732728348772e-04), 
        __fma_rn(val.y,temperature.y,1.208732728348772e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.4282308883632945), 
        __fma_rn(val.y,temperature.y,0.4282308883632945)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species H2O2
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+16*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+16*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(5.511683510614852e-11,temperature.x,-3.276561653654913e-07), 
        __fma_rn(5.511683510614852e-11,temperature.y,-3.276561653654913e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,6.039759417718045e-04), 
        __fma_rn(val.y,temperature.y,6.039759417718045e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.1805136772555028), 
        __fma_rn(val.y,temperature.y,0.1805136772555028)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(1.532346392222491e-11,temperature.x,-7.73071791551923e-08), 
        __fma_rn(1.532346392222491e-11,temperature.y,-7.73071791551923e-08)); 
      val = make_double2( __fma_rn(val.x,temperature.x,1.213131990269295e-04), 
        __fma_rn(val.y,temperature.y,1.213131990269295e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.4297894627248423), 
        __fma_rn(val.y,temperature.y,0.4297894627248423)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CO2
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+17*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+17*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.679597097339202e-11,temperature.x,-4.210644676604187e-07), 
        __fma_rn(6.679597097339202e-11,temperature.y,-4.210644676604187e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,8.504401145902722e-04), 
        __fma_rn(val.y,temperature.y,8.504401145902722e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.0200309448373045), 
        __fma_rn(val.y,temperature.y,-0.0200309448373045)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(4.051765859784344e-11,temperature.x,-2.307057627129864e-07), 
        __fma_rn(4.051765859784344e-11,temperature.y,-2.307057627129864e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,4.014300062227435e-04), 
        __fma_rn(val.y,temperature.y,4.014300062227435e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2931915230505652), 
        __fma_rn(val.y,temperature.y,0.2931915230505652)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CH3HCO
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+18*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+18*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.4281019561444e-11,temperature.x,-4.241403755265837e-07), 
        __fma_rn(6.4281019561444e-11,temperature.y,-4.241403755265837e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.234296792961776e-04), 
        __fma_rn(val.y,temperature.y,9.234296792961776e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.1419518484883568), 
        __fma_rn(val.y,temperature.y,-0.1419518484883568)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.46306750914904e-11,temperature.x,-3.262695732605872e-07), 
        __fma_rn(5.46306750914904e-11,temperature.y,-3.262695732605872e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,6.054913027593865e-04), 
        __fma_rn(val.y,temperature.y,6.054913027593865e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.1599911857471234), 
        __fma_rn(val.y,temperature.y,0.1599911857471234)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species HCOOH
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+19*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+19*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.33759628795356e-11,temperature.x,-4.210744316207838e-07), 
        __fma_rn(6.33759628795356e-11,temperature.y,-4.210744316207838e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.271591784927813e-04), 
        __fma_rn(val.y,temperature.y,9.271591784927813e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.1556825544201592), 
        __fma_rn(val.y,temperature.y,-0.1556825544201592)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.631821141955933e-11,temperature.x,-3.382377920782449e-07), 
        __fma_rn(5.631821141955933e-11,temperature.y,-3.382377920782449e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,6.329413609152518e-04), 
        __fma_rn(val.y,temperature.y,6.329413609152518e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.1422358613007126), 
        __fma_rn(val.y,temperature.y,0.1422358613007126)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CH3OCH3
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+20*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+20*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.678968159099615e-11,temperature.x,-4.305714758783602e-07), 
        __fma_rn(6.678968159099615e-11,temperature.y,-4.305714758783602e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.023015056603979e-04), 
        __fma_rn(val.y,temperature.y,9.023015056603979e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.08712275158999067), 
        __fma_rn(val.y,temperature.y,-0.08712275158999067)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(4.859640883800891e-11,temperature.x,-2.840231652270688e-07), 
        __fma_rn(4.859640883800891e-11,temperature.y,-2.840231652270688e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.111593704013715e-04), 
        __fma_rn(val.y,temperature.y,5.111593704013715e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2270092692143592), 
        __fma_rn(val.y,temperature.y,0.2270092692143592)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CH3OCO
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+21*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+21*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.584178790541647e-11,temperature.x,-4.317978670798312e-07), 
        __fma_rn(6.584178790541647e-11,temperature.y,-4.317978670798312e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.307618485510858e-04), 
        __fma_rn(val.y,temperature.y,9.307618485510858e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.1305226225166914), 
        __fma_rn(val.y,temperature.y,-0.1305226225166914)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.444656143389017e-11,temperature.x,-3.234737004489394e-07), 
        __fma_rn(5.444656143389017e-11,temperature.y,-3.234737004489394e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.957384170040745e-04), 
        __fma_rn(val.y,temperature.y,5.957384170040745e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.1808704574400636), 
        __fma_rn(val.y,temperature.y,0.1808704574400636)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CH3OCHO
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+22*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+22*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.587954147434553e-11,temperature.x,-4.32045459240628e-07), 
        __fma_rn(6.587954147434553e-11,temperature.y,-4.32045459240628e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.312955458081581e-04), 
        __fma_rn(val.y,temperature.y,9.312955458081581e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.1305974639659101), 
        __fma_rn(val.y,temperature.y,-0.1305974639659101)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.450907225208022e-11,temperature.x,-3.238450848880206e-07), 
        __fma_rn(5.450907225208022e-11,temperature.y,-3.238450848880206e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.96422392169694e-04), 
        __fma_rn(val.y,temperature.y,5.96422392169694e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.1810781172074247), 
        __fma_rn(val.y,temperature.y,0.1810781172074247)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CH3OCH2OH
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+23*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+23*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.754758571972398e-11,temperature.x,-4.354574386724192e-07), 
        __fma_rn(6.754758571972398e-11,temperature.y,-4.354574386724192e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.125404830025142e-04), 
        __fma_rn(val.y,temperature.y,9.125404830025142e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.08811138773203048), 
        __fma_rn(val.y,temperature.y,-0.08811138773203048)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(4.97068072303713e-11,temperature.x,-2.905129218490674e-07), 
        __fma_rn(4.97068072303713e-11,temperature.y,-2.905129218490674e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.228390511989111e-04), 
        __fma_rn(val.y,temperature.y,5.228390511989111e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2321962929804006), 
        __fma_rn(val.y,temperature.y,0.2321962929804006)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species OCH2OCHO
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+24*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+24*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.792806834910504e-11,temperature.x,-4.379102871270937e-07), 
        __fma_rn(6.792806834910504e-11,temperature.y,-4.379102871270937e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.176806489860936e-04), 
        __fma_rn(val.y,temperature.y,9.176806489860936e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.08860770232455792), 
        __fma_rn(val.y,temperature.y,-0.08860770232455792)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.026873655050304e-11,temperature.x,-2.937971345708288e-07), 
        __fma_rn(5.026873655050304e-11,temperature.y,-2.937971345708288e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.287496821355728e-04), 
        __fma_rn(val.y,temperature.y,5.287496821355728e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2348212434111704), 
        __fma_rn(val.y,temperature.y,0.2348212434111704)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species HOCH2OCO
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+25*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+25*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.792806834910504e-11,temperature.x,-4.379102871270937e-07), 
        __fma_rn(6.792806834910504e-11,temperature.y,-4.379102871270937e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.176806489860936e-04), 
        __fma_rn(val.y,temperature.y,9.176806489860936e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.08860770232455792), 
        __fma_rn(val.y,temperature.y,-0.08860770232455792)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.026873655050304e-11,temperature.x,-2.937971345708288e-07), 
        __fma_rn(5.026873655050304e-11,temperature.y,-2.937971345708288e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.287496821355728e-04), 
        __fma_rn(val.y,temperature.y,5.287496821355728e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2348212434111704), 
        __fma_rn(val.y,temperature.y,0.2348212434111704)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species CH3OCH2O2
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+26*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+26*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.797583052545046e-11,temperature.x,-4.382181944305923e-07), 
        __fma_rn(6.797583052545046e-11,temperature.y,-4.382181944305923e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.18325896614223e-04), 
        __fma_rn(val.y,temperature.y,9.18325896614223e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.08867000494565155), 
        __fma_rn(val.y,temperature.y,-0.08867000494565155)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.03394895217157e-11,temperature.x,-2.942106524276735e-07), 
        __fma_rn(5.03394895217157e-11,temperature.y,-2.942106524276735e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.294938944155305e-04), 
        __fma_rn(val.y,temperature.y,5.294938944155305e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2351517530244069), 
        __fma_rn(val.y,temperature.y,0.2351517530244069)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species HO2CH2OCHO
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+27*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+27*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.826609941725254e-11,temperature.x,-4.400894640963193e-07), 
        __fma_rn(6.826609941725254e-11,temperature.y,-4.400894640963193e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.222473115975025e-04), 
        __fma_rn(val.y,temperature.y,9.222473115975025e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.08904864164450052), 
        __fma_rn(val.y,temperature.y,-0.08904864164450052)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.07705125768645e-11,temperature.x,-2.967297795676495e-07), 
        __fma_rn(5.07705125768645e-11,temperature.y,-2.967297795676495e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.340275930728274e-04), 
        __fma_rn(val.y,temperature.y,5.340275930728274e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2371651986905267), 
        __fma_rn(val.y,temperature.y,0.2371651986905267)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species O2CH2OCH2O2H
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+28*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+28*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(6.849966660104515e-11,temperature.x,-4.415951961891624e-07), 
        __fma_rn(6.849966660104515e-11,temperature.y,-4.415951961891624e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.254027095061118e-04), 
        __fma_rn(val.y,temperature.y,9.254027095061118e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,-0.08935331469052193), 
        __fma_rn(val.y,temperature.y,-0.08935331469052193)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(5.111862875856663e-11,temperature.x,-2.987643550055875e-07), 
        __fma_rn(5.111862875856663e-11,temperature.y,-2.987643550055875e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.376892391187019e-04), 
        __fma_rn(val.y,temperature.y,5.376892391187019e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2387913600036666), 
        __fma_rn(val.y,temperature.y,0.2387913600036666)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  // Interactions for species N2
  {
    double2 mole_frac;
    #if __CUDA_ARCH__ >= 350
    asm volatile("ld.global.nc.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+29*spec_stride) : "memory"); 
    #else
    asm volatile("ld.global.cg.v2.f64 {%0,%1}, [%2];" : "=d"(mole_frac.x), 
      "=d"(mole_frac.y) : "l"(mole_frac_array+29*spec_stride) : "memory"); 
    #endif
    {
      double2 val = 
        make_double2(__fma_rn(5.228059863164392e-11,temperature.x,-3.085192390840869e-07), 
        __fma_rn(5.228059863164392e-11,temperature.y,-3.085192390840869e-07)); 
      val = make_double2( __fma_rn(val.x,temperature.x,5.627440892870838e-04), 
        __fma_rn(val.y,temperature.y,5.627440892870838e-04)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.2015216426512595), 
        __fma_rn(val.y,temperature.y,0.2015216426512595)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[0] = make_double2( 
        __fma_rn(mole_frac_light_0.x,val.x,thermal[0].x), 
        __fma_rn(mole_frac_light_0.y,val.y,thermal[0].y)); 
    }
    {
      double2 val = 
        make_double2(__fma_rn(1.214379926045129e-11,temperature.x,-5.945096155210955e-08), 
        __fma_rn(1.214379926045129e-11,temperature.y,-5.945096155210955e-08)); 
      val = make_double2( __fma_rn(val.x,temperature.x,9.205368004195816e-05), 
        __fma_rn(val.y,temperature.y,9.205368004195816e-05)); 
      val = make_double2( __fma_rn(val.x,temperature.x,0.4313312690639463), 
        __fma_rn(val.y,temperature.y,0.4313312690639463)); 
      val = make_double2(val.x*mole_frac.x,val.y*mole_frac.y);
      thermal[1] = make_double2( 
        __fma_rn(mole_frac_light_1.x,val.x,thermal[1].x), 
        __fma_rn(mole_frac_light_1.y,val.y,thermal[1].y)); 
    }
  }
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+0*spec_stride), "d"(thermal[0].x), "d"(thermal[0].y) : 
    "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+1*spec_stride), "d"(thermal[1].x), "d"(thermal[1].y) : 
    "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+2*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+3*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+4*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+5*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+6*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+7*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+8*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+9*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+10*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+11*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+12*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+13*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+14*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+15*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+16*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+17*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+18*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+19*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+20*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+21*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+22*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+23*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+24*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+25*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+26*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+27*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+28*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
  asm volatile("st.global.cs.v2.f64 [%0], {%1,%2};" : : 
    "l"(thermal_out+29*spec_stride), "d"(0.0), "d"(0.0) : "memory"); 
}


