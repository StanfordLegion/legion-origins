#!/usr/bin/python

import subprocess
import tempfile
import os
from operator import attrgetter
import sys
import getopt
import re

class TexFile(object):
    def __init__(self, filename, landscape = False, paperwidth=8.5, paperheight=11):
        self.num_figs = 0
        self.filename = filename
        self.f = open(filename, "w")
        self.f.write(r'''\documentclass%s{article}
\usepackage[paperwidth=%fin,paperheight=%fin,margin=0.5in]{geometry}
\usepackage[x11names, rgb]{xcolor}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usetikzlibrary{snakes,arrows,shapes}
\usepackage{amsmath}
\begin{document}
''' % (("[landscape]" if landscape else ""), paperwidth, paperheight))

    def add_figure(self, fig, keep_dot = False, newpage = False, args = dict()):
        if newpage:
            self.f.write("\\newpage\n")

        self.num_figs = self.num_figs + 1
        if keep_dot:
            tfn = "fig_%d.dot" % (self.num_figs,)
            tf = open(tfn, "w")
        else:
            tf = tempfile.NamedTemporaryFile(delete = False)
            tfn = tf.name

        fig.generate_dot(tf, **args)
        tf.close()

        fig_tex = subprocess.check_output(["/usr/bin/dot2tex", "--debug", "--autosize", "--figonly", tfn])
        fig_tex = re.sub(r' \[diagonals\]', '', fig_tex)
        #fig_tex = subprocess.check_output(["/usr/bin/dot2tex", "--figonly", tfn])
        self.f.write("\\resizebox{\\textwidth}{!}{\n")
        self.f.write(fig_tex)
        self.f.write("}\n")

        if not keep_dot:
            os.unlink(tfn)

    def finish(self, gen_pdf = False):
        self.f.write("\\end{document}\n")
        self.f.close()

        if gen_pdf:
            subprocess.call(["pdflatex", self.filename])

def flatten(*args):
    x = []
    for y in args:
        # first try - does it look like a hash?  if so, grab the values
        try:
            for z in y.itervalues():
                x.extend(flatten(z))
            continue
        except:
            pass

        # second try - can we iterate over it?
        try:
            for z in y:
                x.extend(flatten(z))
            continue
        except:
            pass
        
        # fallback - put whatever it is directly on the list
        x.append(y)
    return x

class Data(object):
    next_id = 1
    default_nonarray_shape = "diamond"
    default_array_shape = "Mdiamond"
    default_nonconst_style = ""
    default_const_style = "bold"

    def __init__(self, label, shape=None, style=None, const=False, array=False):
        self.id = "d%d" % (Data.next_id,)
        Data.next_id = Data.next_id + 1
        self.label = label
        self.const = const
        self.array = array
        self.shape = shape or (Data.default_array_shape if array else Data.default_nonarray_shape)
        self.style = style or (Data.default_const_style if const else Data.default_nonconst_style)
        self.read_by = set()
        self.written_by = set()

    def generate_dot(self, f):
        f.write('  %s [shape="%s", texlbl="%s", style="%s", label="", width=0.3, height=0.2];\n' % 
                (self.id, self.shape, self.label, self.style))

class Cluster(object):
    next_id = 1
    default_style = "cluster"

    @classmethod
    def set_default_style(self, newstyle):
        self.default_style = newstyle

    def __init__(self, label, *args, **kwargs):
        self.id = "c%d" % (Cluster.next_id,)
        Cluster.next_id = Cluster.next_id + 1
        self.label = label
        self.style = kwargs.get("style", Cluster.default_style)
        self.tasks = []
        self.add_tasks(args)

    def add_tasks(self, *tasks):
        for t in flatten(tasks):
            t.cluster = self
            self.tasks.append(t)

    def generate_dot(self, f, vis_datas, show_bbox = None):
        # go through the list of data outputs and mark which are internal vs. ext
        internal_outputs = set()
        external_outputs = set()
        for t in self.tasks:
            #print self.label, t
            for o in t.outputs:
                #print "check", o.label
                #print "read by:", (", ".join(tt.label for tt in o.read_by))
                #print "write by:", (", ".join(tt.label for tt in o.written_by))
                if (o.read_by and
                    all((tt.cluster == self) for tt in o.read_by) and
                    all((tt.cluster == self) for tt in o.written_by)):
                    internal_outputs.add(o)
                else:
                    external_outputs.add(o)
        #print "cluster", self.label
        #print "internal:", (", ".join(o.label for o in internal_outputs))
        #print "external:", (", ".join(o.label for o in external_outputs))

        if (self.style == "cluster") or (show_bbox and self in show_bbox):
            f.write("  subgraph cluster_%s {\n" % (self.id,))
            f.write("    texlbl = \"%s\"\n" % (self.label,))
            for t in self.tasks:
                t.generate_dot(f, internal_outputs)
            f.write("  }\n")

            # data arcs have to be outside subgraph
            for t in self.tasks:
                for d in t.inputs:
                    if (d in internal_outputs) or (d in external_outputs):
                        continue
                    f.write('  %s -> %s;\n' % (d.id, t.id))
                for d in t.outputs:
                    if (d in internal_outputs):
                        continue
                    f.write('  %s -> %s;\n' % (t.id, d.id))
            return

        if self.style == "bbox":
            f.write('  %s [shape="%s", texlbl="%s"];\n' % 
                    (self.id, 
                     "rect",
                     (("\\begin{tabular}{l}\\textbf{%s:}" % (self.label,)) +
                      "".join("\\\\%s" % t.label for t in self.tasks) +
                      "\\end{tabular}")))
            done = set()
            for t in self.tasks:
                for i in t.inputs:
                    if (i in internal_outputs) or (i in external_outputs) or (i in done):
                        continue
                    f.write('  %s -> %s;\n' % (i.id, self.id))
                    done.add(i)
                for o in t.outputs:
                    if (o in internal_outputs) or (o in done):
                        continue
                    f.write('  %s -> %s;\n' % (self.id, o.id))
                    done.add(o)
            return

        assert False

class Task(object):
    next_id = 1
    default_nonarray_style = ""
    default_array_style = "diagonals"

    def __init__(self, label, shape="rect", array=False, style=None, inputs=None, outputs=None):
        self.id = "n%d" % (Task.next_id,)
        Task.next_id = Task.next_id + 1
        self.label = label
        self.shape = shape
        self.array = array
        self.style = style or (Task.default_array_style if array else Task.default_nonarray_style)
        self.cluster = None
        self.inputs = set()
        self.outputs = set()
        self.add_inputs(inputs)
        self.add_outputs(outputs)
        self.prgm_preds = []
        self.prgm_succs = []

    def add_inputs(self, *inputs):
        for i in flatten(inputs):
            self.inputs.add(i)
            i.read_by.add(self)

    def add_outputs(self, *outputs):
        for o in flatten(outputs):
            self.outputs.add(o)
            o.written_by.add(self)

    def generate_dot(self, f, vis_datas):
        f.write('  %s [shape="%s", texlbl="%s", style="%s"];\n' % (self.id, self.shape, self.label, self.style))
        for d in self.inputs:
            if (vis_datas is not None) and (d not in vis_datas):
                continue
            f.write('  %s -> %s;\n' % (d.id, self.id))
        for d in self.outputs:
            if (vis_datas is not None) and (d not in vis_datas):
                continue
            f.write('  %s -> %s;\n' % (self.id, d.id))

class Graph(object):
    def __init__(self):
        self.tasks = []
        self.clusters = []
        self.datas = set()

    def add_clusters(self, *clusters):
        for c in clusters:
            self.clusters.append(c)

    def add_tasks(self, *tasks):
        for t in flatten(tasks):
            self.tasks.append(t)
            for d in t.inputs:
                self.datas.add(d)
            for d in t.outputs:
                self.datas.add(d)

    def write_dot_file(self, filename):
        f = open(filename, "w")
        self.generate_dot(f)

    def generate_dot(self, f, show_bbox = None):
        f.write("digraph G {\n");
        #f.write("  size=\"6,8.5\";\n")
        #f.write("  page = \"5,22\";\n")
        #f.write("  ratio = 2;\n")
        #f.write("  d2tfigpreamble = \"\\tikzstyle{every node}=[align=center]\";\n");

        # figure out which datas to emit
        data_vis = dict()
        if show_bbox:
            for c in show_bbox:
                print c
                for t in c.tasks:
                    print t
                    for i in t.inputs:
                        print i
                        data_vis[i] = data_vis.get(i,[]) + [c]
                    for o in t.outputs:
                        data_vis[o] = data_vis.get(o,[]) + [c]
        else:
            for d in self.datas:
                d_readers = set((t.cluster if t.cluster else t) for t in d.read_by)
                d_writers = set((t.cluster if t.cluster else t) for t in d.written_by)
                if ((len(d_readers) <> 1) or (len(d_writers) == 0) or
                    (tuple(d_readers)[0] not in d_writers) or
                    (type(tuple(d_readers)[0]) is not Cluster) or
                    (tuple(d_readers)[0].style <> "bbox")):
                    data_vis[d] = 1

        for d in data_vis.iterkeys():
            d.generate_dot(f)

        if show_bbox:
            for c in show_bbox:
                c.generate_dot(f, None, show_bbox = show_bbox)
        else:
            for c in self.clusters:
                c.generate_dot(f, None)

            for t in self.tasks:
                if t.cluster is not None:
                    continue
                t.generate_dot(f, None)
            #f.write('  %s [shape="%s", texlbl="%s"];\n' % (t.id, t.shape, t.label))
            #for d in t.inputs:
            #    f.write('  %s -> %s;\n' % (d.id, t.id))
            #for d in t.outputs:
            #    f.write('  %s -> %s;\n' % (t.id, d.id))

        f.write("}\n");
        f.close()

    def generate_all(self, texfile):
        texfile.add_figure(self)
        for c in sorted(self.clusters, key=attrgetter("label")):
            if c.style == "bbox":
                texfile.add_figure(self, args = dict(show_bbox = (c,)), newpage = True)

def deriv1d(axis, inputs, outputs, negate = False, show_comm = True, precalc = None, graph = None, cluster = None, array = False):
    x = type("", (object,), dict(datas = dict(), tasks = dict()))()

    if precalc:
        x.datas["pc_out"] = Data("", array = array)
        x.tasks["pc_task"] = Task(precalc, inputs = inputs, outputs = [ x.datas["pc_out"] ], array = array)
        inputs = [ x.datas["pc_out"] ]

    if show_comm:
        x.datas["comm_out"] = Data("", array = array)
        x.tasks["comm_task"] = Task("$\\leftarrow %s \\rightarrow$" % (axis.upper(),),
                                    shape = "oval",
                                    inputs = inputs, outputs = [ x.datas["comm_out"] ], array = array)
        inputs = inputs + [ x.datas["comm_out"] ]

    x.tasks["d_task"] = Task(("$%s\\frac{\\partial}{\\partial %s}$" % (("-" if negate else ""), axis)),
                             inputs = inputs,
                             outputs = outputs, array = array)

    if graph:
        graph.add_tasks(x.tasks)

    if cluster:
        x["cluster"] = Cluster(cluster, x.tasks)
        if graph:
            graph.add_clusters(x["cluster"])

    return x
    
def gradient(inputs, outputs, negate = False, show_comm = True, precalc = None, graph = None, cluster = None, array = False):
    x = type("", (object,), dict(datas = dict(), tasks = dict(), cluster = None))()

    if precalc:
        x.datas["pc_out"] = Data("", array = array)
        x.tasks["pc_task"] = Task(precalc, inputs = inputs, outputs = [ x.datas["pc_out"] ], array = array)
        inputs = [ x.datas["pc_out"] ]

    for axis in 'xyz':
        dx = deriv1d(axis, inputs, outputs, negate = negate, graph = graph, array = array)
        for l in dx.datas:
            x.datas[axis + "_" + l] = dx.datas[l]
        for l in dx.tasks:
            x.tasks[axis + "_" + l] = dx.tasks[l]

    if cluster:
        x.cluster = Cluster(cluster, x.tasks)
        if graph:
            graph.add_clusters(x.cluster)

    return x

def divergence(inputs, outputs, negate = False, show_comm = True, precalc = None, graph = None, cluster = None, array = False):
    x = type("", (object,), dict(datas = dict(), tasks = dict(), cluster = None))()

    if precalc:
        x.datas["pc_out"] = Data("", array = array)
        x.tasks["pc_task"] = Task(precalc, inputs = inputs, outputs = [ x.datas["pc_out"] ], array = array)
        inputs = [ x.datas["pc_out"] ]

    for axis in 'xyz':
        x.datas[axis + "_out"] = Data("", array = array)
        dx = deriv1d(axis, inputs, [ x.datas[axis + "_out"] ], negate = negate, graph = graph, array = array)
        for l in dx.datas:
            x.datas[axis + "_" + l] = dx.datas[l]
        for l in dx.tasks:
            x.tasks[axis + "_" + l] = dx.tasks[l]

    x.tasks["sum"] = Task("+", inputs = [ x.datas[axis + "_out"] for axis in 'xyz' ], outputs = outputs, array = array)
    if graph:
        graph.add_tasks(x.tasks["sum"])

    if cluster:
        x.cluster = Cluster(cluster, x.tasks)
        if graph:
            graph.add_clusters(x.cluster)

    return x

opts, args = getopt.getopt(sys.argv[1:], "f:kh")
opts = dict(opts)

outfile = opts.get("-f", "rhsf.tex")
keepdot = ("-k" in opts)
hierbbox = ("-h" in opts)

# settings
use_compheat = True
baro_switch = True
use_thermdiff = True
nscbc_before = False
getrates_needs_diffusion = True

if hierbbox:
    Cluster.set_default_style("bbox")

g = Graph()

c_molwt = Data("$molwt_i$", const = True, array = True)
c_molwt_c = Data("$molwt_i^{-1}$", const = True, array = True)

q = [ None,
      Data("$\\rho v_x$ (q[1])"),
      Data("$\\rho v_y$ (q[2])"),
      Data("$\\rho v_z$ (q[3])"),
      Data("$\\rho$ (q[4])"),
      Data("$\\rho e_0$ (q[5])"),
      Data("$\\rho Y_i$ (q[6:n])", array = True),
      ]
vol = Data("volume")
gmf1 = Task("$1/\\rho$", inputs=[q[4]], outputs=[vol])
yspec = Data("$yspecies_i$", array = True)
gmf2 = Task("calc yspecies", inputs=[q[4],q[6]], outputs=[yspec])

g.add_tasks(gmf1, gmf2)

vel = Data("$v$")
gv1 = Task("$\\rho v / \\rho$", inputs=[q[1:4], vol], outputs=[vel])

g.add_tasks(gv1)

avmolwt = Data("$avmolwt^{-1}$")
mixmw = Data("mixMW")

cma = Task("$\sum \\frac{yspecies_i}{molwt_i}$", inputs=[c_molwt_c, yspec], outputs=[avmolwt, mixmw])
g.add_tasks(cma)

c_cpcoeff = Data("\\textit{cpcoeff}(BIG)", const = True)
c_enthcoeff = Data("\\textit{enthcoeff}(BIG)", const = True)
inteng = Data("$e_i$ (internal energy)")
temp = Data("$T$")

ct1 = Task("$\\rho e_0 / \\rho - \\frac{1}{2}v \cdot v$", inputs=[q[5], vol, vel], outputs=[inteng])
ct2 = Task("solve $e_i = enth(T) + RT$ for T", inputs=[inteng, c_cpcoeff, c_enthcoeff], outputs=[temp])

g.add_tasks(ct1, ct2)

if use_compheat or True: # needed for boundary conditions too
    mixcp = Data("$mixCP$")
    ct2.add_outputs(mixcp)

    gamma = Data("$\\gamma$")
    cg = Task("$\\frac{mixCP}{mixCP - R*avmolwt}$", inputs=[mixcp,avmolwt], outputs=[gamma])
    g.add_tasks(cg)

press = Data("$P$")
cp = Task("$\\rho * R * T * avmolwt$", inputs=[q[4],temp,avmolwt], outputs=[press])
g.add_tasks(cp)

h_spec = Data("$enth_i$", array = True)
cspenth = Task("$\\mathit{enthcoeff}_a * T + \\mathit{enthcoeff}_b$", inputs=[c_enthcoeff, temp], outputs=[h_spec], array = True)
g.add_tasks(cspenth)

grad_u = Data("\\textit{grad\\_u}")
#cgu = Task("$\\nabla v$", inputs=[vel], outputs=[grad_u])
#g.add_tasks(cgu)
cgu = gradient(cluster = "$\\nabla v$", inputs=[vel], outputs=[grad_u], graph = g)

grad_t = Data("\\textit{grad\\_T}")
#cgt = Task("$\\nabla T$", inputs=[temp], outputs=[grad_t])
#g.add_tasks(cgt)
cgt = gradient(cluster = "$\\nabla T$", inputs=[temp], outputs=[grad_t], graph = g)

grad_y = Data("\\textit{grad\\_}$y_i$", array = True)
#cgy = Task("$\\nabla yspecies_i$", inputs=[yspec], outputs=[grad_y], array = True)
#g.add_tasks(cgy)
cgy = gradient(cluster = "$\\nabla yspecies_i$", inputs=[yspec], outputs=[grad_y], graph = g, array = True)

xspec = Data("$X_i$", array = True)
gdft1 = Task("$mixMW * \\frac{yspecies_i}{molwt_i}$", inputs=[mixmw,yspec,c_molwt_c], outputs=[xspec])

lamb = Data("$\\lambda$")
gdft2 = Task("MCACON($T$, $X_i$)", inputs=[temp, xspec], outputs=[lamb])

vis = Data("$vis$")
gdft3 = Task("MCAVIS($T$, $X_i$)", inputs=[temp, xspec], outputs=[vis])

ds_spec = Data("$Ds_i$", array = True)
gdft4 = Task("MCADIF($P$, $T$, $X_i$) $* \\rho$", inputs=[press, temp, xspec, q[4]], outputs=[ds_spec])

g.add_tasks(gdft1, gdft2, gdft3, gdft4)

tau = Data("$\\tau$")
cst = Task("computeStressTensor", inputs=[vis, grad_u], outputs=[tau])
g.add_tasks(cst)

grad_mixmw = Data("\\textit{grad\\_mixMW}")
#csdf1 = Task("$\\frac{\\nabla mixMW}{mixMW}$", inputs=[mixmw], outputs=[grad_mixmw])
#g.add_tasks(csdf1)
csdf1 = gradient(cluster = "$\\nabla mixMW$", inputs=[mixmw], outputs=[grad_mixmw], graph = g)

diffflux = Data("\\textit{diff\\_flux}$_i$", array = True)
csdf3 = Task("calculate diff\\_flux", inputs=[ds_spec, grad_y, yspec, grad_mixmw], outputs=[diffflux], array = True)

if baro_switch:
    grad_p = Data("\\textit{grad\\_P}")
    #csdf2 = Task("$\\frac{\\nabla P}{P}$", inputs=[press], outputs=[grad_p])
    #g.add_tasks(csdf2)
    csdf2 = gradient(cluster = "$\\nabla P$", inputs=[press], outputs=[grad_p], graph = g)
    csdf3.add_inputs(grad_p)
    
if use_thermdiff:
    rs_spec = Data("$Rs_i$", array = True)
    gdft5 = Task("MCATDR($T$, $X_i$)", inputs=[temp, xspec], outputs=[rs_spec])
    g.add_tasks(gdft5)
    csdf3.add_inputs(rs_spec)

g.add_tasks(csdf3)

diffflux_n = Data("\\textit{diff\\_flux}$_N$")
csdf4 = Task("$-\\sum_{1}^{N-1} \\mathit{diff\\_flux}_i$", inputs=[diffflux], outputs=[diffflux_n])
g.add_tasks(csdf4)

heatflux = Data("\\textit{heat\\_flux}")
csdf5 = Task("$-\\lambda * grad\\_T + \\sum enth_i * \\mathit{diff\\_flux}_i$", inputs=[lamb, grad_t, h_spec, diffflux, diffflux_n], outputs=[heatflux])
g.add_tasks(csdf5)

tau_bc = Data("$\\tau'$")
diffflux_bc = Data("\\textit{diff\\_flux}$_i'$", array = True)
heatflux_bc = Data("$\\mathit{heat\\_flux}'$")
bcf = Task("bc\\_flux, wallBCflux", inputs=[tau,diffflux,diffflux_n,heatflux], outputs=[tau_bc, diffflux_bc, heatflux_bc])
g.add_tasks(bcf)

def rhs_vel(g, comp1, tau_bc, q, vel, press, out, cluster=None):
    pieces = dict()
    subtasks = []
    for comp2 in ("x", "y", "z"):
        tmp = Data("")
        dx = deriv1d(comp2, precalc = ("$\\tau_{%s%s} - (\\rho v_%s)*v_%s%s$" %
                                       (comp1, comp2, comp1, comp2, (" - P" if comp1==comp2 else ""))),
                   inputs=[tau_bc, q, vel] + ([press] if comp1 == comp2 else []), outputs=[tmp], graph = g)
        pieces[comp2] = tmp
        subtasks.append(dx.tasks)
        
    tsum = Task("+", inputs=pieces.itervalues(), outputs=[out])
    g.add_tasks(tsum)
    if cluster:
        g.add_clusters(Cluster(cluster, subtasks, tsum))

#rhs1y = Data("")
#tr1y = Task("$\\frac{\\partial}{\\partial x}[\\tau_{xy} - (\\rho v_x)*v_y]$", inputs=[tau_bc, q[1], vel], outputs=[rhs1y])
#g.add_tasks(tr1y)

#rhs1z = Data("")
#tr1z = Task("$\\frac{\\partial}{\\partial x}[\\tau_{xz} - (\\rho v_x)*v_z]$", inputs=[tau_bc, q[1], vel], outputs=[rhs1z])
#g.add_tasks(tr1z)

div_diff = Data("$\\mathit{div\\_diff}_i$", array = True)
tdd = divergence(cluster = "$\\nabla \\cdot \\mathit{diff\\_flux}_i$", inputs=[diffflux], outputs=[div_diff], graph=g, array=True)

rr_i = Data("$rr_i$", array = True)
if getrates_needs_diffusion:
    trr = Task("reaction\\_rate($P$, $T$, $yspecies_i$, $diff_i$)", inputs=[press, temp, yspec, div_diff], outputs=[rr_i])
else:
    trr = Task("reaction\\_rate($P$, $T$, $yspecies_i$)", inputs=[press, temp, yspec], outputs=[rr_i])
g.add_tasks(trr)

# needed for boundary condition stuff
div_heat = Data("div\\_heat")
tdh = divergence(cluster = "$\\nabla \\cdot \\mathit{heat\\_flux}$", inputs=[heatflux], outputs=[div_heat], graph=g)

div_tau = Data("$div\\_\\tau$")
tdt = divergence(cluster = "$\\nabla \\cdot \\tau$", inputs=[tau], outputs=[div_tau], graph=g)

if nscbc_before:
    rhs = [ None,
            Data("rhs[1]"),
            Data("rhs[2]"),
            Data("rhs[3]"),
            Data("rhs[4]"),
            Data("rhs[5]"),
            Data("rhs[6:N]", array = True),
            ]
    rhs_bc = [ None,
               Data("rhs\\_bc[1]"),
               Data("rhs\\_bc[2]"),
               Data("rhs\\_bc[3]"),
               Data("rhs\\_bc[4]"),
               Data("rhs\\_bc[5]"),
               Data("rhs\\_bc[6:N]", array = True),
               ]
else:
    rhs = [ None,
            Data("rhs\\_pre[1]"),
            Data("rhs\\_pre[2]"),
            Data("rhs\\_pre[3]"),
            Data("rhs\\_pre[4]"),
            Data("rhs\\_pre[5]"),
            Data("rhs\\_pre[6:N]", array = True),
            ]
    rhs_postbc = [ None,
                   Data("rhs[1]"),
                   Data("rhs[2]"),
                   Data("rhs[3]"),
                   Data("rhs[4]"),
                   Data("rhs[5]"),
                   Data("rhs[6:N]", array = True),
                   ]
    tns = Task("nscbc(...)", 
               inputs = [ q[1:6], h_spec, rr_i, grad_y, grad_u, tau, div_heat, div_tau, div_diff, 
                          gamma, mixcp, avmolwt, mixmw, yspec, vel, vol, press, temp, rhs[1:7] ],
               outputs = [ rhs_postbc[1:7] ])
    g.add_tasks(tns)

#tr1 = Task("+", inputs=[rhs1x, rhs1y, rhs1z], outputs=[rhs1])
#g.add_tasks(tr1)
rhs_vel(g, "x", tau_bc, q[1], vel, press, rhs[1], cluster="rhs1")

rhs_vel(g, "y", tau_bc, q[2], vel, press, rhs[2], cluster="rhs2")

rhs_vel(g, "z", tau_bc, q[3], vel, press, rhs[3], cluster="rhs3")

if False:
    rhs4x = Data("")
    tr4x = Task("$-\\frac{\\partial}{\\partial x} \\rho v_x$", inputs=[q[1]], outputs=[rhs4x])
    rhs4y = Data("")
    tr4y = Task("$-\\frac{\\partial}{\\partial y} \\rho v_y$", inputs=[q[2]], outputs=[rhs4y])
    rhs4z = Data("")
    tr4z = Task("$-\\frac{\\partial}{\\partial z} \\rho v_z$", inputs=[q[3]], outputs=[rhs4z])
    tr4 = Task("+", inputs=[rhs4x, rhs4y, rhs4z], outputs=[rhs[4]])
    g.add_tasks(tr4x, tr4y, tr4z, tr4)
else:
    rhs4x = Data("")
    rhs4y = Data("")
    rhs4z = Data("")
    d4x = deriv1d("x", inputs = [q[1]], outputs = [rhs4x], negate = True, graph = g)
    d4y = deriv1d("y", inputs = [q[2]], outputs = [rhs4y], negate = True, graph = g)
    d4z = deriv1d("z", inputs = [q[3]], outputs = [rhs4z], negate = True, graph = g)
    tr4 = Task("+", inputs=[rhs4x, rhs4y, rhs4z], outputs=[rhs[4]])
    g.add_tasks(tr4)

# energy is nasty
rhs5x = Data("")
rhs5y = Data("")
rhs5z = Data("")
d5x = deriv1d('x', negate = True, precalc = "$v_x*(\\rho e_0 + P) - (v \\cdot \\tau_x) - \\mathit{heat\\_flux}_{i_x}'$",
              inputs=[vel, q[5], press, tau_bc, heatflux_bc], outputs=[rhs5x], graph = g)
d5y = deriv1d('y', negate = True, precalc = "$v_y*(\\rho e_0 + P) - (v \\cdot \\tau_y) - \\mathit{heat\\_flux}_{i_y}'$",
              inputs=[vel, q[5], press, tau_bc, heatflux_bc], outputs=[rhs5y], graph = g)
d5z = deriv1d('z', negate = True, precalc = "$v_z*(\\rho e_0 + P) - (v \\cdot \\tau_z) - \\mathit{heat\\_flux}_{i_z}'$",
              inputs=[vel, q[5], press, tau_bc, heatflux_bc], outputs=[rhs5z], graph = g)
tr5 = Task("+", inputs=[rhs5x, rhs5y, rhs5z], outputs=[rhs[5]])
g.add_tasks(tr5)

rhs6x = Data("")
rhs6y = Data("")
rhs6z = Data("")
d6x = deriv1d('x', negate = True, precalc = "$(\\rho Y_i)*v_x - \\mathit{diff\\_flux}_{i_x}'$", inputs=[q[6], vel, diffflux_bc], outputs=[rhs6x], graph = g, array = True)
d6y = deriv1d('y', negate = True, precalc = "$(\\rho Y_i)*v_y - \\mathit{diff\\_flux}_{i_y}'$", inputs=[q[6], vel, diffflux_bc], outputs=[rhs6y], graph = g, array = True)
d6z = deriv1d('z', negate = True, precalc = "$(\\rho Y_i)*v_z - \\mathit{diff\\_flux}_{i_z}'$", inputs=[q[6], vel, diffflux_bc], outputs=[rhs6z], graph = g, array = True)
tr6 = Task("+", inputs=[rhs6x, rhs6y, rhs6z, rr_i], outputs=[rhs[6]], array = True)
g.add_tasks(tr6)

g.add_clusters(Cluster("", gmf1, gmf2, gv1, cma))

c_gdft = Cluster("gdft", gdft1, gdft2, gdft3, gdft4)
if use_thermdiff:
    c_gdft.add_tasks(gdft5)
g.add_clusters(c_gdft)

g.add_clusters(Cluster("calctemp", ct1, ct2))

#g.add_clusters(Cluster("rhs4", tr4x, tr4y, tr4z, tr4))
g.add_clusters(Cluster("rhs4", d4x.tasks, d4y.tasks, d4z.tasks, tr4))
g.add_clusters(Cluster("rhs5", d5x.tasks, d5y.tasks, d5z.tasks, tr5))
#g.add_clusters(Cluster("rhs6", tr6x, tr6y, tr6z, trr, tr6))
g.add_clusters(Cluster("rhs6", d6x.tasks, d6y.tasks, d6z.tasks, tr6))

tt = TexFile(outfile, landscape = True, paperwidth=11, paperheight=8.5)
if hierbbox:
    g.generate_all(tt)
else:
    tt.add_figure(g, keep_dot = keepdot)

#tt.add_figure(g)
#g.write_dot_file("foo.dot")
tt.finish(gen_pdf = True)
