/*
 * PolyGas.cc
 *
 *  Created on: Mar 26, 2012
 *      Author: cferenba
 *
 * Copyright (c) 2012, Los Alamos National Security, LLC.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style open-source
 * license; see top-level LICENSE file for full license text.
 */

#include "PolyGas.hh"

#include "legion.h"

#include "Memory.hh"
#include "InputFile.hh"
#include "Hydro.hh"
#include "Mesh.hh"

using namespace std;
using namespace Memory;
using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;


namespace {  // unnamed
static void __attribute__ ((constructor)) registerTasks() {
    HighLevelRuntime::register_legion_task<PolyGas::calcStateHalfTask>(
            TID_CALCSTATEHALF, Processor::LOC_PROC, true, true,
            AUTO_GENERATE_ID, TaskConfigOptions(), "calc_state_half");
    HighLevelRuntime::register_legion_task<PolyGas::calcForceTask>(
            TID_CALCFORCEPGAS, Processor::LOC_PROC, true, true,
            AUTO_GENERATE_ID, TaskConfigOptions(), "calc_force_pgas");
}
}; // namespace


PolyGas::PolyGas(const InputFile* inp, Hydro* h) : hydro(h) {
    gamma = inp->getDouble("gamma", 5. / 3.);
    ssmin = inp->getDouble("ssmin", 0.);
}


void PolyGas::calcStateHalfTask(
        const Task *task,
        const std::vector<PhysicalRegion> &regions,
        Context ctx,
        HighLevelRuntime *runtime) {
    const double* args = (const double*) task->args;
    const double gamma = args[0];
    const double ssmin = args[1];
    const double dt    = args[2];

    RegionAccessor<AccessorType::Generic, double> acc_zr =
      regions[0].get_field_accessor(FID_ZR).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zvolp =
      regions[0].get_field_accessor(FID_ZVOLP).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zvol0 =
      regions[0].get_field_accessor(FID_ZVOL0).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_ze =
      regions[0].get_field_accessor(FID_ZE).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zwrate =
      regions[0].get_field_accessor(FID_ZWRATE).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zm =
      regions[0].get_field_accessor(FID_ZM).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zp =
      regions[1].get_field_accessor(FID_ZP).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zss =
      regions[1].get_field_accessor(FID_ZSS).typeify<double>();

    const double dth = 0.5 * dt;
    const double gm1 = gamma - 1.;
    const double ssmin2 = max(ssmin * ssmin, 1.e-99);
    const IndexSpace& isz = task->regions[0].region.get_index_space();
    for (Domain::DomainPointIterator itrz(isz); itrz; itrz++)
    {
        // compute EOS at beginning of time step
        ptr_t z = itrz.p.get_index();
        double r = acc_zr.read(z);
        double e = max(acc_ze.read(z), 0.);
        double p = gm1 * r * e;
        double pre = gm1 * e;
        double per = gm1 * r;
        double csqd = max(ssmin2, pre + per * p / (r * r));
        double ss = sqrt(csqd);

        // now advance pressure to the half-step
        double minv = 1. / acc_zm.read(z);
        double volp = acc_zvolp.read(z);
        double vol0 = acc_zvol0.read(z);
        double wrate = acc_zwrate.read(z);
        double dv = (volp - vol0) * minv;
        double bulk = r * csqd;
        double denom = 1. + 0.5 * per * dv;
        double src = wrate * dth * minv;
        p += (per * src - r * bulk * dv) / denom;
        acc_zp.write(z, p);
        acc_zss.write(z, ss);
    }
}


void PolyGas::calcForceTask(
        const Task *task,
        const std::vector<PhysicalRegion> &regions,
        Context ctx,
        HighLevelRuntime *runtime) {
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsz =
      regions[0].get_field_accessor(FID_MAPSZ).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, double2> acc_ssurf =
      regions[0].get_field_accessor(FID_SSURFP).typeify<double2>();
    RegionAccessor<AccessorType::Generic, double> acc_zp =
      regions[1].get_field_accessor(FID_ZP).typeify<double>();
    RegionAccessor<AccessorType::Generic, double2> acc_sf =
      regions[2].get_field_accessor(FID_SFP).typeify<double2>();

    const IndexSpace& iss = task->regions[0].region.get_index_space();
    for (Domain::DomainPointIterator itrs(iss); itrs; itrs++)
    {
        ptr_t s  = itrs.p.get_index();
        ptr_t z  = acc_mapsz.read(s);
        double p = acc_zp.read(z);
        double2 surf = acc_ssurf.read(s);
        double2 sfx = -p * surf;
        acc_sf.write(s, sfx);
    }

}

