/*
 * QCS.cc
 *
 *  Created on: Feb 21, 2012
 *      Author: cferenba
 *
 * Copyright (c) 2012, Los Alamos National Security, LLC.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style open-source
 * license; see top-level LICENSE file for full license text.
 */

#include "QCS.hh"

#include <cmath>

#include "legion.h"

#include "Memory.hh"
#include "InputFile.hh"
#include "Vec2.hh"
#include "Mesh.hh"
#include "Hydro.hh"

using namespace std;
using namespace Memory;
using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;


namespace {  // unnamed
static void __attribute__ ((constructor)) registerTasks() {
    HighLevelRuntime::register_legion_task<QCS::setCornerDivTask>(
            TID_SETCORNERDIV, Processor::LOC_PROC, true, true,
            AUTO_GENERATE_ID, TaskConfigOptions(), "set_corner_div");
    HighLevelRuntime::register_legion_task<QCS::setQCnForceTask>(
            TID_SETQCNFORCE, Processor::LOC_PROC, true, true,
            AUTO_GENERATE_ID, TaskConfigOptions(), "set_qcn_force");
    HighLevelRuntime::register_legion_task<QCS::setForceTask>(
            TID_SETFORCEQCS, Processor::LOC_PROC, true, true,
            AUTO_GENERATE_ID, TaskConfigOptions(), "set_force_qcs");
    HighLevelRuntime::register_legion_task<QCS::setVelDiffTask>(
            TID_SETVELDIFF, Processor::LOC_PROC, true, true,
            AUTO_GENERATE_ID, TaskConfigOptions(), "set_vel_diff");
}
}; // namespace


QCS::QCS(const InputFile* inp, Hydro* h) : hydro(h) {
    qgamma = inp->getDouble("qgamma", 5. / 3.);
    q1 = inp->getDouble("q1", 0.);
    q2 = inp->getDouble("q2", 2.);

    FieldSpace fsz = hydro->mesh->lrz.get_field_space();
    FieldAllocator faz = hydro->runtime->create_field_allocator(
            hydro->ctx, fsz);
    faz.allocate_field(sizeof(double2), FID_ZUC);
    faz.allocate_field(sizeof(double), FID_ZTMP);

    FieldSpace fss = hydro->mesh->lrs.get_field_space();
    FieldAllocator fas = hydro->runtime->create_field_allocator(
            hydro->ctx, fss);
    fas.allocate_field(sizeof(double), FID_CAREA);
    fas.allocate_field(sizeof(double), FID_CEVOL);
    fas.allocate_field(sizeof(double), FID_CDU);
    fas.allocate_field(sizeof(double), FID_CDIV);
    fas.allocate_field(sizeof(double), FID_CCOS);
    fas.allocate_field(sizeof(double2), FID_CQE1);
    fas.allocate_field(sizeof(double2), FID_CQE2);
    fas.allocate_field(sizeof(double), FID_CRMU);
    fas.allocate_field(sizeof(double), FID_CW);
}

QCS::~QCS() {}


// Routine number [2]  in the full algorithm
//     [2.1] Find the corner divergence
//     [2.2] Compute the cos angle for c
//     [2.3] Find the evolution factor cevol(c)
//           and the Delta u(c) = du(c)
void QCS::setCornerDivTask(
        const Task *task,
        const std::vector<PhysicalRegion> &regions,
        Context ctx,
        HighLevelRuntime *runtime) {
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsz =
      regions[0].get_field_accessor(FID_MAPSZ).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsp1 =
      regions[0].get_field_accessor(FID_MAPSP1).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsp2 =
      regions[0].get_field_accessor(FID_MAPSP2).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapss3 =
      regions[0].get_field_accessor(FID_MAPSS3).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, int> acc_mapsp1reg =
      regions[0].get_field_accessor(FID_MAPSP1REG).typeify<int>();
    RegionAccessor<AccessorType::Generic, int> acc_mapsp2reg =
      regions[0].get_field_accessor(FID_MAPSP2REG).typeify<int>();
    RegionAccessor<AccessorType::Generic, double2> acc_ex =
      regions[0].get_field_accessor(FID_EXP).typeify<double2>();
    RegionAccessor<AccessorType::Generic, double> acc_elen =
      regions[0].get_field_accessor(FID_ELEN).typeify<double>();
    RegionAccessor<AccessorType::Generic, int> acc_znump =
      regions[1].get_field_accessor(FID_ZNUMP).typeify<int>();
    RegionAccessor<AccessorType::Generic, double2> acc_zx =
      regions[1].get_field_accessor(FID_ZXP).typeify<double2>();
    RegionAccessor<AccessorType::Generic, double2> acc_pu[2] = {
      regions[2].get_field_accessor(FID_PU0).typeify<double2>(),
      regions[3].get_field_accessor(FID_PU0).typeify<double2>()
    };
    RegionAccessor<AccessorType::Generic, double2> acc_px[2] = {
      regions[2].get_field_accessor(FID_PXP).typeify<double2>(),
      regions[3].get_field_accessor(FID_PXP).typeify<double2>()
    };
    RegionAccessor<AccessorType::Generic, double2> acc_zuc =
      regions[4].get_field_accessor(FID_ZUC).typeify<double2>();
    RegionAccessor<AccessorType::Generic, double> acc_carea =
      regions[5].get_field_accessor(FID_CAREA).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_ccos =
      regions[5].get_field_accessor(FID_CCOS).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_cdiv =
      regions[5].get_field_accessor(FID_CDIV).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_cevol =
      regions[5].get_field_accessor(FID_CEVOL).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_cdu =
      regions[5].get_field_accessor(FID_CDU).typeify<double>();

    // [1] Compute a zone-centered velocity
    const IndexSpace& isz = task->regions[1].region.get_index_space();
    for (Domain::DomainPointIterator itrz(isz); itrz; itrz++)
    {
        ptr_t z = itrz.p.get_index();
        acc_zuc.write(z, double2(0., 0.));
    }

    const IndexSpace& iss = task->regions[0].region.get_index_space();
    for (Domain::DomainPointIterator itrs(iss); itrs; itrs++)
    {
        ptr_t s = itrs.p.get_index();
        ptr_t p = acc_mapsp1.read(s);
        int preg = acc_mapsp1reg.read(s);
        ptr_t z = acc_mapsz.read(s);
        double2 pu = acc_pu[preg].read(p);
        double2 zuc = acc_zuc.read(z);
        int n = acc_znump.read(z);
        zuc += pu / n;
        acc_zuc.write(z, zuc);
    }

    // [2] Divergence at the corner
    for (Domain::DomainPointIterator itrc(iss); itrc; itrc++)
    {
        ptr_t c = itrc.p.get_index();
        ptr_t s2 = c;
        ptr_t s = acc_mapss3.read(s2);
        // Associated zone, point
        ptr_t z = acc_mapsz.read(s);
        ptr_t p = acc_mapsp2.read(s);
        int preg = acc_mapsp2reg.read(s);
        // Neighboring points
        ptr_t p1 = acc_mapsp1.read(s);
        int p1reg = acc_mapsp1reg.read(s);
        ptr_t p2 = acc_mapsp2.read(s2);
        int p2reg = acc_mapsp2reg.read(s2);

        // Velocities and positions
        // 0 = point p
        double2 up0 = acc_pu[preg].read(p);
        double2 xp0 = acc_px[preg].read(p);
        // 1 = edge e2
        double2 up1 = 0.5 * (up0 + acc_pu[p2reg].read(p2));
        double2 xp1 = acc_ex.read(s2);
        // 2 = zone center z
        double2 up2 = acc_zuc.read(z);
        double2 xp2 = acc_zx.read(z);
        // 3 = edge e1
        double2 up3 = 0.5 * (acc_pu[p1reg].read(p1) + up0);
        double2 xp3 = acc_ex.read(s);

        // compute 2d cartesian volume of corner
        double cvolume = 0.5 * cross(xp2 - xp0, xp3 - xp1);
        acc_carea.write(c, cvolume);

        // compute cosine angle
        double2 v1 = xp3 - xp0;
        double2 v2 = xp1 - xp0;
        double de1 = acc_elen.read(s);
        double de2 = acc_elen.read(s2);
        double minelen = min(de1, de2);
        double ccos = ((minelen < 1.e-12) ?
                0. :
                4. * dot(v1, v2) / (de1 * de2));
        acc_ccos.write(c, ccos);

        // compute divergence of corner
        double cdiv = (cross(up2 - up0, xp3 - xp1) -
                cross(up3 - up1, xp2 - xp0)) /
                (2.0 * cvolume);
        acc_cdiv.write(c, cdiv);

        // compute evolution factor
        double2 dxx1 = 0.5 * (xp1 + xp2 - xp0 - xp3);
        double2 dxx2 = 0.5 * (xp2 + xp3 - xp0 - xp1);
        double dx1 = length(dxx1);
        double dx2 = length(dxx2);

        // average corner-centered velocity
        double2 duav = 0.25 * (up0 + up1 + up2 + up3);

        double test1 = abs(dot(dxx1, duav) * dx2);
        double test2 = abs(dot(dxx2, duav) * dx1);
        double num = (test1 > test2 ? dx1 : dx2);
        double den = (test1 > test2 ? dx2 : dx1);
        double r = num / den;
        double evol = sqrt(4.0 * cvolume * r);
        evol = min(evol, 2.0 * minelen);

        // compute delta velocity
        double dv1 = length2(up1 + up2 - up0 - up3);
        double dv2 = length2(up2 + up3 - up0 - up1);
        double du = sqrt(max(dv1, dv2));

        evol = (cdiv < 0.0 ? evol : 0.);
        du   = (cdiv < 0.0 ? du   : 0.);
        acc_cevol.write(c, evol);
        acc_cdu.write(c, du);
    }
}


// Routine number [4]  in the full algorithm CS2DQforce(...)
void QCS::setQCnForceTask(
        const Task *task,
        const std::vector<PhysicalRegion> &regions,
        Context ctx,
        HighLevelRuntime *runtime) {
    const double* args = (const double*) task->args;
    const double qgamma = args[0];
    const double q1     = args[1];
    const double q2     = args[2];

    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsz =
      regions[0].get_field_accessor(FID_MAPSZ).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsp1 =
      regions[0].get_field_accessor(FID_MAPSP1).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsp2 =
      regions[0].get_field_accessor(FID_MAPSP2).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapss3 =
      regions[0].get_field_accessor(FID_MAPSS3).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, int> acc_mapsp1reg =
      regions[0].get_field_accessor(FID_MAPSP1REG).typeify<int>();
    RegionAccessor<AccessorType::Generic, int> acc_mapsp2reg =
      regions[0].get_field_accessor(FID_MAPSP2REG).typeify<int>();
    RegionAccessor<AccessorType::Generic, double> acc_elen =
      regions[0].get_field_accessor(FID_ELEN).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_cdiv =
      regions[0].get_field_accessor(FID_CDIV).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_cdu =
      regions[0].get_field_accessor(FID_CDU).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_cevol =
      regions[0].get_field_accessor(FID_CEVOL).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zrp =
      regions[1].get_field_accessor(FID_ZRP).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zss =
      regions[1].get_field_accessor(FID_ZSS).typeify<double>();
    RegionAccessor<AccessorType::Generic, double2> acc_pu[2] = {
      regions[2].get_field_accessor(FID_PU0).typeify<double2>(),
      regions[3].get_field_accessor(FID_PU0).typeify<double2>()
    };
    RegionAccessor<AccessorType::Generic, double> acc_crmu =
      regions[4].get_field_accessor(FID_CRMU).typeify<double>();
    RegionAccessor<AccessorType::Generic, double2> acc_cqe1 =
      regions[4].get_field_accessor(FID_CQE1).typeify<double2>();
    RegionAccessor<AccessorType::Generic, double2> acc_cqe2 =
      regions[4].get_field_accessor(FID_CQE2).typeify<double2>();

    const double gammap1 = qgamma + 1.0;

    // [4.1] Compute the crmu (real Kurapatenko viscous scalar)
    const IndexSpace& iss = task->regions[0].region.get_index_space();
    for (Domain::DomainPointIterator itrc(iss); itrc; itrc++)
    {
        ptr_t c = itrc.p.get_index();
        ptr_t z = acc_mapsz.read(c);

        // Kurapatenko form of the viscosity
        double cdu = acc_cdu.read(c);
        double ztmp2 = q2 * 0.25 * gammap1 * cdu;
        double zss = acc_zss.read(z);
        double ztmp1 = q1 * zss;
        double zkur = ztmp2 + sqrt(ztmp2 * ztmp2 + ztmp1 * ztmp1);
        // Compute crmu for each corner
        double zrp = acc_zrp.read(z);
        double cevol = acc_cevol.read(c);
        double crmu = zkur * zrp * cevol;
        double cdiv = acc_cdiv.read(c);
        crmu = ((cdiv > 0.0) ? 0. : crmu);
        acc_crmu.write(c, crmu);
    }

    // [4.2] Compute the cqe for each corner
    for (Domain::DomainPointIterator itrc(iss); itrc; itrc++)
    {
        ptr_t c = itrc.p.get_index();
        ptr_t s2 = c;
        ptr_t s = acc_mapss3.read(s2);
        ptr_t p = acc_mapsp2.read(s);
        int preg = acc_mapsp2reg.read(s);
        // Associated point 1
        ptr_t p1 = acc_mapsp1.read(s);
        int p1reg = acc_mapsp1reg.read(s);
        // Associated point 2
        ptr_t p2 = acc_mapsp2.read(s2);
        int p2reg = acc_mapsp2reg.read(s2);

        // Compute: cqe(1,2,3)=edge 1, y component (2nd), 3rd corner
        //          cqe(2,1,3)=edge 2, x component (1st)
        double crmu = acc_crmu.read(c);
        double2 pu = acc_pu[preg].read(p);
        double2 pu1 = acc_pu[p1reg].read(p1);
        double elen = acc_elen.read(s);
        double2 cqe1 = crmu * (pu - pu1) / elen;
        acc_cqe1.write(c, cqe1);
        double2 pu2 = acc_pu[p2reg].read(p2);
        double elen2 = acc_elen.read(s2);
        double2 cqe2 = crmu * (pu2 - pu) / elen2;
        acc_cqe2.write(c, cqe2);
    }
}


// Routine number [5]  in the full algorithm CS2DQforce(...)
void QCS::setForceTask(
        const Task *task,
        const std::vector<PhysicalRegion> &regions,
        Context ctx,
        HighLevelRuntime *runtime) {
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapss4 =
      regions[0].get_field_accessor(FID_MAPSS4).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, double> acc_carea =
      regions[0].get_field_accessor(FID_CAREA).typeify<double>();
    RegionAccessor<AccessorType::Generic, double2> acc_cqe1 =
      regions[0].get_field_accessor(FID_CQE1).typeify<double2>();
    RegionAccessor<AccessorType::Generic, double2> acc_cqe2 =
      regions[0].get_field_accessor(FID_CQE2).typeify<double2>();
    RegionAccessor<AccessorType::Generic, double> acc_elen =
      regions[0].get_field_accessor(FID_ELEN).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_ccos =
      regions[1].get_field_accessor(FID_CCOS).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_cw =
      regions[2].get_field_accessor(FID_CW).typeify<double>();
    RegionAccessor<AccessorType::Generic, double2> acc_sfq =
      regions[2].get_field_accessor(FID_SFQ).typeify<double2>();

    // [5.1] Preparation of extra variables
    const IndexSpace& iss = task->regions[0].region.get_index_space();
    for (Domain::DomainPointIterator itrc(iss); itrc; itrc++)
    {
        ptr_t c = itrc.p.get_index();
        double ccos = acc_ccos.read(c);
        double csin2 = 1.0 - ccos * ccos;
        double carea = acc_carea.read(c);
        double cw = ((csin2 < 1.e-4) ? 0. : carea / csin2);
        acc_cw.write(c, cw);
        ccos      = ((csin2 < 1.e-4) ? 0. : ccos);
        acc_ccos.write(c, ccos);
    }

    // [5.2] Set-Up the forces on corners
    for (Domain::DomainPointIterator itrs(iss); itrs; itrs++)
    {
        ptr_t s = itrs.p.get_index();
        // Associated corners 1 and 2
        ptr_t c1 = s;
        ptr_t c2 = acc_mapss4.read(s);
        // Edge length for c1, c2 contribution to s
        double el = acc_elen.read(s);

        double cw1 = acc_cw.read(c1);
        double cw2 = acc_cw.read(c2);
        double ccos1 = acc_ccos.read(c1);
        double ccos2 = acc_ccos.read(c2);
        double2 cqe11 = acc_cqe1.read(c1);
        double2 cqe12 = acc_cqe1.read(c2);
        double2 cqe21 = acc_cqe2.read(c1);
        double2 cqe22 = acc_cqe2.read(c2);
        double2 sfq = (cw1 * (cqe21 + ccos1 * cqe11) +
                       cw2 * (cqe12 + ccos2 * cqe22)) / el;
        acc_sfq.write(s, sfq);
    }
}


// Routine number [6] in the full algorithm
void QCS::setVelDiffTask(
        const Task *task,
        const std::vector<PhysicalRegion> &regions,
        Context ctx,
        HighLevelRuntime *runtime) {
    const double* args = (const double*) task->args;
    const double q1 = args[0];
    const double q2 = args[1];

    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsz =
      regions[0].get_field_accessor(FID_MAPSZ).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsp1 =
      regions[0].get_field_accessor(FID_MAPSP1).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsp2 =
      regions[0].get_field_accessor(FID_MAPSP2).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, int> acc_mapsp1reg =
      regions[0].get_field_accessor(FID_MAPSP1REG).typeify<int>();
    RegionAccessor<AccessorType::Generic, int> acc_mapsp2reg =
      regions[0].get_field_accessor(FID_MAPSP2REG).typeify<int>();
    RegionAccessor<AccessorType::Generic, double> acc_elen =
      regions[0].get_field_accessor(FID_ELEN).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zss =
      regions[1].get_field_accessor(FID_ZSS).typeify<double>();
    RegionAccessor<AccessorType::Generic, double2> acc_pu[2] = {
      regions[2].get_field_accessor(FID_PU0).typeify<double2>(),
      regions[3].get_field_accessor(FID_PU0).typeify<double2>()
    };
    RegionAccessor<AccessorType::Generic, double2> acc_px[2] = {
      regions[2].get_field_accessor(FID_PXP).typeify<double2>(),
      regions[3].get_field_accessor(FID_PXP).typeify<double2>()
    };
    RegionAccessor<AccessorType::Generic, double> acc_ztmp =
      regions[4].get_field_accessor(FID_ZTMP).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zdu =
      regions[4].get_field_accessor(FID_ZDU).typeify<double>();

    const IndexSpace& isz = task->regions[4].region.get_index_space();
    for (Domain::DomainPointIterator itrz(isz); itrz; itrz++)
    {
        ptr_t z = itrz.p.get_index();
        acc_ztmp.write(z, 0.);
    }

    const IndexSpace& iss = task->regions[0].region.get_index_space();
    for (Domain::DomainPointIterator itrs(iss); itrs; itrs++)
    {
        ptr_t s = itrs.p.get_index();
        ptr_t p1 = acc_mapsp1.read(s);
        int p1reg = acc_mapsp1reg.read(s);
        ptr_t p2 = acc_mapsp2.read(s);
        int p2reg = acc_mapsp2reg.read(s);
        ptr_t z  = acc_mapsz.read(s);

        double2 px1 = acc_px[p1reg].read(p1);
        double2 px2 = acc_px[p2reg].read(p2);
        double2 pu1 = acc_pu[p1reg].read(p1);
        double2 pu2 = acc_pu[p2reg].read(p2);
        double2 dx  = px2 - px1;
        double2 du  = pu2 - pu1;
        double lenx = acc_elen.read(s);
        double dux = dot(du, dx);
        dux = (lenx > 0. ? abs(dux) / lenx : 0.);

        double ztmp  = acc_ztmp.read(z);
        ztmp = max(ztmp, dux);
        acc_ztmp.write(z, ztmp);
    }

    for (Domain::DomainPointIterator itrz(isz); itrz; itrz++)
    {
        ptr_t z = itrz.p.get_index();
        double zss  = acc_zss.read(z);
        double ztmp  = acc_ztmp.read(z);
        double zdu = q1 * zss + 2. * q2 * ztmp;
        acc_zdu.write(z, zdu);
    }
}


#if 0
void QCS::calcForce(
        double2* sf,
        const int sfirst,
        const int slast) {
    int cfirst = sfirst;
    int clast = slast;

    // [1] Find the right, left, top, bottom  edges to use for the
    //     limiters
    // *** NOT IMPLEMENTED IN PENNANT ***

    // [2] Compute corner divergence and related quantities
    // [2.1] Find the corner divergence
    // [2.2] Compute the cos angle for c
    // [2.3] Find the evolution factor cevol(c) and the Delta u(c) = du(c)
    // [2.4] Find the weights cw(c)
//    setCornerDiv(carea, cdiv, cevol, cdu, ccos, sfirst, slast);

    // [3] Find the limiters Psi(c)
    // *** NOT IMPLEMENTED IN PENNANT ***

    // [4] Compute the Q vector (corner based)
    // [4.1] Compute cmu = (1-psi) . crho . zKUR . cevol
    // [4.2] Compute the q vector associated with c on edges
    //       e1=[n0,n1], e2=[n1,n2]
    //       cqe(2,c) = cmu(c).( u(n2)-u(n1) ) / l_{n1->n2}
    //       cqe(1,c) = cmu(c).( u(n1)-u(n0) ) / l_{n0->n1}
//    setQCnForce(cdiv, cdu, cevol, cqe1, cqe2, sfirst, slast);

    // [5] Compute the Q forces
//    setForce(carea, cqe1, cqe2, ccos, sf, sfirst, slast);

    // [6] Set velocity difference to use to compute timestep
    setVelDiff(sfirst, slast);

}
#endif

