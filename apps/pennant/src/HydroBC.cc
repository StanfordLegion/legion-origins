/*
 * HydroBC.cc
 *
 *  Created on: Jan 13, 2012
 *      Author: cferenba
 *
 * Copyright (c) 2012, Los Alamos National Security, LLC.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style open-source
 * license; see top-level LICENSE file for full license text.
 */

#include "HydroBC.hh"

#include "legion.h"

#include "Memory.hh"
#include "Mesh.hh"
#include "Hydro.hh"

using namespace std;
using namespace Memory;
using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;


namespace {  // unnamed
static void __attribute__ ((constructor)) registerTasks() {
    HighLevelRuntime::register_legion_task<HydroBC::applyFixedBCTask>(
            TID_APPLYFIXEDBC, Processor::LOC_PROC, true, true,
            AUTO_GENERATE_ID, TaskConfigOptions(), "apply_fixed_bc");
}
}; // namespace


HydroBC::HydroBC(
        Mesh* msh,
        const double2 v,
        const vector<int>& mbp)
    : mesh(msh), numb(mbp.size()), vfix(v) {

    mapbp = alloc<int>(numb);
    copy(mbp.begin(), mbp.end(), mapbp);

    mesh->getPlaneChunks(numb, mapbp, pchbfirst, pchblast);

    Context ctx = mesh->ctx;
    HighLevelRuntime* runtime = mesh->runtime;

    // create index space for boundary points
    IndexSpace isb = runtime->create_index_space(ctx, numb);
    IndexAllocator iab = runtime->create_index_allocator(ctx, isb);
    iab.alloc(numb);
    FieldSpace fsb = runtime->create_field_space(ctx);
    FieldAllocator fab = runtime->create_field_allocator(ctx, fsb);
    lrb = runtime->create_logical_region(ctx, isb, fsb);
    fab.allocate_field(sizeof(ptr_t), FID_MAPBP);
    fab.allocate_field(sizeof(int), FID_MAPBPREG);

    // create boundary point partition
    Coloring colorb;
    // force all colors to exist, even if they might be empty
    for (int c = 0; c < mesh->numpcs; ++c) {
        colorb[c];
    }
    for (int b = 0; b < numb; ++b) {
        int p = mbp[b];
        int c = mesh->nodecolors[p];
        if (c == MULTICOLOR) c = mesh->nodemcolors[p][0];
        colorb[c].points.insert(b);
    }
    IndexPartition ipb = runtime->create_index_partition(
                ctx, isb, colorb, true);
    lpb = runtime->get_logical_partition(ctx, lrb, ipb);

    // create boundary point maps
    vector<ptr_t> lgmapbp(&mbp[0], &mbp[numb]);
    vector<int> lgmapbpreg(numb);
    for (int b = 0; b < numb; ++b) {
        lgmapbpreg[b] = (mesh->nodecolors[mbp[b]] == MULTICOLOR);
    }

    mesh->setField(lrb, FID_MAPBP, &lgmapbp[0], numb);
    mesh->setField(lrb, FID_MAPBPREG, &lgmapbpreg[0], numb);

}


HydroBC::~HydroBC() {}


void HydroBC::applyFixedBCTask(
        const Task *task,
        const std::vector<PhysicalRegion> &regions,
        Context ctx,
        HighLevelRuntime *runtime) {
    const double2* args = (const double2*) task->args;
    const double2 vfix = args[0];
    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapbp =
      regions[0].get_field_accessor(FID_MAPBP).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, int> acc_mapbpreg =
      regions[0].get_field_accessor(FID_MAPBPREG).typeify<int>();
    RegionAccessor<AccessorType::Generic, double2> acc_pf[2] = {
      regions[1].get_field_accessor(FID_PF).typeify<double2>(),
      regions[2].get_field_accessor(FID_PF).typeify<double2>()
    };
    RegionAccessor<AccessorType::Generic, double2> acc_pu[2] = {
      regions[1].get_field_accessor(FID_PU0).typeify<double2>(),
      regions[2].get_field_accessor(FID_PU0).typeify<double2>()
    };

    const IndexSpace& isb = task->regions[0].region.get_index_space();
    for (Domain::DomainPointIterator itrb(isb); itrb; itrb++)
    {
        ptr_t b = itrb.p.get_index();
        ptr_t p = acc_mapbp.read(b);
        int preg = acc_mapbpreg.read(b);
        double2 pu = acc_pu[preg].read(p);
        double2 pf = acc_pf[preg].read(p);
        pu = project(pu, vfix);
        pf = project(pf, vfix);
        acc_pu[preg].write(p, pu);
        acc_pf[preg].write(p, pf);
    }

}


void HydroBC::applyFixedBC(
        double2* pu,
        double2* pf,
        const int bfirst,
        const int blast) {

    #pragma ivdep
    for (int b = bfirst; b < blast; ++b) {
        int p = mapbp[b];

        pu[p] = project(pu[p], vfix);
        pf[p] = project(pf[p], vfix);
    }

}

