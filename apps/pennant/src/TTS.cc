/*
 * TTS.cc
 *
 *  Created on: Feb 2, 2012
 *      Author: cferenba
 *
 * Copyright (c) 2012, Los Alamos National Security, LLC.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style open-source
 * license; see top-level LICENSE file for full license text.
 */

#include "TTS.hh"

#include "legion.h"

#include "Vec2.hh"
#include "InputFile.hh"
#include "Mesh.hh"
#include "Hydro.hh"

using namespace std;
using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;


namespace {  // unnamed
static void __attribute__ ((constructor)) registerTasks() {
    HighLevelRuntime::register_legion_task<TTS::calcForceTask>(
            TID_CALCFORCETTS, Processor::LOC_PROC, true, true,
            AUTO_GENERATE_ID, TaskConfigOptions(), "calc_force_tts");
}
}; // namespace


TTS::TTS(const InputFile* inp, Hydro* h) : hydro(h) {
    alfa = inp->getDouble("alfa", 0.5);
    ssmin = inp->getDouble("ssmin", 0.);

}


TTS::~TTS() {}


void TTS::calcForceTask(
        const Task *task,
        const std::vector<PhysicalRegion> &regions,
        Context ctx,
        HighLevelRuntime *runtime) {
    const double* args = (const double*) task->args;
    const double alfa  = args[0];
    const double ssmin = args[1];

    RegionAccessor<AccessorType::Generic, ptr_t> acc_mapsz =
      regions[0].get_field_accessor(FID_MAPSZ).typeify<ptr_t>();
    RegionAccessor<AccessorType::Generic, double> acc_sarea =
      regions[0].get_field_accessor(FID_SAREAP).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_smf =
      regions[0].get_field_accessor(FID_SMF).typeify<double>();
    RegionAccessor<AccessorType::Generic, double2> acc_ssurf =
      regions[0].get_field_accessor(FID_SSURFP).typeify<double2>();
    RegionAccessor<AccessorType::Generic, double> acc_zarea =
      regions[1].get_field_accessor(FID_ZAREAP).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zr =
      regions[1].get_field_accessor(FID_ZRP).typeify<double>();
    RegionAccessor<AccessorType::Generic, double> acc_zss =
      regions[1].get_field_accessor(FID_ZSS).typeify<double>();
    RegionAccessor<AccessorType::Generic, double2> acc_sf =
      regions[2].get_field_accessor(FID_SFT).typeify<double2>();

    //  Side density:
    //    srho = sm/sv = zr (sm/zm) / (sv/zv)
    //  Side pressure:
    //    sp   = zp + alfa dpdr (srho-zr)
    //         = zp + sdp
    //  Side delta pressure:
    //    sdp  = alfa dpdr (srho-zr)
    //         = alfa c**2 (srho-zr)
    //
    //    Notes: smf stores (sm/zm)
    //           svfac stores (sv/zv)

    const IndexSpace& iss = task->regions[0].region.get_index_space();
    for (Domain::DomainPointIterator itrs(iss); itrs; itrs++)
    {
        ptr_t s  = itrs.p.get_index();
        ptr_t z  = acc_mapsz.read(s);
        double sarea = acc_sarea.read(s);
        double zarea = acc_zarea.read(z);
        double vfacinv = zarea / sarea;
        double r = acc_zr.read(z);
        double mf = acc_smf.read(s);
        double srho = r * mf * vfacinv;
        double ss = acc_zss.read(z);
        double sstmp = max(ss, ssmin);
        sstmp = alfa * sstmp * sstmp;
        double sdp = sstmp * (srho - r);
        double2 surf = acc_ssurf.read(s);
        double2 sqq = -sdp * surf;
        acc_sf.write(s, sqq);
    }

}

