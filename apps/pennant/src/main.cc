/*
 * main.cc
 *
 *  Created on: Jan 23, 2012
 *      Author: cferenba
 *
 * Copyright (c) 2012, Los Alamos National Security, LLC.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style open-source
 * license; see top-level LICENSE file for full license text.
 */

#include <cstdlib>
#include <string>
#include <iostream>

#include "legion.h"
#include "default_mapper.h"

#include "Parallel.hh"
#include "InputFile.hh"
#include "Driver.hh"

using namespace std;
using namespace LegionRuntime::HighLevel;

enum TaskID {
    TID_MAIN
};


class SOAMapper : public DefaultMapper {
public:
  SOAMapper(Machine *machine,
      HighLevelRuntime *rt, Processor local);
public:
  virtual bool map_task(Task *task);
  virtual bool rank_copy_targets(const Mappable *mappable,
                                 LogicalRegion rebuild_region,
                                 const std::set<Memory> &current_instances,
                                 bool complete,
                                 size_t max_blocking_factor,
                                 std::set<Memory> &to_reuse,
                                 std::vector<Memory> &to_create,
                                 bool &create_one,
                                 size_t &blocking_factor);
};

void mapper_registration(Machine *machine, HighLevelRuntime *rt,
                          const std::set<Processor> &local_procs)
{
  for (std::set<Processor>::const_iterator it = local_procs.begin();
        it != local_procs.end(); it++)
  {
    rt->replace_default_mapper(
        new SOAMapper(machine, rt, *it), *it);
  }
}

SOAMapper::SOAMapper(Machine *m,
                     HighLevelRuntime *rt,
                     Processor p)
  : DefaultMapper(m, rt, p)
{
}

bool SOAMapper::map_task(Task *task)
{
  DefaultMapper::map_task(task);
  for (unsigned idx = 0; idx < task->regions.size(); idx++)
  {
    // use max blocking factor => SOA
    task->regions[idx].blocking_factor = task->regions[idx].max_blocking_factor;
//      task->regions[idx].blocking_factor = 1;
  }
  // Report successful mapping results
  return true;
}

bool SOAMapper::rank_copy_targets(const Mappable *mappable,
                                  LogicalRegion rebuild_region,
                                  const std::set<Memory> &current_instances,
                                  bool complete,
                                  size_t max_blocking_factor,
                                  std::set<Memory> &to_reuse,
                                  std::vector<Memory> &to_create,
                                  bool &create_one, size_t &blocking_factor)
{
    DefaultMapper::rank_copy_targets(mappable,
            rebuild_region,
            current_instances,
            complete,
            max_blocking_factor,
            to_reuse,
            to_create,
            create_one, blocking_factor);
    // use max blocking factor => SOA
    blocking_factor = max_blocking_factor;
    // return true => use composite instances
#ifdef SHARED_LOWLEVEL
    return false;
#else
    return true;
#endif
}


void mainTask(const Task *task,
              const std::vector<PhysicalRegion> &regions,
              Context ctx, HighLevelRuntime *runtime)
{
    Parallel::init();

    const InputArgs& iargs = HighLevelRuntime::get_input_args();

    // skip over legion args if present
    int i = 1;
    while (i < iargs.argc) {
        string arg(iargs.argv[i], 3);
        if (arg != "-ll" && arg != "-hl" && arg != "-ca" && arg != "-le") break;
        i += 2;
    }

    int numpcs = 1;
    const char* filename;
    if (i == iargs.argc - 1) {
        filename = iargs.argv[i];
    }
    else if (i == iargs.argc - 3 && iargs.argv[i] == string("-n")) {
        numpcs = atoi(iargs.argv[i + 1]);
        filename = iargs.argv[i + 2];
    }
    else {
        cerr << "Usage: pennant [legion args] "
             << "[-n <numpcs>] <filename>" << endl;
        exit(1);
    }

    InputFile inp(filename);

    string probname(filename);
    // strip .pnt suffix from filename
    int len = probname.length();
    if (probname.substr(len - 4, 4) == ".pnt")
        probname = probname.substr(0, len - 4);

    Driver drv(&inp, probname, numpcs, ctx, runtime);

    drv.run();

    Parallel::final();

}


int main(int argc, char **argv)
{
    // register main task only; other tasks have already been
    // registered by the classes that own them
    HighLevelRuntime::set_top_level_task_id(0);
    HighLevelRuntime::register_legion_task<mainTask>(
            TID_MAIN, Processor::LOC_PROC, true, false,
            AUTO_GENERATE_ID, TaskConfigOptions(), "main_task");

    HighLevelRuntime::set_registration_callback(mapper_registration);

    return HighLevelRuntime::start(argc, argv);
}

