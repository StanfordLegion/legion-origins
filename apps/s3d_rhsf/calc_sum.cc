
#include "rhsf.h"
#include "rhsf_mapper.h"
#include "rank_helper.h"
#include "calc_sum.h"

#if defined(USE_AVX_KERNELS) || defined(USE_SSE_KERNELS)
#include <x86intrin.h>

template<unsigned BOUNDARY>
static inline bool aligned(const void *ptr)
{
  return ((((unsigned long)ptr) & (BOUNDARY-1)) == 0);
}
#endif

typedef void (*dense_kernelptr_4d)(size_t n_pts, double *f1, double *f2, double *f3, double *f4);
typedef void (*dense_kernelptr_5d)(size_t n_pts, double *f1, double *f2, double *f3, double *f4, double *f5);

template <unsigned DIM, dense_kernelptr_4d KPTR>
bool attempt_dense_4d(const Rect<DIM>& subgrid_bounds,
		      PhysicalRegion r1, FieldID fid1,
		      PhysicalRegion r2, FieldID fid2,
		      PhysicalRegion r3, FieldID fid3,
		      PhysicalRegion r4, FieldID fid4)
{
  Rect<DIM> subrect;
  ByteOffset offsets[DIM], offsets2[DIM];

  double *p1 = r1.get_field_accessor(fid1).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets);
  if(!p1 || (subrect != subgrid_bounds) || !offsets_are_dense<DIM,double>(subgrid_bounds, offsets))
    return false;

  double *p2 = r2.get_field_accessor(fid2).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets2);
  if(!p2 || (subrect != subgrid_bounds) || offset_mismatch(DIM, offsets, offsets2))
    return false;


  double *p3 = r3.get_field_accessor(fid3).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets2);
  if(!p3 || (subrect != subgrid_bounds) || offset_mismatch(DIM, offsets, offsets2))
    return false;


  double *p4 = r4.get_field_accessor(fid4).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets2);
  if(!p4 || (subrect != subgrid_bounds) || offset_mismatch(DIM, offsets, offsets2))
    return false;

  KPTR(subgrid_bounds.volume(), p1, p2, p3, p4);
  return true;
}

template <unsigned DIM, dense_kernelptr_5d KPTR>
bool attempt_dense_5d(const Rect<DIM>& subgrid_bounds,
		      PhysicalRegion r1, FieldID fid1,
		      PhysicalRegion r2, FieldID fid2,
		      PhysicalRegion r3, FieldID fid3,
		      PhysicalRegion r4, FieldID fid4,
		      PhysicalRegion r5, FieldID fid5)
{
  Rect<DIM> subrect;
  ByteOffset offsets[DIM], offsets2[DIM];

  double *p1 = r1.get_field_accessor(fid1).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets);
  if(!p1 || (subrect != subgrid_bounds) || !offsets_are_dense<DIM,double>(subgrid_bounds, offsets))
    return false;

  double *p2 = r2.get_field_accessor(fid2).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets2);
  if(!p2 || (subrect != subgrid_bounds) || offset_mismatch(DIM, offsets, offsets2))
    return false;

  double *p3 = r3.get_field_accessor(fid3).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets2);
  if(!p3 || (subrect != subgrid_bounds) || offset_mismatch(DIM, offsets, offsets2))
    return false;

  double *p4 = r4.get_field_accessor(fid4).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets2);
  if(!p4 || (subrect != subgrid_bounds) || offset_mismatch(DIM, offsets, offsets2))
    return false;

  double *p5 = r5.get_field_accessor(fid5).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets2);
  if(!p5 || (subrect != subgrid_bounds) || offset_mismatch(DIM, offsets, offsets2))
    return false;

  KPTR(subgrid_bounds.volume(), p1, p2, p3, p4, p5);
  return true;
}

Sum3Task::Sum3Task(Domain domain,
                   TaskArgument global_arg,
                   ArgumentMap arg_map,
                   Predicate pred,
                   bool must,
                   MapperID id,
                   MappingTagID tag)
 : IndexLauncher(Sum3Task::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | Sum3Task::MAPPER_TAG)
{
}

/*static*/ const char * const Sum3Task::TASK_NAME = "sum_3_task";

void Sum3Task::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing, these get checked elsewhere
}

/*static*/
void Sum3Task::sum_3_fields(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
		  IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr_in, LogicalRegion lr_out,
		  FieldID fid_1, FieldID fid_2, FieldID fid_3, FieldID fid_out,
		  int stage, bool negate, int tag)
{
  Sum3Task sum_launcher(Domain::from_rect<3>(proc_grid_bounds),
                        TaskArgument(&negate, sizeof(negate)),
                        ArgumentMap());
  sum_launcher.tag |= tag;

  LogicalPartition lp_in_top = runtime->get_logical_partition(ctx, lr_in, ip_top);
  LogicalPartition lp_out_top = runtime->get_logical_partition(ctx, lr_out, ip_top);

  RegionRequirement rr_out(lp_out_top, 0, WRITE_DISCARD, EXCLUSIVE, lr_out);
  rr_out.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_out);

  RegionRequirement rr_in(lp_in_top, 0, READ_ONLY, EXCLUSIVE, lr_in);
  rr_in.add_field(fid_1);
  rr_in.add_field(fid_2);
  rr_in.add_field(fid_3);
  sum_launcher.add_region_requirement(rr_in);

  RankableTaskHelper::dispatch_task(sum_launcher, ctx, runtime, stage);
}

/*static*/
void Sum3Task::sum_3N_fields(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
		  IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr_in, LogicalRegion lr_out,
                  const std::vector<FieldID> &in1_fields, const std::vector<FieldID> &in2_fields,
                  const std::vector<FieldID> &in3_fields, const std::vector<FieldID> &out_fields,
                  int stage, bool negate, int tag)
{
  Sum3Task sum_launcher(Domain::from_rect<3>(proc_grid_bounds),
                        TaskArgument(&negate, sizeof(negate)),
                        ArgumentMap());
  sum_launcher.tag |= tag;

  LogicalPartition lp_in_top = runtime->get_logical_partition(ctx, lr_in, ip_top);
  LogicalPartition lp_out_top = runtime->get_logical_partition(ctx, lr_out, ip_top);

  RegionRequirement rr_out(lp_out_top, 0, WRITE_DISCARD, EXCLUSIVE, lr_out);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
    rr_out.add_field(out_fields[idx]);
  sum_launcher.add_region_requirement(rr_out);

  RegionRequirement rr_in(lp_in_top, 0, READ_ONLY, EXCLUSIVE, lr_in);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
  {
    rr_in.add_field(in1_fields[idx]);
    rr_in.add_field(in2_fields[idx]);
    rr_in.add_field(in3_fields[idx]);
  }
  sum_launcher.add_region_requirement(rr_in);

  RankableTaskHelper::dispatch_task(sum_launcher, ctx, runtime, stage);
}

/*static*/
bool Sum3Task::dense_sum_3(const Rect<3> &subgrid_bounds, bool negate,
                          RegionAccessor<AccessorType::Generic,double> fa_out,
                          RegionAccessor<AccessorType::Generic,double> fa_1,
                          RegionAccessor<AccessorType::Generic,double> fa_2,
                          RegionAccessor<AccessorType::Generic,double> fa_3)
{
  Rect<3> subrect;
  ByteOffset out_offsets[3], offsets[3];

  double *out_ptr = fa_out.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if (!out_ptr || (subrect != subgrid_bounds) || 
      !offsets_are_dense<3, double>(subgrid_bounds, out_offsets)) return false;

  double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in1_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in2_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in3_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  size_t n_pts = subgrid_bounds.volume(); 

  // Large number to get the prefetchers warmed up, 
  // but small enough to still handle small problem sizes
#ifdef BULLDOZER
#define FULL_STRIP_SIZE 512
#else
#define FULL_STRIP_SIZE 1024
#endif

  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined (USE_AVX_KERNELS)
      if (aligned<32>(out_ptr) && aligned<32>(in1_ptr) &&
          aligned<32>(in2_ptr) && aligned<32>(in3_ptr))
      {
        if (negate) {
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d sum = _mm256_load_pd(in1_ptr+(i<<2));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in2_ptr+(i<<2)));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in3_ptr+(i<<2)));
            // Fast negation
            sum = _mm256_xor_pd(sum,_mm256_set1_pd(-0.0));
            _mm256_stream_pd(out_ptr+(i<<2),sum);
          }
        } else {
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d sum = _mm256_load_pd(in1_ptr+(i<<2));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in2_ptr+(i<<2)));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in3_ptr+(i<<2)));
            _mm256_stream_pd(out_ptr+(i<<2),sum);
          }
        }
      } else {
        // Fall back to SSE for unaligned
        if (negate) { 
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
            sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
            sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));
            // Fast negation
            sum = _mm_xor_pd(sum,_mm_set1_pd(-0.0));
            _mm_stream_pd(out_ptr+(i<<1),sum);
          }
        } else {
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
            sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
            sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));
            _mm_stream_pd(out_ptr+(i<<1),sum);
          }
        }
      }
#elif defined(USE_SSE_KERNELS)
      if (negate) { 
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
          sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
          sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));
          // Fast negation
          sum = _mm_xor_pd(sum,_mm_set1_pd(-0.0));
          _mm_stream_pd(out_ptr+(i<<1),sum);
        }
      } else {
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
          sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
          sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));
          _mm_stream_pd(out_ptr+(i<<1),sum);
        }
      }
#else
      if (negate) {
        for (int i = 0; i < FULL_STRIP_SIZE; i++) {
          out_ptr[i] = -(in1_ptr[i] + in2_ptr[i] + in3_ptr[i]);
        }
      } else {
        for (int i = 0; i < FULL_STRIP_SIZE; i++) {
          out_ptr[i] = in1_ptr[i] + in2_ptr[i] + in3_ptr[i];
        }
      }
#endif
      n_pts -= FULL_STRIP_SIZE;
    } else {
      if (negate) {
        for (size_t i = 0; i < n_pts; i++) {
          out_ptr[i] = -(in1_ptr[i] + in2_ptr[i] + in3_ptr[i]);
        }
      } else {
        for (size_t i = 0; i < n_pts; i++) {
          out_ptr[i] = in1_ptr[i] + in2_ptr[i] + in3_ptr[i];
        }
      }
      n_pts = 0;
    }
    out_ptr += FULL_STRIP_SIZE;
    in1_ptr += FULL_STRIP_SIZE;
    in2_ptr += FULL_STRIP_SIZE;
    in3_ptr += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
}

/*static*/
bool Sum3Task::fast_sum_3(RegionAccessor<AccessorType::Generic,double> fa_out,
                          RegionAccessor<AccessorType::Generic,double> fa_1,
                          RegionAccessor<AccessorType::Generic,double> fa_2,
                          RegionAccessor<AccessorType::Generic,double> fa_3,
                          Rect<3> subgrid_bounds,
                          bool negate)
{
  Rect<3> subrect;
  ByteOffset out_offsets[3];
  double *out_ptr = fa_out.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in1_offsets[3];
  double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, in1_offsets);
  if(!in1_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in2_offsets[3];
  double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, in2_offsets);
  if(!in2_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in3_offsets[3];
  double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, in3_offsets);
  if(!in3_ptr) return false;
  assert(subrect == subgrid_bounds);

  for(GenericPointInRectIterator<3> pir(Rect<3>(Point<3>::ZEROES(), subgrid_bounds.hi - subgrid_bounds.lo)); pir; pir++) {
    double *outp = out_ptr + pir.p[0] * out_offsets[0] + pir.p[1] * out_offsets[1] + pir.p[2] * out_offsets[2];
    double *in1p = in1_ptr + pir.p[0] * in1_offsets[0] + pir.p[1] * in1_offsets[1] + pir.p[2] * in1_offsets[2];
    double *in2p = in2_ptr + pir.p[0] * in2_offsets[0] + pir.p[1] * in2_offsets[1] + pir.p[2] * in2_offsets[2];
    double *in3p = in3_ptr + pir.p[0] * in3_offsets[0] + pir.p[1] * in3_offsets[1] + pir.p[2] * in3_offsets[2];
    double sum = *in1p + *in2p + *in3p;
    if (negate)
      sum = -sum;
    *outp = sum;
  }

  return true;
}

/*static*/
void Sum3Task::kernel_sum_3_normal(size_t n_pts, double *out, double *in1, double *in2, double *in3)
{
  //printf("dense sum_3\n");
  for(size_t i = 0; i < n_pts; i++)
    out[i] = (in1[i] + in2[i] + in3[i]);
}

/*static*/
void Sum3Task::kernel_sum_3_negate(size_t n_pts, double *out, double *in1, double *in2, double *in3)
{
  //printf("dense sum_3\n");
  for(size_t i = 0; i < n_pts; i++)
    out[i] = -(in1[i] + in2[i] + in3[i]);
}

/*static*/
void Sum3Task::cpu_base_impl(S3DRank *rank, const Task *task,
                             const Rect<3> &my_subgrid_bounds,
                             const std::vector<RegionRequirement> &reqs,
                             const std::vector<PhysicalRegion> &regions,
                             Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  bool negate = *(bool *)task->args;

  const int num_species = reqs[0].instance_fields.size();

  for (int spec = 0; spec < num_species; spec++)
  {
    // get whatever accessors you need here
    RegionAccessor<AccessorType::Generic,double> fa_out = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_1 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[3*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_2 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[3*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_3 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[3*spec+2]).typeify<double>();

    if (dense_sum_3(my_subgrid_bounds, negate, fa_out, fa_1, fa_2, fa_3))
      continue;

    if(negate) {
      if(attempt_dense_4d<3, kernel_sum_3_negate>(my_subgrid_bounds,
                                                  regions[0], reqs[0].instance_fields[spec],
                                                  regions[1], reqs[1].instance_fields[3*spec],
                                                  regions[1], reqs[1].instance_fields[3*spec+1],
                                                  regions[1], reqs[1].instance_fields[3*spec+2]))
        continue;
    } else {
      if(attempt_dense_4d<3, kernel_sum_3_normal>(my_subgrid_bounds,
                                                  regions[0], reqs[0].instance_fields[spec],
                                                  regions[1], reqs[1].instance_fields[3*spec],
                                                  regions[1], reqs[1].instance_fields[3*spec+1],
                                                  regions[1], reqs[1].instance_fields[3*spec+2]))
        continue;
    }

    

    if(fast_sum_3(fa_out, fa_1, fa_2, fa_3, my_subgrid_bounds, negate)) continue;

    //printf("falling back to slow sum_3\n");

    for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double f1 = fa_1.read(dp);
      double f2 = fa_2.read(dp);
      double f3 = fa_3.read(dp);

      double sum = f1 + f2 + f3;
      if(negate)
        sum = -sum;

      fa_out.write(dp, sum);
    }
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern
void gpu_sum_3_fields(const bool negate,
                      const int num_points,
                      const double *in1_ptr,
                      const double *in2_ptr,
                      const double *in3_ptr,
                      double *out_ptr);
#endif

/*static*/
void Sum3Task::gpu_base_impl(S3DRank *rank, const Task *task,
                             const Rect<3> &subgrid_bounds,
                             const std::vector<RegionRequirement> &reqs,
                             const std::vector<PhysicalRegion> &regions,
                             Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  bool negate = *(bool *)task->args;

  const int num_species = reqs[0].instance_fields.size();

  for (int spec = 0; spec < num_species; spec++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_out = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_1 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[3*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_2 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[3*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_3 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[3*spec+2]).typeify<double>();

    Rect<3> subrect;
    ByteOffset out_offsets[3], offsets[3];

    double *out_ptr = fa_out.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
    if (!out_ptr || (subrect != subgrid_bounds) || 
        !offsets_are_dense<3, double>(subgrid_bounds, out_offsets)) assert(false);

    double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in1_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in2_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in3_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    size_t n_pts = subgrid_bounds.volume();

    gpu_sum_3_fields(negate, n_pts, in1_ptr, in2_ptr, in3_ptr, out_ptr);
  }
#endif
#endif
}

struct SumIntegrateArgs {
  double alpha, beta, err, small;
  int which;
  bool has_small;
};

Sum3IntegrateTask::Sum3IntegrateTask(Domain domain,
				     TaskArgument global_arg,
				     ArgumentMap arg_map,
				     Predicate pred,
				     bool must,
				     MapperID id,
				     MappingTagID tag)
 : IndexLauncher(Sum3IntegrateTask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | Sum3IntegrateTask::MAPPER_TAG)
{
}

/*static*/ const char * const Sum3IntegrateTask::TASK_NAME = "sum_3_integrate_task";

void Sum3IntegrateTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing, these get checked elsewhere
}

/*static*/
void Sum3IntegrateTask::sum_3_integrate_fields(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
					       IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr_in,
					       LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
					       FieldID fid_1, FieldID fid_2, FieldID fid_3, FieldID fid_out,
					       double alpha, double beta, double err, Future f_timestep,
					       int which, int stage, int tag)
{
  SumIntegrateArgs args;
  args.alpha = alpha;
  args.beta = beta;
  args.err = err;
  args.which = which;
  args.has_small = false;

  Sum3IntegrateTask sum_launcher(Domain::from_rect<3>(proc_grid_bounds),
				 TaskArgument(&args, sizeof(args)),
				 ArgumentMap());
  sum_launcher.tag |= tag;

  LogicalPartition lp_in_top = runtime->get_logical_partition(ctx, lr_in, ip_top);
  LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  LogicalPartition lp_qerr_top = runtime->get_logical_partition(ctx, lr_qerr, ip_top);
  LogicalPartition lp_qtmp_top = runtime->get_logical_partition(ctx, lr_qtmp, ip_top);

  RegionRequirement rr_q(lp_q_top, 0, READ_WRITE, EXCLUSIVE, lr_q);
  rr_q.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_q);

  // qerr is only written on the first integation
  RegionRequirement rr_qerr(lp_qerr_top, 0, 
			    ((which == FIRST_INTEGRATION) ? WRITE_DISCARD : READ_WRITE),
			    EXCLUSIVE, lr_qerr);
  rr_qerr.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_qerr);

  // qtmp is only read on the last integration, only written on the first
  RegionRequirement rr_qtmp(lp_qtmp_top, 0, 
			    ((which == FIRST_INTEGRATION) ? WRITE_DISCARD : 
			     (which == LAST_INTEGRATION) ? READ_ONLY :
			     READ_WRITE),
			    EXCLUSIVE, lr_qtmp);
  rr_qtmp.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_qtmp);

  RegionRequirement rr_in(lp_in_top, 0, READ_ONLY, EXCLUSIVE, lr_in);
  rr_in.add_field(fid_1);
  rr_in.add_field(fid_2);
  rr_in.add_field(fid_3);
  sum_launcher.add_region_requirement(rr_in);

  sum_launcher.add_future(f_timestep);

  RankableTaskHelper::dispatch_task(sum_launcher, ctx, runtime, tag);
}

/*static*/
void Sum3IntegrateTask::sum_3N_integrate_fields(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
						IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr_in,
						LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
						const std::vector<FieldID> &in1_fields, const std::vector<FieldID> &in2_fields,
						const std::vector<FieldID> &in3_fields, const std::vector<FieldID> &out_fields,
						double alpha, double beta, double err, Future f_timestep,
						int which, int stage, int tag)
{
  SumIntegrateArgs args;
  args.alpha = alpha;
  args.beta = beta;
  args.err = err;
  args.which = which;
  args.has_small = false;

  Sum3IntegrateTask sum_launcher(Domain::from_rect<3>(proc_grid_bounds),
				 TaskArgument(&args, sizeof(args)),
				 ArgumentMap());
  sum_launcher.tag |= tag;

  LogicalPartition lp_in_top = runtime->get_logical_partition(ctx, lr_in, ip_top);
  LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  LogicalPartition lp_qerr_top = runtime->get_logical_partition(ctx, lr_qerr, ip_top);
  LogicalPartition lp_qtmp_top = runtime->get_logical_partition(ctx, lr_qtmp, ip_top);

  RegionRequirement rr_q(lp_q_top, 0, READ_WRITE, EXCLUSIVE, lr_q);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
    rr_q.add_field(out_fields[idx]);
  sum_launcher.add_region_requirement(rr_q);

  // qerr is only written on the first integation
  RegionRequirement rr_qerr(lp_qerr_top, 0, 
			    ((which == FIRST_INTEGRATION) ? WRITE_DISCARD : 
			     READ_WRITE),
			    EXCLUSIVE, lr_qerr);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
    rr_qerr.add_field(out_fields[idx]);
  sum_launcher.add_region_requirement(rr_qerr);

  // qtmp is only read on the last integration, only written on the first
  RegionRequirement rr_qtmp(lp_qtmp_top, 0, 
			    ((which == FIRST_INTEGRATION) ? WRITE_DISCARD : 
			     (which == LAST_INTEGRATION) ? READ_ONLY :
			     READ_WRITE),
			    EXCLUSIVE, lr_qtmp);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
    rr_qtmp.add_field(out_fields[idx]);
  sum_launcher.add_region_requirement(rr_qtmp);

  RegionRequirement rr_in(lp_in_top, 0, READ_ONLY, EXCLUSIVE, lr_in);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
  {
    rr_in.add_field(in1_fields[idx]);
    rr_in.add_field(in2_fields[idx]);
    rr_in.add_field(in3_fields[idx]);
  }
  sum_launcher.add_region_requirement(rr_in);

  sum_launcher.add_future(f_timestep);

  RankableTaskHelper::dispatch_task(sum_launcher, ctx, runtime, stage);
}

/*static*/
bool Sum3IntegrateTask::dense_sum_3_integrate(const Rect<3> &subgrid_bounds,
					      RegionAccessor<AccessorType::Generic,double> fa_q,
					      RegionAccessor<AccessorType::Generic,double> fa_qerr,
					      RegionAccessor<AccessorType::Generic,double> fa_qtmp,
					      RegionAccessor<AccessorType::Generic,double> fa_1,
					      RegionAccessor<AccessorType::Generic,double> fa_2,
					      RegionAccessor<AccessorType::Generic,double> fa_3,
					      double alpha, double beta, double err, int which)
{
  Rect<3> subrect;
  ByteOffset out_offsets[3], offsets[3];

  double *q_ptr = fa_q.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if (!q_ptr || (subrect != subgrid_bounds) || 
      !offsets_are_dense<3, double>(subgrid_bounds, out_offsets)) return false;

  double *qerr_ptr = fa_qerr.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!qerr_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *qtmp_ptr = fa_qtmp.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!qtmp_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in1_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in2_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in3_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  size_t n_pts = subgrid_bounds.volume(); 

  // Large number to get the prefetchers warmed up, 
  // but small enough to still handle small problem sizes
#ifdef BULLDOZER
#define FULL_STRIP_SIZE 512
#else
#define FULL_STRIP_SIZE 1024
#endif

  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined (USE_AVX_KERNELS)
      if (aligned<32>(q_ptr) && aligned<32>(qerr_ptr) && aligned<32>(qtmp_ptr) &&
	  aligned<32>(in1_ptr) && aligned<32>(in2_ptr) && aligned<32>(in3_ptr))
      {
        if (which == FIRST_INTEGRATION) {
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d sum = _mm256_load_pd(in1_ptr+(i<<2));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in2_ptr+(i<<2)));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in3_ptr+(i<<2)));

            __m256d q = _mm256_load_pd(q_ptr+(i<<2));
            __m256d qerr = _mm256_set1_pd(0.0);
            __m256d qtmp = q;
            qerr = _mm256_add_pd(qerr, _mm256_mul_pd(_mm256_set1_pd(err), sum));
            q = _mm256_add_pd(qtmp, _mm256_mul_pd(_mm256_set1_pd(alpha), sum));
            
            _mm256_store_pd(qerr_ptr+(i<<2),qerr);
            _mm256_store_pd(q_ptr+(i<<2),q);
            qtmp = _mm256_add_pd(q, _mm256_mul_pd(_mm256_set1_pd(beta), sum));
	    _mm256_store_pd(qtmp_ptr+(i<<2),qtmp);
          }
        } else if (which != LAST_INTEGRATION) {
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
           __m256d sum = _mm256_load_pd(in1_ptr+(i<<2));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in2_ptr+(i<<2)));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in3_ptr+(i<<2)));

            __m256d q = _mm256_load_pd(q_ptr+(i<<2)); 
            __m256d qerr = _mm256_load_pd(qerr_ptr+(i<<2));
	    __m256d qtmp = _mm256_load_pd(qtmp_ptr+(i<<2));
            qerr = _mm256_add_pd(qerr, _mm256_mul_pd(_mm256_set1_pd(err), sum));
            q = _mm256_add_pd(qtmp, _mm256_mul_pd(_mm256_set1_pd(alpha), sum));
            
            _mm256_store_pd(qerr_ptr+(i<<2),qerr);
            _mm256_store_pd(q_ptr+(i<<2),q);
            qtmp = _mm256_add_pd(q, _mm256_mul_pd(_mm256_set1_pd(beta), sum));
	    _mm256_store_pd(qtmp_ptr+(i<<2),qtmp);
          }
        } else { // LAST_INTEGRATION
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d sum = _mm256_load_pd(in1_ptr+(i<<2));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in2_ptr+(i<<2)));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in3_ptr+(i<<2)));

            __m256d q = _mm256_load_pd(q_ptr+(i<<2));
            __m256d qerr = _mm256_load_pd(qerr_ptr+(i<<2));
	    __m256d qtmp = _mm256_load_pd(qtmp_ptr+(i<<2));

            qerr = _mm256_add_pd(qerr, _mm256_mul_pd(_mm256_set1_pd(err), sum));
            q = _mm256_add_pd(qtmp, _mm256_mul_pd(_mm256_set1_pd(alpha), sum));
            
            _mm256_store_pd(qerr_ptr+(i<<2),qerr);
            _mm256_store_pd(q_ptr+(i<<2),q);
          }
        }
#if 0
	for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
	  __m256d sum = _mm256_load_pd(in1_ptr+(i<<2));
	  sum = _mm256_add_pd(sum,_mm256_load_pd(in2_ptr+(i<<2)));
	  sum = _mm256_add_pd(sum,_mm256_load_pd(in3_ptr+(i<<2)));

	  __m256d q = _mm256_load_pd(q_ptr+(i<<2));
	  __m256d qerr, qtmp;
	  if(which == FIRST_INTEGRATION) {
	    qerr = _mm256_set1_pd(0.0);
	    qtmp = q;
	  } else {
	    qerr = _mm256_load_pd(qerr_ptr+(i<<2));
	    qtmp = _mm256_load_pd(qtmp_ptr+(i<<2));
	  }

	  qerr = _mm256_add_pd(qerr, _mm256_mul_pd(_mm256_set1_pd(err), sum));
	  q = _mm256_add_pd(qtmp, _mm256_mul_pd(_mm256_set1_pd(alpha), sum));
	  
	  _mm256_stream_pd(qerr_ptr+(i<<2),qerr);
	  _mm256_stream_pd(q_ptr+(i<<2),q);
	  if(which != LAST_INTEGRATION) {
	    qtmp = _mm256_add_pd(q, _mm256_mul_pd(_mm256_set1_pd(beta), sum));
	    _mm256_stream_pd(qtmp_ptr+(i<<2),qtmp);
	  }
        }
#endif
      } else
#endif
#if defined(USE_AVX_KERNELS) || defined(USE_SSE_KERNELS)
      if (aligned<16>(q_ptr) && aligned<16>(qerr_ptr) && aligned<16>(qtmp_ptr) &&
	  aligned<16>(in1_ptr) && aligned<16>(in2_ptr) && aligned<16>(in3_ptr))
      {
        if (which == FIRST_INTEGRATION) {
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
            sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
            sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));

            __m128d q = _mm_load_pd(q_ptr+(i<<1));
            __m128d qerr = _mm_set1_pd(0.0);
	    __m128d qtmp = q;

            qerr = _mm_add_pd(qerr, _mm_mul_pd(_mm_set1_pd(err), sum));
            q = _mm_add_pd(qtmp, _mm_mul_pd(_mm_set1_pd(alpha), sum));
            
            _mm_store_pd(qerr_ptr+(i<<1),qerr);
            _mm_store_pd(q_ptr+(i<<1),q);
            qtmp = _mm_add_pd(q, _mm_mul_pd(_mm_set1_pd(beta), sum));
	    _mm_store_pd(qtmp_ptr+(i<<1),qtmp);
          }
        } else if (which != LAST_INTEGRATION) {
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
            sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
            sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));

            __m128d q = _mm_load_pd(q_ptr+(i<<1));
            __m128d qerr = _mm_load_pd(qerr_ptr+(i<<1));
	    __m128d qtmp = _mm_load_pd(qtmp_ptr+(i<<1));

            qerr = _mm_add_pd(qerr, _mm_mul_pd(_mm_set1_pd(err), sum));
            q = _mm_add_pd(qtmp, _mm_mul_pd(_mm_set1_pd(alpha), sum));
            
            _mm_store_pd(qerr_ptr+(i<<1),qerr);
            _mm_store_pd(q_ptr+(i<<1),q);
            qtmp = _mm_add_pd(q, _mm_mul_pd(_mm_set1_pd(beta), sum));
	    _mm_store_pd(qtmp_ptr+(i<<1),qtmp);
          }
        } else { // LAST_INTEGRATION
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
            sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
            sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));

            __m128d q = _mm_load_pd(q_ptr+(i<<1));
            __m128d qerr = _mm_load_pd(qerr_ptr+(i<<1));
	    __m128d qtmp = _mm_load_pd(qtmp_ptr+(i<<1));

            qerr = _mm_add_pd(qerr, _mm_mul_pd(_mm_set1_pd(err), sum));
            q = _mm_add_pd(qtmp, _mm_mul_pd(_mm_set1_pd(alpha), sum));

            _mm_store_pd(qerr_ptr+(i<<1),qerr);
            _mm_store_pd(q_ptr+(i<<1),q);
          }
        }
#if 0
	for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
	  __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
	  sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
	  sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));

	  __m128d q = _mm_load_pd(q_ptr+(i<<1));
	  __m128d qerr, qtmp;
	  if(which == FIRST_INTEGRATION) {
	    qerr = _mm_set1_pd(0.0);
	    qtmp = q;
	  } else {
	    qerr = _mm_load_pd(qerr_ptr+(i<<1));
	    qtmp = _mm_load_pd(qtmp_ptr+(i<<1));
	  }

	  qerr = _mm_add_pd(qerr, _mm_mul_pd(_mm_set1_pd(err), sum));
	  q = _mm_add_pd(qtmp, _mm_mul_pd(_mm_set1_pd(alpha), sum));
	  
	  _mm_stream_pd(qerr_ptr+(i<<1),qerr);
	  _mm_stream_pd(q_ptr+(i<<1),q);
	  if(which != LAST_INTEGRATION) {
	    qtmp = _mm_add_pd(q, _mm_mul_pd(_mm_set1_pd(beta), sum));
	    _mm_stream_pd(qtmp_ptr+(i<<1),qtmp);
	  }
        }
#endif
      } else
#endif
      {
	// scalar fallback
        if (which == FIRST_INTEGRATION) {
          for (int i = 0; i < FULL_STRIP_SIZE; i++) {
            double sum = in1_ptr[i] + in2_ptr[i] + in3_ptr[i];

            double q = q_ptr[i];
            double qerr = 0;
	    double qtmp = q;

            qerr += err * sum;
            q = qtmp + alpha * sum;
            
            qerr_ptr[i] = qerr;
            q_ptr[i] = q;

            qtmp = q + beta * sum;
	    qtmp_ptr[i] = qtmp;
          }
        } else if (which == LAST_INTEGRATION) {
          for (int i = 0; i < FULL_STRIP_SIZE; i++) {
            double sum = in1_ptr[i] + in2_ptr[i] + in3_ptr[i];

            double q = q_ptr[i];
            double qerr = qerr_ptr[i];
	    double qtmp = qtmp_ptr[i];

            qerr += err * sum;
            q = qtmp + alpha * sum;

            qerr_ptr[i] = qerr;
            q_ptr[i] = q;

            qtmp = q + beta * sum;
	    qtmp_ptr[i] = qtmp;
          }
        } else {
          for (int i = 0; i < FULL_STRIP_SIZE; i++) {
            double sum = in1_ptr[i] + in2_ptr[i] + in3_ptr[i];

            double q = q_ptr[i];
            double qerr = qerr_ptr[i];
	    double qtmp = qtmp_ptr[i];

            qerr += err * sum;
            q = qtmp + alpha * sum;
            
            qerr_ptr[i] = qerr;
            q_ptr[i] = q;
          }
        }
#if 0
        for (int i = 0; i < FULL_STRIP_SIZE; i++) {
	  double sum = in1_ptr[i] + in2_ptr[i] + in3_ptr[i];

	  double q = q_ptr[i];
	  double qerr, qtmp;
	  if(which == FIRST_INTEGRATION) {
	    qerr = 0;
	    qtmp = q;
	  } else {
	    qerr = qerr_ptr[i];
	    qtmp = qtmp_ptr[i];
	  }

	  qerr += err * sum;
	  q = qtmp + alpha * sum;
	  
	  qerr_ptr[i] = qerr;
	  q_ptr[i] = q;

	  if(which != LAST_INTEGRATION) {
	    qtmp = q + beta * sum;
	    qtmp_ptr[i] = qtmp;
	  }
	}
#endif
      }
      n_pts -= FULL_STRIP_SIZE;
    } else {
      {
	// scalar fallback
        for (size_t i = 0; i < n_pts; i++) {
	  double sum = in1_ptr[i] + in2_ptr[i] + in3_ptr[i];

	  double q = q_ptr[i];
	  double qerr, qtmp;
	  if(which == FIRST_INTEGRATION) {
	    qerr = 0;
	    qtmp = q;
	  } else {
	    qerr = qerr_ptr[i];
	    qtmp = qtmp_ptr[i];
	  }

	  qerr += err * sum;
	  q = qtmp + alpha * sum;
	  
	  qerr_ptr[i] = qerr;
	  q_ptr[i] = q;

	  if(which != LAST_INTEGRATION) {
	    qtmp = q + beta * sum;
	    qtmp_ptr[i] = qtmp;
	  }
	}
      }
      n_pts = 0;
    }
    q_ptr += FULL_STRIP_SIZE;
    qerr_ptr += FULL_STRIP_SIZE;
    qtmp_ptr += FULL_STRIP_SIZE;
    in1_ptr += FULL_STRIP_SIZE;
    in2_ptr += FULL_STRIP_SIZE;
    in3_ptr += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
}

/*static*/
bool Sum3IntegrateTask::fast_sum_3_integrate(RegionAccessor<AccessorType::Generic,double> fa_q,
					     RegionAccessor<AccessorType::Generic,double> fa_qerr,
					     RegionAccessor<AccessorType::Generic,double> fa_qtmp,
					     RegionAccessor<AccessorType::Generic,double> fa_1,
					     RegionAccessor<AccessorType::Generic,double> fa_2,
					     RegionAccessor<AccessorType::Generic,double> fa_3,
					     Rect<3> subgrid_bounds,
					     double alpha, double beta, double err, int which)
{
  Rect<3> subrect;
  ByteOffset q_offsets[3];
  double *q_ptr = fa_q.raw_rect_ptr<3>(subgrid_bounds, subrect, q_offsets);
  if(!q_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset qerr_offsets[3];
  double *qerr_ptr = fa_qerr.raw_rect_ptr<3>(subgrid_bounds, subrect, qerr_offsets);
  if(!qerr_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset qtmp_offsets[3];
  double *qtmp_ptr = fa_qtmp.raw_rect_ptr<3>(subgrid_bounds, subrect, qtmp_offsets);
  if(!qtmp_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in1_offsets[3];
  double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, in1_offsets);
  if(!in1_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in2_offsets[3];
  double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, in2_offsets);
  if(!in2_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in3_offsets[3];
  double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, in3_offsets);
  if(!in3_ptr) return false;
  assert(subrect == subgrid_bounds);

  for(GenericPointInRectIterator<3> pir(Rect<3>(Point<3>::ZEROES(), subgrid_bounds.hi - subgrid_bounds.lo)); pir; pir++) {
    double *qp = q_ptr + pir.p[0] * q_offsets[0] + pir.p[1] * q_offsets[1] + pir.p[2] * q_offsets[2];
    double *qerrp = qerr_ptr + pir.p[0] * qerr_offsets[0] + pir.p[1] * qerr_offsets[1] + pir.p[2] * qerr_offsets[2];
    double *qtmpp = qtmp_ptr + pir.p[0] * qtmp_offsets[0] + pir.p[1] * qtmp_offsets[1] + pir.p[2] * qtmp_offsets[2];
    const double *in1p = in1_ptr + pir.p[0] * in1_offsets[0] + pir.p[1] * in1_offsets[1] + pir.p[2] * in1_offsets[2];
    const double *in2p = in2_ptr + pir.p[0] * in2_offsets[0] + pir.p[1] * in2_offsets[1] + pir.p[2] * in2_offsets[2];
    const double *in3p = in3_ptr + pir.p[0] * in3_offsets[0] + pir.p[1] * in3_offsets[1] + pir.p[2] * in3_offsets[2];
    double sum = *in1p + *in2p + *in3p;

    double q = *qp;
    double qerr, qtmp;
    if(which == FIRST_INTEGRATION) {
      qerr = 0;
      qtmp = q;
    } else {
      qerr = *qerrp;
      qtmp = *qtmpp;
    }

    qerr += err * sum;
    q = qtmp + alpha * sum;

    *qerrp = qerr;
    *qp = q;

    if(which != LAST_INTEGRATION) {
      qtmp = q + beta * sum;
      *qtmpp = qtmp;
    }
  }

  return true;
}

/*static*/
void Sum3IntegrateTask::kernel_sum_3_integrate(size_t n_pts,
					       double *q, double *qerr, double *qtmp,
					       const double *in1, const double *in2, const double *in3,
					       double alpha, double beta, double err, int which)
{
  //printf("dense sum_3\n");
  for(size_t i = 0; i < n_pts; i++) {
    double sum = (in1[i] + in2[i] + in3[i]);
    double qv = q[i];
    double qerrv, qtmpv;
    if(which == FIRST_INTEGRATION) {
      qerrv = 0;
      qtmpv = qv;
    } else {
      qerrv = qerr[i];
      qtmpv = qtmp[i];
    }

    qerrv += err * sum;
    qv = qtmpv + alpha * sum;

    qerr[i] = qerrv;
    q[i] = qv;

    if(which != LAST_INTEGRATION) {
      qtmpv = qv + beta * sum;
      qtmp[i] = qtmpv;
    }
  }
}

/*static*/
void Sum3IntegrateTask::cpu_base_impl(S3DRank *rank, const Task *task,
				      const Rect<3> &my_subgrid_bounds,
				      const std::vector<RegionRequirement> &reqs,
				      const std::vector<PhysicalRegion> &regions,
				      Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  const SumIntegrateArgs *args = (const SumIntegrateArgs *)(task->args);

  Future f_timestep = task->futures[0];
  double timestep = f_timestep.get_reference<TimestepInfo>().cur_timestep;

  const int num_species = reqs[0].instance_fields.size();

  for (int spec = 0; spec < num_species; spec++)
  {
    // get whatever accessors you need here
    RegionAccessor<AccessorType::Generic,double> fa_q = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qerr = 
      regions[1].get_field_accessor(reqs[1].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qtmp = 
      regions[2].get_field_accessor(reqs[2].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_1 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[3*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_2 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[3*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_3 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[3*spec+2]).typeify<double>();

#ifdef DEBUG_NONDET
    if(spec == 0) {
      DomainPoint dp = DomainPoint::from_point<3>(my_subgrid_bounds.lo);
      double sum = (fa_1.read(dp) +
		    fa_2.read(dp) +
		    fa_3.read(dp));
      printf(" rhsf(1,1,1,%d) = %g + %g + %g = %g, q = %g (%a %a %a %a)\n",
	     reqs[0].instance_fields[0]+1,
             fa_1.read(dp), fa_2.read(dp), fa_3.read(dp), sum, fa_q.read(dp),
             fa_1.read(dp), fa_2.read(dp), fa_3.read(dp), sum, fa_q.read(dp));
    }
#endif

    if (dense_sum_3_integrate(my_subgrid_bounds, fa_q, fa_qerr, fa_qtmp, fa_1, fa_2, fa_3, 
			      args->alpha * timestep, args->beta * timestep, args->err * timestep,
			      args->which))
      continue;

#if 0
    // can't currently pass extra args to the attempt_dense template
    if(negate) {
      if(attempt_dense_4d<3, kernel_sum_3_negate>(my_subgrid_bounds,
                                                  regions[0], reqs[0].instance_fields[spec],
                                                  regions[1], reqs[1].instance_fields[3*spec],
                                                  regions[1], reqs[1].instance_fields[3*spec+1],
                                                  regions[1], reqs[1].instance_fields[3*spec+2]))
        continue;
    } else {
      if(attempt_dense_4d<3, kernel_sum_3_normal>(my_subgrid_bounds,
                                                  regions[0], reqs[0].instance_fields[spec],
                                                  regions[1], reqs[1].instance_fields[3*spec],
                                                  regions[1], reqs[1].instance_fields[3*spec+1],
                                                  regions[1], reqs[1].instance_fields[3*spec+2]))
        continue;
    }
#endif

    printf("falling back to fast sum_3_integrate\n");

    if(fast_sum_3_integrate(fa_q, fa_qerr, fa_qtmp, fa_1, fa_2, fa_3, my_subgrid_bounds,
			    args->alpha * timestep, args->beta * timestep, args->err *timestep,
			    args->which))
      continue;
    printf("falling back to slow sum_3_integrate\n");

    for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double f1 = fa_1.read(dp);
      double f2 = fa_2.read(dp);
      double f3 = fa_3.read(dp);

      double q = fa_q.read(dp);
      double qerr = ((args->which == FIRST_INTEGRATION) ? 0 : fa_qerr.read(dp));
      double qtmp = ((args->which == FIRST_INTEGRATION) ? q : fa_qtmp.read(dp));

      double sum = f1 + f2 + f3;

      // if(pir.p == my_subgrid_bounds.lo)
      // 	printf("s3i: %d %g / %g %g %g %d / %g %g %g\n",
      // 	       reqs[0].instance_fields[spec], sum, args->alpha * timestep, args->beta * timestep, args->err * timestep, args->which,
      // 	       q, qerr, qtmp);

      qerr += args->err * timestep * sum;
      q = qtmp + args->alpha * timestep * sum;

      fa_q.write(dp, q);
      fa_qerr.write(dp, qerr);

      if(args->which != LAST_INTEGRATION) {
	qtmp = q + args->beta * timestep * sum;
	fa_qtmp.write(dp, qtmp);
      }
    }
#ifdef DEBUG_NONDET
    if(spec == 0) {
      DomainPoint dp = DomainPoint::from_point<3>(my_subgrid_bounds.lo);
      double sum = (fa_1.read(dp) +
		    fa_2.read(dp) +
		    fa_3.read(dp));
      printf(" rhsf(1,1,1,%d) -> %g (%a)\n",
	     reqs[0].instance_fields[0]+1, fa_q.read(dp), fa_q.read(dp));
    }
#endif
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern
void gpu_sum_3_integrate_fields(const int num_points,
				const double *in1_ptr,
				const double *in2_ptr,
				const double *in3_ptr,
				double *q_ptr,
				double *qerr_ptr,
				double *qtmp_ptr,
				double alpha, double beta, double err, int which);
#endif

/*static*/
void Sum3IntegrateTask::gpu_base_impl(S3DRank *rank, const Task *task,
				      const Rect<3> &subgrid_bounds,
				      const std::vector<RegionRequirement> &reqs,
				      const std::vector<PhysicalRegion> &regions,
				      Context ctx, HighLevelRuntime *runtime)
{
#ifdef USE_GPU_KERNELS
#ifndef NO_COMPUTE
  const SumIntegrateArgs *args = (const SumIntegrateArgs *)(task->args);

  Future f_timestep = task->futures[0];
  double timestep = f_timestep.get_reference<TimestepInfo>().cur_timestep;

  const int num_species = reqs[0].instance_fields.size();

  for (int spec = 0; spec < num_species; spec++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_q = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qerr = 
      regions[1].get_field_accessor(reqs[1].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qtmp = 
      regions[2].get_field_accessor(reqs[2].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_1 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[3*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_2 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[3*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_3 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[3*spec+2]).typeify<double>();

    Rect<3> subrect;
    ByteOffset out_offsets[3], offsets[3];

    double *q_ptr = fa_q.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
    if (!q_ptr || (subrect != subgrid_bounds) || 
	!offsets_are_dense<3, double>(subgrid_bounds, out_offsets)) assert(false);

    double *qerr_ptr = fa_qerr.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!qerr_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *qtmp_ptr = fa_qtmp.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!qtmp_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in1_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in2_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in3_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    size_t n_pts = subgrid_bounds.volume();

    gpu_sum_3_integrate_fields(n_pts, in1_ptr, in2_ptr, in3_ptr, q_ptr, qerr_ptr, qtmp_ptr,
			       args->alpha * timestep, args->beta * timestep, args->err * timestep,
			       args->which);
  }
#endif
#else
  assert(0);
#endif
}

Sum4Task::Sum4Task(Domain domain,
                   TaskArgument global_arg,
                   ArgumentMap arg_map,
                   Predicate pred,
                   bool must,
                   MapperID id,
                   MappingTagID tag)
 : IndexLauncher(Sum4Task::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | Sum4Task::MAPPER_TAG)
{
}

/*static*/ const char * const Sum4Task::TASK_NAME = "sum_4_task";

void Sum4Task::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing
}

/*static*/
void Sum4Task::sum_4_fields(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
		  IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr_in, LogicalRegion lr_out,
		  FieldID fid_1, FieldID fid_2, FieldID fid_3, FieldID fid_4, FieldID fid_out,
		  int stage, bool negate, int tag)
{
  Sum4Task sum_launcher(Domain::from_rect<3>(proc_grid_bounds),
                        TaskArgument(&negate, sizeof(negate)),
                        ArgumentMap());
  sum_launcher.tag |= tag;

  LogicalPartition lp_in_top = runtime->get_logical_partition(ctx, lr_in, ip_top);
  LogicalPartition lp_out_top = runtime->get_logical_partition(ctx, lr_out, ip_top);

  RegionRequirement rr_out(lp_out_top, 0, WRITE_DISCARD, EXCLUSIVE, lr_out);
  rr_out.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_out);

  RegionRequirement rr_in(lp_in_top, 0, READ_ONLY, EXCLUSIVE, lr_in);
  rr_in.add_field(fid_1);
  rr_in.add_field(fid_2);
  rr_in.add_field(fid_3);
  rr_in.add_field(fid_4);
  sum_launcher.add_region_requirement(rr_in);

  RankableTaskHelper::dispatch_task(sum_launcher, ctx, runtime, stage);
}

/*static*/
void Sum4Task::sum_4N_fields(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
		  IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr_in, LogicalRegion lr_out,
                  const std::vector<FieldID> &in1_fields, const std::vector<FieldID> &in2_fields,
                  const std::vector<FieldID> &in3_fields, const std::vector<FieldID> &in4_fields,
                  const std::vector<FieldID> &out_fields, int stage, bool negate, int tag)
{
  Sum4Task sum_launcher(Domain::from_rect<3>(proc_grid_bounds),
                        TaskArgument(&negate, sizeof(negate)),
                        ArgumentMap());
  sum_launcher.tag |= tag;

  LogicalPartition lp_in_top = runtime->get_logical_partition(ctx, lr_in, ip_top);
  LogicalPartition lp_out_top = runtime->get_logical_partition(ctx, lr_out, ip_top);

  RegionRequirement rr_out(lp_out_top, 0, WRITE_DISCARD, EXCLUSIVE, lr_out);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
    rr_out.add_field(out_fields[idx]);
  sum_launcher.add_region_requirement(rr_out);

  RegionRequirement rr_in(lp_in_top, 0, READ_ONLY, EXCLUSIVE, lr_in);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
  {
    rr_in.add_field(in1_fields[idx]);
    rr_in.add_field(in2_fields[idx]);
    rr_in.add_field(in3_fields[idx]);
    rr_in.add_field(in4_fields[idx]);
  }
  sum_launcher.add_region_requirement(rr_in);

  RankableTaskHelper::dispatch_task(sum_launcher, ctx, runtime, stage);
}

/*static*/
bool Sum4Task::dense_sum_4(const Rect<3> &subgrid_bounds, bool negate,
                           RegionAccessor<AccessorType::Generic,double> fa_out,
                           RegionAccessor<AccessorType::Generic,double> fa_1,
                           RegionAccessor<AccessorType::Generic,double> fa_2,
                           RegionAccessor<AccessorType::Generic,double> fa_3,
                           RegionAccessor<AccessorType::Generic,double> fa_4)
{
  Rect<3> subrect;
  ByteOffset out_offsets[3], offsets[3];

  double *out_ptr = fa_out.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if (!out_ptr || (subrect != subgrid_bounds) || 
      !offsets_are_dense<3, double>(subgrid_bounds, out_offsets)) return false;

  double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in1_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in2_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in3_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *in4_ptr = fa_4.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in4_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  size_t n_pts = subgrid_bounds.volume(); 

  // Large number to get the prefetchers warmed up, 
  // but small enough to still handle small problem sizes
#ifdef BULLDOZER
#define FULL_STRIP_SIZE 512
#else
#define FULL_STRIP_SIZE 1024
#endif
  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined(USE_AVX_KERNELS)
      if (aligned<32>(out_ptr) && aligned<32>(in1_ptr) && aligned<32>(in2_ptr) &&
          aligned<32>(in3_ptr) && aligned<32>(in4_ptr)) {
        if (negate) {
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d sum = _mm256_load_pd(in1_ptr+(i<<2));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in2_ptr+(i<<2)));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in3_ptr+(i<<2)));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in4_ptr+(i<<2)));
            // Fast negation
            sum = _mm256_xor_pd(sum,_mm256_set1_pd(-0.0));
            _mm256_stream_pd(out_ptr+(i<<2),sum);
          }
        } else {
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d sum = _mm256_load_pd(in1_ptr+(i<<2));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in2_ptr+(i<<2)));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in3_ptr+(i<<2)));
            sum = _mm256_add_pd(sum,_mm256_load_pd(in4_ptr+(i<<2)));
            _mm256_stream_pd(out_ptr+(i<<2),sum);
          }
        }
      } else {
        // Fall back to SSE if unaligned
        if (negate) {
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
            sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
            sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));
            sum = _mm_add_pd(sum,_mm_load_pd(in4_ptr+(i<<1)));
            // Fast negation
            sum = _mm_xor_pd(sum,_mm_set1_pd(-0.0));
            _mm_stream_pd(out_ptr+(i<<1),sum);
          }
        } else {
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
            sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
            sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));
            sum = _mm_add_pd(sum,_mm_load_pd(in4_ptr+(i<<1)));
            _mm_stream_pd(out_ptr+(i<<1),sum);
          }
        }
      }
#elif defined (USE_SSE_KERNELS)
      if (negate) {
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
          sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
          sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));
          sum = _mm_add_pd(sum,_mm_load_pd(in4_ptr+(i<<1)));
          // Fast negation
          sum = _mm_xor_pd(sum,_mm_set1_pd(-0.0));
          _mm_stream_pd(out_ptr+(i<<1),sum);
        }
      } else {
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
          sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
          sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));
          sum = _mm_add_pd(sum,_mm_load_pd(in4_ptr+(i<<1)));
          _mm_stream_pd(out_ptr+(i<<1),sum);
        }
      }
#else
      if (negate) {
        for (int i = 0; i < FULL_STRIP_SIZE; i++) {
          out_ptr[i] = -(in1_ptr[i] + in2_ptr[i] + in3_ptr[i] + in4_ptr[i]);
        }
      } else {
        for (int i = 0; i < FULL_STRIP_SIZE; i++) {
          out_ptr[i] = in1_ptr[i] + in2_ptr[i] + in3_ptr[i] + in4_ptr[i];
        }
      }
#endif
      n_pts -= FULL_STRIP_SIZE;
    } else {
      if (negate) {
        for (size_t i = 0; i < n_pts; i++) {
          out_ptr[i] = -(in1_ptr[i] + in2_ptr[i] + in3_ptr[i] + in4_ptr[i]);
        }
      } else {
        for (size_t i = 0; i < n_pts; i++) {
          out_ptr[i] = in1_ptr[i] + in2_ptr[i] + in3_ptr[i] + in4_ptr[i];
        }
      }
      n_pts = 0;
    }
    out_ptr += FULL_STRIP_SIZE;
    in1_ptr += FULL_STRIP_SIZE;
    in2_ptr += FULL_STRIP_SIZE;
    in3_ptr += FULL_STRIP_SIZE;
    in4_ptr += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
}

/*static*/
bool Sum4Task::fast_sum_4(RegionAccessor<AccessorType::Generic,double> fa_out,
                          RegionAccessor<AccessorType::Generic,double> fa_1,
                          RegionAccessor<AccessorType::Generic,double> fa_2,
                          RegionAccessor<AccessorType::Generic,double> fa_3,
                          RegionAccessor<AccessorType::Generic,double> fa_4,
                          Rect<3> subgrid_bounds,
                          bool negate)
{
  Rect<3> subrect;
  ByteOffset out_offsets[3];
  double *out_ptr = fa_out.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_ptr || (subrect != subgrid_bounds)) return false;

  ByteOffset in_offsets[3], offsets[3];
  const double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in1_ptr || (subrect != subgrid_bounds)) return false;

  const double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in2_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in3_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in4_ptr = fa_4.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in4_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  ByteOffset in_pos;
  for(int z = subgrid_bounds.lo[2]; z <= subgrid_bounds.hi[2]; z++) {
    double *out_ptr_y = out_ptr;
    ByteOffset in_pos_y = in_pos;
    for(int y = subgrid_bounds.lo[1]; y <= subgrid_bounds.hi[1]; y++) {
      double *out_ptr_x = out_ptr_y;
      ByteOffset in_pos_x = in_pos_y;
      for(int x = subgrid_bounds.lo[0]; x <= subgrid_bounds.hi[0]; x++) {
	double sum = (*(in1_ptr + in_pos_x) + 
		      *(in2_ptr + in_pos_x) + 
		      *(in3_ptr + in_pos_x) + 
		      *(in4_ptr + in_pos_x));
	if(negate)
	  sum = -sum;
	*out_ptr_x = sum;

	in_pos_x += in_offsets[0];
	out_ptr_x += out_offsets[0];
      }
      in_pos_y += in_offsets[1];
      out_ptr_y += out_offsets[1];
    }
    in_pos += in_offsets[2];
    out_ptr += out_offsets[2];
  }

  return true;
}

/*static*/
void Sum4Task::kernel_sum_4_normal(size_t n_pts, double *out, double *in1, 
                                   double *in2, double *in3, double *in4)
{
  //printf("dense sum_4\n");
  for(size_t i = 0; i < n_pts; i++)
    out[i] = (in1[i] + in2[i] + in3[i] + in4[i]);
}

/*static*/
void Sum4Task::kernel_sum_4_negate(size_t n_pts, double *out, double *in1, 
                                   double *in2, double *in3, double *in4)
{
  //printf("dense sum_4\n");
  for(size_t i = 0; i < n_pts; i++)
    out[i] = -((in1[i] + in2[i] + in3[i] + in4[i]));
}

/*static*/
void Sum4Task::cpu_base_impl(S3DRank *rank, const Task *task,
                             const Rect<3> &my_subgrid_bounds,
                             const std::vector<RegionRequirement> &reqs,
                             const std::vector<PhysicalRegion> &regions,
                             Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  bool negate = *(bool *)task->args;

  const int num_species = reqs[0].instance_fields.size();

  for (int spec = 0; spec < num_species; spec++)
  {
    // get whatever accessors you need here
    RegionAccessor<AccessorType::Generic,double> fa_out = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_1 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[4*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_2 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[4*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_3 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[4*spec+2]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_4 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[4*spec+3]).typeify<double>();

    if (dense_sum_4(my_subgrid_bounds, negate, fa_out, fa_1, fa_2, fa_3, fa_4))
      continue;

    if(negate) {
      if(attempt_dense_5d<3, kernel_sum_4_negate>(my_subgrid_bounds,
                                                  regions[0], reqs[0].instance_fields[spec],
                                                  regions[1], reqs[1].instance_fields[4*spec],
                                                  regions[1], reqs[1].instance_fields[4*spec+1],
                                                  regions[1], reqs[1].instance_fields[4*spec+2],
                                                  regions[1], reqs[1].instance_fields[4*spec+3]))
        continue;
    } else {
      if(attempt_dense_5d<3, kernel_sum_4_normal>(my_subgrid_bounds,
                                                  regions[0], reqs[0].instance_fields[spec],
                                                  regions[1], reqs[1].instance_fields[4*spec],
                                                  regions[1], reqs[1].instance_fields[4*spec+1],
                                                  regions[1], reqs[1].instance_fields[4*spec+2],
                                                  regions[1], reqs[1].instance_fields[4*spec+3]))
        continue;
    }

    if(fast_sum_4(fa_out, fa_1, fa_2, fa_3, fa_4, my_subgrid_bounds, negate)) {
      //printf("fast sum 4\n");
      continue;
    }

    for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double f1 = fa_1.read(dp);
      double f2 = fa_2.read(dp);
      double f3 = fa_3.read(dp);
      double f4 = fa_4.read(dp);

      double sum = f1 + f2 + f3 + f4;
      if(negate)
        sum = -sum;

      fa_out.write(dp, sum);
    } 
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern
void gpu_sum_4_fields(const bool negate,
                      const int num_points,
                      const double *in1_ptr,
                      const double *in2_ptr,
                      const double *in3_ptr,
                      const double *in4_ptr,
                      double *out_ptr);
#endif

/*static*/
void Sum4Task::gpu_base_impl(S3DRank *rank, const Task *task,
                             const Rect<3> &subgrid_bounds,
                             const std::vector<RegionRequirement> &reqs,
                             const std::vector<PhysicalRegion> &regions,
                             Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  bool negate = *(bool *)task->args;

  const int num_species = reqs[0].instance_fields.size();

  for (int spec = 0; spec < num_species; spec++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_out = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_1 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[4*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_2 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[4*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_3 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[4*spec+2]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_4 = 
      regions[1].get_field_accessor(reqs[1].instance_fields[4*spec+3]).typeify<double>();

    Rect<3> subrect;
    ByteOffset out_offsets[3], offsets[3];

    double *out_ptr = fa_out.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
    if (!out_ptr || (subrect != subgrid_bounds) || 
        !offsets_are_dense<3, double>(subgrid_bounds, out_offsets)) assert(false);

    double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in1_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in2_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in3_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in4_ptr = fa_4.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in4_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    size_t n_pts = subgrid_bounds.volume();

    gpu_sum_4_fields(negate, n_pts, in1_ptr, in2_ptr, in3_ptr, in4_ptr, out_ptr);
  }
#endif
#endif
}

Sum4IntegrateTask::Sum4IntegrateTask(Domain domain,
				     TaskArgument global_arg,
				     ArgumentMap arg_map,
				     Predicate pred,
				     bool must,
				     MapperID id,
				     MappingTagID tag)
 : IndexLauncher(Sum4IntegrateTask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | Sum4IntegrateTask::MAPPER_TAG)
{
}

/*static*/ const char * const Sum4IntegrateTask::TASK_NAME = "sum_4_integrate_task";

void Sum4IntegrateTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing
}

/*static*/
void Sum4IntegrateTask::sum_4_integrate_fields(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
					       IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr_in,
					       LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
					       FieldID fid_1, FieldID fid_2, FieldID fid_3, FieldID fid_4, FieldID fid_out,
					       double alpha, double beta, double err, 
                                               double small, Future f_timestep,
					       int which, int stage, int tag)
{
  SumIntegrateArgs args;
  args.alpha = alpha;
  args.beta = beta;
  args.err = err;
  args.small = small;
  args.which = which;
  args.has_small = true;

  Sum4IntegrateTask sum_launcher(Domain::from_rect<3>(proc_grid_bounds),
				 TaskArgument(&args, sizeof(args)),
				 ArgumentMap());
  sum_launcher.tag |= tag;

  LogicalPartition lp_in_top = runtime->get_logical_partition(ctx, lr_in, ip_top);
  LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  LogicalPartition lp_qerr_top = runtime->get_logical_partition(ctx, lr_qerr, ip_top);
  LogicalPartition lp_qtmp_top = runtime->get_logical_partition(ctx, lr_qtmp, ip_top);

  RegionRequirement rr_q(lp_q_top, 0, READ_WRITE, EXCLUSIVE, lr_q);
  rr_q.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_q);

  // qerr is only written on the first integation
  RegionRequirement rr_qerr(lp_qerr_top, 0, 
			    ((which == FIRST_INTEGRATION) ? WRITE_DISCARD : READ_WRITE),
			    EXCLUSIVE, lr_qerr);
  rr_qerr.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_qerr);

  // qtmp is only read on the last integration, only written on the first
  RegionRequirement rr_qtmp(lp_qtmp_top, 0, 
			    ((which == FIRST_INTEGRATION) ? WRITE_DISCARD : 
			     (which == LAST_INTEGRATION) ? READ_ONLY :
			     READ_WRITE),
			    EXCLUSIVE, lr_qtmp);
  rr_qtmp.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_qtmp);

  RegionRequirement rr_in(lp_in_top, 0, READ_ONLY, EXCLUSIVE, lr_in);
  rr_in.add_field(fid_1);
  rr_in.add_field(fid_2);
  rr_in.add_field(fid_3);
  rr_in.add_field(fid_4);
  sum_launcher.add_region_requirement(rr_in);

  sum_launcher.add_future(f_timestep);

  RankableTaskHelper::dispatch_task(sum_launcher, ctx, runtime, stage);
}

/*static*/
void Sum4IntegrateTask::sum_4N_integrate_fields(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
						IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr_in,
						LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
						const std::vector<FieldID> &in1_fields, const std::vector<FieldID> &in2_fields,
						const std::vector<FieldID> &in3_fields, const std::vector<FieldID> &in4_fields,
						const std::vector<FieldID> &out_fields,
						double alpha, double beta, double err, 
                                                double small, Future f_timestep,
						int which, int stage, int tag)
{
  SumIntegrateArgs args;
  args.alpha = alpha;
  args.beta = beta;
  args.err = err;
  args.small = small;
  args.which = which;
  args.has_small = true;

  Sum4IntegrateTask sum_launcher(Domain::from_rect<3>(proc_grid_bounds),
				 TaskArgument(&args, sizeof(args)),
				 ArgumentMap());
  sum_launcher.tag |= tag;

  LogicalPartition lp_in_top = runtime->get_logical_partition(ctx, lr_in, ip_top);
  LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  LogicalPartition lp_qerr_top = runtime->get_logical_partition(ctx, lr_qerr, ip_top);
  LogicalPartition lp_qtmp_top = runtime->get_logical_partition(ctx, lr_qtmp, ip_top);

  RegionRequirement rr_q(lp_q_top, 0, READ_WRITE, EXCLUSIVE, lr_q);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
    rr_q.add_field(out_fields[idx]);
  sum_launcher.add_region_requirement(rr_q);

  // qerr is only written on the first integation
  RegionRequirement rr_qerr(lp_qerr_top, 0, 
			    ((which == FIRST_INTEGRATION) ? WRITE_DISCARD : 
			     READ_WRITE),
			    EXCLUSIVE, lr_qerr);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
    rr_qerr.add_field(out_fields[idx]);
  sum_launcher.add_region_requirement(rr_qerr);

  // qtmp is only read on the last integration, only written on the first
  RegionRequirement rr_qtmp(lp_qtmp_top, 0, 
			    ((which == FIRST_INTEGRATION) ? WRITE_DISCARD : 
			     (which == LAST_INTEGRATION) ? READ_ONLY :
			     READ_WRITE),
			    EXCLUSIVE, lr_qtmp);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
    rr_qtmp.add_field(out_fields[idx]);
  sum_launcher.add_region_requirement(rr_qtmp);

  RegionRequirement rr_in(lp_in_top, 0, READ_ONLY, EXCLUSIVE, lr_in);
  for (unsigned idx = 0; idx < out_fields.size(); idx++)
  {
    rr_in.add_field(in1_fields[idx]);
    rr_in.add_field(in2_fields[idx]);
    rr_in.add_field(in3_fields[idx]);
    rr_in.add_field(in4_fields[idx]);
  }
  sum_launcher.add_region_requirement(rr_in);

  sum_launcher.add_future(f_timestep);

  RankableTaskHelper::dispatch_task(sum_launcher, ctx, runtime, stage);
}

/*static*/
bool Sum4IntegrateTask::dense_sum_4_integrate(const Rect<3> &subgrid_bounds,
					      RegionAccessor<AccessorType::Generic,double> fa_q,
					      RegionAccessor<AccessorType::Generic,double> fa_qerr,
					      RegionAccessor<AccessorType::Generic,double> fa_qtmp,
					      RegionAccessor<AccessorType::Generic,double> fa_1,
					      RegionAccessor<AccessorType::Generic,double> fa_2,
					      RegionAccessor<AccessorType::Generic,double> fa_3,
					      RegionAccessor<AccessorType::Generic,double> fa_4,
					      double alpha, double beta, double err, 
                                              double small, int which)
{
  Rect<3> subrect;
  ByteOffset out_offsets[3], offsets[3];

  double *q_ptr = fa_q.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if (!q_ptr || (subrect != subgrid_bounds) || 
      !offsets_are_dense<3, double>(subgrid_bounds, out_offsets)) return false;

  double *qerr_ptr = fa_qerr.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!qerr_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *qtmp_ptr = fa_qtmp.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!qtmp_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in1_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in2_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in3_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *in4_ptr = fa_4.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in4_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  size_t n_pts = subgrid_bounds.volume(); 

  // Large number to get the prefetchers warmed up, 
  // but small enough to still handle small problem sizes
#ifdef BULLDOZER
#define FULL_STRIP_SIZE 128 
#else
#define FULL_STRIP_SIZE 1024
#endif
#if defined(USE_AVX_KERNELS)
  __m256d avx_sum[FULL_STRIP_SIZE/4];
#endif
#if defined(USE_AVX_KERNELS) || defined(USE_SSE_KERNELS)
  __m128d sse_sum[FULL_STRIP_SIZE/2];
#endif

  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined (USE_AVX_KERNELS)
      if (aligned<32>(q_ptr) && aligned<32>(qerr_ptr) && aligned<32>(qtmp_ptr) &&
	  aligned<32>(in1_ptr) && aligned<32>(in2_ptr) && aligned<32>(in3_ptr) && aligned<32>(in4_ptr))
      {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          avx_sum[i] = _mm256_load_pd(in1_ptr+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          avx_sum[i] = _mm256_add_pd(avx_sum[i],_mm256_load_pd(in2_ptr+(i<<2)));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          avx_sum[i] = _mm256_add_pd(avx_sum[i],_mm256_load_pd(in3_ptr+(i<<2)));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          avx_sum[i] = _mm256_add_pd(avx_sum[i],_mm256_load_pd(in4_ptr+(i<<2)));
        if (which == FIRST_INTEGRATION) {
          // Do independent work on qerr
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d qerr = _mm256_add_pd(_mm256_set1_pd(0.0), 
                                         _mm256_mul_pd(_mm256_set1_pd(err), avx_sum[i]));
            _mm256_stream_pd(qerr_ptr+(i<<2),qerr);
          }
          // Now do the q and qtmp computation
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d q = _mm256_load_pd(q_ptr+(i<<2));
            q = _mm256_add_pd(q, _mm256_mul_pd(_mm256_set1_pd(alpha), avx_sum[i]));
            // Store since line is already in cache
            _mm256_store_pd(q_ptr+(i<<2), q);
            __m256d qtmp = _mm256_add_pd(q, _mm256_mul_pd(
                              _mm256_set1_pd(beta), avx_sum[i]));
            _mm256_stream_pd(qtmp_ptr+(i<<2), qtmp);
          }
        } else if (which != LAST_INTEGRATION) { 
          // Do independent work on q err
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d qerr = _mm256_load_pd(qerr_ptr+(i<<2));
            qerr = _mm256_add_pd(qerr, _mm256_mul_pd(_mm256_set1_pd(err), avx_sum[i]));
            // Store, since line is already in cache
            _mm256_store_pd(qerr_ptr+(i<<2), qerr);
          }
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d qtmp = _mm256_load_pd(qtmp_ptr+(i<<2));
            __m256d q = _mm256_add_pd(qtmp, _mm256_mul_pd(
                                      _mm256_set1_pd(alpha), avx_sum[i]));
            _mm256_stream_pd(q_ptr+(i<<2), q);
            qtmp = _mm256_add_pd(q, _mm256_mul_pd(_mm256_set1_pd(beta), avx_sum[i]));
            // Store since this is already in cache
            _mm256_store_pd(qtmp_ptr+(i<<2), qtmp);
          }
        } else { // LAST_INTEGRATION
          // Do qerr first
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d qerr = _mm256_load_pd(qerr_ptr+(i<<2));
            qerr = _mm256_add_pd(qerr, _mm256_mul_pd(_mm256_set1_pd(err), avx_sum[i]));
            // Store since this is already in cache
            _mm256_store_pd(qerr_ptr+(i<<2),qerr);
          }
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
	    __m256d qtmp = _mm256_load_pd(qtmp_ptr+(i<<2));
            __m256d q = _mm256_add_pd(qtmp, _mm256_mul_pd(
                                      _mm256_set1_pd(alpha), avx_sum[i]));
            // Make sure all the q values are positive and greater than small
            // on the way back out
            __m256d smalld = _mm256_set1_pd(small);
            __m256d mask = _mm256_cmp_pd(q,smalld,_CMP_GT_OQ);
            q = _mm256_add_pd(_mm256_and_pd(mask,q),_mm256_andnot_pd(mask,smalld));
            _mm256_stream_pd(q_ptr+(i<<2),q);
          }
        }
#if 0
	for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
	  __m256d sum = _mm256_load_pd(in1_ptr+(i<<2));
	  sum = _mm256_add_pd(sum,_mm256_load_pd(in2_ptr+(i<<2)));
	  sum = _mm256_add_pd(sum,_mm256_load_pd(in3_ptr+(i<<2)));
	  sum = _mm256_add_pd(sum,_mm256_load_pd(in4_ptr+(i<<2)));

	  __m256d q = _mm256_load_pd(q_ptr+(i<<2));
	  __m256d qerr, qtmp;
	  if(which == FIRST_INTEGRATION) {
	    qerr = _mm256_set1_pd(0.0);
	    qtmp = q;
	  } else {
	    qerr = _mm256_load_pd(qerr_ptr+(i<<2));
	    qtmp = _mm256_load_pd(qtmp_ptr+(i<<2));
	  }

	  qerr = _mm256_add_pd(qerr, _mm256_mul_pd(_mm256_set1_pd(err), sum));
	  q = _mm256_add_pd(qtmp, _mm256_mul_pd(_mm256_set1_pd(alpha), sum));
	  
	  _mm256_stream_pd(qerr_ptr+(i<<2),qerr);
	  _mm256_stream_pd(q_ptr+(i<<2),q);
	  if(which != LAST_INTEGRATION) {
	    qtmp = _mm256_add_pd(q, _mm256_mul_pd(_mm256_set1_pd(beta), sum));
	    _mm256_stream_pd(qtmp_ptr+(i<<2),qtmp);
	  }
        }
#endif
      } else
#endif
#if defined(USE_AVX_KERNELS) || defined(USE_SSE_KERNELS)
      if (aligned<16>(q_ptr) && aligned<16>(qerr_ptr) && aligned<16>(qtmp_ptr) &&
	  aligned<16>(in1_ptr) && aligned<16>(in2_ptr) && aligned<16>(in3_ptr) && aligned<16>(in4_ptr))
      {
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++)
          sse_sum[i] = _mm_load_pd(in1_ptr+(i<<1));
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++)
          sse_sum[i] = _mm_add_pd(sse_sum[i], _mm_load_pd(in2_ptr+(i<<1)));
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++)
          sse_sum[i] = _mm_add_pd(sse_sum[i], _mm_load_pd(in3_ptr+(i<<1)));
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++)
          sse_sum[i] = _mm_add_pd(sse_sum[i], _mm_load_pd(in4_ptr+(i<<1)));
        if (which == FIRST_INTEGRATION) {
          // Do independent work on qerr
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d qerr = _mm_add_pd(_mm_set1_pd(0.0), 
                                       _mm_mul_pd(_mm_set1_pd(err), sse_sum[i]));
            _mm_stream_pd(qerr_ptr+(i<<1),qerr);
          }
          // Now do the q and qtmp computation
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d q = _mm_load_pd(q_ptr+(i<<1));
            q = _mm_add_pd(q, _mm_mul_pd(_mm_set1_pd(alpha), sse_sum[i]));
            // Store since line is already in cache
            _mm_store_pd(q_ptr+(i<<1), q);
            __m128d qtmp = _mm_add_pd(q, _mm_mul_pd(
                              _mm_set1_pd(beta), sse_sum[i]));
            _mm_stream_pd(qtmp_ptr+(i<<1), qtmp);
          }
        } else if (which != LAST_INTEGRATION) {
          // Do independent work on q err
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d qerr = _mm_load_pd(qerr_ptr+(i<<1));
            qerr = _mm_add_pd(qerr, _mm_mul_pd(_mm_set1_pd(err), sse_sum[i]));
            // Store, since line is already in cache
            _mm_store_pd(qerr_ptr+(i<<1), qerr);
          }
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d qtmp = _mm_load_pd(qtmp_ptr+(i<<1));
            __m128d q = _mm_add_pd(qtmp, _mm_mul_pd(
                                      _mm_set1_pd(alpha), sse_sum[i]));
            _mm_stream_pd(q_ptr+(i<<1), q);
            qtmp = _mm_add_pd(q, _mm_mul_pd(_mm_set1_pd(beta), sse_sum[i]));
            // Store since this is already in cache
            _mm_store_pd(qtmp_ptr+(i<<1), qtmp);
          }
        } else { // LAST_INTEGRATION
          // Do qerr first
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
            __m128d qerr = _mm_load_pd(qerr_ptr+(i<<1));
            qerr = _mm_add_pd(qerr, _mm_mul_pd(_mm_set1_pd(err), sse_sum[i]));
            // Store since this is already in cache
            _mm_store_pd(qerr_ptr+(i<<1),qerr);
          }
          for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
	    __m128d qtmp = _mm_load_pd(qtmp_ptr+(i<<1));
            __m128d q = _mm_add_pd(qtmp, _mm_mul_pd(
                                      _mm_set1_pd(alpha), sse_sum[i]));
            // Make sure all the q values are positive and greater than small
            // on the way back out
            __m128d smalld = _mm_set1_pd(small);
            __m128d mask = _mm_cmpgt_pd(q,smalld);
            q = _mm_add_pd(_mm_and_pd(mask,q),_mm_andnot_pd(mask,smalld));
            _mm_stream_pd(q_ptr+(i<<1),q);
          }
        }
#if 0
	for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
	  __m128d sum = _mm_load_pd(in1_ptr+(i<<1));
	  sum = _mm_add_pd(sum,_mm_load_pd(in2_ptr+(i<<1)));
	  sum = _mm_add_pd(sum,_mm_load_pd(in3_ptr+(i<<1)));
	  sum = _mm_add_pd(sum,_mm_load_pd(in4_ptr+(i<<1)));

	  __m128d q = _mm_load_pd(q_ptr+(i<<1));
	  __m128d qerr, qtmp;
	  if(which == FIRST_INTEGRATION) {
	    qerr = _mm_set1_pd(0.0);
	    qtmp = q;
	  } else {
	    qerr = _mm_load_pd(qerr_ptr+(i<<1));
	    qtmp = _mm_load_pd(qtmp_ptr+(i<<1));
	  }

	  qerr = _mm_add_pd(qerr, _mm_mul_pd(_mm_set1_pd(err), sum));
	  q = _mm_add_pd(qtmp, _mm_mul_pd(_mm_set1_pd(alpha), sum));
	  
	  _mm_stream_pd(qerr_ptr+(i<<1),qerr);
	  _mm_stream_pd(q_ptr+(i<<1),q);
	  if(which != LAST_INTEGRATION) {
	    qtmp = _mm_add_pd(q, _mm_mul_pd(_mm_set1_pd(beta), sum));
	    _mm_stream_pd(qtmp_ptr+(i<<1),qtmp);
	  }
        }
#endif
      } else
#endif
      {
	// scalar fallback
        if (which == FIRST_INTEGRATION) {
          for (int i = 0; i < FULL_STRIP_SIZE; i++) {
            double sum = in1_ptr[i] + in2_ptr[i] + in3_ptr[i] + in4_ptr[i];

            double q = q_ptr[i];
            double qerr = 0;
            double qtmp = q;

            qerr += err * sum;
            q = qtmp + alpha * sum;
            
            qerr_ptr[i] = qerr;
            q_ptr[i] = q;

            qtmp = q + beta * sum;
            qtmp_ptr[i] = qtmp;
          }
        } else if (which != LAST_INTEGRATION) {
          for (int i = 0; i < FULL_STRIP_SIZE; i++) {
            double sum = in1_ptr[i] + in2_ptr[i] + in3_ptr[i] + in4_ptr[i];

            double q = q_ptr[i];
            double qerr = qerr_ptr[i];
            double qtmp = qtmp_ptr[i];

            qerr += err * sum;
            q = qtmp + alpha * sum;
            
            qerr_ptr[i] = qerr;
            q_ptr[i] = q;

            qtmp = q + beta * sum;
            qtmp_ptr[i] = qtmp;
          }
        } else { // LAST_INTEGRATION
          for (int i = 0; i < FULL_STRIP_SIZE; i++) {
            double sum = in1_ptr[i] + in2_ptr[i] + in3_ptr[i] + in4_ptr[i];

            double q = q_ptr[i];
            double qerr = qerr_ptr[i];
            double qtmp = qtmp_ptr[i];

            qerr += err * sum;
            q = qtmp + alpha * sum;
            
            qerr_ptr[i] = qerr;
            if (q < small)
              q = small;
            q_ptr[i] = q;
          }
        }
#if 0
        for (int i = 0; i < FULL_STRIP_SIZE; i++) {
	  double sum = in1_ptr[i] + in2_ptr[i] + in3_ptr[i] + in4_ptr[i];

	  double q = q_ptr[i];
	  double qerr, qtmp;
	  if(which == FIRST_INTEGRATION) {
	    qerr = 0;
	    qtmp = q;
	  } else {
	    qerr = qerr_ptr[i];
	    qtmp = qtmp_ptr[i];
	  }

	  qerr += err * sum;
	  q = qtmp + alpha * sum;
	  
	  qerr_ptr[i] = qerr;
	  q_ptr[i] = q;

	  if(which != LAST_INTEGRATION) {
	    qtmp = q + beta * sum;
	    qtmp_ptr[i] = qtmp;
	  }
	}
#endif
      }
      n_pts -= FULL_STRIP_SIZE;
    } else {
      {
	// scalar fallback
        for (size_t i = 0; i < n_pts; i++) {
	  double sum = in1_ptr[i] + in2_ptr[i] + in3_ptr[i] + in4_ptr[i];

	  double q = q_ptr[i];
	  double qerr, qtmp;
	  if(which == FIRST_INTEGRATION) {
	    qerr = 0;
	    qtmp = q;
	  } else {
	    qerr = qerr_ptr[i];
	    qtmp = qtmp_ptr[i];
	  }

	  qerr += err * sum;
	  q = qtmp + alpha * sum;
	  
	  qerr_ptr[i] = qerr;

	  if(which != LAST_INTEGRATION) {
	    qtmp = q + beta * sum;
	    qtmp_ptr[i] = qtmp;
	  }
          else if (q < small)
            q = small;

	  q_ptr[i] = q;
	}
      }
      n_pts = 0;
    }
    q_ptr += FULL_STRIP_SIZE;
    qerr_ptr += FULL_STRIP_SIZE;
    qtmp_ptr += FULL_STRIP_SIZE;
    in1_ptr += FULL_STRIP_SIZE;
    in2_ptr += FULL_STRIP_SIZE;
    in3_ptr += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
}

/*static*/
bool Sum4IntegrateTask::fast_sum_4_integrate(RegionAccessor<AccessorType::Generic,double> fa_q,
					     RegionAccessor<AccessorType::Generic,double> fa_qerr,
					     RegionAccessor<AccessorType::Generic,double> fa_qtmp,
					     RegionAccessor<AccessorType::Generic,double> fa_1,
					     RegionAccessor<AccessorType::Generic,double> fa_2,
					     RegionAccessor<AccessorType::Generic,double> fa_3,
					     RegionAccessor<AccessorType::Generic,double> fa_4,
					     Rect<3> subgrid_bounds,
					     double alpha, double beta, double err, 
                                             double small, int which)
{
  Rect<3> subrect;
  ByteOffset q_offsets[3];
  double *q_ptr = fa_q.raw_rect_ptr<3>(subgrid_bounds, subrect, q_offsets);
  if(!q_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset qerr_offsets[3];
  double *qerr_ptr = fa_qerr.raw_rect_ptr<3>(subgrid_bounds, subrect, qerr_offsets);
  if(!qerr_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset qtmp_offsets[3];
  double *qtmp_ptr = fa_qtmp.raw_rect_ptr<3>(subgrid_bounds, subrect, qtmp_offsets);
  if(!qtmp_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in1_offsets[3];
  double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, in1_offsets);
  if(!in1_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in2_offsets[3];
  double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, in2_offsets);
  if(!in2_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in3_offsets[3];
  double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, in3_offsets);
  if(!in3_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in4_offsets[3];
  double *in4_ptr = fa_4.raw_rect_ptr<3>(subgrid_bounds, subrect, in4_offsets);
  if(!in4_ptr) return false;
  assert(subrect == subgrid_bounds);

  for(GenericPointInRectIterator<3> pir(Rect<3>(Point<3>::ZEROES(), subgrid_bounds.hi - subgrid_bounds.lo)); pir; pir++) {
    double *qp = q_ptr + pir.p[0] * q_offsets[0] + pir.p[1] * q_offsets[1] + pir.p[2] * q_offsets[2];
    double *qerrp = qerr_ptr + pir.p[0] * qerr_offsets[0] + pir.p[1] * qerr_offsets[1] + pir.p[2] * qerr_offsets[2];
    double *qtmpp = qtmp_ptr + pir.p[0] * qtmp_offsets[0] + pir.p[1] * qtmp_offsets[1] + pir.p[2] * qtmp_offsets[2];
    const double *in1p = in1_ptr + pir.p[0] * in1_offsets[0] + pir.p[1] * in1_offsets[1] + pir.p[2] * in1_offsets[2];
    const double *in2p = in2_ptr + pir.p[0] * in2_offsets[0] + pir.p[1] * in2_offsets[1] + pir.p[2] * in2_offsets[2];
    const double *in3p = in3_ptr + pir.p[0] * in3_offsets[0] + pir.p[1] * in3_offsets[1] + pir.p[2] * in3_offsets[2];
    const double *in4p = in4_ptr + pir.p[0] * in4_offsets[0] + pir.p[1] * in4_offsets[1] + pir.p[2] * in4_offsets[2];
    double sum = *in1p + *in2p + *in3p + *in4p;

    double q = *qp;
    double qerr, qtmp;
    if(which == FIRST_INTEGRATION) {
      qerr = 0;
      qtmp = q;
    } else {
      qerr = *qerrp;
      qtmp = *qtmpp;
    }

    qerr += err * sum;
    q = qtmp + alpha * sum;

    *qerrp = qerr;

    if(which != LAST_INTEGRATION) {
      qtmp = q + beta * sum;
      *qtmpp = qtmp;
    }
    else if (q < small) // keep q values positive on last integration
      q = small;

    *qp = q;
  }

  return true;
}

#if 0
/*static*/
void Sum4IntegrateTask::kernel_sum_4_normal(size_t n_pts, double *out, double *in1, 
                                   double *in2, double *in3, double *in4)
{
  //printf("dense sum_4\n");
  for(size_t i = 0; i < n_pts; i++)
    out[i] = (in1[i] + in2[i] + in3[i] + in4[i]);
}
#endif

/*static*/
void Sum4IntegrateTask::cpu_base_impl(S3DRank *rank, const Task *task,
				      const Rect<3> &my_subgrid_bounds,
				      const std::vector<RegionRequirement> &reqs,
				      const std::vector<PhysicalRegion> &regions,
				      Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  const SumIntegrateArgs *args = (const SumIntegrateArgs *)(task->args);

  Future f_timestep = task->futures[0];
  double timestep = f_timestep.get_reference<TimestepInfo>().cur_timestep;

  const int num_species = reqs[0].instance_fields.size();

  for (int spec = 0; spec < num_species; spec++)
  {
    // get whatever accessors you need here
    RegionAccessor<AccessorType::Generic,double> fa_q = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qerr = 
      regions[1].get_field_accessor(reqs[1].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qtmp = 
      regions[2].get_field_accessor(reqs[2].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_1 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[4*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_2 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[4*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_3 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[4*spec+2]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_4 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[4*spec+3]).typeify<double>();

#ifdef DEBUG_NONDET
    if(spec == 0) {
      DomainPoint dp = DomainPoint::from_point<3>(my_subgrid_bounds.lo);
      double sum = (fa_1.read(dp) +
		    fa_2.read(dp) +
		    fa_3.read(dp) +
		    fa_4.read(dp));
      printf(" rhsf(1,1,1,%d) = %g, q = %g\n",
	     reqs[0].instance_fields[0]+1, sum, fa_q.read(dp));
    }
#endif

    if (dense_sum_4_integrate(my_subgrid_bounds, fa_q, fa_qerr, fa_qtmp, fa_1, fa_2, fa_3, fa_4,
			      args->alpha * timestep, args->beta * timestep, args->err * timestep,
			      args->small, args->which))
      continue;

#if 0
    if(negate) {
      if(attempt_dense_5d<3, kernel_sum_4_negate>(my_subgrid_bounds,
                                                  regions[0], reqs[0].instance_fields[spec],
                                                  regions[1], reqs[1].instance_fields[4*spec],
                                                  regions[1], reqs[1].instance_fields[4*spec+1],
                                                  regions[1], reqs[1].instance_fields[4*spec+2],
                                                  regions[1], reqs[1].instance_fields[4*spec+3]))
        continue;
    } else {
      if(attempt_dense_5d<3, kernel_sum_4_normal>(my_subgrid_bounds,
                                                  regions[0], reqs[0].instance_fields[spec],
                                                  regions[1], reqs[1].instance_fields[4*spec],
                                                  regions[1], reqs[1].instance_fields[4*spec+1],
                                                  regions[1], reqs[1].instance_fields[4*spec+2],
                                                  regions[1], reqs[1].instance_fields[4*spec+3]))
        continue;
    }
#endif

    printf("falling back to fast sum_4_integrate\n");

    if(fast_sum_4_integrate(fa_q, fa_qerr, fa_qtmp, fa_1, fa_2, fa_3, fa_4, my_subgrid_bounds,
			    args->alpha * timestep, args->beta * timestep, args->err * timestep,
			    args->small, args->which)) {
      //printf("fast sum 4\n");
      continue;
    }

    printf("falling back to slow sum_4_integrate\n");

    for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double f1 = fa_1.read(dp);
      double f2 = fa_2.read(dp);
      double f3 = fa_3.read(dp);
      double f4 = fa_4.read(dp);

      double q = fa_q.read(dp);
      double qerr = ((args->which == FIRST_INTEGRATION) ? 0 : fa_qerr.read(dp));
      double qtmp = ((args->which == FIRST_INTEGRATION) ? q : fa_qtmp.read(dp));

      double sum = f1 + f2 + f3 + f4;

      if(0 && pir.p == my_subgrid_bounds.lo)
	printf("s4i: %d %g / %g %g %g %d / %g %g %g\n",
	       reqs[0].instance_fields[spec], sum, args->alpha * timestep, args->beta * timestep, args->err * timestep, args->which,
	       q, qerr, qtmp);

      qerr += args->err * timestep * sum;
      q = qtmp + args->alpha * timestep * sum; 

      if(args->which != LAST_INTEGRATION) {
	qtmp = q + args->beta * timestep * sum;
	fa_qtmp.write(dp, qtmp);
      }
      else if (q < args->small)
        q = args->small; // keep all the q values positive on the last integration

      fa_q.write(dp, q);
      fa_qerr.write(dp, qerr);
    } 
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern
void gpu_sum_4_integrate_fields(const int num_points,
				const double *in1_ptr,
				const double *in2_ptr,
				const double *in3_ptr,
				const double *in4_ptr,
				double *q_ptr,
				double *qerr_ptr,
				double *qtmp_ptr,
				double alpha, double beta, double err, 
                                double small, int which);
#endif

/*static*/
void Sum4IntegrateTask::gpu_base_impl(S3DRank *rank, const Task *task,
                             const Rect<3> &subgrid_bounds,
                             const std::vector<RegionRequirement> &reqs,
                             const std::vector<PhysicalRegion> &regions,
                             Context ctx, HighLevelRuntime *runtime)
{
#ifdef USE_GPU_KERNELS
#ifndef NO_COMPUTE
  const SumIntegrateArgs *args = (const SumIntegrateArgs *)(task->args);

  Future f_timestep = task->futures[0];
  double timestep = f_timestep.get_reference<TimestepInfo>().cur_timestep;

  const int num_species = reqs[0].instance_fields.size();

  for (int spec = 0; spec < num_species; spec++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_q = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qerr = 
      regions[1].get_field_accessor(reqs[1].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qtmp = 
      regions[2].get_field_accessor(reqs[2].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_1 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[4*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_2 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[4*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_3 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[4*spec+2]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_4 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[4*spec+3]).typeify<double>();

    Rect<3> subrect;
    ByteOffset out_offsets[3], offsets[3];

    double *q_ptr = fa_q.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
    if (!q_ptr || (subrect != subgrid_bounds) || 
	!offsets_are_dense<3, double>(subgrid_bounds, out_offsets)) assert(false);

    double *qerr_ptr = fa_qerr.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!qerr_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *qtmp_ptr = fa_qtmp.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!qtmp_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in1_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in2_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in3_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in4_ptr = fa_4.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in4_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    size_t n_pts = subgrid_bounds.volume();

    gpu_sum_4_integrate_fields(n_pts, in1_ptr, in2_ptr, in3_ptr, in4_ptr, q_ptr, qerr_ptr, qtmp_ptr,
			       args->alpha * timestep, args->beta * timestep, args->err * timestep,
			       args->small, args->which);
  }
#endif
#else
  assert(0);
#endif
}

CalcRHSVelTask::CalcRHSVelTask(Domain domain,
                               TaskArgument global_arg,
                               ArgumentMap arg_map,
                               Predicate pred,
                               bool must,
                               MapperID id,
                               MappingTagID tag)
 : IndexLauncher(CalcRHSVelTask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | CalcRHSVelTask::MAPPER_TAG)
{
}

/*static*/ const char * const CalcRHSVelTask::TASK_NAME = "calc_rhs_vel_task";

void CalcRHSVelTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing
}

/*static*/
void CalcRHSVelTask::calc_rhs_vel_deriv_in(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
			   IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr_q, LogicalRegion lr_int,
			   FieldID fid_rho_vel, FieldID fid_vel, FieldID fid_tau, FieldID fid_pressure,
			   FieldID fid_out, int stage)
{
  CalcRHSVelTask deriv_launcher(Domain::from_rect<3>(proc_grid_bounds),
                                TaskArgument(0, 0),
                                ArgumentMap());

  LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  LogicalPartition lp_int_top = runtime->get_logical_partition(ctx, lr_int, ip_top);

  RegionRequirement rr_out(lp_int_top, 0, WRITE_ONLY, EXCLUSIVE, lr_int);
  rr_out.add_field(fid_out);
  deriv_launcher.add_region_requirement(rr_out);

  // rho_vel comes from q
  RegionRequirement rr_q(lp_q_top, 0, READ_ONLY, EXCLUSIVE, lr_q);
  rr_q.add_field(fid_rho_vel);
  deriv_launcher.add_region_requirement(rr_q);

  // vel, tau are always inputs from int
  RegionRequirement rr_in(lp_int_top, 0, READ_ONLY, EXCLUSIVE, lr_int);
  rr_in.add_field(fid_vel);
  rr_in.add_field(fid_tau);

  // pressure is optional
  if(fid_pressure != FID_NONE) {
    rr_in.add_field(fid_pressure);
  }
  deriv_launcher.add_region_requirement(rr_in);

  RankableTaskHelper::dispatch_task(deriv_launcher, ctx, runtime, stage);
}

/*static*/
void CalcRHSVelTask::kernel_rhs_vel_in(size_t n_pts, double *out, double *rho_vel, 
                                       double *vel, double *tau)
{
  for(size_t i = 0; i < n_pts; i++)
    out[i] = tau[i] - rho_vel[i] * vel[i];
}

/*static*/
void CalcRHSVelTask::kernel_rhs_vel_in_pressure(size_t n_pts, double *out, double *rho_vel, 
                                                double *vel, double *tau, double *pressure)
{
  for(size_t i = 0; i < n_pts; i++)
    out[i] = tau[i] - rho_vel[i] * vel[i] - pressure[i];
}

/*static*/
void CalcRHSVelTask::cpu_base_impl(S3DRank *rank, const Task *task,
                                   const Rect<3> &my_subgrid_bounds,
                                   const std::vector<RegionRequirement> &reqs,
                                   const std::vector<PhysicalRegion> &regions,
                                   Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  if(reqs[2].instance_fields.size() > 2) {
    // have a pressure term
    if(attempt_dense_5d<3, kernel_rhs_vel_in_pressure>(my_subgrid_bounds,
						       regions[0], reqs[0].instance_fields[0],
						       regions[1], reqs[1].instance_fields[0],
						       regions[2], reqs[2].instance_fields[0],
						       regions[2], reqs[2].instance_fields[1],
						       regions[2], reqs[2].instance_fields[2]))
      return;
  } else {
    // no pressure term
    if(attempt_dense_4d<3, kernel_rhs_vel_in>(my_subgrid_bounds,
					      regions[0], reqs[0].instance_fields[0],
					      regions[1], reqs[1].instance_fields[0],
					      regions[2], reqs[2].instance_fields[0],
					      regions[2], reqs[2].instance_fields[1]))
      return;
  }

  // get whatever accessors you need here
  RegionAccessor<AccessorType::Generic,double> fa_out = 
    regions[0].get_field_accessor(reqs[0].instance_fields[0]).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_rho_vel = 
    regions[1].get_field_accessor(reqs[1].instance_fields[0]).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_vel = 
    regions[2].get_field_accessor(reqs[2].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau = 
    regions[2].get_field_accessor(reqs[2].instance_fields[1]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_pressure;
  bool have_pressure = reqs[2].instance_fields.size() > 2;
  if(have_pressure)
    fa_pressure = regions[2].get_field_accessor(reqs[2].instance_fields[2]).typeify<double>();

  for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double rho_vel = fa_rho_vel.read(dp);
    double vel = fa_vel.read(dp);
    double tau = fa_tau.read(dp);

    double deriv_in = tau - rho_vel * vel;

    if(have_pressure) {
      double pressure = fa_pressure.read(dp);
      deriv_in -= pressure;
    }

    fa_out.write(dp, deriv_in);
  }
#endif
}

