
#include "rhsf.h"
#include "impose_pressure.h"
#include "rank_helper.h"
#include "legion.h"
#include "arrays.h"
#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
#include <x86intrin.h>
#endif
#include <cmath>

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
static inline __m128d _mm_stream_load_pd(const double *ptr) 
{
#ifdef __SSE4_1__
  return _mm_castsi128_pd(_mm_stream_load_si128(reinterpret_cast<__m128i *>(const_cast<double *>(ptr))));
#else
  return *(const __m128d *)ptr;
#endif
}
#endif

#ifdef USE_AVX_KERNELS
template<unsigned BOUNDARY>
static inline bool aligned(const void *ptr)
{
  return ((((unsigned long)ptr) & (BOUNDARY-1)) == 0);
}
#endif

CompressionSourceTerm::CompressionSourceTerm(double _geomfac2, double _polyn,
					     double _rpm, double _ttdc, double _toff, double _tp, double _t90,
					     double _ptdc, double _pc, bool _enforce)
  : geomfac2(_geomfac2), polyn(_polyn)
  , rpm(_rpm), ttdc(_ttdc), toff(_toff), tp(_tp), t90(_t90)
  , ptdc(_ptdc), pc(_pc), enforce(_enforce)
{}

CompressionSourceTerm::~CompressionSourceTerm(void)
{
}

double CompressionSourceTerm::calc_target_pressure(double t) const
{
  double time_ref = L_REF / A_REF;

  double tcrank_nd = (60.0 / rpm) / time_ref;
  double ttdc_nd = tcrank_nd * ttdc / 360.0;
  double toff_nd = tcrank_nd * toff / 360.0;
  double ptdc_nd = ptdc / (P_REF * 0.1);
  double pcrise_nd = (pc - ptdc) / (P_REF * 0.1);
  double tp_nd = tcrank_nd * tp / 360.0;
  double t90_nd = tcrank_nd * t90 / 360.0 * 3.3; // factor of 3.3 gets divided right back out below

  double pi = 4.0 * atan(1.0);
  double mu = t90_nd / 3.3;
  double tm = (t - ttdc_nd - toff_nd) / tcrank_nd;
  double tr = (t - ttdc_nd - tp_nd) / mu / sqrt(2.0);

  double timeerf = erf(tr);

  double Pt_phi = 0.5 * ((ptdc_nd) * (1.0 - timeerf) + (ptdc_nd + pcrise_nd) * (1.0 + timeerf));
  double Pt_geom = pow(1.0 + geomfac2*pi*pi*tm*tm, -polyn);

  double Pt_model = Pt_phi * Pt_geom;

  return Pt_model;
}

double CompressionSourceTerm::calc_target_dpdt(double t) const
{
  double time_ref = L_REF / A_REF;

  double tcrank_nd = (60.0 / rpm) / time_ref;
  double ttdc_nd = tcrank_nd * ttdc / 360.0;
  double toff_nd = tcrank_nd * toff / 360.0;
  double ptdc_nd = ptdc / (P_REF * 0.1);
  double pcrise_nd = (pc - ptdc) / (P_REF * 0.1);
  double tp_nd = tcrank_nd * tp / 360.0;
  double t90_nd = tcrank_nd * t90 / 360.0 * 3.3; // factor of 3.3 gets divided right back out below

  double pi = 4.0 * atan(1.0);
  double mu = t90_nd / 3.3;
  double tm = (t - ttdc_nd - toff_nd) / tcrank_nd;
  double tr = (t - ttdc_nd - tp_nd) / mu / sqrt(2.0);

  double timeerf = erf(tr);

  double Pt_phi = 0.5 * ((ptdc_nd) * (1.0 - timeerf) + (ptdc_nd + pcrise_nd) * (1.0 + timeerf));
  double Pt_geom = pow(1.0 + geomfac2*pi*pi*tm*tm, -polyn);

  double dPtdt_phi = (pcrise_nd / mu / sqrt(2.0 * pi)) * exp(-tr*tr);
  double dPtdt_geom = -2.0 * (polyn*geomfac2*pi*pi*tm/tcrank_nd) * pow(1.0 + geomfac2*pi*pi*tm*tm, -polyn-1.0);

  double dPtdt_model = Pt_phi * dPtdt_geom + dPtdt_phi * Pt_geom;

  return dPtdt_model;
}

SumFieldTask::SumFieldTask(S3DRank *r,
			   LogicalRegion lr,
			   LogicalPartition lp,
			   FieldID field,
			   Domain domain,
			   TaskArgument global_arg,
			   ArgumentMap arg_map,
			   Predicate pred,
			   bool must,
			   MapperID id,
			   MappingTagID tag,
			   bool add_requirements)
  : IndexLauncher(SumFieldTask::TASK_ID, domain, global_arg, 
                  arg_map, pred, must, id, tag | SumFieldTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    // just the one requested field needs to be read
    add_region_requirement(RegionRequirement(lp, 0, READ_ONLY, EXCLUSIVE, lr).add_field(field));
  }
}

void SumFieldTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // no checks right now
}

/*static*/ const char * const SumFieldTask::TASK_NAME = "sum_field";

/*static*/
double SumFieldTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
				   const std::vector<RegionRequirement> &reqs,
				   const std::vector<PhysicalRegion> &regions,
				   Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  RegionAccessor<AccessorType::Generic,double> fa = regions[0].get_field_accessor(reqs[0].instance_fields[0]).typeify<double>();

  // try to get a raw pointer
  Rect<3> subrect;
  ByteOffset offsets[3];
  const double *in_ptr = fa.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);

  // TODO: actually check if offsets are dense!  (many leaf tasks have this problem)
  if(in_ptr && (subrect == subgrid_bounds)) {
    size_t n_pts = subgrid_bounds.volume();

    double sum = 0;
    for(size_t i = 0; i < n_pts; i++)
      sum += in_ptr[i];

    return sum;
  }

  // slow fallback case
  double sum = 0.0;
  for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double v = fa.read(dp);
    sum += v;
  }

  return sum;
#else
  return 0.0;
#endif
}

#ifdef USE_GPU_KERNELS
extern double gpu_reduce_sum(const size_t num_points, const double *data);
#endif

/*static*/
double SumFieldTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
				   const std::vector<RegionRequirement> &reqs,
				   const std::vector<PhysicalRegion> &regions,
				   Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  RegionAccessor<AccessorType::Generic,double> fa = regions[0].get_field_accessor(reqs[0].instance_fields[0]).typeify<double>();

  // try to get a raw pointer
  Rect<3> subrect;
  ByteOffset offsets[3];
  const double *in_ptr = fa.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);

  // TODO: actually check if offsets are dense!  (many leaf tasks have this problem)
  if(in_ptr && (subrect == subgrid_bounds)) {
    size_t n_pts = subgrid_bounds.volume();

    double sum = gpu_reduce_sum(n_pts, in_ptr);

    return sum;
  }

  // no fallback case
  assert(0 && "no slow fallback for GPU sum_field!");
#else
  assert(0 && "no GPU kernel for sum_field!");
#endif
#else
  return 0.0;
#endif
}

////////////////////////////////////////////////////////////////////

CalcImposedPressureTask::CalcImposedPressureTask(S3DRank *rank,
						 Domain domain,
						 TaskArgument global_arg,
						 ArgumentMap arg_map,
						 Predicate pred,
						 bool must,
						 MapperID id,
						 MappingTagID tag,
						 bool add_requirements)
  : IndexLauncher(CalcImposedPressureTask::TASK_ID, domain, global_arg, 
                  arg_map, pred, must, id, tag | CalcImposedPressureTask::MAPPER_TAG), rank(rank)
{
  if (add_requirements)
  {
    // we read a boatload of things for this

    // all the rhs values
    RegionRequirement rr_rhs(rank->lp_rhs_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_rhs);
    for(int fid = 0; fid < FID_RHO_Y(rank->n_spec - 1); fid++)
      rr_rhs.add_field(fid);
    add_region_requirement(rr_rhs);

    // a bunch of intermediate fields too
    RegionRequirement rr_int(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    rr_int.add_field(FID_GAMMA);
    rr_int.add_field(FID_VEL_X);
    rr_int.add_field(FID_VEL_Y);
    rr_int.add_field(FID_VEL_Z);
    for(int s = 0; s < rank->n_spec - 1; s++)
      rr_int.add_field(FID_H_SPEC(s));
    add_region_requirement(rr_int);

    // and temperature
    RegionRequirement rr_s(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_s.add_field(FID_TEMP);
    add_region_requirement(rr_s);
  }
}

void CalcImposedPressureTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // no checks right now
}

/*static*/ const char * const CalcImposedPressureTask::TASK_NAME = "calc_imposed_pressure";

template <typename T>
static inline T min(T a, T b)
{
  if(a < b)
    return a;
  else
    return b;
}

/*static*/
double CalcImposedPressureTask::dense_calc_imposed(S3DRank *rank, 
						   const Rect<3> &subgrid_bounds,
						   bool& ok,
						   const std::vector<PhysicalRegion> &regions)
{
  // if we exit early, that's not ok
  ok = false;

  // lots of pointers
  const double *gamma_ptr = get_dense_ptr<double,3>(regions[1], FID_GAMMA, subgrid_bounds, true);
  if(!gamma_ptr) return 0;
  const double *vel_x_ptr = get_dense_ptr<double,3>(regions[1], FID_VEL_X, subgrid_bounds, true);
  if(!vel_x_ptr) return 0;
  const double *vel_y_ptr = get_dense_ptr<double,3>(regions[1], FID_VEL_Y, subgrid_bounds, true);
  if(!vel_y_ptr) return 0;
  const double *vel_z_ptr = get_dense_ptr<double,3>(regions[1], FID_VEL_Z, subgrid_bounds, true);
  if(!vel_z_ptr) return 0;
  const double *temp_ptr = get_dense_ptr<double,3>(regions[2], FID_TEMP, subgrid_bounds, true);
  if(!temp_ptr) return 0;
  const double *rho_u_ptr = get_dense_ptr<double,3>(regions[0], FID_RHO_U, subgrid_bounds, true);
  if(!rho_u_ptr) return 0;
  const double *rho_v_ptr = get_dense_ptr<double,3>(regions[0], FID_RHO_V, subgrid_bounds, true);
  if(!rho_v_ptr) return 0;
  const double *rho_w_ptr = get_dense_ptr<double,3>(regions[0], FID_RHO_W, subgrid_bounds, true);
  if(!rho_w_ptr) return 0;
  const double *rho_ptr = get_dense_ptr<double,3>(regions[0], FID_RHO, subgrid_bounds, true);
  if(!rho_ptr) return 0;
  const double *rho_e_ptr = get_dense_ptr<double,3>(regions[0], FID_RHO_E, subgrid_bounds, true);
  if(!rho_e_ptr) return 0;

  ByteOffset rho_ys_stride;
  const double *rho_ys_ptr = get_strided_dense_ptrs<double,3>(regions[0], FID_RHO_Y(0), rank->n_spec - 1, subgrid_bounds, rho_ys_stride, true);
  if(!rho_ys_ptr) return 0;

  ByteOffset h_spec_stride;
  const double *h_spec_ptr = get_strided_dense_ptrs<double,3>(regions[1], FID_H_SPEC(0), rank->n_spec - 1, subgrid_bounds, h_spec_stride, true);
  if(!h_spec_ptr) return 0;

  // all pointers are good - go ahead and calculate
  ok = true;
  size_t n_pts = subgrid_bounds.volume();
  const size_t STRIP_SIZE = 512; // want to fit in Titan's CPU cache
  const int num_species = rank->n_spec; 
#if defined(USE_AVX_KERNELS)
  __m256d avx_temp[STRIP_SIZE/4];
  __m256d avx_gamma[STRIP_SIZE/4];
  __m256d avx_sum = _mm256_set1_pd(0.0);
#elif defined(USE_SSE_KERNELS)
  __m128d sse_temp[STRIP_SIZE/2];
  __m128d sse_gamma[STRIP_SIZE/2];
  __m128d sse_sum = _mm_set1_pd(0.0);
#endif
  double temp[STRIP_SIZE]; // temporary or temperature
  double gamma[STRIP_SIZE];
  double sum = 0.0;
  while (n_pts > 0) {
    if (n_pts >= STRIP_SIZE) {
#if defined(USE_AVX_KERNELS)
      if (aligned<32>(gamma_ptr) && aligned<32>(rho_e_ptr)) {
        for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
          avx_gamma[i] = _mm256_load_pd(gamma_ptr+(i<<2));
          avx_sum = _mm256_add_pd(avx_sum,
              _mm256_mul_pd(_mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),
                        _mm256_load_pd(rho_e_ptr+(i<<2))));
        }
      } else {
        for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
          avx_gamma[i] = _mm256_loadu_pd(gamma_ptr+(i<<2));
          avx_sum = _mm256_add_pd(avx_sum,
              _mm256_mul_pd(_mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),
                        _mm256_loadu_pd(rho_e_ptr+(i<<2))));
        }
      }
      // dv_x/dt
      if (aligned<32>(vel_x_ptr) && aligned<32>(rho_u_ptr)) {
        for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
          __m256d v = _mm256_load_pd(vel_x_ptr+(i<<2));
          avx_sum = _mm256_sub_pd(avx_sum,
            _mm256_mul_pd(_mm256_mul_pd(_mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),v),
                          _mm256_load_pd(rho_u_ptr+(i<<2))));
          avx_temp[i] = _mm256_mul_pd(v,v);
        }
      } else {
        for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
          __m256d v = _mm256_loadu_pd(vel_x_ptr+(i<<2));
          avx_sum = _mm256_sub_pd(avx_sum,
            _mm256_mul_pd(_mm256_mul_pd(_mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),v),
                          _mm256_loadu_pd(rho_u_ptr+(i<<2))));
          avx_temp[i] = _mm256_mul_pd(v,v);
        }
      }
      // dv_y/dt
      if (aligned<32>(vel_y_ptr) && aligned<32>(rho_v_ptr)) {
        for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
          __m256d v = _mm256_load_pd(vel_y_ptr+(i<<2));
          avx_sum = _mm256_sub_pd(avx_sum,
            _mm256_mul_pd(_mm256_mul_pd(_mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),v),
                          _mm256_load_pd(rho_v_ptr+(i<<2))));
          avx_temp[i] = _mm256_add_pd(avx_temp[i], _mm256_mul_pd(v,v));
        }
      } else {
        for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
          __m256d v = _mm256_loadu_pd(vel_y_ptr+(i<<2));
          avx_sum = _mm256_sub_pd(avx_sum,
            _mm256_mul_pd(_mm256_mul_pd(_mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),v),
                          _mm256_loadu_pd(rho_v_ptr+(i<<2))));
          avx_temp[i] = _mm256_add_pd(avx_temp[i], _mm256_mul_pd(v,v));
        }
      }
      // dv_z/dt
      if (aligned<32>(vel_z_ptr) && aligned<32>(rho_w_ptr)) {
        for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
          __m256d v = _mm256_load_pd(vel_z_ptr+(i<<2));
          avx_sum = _mm256_sub_pd(avx_sum,
            _mm256_mul_pd(_mm256_mul_pd(_mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),v),
                          _mm256_load_pd(rho_w_ptr+(i<<2))));
          avx_temp[i] = _mm256_add_pd(avx_temp[i], _mm256_mul_pd(v,v));
        }
      } else {
        for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
          __m256d v = _mm256_loadu_pd(vel_z_ptr+(i<<2));
          avx_sum = _mm256_sub_pd(avx_sum,
            _mm256_mul_pd(_mm256_mul_pd(_mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),v),
                          _mm256_loadu_pd(rho_w_ptr+(i<<2))));
          avx_temp[i] = _mm256_add_pd(avx_temp[i], _mm256_mul_pd(v,v));
        }
      }
      // drho/dt
      if (aligned<32>(rho_ptr) && aligned<32>(temp_ptr)) {
        for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
          avx_sum = _mm256_add_pd(avx_sum,_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(
              _mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),_mm256_set1_pd(0.5)),
                _mm256_load_pd(rho_ptr+(i<<2))),avx_temp[i]));
          avx_temp[i] = _mm256_load_pd(temp_ptr+(i<<2));
        }
      } else {
        for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
          avx_sum = _mm256_add_pd(avx_sum,_mm256_mul_pd(_mm256_mul_pd(_mm256_mul_pd(
              _mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),_mm256_set1_pd(0.5)),
                _mm256_loadu_pd(rho_ptr+(i<<2))),avx_temp[i]));
          avx_temp[i] = _mm256_loadu_pd(temp_ptr+(i<<2));
        }
      }
      const double *rho_ys_cur = rho_ys_ptr;
      const double *h_spec_cur = h_spec_ptr;
      if (aligned<32>(rho_ys_cur) && aligned<32>(h_spec_cur)) {
        for (int s = 0; s < (num_species-1); s++) {
          __m256d Ru_div_molwt = _mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(8.31451/CP_REF),
                                   _mm256_set1_pd(recip_molecular_masses[s])),
                                   _mm256_set1_pd(1e3));
          for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
            __m256d ys = _mm256_load_pd(rho_ys_cur+(i<<2));
            avx_sum = _mm256_sub_pd(avx_sum,_mm256_mul_pd(_mm256_mul_pd(
                       _mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),
                       _mm256_load_pd(h_spec_cur+(i<<2))),ys));
            avx_sum = _mm256_add_pd(avx_sum,_mm256_mul_pd(_mm256_mul_pd(
              _mm256_mul_pd(avx_gamma[i],Ru_div_molwt),avx_temp[i]),ys));
          }
          rho_ys_cur += rho_ys_stride;
          h_spec_cur += h_spec_stride;
        }
      } else {
        for (int s = 0; s < (num_species-1); s++) {
          __m256d Ru_div_molwt = _mm256_mul_pd(_mm256_mul_pd(_mm256_set1_pd(8.31451/CP_REF),
                                   _mm256_set1_pd(recip_molecular_masses[s])),
                                   _mm256_set1_pd(1e3));
          for (size_t i = 0; i < (STRIP_SIZE/4); i++) {
            __m256d ys = _mm256_loadu_pd(rho_ys_cur+(i<<2));
            avx_sum = _mm256_sub_pd(avx_sum,_mm256_mul_pd(_mm256_mul_pd(
                       _mm256_sub_pd(avx_gamma[i],_mm256_set1_pd(1.0)),
                       _mm256_loadu_pd(h_spec_cur+(i<<2))),ys));
            avx_sum = _mm256_add_pd(avx_sum,_mm256_mul_pd(_mm256_mul_pd(
              _mm256_mul_pd(avx_gamma[i],Ru_div_molwt),avx_temp[i]),ys));
          }
          rho_ys_cur += rho_ys_stride;
          h_spec_cur += h_spec_stride;
        }
      }
#elif defined(USE_SSE_KERNELS)
      for (size_t i = 0; i < (STRIP_SIZE/2); i++) {
        sse_gamma[i] = _mm_stream_load_pd(gamma_ptr+(i<<1));
        sse_sum = _mm_add_pd(sse_sum, 
            _mm_mul_pd(_mm_sub_pd(sse_gamma[i],_mm_set1_pd(1.0)),
                       _mm_stream_load_pd(rho_e_ptr+(i<<1))));
      }
      // dv_x/dt
      for (size_t i = 0; i < (STRIP_SIZE/2); i++) {
        __m128d v = _mm_stream_load_pd(vel_x_ptr+(i<<1));
        sse_sum = _mm_sub_pd(sse_sum, 
            _mm_mul_pd(_mm_mul_pd(_mm_sub_pd(sse_gamma[i],_mm_set1_pd(1.0)),v),
                       _mm_stream_load_pd(rho_u_ptr+(i<<1))));
        sse_temp[i] = _mm_mul_pd(v,v);
      }
      // dv_y/dt
      for (size_t i = 0; i <(STRIP_SIZE/2); i++) {
        __m128d v = _mm_stream_load_pd(vel_y_ptr+(i<<1));
        sse_sum = _mm_sub_pd(sse_sum,
            _mm_mul_pd(_mm_mul_pd(_mm_sub_pd(sse_gamma[i],_mm_set1_pd(1.0)),v),
                       _mm_stream_load_pd(rho_v_ptr+(i<<1))));
        sse_temp[i] = _mm_add_pd(sse_temp[i],_mm_mul_pd(v,v));
      }
      // dv_z/dt
      for (size_t i = 0; i < (STRIP_SIZE/2); i++) {
        __m128d v = _mm_stream_load_pd(vel_z_ptr+(i<<1));
        sse_sum = _mm_sub_pd(sse_sum,
            _mm_mul_pd(_mm_mul_pd(_mm_sub_pd(sse_gamma[i],_mm_set1_pd(1.0)),v),
                       _mm_stream_load_pd(rho_w_ptr+(i<<1))));
        sse_temp[i] = _mm_add_pd(sse_temp[i],_mm_mul_pd(v,v));
      }
      // drho/dt
      for (size_t i = 0; i < (STRIP_SIZE/2); i++) {
        sse_sum = _mm_add_pd(sse_sum,_mm_mul_pd(_mm_mul_pd(_mm_mul_pd(
                    _mm_sub_pd(sse_gamma[i],_mm_set1_pd(1.0)),_mm_set1_pd(0.5)),
                    _mm_stream_load_pd(rho_ptr+(i<<1))), sse_temp[i]));
        sse_temp[i] = _mm_stream_load_pd(temp_ptr+(i<<1));
      }
      const double *rho_ys_cur = rho_ys_ptr;
      const double *h_spec_cur = h_spec_ptr;
      for (int s = 0; s < (num_species-1); s++) {
        __m128d Ru_div_molwt = _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(8.31451 / CP_REF),
                                _mm_set1_pd(recip_molecular_masses[s])),
                                _mm_set1_pd(1e3));
        for (size_t i = 0; i < (STRIP_SIZE/2); i++) {
          __m128d ys = _mm_stream_load_pd(rho_ys_cur+(i<<1));
          sse_sum = _mm_sub_pd(sse_sum,_mm_mul_pd(_mm_mul_pd(
                      _mm_sub_pd(sse_gamma[i],_mm_set1_pd(1.0)),
                      _mm_stream_load_pd(h_spec_cur+(i<<1))),ys));
          sse_sum = _mm_add_pd(sse_sum,_mm_mul_pd(_mm_mul_pd(
                      _mm_mul_pd(sse_gamma[i],Ru_div_molwt),sse_temp[i]),ys));

        }
        rho_ys_cur += rho_ys_stride;
        h_spec_cur += h_spec_stride;
      }
#else
      for (size_t i = 0; i < STRIP_SIZE; i++) {
        gamma[i] = gamma_ptr[i];
        sum += ((gamma[i] - 1.0) * rho_e_ptr[i]);
      }
      // dv_x/dt
      for (size_t i = 0; i < STRIP_SIZE; i++) {
        double v = vel_x_ptr[i];
        sum -= ((gamma[i] - 1.0) * v * rho_u_ptr[i]);
        temp[i] = v * v;
      }
      // dv_y/dt
      for (size_t i = 0; i < STRIP_SIZE; i++) {
        double v = vel_y_ptr[i];
        sum -= ((gamma[i] - 1.0) * v * rho_v_ptr[i]);
        temp[i] += (v * v);
      }
      // dv_z/dt
      for (size_t i = 0; i < STRIP_SIZE; i++) { 
        double v = vel_z_ptr[i];
        sum -= ((gamma[i] - 1.0) * v * rho_w_ptr[i]);
        temp[i] += (v * v);
      }
      // drho/dt
      for (size_t i = 0; i < STRIP_SIZE; i++) { 
        sum += ((gamma[i] - 1.0) * 0.5 * rho_ptr[i] * temp[i]);
        temp[i] = temp_ptr[i];
      }
      const double *rho_ys_cur = rho_ys_ptr;
      const double *h_spec_cur = h_spec_ptr;
      for (int s = 0; s < (num_species-1); s++) {
        double Ru_div_molwt = 8.31451 / CP_REF * recip_molecular_masses[s] * 1e3;
        for (size_t i = 0; i < STRIP_SIZE; i++) {
          double ys = rho_ys_cur[i];
          sum -= ((gamma[i]-1.0) * h_spec_cur[i] * ys);
          sum += (gamma[i] * Ru_div_molwt * temp[i] * ys);
        }
        rho_ys_cur += rho_ys_stride;
        h_spec_cur += h_spec_stride;
      }
#endif
      n_pts -= STRIP_SIZE;
    } else {
      // dE/dt
      for (size_t i = 0; i < n_pts; i++) {
        gamma[i] = gamma_ptr[i];
        sum += ((gamma[i] - 1.0) * rho_e_ptr[i]);
      }
      // dv_x/dt
      for (size_t i = 0; i < n_pts; i++) {
        double v = vel_x_ptr[i];
        sum -= ((gamma[i] - 1.0) * v * rho_u_ptr[i]);
        temp[i] = v * v;
      }
      // dv_y/dt
      for (size_t i = 0; i < n_pts; i++) {
        double v = vel_y_ptr[i];
        sum -= ((gamma[i] - 1.0) * v * rho_v_ptr[i]);
        temp[i] += (v * v);
      }
      // dv_z/dt
      for (size_t i = 0; i < n_pts; i++) { 
        double v = vel_z_ptr[i];
        sum -= ((gamma[i] - 1.0) * v * rho_w_ptr[i]);
        temp[i] += (v * v);
      }
      // drho/dt
      for (size_t i = 0; i < n_pts; i++) { 
        sum += ((gamma[i] - 1.0) * 0.5 * rho_ptr[i] * temp[i]);
        temp[i] = temp_ptr[i];
      }
      const double *rho_ys_cur = rho_ys_ptr;
      const double *h_spec_cur = h_spec_ptr;
      for (int s = 0; s < (num_species-1); s++) {
        double Ru_div_molwt = 8.31451 / CP_REF * recip_molecular_masses[s] * 1e3;
        for (size_t i = 0; i < n_pts; i++) {
          double ys = rho_ys_cur[i];
          sum -= ((gamma[i]-1.0) * h_spec_cur[i] * ys);
          sum += (gamma[i] * Ru_div_molwt * temp[i] * ys);
        }
        rho_ys_cur += rho_ys_stride;
        h_spec_cur += h_spec_stride;
      }
      n_pts = 0;
    }
    gamma_ptr += STRIP_SIZE;
    vel_x_ptr += STRIP_SIZE;
    vel_y_ptr += STRIP_SIZE;
    vel_z_ptr += STRIP_SIZE;
    temp_ptr += STRIP_SIZE;
    rho_u_ptr += STRIP_SIZE;
    rho_v_ptr += STRIP_SIZE;
    rho_w_ptr += STRIP_SIZE;
    rho_ptr += STRIP_SIZE;
    rho_e_ptr += STRIP_SIZE;
    rho_ys_ptr += STRIP_SIZE;
    h_spec_ptr += STRIP_SIZE;
  }
#if defined(USE_AVX_KERNELS)
  __m128d lower = _mm256_extractf128_pd(avx_sum,0);
  __m128d upper = _mm256_extractf128_pd(avx_sum,1);
  _mm256_zeroall();
  sum = _mm_cvtsd_f64(lower) + _mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1)) +
        _mm_cvtsd_f64(upper) + _mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1));
#elif defined(USE_SSE_KERNELS)
  sum = _mm_cvtsd_f64(sse_sum) + _mm_cvtsd_f64(_mm_shuffle_pd(sse_sum,sse_sum,1));
#endif

#if 0
  // each dPdt is a sum of a bunch of terms, so just lump everything into
  //  the final sum as we do terms for a bunch of points at a time
  double sum = 0;
  for(size_t ofs = 0; ofs < n_pts; ofs += STRIP_SIZE) {
    size_t width = min(n_pts - ofs, STRIP_SIZE);

    // we need one set of temporaries to accumulate velocity^2
    double v2[STRIP_SIZE];

    // dE/dt
    for(size_t i = 0; i < width; i++) {
      sum += (gamma_ptr[i] - 1.0) * rho_e_ptr[i];
    }

    // dv_x/dt
    for(size_t i = 0; i < width; i++) {
      double v = vel_x_ptr[i];
      sum -= (gamma_ptr[i] - 1.0) * v * rho_u_ptr[i];
      v2[i] = v * v;
    }

    // dv_y/dt
    for(size_t i = 0; i < width; i++) {
      double v = vel_y_ptr[i];
      sum -= (gamma_ptr[i] - 1.0) * v * rho_v_ptr[i];
      v2[i] += v * v;
    }

    // dv_z/dt
    for(size_t i = 0; i < width; i++) {
      double v = vel_z_ptr[i];
      sum -= (gamma_ptr[i] - 1.0) * v * rho_w_ptr[i];
      v2[i] += v * v;
    }

    // drho/dt
    for(size_t i = 0; i < width; i++) {
      sum += (gamma_ptr[i] - 1.0) * 0.5 * rho_ptr[i] * v2[i];
    }

    // now a bunch of per-species stuff
    const double *rho_ys_cur = rho_ys_ptr;
    const double *h_spec_cur = h_spec_ptr;
    for(int s = 0; s < rank->n_spec - 1; s++) {
      double Ru_div_molwt = 8.31451 / CP_REF * recip_molecular_masses[s] * 1e3;
      for(size_t i = 0; i < width; i++) {
	double ys = rho_ys_cur[i];
	double gamma = gamma_ptr[i];
	sum -= (gamma - 1.0) * h_spec_cur[i] * ys;
	sum += gamma * Ru_div_molwt * temp_ptr[i] * ys;
	//printf("A %d %g %g %g %g %g %g\n",
	//       s, gamma, h_spec_cur[i], ys, Ru_div_molwt, temp_ptr[i], sum);
      }
      //printf("s %d %.12g\n", s, sum/8);

      rho_ys_cur += rho_ys_stride;
      h_spec_cur += h_spec_stride;
    }

    // update pointers
    gamma_ptr += STRIP_SIZE;
    vel_x_ptr += STRIP_SIZE;
    vel_y_ptr += STRIP_SIZE;
    vel_z_ptr += STRIP_SIZE;
    temp_ptr += STRIP_SIZE;
    rho_u_ptr += STRIP_SIZE;
    rho_v_ptr += STRIP_SIZE;
    rho_w_ptr += STRIP_SIZE;
    rho_ptr += STRIP_SIZE;
    rho_e_ptr += STRIP_SIZE;
    rho_ys_ptr += STRIP_SIZE;
    h_spec_ptr += STRIP_SIZE;
  }
#endif

  return sum;
}

/*static*/
double CalcImposedPressureTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
				   const std::vector<RegionRequirement> &reqs,
				   const std::vector<PhysicalRegion> &regions,
				   Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  // timestep info from a future and the fractional value from input
  Future f_timestep = task->futures[0];
  const TimestepInfo &ts(f_timestep.get_reference<TimestepInfo>());
  double t_frac = *(const double *)(task->args);

  double t = ts.cur_time + t_frac * ts.cur_timestep;
  //printf("T: %g + %g * %g = %g\n", ts.cur_time, t_frac, ts.cur_timestep, t);

  // target pressure
  double dPtdt = rank->compression->calc_target_dpdt(t);

  // attempt fast calculation
  bool ok = false;
  double sum = dense_calc_imposed(rank, subgrid_bounds,
				  ok,
				  regions);

  if(!ok) {
    RegionAccessor<AccessorType::Generic,double> fa_gamma = regions[1].get_field_accessor(FID_GAMMA).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_vel_x = regions[1].get_field_accessor(FID_VEL_X).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_vel_y = regions[1].get_field_accessor(FID_VEL_Y).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_vel_z = regions[1].get_field_accessor(FID_VEL_Z).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_temp = regions[2].get_field_accessor(FID_TEMP).typeify<double>();

    sum = 0.0;
    for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double gamma = fa_gamma.read(dp);
      double vel_x = fa_vel_x.read(dp);
      double vel_y = fa_vel_y.read(dp);
      double vel_z = fa_vel_z.read(dp);
      double temp = fa_temp.read(dp);

      double rhs_rho_u = regions[0].get_field_accessor(FID_RHO_U).typeify<double>().read(dp);
      double rhs_rho_v = regions[0].get_field_accessor(FID_RHO_V).typeify<double>().read(dp);
      double rhs_rho_w = regions[0].get_field_accessor(FID_RHO_W).typeify<double>().read(dp);
      double rhs_rho = regions[0].get_field_accessor(FID_RHO).typeify<double>().read(dp);
      double rhs_rho_e = regions[0].get_field_accessor(FID_RHO_E).typeify<double>().read(dp);

      // pressure change implied by current rhs
      double dPdt = ((gamma - 1.0) *
		     (rhs_rho_e
		      - vel_x * rhs_rho_u - vel_y * rhs_rho_v - vel_z * rhs_rho_w
		      + 0.5 * rhs_rho * (vel_x * vel_x + vel_y * vel_y + vel_z * vel_z)));
    
      const double Ru = 8.31451 / CP_REF;
      for(int s = 0; s < rank->n_spec - 1; s++) {
	double rhs_rho_ys = regions[0].get_field_accessor(FID_RHO_Y(s)).typeify<double>().read(dp);
	double h_spec = regions[1].get_field_accessor(FID_H_SPEC(s)).typeify<double>().read(dp);
	dPdt -= (gamma - 1.0) * h_spec * rhs_rho_ys;
	dPdt += gamma * Ru * temp * 1e3 * recip_molecular_masses[s] * rhs_rho_ys;
	//printf("B %d %g %g %g %g %g %g\n",
	//     s, gamma, h_spec, rhs_rho_ys, 1e3*Ru*recip_molecular_masses[s], temp, dPdt);
	//printf("s %d %.12g\n", s, dPdt);
      }

      sum += dPdt;
    }
  }

#ifdef COMPARE_SUMS
  // compare fast and slow sums for now
  if(ok) {
    double reldiff = fabs((fast_sum - sum) / sum);
    if(reldiff < 1e-10) {
      // ok - use fast value
      sum = fast_sum;
    } else {
      // bad
      printf("CHECK: %.12g %.12g (%.12g) %d\n",
	     fast_sum, sum, fast_sum - sum, ok);
      assert(0);
    }
  }
#endif
  //printf("dPtdt = %.12g, local sum = %.12g\n", dPtdt, sum);

  // our contribution to the imposed pressure is the difference between the target and the
  //  implied, divided by the total number of grid points (so we don't have to normalize later)
  size_t total_grid_pts = (rank->global_grid_size[0] *
                           rank->global_grid_size[1] *
                           rank->global_grid_size[2]);
  return ((subgrid_bounds.volume() * dPtdt) - sum) / total_grid_pts;
#else
  return 0.0;
#endif
}

////////////////////////////////////////////////////////////////////

CalcSourceTermTask::CalcSourceTermTask(S3DRank *rank,
				       Domain domain,
				       TaskArgument global_arg,
				       ArgumentMap arg_map,
				       Predicate pred,
				       bool must,
				       MapperID id,
				       MappingTagID tag,
				       bool add_requirements)
 : IndexLauncher(CalcSourceTermTask::TASK_ID, domain, global_arg, arg_map, pred, must,
                 id, tag | CalcSourceTermTask::MAPPER_TAG), rank(rank)
{
  if (add_requirements)
  {
    add_region_requirement(RegionRequirement(rank->lp_int_top, 0, 
					     WRITE_DISCARD, EXCLUSIVE, rank->lr_int)
			   .add_field(FID_SOURCE_TERM));

    add_region_requirement(RegionRequirement(rank->lp_int_top, 0,
					     READ_ONLY, EXCLUSIVE, rank->lr_int)
			   .add_field(FID_GAMMA)
			   .add_field(FID_PRESSURE));
  }
}

void CalcSourceTermTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // check_field(runtime, ctx, rank->proc_grid_bounds, 
  //             rank->is_grid, rank->ip_top, rank->lr_int, FID_GAMMA,
  //             rank, &S3DRank::RHSFArrays::gamma, 0, 1e-10, 1e10, 0, "gamma");
}

/*static*/
const char * const CalcSourceTermTask::TASK_NAME = "calc_source_term_task";

/*static*/
void CalcSourceTermTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
				       const std::vector<RegionRequirement> &reqs,
				       const std::vector<PhysicalRegion> &regions,
				       Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  Future f = task->futures[0];

  double dPdt_imposed = f.get_result<double>();

  //printf("dPdt_imposed = %.12g\n", dPdt_imposed);

  // fast version
  const double *in_gamma_ptr = get_dense_ptr<double,3>(regions[1], FID_GAMMA, subgrid_bounds, true/*failure ok*/);
  const double *in_pressure_ptr = get_dense_ptr<double,3>(regions[1], FID_PRESSURE, subgrid_bounds, true/*failure ok*/);
  double *out_source_ptr = get_dense_ptr<double,3>(regions[0], FID_SOURCE_TERM, subgrid_bounds, true/*failure ok*/);
  
  if(in_gamma_ptr && in_pressure_ptr && out_source_ptr) {
    size_t n_pts = subgrid_bounds.volume();
    for(size_t i = 0; i < n_pts; i++) {
      double gamma = in_gamma_ptr[i];
      double pressure = in_pressure_ptr[i];
      double source = dPdt_imposed / (gamma * pressure);
      out_source_ptr[i] = source;
    }

    return;
  }

  RegionAccessor<AccessorType::Generic,double> fa_gamma = regions[1].get_field_accessor(FID_GAMMA).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_pressure = regions[1].get_field_accessor(FID_PRESSURE).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_source = regions[0].get_field_accessor(FID_SOURCE_TERM).typeify<double>();


  for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double gamma = fa_gamma.read(dp);
    double pressure = fa_pressure.read(dp);
    double source = dPdt_imposed / (gamma * pressure);
    fa_source.write(dp, source);
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern void calc_source_gpu(size_t n_pts, double dPdt_imposed,
                            const double *in_gamma_ptr, const double *in_pressure_ptr,
                            double *out_source_ptr);
#endif

/*static*/
void CalcSourceTermTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
    const std::vector<RegionRequirement> &reqs,
                                    const std::vector<PhysicalRegion> &regions,
                                    Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  Future f = task->futures[0];

  double dPdt_imposed = f.get_result<double>();

  RegionAccessor<AccessorType::Generic,double> fa_in_gamma = regions[1].get_field_accessor(FID_GAMMA).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_pressure = regions[1].get_field_accessor(FID_PRESSURE).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_out_source = regions[0].get_field_accessor(FID_SOURCE_TERM).typeify<double>();

  Rect<3> subrect;
  ByteOffset offsets[3];
  const double *in_gamma_ptr = fa_in_gamma.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  assert(in_gamma_ptr && (subrect == subgrid_bounds) &&
	 (offsets_are_dense<3, double>(subgrid_bounds, offsets)));

  double *in_pressure_ptr = fa_in_pressure.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  assert(in_pressure_ptr && (subrect == subgrid_bounds) &&
	 (offsets_are_dense<3, double>(subgrid_bounds, offsets)));

  double *out_source_ptr = fa_out_source.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  assert(out_source_ptr && (subrect == subgrid_bounds) &&
	 (offsets_are_dense<3, double>(subgrid_bounds, offsets)));

  size_t n_pts = subgrid_bounds.volume();

  calc_source_gpu(n_pts, dPdt_imposed, in_gamma_ptr, in_pressure_ptr, out_source_ptr);
#endif
#endif
}

////////////////////////////////////////////////////////////////////

struct SumIntegrateArgs {
  double alpha, beta, err, small;
  int which;
  bool has_small;
};

IntegrateSourceTask::IntegrateSourceTask(Domain domain,
					 TaskArgument global_arg,
					 ArgumentMap arg_map,
					 Predicate pred,
					 bool must,
					 MapperID id,
					 MappingTagID tag)
 : IndexLauncher(IntegrateSourceTask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | IntegrateSourceTask::MAPPER_TAG)
{
}

/*static*/ const char * const IntegrateSourceTask::TASK_NAME = "integrate_source_task";

void IntegrateSourceTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing, these get checked elsewhere
}

/*static*/
void IntegrateSourceTask::integrate_field(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
					  IndexSpace is_grid, IndexPartition ip_top,
					  LogicalRegion lr_int, LogicalRegion lr_rhs,
					  LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
					  FieldID fid, FieldID fid2,
					  double alpha, double beta, double err, Future f_timestep,
					  int which, int stage, int tag)
{
  SumIntegrateArgs args;
  args.alpha = alpha;
  args.beta = beta;
  args.err = err;
  args.which = which;
  args.has_small = false;

  IntegrateSourceTask launcher(Domain::from_rect<3>(proc_grid_bounds),
			       TaskArgument(&args, sizeof(args)),
			       ArgumentMap());
  launcher.tag |= tag;

  LogicalPartition lp_int_top = runtime->get_logical_partition(ctx, lr_int, ip_top);
  LogicalPartition lp_rhs_top = runtime->get_logical_partition(ctx, lr_rhs, ip_top);
  LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  LogicalPartition lp_qerr_top = runtime->get_logical_partition(ctx, lr_qerr, ip_top);
  LogicalPartition lp_qtmp_top = runtime->get_logical_partition(ctx, lr_qtmp, ip_top);

  // state variables are output and input in this case
  launcher.add_region_requirement(RegionRequirement(lp_q_top, 0,
						    READ_WRITE, EXCLUSIVE, lr_q)
				  .add_field(fid));

  // qerr is only written on the first integation
  launcher.add_region_requirement(RegionRequirement(lp_qerr_top, 0,
						    ((which == FIRST_INTEGRATION) ? 
						       WRITE_DISCARD : 
						       READ_WRITE),
						    EXCLUSIVE, lr_qerr)
				  .add_field(fid));

  // qtmp is only read on the last integration, only written on the first
  launcher.add_region_requirement(RegionRequirement(lp_qtmp_top, 0,
						    ((which == FIRST_INTEGRATION) ? 
						       WRITE_DISCARD : 
						       READ_ONLY),
						    EXCLUSIVE, lr_qtmp)
				  .add_field(fid));

  // uncorrected rhs term is an input
  launcher.add_region_requirement(RegionRequirement(lp_rhs_top, 0,
						    READ_ONLY, EXCLUSIVE, lr_rhs)
				  .add_field(fid));

  // source term is also an input
  launcher.add_region_requirement(RegionRequirement(lp_int_top, 0,
						    READ_ONLY, EXCLUSIVE, lr_int)
				  .add_field(FID_SOURCE_TERM));

  // energy uses a second source term
  if((int)fid2 >= 0)
    launcher.add_region_requirement(RegionRequirement(lp_int_top, 0,
						      READ_ONLY, EXCLUSIVE, lr_int)
				    .add_field(fid2));

  launcher.add_future(f_timestep);

  RankableTaskHelper::dispatch_task(launcher, ctx, runtime, tag);
}

/*static*/
void IntegrateSourceTask::integrate_fields(HighLevelRuntime *runtime, Context ctx, 
					   const Rect<3>& proc_grid_bounds,
					   IndexSpace is_grid, IndexPartition ip_top, 
					   LogicalRegion lr_int, LogicalRegion lr_rhs,
					   LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
					   const std::vector<FieldID>& fids,
					   double alpha, double beta, double err, Future f_timestep, 
					   int which, int stage, int tag)
{
  SumIntegrateArgs args;
  args.alpha = alpha;
  args.beta = beta;
  args.err = err;
  args.which = which;
  args.has_small = false;

  IntegrateSourceTask launcher(Domain::from_rect<3>(proc_grid_bounds),
			       TaskArgument(&args, sizeof(args)),
			       ArgumentMap());
  launcher.tag |= tag;

  LogicalPartition lp_int_top = runtime->get_logical_partition(ctx, lr_int, ip_top);
  LogicalPartition lp_rhs_top = runtime->get_logical_partition(ctx, lr_rhs, ip_top);
  LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  LogicalPartition lp_qerr_top = runtime->get_logical_partition(ctx, lr_qerr, ip_top);
  LogicalPartition lp_qtmp_top = runtime->get_logical_partition(ctx, lr_qtmp, ip_top);

  // state variables are output and input in this case
  launcher.add_region_requirement(RegionRequirement(lp_q_top, 0,
						    READ_WRITE, EXCLUSIVE, lr_q)
				  .add_fields(fids));

  // qerr is only written on the first integation
  launcher.add_region_requirement(RegionRequirement(lp_qerr_top, 0,
						    ((which == FIRST_INTEGRATION) ? 
						       WRITE_DISCARD : 
						       READ_WRITE),
						    EXCLUSIVE, lr_qerr)
				  .add_fields(fids));

  // qtmp is only read on the last integration, only written on the first
  launcher.add_region_requirement(RegionRequirement(lp_qtmp_top, 0,
						    ((which == FIRST_INTEGRATION) ? 
						       WRITE_DISCARD : 
						       READ_ONLY),
						    EXCLUSIVE, lr_qtmp)
				  .add_fields(fids));

  // uncorrected rhs term is an input
  launcher.add_region_requirement(RegionRequirement(lp_rhs_top, 0,
						    READ_ONLY, EXCLUSIVE, lr_rhs)
				  .add_fields(fids));

  // source term is also an input
  launcher.add_region_requirement(RegionRequirement(lp_int_top, 0,
						    READ_ONLY, EXCLUSIVE, lr_int)
				  .add_field(FID_SOURCE_TERM));

  launcher.add_future(f_timestep);

  RankableTaskHelper::dispatch_task(launcher, ctx, runtime, tag);
}

/*static*/
void IntegrateSourceTask::cpu_base_impl(S3DRank *rank, const Task *task,
					const Rect<3> &my_subgrid_bounds,
					const std::vector<RegionRequirement> &reqs,
					const std::vector<PhysicalRegion> &regions,
					Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  const SumIntegrateArgs *args = (const SumIntegrateArgs *)(task->args);

  Future f_timestep = task->futures[0];
  double timestep = f_timestep.get_reference<TimestepInfo>().cur_timestep;

  const int num_species = reqs[0].instance_fields.size();
  size_t n_pts = my_subgrid_bounds.volume();

  for (int spec = 0; spec < num_species; spec++)
  {
    // attempt fast version
    double *q_ptr = get_dense_ptr<double,3>(regions[0], reqs[0].instance_fields[spec], my_subgrid_bounds, true);
    double *qerr_ptr = get_dense_ptr<double,3>(regions[1], reqs[1].instance_fields[spec], my_subgrid_bounds, true);
    double *qtmp_ptr = get_dense_ptr<double,3>(regions[2], reqs[2].instance_fields[spec], my_subgrid_bounds, true);
    double *rhs_ptr = get_dense_ptr<double,3>(regions[3], reqs[3].instance_fields[spec], my_subgrid_bounds, true);
    double *src_ptr = get_dense_ptr<double,3>(regions[4], FID_SOURCE_TERM, my_subgrid_bounds, true);
    double *src2_ptr = ((regions.size() > 5) ?
  			 get_dense_ptr<double,3>(regions[5], reqs[5].instance_fields[0], my_subgrid_bounds, true) :
			 0);

    if(q_ptr && qerr_ptr && qtmp_ptr && rhs_ptr && src_ptr && ((regions.size() <= 5) || src2_ptr)) {
      switch(args->which) {
      case FIRST_INTEGRATION:
	{
	  for(size_t i = 0; i < n_pts; i++) {
	    double q = q_ptr[i];
	    double qerr = 0;
	    double qtmp = q;
	    double ts_rhs = timestep * (rhs_ptr[i] + src_ptr[i] * (q + (src2_ptr ? src2_ptr[i] : 0)));
	    q = qtmp + args->alpha * ts_rhs;
	    qerr += args->err * ts_rhs;
	    qtmp = q + args->beta * ts_rhs;
	    q_ptr[i] = q;
	    qerr_ptr[i] = qerr;
	    qtmp_ptr[i] = qtmp;
	  }
	  break;
	}

      case MIDDLE_INTEGRATION:
	{
	  for(size_t i = 0; i < n_pts; i++) {
	    double q = q_ptr[i];
	    double qerr = qerr_ptr[i];
	    double qtmp = qtmp_ptr[i];
	    double ts_rhs = timestep * (rhs_ptr[i] + src_ptr[i] * (q + (src2_ptr ? src2_ptr[i] : 0)));
	    q = qtmp + args->alpha * ts_rhs;
	    qerr += args->err * ts_rhs;
	    qtmp = q + args->beta * ts_rhs;
	    q_ptr[i] = q;
	    qerr_ptr[i] = qerr;
	    qtmp_ptr[i] = qtmp;
	  }
	  break;
	}

      case LAST_INTEGRATION:
	{
	  for(size_t i = 0; i < n_pts; i++) {
	    double q = q_ptr[i];
	    double qerr = qerr_ptr[i];
	    double qtmp = qtmp_ptr[i];
	    double ts_rhs = timestep * (rhs_ptr[i] + src_ptr[i] * (q + (src2_ptr ? src2_ptr[i] : 0)));
	    q = qtmp + args->alpha * ts_rhs;
	    qerr += args->err * ts_rhs;
	    qtmp = q + args->beta * ts_rhs;
	    q_ptr[i] = q;
	    qerr_ptr[i] = qerr;
	    // no write to qtmp on last pass
	  }
	  break;
	}
      }

      continue;
    }

    // get whatever accessors you need here
    RegionAccessor<AccessorType::Generic,double> fa_q = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qerr = 
      regions[1].get_field_accessor(reqs[1].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qtmp = 
      regions[2].get_field_accessor(reqs[2].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_rhs = 
      regions[3].get_field_accessor(reqs[3].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_src = 
      regions[4].get_field_accessor(FID_SOURCE_TERM).typeify<double>();
    // this is just for energy
    RegionAccessor<AccessorType::Generic,double> fa_src2;
    if(regions.size() > 5)
      fa_src2 = regions[5].get_field_accessor(reqs[5].instance_fields[0]).typeify<double>();

    for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double q = fa_q.read(dp);
      double qerr = ((args->which == FIRST_INTEGRATION) ? 0 : fa_qerr.read(dp));
      double qtmp = ((args->which == FIRST_INTEGRATION) ? q : fa_qtmp.read(dp));
      double rhs = fa_rhs.read(dp);
      double src = fa_src.read(dp);
      double src2 = (regions.size() > 5) ? fa_src2.read(dp) : 0;

      // rhs is augmented with the current state multiplied by the source term
      rhs += (q + src2) * src;

      qerr += args->err * timestep * rhs;
      q = qtmp + args->alpha * timestep * rhs;

      fa_q.write(dp, q);
      fa_qerr.write(dp, qerr);

      if(args->which != LAST_INTEGRATION) {
	qtmp = q + args->beta * timestep * rhs;
	fa_qtmp.write(dp, qtmp);
      }
    }
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern void gpu_integrate_source(size_t n_pts,
				 double *q_ptr,
				 double *qerr_ptr,
				 double *qtmp_ptr,
				 const double *rhs_ptr,
				 const double *src_ptr,
				 const double *src2_ptr,
				double alpha, double beta, double err, int which);
#endif

/*static*/
void IntegrateSourceTask::gpu_base_impl(S3DRank *rank, const Task *task,
					const Rect<3> &my_subgrid_bounds,
					const std::vector<RegionRequirement> &reqs,
					const std::vector<PhysicalRegion> &regions,
					Context ctx, HighLevelRuntime *runtime)
{
#ifdef USE_GPU_KERNELS
#ifndef NO_COMPUTE
  const SumIntegrateArgs *args = (const SumIntegrateArgs *)(task->args);

  Future f_timestep = task->futures[0];
  double timestep = f_timestep.get_reference<TimestepInfo>().cur_timestep;

  const int num_species = reqs[0].instance_fields.size();
  const size_t n_pts = my_subgrid_bounds.volume();

  for (int spec = 0; spec < num_species; spec++) {
    gpu_integrate_source(n_pts,
                         get_dense_ptr<double,3>(regions[0], reqs[0].instance_fields[spec], my_subgrid_bounds), // q
                         get_dense_ptr<double,3>(regions[1], reqs[1].instance_fields[spec], my_subgrid_bounds), // qerr
                         get_dense_ptr<double,3>(regions[2], reqs[2].instance_fields[spec], my_subgrid_bounds), // qtmp
                         get_dense_ptr<double,3>(regions[3], reqs[3].instance_fields[spec], my_subgrid_bounds), // rhs
                         get_dense_ptr<double,3>(regions[4], FID_SOURCE_TERM, my_subgrid_bounds), // src
                         ((regions.size() > 5) ?
                          get_dense_ptr<double,3>(regions[5], reqs[5].instance_fields[0], my_subgrid_bounds) : // src2
                          0),
			 timestep * args->alpha,
			 timestep * args->beta,
			 timestep * args->err,
			 args->which);
  }
#endif
#else
  assert(0);
#endif
}
