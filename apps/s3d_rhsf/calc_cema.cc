#include "rhsf.h"
#include "calc_cema.h"

#include "philox.h"

#include <math.h> 
#include <unistd.h>

typedef Philox_2x32<10> Rand;

#include <cblas.h>

// LAPACK doesn't provide prototypes?
extern "C" {
  extern void dgeev_(const char *jobvl, const char *jobvr, const int *n, const double *a,
		     const int *lda, double *wr, double *wi, double *vl, const int *ldvl,
		     double *vr, const int *ldvr, double *work, int *lwork, int *info);
};

// adapted from m_cema.f90 from Sandia folks

// calls provided by Sandia Fortran code
extern "C" {
  // SUBROUTINE CKSIZE (MM, KK, II, KKRD)
  void cksize_(int *MM, int *KK, int *II, int *KKRD);

  // SUBROUTINE GETM (NELEM)
  void getm_(int *NELEM);

  // SUBROUTINE CKUML  (T, ICKWRK, RCKWRK, UML)
  void ckuml_(const double *T, const int *ICKWRK /*unused*/,
	      const double *RCKWRK /*unused*/, double *UML);

  // SUBROUTINE CKCVML  (T, ICKWRK, RCKWRK, CVML)
  void ckcvml_(const double *T, const int *ICKWRK /*unused*/,
	      const double *RCKWRK /*unused*/, double *CVML);

  void rd2sk_(const double *P, const double *T, const double *Y,
	      double *C, double *RF, double *RB);

  // SUBROUTINE DWDCT(T, C, A, LDA)
  void dwdct_(const double *T, const double *C, double *A, const int *LDA);
};

// this version of CEMA only returns the largest eigenvalue, which is what is needed for the
//  in-situ analysis of when to trigger refinement

struct sort_pair { 
  double WABS, WR;
  static int descending_absval(const void *a, const void *b)
  {
    double diff = ((const sort_pair *)b)->WABS - ((const sort_pair *)a)->WABS;
    return (diff < 0) ? -1 : (diff > 0) ? 1 : 0;
  }
};

static double cema_ev_only(double temp, double pressure, const double *yspec)
{
#if 0
  // we'll need a bunch of temporary arrays - getrates provides calls to tell us how big
  //  those arrays need to be
  int MM, KK, II, KKRD, NELEM;
  cksize_(&MM, &KK, &II, &KKRD);
  getm_(&NELEM);

  double *C = new double[KK];
  double *RF = new double[II];
  double *RB = new double[II];
  double *A = new double[(KK + 1) * (KK + 1)];
  double *WR = new double[KK + 1];
  double *WI = new double[KK + 1];
  double *UML = new double[KK];
  double *CVML = new double[KK];

  // for the eigenvalue-only case, we don't need RF and RB, but we do need C, which involves nearly
  //  all of the work...
  rd2sk_(&pressure, &temp, yspec, C, RF, RB);

  // now build most of the Jacobian
  int KKp1 = KK + 1;  // so we can pass by reference...
  dwdct_(&temp, C, A, &KKp1);

  // last row of the Jacobian requires some computation
  ckuml_(&temp, 0, 0, UML);
  ckcvml_(&temp, 0, 0, CVML);

  // ! compute the last row in the Jacobian for energy equation assuming constant CV
  // CV = dot_product(C, CVML)
  // A(kk+1, 1:kk+1) = - matmul( UMLT(1, 1:kk), A(1:kk, 1:kk+1))/CV
  double CV = cblas_ddot(KK, C, 1, CVML, 1);
  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, 1, KK + 1, KK,
	      -1.0 / CV, UML, 1, A, KK + 1, 0.0, (A + KK), KK + 1);

  // now we can call DGEEV, which does all the work
  //  except it needs a scratch area, so query the needed size and allocate that too
  int LWORK, INFO;
  double requested_size;
  LWORK = -1;
  dgeev_("N", "N", &KKp1, A, &KKp1, WR, WI, 
	 0, &KKp1, 0, &KKp1, 
	 &requested_size, &LWORK, &INFO);
  assert(INFO == 0);
  //printf("requested = %f\n", requested_size);
  LWORK = (int)requested_size;
  double *RWORK = new double[LWORK];
#ifdef TIME_CEMA
  struct timespec ts1, ts2;
  clock_gettime(CLOCK_MONOTONIC, &ts1);
#endif
  dgeev_("N", "N", &KKp1, A, &KKp1, WR, WI,
	 0, &KKp1, 0, &KKp1, RWORK, &LWORK, &INFO);
#ifdef TIME_CEMA
  clock_gettime(CLOCK_MONOTONIC, &ts2);
  double e = (ts2.tv_sec - ts1.tv_sec) + 1e-9 * (ts2.tv_nsec - ts1.tv_nsec);
  printf("elapsed = %g\n", e);
#endif
  // Its also possible that dgeev wasn't able to compute
  // eigenvectors, but it got all the eigenvalues, which
  // is good enough for us.
  assert(INFO >= 0);
  double TEXP = 0.0;
  if (INFO == 0) {
    // now we want to find the first explosive mode - do this by:
    // 1) computing the absolute value of each eigenvalue
    // 2) sort those and throw out the first NELEM+1
    // 3) choose the largest real component of any eigenvalue that wasn't thrown out
    sort_pair *list = new sort_pair[KK + 1];
    for(int i = 0; i < KK + 1; i++) {
      list[i].WABS = WR[i] * WR[i] + WI[i] * WI[i];
      list[i].WR = WR[i];
    }
    qsort(list, KK + 1, sizeof(sort_pair), sort_pair::descending_absval);

    TEXP = list[NELEM + 1].WR;
    for(int i = NELEM + 2; i < KK + 1; i++)
      if(TEXP < list[i].WR)
        TEXP = list[i].WR;
    delete [] list;
  }

  // free everything we allocated
  delete[] C;
  delete[] RF;
  delete[] RB;
  delete[] A;
  delete[] WR;
  delete[] WI;
  delete[] UML;
  delete[] CVML;
  delete[] RWORK;

  return TEXP;
#else
  usleep(30000);
  return 0.0;
#endif
}

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

SampleCEMATask::SampleCEMATask(S3DRank *r,
                                   Domain domain,
                                   TaskArgument global_arg,
                                   ArgumentMap arg_map,
                                   Predicate pred,
                                   bool must,
                                   MapperID id,
                                   MappingTagID tag,
                                   bool add_requirements)
  : IndexLauncher(SampleCEMATask::TASK_ID, domain, global_arg, 
                  arg_map, pred, must, id, tag | SampleCEMATask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_cema_proc, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_cema_local);
    rr_out.add_field(FID_SAMPLE_X);
    rr_out.add_field(FID_SAMPLE_Y);
    rr_out.add_field(FID_SAMPLE_Z);
    rr_out.add_field(FID_TEMP);
    rr_out.add_field(FID_PRESSURE);
    for(int i = 0; i < rank->n_spec; i++)
      rr_out.add_field(FID_YSPECIES(i));
    add_region_requirement(rr_out);

    RegionRequirement rr_state(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_state.add_field(FID_TEMP);
    add_region_requirement(rr_state);

    RegionRequirement rr_in(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    rr_in.add_field(FID_PRESSURE);
    for(int i = 0; i < rank->n_spec; i++)
      rr_in.add_field(FID_YSPECIES(i));
    add_region_requirement(rr_in);
  }
}

void SampleCEMATask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // for(int s = 0; s < rank->n_spec; s++)
  //     check_field(runtime, ctx, rank->proc_grid_bounds, 
  //                 rank->is_grid, rank->ip_top, rank->lr_int, FID_RR(s),
  // 		  rank, &S3DRank::RHSFArrays::rr_r, s, 1e-10, 1e5, 1e-50, "rr_r[%d]", s);
}

/*static*/ const char * const SampleCEMATask::TASK_NAME = "sample_cema_task";

/*static*/
void SampleCEMATask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
				 const std::vector<RegionRequirement> &reqs,
				 const std::vector<PhysicalRegion> &regions,
				 Context ctx, HighLevelRuntime *runtime)
{
  Point<3> rank_point = task->index_point.get_point<3>();

#ifdef DEBUG_CEMA
  printf("point: (%d,%d,%d)\n", rank_point[0], rank_point[1], rank_point[2]);
#endif

  int first_sample = (rank->cema_samples_per_rank *
		      ((rank_point[2] * rank->proc_grid_size[1] + 
			rank_point[1]) * rank->proc_grid_size[0] + 
		       rank_point[0]));

  Point<3> sp = rank_point;
  sp.x[0] *= rank->cema_samples_per_rank;

  // first, figure out our sample locations
#ifdef DEBUG_CEMA
  printf("bounds: (%d,%d,%d) -> (%d,%d,%d)\n",
	 subgrid_bounds.lo[0], subgrid_bounds.lo[1], subgrid_bounds.lo[2],
	 subgrid_bounds.hi[0], subgrid_bounds.hi[1], subgrid_bounds.hi[2]);
#endif

  // convert the current time to a integer by multiplying by 10^10
  Future f_timestep = task->futures[0];
  double ref_time = L_REF / A_REF;
  double cur_time = f_timestep.get_reference<TimestepInfo>().cur_time * ref_time;
  unsigned long long cur_time_as_int = (unsigned long long)(cur_time * 1e10 + 0.5);
#ifdef DEBUG_CEMA
  printf("time = %g (%lld)\n", cur_time, cur_time_as_int);
#endif

  RegionAccessor<AccessorType::Generic,int> fa_s_x = regions[0].get_field_accessor(FID_SAMPLE_X).typeify<int>();
  RegionAccessor<AccessorType::Generic,int> fa_s_y = regions[0].get_field_accessor(FID_SAMPLE_Y).typeify<int>();
  RegionAccessor<AccessorType::Generic,int> fa_s_z = regions[0].get_field_accessor(FID_SAMPLE_Z).typeify<int>();
  RegionAccessor<AccessorType::Generic,double> fa_s_temp = regions[0].get_field_accessor(FID_TEMP).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_s_press = regions[0].get_field_accessor(FID_PRESSURE).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_g_temp = regions[1].get_field_accessor(FID_TEMP).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_g_press = regions[2].get_field_accessor(FID_PRESSURE).typeify<double>();
	 
  for(int i = 0; i < rank->cema_samples_per_rank; i++) {
    Point<3> gp;
    gp.x[0] = subgrid_bounds.lo[0] + Rand::rand_int(first_sample + i, 
						    cur_time_as_int >> 32,
						    cur_time_as_int + 0,
						    subgrid_bounds.dim_size(0));
    gp.x[1] = subgrid_bounds.lo[1] + Rand::rand_int(first_sample + i, 
						    cur_time_as_int >> 32,
						    cur_time_as_int + 1,
						    subgrid_bounds.dim_size(1));
    gp.x[2] = subgrid_bounds.lo[2] + Rand::rand_int(first_sample + i, 
						    cur_time_as_int >> 32,
						    cur_time_as_int + 2,
						    subgrid_bounds.dim_size(2));
    DomainPoint gdp = DomainPoint::from_point<3>(gp);

    DomainPoint sdp = DomainPoint::from_point<3>(sp);

#ifdef DEBUG_CEMA
    printf("sample (%d,%d,%d) -> (%d,%d,%d)\n", sp.x[0], sp.x[1], sp.x[2], gp.x[0], gp.x[1], gp.x[2]);
#endif

    fa_s_x.write(sdp, gp.x[0]);
    fa_s_y.write(sdp, gp.x[1]);
    fa_s_z.write(sdp, gp.x[2]);
    fa_s_temp.write(sdp, fa_g_temp.read(gdp));
    fa_s_press.write(sdp, fa_g_press.read(gdp));
    for(int s = 0; s < rank->n_spec; s++) {
      RegionAccessor<AccessorType::Generic,double> fa_s_y = regions[0].get_field_accessor(FID_YSPECIES(s)).typeify<double>();
      RegionAccessor<AccessorType::Generic,double> fa_g_y = regions[2].get_field_accessor(FID_YSPECIES(s)).typeify<double>();

      fa_s_y.write(sdp, fa_g_y.read(gdp));
    }
      
    sp.x[0]++;
  }
}

extern void gpu_cema_samples(unsigned first_sample, unsigned num_samples, unsigned n_spec,
			     unsigned seq_hi, unsigned seq_lo,
			     unsigned dim_x, unsigned dim_y, unsigned dim_z,
			     int *sample_x_ptr,
			     int *sample_y_ptr,
			     int *sample_z_ptr,
			     double *sample_temp_ptr,
			     double *sample_press_ptr,
			     double *sample_ys_ptr,
			     int sample_ys_stride,
			     const double *grid_temp_ptr,
			     const double *grid_press_ptr,
			     const double *grid_ys_ptr,
			     int grid_x_step,
			     int grid_y_step,
			     int grid_z_step,
			     int grid_ys_stride);

/*static*/
void SampleCEMATask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
				   const std::vector<RegionRequirement> &reqs,
				   const std::vector<PhysicalRegion> &regions,
				   Context ctx, HighLevelRuntime *runtime)
{
  Point<3> rank_point = task->index_point.get_point<3>();

#ifdef DEBUG_CEMA
  printf("point: (%d,%d,%d)\n", rank_point[0], rank_point[1], rank_point[2]);
#endif

  int first_sample = (rank->cema_samples_per_rank *
		      ((rank_point[2] * rank->proc_grid_size[1] + 
			rank_point[1]) * rank->proc_grid_size[0] + 
		       rank_point[0]));

  Point<3> sp_lo = rank_point;
  sp_lo.x[0] *= rank->cema_samples_per_rank;
  Point<3> sp_hi = sp_lo;
  sp_hi.x[0] += (rank->cema_samples_per_rank - 1);

  Rect<3> sample_bounds(sp_lo, sp_hi);

  // first, figure out our sample locations
#ifdef DEBUG_CEMA
  printf("bounds: (%d,%d,%d) -> (%d,%d,%d)\n",
	 subgrid_bounds.lo[0], subgrid_bounds.lo[1], subgrid_bounds.lo[2],
	 subgrid_bounds.hi[0], subgrid_bounds.hi[1], subgrid_bounds.hi[2]);
#endif

  // convert the current time to a integer by multiplying by 10^10
  Future f_timestep = task->futures[0];
  double ref_time = L_REF / A_REF;
  double cur_time = f_timestep.get_reference<TimestepInfo>().cur_time * ref_time;
  unsigned long long cur_time_as_int = (unsigned long long)(cur_time * 1e10 + 0.5);
#ifdef DEBUG_CEMA
  printf("time = %g (%lld)\n", cur_time, cur_time_as_int);
#endif

  // get pointers - sample ones are ideally in zero-copy memory
  int *sample_x_ptr = get_dense_ptr<int, 3>(regions[0], FID_SAMPLE_X, sample_bounds);
  int *sample_y_ptr = get_dense_ptr<int, 3>(regions[0], FID_SAMPLE_Y, sample_bounds);
  int *sample_z_ptr = get_dense_ptr<int, 3>(regions[0], FID_SAMPLE_Z, sample_bounds);
  double *sample_temp_ptr = get_dense_ptr<double, 3>(regions[0], FID_TEMP, sample_bounds);
  double *sample_press_ptr = get_dense_ptr<double, 3>(regions[0], FID_PRESSURE, sample_bounds);
  ByteOffset sample_ys_stride;
  double *sample_ys_ptr = get_strided_dense_ptrs<double, 3>(regions[0],
							    FID_YSPECIES(0),
							    rank->n_spec,
							    sample_bounds,
							    sample_ys_stride);

  // grid points are in FB, and we want strides in each dimension (i.e. not a dense ptr)
  Rect<3> subrect;
  ByteOffset grid_offsets[3], offsets[3], grid_ys_stride;
  const double *grid_temp_ptr = regions[1].get_field_accessor(FID_TEMP).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, grid_offsets);
  assert(grid_temp_ptr && (subrect == subgrid_bounds));
  const double *grid_press_ptr = regions[2].get_field_accessor(FID_PRESSURE).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  assert(grid_press_ptr && (subrect == subgrid_bounds) && !offset_mismatch(3, grid_offsets, offsets));
  const double *grid_ys_ptr = regions[2].get_field_accessor(FID_YSPECIES(0)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  assert(grid_ys_ptr && (subrect == subgrid_bounds) && !offset_mismatch(3, grid_offsets, offsets));
  for(int i = 1; i < rank->n_spec; i++) {
    const double *next_ys = regions[2].get_field_accessor(FID_YSPECIES(i)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    assert(next_ys && (subrect == subgrid_bounds) && !offset_mismatch(3, grid_offsets, offsets));
    ByteOffset s(next_ys, grid_ys_ptr);
    if(i == 1) {
      grid_ys_stride = s;
    } else {
      assert(s == (grid_ys_stride * i));
    }
  }

  gpu_cema_samples(first_sample, rank->cema_samples_per_rank, rank->n_spec,
		   cur_time_as_int >> 32, cur_time_as_int,
		   subgrid_bounds.dim_size(0),
		   subgrid_bounds.dim_size(1),
		   subgrid_bounds.dim_size(2),
		   sample_x_ptr,
		   sample_y_ptr,
		   sample_z_ptr,
		   sample_temp_ptr,
		   sample_press_ptr,
		   sample_ys_ptr,
		   sample_ys_stride.offset / sizeof(double),
		   grid_temp_ptr,
		   grid_press_ptr,
		   grid_ys_ptr,
		   grid_offsets[0].offset / sizeof(double),
		   grid_offsets[1].offset / sizeof(double),
		   grid_offsets[2].offset / sizeof(double),
		   grid_ys_stride.offset / sizeof(double));
}

/*static*/ void CalcCEMATask::register_task(void)
{
  HighLevelRuntime::register_legion_task<cpu_task>(TASK_ID, Processor::LOC_PROC,
						   false/*single*/, true/*index*/,
						   RHSF_CPU_LEAF_VARIANT,
						   TaskConfigOptions(CPU_BASE_LEAF),
						   TASK_NAME);
}

/*static*/ const char * const CalcCEMATask::TASK_NAME = "cema_task";

/*static*/
void CalcCEMATask::cpu_task(const Task *task,
			    const std::vector<PhysicalRegion> &regions,
			    Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef DEBUG_CEMA
  Point<3> p = task->index_point.get_point<3>();
  printf("calc_cema, pt=(%d,%d,%d), proc=" IDFMT "\n", p[0], p[1], p[2], runtime->get_executing_processor(ctx).id);
#endif

  double temperature = regions[1].get_field_accessor(FID_TEMP).typeify<double>().read(task->index_point);
  double pressure = regions[1].get_field_accessor(FID_PRESSURE).typeify<double>().read(task->index_point);

  int n_spec = task->regions[1].instance_fields.size() - 2;
#ifdef DEBUG_CEMA
  // three more fields for sample_{x,y,z}
  n_spec -= 3;
#endif
  double *yspecs =  (double *)alloca(n_spec * sizeof(double));
  for(int s = 0; s < n_spec; s++) {
    yspecs[s] = regions[1].get_field_accessor(FID_YSPECIES(s)).typeify<double>().read(task->index_point);
  }

  // CEMA code wants dimensionalized temperature and pressure
  temperature *= T_REF;
  pressure *= P_REF * 10.0;

#ifdef DEBUG_CEMA
  printf("(%d,%d,%d): T = %g, P = %g\n",
	 regions[1].get_field_accessor(FID_SAMPLE_X).typeify<int>().read(task->index_point),
	 regions[1].get_field_accessor(FID_SAMPLE_Y).typeify<int>().read(task->index_point),
	 regions[1].get_field_accessor(FID_SAMPLE_Z).typeify<int>().read(task->index_point),
	 temperature, pressure);
#endif
  double texp = cema_ev_only(temperature, pressure, yspecs);
  //printf("texp = %g\n", texp);

  // write the result
  regions[0].get_field_accessor(FID_TEXP).typeify<double>().write(task->index_point, texp);
#endif
}

/*static*/ void SortCEMATask::register_task(void)
{
  HighLevelRuntime::register_legion_task<cpu_task>(TASK_ID, Processor::LOC_PROC,
						   false/*single*/, true/*index*/,
						   RHSF_CPU_LEAF_VARIANT,
						   TaskConfigOptions(CPU_BASE_LEAF),
						   TASK_NAME);
}

/*static*/ const char * const SortCEMATask::TASK_NAME = "sort_cema_task";

template <typename T>
int qsort_cmp(const void *a, const void *b)
{
  T av = *reinterpret_cast<const T *>(a);
  T bv = *reinterpret_cast<const T *>(b);
  if(av < bv) return -1;
  if(av > bv) return  1;
  return 0;
}

/*static*/
void SortCEMATask::cpu_task(const Task *task,
			    const std::vector<PhysicalRegion> &regions,
			    Context ctx, HighLevelRuntime *runtime)
{
  Future f_timestep = task->futures[0];
  double ref_time = L_REF / A_REF;
  double cur_time = f_timestep.get_reference<TimestepInfo>().cur_time * ref_time;

  RegionAccessor<AccessorType::Generic,double> fa_texp = regions[0].get_field_accessor(FID_TEXP).typeify<double>();

  S3DRank *rank = S3DRank::get_rank(Point<3>::ZEROES(), false);
  Point<3> cema_grid_size(rank->proc_grid_size);
  cema_grid_size.x[0] *= rank->cema_samples_per_rank;

  Rect<3> cema_sample_bounds(Point<3>::ZEROES(), cema_grid_size - Point<3>::ONES());
  Rect<3> subrect;
  ByteOffset offset;
  double *texp = fa_texp.raw_dense_ptr<3>(cema_sample_bounds, subrect, offset);
  assert(subrect == cema_sample_bounds);
  assert(offset.offset == sizeof(double));

  // sort the values from smallest to largest
  size_t n_samp = cema_sample_bounds.volume();
  qsort(texp, n_samp, sizeof(double), qsort_cmp<double>);

#ifdef DEBUG_CEMA
  printf("sort: t = %g\n", cur_time);
  for(size_t i = 0; i < cema_sample_bounds.volume(); i++)
    printf(" texp[%zd] = %g\n", i, texp[i]);
#endif

  // P and C metrics are based on percentiles
  size_t pct_a = (size_t)(0.97 * n_samp);
  size_t pct_b = (size_t)(0.98 * n_samp);
  size_t pct_gm = (size_t)(0.90 * n_samp);
  size_t pct_delta = (size_t)(0.98 * n_samp);

  // make sure a < b and gm < delta
  if(pct_a == pct_b) { assert(pct_a > 0); pct_a--; }
  if(pct_gm == pct_delta) { assert(pct_gm > 0); pct_gm--; }

  // P metric is a ratio of differences
  double p_metric = (texp[pct_a] - texp[0]) / (texp[pct_b] - texp[0]);
  
  // C metric is based on standard deviation
  double mu = 0;
  for(size_t i = pct_gm; i <= pct_delta; i++)
    mu += texp[i];
  mu /= (pct_delta - pct_gm + 1);

  double var = 0;
  for(size_t i = pct_gm; i <= pct_delta; i++) {
    double v = ((texp[i] / mu) - 1.0);
    var += v * v;
  }
  double c_metric = 1.0 - sqrt(var / (pct_delta - pct_gm));

  static FILE *outfile = 0;
  if(!outfile) {
    const char *cema_filename = getenv("RHSF_CEMA_OUTFILE");
    if(!cema_filename) 
      cema_filename = "cema_stats.dat";
    if(!strcmp(cema_filename, "-"))
      outfile = stdout;
    else
      outfile = fopen(cema_filename, "a");
    assert(outfile);
  }
  fprintf(outfile, " %.5E  %.5E  %.5E  %.5E  %.5E\n",
	  cur_time, p_metric, c_metric, texp[0], texp[n_samp - 1]);
  fflush(outfile);
}
