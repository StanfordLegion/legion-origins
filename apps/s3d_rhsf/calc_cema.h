
#ifndef _CALC_CEMA_H_
#define _CALC_CEMA_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

class SampleCEMATask : public IndexLauncher {
public:
  SampleCEMATask(S3DRank *rank,
		 Domain domain,
		 TaskArgument global_arg,
		 ArgumentMap arg_map,
		 Predicate pred = Predicate::TRUE_PRED,
		 bool must = false,
		 MapperID id = 0,
		 MappingTagID tag = 0,
		 bool add_requirement = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = SAMPLE_POINTS_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
protected:
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcCEMATask : public IndexLauncher {
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_CEMA_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  //static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
public:
  static void register_task(void);
  static void cpu_task(const Task *task,
		       const std::vector<PhysicalRegion> &regions,
		       Context ctx, HighLevelRuntime *runtime);
};

class SortCEMATask : public IndexLauncher {
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = SORT_CEMA_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  //static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
public:
  static void register_task(void);
  static void cpu_task(const Task *task,
		       const std::vector<PhysicalRegion> &regions,
		       Context ctx, HighLevelRuntime *runtime);
};

#endif
