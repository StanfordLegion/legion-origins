
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <assert.h>

#include "cuda.h"
#include "cuda_runtime.h"

//#define DEBUG_CUDA

#ifdef DEBUG_CUDA
#define CUDA_SAFE_CALL(expr)                        \
  {                                                 \
    cudaError_t err = (expr);                       \
    if (err != cudaSuccess)                         \
    {                                               \
      printf("Cuda error: %s\n", cudaGetErrorString(err));  \
      assert(false);                                \
    }                                               \
  }
#else
#define CUDA_SAFE_CALL(expr)  expr
#endif

__global__
void check_temperature_kernel(const int max_elements,
                              const double min_temp,
                              const double max_temp,
                              const double *temp_ptr,
                              const int rank_x,
                              const int rank_y,
                              const int rank_z)
{
  const int idx = blockIdx.x*blockDim.x+threadIdx.x;
  if (idx >= max_elements) return;

  double temp = temp_ptr[idx];
  if ((temp >= min_temp) && (temp <= max_temp)) {
    // ok - check this way so that NaN's fail too
  } else
    printf("Temperature %.8g out of range at index %d on rank (%d,%d,%d)\n",
            temp, idx, rank_x, rank_y, rank_z);
}

__host__
void gpu_check_temperature(const int max_elements,
                           const double min_temp,
                           const double max_temp,
                           const double *temp_ptr,
                           const int rank_x,
                           const int rank_y,
                           const int rank_z)
{
  const int threads_per_block = 256;
  const int num_blocks = (max_elements + (threads_per_block-1))/threads_per_block;

  check_temperature_kernel<<<num_blocks,threads_per_block>>>(
                                                    max_elements,
                                                    min_temp,
                                                    max_temp,
                                                    temp_ptr,
                                                    rank_x,
                                                    rank_y,
                                                    rank_z);
}

__global__
void check_chemistry_kernel(const int max_elements,
                            const double max_rate,
                            const double *rates,
                            const int num_specs,
                            const int rank_x, 
                            const int rank_y,
                            const int rank_z)
{
  const int idx = blockIdx.x*blockDim.x+threadIdx.x;
  if (idx >= max_elements) return;

  for (int s = 0; s < num_specs; s++)
  {
    double rate = rates[s*max_elements+idx];
    if (0 && fabs(rate) <= max_rate) {
      // ok - check this way so that NaN's fail too
    } else
      printf("Species %d chemistry rate %.8g too large at index %d on rank (%d,%d,%d)\n",
              s, rate, idx, rank_x, rank_y, rank_z);
  }
}

__host__
void gpu_check_chemistry(const int max_elements,
                         const double max_rate,
                         const double *rates,
                         const int num_species,
                         const int rank_x,
                         const int rank_y,
                         const int rank_z)
{
  const int threads_per_block = 256;
  const int num_blocks = (max_elements + (threads_per_block-1))/threads_per_block;

  check_chemistry_kernel<<<num_blocks,threads_per_block>>>(
                                                  max_elements,
                                                  max_rate,
                                                  rates,
                                                  num_species,
                                                  rank_x,
                                                  rank_y,
                                                  rank_z);
}

