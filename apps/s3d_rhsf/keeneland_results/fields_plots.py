#!/usr/bin/python

import subprocess
import sys, os, shutil
import string, re
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages
#matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

def get_sim_time(fname, which):
    cum_time = 0
    ok = False
    npes = 0
    steps = 0
    f = open(fname, "r")
    for l in f.readlines():
        m = re.search(r'rhsf '+which+r'\s+\d+\s+\d+\s+\d+\s+(\d+(\.\d+)?([Ee][+-]?\d+)?)', l)
        if m is not None:
            cum_time += float(m.group(1))

        if 'Program finished normally' in l:
            ok = True

        m = re.search(r'writing save    files for: i_time =\s+(\d+)', l)
        if m is not None:
            steps = int(m.group(1))

        m = re.search(r'Initialized topology module.+npes =\s+(\d+)', l)
        if m is not None:
            npes = int(m.group(1))

    if ok:
        return cum_time / steps / npes
    else:
        raise Exception
  
home_dir="./"
out_dir = home_dir + "figs/"
expr_name="ckt_sim"

tableau1 = (0.968,0.714,0.824)
tableau2 = (0.882,0.478,0.470)
tableau3 = (0.565,0.663,0.792)
tableau4 = (0.635,0.635,0.635)
tableau5 = (0.678,0.545,0.788)
tableau6 = (1.000,0.620,0.290)
tableau7 = (0.859,0.859,0.553)
tableau8 = (0.769,0.612,0.580)
tableau9 = (0.478,0.757,0.424)
tableau10= (0.427,0.800,0.855)
tableau11= (0.929,0.592,0.792)
tableau12= (0.929,0.400,0.364)
tableau13= (0.447,0.620,0.808)
tableau14= (0.780,0.780,0.780)
tableau15= (0.773,0.690,0.835)
tableau16= (0.882,0.616,0.353)
tableau17= (0.804,0.800,0.365)
tableau18= (0.659,0.471,0.431)
tableau18= (0.404,0.749,0.361)
tableau19= (0.137,0.122,0.125)

def do_problem_size_graph(indir, mech, mechname, outfig):
    fig = plt.figure(figsize = (8,5))
    plt.title('Problem Size Scaling (%s Mechanism)' % mechname)
    plt.xlabel('Problem Size (\\# points)')
    plt.ylabel('Points/sec')
    plt.xscale('log', basex=10**10, subsx=[])
    cpu_data = ( (12**3, 12**3/get_sim_time(indir+'/cpu_%s_12.out'%mech, 'fortran')),
                 (16**3, 16**3/get_sim_time(indir+'/cpu_%s_16.out'%mech, 'fortran')),
                 (20**3, 20**3/get_sim_time(indir+'/cpu_%s_20.out'%mech, 'fortran')),
                 (24**3, 24**3/get_sim_time(indir+'/cpu_%s_24.out'%mech, 'fortran')),
                 (28**3, 28**3/get_sim_time(indir+'/cpu_%s_28.out'%mech, 'fortran')),
                 (32**3, 32**3/get_sim_time(indir+'/cpu_%s_32.out'%mech, 'fortran')),
                 (48**3, 48**3/get_sim_time(indir+'/cpu_%s_48.out'%mech, 'fortran')),
                 (64**3, 64**3/get_sim_time(indir+'/cpu_%s_64.out'%mech, 'fortran')),
                 (96**3, 96**3/get_sim_time(indir+'/leg_%s_96.out'%mech, 'fortran')),
                 )
    leg_data = ( (16**3, 16**3/get_sim_time(indir+'/leg_%s_16.out'%mech, 'legion')),
                 (32**3, 32**3/get_sim_time(indir+'/leg_%s_32.out'%mech, 'legion')),
                 (48**3, 48**3/get_sim_time(indir+'/leg_%s_48.out'%mech, 'legion')),
                 (64**3, 64**3/get_sim_time(indir+'/leg_%s_64.out'%mech, 'legion')),
                 (80**3, 80**3/get_sim_time(indir+'/leg_%s_80.out'%mech, 'legion')),
                 (96**3, 96**3/get_sim_time(indir+'/leg_%s_96.out'%mech, 'legion')),
                 )
    gpu_data = ( (32*32*32, 32*32*32/get_sim_time(indir+'/gpu_%s_32_32_32.out'%mech, 'legion')),
                 (64*32*32, 64*32*32/get_sim_time(indir+'/gpu_%s_64_32_32.out'%mech, 'legion')),
                 (64*64*32, 64*64*32/get_sim_time(indir+'/gpu_%s_64_64_32.out'%mech, 'legion')),
                 (64*64*64, 64*64*64/get_sim_time(indir+'/gpu_%s_64_64_64.out'%mech, 'legion')),
                 (96*64*64, 96*64*64/get_sim_time(indir+'/gpu_%s_96_64_64.out'%mech, 'legion')),
                 (96*96*64, 96*96*64/get_sim_time(indir+'/gpu_%s_96_96_64.out'%mech, 'legion')),
                 (96*96*96, 96*96*96/get_sim_time(indir+'/gpu_%s_96_96_96.out'%mech, 'legion')),
                 )

    xtics = ( (12**3, "$12^3$"),
              (16**3, "$16^3$"),
              (24**3, "$24^3$"),
              (32**3, "$32^3$"),
              (48**3, "$48^3$"),
              (64**3, "$64^3$"),
              (96**3, "$96^3$"),
              )
    plt.xlim([ 11**3, 110**3 ])
    plt.xticks(*zip(*xtics))

    plt.plot(*zip(*cpu_data), label = "Fortran", color=tableau6, 
              marker='o',markerfacecolor=tableau6,linestyle='dashed',linewidth=1.0)
    plt.plot(*zip(*leg_data), label = "Legion (CPU only)", color=tableau2, 
              marker='s',markerfacecolor=tableau2,linestyle='dashed',linewidth=1.0)
    plt.plot(*zip(*gpu_data), label = "Legion (CPU+GPU)", color=tableau10, 
              marker='D',markerfacecolor=tableau10,linestyle='dashed',linewidth=1.0)
    plt.legend(loc = 2)
    if outfig:
        fig.savefig(outfig, format="pdf", bbox_inches="tight")

def do_intra_node_graph(indir, mech, mechname, outfig):
    fig = plt.figure(figsize = (8,5))
    plt.title('Performance Scaling - Single Node (%s Mechanism)' % mechname)
    plt.xlabel('\\# Cores')
    plt.ylabel('Points/sec')
    cpu_data = ( (1, 96**3/get_sim_time(indir+'/cpu_%s_1.out'%mech, 'fortran')),
                 (2, 96**3/get_sim_time(indir+'/cpu_%s_2.out'%mech, 'fortran')),
                 (4, 96**3/get_sim_time(indir+'/cpu_%s_4.out'%mech, 'fortran')),
                 (8, 96**3/get_sim_time(indir+'/cpu_%s_8.out'%mech, 'fortran')),
                 (12, 96**3/get_sim_time(indir+'/cpu_%s_12.out'%mech, 'fortran')),
                 (16, 96**3/get_sim_time(indir+'/cpu_%s_16.out'%mech, 'fortran')),
                 )
    leg_data = ( (1, 96**3/get_sim_time(indir+'/leg_%s_1.out'%mech, 'legion')),
                 (2, 96**3/get_sim_time(indir+'/leg_%s_2.out'%mech, 'legion')),
                 (4, 96**3/get_sim_time(indir+'/leg_%s_4.out'%mech, 'legion')),
                 (8, 96**3/get_sim_time(indir+'/leg_%s_8.out'%mech, 'legion')),
                 (12, 96**3/get_sim_time(indir+'/leg_%s_12.out'%mech, 'legion')),
                 (16, 96**3/get_sim_time(indir+'/leg_%s_16.out'%mech, 'legion')),
                 )
    gpu1_data = ( (1, 96**3/get_sim_time(indir+'/1gpu_%s_1.out'%mech, 'legion')),
                  (2, 96**3/get_sim_time(indir+'/1gpu_%s_2.out'%mech, 'legion')),
                  (4, 96**3/get_sim_time(indir+'/1gpu_%s_4.out'%mech, 'legion')),
                  (8, 96**3/get_sim_time(indir+'/1gpu_%s_8.out'%mech, 'legion')),
                  (12, 96**3/get_sim_time(indir+'/1gpu_%s_12.out'%mech, 'legion')),
                  (16, 96**3/get_sim_time(indir+'/1gpu_%s_16.out'%mech, 'legion')),
                  )
    gpu2_data = ( (1, 96**3/get_sim_time(indir+'/2gpu_%s_1.out'%mech, 'legion')),
                  (2, 96**3/get_sim_time(indir+'/2gpu_%s_2.out'%mech, 'legion')),
                  (4, 96**3/get_sim_time(indir+'/2gpu_%s_4.out'%mech, 'legion')),
                  (8, 96**3/get_sim_time(indir+'/2gpu_%s_8.out'%mech, 'legion')),
                  (12, 96**3/get_sim_time(indir+'/2gpu_%s_12.out'%mech, 'legion')),
                  (16, 96**3/get_sim_time(indir+'/2gpu_%s_16.out'%mech, 'legion')),
                  )
    gpu3_data = ( (1, 96**3/get_sim_time(indir+'/3gpu_%s_1.out'%mech, 'legion')),
                  (2, 96**3/get_sim_time(indir+'/3gpu_%s_2.out'%mech, 'legion')),
                  (4, 96**3/get_sim_time(indir+'/3gpu_%s_4.out'%mech, 'legion')),
                  (8, 96**3/get_sim_time(indir+'/3gpu_%s_8.out'%mech, 'legion')),
                  (12, 96**3/get_sim_time(indir+'/3gpu_%s_12.out'%mech, 'legion')),
                  (16, 96**3/get_sim_time(indir+'/3gpu_%s_16.out'%mech, 'legion')),
                  )

    xtics = ( (1, "$1$"),
              (2, "$2$"),
              (4, "$4$"),
              (8, "$8$"),
              (12, "$12$"),
              (16, "$16$"),
              )
    plt.xlim([ 0.5, 16.5 ])
    plt.xticks(*zip(*xtics))
    max_height = 0
    for x,y in cpu_data:
        if y > max_height:
            max_height = y;
    for x,y in leg_data:
        if y > max_height:
            max_height = y
    for x,y in gpu1_data:
        if y > max_height:
            max_height = y
    for x,y in gpu2_data:
        if y > max_height:
            max_height = y
    for x,y in gpu3_data:
        if y > max_height:
            max_height = y
    plt.ylim([ 0.0, 1.1*max_height])

    plt.plot(*zip(*cpu_data), label = "Fortran", color=tableau6, marker='o',
              markerfacecolor=tableau6,linestyle='dashed',linewidth=1.0)
    plt.plot(*zip(*leg_data), label = "Legion - CPU only", color=tableau2, 
              marker='s', markerfacecolor=tableau2,linestyle='dashed',linewidth=1.0)
    plt.plot(*zip(*gpu1_data), label = "Legion - Hybrid (1 GPU)", color=tableau10,
              marker='D',markerfacecolor=tableau10,linestyle='dashed',linewidth=1.0)
    plt.plot(*zip(*gpu2_data), label = "Legion - Hybrid (2 GPUs)", color=tableau18,
              marker='v',markerfacecolor=tableau18,linestyle='dashed',linewidth=1.0)
    plt.plot(*zip(*gpu3_data), label = "Legion - Hybrid (3 GPUs)", color=tableau5,
              marker='^',markerfacecolor=tableau5,linestyle='dashed',linewidth=1.0)
    plt.legend(loc = 4, prop = { 'size': 11 })
    if outfig:
        fig.savefig(outfig, format="pdf", bbox_inches="tight")

def do_lagging_barchart(indir, mech, mechname, outfig):
    fig = plt.figure(figsize = (8,5))
    plt.title('Benefit of Lagging Switch (%s Mechanism)' % mechname)
    plt.ylabel('Points/sec')
    bars = ( (1, 96**3/get_sim_time(indir+'/cpu_%s_16.out'%mech, 'fortran')),
             (2, 96**3/get_sim_time(indir+'.nl/cpu_%s_16.out'%mech, 'fortran')),
             
             (4, 96**3/get_sim_time(indir+'/leg_%s_16.out'%mech, 'legion')),
             (5, 96**3/get_sim_time(indir+'.nl/leg_%s_16.out'%mech, 'legion')),

             (7, 96**3/get_sim_time(indir+'/gpu_%s_2%s.out'%(mech,"b" if mech == "hept" else ""),
                                    'legion')),
             (8, 96**3/get_sim_time(indir+'.nl/gpu_%s_2.out'%mech,
                                    'legion')),
             )

    plt.bar(*zip(*bars))
    if outfig:
        fig.savefig(outfig, format="pdf", bbox_inches="tight")    

def make_plots(show = True, save = True, out_dir="figs/"):
    do_problem_size_graph(home_dir+"/out.ps", "h2", "H2", 
                          out_dir+"psize_h2.pdf" if save else None)
    do_problem_size_graph(home_dir+"/out.ps", "dme", "DME", 
                          out_dir+"psize_dme.pdf" if save else None)
    do_problem_size_graph(home_dir+"/out.ps", "hept", "Heptane", 
                          out_dir+"psize_hept.pdf" if save else None)

    #do_intra_node_graph(home_dir+"/out.sn", "h2", "H2",
    #                    out_dir+"scale_h2.pdf" if save else None)
    do_intra_node_graph(home_dir+"/out.sn", "dme", "DME",
                        out_dir+"scale_dme.pdf" if save else None)
    do_intra_node_graph(home_dir+"/out.sn", "hept", "Heptane",
                        out_dir+"scale_hept.pdf" if save else None)

    do_lagging_barchart(home_dir+"/out.sn", "h2", "H2",
                        out_dir+"lag_h2.pdf" if save else None)
    do_lagging_barchart(home_dir+"/out.sn", "dme", "DME",
                        out_dir+"lag_dme.pdf" if save else None)
    do_lagging_barchart(home_dir+"/out.sn", "hept", "Heptane",
                        out_dir+"lag_hept.pdf" if save else None)

    if show:
        plt.show()

if __name__ == "__main__":
    make_plots(not("-s" in sys.argv), True)  

