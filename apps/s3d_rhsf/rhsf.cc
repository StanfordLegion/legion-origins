#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <unistd.h>

// Only include MPI if we're
// actually going to sync up
#ifndef SHARED_LOWLEVEL
#define OMPI_SKIP_MPICXX
#include <mpi.h>
#endif

#include "rhsf.h"
#include "rhsf_init.h"
#include "rhsf_impl.h"

#include "rank_helper.h"
#include "calc_volume.h"
#include "calc_temp.h"
#include "calc_physics.h"
#include "calc_chem.h"
#include "calc_flux.h"
#include "calc_scalar.h"
#include "calc_pre.h"
#include "calc_stencil.h"
#include "calc_sum.h"
#ifdef USE_CEMA
#include "calc_cema.h"
#endif
#include "check_field.h"
#include "impose_pressure.h"

// Include Singe emitted header files
#include "getcoeffs.h"
#include "getrates.h"

// the Singe header files don't explicitly give the number of species, but
//  we can derive it
#define NUM_SPECIES (sizeof(molecular_masses)/sizeof(molecular_masses[0]))

// Include Legion header files
#include "legion.h"
#include "arrays.h"

#define CHECK_PTHREAD(cmd) do { \
  int ret = (cmd); \
  if(ret != 0) { \
    fprintf(stderr, "PTHREAD: %s = %d (%s)\n", #cmd, ret, strerror(ret)); \
    exit(1); \
  } \
} while(0)

// Bring in the Legion namespaces
using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

// Normally global variables are illegal in
// Legion applications, but these variables
// are all set before starting the Legion
// runtime and then remain constant for the
// duration of the application so they are alright.

/*static*/ const int S3DRank::diff_max_errors = 10;
/*static*/ const bool S3DRank::show_all_error_counts = false;
/*static*/ bool& S3DRank::get_diff_replace_data(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_show_progress(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_enable_profiling(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_perform_waits(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_perform_all_checks(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_perform_final_checks(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_explicit_ghost_cells(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_skip_nonstiff_diffusion(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_hoist_subtasks(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_mixed_stencils(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_all_gpu(void)
{
  static bool value = false;
  return value;
}
/*static*/ bool& S3DRank::get_field_centric(void)
{
  static bool value = false;
  return value;
}
/*static*/ int& S3DRank::get_fuse_fields(void)
{
  static int value = 1;
  return value;
}
/*static*/ int& S3DRank::get_legion_pass(void)
{
  static int value = 0;
  return value;
}

/*static*/ int& S3DRank::get_compression_lag(void)
{
  static int value = 1;
  return value;
}

// Helper method for parsing command line arguments
void S3DRank::parse_cmdline_args(int argc, const char *argv[])
{
  // Set the default values first in case they haven't
  // already been set
  for(int i = 1; i < argc; i++) {
    if(!strcmp(argv[i], "-progress")) {
      get_show_progress() = true;
      continue;
    }

    if(!strcmp(argv[i], "-profile")) {
      get_enable_profiling() = true;
      continue;
    }

    if(!strcmp(argv[i], "-waits")) {
      get_perform_waits() = true;
      continue;
    }

    if(!strcmp(argv[i], "-checks")) {
      get_perform_all_checks() = true;
      get_perform_final_checks() = true;
      continue;
    }

    if(!strcmp(argv[i], "-final")) {
      get_perform_final_checks() = true;
      continue;
    }

    if(!strcmp(argv[i], "-repl")) {
      get_perform_all_checks() = true;
      get_perform_final_checks() = true;
      get_diff_replace_data() = true;
      continue;
    }

    if(!strcmp(argv[i], "-ghost")) {
      get_explicit_ghost_cells() = true;
      continue;
    }

    if(!strcmp(argv[i], "-stiffopt")) {
      get_skip_nonstiff_diffusion() = true;
      continue;
    }
    
    if(!strcmp(argv[i], "-hoist")) {
      get_hoist_subtasks() = true;
      continue;
    }
    
    if(!strcmp(argv[i],"-mixed")) {
      get_mixed_stencils() = true;
      continue;
    }

    if(!strcmp(argv[i],"-allgpu")) {
      get_all_gpu() = true;
      continue;
    }

    if(!strcmp(argv[i],"-field-centric")) {
      get_field_centric() = true;
      continue;
    }

    if(!strcmp(argv[i],"-fuse")) {
      get_fuse_fields() = atoi(argv[++i]);
      continue;
    }
  }
}

// Some dummy argument lists for when we run in test mode
static const char * const dummy_args[] = { "rhsf_test", "-MPI",
                                           "-level", "3",
                                           "-ll:cpu", "4",
					   "-ll:gsize", "128",
                                           "-ll:csize", "4096",
                                           "-ll:fsize", "1024",
                                           //"-level", "2", "-cat", "dma",
#ifdef LEGION_SPY
                                           "-level", "2", "-cat", "legion_spy",
#endif
                                           //"-hl:tree",
#ifdef LEGION_PROF
                                           "-level", "2", "-cat", "legion_prof",
#endif

#ifdef LEGION_LOGGING
                                           "-level", "2", "-cat", "legion_logging",
#endif
                                           "-ll:gpu", "2",
                                           "-ll:util", "1",
                                           "-hl:nosteal",
                                           "-ll:dma", "2",
                                           //"-hl:separate",
                                           0 }; 
static const int dummy_argc = (sizeof(dummy_args) / sizeof(dummy_args[0])) - 1;

#define TRACK_HANDSHAKE_TIMING
#ifdef TRACK_HANDSHAKE_TIMING
class HandshakeTimingTracker {
public:
  HandshakeTimingTracker(const char *_filename, int _queue_size, int _write_size, int _min_gap);
  ~HandshakeTimingTracker(void);

  void log_event(const char *label);
  void flush_events(bool flush_all = false);

protected:
  char *filename;
  int queue_size, write_size, min_gap; 
  int write_pos, read_pos;
  const char **event_labels;
  double *event_times;
};

HandshakeTimingTracker::HandshakeTimingTracker(const char *_filename, int _queue_size, int _write_size, int _min_gap)
  : filename(strdup(_filename)), queue_size(_queue_size), write_size(_write_size), min_gap(_min_gap),
    write_pos(0), read_pos(0)
{
  event_labels = new const char *[queue_size];
  event_times = new double[queue_size];
  for(int i = 0; i < queue_size; i++) {
    event_labels[i] = "init";
    event_times[i] = 0;
  }
}

HandshakeTimingTracker::~HandshakeTimingTracker(void)
{
  flush_events(true);
  free(filename);
  delete[] event_labels;
  delete[] event_times;
}

void HandshakeTimingTracker::log_event(const char *label)
{
  struct timespec tp;
  clock_gettime(CLOCK_MONOTONIC, &tp);
  double now = tp.tv_sec + 1e-9 * tp.tv_nsec;

  // get the next write position
  int my_wpos = __sync_fetch_and_add(&write_pos, 1);
  // look for obvious overflow
  assert(my_wpos < (read_pos + queue_size));

  event_times[my_wpos % queue_size] = now;
  event_labels[my_wpos % queue_size] = label;
}

void HandshakeTimingTracker::flush_events(bool flush_all)
{
  // first see if it's time to write
  if(!flush_all && ((write_pos - read_pos) < (write_size + min_gap))) return;

  FILE *f = fopen(filename, "a");
  assert(f);
  int count = 0;
  while((flush_all || (count < write_size)) && (read_pos < write_pos)) {
    fprintf(f, "%20.9f %s\n", event_times[read_pos % queue_size], event_labels[read_pos % queue_size]);
    event_labels[read_pos % queue_size] = "wrap";
    event_times[read_pos % queue_size] = 0;
    read_pos++;
    count++;
  }

  fclose(f);
}

HandshakeTimingTracker *timing_tracker = 0;
#endif

ExtLegionHandshake::ExtLegionHandshake(int init_state, int _ext_queue_depth, int _legion_queue_depth)
  : state(init_state), ext_queue_depth(_ext_queue_depth), legion_queue_depth(_legion_queue_depth),
    ext_count(0), legion_count(0)
{
  pthread_mutex_init(&sync_mutex, 0);
  pthread_cond_init(&sync_cond, 0);
#ifndef SHARED_LOWLEVEL
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  //printf("handshake %p created on rank %d\n", this, rank);
#endif

#ifdef TRACK_HANDSHAKE_TIMING
  const char *s = getenv("HANDSHAKE_TIMING_FILE");
  if(s) {
    char filename[256];
    strcpy(filename, s);
    char *pos = filename + strlen(filename);
    *pos++ = '.';
    gethostname(pos, 32);
    timing_tracker = new HandshakeTimingTracker(filename, 16384, 128, 64);
    timing_tracker->log_event("creation");
  }
#endif
}

void ExtLegionHandshake::ext_init(void)
{
#ifdef TRACK_HANDSHAKE_TIMING
  if(timing_tracker) timing_tracker->log_event("ext_init_entry");
#endif
  CHECK_PTHREAD( pthread_mutex_lock(&sync_mutex) );

  //printf("handshake %p: ext init - counts = L=%d, E=%d\n", this, legion_count, ext_count);
  
  ext_count++;

  if(legion_count == 0) {
    // no legion threads have arrived, so sleep until one does
    //printf("ext sleeping...\n");
    CHECK_PTHREAD( pthread_cond_wait(&sync_cond, &sync_mutex) );
    //printf("ext awake...\n");
  } else {
    // if we were the first ext thread to arrive, wake the legion thread(s)
    if(ext_count == 1) {
      //printf("signalling\n");
      CHECK_PTHREAD( pthread_cond_broadcast(&sync_cond) );
    }
  }

  CHECK_PTHREAD( pthread_mutex_unlock(&sync_mutex) );
#ifdef TRACK_HANDSHAKE_TIMING
  if(timing_tracker) timing_tracker->log_event("ext_init_exit");
#endif
}

void ExtLegionHandshake::legion_init(void)
{
#ifdef TRACK_HANDSHAKE_TIMING
  if(timing_tracker) timing_tracker->log_event("legion_init_entry");
#endif
  CHECK_PTHREAD( pthread_mutex_lock(&sync_mutex) );
  
  if(!legion_count) {
    // first legion thread creates the events/queues for later synchronization, then arrive at initialization barrier
    ext_queue = new UserEvent[ext_queue_depth];
    for(int i = 0; i < ext_queue_depth; i++)
      ext_queue[i] = ((i || (state == IN_EXT)) ? 
                        UserEvent::create_user_event() :
                        UserEvent());

    legion_queue = new UserEvent[legion_queue_depth];
    for(int i = 0; i < legion_queue_depth; i++)
      legion_queue[i] = ((i || (state == IN_LEGION)) ?
                           UserEvent::create_user_event() :
                           UserEvent());
  }
  
  //printf("handshake %p: legion init - counts = L=%d, E=%d\n", this, legion_count, ext_count);
  legion_count++;

  if(ext_count == 0) {
    // no external threads have arrived, so sleep until one does
    //printf("legion sleeping...\n");
    CHECK_PTHREAD( pthread_cond_wait(&sync_cond, &sync_mutex) );
    //printf("legion awake...\n");
  } else {
    // if we were the first legion thread to arrive, wake the ext thread(s)
    if(legion_count == 1) {
      //printf("signalling\n");
      CHECK_PTHREAD( pthread_cond_broadcast(&sync_cond) );
    }
  }

  CHECK_PTHREAD( pthread_mutex_unlock(&sync_mutex) );
#ifdef TRACK_HANDSHAKE_TIMING
  if(timing_tracker) timing_tracker->log_event("legion_init_exit");
#endif
}

void ExtLegionHandshake::ext_handoff_to_legion(void)
{
  assert(state == IN_EXT);

  // we'll trigger the first event in the ext queue, but first, create a new event for the legion queue
  //  and shift it onto the end
  assert(legion_queue[0].has_triggered());
  for(int i = 1; i < legion_queue_depth; i++)
    legion_queue[i - 1] = legion_queue[i];
  legion_queue[legion_queue_depth - 1] = UserEvent::create_user_event();

#ifdef TRACK_HANDSHAKE_TIMING
  if(timing_tracker) timing_tracker->log_event("ext_handoff");
#endif
  state = IN_LEGION;
  ext_queue[0].trigger();
}

void ExtLegionHandshake::ext_wait_on_legion(void)
{
#ifdef TRACK_HANDSHAKE_TIMING
  if(timing_tracker) timing_tracker->log_event("ext_wait_entry");
#endif
  legion_queue[0].external_wait();
  assert(state == IN_EXT);
#ifdef TRACK_HANDSHAKE_TIMING
  if(timing_tracker) {
    timing_tracker->log_event("ext_wait_exit");
    timing_tracker->flush_events();
  }
#endif
}

void ExtLegionHandshake::legion_handoff_to_ext(void)
{
  assert(state == IN_LEGION);

  // we'll trigger the first event in the ext queue, but first, create a new event for the legion queue
  //  and shift it onto the end
  assert(ext_queue[0].has_triggered());
  for(int i = 1; i < ext_queue_depth; i++)
    ext_queue[i - 1] = ext_queue[i];
  ext_queue[ext_queue_depth - 1] = UserEvent::create_user_event();

#ifdef TRACK_HANDSHAKE_TIMING
  if(timing_tracker) timing_tracker->log_event("legion_handoff");
#endif
  state = IN_EXT;
  legion_queue[0].trigger();
}

void ExtLegionHandshake::legion_wait_on_ext(void)
{
#ifdef TRACK_HANDSHAKE_TIMING
  if(timing_tracker) timing_tracker->log_event("legion_wait_entry");
#endif
  ext_queue[0].wait();
  assert(state == IN_LEGION);
#ifdef TRACK_HANDSHAKE_TIMING
  if(timing_tracker) timing_tracker->log_event("legion_wait_exit");
#endif
}

#ifndef SHARED_LOWLEVEL
/*static*/ S3DRank*& S3DRank::get_local_rank(void)
{
  static S3DRank *s3d_rank = NULL;
  return s3d_rank;
}

/*static*/ S3DRank* S3DRank::get_rank(const Point<3> &pt, bool must_match)
{
  if (must_match)
    assert(pt == get_local_rank()->my_proc_id);
  return get_local_rank();
}
#else
/*static*/ std::map<Point<3>, S3DRank*, Point<3>::STLComparator>& S3DRank::get_local_ranks(void)
{
  static std::map<Point<3>, S3DRank*, Point<3>::STLComparator> s3d_ranks;
  return s3d_ranks;
}

/*static*/ pthread_mutex_t& S3DRank::get_local_mutex(void)
{
  static pthread_mutex_t s3d_mutex = PTHREAD_MUTEX_INITIALIZER;
  return s3d_mutex;
}

/*static*/ S3DRank* S3DRank::get_rank(const Point<3> &pt, bool must_match)
{
  return get_local_ranks()[pt];
}

/*static*/ bool S3DRank::has_rank(const Point<3> &pt)
{
  return get_local_ranks().count(pt);
}
#endif

S3DRank::S3DRank(const int *local_grid,
		 const int *global_grid,
		 const int *dist_grid,
		 const int *proc_grid,
		 const int *proc_id,
		 const int *p_vary_in_dims,
		 const int *p_nvar_tot,
		 const int *p_n_spec,
		 const int *p_n_reg,
		 const int *p_n_scalar,
		 const int *p_iorder,
		 const int *p_iforder,
		 const int *p_n_stages,
		 const int *p_n_steps,
		 const int *p_n_subranks,
		 const int *p_lagging_switch,
		 const int *p_lag_steps,
                 const int *p_i_react,
		 const double *_scale_dims
#ifdef USE_CEMA
		 , const int *p_cema_samples_per_rank
#endif
		 )
  : local_grid_size(local_grid), global_grid_size(global_grid), dist_grid_size(dist_grid),
    proc_grid_size(proc_grid), my_proc_id(proc_id),
    nvar_tot(*p_nvar_tot), n_spec(*p_n_spec), n_reg(*p_n_reg), n_scalar(*p_n_scalar),
    iorder(*p_iorder), iforder(*p_iforder), n_stages(*p_n_stages), n_steps(*p_n_steps),
    n_subranks(*p_n_subranks), 
    lagging_switch(*p_lagging_switch), lag_steps(*p_lag_steps), cur_stage(1), next_therm_stage(1), i_react(*p_i_react),
    vary_in_dims(p_vary_in_dims), subrank_bounds(0), compression(0),
    cur_handback(1), next_handback(1), did_last_handback(1),
    cur_legion_stage(0), 
    erk_stages(0), erk_alpha(0), erk_beta(0), erk_error(0)
#ifdef USE_CEMA
  , cema_samples_per_rank(*p_cema_samples_per_rank)
#endif
{
  // physical dimension scaling factors
  for(int i = 0; i < 3; i++)
    scale_dims[i] = _scale_dims[i];

  // sanity check that we're using the right mechanism (well, at least one with
  //  the right number of species
  if(*p_n_spec != NUM_SPECIES) {
    fprintf(stderr, "ERROR!  Species count mismatch (rhsf = %zd, caller = %d)\n",
	    NUM_SPECIES, *p_n_spec);
    assert(false);
  }

  if(!get_explicit_ghost_cells()) {
    if(dist_grid_size != Point<3>::ONES()) {
      printf("disabling hierarchical distribution because ghost cells are disabled\n");
      dist_grid_size = Point<3>::ONES();
    }
  }

  cur_data = 0;
  handshake = new ExtLegionHandshake(ExtLegionHandshake::IN_EXT);
  bzero(&cpe_data, sizeof(cpe_data));

  bool create_runtime = false;
#ifndef SHARED_LOWLEVEL
  get_local_rank() = this;
  create_runtime = true;
#else
  CHECK_PTHREAD( pthread_mutex_lock(&get_local_mutex()) );
  get_local_ranks()[my_proc_id] = this;
  // if we're the last to be added to the map, we'll create the runtime
  printf("have %zd out of %d ranks added\n", get_local_ranks().size(), Rect<3>(Point<3>::ONES(), proc_grid_size).volume());
  if(((int)get_local_ranks().size()) == Rect<3>(Point<3>::ONES(), proc_grid_size).volume())
    create_runtime = true;
  CHECK_PTHREAD( pthread_mutex_unlock(&get_local_mutex()) );
#endif

  if(create_runtime) {
    HighLevelRuntime::set_registration_callback(create_mappers);
    HighLevelRuntime::set_top_level_task_id(TOP_LEVEL_TASK_ID);
    HighLevelRuntime::register_legion_task<top_level_task>(TOP_LEVEL_TASK_ID, Processor::LOC_PROC,
                                                           true/*single*/, false/*index*/,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(), "top_level_task");
    HighLevelRuntime::register_legion_task<MPIConnectResult, connect_mpi_task>(CONNECT_MPI_TASK_ID, Processor::LOC_PROC,
                                                           false/*single*/, true/*index*/,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "connect_mpi_task");
    HighLevelRuntime::register_legion_task<update_mappers_task>(UPDATE_MAPPERS_TASK_ID, Processor::LOC_PROC,
                                                           false/*single*/, true/*index*/,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "update_mappers_task");
    HighLevelRuntime::register_legion_task<update_subrank_bounds_task>(UPDATE_SUBRANK_BOUNDS_TASK_ID, Processor::LOC_PROC,
                                                           false/*single*/, true/*index*/,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "update_subrank_bounds_task");
#ifndef REMOVE_OLD_BARRIER_EXCHANGE
    HighLevelRuntime::register_legion_task<create_ghost_regions_task>(CREATE_GHOST_REGIONS_TASK_ID, Processor::LOC_PROC,
                                                           true/*single*/, false/*index*/,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "create_ghost_regions_task");
    HighLevelRuntime::register_legion_task<gather_barriers_task>(GATHER_BARRIERS_TASK_ID, Processor::LOC_PROC,
                                                           true/*single*/, false/*index*/,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "gather_barriers_task");
#endif
#if 0
    HighLevelRuntime::register_legion_task<ghost_regions_fixup_task>(GHOST_REGIONS_FIXUP_TASK_ID, Processor::LOC_PROC,
                                                           true/*single*/, false/*index*/,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "ghost_regions_fixup_task");
#endif
    HighLevelRuntime::register_legion_task<distribute_task>(DISTRIBUTE_TASK_ID, Processor::LOC_PROC,
                                                           true/*single*/, false/*index */,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(), "distribute_task");
    HighLevelRuntime::register_legion_task<get_init_temp_task>(GET_INIT_TEMP_TASK_ID, Processor::LOC_PROC,
                                                           false/*single*/, true/*index*/,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "get_init_temp_task");
    HighLevelRuntime::register_legion_task<check_field_task>(CHECK_FIELD_TASK_ID, Processor::LOC_PROC,
                                                           false/*single*/, true/*index*/,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/),
                                                           "check_field_task");
    HighLevelRuntime::register_legion_task<dump_field_task>(DUMP_FIELD_TASK_ID, Processor::LOC_PROC,
                                                           false/*single*/, true/*index*/,
                                                           AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/),
                                                           "dump_field_task");
    HighLevelRuntime::register_legion_task<clear_field_task>(CLEAR_FIELD_TASK_ID, Processor::LOC_PROC,
							     false/*single*/, true/*index*/,
							     AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/),
							     "clear_field_task");

    // These are per-rank tasks for RHSF which are not sub-rankable
    RankableTaskHelper::register_task<AwaitMPITask, TimestepInfo>();
    RankableTaskHelper::register_task<HandoffToMPITask>();
    RankableTaskHelper::register_task<CalcEnergyTask>();
    RankableTaskHelper::register_task<CalcRHSVelTask>();
    if (get_field_centric())
    {
      if (get_all_gpu())
        RankableTaskHelper::register_hybrid_variants<CalcSpecPerFieldTask>();
      else
        RankableTaskHelper::register_task<CalcSpecPerFieldTask>();
      RankableTaskHelper::register_hybrid_variants<CalcYDiffFluxFieldTask>();
    }
    else
    {
      RankableTaskHelper::register_task<CalcSpecTask>();
      SubrankableTaskHelper::register_cpu_variants<CalcYDiffFluxTask>();
      //SubrankableTaskHelper::register_hybrid_variants<CalcYDiffFluxTask>();
    }

#ifdef UNION_HEATFLUX 
    SubrankableTaskHelper::register_cpu_variants<CalcHeatFluxTask>();
#else
    SubrankableTaskHelper::register_cpu_variants<CalcEnthalpyTask>();
    if (get_all_gpu())
      SubrankableTaskHelper::register_hybrid_variants<CalcShortHeatFluxTask>();
    else
      SubrankableTaskHelper::register_cpu_variants<CalcShortHeatFluxTask>();
#endif
    // Only have CPU variants of these sub-rankable tasks
    SubrankableTaskHelper::register_cpu_variants<CalcVolumeTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcTauTask>();
#ifdef USE_CEMA
#ifndef USE_GPU_KERNELS
    SubrankableTaskHelper::register_cpu_variants<SampleCEMATask>();
#else
    SubrankableTaskHelper::register_hybrid_variants<SampleCEMATask>();
#endif
    CalcCEMATask::register_task();
    SortCEMATask::register_task();
#endif
    // All the other sub-rankable tasks have both CPU and GPU variants
    // We only register GPU variants if we're allowed to.
#ifndef USE_GPU_KERNELS
    RankableTaskHelper::register_task<Sum3Task>();
    RankableTaskHelper::register_task<Sum4Task>();
    RankableTaskHelper::register_task<Sum3IntegrateTask>();
    RankableTaskHelper::register_task<Sum4IntegrateTask>();
    RankableTaskHelper::register_task<ScalarSum6IntegrateTask>();
    RankableTaskHelper::register_task<IntegrateSourceTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcSourceTermTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcTempTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcSpeciesTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcGammaTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcDiffusionTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcViscosityTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcConductivityTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcThermalTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcGetratesTask>();
    SubrankableTaskHelper::register_cpu_variants<CalcScalarFluxTask>();
    CalcStencilTask::register_cpu_variants();
    RankableTaskHelper::register_task<SumFieldTask, double>();
    RankableTaskHelper::register_task<CalcImposedPressureTask, double>();
#else
    SubrankableTaskHelper::register_hybrid_variants<CalcSpeciesTask>();
    SubrankableTaskHelper::register_hybrid_variants<CalcGammaTask>();
    RankableTaskHelper::register_hybrid_variants<Sum3Task>();
    RankableTaskHelper::register_hybrid_variants<Sum4Task>();
    RankableTaskHelper::register_hybrid_variants<Sum3IntegrateTask>();
    RankableTaskHelper::register_hybrid_variants<Sum4IntegrateTask>();
    RankableTaskHelper::register_hybrid_variants<ScalarSum6IntegrateTask>();
    RankableTaskHelper::register_hybrid_variants<IntegrateSourceTask>();
    SubrankableTaskHelper::register_hybrid_variants<CalcSourceTermTask>();
    SubrankableTaskHelper::register_hybrid_variants<CalcTempTask>();
    SubrankableTaskHelper::register_hybrid_variants<CalcDiffusionTask>();
    SubrankableTaskHelper::register_hybrid_variants<CalcViscosityTask>();
    SubrankableTaskHelper::register_hybrid_variants<CalcConductivityTask>();
    SubrankableTaskHelper::register_hybrid_variants<CalcThermalTask>();
    SubrankableTaskHelper::register_hybrid_variants<CalcGetratesTask>();
    SubrankableTaskHelper::register_hybrid_variants<CalcScalarFluxTask>();
    CalcStencilTask::register_hybrid_variants();
    RankableTaskHelper::register_hybrid_variants<SumFieldTask, double>();
    RankableTaskHelper::register_task<CalcImposedPressureTask, double>();
#endif

    HighLevelRuntime::register_reduction_op<ReductionSum<double> >(REDOP_SUM_DOUBLE);

    int argc = 0;
    char **argv = new char *[100];
    for(int i = 0; i < dummy_argc; i++)
      argv[argc++] = (char *)(dummy_args[i]);
    if(getenv("RHSF_ARGS") != 0) {
      char *s = strdup(getenv("RHSF_ARGS"));
      char *saveptr;
      char *p = strtok_r(s, " ", &saveptr);
      while(p) {
	argv[argc++] = strdup(p);
	p = strtok_r(0, " ", &saveptr);
      }
      free(s);
    }
    argv[argc] = 0;
    HighLevelRuntime::start(argc, argv, true);
  }
}

S3DRank::~S3DRank(void)
{
#ifdef SHARED_LOWLEVEL
  bool shutdown_runtime = true;
  // If we're the last one, tell the legion runtime to shutdown
  CHECK_PTHREAD(pthread_mutex_lock(&get_local_mutex()));
  get_local_ranks().erase(my_proc_id);
  if (!get_local_ranks().empty())
    shutdown_runtime = false;
  CHECK_PTHREAD(pthread_mutex_unlock(&get_local_mutex()));
#endif
  // Wait until the runtime shuts itself down
  HighLevelRuntime::wait_for_shutdown();

  if(compression)
    delete compression;
}

void S3DRank::complete_configure(void)
{
  //printf("MPI thread waiting on Legion thread...\n");
  handshake->ext_init();
  //printf("completed initialization\n");
}

void S3DRank::begin_rhsf_step(RHSFArrays *step_data, double timestep)
{
  cur_data = step_data;
  cur_time = -1;
  cur_timestep = timestep;
  handshake->ext_handoff_to_legion();
}

void S3DRank::finish_rhsf_step(void)
{
  handshake->ext_wait_on_legion();
  cur_legion_stage++;
}

void S3DRank::begin_integration(IntegrationArrays *step_data, double time_accum, double timestep, 
                                int handback)
{
  cur_integ_data = step_data;
  cur_time = time_accum;
  cur_timestep = timestep;
  next_handback = handback;
  handshake->ext_handoff_to_legion();
}

#include <sys/time.h>
#include <sys/resource.h>

void S3DRank::finish_integration(void)
{
  //bool print_usage = ((cur_legion_stage % 100) == 0);
  const bool print_usage = false;
  handshake->ext_wait_on_legion();
  cur_legion_stage++;
  if (print_usage) {
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);
    fprintf(stdout,"Usage of rank (%d,%d,%d) at step %d: %ld\n",
	my_proc_id[0], my_proc_id[1], my_proc_id[2], 
	cur_legion_stage, usage.ru_maxrss);
    fflush(stdout); 
  }
}

template <typename T>
static T *memdup(const T *data, size_t num_elmts)
{
  T *newdata = new T[num_elmts];
  for(size_t i = 0; i < num_elmts; i++)
    newdata[i] = data[i];
  return newdata;
}

void S3DRank::provide_cpe_data(int num_pts, 
			       const double *enth_aa, const double *enth_bb,
			       const double *cp_aa, const double *cp_bb)
{
  cpe_data.num_pts = num_pts;
  cpe_data.enth_aa = memdup(enth_aa, n_spec * num_pts);
  cpe_data.enth_bb = memdup(enth_bb, n_spec * num_pts);
  cpe_data.cp_aa = memdup(cp_aa, n_spec * num_pts);
  cpe_data.cp_bb = memdup(cp_bb, n_spec * num_pts);
}

void S3DRank::provide_initial_temperature(const double *temperature)
{
  temp_data = temperature;
  handshake->ext_handoff_to_legion();
  //printf("temperature handed to legion - waiting for ack\n");
  handshake->ext_wait_on_legion();
}

void S3DRank::configure_erk(int stages, const double *alpha,
                            const double *beta, const double *error,
                            const double *small)
{
  // free old data, if present
  if(erk_alpha) delete[] erk_alpha;
  if(erk_beta) delete[] erk_beta;
  if(erk_error) delete[] erk_error;

  erk_stages = stages;
  if(stages > 0) {
    erk_alpha = new double[stages];
    erk_beta = new double[stages];
    erk_error = new double[stages];
    for(int i = 0; i < stages; i++) {
      erk_alpha[i] = alpha[i];
      erk_beta[i] = beta[i];
      erk_error[i] = error[i];
    }
  } else {
    erk_alpha = 0;
    erk_beta = 0;
    erk_error = 0;
  }
  // Read out the small value
  small_value = *small;
}

S3DRank::RHSFArrays::RHSFArrays(void)
  : q(0), volume(0), yspecies(0), grad_u(0), grad_T(0), grad_ys(0), temp_in(0),
    temperature(0), avmolwt(0), cpmix(0), gamma(0), pressure(0), viscosity(0),
    lambda(0), ds_mixavg(0), rs_therm_diff(0), tau(0), h_spec(0), heatflux(0),
    diffusion(0), mixmw(0), grad_mixmw(0), ydiffflux(0), rr_r(0), rhs_cmp(0),
    rhs(0)
{}

void S3DRank::RHSFArrays::release_memory(void)
{
  delete[] q;
  delete[] volume;
  delete[] yspecies;
  delete[] grad_u;
  delete[] grad_T;
  delete[] grad_ys;
  delete[] temp_in;
  delete[] temperature;
  delete[] avmolwt;
  delete[] cpmix;
  delete[] gamma;
  delete[] pressure;
  delete[] viscosity;
  delete[] lambda;
  delete[] ds_mixavg;
  delete[] rs_therm_diff;
  delete[] tau;
  delete[] h_spec;
  delete[] heatflux;
  delete[] diffusion;
  delete[] mixmw;
  delete[] grad_mixmw;
  delete[] ydiffflux;
  delete[] rr_r;
  delete[] rhs_cmp;
  if(rhs != rhs_cmp)
    delete[] rhs;

  q = 0;
  volume = 0;
  yspecies = 0;
  grad_u = 0;
  grad_T = 0;
  grad_ys = 0;
  temp_in = 0;
  temperature = 0;
  avmolwt = 0;
  cpmix = 0;
  gamma = 0;
  pressure = 0;
  viscosity = 0;
  lambda = 0;
  ds_mixavg = 0;
  rs_therm_diff = 0;
  tau = 0;
  h_spec = 0;
  heatflux = 0;
  diffusion = 0;
  mixmw = 0;
  grad_mixmw = 0;
  ydiffflux = 0;
  rr_r = 0;
  rhs_cmp = 0;
  rhs = 0;
}

S3DRank::IntegrationArrays::IntegrationArrays(void)
  : q(0), q_err(0)
{}



