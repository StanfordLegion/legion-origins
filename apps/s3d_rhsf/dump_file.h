#ifndef _DUMP_FILE_H_
#define _DUMP_FILE_H_

#include <string.h>
#include <assert.h>

#ifdef USE_ZLIB
#include <zlib.h>
#endif

#include "arrays.h"

using namespace LegionRuntime::Arrays;

class DumpedArray;

class DumpFile {
 public:
  DumpFile(const char *_filename);
  ~DumpFile(void);

  DumpedArray *get_next_array(void);

 protected:
  friend class DumpedArray;

  void load_bytes(void *dst, off_t offset, size_t bytes);

#ifdef USE_ZLIB
  gzFile f;
#else
  int fd;
#endif
  off_t cur_pos, act_pos;
};

class DumpedArray {
 public:
  DumpedArray(DumpFile *_dump_file,
	      const char *_name, int _rank, const int *_dims)
    : dump_file(_dump_file), bytes(0), rank(_rank)
  {
    strncpy(name, _name, 20);
    for(int i = 0; i < 20; i++)
      if(name[i] == ' ') {
	name[i] = 0;
	break;
      }

    dims = new int[rank];
    for(int i = 0; i < rank; i++)
      dims[i] = _dims[i];
  }

  ~DumpedArray(void) { delete[] dims; }

  void add_segment(off_t seg_pos, off_t seg_size)
  {
    segs_pos.push_back(seg_pos);
    segs_size.push_back(seg_size);
    bytes += seg_size;
  }
  
  const char *get_name(void) const { return name; }

  int get_rank(void) const { return rank; }

  template <unsigned DIM>
  Point<DIM> get_dims(void) const { return Point<DIM>(dims); }

  template <typename T>
  T *load_data(void) const
  {
    int elements = 1;
    for(int i = 0; i < rank; i++) elements *= dims[i];
    off_t skip = 20 + (rank + 1)*sizeof(int);
    assert((off_t)(skip + elements*sizeof(T)) == bytes);
    T *ptr = new T[elements];
    char *cptr = (char *)ptr;
    for(unsigned i = 0; i < segs_pos.size(); i++) {
#ifndef NO_COMPUTE
      dump_file->load_bytes(cptr, 
			    segs_pos[i] + skip,
			    segs_size[i] - skip);
#endif
      cptr += segs_size[i] - skip;
      skip = 0;  // only skip in the first piece
    }
    return ptr;
  }

 protected:
  DumpFile *dump_file;
  off_t bytes;
  std::vector<off_t> segs_pos;
  std::vector<off_t> segs_size;
  char name[20];
  int rank;
  int *dims;
};

#endif
