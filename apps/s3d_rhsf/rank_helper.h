
#ifndef _HELPER_H_
#define _HELPER_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"
#include "arrays.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

namespace RankableTaskHelper {

  template<typename T>
  void dispatch_task(T &launcher, FutureMap& fm, Context ctx, 
                     HighLevelRuntime *runtime, int stage)
  {
    launcher.tag |= (RHSF_MAPPER_FORCE_RANK_MATCH | (stage & RHSF_MAPPER_STAGE_MASK));
    fm = runtime->execute_index_space(ctx, launcher);
    if (S3DRank::get_perform_waits())
      fm.wait_all_results();
    // See if we need to perform any checks
    if (S3DRank::get_perform_all_checks())
      launcher.launch_check_fields(ctx, runtime);
  }

  template<typename T>
  void dispatch_task(T &launcher, Context ctx, 
                     HighLevelRuntime *runtime, int stage)
  {
    // just a wrapper that calls the FutureMap one (and then ignores the FutureMap)
    FutureMap fm;
    dispatch_task(launcher, fm, ctx, runtime, stage);
  }

  // reduction version
  template<typename T>
  Future dispatch_task(T &launcher, ReductionOpID redop_id, Context ctx, 
		       HighLevelRuntime *runtime, int stage)
  {
    launcher.tag |= (RHSF_MAPPER_FORCE_RANK_MATCH | (stage & RHSF_MAPPER_STAGE_MASK));
    Future f = runtime->execute_index_space(ctx, launcher, redop_id);
    if (S3DRank::get_perform_waits())
      f.get_void_result();
    // See if we need to perform any checks
    if (S3DRank::get_perform_all_checks())
      launcher.launch_check_fields(ctx, runtime);
    return f;
  }

  template<typename T>
  void base_cpu_wrapper(const Task *task,
                        const std::vector<PhysicalRegion> &regions,
                        Context ctx, HighLevelRuntime *runtime)
  {
    Point<3> rank_point = task->index_point.get_point<3>();
    if (S3DRank::get_show_progress())
      printf("%s CPU task, pt = (%d, %d, %d), proc = " IDFMT "\n", T::TASK_NAME,
             rank_point[0], rank_point[1], rank_point[2],
             runtime->get_executing_processor(ctx).id);

    S3DRank *rank = S3DRank::get_rank(rank_point, true);

    Blockify<3> grid2proc_map(rank->local_grid_size);
    Rect<3> my_subgrid_bounds = grid2proc_map.preimage(rank_point);

    T::cpu_base_impl(rank, task, my_subgrid_bounds, task->regions, regions, ctx, runtime);
  }

  template<typename T, typename RT>
  RT base_cpu_wrapper_wret(const Task *task,
			   const std::vector<PhysicalRegion> &regions,
			   Context ctx, HighLevelRuntime *runtime)
  {
    Point<3> rank_point = task->index_point.get_point<3>();
    if (S3DRank::get_show_progress())
      printf("%s CPU task, pt = (%d, %d, %d), proc = " IDFMT "\n", T::TASK_NAME,
             rank_point[0], rank_point[1], rank_point[2],
             runtime->get_executing_processor(ctx).id);

    S3DRank *rank = S3DRank::get_rank(rank_point, true);

    Blockify<3> grid2proc_map(rank->local_grid_size);
    Rect<3> my_subgrid_bounds = grid2proc_map.preimage(rank_point);

    return T::cpu_base_impl(rank, task, my_subgrid_bounds, task->regions, regions, ctx, runtime);
  }

  template<typename T>
  void base_gpu_wrapper(const Task *task,
                        const std::vector<PhysicalRegion> &regions,
                        Context ctx, HighLevelRuntime *runtime)
  {
    Point<3> rank_point = task->index_point.get_point<3>();
    if (S3DRank::get_show_progress())
      printf("%s GPU task, pt = (%d, %d, %d), proc = " IDFMT "\n", T::TASK_NAME,
             rank_point[0], rank_point[1], rank_point[2],
             runtime->get_executing_processor(ctx).id);

    S3DRank *rank = S3DRank::get_rank(rank_point, true);

    Blockify<3> grid2proc_map(rank->local_grid_size);
    Rect<3> my_subgrid_bounds = grid2proc_map.preimage(rank_point);

    T::gpu_base_impl(rank, task, my_subgrid_bounds, task->regions, regions, ctx, runtime);
  }

  template<typename T, typename RT>
  RT base_gpu_wrapper_wret(const Task *task,
			   const std::vector<PhysicalRegion> &regions,
			   Context ctx, HighLevelRuntime *runtime)
  {
    Point<3> rank_point = task->index_point.get_point<3>();
    if (S3DRank::get_show_progress())
      printf("%s GPU task, pt = (%d, %d, %d), proc = " IDFMT "\n", T::TASK_NAME,
             rank_point[0], rank_point[1], rank_point[2],
             runtime->get_executing_processor(ctx).id);

    S3DRank *rank = S3DRank::get_rank(rank_point, true);

    Blockify<3> grid2proc_map(rank->local_grid_size);
    Rect<3> my_subgrid_bounds = grid2proc_map.preimage(rank_point);

    return T::gpu_base_impl(rank, task, my_subgrid_bounds, task->regions, regions, ctx, runtime);
  }

  template<typename T>
  void register_task(void)
  {
    HighLevelRuntime::register_legion_task<base_cpu_wrapper<T> >(T::TASK_ID, Processor::LOC_PROC,
                                                                 false/*single*/, true/*index*/,
                                                                 RHSF_CPU_LEAF_VARIANT,
                                                                 TaskConfigOptions(T::CPU_BASE_LEAF),
                                                                 T::TASK_NAME);
  }

  template<typename T, typename RT>
  void register_task(void)
  {
    HighLevelRuntime::register_legion_task<RT, base_cpu_wrapper_wret<T, RT> >((TaskID)T::TASK_ID, Processor::LOC_PROC,
                                                                 false/*single*/, true/*index*/,
                                                                 RHSF_CPU_LEAF_VARIANT,
                                                                 TaskConfigOptions(T::CPU_BASE_LEAF),
                                                                 T::TASK_NAME);
  }

  template<typename T>
  void register_hybrid_variants(void)
  {
    HighLevelRuntime::register_legion_task<base_cpu_wrapper<T> >(T::TASK_ID, Processor::LOC_PROC,
                                                                 false/*single*/, true/*index*/,
                                                                 RHSF_CPU_LEAF_VARIANT,
                                                                 TaskConfigOptions(T::CPU_BASE_LEAF),
                                                                 T::TASK_NAME);
    HighLevelRuntime::register_legion_task<base_gpu_wrapper<T> >(T::TASK_ID, Processor::TOC_PROC,
                                                                 false/*single*/, true/*index*/,
                                                                 RHSF_GPU_LEAF_VARIANT,
                                                                 TaskConfigOptions(T::GPU_BASE_LEAF),
                                                                 T::TASK_NAME);
  }

  template<typename T, typename RT>
  void register_hybrid_variants(void)
  {
    HighLevelRuntime::register_legion_task<RT, base_cpu_wrapper_wret<T, RT> >(T::TASK_ID, Processor::LOC_PROC,
                                                                 false/*single*/, true/*index*/,
                                                                 RHSF_CPU_LEAF_VARIANT,
                                                                 TaskConfigOptions(T::CPU_BASE_LEAF),
                                                                 T::TASK_NAME);
    HighLevelRuntime::register_legion_task<RT, base_gpu_wrapper_wret<T, RT> >(T::TASK_ID, Processor::TOC_PROC,
                                                                 false/*single*/, true/*index*/,
                                                                 RHSF_GPU_LEAF_VARIANT,
                                                                 TaskConfigOptions(T::GPU_BASE_LEAF),
                                                                 T::TASK_NAME);
  }
};

namespace SubrankableTaskHelper {

  template<typename T>
  void dispatch_task(T &launcher, Context ctx, HighLevelRuntime *runtime, int stage)
  {
    // Check to see if we should hoist the sub-ranks into this context
    if (S3DRank::get_hoist_subtasks() && (launcher.rank->n_subranks > 0))
    {
      S3DRank *rank = launcher.rank; 
      // For right now we'll only allow hoisting if our distribute
      // task is only responsible for launching a single sub-task on
      // the local node since our mapper isn't smart enough to handle
      // the other case at the moment.
      assert(rank->proc_grid_bounds.volume() == 1);
      // Since we know there is only one point we
      // know what our rank point is.
      Point<3> rank_point = rank->proc_grid_bounds.lo; 
      // Make a new launcher for the sub-rank index space launch
      T sub_launcher(rank, Domain::from_rect<1>(Rect<1>(0, rank->n_subranks-1)),
                     TaskArgument(&rank_point, sizeof(rank_point)),
                     launcher.argument_map, Predicate::TRUE_PRED, false/*must*/,
                     launcher.map_id, launcher.tag | (stage & RHSF_MAPPER_STAGE_MASK), 
                     false/*add requirements*/);
      if (!launcher.region_requirements.empty())
      {
        // Compute the region requirements for the sub-rank launch
        // based on the current region requirements. Make a small assumption
        // that all region requirements are projection requirements.
        assert(launcher.region_requirements[0].handle_type == PART_PROJECTION);
        Rect<3> color_space = runtime->get_index_partition_color_space(ctx, 
          launcher.region_requirements[0].partition.get_index_partition()).template get_rect<3>(); 
        CArrayLinearization<3> color_space_lin(color_space);
        Color subregion_color = (Color)(color_space_lin.image(rank_point)); 
        for (std::vector<RegionRequirement>::const_iterator it = 
              launcher.region_requirements.begin(); it !=
              launcher.region_requirements.end(); it++)
        {
          assert(it->handle_type == PART_PROJECTION);
          LogicalRegion lr = 
            runtime->get_logical_subregion_by_color(ctx, it->partition, subregion_color);
          LogicalPartition lp = runtime->get_logical_partition_by_color(ctx, lr, PID_SUBRANK);

          RegionRequirement sub_rr(lp, 0, it->privilege_fields, it->instance_fields,
                                    it->privilege, it->prop, it->parent);
          sub_launcher.add_region_requirement(sub_rr);
        }
      }
      if (!launcher.futures.empty())
        sub_launcher.futures = launcher.futures;
      // Now we can do the launch
      FutureMap fm = runtime->execute_index_space(ctx, sub_launcher);
      if (S3DRank::get_perform_waits())
        fm.wait_all_results();
    }
    else
    {
      // Set the additional tags on the launcher
      launcher.tag |= (RHSF_MAPPER_FORCE_RANK_MATCH | RHSF_MAPPER_SUBRANKABLE | 
                        (stage & RHSF_MAPPER_STAGE_MASK));
      if (launcher.rank->proc_grid_bounds.volume() == 1)
        launcher.tag |= RHSF_MAPPER_SINGLE_RANK;
      FutureMap fm = runtime->execute_index_space(ctx, launcher);
      if (S3DRank::get_perform_waits())
        fm.wait_all_results();
    }
    // See if we need to perform any checks
    if (S3DRank::get_perform_all_checks())
      launcher.launch_check_fields(ctx, runtime);
  }

  template<typename T>
  void inner_task_wrapper(const Task *task,
                          const std::vector<PhysicalRegion> &regions,
                          Context ctx, HighLevelRuntime *runtime)
  {
    Point<3> rank_point = task->index_point.get_point<3>();

    if (S3DRank::get_show_progress())
      printf("%s INNER task, pt = (%d, %d, %d), proc = " IDFMT "\n", T::TASK_NAME,
	     rank_point[0], rank_point[1], rank_point[2],
             runtime->get_executing_processor(ctx).id);

    S3DRank *rank = S3DRank::get_rank(rank_point, true);

    Blockify<3> grid2proc_map(rank->local_grid_size);
    //Rect<3> my_subgrid_bounds = grid2proc_map.preimage(rank_point);

    T launcher(rank, Domain::from_rect<1>(Rect<1>(0, rank->n_subranks - 1)),
               TaskArgument(&rank_point, sizeof(rank_point)),
               ArgumentMap(), Predicate::TRUE_PRED, false/*must*/,
               task->map_id, 0/*tag*/, false/*add requirements*/);

    // make an partition region requirement for each input region requirement
    for(unsigned i = 0; i < task->regions.size(); i++) {
      LogicalRegion lr = task->regions[i].region;
      IndexPartition index_part = runtime->get_index_partition(ctx, lr.get_index_space(), PID_SUBRANK);
      LogicalPartition lp = runtime->get_logical_partition(ctx, lr, index_part);
      
      RegionRequirement sub_rr(lp, 0, task->regions[i].privilege_fields, task->regions[i].instance_fields,
                               task->regions[i].privilege, task->regions[i].prop, lr);

      launcher.add_region_requirement(sub_rr);
    }
    if (!task->futures.empty())
      launcher.futures = task->futures;

    // unmap all the regions that the parent task had access to
    runtime->unmap_all_regions(ctx);

    FutureMap fm = runtime->execute_index_space(ctx, launcher);
    if (S3DRank::get_perform_waits())
      fm.wait_all_results();
  }

  template<typename T>
  void base_cpu_wrapper(const Task *task,
                        const std::vector<PhysicalRegion> &regions,
                        Context ctx, HighLevelRuntime *runtime)
  {
    // Handle two different cases depending on whether we were
    // called directly or whether we were called as a sub-rank task
    if (task->index_point.get_dim() == 1)
    {
      // We we're called as a sub-rank
      assert(task->arglen == sizeof(Point<3>));
      Point<3> rank_point = *(Point<3> *)task->args;
      int subrank_point = task->index_point.get_point<1>()[0];

      if (S3DRank::get_show_progress())
        printf("%s CPU task, pt = (%d, %d, %d), subrank = %d, proc = " IDFMT "\n", T::TASK_NAME,
               rank_point[0], rank_point[1], rank_point[2], subrank_point,
               runtime->get_executing_processor(ctx).id);

      S3DRank *rank = S3DRank::get_rank(rank_point, true);

      assert(subrank_point < rank->n_subranks);
      Rect<3> my_subgrid_bounds = rank->subrank_bounds[subrank_point];

      T::cpu_base_impl(rank, task, my_subgrid_bounds, task->regions, regions, ctx, runtime);
    }
    else
    {
      // We we're called directly
      Point<3> rank_point = task->index_point.get_point<3>();
      if (S3DRank::get_show_progress())
        printf("%s CPU task, pt = (%d, %d, %d), proc = " IDFMT "\n", T::TASK_NAME,
               rank_point[0], rank_point[1], rank_point[2],
               runtime->get_executing_processor(ctx).id);

      S3DRank *rank = S3DRank::get_rank(rank_point, true);

      Blockify<3> grid2proc_map(rank->local_grid_size);
      Rect<3> my_subgrid_bounds = grid2proc_map.preimage(rank_point);

      T::cpu_base_impl(rank, task, my_subgrid_bounds, task->regions, regions, ctx, runtime);
    }
  }
  
  template<typename T>
  void base_gpu_wrapper(const Task *task,
                        const std::vector<PhysicalRegion> &regions,
                        Context ctx, HighLevelRuntime *runtime)
  {
    // Handle two different cases depending on whether we were
    // called directly or whether we were called as a sub-rank task
    if (task->arglen == sizeof(Point<3>))
    {
      // We were called as a sub-rank
      Point<3> rank_point = *(Point<3> *)task->args;
      int subrank_point = task->index_point.get_point<1>()[0];

      if (S3DRank::get_show_progress())
        printf("%s GPU task, pt = (%d, %d, %d), subrank = %d, proc = " IDFMT "\n", T::TASK_NAME,
               rank_point[0], rank_point[1], rank_point[2], subrank_point,
               runtime->get_executing_processor(ctx).id);

      S3DRank *rank = S3DRank::get_rank(rank_point, true);

      assert(subrank_point < rank->n_subranks);
      Rect<3> my_subgrid_bounds = rank->subrank_bounds[subrank_point];

      T::gpu_base_impl(rank, task, my_subgrid_bounds, task->regions, regions, ctx, runtime);
    }
    else
    {
      // We we're called directly
      Point<3> rank_point = task->index_point.get_point<3>();
      if (S3DRank::get_show_progress())
        printf("%s GPU task, pt = (%d, %d, %d), proc = " IDFMT "\n", T::TASK_NAME,
               rank_point[0], rank_point[1], rank_point[2],
               runtime->get_executing_processor(ctx).id);

      S3DRank *rank = S3DRank::get_rank(rank_point, true);

      Blockify<3> grid2proc_map(rank->local_grid_size);
      Rect<3> my_subgrid_bounds = grid2proc_map.preimage(rank_point);

      T::gpu_base_impl(rank, task, my_subgrid_bounds, task->regions, regions, ctx, runtime);
    }
  }

  template<typename T>
  void register_cpu_variants(void)
  {
    HighLevelRuntime::register_legion_task<inner_task_wrapper<T> >(T::TASK_ID,
                                                                   Processor::LOC_PROC,
                                                                   false/*single*/, true/*index*/,
                                                                   RHSF_INNER_VARIANT,
                                                                   TaskConfigOptions(false/*leaf*/,
                                                                                     true/*inner*/),
                                                                   T::TASK_NAME);
    HighLevelRuntime::register_legion_task<base_cpu_wrapper<T> >(T::TASK_ID,
                                                                 Processor::LOC_PROC,
                                                                 false/*single*/, true/*index*/,
                                                                 RHSF_CPU_LEAF_VARIANT,
                                                                 TaskConfigOptions(T::CPU_BASE_LEAF),
                                                                 T::TASK_NAME);
  }                

  template<typename T>
  void register_hybrid_variants(void)
  {
    HighLevelRuntime::register_legion_task<inner_task_wrapper<T> >(T::TASK_ID,
                                                                   Processor::LOC_PROC,
                                                                   false/*single*/, true/*index*/,
                                                                   RHSF_INNER_VARIANT,
                                                                   TaskConfigOptions(false/*leaf*/,
                                                                                     true/*inner*/),
                                                                   T::TASK_NAME);
    HighLevelRuntime::register_legion_task<base_cpu_wrapper<T> >(T::TASK_ID,
                                                                 Processor::LOC_PROC,
                                                                 false/*single*/, true/*index*/,
                                                                 RHSF_CPU_LEAF_VARIANT,
                                                                 TaskConfigOptions(T::CPU_BASE_LEAF),
                                                                 T::TASK_NAME);
    HighLevelRuntime::register_legion_task<base_gpu_wrapper<T> >(T::TASK_ID,
                                                                 Processor::TOC_PROC,
                                                                 false/*single*/, true/*index*/,
                                                                 RHSF_GPU_LEAF_VARIANT,
                                                                 TaskConfigOptions(T::GPU_BASE_LEAF),
                                                                 T::TASK_NAME);
  }
};

#endif
