
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <unistd.h>

#include "rhsf.h"
#include "rhsf_init.h"
#include "rhsf_impl.h"
#include "rhsf_mapper.h"

#include "calc_volume.h"
#include "calc_temp.h"
#include "calc_physics.h"
#include "calc_chem.h"
#include "calc_flux.h"
#include "calc_scalar.h"
#include "calc_pre.h"
#include "calc_stencil.h"
#include "calc_sum.h"
#ifdef USE_CEMA
#include "calc_cema.h"
#endif
#include "check_field.h"
#include "impose_pressure.h"
#include "rank_helper.h"

// Include Singe emitted header files
#include "getcoeffs.h"
#include "getrates.h"

// Singe header files for SSE
#ifdef USE_SSE_KERNELS
#include "sse_getcoeffs.h"
#include "sse_getrates.h"
#endif

// Singe header files for AVX
#ifdef USE_AVX_KERNELS
#include "avx_getcoeffs.h"
#include "avx_getrates.h"
#endif

// Stencil kernels
#include "cpu_stencils.h"
#ifdef USE_GPU_KERNELS
#include "gpu_stencils.h"
#endif

// Chemistry and physics for the GPU
#ifdef USE_GPU_KERNELS
#include "rhsf_gpu.h"
#endif

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

void clear_field(HighLevelRuntime *runtime, Context ctx, 
		 LogicalRegion lr, LogicalPartition lp, const Rect<3>& proc_grid_bounds,
		 FieldID fid, double clearval);

void rhsf_top(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
	      const Rect<3>& proc_grid_bounds, const Rect<3>& full_proc_grid_bounds,
	      IndexSpace is_grid, IndexSpace is_subdist, IndexPartition ip_top, 
	      FieldSpace fs_q, LogicalRegion lr_q, LogicalRegion lr_rhs,
	      FieldSpace fs_state, LogicalRegion lr_state,
	      FieldSpace fs_ghost, const LogicalRegion *lr_ghost,
              FieldSpace fs_int, LogicalRegion lr_int,
	      const std::vector<PhysicalRegion> &regions);

void integrate_top(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
		   const Rect<3>& proc_grid_bounds, const Rect<3>& full_proc_grid_bounds,
		   IndexSpace is_grid, IndexSpace is_subdist, IndexPartition ip_top, 
		   FieldSpace fs_q, LogicalRegion lr_q, LogicalRegion lr_rhs,
		   LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
		   FieldSpace fs_state, LogicalRegion lr_state,
		   FieldSpace fs_ghost, const LogicalRegion *lr_ghost,
		   FieldSpace fs_int, LogicalRegion lr_int,
		   const std::vector<PhysicalRegion> &regions);

void distribute_task(const Task *task,
                     const std::vector<PhysicalRegion> &regions,
                     Context ctx, HighLevelRuntime *runtime)
{
  printf("my proc = "IDFMT"\n", runtime->get_executing_processor(ctx).id);
  // we don't use any of the data directly
  runtime->unmap_all_regions(ctx);

  const RhsfDistributeArgs *d_args = (const RhsfDistributeArgs *)task->args;

  const IndexSpace& is_grid = d_args->is_grid;
  const IndexSpace& is_subdist = d_args->is_subdist;
  const IndexPartition& ip_top = d_args->ip_proc;
  const Rect<3>& proc_grid_bounds = d_args->dist_proc_subgrid;
  const Rect<3>& full_proc_grid_bounds = d_args->full_proc_subgrid;

  const FieldSpace& fs_ghost = d_args->fs_ghost;
  const LogicalRegion *lr_ghost = d_args->lr_ghost;

  S3DRank *rank = S3DRank::get_rank(proc_grid_bounds.lo, false);

#ifdef NEW_BARRIER_EXCHANGE
  do_barrier_exchange(task, regions, ctx, runtime, proc_grid_bounds);
#endif

  rank->global_sum_collective = d_args->global_sum_collective;

  // now the field space used by persistent state (temperature, thermal coefficients)
  FieldSpace fs_state = runtime->create_field_space(ctx);
  {
    FieldAllocator fsa_state = runtime->create_field_allocator(ctx, fs_state);
    fsa_state.allocate_field(sizeof(double), FID_TEMP);
    fsa_state.allocate_field(sizeof(double), FID_VISCOSITY);
    fsa_state.allocate_field(sizeof(double), FID_LAMBDA);
    for(int i = 0; i < rank->n_spec; i++)
      fsa_state.allocate_field(sizeof(double), FID_DS_MIXAVG(i));
    for(int i = 0; i < rank->n_spec; i++)
      fsa_state.allocate_field(sizeof(double), FID_RS_THERM_DIFF(i));
  }

  LogicalRegion lr_state = runtime->create_logical_region(ctx, is_subdist, fs_state);

  // now the field space used by q, rhs, q_err
  FieldSpace fs_q = runtime->create_field_space(ctx);
  {
    FieldAllocator fsa_q = runtime->create_field_allocator(ctx, fs_q);
    fsa_q.allocate_field(sizeof(double), FID_RHO_U);
    fsa_q.allocate_field(sizeof(double), FID_RHO_V);
    fsa_q.allocate_field(sizeof(double), FID_RHO_W);
    fsa_q.allocate_field(sizeof(double), FID_RHO);
    fsa_q.allocate_field(sizeof(double), FID_RHO_E);
    for(int i = 0; i < rank->n_spec - 1; i++)
      fsa_q.allocate_field(sizeof(double), FID_RHO_Y(i));
    for(int i = 0; i < rank->n_scalar; i++)
      fsa_q.allocate_field(sizeof(double), FID_SCALAR(i));
  }

  // q and rhs are two different logical regions using these fields
  LogicalRegion lr_q = runtime->create_logical_region(ctx, is_subdist, fs_q);
  LogicalRegion lr_rhs = runtime->create_logical_region(ctx, is_subdist, fs_q);
  LogicalRegion lr_qerr = runtime->create_logical_region(ctx, is_subdist, fs_q);
  LogicalRegion lr_qtmp = runtime->create_logical_region(ctx, is_subdist, fs_q);
      
  // create the int field space and region
  FieldSpace fs_int = runtime->create_field_space(ctx);
  {
    FieldAllocator fsa_int = runtime->create_field_allocator(ctx, fs_int);
    fsa_int.allocate_field(sizeof(double), FID_VOLUME);
    fsa_int.allocate_field(sizeof(double), FID_AVMOLWT);
    fsa_int.allocate_field(sizeof(double), FID_MIXMW);
    fsa_int.allocate_field(sizeof(double), FID_VEL_X);
    fsa_int.allocate_field(sizeof(double), FID_VEL_Y);
    fsa_int.allocate_field(sizeof(double), FID_VEL_Z);
    fsa_int.allocate_field(sizeof(double), FID_CPMIX);
    fsa_int.allocate_field(sizeof(double), FID_GAMMA);
    fsa_int.allocate_field(sizeof(double), FID_PRESSURE);
    fsa_int.allocate_field(sizeof(double), FID_SOURCE_TERM);
#ifdef REDUNDANT_WORK
    fsa_int.allocate_field(sizeof(double), FID_AVMOLWT_ALT);
    fsa_int.allocate_field(sizeof(double), FID_MIXMW_ALT);
#endif

    fsa_int.allocate_field(sizeof(double), FID_TAU_XX);
    fsa_int.allocate_field(sizeof(double), FID_TAU_XY);
    fsa_int.allocate_field(sizeof(double), FID_TAU_XZ);
    fsa_int.allocate_field(sizeof(double), FID_TAU_YY);
    fsa_int.allocate_field(sizeof(double), FID_TAU_YZ);
    fsa_int.allocate_field(sizeof(double), FID_TAU_ZZ);

    fsa_int.allocate_field(sizeof(double), FID_GRAD_T_X);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_T_Y);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_T_Z);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_MIXMW_X);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_MIXMW_Y);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_MIXMW_Z);
    fsa_int.allocate_field(sizeof(double), FID_HEATFLUX_X);
    fsa_int.allocate_field(sizeof(double), FID_HEATFLUX_Y);
    fsa_int.allocate_field(sizeof(double), FID_HEATFLUX_Z);

    fsa_int.allocate_field(sizeof(double), FID_GRAD_VEL_X_X);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_VEL_X_Y);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_VEL_X_Z);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_VEL_Y_X);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_VEL_Y_Y);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_VEL_Y_Z);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_VEL_Z_X);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_VEL_Z_Y);
    fsa_int.allocate_field(sizeof(double), FID_GRAD_VEL_Z_Z);

    fsa_int.allocate_field(sizeof(double), FID_RHS_1_DX_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_1_DX);
    fsa_int.allocate_field(sizeof(double), FID_RHS_1_DY_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_1_DY);
    fsa_int.allocate_field(sizeof(double), FID_RHS_1_DZ_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_1_DZ);
    fsa_int.allocate_field(sizeof(double), FID_RHS_2_DX_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_2_DX);
    fsa_int.allocate_field(sizeof(double), FID_RHS_2_DY_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_2_DY);
    fsa_int.allocate_field(sizeof(double), FID_RHS_2_DZ_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_2_DZ);
    fsa_int.allocate_field(sizeof(double), FID_RHS_3_DX_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_3_DX);
    fsa_int.allocate_field(sizeof(double), FID_RHS_3_DY_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_3_DY);
    fsa_int.allocate_field(sizeof(double), FID_RHS_3_DZ_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_3_DZ);

    fsa_int.allocate_field(sizeof(double), FID_RHS_4_DX);
    fsa_int.allocate_field(sizeof(double), FID_RHS_4_DY);
    fsa_int.allocate_field(sizeof(double), FID_RHS_4_DZ);

    fsa_int.allocate_field(sizeof(double), FID_RHS_5_DX_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_5_DX);
    fsa_int.allocate_field(sizeof(double), FID_RHS_5_DY_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_5_DY);
    fsa_int.allocate_field(sizeof(double), FID_RHS_5_DZ_IN);
    fsa_int.allocate_field(sizeof(double), FID_RHS_5_DZ);

    for(int i = 0; i < rank->n_spec; i++) {
      for(int d = 0; d < 3; d++) {
	fsa_int.allocate_field(sizeof(double), FID_RHS_6_D_IN(i, d));
	fsa_int.allocate_field(sizeof(double), FID_RHS_6_D(i, d));
      }
    }

    for(int i = 0; i < rank->n_spec; i++) {
      fsa_int.allocate_field(sizeof(double), FID_YSPECIES(i));
      fsa_int.allocate_field(sizeof(double), FID_H_SPEC(i));
#ifndef OVERLAP_FIELDS
      fsa_int.allocate_field(sizeof(double), FID_GRAD_YS_X(i));
      fsa_int.allocate_field(sizeof(double), FID_GRAD_YS_Y(i));
      fsa_int.allocate_field(sizeof(double), FID_GRAD_YS_Z(i));
#endif
      fsa_int.allocate_field(sizeof(double), FID_RR(i));
#ifdef GETRATES_NEEDS_DIFFUSION
      fsa_int.allocate_field(sizeof(double), FID_DIFFUSION(i));
      for(int d = 0; d < 3; d++)
	fsa_int.allocate_field(sizeof(double), FID_DIFF_D(i,d));
#endif
    }
#ifdef REDUNDANT_WORK
    for(int i = 0; i < rank->n_spec; i++) {
      fsa_int.allocate_field(sizeof(double), FID_YSPECIES_ALT(i));
    }
#endif

    for(int i = 0; i < rank->n_spec; i++) {
      fsa_int.allocate_field(sizeof(double), FID_YDIFFFLUX_X(i));
      fsa_int.allocate_field(sizeof(double), FID_YDIFFFLUX_Y(i));
      fsa_int.allocate_field(sizeof(double), FID_YDIFFFLUX_Z(i));
    }
#ifdef REDUNDANT_WORK 
    for (int i = 0; i < rank->n_spec; i++) {
      fsa_int.allocate_field(sizeof(double), FID_YDIFFFLUX_ALT_X(i));
      fsa_int.allocate_field(sizeof(double), FID_YDIFFFLUX_ALT_Y(i));
      fsa_int.allocate_field(sizeof(double), FID_YDIFFFLUX_ALT_Z(i));
    }
#endif

    for(int i = 0; i < rank->n_scalar; i++) {
      fsa_int.allocate_field(sizeof(double), FID_GRAD_SCALAR_X(i));
      fsa_int.allocate_field(sizeof(double), FID_GRAD_SCALAR_Y(i));
      fsa_int.allocate_field(sizeof(double), FID_GRAD_SCALAR_Z(i));
    }

    for(int i = 0; i < rank->n_scalar; i++) {
      fsa_int.allocate_field(sizeof(double), FID_SCALAR_FLUX_X(i));
      fsa_int.allocate_field(sizeof(double), FID_SCALAR_FLUX_Y(i));
      fsa_int.allocate_field(sizeof(double), FID_SCALAR_FLUX_Z(i));
    }

    for(int i = 0; i < rank->n_scalar; i++) {
      for(int d = 0; d < 3; d++) {
	fsa_int.allocate_field(sizeof(double), FID_SCALAR_RHS_D_IN(i, d));
	fsa_int.allocate_field(sizeof(double), FID_SCALAR_RHS_D(i, d));
      }
    }
  }

  LogicalRegion lr_int = runtime->create_logical_region(ctx, is_subdist, fs_int);

  // Save some fields on our rank for building region requirements
  // Note these might not get saved on all the ranks, but they will
  // get saved on all the ranks running distribute tasks which will
  // need them.
  {
    rank->is_grid = is_grid;
    rank->ip_top = ip_top;
    rank->lr_q = lr_q;
    rank->lr_rhs = lr_rhs;
    rank->lr_qerr = lr_qerr;
    rank->lr_qtmp = lr_qtmp;
    rank->lr_state = lr_state;
    rank->lr_int = lr_int;
    rank->lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
    rank->lp_rhs_top = runtime->get_logical_partition(ctx, lr_rhs, ip_top);
    rank->lp_qerr_top = runtime->get_logical_partition(ctx, lr_qerr, ip_top);
    rank->lp_qtmp_top = runtime->get_logical_partition(ctx, lr_qtmp, ip_top);
    rank->lp_state_top = runtime->get_logical_partition(ctx, lr_state, ip_top);
    rank->lp_int_top = runtime->get_logical_partition(ctx, lr_int, ip_top);
    rank->proc_grid_bounds = proc_grid_bounds;
  }

#ifdef USE_CEMA
  FieldSpace fs_cema_local = FieldSpace::NO_SPACE;
  LogicalRegion lr_cema_local = LogicalRegion::NO_REGION;
  IndexSpace is_cema = d_args->is_cema;
  IndexSpace is_cema_subdist = IndexSpace::NO_SPACE;
  LogicalRegion lr_cema_subdist = LogicalRegion::NO_REGION;
  if(rank->cema_samples_per_rank) {
    fs_cema_local = runtime->create_field_space(ctx);

    {
      FieldAllocator fsa_cema = runtime->create_field_allocator(ctx, fs_cema_local);

      fsa_cema.allocate_field(sizeof(int), FID_SAMPLE_X);
      fsa_cema.allocate_field(sizeof(int), FID_SAMPLE_Y);
      fsa_cema.allocate_field(sizeof(int), FID_SAMPLE_Z);
      fsa_cema.allocate_field(sizeof(double), FID_TEXP);
      fsa_cema.allocate_field(sizeof(double), FID_TEMP);
      fsa_cema.allocate_field(sizeof(double), FID_PRESSURE);
      for(int s = 0; s < rank->n_spec; s++)
	fsa_cema.allocate_field(sizeof(double), FID_YSPECIES(s));
    }

    Point<3> cema_grid_size(rank->proc_grid_size);
    cema_grid_size.x[0] *= rank->cema_samples_per_rank;

    // two-level partition of cema samples matching dist/proc hierarchy
    Blockify<3> cema2dist_map(cema_grid_size / rank->dist_grid_size);
    Blockify<3> cema2proc_map(cema_grid_size / rank->proc_grid_size);

    // make the whole distribution partition and then pull out our subspace
    IndexPartition ip_cema_dist = runtime->create_index_partition(ctx, is_cema, cema2dist_map, 0);
    is_cema_subdist = runtime->get_index_subspace<3>(ctx, ip_cema_dist, task->index_point.get_point<3>());
    
    IndexPartition ip_cema_proc = runtime->create_index_partition(ctx, is_cema_subdist, cema2proc_map, 0);

    // blast each processor's space in ip_cema_proc into individual points so we can do all the CEMA
    //  calculations in parallel
    Blockify<3> cema2point_map(Point<3>::ONES());
    for(GenericPointInRectIterator<3> pir_p(proc_grid_bounds); pir_p; pir_p++) {
      IndexSpace is = runtime->get_index_subspace<3>(ctx, ip_cema_proc, pir_p.p);
      runtime->create_index_partition(ctx, is, cema2point_map, 0);
    }

    lr_cema_local = runtime->create_logical_region(ctx, is_cema_subdist, fs_cema_local);

    LogicalPartition lp_cema = runtime->get_logical_partition_by_color(ctx, d_args->lr_cema_global, 0);
    lr_cema_subdist = runtime->get_logical_subregion(ctx, lp_cema, is_cema_subdist);

    rank->lr_cema_global = d_args->lr_cema_global;
    rank->lr_cema_subdist = lr_cema_subdist;
    rank->lr_cema_local = lr_cema_local;
    rank->ip_cema_proc = ip_cema_proc;
    rank->lp_cema_proc = runtime->get_logical_partition(ctx, lr_cema_local, ip_cema_proc);

    rank->cema_ready_barrier = d_args->cema_ready_barrier;
    rank->cema_done_barrier = d_args->cema_done_barrier;
  }
#endif

  // if chemistry is turned off, set the FID_RR(*) fields to 0 so that we
  //  can still add them in the sum4's
  if(!rank->i_react) {
    printf("clearing getrates output fields\n");
    for(int s = 0; s < rank->n_spec; s++)
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RR(s), 0.0);
  }

  // if spatial variation is turned off in any of the three dimensions, the
  //  corresponding stencils will be NOP'd, so we need to make sure they have
  //  valid (i.e. = 0) initial values
  if(!rank->vary_in_dims[0] || !rank->vary_in_dims[1] || !rank->vary_in_dims[2]) {
    printf("clearing stencil-related fields\n");
    if(!rank->vary_in_dims[0]) {
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_1_DX, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_2_DX, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_3_DX, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_4_DX, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_5_DX, 0.0);
      for(int s = 0; s < rank->n_spec - 1; s++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_6_DX(s), 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_T_X, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_MIXMW_X, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_VEL_X_X, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_VEL_Y_X, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_VEL_Z_X, 0.0);
      for(int s = 0; s < rank->n_spec; s++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_YS_X(s), 0.0);
#ifdef GETRATES_NEEDS_DIFFUSION
      for(int i = 0; i < GETRATES_STIFF_SPECIES; i++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_DIFF_DX(stif_species_indexes[i]), 0.0);
#endif
      for(int i = 0; i < rank->n_scalar; i++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_SCALAR_X(i), 0.0);

      for(int i = 0; i < rank->n_scalar; i++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_SCALAR_RHS_DX(i), 0.0);
    }
    if(!rank->vary_in_dims[1]) {
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_1_DY, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_2_DY, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_3_DY, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_4_DY, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_5_DY, 0.0);
      for(int s = 0; s < rank->n_spec - 1; s++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_6_DY(s), 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_T_Y, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_MIXMW_Y, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_VEL_X_Y, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_VEL_Y_Y, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_VEL_Z_Y, 0.0);
      for(int s = 0; s < rank->n_spec; s++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_YS_Y(s), 0.0);
#ifdef GETRATES_NEEDS_DIFFUSION
      for(int i = 0; i < GETRATES_STIFF_SPECIES; i++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_DIFF_DY(stif_species_indexes[i]), 0.0);
#endif
      for(int i = 0; i < rank->n_scalar; i++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_SCALAR_Y(i), 0.0);

      for(int i = 0; i < rank->n_scalar; i++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_SCALAR_RHS_DY(i), 0.0);
    }
    if(!rank->vary_in_dims[2]) {
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_1_DZ, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_2_DZ, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_3_DZ, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_4_DZ, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_5_DZ, 0.0);
      for(int s = 0; s < rank->n_spec - 1; s++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_RHS_6_DZ(s), 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_T_Z, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_MIXMW_Z, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_VEL_X_Z, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_VEL_Y_Z, 0.0);
      clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_VEL_Z_Z, 0.0);
      for(int s = 0; s < rank->n_spec; s++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_YS_Z(s), 0.0);
#ifdef GETRATES_NEEDS_DIFFUSION
      for(int i = 0; i < GETRATES_STIFF_SPECIES; i++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_DIFF_DZ(stif_species_indexes[i]), 0.0);
#endif
      for(int i = 0; i < rank->n_scalar; i++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_GRAD_SCALAR_Z(i), 0.0);

      for(int i = 0; i < rank->n_scalar; i++)
	clear_field(runtime, ctx, lr_int, rank->lp_int_top, proc_grid_bounds, FID_SCALAR_RHS_DZ(i), 0.0);
    }
  }

  // get initial temperature
  {
    ArgumentMap arg_map;
    IndexLauncher init_temp_launcher(GET_INIT_TEMP_TASK_ID,
                                     Domain::from_rect<3>(proc_grid_bounds),
                                     TaskArgument(0, 0),
                                     arg_map);
    init_temp_launcher.tag = RHSF_MAPPER_FORCE_RANK_MATCH;

    LogicalPartition lp_state_top = runtime->get_logical_partition(ctx, lr_state, ip_top);

    RegionRequirement rr_s(lp_state_top, 0, WRITE_DISCARD, EXCLUSIVE, lr_state);
    rr_s.add_field(FID_TEMP);
    init_temp_launcher.add_region_requirement(rr_s);

    FutureMap fm = runtime->execute_index_space(ctx, init_temp_launcher);
    fm.wait_all_results();
  }

  // if we're doing compression lag, we need to pre-"calculate" several
  //  steps worth of dummy values (0 is a good choice)
  if(S3DRank::get_compression_lag()) {
    DynamicCollective c = rank->global_sum_collective;
    const double zero = 0.0;
    for(int i = 0; i < S3DRank::get_compression_lag(); i++) {
      runtime->arrive_dynamic_collective(ctx, c, &zero, sizeof(zero));
      //runtime->defer_dynamic_collective_arrival(ctx, c,
      //                                          Future::from_value(runtime, zero));
      c = runtime->advance_dynamic_collective(ctx, c);
    }
  }

  // after the initial temperature data is handed off, we know if we're doing 
  //  separate rhsf calls or full ERK integrations

  if(rank->erk_stages > 0) {
    //printf("performing full integrations\n");
    for (int i = 0; i < rank->n_steps; i++)
      integrate_top(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds,
		    is_grid, is_subdist, ip_top, fs_q, lr_q, lr_rhs, lr_qerr, lr_qtmp, fs_state, lr_state,
		    fs_ghost, lr_ghost, fs_int, lr_int, regions);
  } else {
    //printf("performing individual rhsf() calls\n");
    for (int i = 0; i < (rank->n_stages * rank->n_steps); i++)
      rhsf_top(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds,
	       is_grid, is_subdist, ip_top, fs_q, lr_q, lr_rhs, fs_state, lr_state,
	       fs_ghost, lr_ghost, fs_int, lr_int, regions);
  }

  runtime->destroy_logical_region(ctx, lr_int);
  runtime->destroy_field_space(ctx, fs_int);

  runtime->destroy_logical_region(ctx, lr_state);
  runtime->destroy_field_space(ctx, fs_state);

  runtime->destroy_logical_region(ctx, lr_q);
  runtime->destroy_logical_region(ctx, lr_rhs);
  runtime->destroy_logical_region(ctx, lr_qerr);
  runtime->destroy_field_space(ctx, fs_q);
}

// the rhsf done in an integrate and the standalone rhsf have a lot in common - only
//  real difference is in how they do the final summations
void rhsf_common(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
		 const Rect<3>& proc_grid_bounds, const Rect<3>& full_proc_grid_bounds,
		 IndexSpace is_grid, IndexSpace is_subdist, IndexPartition ip_top, 
		 FieldSpace fs_q, LogicalRegion lr_q, LogicalRegion lr_rhs,
		 FieldSpace fs_state, LogicalRegion lr_state,
		 FieldSpace fs_ghost, const LogicalRegion *lr_ghost,
		 FieldSpace fs_int, LogicalRegion lr_int,
		 Future f_timestep,
		 const std::vector<PhysicalRegion> &regions,
		 bool calc_therm_coeffs,
                 int rhsf_stage)
{
  //LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  //LogicalPartition lp_rhs_top = runtime->get_logical_partition(ctx, lr_rhs, ip_top);
  //LogicalPartition lp_state_top = runtime->get_logical_partition(ctx, lr_state, ip_top);
  //LogicalPartition lp_int_top = runtime->get_logical_partition(ctx, lr_int, ip_top);

  ArgumentMap empty_map;

  // first step is to divide a whole bunch of things by rho
  {
    CalcVolumeTask calc_volume_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                        TaskArgument(0, 0), empty_map);
    SubrankableTaskHelper::dispatch_task(calc_volume_launcher, ctx, runtime, rhsf_stage);
  }

  // solve for local temperature and pressure in each cell
  {
    CalcTempTask calc_temp_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                    TaskArgument(0, 0), empty_map);
    SubrankableTaskHelper::dispatch_task(calc_temp_launcher, ctx, runtime, rhsf_stage);
  }

  // compute the yspecies values along with avmolwt and mixmw
  {
    CalcSpeciesTask calc_species_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                          TaskArgument(0, 0), empty_map);
    if (S3DRank::get_all_gpu())
      calc_species_launcher.tag |= RHSF_MAPPER_ALL_GPU;
    SubrankableTaskHelper::dispatch_task(calc_species_launcher, ctx, runtime, rhsf_stage);
  }
#ifdef REDUNDANT_WORK
  {
    CalcSpeciesTask calc_species_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                          TaskArgument(0, 0), empty_map, Predicate::TRUE_PRED,
                                          false, 0, 0, true, true/*redundant*/);
    SubrankableTaskHelper::dispatch_task(calc_species_launcher, ctx, runtime, rhsf_stage);
  }
#endif

  // Launch all the physics tasks
  if (calc_therm_coeffs)
  {
    // Diffusion first since it is on the critical path
    {
#ifdef REDUNDANT_WORK
      CalcDiffusionTask diffusion_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                           TaskArgument(0, 0), empty_map, Predicate::TRUE_PRED,
                                           false, 0, 0, true, true/*redundant*/);
#else
      CalcDiffusionTask diffusion_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                           TaskArgument(0, 0), empty_map);
#endif
      SubrankableTaskHelper::dispatch_task(diffusion_launcher, ctx, runtime, rhsf_stage);
    }
    // Then viscosity
    {
#ifdef REDUNDANT_WORK
      CalcViscosityTask viscosity_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                           TaskArgument(0, 0), empty_map, Predicate::TRUE_PRED,
                                           false, 0, 0, true, true/*redundant*/);
#else
      CalcViscosityTask viscosity_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                           TaskArgument(0, 0), empty_map);
#endif
      SubrankableTaskHelper::dispatch_task(viscosity_launcher, ctx, runtime, rhsf_stage);
    }
    // Followed by conductivity
    {
#ifdef REDUNDANT_WORK
      CalcConductivityTask conductivity_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                         TaskArgument(0, 0), empty_map, Predicate::TRUE_PRED,
                                         false, 0, 0, true, true/*redundant*/);
#else
      CalcConductivityTask conductivity_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                                 TaskArgument(0, 0), empty_map);
#endif
      SubrankableTaskHelper::dispatch_task(conductivity_launcher, ctx, runtime, rhsf_stage);
    }
    // Finally thermal
    // We skip thermal for now since no one uses its results
    //{
    //  CalcThermalTask thermal_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
    //                                   TaskArgument(0, 0), empty_map);
    //  SubrankableTaskHelper::dispatch_task(thermal_launcher, ctx, runtime);
    //}
  } 

  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_MIXMW, fs_int, lr_int, FID_GRAD_MIXMW_X,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_MIXMW, 0, true /*periodic*/, 
                                   rhsf_stage, RHSF_MAPPER_ALL_GPU, S3DRank::get_all_gpu());
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_MIXMW, fs_int, lr_int, FID_GRAD_MIXMW_Y,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_MIXMW, 1, true /*periodic*/, 
                                   rhsf_stage, RHSF_MAPPER_ALL_GPU, S3DRank::get_all_gpu());
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_MIXMW, fs_int, lr_int, FID_GRAD_MIXMW_Z,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_MIXMW, 2, true /*periodic*/, 
                                   rhsf_stage, RHSF_MAPPER_ALL_GPU, S3DRank::get_all_gpu());

  if (S3DRank::get_perform_all_checks()) {
    for(int i = 0; i < 3; i++)
      check_field(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, FID_GRAD_MIXMW_X + i,
		  rank, &S3DRank::RHSFArrays::grad_mixmw, i, 1e-15, 1e10, 1e-30, "grad_mixmw[%d]", i);
  }

  // yspecies gradients
  if (S3DRank::get_fuse_fields() <= 1)
  {
    for(int s = 0; s < rank->n_spec; s++)
      for(int d = 0; d < 3; d++)
        CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                         fs_int, lr_int, FID_YSPECIES(s), fs_int, lr_int, FID_GRAD_YS(s,d),
                                         fs_ghost, lr_ghost, regions, FID_GHOST_YSPEC(s), d, true /* periodic */, 
                                         rhsf_stage, RHSF_MAPPER_ALL_GPU, S3DRank::get_all_gpu());
  }
  else
  {
    const int fuse = S3DRank::get_fuse_fields();
    // If we need diffusion, give priority to the species which are
    // needed for the diffusion stencils
#ifdef GETRATES_NEEDS_DIFFUSION
    // First launch fused versions of all the species needed for diffusion
    for (int i = 0; i < GETRATES_STIFF_SPECIES; i += fuse)
    {
      std::vector<FieldID> src_fields;
      std::vector<FieldID> ghost_fields;
      int max_i = ((i+fuse-1) < (GETRATES_STIFF_SPECIES-1)) ? (i+fuse-1) : (GETRATES_STIFF_SPECIES-1);
      for (int idx = i; idx <= max_i; idx++)
      {
        int spec = stif_species_indexes[idx];
        src_fields.push_back(FID_YSPECIES(spec));
        ghost_fields.push_back(FID_GHOST_YSPEC(spec));
      }
      for (int d = 0; d < 3; d++)
      {
        std::vector<FieldID> dst_fields;
        for (int idx = i; idx <= max_i; idx++)
          dst_fields.push_back(FID_GRAD_YS(stif_species_indexes[idx],d));
        CalcStencilTask::calc_N_stencil_1d(runtime, ctx, rank, proc_grid_bounds,
                                           full_proc_grid_bounds, is_grid, ip_top,
                                           fs_int, lr_int, src_fields, 
                                           fs_int, lr_int, dst_fields,
                                           fs_ghost, lr_ghost, regions, ghost_fields,
                                           d, true/*periodic*/, rhsf_stage,
                                           RHSF_MAPPER_ALL_GPU | 
#ifdef REDUNDANT_WORK
                                           RHSF_MAPPER_MIXED_GPU |
#endif
                                           RHSF_MAPPER_PRIORITIZE,
#ifdef REDUNDANT_WORK
                                           S3DRank::get_mixed_stencils() ||
#endif
                                           S3DRank::get_all_gpu());
      }
    }
    // Then do the species which are not needed for diffusion
    int spec = 0, stif_idx = 0;
    while (spec < rank->n_spec)
    {
      std::vector<FieldID> src_fields;
      std::vector<FieldID> ghost_fields;
      std::vector<FieldID> dst_fields[3];
      while ((spec < rank->n_spec) && (src_fields.size() < (size_t)fuse))
      {
        if ((stif_idx < GETRATES_STIFF_SPECIES) &&
            (spec == (int)stif_species_indexes[stif_idx]))
        {
          stif_idx++;
          spec++;
          continue;
        }
        src_fields.push_back(FID_YSPECIES(spec));
        ghost_fields.push_back(FID_GHOST_YSPEC(spec));
        for (int d = 0; d < 3; d++)
          dst_fields[d].push_back(FID_GRAD_YS(spec,d));
        spec++;
      }
      for (int d = 0; d < 3; d++)
      {
        CalcStencilTask::calc_N_stencil_1d(runtime, ctx, rank, proc_grid_bounds,
                                           full_proc_grid_bounds, is_grid, ip_top,
                                           fs_int, lr_int, src_fields, 
                                           fs_int, lr_int, dst_fields[d],
                                           fs_ghost, lr_ghost, regions, ghost_fields,
                                           d, true/*periodic*/, rhsf_stage, RHSF_MAPPER_ALL_GPU,
                                           S3DRank::get_all_gpu());
      }
    }
#else
    for (int s = 0; s < rank->n_spec; s+=fuse)
    {
      std::vector<FieldID> src_fields;
      std::vector<FieldID> ghost_fields;
      for (int i = 0; (i < fuse) && ((s+i) < rank->n_spec); i++)
      {
        src_fields.push_back(FID_YSPECIES(s+i));
        ghost_fields.push_back(FID_GHOST_YSPEC(s+i));
      }
      for (int d = 0; d < 3; d++)
      {
        std::vector<FieldID> dst_fields;
        for (int i = 0; (i < fuse) && ((s+i) < rank->n_spec); i++)
          dst_fields.push_back(FID_GRAD_YS((s+i),d));
        CalcStencilTask::calc_N_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                           fs_int, lr_int, src_fields, fs_int, lr_int, dst_fields,
                                           fs_ghost, lr_ghost, regions, ghost_fields, d, true /*periodic*/, 
                                           rhsf_stage, RHSF_MAPPER_ALL_GPU, S3DRank::get_all_gpu());
      }
    }
#endif
  }

  if (S3DRank::get_perform_all_checks()) {
    for(int s = 0; s < rank->n_spec; s++)
      for(int d = 0; d < 3; d++)
	check_field(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, FID_GRAD_YS(s, d),
		    rank, &S3DRank::RHSFArrays::grad_ys, d * rank->n_spec + s, 1e-15, 1e10, 1e-30, "grad_Y[%d,%d]", s, d);
  }

  // Launch the Calc-Ydiffflux tasks
  if (S3DRank::get_field_centric())
  {
    if (S3DRank::get_fuse_fields() <= 1)
    {
      for (int s = 0, stif_idx = 0; s < (rank->n_spec-1); s++)
      {
        CalcYDiffFluxFieldTask ydiff_launcher(rank, s, Domain::from_rect<3>(proc_grid_bounds),
                                              TaskArgument(0, 0), empty_map, false/*redundant*/);
#ifdef GETRATES_NEEDS_DIFFUSION
        // Give priority to the fields which are needed for the diffusion stencils
        if ((stif_idx < GETRATES_STIFF_SPECIES) && (s == (int)stif_species_indexes[stif_idx]))
        {
          ydiff_launcher.tag |= RHSF_MAPPER_PRIORITIZE;
          stif_idx++;
        }
#endif
        RankableTaskHelper::dispatch_task(ydiff_launcher, ctx, runtime, rhsf_stage);
#ifdef REDUNDANT_WORK 
        
#ifdef GETRATES_NEEDS_DIFFUSION
        // Give priority to the fields which are needed for the diffusion stencils
        if ((stif_idx < GETRATES_STIFF_SPECIES) && (s == (int)stif_species_indexes[stif_idx]))
        {
          CalcYDiffFluxFieldTask alt_ydiff_launcher(rank, s, Domain::from_rect<3>(proc_grid_bounds),
                                              TaskArgument(0, 0), empty_map, true/*redundant*/);
          alt_ydiff_launcher.tag |= RHSF_MAPPER_PRIORITIZE;
          stif_idx++;
          RankableTaskHelper::dispatch_task(alt_ydiff_launcher, ctx, runtime, rhsf_stage);
        }
#endif
#endif
      }
    }
    else
    {
      // Fuse together some fields for the y diff flux
      const int fuse = S3DRank::get_fuse_fields();
      std::vector<int> specs; 
#ifdef GETRATES_NEEDS_DIFFUSION
      // First launch fused versions of the all tasks which are needed for diffusion
      for (int i = 0; i < GETRATES_STIFF_SPECIES; i+=fuse)
      {
        specs.clear();
        int max_i = ((i+fuse-1) < (GETRATES_STIFF_SPECIES-1)) ? (i+fuse-1) : (GETRATES_STIFF_SPECIES-1);
        for (int idx = i; idx <= max_i; idx++)
          specs.push_back(stif_species_indexes[idx]);
        if (!specs.empty())
        {
          CalcYDiffFluxFieldTask ydiff_launcher(rank, specs, Domain::from_rect<3>(proc_grid_bounds),
                                                TaskArgument(0, 0), empty_map, false/*redundant*/);
          // Give all of these priorities
          ydiff_launcher.tag |= RHSF_MAPPER_PRIORITIZE;
          RankableTaskHelper::dispatch_task(ydiff_launcher, ctx, runtime, rhsf_stage);
#ifdef REDUNDANT_WORK 
          CalcYDiffFluxFieldTask alt_ydiff_launcher(rank, specs, Domain::from_rect<3>(proc_grid_bounds),
                                                TaskArgument(0, 0), empty_map, true/*redundant*/);
          // Give all of these priorities
          alt_ydiff_launcher.tag |= RHSF_MAPPER_PRIORITIZE;
          RankableTaskHelper::dispatch_task(alt_ydiff_launcher, ctx, runtime, rhsf_stage);
#endif
        }
      }
#endif
      // Now fuse together the remaining species
      int spec = 0, stif_idx = 0;
      while (spec < (rank->n_spec-1))
      {
        specs.clear();
        while ((spec < (rank->n_spec-1)) && (specs.size() < (size_t)fuse))
        {
#ifdef GETRATES_NEEDS_DIFFUSION
          // We already did the stiff species, so skip them
          if ((stif_idx < GETRATES_STIFF_SPECIES) &&
              (spec == (int)stif_species_indexes[stif_idx]))
          {
            stif_idx++;
            spec++;
            continue;
          }
#endif
          specs.push_back(spec);
          spec++;
        }
        if (!specs.empty())
        {
          CalcYDiffFluxFieldTask ydiff_launcher(rank, specs, Domain::from_rect<3>(proc_grid_bounds),
                                                TaskArgument(0, 0), empty_map, false/*redundant*/);
          // No need to give these priority
          RankableTaskHelper::dispatch_task(ydiff_launcher, ctx, runtime, rhsf_stage);
          // No need to do alternate here since we don't need them
        }
      } 
    }
  }
  else
  {
    CalcYDiffFluxTask ydiff_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                     TaskArgument(0, 0), empty_map);
    SubrankableTaskHelper::dispatch_task(ydiff_launcher, ctx, runtime, rhsf_stage);
  }

  // Next compute the diffusion values off the calc_ydiffflux fields
  // It is only safe to hoist this computation up here if the last species is not stiff
#ifdef GETRATES_NEEDS_DIFFUSION
  if (S3DRank::get_fuse_fields() <= 1)
  {
    int stif_idx = 0;
    for(int i = 0; i < rank->n_spec; i++) {
      if((stif_idx < GETRATES_STIFF_SPECIES) &&
         (i == (int)stif_species_indexes[stif_idx])) {
        // this is a stiff species, so calculate diffusion
        stif_idx++;
      } else {
        // not a stif species
        continue;
      }
      // If we ever hit this assert move this code and the getrates code down below calc_heatflux
      assert(i != (rank->n_spec-1));
      // Give these tasks launches priority so that they can run early and get
      // the getrates code in flight.
      for(int d = 0; d < 3; d++) {
#ifdef REDUNDANT_WORK 
        CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                         fs_int, lr_int, FID_YDIFFFLUX_ALT(i, d), 
                                         fs_int, lr_int, FID_DIFF_D(i, d),
                                         fs_ghost, lr_ghost, regions, FID_GHOST_YDIFF(i),
                                         d, true /* periodic */, rhsf_stage,
                                         RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_MIXED_GPU,
                                         S3DRank::get_mixed_stencils() || S3DRank::get_all_gpu());
#else
        CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                         fs_int, lr_int, FID_YDIFFFLUX(i, d), 
                                         fs_int, lr_int, FID_DIFF_D(i, d),
                                         fs_ghost, lr_ghost, regions, FID_GHOST_YDIFF(i),
                                         d, true /* periodic */, rhsf_stage,
                                         RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_MIXED_GPU,
                                         S3DRank::get_mixed_stencils() || S3DRank::get_all_gpu());
#endif
      }
      Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_int,
                   FID_DIFF_DX(i), FID_DIFF_DY(i), FID_DIFF_DZ(i), FID_DIFFUSION(i),
                   true /* negate */, rhsf_stage, RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_MIXED_GPU);

      if (S3DRank::get_perform_all_checks()) {
	check_field(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, FID_DIFFUSION(i),
		    rank, &S3DRank::RHSFArrays::diffusion, i, 1e-10, 1e10, 0, "diffusion[%d]", i);
      }
    }
  }
  else
  {
    const int fuse = S3DRank::get_fuse_fields();
    for (unsigned idx = 0; idx < GETRATES_STIFF_SPECIES; idx+=fuse)
    {
      std::vector<FieldID> ghost_fields;
      std::vector<FieldID> src_fields[3];
      std::vector<FieldID> dst_fields[3];
      std::vector<FieldID> sum_dst_fields;
      for (int i = 0; (i < fuse) && ((idx+i) < GETRATES_STIFF_SPECIES); i++)
      {
        int s = stif_species_indexes[idx+i];
        ghost_fields.push_back(FID_GHOST_YDIFF(s));
        sum_dst_fields.push_back(FID_DIFFUSION(s));
        for (int d = 0; d < 3; d++)
        {
#ifdef REDUNDANT_WORK 
          src_fields[d].push_back(FID_YDIFFFLUX_ALT(s, d));
#else
          src_fields[d].push_back(FID_YDIFFFLUX(s, d));
#endif
          dst_fields[d].push_back(FID_DIFF_D(s, d));
        }
      }
      for (int d = 0; d < 3; d++)
        CalcStencilTask::calc_N_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                           fs_int, lr_int, src_fields[d],
                                           fs_int, lr_int, dst_fields[d],
                                           fs_ghost, lr_ghost, regions, ghost_fields,
                                           d/*dir*/, true /*periodic*/, rhsf_stage,
                                           RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_MIXED_GPU,
                                           S3DRank::get_mixed_stencils() || S3DRank::get_all_gpu());
      Sum3Task::sum_3N_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_int,
                              dst_fields[0], dst_fields[1], dst_fields[2], sum_dst_fields,
                              true/*negate*/, rhsf_stage, RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_MIXED_GPU);
    }
    if (S3DRank::get_perform_all_checks())
    {
      for (unsigned idx = 0; idx < GETRATES_STIFF_SPECIES; idx++)
      {
        int s = stif_species_indexes[idx];
        check_field(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, FID_DIFFUSION(s),
		    rank, &S3DRank::RHSFArrays::diffusion, s, 1e-10, 1e10, 0, "diffusion[%d]", s);
      }
    }
  }
#endif

  // Once we have the diffusion values then we can launch the getrates chemistry task (if enabled)
  if(rank->i_react) {
#ifdef REDUNDANT_WORK
    CalcGetratesTask getrates_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                       TaskArgument(0, 0), empty_map, Predicate::TRUE_PRED,
                                       false, 0, 0, true, true/*redundant*/);
#else
    CalcGetratesTask getrates_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                       TaskArgument(0, 0), empty_map);
#endif
#ifdef GETRATES_NEEDS_DIFFUSION
    getrates_launcher.add_future(f_timestep);
#endif
    SubrankableTaskHelper::dispatch_task(getrates_launcher, ctx, runtime, rhsf_stage);

#ifdef USE_CEMA
    if(rank->cema_samples_per_rank) {
      // we only want to do this once per timestep - take advantage of the transport coeff lagging
      //  flag
      if(calc_therm_coeffs) {
	SampleCEMATask sample_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
				       TaskArgument(0, 0), empty_map);
	sample_launcher.add_future(f_timestep);
	SubrankableTaskHelper::dispatch_task(sample_launcher, ctx, runtime, rhsf_stage);

#ifdef CEMA_ACQUIRE_RELEASE
	// before we can do the CEMA calculations, acquire the section of the global
	//  cema sample list that belongs to us
	{
	  //const PhysicalRegion& pr_cema = regions[regions.size() - 1];
	  const PhysicalRegion& pr_cema = regions[2 * (rank->vary_in_dims[0] +
						       rank->vary_in_dims[1] + 
						       rank->vary_in_dims[2])];
	  AcquireLauncher acq_launcher(
#ifdef SUBREGION_ACQUIRE
				       rank->lr_cema_subdist,
#else
				       rank->lr_cema_global,
#endif
				       rank->lr_cema_global,
				       pr_cema);

	  acq_launcher.add_field(FID_TEXP);
	  acq_launcher.add_wait_barrier(rank->cema_done_barrier);
	  
	  runtime->issue_acquire(ctx, acq_launcher);
	}
#endif

	// for the actual CEMA calculations, iterate over the proc grids so we can launch a task per
	//  sample point
	for(GenericPointInRectIterator<3> pir_p(proc_grid_bounds); pir_p; pir_p++) {
	  IndexSpace is = runtime->get_index_subspace<3>(ctx, rank->ip_cema_proc, pir_p.p);
	  LogicalRegion lr_proc = runtime->get_logical_subregion(ctx, rank->lp_cema_proc, is);

	  Point<3> p_lo = pir_p.p;
	  p_lo.x[0] *= rank->cema_samples_per_rank;
	  Point<3> p_hi = p_lo;
	  p_hi.x[0] += (rank->cema_samples_per_rank - 1);

	  IndexLauncher cema_launcher(CALC_CEMA_TASK_ID,
				      Domain::from_rect<3>(Rect<3>(p_lo, p_hi)),
				      TaskArgument(0, 0), ArgumentMap());

	  RegionRequirement rr_global(
#ifdef SUBREGION_ACQUIRE
				      rank->lr_cema_subdist,
#else
				      rank->lr_cema_global,
#endif
				      READ_WRITE, SIMULTANEOUS, rank->lr_cema_global);
	  rr_global.add_flags(NO_ACCESS_FLAG); // remote instance ok - just copies
	  rr_global.add_field(FID_TEXP);
	  cema_launcher.add_region_requirement(rr_global);

	  // ask for read on the whole region - shouldn't hurt anything
	  RegionRequirement rr_local(lr_proc, READ_ONLY, EXCLUSIVE, rank->lr_cema_local);
#ifdef DEBUG_CEMA
	  rr_local.add_field(FID_SAMPLE_X);
	  rr_local.add_field(FID_SAMPLE_Y);
	  rr_local.add_field(FID_SAMPLE_Z);
#endif
	  rr_local.add_field(FID_TEMP);
	  rr_local.add_field(FID_PRESSURE);
	  for(int s = 0; s < rank->n_spec; s++)
	    rr_local.add_field(FID_YSPECIES(s));
	  cema_launcher.add_region_requirement(rr_local);

#ifndef CEMA_ACQUIRE_RELEASE
	  // without acquire and release, this task should directly interact with
	  //  CEMA barriers
	  cema_launcher.add_wait_barrier(rank->cema_done_barrier);
	  cema_launcher.add_arrival_barrier(rank->cema_ready_barrier);
#endif

	  runtime->execute_index_space(ctx, cema_launcher);
	}

#ifdef CEMA_ACQUIRE_RELEASE
	// now release our CEMA samples, signalling on cema_ready_barrier
	{
	  //const PhysicalRegion& pr_cema = regions[regions.size() - 1];
	  const PhysicalRegion& pr_cema = regions[2 * (rank->vary_in_dims[0] +
						       rank->vary_in_dims[1] + 
						       rank->vary_in_dims[2])];
	  ReleaseLauncher rls_launcher(
#ifdef SUBREGION_ACQUIRE
				       rank->lr_cema_subdist,
#else
				       rank->lr_cema_global,
#endif
				       rank->lr_cema_global,
				       pr_cema);

	  rls_launcher.add_field(FID_TEXP);
	  rls_launcher.add_arrival_barrier(rank->cema_ready_barrier);

	  runtime->issue_release(ctx, rls_launcher);
	}
#endif

	// advance both barriers
	rank->cema_ready_barrier = runtime->advance_phase_barrier(ctx,
								  rank->cema_ready_barrier);
	rank->cema_done_barrier = runtime->advance_phase_barrier(ctx,
								 rank->cema_done_barrier);

	// are we the last rank?
#ifdef CEMA_SORT_ON_LAST_NODE
	if(proc_grid_bounds.hi == rank->proc_grid_size - Point<3>::ONES()) {
#else
	if(proc_grid_bounds.lo == Point<3>::ZEROES()) {
#endif
	  TaskLauncher sort_launcher(SORT_CEMA_TASK_ID, TaskArgument(0, 0));

	  RegionRequirement rr_global(rank->lr_cema_global, READ_WRITE, EXCLUSIVE, rank->lr_cema_global);
	  rr_global.add_field(FID_TEXP);
	  sort_launcher.add_region_requirement(rr_global);

	  sort_launcher.add_future(f_timestep);

	  sort_launcher.add_wait_barrier(rank->cema_ready_barrier);
	  sort_launcher.add_arrival_barrier(rank->cema_done_barrier);

	  runtime->execute_task(ctx, sort_launcher);
	}
      }
    }
#endif
  }

  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_state, lr_state, FID_TEMP, fs_int, lr_int, FID_GRAD_T_X,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_T, 0, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_state, lr_state, FID_TEMP, fs_int, lr_int, FID_GRAD_T_Y,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_T, 1, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_state, lr_state, FID_TEMP, fs_int, lr_int, FID_GRAD_T_Z,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_T, 2, true /*periodic*/, rhsf_stage);

  if (S3DRank::get_perform_all_checks()) {
    for(int i = 0; i < 3; i++)
      check_field(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, FID_GRAD_T_X + i,
		  rank, &S3DRank::RHSFArrays::grad_T, i, 1e-10, 1e10, 0, "grad_T[%d]", i);
  }

  // heat flux is a combination of temp gradient and diffusion of species
  // changed to calculate h_spec's internally from temperature, merged all 3 directions
#ifdef UNION_HEATFLUX
  {
    CalcHeatFluxTask heatflux_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                       TaskArgument(0, 0), empty_map);
    SubrankableTaskHelper::dispatch_task(heatflux_launcher, ctx, runtime, rhsf_stage);
  }
#else
  {
    CalcEnthalpyTask enthalpy_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                       TaskArgument(0, 0), empty_map);
    SubrankableTaskHelper::dispatch_task(enthalpy_launcher, ctx, runtime, rhsf_stage);

    CalcShortHeatFluxTask heatflux_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                            TaskArgument(0, 0), empty_map);
    SubrankableTaskHelper::dispatch_task(heatflux_launcher, ctx, runtime, rhsf_stage);
  }
#endif

  // gradient of velocity field, yspecies
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_VEL_X, fs_int, lr_int, FID_GRAD_VEL_X_X,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_VEL_X, 0, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_VEL_X, fs_int, lr_int, FID_GRAD_VEL_X_Y,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_VEL_X, 1, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_VEL_X, fs_int, lr_int, FID_GRAD_VEL_X_Z,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_VEL_X, 2, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_VEL_Y, fs_int, lr_int, FID_GRAD_VEL_Y_X,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_VEL_Y, 0, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_VEL_Y, fs_int, lr_int, FID_GRAD_VEL_Y_Y,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_VEL_Y, 1, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_VEL_Y, fs_int, lr_int, FID_GRAD_VEL_Y_Z,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_VEL_Y, 2, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_VEL_Z, fs_int, lr_int, FID_GRAD_VEL_Z_X,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_VEL_Z, 0, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_VEL_Z, fs_int, lr_int, FID_GRAD_VEL_Z_Y,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_VEL_Z, 1, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_VEL_Z, fs_int, lr_int, FID_GRAD_VEL_Z_Z,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_VEL_Z, 2, true /*periodic*/, rhsf_stage);

  if (S3DRank::get_perform_all_checks()) {
    for(int i = 0; i < 9; i++)
      check_field(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, FID_GRAD_VEL_X_X + i,
		  rank, &S3DRank::RHSFArrays::grad_u, i, 1e-15, 1e10, 0, "grad_u[%d]", i); 
  }

  // viscosity and velocity gradient turn into tau
  {
    CalcTauTask tau_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                             TaskArgument(0, 0), empty_map);
    SubrankableTaskHelper::dispatch_task(tau_launcher, ctx, runtime, rhsf_stage);
  }

  // rhs1 - x velocity component
  CalcRHSVelTask::calc_rhs_vel_deriv_in(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_q, lr_int,
			FID_RHO_U, FID_VEL_X, FID_TAU_XX, FID_PRESSURE, FID_RHS_1_DX_IN, rhsf_stage);
  CalcRHSVelTask::calc_rhs_vel_deriv_in(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_q, lr_int,
			FID_RHO_U, FID_VEL_Y, FID_TAU_XY, FID_NONE, FID_RHS_1_DY_IN, rhsf_stage);
  CalcRHSVelTask::calc_rhs_vel_deriv_in(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_q, lr_int,
			FID_RHO_U, FID_VEL_Z, FID_TAU_XZ, FID_NONE, FID_RHS_1_DZ_IN, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_1_DX_IN, fs_int, lr_int, FID_RHS_1_DX,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS1, 0, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_1_DY_IN, fs_int, lr_int, FID_RHS_1_DY,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS1, 1, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_1_DZ_IN, fs_int, lr_int, FID_RHS_1_DZ,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS1, 2, true /*periodic*/, rhsf_stage);

  // rhs2 - y velocity component
  CalcRHSVelTask::calc_rhs_vel_deriv_in(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_q, lr_int,
			FID_RHO_V, FID_VEL_X, FID_TAU_XY, FID_NONE, FID_RHS_2_DX_IN, rhsf_stage);
  CalcRHSVelTask::calc_rhs_vel_deriv_in(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_q, lr_int,
			FID_RHO_V, FID_VEL_Y, FID_TAU_YY, FID_PRESSURE, FID_RHS_2_DY_IN, rhsf_stage);
  CalcRHSVelTask::calc_rhs_vel_deriv_in(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_q, lr_int,
			FID_RHO_V, FID_VEL_Z, FID_TAU_YZ, FID_NONE, FID_RHS_2_DZ_IN, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_2_DX_IN, fs_int, lr_int, FID_RHS_2_DX,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS2, 0, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_2_DY_IN, fs_int, lr_int, FID_RHS_2_DY,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS2, 1, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_2_DZ_IN, fs_int, lr_int, FID_RHS_2_DZ,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS2, 2, true /*periodic*/, rhsf_stage);

  // rhs3 - x velocity component
  CalcRHSVelTask::calc_rhs_vel_deriv_in(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_q, lr_int,
			FID_RHO_W, FID_VEL_X, FID_TAU_XZ, FID_NONE, FID_RHS_3_DX_IN, rhsf_stage);
  CalcRHSVelTask::calc_rhs_vel_deriv_in(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_q, lr_int,
			FID_RHO_W, FID_VEL_Y, FID_TAU_YZ, FID_NONE, FID_RHS_3_DY_IN, rhsf_stage);
  CalcRHSVelTask::calc_rhs_vel_deriv_in(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_q, lr_int,
			FID_RHO_W, FID_VEL_Z, FID_TAU_ZZ, FID_PRESSURE, FID_RHS_3_DZ_IN, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_3_DX_IN, fs_int, lr_int, FID_RHS_3_DX,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS3, 0, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_3_DY_IN, fs_int, lr_int, FID_RHS_3_DY,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS3, 1, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_3_DZ_IN, fs_int, lr_int, FID_RHS_3_DZ,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS3, 2, true /*periodic*/, rhsf_stage);

  // rhs4 - continuity
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_q, lr_q, FID_RHO_U, fs_int, lr_int, FID_RHS_4_DX,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS4, 0, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_q, lr_q, FID_RHO_V, fs_int, lr_int, FID_RHS_4_DY,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS4, 1, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_q, lr_q, FID_RHO_W, fs_int, lr_int, FID_RHS_4_DZ,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS4, 2, true /*periodic*/, rhsf_stage);

  // rhs5 - energy
  {
    CalcEnergyTask energy_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                   TaskArgument(0, 0), empty_map);
    RankableTaskHelper::dispatch_task(energy_launcher, ctx, runtime, rhsf_stage);
  }

  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_5_DX_IN, fs_int, lr_int, FID_RHS_5_DX,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS5, 0, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_5_DY_IN, fs_int, lr_int, FID_RHS_5_DY,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS5, 1, true /*periodic*/, rhsf_stage);
  CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                   fs_int, lr_int, FID_RHS_5_DZ_IN, fs_int, lr_int, FID_RHS_5_DZ,
                                   fs_ghost, lr_ghost, regions, FID_GHOST_RHS5, 2, true /*periodic*/, rhsf_stage);

  // precalc
  if (S3DRank::get_fuse_fields() <= 1)
  {
    if (S3DRank::get_field_centric())
    {
      for (int i = 0; i < rank->n_spec-1; i++)
      {
        CalcSpecPerFieldTask spec_launcher(rank, i, Domain::from_rect<3>(proc_grid_bounds),
                                           TaskArgument(0, 0), empty_map);
        RankableTaskHelper::dispatch_task(spec_launcher, ctx, runtime, rhsf_stage);
        for (int d = 0; d < 3; d++)
        {
          CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                           fs_int, lr_int, FID_RHS_6_D_IN(i, d), fs_int, lr_int, FID_RHS_6_D(i, d),
                                           fs_ghost, lr_ghost, regions, FID_GHOST_RHS6(i), d, true /*periodic*/,
                                           rhsf_stage, RHSF_MAPPER_ALL_GPU, S3DRank::get_all_gpu());
        }
      }
      
    }
    else
    {
      for (int i = 0; i < rank->n_spec-1; i++) 
      {
        for (int d = 0; d < 3; d++)
        {
          CalcSpecTask spec_launcher(rank, i, d, Domain::from_rect<3>(proc_grid_bounds),
                                     TaskArgument(0, 0), empty_map);
          RankableTaskHelper::dispatch_task(spec_launcher, ctx, runtime, rhsf_stage);

          CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                           fs_int, lr_int, FID_RHS_6_D_IN(i, d), fs_int, lr_int, FID_RHS_6_D(i, d),
                                           fs_ghost, lr_ghost, regions, FID_GHOST_RHS6(i), d, true /*periodic*/,
                                           rhsf_stage, RHSF_MAPPER_ALL_GPU, S3DRank::get_all_gpu());
        }
      }
    }
  }
  else
  {
    const int fuse = S3DRank::get_fuse_fields();
    for (int s = 0; s < rank->n_spec-1; s+=fuse)
    {
      const int max_s = ((s+fuse) < (rank->n_spec-1)) ? (s+fuse) : (rank->n_spec-1);
      std::vector<FieldID> ghost_fields;
      std::vector<FieldID> src_fields[3];
      std::vector<FieldID> dst_fields[3];
      for (int i = s; i < max_s; i++)
      {
        ghost_fields.push_back(FID_GHOST_RHS6(i));
        for (int d = 0; d < 3; d++)
        {
          src_fields[d].push_back(FID_RHS_6_D_IN(i, d));
          dst_fields[d].push_back(FID_RHS_6_D(i, d));
        }
      }
      if (S3DRank::get_field_centric())
      {
        CalcSpecPerFieldTask spec_launcher(rank, s, max_s, Domain::from_rect<3>(proc_grid_bounds),
                                           TaskArgument(0, 0), empty_map);
        RankableTaskHelper::dispatch_task(spec_launcher, ctx, runtime, rhsf_stage);
      }
      else
      {
        for (int d = 0; d < 3; d++)
        {
          CalcSpecTask spec_launcher(rank, s, max_s, d, Domain::from_rect<3>(proc_grid_bounds),
                                     TaskArgument(0, 0), empty_map);
          RankableTaskHelper::dispatch_task(spec_launcher, ctx, runtime, rhsf_stage);
        }
      }
      for (int d = 0; d < 3; d++)
      {
        CalcStencilTask::calc_N_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
                                           fs_int, lr_int, src_fields[d], fs_int, lr_int, dst_fields[d],
                                           fs_ghost, lr_ghost, regions, ghost_fields, d, true/*periodic*/,
                                           rhsf_stage, RHSF_MAPPER_ALL_GPU, S3DRank::get_all_gpu());
      }
    }
  }

  // scalar transport code
  if(rank->n_scalar > 0) {
    // gradients for scalars (if any) are not fused
    for(int i = 0; i < rank->n_scalar; i++)
      for(int d = 0; d < 3; d++)
	CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
					 fs_q, lr_q, FID_SCALAR(i), fs_int, lr_int, FID_GRAD_SCALAR(i,d),
					 fs_ghost, lr_ghost, regions, FID_GHOST_SCALAR(i), d, true /* periodic */, 
					 rhsf_stage, RHSF_MAPPER_ALL_GPU, S3DRank::get_all_gpu());

    // all scalar flux calculation done as a monolithic task right now
    CalcScalarFluxTask sflux_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
				      TaskArgument(0, 0), empty_map);

    RankableTaskHelper::dispatch_task(sflux_launcher, ctx, runtime, rhsf_stage);

    // we can use the species pre-calc tasks for scalar fields too - negative "species" indices signal that
    //   we want the scalar fields
    for(int i = 0; i < rank->n_scalar; i++) {
      CalcSpecPerFieldTask spec_launcher(rank, -1 - i, Domain::from_rect<3>(proc_grid_bounds),
					 TaskArgument(0, 0), empty_map);
      RankableTaskHelper::dispatch_task(spec_launcher, ctx, runtime, rhsf_stage);
      for(int d = 0; d < 3; d++)
	CalcStencilTask::calc_stencil_1d(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds, is_grid, ip_top,
					 fs_int, lr_int, FID_SCALAR_RHS_D_IN(i, d), fs_int, lr_int, FID_SCALAR_RHS_D(i, d),
					 fs_ghost, lr_ghost, regions, FID_GHOST_SCALAR_RHS(i), d, true /*periodic*/,
					 rhsf_stage, RHSF_MAPPER_ALL_GPU, S3DRank::get_all_gpu());
    }
  }
}

void rhsf_top(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
	      const Rect<3>& proc_grid_bounds, const Rect<3>& full_proc_grid_bounds,
	      IndexSpace is_grid, IndexSpace is_subdist, IndexPartition ip_top, 
	      FieldSpace fs_q, LogicalRegion lr_q, LogicalRegion lr_rhs,
	      FieldSpace fs_state, LogicalRegion lr_state,
	      FieldSpace fs_ghost, const LogicalRegion *lr_ghost,
              FieldSpace fs_int, LogicalRegion lr_int,
	      const std::vector<PhysicalRegion> &regions)
{
  //LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  //LogicalPartition lp_rhs_top = runtime->get_logical_partition(ctx, lr_rhs, ip_top);
  //LogicalPartition lp_state_top = runtime->get_logical_partition(ctx, lr_state, ip_top);
  //LogicalPartition lp_int_top = runtime->get_logical_partition(ctx, lr_int, ip_top);

  ArgumentMap empty_map;

  // determine if we need to recalculate thermal coefficients
  bool calc_therm_coeffs = true;
  if(rank->lagging_switch) {
    if(rank->cur_stage == rank->next_therm_stage) {
      rank->next_therm_stage += rank->n_stages * rank->lag_steps;
      //printf("lagging: calculating for stage=%d, next on %d\n", rank->cur_stage, rank->next_therm_stage);
    } else {
      //printf("lagging: no calculation on stage=%d\n", rank->cur_stage);
      calc_therm_coeffs = false;
    }
    rank->cur_stage++;
  }

  const int rhsf_stage = (rank->cur_stage % 6);

  if (calc_therm_coeffs)
    runtime->begin_trace(ctx, TRACE_WITH_THERM_COEFFS);
  else
    runtime->begin_trace(ctx, TRACE_WITHOUT_THERM_COEFFS);

  // step 1: get q input data from MPI side (writes to all fields of q)
  Future f_timestep;  // timestep is returned by AwaitMPITask
  {
    AwaitMPITask await_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                TaskArgument(0, 0), empty_map);
    await_launcher.tag |= (rhsf_stage & RHSF_MAPPER_STAGE_MASK);
    FutureMap fm = runtime->execute_index_space(ctx, await_launcher);
    if (S3DRank::get_perform_waits() || S3DRank::get_perform_all_checks() || S3DRank::get_perform_final_checks())
      fm.wait_all_results();
    f_timestep = fm.get_future(DomainPoint::from_point<3>(proc_grid_bounds.lo));
  }

  // common code does everything except final summations
  rhsf_common(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds,
	      is_grid, is_subdist, ip_top,
	      fs_q, lr_q, lr_rhs,
	      fs_state, lr_state,
	      fs_ghost, lr_ghost,
	      fs_int, lr_int,
	      f_timestep,
	      regions,
	      calc_therm_coeffs, 
              rhsf_stage);

  Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
		   FID_RHS_1_DX, FID_RHS_1_DY, FID_RHS_1_DZ, FID_RHO_U, rhsf_stage);

  Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
		   FID_RHS_2_DX, FID_RHS_2_DY, FID_RHS_2_DZ, FID_RHO_V, rhsf_stage);

  Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
		   FID_RHS_3_DX, FID_RHS_3_DY, FID_RHS_3_DZ, FID_RHO_W, rhsf_stage);

  Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
	       FID_RHS_4_DX, FID_RHS_4_DY, FID_RHS_4_DZ, FID_RHO, rhsf_stage, true /* negate */);

  Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
		   FID_RHS_5_DX, FID_RHS_5_DY, FID_RHS_5_DZ, FID_RHO_E, rhsf_stage);

  if (S3DRank::get_fuse_fields() <= 1) {
    for (int i = 0; i < rank->n_spec-1; i++) {
      Sum4Task::sum_4_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
			     FID_RHS_6_DX(i), FID_RHS_6_DY(i), FID_RHS_6_DZ(i), FID_RR(i), FID_RHO_Y(i),
			     rhsf_stage, false/*negate*/, RHSF_MAPPER_ALL_GPU);
    }
  } else {
    const int fuse = S3DRank::get_fuse_fields();
    for (int s = 0; s < rank->n_spec-1; s+=fuse) {
      const int max_s = ((s+fuse) < (rank->n_spec-1)) ? (s+fuse) : (rank->n_spec-1);
      std::vector<FieldID> dst_fields[3];
      std::vector<FieldID> rr_fields;
      std::vector<FieldID> rho_y_fields;
      for (int i = s; i < max_s; i++) {
        rr_fields.push_back(FID_RR(i));
        rho_y_fields.push_back(FID_RHO_Y(i));
        for (int d = 0; d < 3; d++) {
          dst_fields[d].push_back(FID_RHS_6_D(i, d));
        }
      }

      Sum4Task::sum_4N_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
                              dst_fields[0], dst_fields[1], dst_fields[2], rr_fields, rho_y_fields,
                              rhsf_stage, false/*negate*/, RHSF_MAPPER_ALL_GPU);
    }
  }

  // no support for scalar fields in standalone rhsf() - must use integration
  assert(rank->n_scalar == 0);
  
  if (S3DRank::get_perform_final_checks()) {
    // these are derivatives that will be multiplied by a timestep on the
    //  order of 1e-9s, so anything less than 1e-10 is easily below FP64
    //  precision
    for(int fid = FID_RHO_U; fid < (FID_RHO_Y0 + rank->n_spec - 1); fid++)
      check_field(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_rhs, fid,
		  rank, &S3DRank::RHSFArrays::rhs_cmp, fid, 1e-10, 1e3, 1e-20, "rhs[%d]", fid);
  }

  // step N: pass rhs output data back to MPI side (reads to all fields of rhs)
  {
    HandoffToMPITask handoff_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                      TaskArgument(0, 0), empty_map);
    handoff_launcher.tag |= (rhsf_stage & RHSF_MAPPER_STAGE_MASK);
    FutureMap fm = runtime->execute_index_space(ctx, handoff_launcher);
    // cannot wait if using ghost cells and barriers
    if (S3DRank::get_perform_waits() || !S3DRank::get_explicit_ghost_cells())
      fm.wait_all_results();
  }

  if (calc_therm_coeffs)
    runtime->end_trace(ctx, TRACE_WITH_THERM_COEFFS);
  else
    runtime->end_trace(ctx, TRACE_WITHOUT_THERM_COEFFS);
  // Mark that we've issued another frame for the mapper
  runtime->complete_frame(ctx);
}

void integrate_top(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
		   const Rect<3>& proc_grid_bounds, const Rect<3>& full_proc_grid_bounds,
		   IndexSpace is_grid, IndexSpace is_subdist, IndexPartition ip_top, 
		   FieldSpace fs_q, LogicalRegion lr_q, LogicalRegion lr_rhs,
		   LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
		   FieldSpace fs_state, LogicalRegion lr_state,
		   FieldSpace fs_ghost, const LogicalRegion *lr_ghost,
		   FieldSpace fs_int, LogicalRegion lr_int,
		   const std::vector<PhysicalRegion> &regions)
{
  //LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  //LogicalPartition lp_qerr_top = runtime->get_logical_partition(ctx, lr_qerr, ip_top);
  //LogicalPartition lp_qtmp_top = runtime->get_logical_partition(ctx, lr_qtmp, ip_top);
  //LogicalPartition lp_state_top = runtime->get_logical_partition(ctx, lr_state, ip_top);
  //LogicalPartition lp_int_top = runtime->get_logical_partition(ctx, lr_int, ip_top);

  ArgumentMap empty_map;

  // step 1: get q input data from MPI side (writes to all fields of q)
  Future f_timestep;  // timestep is returned by AwaitMPITask
  {
    AwaitMPITask await_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                TaskArgument(0, 0), empty_map);
    FutureMap fm = runtime->execute_index_space(ctx, await_launcher);
    if (S3DRank::get_perform_waits() || S3DRank::get_perform_all_checks() || S3DRank::get_perform_final_checks())
      fm.wait_all_results();
    f_timestep = fm.get_future(DomainPoint::from_point<3>(proc_grid_bounds.lo));
  }

  // trace begins after AwaitMPITask, because it uses different requirements
  //  based on the cur_handback flag
  runtime->begin_trace(ctx, TRACE_FULL_INTEGRATION);

  for(int stage = 0; stage < rank->erk_stages; stage++) {
    // determine if we need to recalculate thermal coefficients
    bool calc_therm_coeffs = true;
    if(rank->lagging_switch) {
      if(rank->cur_stage == rank->next_therm_stage) {
	rank->next_therm_stage += rank->n_stages * rank->lag_steps;
	//printf("lagging: calculating for stage=%d, next on %d\n", rank->cur_stage, rank->next_therm_stage);
      } else {
	//printf("lagging: no calculation on stage=%d\n", rank->cur_stage);
	calc_therm_coeffs = false;
      }
      rank->cur_stage++;
    }

    // common code does everything except final summations
    rhsf_common(runtime, ctx, rank, proc_grid_bounds, full_proc_grid_bounds,
		is_grid, is_subdist, ip_top,
		fs_q, lr_q, lr_rhs,
		fs_state, lr_state,
		fs_ghost, lr_ghost,
		fs_int, lr_int,
		f_timestep,
		regions,
		calc_therm_coeffs,
                stage);

    // if we're imposing pressure traces, we need to break the integration apart so 
    //  we can add in source terms
    if(rank->compression) {
      // we're going to need gamma
      {
	CalcGammaTask calc_gamma_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
					      TaskArgument(0, 0), empty_map);
	SubrankableTaskHelper::dispatch_task(calc_gamma_launcher, ctx, runtime, stage);
      }

      // and the rhs terms that include everything except this source term
      Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
			     FID_RHS_1_DX, FID_RHS_1_DY, FID_RHS_1_DZ, FID_RHO_U, stage);

      Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
			     FID_RHS_2_DX, FID_RHS_2_DY, FID_RHS_2_DZ, FID_RHO_V, stage);

      Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
			     FID_RHS_3_DX, FID_RHS_3_DY, FID_RHS_3_DZ, FID_RHO_W, stage);

      Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
			     FID_RHS_4_DX, FID_RHS_4_DY, FID_RHS_4_DZ, FID_RHO, stage, true /* negate */);

      Sum3Task::sum_3_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
			     FID_RHS_5_DX, FID_RHS_5_DY, FID_RHS_5_DZ, FID_RHO_E, stage);

      if (S3DRank::get_fuse_fields() <= 1) {
	for (int i = 0; i < rank->n_spec-1; i++) {
	  Sum4Task::sum_4_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
				 FID_RHS_6_DX(i), FID_RHS_6_DY(i), FID_RHS_6_DZ(i), FID_RR(i), FID_RHO_Y(i),
				 stage, false/*negate*/, RHSF_MAPPER_ALL_GPU);
	}
      } else {
	const int fuse = S3DRank::get_fuse_fields();
	for (int s = 0; s < rank->n_spec-1; s+=fuse) {
	  const int max_s = ((s+fuse) < (rank->n_spec-1)) ? (s+fuse) : (rank->n_spec-1);
	  std::vector<FieldID> dst_fields[3];
	  std::vector<FieldID> rr_fields;
	  std::vector<FieldID> rho_y_fields;
	  for (int i = s; i < max_s; i++) {
	    rr_fields.push_back(FID_RR(i));
	    rho_y_fields.push_back(FID_RHO_Y(i));
	    for (int d = 0; d < 3; d++) {
	      dst_fields[d].push_back(FID_RHS_6_D(i, d));
	    }
	  }

	  Sum4Task::sum_4N_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top, lr_int, lr_rhs,
				  dst_fields[0], dst_fields[1], dst_fields[2], rr_fields, rho_y_fields,
				  stage, false/*negate*/, RHSF_MAPPER_ALL_GPU);
	}
      }

      // in one task, every point calculates the dPdt implied by the calculated
      //  rhs and determines its desired correction factor
      // this requires the current timestep info as well as knowing where we
      //  are within the timestep
      Future local_sum;
      {
	double t_frac = 0;
	if(rank->erk_stages > 0) {
	  // we don't have the position within the timestep explicitly, but
	  //  we can regenerate it from the RK alphas and betas
	  for(int i = 0; i < stage; i++) {
	    t_frac += rank->erk_alpha[i];
	    if(i)
	      t_frac += rank->erk_beta[i - 1];
	  }
	}
	CalcImposedPressureTask imposed_launcher(rank,
						 Domain::from_rect<3>(proc_grid_bounds),
						 TaskArgument(&t_frac, sizeof(double)), empty_map);

	imposed_launcher.add_future(f_timestep);

	local_sum = RankableTaskHelper::dispatch_task(imposed_launcher, REDOP_SUM_DOUBLE,
						      ctx, runtime, stage);
      }

      // now use a dynamic collective to combine all those local sums into a global
      //  dPdt_imposed
      Future global_sum;
      {
//define DEBUG_REDUCTIONS
#ifdef DEBUG_REDUCTIONS
	double d = local_sum.get_result<double>();
	printf("sum is %g\n", d);
#endif

	// global sum - apply lagging if requested
        {
          DynamicCollective c = rank->global_sum_collective;
          for(int i = 0; i < S3DRank::get_compression_lag(); i++)
             c = runtime->advance_dynamic_collective(ctx, c);
	  runtime->defer_dynamic_collective_arrival(ctx, c, local_sum);
        }

	rank->global_sum_collective = runtime->advance_dynamic_collective(ctx, rank->global_sum_collective);

	global_sum = runtime->get_dynamic_collective_result(ctx, rank->global_sum_collective);

#ifdef DEBUG_REDUCTIONS
	double d2 = global_sum.get_result<double>();
	printf("global sum is %g\n", d2);
#endif
      }

      // the global dPdt_imposed turns into a per-grid point (dSdt / rho) by dividing
      //  by (gamma * pressure)
      {
	CalcSourceTermTask source_term_launcher(rank,
						Domain::from_rect<3>(proc_grid_bounds),
						TaskArgument(0, 0), empty_map);
	source_term_launcher.add_future(global_sum);
	SubrankableTaskHelper::dispatch_task(source_term_launcher,
					     ctx, runtime, stage);
      }

      // now do the integration - each rhs term is augmented with a source term based
      //  on the imposed dPdt and the current state variables - only energy is special
      if (S3DRank::get_fuse_fields() <= 1) {
	for(int fid = FID_RHO_U; fid < FID_RHO_Y(rank->n_spec - 1); fid++)
	  if(fid != FID_RHO_E)
	    IntegrateSourceTask::integrate_field(runtime, ctx,
						 proc_grid_bounds, is_grid, ip_top,
						 lr_int, lr_rhs, lr_q, lr_qerr, lr_qtmp,
						 fid, -1, /*no second source term*/
						 rank->erk_alpha[stage],
						 rank->erk_beta[stage],
						 rank->erk_error[stage],
						 f_timestep,
						 ((stage == 0) ? IntegrateSourceTask::FIRST_INTEGRATION :
						  (stage == (rank->erk_stages - 1)) ? IntegrateSourceTask::LAST_INTEGRATION :
						  IntegrateSourceTask::MIDDLE_INTEGRATION), stage);
      } else {
	const int fuse = S3DRank::get_fuse_fields();

	std::vector<FieldID> fids;
	// fuse momentum and density first, then do species in chunks
	{
	  fids.push_back(FID_RHO_U);
	  fids.push_back(FID_RHO_V);
	  fids.push_back(FID_RHO_W);
	  fids.push_back(FID_RHO);

	  IntegrateSourceTask::integrate_fields(runtime, ctx,
						proc_grid_bounds, is_grid, ip_top,
						lr_int, lr_rhs, lr_q, lr_qerr, lr_qtmp,
						fids,
						rank->erk_alpha[stage],
						rank->erk_beta[stage],
						rank->erk_error[stage],
						f_timestep,
						((stage == 0) ? IntegrateSourceTask::FIRST_INTEGRATION :
						 (stage == (rank->erk_stages - 1)) ? IntegrateSourceTask::LAST_INTEGRATION :
						 IntegrateSourceTask::MIDDLE_INTEGRATION), stage);
	  fids.clear();
	}

	for (int s = 0; s < rank->n_spec-1; s++) {
	  fids.push_back(FID_RHO_Y(s));
	  if((fids.size() >= (size_t)fuse) || (s == (rank->n_spec - 2))) {

	    IntegrateSourceTask::integrate_fields(runtime, ctx,
						  proc_grid_bounds, is_grid, ip_top,
						  lr_int, lr_rhs, lr_q, lr_qerr, lr_qtmp,
						  fids,
						  rank->erk_alpha[stage],
						  rank->erk_beta[stage],
						  rank->erk_error[stage],
						  f_timestep,
						  ((stage == 0) ? IntegrateSourceTask::FIRST_INTEGRATION :
						   (stage == (rank->erk_stages - 1)) ? IntegrateSourceTask::LAST_INTEGRATION :
						   IntegrateSourceTask::MIDDLE_INTEGRATION), stage);
	    fids.clear();
	  }
	}
	assert(fids.empty());
      }

      // energy also includes a second component of the source term based on pressure
      IntegrateSourceTask::integrate_field(runtime, ctx,
					   proc_grid_bounds, is_grid, ip_top,
					   lr_int, lr_rhs, lr_q, lr_qerr, lr_qtmp,
					   FID_RHO_E, FID_PRESSURE,
					   rank->erk_alpha[stage],
					   rank->erk_beta[stage],
					   rank->erk_error[stage],
					   f_timestep,
					   ((stage == 0) ? IntegrateSourceTask::FIRST_INTEGRATION :
					    (stage == (rank->erk_stages - 1)) ? IntegrateSourceTask::LAST_INTEGRATION :
					    IntegrateSourceTask::MIDDLE_INTEGRATION), stage);
    } else {
      // combine the summation of rhs terms with the actual integration
      Sum3IntegrateTask::sum_3_integrate_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top,
						lr_int, lr_q, lr_qerr, lr_qtmp,
						FID_RHS_1_DX, FID_RHS_1_DY, FID_RHS_1_DZ, FID_RHO_U,
						rank->erk_alpha[stage],
						rank->erk_beta[stage],
						rank->erk_error[stage],
						f_timestep,
						((stage == 0) ? Sum3IntegrateTask::FIRST_INTEGRATION :
						 (stage == (rank->erk_stages - 1)) ? Sum3IntegrateTask::LAST_INTEGRATION :
						 Sum3IntegrateTask::MIDDLE_INTEGRATION), stage);

      Sum3IntegrateTask::sum_3_integrate_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top,
						lr_int, lr_q, lr_qerr, lr_qtmp,
						FID_RHS_2_DX, FID_RHS_2_DY, FID_RHS_2_DZ, FID_RHO_V,
						rank->erk_alpha[stage],
						rank->erk_beta[stage],
						rank->erk_error[stage],
						f_timestep,
						((stage == 0) ? Sum3IntegrateTask::FIRST_INTEGRATION :
						 (stage == (rank->erk_stages - 1)) ? Sum3IntegrateTask::LAST_INTEGRATION :
						 Sum3IntegrateTask::MIDDLE_INTEGRATION), stage);

      Sum3IntegrateTask::sum_3_integrate_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top,
						lr_int, lr_q, lr_qerr, lr_qtmp,
						FID_RHS_3_DX, FID_RHS_3_DY, FID_RHS_3_DZ, FID_RHO_W,
						rank->erk_alpha[stage],
						rank->erk_beta[stage],
						rank->erk_error[stage],
						f_timestep,
						((stage == 0) ? Sum3IntegrateTask::FIRST_INTEGRATION :
						 (stage == (rank->erk_stages - 1)) ? Sum3IntegrateTask::LAST_INTEGRATION :
						 Sum3IntegrateTask::MIDDLE_INTEGRATION), stage);

      // rhs4 needs to fold the negation into alpha/beta/err
      Sum3IntegrateTask::sum_3_integrate_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top,
						lr_int, lr_q, lr_qerr, lr_qtmp,
						FID_RHS_4_DX, FID_RHS_4_DY, FID_RHS_4_DZ, FID_RHO,
						-rank->erk_alpha[stage],
						-rank->erk_beta[stage],
						-rank->erk_error[stage],
						f_timestep,
						((stage == 0) ? Sum3IntegrateTask::FIRST_INTEGRATION :
						 (stage == (rank->erk_stages - 1)) ? Sum3IntegrateTask::LAST_INTEGRATION :
						 Sum3IntegrateTask::MIDDLE_INTEGRATION), stage);

      Sum3IntegrateTask::sum_3_integrate_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top,
						lr_int, lr_q, lr_qerr, lr_qtmp,
						FID_RHS_5_DX, FID_RHS_5_DY, FID_RHS_5_DZ, FID_RHO_E,
						rank->erk_alpha[stage],
						rank->erk_beta[stage],
						rank->erk_error[stage],
						f_timestep,
						((stage == 0) ? Sum3IntegrateTask::FIRST_INTEGRATION :
						 (stage == (rank->erk_stages - 1)) ? Sum3IntegrateTask::LAST_INTEGRATION :
						 Sum3IntegrateTask::MIDDLE_INTEGRATION), stage);

      if (S3DRank::get_fuse_fields() <= 1) {
	for (int i = 0; i < rank->n_spec-1; i++) {
	  Sum4IntegrateTask::sum_4_integrate_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top,
						    lr_int, lr_q, lr_qerr, lr_qtmp,
						    FID_RHS_6_DX(i), FID_RHS_6_DY(i), FID_RHS_6_DZ(i), FID_RR(i), FID_RHO_Y(i),
						    rank->erk_alpha[stage],
						    rank->erk_beta[stage],
						    rank->erk_error[stage],
						    rank->small_value,
						    f_timestep,
						    ((stage == 0) ? Sum4IntegrateTask::FIRST_INTEGRATION :
						     (stage == (rank->erk_stages - 1)) ? Sum4IntegrateTask::LAST_INTEGRATION :
						     Sum4IntegrateTask::MIDDLE_INTEGRATION), stage);
	}
      } else {
	const int fuse = S3DRank::get_fuse_fields();
	for (int s = 0; s < rank->n_spec-1; s+=fuse) {
	  const int max_s = ((s+fuse) < (rank->n_spec-1)) ? (s+fuse) : (rank->n_spec-1);
	  std::vector<FieldID> dst_fields[3];
	  std::vector<FieldID> rr_fields;
	  std::vector<FieldID> rho_y_fields;
	  for (int i = s; i < max_s; i++) {
	    rr_fields.push_back(FID_RR(i));
	    rho_y_fields.push_back(FID_RHO_Y(i));
	    for (int d = 0; d < 3; d++) {
	      dst_fields[d].push_back(FID_RHS_6_D(i, d));
	    }
	  }
	
	  Sum4IntegrateTask::sum_4N_integrate_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top,
						     lr_int, lr_q, lr_qerr, lr_qtmp,
						     dst_fields[0], dst_fields[1], dst_fields[2], rr_fields, rho_y_fields,
						     rank->erk_alpha[stage],
						     rank->erk_beta[stage],
						     rank->erk_error[stage],
						     rank->small_value,
						     f_timestep,
						     ((stage == 0) ? Sum4IntegrateTask::FIRST_INTEGRATION :
						      (stage == (rank->erk_stages - 1)) ? Sum4IntegrateTask::LAST_INTEGRATION :
						      Sum4IntegrateTask::MIDDLE_INTEGRATION), stage);
	}
      }
    }

    // scalar fields - these are hardcoded in the Fortran...
    if(rank->n_scalar > 0) {
      // make sure they match the version we expect (2 scalar fields, based on PRF)
      assert(rank->n_scalar == 2);
      assert(rank->n_spec == 116);

      // first scalar field has no source terms, so use normal sum_3_integrate
      Sum3IntegrateTask::sum_3_integrate_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top,
						lr_int, lr_q, lr_qerr, lr_qtmp,
						FID_SCALAR_RHS_DX(0), FID_SCALAR_RHS_DY(0), FID_SCALAR_RHS_DZ(0),
						FID_SCALAR(0),
						rank->erk_alpha[stage],
						rank->erk_beta[stage],
						rank->erk_error[stage],
						f_timestep,
						((stage == 0) ? Sum3IntegrateTask::FIRST_INTEGRATION :
						 (stage == (rank->erk_stages - 1)) ? Sum3IntegrateTask::LAST_INTEGRATION :
						 Sum3IntegrateTask::MIDDLE_INTEGRATION), stage);

      // second scalar field (for PRF) has a source term of: volume * (rr_r[9] + rr_r[10])
      ScalarSum6IntegrateTask::sum_6_integrate_fields(runtime, ctx, proc_grid_bounds, is_grid, ip_top,
						      lr_int, lr_q, lr_qerr, lr_qtmp,
						      FID_SCALAR_RHS_DX(1), FID_SCALAR_RHS_DY(1), FID_SCALAR_RHS_DZ(1),
						      FID_VOLUME, FID_RR(8), FID_RR(9),
						      FID_SCALAR(1),
						      rank->erk_alpha[stage],
						      rank->erk_beta[stage],
						      rank->erk_error[stage],
						      f_timestep,
						      ((stage == 0) ? ScalarSum6IntegrateTask::FIRST_INTEGRATION :
						       (stage == (rank->erk_stages - 1)) ? ScalarSum6IntegrateTask::LAST_INTEGRATION :
						       ScalarSum6IntegrateTask::MIDDLE_INTEGRATION), stage);
    }

    // no final checks we can perform here :(
  }

  // tracing ends before the handoff task, as it is optional
  runtime->end_trace(ctx, TRACE_FULL_INTEGRATION);

  // step N: pass rhs output data back to MPI side (reads to all fields of q,
  //   qerr)
  // NOTE: We only want to do this if rank->cur_handback is true, but that is
  //  only valid once AwaitMPITask has actually returned, so we'll block on
  //  that future here now.  This means that our runtime analysis can get at
  //  most one timestep ahead right now - this may be a limiter for smaller
  //  chemical mechanisms.
  f_timestep.get_void_result();  // don't actually need the value here
  //printf("await returned: %d\n", rank->cur_handback);
  if(rank->cur_handback) {
    HandoffToMPITask handoff_launcher(rank, Domain::from_rect<3>(proc_grid_bounds),
                                      TaskArgument(0, 0), empty_map);
    handoff_launcher.tag = (rank->erk_stages-1);
    FutureMap fm = runtime->execute_index_space(ctx, handoff_launcher);
    // cannot wait if using ghost cells and barriers
    if (S3DRank::get_perform_waits() || !S3DRank::get_explicit_ghost_cells())
      fm.wait_all_results();
  }

  // Mark that we've completed another frame for the mapper
  runtime->complete_frame(ctx);
}

template <typename T, unsigned DIM, typename AT>
const T* copy_from_fortran_array(const T* data, const Rect<DIM>& bounds, AT field_accessor)
{
  // this is a fortran array we're loading from, so enumerate the points in fortran order
  for(GenericPointInRectIterator<DIM> pir(bounds); pir; pir++)
    field_accessor.write(DomainPoint::from_point<3>(pir.p), *data++);

  return data;
}

template <typename T, typename AT>
const T* copy_from_fortran_array(const T* data, const Rect<3>& bounds, AT field_accessor)
{
  ByteOffset offsets[3];
  Rect<3> subrect;
  T *target = field_accessor.template raw_rect_ptr<3>(bounds, subrect, offsets);
  if(target && (subrect == bounds)) {
    // dense version
    if (offsets_are_dense<3, T>(bounds, offsets)) {
      size_t n_pts = bounds.volume();
      memcpy(target, data, n_pts*sizeof(T));
      return (data+n_pts);
    }
    // fast version
    for(int z = bounds.lo[2]; z <= bounds.hi[2]; z++) {
      T *t_y = target;
      for(int y = bounds.lo[1]; y <= bounds.hi[1]; y++) {
	T *t_x = t_y;
	for(int x = bounds.lo[0]; x <= bounds.hi[0]; x++) {
	  *t_x = *data++;  t_x += offsets[0];
	}
	t_y += offsets[1];
      }
      target += offsets[2];
    }
    return data;
  }

  // this is a fortran array we're loading from, so enumerate the points in fortran order
  for(GenericPointInRectIterator<3> pir(bounds); pir; pir++)
    field_accessor.write(DomainPoint::from_point<3>(pir.p), *data++);

  return data;
}

template <typename T, unsigned DIM, typename AT>
T* copy_to_fortran_array(T* data, const Rect<DIM>& bounds, AT field_accessor)
{
  // this is a fortran array we're storing to, so enumerate the points in fortran order
  for(GenericPointInRectIterator<DIM> pir(bounds); pir; pir++)
    *data++ = field_accessor.read(DomainPoint::from_point<DIM>(pir.p));

  return data;
}

template <typename T, typename AT>
T* copy_to_fortran_array(T* data, const Rect<3>& bounds, AT field_accessor)
{
  ByteOffset offsets[3];
  Rect<3> subrect;
  T *target = field_accessor.template raw_rect_ptr<3>(bounds, subrect, offsets);
  if(target && (subrect == bounds)) {
    // dense version
    if (offsets_are_dense<3, T>(bounds, offsets)) {
      size_t n_pts = bounds.volume();
      memcpy(data, target, n_pts*sizeof(T));
      return (data+n_pts);
    }  
    // fast version
    for(int z = bounds.lo[2]; z <= bounds.hi[2]; z++) {
      T *t_y = target;
      for(int y = bounds.lo[1]; y <= bounds.hi[1]; y++) {
	T *t_x = t_y;
	for(int x = bounds.lo[0]; x <= bounds.hi[0]; x++) {
	  *data++ = *t_x;  t_x += offsets[0];
	}
	t_y += offsets[1];
      }
      target += offsets[2];
    }
    return data;
  }

  // this is a fortran array we're storing to, so enumerate the points in fortran order
  for(GenericPointInRectIterator<3> pir(bounds); pir; pir++)
    *data++ = field_accessor.read(DomainPoint::from_point<3>(pir.p));

  return data;
}

void get_init_temp_task(const Task *task,
                        const std::vector<PhysicalRegion> &regions,
                        Context ctx, HighLevelRuntime *runtime)
{
  if (S3DRank::get_show_progress())
    printf("init temp task, pt = (%d, %d, %d)\n", 
	   task->index_point.get_point<3>()[0], 
	   task->index_point.get_point<3>()[1], 
	   task->index_point.get_point<3>()[2]);

  S3DRank *rank = S3DRank::get_rank(task->index_point.get_point<3>(), true);

  Blockify<3> grid2proc_map(rank->local_grid_size);
  Rect<3> my_subgrid_bounds = grid2proc_map.preimage(task->index_point.get_point<3>());

  //printf("Legion: waiting on mpi\n");
  rank->handshake->legion_wait_on_ext();

  //printf("Legion: done waiting on MPI\n");
  const double *fortran_temp = rank->temp_data;
  //printf("temp data is %p\n", fortran_temp);
  copy_from_fortran_array(fortran_temp, my_subgrid_bounds, 
                      regions[0].get_field_accessor(FID_TEMP).typeify<double>());

#ifdef RUN_CHECKS
  const double temp_hi = 3500.0 / T_REF;
  const double temp_lo = 250.0 / T_REF;
  int n_pts = my_subgrid_bounds.volume();
  for (int idx = 0; idx < n_pts; idx++)
    assert((fortran_temp[idx] >= temp_lo) && (fortran_temp[idx] <= temp_hi));
#endif

  //printf("Legion: data copied - ack'ing MPI\n");
  rank->handshake->legion_handoff_to_ext();
}

AwaitMPITask::AwaitMPITask(S3DRank *r,
                           Domain domain,
                           TaskArgument global_arg,
                           ArgumentMap arg_map,
                           Predicate pred,
                           bool must,
                           MapperID id,
                           MappingTagID tag,
                           bool add_requirements)
 : IndexLauncher(AwaitMPITask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | AwaitMPITask::MAPPER_TAG), rank(r)
{
  // if rank->cur_handback is false, we didn't give data back to Fortran
  //  on the previous timestep, so we're not going to copy stuff back now,
  //  and don't need to specify any region requirements
  // (leaving them out also means that much of the timestep can proceed
  //  without waiting for the handshake from the Fortran side)
  if (add_requirements && rank->cur_handback)
  {
    std::set<FieldID> temp_priv_fields;
    std::vector<FieldID> temp_inst_fields;
    std::set<FieldID> q_priv_fields; // also useful for rhs
    std::vector<FieldID> q_inst_fields;

    for(int i = FID_RHO_U; i < FID_RHO_Y(rank->n_spec - 1); i++) {
      q_priv_fields.insert(i);
      q_inst_fields.push_back(i);
    }
    for(int i = 0; i < rank->n_scalar; i++) {
      q_priv_fields.insert(FID_SCALAR(i));
      q_inst_fields.push_back(FID_SCALAR(i));
    }

    // If we are doing erk-stages which may not have passed data back
    // to the fortran, then this needs to have read-write privilege
    add_region_requirement(RegionRequirement(rank->lp_q_top, 0, 
                                          q_priv_fields, q_inst_fields,
                                          (rank->erk_stages > 0) ? READ_WRITE : WRITE_DISCARD, 
                                          EXCLUSIVE, rank->lr_q));

    add_region_requirement(RegionRequirement(rank->lp_rhs_top, 0, 
                                          q_priv_fields, q_inst_fields,
                                          WRITE_DISCARD, EXCLUSIVE, rank->lr_rhs));

    // Dummy requirements to get an instance made that everyone can use
    add_region_requirement(RegionRequirement(rank->lp_int_top, 0, 
                                          READ_WRITE, EXCLUSIVE, rank->lr_int));
    add_field(2/*idx*/, FID_VOLUME);
    
    add_region_requirement(RegionRequirement(rank->lp_state_top, 0,
                                          READ_WRITE, EXCLUSIVE, rank->lr_state));
    add_field(3/*idx*/, FID_TEMP);

    if (rank->erk_stages > 0)
      add_region_requirement(RegionRequirement(rank->lp_qerr_top, 0,
                                               q_priv_fields, q_inst_fields,
                                               WRITE_DISCARD, EXCLUSIVE, rank->lr_qerr));
  }
}

/*static*/ const char * const AwaitMPITask::TASK_NAME = "await_mpi_task";

void AwaitMPITask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing
}

TimestepInfo AwaitMPITask::cpu_base_impl(S3DRank *rank, const Task *task, 
					 const Rect<3> &my_subgrid_bounds,
					 const std::vector<RegionRequirement> &reqs,
					 const std::vector<PhysicalRegion> &regions,
					 Context ctx, HighLevelRuntime *runtime)
{
  if (S3DRank::get_show_progress())
    printf("await task, pt = (%d, %d, %d)\n", 
            task->index_point.get_point<3>()[0], 
            task->index_point.get_point<3>()[1], 
            task->index_point.get_point<3>()[2]);

#ifdef DEBUG_HANDSHAKE
  printf("Legion: pass=%d waiting on mpi\n", S3DRank::legion_pass);
#endif

  rank->handshake->legion_wait_on_ext();

#ifdef DEBUG_HANDSHAKE
  printf("Legion: pass=%d done waiting on MPI\n", S3DRank::legion_pass);
#endif
#ifndef NO_COMPUTE
  const double *fortran_q;
  const double *fortran_qs;
#endif
  if(rank->erk_stages > 0) {
    assert(rank->cur_integ_data);
#ifndef NO_COMPUTE
    fortran_q = rank->cur_integ_data->q;
    fortran_qs = rank->cur_integ_data->qs;
#endif
  } else {
    assert(rank->cur_data);
#ifndef NO_COMPUTE
    fortran_q = rank->cur_data->q;
    fortran_qs = rank->cur_data->qs;
#endif
  }
#ifdef DEBUG_HANDSHAKE
  printf("q data is %p\n", fortran_q);
#endif
#ifndef NO_COMPUTE
  // at this point, rank->next_handback says whether the Fortran side
  //  wants to see the result of this timestep - before we copy that
  //  into cur_handback, we'll use the old value to decide whether or not
  //  to copy data back from Fortran

  if(rank->cur_handback) {
    for(int fid = FID_RHO_U; fid < (FID_RHO_Y0 + rank->n_spec - 1); fid++) {
      fortran_q = copy_from_fortran_array(fortran_q, my_subgrid_bounds, 
                    regions[0].get_field_accessor(fid).typeify<double>());
    }
    for(int i = 0; i < rank->n_scalar; i++) {
      fortran_qs = copy_from_fortran_array(fortran_qs, my_subgrid_bounds,
					   regions[0].get_field_accessor(FID_SCALAR(i)).typeify<double>());
    }
  }
#endif

  // update the current handback flag and save timestep info before we
  //   possibly do a handoff back to Fortran below
  rank->cur_handback = rank->next_handback;

  struct TimestepInfo ts;
  ts.cur_time = rank->cur_time;
  ts.cur_timestep = rank->cur_timestep;

  // if we're not being asked to hand back data
  //  on this timestep, we can do the "handoff" immediately
  if(!rank->next_handback) {
    //printf("early handoff to MPI\n");
#ifdef DEBUG_HANDSHAKE
    printf("Legion: pass=%d handoff to MPI\n", S3DRank::legion_pass);
#endif
    rank->handshake->legion_handoff_to_ext();

    S3DRank::get_legion_pass()++;
  }

  return ts;
}

HandoffToMPITask::HandoffToMPITask(S3DRank *r,
                                   Domain domain,
                                   TaskArgument global_arg,
                                   ArgumentMap arg_map,
                                   Predicate pred,
                                   bool must,
                                   MapperID id,
                                   MappingTagID tag,
                                   bool add_requirements)
 : IndexLauncher(HandoffToMPITask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | HandoffToMPITask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    std::set<FieldID> q_priv_fields; // also useful for rhs
    std::vector<FieldID> q_inst_fields;

    for(int i = FID_RHO_U; i < FID_RHO_Y(rank->n_spec - 1); i++) {
      q_priv_fields.insert(i);
      q_inst_fields.push_back(i);
    }
    for(int i = 0; i < rank->n_scalar; i++) {
      q_priv_fields.insert(FID_SCALAR(i));
      q_inst_fields.push_back(FID_SCALAR(i));
    }

    // handoff is different in integration mode - give back q and qerr, not rhs
    if(rank->erk_stages > 0) {
      add_region_requirement(RegionRequirement(rank->lp_q_top, 0, 
					       q_priv_fields, q_inst_fields,
					       READ_ONLY, EXCLUSIVE, rank->lr_q));
      add_region_requirement(RegionRequirement(rank->lp_qerr_top, 0, 
					       q_priv_fields, q_inst_fields,
					       READ_ONLY, EXCLUSIVE, rank->lr_qerr));
    } else {
      add_region_requirement(RegionRequirement(rank->lp_rhs_top, 0, 
					       q_priv_fields, q_inst_fields,
					       READ_ONLY, EXCLUSIVE, rank->lr_rhs));
    }
  }
}

/*static*/ const char * const HandoffToMPITask::TASK_NAME = "handoff_to_mpi_task";

void HandoffToMPITask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing
}

void HandoffToMPITask::cpu_base_impl(S3DRank *rank, const Task *task, 
                                     const Rect<3> &my_subgrid_bounds,
                                     const std::vector<RegionRequirement> &reqs,
                                     const std::vector<PhysicalRegion> &regions,
                                     Context ctx, HighLevelRuntime *runtime)
{
  if (S3DRank::get_show_progress())
    printf("handoff task, pt = (%d, %d, %d)\n", 
            task->index_point.get_point<3>()[0], 
            task->index_point.get_point<3>()[1], 
            task->index_point.get_point<3>()[2]);

  if(rank->erk_stages > 0) {
    // in integration mode, we should only get here if the fortran actually
    //  wants data
    assert(rank->next_handback);

    double *fortran_q = rank->cur_integ_data->q;
#ifndef NO_COMPUTE_
    for(int fid = FID_RHO_U; fid < (FID_RHO_Y0 + rank->n_spec - 1); fid++)
      fortran_q = copy_to_fortran_array(fortran_q, my_subgrid_bounds, 
					regions[0].get_field_accessor(fid).typeify<double>());
#endif
    double *fortran_q_err = rank->cur_integ_data->q_err;
#ifndef NO_COMPUTE_
    for(int fid = FID_RHO_U; fid < (FID_RHO_Y0 + rank->n_spec - 1); fid++)
      fortran_q_err = copy_to_fortran_array(fortran_q_err, my_subgrid_bounds, 
					    regions[1].get_field_accessor(fid).typeify<double>());
#endif
    double *fortran_qs = rank->cur_integ_data->qs;
#ifndef NO_COMPUTE_
    for(int i = 0; i < rank->n_scalar; i++)
      fortran_qs = copy_to_fortran_array(fortran_qs, my_subgrid_bounds, 
					 regions[0].get_field_accessor(FID_SCALAR(i)).typeify<double>());
#endif
  } else {
    // single rhsf() call
    double *fortran_rhs = rank->cur_data->rhs;
#ifdef DEBUG_HANDSHAKE
    printf("rhs data is %p\n", fortran_rhs);
#endif
#ifndef NO_COMPUTE_
    for(int fid = FID_RHO_U; fid < (FID_RHO_Y0 + rank->n_spec - 1); fid++)
      fortran_rhs = copy_to_fortran_array(fortran_rhs, my_subgrid_bounds, 
					  regions[0].get_field_accessor(fid).typeify<double>());
#endif
    double *fortran_rhs_scalar = rank->cur_data->rhs_scalar;
#ifdef DEBUG_HANDSHAKE
    printf("rhs data is %p\n", fortran_rhs);
#endif
#ifndef NO_COMPUTE_
    for(int i = 0; i < rank->n_scalar; i++)
      fortran_rhs_scalar = copy_to_fortran_array(fortran_rhs_scalar, my_subgrid_bounds, 
	                                         regions[0].get_field_accessor(FID_SCALAR(i)).typeify<double>());
#endif
  }

#ifdef DEBUG_HANDSHAKE
  printf("Legion: pass=%d handoff to MPI\n", S3DRank::legion_pass);
#endif
  rank->handshake->legion_handoff_to_ext();

  S3DRank::get_legion_pass()++;
}

void clear_field_task(const Task *task,
		      const std::vector<PhysicalRegion> &regions,
		      Context ctx, HighLevelRuntime *runtime)
{
  if (S3DRank::get_show_progress())
    printf("clear field task, pt = (%d, %d, %d), fid = %d\n", 
	   task->index_point.get_point<3>()[0], 
	   task->index_point.get_point<3>()[1], 
	   task->index_point.get_point<3>()[2],
	   task->regions[0].instance_fields[0]);

  S3DRank *rank = S3DRank::get_rank(task->index_point.get_point<3>(), true);

  Blockify<3> grid2proc_map(rank->local_grid_size);
  Rect<3> my_subgrid_bounds = grid2proc_map.preimage(task->index_point.get_point<3>());

  RegionAccessor<AccessorType::Generic,double> fa = 
      regions[0].get_field_accessor(task->regions[0].instance_fields[0]).typeify<double>();

  assert(task->arglen == sizeof(double));
  double clearval = *(double *)(task->args);

  for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++)
    fa.write(DomainPoint::from_point<3>(pir.p), clearval);
}

void clear_field(HighLevelRuntime *runtime, Context ctx, 
		 LogicalRegion lr, LogicalPartition lp, const Rect<3>& proc_grid_bounds,
		 FieldID fid, double clearval)
{
  IndexLauncher launcher(CLEAR_FIELD_TASK_ID,
			 Domain::from_rect<3>(proc_grid_bounds),
			 TaskArgument(&clearval, sizeof(double)),
			 ArgumentMap());

  launcher.tag = RHSF_MAPPER_FORCE_RANK_MATCH;

  RegionRequirement rr(lp, 0, WRITE_DISCARD, EXCLUSIVE, lr);
  rr.add_field(fid);
  launcher.add_region_requirement(rr);

  runtime->execute_index_space(ctx, launcher);
}

