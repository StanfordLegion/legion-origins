#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <cstring>

#ifndef SHARED_LOWLEVEL
#define OMPI_SKIP_MPICXX
#include <mpi.h>
#endif

#include "rhsf.h"
#include "dump_file.h"

#ifndef SHARED_LOWLEVEL
#define CHECK_MPI(cmd) do { \
  int ret = (cmd); \
  if(ret != MPI_SUCCESS) { \
    char buffer[256]; int len = 256; \
    MPI_Error_string(ret, buffer, &len); buffer[len] = 0; \
    fprintf(stderr, "MPI: %s = %d (%s)\n", #cmd, ret, buffer); \
    exit(1); \
  } \
} while(0)
#else
#include <pthread.h>

#define CHECK_PTHREAD(cmd) do { \
  int ret = (cmd); \
  if(ret != 0) { \
    fprintf(stderr, "PTHREAD: %s = %d (%s)\n", #cmd, ret, strerror(ret)); \
    exit(1); \
  } \
} while(0)
#endif

static void read_grid_size(const char *s, int *grid)
{
  char *pos;

  grid[0] = strtol(s, &pos, 10);

  if(*pos == 'x') {
    grid[1] = strtol(pos+1, &pos, 10);
    assert(*pos == 'x');
    grid[2] = strtol(pos+1, &pos, 10);
    assert(*pos == 0);
  } else {
    grid[1] = grid[0];
    grid[2] = grid[1];
  }
}

static int local_grid[3] = { 16, 16, 16 };
static int proc_grid[3] = { 1, 1, 1 };
static int dist_grid[3] = { 1, 1, 1 };
static int global_grid[3] = { 0, 0, 0 };
static int vary_in_dims[3] = { 1, 1, 1 };
static int nvar_tot = 57;
static int n_spec = 52;
static int n_reg = 3;
static int iorder = 8;
static int iforder = 10;
static int n_stages = 1;
static int n_steps = 1;
static int n_subranks = 0;
static char dump_file_dir[256] = ".";
static int frame_skip = 0;
static int lagging = 0;

template <int DIM, typename PTR>
static bool load_array(const DumpedArray *da, const char *name, const Point<DIM>& exp_size, PTR& data_ptr)
{
  if(strcmp(da->get_name(), name))
    return false;

  assert(da->get_rank() == DIM);

  Point<DIM> dims = da->get_dims<DIM>();

  assert(dims == exp_size);

  data_ptr = da->load_data<double>();

  return true;
}

static void do_single_rank(int rank, int size)
{
  int proc_id[3];

  assert((proc_grid[0] * proc_grid[1] * proc_grid[2]) == size);
  proc_id[0] = rank % proc_grid[0];
  proc_id[1] = (rank / proc_grid[0]) % proc_grid[1];
  proc_id[2] = (rank / proc_grid[0]) / proc_grid[1];

  for(int i = 0; i < 3; i++)
    global_grid[i] = local_grid[i] * proc_grid[i];

  // chemistry is always enabled right now
  int i_react = 1;
  double scale_dims[3] = { 1.0, 1.0, 1.0 };
  int n_scalar = 0;
  
#ifdef USE_CEMA
  int cema_samples_per_rank = 10;
#endif

  S3DRank *s3d_rank = new S3DRank(local_grid, global_grid, dist_grid, proc_grid, proc_id, vary_in_dims,
				  &nvar_tot, &n_spec, &n_reg, &n_scalar, &iorder, &iforder,
				  &n_stages, &n_steps, &n_subranks, &lagging, &lagging,
				  &i_react, scale_dims
#ifdef USE_CEMA
				  , &cema_samples_per_rank
#endif
				  );
  // No extra configuraiton here so we can do this immediately
  s3d_rank->complete_configure();

  char filename[256];
  sprintf(filename, "%s/s3d_dump_%03d_%03d_%03d.dat", dump_file_dir, proc_id[0], proc_id[1], proc_id[2]);
  DumpFile *df = new DumpFile(filename);

  Point<3> exp_size_3d(local_grid);
  Point<4> exp_size_4d_nvar;
  exp_size_4d_nvar.x[0] = local_grid[0];
  exp_size_4d_nvar.x[1] = local_grid[1];
  exp_size_4d_nvar.x[2] = local_grid[2];
  exp_size_4d_nvar.x[3] = nvar_tot;
  Point<4> exp_size_4d_3;
  exp_size_4d_3.x[0] = local_grid[0];
  exp_size_4d_3.x[1] = local_grid[1];
  exp_size_4d_3.x[2] = local_grid[2];
  exp_size_4d_3.x[3] = 3;
  Point<4> exp_size_4d_nspec;
  exp_size_4d_nspec.x[0] = local_grid[0];
  exp_size_4d_nspec.x[1] = local_grid[1];
  exp_size_4d_nspec.x[2] = local_grid[2];
  exp_size_4d_nspec.x[3] = n_spec;
  Point<5> exp_size_5d_3x3;
  exp_size_5d_3x3.x[0] = local_grid[0];
  exp_size_5d_3x3.x[1] = local_grid[1];
  exp_size_5d_3x3.x[2] = local_grid[2];
  exp_size_5d_3x3.x[3] = 3;
  exp_size_5d_3x3.x[4] = 3;
  Point<5> exp_size_5d_nspecx3;
  exp_size_5d_nspecx3.x[0] = local_grid[0];
  exp_size_5d_nspecx3.x[1] = local_grid[1];
  exp_size_5d_nspecx3.x[2] = local_grid[2];
  exp_size_5d_nspecx3.x[3] = n_spec;
  exp_size_5d_nspecx3.x[4] = 3;

  int num_cpe_pts = 0;
  double *enthCoeff_aa = 0;
  double *enthCoeff_bb = 0;
  double *cpCoeff_aa = 0;
  double *cpCoeff_bb = 0;
  const double *init_temp = 0;

  for(int mpi_pass = 0; mpi_pass < n_steps * n_stages; mpi_pass++) {
    S3DRank::RHSFArrays data;

    while(true) {
      DumpedArray *da = df->get_next_array();
      if(!da) {
	printf("ran out of input data?\n");
	exit(1);
      }

      if(!strcmp(da->get_name(), "enthCoeff_aa")) {
	Point<2> dims = da->get_dims<2>();
	assert(dims[0] == n_spec);
	assert((num_cpe_pts == 0) || (num_cpe_pts == dims[1]));
	num_cpe_pts = dims[1];
	enthCoeff_aa = da->load_data<double>();
	continue;
      }

      if(!strcmp(da->get_name(), "enthCoeff_bb")) {
	Point<2> dims = da->get_dims<2>();
	assert(dims[0] == n_spec);
	assert((num_cpe_pts == 0) || (num_cpe_pts == dims[1]));
	num_cpe_pts = dims[1];
	enthCoeff_bb = da->load_data<double>();
	continue;
      }

      if(!strcmp(da->get_name(), "cpCoeff_aa")) {
	Point<2> dims = da->get_dims<2>();
	assert(dims[0] == n_spec);
	assert((num_cpe_pts == 0) || (num_cpe_pts == dims[1]));
	num_cpe_pts = dims[1];
	cpCoeff_aa = da->load_data<double>();
	continue;
      }

      if(!strcmp(da->get_name(), "cpCoeff_bb")) {
	Point<2> dims = da->get_dims<2>();
	assert(dims[0] == n_spec);
	assert((num_cpe_pts == 0) || (num_cpe_pts == dims[1]));
	num_cpe_pts = dims[1];
	cpCoeff_bb = da->load_data<double>();
	continue;
      }

      if(frame_skip > 0) {
	if(!strcmp(da->get_name(), "rhs"))
	  frame_skip--;
	continue;
      }

      if(load_array<3>(da, "temp_in", exp_size_3d, data.temp_in)) {
	if(!init_temp) {
	  init_temp = data.temp_in;
          // We also need to provide CPE data prior to setting
          // the temperature because Legion will run with the
          // temperature once it gets it and it will need the CPE
          // data immediately.
          assert(num_cpe_pts > 0);
          assert(enthCoeff_aa != NULL);
          assert(enthCoeff_bb != NULL);
          assert(cpCoeff_aa != NULL);
          assert(cpCoeff_bb != NULL);
          s3d_rank->provide_cpe_data(num_cpe_pts, enthCoeff_aa, enthCoeff_bb, cpCoeff_aa, cpCoeff_bb);
	  s3d_rank->provide_initial_temperature(init_temp);
	}
	continue;
      }

      if(load_array<4>(da, "q", exp_size_4d_nvar, data.q)) continue;

      // rhs is always the last one per timestep
      if(load_array<4>(da, "rhs", exp_size_4d_nvar, data.rhs)) {
	// we compare against this data too
	data.rhs_cmp = data.rhs;
	break;
      }

      if(load_array<3>(da, "volume", exp_size_3d, data.volume)) continue;
      if(load_array<3>(da, "temp", exp_size_3d, data.temperature)) continue;
      if(load_array<3>(da, "avmolwt", exp_size_3d, data.avmolwt)) continue;
      if(load_array<3>(da, "mixMW", exp_size_3d, data.mixmw)) continue;
      if(load_array<3>(da, "cpmix", exp_size_3d, data.cpmix)) continue;
      if(load_array<3>(da, "gamma", exp_size_3d, data.gamma)) continue;
      if(load_array<3>(da, "pressure", exp_size_3d, data.pressure)) continue;

      if(load_array<4>(da, "yspecies", exp_size_4d_nspec, data.yspecies)) continue;
      if(load_array<4>(da, "h_spec", exp_size_4d_nspec, data.h_spec)) continue;

      if(load_array<5>(da, "grad_u", exp_size_5d_3x3, data.grad_u)) continue;
      if(load_array<5>(da, "grad_Ys", exp_size_5d_nspecx3, data.grad_ys)) continue;

      if(load_array<4>(da, "grad_T", exp_size_4d_3, data.grad_T)) continue;
      if(load_array<4>(da, "grad_mixMW", exp_size_4d_3, data.grad_mixmw)) continue;
      if(load_array<5>(da, "Ydiffflux", exp_size_5d_nspecx3, data.ydiffflux)) continue;
      if(load_array<4>(da, "heatflux", exp_size_4d_3, data.heatflux)) continue;

      if(load_array<3>(da, "viscosity", exp_size_3d, data.viscosity)) continue;
      if(load_array<3>(da, "lambda", exp_size_3d, data.lambda)) continue;
      if(load_array<4>(da, "Ds_mixavg", exp_size_4d_nspec, data.ds_mixavg)) continue;
      if(load_array<4>(da, "Rs_therm_diff", exp_size_4d_nspec, data.rs_therm_diff)) continue;
      if(load_array<5>(da, "tau", exp_size_5d_3x3, data.tau)) continue;
      if(load_array<4>(da, "diffusion", exp_size_4d_nspec, data.diffusion)) continue;
      if(load_array<4>(da, "rr_r", exp_size_4d_nspec, data.rr_r)) continue;

      printf("unused array: %s\n", da->get_name());
    }

    assert(data.q);
    assert(data.rhs);
    if(!data.q)
      data.q = new double[local_grid[0] * local_grid[1] * local_grid[2] * nvar_tot];
    if(!data.rhs)
      data.rhs = new double[local_grid[0] * local_grid[1] * local_grid[2] * nvar_tot];

    //if(num_cpe_pts)
    //  s3d_rank->provide_cpe_data(num_cpe_pts, enthCoeff_aa, enthCoeff_bb, cpCoeff_aa, cpCoeff_bb);

    printf("MPI: pass=%d handoff to legion\n", mpi_pass);
    s3d_rank->begin_rhsf_step(&data, 0.000384944); // this is what fortran passes for timestep = 5e-9

    printf("MPI: pass=%d waiting on legion\n", mpi_pass);
    s3d_rank->finish_rhsf_step();

    printf("MPI: pass=%d done waiting on legion\n", mpi_pass);

    data.release_memory();
  }

  delete s3d_rank;
}

#ifdef SHARED_LOWLEVEL
static void *fakempi_thread_entry(void *data)
{
  int myrank = (off_t)data;

  int mysize = proc_grid[0] * proc_grid[1] * proc_grid[2];

  do_single_rank(myrank, mysize);

  return 0;
}
#endif

int main(int argc, const char *argv[])
{

  for(int i = 1; i < argc; i++) {
    if(!strcmp(argv[i], "-d")) {
      strcpy(dump_file_dir, argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-l")) {
      read_grid_size(argv[++i], local_grid);
      continue;
    }

    if(!strcmp(argv[i], "-p")) {
      read_grid_size(argv[++i], proc_grid);
      continue;
    }

    if(!strcmp(argv[i], "-r")) {
      read_grid_size(argv[++i], dist_grid);
      continue;
    }

    if(!strcmp(argv[i], "-s")) {
      n_spec = atoi(argv[++i]);
      nvar_tot = n_spec + 4;
      continue;
    }

    if(!strcmp(argv[i], "-sr")) {
      n_subranks = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-f")) {
      frame_skip = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-n")) {
      char *pos;
      n_steps = strtol(argv[++i], &pos, 10);
      if(*pos) {
	n_stages = atoi(pos+1);
      } else {
	n_stages = 1;
      }
      continue;
    }

    if(!strcmp(argv[i], "-lag")) {
      lagging = atoi(argv[++i]);
      continue;
    }
  }

  // let the rhsf implementation also see the args
  S3DRank::parse_cmdline_args(argc, argv);

#ifndef SHARED_LOWLEVEL
  CHECK_MPI( MPI_Init(&argc, (char ***)&argv) );

  int rank, size;
  CHECK_MPI( MPI_Comm_rank(MPI_COMM_WORLD, &rank) );
  CHECK_MPI( MPI_Comm_size(MPI_COMM_WORLD, &size) );
  printf("rank=%d, size=%d\n", rank, size);

  assert((proc_grid[0] * proc_grid[1] * proc_grid[2]) == size);
  do_single_rank(rank, size);

  CHECK_MPI( MPI_Finalize() );
#else
  int size = proc_grid[0] * proc_grid[1] * proc_grid[2];

  pthread_t *threads = new pthread_t[size];

  for(int i = 0; i < size; i++)
    CHECK_PTHREAD( pthread_create(&threads[i], 0, fakempi_thread_entry, (void *)(off_t)i) );

  void *dummy_ret;
  for(int i = 0; i < size; i++)
    CHECK_PTHREAD( pthread_join(threads[i], &dummy_ret) );
#endif

  return 0;
}
