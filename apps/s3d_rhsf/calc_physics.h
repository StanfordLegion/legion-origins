
#ifndef _CALC_PHYSICS_H_
#define _CALC_PHYSICS_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

class CalcDiffusionTask : public IndexLauncher {
public:
  CalcDiffusionTask(S3DRank *rank,
                    Domain domain,
                    TaskArgument global_arg,
                    ArgumentMap arg_map,
                    Predicate pred = Predicate::TRUE_PRED,
                    bool must = false,
                    MapperID id = 0,
                    MappingTagID tag = 0,
                    bool add_requirements = true,
                    bool redundant = false);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_DIFFUSION_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_PRIORITIZE;
protected:
  static bool dense_diffusion(const Rect<3> &my_subgrid_bounds,
                              const std::vector<RegionRequirement> &reqs,
                              const std::vector<PhysicalRegion> &regions,
                              RegionAccessor<AccessorType::Generic,double> fa_mixmw,
                              RegionAccessor<AccessorType::Generic,double> fa_temp,
                              RegionAccessor<AccessorType::Generic,double> fa_pressure);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcViscosityTask : public IndexLauncher {
public:
  CalcViscosityTask(S3DRank *rank,
                    Domain domain,
                    TaskArgument global_arg,
                    ArgumentMap arg_map,
                    Predicate pred = Predicate::TRUE_PRED,
                    bool must = false,
                    MapperID id = 0,
                    MappingTagID tag = 0,
                    bool add_requirements = true,
                    bool redundant = false);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_VISCOSITY_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
protected:
  static bool dense_viscosity(const Rect<3> &my_subgrid_bounds,
                              const std::vector<RegionRequirement> &reqs,
                              const std::vector<PhysicalRegion> &regions,
                              RegionAccessor<AccessorType::Generic,double> fa_viscosity,
                              RegionAccessor<AccessorType::Generic,double> fa_mixmw,
                              RegionAccessor<AccessorType::Generic,double> fa_temp);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcConductivityTask : public IndexLauncher {
public:
  CalcConductivityTask(S3DRank *rank,
                       Domain domain,
                       TaskArgument global_arg,
                       ArgumentMap arg_map,
                       Predicate pred = Predicate::TRUE_PRED,
                       bool must = false,
                       MapperID id = 0,
                       MappingTagID tag = 0,
                       bool add_requirements = true,
                       bool redundant = false);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_CONDUCTIVITY_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
protected:
  static bool dense_conductivity(const Rect<3> &my_subgrid_bounds,
                                 const std::vector<RegionRequirement> &reqs,
                                 const std::vector<PhysicalRegion> &regions,
                                 RegionAccessor<AccessorType::Generic,double> fa_lambda,
                                 RegionAccessor<AccessorType::Generic,double> fa_mixmw,
                                 RegionAccessor<AccessorType::Generic,double> fa_temp);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcThermalTask : public IndexLauncher {
public:
  CalcThermalTask(S3DRank *rank,
                  Domain domain,
                  TaskArgument global_arg,
                  ArgumentMap arg_map,
                  Predicate pred = Predicate::TRUE_PRED,
                  bool must = false,
                  MapperID id = 0,
                  MappingTagID tag = 0,
                  bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_THERMAL_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
protected:
  static bool dense_thermal(const Rect<3> &my_subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            RegionAccessor<AccessorType::Generic,double> fa_mixmw,
                            RegionAccessor<AccessorType::Generic,double> fa_temp);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

#endif

