
#ifndef _IMPOSE_PRESSURE_H_
#define _IMPOSE_PRESSURE_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

template <typename T>
class ReductionSum {
public:
  typedef T LHS;
  typedef T RHS;
  static const T identity;

  template <bool EXCLUSIVE> static void apply(LHS &lhs, RHS rhs) { lhs += rhs; }

  template <bool EXCLUSIVE> static void fold(RHS &rhs1, RHS rhs2) { rhs1 += rhs2; }
};

template <typename T>
/*static*/ const T ReductionSum<T>::identity = 0;

class CompressionSourceTerm {
 public:
  CompressionSourceTerm(double _geomfac2, double _polyn,
			double _rpm, double _ttdc, double _toff, double _tp, double _t90,
			double _ptdc, double _pc, bool _enforce);
  ~CompressionSourceTerm(void);

  double calc_target_pressure(double t) const;
  double calc_target_dpdt(double t) const;

 protected:
  double geomfac2, polyn;
  double rpm;
  double ttdc, toff, tp, t90;  // times are measured in CAD (crank angle degrees)
  double ptdc, pc;
  bool enforce;
};

class SumFieldTask : public IndexLauncher {
 public:
  SumFieldTask(S3DRank *rank,
	       LogicalRegion lr,
	       LogicalPartition lp,
	       FieldID field,
	       Domain domain,
	       TaskArgument global_arg,
	       ArgumentMap arg_map,
	       Predicate pred = Predicate::TRUE_PRED,
	       bool must = false,
	       MapperID id = 0,
	       MappingTagID tag = 0,
	       bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = SUM_FIELD_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_ALL_GPU;

  static double cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
			      const std::vector<RegionRequirement> &reqs,
			      const std::vector<PhysicalRegion> &regions,
			      Context ctx, HighLevelRuntime *runtime);
  static double gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
			      const std::vector<RegionRequirement> &reqs,
			      const std::vector<PhysicalRegion> &regions,
			      Context ctx, HighLevelRuntime *runtime);
};

class CalcImposedPressureTask : public IndexLauncher {
 public:
  CalcImposedPressureTask(S3DRank *rank,
			  Domain domain,
			  TaskArgument global_arg,
			  ArgumentMap arg_map,
			  Predicate pred = Predicate::TRUE_PRED,
			  bool must = false,
			  MapperID id = 0,
			  MappingTagID tag = 0,
			  bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_IMPOSED_PRESSURE_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_ALL_GPU;

protected:
  static double dense_calc_imposed(S3DRank *rank, const Rect<3> &subgrid_bounds,
				   bool& ok,
				   const std::vector<PhysicalRegion> &regions);
public:
  static double cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
			      const std::vector<RegionRequirement> &reqs,
			      const std::vector<PhysicalRegion> &regions,
			      Context ctx, HighLevelRuntime *runtime);
  // no gpu implementation
};

class CalcSourceTermTask : public IndexLauncher {
public:
  CalcSourceTermTask(S3DRank *rank,
		     Domain domain,
		     TaskArgument global_arg,
		     ArgumentMap arg_map,
		     Predicate pred = Predicate::TRUE_PRED,
		     bool must = false,
		     MapperID id = 0,
		     MappingTagID tag = 0,
		     bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SOURCE_TERM_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
protected:
  static bool dense_calc_source(S3DRank *rank, const Rect<3> &subgrid_bounds,
				RegionAccessor<AccessorType::Generic,double> fa_in_rho,
				RegionAccessor<AccessorType::Generic,double> fa_out_avmolwt,
				RegionAccessor<AccessorType::Generic,double> fa_out_mixmw,
				const std::vector<RegionRequirement> &reqs,
				const std::vector<PhysicalRegion> &regions);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class IntegrateSourceTask : public IndexLauncher {
public:
  enum { FIRST_INTEGRATION, MIDDLE_INTEGRATION, LAST_INTEGRATION };

  IntegrateSourceTask(Domain domain,
		      TaskArgument global_arg,
		      ArgumentMap arg_map,
		      Predicate pred = Predicate::TRUE_PRED,
		      bool must = false,
		      MapperID id = 0,
		      MappingTagID tag = 0);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_INTEGRATE_SOURCE_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_BALANCE | RHSF_MAPPER_ALL_GPU;
protected:
  static bool dense_sum_3_integrate(const Rect<3> &my_subgrid_bounds,
				    RegionAccessor<AccessorType::Generic,double> fa_q,
				    RegionAccessor<AccessorType::Generic,double> fa_qerr,
				    RegionAccessor<AccessorType::Generic,double> fa_qtmp,
				    RegionAccessor<AccessorType::Generic,double> fa_1,
				    RegionAccessor<AccessorType::Generic,double> fa_2,
				    RegionAccessor<AccessorType::Generic,double> fa_3,
				    double alpha, double beta, double err, int which);
  static bool fast_sum_3_integrate(RegionAccessor<AccessorType::Generic,double> fa_q,
				   RegionAccessor<AccessorType::Generic,double> fa_qerr,
				   RegionAccessor<AccessorType::Generic,double> fa_qtmp,
				   RegionAccessor<AccessorType::Generic,double> fa_1,
				   RegionAccessor<AccessorType::Generic,double> fa_2,
				   RegionAccessor<AccessorType::Generic,double> fa_3,
				   Rect<3> subgrid_bounds,
				   double alpha, double beta, double err, int which);
  static void kernel_sum_3_integrate(size_t n_pts,
				     double *q, double *qerr, double *qtmp,
				     const double *in1, const double *in2, const double *in3,
				     double alpha, double beta, double err, int which);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, 
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task,
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  // second component is needed only for energy source term - set fid2 to -1 in all
  //  other cases
  static void integrate_field(HighLevelRuntime *runtime, Context ctx, 
			      const Rect<3>& proc_grid_bounds,
			      IndexSpace is_grid, IndexPartition ip_top, 
			      LogicalRegion lr_int, LogicalRegion lr_rhs,
			      LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
			      FieldID fid, FieldID fid2,
			      double alpha, double beta, double err, Future f_timestep, 
			      int which, int stage, int tag = 0);

  static void integrate_fields(HighLevelRuntime *runtime, Context ctx, 
			       const Rect<3>& proc_grid_bounds,
			       IndexSpace is_grid, IndexPartition ip_top, 
			       LogicalRegion lr_int, LogicalRegion lr_rhs,
			       LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
			       const std::vector<FieldID>& fids,
			       double alpha, double beta, double err, Future f_timestep, 
			       int which, int stage, int tag = 0);
};

#endif
