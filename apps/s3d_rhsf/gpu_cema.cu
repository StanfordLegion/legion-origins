#include "philox.h"

typedef Philox_2x32<10> Rand;

__global__
void gpu_cema_samples_kernel(unsigned first_sample, unsigned n_spec,
			     unsigned seq_hi, unsigned seq_lo,
			     unsigned dim_x, unsigned dim_y, unsigned dim_z,	
			     int *sample_x_ptr,
			     int *sample_y_ptr,
			     int *sample_z_ptr,
			     double *sample_temp_ptr,
			     double *sample_press_ptr,
			     double *sample_ys_ptr,
			     int sample_ys_stride,
			     const double *grid_temp_ptr,
			     const double *grid_press_ptr,
			     const double *grid_ys_ptr,
			     int grid_x_step,
			     int grid_y_step,
			     int grid_z_step,
			     int grid_ys_stride)
{
  // each thread does a single sample
  int rel_sample_idx = threadIdx.x;
  int abs_sample_idx = first_sample + rel_sample_idx;

  // random numbers for x, y, z
  int x = Rand::rand_int(abs_sample_idx, seq_hi, seq_lo + 0, dim_x);
  int y = Rand::rand_int(abs_sample_idx, seq_hi, seq_lo + 1, dim_y);
  int z = Rand::rand_int(abs_sample_idx, seq_hi, seq_lo + 2, dim_z);

  sample_x_ptr[rel_sample_idx] = x;
  sample_y_ptr[rel_sample_idx] = y;
  sample_z_ptr[rel_sample_idx] = z;

  int grid_ofs = ((x * grid_x_step) +
			 (y * grid_y_step) +
			 (z * grid_z_step));

  sample_temp_ptr[rel_sample_idx] = *(grid_temp_ptr + grid_ofs);
  sample_press_ptr[rel_sample_idx] = *(grid_press_ptr + grid_ofs);
  for(int i = 0; i < n_spec; i++) {
    sample_ys_ptr[rel_sample_idx] = *(grid_ys_ptr + grid_ofs);
    sample_ys_ptr += sample_ys_stride;
    grid_ys_ptr += grid_ys_stride;
  }
}

void gpu_cema_samples(unsigned first_sample, unsigned num_samples, unsigned n_spec,
		      unsigned seq_hi, unsigned seq_lo,
		      unsigned dim_x, unsigned dim_y, unsigned dim_z,	
		      int *sample_x_ptr,
		      int *sample_y_ptr,
		      int *sample_z_ptr,
		      double *sample_temp_ptr,
		      double *sample_press_ptr,
		      double *sample_ys_ptr,
		      int sample_ys_stride,
		      const double *grid_temp_ptr,
		      const double *grid_press_ptr,
		      const double *grid_ys_ptr,
		      int grid_x_step,
		      int grid_y_step,
		      int grid_z_step,
		      int grid_ys_stride)
{
  // assume the number of samples is small and do this in a single block
  int threads_per_block = num_samples;
  int num_blocks = 1;

  gpu_cema_samples_kernel<<<num_blocks, threads_per_block>>>
    (first_sample, n_spec, seq_hi, seq_lo, dim_x, dim_y, dim_z,
     sample_x_ptr, sample_y_ptr, sample_z_ptr,
     sample_temp_ptr, sample_press_ptr, sample_ys_ptr,
     sample_ys_stride,
     grid_temp_ptr, grid_press_ptr, grid_ys_ptr,
     grid_x_step, grid_y_step, grid_z_step, grid_ys_stride);
}
