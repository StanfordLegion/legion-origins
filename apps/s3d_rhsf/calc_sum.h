
#ifndef _CALC_SUM_H_
#define _CALC_SUM_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"
#include "arrays.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Arrays;

class Sum3Task : public IndexLauncher {
public:
  Sum3Task(Domain domain,
           TaskArgument global_arg,
           ArgumentMap arg_map,
           Predicate pred = Predicate::TRUE_PRED,
           bool must = false,
           MapperID id = 0,
           MappingTagID tag = 0);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SUM_3_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_BALANCE;
protected:
  static bool dense_sum_3(const Rect<3> &my_subgrid_bounds, bool negate,
                          RegionAccessor<AccessorType::Generic,double> fa_out,
                          RegionAccessor<AccessorType::Generic,double> fa_1,
                          RegionAccessor<AccessorType::Generic,double> fa_2,
                          RegionAccessor<AccessorType::Generic,double> fa_3);
  static bool fast_sum_3(RegionAccessor<AccessorType::Generic,double> fa_out,
                         RegionAccessor<AccessorType::Generic,double> fa_1,
                         RegionAccessor<AccessorType::Generic,double> fa_2,
                         RegionAccessor<AccessorType::Generic,double> fa_3,
                         Rect<3> subgrid_bounds,
                         bool negate);
  static void kernel_sum_3_normal(size_t n_pts, double *out, double *in1, 
                                  double *in2, double *in3);
  static void kernel_sum_3_negate(size_t n_pts, double *out, double *in1, 
                                  double *in2, double *in3);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, 
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task,
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void sum_3_fields(HighLevelRuntime *runtime, Context ctx, 
                           const Rect<3>& proc_grid_bounds,
		           IndexSpace is_grid, IndexPartition ip_top, 
                           LogicalRegion lr_in, LogicalRegion lr_out,
		           FieldID fid_1, FieldID fid_2, FieldID fid_3, FieldID fid_out,
                           int stage, bool negate = false, int tag = 0);
  static void sum_3N_fields(HighLevelRuntime *runtime, Context ctx,
                            const Rect<3> &proc_grid_bounds,
                            IndexSpace is_grid, IndexPartition ip_top,
                            LogicalRegion lr_in, LogicalRegion lr_out,
                            const std::vector<FieldID> &in1_fields,
                            const std::vector<FieldID> &in2_fields,
                            const std::vector<FieldID> &in3_fields,
                            const std::vector<FieldID> &out_fields,
                            int stage, bool negate = false, int tag = 0);
};

class Sum4Task : public IndexLauncher {
public:
  Sum4Task(Domain domain,
           TaskArgument global_arg,
           ArgumentMap arg_map,
           Predicate pred = Predicate::TRUE_PRED,
           bool must = false,
           MapperID id = 0,
           MappingTagID tag = 0);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SUM_4_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_BALANCE;
protected:
  static bool dense_sum_4(const Rect<3> &subgrid_bounds, bool negate,
                          RegionAccessor<AccessorType::Generic,double> fa_out,
                          RegionAccessor<AccessorType::Generic,double> fa_1,
                          RegionAccessor<AccessorType::Generic,double> fa_2,
                          RegionAccessor<AccessorType::Generic,double> fa_3,
                          RegionAccessor<AccessorType::Generic,double> fa_4);
  static bool fast_sum_4(RegionAccessor<AccessorType::Generic,double> fa_out,
                         RegionAccessor<AccessorType::Generic,double> fa_1,
                         RegionAccessor<AccessorType::Generic,double> fa_2,
                         RegionAccessor<AccessorType::Generic,double> fa_3,
                         RegionAccessor<AccessorType::Generic,double> fa_4,
                         Rect<3> subgrid_bounds,
                         bool negate);
  static void kernel_sum_4_normal(size_t n_pts, double *out, double *in1, 
                                  double *in2, double *in3, double *in4);
  static void kernel_sum_4_negate(size_t n_pts, double *out, double *in1, 
                                  double *in2, double *in3, double *in4);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task,
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task,
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void sum_4_fields(HighLevelRuntime *runtime, Context ctx, 
                           const Rect<3>& proc_grid_bounds,
		           IndexSpace is_grid, IndexPartition ip_top, 
                           LogicalRegion lr_in, LogicalRegion lr_out,
		           FieldID fid_1, FieldID fid_2, FieldID fid_3, 
                           FieldID fid_4, FieldID fid_out,
                           int stage, bool negate = false, int tag = 0);
  static void sum_4N_fields(HighLevelRuntime *runtime, Context ctx,
                            const Rect<3> &proc_grid_bounds,
                            IndexSpace is_grid, IndexPartition ip_top,
                            LogicalRegion lr_in, LogicalRegion lr_out,
                            const std::vector<FieldID> &in1_fields,
                            const std::vector<FieldID> &in2_fields,
                            const std::vector<FieldID> &in3_fields,
                            const std::vector<FieldID> &in4_fields,
                            const std::vector<FieldID> &out_fields,
                            int stage, bool negate = false, int tag = 0);
};

class Sum3IntegrateTask : public IndexLauncher {
public:
  enum { FIRST_INTEGRATION, MIDDLE_INTEGRATION, LAST_INTEGRATION };

  Sum3IntegrateTask(Domain domain,
		    TaskArgument global_arg,
		    ArgumentMap arg_map,
		    Predicate pred = Predicate::TRUE_PRED,
		    bool must = false,
		    MapperID id = 0,
		    MappingTagID tag = 0);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SUM_3_INTEGRATE_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_BALANCE | RHSF_MAPPER_ALL_GPU;
protected:
  static bool dense_sum_3_integrate(const Rect<3> &my_subgrid_bounds,
				    RegionAccessor<AccessorType::Generic,double> fa_q,
				    RegionAccessor<AccessorType::Generic,double> fa_qerr,
				    RegionAccessor<AccessorType::Generic,double> fa_qtmp,
				    RegionAccessor<AccessorType::Generic,double> fa_1,
				    RegionAccessor<AccessorType::Generic,double> fa_2,
				    RegionAccessor<AccessorType::Generic,double> fa_3,
				    double alpha, double beta, double err, int which);
  static bool fast_sum_3_integrate(RegionAccessor<AccessorType::Generic,double> fa_q,
				   RegionAccessor<AccessorType::Generic,double> fa_qerr,
				   RegionAccessor<AccessorType::Generic,double> fa_qtmp,
				   RegionAccessor<AccessorType::Generic,double> fa_1,
				   RegionAccessor<AccessorType::Generic,double> fa_2,
				   RegionAccessor<AccessorType::Generic,double> fa_3,
				   Rect<3> subgrid_bounds,
				   double alpha, double beta, double err, int which);
  static void kernel_sum_3_integrate(size_t n_pts,
				     double *q, double *qerr, double *qtmp,
				     const double *in1, const double *in2, const double *in3,
				     double alpha, double beta, double err, int which);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, 
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task,
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void sum_3_integrate_fields(HighLevelRuntime *runtime, Context ctx, 
				     const Rect<3>& proc_grid_bounds,
				     IndexSpace is_grid, IndexPartition ip_top, 
				     LogicalRegion lr_in, 
				     LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
				     FieldID fid_1, FieldID fid_2, FieldID fid_3, FieldID fid_out,
				     double alpha, double beta, double err, Future f_timestep, 
                                     int which, int stage, int tag = 0);
  static void sum_3N_integrate_fields(HighLevelRuntime *runtime, Context ctx,
				      const Rect<3> &proc_grid_bounds,
				      IndexSpace is_grid, IndexPartition ip_top,
				      LogicalRegion lr_in,
				      LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
				      const std::vector<FieldID> &in1_fields,
				      const std::vector<FieldID> &in2_fields,
				      const std::vector<FieldID> &in3_fields,
				      const std::vector<FieldID> &out_fields,
				      double alpha, double beta, double err, Future f_timestep, 
                                      int which, int stage, int tag = 0);
};

class Sum4IntegrateTask : public IndexLauncher {
public:
  enum { FIRST_INTEGRATION, MIDDLE_INTEGRATION, LAST_INTEGRATION };

  Sum4IntegrateTask(Domain domain,
		    TaskArgument global_arg,
		    ArgumentMap arg_map,
		    Predicate pred = Predicate::TRUE_PRED,
		    bool must = false,
		    MapperID id = 0,
		    MappingTagID tag = 0);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SUM_4_INTEGRATE_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_BALANCE | RHSF_MAPPER_ALL_GPU;
protected:
  static bool dense_sum_4_integrate(const Rect<3> &subgrid_bounds,
				    RegionAccessor<AccessorType::Generic,double> fa_q,
				    RegionAccessor<AccessorType::Generic,double> fa_qerr,
				    RegionAccessor<AccessorType::Generic,double> fa_qtmp,
				    RegionAccessor<AccessorType::Generic,double> fa_1,
				    RegionAccessor<AccessorType::Generic,double> fa_2,
				    RegionAccessor<AccessorType::Generic,double> fa_3,
				    RegionAccessor<AccessorType::Generic,double> fa_4,
				    double alpha, double beta, double err, 
                                    double small, int which);
  static bool fast_sum_4_integrate(RegionAccessor<AccessorType::Generic,double> fa_q,
				   RegionAccessor<AccessorType::Generic,double> fa_qerr,
				   RegionAccessor<AccessorType::Generic,double> fa_qtmp,
				   RegionAccessor<AccessorType::Generic,double> fa_1,
				   RegionAccessor<AccessorType::Generic,double> fa_2,
				   RegionAccessor<AccessorType::Generic,double> fa_3,
				   RegionAccessor<AccessorType::Generic,double> fa_4,
				   Rect<3> subgrid_bounds,
				   double alpha, double beta, double err, 
                                   double small, int which);
  static void kernel_sum_4_integrate(size_t n_pts,
				     double *q, double *qerr, double *qtmp,
				     const double *in1, const double *in2,
				     const double *in3, const double *in4,
				     double alpha, double beta, double err, int which);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task,
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task,
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void sum_4_integrate_fields(HighLevelRuntime *runtime, Context ctx, 
				     const Rect<3>& proc_grid_bounds,
				     IndexSpace is_grid, IndexPartition ip_top, 
				     LogicalRegion lr_in,
				     LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
				     FieldID fid_1, FieldID fid_2, FieldID fid_3, 
				     FieldID fid_4, FieldID fid_out,
				     double alpha, double beta, double err, 
                                     double small, Future f_timestep, 
                                     int which, int stage, int tag = 0);
  static void sum_4N_integrate_fields(HighLevelRuntime *runtime, Context ctx,
				      const Rect<3> &proc_grid_bounds,
				      IndexSpace is_grid, IndexPartition ip_top,
				      LogicalRegion lr_in,
				      LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
				      const std::vector<FieldID> &in1_fields,
				      const std::vector<FieldID> &in2_fields,
				      const std::vector<FieldID> &in3_fields,
				      const std::vector<FieldID> &in4_fields,
				      const std::vector<FieldID> &out_fields,
				      double alpha, double beta, double err, 
                                      double small, Future f_timestep, 
                                      int which, int stage, int tag = 0);
};

class CalcRHSVelTask : public IndexLauncher {
public:
  CalcRHSVelTask(Domain domain,
                 TaskArgument global_arg,
                 ArgumentMap arg_map,
                 Predicate pred = Predicate::TRUE_PRED,
                 bool must = false,
                 MapperID id = 0,
                 MappingTagID tag = 0);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_RHS_VEL_IN_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_BALANCE;
protected:
  static void kernel_rhs_vel_in(size_t n_pts, double *out, double *tau, 
                                double *rho_vel, double *vel);
  static void kernel_rhs_vel_in_pressure(size_t n_pts, double *out, double *tau, 
                                         double *rho_vel, double *vel, double *pressure);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, 
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void calc_rhs_vel_deriv_in(HighLevelRuntime *runtime, Context ctx, 
                                    const Rect<3>& proc_grid_bounds,
                                    IndexSpace is_grid, IndexPartition ip_top, 
                                    LogicalRegion lr_q, LogicalRegion lr_int,
			            FieldID fid_rho_vel, FieldID fid_vel, FieldID fid_tau, 
                                    FieldID fid_pressure, FieldID fid_out, int stage);
};

#endif
