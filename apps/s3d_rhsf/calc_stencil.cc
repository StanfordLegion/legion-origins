
#include "rhsf.h"
#include "rhsf_init.h"
#include "rhsf_impl.h"
#include "rhsf_mapper.h"
#include "calc_stencil.h"

// Stencil kernels
#include "cpu_stencils.h"
#ifdef USE_GPU_KERNELS
#include "gpu_stencils.h"
#endif

// for fused stencils, use the first field's barrier for all fields
// (this greatly reduces the inter-node barrier traffic)
#define FUSED_STENCIL_SINGLE_BARRIER

#ifdef DEBUG_GHOST_CELLS
static void print_rect(const char *pfx, const Rect<3>&r)
{
  printf("%s: (%d,%d,%d) -> (%d,%d,%d)\n", pfx, r.lo[0], r.lo[1], r.lo[2], r.hi[
0], r.hi[1], r.hi[2]);
}
#endif

// Helper method
static int calc_ghost_region_index(S3DRank *rank, Point<3> p, Rect<3> r, int direction, bool mine, int side)
{
  int ofs = 0;

  // should never ask for a direction we're not stenciling in
  assert(rank->vary_in_dims[direction]);

  // if the proc grid size is only 1 in this dimension, it wraps around
  //  onto itself and there's only 2 unique ghost regions instead of 4
  bool x_wrap = rank->proc_grid_size[0] == 1;
  if(direction == 0) { 
    // x plane is Fortran ordered: [mine][side][y][z]
    ofs += (x_wrap ? 2 : 4) * (r.hi[1] - r.lo[1] + 1) * (p[2] - r.lo[2]);
    ofs += (x_wrap ? 2 : 4) * (p[1] - r.lo[1]);
    if(x_wrap) {
      // instead of 0 1 2 3, we have 0 1 1 0
      if((side == PCLR_XYZHI) ^ mine)
	ofs += 1;
    } else {
      if(side == PCLR_XYZHI)
	ofs += 2;
      if(mine)
	ofs += 1;
    }
#ifdef DEBUG_GHOST_CELLS
    printf("gri:x (%d,%d,%d) (%d,%d,%d)-(%d,%d,%d) %d %d = %d\n",
	   p[0], p[1], p[2], r.lo[0], r.lo[1], r.lo[2], r.hi[0], r.hi[1], r.hi[2],
	   (int)mine, side, ofs);
#endif
    return ofs;
  } else 
    if(rank->vary_in_dims[0])
      ofs += (x_wrap ? 2 : 4) * (r.hi[1] - r.lo[1] + 1) * (r.hi[2] - r.lo[2] + 1);

  // if the proc grid size is only 1 in this dimension, it wraps around
  //  onto itself and there's only 2 unique ghost regions instead of 4
  bool y_wrap = rank->proc_grid_size[1] == 1;
  if(direction == 1) { 
    // y plane is Fortran ordered: [mine][side][x][z]
    ofs += (y_wrap ? 2 : 4) * (r.hi[0] - r.lo[0] + 1) * (p[2] - r.lo[2]);
    ofs += (y_wrap ? 2 : 4) * (p[0] - r.lo[0]);
    if(y_wrap) {
      // instead of 0 1 2 3, we have 0 1 1 0
      if((side == PCLR_XYZHI) ^ mine)
	ofs += 1;
    } else {
      if(side == PCLR_XYZHI)
	ofs += 2;
      if(mine)
	ofs += 1;
    }
#ifdef DEBUG_GHOST_CELLS
    printf("gri:y (%d,%d,%d) (%d,%d,%d)-(%d,%d,%d) %d %d = %d\n",
	   p[0], p[1], p[2], r.lo[0], r.lo[1], r.lo[2], r.hi[0], r.hi[1], r.hi[2],
	   (int)mine, side, ofs);
#endif
    return ofs;
  } else 
    if(rank->vary_in_dims[1])
      ofs += (y_wrap ? 2 : 4) * (r.hi[0] - r.lo[0] + 1) * (r.hi[2] - r.lo[2] + 1);

  // if the proc grid size is only 1 in this dimension, it wraps around
  //  onto itself and there's only 2 unique ghost regions instead of 4
  bool z_wrap = rank->proc_grid_size[2] == 1;
  if(direction == 2) { 
    // z plane is Fortran ordered: [mine][side][x][y]
    ofs += (z_wrap ? 2 : 4) * (r.hi[0] - r.lo[0] + 1) * (p[1] - r.lo[1]);
    ofs += (z_wrap ? 2 : 4) * (p[0] - r.lo[0]);
    if(z_wrap) {
      // instead of 0 1 2 3, we have 0 1 1 0
      if((side == PCLR_XYZHI) ^ mine)
	ofs += 1;
    } else {
      if(side == PCLR_XYZHI)
	ofs += 2;
      if(mine)
	ofs += 1;
    }
#ifdef DEBUG_GHOST_CELLS
    printf("gri:z (%d,%d,%d) (%d,%d,%d)-(%d,%d,%d) %d %d = %d\n",
	   p[0], p[1], p[2], r.lo[0], r.lo[1], r.lo[2], r.hi[0], r.hi[1], r.hi[2],
	   (int)mine, side, ofs);
#endif
    return ofs;
  }

  assert(0);
  return -1;
}

#ifdef NEW_BARRIER_EXCHANGE
// not a task on its own - called by distribute_task
void do_barrier_exchange(const Task *task,
			 const std::vector<PhysicalRegion> &regions,
			 Context ctx, HighLevelRuntime *runtime,
			 const Rect<3>& proc_grid_bounds)
{
  const size_t DOUBLES_NEEDED = (sizeof(PhaseBarrier) + sizeof(double) - 1) / sizeof(double);
  union {
    //PhaseBarrier pb;  C++ won't let us do this because it has a constructor
    char as_bytes[sizeof(PhaseBarrier)];
    double as_dbl[DOUBLES_NEEDED];
  } u;

  Point<3> p = task->index_point.get_point<3>();

  S3DRank *rank = S3DRank::get_rank(p, false);

  PhaseBarrier barrier_exchange_barrier = ((RhsfDistributeArgs *)(task->args))->barrier_exchange_barrier;

  // printf("barrier exchange entry, pt = (%d, %d, %d), pb = " IDFMT "\n",
  // 	 p[0], p[1], p[2], barrier_exchange_barrier.get_barrier().id);

  GhostBarrierMap *gbm = new GhostBarrierMap;
  rank->barrier_data = gbm;

  // first, create barriers for each field in each of the ghost regions that
  //  are mapped here (i.e. the ones not in our own subgrid)
  for(int dir = 0; dir < 3; dir++) {
    if(!rank->vary_in_dims[dir]) continue;

    Rect<3> face(proc_grid_bounds);
    face.hi.x[dir] = face.lo[dir];

    for(GenericPointInRectIterator<3> pir(face); pir; pir++) {
      for(int side = PCLR_XYZLO; side <= PCLR_XYZHI; side++) {
        int lrg_index = calc_ghost_region_index(rank, pir.p, 
						proc_grid_bounds, 
						dir, true /*mine*/, side);

	const PhysicalRegion& pr = regions[lrg_index];
	LogicalRegion lr = pr.get_logical_region();
	Rect<3> lrd = runtime->get_index_space_domain(ctx, lr.get_index_space()).get_rect<3>();

	//printf("dir=%d, side=%d, lr=" IDFMT ", lo=(%d,%d,%d)\n",
	//      dir, side, lr.get_index_space().id, lrd.lo[0], lrd.lo[1], lrd.lo[2]);

	std::map<FieldID, PhaseBarrier>& bm(gbm->outside[lr]);

	for(std::vector<FieldID>::const_iterator it = task->regions[lrg_index].instance_fields.begin();
	    it != task->regions[lrg_index].instance_fields.end();
	    it++) {
	  RegionAccessor<AccessorType::Generic, double> fa = pr.get_field_accessor(*it).typeify<double>();

	  PhaseBarrier pb = runtime->create_phase_barrier(ctx, 1);
	  //printf("GE: (%d,%d,%d) lr=" IDFMT " write field %d: pb = " IDFMT "\n",
	  //       p[0], p[1], p[2], lr.get_index_space().id, *it, pb.get_barrier().id);
	  bm[*it] = pb;

	  *(PhaseBarrier *)u.as_bytes = pb;

	  Point<3> p2 = lrd.lo;

	  for(size_t k = 0; k < DOUBLES_NEEDED; k++) {
	    fa.write(DomainPoint::from_point<3>(p2), u.as_dbl[k]);
	    p2.x[0]++;
	  }
	}

        // we'll never access this region again from this task, so unmap
        runtime->unmap_region(ctx, pr);
      }
    }
  }

  // now we can use the barrier_exchange_barrier to make sure everybody has
  //  written their per-field barrier ids
  barrier_exchange_barrier.arrive();
  //barrier_exchange_barrier = runtime->advance_phase_barrier(ctx, barrier_exchange_barrier);
  barrier_exchange_barrier.wait();

  // try just doing remote reads for these...
  for(int dir = 0; dir < 3; dir++) {
    if(!rank->vary_in_dims[dir]) continue;

    Rect<3> face(proc_grid_bounds);
    face.hi.x[dir] = face.lo[dir];

    for(GenericPointInRectIterator<3> pir(face); pir; pir++) {
      for(int side = PCLR_XYZLO; side <= PCLR_XYZHI; side++) {
        int lrg_index = calc_ghost_region_index(rank, pir.p, 
						proc_grid_bounds, 
						dir, false /*not mine*/, side);

	const PhysicalRegion& pr = regions[lrg_index];
	LogicalRegion lr = pr.get_logical_region();
	Rect<3> lrd = runtime->get_index_space_domain(ctx, lr.get_index_space()).get_rect<3>();

	//printf("dir=%d, side=%d, lr=" IDFMT ", lo=(%d,%d,%d)\n",
	//       dir, side, lr.get_index_space().id, lrd.lo[0], lrd.lo[1], lrd.lo[2]);

	std::map<FieldID, PhaseBarrier>& bm(gbm->inside[lr]);

	for(std::vector<FieldID>::const_iterator it = task->regions[lrg_index].instance_fields.begin();
	    it != task->regions[lrg_index].instance_fields.end();
	    it++) {
	  RegionAccessor<AccessorType::Generic, double> fa = pr.get_field_accessor(*it).typeify<double>();

	  Point<3> p2 = lrd.lo;

	  for(size_t k = 0; k < DOUBLES_NEEDED; k++) {
	    u.as_dbl[k] = fa.read(DomainPoint::from_point<3>(p2));
	    p2.x[0]++;
	  }

	  PhaseBarrier pb = *(PhaseBarrier *)u.as_bytes;

	  //printf("GE: (%d,%d,%d) lr=" IDFMT " read field %d: pb = " IDFMT "\n",
	  //       p[0], p[1], p[2], lr.get_index_space().id, *it, pb.get_barrier().id);
	  bm[*it] = pb;
	}

        // we'll never access this region again from this task, so unmap
        runtime->unmap_region(ctx, pr);
      }
    }
  }
}
#endif

/*static*/
void CalcStencilTask::calc_stencil_1d(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
                                      const Rect<3>& proc_grid_bounds, const Rect<3>& full_proc_grid_bounds,
                                      IndexSpace is_grid, IndexPartition ip_top,
                                      FieldSpace fs_src, LogicalRegion lr_src, FieldID fid_src,
                                      FieldSpace fs_dst, LogicalRegion lr_dst, FieldID fid_dst,
                                      FieldSpace fs_ghost, const LogicalRegion *lr_ghost,
                                      const std::vector<PhysicalRegion> &regions, FieldID fid_ghost,
                                      int direction, bool periodic, int stage, int tag, bool enable_gpu)
{
  if(!rank->vary_in_dims[direction]) {
    //printf("vary_in_dims[%d] == %d: skipping stencil computation (field = %d)\n",
    //       direction, rank->vary_in_dims[direction], fid_dst);
    return;
  }

  tag |= (stage & RHSF_MAPPER_STAGE_MASK);
  if (S3DRank::get_explicit_ghost_cells())
  {
#ifdef DEBUG_GHOST_CELLS
    printf("STENCIL %d -> %d (%d)\n", fid_src, fid_dst, direction);
    for(size_t i = 0; i < regions.size(); i++) {
      printf("%zd: %x.%llx - ", i, regions[i].get_logical_region().get_tree_id(), regions[i].get_logical_region().get_index_space().id);
      Rect<3> r = runtime->get_index_space_domain(ctx, regions[i].get_logical_region().get_index_space()).get_rect<3>();
      print_rect("", r);
    }
#endif
    int dir = direction;
    //for(int dir = 0; dir < 3; dir++) {
    Rect<3> face(proc_grid_bounds);
    face.hi.x[dir] = face.lo[dir];

    for(GenericPointInRectIterator<3> pir(face); pir; pir++) {
      // minus side
      {
        Point<3> p_lo(pir.p);

        // get the right subregion of our own region
        IndexSpace is_me = runtime->get_index_subspace<3>(ctx, ip_top, p_lo);
        IndexPartition is_dir = runtime->get_index_partition(ctx, is_me, PID_X + dir);
        IndexSpace is_src = runtime->get_index_subspace(ctx, is_dir, PCLR_XYZLO);
        LogicalRegion lr_edge;
        // A little hack here to over approximate the source instance when we need to
        // copy up from the GPU first since our copies don't have scatters yet.
        if (S3DRank::get_all_gpu())
          lr_edge = runtime->get_logical_subregion_by_tree(ctx, is_me, fs_src, lr_src.get_tree_id());
        else
          lr_edge = runtime->get_logical_subregion_by_tree(ctx, is_src, fs_src, lr_src.get_tree_id());

        int lrg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, false, PCLR_XYZLO);
        LogicalRegion lr_minus = regions[lrg_index].get_logical_region();
        copy_region(runtime, ctx, rank, lr_edge, fid_src, lr_minus, fid_ghost, lr_src);
      }

      // plus side
      {
        Point<3> p_hi(pir.p);
        p_hi.x[dir] = proc_grid_bounds.hi[dir];

        // get the right subregion of our own region
        IndexSpace is_me = runtime->get_index_subspace<3>(ctx, ip_top, p_hi);
        IndexPartition is_dir = runtime->get_index_partition(ctx, is_me, PID_X + dir);
        IndexSpace is_src = runtime->get_index_subspace(ctx, is_dir, PCLR_XYZHI);
        LogicalRegion lr_edge;
        if (S3DRank::get_all_gpu())
          lr_edge = runtime->get_logical_subregion_by_tree(ctx, is_me, fs_src, lr_src.get_tree_id());
        else
          lr_edge = runtime->get_logical_subregion_by_tree(ctx, is_src, fs_src, lr_src.get_tree_id());

        int lrg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, false, PCLR_XYZHI);
        LogicalRegion lr_plus = lr_ghost[lrg_index];
        copy_region(runtime, ctx, rank, lr_edge, fid_src, lr_plus, fid_ghost, lr_src);
      }
    }
  }
  TaskLauncher stencil_launcher(CALC_STENCIL_1D_TASK_ID, TaskArgument(&direction, sizeof(direction)));
  stencil_launcher.tag = (RHSF_MAPPER_FORCE_RANK_MATCH | RHSF_MAPPER_BALANCE | tag);
  if (enable_gpu && (proc_grid_bounds.volume() > 1))
  {
    printf("NEED TO TEACH RHSF MAPPER ABOUT GPUs for other nodes!!!!\n");
    assert(false);
  }
  // can't currently use index space launches because of the way neighbor's regions are calculated
  for (GenericPointInRectIterator<3> pir(proc_grid_bounds); pir; pir++)
  {
    // Reset the launcher and assign our point
    stencil_launcher.region_requirements.clear();
    stencil_launcher.point = DomainPoint::from_point<3>(pir.p);

    // rr[0] is output - always present
    IndexSpace is_me = runtime->get_index_subspace<3>(ctx, ip_top, pir.p);
    LogicalRegion lr_out = runtime->get_logical_subregion_by_tree(ctx, is_me, fs_dst, lr_dst.get_tree_id());
    RegionRequirement rr_out(lr_out, WRITE_DISCARD, EXCLUSIVE, lr_dst);
    rr_out.add_field(fid_dst);
    stencil_launcher.add_region_requirement(rr_out);

    // rr[1] is local input - always present
    LogicalRegion lr_in = runtime->get_logical_subregion_by_tree(ctx, is_me, fs_src, lr_src.get_tree_id());
    RegionRequirement rr_in(lr_in, READ_ONLY, EXCLUSIVE, lr_src);
    rr_in.add_field(fid_src);
    stencil_launcher.add_region_requirement(rr_in);

    // special case: if we're the only processor along the stencil dimension, we can't ask for our
    //  input data a second (and third) time because it creates a nasty case for the runtime to handle,
    //  so don't, and fix it up in the task itself
    if((full_proc_grid_bounds.lo[direction] == full_proc_grid_bounds.hi[direction]) 
        && !S3DRank::get_explicit_ghost_cells()) {
      //printf("single processor in direction %d - not asking for hi and lo\n", direction);
    } else {
      // rr[2] is input from negative neighbor 
      // if we're the first in the dimension and not doing a periodic stencil, we request NO_ACCESS instead of READ_ONLY
      Point<3> p_minus(pir.p);
      PrivilegeMode priv_minus;
      if(pir.p[direction] > full_proc_grid_bounds.lo[direction]) {
	p_minus.x[direction]--;
	priv_minus = READ_ONLY;
      } else {
	p_minus.x[direction] = full_proc_grid_bounds.hi[direction];
	priv_minus = periodic ? READ_ONLY : NO_ACCESS;
      }

      // are we using explicit ghost cells and are on the low edge of our distribution box?
      if (S3DRank::get_explicit_ghost_cells() && (pir.p[direction] == proc_grid_bounds.lo[direction])) {
	int lrg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZLO);
	LogicalRegion lr_minus = lr_ghost[lrg_index];

#ifdef DEBUG_GHOST_CELLS
	printf("ghost cell needed (lo %d) -> %d -> %x.%llx\n", direction, lrg_index,
	       lr_minus.get_tree_id(), lr_minus.get_index_space().id);
#endif

	RegionRequirement rr_minus(lr_minus, priv_minus, EXCLUSIVE, lr_minus);
	rr_minus.add_field(fid_ghost);
        stencil_launcher.add_region_requirement(rr_minus);

        // If we're not going to do an acquire, have the stencil take a phase barrier wait
        if (!enable_gpu)
        {
          GhostBarrierMap *gbm = (GhostBarrierMap *)(rank->barrier_data);
          assert(gbm);
          assert(gbm->outside.find(lr_minus) != gbm->outside.end());
          assert(gbm->outside[lr_minus].find(fid_ghost) != gbm->outside[lr_minus].end());
          PhaseBarrier& pb = gbm->outside[lr_minus][fid_ghost];

          // step the barrier and then have this task wait on the next phase
          pb = runtime->advance_phase_barrier(ctx, pb);
          stencil_launcher.add_wait_barrier(pb);
#ifdef DEBUG_GHOST_CELLS
          printf("ghost cell wait (lo %d) -> %d -> %llx/%x\n", direction, lrg_index, pb.get_barrier().id, pb.get_barrier().gen);
#endif
        }
      } else {
	IndexSpace is_minus = runtime->get_index_subspace<3>(ctx, ip_top, p_minus);
	IndexPartition ip_minus_dir = runtime->get_index_partition(ctx, is_minus, PID_X+direction);
	IndexSpace is_xyzhi = runtime->get_index_subspace(ctx, ip_minus_dir, PCLR_XYZHI);
	LogicalRegion lr_minus = runtime->get_logical_subregion_by_tree(ctx, is_xyzhi, fs_src, lr_src.get_tree_id());
	RegionRequirement rr_minus(lr_minus, priv_minus, EXCLUSIVE, lr_src);
	rr_minus.add_field(fid_src);
        stencil_launcher.add_region_requirement(rr_minus);
      }

      // rr[3] is input from positive neighbor
      // if we're the last in the dimension and not doing a periodic stencil, we request NO_ACCESS instead of READ_ONLY
      Point<3> p_plus(pir.p);
      PrivilegeMode priv_plus;
      if(pir.p[direction] < full_proc_grid_bounds.hi[direction]) {
	p_plus.x[direction]++;
	priv_plus = READ_ONLY;
      } else {
	p_plus.x[direction] = full_proc_grid_bounds.lo[direction];
	priv_plus = periodic ? READ_ONLY : NO_ACCESS;
      }

      // are we using explicit ghost cells and are on the low edge of our distribution box?
      if (S3DRank::get_explicit_ghost_cells() && (pir.p[direction] == proc_grid_bounds.hi[direction])) {
	int lrg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZHI);
	LogicalRegion lr_plus = lr_ghost[lrg_index];

#ifdef DEBUG_GHOST_CELLS
	printf("ghost cell needed (hi %d) -> %d -> %x.%llx\n", direction, lrg_index,
	       lr_plus.get_tree_id(), lr_plus.get_index_space().id);
#endif

	RegionRequirement rr_plus(lr_plus, priv_plus, EXCLUSIVE, lr_plus);
	rr_plus.add_field(fid_ghost);
        stencil_launcher.add_region_requirement(rr_plus);

        // Only add the barrier here if we're not going to do it on the acquire
        if (!enable_gpu)
        {
          GhostBarrierMap *gbm = (GhostBarrierMap *)(rank->barrier_data);
          assert(gbm);
          assert(gbm->outside.find(lr_plus) != gbm->outside.end());
          assert(gbm->outside[lr_plus].find(fid_ghost) != gbm->outside[lr_plus].end());
          PhaseBarrier& pb = gbm->outside[lr_plus][fid_ghost];

          // step the barrier and then have this task wait on the next phase
          pb = runtime->advance_phase_barrier(ctx, pb);
          stencil_launcher.add_wait_barrier(pb);
#ifdef DEBUG_GHOST_CELLS
          printf("ghost cell wait (hi %d) -> %d -> %llx/%x\n", direction, lrg_index, pb.get_barrier().id, pb.get_barrier().gen);
#endif
        }
      } else {
	IndexSpace is_plus = runtime->get_index_subspace<3>(ctx, ip_top, p_plus);
	IndexPartition ip_plus_dir = runtime->get_index_partition(ctx, is_plus, PID_X+direction);
	IndexSpace is_xyzlo = runtime->get_index_subspace(ctx, ip_plus_dir, PCLR_XYZLO);
	LogicalRegion lr_plus = runtime->get_logical_subregion_by_tree(ctx, is_xyzlo, fs_src, lr_src.get_tree_id());
	RegionRequirement rr_plus(lr_plus, priv_plus, EXCLUSIVE, lr_src);
	rr_plus.add_field(fid_src);
        stencil_launcher.add_region_requirement(rr_plus);
      }
      
      // if we want to run this on the GPU we need to acquire coherence on our two
      // regions which we have in simultaneous mode
      if (enable_gpu && S3DRank::get_explicit_ghost_cells())
      {
        GhostBarrierMap *gbm = (GhostBarrierMap *)(rank->barrier_data);
        assert(gbm);
        // Do minus first
        if (pir.p[direction] == proc_grid_bounds.lo[direction])
        {
          const RegionRequirement &minus_req = stencil_launcher.region_requirements[2];
          int reg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZLO);
          AcquireLauncher minus_launcher(minus_req.region,
                                         minus_req.parent,
                                         regions[reg_index]);
          minus_launcher.fields = minus_req.privilege_fields;

          // Now get the barrier that we need to wait on
          LogicalRegion lr_minus = minus_req.region;
          assert(gbm->outside.find(lr_minus) != gbm->outside.end());
          assert(gbm->outside[lr_minus].find(fid_ghost) != gbm->outside[lr_minus].end());
          PhaseBarrier& pb = gbm->outside[lr_minus][fid_ghost];

          // step the barrier and then have this task wait on the next phase
          pb = runtime->advance_phase_barrier(ctx, pb);
          minus_launcher.add_wait_barrier(pb);         
#ifdef DEBUG_GHOST_CELLS
          printf("minus acquire wait -> %llx/%x\n", pb.get_barrier().id, pb.get_barrier().gen);
#endif

          runtime->issue_acquire(ctx, minus_launcher);
        }
        // Then do plus
        if (pir.p[direction] == proc_grid_bounds.hi[direction])
        {
          const RegionRequirement &plus_req = stencil_launcher.region_requirements[3];
          int reg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZHI);
          AcquireLauncher plus_launcher(plus_req.region,
                                        plus_req.parent,
                                        regions[reg_index]);
          plus_launcher.fields = plus_req.privilege_fields;

          // Now get the phase barrier that we need to issue
          LogicalRegion lr_plus = plus_req.region;
          assert(gbm->outside.find(lr_plus) != gbm->outside.end());
          assert(gbm->outside[lr_plus].find(fid_ghost) != gbm->outside[lr_plus].end());
          PhaseBarrier& pb = gbm->outside[lr_plus][fid_ghost];

          // step the barrier and then have this task wait on the next phase
          pb = runtime->advance_phase_barrier(ctx, pb);
          plus_launcher.add_wait_barrier(pb);
#ifdef DEBUG_GHOST_CELLS
          printf("plus acquire wait -> %llx/%x\n", pb.get_barrier().id, pb.get_barrier().gen);
#endif

          runtime->issue_acquire(ctx, plus_launcher);
        }
      }
    }
    // Now we can launch our task
    Future f = runtime->execute_task(ctx, stencil_launcher);
    if (S3DRank::get_perform_waits())
      f.get_void_result();

    // If we acquired coherence on our simultaneous regions now we need to release it
    if (enable_gpu && S3DRank::get_explicit_ghost_cells())
    {
      // Minus first
      if (pir.p[direction] == proc_grid_bounds.lo[direction])
      {
        const RegionRequirement &minus_req = stencil_launcher.region_requirements[2];
        int reg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZLO);
        ReleaseLauncher minus_launcher(minus_req.region,
                                       minus_req.parent,
                                       regions[reg_index]);
        minus_launcher.fields = minus_req.privilege_fields;
        runtime->issue_release(ctx, minus_launcher);
      }
      // Then plus
      if (pir.p[direction] == proc_grid_bounds.hi[direction])
      {
        const RegionRequirement &plus_req = stencil_launcher.region_requirements[3];
        int reg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZHI);
        ReleaseLauncher plus_launcher(plus_req.region,
                                      plus_req.parent,
                                      regions[reg_index]);
        plus_launcher.fields = plus_req.privilege_fields;
        runtime->issue_release(ctx, plus_launcher);
      }
    }
  }
}

/*static*/
void CalcStencilTask::calc_N_stencil_1d(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
                                        const Rect<3>& proc_grid_bounds, const Rect<3>& full_proc_grid_bounds,
                                        IndexSpace is_grid, IndexPartition ip_top,
                                        FieldSpace fs_src, LogicalRegion lr_src, const std::vector<FieldID> &src_fields,
                                        FieldSpace fs_dst, LogicalRegion lr_dst, const std::vector<FieldID> &dst_fields,
                                        FieldSpace fs_ghost, const LogicalRegion *lr_ghost,
                                        const std::vector<PhysicalRegion> &regions, const std::vector<FieldID> &ghost_fields,
                                        int direction, bool periodic, int stage, int tag, bool enable_gpu)
{
  if(!rank->vary_in_dims[direction]) {
    //printf("vary_in_dims[%d] == %d: skipping stencil computation (fields =",
    //       direction, rank->vary_in_dims[direction]);
    //for(size_t i = 0; i < dst_fields.size(); i++)
    //  printf("%s%d", (i ? "," : ""), dst_fields[i]);
    //printf(")\n");
    return;
  }

  tag |= (stage & RHSF_MAPPER_STAGE_MASK);
  assert(src_fields.size() == dst_fields.size());
  assert(src_fields.size() == ghost_fields.size());

  if (S3DRank::get_explicit_ghost_cells())
  {
#ifdef DEBUG_GHOST_CELLS
    for(size_t i = 0; i < regions.size(); i++) {
      printf("%zd: %x.%llx - ", i, regions[i].get_logical_region().get_tree_id(), regions[i].get_logical_region().get_index_space().id);
      Rect<3> r = runtime->get_index_space_domain(ctx, regions[i].get_logical_region().get_index_space()).get_rect<3>();
      print_rect("", r);
    }
#endif
    int dir = direction;
    //for(int dir = 0; dir < 3; dir++) {
    Rect<3> face(proc_grid_bounds);
    face.hi.x[dir] = face.lo[dir];

    for(GenericPointInRectIterator<3> pir(face); pir; pir++) {
      // minus side
      {
        Point<3> p_lo(pir.p);

        // get the right subregion of our own region
        IndexSpace is_me = runtime->get_index_subspace<3>(ctx, ip_top, p_lo);
        IndexPartition is_dir = runtime->get_index_partition(ctx, is_me, PID_X + dir);
        IndexSpace is_src = runtime->get_index_subspace(ctx, is_dir, PCLR_XYZLO);
        LogicalRegion lr_edge;
        // See the note in copy_region that explains this hack
        if (S3DRank::get_all_gpu())
          lr_edge = runtime->get_logical_subregion_by_tree(ctx, is_me, fs_src, lr_src.get_tree_id());
        else
          lr_edge = runtime->get_logical_subregion_by_tree(ctx, is_src, fs_src, lr_src.get_tree_id());

        int lrg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, false, PCLR_XYZLO);
        LogicalRegion lr_minus = regions[lrg_index].get_logical_region();
        copy_N_region(runtime, ctx, rank, lr_edge, src_fields, lr_minus, ghost_fields, lr_src);
      }

      // plus side
      {
        Point<3> p_hi(pir.p);
        p_hi.x[dir] = proc_grid_bounds.hi[dir];

        // get the right subregion of our own region
        IndexSpace is_me = runtime->get_index_subspace<3>(ctx, ip_top, p_hi);
        IndexPartition is_dir = runtime->get_index_partition(ctx, is_me, PID_X + dir);
        IndexSpace is_src = runtime->get_index_subspace(ctx, is_dir, PCLR_XYZHI);
        LogicalRegion lr_edge;
        if (S3DRank::get_all_gpu())
          lr_edge = runtime->get_logical_subregion_by_tree(ctx, is_me, fs_src, lr_src.get_tree_id());
        else
          lr_edge = runtime->get_logical_subregion_by_tree(ctx, is_src, fs_src, lr_src.get_tree_id());

        int lrg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, false, PCLR_XYZHI);
        LogicalRegion lr_plus = lr_ghost[lrg_index];
        copy_N_region(runtime, ctx, rank, lr_edge, src_fields, lr_plus, ghost_fields, lr_src);
      }
    }
  }
  TaskLauncher stencil_launcher(CALC_STENCIL_1D_TASK_ID, TaskArgument(&direction, sizeof(direction)));
  stencil_launcher.tag = (RHSF_MAPPER_FORCE_RANK_MATCH | RHSF_MAPPER_BALANCE | tag);
  if (enable_gpu && (proc_grid_bounds.volume() > 1))
  {
    printf("NEED TO TEACH RHSF MAPPER ABOUT GPUs for other nodes!!!!\n");
    assert(false);
  }
  // can't currently use index space launches because of the way neighbor's regions are calculated
  for (GenericPointInRectIterator<3> pir(proc_grid_bounds); pir; pir++)
  {
    // Reset the launcher and assign our point
    stencil_launcher.region_requirements.clear();
    stencil_launcher.point = DomainPoint::from_point<3>(pir.p);

    // rr[0] is output - always present
    IndexSpace is_me = runtime->get_index_subspace<3>(ctx, ip_top, pir.p);
    LogicalRegion lr_out = runtime->get_logical_subregion_by_tree(ctx, is_me, fs_dst, lr_dst.get_tree_id());
    RegionRequirement rr_out(lr_out, WRITE_DISCARD, EXCLUSIVE, lr_dst);
    for (std::vector<FieldID>::const_iterator it = dst_fields.begin();
          it != dst_fields.end(); it++)
      rr_out.add_field(*it);
    stencil_launcher.add_region_requirement(rr_out);

    // rr[1] is local input - always present
    LogicalRegion lr_in = runtime->get_logical_subregion_by_tree(ctx, is_me, fs_src, lr_src.get_tree_id());
    RegionRequirement rr_in(lr_in, READ_ONLY, EXCLUSIVE, lr_src);
    for (std::vector<FieldID>::const_iterator it = src_fields.begin();
          it != src_fields.end(); it++)
      rr_in.add_field(*it);
    stencil_launcher.add_region_requirement(rr_in);

    // special case: if we're the only processor along the stencil dimension, we can't ask for our
    //  input data a second (and third) time because it creates a nasty case for the runtime to handle,
    //  so don't, and fix it up in the task itself
    if((full_proc_grid_bounds.lo[direction] == full_proc_grid_bounds.hi[direction]) 
        && !S3DRank::get_explicit_ghost_cells()) {
      //printf("single processor in direction %d - not asking for hi and lo\n", direction);
    } else {
      // rr[2] is input from negative neighbor 
      // if we're the first in the dimension and not doing a periodic stencil, we request NO_ACCESS instead of READ_ONLY
      Point<3> p_minus(pir.p);
      PrivilegeMode priv_minus;
      if(pir.p[direction] > full_proc_grid_bounds.lo[direction]) {
	p_minus.x[direction]--;
	priv_minus = READ_ONLY;
      } else {
	p_minus.x[direction] = full_proc_grid_bounds.hi[direction];
	priv_minus = periodic ? READ_ONLY : NO_ACCESS;
      }

      // are we using explicit ghost cells and are on the low edge of our distribution box?
      if (S3DRank::get_explicit_ghost_cells() && (pir.p[direction] == proc_grid_bounds.lo[direction])) {
	int lrg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZLO);
	LogicalRegion lr_minus = lr_ghost[lrg_index];

#ifdef DEBUG_GHOST_CELLS
	printf("ghost cell needed (lo %d) -> %d -> %x.%llx\n", direction, lrg_index,
	       lr_minus.get_tree_id(), lr_minus.get_index_space().id);
#endif

	RegionRequirement rr_minus(lr_minus, priv_minus, EXCLUSIVE, lr_minus);
        for (std::vector<FieldID>::const_iterator it = ghost_fields.begin();
              it != ghost_fields.end(); it++)
          rr_minus.add_field(*it);
        stencil_launcher.add_region_requirement(rr_minus);

        // If we're not going to do an acquire later, then issue this here
        if (!enable_gpu)
        {
          GhostBarrierMap *gbm = (GhostBarrierMap *)(rank->barrier_data);
          assert(gbm);
          assert(gbm->outside.find(lr_minus) != gbm->outside.end());
          for (std::vector<FieldID>::const_iterator it = ghost_fields.begin();
                it != ghost_fields.end(); it++)
          {
            assert(gbm->outside[lr_minus].find(*it) != gbm->outside[lr_minus].end());
            PhaseBarrier& pb = gbm->outside[lr_minus][*it];

            // step the barrier and then have this task wait on the next phase
            pb = runtime->advance_phase_barrier(ctx, pb);
            stencil_launcher.add_wait_barrier(pb);
#ifdef FUSED_STENCIL_SINGLE_BARRIER
	    break; // the first barrier speaks for all fields
#endif
          }
        }
      } else {
	IndexSpace is_minus = runtime->get_index_subspace<3>(ctx, ip_top, p_minus);
	IndexPartition ip_minus_dir = runtime->get_index_partition(ctx, is_minus, PID_X+direction);
	IndexSpace is_xyzhi = runtime->get_index_subspace(ctx, ip_minus_dir, PCLR_XYZHI);
	LogicalRegion lr_minus = runtime->get_logical_subregion_by_tree(ctx, is_xyzhi, fs_src, lr_src.get_tree_id());
	RegionRequirement rr_minus(lr_minus, priv_minus, EXCLUSIVE, lr_src);
        for (std::vector<FieldID>::const_iterator it = src_fields.begin();
              it != src_fields.end(); it++)
          rr_minus.add_field(*it);
        stencil_launcher.add_region_requirement(rr_minus);
      }

      // rr[3] is input from positive neighbor
      // if we're the last in the dimension and not doing a periodic stencil, we request NO_ACCESS instead of READ_ONLY
      Point<3> p_plus(pir.p);
      PrivilegeMode priv_plus;
      if(pir.p[direction] < full_proc_grid_bounds.hi[direction]) {
	p_plus.x[direction]++;
	priv_plus = READ_ONLY;
      } else {
	p_plus.x[direction] = full_proc_grid_bounds.lo[direction];
	priv_plus = periodic ? READ_ONLY : NO_ACCESS;
      }

      // are we using explicit ghost cells and are on the low edge of our distribution box?
      if (S3DRank::get_explicit_ghost_cells() && (pir.p[direction] == proc_grid_bounds.hi[direction])) {
	int lrg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZHI);
	LogicalRegion lr_plus = lr_ghost[lrg_index];

#ifdef DEBUG_GHOST_CELLS
	printf("ghost cell needed (hi %d) -> %d -> %x.%llx\n", direction, lrg_index,
	       lr_plus.get_tree_id(), lr_plus.get_index_space().id);
#endif

	RegionRequirement rr_plus(lr_plus, priv_plus, EXCLUSIVE, lr_plus);
        for (std::vector<FieldID>::const_iterator it = ghost_fields.begin();
              it != ghost_fields.end(); it++)
          rr_plus.add_field(*it);
        stencil_launcher.add_region_requirement(rr_plus);

        // If we're not going to do an acquire later, get the barrier to wait on
        if (!enable_gpu)
        {
          GhostBarrierMap *gbm = (GhostBarrierMap *)(rank->barrier_data);
          assert(gbm);
          assert(gbm->outside.find(lr_plus) != gbm->outside.end());
          for (std::vector<FieldID>::const_iterator it = ghost_fields.begin();
                it != ghost_fields.end(); it++)
          {
            assert(gbm->outside[lr_plus].find(*it) != gbm->outside[lr_plus].end());
            PhaseBarrier& pb = gbm->outside[lr_plus][*it];

            // step the barrier and then have this task wait on the next phase
            pb = runtime->advance_phase_barrier(ctx, pb);
            stencil_launcher.add_wait_barrier(pb);
#ifdef FUSED_STENCIL_SINGLE_BARRIER
	    break; // the first barrier speaks for all fields
#endif
          }
        }
      } else {
	IndexSpace is_plus = runtime->get_index_subspace<3>(ctx, ip_top, p_plus);
	IndexPartition ip_plus_dir = runtime->get_index_partition(ctx, is_plus, PID_X+direction);
	IndexSpace is_xyzlo = runtime->get_index_subspace(ctx, ip_plus_dir, PCLR_XYZLO);
	LogicalRegion lr_plus = runtime->get_logical_subregion_by_tree(ctx, is_xyzlo, fs_src, lr_src.get_tree_id());
	RegionRequirement rr_plus(lr_plus, priv_plus, EXCLUSIVE, lr_src);
        for (std::vector<FieldID>::const_iterator it = src_fields.begin();
              it != src_fields.end(); it++)
          rr_plus.add_field(*it);
        stencil_launcher.add_region_requirement(rr_plus);
      }
      
      // if we want to run this on the GPU we need to acquire coherence on our two
      // regions which we have in simultaneous mode
      if (enable_gpu && S3DRank::get_explicit_ghost_cells())
      {
        GhostBarrierMap *gbm = (GhostBarrierMap *)(rank->barrier_data);
        assert(gbm);
        // Do minus first
        if (pir.p[direction] == proc_grid_bounds.lo[direction])
        {
          const RegionRequirement &minus_req = stencil_launcher.region_requirements[2];
          int reg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZLO);
          AcquireLauncher minus_launcher(minus_req.region,
                                         minus_req.parent,
                                         regions[reg_index]);
          minus_launcher.fields = minus_req.privilege_fields;

          // Get any barriers that we need to wait on
          LogicalRegion lr_minus = minus_req.region;
          assert(gbm->outside.find(lr_minus) != gbm->outside.end());
          for (std::vector<FieldID>::const_iterator it = ghost_fields.begin();
                it != ghost_fields.end(); it++)
          {
            assert(gbm->outside[lr_minus].find(*it) != gbm->outside[lr_minus].end());
            PhaseBarrier& pb = gbm->outside[lr_minus][*it];

            // step the barrier and then have this task wait on the next phase
            pb = runtime->advance_phase_barrier(ctx, pb);
            minus_launcher.add_wait_barrier(pb);
#ifdef FUSED_STENCIL_SINGLE_BARRIER
	    break; // the first barrier speaks for all fields
#endif
          }

          runtime->issue_acquire(ctx, minus_launcher);
        }
        // Then do plus
        if (pir.p[direction] == proc_grid_bounds.hi[direction])
        {
          const RegionRequirement &plus_req = stencil_launcher.region_requirements[3];
          int reg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZHI);
          AcquireLauncher plus_launcher(plus_req.region,
                                        plus_req.parent,
                                        regions[reg_index]);
          plus_launcher.fields = plus_req.privilege_fields;

          // Get any barriers that we need to wait on
          LogicalRegion lr_plus = plus_req.region;
          assert(gbm->outside.find(lr_plus) != gbm->outside.end());
          for (std::vector<FieldID>::const_iterator it = ghost_fields.begin();
                it != ghost_fields.end(); it++)
          {
            assert(gbm->outside[lr_plus].find(*it) != gbm->outside[lr_plus].end());
            PhaseBarrier& pb = gbm->outside[lr_plus][*it];

            // step the barrier and then have this task wait on the next phase
            pb = runtime->advance_phase_barrier(ctx, pb);
            plus_launcher.add_wait_barrier(pb);
#ifdef FUSED_STENCIL_SINGLE_BARRIER
	    break; // the first barrier speaks for all fields
#endif
          }
          runtime->issue_acquire(ctx, plus_launcher);
        }
      }
    }
    // Now we can launch our task
    Future f = runtime->execute_task(ctx, stencil_launcher);
    if (S3DRank::get_perform_waits())
      f.get_void_result();

    // If we acquired coherence on our simultaneous regions now we need to release it
    if (enable_gpu && S3DRank::get_explicit_ghost_cells())
    {
      // Minus first
      if (pir.p[direction] == proc_grid_bounds.lo[direction])
      {
        const RegionRequirement &minus_req = stencil_launcher.region_requirements[2];
        int reg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZLO);
        ReleaseLauncher minus_launcher(minus_req.region,
                                       minus_req.parent,
                                       regions[reg_index]);
        minus_launcher.fields = minus_req.privilege_fields;
        runtime->issue_release(ctx, minus_launcher);
      }
      // Then plus
      if (pir.p[direction] == proc_grid_bounds.hi[direction])
      {
        const RegionRequirement &plus_req = stencil_launcher.region_requirements[3];
        int reg_index = calc_ghost_region_index(rank, pir.p, proc_grid_bounds, direction, true, PCLR_XYZHI);
        ReleaseLauncher plus_launcher(plus_req.region,
                                      plus_req.parent,
                                      regions[reg_index]);
        plus_launcher.fields = plus_req.privilege_fields;
        runtime->issue_release(ctx, plus_launcher);
      }
    }
  }
}

/*static*/
void CalcStencilTask::copy_region(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
                                  LogicalRegion lr_src, FieldID fid_src,
                                  LogicalRegion lr_dst, FieldID fid_dst, LogicalRegion lr_src_parent)
{
#ifdef DEBUG_GHOST_CELLS
  Rect<3> src_rect = runtime->get_index_space_domain(ctx, lr_src.get_index_space()).get_rect<3>();
  Rect<3> dst_rect = runtime->get_index_space_domain(ctx, lr_dst.get_index_space()).get_rect<3>();

  printf("copy %x.%llx.%d -> %x.%llx.%d\n",
	 lr_src.get_tree_id(), lr_src.get_index_space().id, fid_src,
	 lr_dst.get_tree_id(), lr_dst.get_index_space().id, fid_dst);
  print_rect("src_rect", src_rect);
  print_rect("dst_rect", dst_rect);
  fflush(stdout);
#endif

  RegionRequirement rr_src(lr_src, READ_ONLY, EXCLUSIVE, lr_src_parent);
  rr_src.add_field(fid_src);

  RegionRequirement rr_dst(lr_dst, READ_WRITE, EXCLUSIVE, lr_dst);
  rr_dst.add_field(fid_dst);

  CopyLauncher clr;
  clr.add_copy_requirements(rr_src, rr_dst);

  // get the barrier for the target region and make the copy trigger it
  GhostBarrierMap *gbm = (GhostBarrierMap *)(rank->barrier_data);
  assert(gbm);
#ifdef DEBUG_GHOST_CELLS
  printf("rank = %p\n", rank);
  printf("INSIDE:"); for(std::map<LogicalRegion, std::map<FieldID, PhaseBarrier> >::iterator it = gbm->inside.begin(); it != gbm->inside.end(); it++) printf(" %x.%llx", it->first.get_tree_id(), it->first.get_index_space().id); printf("\n");
  printf("OUTSIDE:"); for(std::map<LogicalRegion, std::map<FieldID, PhaseBarrier> >::iterator it = gbm->outside.begin(); it != gbm->outside.end(); it++) printf(" %x.%llx", it->first.get_tree_id(), it->first.get_index_space().id); printf("\n");
#endif
  assert(gbm->inside.find(lr_dst) != gbm->inside.end());
  assert(gbm->inside[lr_dst].find(fid_dst) != gbm->inside[lr_dst].end());
  PhaseBarrier& pb = gbm->inside[lr_dst][fid_dst];

  clr.add_arrival_barrier(pb);
#ifdef DEBUG_GHOST_CELLS
  printf("copy %x.%llx.%d -> %x.%llx.%d arrival = %llx/%d\n",
         lr_src.get_tree_id(), lr_src.get_index_space().id, fid_src,
         lr_dst.get_tree_id(), lr_dst.get_index_space().id, fid_dst,
         pb.get_barrier().id, pb.get_barrier().gen);
#endif


  runtime->issue_copy_operation(ctx, clr);

  // now step the barrier
  pb = runtime->advance_phase_barrier(ctx, pb);
}

/*static*/
void CalcStencilTask::copy_N_region(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
                                    LogicalRegion lr_src, const std::vector<FieldID> &src_fields,
                                    LogicalRegion lr_dst, const std::vector<FieldID> &dst_fields,
                                    LogicalRegion lr_src_parent)
{
  //Rect<3> src_rect = runtime->get_index_space_domain(ctx, lr_src.get_index_space()).get_rect<3>();
  //Rect<3> dst_rect = runtime->get_index_space_domain(ctx, lr_dst.get_index_space()).get_rect<3>();

  RegionRequirement rr_src(lr_src, READ_ONLY, EXCLUSIVE, lr_src_parent);
  for (std::vector<FieldID>::const_iterator it = src_fields.begin();
        it != src_fields.end(); it++)
    rr_src.add_field(*it);

  RegionRequirement rr_dst(lr_dst, READ_WRITE, EXCLUSIVE, lr_dst);
  for (std::vector<FieldID>::const_iterator it = dst_fields.begin();
        it != dst_fields.end(); it++)
    rr_dst.add_field(*it);

  CopyLauncher clr;
  clr.add_copy_requirements(rr_src, rr_dst);

  // get the barrier for the target region and make the copy trigger it
  GhostBarrierMap *gbm = (GhostBarrierMap *)(rank->barrier_data);
  assert(gbm);
  assert(gbm->inside.find(lr_dst) != gbm->inside.end());
  for (std::vector<FieldID>::const_iterator it = dst_fields.begin();
        it != dst_fields.end(); it++)
  {
    assert(gbm->inside[lr_dst].find(*it) != gbm->inside[lr_dst].end());
    PhaseBarrier &pb = gbm->inside[lr_dst][*it];

    clr.add_arrival_barrier(pb);
#ifdef FUSED_STENCIL_SINGLE_BARRIER
    break; // the first barrier speaks for all fields
#endif
  }

  runtime->issue_copy_operation(ctx, clr);

  // now step the barriers
  for (std::vector<FieldID>::const_iterator it = dst_fields.begin();
        it != dst_fields.end(); it++)
  {
    PhaseBarrier &pb = gbm->inside[lr_dst][*it];
    pb = runtime->advance_phase_barrier(ctx, pb);
#ifdef FUSED_STENCIL_SINGLE_BARRIER
    break; // the first barrier speaks for all fields
#endif
  }
}

/*static*/
void CalcStencilTask::register_cpu_variants(void)
{
  HighLevelRuntime::register_legion_task<calc_stencil_1d_cpu_task>(CALC_STENCIL_1D_TASK_ID, Processor::LOC_PROC,
                                                                   true/*single*/, true/*index*/,
                                                                   RHSF_CPU_LEAF_VARIANT, TaskConfigOptions(true/*leaf*/),
                                                                   "calc_stencil_1d");
}

/*static*/
void CalcStencilTask::register_hybrid_variants(void)
{
  HighLevelRuntime::register_legion_task<calc_stencil_1d_cpu_task>(CALC_STENCIL_1D_TASK_ID, Processor::LOC_PROC,
                                                                   true/*single*/, true/*index*/,
                                                                   RHSF_CPU_LEAF_VARIANT, TaskConfigOptions(true/*leaf*/),
                                                                   "calc_stencil_1d");
#ifdef USE_GPU_KERNELS
  HighLevelRuntime::register_legion_task<calc_stencil_1d_gpu_task>(CALC_STENCIL_1D_TASK_ID, Processor::TOC_PROC,
                                                                   true/*single*/, true/*index*/,
                                                                   RHSF_GPU_LEAF_VARIANT, TaskConfigOptions(true/*leaf*/),
                                                                   "calc_stencil_1d");
#endif
}

/*static*/
void CalcStencilTask::calc_stencil_1d_cpu_task(const Task *task,
                                               const std::vector<PhysicalRegion> &regions,
                                               Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  assert(task->arglen == sizeof(int));
  int direction = *(const int*)(task->args);
  
  rhsf_stencil_1d_cpu_task(task->index_point.get_point<3>(), direction,
                           task->regions, regions, ctx, runtime);
#endif
}

/*static*/
void CalcStencilTask::calc_stencil_1d_gpu_task(const Task *task,
                              const std::vector<PhysicalRegion> &regions,
                              Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  assert(task->arglen == sizeof(int));
  int direction = *(const int*)(task->args);

  rhsf_stencil_1d_gpu_task(task->index_point.get_point<3>(), direction,
                           task->regions, regions, ctx, runtime);
#endif
#endif
}

/*static*/
void CalcStencilTask::rhsf_stencil_1d_cpu_task(Point<3> point, int direction,
                                               const std::vector<RegionRequirement> &reqs,
                                               const std::vector<PhysicalRegion> &regions,
                                               Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  if (S3DRank::get_show_progress())
    printf("%s task, pt = (%d, %d, %d), fid = %d\n", "stencil 1d cpu", point[0], point[1], point[2], reqs[0].instance_fields[0]);

  //define DEBUG_STENCIL_REGIONS
#ifdef DEBUG_STENCIL_REGIONS
  print_region(runtime, ctx, "stencil out: ", regions[0].get_logical_region());
  print_region(runtime, ctx, "stencil in: ", regions[1].get_logical_region());
  if(regions.size() > 2)
    print_region(runtime, ctx, "stencil minus: ", regions[2].get_logical_region());
  if(regions.size() > 3)
    print_region(runtime, ctx, "stencil plus: ", regions[3].get_logical_region());
#endif

  S3DRank *rank = S3DRank::get_rank(point, true);

  Blockify<3> grid2proc_map(rank->local_grid_size);
  Rect<3> my_subgrid_bounds = grid2proc_map.preimage(point);

  // Iterate over all the fields we need to compute stencils for
  for (unsigned idx = 0; idx < reqs[0].instance_fields.size(); idx++)
  {
    // get whatever accessors you need here
    RegionAccessor<AccessorType::Generic,double> fa_out = 
      regions[0].get_field_accessor(reqs[0].instance_fields[idx]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_in = 
      regions[1].get_field_accessor(reqs[1].instance_fields[idx]).typeify<double>();

    // fixup for singleton processor case
    int minus_region_index, plus_region_index;
    if(rank->proc_grid_size[direction] > 1) {
      minus_region_index = 2;
      plus_region_index = 3;
    } else {
      minus_region_index = 1;
      plus_region_index = 1;
    }

    RegionAccessor<AccessorType::Generic,double> fa_minus;
    bool have_minus_data = reqs[minus_region_index].privilege & READ_ONLY;
    if(have_minus_data)
      fa_minus = regions[minus_region_index].get_field_accessor(reqs[minus_region_index].instance_fields[idx]).typeify<double>();

    RegionAccessor<AccessorType::Generic,double> fa_plus;
    bool have_plus_data = reqs[plus_region_index].privilege & READ_ONLY;
    if(have_plus_data)
      fa_plus = regions[plus_region_index].get_field_accessor(reqs[plus_region_index].instance_fields[idx]).typeify<double>();

    Rect<3> sim_grid_bounds(Point<3>::ZEROES(), rank->global_grid_size - Point<3>::ONES());

    // physical scale factor comes from Fortran side - multiply by number of grid
    //  points in corresponding dimension
    double ds = (rank->global_grid_size[direction] - 1) * rank->scale_dims[direction];

    if(fast_stencil_1d(fa_out, fa_in, fa_minus, fa_plus, 
                       my_subgrid_bounds, sim_grid_bounds,
                       direction, ds, have_minus_data, have_plus_data)) {
      //printf("did fast stencil!\n");
      continue;
    }

    double ae = 4.0 / 5.0 * ds;
    double be = -1.0 / 5.0 * ds;
    double ce = 4.0 / 105.0 * ds;
    double de = -1.0 / 280.0 * ds;

    // we do the stencil by walking "pencils" in the stencil direction, so the outer loop
    // iterates over the face of our subgrid bounds where the pencils "start"
    Rect<3> pencil_face(my_subgrid_bounds);
    pencil_face.hi.x[direction] = pencil_face.lo.x[direction];
    for(GenericPointInRectIterator<3> pir(pencil_face); pir; pir++) {
      // order-8 stencil hardcoded for now - keeps a sliding window of 9 values to stencil from
      double window[9];
      Point<3> p_read(pir.p);
      Point<3> p_write(pir.p);

      if(have_minus_data) {
        // first 4 values come from a neighbor (if we have a neighbor)
        Point<3> pm(pir.p);
        if(point[direction] > 0)
          pm.x[direction] -= 4;
        else
          pm.x[direction] = rank->global_grid_size[direction] - 4;
        for(int i = 0; i < 4; i++) {
          window[i] = fa_minus.read(DomainPoint::from_point<3>(pm));
          pm.x[direction]++;
        }

        // now prime the window by reading the first 4 values from our pencil
        for(int i = 0; i < 4; i++) {
          window[i + 4] = fa_in.read(DomainPoint::from_point<3>(p_read));
          p_read.x[direction]++;
        }

        // and do the first 4 stencil steps
        for(int i = 0; i < 4; i++) {
          window[8] = fa_in.read(DomainPoint::from_point<3>(p_read));
          p_read.x[direction]++;
          // try to do this exactly the same as fortran's math order
          // double deriv = ds * ((4.0 / 5.0 * (window[5] - window[3])) -
          // 		     (1.0 / 5.0 * (window[6] - window[2])) +
          // 		     (4.0 / 105.0 * (window[7] - window[1])) -
          // 		     (1.0 / 280.0 * (window[8] - window[0])));
          double deriv = (((ae * (window[5] - window[3])
                            + be * (window[6] - window[2]))
                           + ce * (window[7] - window[1]))
                          + de * (window[8] - window[0]));

          // if (pir.p == pencil_face.lo)
          //   printf("(%d,%d,%d) %.20g / %.20g / %.20g / %.20g / - / %.20g / %.20g / %.20g / %.20g -> %g\n",
          // 	 p_write[0], p_write[1], p_write[2], 
          // 	 window[0], window[1], window[2], window[3],
          // 	 window[5], window[6], window[7], window[8], deriv);

          fa_out.write(DomainPoint::from_point<3>(p_write), deriv);
          p_write.x[direction]++;

          for(int j = 0; j < 8; j++)
            window[j] = window[j + 1];
        }
      } else {
        assert(0); // at least 2 other boundary modes to worry about here
      }

      // the interior region is nice and simple - fetch from the read point, stencil, write to the write point
      for(int i = 0; i < (my_subgrid_bounds.hi[direction] - my_subgrid_bounds.lo[direction] - 7); i++) {
        window[8] = fa_in.read(DomainPoint::from_point<3>(p_read));
        p_read.x[direction]++;
        // try to do this exactly the same as fortran's math order
        // double deriv = ds * ((4.0 / 5.0 * (window[5] - window[3])) -
        // 		     (1.0 / 5.0 * (window[6] - window[2])) +
        // 		     (4.0 / 105.0 * (window[7] - window[1])) -
        // 		     (1.0 / 280.0 * (window[8] - window[0])));
        double deriv = (((ae * (window[5] - window[3])
                          + be * (window[6] - window[2]))
                         + ce * (window[7] - window[1]))
                        + de * (window[8] - window[0]));
        // printf("(%d,%d,%d) %g / %g / %g / %g / - / %g / %g / %g / %g -> %g\n",
        // 	      pir.p[0], pir.p[1], pir.p[2], 
        // 	      window[0], window[1], window[2], window[3],
        // 	      window[5], window[6], window[7], window[8], deriv);

        fa_out.write(DomainPoint::from_point<3>(p_write), deriv);
        p_write.x[direction]++;

        for(int j = 0; j < 8; j++)
          window[j] = window[j + 1];
      }

      // high boundary
      if(have_plus_data) {
        // last 4 values come from a neighbor (if we have a neighbor)
        Point<3> pp(p_read); // this is already one off the edge - right where we want to be
        if(point[direction] == (rank->proc_grid_size[direction] - 1))
          pp.x[direction] = 0;

        for(int i = 0; i < 4; i++) {
          window[8] = fa_plus.read(DomainPoint::from_point<3>(pp));
          pp.x[direction]++;
          // try to do this exactly the same as fortran's math order
          // double deriv = ds * ((4.0 / 5.0 * (window[5] - window[3])) -
          // 		     (1.0 / 5.0 * (window[6] - window[2])) +
          // 		     (4.0 / 105.0 * (window[7] - window[1])) -
          // 		     (1.0 / 280.0 * (window[8] - window[0])));
          double deriv = (((ae * (window[5] - window[3])
                            + be * (window[6] - window[2]))
                           + ce * (window[7] - window[1]))
                          + de * (window[8] - window[0]));

          fa_out.write(DomainPoint::from_point<3>(p_write), deriv);
          p_write.x[direction]++;

          for(int j = 0; j < 8; j++)
            window[j] = window[j + 1];
        }
      } else {
        assert(0); // at least 2 other boundary modes to worry about here
      }

      // printf("(%d,%d,%d) %g / %g / %g / %g / - / %g / %g / %g / %g -> %g\n",
      // 	   pir.p[0], pir.p[1], pir.p[2], x_minus[3], x_minus[2], x_minus[1], x_minus[0],
      // 	   x_plus[0], x_plus[1], x_plus[2], x_plus[3], deriv);
      // break;
    }
  }
#endif
}

/*static*/
void CalcStencilTask::rhsf_stencil_1d_gpu_task(Point<3> point, int direction,
                                               const std::vector<RegionRequirement> &reqs,
                                               const std::vector<PhysicalRegion> &regions,
                                               Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  if (S3DRank::get_show_progress())
    printf("%s task, pt = (%d, %d, %d), fid = %d\n", "stencil 1d gpu", point[0], point[1], point[2], reqs[0].instance_fields[0]);

  //define DEBUG_STENCIL_REGIONS
#ifdef DEBUG_STENCIL_REGIONS
  print_region(runtime, ctx, "stencil out: ", regions[0].get_logical_region());
  print_region(runtime, ctx, "stencil in: ", regions[1].get_logical_region());
  if(regions.size() > 2)
    print_region(runtime, ctx, "stencil minus: ", regions[2].get_logical_region());
  if(regions.size() > 3)
    print_region(runtime, ctx, "stencil plus: ", regions[3].get_logical_region());
#endif

  S3DRank *rank = S3DRank::get_rank(point, true);

  Blockify<3> grid2proc_map(rank->local_grid_size);
  Rect<3> my_subgrid_bounds = grid2proc_map.preimage(point);

  // Iterate over all the fields for our stencil
  for (unsigned idx = 0; idx < reqs[0].instance_fields.size(); idx++)
  {
    // get whatever accessors you need here
    RegionAccessor<AccessorType::Generic,double> fa_out = 
      regions[0].get_field_accessor(reqs[0].instance_fields[idx]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_in = 
      regions[1].get_field_accessor(reqs[1].instance_fields[idx]).typeify<double>();

    // fixup for singleton processor case
    int minus_region_index, plus_region_index;
    if(rank->proc_grid_size[direction] > 1) {
      minus_region_index = 2;
      plus_region_index = 3;
    } else {
      minus_region_index = 1;
      plus_region_index = 1;
    }

    RegionAccessor<AccessorType::Generic,double> fa_minus;
    bool have_minus_data = reqs[minus_region_index].privilege & READ_ONLY;
    if(have_minus_data)
      fa_minus = regions[minus_region_index].get_field_accessor(reqs[minus_region_index].instance_fields[idx]).typeify<double>();

    RegionAccessor<AccessorType::Generic,double> fa_plus;
    bool have_plus_data = reqs[plus_region_index].privilege & READ_ONLY;
    if(have_plus_data)
      fa_plus = regions[plus_region_index].get_field_accessor(reqs[plus_region_index].instance_fields[idx]).typeify<double>();

    Rect<3> sim_grid_bounds(Point<3>::ZEROES(), rank->global_grid_size - Point<3>::ONES());

    static const int ORDER = 4;

    Rect<3> subrect;
    ByteOffset out_offsets[3];
    double *out_ptr = fa_out.raw_rect_ptr<3>(my_subgrid_bounds, subrect, out_offsets);
    if(!out_ptr || (subrect != my_subgrid_bounds)) assert(0);

    ByteOffset in_offsets[3];
    const double *in_ptr = fa_in.raw_rect_ptr<3>(my_subgrid_bounds, subrect, in_offsets);
    if(!in_ptr || (subrect != my_subgrid_bounds)) assert(0);

    ByteOffset minus_offsets[3];
    const double *minus_ptr = 0;
    if(have_minus_data) {
      Rect<3> minus_rect(my_subgrid_bounds);
      minus_rect.hi.x[direction] = ((my_subgrid_bounds.lo[direction] > sim_grid_bounds.lo[direction]) ?
                                      my_subgrid_bounds.lo[direction] - 1 :
                                      sim_grid_bounds.hi[direction]);
      minus_rect.lo.x[direction] = minus_rect.hi[direction] - (ORDER - 1);

      minus_ptr = fa_minus.raw_rect_ptr<3>(minus_rect, subrect, minus_offsets);
      if(!minus_ptr || (subrect != minus_rect)) assert(0);
    }

    ByteOffset plus_offsets[3];
    const double *plus_ptr = 0;
    if(have_plus_data) {
      Rect<3> plus_rect(my_subgrid_bounds);
      plus_rect.lo.x[direction] = ((my_subgrid_bounds.hi[direction] < sim_grid_bounds.hi[direction]) ?
                                     my_subgrid_bounds.hi[direction] + 1 :
                                     sim_grid_bounds.lo[direction]);
      plus_rect.hi.x[direction] = plus_rect.lo[direction] + (ORDER - 1);

      plus_ptr = fa_plus.raw_rect_ptr<3>(plus_rect, subrect, plus_offsets);
      if(!plus_ptr || (subrect != plus_rect)) assert(0);
    }

    // physical scale factor comes from Fortran side - multiply by number of grid
    //  points in corresponding dimension
    double ds = (rank->global_grid_size[direction] - 1) * rank->scale_dims[direction];

    // double ae = 4.0 / 5.0 * ds;
    // double be = -1.0 / 5.0 * ds;
    // double ce = 4.0 / 105.0 * ds;
    // double de = -1.0 / 280.0 * ds;

    // to call the fast stencil routines, we'll want to sort the directions
    //  by increasing strides
    int other_dir1 = (direction + 1) % 3;
    int other_dir2 = (direction + 2) % 3;
    if(in_offsets[other_dir1].offset > in_offsets[other_dir2].offset) {
      int tmp = other_dir1;
      other_dir1 = other_dir2;
      other_dir2 = tmp;
    }
    fast_gpu_stencil(my_subgrid_bounds.hi[direction] - my_subgrid_bounds.lo[direction] + 1,
                     my_subgrid_bounds.hi[other_dir1] - my_subgrid_bounds.lo[other_dir1] + 1,
                     my_subgrid_bounds.hi[other_dir2] - my_subgrid_bounds.lo[other_dir2] + 1,
                     ds,
                     minus_ptr,
                     minus_offsets[direction].offset,
                     minus_offsets[other_dir1].offset,
                     minus_offsets[other_dir2].offset,
                     in_ptr,
                     in_offsets[direction].offset,
                     in_offsets[other_dir1].offset,
                     in_offsets[other_dir2].offset,
                     plus_ptr,
                     plus_offsets[direction].offset,
                     plus_offsets[other_dir1].offset,
                     plus_offsets[other_dir2].offset,
                     out_ptr,
                     out_offsets[direction].offset,
                     out_offsets[other_dir1].offset,
                     out_offsets[other_dir2].offset);
  }
#endif
#endif
}

/*static*/
bool CalcStencilTask::fast_stencil_1d(RegionAccessor<AccessorType::Generic,double> fa_out,
                                      RegionAccessor<AccessorType::Generic,double> fa_in,
                                      RegionAccessor<AccessorType::Generic,double> fa_minus,
                                      RegionAccessor<AccessorType::Generic,double> fa_plus,
                                      Rect<3> subgrid_bounds, Rect<3> grid_bounds, 
                                      int direction, double ds,
				      bool have_minus_data, bool have_plus_data)
{
  static const int ORDER = 4;

  Rect<3> subrect;
  ByteOffset out_offsets[3];
  double *out_ptr = fa_out.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_ptr || (subrect != subgrid_bounds)) return false;

  ByteOffset in_offsets[3];
  const double *in_ptr = fa_in.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_ptr || (subrect != subgrid_bounds)) return false;

  ByteOffset minus_offsets[3];
  const double *minus_ptr = 0;
  if(have_minus_data) {
    Rect<3> minus_rect(subgrid_bounds);
    minus_rect.hi.x[direction] = ((subgrid_bounds.lo[direction] > grid_bounds.lo[direction]) ?
 				    subgrid_bounds.lo[direction] - 1 :
				    grid_bounds.hi[direction]);
    minus_rect.lo.x[direction] = minus_rect.hi[direction] - (ORDER - 1);

    minus_ptr = fa_minus.raw_rect_ptr<3>(minus_rect, subrect, minus_offsets);
    if(!minus_ptr || (subrect != minus_rect)) return false;
  }

  ByteOffset plus_offsets[3];
  const double *plus_ptr = 0;
  if(have_plus_data) {
    Rect<3> plus_rect(subgrid_bounds);
    plus_rect.lo.x[direction] = ((subgrid_bounds.hi[direction] < grid_bounds.hi[direction]) ?
		  	           subgrid_bounds.hi[direction] + 1 :
			           grid_bounds.lo[direction]);
    plus_rect.hi.x[direction] = plus_rect.lo[direction] + (ORDER - 1);

    plus_ptr = fa_plus.raw_rect_ptr<3>(plus_rect, subrect, plus_offsets);
    if(!plus_ptr || (subrect != plus_rect)) return false;
  }

  double ae = 4.0 / 5.0 * ds;
  double be = -1.0 / 5.0 * ds;
  double ce = 4.0 / 105.0 * ds;
  double de = -1.0 / 280.0 * ds;

  // to call the fast stencil routines, we'll want to sort the directions
  //  by increasing strides
  int other_dir1 = (direction + 1) % 3;
  int other_dir2 = (direction + 2) % 3;
  if(in_offsets[other_dir1].offset > in_offsets[other_dir2].offset) {
    int tmp = other_dir1;
    other_dir1 = other_dir2;
    other_dir2 = tmp;
  }
  fast_cpu_stencil(subgrid_bounds.hi[direction] - subgrid_bounds.lo[direction] + 1,
		   subgrid_bounds.hi[other_dir1] - subgrid_bounds.lo[other_dir1] + 1,
		   subgrid_bounds.hi[other_dir2] - subgrid_bounds.lo[other_dir2] + 1,
		   ds,
		   minus_ptr,
		   minus_offsets[direction].offset,
		   minus_offsets[other_dir1].offset,
		   minus_offsets[other_dir2].offset,
		   in_ptr,
		   in_offsets[direction].offset,
		   in_offsets[other_dir1].offset,
		   in_offsets[other_dir2].offset,
		   plus_ptr,
		   plus_offsets[direction].offset,
		   plus_offsets[other_dir1].offset,
		   plus_offsets[other_dir2].offset,
		   out_ptr,
		   out_offsets[direction].offset,
		   out_offsets[other_dir1].offset,
		   out_offsets[other_dir2].offset);
  return true;
		   

  // this looks like three nested loops but is really only two since we don't iterate along the stencil direction
  Rect<3> pencil_face(subgrid_bounds);
  pencil_face.hi.x[direction] = pencil_face.lo.x[direction];
  for(int z = pencil_face.lo[2]; z <= pencil_face.hi[2]; z++) {
    const double *in_ptr_y = in_ptr;
    const double *minus_ptr_y = minus_ptr;
    const double *plus_ptr_y = plus_ptr;
    double *out_ptr_y = out_ptr;

    for(int y = pencil_face.lo[1]; y <= pencil_face.hi[1]; y++) {
      const double *in_ptr_x = in_ptr_y;
      const double *minus_ptr_x = minus_ptr_y;
      const double *plus_ptr_x = plus_ptr_y;
      double *out_ptr_x = out_ptr_y;

      for(int x = pencil_face.lo[0]; x <= pencil_face.hi[0]; x++) {
	const double *in_ptr_p = in_ptr_x;
	const double *minus_ptr_p = minus_ptr_x;
	const double *plus_ptr_p = plus_ptr_x;
	double *out_ptr_p = out_ptr_x;

	// actual pencil of stencil
	{
	  double window[2 * ORDER + 1];

	  if(have_minus_data) {
	    // prime the window with minus data and the first ORDER values from the input
	    for(int i = 0; i < ORDER; i++) {
	      window[i] = *minus_ptr_p; minus_ptr_p += minus_offsets[direction];
	    }
	    for(int i = 0; i < ORDER; i++) {
	      window[i + ORDER] = *in_ptr_p; in_ptr_p += in_offsets[direction];
	    }

	    // and do the first ORDER actual stencil steps
	    for(int i = 0; i < ORDER; i++) {
	      window[2 * ORDER] = *in_ptr_p; in_ptr_p += in_offsets[direction];

	      double deriv = (((ae * (window[5] - window[3])
				+ be * (window[6] - window[2]))
			       + ce * (window[7] - window[1]))
			      + de * (window[8] - window[0]));
	      
	      *out_ptr_p = deriv; out_ptr_p += out_offsets[direction];

	      for(int j = 0; j < ORDER * 2; j++)
		window[j] = window[j + 1];
	    }
	  } else {
	    assert(0);
	  }

	  // the interior region is nice and simple - fetch from the read point, stencil, write to the write point
	  for(int i = ORDER; i <= (subgrid_bounds.hi[direction] - ORDER - subgrid_bounds.lo[direction]); i++) { 
	    window[2 * ORDER] = *in_ptr_p; in_ptr_p += in_offsets[direction];

	    double deriv = (((ae * (window[5] - window[3])
			      + be * (window[6] - window[2]))
			     + ce * (window[7] - window[1]))
			    + de * (window[8] - window[0]));
	      
	    *out_ptr_p = deriv; out_ptr_p += out_offsets[direction];
	    
	    for(int j = 0; j < ORDER * 2; j++)
	      window[j] = window[j + 1];
	  }

	  if(have_plus_data) {
	    // ORDER more stencil operations, pulling data from plus rect
	    for(int i = 0; i < ORDER; i++) {
	      window[2 * ORDER] = *plus_ptr_p; plus_ptr_p += plus_offsets[direction];

	      double deriv = (((ae * (window[5] - window[3])
				+ be * (window[6] - window[2]))
			       + ce * (window[7] - window[1]))
			      + de * (window[8] - window[0]));
	      
	      *out_ptr_p = deriv; out_ptr_p += out_offsets[direction];
	    
	      for(int j = 0; j < ORDER * 2; j++)
		window[j] = window[j + 1];
	    }
	  } else {
	    assert(0);
	  }
	}

	in_ptr_x += in_offsets[0];
	minus_ptr_x += minus_offsets[0];
	plus_ptr_x += plus_offsets[0];
	out_ptr_x += out_offsets[0];
      }

      in_ptr_y += in_offsets[1];
      minus_ptr_y += minus_offsets[1];
      plus_ptr_y += plus_offsets[1];
      out_ptr_y += out_offsets[1];
    }

    in_ptr += in_offsets[2];
    minus_ptr += minus_offsets[2];
    plus_ptr += plus_offsets[2];
    out_ptr += out_offsets[2];
  }

  return true;
}


