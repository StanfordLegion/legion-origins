
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <assert.h>

#include "cuda.h"
#include "cuda_runtime.h"
#include "gpu_help.h"

//#define DEBUG_CUDA

#ifdef DEBUG_CUDA
#define CUDA_SAFE_CALL(expr)                        \
  {                                                 \
    cudaError_t err = (expr);                       \
    if (err != cudaSuccess)                         \
    {                                               \
      printf("Cuda error: %s\n", cudaGetErrorString(err));  \
      assert(false);                                \
    }                                               \
  }
#else
#define CUDA_SAFE_CALL(expr)  expr
#endif

template<bool NEGATE>
__global__
void sum_3_kernel(const int num_points,
                  const double *in1_ptr,
                  const double *in2_ptr,
                  const double *in3_ptr,
                  double *out_ptr)
{
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  if (idx >= num_points) return;

  double in1 = stream_load(in1_ptr+idx);
  double in2 = stream_load(in2_ptr+idx);
  double in3 = stream_load(in3_ptr+idx);
  double value = in1 + in2 + in3;
  if (NEGATE)
    value = -value;
  stream_store(out_ptr+idx, value);
}

__host__
void gpu_sum_3_fields(const bool negate,
                      const int num_points,
                      const double *in1_ptr,
                      const double *in2_ptr,
                      const double *in3_ptr,
                      double *out_ptr)
{
  const int threads_per_block = 256;
  const int num_blocks = (num_points + (threads_per_block-1))/threads_per_block;

  // Make a separate stream so we can run the kernel
  // parallel with the temp kernel
  if (negate)
  {
    sum_3_kernel<true><<<num_blocks,threads_per_block>>>(
          num_points, in1_ptr, in2_ptr, in3_ptr, out_ptr);
  }
  else
  {
    sum_3_kernel<false><<<num_blocks,threads_per_block>>>(
          num_points, in1_ptr, in2_ptr, in3_ptr, out_ptr);
  }
}

__global__
void sum_3_integrate_kernel(const int num_points,
			    const double *in1_ptr,
			    const double *in2_ptr,
			    const double *in3_ptr,
			    double *q_ptr,
			    double *qerr_ptr,
			    double *qtmp_ptr,
			    double alpha, double beta, double err, int which)
{
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  if (idx >= num_points) return;

  double in1 = stream_load(in1_ptr+idx);
  double in2 = stream_load(in2_ptr+idx);
  double in3 = stream_load(in3_ptr+idx);
  double sum = in1 + in2 + in3;

  double q = stream_load(q_ptr+idx);
  double qerr, qtmp;
  if(which == 0 /* FIRST_INTEGRATION */) {
    qerr = 0;
    qtmp = q;
  } else {
    qerr = stream_load(qerr_ptr+idx);
    qtmp = stream_load(qtmp_ptr+idx);
  }

  qerr += err * sum;
  q = qtmp + alpha * sum;
  qtmp = q + beta * sum; // always do add - math is free on GPU

  stream_store(q_ptr+idx, q);
  stream_store(qerr_ptr+idx, qerr);
  if(which != 2 /* LAST_INTEGRATION */)
    stream_store(qtmp_ptr+idx, qtmp);
}

__host__
void gpu_sum_3_integrate_fields(const int num_points,
				const double *in1_ptr,
				const double *in2_ptr,
				const double *in3_ptr,
				double *q_ptr,
				double *qerr_ptr,
				double *qtmp_ptr,
				double alpha, double beta, double err, int which)
{
  const int threads_per_block = 256;
  const int num_blocks = (num_points + (threads_per_block-1))/threads_per_block;

  // Make a separate stream so we can run the kernel
  // parallel with the temp kernel
  sum_3_integrate_kernel<<<num_blocks,threads_per_block>>>
    (num_points, in1_ptr, in2_ptr, in3_ptr, q_ptr, qerr_ptr, qtmp_ptr,
     alpha, beta, err, which);
}

template<bool NEGATE>
__global__
void sum_4_kernel(const int num_points,
                  const double *in1_ptr,
                  const double *in2_ptr,
                  const double *in3_ptr,
                  const double *in4_ptr,
                  double *out_ptr)
{
  const int idx = blockIdx.x*blockDim.x + threadIdx.x; 
  if (idx >= num_points) return;

  double in1 = stream_load(in1_ptr+idx);
  double in2 = stream_load(in2_ptr+idx);
  double in3 = stream_load(in3_ptr+idx);
  double in4 = stream_load(in4_ptr+idx);
  double value = in1 + in2 + in3 + in4;
  if (NEGATE)
    value = -value;
  stream_store(out_ptr+idx, value);
}

__host__
void gpu_sum_4_fields(const bool negate,
                      const int num_points,
                      const double *in1_ptr,
                      const double *in2_ptr,
                      const double *in3_ptr,
                      const double *in4_ptr,
                      double *out_ptr)
{
  const int threads_per_block = 256;
  const int num_blocks = (num_points + (threads_per_block-1))/threads_per_block;

  // Make a separate stream so we can run the kernel
  // parallel with the temp kernel
  if (negate)
  {
    sum_4_kernel<true><<<num_blocks,threads_per_block>>>(
          num_points, in1_ptr, in2_ptr, in3_ptr, in4_ptr, out_ptr);
  }
  else
  {
    sum_4_kernel<false><<<num_blocks,threads_per_block>>>(
          num_points, in1_ptr, in2_ptr, in3_ptr, in4_ptr, out_ptr);
  }
}

__global__
void sum_4_integrate_kernel(const int num_points,
			    const double *in1_ptr,
			    const double *in2_ptr,
			    const double *in3_ptr,
			    const double *in4_ptr,
			    double *q_ptr,
			    double *qerr_ptr,
			    double *qtmp_ptr,
			    double alpha, double beta, double err, 
                            double small, int which)
{
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  if (idx >= num_points) return;

  double in1 = stream_load(in1_ptr+idx);
  double in2 = stream_load(in2_ptr+idx);
  double in3 = stream_load(in3_ptr+idx);
  double in4 = stream_load(in4_ptr+idx);
  double sum = in1 + in2 + in3 + in4;

  double q = stream_load(q_ptr+idx);
  double qerr, qtmp;
  if(which == 0 /* FIRST_INTEGRATION */) {
    qerr = 0;
    qtmp = q;
  } else {
    qerr = stream_load(qerr_ptr+idx);
    qtmp = stream_load(qtmp_ptr+idx);
  }

  qerr += err * sum;
  q = qtmp + alpha * sum;
  qtmp = q + beta * sum; // always do add - math is free on GPU

  stream_store(qerr_ptr+idx, qerr);
  if(which != 2 /* LAST_INTEGRATION */)
    stream_store(qtmp_ptr+idx, qtmp);
  else if (q < small)
    q = small;
  stream_store(q_ptr+idx, q);
}

__host__
void gpu_sum_4_integrate_fields(const int num_points,
				const double *in1_ptr,
				const double *in2_ptr,
				const double *in3_ptr,
				const double *in4_ptr,
				double *q_ptr,
				double *qerr_ptr,
				double *qtmp_ptr,
				double alpha, double beta, double err, 
                                double small, int which)
{
  const int threads_per_block = 256;
  const int num_blocks = (num_points + (threads_per_block-1))/threads_per_block;

  // Make a separate stream so we can run the kernel
  // parallel with the temp kernel
  sum_4_integrate_kernel<<<num_blocks,threads_per_block>>>
    (num_points, in1_ptr, in2_ptr, in3_ptr, in4_ptr, q_ptr, qerr_ptr, qtmp_ptr,
     alpha, beta, err, small, which);
}

__global__
void sum_6_integrate_kernel(const int num_points,
			    const double *in1_ptr,
			    const double *in2_ptr,
			    const double *in3_ptr,
			    const double *in4_ptr,
			    const double *in5_ptr,
			    const double *in6_ptr,
			    double *q_ptr,
			    double *qerr_ptr,
			    double *qtmp_ptr,
			    double alpha, double beta, double err, 
                            int which)
{
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  if (idx >= num_points) return;

  double in1 = stream_load(in1_ptr+idx);
  double in2 = stream_load(in2_ptr+idx);
  double in3 = stream_load(in3_ptr+idx);
  double in4 = stream_load(in4_ptr+idx);
  double in5 = stream_load(in5_ptr+idx);
  double in6 = stream_load(in6_ptr+idx);
  double sum = in1 + in2 + in3 + (in4 * (in5 + in6));

  double q = stream_load(q_ptr+idx);
  double qerr, qtmp;
  if(which == 0 /* FIRST_INTEGRATION */) {
    qerr = 0;
    qtmp = q;
  } else {
    qerr = stream_load(qerr_ptr+idx);
    qtmp = stream_load(qtmp_ptr+idx);
  }

  qerr += err * sum;
  q = qtmp + alpha * sum;
  qtmp = q + beta * sum; // always do add - math is free on GPU

  stream_store(qerr_ptr+idx, qerr);
  if(which != 2 /* LAST_INTEGRATION */)
    stream_store(qtmp_ptr+idx, qtmp);
  stream_store(q_ptr+idx, q);
}

__host__
void gpu_sum_6_integrate_fields(const int num_points,
				const double *in1_ptr,
				const double *in2_ptr,
				const double *in3_ptr,
				const double *in4_ptr,
				const double *in5_ptr,
				const double *in6_ptr,
				double *q_ptr,
				double *qerr_ptr,
				double *qtmp_ptr,
				double alpha, double beta, double err, 
                                int which)
{
  const int threads_per_block = 256;
  const int num_blocks = (num_points + (threads_per_block-1))/threads_per_block;

  // Make a separate stream so we can run the kernel
  // parallel with the temp kernel
  sum_6_integrate_kernel<<<num_blocks,threads_per_block>>>
    (num_points, in1_ptr, in2_ptr, in3_ptr, in4_ptr, in5_ptr, in6_ptr, q_ptr, qerr_ptr, qtmp_ptr,
     alpha, beta, err, which);
}

template <typename T>
__global__
void gpu_reduce_sum_kernel(size_t num_points, const T *data, T *partial_sums)
{
  int my_global_tid = blockIdx.x * blockDim.x + threadIdx.x;
  int total_threads = gridDim.x * blockDim.x;

  T my_sum = 0;
  for(int ofs = my_global_tid; ofs < num_points; ofs += total_threads) {
    T v = stream_load<T>(data+ofs);
    my_sum += v;
  }

  extern __shared__ T scratch[];
  // now a decent-but-not-perfect intra-CTA reduction
  int n = blockDim.x;
  while(n > 1) {
    int n_half = (n + 1) >> 1; // round up

    // upper half put their value in scratch
    if((threadIdx.x >= n_half) && (threadIdx.x < n))
      scratch[threadIdx.x - n_half] = my_sum;

    if(n > 32)
      syncthreads();  // unnecessary once we're intra-warp

    // lower half read those values (handle nasty edge case)
    if(threadIdx.x < (n - n_half))
      my_sum += scratch[threadIdx.x];

    n = n_half;
  }

  // last thread writes out the CTA-reduced value
  if(threadIdx.x == 0)
    stream_store<T>(partial_sums+blockIdx.x, my_sum);
}

__host__
double gpu_reduce_sum(size_t num_points, const double *data)
{
  const int threads_per_block = 256;
  const int num_blocks = (num_points + (threads_per_block-1))/threads_per_block;

  // temp buffer on GPU for partial sums
  double *gpu_partials;
  CUDA_SAFE_CALL(cudaMalloc(&gpu_partials, sizeof(double) * num_blocks));

  // Make a separate stream so we can run the kernel
  // parallel with the temp kernel

#ifdef K20_ARCH
  CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte)); 
#endif

  gpu_reduce_sum_kernel<<<num_blocks, threads_per_block, 
    (threads_per_block >> 1) * sizeof(double)>>>(num_points, data, gpu_partials);

  // copy partial sums back
  double *cpu_partials = (double *)alloca(sizeof(double) * num_blocks);
  CUDA_SAFE_CALL(cudaMemcpy(cpu_partials, gpu_partials, sizeof(double) * num_blocks,
				 cudaMemcpyDeviceToHost));
  CUDA_SAFE_CALL(cudaFree(gpu_partials));

  // now do final bit of sum on CPU
  double sum = 0;
  for(size_t i = 0; i < num_blocks; i++) {
    printf("partial %ld = %g\n", i, cpu_partials[i]);
    sum += cpu_partials[i];
  }

  return sum;
}
