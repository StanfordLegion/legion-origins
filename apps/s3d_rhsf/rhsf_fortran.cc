#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <mpi.h>

#include "rhsf.h"
#include "impose_pressure.h"

static int mpi_pass = 1;

static S3DRank *rank = 0;

extern "C" void initialize_rhsf_legion_(const char *mechanism_name,
					const int *local_grid,
					const int *global_grid,
					const int *proc_grid,
					const int *proc_id,
					const int *vary_in_dims,
					const int *p_nvar_tot,
					const int *p_n_spec,
					const int *p_n_reg,
					const int *p_n_scalar,
					const int *p_iorder,
					const int *p_iforder,
					const int *p_n_steps,
					const int *p_n_stages,
					const int *p_lagging_switch,
					const int *p_lag_steps,
					const int *p_npts,
					const double *p_tempmin,
					const double *p_tempmax,
					const double *cpCoeff_aa,
					const double *cpCoeff_bb,
					const double *enthCoeff_aa,
					const double *enthCoeff_bb,
					const int *p_i_react,
					const int *p_unif_grid_dims,
					const double *scale_1x,
					const double *scale_1y,
					const double *scale_1z,
					const double *reference_values)
{
  bool first_rank = ((proc_id[0] == 0) && (proc_id[1] == 0) && (proc_id[2] == 0));

  // check that grid is uniform and that we're using the set of reference
  //  values that we have compiled in to the Legion version
  if(!p_unif_grid_dims[0] || !p_unif_grid_dims[1] || !p_unif_grid_dims[2]) {
    if(first_rank)
      printf("ERROR: Legion S3D does not support non-uniform grids!\n");
    exit(1);
  }
  if((reference_values[0] != MACH_NO) || (reference_values[1] != RE_REAL) ||
     (reference_values[2] != PARAM_PR) || (reference_values[3] != G_REF) ||
     (reference_values[4] != A_REF) || (reference_values[5] != T_0) ||
     (reference_values[6] != RHO_REF) || (reference_values[7] != LAMBDA_REF)) {
    if(first_rank) {
      printf("ERROR: Reference value mismatch:\n");
      printf("  Mach number     :  %g (Fortran) vs. %g (S3D)\n", reference_values[0], MACH_NO);
      printf("  Reynolds number :  %g (Fortran) vs. %g (S3D)\n", reference_values[1], RE_REAL);
      printf("  Prandtl number  :  %g (Fortran) vs. %g (S3D)\n", reference_values[2], PARAM_PR);
      printf("  Spec heat ratio :  %g (Fortran) vs. %g (S3D)\n", reference_values[3], G_REF);
      printf("  Speed of sound  :  %g (Fortran) vs. %g (S3D)\n", reference_values[4], A_REF);
      printf("  Freestream temp :  %g (Fortran) vs. %g (S3D)\n", reference_values[5], T_0);
      printf("  Ref density     :  %g (Fortran) vs. %g (S3D)\n", reference_values[6], RHO_REF);
      printf("  Ref conductivity:  %g (Fortran) vs. %g (S3D)\n", reference_values[7], LAMBDA_REF);
    }
    exit(1);
  }

  if(first_rank) {
    printf("Legion Init:\n");
    printf("Mechanism = '%s'\n", mechanism_name);
    printf("Local Grid Size = (%3d, %3d, %3d)\n", local_grid[0], local_grid[1], local_grid[2]);
    printf("Global Grid Size = (%3d, %3d, %3d)\n", global_grid[0], global_grid[1], global_grid[2]);
    printf("Proc Grid Size = (%3d, %3d, %3d)\n", proc_grid[0], proc_grid[1], proc_grid[2]);
    printf("Proc ID = (%3d, %3d, %3d)\n", proc_id[0], proc_id[1], proc_id[2]);
    printf("nvar_tot = %d, n_spec = %d, n_reg = %d, n_scalar = %d\n",
	   *p_nvar_tot, *p_n_spec, *p_n_reg, *p_n_scalar);
    printf("iorder = %d, iforder = %d, vary = %c%c%c\n",
	   *p_iorder, *p_iforder,
	   (vary_in_dims[0] ? 'X' : '-'),
	   (vary_in_dims[1] ? 'Y' : '-'),
	   (vary_in_dims[2] ? 'Z' : '-'));
  }

  // hardcoded for now - should decide based on available CPUs, GPUs
  int n_subranks = 0;
  {
    const char *e = getenv("RHSF_SUBRANKS");
    if(e) {
      n_subranks = atoi(e);
      if(first_rank)
	printf("subranks set to %d from environment\n", n_subranks);
    }
  }

  // full distribution right now
  const int *dist_grid = proc_grid;
  // turn on ghost cells
  const char *dummy_args[] = { 0, "-ghost", "-hoist", "-field-centric", "-progress", 0 };
  const int dummy_argc = (sizeof(dummy_args) / sizeof(dummy_args[0])) - 1;
  S3DRank::parse_cmdline_args(dummy_argc, dummy_args);

  {
    const char *e = getenv("RHSF_FUSE");
    if (e) {
      S3DRank::get_fuse_fields() = atoi(e);
      if(first_rank)
	printf("fuse fields set to %d from environment\n", S3DRank::get_fuse_fields());
    }
  }
  {
    const char *e = getenv("RHSF_MIXED");
    if (e) {
      bool enabled = atoi(e) != 0;
      S3DRank::get_mixed_stencils() = enabled;
      if(first_rank)
	printf("set mixed stencils to '%s' from environment\n", enabled ? "true" : "false");
    }
  }
  {
    const char *e = getenv("RHSF_ALLGPU");
    if (e) {
      bool enabled = atoi(e) != 0;
      S3DRank::get_all_gpu() = enabled;
      if(first_rank)
	printf("set all gpus to '%s' from environment\n", enabled ? "true" : "false");
    }
  }
  {
    const char *e = getenv("RHSF_PROFILE");
    if (e) {
      bool enabled = atoi(e) != 0;
      S3DRank::get_enable_profiling() = enabled;
      if(first_rank)
	printf("s3d profiling to '%s' from environment\n", enabled ? "true" : "false");
    }
  }

  {
    const char *e = getenv("RHSF_COMPLAG");
    if (e) {
      int val = atoi(e);
      S3DRank::get_compression_lag() = val;
      if(first_rank)
	printf("s3d compression lag set to %d from environment\n", val);
    }
  }

#ifdef USE_CEMA
  int cema_samples_per_rank = 0; // default is 0 == disabled
  {
    const char *e = getenv("RHSF_CEMA_SAMPLES");
    if (e) {
      cema_samples_per_rank = atoi(e);
      if(first_rank)
	printf("s3d CEMA samples per rank set to %d from environment\n",
	       cema_samples_per_rank);
    }
  }
#endif

  // we're requiring uniform grids above, so just take the first scale factor in each
  //  dimension
  double scale_dims[3];
  scale_dims[0] = scale_1x[0];
  scale_dims[1] = scale_1y[0];
  scale_dims[2] = scale_1z[0];

  rank = new S3DRank(local_grid, global_grid, dist_grid, proc_grid, proc_id, vary_in_dims,
		     p_nvar_tot, p_n_spec, p_n_reg, p_n_scalar, p_iorder, p_iforder,
		     p_n_stages, p_n_steps, &n_subranks, p_lagging_switch, p_lag_steps,
                     p_i_react, scale_dims
#ifdef USE_CEMA
		     , &cema_samples_per_rank
#endif
		     );

  rank->provide_cpe_data(*p_npts, enthCoeff_aa, enthCoeff_bb, cpCoeff_aa, cpCoeff_bb);
}

extern "C" void complete_legion_configure_(void)
{
  rank->complete_configure();
}

extern "C" void setup_compression_legion_(const double *p_geomfac2,
					  const double *p_polyn,
					  const double *p_rpm,
					  const double *p_ttdc,
					  const double *p_toff,
					  const double *p_tp,
					  const double *p_t90,
					  const double *p_ptdc,
					  const double *p_pc,
					  const int    *p_enforce)
{
  bool first_rank = ((rank->my_proc_id[0] == 0) &&
		     (rank->my_proc_id[1] == 0) &&
		     (rank->my_proc_id[2] == 0));

  if(getenv("RHSF_IGNORE_COMPRESSION") && atoi(getenv("RHSF_IGNORE_COMPRESSION"))) {
    if(first_rank)
      printf("IGNORING COMPRESSION!\n");
    return;
  }
  if(first_rank)
    printf("setup compression: %g %g / %g / %g %g %g %g / %g %g / %d\n",
	   *p_geomfac2, *p_polyn, *p_rpm,
	   *p_ttdc, *p_toff, *p_tp, *p_t90,
	   *p_ptdc, *p_pc, *p_enforce);

  rank->compression = new CompressionSourceTerm(*p_geomfac2, *p_polyn, *p_rpm,
						*p_ttdc, *p_toff, *p_tp, *p_t90,
						*p_ptdc, *p_pc, *p_enforce);
}

extern "C" void rhsf_legion_init_temp_(const double *temp)
{
  //printf("initial temps: %g %g %g\n", temp[0], temp[1], temp[2]);
  rank->provide_initial_temperature(temp);
}

extern "C" void rhsf_legion_(double *p_tstep, double *q, double *rhs, double *qs, double *rhs_scalar)
{
  // this entry point should only be used if ERK is not configured
  assert(rank->erk_stages == 0);

  S3DRank::RHSFArrays step_data;
  bzero(&step_data, sizeof(step_data));
  step_data.q = q;
  step_data.rhs_cmp = rhs;
  step_data.rhs = rhs;
  if(rank->n_scalar > 0) {
    step_data.qs = qs;
    step_data.rhs_scalar = rhs_scalar;
  }

#ifdef DEBUG_HANDSHAKE
  printf("MPI: pass=%d handoff to legion\n", mpi_pass);
#endif
  rank->begin_rhsf_step(&step_data, *p_tstep);

#ifdef DEBUG_HANDSHAKE
  printf("MPI: pass=%d waiting on legion\n", mpi_pass);
#endif
  rank->finish_rhsf_step();
#ifdef DEBUG_HANDSHAKE
  printf("MPI: pass=%d done waiting on legion\n", mpi_pass);
#endif
  mpi_pass++;
}

extern "C" void configure_erk_legion_(const int *p_nstages, const double *alpha,
				      const double *beta, const double *error,
                                      const double *small)
{
  //printf("configuring ERK: stages = %d\n", *p_nstages);

  rank->configure_erk(*p_nstages, alpha, beta, error, small);
}

extern "C" void integrate_legion_(const int *p_nstages, const double *alpha,
				  const double *beta, const double *error,
				  double *p_time, double *p_tstep, int *p_handback,
                                  double *q, double *q_err, double *qs)
{
  //printf("integrate_legion called: %d stages\n", rank->erk_stages);
  double time_accum = *p_time;
  double tstep = *p_tstep;
  int handback = *p_handback;

  if(rank->erk_stages == 0) {
    //printf("erk not set up - manually unrolling\n");

    // going to need 2 temp arrays
    int fields = rank->nvar_tot;
    int cells = (rank->local_grid_size[0] *
		 rank->local_grid_size[1] *
		 rank->local_grid_size[2]);
    double *rhs = (double *)malloc(fields * cells * sizeof(double));
    double *qtmp = (double *)malloc(fields * cells * sizeof(double));
    double *rhs_scalar = 0;
    double *qstmp = 0;
    if(rank->n_scalar > 0) {
      rhs_scalar = (double *)malloc(rank->n_scalar * cells * sizeof(double));
      qstmp = (double *)malloc(rank->n_scalar * cells * sizeof(double));
    }

    for(int i = 0; i < fields * cells; i++) {
      q_err[i] = 0.0;
      qtmp[i] = q[i];
    }

    if(rank->n_scalar > 0)
      for(int i = 0; i < rank->n_scalar * cells; i++) {
	qstmp[i] = qs[i];
      }

    for(int i = 0; i < *p_nstages; i++) {
      S3DRank::RHSFArrays step_data;
      bzero(&step_data, sizeof(step_data));
      step_data.q = q;
      step_data.rhs = rhs;
      if(rank->n_scalar > 0) {
	step_data.qs = qs;
	step_data.rhs_scalar = rhs_scalar;
      }

      rank->begin_rhsf_step(&step_data, *p_tstep);
      rank->finish_rhsf_step();
      mpi_pass++;

      // printf("  stage %d integration: alpha = %g, beta = %g, err = %g\n",
      // 	     i+1, alpha[i], beta[i], error[i]);
      // for(int j = 0; j < fields; j++)
      // 	printf(" rhsf(1,1,1,%d) = %g, q = %g\n", j+1, rhs[cells * j], q[cells * j]);

      for(int j = 0; j < fields * cells; j++) {
	q_err[j] += error[i] * tstep * rhs[j];
	q[j] = qtmp[j] + alpha[i] * tstep * rhs[j];
	qtmp[j] = q[j] + beta[i] * tstep * rhs[j];
      }

      if(rank->n_scalar > 0)
	for(int j = 0; j < rank->n_scalar * cells; j++) {
	  qs[j] = qstmp[j] + alpha[i] * tstep * rhs_scalar[j];
	  qstmp[j] = qs[j] + beta[i] * tstep * rhs_scalar[j];
	}
    }

    free(rhs);
    free(qtmp);
    if(rank->n_scalar > 0) {
      free(rhs_scalar);
      free(qstmp);
    }
  } else {
    // full RK integration handled within legion
    S3DRank::IntegrationArrays step_data;
    bzero(&step_data, sizeof(step_data));
    step_data.q = q;
    step_data.q_err = q_err;
    if(rank->n_scalar > 0)
      step_data.qs = qs;

    rank->begin_integration(&step_data, time_accum, tstep, handback);
    rank->finish_integration();
    mpi_pass++;
  }
}

extern "C" void shutdown_rhsf_legion_(void)
{
  // This will invoked the method to shutdown the Legion runtime
  delete rank;
}

