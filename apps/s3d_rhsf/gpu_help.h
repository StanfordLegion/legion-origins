
#ifndef __GPU_HELP_H__
#define __GPU_HELP_H__

template<typename T>
__device__ __forceinline__
T stream_load(const T *ptr)
{
  return (*ptr);
}

template<typename T>
__device__ __forceinline__
void stream_store(T *ptr, const T &value)
{
  (*ptr) = value;
}

// Specialize for floats 
template<>
__device__ __forceinline__
float stream_load<float>(const float *ptr)
{
  float result;
#if __CUDA_ARCH__ == 350
  asm volatile("ld.global.nc.f32 %0, [%1];" : "=f"(result) : "l"(ptr) : "memory");
#else
  asm volatile("ld.global.cs.f32 %0, [%1];" : "=f"(result) : "l"(ptr) : "memory");
#endif
  return result;
}

template<>
__device__ __forceinline__
void stream_store<float>(float *ptr, const float &value)
{
  asm volatile("st.global.cs.f32 [%0], %1;" :  : "l"(ptr), "f"(value) : "memory");
}

// Specialize for doubles
template<>
__device__ __forceinline__
double stream_load<double>(const double *ptr)
{
  double result;
#if __CUDA_ARCH__ == 350
  asm volatile("ld.global.nc.f64 %0, [%1];" : "=d"(result) : "l"(ptr) : "memory");
#else
  asm volatile("ld.global.cs.f64 %0, [%1];" : "=d"(result) : "l"(ptr) : "memory");
#endif
  return result;
}

template<>
__device__ __forceinline__
void stream_store<double>(double *ptr, const double &value)
{
  asm volatile("st.global.cs.f64 [%0], %1;" : : "l"(ptr), "d"(value) : "memory");
}

#endif // __GPU_HELP_H__
