
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <assert.h>

#include "cuda.h"
#include "cuda_runtime.h"
#include "gpu_help.h"

#include "gpu_s3d_constants.h"

#define MACH_NO           (0.001)
#define RE_REAL           (100.0)
#define PARAM_PR          (0.708)
#define G_REF             (1.4)
#define A_REF             (347.2) // m/s
#define T_0               (300.0) // K
#define RHO_REF           (1.1766)  // Kg/m3
#define LAMBDA_REF        (26.14e-3)
#define PARAM_RE          (RE_REAL / MACH_NO)
#define T_REF             (T_0 * (G_REF - 1))
#define CP_REF            ((A_REF * A_REF) / T_REF)
#define L_REF             (PARAM_PR * LAMBDA_REF / CP_REF * PARAM_RE / RHO_REF / A_REF)
#define VIS_REF           (RHO_REF * A_REF * L_REF)
#define DIF_REF           (A_REF * L_REF)
#define P_REF             ((A_REF * A_REF) * RHO_REF * 10.0)

//#define DEBUG_CUDA

#ifdef DEBUG_CUDA
#define CUDA_SAFE_CALL(expr)                        \
  {                                                 \
    cudaError_t err = (expr);                       \
    if (err != cudaSuccess)                         \
    {                                               \
      printf("Cuda error: %s\n", cudaGetErrorString(err));  \
      assert(false);                                \
    }                                               \
  }
#else
#define CUDA_SAFE_CALL(expr)  expr
#endif

template<int N_SPEC>
__global__
void calc_species_kernel(const size_t n_pts,
                         const double *in_rho,
                         const double *in_rho_y,
                         const int rho_y_stride,
                         double *out_avmolwt,
                         double *out_mixmw,
                         double *out_yspec,
                         const int yspec_stride)
{
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  // Check for upper bound
  if (idx >= n_pts) return;
  const double volume = 1.0 / stream_load(in_rho+idx);
  
  double yspec_n2 = 1.0;
  double avmolwt = 0.0;
  #pragma unroll
  for (int s = 0; s < (N_SPEC-1); s++) {
    double yspec = volume * stream_load(in_rho_y+s*rho_y_stride+idx);
    stream_store(out_yspec+s*yspec_stride+idx, yspec);
    yspec_n2 -= yspec;
    avmolwt += yspec * recip_molecular_masses[s];
  }
  // Write out the last species
  stream_store(out_yspec+(N_SPEC-1)*yspec_stride+idx, yspec_n2);

  avmolwt += yspec_n2 * recip_molecular_masses[N_SPEC-1];
  avmolwt *= 1000.0;
  double mixmw = 1.0 / avmolwt;
  stream_store(out_avmolwt+idx, avmolwt);
  stream_store(out_mixmw+idx, mixmw);
}

__host__
void calc_species_gpu(size_t n_pts, size_t n_spec,
                      const double *in_rho_d,
                      const double *in_rho_y_d,
                      const int rho_y_stride,
                      double *out_avmolwt_d,
                      double *out_mixmw_d,
                      double *out_yspec_d,
                      const int yspec_stride)
{
  const int threads_per_block = 256;
  const int num_blocks = (n_pts + (threads_per_block-1))/threads_per_block;

  // Make a separate stream so we can run the kernel
  // parallel with the temp kernel
  switch (n_spec)
  {
    case 9:
      {
        calc_species_kernel<9><<<num_blocks,threads_per_block>>>(
                                  n_pts, in_rho_d, in_rho_y_d, rho_y_stride,
                                  out_avmolwt_d, out_mixmw_d, out_yspec_d, yspec_stride);
        break;
      }
    case 30:
      {
        calc_species_kernel<30><<<num_blocks,threads_per_block>>>(
                                  n_pts, in_rho_d, in_rho_y_d, rho_y_stride,
                                  out_avmolwt_d, out_mixmw_d, out_yspec_d, yspec_stride);
        break;
      }
    case 52:
      {
        calc_species_kernel<52><<<num_blocks,threads_per_block>>>(
                                  n_pts, in_rho_d, in_rho_y_d, rho_y_stride,
                                  out_avmolwt_d, out_mixmw_d, out_yspec_d, yspec_stride);
        break;
      }
    case 116:
      {
        calc_species_kernel<116><<<num_blocks,threads_per_block>>>(
                                   n_pts, in_rho_d, in_rho_y_d, rho_y_stride,
                                   out_avmolwt_d, out_mixmw_d, out_yspec_d, yspec_stride);
        break;
      }
    default:
      {
        printf("NEED TO ADD CASE FOR %ld SPECIES IN gpu_temp.cu!!!!!!!\n", n_spec);
        assert(false);
      }
  }
}

__global__
void spec_precalc_gpu(const size_t n_pts, const double *rho,
                      const double *vel_x, const double *vel_y,
                      const double *vel_z, const double *flux_x,
                      const double *flux_y, const double *flux_z,
                      double *pre_x, double *pre_y, double *pre_z)
{
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  // Check for upper bound
  if (idx >= n_pts) return;  
  const double p = -(stream_load(rho+idx));

  double x = p * stream_load(vel_x+idx) - stream_load(flux_x+idx);
  double y = p * stream_load(vel_y+idx) - stream_load(flux_y+idx);
  double z = p * stream_load(vel_z+idx) - stream_load(flux_z+idx);
  stream_store(pre_x+idx, x);
  stream_store(pre_y+idx, y);
  stream_store(pre_z+idx, z);
}

__host__
void gpu_field_spec_precalc(const size_t n_pts, const double *rho_d,
                             const double *vel_x_d, const double *vel_y_d,
                             const double *vel_z_d, const double *flux_x_d,
                             const double *flux_y_d, const double *flux_z_d,
                             double *pre_x_d, double *pre_y_d, double *pre_z_d)
{
  const int threads_per_block = 256;
  const int num_blocks = (n_pts + (threads_per_block-1))/threads_per_block;

  spec_precalc_gpu<<<num_blocks,threads_per_block>>>(
                        n_pts, rho_d, vel_x_d, vel_y_d, vel_z_d,
                        flux_x_d, flux_y_d, flux_z_d,
                        pre_x_d, pre_y_d, pre_z_d);
}

__global__
void calc_gamma_kernel(size_t n_pts,
		       const double *in_cpmix_ptr, const double *in_avmolwt_ptr,
		       double *out_gamma_ptr)
{
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  // Check for upper bound
  if (idx >= n_pts) return;  

  const double Ru = 8.31451 / CP_REF;
  //  for(size_t i = 0; i < n_pts; i++) {
  double cpmix = in_cpmix_ptr[idx];
  double avmolwt = in_avmolwt_ptr[idx];
  double gamma = cpmix / (cpmix - avmolwt * Ru);
  out_gamma_ptr[idx] = gamma;
}

__host__
void calc_gamma_gpu(size_t n_pts,
		    const double *in_cpmix, const double *in_avmolwt,
		    double *out_gamma)
{
  const int threads_per_block = 256;
  const int num_blocks = (n_pts + (threads_per_block-1))/threads_per_block;

  calc_gamma_kernel<<<num_blocks,threads_per_block>>>(n_pts, in_cpmix, in_avmolwt,
						      out_gamma);
}
