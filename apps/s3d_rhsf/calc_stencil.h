
#ifndef _CALC_STENCIL_H_
#define _CALC_STENCIL_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

class CalcStencilTask {
public:
  static void calc_stencil_1d(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
                              const Rect<3>& proc_grid_bounds, const Rect<3>& full_proc_grid_bounds,
                              IndexSpace is_grid, IndexPartition ip_top,
                              FieldSpace fs_src, LogicalRegion lr_src, FieldID fid_src,
                              FieldSpace fs_dst, LogicalRegion lr_dst, FieldID fid_dst,
                              FieldSpace fs_ghost, const LogicalRegion *lr_ghost,
                              const std::vector<PhysicalRegion> &regions, FieldID fid_ghost,
                              int direction, bool periodic, int stage, int tag = 0, bool enable_gpu = false);
  static void calc_N_stencil_1d(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
                                const Rect<3>& proc_grid_bounds, const Rect<3>& full_proc_grid_bounds,
                                IndexSpace is_grid, IndexPartition ip_top,
                                FieldSpace fs_src, LogicalRegion lr_src, const std::vector<FieldID> &src_fields,
                                FieldSpace fs_dst, LogicalRegion lr_dst, const std::vector<FieldID> &dst_fields,
                                FieldSpace fs_ghost, const LogicalRegion *lr_ghost,
                                const std::vector<PhysicalRegion> &regions, const std::vector<FieldID> &ghost_fields,
                                int direction, bool periodic, int stage, int tag = 0, bool enable_gpu = false);
protected:
  static void copy_region(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
                          LogicalRegion lr_src, FieldID fid_src,
                          LogicalRegion lr_dst, FieldID fid_dst, LogicalRegion lr_src_parent);
  static void copy_N_region(HighLevelRuntime *runtime, Context ctx, S3DRank *rank,
                            LogicalRegion lr_src, const std::vector<FieldID> &src_fields,
                            LogicalRegion lr_dst, const std::vector<FieldID> &dst_fields,
                            LogicalRegion lr_src_parent);
public:
  static void register_cpu_variants(void);
  static void register_hybrid_variants(void);
public:
  static void calc_stencil_1d_cpu_task(const Task *task,
                                       const std::vector<PhysicalRegion> &regions,
                                       Context ctx, HighLevelRuntime *runtime);
  static void calc_stencil_1d_gpu_task(const Task *task,
                                       const std::vector<PhysicalRegion> &regions,
                                       Context ctx, HighLevelRuntime *runtime);
protected:
  static void rhsf_stencil_1d_cpu_task(Point<3> point, int direction,
                                       const std::vector<RegionRequirement> &reqs,
                                       const std::vector<PhysicalRegion> &regions,
                                       Context ctx, HighLevelRuntime *runtime);
  static void rhsf_stencil_1d_gpu_task(Point<3> point, int direction,
                                       const std::vector<RegionRequirement> &reqs,
                                       const std::vector<PhysicalRegion> &regions,
                                       Context ctx, HighLevelRuntime *runtime);
protected:
  static bool fast_stencil_1d(RegionAccessor<AccessorType::Generic,double> fa_out,
                              RegionAccessor<AccessorType::Generic,double> fa_in,
                              RegionAccessor<AccessorType::Generic,double> fa_minus,
                              RegionAccessor<AccessorType::Generic,double> fa_plus,
                              Rect<3> subgrid_bounds, Rect<3> grid_bounds, 
                              int direction, double ds,
			      bool have_minus_data, bool have_plus_data);
};

#ifdef NEW_BARRIER_EXCHANGE
// exchanges PhaseBarriers necessary for stencil operations
void do_barrier_exchange(const Task *task,
			 const std::vector<PhysicalRegion> &regions,
			 Context ctx, HighLevelRuntime *runtime,
			 const Rect<3>& proc_grid_bounds);
#endif

#endif
