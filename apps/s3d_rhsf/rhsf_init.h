
#ifndef _RHSF_INIT_H_
#define _RHSF_INIT_H_

#include "legion.h"

using namespace LegionRuntime::HighLevel;

// These are the tasks that are used to set up the Legion side
// for running iterations of rhsf.

// Result struct returned out of connect_mpi_task to indicate status
// to the top_level_task, needed for mapping tasks to processors in
// MPISyncMapper.
struct MPIConnectResult {
  LegionRuntime::HighLevel::DomainPoint point;
  LegionRuntime::HighLevel::Processor proc;
  bool success;
  int subranks_needed;
};

// Struct used for keeping track of which phase barriers are
// associated with certain fields of ghost regions.
struct GhostBarrierMap {
  std::map<LogicalRegion, std::map<FieldID, PhaseBarrier> > inside, outside;
};

// The callback function for registering mappers
void create_mappers(Machine machine, HighLevelRuntime *runtime,
                    const std::set<Processor> &local_procs);

// The top-level task that will be invoked and where
// most of the interesting logical region creation and
// partitioning occurs.
void top_level_task(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, HighLevelRuntime *runtime);

MPIConnectResult connect_mpi_task(const Task *task,
                                  const std::vector<PhysicalRegion> &regions,
                                  Context ctx, HighLevelRuntime *runtime);

void update_mappers_task(const Task *task,
                         const std::vector<PhysicalRegion> &regions,
                         Context ctx, HighLevelRuntime *runtime);

void update_subrank_bounds_task(const Task *task,
                                const std::vector<PhysicalRegion> &regions,
                                Context ctx, HighLevelRuntime *runtime);

void create_ghost_regions_task(const Task *task,
                               const std::vector<PhysicalRegion> &regions,
                               Context ctx, HighLevelRuntime *runtime);

void gather_barriers_task(const Task *task,
                          const std::vector<PhysicalRegion> &regions,
                          Context ctx, HighLevelRuntime *runtime);

void ghost_regions_fixup_task(const Task *task,
                              const std::vector<PhysicalRegion> &regions,
                              Context ctx, HighLevelRuntime *runtime);
                    
#endif
