
#include "rhsf.h"
#include "rhsf_init.h"
#include "rhsf_impl.h"
#include "rhsf_mapper.h"

#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <unistd.h>

// Include Singe emitted header files
#include "getcoeffs.h"
#include "getrates.h"

// Singe header files for SSE
#ifdef USE_SSE_KERNELS
#include "sse_getcoeffs.h"
#include "sse_getrates.h"
#endif

// Singe header files for AVX
#ifdef USE_AVX_KERNELS
#include "avx_getcoeffs.h"
#include "avx_getrates.h"
#endif

// Stencil kernels
#include "cpu_stencils.h"
#ifdef USE_GPU_KERNELS
#include "gpu_stencils.h"
#endif

// Chemistry and physics for the GPU
#ifdef USE_GPU_KERNELS
#include "rhsf_gpu.h"
#endif

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

void create_mappers(Machine machine, HighLevelRuntime *runtime,
                    const std::set<Processor> &local_procs)
{
  S3DRank *rank = S3DRank::get_rank(Point<3>::ZEROES(), false);
  std::vector<FieldID> all_q_fields;
  all_q_fields.push_back(FID_RHO_U);
  all_q_fields.push_back(FID_RHO_V);
  all_q_fields.push_back(FID_RHO_W);
  all_q_fields.push_back(FID_RHO);
  all_q_fields.push_back(FID_RHO_E);
  for (int i = 0; i < (rank->n_spec-1); i++)
    all_q_fields.push_back(FID_RHO_Y(i));
  for (int i = 0; i < rank->n_scalar; i++)
    all_q_fields.push_back(FID_SCALAR(i));

  std::vector<FieldID> all_temp_fields;
  all_temp_fields.push_back(FID_VOLUME);
  all_temp_fields.push_back(FID_VEL_X);
  all_temp_fields.push_back(FID_VEL_Y);
  all_temp_fields.push_back(FID_VEL_Z);
  all_temp_fields.push_back(FID_CPMIX);
  all_temp_fields.push_back(FID_AVMOLWT);
  all_temp_fields.push_back(FID_MIXMW);
  all_temp_fields.push_back(FID_GAMMA);
  all_temp_fields.push_back(FID_PRESSURE);
  all_temp_fields.push_back(FID_SOURCE_TERM);
  all_temp_fields.push_back(FID_TAU_XX);
  all_temp_fields.push_back(FID_TAU_XY);
  all_temp_fields.push_back(FID_TAU_XZ);
  all_temp_fields.push_back(FID_TAU_YY);
  all_temp_fields.push_back(FID_TAU_YZ);
  all_temp_fields.push_back(FID_TAU_ZZ);
  all_temp_fields.push_back(FID_RHS_1_DX_IN);
  all_temp_fields.push_back(FID_RHS_1_DX);
  all_temp_fields.push_back(FID_RHS_1_DY_IN);
  all_temp_fields.push_back(FID_RHS_1_DY);
  all_temp_fields.push_back(FID_RHS_1_DZ_IN);
  all_temp_fields.push_back(FID_RHS_1_DZ);
  all_temp_fields.push_back(FID_RHS_2_DX_IN);
  all_temp_fields.push_back(FID_RHS_2_DX);
  all_temp_fields.push_back(FID_RHS_2_DY_IN);
  all_temp_fields.push_back(FID_RHS_2_DY);
  all_temp_fields.push_back(FID_RHS_2_DZ_IN);
  all_temp_fields.push_back(FID_RHS_2_DZ);
  all_temp_fields.push_back(FID_RHS_3_DX_IN);
  all_temp_fields.push_back(FID_RHS_3_DX);
  all_temp_fields.push_back(FID_RHS_3_DY_IN);
  all_temp_fields.push_back(FID_RHS_3_DY);
  all_temp_fields.push_back(FID_RHS_3_DZ_IN);
  all_temp_fields.push_back(FID_RHS_3_DZ);
  all_temp_fields.push_back(FID_RHS_4_DX);
  all_temp_fields.push_back(FID_RHS_4_DY);
  all_temp_fields.push_back(FID_RHS_4_DZ);
  all_temp_fields.push_back(FID_RHS_5_DX_IN);
  all_temp_fields.push_back(FID_RHS_5_DX);
  all_temp_fields.push_back(FID_RHS_5_DY_IN);
  all_temp_fields.push_back(FID_RHS_5_DY);
  all_temp_fields.push_back(FID_RHS_5_DZ_IN);
  all_temp_fields.push_back(FID_RHS_5_DZ);
  for (int i = 0; i < rank->n_spec; i++)
    for (int d = 0; d < 3; d++) {
      all_temp_fields.push_back(FID_RHS_6_D_IN(i, d));
      all_temp_fields.push_back(FID_RHS_6_D(i, d));
    }
  all_temp_fields.push_back(FID_GRAD_T_X);
  all_temp_fields.push_back(FID_GRAD_T_Y);
  all_temp_fields.push_back(FID_GRAD_T_Z);
  all_temp_fields.push_back(FID_GRAD_MIXMW_X);
  all_temp_fields.push_back(FID_GRAD_MIXMW_Y);
  all_temp_fields.push_back(FID_GRAD_MIXMW_Z);
  all_temp_fields.push_back(FID_HEATFLUX_X);
  all_temp_fields.push_back(FID_HEATFLUX_Y);
  all_temp_fields.push_back(FID_HEATFLUX_Z);
  all_temp_fields.push_back(FID_GRAD_VEL_X_X);
  all_temp_fields.push_back(FID_GRAD_VEL_X_Y);
  all_temp_fields.push_back(FID_GRAD_VEL_X_Z);
  all_temp_fields.push_back(FID_GRAD_VEL_Y_X);
  all_temp_fields.push_back(FID_GRAD_VEL_Y_Y);
  all_temp_fields.push_back(FID_GRAD_VEL_Y_Z);
  all_temp_fields.push_back(FID_GRAD_VEL_Z_X);
  all_temp_fields.push_back(FID_GRAD_VEL_Z_Y);
  all_temp_fields.push_back(FID_GRAD_VEL_Z_Z);
  for (int i = 0; i < rank->n_spec; i++)
    all_temp_fields.push_back(FID_YSPECIES(i));
  for (int i = 0; i < rank->n_spec; i++)
    all_temp_fields.push_back(FID_H_SPEC(i));
#ifndef OVERLAP_FIELDS
  for (int i = 0; i < rank->n_spec; i++)
    all_temp_fields.push_back(FID_GRAD_YS_X(i));
  for (int i = 0; i < rank->n_spec; i++)
    all_temp_fields.push_back(FID_GRAD_YS_Y(i));
  for (int i = 0; i < rank->n_spec; i++)
    all_temp_fields.push_back(FID_GRAD_YS_Z(i));
#endif
  for (int i = 0; i < rank->n_spec; i++) {
    all_temp_fields.push_back(FID_RR(i));
    all_temp_fields.push_back(FID_YDIFFFLUX_X(i));
    all_temp_fields.push_back(FID_YDIFFFLUX_Y(i));
    all_temp_fields.push_back(FID_YDIFFFLUX_Z(i));
  }
#ifdef GETRATES_NEEDS_DIFFUSION
  for (int i = 0; i < rank->n_spec; i++)
    all_temp_fields.push_back(FID_DIFFUSION(i));
  for (int i = 0; i < rank->n_spec; i++)
    for(int d = 0; d < 3; d++)
      all_temp_fields.push_back(FID_DIFF_D(i, d));
#endif
#ifndef DEBUG_SCALAR_DATA_PROBLEM
  for(int i = 0; i < rank->n_scalar; i++)
    for(int d = 0; d < 3; d++)
      all_temp_fields.push_back(FID_GRAD_SCALAR(i,d));
  for(int i = 0; i < rank->n_scalar; i++)
    for(int d = 0; d < 3; d++)
      all_temp_fields.push_back(FID_SCALAR_FLUX(i,d));
  for(int i = 0; i < rank->n_scalar; i++)
    for(int d = 0; d < 3; d++)
      all_temp_fields.push_back(FID_SCALAR_RHS_D_IN(i,d));
  for(int i = 0; i < rank->n_scalar; i++)
    for(int d = 0; d < 3; d++)
      all_temp_fields.push_back(FID_SCALAR_RHS_D(i,d));
#endif

  std::vector<FieldID> all_state_fields;
  all_state_fields.push_back(FID_TEMP);
  all_state_fields.push_back(FID_VISCOSITY);
  all_state_fields.push_back(FID_LAMBDA);
  for (int s = 0; s < rank->n_spec; s++)
    all_state_fields.push_back(FID_DS_MIXAVG(s));
  for (int s = 0; s < rank->n_spec; s++)
    all_state_fields.push_back(FID_RS_THERM_DIFF(s));

  std::vector<FieldID> all_gpu_temp_fields;
  all_gpu_temp_fields.push_back(FID_AVMOLWT);
  all_gpu_temp_fields.push_back(FID_MIXMW);
  all_gpu_temp_fields.push_back(FID_GAMMA);
  all_gpu_temp_fields.push_back(FID_PRESSURE);
  all_gpu_temp_fields.push_back(FID_SOURCE_TERM);
  for (int i = 0; i < rank->n_spec; i++)
    all_gpu_temp_fields.push_back(FID_YSPECIES(i));
#ifdef GETRATES_NEEDS_DIFFUSION
  for (int i = 0; i < rank->n_spec; i++)
    all_gpu_temp_fields.push_back(FID_DIFFUSION(i));
#endif

  // only first node needs to print this
  if(rank->is_first_proc())
    printf("There are %ld q fields, %ld state fields, "
	   "and %ld temp fields\n", 
	   all_q_fields.size(), 
	   all_state_fields.size(),
	   all_temp_fields.size());

  for (std::set<Processor>::const_iterator it = local_procs.begin();
        it != local_procs.end(); it++)
  {
    RHSFMapper *mapper = new RHSFMapper(machine, runtime, *it,
                                        all_q_fields, all_temp_fields,
                                        all_state_fields,
                                        all_gpu_temp_fields,
					S3DRank::get_enable_profiling());
    runtime->replace_default_mapper(mapper, *it);
  }
}

static void print_point(const char *pfx, const Point<3>&p)
{
  printf("%s: (%d,%d,%d)\n", pfx, p[0], p[1], p[2]);
}

static void print_rect(const char *pfx, const Rect<3>&r)
{
  printf("%s: (%d,%d,%d) -> (%d,%d,%d)\n", pfx, r.lo[0], r.lo[1], r.lo[2], r.hi[0], r.hi[1], r.hi[2]);
}

template <typename T>
static void allocate_fields(FieldAllocator& fsa, const std::vector<FieldID>& fields)
{
  for(std::vector<FieldID>::const_iterator it = fields.begin(); it != fields.end(); it++)
    fsa.allocate_field(sizeof(T), *it);
}

static void add_fields(RegionRequirement& rr, const std::vector<FieldID>& fields)
{
  for(std::vector<FieldID>::const_iterator it = fields.begin(); it != fields.end(); it++)
    rr.add_field(*it);
}

static int plusmod(int x, int divisor, int amt = 1)
{
  x += amt;
  if(x >= divisor) x -= divisor;
  return x;
}

static int minusmod(int x, int divisor, int amt = 1)
{
  x -= amt;
  if (x < 0) x += divisor;
  return x;
}

#include <sys/time.h>
#include <sys/resource.h>

static void print_usage(const char *prefix)
{
  struct rusage usage;
  getrusage(RUSAGE_SELF, &usage);
  fprintf(stdout,"MEMORY RESOURCE USAGE at %s: %ld\n", prefix, usage.ru_maxrss);
  fflush(stdout);
}

void top_level_task(const Task *task,
                    const std::vector<PhysicalRegion> &regions,
                    Context ctx, HighLevelRuntime *runtime)
{
  const InputArgs &inputs = HighLevelRuntime::get_input_args();
  char hostname[80];
  gethostname(hostname, 80);
  printf("in top level task: %s (%d)\n", hostname, inputs.argc);
  print_usage("start top-level task");

  S3DRank *rank = S3DRank::get_rank(Point<3>::ZEROES(), false);
  double begin_init_time = Realm::Clock::current_time_in_microseconds();

  // TODO: would like to be able to use fortran 1-based indexing, but blockify needs to be composed
  //   with a translation to make that happen
  Rect<3> sim_grid_bounds(Point<3>::ZEROES(), rank->global_grid_size - Point<3>::ONES());
  Rect<3> dist_grid_bounds(Point<3>::ZEROES(), rank->dist_grid_size - Point<3>::ONES());
  Rect<3> proc_grid_bounds(Point<3>::ZEROES(), rank->proc_grid_size - Point<3>::ONES());
  
  Point<3> dist_blocking_factor = rank->global_grid_size / rank->dist_grid_size;
  Point<3> proc_per_dist = rank->proc_grid_size / rank->dist_grid_size;
  Blockify<3> grid2dist_map(dist_blocking_factor);
  Blockify<3> grid2proc_map(rank->local_grid_size);

  printf("Total simulation volume: %zd\n", sim_grid_bounds.volume());
  IndexSpace is_grid = runtime->create_index_space(ctx, Domain::from_rect<3>(sim_grid_bounds));

  // first partition is on rhsf distribution granularity
  IndexPartition ip_dist = runtime->create_index_partition(ctx, is_grid, grid2dist_map, 0);

  // for each piece of distribution hierachy, split by proc grid
  for(GenericPointInRectIterator<3> pir_d(dist_grid_bounds); pir_d; pir_d++) {
    Rect<3> subgrid_bounds = grid2dist_map.preimage(pir_d.p);
    Rect<3> dist_proc_subgrid = grid2proc_map.image_convex(subgrid_bounds);
    if(0) {
      print_point("pir_d", pir_d.p);
      print_rect("sgb", subgrid_bounds);
      print_rect("dps", dist_proc_subgrid);
    }
    IndexSpace is_subdist = runtime->get_index_subspace<3>(ctx, ip_dist, pir_d.p);

    IndexPartition ip_proc = runtime->create_index_partition(ctx, is_subdist, grid2proc_map, 0);

    // now create x/y/z +/- subpartitions of each block
    for(GenericPointInRectIterator<3> pir(dist_proc_subgrid); pir; pir++) {
      Rect<3> procgrid_bounds = grid2proc_map.preimage(pir.p);
      if(0) {
	print_point("pir", pir.p);
	print_rect("pgb", procgrid_bounds);
      }

      IndexSpace is_procgrid = runtime->get_index_subspace<3>(ctx, ip_proc, pir.p);
      for(int a = 0; a < 3; a++) {
	DomainColoring dc;

	Rect<3> procgrid_lo = procgrid_bounds;
	procgrid_lo.hi.x[a] = procgrid_lo.lo[a] + rank->iorder/2 - 1;
	dc[PCLR_XYZLO] = Domain::from_rect<3>(procgrid_lo);
	
	Rect<3> procgrid_hi = procgrid_bounds;
	procgrid_hi.lo.x[a] = procgrid_hi.hi[a] - rank->iorder/2 + 1;
	dc[PCLR_XYZHI] = Domain::from_rect<3>(procgrid_hi);

	runtime->create_index_partition(ctx, is_procgrid, Domain::from_rect<1>(Rect<1>(PCLR_XYZLO, PCLR_XYZHI)),
					dc, true /* disjoint */, a);
      }
    }
  }

  std::set<Processor> all_processors;
  Machine::get_machine().get_all_processors(all_processors);
#ifndef SHARED_LOWLEVEL
  // Only the shared lowlevel runtime needs to iterate over all points
  // on each processor.
  int num_points = 1;
  int num_procs = 0;
  {
    for(std::set<Processor>::const_iterator it = all_processors.begin(); it != all_processors.end(); it++)
      if(it->kind() == Processor::LOC_PROC)
        num_procs++;
  }
#else
  int num_procs = all_processors.size();
  int num_points = rank->proc_grid_size.x[0] * rank->proc_grid_size.x[1] * rank->proc_grid_size.x[2];
#endif
  printf("Attempting to connect %d processors with %d points per processor\n",
         num_procs, num_points);
  Point<2> all_procs_lo, all_procs_hi;
  all_procs_lo.x[0] = all_procs_lo.x[1] = 0;
  all_procs_hi.x[0] = num_procs - 1;
  all_procs_hi.x[1] = num_points - 1;
  Rect<2> all_procs = Rect<2>(all_procs_lo, all_procs_hi);

  print_usage("before connect mpi");
  double before_connect_time = Realm::Clock::current_time_in_microseconds();
  printf("TIME BEFORE CONNECT: %7.3g ms\n", (before_connect_time-begin_init_time)*1e-3);
  FutureMap connect_mpi_results;
  double after_connect_time;
  {
    ArgumentMap arg_map;
    for(int proc_num = 0; proc_num < num_procs; proc_num++) {
#ifndef SHARED_LOWLEVEL
      Point<3> point = Point<3>::ZEROES();
      int arg_point[2] = {proc_num, 0};
      DomainPoint domain_point = DomainPoint::from_point<3>(point);
      arg_map.set_point_arg<int, 2>(arg_point, TaskArgument(&domain_point, sizeof(domain_point)));
#else
      int point_num = 0;
      for(GenericPointInRectIterator<3> pir(proc_grid_bounds); pir; pir++) {
	Point<3> point = pir.p;
	int arg_point[2] = {proc_num, point_num};
	DomainPoint domain_point = DomainPoint::from_point<3>(point);
	arg_map.set_point_arg<int, 2>(arg_point, TaskArgument(&domain_point, sizeof(domain_point)));
	
	if ((++point_num) >= num_points) {
	  break;
	}
      }
#endif
    }
    IndexLauncher connect_mpi_launcher(CONNECT_MPI_TASK_ID,
                                       Domain::from_rect<2>(all_procs),
                                       TaskArgument(0, 0),
                                       arg_map);

    connect_mpi_results = runtime->execute_index_space(ctx, connect_mpi_launcher);
    connect_mpi_results.wait_all_results();
    printf("connect_mpi finished\n");

    // first, build up a blob containing all the point/proc pairs so everybody's mapper can get updated
    std::map<Point<3>, std::vector<Processor>, Point<3>::STLComparator> procs_by_point;
    size_t num_procs = 0;
    for(GenericPointInRectIterator<2> pir(all_procs); pir; pir++) {
      int point[2] = {pir.p[0], pir.p[1]};
      //printf("getting result for (%d,%d)\n", pir.p[0], pir.p[1]);
      MPIConnectResult result = connect_mpi_results.get_result<MPIConnectResult, int, 2>(point);
      // printf("result for (%d,%d) = %d, %x, (%d, %d, %d), %d\n", pir.p[0], pir.p[1], result.success,
      // 	     result.proc.id, result.point.get_point<3>()[0], result.point.get_point<3>()[1],
      // 	     result.point.get_point<3>()[2], result.subranks_needed);
      if(result.success) {
	std::vector<Processor>& plist = procs_by_point[result.point.get_point<3>()];
	plist.push_back(result.proc);
	num_procs++;
      }
    }
    printf("NUM PROCS=%ld  PROCS BY POINT=%ld\n", num_procs, procs_by_point.size());
    print_usage("after connect mpi");
    after_connect_time = Realm::Clock::current_time_in_microseconds();
    printf("TIME AFTER CONNECT MPI: %7.3g ms\n", (after_connect_time-before_connect_time)*1e-3); 
    // Instead of building a blob and broadcasting it, let's put everything in a region
    // so that we can avoid memory spikes on node zero that is running this initialization
    IndexSpace node_space = runtime->create_index_space(ctx, Domain::from_rect<3>(proc_grid_bounds));
    FieldSpace proc_space = runtime->create_field_space(ctx);
    // Ask the mapper to tell us how many processors per node there are
    // and allocate that many fields in the field space
    int procs_per_node = runtime->get_tunable_value(ctx, TUNABLE_PROCS_PER_NODE);
    FieldAllocator allocator = runtime->create_field_allocator(ctx, proc_space);
    std::vector<FieldID> proc_fields;
    for (int idx = 0; idx < procs_per_node; idx++)
    {
      FieldID fid = allocator.allocate_field(sizeof(Processor));
      proc_fields.push_back(fid);
    }
    // Create the logical region
    LogicalRegion lr_rank_mapping = runtime->create_logical_region(ctx, node_space, proc_space);
    // Do an inline mapping and fill in the region
    {
      InlineLauncher rank_mapping_launcher(RegionRequirement(lr_rank_mapping, READ_WRITE,
                                                             EXCLUSIVE, lr_rank_mapping));
      rank_mapping_launcher.requirement.add_fields(proc_fields);
      PhysicalRegion rank_region = runtime->map_region(ctx, rank_mapping_launcher);
      rank_region.wait_until_valid();
      
      std::vector<RegionAccessor<AccessorType::Generic, Processor> > proc_accessors(procs_per_node);
      for (int idx = 0; idx < procs_per_node; idx++)
        proc_accessors[idx] = rank_region.get_field_accessor(proc_fields[idx]).typeify<Processor>();
      for(std::map<Point<3>, std::vector<Processor> >::const_iterator it = 
           procs_by_point.begin(); it != procs_by_point.end(); it++) 
      {
        const std::vector<Processor>& plist = it->second;
        assert(plist.size() == (size_t)procs_per_node);
        for (int idx = 0; idx < procs_per_node; idx++)
          proc_accessors[idx].write(DomainPoint::from_point<3>(it->first), plist[idx]);
      }
      runtime->unmap_region(ctx, rank_region);
    }
    // Now we've written all the data into the region, so send off the update mappers tasks
    // Instead of launching one for every processor, let's only launch one for every node
    ArgumentMap empty_map;
    int num_nodes = proc_grid_bounds.volume();
    IndexLauncher update_mappers_launcher(UPDATE_MAPPERS_TASK_ID,
                                          Domain::from_rect<1>(Rect<1>(0, num_nodes-1)),
                                          TaskArgument(&proc_grid_bounds, 
                                                       sizeof(proc_grid_bounds)), empty_map);
    RegionRequirement update_req(lr_rank_mapping, READ_ONLY, EXCLUSIVE, lr_rank_mapping);
    update_req.add_fields(proc_fields);
    update_mappers_launcher.add_region_requirement(update_req);
    FutureMap fm_update_mappers = runtime->execute_index_space(ctx, update_mappers_launcher);
    fm_update_mappers.wait_all_results();
    // Now we can clean up our resources
    runtime->destroy_logical_region(ctx, lr_rank_mapping);
    runtime->destroy_index_space(ctx, node_space);
    runtime->destroy_field_space(ctx, proc_space);
    printf("all mappers updated\n");
    print_usage("after update mappers");
  }
  double after_update_mappers_time = Realm::Clock::current_time_in_microseconds();
  printf("TIME AFTER UPDATE MAPPERS: %7.3g ms\n", 
	(after_update_mappers_time - after_connect_time)*1e-3);
  
  ArgumentMap subrank_bounds_map = runtime->create_argument_map(ctx);
  std::set<Point<3>, Point<3>::STLComparator> subrank_partitions_created;
  for(GenericPointInRectIterator<2> pir(all_procs); pir; pir++) {
    int point[2] = {pir.p[0], pir.p[1]};
    //printf("getting result for (%d,%d)\n", pir.p[0], pir.p[1]);
    MPIConnectResult result = connect_mpi_results.get_result<MPIConnectResult, int, 2>(point);
    // printf("result for (%d,%d) = %d, %x, (%d, %d, %d), %d\n", pir.p[0], pir.p[1], result.success,
    // 	   result.proc.id, result.point.get_point<3>()[0], result.point.get_point<3>()[1],
    // 	   result.point.get_point<3>()[2], result.subranks_needed);
    if (result.success) {
      if(result.subranks_needed > 0) {
	Rect<3> subgrid_bounds = grid2proc_map.preimage(result.point.get_point<3>());
	Rect<3> dist_point = grid2dist_map.image_convex(subgrid_bounds);
	assert(dist_point.lo == dist_point.hi);

	IndexSpace is_subdist = runtime->get_index_subspace<3>(ctx, ip_dist, dist_point.lo);
	IndexPartition ip_proc = runtime->get_index_partition(ctx, is_subdist, 0);
	IndexSpace is_subgrid = runtime->get_index_subspace<3>(ctx, ip_proc, result.point.get_point<3>());

	Rect<3> *subrank_bounds = new Rect<3>[result.subranks_needed];

	DomainColoring subrank_coloring;

	int split_dim = 2;  // should be 2 for Fortran-ordered arrays, 0 for C
	int num_planes = subgrid_bounds.hi[split_dim] - subgrid_bounds.lo[split_dim] + 1;
	  
	for(int s = 0; s < result.subranks_needed; s++) {
	  // calculating start and end this way spreads out "extra" rows evenly
	  int subrank_start = int((num_planes * s) / result.subranks_needed);
	  int subrank_end = int((num_planes * (s + 1)) / result.subranks_needed) - 1;

	  subrank_bounds[s] = subgrid_bounds;
	  subrank_bounds[s].lo.x[split_dim] = subgrid_bounds.lo[split_dim] + subrank_start;
	  subrank_bounds[s].hi.x[split_dim] = subgrid_bounds.lo[split_dim] + subrank_end;

	  subrank_coloring[s] = Domain::from_rect<3>(subrank_bounds[s]);
	}

	if(subrank_partitions_created.find(result.point.get_point<3>()) == subrank_partitions_created.end()) {
	  runtime->create_index_partition(ctx, is_subgrid, 
					  Domain::from_rect<1>(Rect<1>(0, result.subranks_needed - 1)),
					  subrank_coloring,
					  true /*disjoint*/,
					  PID_SUBRANK);
	  subrank_partitions_created.insert(result.point.get_point<3>());
	}

	subrank_bounds_map.set_point_arg<int, 3>(result.point.get_point<3>().x,
						 TaskArgument(subrank_bounds, result.subranks_needed * sizeof(Rect<3>)));
        delete [] subrank_bounds;
      }
    }
  }
  
  print_usage("before update subrank bounds");
  double before_subrank_bounds_time = Realm::Clock::current_time_in_microseconds();
  printf("TIME BEFORE UPDATE SUBRANK BOUNDS: %7.3g ms\n", 
	(before_subrank_bounds_time - after_update_mappers_time)*1e-3);
  // use an index space launch to push the computed subrank bounds to the actual ranks
  {
    IndexLauncher bounds_launcher(UPDATE_SUBRANK_BOUNDS_TASK_ID,
                                  Domain::from_rect<3>(proc_grid_bounds),
                                  TaskArgument(0, 0),
                                  subrank_bounds_map);
    // Each point task must go to the corresponding rank so set the flag appropriately
    bounds_launcher.tag = RHSF_MAPPER_FORCE_RANK_MATCH;
    FutureMap fm = runtime->execute_index_space(ctx, bounds_launcher);
    fm.wait_all_results();
  }
  print_usage("after update subrank bounds");
  double after_subrank_bounds_time = Realm::Clock::current_time_in_microseconds();
  printf("TIME AFTER UPDATE SUBRANK BOUNDS: %7.3g ms\n",
        (after_subrank_bounds_time - before_subrank_bounds_time)*1e-3); 

  FieldSpace fs_ghost;
  LogicalRegion lr_ghost[3];
  std::vector<FieldID> ghost_fields;
  std::map<Point<3>, std::vector<LogicalRegion>, Point<3>::STLComparator> ghost_regions;
  if (S3DRank::get_explicit_ghost_cells()) {
    ghost_fields.push_back(FID_GHOST_VEL_X);
    ghost_fields.push_back(FID_GHOST_VEL_Y);
    ghost_fields.push_back(FID_GHOST_VEL_Z);
    ghost_fields.push_back(FID_GHOST_T);
    ghost_fields.push_back(FID_GHOST_MIXMW);
    ghost_fields.push_back(FID_GHOST_RHS1);
    ghost_fields.push_back(FID_GHOST_RHS2);
    ghost_fields.push_back(FID_GHOST_RHS3);
    ghost_fields.push_back(FID_GHOST_RHS4);
    ghost_fields.push_back(FID_GHOST_RHS5);
    for(int s = 0; s < rank->n_spec; s++)
      ghost_fields.push_back(FID_GHOST_YSPEC(s));
    for(int s = 0; s < rank->n_spec; s++)
      ghost_fields.push_back(FID_GHOST_YDIFF(s));
    for(int s = 0; s < rank->n_spec - 1; s++)
      ghost_fields.push_back(FID_GHOST_RHS6(s));
    for(int i = 0; i < rank->n_scalar; i++)
      ghost_fields.push_back(FID_GHOST_SCALAR(i));
    for(int i = 0; i < rank->n_scalar; i++)
      ghost_fields.push_back(FID_GHOST_SCALAR_RHS(i));

    fs_ghost = runtime->create_field_space(ctx);
    FieldAllocator fsa_ghost = runtime->create_field_allocator(ctx, fs_ghost);
    allocate_fields<double>(fsa_ghost, ghost_fields);

    for(int i = 0; i < 3; i++)
      if(rank->vary_in_dims[i])
	lr_ghost[i] = runtime->create_logical_region(ctx, is_grid, fs_ghost);
      else
	lr_ghost[i] = LogicalRegion::NO_REGION;

#ifndef REMOVE_OLD_BARRIER_EXCHANGE
    // have each rank create any ghost regions and necessary phase barriers
    std::map<Point<3>, Future, Point<3>::STLComparator> cgr_futures;
    for(GenericPointInRectIterator<3> pir(proc_grid_bounds); pir; pir++) {

      TaskLauncher ghost_launcher(CREATE_GHOST_REGIONS_TASK_ID,
                                  TaskArgument(0, 0));
      ghost_launcher.point = DomainPoint::from_point<3>(pir.p);
      // Guarantee this gets sent to the correct processors
      ghost_launcher.tag = RHSF_MAPPER_FORCE_RANK_MATCH;
      Point<3> p_dist;
      for(int i = 0; i < 3; i++)
	p_dist.x[i] = pir.p[i] * rank->proc_grid_size[i] / rank->dist_grid_size[i];

      for(int i = 0; i < 3; i++)
	if(rank->vary_in_dims[i]) {
	  int relproc = pir.p[i] % proc_per_dist[i];

	  if((relproc > 0) && (relproc < (proc_per_dist[i] - 1)))
	    continue;
	
	  if(relproc == 0) {
	    // we nee the HI of the processor below us
	    Point<3> p_lo(pir.p);
	    p_lo.x[i] = minusmod(p_lo.x[i], rank->proc_grid_size[i]);

	    IndexSpace is_mdist = runtime->get_index_subspace<3>(ctx, ip_dist, p_lo / proc_per_dist);
	    IndexPartition ip_mproc = runtime->get_index_partition(ctx, is_mdist, 0);
	    IndexSpace is_mproc = runtime->get_index_subspace<3>(ctx, ip_mproc, p_lo);
	    IndexPartition ip_mdir = runtime->get_index_partition(ctx, is_mproc, PID_X + i);
	    IndexSpace is_minus = runtime->get_index_subspace(ctx, ip_mdir, PCLR_XYZHI);

#define NODEBUG_GHOST_CELLS
#ifdef DEBUG_GHOST_CELLS
	    printf("CHECK: %d LO\n", i);
	    print_point("pir.p", pir.p);
	    print_rect("domain", runtime->get_index_space_domain(ctx, is_minus).get_rect<3>());
#endif

	    LogicalRegion lr_g_minus = runtime->get_logical_subregion_by_tree(ctx,
									      is_minus,
									      fs_ghost,
									      lr_ghost[i].get_tree_id());

#ifdef DEBUG_GHOST_CELLS
	    printf("%c minus: (%d,%d,%d) = %x."IDFMT"\n", 'x'+i, pir.p[0], pir.p[1], pir.p[2],
		   lr_g_minus.get_tree_id(), lr_g_minus.get_index_space().id);
#endif
	    RegionRequirement rr(lr_g_minus, READ_WRITE, EXCLUSIVE, lr_ghost[i]);
	    add_fields(rr, ghost_fields);
	    ghost_launcher.add_region_requirement(rr);
	  }

	  if(relproc == (proc_per_dist[i] - 1)) {
	    // we nee the LO of the processor above us
	    Point<3> p_hi(pir.p);
	    p_hi.x[i] = plusmod(p_hi.x[i], rank->proc_grid_size[i]);

	    IndexSpace is_pdist = runtime->get_index_subspace<3>(ctx, ip_dist, p_hi / proc_per_dist);
	    IndexPartition ip_pproc = runtime->get_index_partition(ctx, is_pdist, 0);
	    IndexSpace is_pproc = runtime->get_index_subspace<3>(ctx, ip_pproc, p_hi);
	    IndexPartition ip_pdir = runtime->get_index_partition(ctx, is_pproc, PID_X + i);
	    IndexSpace is_plus = runtime->get_index_subspace(ctx, ip_pdir, PCLR_XYZLO);

#ifdef DEBUG_GHOST_CELLS
	    printf("CHECK: %d HI\n", i);
	    print_point("pir.p", pir.p);
	    print_rect("domain", runtime->get_index_space_domain(ctx, is_plus).get_rect<3>());
#endif

	    LogicalRegion lr_g_plus = runtime->get_logical_subregion_by_tree(ctx,
									     is_plus,
									     fs_ghost,
									     lr_ghost[i].get_tree_id());

#ifdef DEBUG_GHOST_CELLS
	    printf("%c plus: (%d,%d,%d) = %x."IDFMT"\n", 'x'+i, pir.p[0], pir.p[1], pir.p[2],
		   lr_g_plus.get_tree_id(), lr_g_plus.get_index_space().id);
#endif
	    RegionRequirement rr(lr_g_plus, READ_WRITE, EXCLUSIVE, lr_ghost[i]);
	    add_fields(rr, ghost_fields);
	    ghost_launcher.add_region_requirement(rr);
	  }
	}

      // no need to run this for interior processors
      if (ghost_launcher.region_requirements.empty()) continue;

      Future f = runtime->execute_task(ctx, ghost_launcher);
      cgr_futures[pir.p] = f;
    }

    // now wait on all of them
    for(std::map<Point<3>, Future, Point<3>::STLComparator>::iterator it = cgr_futures.begin();
	it != cgr_futures.end();
	it++) {
      //const Point<3>& p = it->first;
      Future f = it->second;

      f.get_void_result();
    }

    print_usage("after create ghost regions");
    double after_create_ghost_regions_time = Realm::Clock::current_time_in_microseconds();
    printf("TIME AFTER CREATE GHOST REGIONS: %7.3g ms\n",
	  (after_create_ghost_regions_time - after_subrank_bounds_time)*1e-3);
    // build the list of ghost regions used by each distribution node
    {
      for(GenericPointInRectIterator<3> pir_d(dist_grid_bounds); pir_d; pir_d++) {
	//IndexSpace is_subdist = runtime->get_index_subspace<3>(ctx, ip_dist, pir_d.p);
	//IndexPartition ip_proc = runtime->get_index_partition(ctx, is_subdist, 0);
	Rect<3> subgrid_bounds = grid2dist_map.preimage(pir_d.p);
	Rect<3> dist_proc_subgrid = grid2proc_map.image_convex(subgrid_bounds);

	int num_ghost_regions = 0;
	// 2 regions for each point on edge, so 4 regions for each point on planes
	for(int i = 0; i < 3; i++)
	  if(rank->vary_in_dims[i])
	    num_ghost_regions += (4 * 
				  (dist_proc_subgrid.hi[(i+1)%3] - dist_proc_subgrid.lo[(i+1)%3] + 1) * 
				  (dist_proc_subgrid.hi[(i+2)%3] - dist_proc_subgrid.lo[(i+2)%3] + 1));

	std::vector<LogicalRegion> &regions = ghost_regions[pir_d.p];
	regions.resize(num_ghost_regions);
	std::vector<LogicalRegion>::iterator lrptr = regions.begin();

	for(int dir = 0; dir < 3; dir++)
	  if(rank->vary_in_dims[dir]) {
	    // triple nested loops, but don't iterate in the current direction
	    Rect<3> rect = dist_proc_subgrid;
	    rect.hi.x[dir] = rect.lo[dir];
	
	    for(GenericPointInRectIterator<3> pir2(rect); pir2; pir2++) {
	      Point<3> p2 = pir2.p;

	      // first my d_minus
	      p2.x[dir] = dist_proc_subgrid.lo[dir];
	      {
		IndexSpace is_mydist = runtime->get_index_subspace<3>(ctx, ip_dist, p2 / proc_per_dist);
		IndexPartition ip_myproc = runtime->get_index_partition(ctx, is_mydist, 0);
		IndexSpace is_myproc = runtime->get_index_subspace<3>(ctx, ip_myproc, p2);
		IndexPartition ip_mydir = runtime->get_index_partition(ctx, is_myproc, PID_X + dir);
		IndexSpace is_myghost = runtime->get_index_subspace(ctx, ip_mydir, PCLR_XYZLO);

		LogicalRegion r = runtime->get_logical_subregion_by_tree(ctx,
									 is_myghost,
									 fs_ghost,
									 lr_ghost[dir].get_tree_id());
#ifdef DEBUG_GHOST_CELLS
		printf("CHECK: %d my dminus\n", dir);
		print_point("pir2.p", pir2.p);
		print_point("p2", p2);
		print_rect("domain", runtime->get_index_space_domain(ctx, is_myghost).get_rect<3>());

		printf("zz: (%d, %d, %d) -> %x."IDFMT"\n",
		       p2.x[0], p2.x[1], p2.x[2], r.get_tree_id(), r.get_index_space().id);
#endif
		assert(r != LogicalRegion::NO_REGION);
		*lrptr++ = r;
	      }

	      // now my negative neighbor's d_plus
	      p2.x[dir] = minusmod(dist_proc_subgrid.lo[dir], rank->proc_grid_size[dir]);
	      {
		IndexSpace is_mydist = runtime->get_index_subspace<3>(ctx, ip_dist, p2 / proc_per_dist);
		IndexPartition ip_myproc = runtime->get_index_partition(ctx, is_mydist, 0);
		IndexSpace is_myproc = runtime->get_index_subspace<3>(ctx, ip_myproc, p2);
		IndexPartition ip_mydir = runtime->get_index_partition(ctx, is_myproc, PID_X + dir);
		IndexSpace is_myghost = runtime->get_index_subspace(ctx, ip_mydir, PCLR_XYZHI);

		LogicalRegion r = runtime->get_logical_subregion_by_tree(ctx,
									 is_myghost,
									 fs_ghost,
									 lr_ghost[dir].get_tree_id());
#ifdef DEBUG_GHOST_CELLS
		printf("CHECK: %d neighbor dminus\n", dir);
		print_point("pir2.p", pir2.p);
		print_point("p2", p2);
		print_rect("domain", runtime->get_index_space_domain(ctx, is_myghost).get_rect<3>());

		printf("zz: (%d, %d, %d) -> %x."IDFMT"\n",
		       p2.x[0], p2.x[1], p2.x[2], r.get_tree_id(), r.get_index_space().id);
#endif
		assert(r != LogicalRegion::NO_REGION);
		*lrptr++ = r;
	      }

	      // then my d_plus
	      p2.x[dir] = dist_proc_subgrid.hi[dir];
	      {
		IndexSpace is_mydist = runtime->get_index_subspace<3>(ctx, ip_dist, p2 / proc_per_dist);
		IndexPartition ip_myproc = runtime->get_index_partition(ctx, is_mydist, 0);
		IndexSpace is_myproc = runtime->get_index_subspace<3>(ctx, ip_myproc, p2);
		IndexPartition ip_mydir = runtime->get_index_partition(ctx, is_myproc, PID_X + dir);
		IndexSpace is_myghost = runtime->get_index_subspace(ctx, ip_mydir, PCLR_XYZHI);
		
		LogicalRegion r = runtime->get_logical_subregion_by_tree(ctx,
									 is_myghost,
									 fs_ghost,
									 lr_ghost[dir].get_tree_id());
#ifdef DEBUG_GHOST_CELLS
		printf("CHECK: %d my dplus\n", dir);
		print_point("pir2.p", pir2.p);
		print_point("p2", p2);
		print_rect("domain", runtime->get_index_space_domain(ctx, is_myghost).get_rect<3>());

		printf("zz: (%d, %d, %d) -> %x."IDFMT"\n",
		       p2.x[0], p2.x[1], p2.x[2], r.get_tree_id(), r.get_index_space().id);
#endif
		assert(r != LogicalRegion::NO_REGION);
		*lrptr++ = r;
	      }

	      // finally, my positive neighbor's d_minus
	      p2.x[dir] = plusmod(dist_proc_subgrid.hi[dir], rank->proc_grid_size[dir]);
	      {
		IndexSpace is_mydist = runtime->get_index_subspace<3>(ctx, ip_dist, p2 / proc_per_dist);
		IndexPartition ip_myproc = runtime->get_index_partition(ctx, is_mydist, 0);
		IndexSpace is_myproc = runtime->get_index_subspace<3>(ctx, ip_myproc, p2);
		IndexPartition ip_mydir = runtime->get_index_partition(ctx, is_myproc, PID_X + dir);
		IndexSpace is_myghost = runtime->get_index_subspace(ctx, ip_mydir, PCLR_XYZLO);
		
		LogicalRegion r = runtime->get_logical_subregion_by_tree(ctx,
									 is_myghost,
									 fs_ghost,
									 lr_ghost[dir].get_tree_id());
#ifdef DEBUG_GHOST_CELLS
		printf("CHECK: %d neighbor dplus\n", dir);
		print_point("pir2.p", pir2.p);
		print_point("p2", p2);
		print_rect("domain", runtime->get_index_space_domain(ctx, is_myghost).get_rect<3>());
		
		printf("zz: (%d, %d, %d) -> %x."IDFMT"\n",
		       p2.x[0], p2.x[1], p2.x[2], r.get_tree_id(), r.get_index_space().id);
#endif
		assert(r != LogicalRegion::NO_REGION);
		*lrptr++ = r;
	      }
	    }
	  }

	// should have exactly filled our list
	assert(lrptr == regions.end());
      }
    }

    print_usage("before gather barriers");
    double before_gather_barriers_time = Realm::Clock::current_time_in_microseconds();
    printf("TIME BEFORE GATHER BARRIERS: %7.3g ms\n",
	  (before_gather_barriers_time - after_create_ghost_regions_time)*1e-3);
    // now we need each distribution node to collect the barriers it will use for the rest of the sim
    {
      std::vector<Future> gather_futures;
      for(GenericPointInRectIterator<3> pir_d(dist_grid_bounds); pir_d; pir_d++) {
	//IndexSpace is_subdist = runtime->get_index_subspace<3>(ctx, ip_dist, pir_d.p);
	//IndexPartition ip_proc = runtime->get_index_partition(ctx, is_subdist, 0);
	Rect<3> subgrid_bounds = grid2dist_map.preimage(pir_d.p);
	Rect<3> dist_proc_subgrid = grid2proc_map.image_convex(subgrid_bounds);

	// get our list of ghost regions
	std::vector<LogicalRegion>& regions = ghost_regions[pir_d.p];

        TaskLauncher gather_launcher(GATHER_BARRIERS_TASK_ID,
                                     TaskArgument(0, 0));
        gather_launcher.tag = RHSF_MAPPER_FORCE_RANK_MATCH;
        gather_launcher.point = DomainPoint::from_point<3>(dist_proc_subgrid.lo);

	size_t num_ghost_regions[3];
	for(int i = 0; i < 3; i++)
	  if(rank->vary_in_dims[i])
	    num_ghost_regions[i] = (4 * 
				    (dist_proc_subgrid.hi[(i+1)%3] - dist_proc_subgrid.lo[(i+1)%3] + 1) * 
				    (dist_proc_subgrid.hi[(i+2)%3] - dist_proc_subgrid.lo[(i+2)%3] + 1));
	  else
	    num_ghost_regions[i] = 0;

	// need read access on every ghost regions
	std::vector<LogicalRegion>::const_iterator lrptr = regions.begin();

	for(int dir = 0; dir < 3; dir++)
	  for(size_t i = 0; i < num_ghost_regions[dir]; i++) {
	    RegionRequirement rr(*lrptr++, READ_ONLY, EXCLUSIVE, lr_ghost[dir]);
	    add_fields(rr, ghost_fields);
            gather_launcher.add_region_requirement(rr);
	  }

	assert(lrptr == regions.end());

        Future f = runtime->execute_task(ctx, gather_launcher);
	gather_futures.push_back(f);
      }

      while(gather_futures.size()) {
	Future f = gather_futures.back();
	gather_futures.pop_back();
	f.get_void_result();
      }
    }
    print_usage("after gather barriers");
    double after_gather_barriers_time = Realm::Clock::current_time_in_microseconds();
    printf("TIME AFTER GATHER BARRIERS: %7.3g ms\n", 
	   (after_gather_barriers_time - before_gather_barriers_time)*1e-3);
#endif
  }

#ifdef NEW_BARRIER_EXCHANGE
  PhaseBarrier barrier_exchange_barrier = runtime->create_phase_barrier(ctx,
									proc_grid_bounds.volume());
#endif

  // for imposing a pressure trace, we'll need to do global reduction sums - so create that
  //   collective here
  const double zero = 0.0;
  DynamicCollective global_sum_collective = runtime->create_dynamic_collective(ctx,
									       proc_grid_bounds.volume(),
									       REDOP_SUM_DOUBLE,
									       &zero, sizeof(zero));

#ifdef USE_CEMA
  IndexSpace is_cema = IndexSpace::NO_SPACE;
  //IndexPartition ip_cema_dist;
  FieldSpace fs_cema = FieldSpace::NO_SPACE;
  LogicalRegion lr_cema_global = LogicalRegion::NO_REGION;
  PhaseBarrier cema_ready_barrier, cema_done_barrier;
  if(rank->cema_samples_per_rank) {
    // CEMA samples are laid out in a 3D grid to better match how everything else is decomposed
    // each rank will own an Nx1x1 piece of the sample grid
    Point<3> cema_grid_size(rank->proc_grid_size);
    cema_grid_size.x[0] *= rank->cema_samples_per_rank;

    Rect<3> cema_sample_bounds(Point<3>::ZEROES(), cema_grid_size - Point<3>::ONES());
  
    is_cema = runtime->create_index_space(ctx, Domain::from_rect<3>(cema_sample_bounds));

    fs_cema = runtime->create_field_space(ctx);
    {
      FieldAllocator fsa_cema = runtime->create_field_allocator(ctx, fs_cema);
      // fsa_cema.allocate_field(sizeof(double), FID_TEMP);
      // fsa_cema.allocate_field(sizeof(double), FID_PRESSURE);
      // for(int s = 0; s < rank->n_spec; s++)
      //   fsa_cema.allocate_field(sizeof(double), FID_YSPECIES(s));
      fsa_cema.allocate_field(sizeof(double), FID_TEXP);
      fsa_cema.allocate_field(sizeof(int), FID_SAMPLE_X);
      fsa_cema.allocate_field(sizeof(int), FID_SAMPLE_Y);
      fsa_cema.allocate_field(sizeof(int), FID_SAMPLE_Z);
    }

    lr_cema_global = runtime->create_logical_region(ctx, is_cema, fs_cema);

    cema_ready_barrier = runtime->create_phase_barrier(ctx, proc_grid_bounds.volume());
    cema_done_barrier = runtime->create_phase_barrier(ctx, 1);
    // the first generation of the done barrier is triggered here since a timestep does:
    //  wait, advance, trigger
    cema_done_barrier.arrive(1);
  }
#endif

  // Do some timing on the execution of this function
  double start_time = Realm::Clock::current_time_in_microseconds();

  // distribute rhsf top-level across multiple nodes
  // std::vector<Future> dist_futures;
  MustEpochLauncher spmd_launcher;
  for(GenericPointInRectIterator<3> pir_d(dist_grid_bounds); pir_d; pir_d++) {
    IndexSpace is_subdist = runtime->get_index_subspace<3>(ctx, ip_dist, pir_d.p);
    IndexPartition ip_proc = runtime->get_index_partition(ctx, is_subdist, 0);
    Rect<3> subgrid_bounds = grid2dist_map.preimage(pir_d.p);
    Rect<3> dist_proc_subgrid = grid2proc_map.image_convex(subgrid_bounds);

    int num_ghost_regions = 0;
    if (S3DRank::get_explicit_ghost_cells()) {
      // 2 regions for each point on edge, so 4 regions for each point on planes
      num_ghost_regions += 4 * (dist_proc_subgrid.hi[1] - dist_proc_subgrid.lo[1] + 1) * (dist_proc_subgrid.hi[2] - dist_proc_subgrid.lo[2] + 1);
      num_ghost_regions += 4 * (dist_proc_subgrid.hi[0] - dist_proc_subgrid.lo[0] + 1) * (dist_proc_subgrid.hi[2] - dist_proc_subgrid.lo[2] + 1);
      num_ghost_regions += 4 * (dist_proc_subgrid.hi[0] - dist_proc_subgrid.lo[0] + 1) * (dist_proc_subgrid.hi[1] - dist_proc_subgrid.lo[1] + 1);
    }

    size_t args_size = sizeof(RhsfDistributeArgs) + num_ghost_regions * sizeof(LogicalRegion);
    RhsfDistributeArgs *args = (RhsfDistributeArgs *)malloc(args_size);

    args->dist_proc_pt = dist_proc_subgrid.lo;
    args->is_grid = is_grid;
    args->is_subdist = is_subdist;
    args->ip_proc = ip_proc;
    args->dist_proc_subgrid = dist_proc_subgrid;
    args->full_proc_subgrid = proc_grid_bounds;
    args->global_sum_collective = global_sum_collective;
#ifdef NEW_BARRIER_EXCHANGE
    args->barrier_exchange_barrier = barrier_exchange_barrier;
#endif
    args->fs_ghost = fs_ghost;
    //for(int i = 0; i < 3; i++)
    //  args->lr_ghost[i] = lr_ghost[i];
#ifdef USE_CEMA
    args->lr_cema_global = lr_cema_global;
    args->is_cema = is_cema;
    args->cema_ready_barrier = cema_ready_barrier;
    args->cema_done_barrier = cema_done_barrier;
#endif

    TaskLauncher distribute_launcher(DISTRIBUTE_TASK_ID,
                                     TaskArgument(args, args_size));
    distribute_launcher.point = DomainPoint::from_point<3>(dist_proc_subgrid.lo);
    distribute_launcher.tag = RHSF_MAPPER_FORCE_RANK_MATCH;

    // Mark that we have an index space requirement on our sub index space
    // which guarantees that we have all of the needed parts of the index
    // space on whatever node we get sent to.
    distribute_launcher.add_index_requirement(IndexSpaceRequirement(is_subdist, ALL_MEMORY, is_grid)); 

    if (S3DRank::get_explicit_ghost_cells()) {
      // build up the list of ghost regions that border this distribution subgrid
      LogicalRegion *lrptr = args->lr_ghost;
      
      for(int dir = 0; dir < 3; dir++)
	if(rank->vary_in_dims[dir]) {
	  // triple nested loops, but don't iterate in the current direction
	  Rect<3> rect = dist_proc_subgrid;
	  rect.hi.x[dir] = rect.lo[dir];
	
	  for(GenericPointInRectIterator<3> pir2(rect); pir2; pir2++) {
	    Point<3> p2 = pir2.p;

	    // first my d_minus
	    p2.x[dir] = dist_proc_subgrid.lo[dir];
	    {
	      IndexSpace is_mydist = runtime->get_index_subspace<3>(ctx, ip_dist, p2 / proc_per_dist);
	      IndexPartition ip_myproc = runtime->get_index_partition(ctx, is_mydist, 0);
	      IndexSpace is_myproc = runtime->get_index_subspace<3>(ctx, ip_myproc, p2);
	      IndexPartition ip_mydir = runtime->get_index_partition(ctx, is_myproc, PID_X + dir);
	      IndexSpace is_myghost = runtime->get_index_subspace(ctx, ip_mydir, PCLR_XYZLO);
	      
	      LogicalRegion r = runtime->get_logical_subregion_by_tree(ctx,
								       is_myghost,
								       fs_ghost,
								       lr_ghost[dir].get_tree_id());
#ifdef DEBUG_GHOST_CELLS
	      printf("CHECK: %d my dminus\n", dir);
	      print_point("pir2.p", pir2.p);
	      print_point("p2", p2);
	      print_rect("domain", runtime->get_index_space_domain(ctx, is_myghost).get_rect<3>());
	      
	      printf("zz: (%d, %d, %d) -> %x."IDFMT"\n",
		     p2.x[0], p2.x[1], p2.x[2], r.get_tree_id(), r.get_index_space().id);
#endif
	      assert(r != LogicalRegion::NO_REGION);
	      *lrptr++ = r;

	      RegionRequirement rr(r, READ_WRITE, SIMULTANEOUS, lr_ghost[dir]);
	      rr.tag = RHSF_GHOST_PREV(dir);
	      rr.flags |= NO_ACCESS_FLAG; // remote instance ok - just copies
	      add_fields(rr, ghost_fields);
	      distribute_launcher.add_region_requirement(rr);
	    }
	    
	    // now my negative neighbor's d_plus (unless the neighbor is me)
	    if(rank->proc_grid_size[dir] > 1) {
	      p2.x[dir] = minusmod(dist_proc_subgrid.lo[dir], rank->proc_grid_size[dir]);
	      {
		IndexSpace is_mydist = runtime->get_index_subspace<3>(ctx, ip_dist, p2 / proc_per_dist);
		IndexPartition ip_myproc = runtime->get_index_partition(ctx, is_mydist, 0);
		IndexSpace is_myproc = runtime->get_index_subspace<3>(ctx, ip_myproc, p2);
		IndexPartition ip_mydir = runtime->get_index_partition(ctx, is_myproc, PID_X + dir);
		IndexSpace is_myghost = runtime->get_index_subspace(ctx, ip_mydir, PCLR_XYZHI);
	      
		LogicalRegion r = runtime->get_logical_subregion_by_tree(ctx,
									 is_myghost,
									 fs_ghost,
									 lr_ghost[dir].get_tree_id());
#ifdef DEBUG_GHOST_CELLS
		printf("CHECK: %d neighbor dminus\n", dir);
		print_point("pir2.p", pir2.p);
		print_point("p2", p2);
		print_rect("domain", runtime->get_index_space_domain(ctx, is_myghost).get_rect<3>());
	      
		printf("zz: (%d, %d, %d) -> %x."IDFMT"\n",
		       p2.x[0], p2.x[1], p2.x[2], r.get_tree_id(), r.get_index_space().id);
#endif
		assert(r != LogicalRegion::NO_REGION);
		*lrptr++ = r;

		RegionRequirement rr(r, READ_WRITE, SIMULTANEOUS, lr_ghost[dir]);
		rr.tag = RHSF_GHOST_LOCAL;
		add_fields(rr, ghost_fields);
		distribute_launcher.add_region_requirement(rr);
	      }
	    }
	    
	    // then my d_plus
	    p2.x[dir] = dist_proc_subgrid.hi[dir];
	    {
	      IndexSpace is_mydist = runtime->get_index_subspace<3>(ctx, ip_dist, p2 / proc_per_dist);
	      IndexPartition ip_myproc = runtime->get_index_partition(ctx, is_mydist, 0);
	      IndexSpace is_myproc = runtime->get_index_subspace<3>(ctx, ip_myproc, p2);
	      IndexPartition ip_mydir = runtime->get_index_partition(ctx, is_myproc, PID_X + dir);
	      IndexSpace is_myghost = runtime->get_index_subspace(ctx, ip_mydir, PCLR_XYZHI);
	      
	      LogicalRegion r = runtime->get_logical_subregion_by_tree(ctx,
								       is_myghost,
								       fs_ghost,
								       lr_ghost[dir].get_tree_id());
#ifdef DEBUG_GHOST_CELLS
	      printf("CHECK: %d my dplus\n", dir);
	      print_point("pir2.p", pir2.p);
	      print_point("p2", p2);
	      print_rect("domain", runtime->get_index_space_domain(ctx, is_myghost).get_rect<3>());
	      
	      printf("zz: (%d, %d, %d) -> %x."IDFMT"\n",
		     p2.x[0], p2.x[1], p2.x[2], r.get_tree_id(), r.get_index_space().id);
#endif
	      assert(r != LogicalRegion::NO_REGION);
	      *lrptr++ = r;

	      RegionRequirement rr(r, READ_WRITE, SIMULTANEOUS, lr_ghost[dir]);
	      rr.tag = RHSF_GHOST_NEXT(dir);
	      rr.flags |= NO_ACCESS_FLAG; // remote instance ok - just copies
	      add_fields(rr, ghost_fields);
	      distribute_launcher.add_region_requirement(rr);
	    }

	    // finally, my positive neighbor's d_minus (unless the neighbor is me)
	    if(rank->proc_grid_size[dir] > 1) {
	      p2.x[dir] = plusmod(dist_proc_subgrid.hi[dir], rank->proc_grid_size[dir]);
	      {
		IndexSpace is_mydist = runtime->get_index_subspace<3>(ctx, ip_dist, p2 / proc_per_dist);
		IndexPartition ip_myproc = runtime->get_index_partition(ctx, is_mydist, 0);
		IndexSpace is_myproc = runtime->get_index_subspace<3>(ctx, ip_myproc, p2);
		IndexPartition ip_mydir = runtime->get_index_partition(ctx, is_myproc, PID_X + dir);
		IndexSpace is_myghost = runtime->get_index_subspace(ctx, ip_mydir, PCLR_XYZLO);
	      
		LogicalRegion r = runtime->get_logical_subregion_by_tree(ctx,
									 is_myghost,
									 fs_ghost,
									 lr_ghost[dir].get_tree_id());
#ifdef DEBUG_GHOST_CELLS
		printf("CHECK: %d neighbor dplus\n", dir);
		print_point("pir2.p", pir2.p);
		print_point("p2", p2);
		print_rect("domain", runtime->get_index_space_domain(ctx, is_myghost).get_rect<3>());
	      
		printf("zz: (%d, %d, %d) -> %x."IDFMT"\n",
		       p2.x[0], p2.x[1], p2.x[2], r.get_tree_id(), r.get_index_space().id);
#endif
		assert(r != LogicalRegion::NO_REGION);
		*lrptr++ = r;

		RegionRequirement rr(r, READ_WRITE, SIMULTANEOUS, lr_ghost[dir]);
		rr.tag = RHSF_GHOST_LOCAL;
		add_fields(rr, ghost_fields);
		distribute_launcher.add_region_requirement(rr);
	      }
	    }
	  }
	}

#ifdef DEBUG_GHOST_CELLS
      printf("(%d,%d,%d):", pir_d.p[0], pir_d.p[1], pir_d.p[2]);
      for(size_t i = 0; i < num_ghost_regions; i++)
	printf(" %x."IDFMT"",
	       args->lr_ghost[i].get_tree_id(), args->lr_ghost[i].get_index_space().id);
      printf("\n");
#endif
    }

#ifdef USE_CEMA
    if(rank->cema_samples_per_rank) {
      // everybody gets simultaneous access to the global sample list
      distribute_launcher.add_region_requirement(RegionRequirement(lr_cema_global, READ_WRITE, SIMULTANEOUS,
								   lr_cema_global,
#ifdef CEMA_SORT_ON_LAST_NODE
								   RHSF_GHOST_LAST_NODE
#else
								   RHSF_GHOST_FIRST_NODE
#endif
								   )
						 .add_flags(NO_ACCESS_FLAG)
						 .add_field(FID_SAMPLE_X)
						 .add_field(FID_SAMPLE_Y)
						 .add_field(FID_SAMPLE_Z)
						 .add_field(FID_TEXP));

      //distribute_launcher.add_index_requirement(IndexSpaceRequirement(is_cema, ALL_MEMORY, is_cema)); 
    }
#endif

    //Future f = runtime->execute_task(ctx, distribute_launcher);
    // cannot wait for results here - these tasks synchronize with each other via barriers
    //dist_futures.push_back(f);
    spmd_launcher.add_single_task(DomainPoint::from_point<3>(pir_d.p), distribute_launcher);
  }
  print_usage("after distribute launch");

  // wait on all distributed pieces to finish
#if 0
  while(dist_futures.size()) {
    Future f = dist_futures.back();
    dist_futures.pop_back();
    f.get_void_result();
  }
#endif
  FutureMap spmd_map = runtime->execute_must_epoch(ctx, spmd_launcher);
  spmd_map.wait_all_results();

  double stop_time = Realm::Clock::current_time_in_microseconds();
  printf("RHSF TOP Execution Time: %7.3f ms\n", (stop_time - start_time)*1e-3);

  if (S3DRank::get_explicit_ghost_cells()) {
    for(int i = 0; i < 3; i++)
      if(rank->vary_in_dims[i])
	runtime->destroy_logical_region(ctx, lr_ghost[i]);
    runtime->destroy_field_space(ctx, fs_ghost);
  }

  runtime->destroy_index_space(ctx, is_grid);
  printf("all done!\n");
}

MPIConnectResult connect_mpi_task(const Task *task,
                                  const std::vector<PhysicalRegion> &regions,
                                  Context ctx, HighLevelRuntime *runtime)
{
  assert(task->local_arglen == sizeof(DomainPoint));
  DomainPoint test_point = *(DomainPoint*)task->local_args;

  if (S3DRank::get_show_progress())
    printf("connect task, pt = (%d, %d, %d)\n",
	   test_point.get_point<3>()[0], test_point.get_point<3>()[1], test_point.get_point<3>()[2]);

#ifdef SHARED_LOWLEVEL
  if (!S3DRank::has_rank(test_point.get_point<3>())) {
    MPIConnectResult result;
    result.success = false;
    return result;
  }
#endif

  S3DRank *rank = S3DRank::get_rank(test_point.get_point<3>(), false);

  rank->handshake->legion_init();

  MPIConnectResult result;
  result.point = DomainPoint::from_point<3>(rank->my_proc_id);
  result.proc = runtime->get_executing_processor(ctx);
  result.success = true;
  result.subranks_needed = rank->n_subranks;
  return result;
}

void update_mappers_task(const Task *task,
                         const std::vector<PhysicalRegion> &regions,
                         Context ctx, HighLevelRuntime *runtime)
{
#if 0
  const char *pos = (const char *)task->args;
  int left = task->arglen;
#endif

  Processor me = runtime->get_executing_processor(ctx);
#if 0
  RHSFMapper *mapper =
    static_cast<RHSFMapper *>(runtime->get_mapper(ctx, 0 /* default mapper is RHSFMapper */));

  // args should be a list of [ Point<3>, int, [ Processor,... ] ]
  while(left > 0) {
    Point<3> p = *(const Point<3> *)pos;  pos += sizeof(Point<3>);  left -= sizeof(Point<3>);
    int count = *(const int *)pos; pos += sizeof(int);  left -= sizeof(int);
    assert(count >= 0);
    while(count-- > 0) {
      Processor proc = *(Processor *)pos;  pos += sizeof(Processor);  left -= sizeof(Processor);
      //printf("mapper on proc "IDFMT" assigning proc "IDFMT" to point (%d,%d,%d)\n",
      //	     me.id, proc.id, p[0], p[1], p[2]);
      mapper->assign_point(DomainPoint::from_point<3>(p), proc);
    }
  }
  assert(left == 0);
#else
  assert(regions.size() == 1);
  assert(task->arglen == sizeof(Rect<3>));
  Rect<3> bounds = *((const Rect<3>*)task->args);
  int procs_per_node = task->regions[0].privilege_fields.size();
  std::vector<RegionAccessor<AccessorType::Generic, Processor> > proc_accessors(procs_per_node);
  for (int idx = 0; idx < procs_per_node; idx++)
    proc_accessors[idx] = regions[0].get_field_accessor(
                            task->regions[0].instance_fields[idx]).typeify<Processor>();
  // Build a set of mappers for all our local processors
  std::vector<RHSFMapper*> local_mappers;
  Machine machine = Machine::get_machine();
  std::set<Processor> all_procs;
  machine.get_all_processors(all_procs);
  for (std::set<Processor>::const_iterator it = all_procs.begin();
	it != all_procs.end(); it++)
  {
    if (it->kind() != Processor::LOC_PROC)
      continue;
    if (it->address_space() == me.address_space())
      local_mappers.push_back(static_cast<RHSFMapper*>(runtime->get_mapper(ctx, 0/*id*/, (*it))));
  }
  for (std::vector<RHSFMapper*>::const_iterator it = local_mappers.begin();
       it != local_mappers.end(); it++)
    (*it)->set_bounds(bounds);
  for(GenericPointInRectIterator<3> pir(bounds); pir; pir++)
  {
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);
    for (int idx = 0; idx < procs_per_node; idx++)
    {
      Processor proc = proc_accessors[idx].read(dp);
      for (std::vector<RHSFMapper*>::const_iterator it = local_mappers.begin();
	    it != local_mappers.end(); it++)
      {
	(*it)->assign_point(dp, proc);
      }
    }
  }
#endif
}

void update_subrank_bounds_task(const Task *task,
                                const std::vector<PhysicalRegion> &regions,
                                Context ctx, HighLevelRuntime *runtime)
{
  printf("update subrank bounds task, pt = (%d, %d, %d)\n", 
      task->index_point.get_point<3>()[0], 
      task->index_point.get_point<3>()[1], 
      task->index_point.get_point<3>()[2]);

  S3DRank *rank = S3DRank::get_rank(task->index_point.get_point<3>(), true);

  printf("args = %p, %zd\n", task->local_args, task->local_arglen);

  if(task->local_arglen) {
    rank->subrank_bounds = (Rect<3> *)malloc(task->local_arglen);
    memcpy(rank->subrank_bounds, task->local_args, task->local_arglen);
  }
}

#ifndef REMOVE_OLD_BARRIER_EXCHANGE
void create_ghost_regions_task(const Task *task,
                               const std::vector<PhysicalRegion> &regions,
                               Context ctx, HighLevelRuntime *runtime)
{
  Point<3> p = task->index_point.get_point<3>();

  if (S3DRank::get_show_progress())
    printf("%s task, pt = (%d, %d, %d)\n", "create_ghost", p[0], p[1], p[2]);

  //S3DRank *rank = S3DRank::get_rank(p, false);

  // don't have to do anything here yet - eventually make barriers and store them
  // for each region and each field, create a barrier
  for(size_t i = 0; i < task->regions.size(); i++) {
    Rect<3> r = runtime->get_index_space_domain(ctx,
						regions[i].get_logical_region().get_index_space()).get_rect<3>();

    for(std::vector<FieldID>::const_iterator it = task->regions[i].instance_fields.begin();
	it != task->regions[i].instance_fields.end();
	it++) {
      RegionAccessor<AccessorType::Generic, double> fa = regions[i].get_field_accessor(*it).typeify<double>();

      const size_t DOUBLES_NEEDED = (sizeof(PhaseBarrier) + sizeof(double) - 1) / sizeof(double);
      union {
	//PhaseBarrier pb;  C++ won't let us do this because it has a constructor
	char as_bytes[sizeof(PhaseBarrier)];
	double as_dbl[DOUBLES_NEEDED];
      } u;

      // TODO: Put this back in once Sean fixes phase barriers
      //PhaseBarrier pb = runtime->create_phase_barrier(ctx, 2);
      PhaseBarrier pb = runtime->create_phase_barrier(ctx, 1);
      *(PhaseBarrier *)u.as_bytes = pb;

      Point<3> p2 = r.lo;

#ifdef DEBUG_GHOST_CELLS
      if(it == task->regions[i].instance_fields.begin()) {
	printf("CREATE: (%d,%d,%d) (%d,%d,%d)/%d "IDFMT"\n",
	       p[0], p[1], p[2], p2[0], p2[1], p2[2], *it, pb.get_barrier().id);
      }
#endif

      for(size_t k = 0; k < DOUBLES_NEEDED; k++) {
	fa.write(DomainPoint::from_point<3>(p2), u.as_dbl[k]);
	p2.x[0]++;
      }
    }
  }
}

void gather_barriers_task(const Task *task,
                          const std::vector<PhysicalRegion> &regions,
                          Context ctx, HighLevelRuntime *runtime)
{
  Point<3> p = task->index_point.get_point<3>();

  if (S3DRank::get_show_progress())
    printf("%s task, pt = (%d, %d, %d)\n", "gather barriers", p[0], p[1], p[2]);

  S3DRank *rank = S3DRank::get_rank(p, false);

  GhostBarrierMap *gbm = new GhostBarrierMap;
  printf("rank = %p\n", rank);
  rank->barrier_data = gbm;

  // for each region and field, fetch the barrier and store it in our map
  for(size_t i = 0; i < task->regions.size(); i++) {
    LogicalRegion lr = regions[i].get_logical_region();
    Rect<3> r = runtime->get_index_space_domain(ctx, lr.get_index_space()).get_rect<3>();

    std::map<FieldID, PhaseBarrier>& per_region_map = (i & 1) ? gbm->outside[lr] : gbm->inside[lr];

    for(std::vector<FieldID>::const_iterator it = task->regions[i].instance_fields.begin();
	it != task->regions[i].instance_fields.end();
	it++) {
      FieldID fid = *it;

      RegionAccessor<AccessorType::Generic, double> fa = regions[i].get_field_accessor(fid).typeify<double>();

      const size_t DOUBLES_NEEDED = (sizeof(PhaseBarrier) + sizeof(double) - 1) / sizeof(double);
      union {
	//PhaseBarrier pb;  C++ won't let us do this because it has a constructor
	char as_bytes[sizeof(PhaseBarrier)];
	double as_dbl[DOUBLES_NEEDED];
      } u;

      Point<3> p2 = r.lo;
      for(size_t k = 0; k < DOUBLES_NEEDED; k++) {
	u.as_dbl[k] = fa.read(DomainPoint::from_point<3>(p2));
	p2.x[0]++;
      }

      PhaseBarrier pb = *(PhaseBarrier *)u.as_bytes;
      per_region_map[fid] = pb;

#ifdef DEBUG_GHOST_CELLS
      {
	p2 = r.lo;
	if(it == task->regions[i].instance_fields.begin()) {
	  printf("READ: (%d,%d,%d) (%d,%d,%d)/%d "IDFMT"\n",
		 p[0], p[1], p[2], p2[0], p2[1], p2[2], fid, pb.get_barrier().id);
	}
      }
#endif
    }
  }
}
#endif

#if 0
void ghost_regions_fixup_task(const Task *task,
                              const std::vector<PhysicalRegion> &regions,
                              Context ctx, HighLevelRuntime *runtime)
{
  Point<3> p = task->index_point.get_point<3>();

  if (S3DRank::get_show_progress())
    printf("%s task, pt = (%d, %d, %d), proc = "IDFMT"\n",
	   "ghost_fixup", p[0], p[1], p[2], runtime->get_executing_processor(ctx).id);

#define GHOST_REGION_COHERENCE_DEBUG
#ifdef GHOST_REGION_COHERENCE_DEBUG
  // fill all entries in ghost regions with NaN's
  double nan = 0.0 / 0.0;
  for(size_t i = 0; i < task->regions.size(); i++) {
    Rect<3> r = runtime->get_index_space_domain(ctx,
						regions[i].get_logical_region().get_index_space()).get_rect<3>();

    for(std::vector<FieldID>::const_iterator it = task->regions[i].instance_fields.begin();
	it != task->regions[i].instance_fields.end();
	it++) {
      RegionAccessor<AccessorType::Generic, double> fa = regions[i].get_field_accessor(*it).typeify<double>();

      for(GenericPointInRectIterator<3> pir(r); pir; pir++)
        fa.write(DomainPoint::from_point<3>(pir.p), nan);
    }
  }
#endif
}
#endif
