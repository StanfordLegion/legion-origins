
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <unistd.h>

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "check_field.h"
#include "legion.h"
#include "arrays.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

struct CheckFieldArgs {
  const double * S3DRank::RHSFArrays::* ref_ptr;
  int plane_number;
  double abs_tolerance, rel_tolerance, min_difference;
  char prefix[50];
};

void check_field(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
		 IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr, FieldID fid,
		 const S3DRank *rank, const double * S3DRank::RHSFArrays::* ref_ptr, int plane_number, 
		 double abs_tolerance, double rel_tolerance, double min_difference,
		 const char *fmt, ...)
{
  // a few early outs:
  //if(!(rank->cur_data->*ref_ptr)) return; // if we have no data, assume nobody else does either

  CheckFieldArgs args;
  args.ref_ptr = ref_ptr;
  args.plane_number = plane_number;
  args.abs_tolerance = abs_tolerance;
  args.rel_tolerance = rel_tolerance;
  args.min_difference = min_difference;

  va_list ap;
  va_start(ap, fmt);
  vsnprintf(args.prefix, 50, fmt, ap);
  va_end(ap);

  LogicalPartition lp_top = runtime->get_logical_partition(ctx, lr, ip_top);

  {
    IndexLauncher check_launcher(CHECK_FIELD_TASK_ID,
                                 Domain::from_rect<3>(proc_grid_bounds),
                                 TaskArgument(&args, sizeof(args)),
                                 ArgumentMap()); 
    check_launcher.tag = RHSF_MAPPER_FORCE_RANK_MATCH | RHSF_MAPPER_BALANCE;

    RegionRequirement rr(lp_top, 0, (S3DRank::get_diff_replace_data() ? READ_WRITE : READ_ONLY), EXCLUSIVE, lr);
    rr.add_field(fid);
    check_launcher.add_region_requirement(rr);

    FutureMap fm = runtime->execute_index_space(ctx, check_launcher);
    if (S3DRank::get_perform_waits())
      fm.wait_all_results();
  }
}

template <unsigned DIM, typename AT>
void check_for_nans(const Rect<DIM>& bounds, AT field_accessor,
		    int *p_errcount = 0, int err_limit = -1,
		    const char *prefix = 0)
{
  for(GenericPointInRectIterator<DIM> pir(bounds); pir; pir++) {
    double act_val = field_accessor.read(DomainPoint::from_point<3>(pir.p));
    if(isnan(act_val)) {
      if((err_limit < 0) || !p_errcount || (*p_errcount < err_limit)) {
	printf("%sNaN CHECK: (%d, %d, %d): act = %g\n",
	       prefix ? prefix : "", pir.p[0], pir.p[1], pir.p[2], act_val);
      }
      if(p_errcount) (*p_errcount)++;
    }
  }
}

template <typename T, unsigned DIM, typename AT>
const T* compare_with_fortran_array(const T* data, const Rect<DIM>& bounds, AT field_accessor,
				    T abs_tolerance, T rel_tolerance, int *p_errcount = 0, int err_limit = -1,
				    bool replace_data = false, T min_difference = 0, const char *prefix = 0)
{
  // this is a fortran array we're loading from, so enumerate the points in fortran order
  for(GenericPointInRectIterator<DIM> pir(bounds); pir; pir++) {
    T ref_val = *data++;
    T act_val = field_accessor.read(DomainPoint::from_point<3>(pir.p));
    T diff = ref_val - act_val;
    if(diff < 0) diff = -diff;
    if((diff > abs_tolerance) || 
       ((diff > min_difference) && (((diff * rel_tolerance) > fabs(ref_val)) || ((diff * rel_tolerance) > fabs(act_val)))) ||
       (isnan(ref_val) != isnan(act_val)) || 
       (isinf(ref_val) != isinf(act_val))) {
      if((err_limit < 0) || !p_errcount || (*p_errcount < err_limit)) {
	printf("%sMISMATCH: (%d, %d, %d): ref = %g, act = %g, diff = %g\n",
	       prefix ? prefix : "", pir.p[0], pir.p[1], pir.p[2], ref_val, act_val, diff);
      }
      if(p_errcount) (*p_errcount)++;
    }
    if(replace_data)
      field_accessor.write(DomainPoint::from_point<3>(pir.p), ref_val);
  }

  return data;
}

template <typename T, typename AT>
const T* compare_with_fortran_array(const T* data, const Rect<3>& bounds, AT field_accessor,
				    T abs_tolerance, T rel_tolerance, int *p_errcount = 0, int err_limit = -1,
				    bool replace_data = false, T min_difference = 0, const char *prefix = 0)
{
  ByteOffset offsets[3];
  Rect<3> subrect;
  T *target = field_accessor.template raw_rect_ptr<3>(bounds, subrect, offsets);
  if(target && (subrect == bounds)) {
    // fast version
    for(int z = bounds.lo[2]; z <= bounds.hi[2]; z++) {
      T *t_y = target;
      for(int y = bounds.lo[1]; y <= bounds.hi[1]; y++) {
	T *t_x = t_y;
	for(int x = bounds.lo[0]; x <= bounds.hi[0]; x++) {
	  T ref_val = *data++;
	  T act_val = *t_x;
	  T diff = ref_val - act_val;
	  if(diff < 0) diff = -diff;
	  if((diff > abs_tolerance) || 
	     ((diff > min_difference) && (((diff * rel_tolerance) > fabs(ref_val)) || ((diff * rel_tolerance) > fabs(act_val)))) ||
	     (isnan(ref_val) != isnan(act_val)) || 
	     (isinf(ref_val) != isinf(act_val))) {
	    if((err_limit < 0) || !p_errcount || (*p_errcount < err_limit)) {
	      printf("%sMISMATCH: (%d, %d, %d): ref = %g, act = %g, diff = %g\n",
		     prefix ? prefix : "", x, y, z, ref_val, act_val, diff);
	    }
	    if(p_errcount) (*p_errcount)++;
	  }
	  if(replace_data)
	    *t_x = ref_val;

	  t_x += offsets[0];
	}
	t_y += offsets[1];
      }
      target += offsets[2];
    }
    return data;
  }

  // this is a fortran array we're loading from, so enumerate the points in fortran order
  for(GenericPointInRectIterator<3> pir(bounds); pir; pir++) {
    T ref_val = *data++;
    T act_val = field_accessor.read(DomainPoint::from_point<3>(pir.p));
    T diff = ref_val - act_val;
    if(diff < 0) diff = -diff;
    if((diff > abs_tolerance) || 
       ((diff > min_difference) && (((diff * rel_tolerance) > fabs(ref_val)) || ((diff * rel_tolerance) > fabs(act_val)))) ||
       (isnan(ref_val) != isnan(act_val)) || 
       (isinf(ref_val) != isinf(act_val))) {
      if((err_limit < 0) || !p_errcount || (*p_errcount < err_limit)) {
	printf("%sMISMATCH: (%d, %d, %d): ref = %g, act = %g, diff = %g\n",
	       prefix ? prefix : "", pir.p[0], pir.p[1], pir.p[2], ref_val, act_val, diff);
      }
      if(p_errcount) (*p_errcount)++;
    }
    if(replace_data)
      field_accessor.write(DomainPoint::from_point<3>(pir.p), ref_val);
  }

  return data;
}

void check_field_task(const Task *task,
                      const std::vector<PhysicalRegion> &regions,
                      Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(CheckFieldArgs));
  const CheckFieldArgs *args = (const CheckFieldArgs *)task->args;

  char pfx[80];
  sprintf(pfx, "%s [%d,%d,%d]: ", args->prefix, 
      task->index_point.get_point<3>()[0], 
      task->index_point.get_point<3>()[1], 
      task->index_point.get_point<3>()[2]);

  S3DRank *rank = S3DRank::get_rank(task->index_point.get_point<3>(), true);

  Blockify<3> grid2proc_map(rank->local_grid_size);
  Rect<3> my_subgrid_bounds = grid2proc_map.preimage(task->index_point.get_point<3>());

  // get whatever accessors you need here
  RegionAccessor<AccessorType::Generic,double> fa_in = 
    regions[0].get_field_accessor(task->regions[0].instance_fields[0]).typeify<double>();

  int errors = 0;
  const double *refdata = rank->cur_data->*(args->ref_ptr);
  if(refdata)
    compare_with_fortran_array(refdata + args->plane_number * my_subgrid_bounds.volume(),
			       my_subgrid_bounds,
			       fa_in,
			       args->abs_tolerance,
			       args->rel_tolerance,
			       &errors,
			       S3DRank::diff_max_errors,
			       S3DRank::get_diff_replace_data(),
			       args->min_difference,
			       (const char *)pfx);
  else
    check_for_nans(my_subgrid_bounds,
		   fa_in,
		   &errors,
		   S3DRank::diff_max_errors,
		   (const char *)pfx);
  if (errors || S3DRank::show_all_error_counts)
    printf("%s%d errors\n", pfx, errors);
}

struct DumpFieldArgs {
  char prefix[50];
};

void dump_field(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
		IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr, FieldID fid,
		const S3DRank *rank, bool blocking,
		const char *fmt, ...)
{
  DumpFieldArgs args;

  va_list ap;
  va_start(ap, fmt);
  vsnprintf(args.prefix, 50, fmt, ap);
  va_end(ap);

  LogicalPartition lp_top = runtime->get_logical_partition(ctx, lr, ip_top);

  {
    IndexLauncher dump_launcher(DUMP_FIELD_TASK_ID,
				Domain::from_rect<3>(proc_grid_bounds),
				TaskArgument(&args, sizeof(args)),
				ArgumentMap()); 
    dump_launcher.tag = RHSF_MAPPER_FORCE_RANK_MATCH | RHSF_MAPPER_BALANCE;

    RegionRequirement rr(lp_top, 0, READ_ONLY, EXCLUSIVE, lr);
    rr.add_field(fid);
    dump_launcher.add_region_requirement(rr);

    FutureMap fm = runtime->execute_index_space(ctx, dump_launcher);
    if (blocking || S3DRank::get_perform_waits())
      fm.wait_all_results();
  }
}

void dump_field_task(const Task *task,
                      const std::vector<PhysicalRegion> &regions,
                      Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(DumpFieldArgs));
  const DumpFieldArgs *args = (const DumpFieldArgs *)task->args;

  char pfx[80];
  sprintf(pfx, "%s [%d,%d,%d]: ", args->prefix, 
      task->index_point.get_point<3>()[0], 
      task->index_point.get_point<3>()[1], 
      task->index_point.get_point<3>()[2]);

  S3DRank *rank = S3DRank::get_rank(task->index_point.get_point<3>(), true);

  Blockify<3> grid2proc_map(rank->local_grid_size);
  Rect<3> my_subgrid_bounds = grid2proc_map.preimage(task->index_point.get_point<3>());

  // get whatever accessors you need here
  RegionAccessor<AccessorType::Generic,double> fa_in = 
    regions[0].get_field_accessor(task->regions[0].instance_fields[0]).typeify<double>();

  // this is a fortran array we're loading from, so enumerate the points in fortran order
  for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
    double val = fa_in.read(DomainPoint::from_point<3>(pir.p));
    printf("%s: (%d, %d, %d): %g (%llx)\n",
	   pfx, pir.p[0], pir.p[1], pir.p[2], val, *(unsigned long long *)&val);
  }
}

