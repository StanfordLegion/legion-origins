
#include "rhsf.h"
#include "calc_flux.h"
#include "check_field.h"
#include "legion.h"
#include "arrays.h"
#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
#include <x86intrin.h>
#endif
#include <cmath>

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
static inline __m128d _mm_stream_load_pd(const double *ptr) 
{
#ifdef __SSE4_1__
  return _mm_castsi128_pd(_mm_stream_load_si128(reinterpret_cast<__m128i *>(const_cast<double *>(ptr))));
#else
  return *(const __m128d *)ptr;
#endif
}
#endif

#ifdef USE_AVX_KERNELS
template<unsigned BOUNDARY>
static inline bool aligned(const void *ptr)
{
  return ((((unsigned long)ptr) & (BOUNDARY-1)) == 0);
}
#endif

CalcYDiffFluxTask::CalcYDiffFluxTask(S3DRank *r,
                                     Domain domain,
                                     TaskArgument global_arg,
                                     ArgumentMap arg_map,
                                     Predicate pred,
                                     bool must,
                                     MapperID id,
                                     MappingTagID tag,
                                     bool add_requirements)
  : IndexLauncher(CalcYDiffFluxTask::TASK_ID, domain, global_arg, 
                  arg_map, pred, must, id, tag | CalcYDiffFluxTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    for (int s = 0; s < rank->n_spec-1; s++)
      for (int d = 0; d < 3; d++)
        rr_out.add_field(FID_YDIFFFLUX(s, d));
    add_region_requirement(rr_out);

    RegionRequirement rr_state(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    for (int s = 0; s < rank->n_spec; s++)
      rr_state.add_field(FID_DS_MIXAVG(s));
    add_region_requirement(rr_state);

    RegionRequirement rr_in(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    for (int s = 0; s < rank->n_spec-1; s++)
    {
      rr_in.add_field(FID_YSPECIES(s));
      for (int d = 0; d < 3; d++)
        rr_in.add_field(FID_GRAD_YS(s, d));
    }
    for (int d = 0; d < 3; d++)
      rr_in.add_field(FID_GRAD_MIXMW(d));
    rr_in.add_field(FID_AVMOLWT);
    add_region_requirement(rr_in);

    RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
    rr_q.add_field(FID_RHO);
    add_region_requirement(rr_q);
  }
}

void CalcYDiffFluxTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  for(int s = 0; s < rank->n_spec-1; s++)
    for(int d = 0; d < 3; d++)
      check_field(runtime, ctx, rank->proc_grid_bounds, rank->is_grid, 
                  rank->ip_top, rank->lr_int, FID_YDIFFFLUX(s, d),
                  rank, &S3DRank::RHSFArrays::ydiffflux, d * rank->n_spec + s, 
                  1e-15, 1e8, 0, "Ydiffflux[%d,%d]", s, d);
}

/*static*/ const char * const CalcYDiffFluxTask::TASK_NAME = "calc_ydiffflux";

/*static*/
bool CalcYDiffFluxTask::dense_calc_ydiffflux_task(S3DRank *rank, const Rect<3> &subgrid_bounds,
                                                  const std::vector<RegionRequirement> &reqs,
                                                  const std::vector<PhysicalRegion> &regions)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], offsets[3];

  RegionAccessor<AccessorType::Generic,double> fa_in_avmolwt = regions[2].get_field_accessor(FID_AVMOLWT).typeify<double>();
  const double *in_avmolwt_ptr = fa_in_avmolwt.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_avmolwt_ptr || (subrect != subgrid_bounds)) return false;

  RegionAccessor<AccessorType::Generic,double> fa_in_rho = regions[3].get_field_accessor(FID_RHO).typeify<double>();
  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw = regions[2].get_field_accessor(FID_GRAD_MIXMW(0)).typeify<double>();
  const double *in_grad_mixmw_ptr = fa_in_grad_mixmw.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_mixmw_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  off_t grad_mixmw_stride; 
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_grad_mixmw = regions[2].get_field_accessor(FID_GRAD_MIXMW(1)).typeify<double>();
    const double *next_grad_mixmw_ptr = fa_next_grad_mixmw.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_grad_mixmw_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
    ByteOffset stride(next_grad_mixmw_ptr,in_grad_mixmw_ptr);
    grad_mixmw_stride = stride.offset/sizeof(double);
  }

  RegionAccessor<AccessorType::Generic,double> fa_in_ds_mixavg = regions[1].get_field_accessor(FID_DS_MIXAVG(0)).typeify<double>();
  const double *in_ds_mixavg_ptr = fa_in_ds_mixavg.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_ds_mixavg_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  off_t ds_mixavg_stride;
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_ds_mixavg = regions[1].get_field_accessor(FID_DS_MIXAVG(1)).typeify<double>();
    const double *next_ds_mixavg_ptr = fa_next_ds_mixavg.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
    if(!next_ds_mixavg_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
    ByteOffset stride(next_ds_mixavg_ptr,in_ds_mixavg_ptr);
    ds_mixavg_stride = stride.offset/sizeof(double);
  }

  RegionAccessor<AccessorType::Generic,double> fa_in_yspecies = regions[2].get_field_accessor(FID_YSPECIES(0)).typeify<double>();
  const double *in_yspecies_ptr = fa_in_yspecies.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_yspecies_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  off_t yspecs_stride;
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_yspecies = regions[2].get_field_accessor(FID_YSPECIES(1)).typeify<double>();
    const double *next_yspecies_ptr = fa_next_yspecies.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_yspecies_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
    ByteOffset stride(next_yspecies_ptr,in_yspecies_ptr);
    yspecs_stride = stride.offset/sizeof(double);
  }

  RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys = regions[2].get_field_accessor(FID_GRAD_YS(0,0)).typeify<double>();
  const double *in_grad_ys_ptr = fa_in_grad_ys.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_ys_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
  
  off_t grad_ys_stride;
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_grad_ys = regions[2].get_field_accessor(FID_GRAD_YS(0,1)).typeify<double>();
    const double *next_grad_ys_ptr = fa_next_grad_ys.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_grad_ys_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
    ByteOffset stride(next_grad_ys_ptr,in_grad_ys_ptr);
    grad_ys_stride = stride.offset/sizeof(double);
  }

  RegionAccessor<AccessorType::Generic,double> fa_out_ydiffflux = regions[0].get_field_accessor(FID_YDIFFFLUX(0,0)).typeify<double>();
  double *out_ydiffflux_ptr = fa_out_ydiffflux.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_ydiffflux_ptr || (subrect != subgrid_bounds)) return false;

  off_t diffflux_stride;
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_ydiffflux = regions[0].get_field_accessor(FID_YDIFFFLUX(0,1)).typeify<double>();
    double *next_ydiffflux_ptr = fa_next_ydiffflux.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_ydiffflux_ptr || (subrect != subgrid_bounds)) return false;
    ByteOffset stride(next_ydiffflux_ptr,out_ydiffflux_ptr);
    diffflux_stride = stride.offset/sizeof(double);
  }

#ifdef BULLDOZER
// Fit in L1 and only use first set
// to minimize set associativity usage
#define FULL_STRIP_SIZE 64 
#else
// size to fit in L1
#define FULL_STRIP_SIZE 512 
#endif

#if defined(USE_AVX_KERNELS)
  __m256d avx_rho[FULL_STRIP_SIZE/4];
  __m256d avx_grad_mixmw_x[FULL_STRIP_SIZE/4];
  __m256d avx_grad_mixmw_y[FULL_STRIP_SIZE/4];
  __m256d avx_grad_mixmw_z[FULL_STRIP_SIZE/4];
  __m256d avx_yspecies[FULL_STRIP_SIZE/4];
  __m256d avx_ds_mixavg[FULL_STRIP_SIZE/4];
#elif defined(USE_SSE_KERNELS)
  __m128d sse_rho[FULL_STRIP_SIZE/2];
  __m128d sse_grad_mixmw_x[FULL_STRIP_SIZE/2];
  __m128d sse_grad_mixmw_y[FULL_STRIP_SIZE/2];
  __m128d sse_grad_mixmw_z[FULL_STRIP_SIZE/2];
  __m128d sse_yspecies[FULL_STRIP_SIZE/2];
  __m128d sse_ds_mixavg[FULL_STRIP_SIZE/2];
#endif
  // We need these arrays in every case for the slow version
  double rho[FULL_STRIP_SIZE];
  // We'll fold avmolwt directly into these since it is the same across all species and directions
  double grad_mixmw_x[FULL_STRIP_SIZE];
  double grad_mixmw_y[FULL_STRIP_SIZE];
  double grad_mixmw_z[FULL_STRIP_SIZE];

  // Now we can do the actual math
  size_t n_pts = subgrid_bounds.volume();

  // Use this count of number of species because we might not be
  // computing ydiffflux for all the species.  Sometimes the last
  // species is omitted and computed by 1 - (sum of all other species)
  const int num_species = reqs[0].privilege_fields.size()/3;
  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
      // Fast case with a full strip

#if defined(USE_AVX_KERNELS)
      // Only need to check the outputs for alignement
      if (aligned<32>(out_ydiffflux_ptr) && aligned<32>(in_avmolwt_ptr) && aligned<32>(in_rho_ptr) &&
          aligned<32>(in_grad_mixmw_ptr) && aligned<32>(in_yspecies_ptr) && aligned<32>(in_ds_mixavg_ptr) &&
          aligned<32>(in_grad_ys_ptr)) {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d avmolwt = _mm256_load_pd(in_avmolwt_ptr+(i<<2));
          avx_rho[i] = _mm256_xor_pd(_mm256_load_pd(in_rho_ptr+(i<<2)),_mm256_set1_pd(-0.0));
          avx_grad_mixmw_x[i] = _mm256_mul_pd(avmolwt,_mm256_load_pd(in_grad_mixmw_ptr+(i<<2)));
          avx_grad_mixmw_y[i] = _mm256_mul_pd(avmolwt,_mm256_load_pd(in_grad_mixmw_ptr+(i<<2)+grad_mixmw_stride));
          avx_grad_mixmw_z[i] = _mm256_mul_pd(avmolwt,_mm256_load_pd(in_grad_mixmw_ptr+(i<<2)+2*grad_mixmw_stride));
        }

        for (int s = 0; s < num_species; s++) {

          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            avx_yspecies[i] = _mm256_load_pd(in_yspecies_ptr+(i<<2)+(s*yspecs_stride));
          }

          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) { 
            avx_ds_mixavg[i] = _mm256_mul_pd(avx_rho[i],_mm256_load_pd(in_ds_mixavg_ptr+(i<<2)+(s*ds_mixavg_stride)));
          }

          // X direction
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d grad_ys_x = _mm256_load_pd(in_grad_ys_ptr+(i<<2)+(s*3)*grad_ys_stride);
            __m256d ydiffflux_x = _mm256_mul_pd(avx_ds_mixavg[i],_mm256_add_pd(grad_ys_x,_mm256_mul_pd(avx_yspecies[i],avx_grad_mixmw_x[i])));
            _mm256_stream_pd(out_ydiffflux_ptr+(i<<2)+(s*3)*diffflux_stride,ydiffflux_x);
          }

          // Y direction
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) { 
            __m256d grad_ys_y = _mm256_load_pd(in_grad_ys_ptr+(i<<2)+(s*3+1)*grad_ys_stride);
            __m256d ydiffflux_y = _mm256_mul_pd(avx_ds_mixavg[i],_mm256_add_pd(grad_ys_y,_mm256_mul_pd(avx_yspecies[i],avx_grad_mixmw_y[i])));
            _mm256_stream_pd(out_ydiffflux_ptr+(i<<2)+(s*3+1)*diffflux_stride,ydiffflux_y);
          }

          // Z direction
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d grad_ys_z = _mm256_load_pd(in_grad_ys_ptr+(i<<2)+(s*3+2)*grad_ys_stride);
            __m256d ydiffflux_z = _mm256_mul_pd(avx_ds_mixavg[i],_mm256_add_pd(grad_ys_z,_mm256_mul_pd(avx_yspecies[i],avx_grad_mixmw_z[i])));
            _mm256_stream_pd(out_ydiffflux_ptr+(i<<2)+(s*3+2)*diffflux_stride,ydiffflux_z);
          }
        }
      }
      else {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d avmolwt = _mm256_loadu_pd(in_avmolwt_ptr+(i<<2));
          avx_rho[i] = _mm256_xor_pd(_mm256_loadu_pd(in_rho_ptr+(i<<2)),_mm256_set1_pd(-0.0));
          avx_grad_mixmw_x[i] = _mm256_mul_pd(avmolwt,_mm256_loadu_pd(in_grad_mixmw_ptr+(i<<2)));
          avx_grad_mixmw_y[i] = _mm256_mul_pd(avmolwt,_mm256_loadu_pd(in_grad_mixmw_ptr+(i<<2)+grad_mixmw_stride));
          avx_grad_mixmw_z[i] = _mm256_mul_pd(avmolwt,_mm256_loadu_pd(in_grad_mixmw_ptr+(i<<2)+2*grad_mixmw_stride));
        }

        for (int s = 0; s < num_species; s++) {

          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            avx_yspecies[i] = _mm256_loadu_pd(in_yspecies_ptr+(i<<2)+(s*yspecs_stride));
          }

          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) { 
            avx_ds_mixavg[i] = _mm256_mul_pd(avx_rho[i],_mm256_loadu_pd(in_ds_mixavg_ptr+(i<<2)+(s*ds_mixavg_stride)));
          }

          // X direction
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d grad_ys_x = _mm256_loadu_pd(in_grad_ys_ptr+(i<<2)+(s*3)*grad_ys_stride);
            __m256d ydiffflux_x = _mm256_mul_pd(avx_ds_mixavg[i],_mm256_add_pd(grad_ys_x,_mm256_mul_pd(avx_yspecies[i],avx_grad_mixmw_x[i])));
            _mm256_storeu_pd(out_ydiffflux_ptr+(i<<2)+(s*3)*diffflux_stride,ydiffflux_x);
          }

          // Y direction
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) { 
            __m256d grad_ys_y = _mm256_loadu_pd(in_grad_ys_ptr+(i<<2)+(s*3+1)*grad_ys_stride);
            __m256d ydiffflux_y = _mm256_mul_pd(avx_ds_mixavg[i],_mm256_add_pd(grad_ys_y,_mm256_mul_pd(avx_yspecies[i],avx_grad_mixmw_y[i])));
            _mm256_storeu_pd(out_ydiffflux_ptr+(i<<2)+(s*3+1)*diffflux_stride,ydiffflux_y);
          }

          // Z direction
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d grad_ys_z = _mm256_loadu_pd(in_grad_ys_ptr+(i<<2)+(s*3+2)*grad_ys_stride);
            __m256d ydiffflux_z = _mm256_mul_pd(avx_ds_mixavg[i],_mm256_add_pd(grad_ys_z,_mm256_mul_pd(avx_yspecies[i],avx_grad_mixmw_z[i])));
            _mm256_storeu_pd(out_ydiffflux_ptr+(i<<2)+(s*3+2)*diffflux_stride,ydiffflux_z);
          }
        }
      }
#elif defined(USE_SSE_KERNELS)
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d avmolwt = _mm_stream_load_pd(in_avmolwt_ptr+(i<<1));
        sse_rho[i] = _mm_xor_pd(_mm_stream_load_pd(in_rho_ptr+(i<<1)),_mm_set1_pd(-0.0));
        sse_grad_mixmw_x[i] = _mm_mul_pd(avmolwt,_mm_stream_load_pd(in_grad_mixmw_ptr+(i<<1)));
        sse_grad_mixmw_y[i] = _mm_mul_pd(avmolwt,_mm_stream_load_pd(in_grad_mixmw_ptr+(i<<1)+grad_mixmw_stride));
        sse_grad_mixmw_z[i] = _mm_mul_pd(avmolwt,_mm_stream_load_pd(in_grad_mixmw_ptr+(i<<1)+2*grad_mixmw_stride));
      }

      for (int s = 0; s < num_species; s++) {

        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          sse_yspecies[i] = _mm_stream_load_pd(in_yspecies_ptr+(i<<1)+(s*yspecs_stride));
        }

        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          sse_ds_mixavg[i] = _mm_mul_pd(sse_rho[i],_mm_stream_load_pd(in_ds_mixavg_ptr+(i<<1)+(s*ds_mixavg_stride)));
        }

        // X direction
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          __m128d grad_ys_x = _mm_stream_load_pd(in_grad_ys_ptr+(i<<1)+(s*3)*grad_ys_stride);
          __m128d ydiffflux_x = _mm_mul_pd(sse_ds_mixavg[i],_mm_add_pd(grad_ys_x,_mm_mul_pd(sse_yspecies[i],sse_grad_mixmw_x[i])));
          _mm_stream_pd(out_ydiffflux_ptr+(i<<1)+(s*3)*diffflux_stride,ydiffflux_x);
        }

        // Y direction
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          __m128d grad_ys_y = _mm_stream_load_pd(in_grad_ys_ptr+(i<<1)+(s*3+1)*grad_ys_stride);
          __m128d ydiffflux_y = _mm_mul_pd(sse_ds_mixavg[i],_mm_add_pd(grad_ys_y,_mm_mul_pd(sse_yspecies[i],sse_grad_mixmw_y[i])));
          _mm_stream_pd(out_ydiffflux_ptr+(i<<1)+(s*3+1)*diffflux_stride,ydiffflux_y);
        }

        // Z direction
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          __m128d grad_ys_z = _mm_stream_load_pd(in_grad_ys_ptr+(i<<1)+(s*3+2)*grad_ys_stride);
          __m128d ydiffflux_z = _mm_mul_pd(sse_ds_mixavg[i],_mm_add_pd(grad_ys_z,_mm_mul_pd(sse_yspecies[i],sse_grad_mixmw_z[i])));
          _mm_stream_pd(out_ydiffflux_ptr+(i<<1)+(s*3+2)*diffflux_stride,ydiffflux_z);
        }
      }
#else
      // First load the grad mixm values and fold avmolwt into all them
      for (int i = 0; i < FULL_STRIP_SIZE; i++) { 
        double avmolwt = in_avmolwt_ptr[i]; 
        rho[i] = -(in_rho_ptr[i]);
        grad_mixmw_x[i] = avmolwt * in_grad_mixmw_ptr[i];
        grad_mixmw_y[i] = avmolwt * in_grad_mixmw_ptr[i+grad_mixmw_stride];
        grad_mixmw_z[i] = avmolwt * in_grad_mixmw_ptr[i+2*grad_mixmw_stride];
      }

      // Now iterate over all the species and do the computation
      for (int s = 0; s < num_species; s++) {
        // Load the ds_mixavg and fold in the rho
        // Load the yspecies too
        for (int i = 0; i < FULL_STRIP_SIZE; i++) {
          double yspecies = in_yspecies_ptr[i+(s*yspecs_stride)];
          double ds_mixavg = rho[i] * in_ds_mixavg_ptr[i+(s*ds_mixavg_stride)];
          double grad_ys_x = in_grad_ys_ptr[i+(s*3)*grad_ys_stride];
          double grad_ys_y = in_grad_ys_ptr[i+(s*3+1)*grad_ys_stride];
          double grad_ys_z = in_grad_ys_ptr[i+(s*3+2)*grad_ys_stride];
          double ydiffflux_x = ds_mixavg * (grad_ys_x + yspecies * grad_mixmw_x[i]);
          double ydiffflux_y = ds_mixavg * (grad_ys_y + yspecies * grad_mixmw_y[i]);
          double ydiffflux_z = ds_mixavg * (grad_ys_z + yspecies * grad_mixmw_z[i]);
          out_ydiffflux_ptr[i+(s*3)*diffflux_stride] = ydiffflux_x;
          out_ydiffflux_ptr[i+(s*3+1)*diffflux_stride] = ydiffflux_y;
          out_ydiffflux_ptr[i+(s*3+2)*diffflux_stride] = ydiffflux_z;
        }
      }
#endif
      n_pts -= FULL_STRIP_SIZE;
    } else {
      // Partial strip

      // First load the grad mixm values and fold avmolwt into all them
      for (size_t i = 0; i < n_pts; i++) { 
        double avmolwt = in_avmolwt_ptr[i]; 
        rho[i] = -(in_rho_ptr[i]);
        grad_mixmw_x[i] = avmolwt * in_grad_mixmw_ptr[i];
        grad_mixmw_y[i] = avmolwt * in_grad_mixmw_ptr[i+grad_mixmw_stride];
        grad_mixmw_z[i] = avmolwt * in_grad_mixmw_ptr[i+2*grad_mixmw_stride];
      }

      // Now iterate over all the species and do the computation
      for (int s = 0; s < num_species; s++) {
        // Load the ds_mixavg and fold in the rho
        // Load the yspecies too
        for (size_t i = 0; i < n_pts; i++) {
          double yspecies = in_yspecies_ptr[i+(s*yspecs_stride)];
          double ds_mixavg = rho[i] * in_ds_mixavg_ptr[i+(s*ds_mixavg_stride)];
          double grad_ys_x = in_grad_ys_ptr[i+(s*3)*grad_ys_stride];
          double grad_ys_y = in_grad_ys_ptr[i+(s*3+1)*grad_ys_stride];
          double grad_ys_z = in_grad_ys_ptr[i+(s*3+2)*grad_ys_stride];
          double ydiffflux_x = ds_mixavg * (grad_ys_x + yspecies * grad_mixmw_x[i]);
          double ydiffflux_y = ds_mixavg * (grad_ys_y + yspecies * grad_mixmw_y[i]);
          double ydiffflux_z = ds_mixavg * (grad_ys_z + yspecies * grad_mixmw_z[i]);
          out_ydiffflux_ptr[i+(s*3)*diffflux_stride] = ydiffflux_x;
          out_ydiffflux_ptr[i+(s*3+1)*diffflux_stride] = ydiffflux_y;
          out_ydiffflux_ptr[i+(s*3+2)*diffflux_stride] = ydiffflux_z;
        }
      }
      n_pts = 0;
    }
    
    // Update our pointers
    in_avmolwt_ptr += FULL_STRIP_SIZE;
    in_rho_ptr += FULL_STRIP_SIZE;
    in_grad_mixmw_ptr += FULL_STRIP_SIZE;
    in_ds_mixavg_ptr += FULL_STRIP_SIZE;
    in_yspecies_ptr += FULL_STRIP_SIZE;
    in_grad_ys_ptr += FULL_STRIP_SIZE;
    out_ydiffflux_ptr += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
}

/*static*/
bool CalcYDiffFluxTask::fast_calc_ydiffflux_task(RegionAccessor<AccessorType::Generic,double> fa_in_ds_mixavg,
                                RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys,
                                RegionAccessor<AccessorType::Generic,double> fa_in_yspecies,
                                RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw,
                                RegionAccessor<AccessorType::Generic,double> fa_in_avmolwt,
                                RegionAccessor<AccessorType::Generic,double> fa_in_rho,
                                RegionAccessor<AccessorType::Generic,double> fa_out_ydiffflux,
                                Rect<3> subgrid_bounds)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3], offsets[3];

  const double *in_ds_mixavg_ptr = fa_in_ds_mixavg.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_ds_mixavg_ptr || (subrect != subgrid_bounds)) return false;

  const double *in_grad_ys_ptr = fa_in_grad_ys.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_ys_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_yspecies_ptr = fa_in_yspecies.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_yspecies_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_grad_mixmw_ptr = fa_in_grad_mixmw.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_mixmw_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_avmolwt_ptr = fa_in_avmolwt.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_avmolwt_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_ydiffflux_ptr = fa_out_ydiffflux.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_ydiffflux_ptr || (subrect != subgrid_bounds)) return false;

  // now that we have all those pointers, we can do the actual work
  ByteOffset in_pos_z, out_pos_z;
  for(int z = subgrid_bounds.lo[2]; z <= subgrid_bounds.hi[2]; z++) {
    ByteOffset in_pos_y = in_pos_z;
    ByteOffset out_pos_y = out_pos_z;
    for(int y = subgrid_bounds.lo[1]; y <= subgrid_bounds.hi[1]; y++) {
      ByteOffset in_pos_x = in_pos_y;
      ByteOffset out_pos_x = out_pos_y;
      for(int x = subgrid_bounds.lo[0]; x <= subgrid_bounds.hi[0]; x++) {
        double rho = *(in_rho_ptr + in_pos_x);
        double ds_mixavg = *(in_ds_mixavg_ptr + in_pos_x);
        double grad_ys = *(in_grad_ys_ptr + in_pos_x);
        double yspecies = *(in_yspecies_ptr + in_pos_x);
        double grad_mixmw = *(in_grad_mixmw_ptr + in_pos_x);
        double avmolwt = *(in_avmolwt_ptr + in_pos_x);

        double ydiffflux = -rho * ds_mixavg * (grad_ys + yspecies * grad_mixmw * avmolwt);

        *(out_ydiffflux_ptr + out_pos_x) = ydiffflux;

        in_pos_x += in_offsets[0];
        out_pos_x += out_offsets[0];
      }
      in_pos_y += in_offsets[1];
      out_pos_y += out_offsets[1];
    }
    in_pos_z += in_offsets[2];
    out_pos_z += out_offsets[2];
  }
  return true;
}

/*static*/
void CalcYDiffFluxTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE

  // See if we can do the dense version
  if (dense_calc_ydiffflux_task(rank, subgrid_bounds, reqs, regions))
    return;

  const int num_species = reqs[0].privilege_fields.size()/3;
  RegionAccessor<AccessorType::Generic,double> fa_avmolwt = regions[2].get_field_accessor(FID_AVMOLWT).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_rho = regions[3].get_field_accessor(FID_RHO).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_mixmw[3];
  for (int d = 0; d < 3; d++)
    fa_grad_mixmw[d] = regions[2].get_field_accessor(FID_GRAD_MIXMW(d)).typeify<double>();
  for (int s = 0; s < num_species; s++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_ds_mixavg = regions[1].get_field_accessor(FID_DS_MIXAVG(s)).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_yspecies = regions[2].get_field_accessor(FID_YSPECIES(s)).typeify<double>();
    for (int d = 0; d < 3; d++)
    {
      RegionAccessor<AccessorType::Generic,double> fa_ydiffflux = regions[0].get_field_accessor(FID_YDIFFFLUX(s,d)).typeify<double>();
      RegionAccessor<AccessorType::Generic,double> fa_grad_ys = regions[2].get_field_accessor(FID_GRAD_YS(s,d)).typeify<double>();
      // See if we hit the fast path
      if (fast_calc_ydiffflux_task(fa_ds_mixavg, fa_grad_ys, fa_yspecies, fa_grad_mixmw[d], fa_avmolwt, fa_rho,
                                   fa_ydiffflux, subgrid_bounds))
        continue;
      // Otherwise do the slow version
      for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
        // accessors use DomainPoint's for now
        DomainPoint dp = DomainPoint::from_point<3>(pir.p);

        double rho = fa_rho.read(dp);
        double ds_mixavg = fa_ds_mixavg.read(dp);
        double grad_ys = fa_grad_ys.read(dp);
        double yspecies = fa_yspecies.read(dp);
        double grad_mixmw = fa_grad_mixmw[d].read(dp);
        double avmolwt = fa_avmolwt.read(dp);

        double ydiffflux = -rho * ds_mixavg * (grad_ys + yspecies * grad_mixmw * avmolwt);
      
        fa_ydiffflux.write(dp, ydiffflux);
      }
    }
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern void gpu_ydiff_flux(const int num_species, const int field_stride, const int max_elements,
                           const double *ds_mixavg_d, const double *yspecs_d,
                           const double *grad_yspecs_d, const double *grad_mixmw_d,
                           const double *avmolwt_d, const double *rho_d,
                           double *diff_flux_d);
#endif

/*static*/
void CalcYDiffFluxTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                      const std::vector<RegionRequirement> &reqs,
                                      const std::vector<PhysicalRegion> &regions,
                                      Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  Rect<3> subrect;
  ByteOffset in_offsets[3], offsets[3];

  RegionAccessor<AccessorType::Generic,double> fa_in_avmolwt = regions[2].get_field_accessor(FID_AVMOLWT).typeify<double>();
  const double *in_avmolwt_ptr = fa_in_avmolwt.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_avmolwt_ptr || (subrect != subgrid_bounds)) assert(false);

  RegionAccessor<AccessorType::Generic,double> fa_in_rho = regions[3].get_field_accessor(FID_RHO).typeify<double>();
  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw = regions[2].get_field_accessor(FID_GRAD_MIXMW(0)).typeify<double>();
  const double *in_grad_mixmw_ptr = fa_in_grad_mixmw.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_mixmw_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  off_t field_stride;
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_grad_mixmw = regions[2].get_field_accessor(FID_GRAD_MIXMW(1)).typeify<double>();
    const double *next_grad_mixmw_ptr = fa_next_grad_mixmw.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_grad_mixmw_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
    ByteOffset stride(next_grad_mixmw_ptr,in_grad_mixmw_ptr);
    field_stride = stride.offset/sizeof(double);
  }

  RegionAccessor<AccessorType::Generic,double> fa_in_ds_mixavg = regions[1].get_field_accessor(FID_DS_MIXAVG(0)).typeify<double>();
  const double *in_ds_mixavg_ptr = fa_in_ds_mixavg.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_ds_mixavg_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  {
    RegionAccessor<AccessorType::Generic,double> fa_next_ds_mixavg = regions[1].get_field_accessor(FID_DS_MIXAVG(1)).typeify<double>();
    const double *next_ds_mixavg_ptr = fa_next_ds_mixavg.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
    if(!next_ds_mixavg_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
    ByteOffset stride(next_ds_mixavg_ptr,in_ds_mixavg_ptr);
    assert(field_stride == (off_t)(stride.offset/sizeof(double)));
  }

  RegionAccessor<AccessorType::Generic,double> fa_in_yspecies = regions[2].get_field_accessor(FID_YSPECIES(0)).typeify<double>();
  const double *in_yspecies_ptr = fa_in_yspecies.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_yspecies_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  {
    RegionAccessor<AccessorType::Generic,double> fa_next_yspecies = regions[2].get_field_accessor(FID_YSPECIES(1)).typeify<double>();
    const double *next_yspecies_ptr = fa_next_yspecies.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_yspecies_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
    ByteOffset stride(next_yspecies_ptr,in_yspecies_ptr);
    assert(field_stride == (off_t)(stride.offset/sizeof(double)));
  }

  RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys = regions[2].get_field_accessor(FID_GRAD_YS(0,0)).typeify<double>();
  const double *in_grad_ys_ptr = fa_in_grad_ys.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_ys_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
  
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_grad_ys = regions[2].get_field_accessor(FID_GRAD_YS(0,1)).typeify<double>();
    const double *next_grad_ys_ptr = fa_next_grad_ys.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_grad_ys_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
    ByteOffset stride(next_grad_ys_ptr,in_grad_ys_ptr);
    assert(field_stride == (off_t)(stride.offset/sizeof(double)));
  }

  RegionAccessor<AccessorType::Generic,double> fa_out_ydiffflux = regions[0].get_field_accessor(FID_YDIFFFLUX(0,0)).typeify<double>();
  double *out_ydiffflux_ptr = fa_out_ydiffflux.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_ydiffflux_ptr || (subrect != subgrid_bounds)) assert(false);

  {
    RegionAccessor<AccessorType::Generic,double> fa_next_ydiffflux = regions[0].get_field_accessor(FID_YDIFFFLUX(0,1)).typeify<double>();
    double *next_ydiffflux_ptr = fa_next_ydiffflux.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_ydiffflux_ptr || (subrect != subgrid_bounds)) assert(false);
    ByteOffset stride(next_ydiffflux_ptr,out_ydiffflux_ptr);
    assert(field_stride == (off_t)(stride.offset/sizeof(double)));
  }

  // Now we can do the actual math
  size_t n_pts = subgrid_bounds.volume();

  // Use this count of number of species because we might not be
  // computing ydiffflux for all the species.  Sometimes the last
  // species is omitted and computed by 1 - (sum of all other species)
  const int num_species = reqs[0].privilege_fields.size()/3;

  gpu_ydiff_flux(num_species, field_stride, n_pts,
                 in_ds_mixavg_ptr, in_yspecies_ptr,
                 in_grad_ys_ptr, in_grad_mixmw_ptr,
                 in_avmolwt_ptr, in_rho_ptr,
                 out_ydiffflux_ptr);
#endif
#endif
}

CalcYDiffFluxFieldTask::CalcYDiffFluxFieldTask(S3DRank *r, int s,
                       Domain domain,
                       TaskArgument global_arg,
                       ArgumentMap arg_map,
                       bool redundant,
                       Predicate pred,
                       bool must,
                       MapperID id,
                       MappingTagID tag,
                       bool add_requirements)
 : IndexLauncher(CalcYDiffFluxFieldTask::TASK_ID,
                 domain, global_arg, arg_map, pred, must,
                 id, tag | CalcYDiffFluxFieldTask::MAPPER_TAG), rank(r)
{
  species.push_back(s);
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
#ifdef REDUNDANT_WORK 
    if (redundant) {
      for (int d = 0; d < 3; d++)
        rr_out.add_field(FID_YDIFFFLUX_ALT(s, d));
      // put this on the GPU if redundant
      this->tag |= (RHSF_MAPPER_MIXED_GPU | RHSF_MAPPER_ALL_GPU);  
    } else {
      for (int d = 0; d < 3; d++)
        rr_out.add_field(FID_YDIFFFLUX(s, d));
    }
#else
    assert(!redundant);
    for (int d = 0; d < 3; d++)
      rr_out.add_field(FID_YDIFFFLUX(s, d));
#endif
    add_region_requirement(rr_out);

    RegionRequirement rr_state(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_state.add_field(FID_DS_MIXAVG(s));
    add_region_requirement(rr_state);

    RegionRequirement rr_in(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    rr_in.add_field(FID_YSPECIES(s));
    for (int d = 0; d < 3; d++)
      rr_in.add_field(FID_GRAD_YS(s, d));
    add_region_requirement(rr_in);

    // Break these out in a separate region requirement so any instances we make
    // can be reused by other diff flux tasks operating on different fields
    RegionRequirement rr_in2(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    for (int d = 0; d < 3; d++)
      rr_in2.add_field(FID_GRAD_MIXMW(d));
#ifdef REDUNDANT_WORK
    if (redundant)
      rr_in2.add_field(FID_AVMOLWT_ALT);
    else
      rr_in2.add_field(FID_AVMOLWT);
#else
    assert(!redundant);
    rr_in2.add_field(FID_AVMOLWT);
#endif
    add_region_requirement(rr_in2);

    RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
    rr_q.add_field(FID_RHO);
    add_region_requirement(rr_q);
  }
}

CalcYDiffFluxFieldTask::CalcYDiffFluxFieldTask(S3DRank *r,
                        const std::vector<int> &specs,
                        Domain domain,
                        TaskArgument global_arg,
                        ArgumentMap arg_map,
                        bool redundant,
                        Predicate pred,
                        bool must,
                        MapperID id,
                        MappingTagID tag,
                        bool add_requirements)
 : IndexLauncher(CalcYDiffFluxFieldTask::TASK_ID,
                 domain, global_arg, arg_map, pred, must,
                 id, tag | CalcYDiffFluxFieldTask::MAPPER_TAG), rank(r), species(specs)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
#ifdef REDUNDANT_WORK 
    if (redundant) {
      for (unsigned idx = 0; idx < specs.size(); idx++) {
        for (int d = 0; d < 3; d++)
          rr_out.add_field(FID_YDIFFFLUX_ALT(specs[idx], d));
      }
      // put this on the GPU if redundant
      this->tag |= (RHSF_MAPPER_MIXED_GPU | RHSF_MAPPER_ALL_GPU);  
    } else {
      for (unsigned idx = 0; idx < specs.size(); idx++) {
        for (int d = 0; d < 3; d++)
          rr_out.add_field(FID_YDIFFFLUX(specs[idx], d));
      }
    }
#else
    assert(!redundant);
    for (unsigned idx = 0; idx < specs.size(); idx++) {
      for (int d = 0; d < 3; d++)
        rr_out.add_field(FID_YDIFFFLUX(specs[idx], d));
    }
#endif
    add_region_requirement(rr_out);

    RegionRequirement rr_state(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    for (unsigned idx = 0; idx < specs.size(); idx++)
      rr_state.add_field(FID_DS_MIXAVG(specs[idx]));
    add_region_requirement(rr_state);

    RegionRequirement rr_in(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    for (unsigned idx = 0; idx < specs.size(); idx++)
    {
      rr_in.add_field(FID_YSPECIES(specs[idx]));
      for (int d = 0; d < 3; d++)
        rr_in.add_field(FID_GRAD_YS(specs[idx], d));
    }
    add_region_requirement(rr_in);

    // Break these out in a separate region requirement so any instances we make
    // can be reused by other diff flux tasks operating on different fields
    RegionRequirement rr_in2(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    for (int d = 0; d < 3; d++)
      rr_in2.add_field(FID_GRAD_MIXMW(d));
#ifdef REDUNDANT_WORK
    if (redundant)
      rr_in2.add_field(FID_AVMOLWT_ALT);
    else
      rr_in2.add_field(FID_AVMOLWT);
#else
    assert(!redundant);
    rr_in2.add_field(FID_AVMOLWT);
#endif
    add_region_requirement(rr_in2);

    RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
    rr_q.add_field(FID_RHO);
    add_region_requirement(rr_q);
  }
}

void CalcYDiffFluxFieldTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  for (unsigned idx = 0; idx < species.size(); idx++)
  {
    for (int d = 0; d < 3; d++)
      check_field(runtime, ctx, rank->proc_grid_bounds, rank->is_grid, 
                  rank->ip_top, rank->lr_int, FID_YDIFFFLUX(species[idx], d),
                  rank, &S3DRank::RHSFArrays::ydiffflux, d * rank->n_spec + species[idx], 
                  1e-15, 1e8, 0, "Ydiffflux[%d,%d]", species[idx], d);
  }
}

/*static*/
const char * const CalcYDiffFluxFieldTask::TASK_NAME = "calc_ydiffflux_per_field";

/*static*/
bool CalcYDiffFluxFieldTask::dense_calc_ydiffflux_task(const Rect<3> &subgrid_bounds,
                      RegionAccessor<AccessorType::Generic,double> fa_in_rho,
                      RegionAccessor<AccessorType::Generic,double> fa_in_avmolwt,
                      RegionAccessor<AccessorType::Generic,double> fa_in_yspec,
                      RegionAccessor<AccessorType::Generic,double> fa_in_ds_mixavg,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw_x,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw_y,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw_z,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys_x,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys_y,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys_z,
                      RegionAccessor<AccessorType::Generic,double> fa_out_flux_x,
                      RegionAccessor<AccessorType::Generic,double> fa_out_flux_y,
                      RegionAccessor<AccessorType::Generic,double> fa_out_flux_z)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], offsets[3];

  const double *in_avmolwt_ptr = fa_in_avmolwt.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_avmolwt_ptr || (subrect != subgrid_bounds) ||
     !offsets_are_dense<3,double>(subgrid_bounds, in_offsets)) return false;
  
  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_yspec_ptr = fa_in_yspec.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_yspec_ptr || (subrect != subgrid_bounds) || (offset_mismatch(3, in_offsets, offsets))) return false;

  const double *in_ds_mixavg_ptr = fa_in_ds_mixavg.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_ds_mixavg_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_grad_mixmw_x_ptr = fa_in_grad_mixmw_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_mixmw_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_grad_mixmw_y_ptr = fa_in_grad_mixmw_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_mixmw_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_grad_mixmw_z_ptr = fa_in_grad_mixmw_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_mixmw_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_grad_ys_x_ptr = fa_in_grad_ys_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_ys_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_grad_ys_y_ptr = fa_in_grad_ys_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_ys_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_grad_ys_z_ptr = fa_in_grad_ys_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_ys_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_flux_x_ptr = fa_out_flux_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_flux_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_flux_y_ptr = fa_out_flux_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_flux_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_flux_z_ptr = fa_out_flux_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_flux_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

#ifdef BULLDOZER
// Fit in L1 and only take up the first
// set to maximize available associativity
#define FULL_STRIP_SIZE 256 
#else
#define FULL_STRIP_SIZE 1024
#endif

#if defined(USE_AVX_KERNELS)
  __m256d avx_yspec_av[FULL_STRIP_SIZE/4];
  __m256d avx_rho_ds[FULL_STRIP_SIZE/4];
#elif defined(USE_SSE_KERNELS)
  __m128d sse_yspec_av[FULL_STRIP_SIZE/2];
  __m128d sse_rho_ds[FULL_STRIP_SIZE/2];
#endif

  double yspec_av[FULL_STRIP_SIZE];
  double rho_ds[FULL_STRIP_SIZE];

  size_t n_pts = subgrid_bounds.volume();

  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined(USE_AVX_KERNELS)
      // Check for alignment on inputs
      if (aligned<32>(in_avmolwt_ptr) && aligned<32>(in_yspec_ptr) &&
          aligned<32>(in_rho_ptr) && aligned<32>(in_ds_mixavg_ptr)) {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d avmolwt = _mm256_load_pd(in_avmolwt_ptr+(i<<2));
          __m256d yspec = _mm256_load_pd(in_yspec_ptr+(i<<2));
          avx_yspec_av[i] = _mm256_mul_pd(avmolwt,yspec);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d rho = _mm256_load_pd(in_rho_ptr+(i<<2));
          // Fast negation
          rho = _mm256_xor_pd(rho,_mm256_set1_pd(-0.0));
          __m256d ds_mixavg = _mm256_load_pd(in_ds_mixavg_ptr+(i<<2));
          avx_rho_ds[i] = _mm256_mul_pd(rho,ds_mixavg);
        }
      } else {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d avmolwt = _mm256_loadu_pd(in_avmolwt_ptr+(i<<2));
          __m256d yspec = _mm256_loadu_pd(in_yspec_ptr+(i<<2));
          avx_yspec_av[i] = _mm256_mul_pd(avmolwt,yspec);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d rho = _mm256_loadu_pd(in_rho_ptr+(i<<2));
          // Fast negation
          rho = _mm256_xor_pd(rho,_mm256_set1_pd(-0.0));
          __m256d ds_mixavg = _mm256_loadu_pd(in_ds_mixavg_ptr+(i<<2));
          avx_rho_ds[i] = _mm256_mul_pd(rho,ds_mixavg);
        }
      }
      if (aligned<32>(out_flux_x_ptr) && aligned<32>(out_flux_y_ptr) &&
          aligned<32>(out_flux_z_ptr) && aligned<32>(in_grad_ys_x_ptr) &&
          aligned<32>(in_grad_ys_y_ptr) && aligned<32>(in_grad_ys_z_ptr) &&
          aligned<32>(in_grad_mixmw_x_ptr) && aligned<32>(in_grad_mixmw_y_ptr) &&
          aligned<32>(in_grad_mixmw_z_ptr)) {
        // X direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_ys = _mm256_load_pd(in_grad_ys_x_ptr+(i<<2));
          __m256d grad_mixmw = _mm256_load_pd(in_grad_mixmw_x_ptr+(i<<2));
          __m256d flux = _mm256_mul_pd(avx_rho_ds[i],_mm256_add_pd(grad_ys,
                _mm256_mul_pd(avx_yspec_av[i],grad_mixmw)));
          _mm256_stream_pd(out_flux_x_ptr+(i<<2),flux);
        }
        // Y direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_ys = _mm256_load_pd(in_grad_ys_y_ptr+(i<<2));
          __m256d grad_mixmw = _mm256_load_pd(in_grad_mixmw_y_ptr+(i<<2));
          __m256d flux = _mm256_mul_pd(avx_rho_ds[i],_mm256_add_pd(grad_ys,
                _mm256_mul_pd(avx_yspec_av[i],grad_mixmw)));
          _mm256_stream_pd(out_flux_y_ptr+(i<<2),flux);
        }
        // Z direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_ys = _mm256_load_pd(in_grad_ys_z_ptr+(i<<2));
          __m256d grad_mixmw = _mm256_load_pd(in_grad_mixmw_z_ptr+(i<<2));
          __m256d flux = _mm256_mul_pd(avx_rho_ds[i],_mm256_add_pd(grad_ys,
                _mm256_mul_pd(avx_yspec_av[i],grad_mixmw)));
          _mm256_stream_pd(out_flux_z_ptr+(i<<2),flux);
        }
      } else {
        // X direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_ys = _mm256_loadu_pd(in_grad_ys_x_ptr+(i<<2));
          __m256d grad_mixmw = _mm256_loadu_pd(in_grad_mixmw_x_ptr+(i<<2));
          __m256d flux = _mm256_mul_pd(avx_rho_ds[i],_mm256_add_pd(grad_ys,
                _mm256_mul_pd(avx_yspec_av[i],grad_mixmw)));
          _mm256_storeu_pd(out_flux_x_ptr+(i<<2),flux);
        }
        // Y direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_ys = _mm256_loadu_pd(in_grad_ys_y_ptr+(i<<2));
          __m256d grad_mixmw = _mm256_loadu_pd(in_grad_mixmw_y_ptr+(i<<2));
          __m256d flux = _mm256_mul_pd(avx_rho_ds[i],_mm256_add_pd(grad_ys,
                _mm256_mul_pd(avx_yspec_av[i],grad_mixmw)));
          _mm256_storeu_pd(out_flux_y_ptr+(i<<2),flux);
        }
        // Z direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_ys = _mm256_loadu_pd(in_grad_ys_z_ptr+(i<<2));
          __m256d grad_mixmw = _mm256_loadu_pd(in_grad_mixmw_z_ptr+(i<<2));
          __m256d flux = _mm256_mul_pd(avx_rho_ds[i],_mm256_add_pd(grad_ys,
                _mm256_mul_pd(avx_yspec_av[i],grad_mixmw)));
          _mm256_storeu_pd(out_flux_z_ptr+(i<<2),flux);
        }
      }
#elif defined(USE_SSE_KERNELS)
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d avmolwt = _mm_stream_load_pd(in_avmolwt_ptr+(i<<1));
        __m128d yspec = _mm_stream_load_pd(in_yspec_ptr+(i<<1));
        sse_yspec_av[i] = _mm_mul_pd(avmolwt,yspec);
      }
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) { 
        __m128d rho = _mm_stream_load_pd(in_rho_ptr+(i<<1));
        // Fast negation
        rho = _mm_xor_pd(rho,_mm_set1_pd(-0.0));
        __m128d ds_mixavg = _mm_stream_load_pd(in_ds_mixavg_ptr+(i<<1));
        sse_rho_ds[i] = _mm_mul_pd(rho, ds_mixavg);
      }
      // X direction
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d grad_ys = _mm_stream_load_pd(in_grad_ys_x_ptr+(i<<1));
        __m128d grad_mixmw = _mm_stream_load_pd(in_grad_mixmw_x_ptr+(i<<1));
        __m128d flux = _mm_mul_pd(sse_rho_ds[i],_mm_add_pd(grad_ys,
              _mm_mul_pd(sse_yspec_av[i],grad_mixmw)));
        _mm_stream_pd(out_flux_x_ptr+(i<<1),flux); 
      }
      // Y direction
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d grad_ys = _mm_stream_load_pd(in_grad_ys_y_ptr+(i<<1));
        __m128d grad_mixmw = _mm_stream_load_pd(in_grad_mixmw_y_ptr+(i<<1));
        __m128d flux = _mm_mul_pd(sse_rho_ds[i],_mm_add_pd(grad_ys,
              _mm_mul_pd(sse_yspec_av[i],grad_mixmw)));
        _mm_stream_pd(out_flux_y_ptr+(i<<1),flux); 
      }
      // Z direction
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d grad_ys = _mm_stream_load_pd(in_grad_ys_z_ptr+(i<<1));
        __m128d grad_mixmw = _mm_stream_load_pd(in_grad_mixmw_z_ptr+(i<<1));
        __m128d flux = _mm_mul_pd(sse_rho_ds[i],_mm_add_pd(grad_ys,
              _mm_mul_pd(sse_yspec_av[i],grad_mixmw)));
        _mm_stream_pd(out_flux_z_ptr+(i<<1),flux); 
      }
#else
      for (int i = 0; i < FULL_STRIP_SIZE; i++)
        yspec_av[i] = in_avmolwt_ptr[i] * in_yspec_ptr[i];
      for (int i = 0; i < FULL_STRIP_SIZE; i++)
        rho_ds[i] = -(in_rho_ptr[i]) * in_ds_mixavg_ptr[i];
      // X direction
      for (int i = 0; i < FULL_STRIP_SIZE; i++)
      {
        double grad_ys = in_grad_ys_x_ptr[i];
        double grad_mixmw = in_grad_mixmw_x_ptr[i];
        double flux = rho_ds[i] * (grad_ys + yspec_av[i] * grad_mixmw);
        out_flux_x_ptr[i] = flux;
      }
      // Y direction
      for (int i = 0; i < FULL_STRIP_SIZE; i++)
      {
        double grad_ys = in_grad_ys_y_ptr[i];
        double grad_mixmw = in_grad_mixmw_y_ptr[i];
        double flux = rho_ds[i] * (grad_ys + yspec_av[i] * grad_mixmw);
        out_flux_y_ptr[i] = flux;
      }
      // Z direction
      for (int i = 0; i < FULL_STRIP_SIZE; i++)
      {
        double grad_ys = in_grad_ys_z_ptr[i];
        double grad_mixmw = in_grad_mixmw_z_ptr[i];
        double flux = rho_ds[i] * (grad_ys + yspec_av[i] * grad_mixmw);
        out_flux_z_ptr[i] = flux;
      }
#endif
      n_pts -= FULL_STRIP_SIZE;
    } else {
      for (size_t i = 0; i < n_pts; i++)
        yspec_av[i] = in_avmolwt_ptr[i] * in_yspec_ptr[i];
      for (size_t i = 0; i < n_pts; i++)
        rho_ds[i] = -(in_rho_ptr[i]) * in_ds_mixavg_ptr[i];
      // X direction
      for (size_t i = 0; i < n_pts; i++)
      {
        double grad_ys = in_grad_ys_x_ptr[i];
        double grad_mixmw = in_grad_mixmw_x_ptr[i];
        double flux = rho_ds[i] * (grad_ys + yspec_av[i] * grad_mixmw);
        out_flux_x_ptr[i] = flux;
      }
      // Y direction
      for (size_t i = 0; i < n_pts; i++)
      {
        double grad_ys = in_grad_ys_y_ptr[i];
        double grad_mixmw = in_grad_mixmw_y_ptr[i];
        double flux = rho_ds[i] * (grad_ys + yspec_av[i] * grad_mixmw);
        out_flux_y_ptr[i] = flux;
      }
      // Z direction
      for (size_t i = 0; i < n_pts; i++)
      {
        double grad_ys = in_grad_ys_z_ptr[i];
        double grad_mixmw = in_grad_mixmw_z_ptr[i];
        double flux = rho_ds[i] * (grad_ys + yspec_av[i] * grad_mixmw);
        out_flux_z_ptr[i] = flux;
      }
      n_pts = 0;
    }
    in_avmolwt_ptr += FULL_STRIP_SIZE;
    in_rho_ptr += FULL_STRIP_SIZE;
    in_yspec_ptr += FULL_STRIP_SIZE;
    in_ds_mixavg_ptr += FULL_STRIP_SIZE;
    in_grad_mixmw_x_ptr += FULL_STRIP_SIZE;
    in_grad_mixmw_y_ptr += FULL_STRIP_SIZE;
    in_grad_mixmw_z_ptr += FULL_STRIP_SIZE;
    in_grad_ys_x_ptr += FULL_STRIP_SIZE;
    in_grad_ys_y_ptr += FULL_STRIP_SIZE;
    in_grad_ys_z_ptr += FULL_STRIP_SIZE;
    out_flux_x_ptr += FULL_STRIP_SIZE;
    out_flux_y_ptr += FULL_STRIP_SIZE;
    out_flux_z_ptr += FULL_STRIP_SIZE;
  }
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
#undef FULL_STRIP_SIZE
}

/*static*/
void CalcYDiffFluxFieldTask::cpu_base_impl(S3DRank *rank, const Task *task,
                                           const Rect<3> &subgrid_bounds,
                                           const std::vector<RegionRequirement> &reqs,
                                           const std::vector<PhysicalRegion> &regions,
                                           Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  RegionAccessor<AccessorType::Generic,double> fa_rho = 
    regions[4].get_field_accessor(FID_RHO).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_avmolwt = 
    regions[3].get_field_accessor(reqs[3].instance_fields[3]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_mixmw_x = 
    regions[3].get_field_accessor(reqs[3].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_mixmw_y =
    regions[3].get_field_accessor(reqs[3].instance_fields[1]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_mixmw_z = 
    regions[3].get_field_accessor(reqs[3].instance_fields[2]).typeify<double>();

  const int num_species = (task->regions[0].instance_fields.size()/3); 
  for (int spec = 0; spec < num_species; spec++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_yspec = 
      regions[2].get_field_accessor(reqs[2].instance_fields[4*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_grad_ys_x = 
      regions[2].get_field_accessor(reqs[2].instance_fields[4*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_grad_ys_y = 
      regions[2].get_field_accessor(reqs[2].instance_fields[4*spec+2]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_grad_ys_z = 
      regions[2].get_field_accessor(reqs[2].instance_fields[4*spec+3]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_ds_mixavg = 
      regions[1].get_field_accessor(reqs[1].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_flux_x = 
      regions[0].get_field_accessor(reqs[0].instance_fields[3*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_flux_y = 
      regions[0].get_field_accessor(reqs[0].instance_fields[3*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_flux_z = 
      regions[0].get_field_accessor(reqs[0].instance_fields[3*spec+2]).typeify<double>();

    if (dense_calc_ydiffflux_task(subgrid_bounds, fa_rho, fa_avmolwt, fa_yspec,
          fa_ds_mixavg, fa_grad_mixmw_x, fa_grad_mixmw_y, fa_grad_mixmw_z,
          fa_grad_ys_x, fa_grad_ys_y, fa_grad_ys_z, fa_flux_x,
          fa_flux_y, fa_flux_z))
      continue;

    for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);
      double rho = fa_rho.read(dp);
      double ds_mixavg = fa_ds_mixavg.read(dp);
      double yspec = fa_yspec.read(dp);
      double avmolwt = fa_avmolwt.read(dp);

      // X direction
      {
        double grad_ys = fa_grad_ys_x.read(dp);
        double grad_mixmw = fa_grad_mixmw_x.read(dp);
        double flux = -rho * ds_mixavg * (grad_ys + yspec * grad_mixmw * avmolwt);
        fa_flux_x.write(dp, flux);
      }

      // Y direction
      {
        double grad_ys = fa_grad_ys_y.read(dp);
        double grad_mixmw = fa_grad_mixmw_y.read(dp);
        double flux = -rho * ds_mixavg * (grad_ys + yspec * grad_mixmw * avmolwt);
        fa_flux_y.write(dp, flux);
      }

      // Z direction
      {
        double grad_ys = fa_grad_ys_z.read(dp);
        double grad_mixmw = fa_grad_mixmw_z.read(dp);
        double flux = -rho * ds_mixavg * (grad_ys + yspec * grad_mixmw * avmolwt);
        fa_flux_z.write(dp, flux);
      }
    }
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern
void gpu_field_ydiff_flux(const int max_elements,
                          const double *rho_d,
                          const double *avmolwt_d,
                          const double *yspec_d,
                          const double *ds_mixavg_d,
                          const double *grad_ys_x_d,
                          const double *grad_ys_y_d,
                          const double *grad_ys_z_d,
                          const double *grad_mixmw_x_d,
                          const double *grad_mixmw_y_d,
                          const double *grad_mixmw_z_d,
                          double *diff_flux_x_d,
                          double *diff_flux_y_d,
                          double *diff_flux_z_d);
#endif

/*static*/
void CalcYDiffFluxFieldTask::gpu_base_impl(S3DRank *rank, const Task *task,
                                           const Rect<3> &subgrid_bounds,
                                           const std::vector<RegionRequirement> &reqs,
                                           const std::vector<PhysicalRegion> &regions,
                                           Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  RegionAccessor<AccessorType::Generic,double> fa_in_rho = 
    regions[4].get_field_accessor(FID_RHO).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_avmolwt = 
    regions[3].get_field_accessor(reqs[3].instance_fields[3]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw_x = 
    regions[3].get_field_accessor(reqs[3].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw_y =
    regions[3].get_field_accessor(reqs[3].instance_fields[1]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw_z = 
    regions[3].get_field_accessor(reqs[3].instance_fields[2]).typeify<double>();

  Rect<3> subrect;
  ByteOffset in_offsets[3], offsets[3];

  const double *in_avmolwt_ptr = fa_in_avmolwt.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_avmolwt_ptr || (subrect != subgrid_bounds) ||
     !offsets_are_dense<3,double>(subgrid_bounds, in_offsets)) assert(false);
  
  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const double *in_grad_mixmw_x_ptr = fa_in_grad_mixmw_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_mixmw_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const double *in_grad_mixmw_y_ptr = fa_in_grad_mixmw_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_mixmw_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const double *in_grad_mixmw_z_ptr = fa_in_grad_mixmw_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_mixmw_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const int num_species = (task->regions[0].instance_fields.size()/3); 

  for (int spec = 0; spec < num_species; spec++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_in_yspec = 
      regions[2].get_field_accessor(reqs[2].instance_fields[4*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys_x = 
      regions[2].get_field_accessor(reqs[2].instance_fields[4*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys_y = 
      regions[2].get_field_accessor(reqs[2].instance_fields[4*spec+2]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys_z = 
      regions[2].get_field_accessor(reqs[2].instance_fields[4*spec+3]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_in_ds_mixavg = 
      regions[1].get_field_accessor(reqs[1].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_out_flux_x = 
      regions[0].get_field_accessor(reqs[0].instance_fields[3*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_out_flux_y = 
      regions[0].get_field_accessor(reqs[0].instance_fields[3*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_out_flux_z = 
      regions[0].get_field_accessor(reqs[0].instance_fields[3*spec+2]).typeify<double>();

    const double *in_yspec_ptr = fa_in_yspec.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_yspec_ptr || (subrect != subgrid_bounds) || (offset_mismatch(3, in_offsets, offsets))) assert(false);

    const double *in_ds_mixavg_ptr = fa_in_ds_mixavg.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_ds_mixavg_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    const double *in_grad_ys_x_ptr = fa_in_grad_ys_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_grad_ys_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    const double *in_grad_ys_y_ptr = fa_in_grad_ys_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_grad_ys_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    const double *in_grad_ys_z_ptr = fa_in_grad_ys_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_grad_ys_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    double *out_flux_x_ptr = fa_out_flux_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_flux_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    double *out_flux_y_ptr = fa_out_flux_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_flux_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    double *out_flux_z_ptr = fa_out_flux_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_flux_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    // Now we can do the actual math
    size_t n_pts = subgrid_bounds.volume();

    gpu_field_ydiff_flux(n_pts, in_rho_ptr, in_avmolwt_ptr, in_yspec_ptr,
                         in_ds_mixavg_ptr, in_grad_ys_x_ptr, in_grad_ys_y_ptr,
                         in_grad_ys_z_ptr, in_grad_mixmw_x_ptr, in_grad_mixmw_y_ptr,
                         in_grad_mixmw_z_ptr, out_flux_x_ptr, out_flux_y_ptr,
                         out_flux_z_ptr);
  }
#endif
#endif
}

CalcHeatFluxTask::CalcHeatFluxTask(S3DRank *r,
                                   Domain domain,
                                   TaskArgument global_arg,
                                   ArgumentMap arg_map,
                                   Predicate pred,
                                   bool must,
                                   MapperID id,
                                   MappingTagID tag,
                                   bool add_requirements)
  : IndexLauncher(CalcHeatFluxTask::TASK_ID, domain, global_arg, 
                  arg_map, pred, must, id, tag | CalcHeatFluxTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    for(int d = 0; d < 3; d++) {
      rr_out.add_field(FID_HEATFLUX(d));
      rr_out.add_field(FID_YDIFFFLUX(rank->n_spec - 1, d));
    }
    add_region_requirement(rr_out);

    RegionRequirement rr_state(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_state.add_field(FID_TEMP);
    rr_state.add_field(FID_LAMBDA);
    add_region_requirement(rr_state);

    RegionRequirement rr_in(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    for(int d = 0; d < 3; d++) {
      rr_in.add_field(FID_GRAD_T(d));
      for(int i = 0; i < rank->n_spec - 1; i++)
	rr_in.add_field(FID_YDIFFFLUX(i,d));
    }
    add_region_requirement(rr_in);

    RegionRequirement rr_hspec(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    for(int i = 0; i < rank->n_spec; i++)
      rr_hspec.add_field(FID_H_SPEC(i));
    add_region_requirement(rr_hspec);
  }
}

void CalcHeatFluxTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  for(int s = 0; s < rank->n_spec; s++)
    check_field(runtime, ctx, rank->proc_grid_bounds, 
                rank->is_grid, rank->ip_top, rank->lr_int, FID_H_SPEC(s),
                rank, &S3DRank::RHSFArrays::h_spec, s, 1e-10, 1e10, 0, "h_spec[%d]", s);
  for(int d = 0; d < 3; d++)
    check_field(runtime, ctx, rank->proc_grid_bounds, 
                rank->is_grid, rank->ip_top, rank->lr_int, FID_YDIFFFLUX(rank->n_spec-1, d),
                rank, &S3DRank::RHSFArrays::ydiffflux, d * rank->n_spec + (rank->n_spec - 1), 1e-15, 1e8, 0, 
                "Ydiffflux[%d,%d]", rank->n_spec - 1, d);
  for(int d = 0; d < 3; d++)
    check_field(runtime, ctx, rank->proc_grid_bounds, 
                rank->is_grid, rank->ip_top, rank->lr_int, FID_HEATFLUX(d),
                rank, &S3DRank::RHSFArrays::heatflux, d, 1e-10, 1e8, 0, "heatflux[%d]", d);
}

/*static*/ const char * const CalcHeatFluxTask::TASK_NAME = "calc_heatflux";

/*static*/
void CalcHeatFluxTask::dense_calc_heatflux_task(size_t n_pts,
				       S3DRank::CPETable& cpe_data,
				       size_t n_spec,
				       const double *in_grad_t_x,
				       const double *in_grad_t_y,
				       const double *in_grad_t_z,
				       const double *in_lambda,
				       const double *in_temp,
				       double *out_h_spec, 
                                       off_t h_spec_stride,
				       const double *in_ydiffflux_x,
				       const double *in_ydiffflux_y,
				       const double *in_ydiffflux_z,
				       off_t ydiffflux_stride,
				       double *out_heatflux_x,
				       double *out_heatflux_y,
				       double *out_heatflux_z,
				       double *out_ydiffflux_last_x,
				       double *out_ydiffflux_last_y,
				       double *out_ydiffflux_last_z)
{
#ifdef BULLDOZER
// Fit in L1 and consume only the
// first way in each set
#define FULL_STRIP_SIZE 64 
#else
// size to fit in L1 - 32KB -> 4K doubles
#define FULL_STRIP_SIZE 256 
#endif

#if defined(USE_AVX_KERNELS)
  __m256d heatflux_x[FULL_STRIP_SIZE/4];
  __m256d heatflux_y[FULL_STRIP_SIZE/4];
  __m256d heatflux_z[FULL_STRIP_SIZE/4];
  __m256d lastflux_x[FULL_STRIP_SIZE/4];
  __m256d lastflux_y[FULL_STRIP_SIZE/4];
  __m256d lastflux_z[FULL_STRIP_SIZE/4];
  double temp[FULL_STRIP_SIZE];
#elif defined(USE_SSE_KERNELS)
  __m128d heatflux_x[FULL_STRIP_SIZE/2];
  __m128d heatflux_y[FULL_STRIP_SIZE/2];
  __m128d heatflux_z[FULL_STRIP_SIZE/2];
  __m128d lastflux_x[FULL_STRIP_SIZE/2];
  __m128d lastflux_y[FULL_STRIP_SIZE/2];
  __m128d lastflux_z[FULL_STRIP_SIZE/2];
  double temp[FULL_STRIP_SIZE];
#endif
  int enth_idx[FULL_STRIP_SIZE];  // must be at least as large as strip size
  double temp_hi = 3500.0 / T_REF;
  double temp_lo = 250.0 / T_REF;
  assert(cpe_data.num_pts > 0);
  double temp_step_inv = (cpe_data.num_pts - 1) / (temp_hi - temp_lo);

  while(n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE)
    {
#if defined(USE_AVX_KERNELS)
      if (aligned<32>(out_heatflux_x) && aligned<32>(out_heatflux_y) && aligned<32>(out_heatflux_z) &&
          aligned<32>(out_ydiffflux_last_x) && aligned<32>(out_ydiffflux_last_y) && aligned<32>(out_ydiffflux_last_z) &&
          aligned<32>(in_lambda) && aligned<32>(in_grad_t_x) && aligned<32>(in_grad_t_y) && aligned<32>(in_grad_t_z) &&
          aligned<32>(in_ydiffflux_x) && aligned<32>(in_ydiffflux_y) && aligned<32>(in_ydiffflux_z)) {
        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d lambda = _mm256_load_pd(in_lambda+(i<<2));
          __m256d gradtx = _mm256_load_pd(in_grad_t_x+(i<<2));
          __m256d gradty = _mm256_load_pd(in_grad_t_y+(i<<2));
          __m256d gradtz = _mm256_load_pd(in_grad_t_z+(i<<2));
          // Fast negation of lambda
          lambda = _mm256_xor_pd(lambda,_mm256_set1_pd(-0.0));
          heatflux_x[i] = _mm256_mul_pd(lambda,gradtx);
          heatflux_y[i] = _mm256_mul_pd(lambda,gradty);
          heatflux_z[i] = _mm256_mul_pd(lambda,gradtz);
          // Load the last flux values
          lastflux_x[i] = _mm256_set1_pd(0.0);
          lastflux_y[i] = _mm256_set1_pd(0.0);
          lastflux_z[i] = _mm256_set1_pd(0.0);
        }

        // Do the bucket computation like normal
        for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
          temp[i] = in_temp[i];
          int bucket = (temp[i] - temp_lo) * temp_step_inv;
          assert((bucket >= 0) && (bucket < cpe_data.num_pts));
          enth_idx[i] = bucket * n_spec;
        }

        double *h_spec_s = out_h_spec;
        const double *ydiff_x_s = in_ydiffflux_x;
        const double *ydiff_y_s = in_ydiffflux_y;
        const double *ydiff_z_s = in_ydiffflux_z;

        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d ydiff_x = _mm256_load_pd(ydiff_x_s+(i<<2));
          __m256d ydiff_y = _mm256_load_pd(ydiff_y_s+(i<<2));
          __m256d ydiff_z = _mm256_load_pd(ydiff_z_s+(i<<2));
          int idx0 = enth_idx[(i<<2)]++;
          int idx1 = enth_idx[(i<<2)+1]++;
          int idx2 = enth_idx[(i<<2)+2]++;
          int idx3 = enth_idx[(i<<2)+3]++;
          double h_spec0 = (cpe_data.enth_aa[idx0] * temp[(i<<2)]  + cpe_data.enth_bb[idx0]);
          double h_spec1 = (cpe_data.enth_aa[idx1] * temp[(i<<2)+1] + cpe_data.enth_bb[idx1]);
          double h_spec2 = (cpe_data.enth_aa[idx2] * temp[(i<<2)+2] + cpe_data.enth_bb[idx2]);
          double h_spec3 = (cpe_data.enth_aa[idx3] * temp[(i<<2)+3] + cpe_data.enth_bb[idx3]);
          __m256d h_spec = _mm256_set_pd(h_spec3,h_spec2,h_spec1,h_spec0);
          // Write out the h_spec value
          _mm256_stream_pd(h_spec_s+(i<<2),h_spec);
          heatflux_x[i] = _mm256_add_pd(heatflux_x[i],_mm256_mul_pd(h_spec, ydiff_x));
          heatflux_y[i] = _mm256_add_pd(heatflux_y[i],_mm256_mul_pd(h_spec, ydiff_y));
          heatflux_z[i] = _mm256_add_pd(heatflux_z[i],_mm256_mul_pd(h_spec, ydiff_z));
          // Set last flux to be negative of each ydiff
          lastflux_x[i] = _mm256_xor_pd(ydiff_x,_mm256_set1_pd(-0.0));
          lastflux_y[i] = _mm256_xor_pd(ydiff_y,_mm256_set1_pd(-0.0));
          lastflux_z[i] = _mm256_xor_pd(ydiff_z,_mm256_set1_pd(-0.0));
        }

        for(size_t s = 1; s < (n_spec - 1); s++) {
          h_spec_s += ByteOffset(h_spec_stride);
          ydiff_x_s += ByteOffset(ydiffflux_stride);
          ydiff_y_s += ByteOffset(ydiffflux_stride);
          ydiff_z_s += ByteOffset(ydiffflux_stride);

          for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) { 
            __m256d ydiff_x = _mm256_load_pd(ydiff_x_s+(i<<2));
            __m256d ydiff_y = _mm256_load_pd(ydiff_y_s+(i<<2));
            __m256d ydiff_z = _mm256_load_pd(ydiff_z_s+(i<<2));
            int idx0 = enth_idx[(i<<2)]++;
            int idx1 = enth_idx[(i<<2)+1]++;
            int idx2 = enth_idx[(i<<2)+2]++;
            int idx3 = enth_idx[(i<<2)+3]++;
            double h_spec0 = (cpe_data.enth_aa[idx0] * temp[(i<<2)]  + cpe_data.enth_bb[idx0]);
            double h_spec1 = (cpe_data.enth_aa[idx1] * temp[(i<<2)+1] + cpe_data.enth_bb[idx1]);
            double h_spec2 = (cpe_data.enth_aa[idx2] * temp[(i<<2)+2] + cpe_data.enth_bb[idx2]);
            double h_spec3 = (cpe_data.enth_aa[idx3] * temp[(i<<2)+3] + cpe_data.enth_bb[idx3]);
            __m256d h_spec = _mm256_set_pd(h_spec3,h_spec2,h_spec1,h_spec0);
            // Write out the h_spec value
            _mm256_stream_pd(h_spec_s+(i<<2),h_spec);
            heatflux_x[i] = _mm256_add_pd(heatflux_x[i],_mm256_mul_pd(h_spec, ydiff_x));
            heatflux_y[i] = _mm256_add_pd(heatflux_y[i],_mm256_mul_pd(h_spec, ydiff_y));
            heatflux_z[i] = _mm256_add_pd(heatflux_z[i],_mm256_mul_pd(h_spec, ydiff_z));
            lastflux_x[i] = _mm256_sub_pd(lastflux_x[i],ydiff_x);
            lastflux_y[i] = _mm256_sub_pd(lastflux_y[i],ydiff_y);
            lastflux_z[i] = _mm256_sub_pd(lastflux_z[i],ydiff_z);
          }
        }
        
        // Now do the last species and write out the results
        h_spec_s += ByteOffset(h_spec_stride);
        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          int idx0 = enth_idx[(i<<2)]++;
          int idx1 = enth_idx[(i<<2)+1]++;
          int idx2 = enth_idx[(i<<2)+2]++;
          int idx3 = enth_idx[(i<<2)+3]++;
          double h_spec0 = (cpe_data.enth_aa[idx0] * temp[(i<<2)]  + cpe_data.enth_bb[idx0]);
          double h_spec1 = (cpe_data.enth_aa[idx1] * temp[(i<<2)+1] + cpe_data.enth_bb[idx1]);
          double h_spec2 = (cpe_data.enth_aa[idx2] * temp[(i<<2)+2] + cpe_data.enth_bb[idx2]);
          double h_spec3 = (cpe_data.enth_aa[idx3] * temp[(i<<2)+3] + cpe_data.enth_bb[idx3]);
          __m256d h_spec = _mm256_set_pd(h_spec3,h_spec2,h_spec1,h_spec0);
          // Write out the h_spec value
          _mm256_stream_pd(h_spec_s+(i<<2),h_spec);
          heatflux_x[i] = _mm256_add_pd(heatflux_x[i],_mm256_mul_pd(h_spec, lastflux_x[i]));
          heatflux_y[i] = _mm256_add_pd(heatflux_y[i],_mm256_mul_pd(h_spec, lastflux_y[i]));
          heatflux_z[i] = _mm256_add_pd(heatflux_z[i],_mm256_mul_pd(h_spec, lastflux_z[i]));
          // Now write out all the values
          _mm256_stream_pd(out_heatflux_x+(i<<2),heatflux_x[i]);
          _mm256_stream_pd(out_heatflux_y+(i<<2),heatflux_y[i]);
          _mm256_stream_pd(out_heatflux_z+(i<<2),heatflux_z[i]);
          _mm256_stream_pd(out_ydiffflux_last_x+(i<<2),lastflux_x[i]);
          _mm256_stream_pd(out_ydiffflux_last_y+(i<<2),lastflux_y[i]);
          _mm256_stream_pd(out_ydiffflux_last_z+(i<<2),lastflux_z[i]);
        }
      }
      else {
        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d lambda = _mm256_loadu_pd(in_lambda+(i<<2));
          __m256d gradtx = _mm256_loadu_pd(in_grad_t_x+(i<<2));
          __m256d gradty = _mm256_loadu_pd(in_grad_t_y+(i<<2));
          __m256d gradtz = _mm256_loadu_pd(in_grad_t_z+(i<<2));
          // Fast negation of lambda
          lambda = _mm256_xor_pd(lambda,_mm256_set1_pd(-0.0));
          heatflux_x[i] = _mm256_mul_pd(lambda,gradtx);
          heatflux_y[i] = _mm256_mul_pd(lambda,gradty);
          heatflux_z[i] = _mm256_mul_pd(lambda,gradtz);
          // Load the last flux values
          lastflux_x[i] = _mm256_set1_pd(0.0);
          lastflux_y[i] = _mm256_set1_pd(0.0);
          lastflux_z[i] = _mm256_set1_pd(0.0);
        }

        // Do the bucket computation like normal
        for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
          temp[i] = in_temp[i];
          int bucket = (temp[i] - temp_lo) * temp_step_inv;
          assert((bucket >= 0) && (bucket < cpe_data.num_pts));
          enth_idx[i] = bucket * n_spec;
        }

        double *h_spec_s = out_h_spec;
        const double *ydiff_x_s = in_ydiffflux_x;
        const double *ydiff_y_s = in_ydiffflux_y;
        const double *ydiff_z_s = in_ydiffflux_z;

        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d ydiff_x = _mm256_loadu_pd(ydiff_x_s+(i<<2));
          __m256d ydiff_y = _mm256_loadu_pd(ydiff_y_s+(i<<2));
          __m256d ydiff_z = _mm256_loadu_pd(ydiff_z_s+(i<<2));
          int idx0 = enth_idx[(i<<2)]++;
          int idx1 = enth_idx[(i<<2)+1]++;
          int idx2 = enth_idx[(i<<2)+2]++;
          int idx3 = enth_idx[(i<<2)+3]++;
          double h_spec0 = (cpe_data.enth_aa[idx0] * temp[(i<<2)]  + cpe_data.enth_bb[idx0]);
          double h_spec1 = (cpe_data.enth_aa[idx1] * temp[(i<<2)+1] + cpe_data.enth_bb[idx1]);
          double h_spec2 = (cpe_data.enth_aa[idx2] * temp[(i<<2)+2] + cpe_data.enth_bb[idx2]);
          double h_spec3 = (cpe_data.enth_aa[idx3] * temp[(i<<2)+3] + cpe_data.enth_bb[idx3]);
          __m256d h_spec = _mm256_set_pd(h_spec3,h_spec2,h_spec1,h_spec0);
          // Write out the h_spec value
          _mm256_storeu_pd(h_spec_s+(i<<2),h_spec);
          heatflux_x[i] = _mm256_add_pd(heatflux_x[i],_mm256_mul_pd(h_spec, ydiff_x));
          heatflux_y[i] = _mm256_add_pd(heatflux_y[i],_mm256_mul_pd(h_spec, ydiff_y));
          heatflux_z[i] = _mm256_add_pd(heatflux_z[i],_mm256_mul_pd(h_spec, ydiff_z));
          // Set last flux to be negative of each ydiff
          lastflux_x[i] = _mm256_xor_pd(ydiff_x,_mm256_set1_pd(-0.0));
          lastflux_y[i] = _mm256_xor_pd(ydiff_y,_mm256_set1_pd(-0.0));
          lastflux_z[i] = _mm256_xor_pd(ydiff_z,_mm256_set1_pd(-0.0));
        }

        for(size_t s = 1; s < (n_spec - 1); s++) {
          h_spec_s += ByteOffset(h_spec_stride);
          ydiff_x_s += ByteOffset(ydiffflux_stride);
          ydiff_y_s += ByteOffset(ydiffflux_stride);
          ydiff_z_s += ByteOffset(ydiffflux_stride);

          for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) { 
            __m256d ydiff_x = _mm256_loadu_pd(ydiff_x_s+(i<<2));
            __m256d ydiff_y = _mm256_loadu_pd(ydiff_y_s+(i<<2));
            __m256d ydiff_z = _mm256_loadu_pd(ydiff_z_s+(i<<2));
            int idx0 = enth_idx[(i<<2)]++;
            int idx1 = enth_idx[(i<<2)+1]++;
            int idx2 = enth_idx[(i<<2)+2]++;
            int idx3 = enth_idx[(i<<2)+3]++;
            double h_spec0 = (cpe_data.enth_aa[idx0] * temp[(i<<2)]  + cpe_data.enth_bb[idx0]);
            double h_spec1 = (cpe_data.enth_aa[idx1] * temp[(i<<2)+1] + cpe_data.enth_bb[idx1]);
            double h_spec2 = (cpe_data.enth_aa[idx2] * temp[(i<<2)+2] + cpe_data.enth_bb[idx2]);
            double h_spec3 = (cpe_data.enth_aa[idx3] * temp[(i<<2)+3] + cpe_data.enth_bb[idx3]);
            __m256d h_spec = _mm256_set_pd(h_spec3,h_spec2,h_spec1,h_spec0);
            // Write out the h_spec value
            _mm256_storeu_pd(h_spec_s+(i<<2),h_spec);
            heatflux_x[i] = _mm256_add_pd(heatflux_x[i],_mm256_mul_pd(h_spec, ydiff_x));
            heatflux_y[i] = _mm256_add_pd(heatflux_y[i],_mm256_mul_pd(h_spec, ydiff_y));
            heatflux_z[i] = _mm256_add_pd(heatflux_z[i],_mm256_mul_pd(h_spec, ydiff_z));
            lastflux_x[i] = _mm256_sub_pd(lastflux_x[i],ydiff_x);
            lastflux_y[i] = _mm256_sub_pd(lastflux_y[i],ydiff_y);
            lastflux_z[i] = _mm256_sub_pd(lastflux_z[i],ydiff_z);
          }
        }
        
        // Now do the last species and write out the results
        h_spec_s += ByteOffset(h_spec_stride);
        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          int idx0 = enth_idx[(i<<2)]++;
          int idx1 = enth_idx[(i<<2)+1]++;
          int idx2 = enth_idx[(i<<2)+2]++;
          int idx3 = enth_idx[(i<<2)+3]++;
          double h_spec0 = (cpe_data.enth_aa[idx0] * temp[(i<<2)]  + cpe_data.enth_bb[idx0]);
          double h_spec1 = (cpe_data.enth_aa[idx1] * temp[(i<<2)+1] + cpe_data.enth_bb[idx1]);
          double h_spec2 = (cpe_data.enth_aa[idx2] * temp[(i<<2)+2] + cpe_data.enth_bb[idx2]);
          double h_spec3 = (cpe_data.enth_aa[idx3] * temp[(i<<2)+3] + cpe_data.enth_bb[idx3]);
          __m256d h_spec = _mm256_set_pd(h_spec3,h_spec2,h_spec1,h_spec0);
          // Write out the h_spec value
          _mm256_storeu_pd(h_spec_s+(i<<2),h_spec);
          heatflux_x[i] = _mm256_add_pd(heatflux_x[i],_mm256_mul_pd(h_spec, lastflux_x[i]));
          heatflux_y[i] = _mm256_add_pd(heatflux_y[i],_mm256_mul_pd(h_spec, lastflux_y[i]));
          heatflux_z[i] = _mm256_add_pd(heatflux_z[i],_mm256_mul_pd(h_spec, lastflux_z[i]));
          // Now write out all the values
          _mm256_storeu_pd(out_heatflux_x+(i<<2),heatflux_x[i]);
          _mm256_storeu_pd(out_heatflux_y+(i<<2),heatflux_y[i]);
          _mm256_storeu_pd(out_heatflux_z+(i<<2),heatflux_z[i]);
          _mm256_storeu_pd(out_ydiffflux_last_x+(i<<2),lastflux_x[i]);
          _mm256_storeu_pd(out_ydiffflux_last_y+(i<<2),lastflux_y[i]);
          _mm256_storeu_pd(out_ydiffflux_last_z+(i<<2),lastflux_z[i]);
        }
      }
#elif defined(USE_SSE_KERNELS)
      for(size_t i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d lambda = _mm_stream_load_pd(in_lambda+(i<<1));
        __m128d gradtx = _mm_stream_load_pd(in_grad_t_x+(i<<1));
        __m128d gradty = _mm_stream_load_pd(in_grad_t_y+(i<<1));
        __m128d gradtz = _mm_stream_load_pd(in_grad_t_z+(i<<1));
        // Fast negation of lambda
        lambda = _mm_xor_pd(lambda,_mm_set1_pd(-0.0));
        heatflux_x[i] = _mm_mul_pd(lambda,gradtx);
        heatflux_y[i] = _mm_mul_pd(lambda,gradty);
        heatflux_z[i] = _mm_mul_pd(lambda,gradtz);
        // Load the last flux values
        lastflux_x[i] = _mm_set1_pd(0.0);
        lastflux_y[i] = _mm_set1_pd(0.0);
        lastflux_z[i] = _mm_set1_pd(0.0);
      }

      // Do the bucket computation like normal
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
        temp[i] = in_temp[i];
        int bucket = (temp[i] - temp_lo) * temp_step_inv;
        assert((bucket >= 0) && (bucket < cpe_data.num_pts));
        enth_idx[i] = bucket * n_spec;
      }

      double *h_spec_s = out_h_spec;
      const double *ydiff_x_s = in_ydiffflux_x;
      const double *ydiff_y_s = in_ydiffflux_y;
      const double *ydiff_z_s = in_ydiffflux_z;

      for(size_t i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d ydiff_x = _mm_stream_load_pd(ydiff_x_s+(i<<1));
        __m128d ydiff_y = _mm_stream_load_pd(ydiff_y_s+(i<<1));
        __m128d ydiff_z = _mm_stream_load_pd(ydiff_z_s+(i<<1));
        int idx0 = enth_idx[(i<<1)]++;
        int idx1 = enth_idx[(i<<1)+1]++;
        double h_spec0 = (cpe_data.enth_aa[idx0] * temp[(i<<1)]  + cpe_data.enth_bb[idx0]);
        double h_spec1 = (cpe_data.enth_aa[idx1] * temp[(i<<1)+1] + cpe_data.enth_bb[idx1]);
        __m128d h_spec = _mm_set_pd(h_spec1,h_spec0);
        // Write out the h_spec value
        _mm_stream_pd(h_spec_s+(i<<1),h_spec);
        heatflux_x[i] = _mm_add_pd(heatflux_x[i],_mm_mul_pd(h_spec, ydiff_x));
        heatflux_y[i] = _mm_add_pd(heatflux_y[i],_mm_mul_pd(h_spec, ydiff_y));
        heatflux_z[i] = _mm_add_pd(heatflux_z[i],_mm_mul_pd(h_spec, ydiff_z));
        // Set last flux to be negative of each ydiff
        lastflux_x[i] = _mm_xor_pd(ydiff_x,_mm_set1_pd(-0.0));
        lastflux_y[i] = _mm_xor_pd(ydiff_y,_mm_set1_pd(-0.0));
        lastflux_z[i] = _mm_xor_pd(ydiff_z,_mm_set1_pd(-0.0));
      }

      for(size_t s = 1; s < (n_spec - 1); s++) {
        h_spec_s += ByteOffset(h_spec_stride);
        ydiff_x_s += ByteOffset(ydiffflux_stride);
        ydiff_y_s += ByteOffset(ydiffflux_stride);
        ydiff_z_s += ByteOffset(ydiffflux_stride);

        for(size_t i = 0; i < (FULL_STRIP_SIZE/2); i++) { 
          __m128d ydiff_x = _mm_stream_load_pd(ydiff_x_s+(i<<1));
          __m128d ydiff_y = _mm_stream_load_pd(ydiff_y_s+(i<<1));
          __m128d ydiff_z = _mm_stream_load_pd(ydiff_z_s+(i<<1));
          int idx0 = enth_idx[(i<<1)]++;
          int idx1 = enth_idx[(i<<1)+1]++;
          double h_spec0 = (cpe_data.enth_aa[idx0] * temp[(i<<1)]  + cpe_data.enth_bb[idx0]);
          double h_spec1 = (cpe_data.enth_aa[idx1] * temp[(i<<1)+1] + cpe_data.enth_bb[idx1]);
          __m128d h_spec = _mm_set_pd(h_spec1,h_spec0);
          // Write out the h_spec value
          _mm_stream_pd(h_spec_s+(i<<1),h_spec);
          heatflux_x[i] = _mm_add_pd(heatflux_x[i],_mm_mul_pd(h_spec, ydiff_x));
          heatflux_y[i] = _mm_add_pd(heatflux_y[i],_mm_mul_pd(h_spec, ydiff_y));
          heatflux_z[i] = _mm_add_pd(heatflux_z[i],_mm_mul_pd(h_spec, ydiff_z));
          lastflux_x[i] = _mm_sub_pd(lastflux_x[i],ydiff_x);
          lastflux_y[i] = _mm_sub_pd(lastflux_y[i],ydiff_y);
          lastflux_z[i] = _mm_sub_pd(lastflux_z[i],ydiff_z);
        }
      }
      
      // Now do the last species and write out the results
      h_spec_s += ByteOffset(h_spec_stride);
      for(size_t i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        int idx0 = enth_idx[(i<<1)]++;
        int idx1 = enth_idx[(i<<1)+1]++;
        double h_spec0 = (cpe_data.enth_aa[idx0] * temp[(i<<1)]  + cpe_data.enth_bb[idx0]);
        double h_spec1 = (cpe_data.enth_aa[idx1] * temp[(i<<1)+1] + cpe_data.enth_bb[idx1]);
        __m128d h_spec = _mm_set_pd(h_spec1,h_spec0);
        // Write out the h_spec value
        _mm_stream_pd(h_spec_s+(i<<1),h_spec);
        heatflux_x[i] = _mm_add_pd(heatflux_x[i],_mm_mul_pd(h_spec, lastflux_x[i]));
        heatflux_y[i] = _mm_add_pd(heatflux_y[i],_mm_mul_pd(h_spec, lastflux_y[i]));
        heatflux_z[i] = _mm_add_pd(heatflux_z[i],_mm_mul_pd(h_spec, lastflux_z[i]));
        // Now write out all the values
        _mm_stream_pd(out_heatflux_x+(i<<1),heatflux_x[i]);
        _mm_stream_pd(out_heatflux_y+(i<<1),heatflux_y[i]);
        _mm_stream_pd(out_heatflux_z+(i<<1),heatflux_z[i]);
        _mm_stream_pd(out_ydiffflux_last_x+(i<<1),lastflux_x[i]);
        _mm_stream_pd(out_ydiffflux_last_y+(i<<1),lastflux_y[i]);
        _mm_stream_pd(out_ydiffflux_last_z+(i<<1),lastflux_z[i]);
      }
#else
      // Fast case with statically bounded loops
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
        out_heatflux_x[i] = -in_lambda[i] * in_grad_t_x[i];
        out_heatflux_y[i] = -in_lambda[i] * in_grad_t_y[i];
        out_heatflux_z[i] = -in_lambda[i] * in_grad_t_z[i];
        out_ydiffflux_last_x[i] = 0.0;
        out_ydiffflux_last_y[i] = 0.0;
        out_ydiffflux_last_z[i] = 0.0;
      }

      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
        int bucket = (in_temp[i] - temp_lo) * temp_step_inv;
        assert((bucket >= 0) && (bucket < cpe_data.num_pts));
        enth_idx[i] = bucket * n_spec;
      }

      double *h_spec_s = out_h_spec;
      const double *ydiff_x_s = in_ydiffflux_x;
      const double *ydiff_y_s = in_ydiffflux_y;
      const double *ydiff_z_s = in_ydiffflux_z;

      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
        int idx = enth_idx[i]++;
        double h_spec = (cpe_data.enth_aa[idx] * in_temp[i] + cpe_data.enth_bb[idx]);
        h_spec_s[i] = h_spec;
        out_heatflux_x[i] += h_spec * ydiff_x_s[i];
        out_heatflux_y[i] += h_spec * ydiff_y_s[i];
        out_heatflux_z[i] += h_spec * ydiff_z_s[i];
        out_ydiffflux_last_x[i] = -ydiff_x_s[i];
        out_ydiffflux_last_y[i] = -ydiff_y_s[i];
        out_ydiffflux_last_z[i] = -ydiff_z_s[i];
      }

      for(size_t s = 1; s < (n_spec - 1); s++) {
        h_spec_s += ByteOffset(h_spec_stride);
        ydiff_x_s += ByteOffset(ydiffflux_stride);
        ydiff_y_s += ByteOffset(ydiffflux_stride);
        ydiff_z_s += ByteOffset(ydiffflux_stride);
      
        for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
          int idx = enth_idx[i]++;
          double h_spec = (cpe_data.enth_aa[idx] * in_temp[i] + cpe_data.enth_bb[idx]);
          h_spec_s[i] = h_spec;
          out_heatflux_x[i] += h_spec * ydiff_x_s[i];
          out_heatflux_y[i] += h_spec * ydiff_y_s[i];
          out_heatflux_z[i] += h_spec * ydiff_z_s[i];
          out_ydiffflux_last_x[i] -= ydiff_x_s[i];
          out_ydiffflux_last_y[i] -= ydiff_y_s[i];
          out_ydiffflux_last_z[i] -= ydiff_z_s[i];
        }
      }

      // last species
      h_spec_s += ByteOffset(h_spec_stride);
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
        int idx = enth_idx[i];
        double h_spec = (cpe_data.enth_aa[idx] * in_temp[i] + cpe_data.enth_bb[idx]);
        h_spec_s[i] = h_spec;
        out_heatflux_x[i] += h_spec * out_ydiffflux_last_x[i];
        out_heatflux_y[i] += h_spec * out_ydiffflux_last_y[i];
        out_heatflux_z[i] += h_spec * out_ydiffflux_last_z[i];
      }
#endif
      n_pts -= FULL_STRIP_SIZE;
    }
    else
    {
      // Slow case where the compiler can't do loop unrolling
      size_t strip_size = n_pts;
      for(size_t i = 0; i < strip_size; i++) {
        out_heatflux_x[i] = -in_lambda[i] * in_grad_t_x[i];
        out_heatflux_y[i] = -in_lambda[i] * in_grad_t_y[i];
        out_heatflux_z[i] = -in_lambda[i] * in_grad_t_z[i];
        out_ydiffflux_last_x[i] = 0.0;
        out_ydiffflux_last_y[i] = 0.0;
        out_ydiffflux_last_z[i] = 0.0;
      }

      for(size_t i = 0; i < strip_size; i++) {
        int bucket = (in_temp[i] - temp_lo) * temp_step_inv;
        assert((bucket >= 0) && (bucket < cpe_data.num_pts));
        enth_idx[i] = bucket * n_spec;
      }

      double *h_spec_s = out_h_spec;
      const double *ydiff_x_s = in_ydiffflux_x;
      const double *ydiff_y_s = in_ydiffflux_y;
      const double *ydiff_z_s = in_ydiffflux_z;

      for(size_t i = 0; i < strip_size; i++) {
        int idx = enth_idx[i]++;
        double h_spec = (cpe_data.enth_aa[idx] * in_temp[i] + cpe_data.enth_bb[idx]);
        h_spec_s[i] = h_spec;
        out_heatflux_x[i] += h_spec * ydiff_x_s[i];
        out_heatflux_y[i] += h_spec * ydiff_y_s[i];
        out_heatflux_z[i] += h_spec * ydiff_z_s[i];
        out_ydiffflux_last_x[i] = -ydiff_x_s[i];
        out_ydiffflux_last_y[i] = -ydiff_y_s[i];
        out_ydiffflux_last_z[i] = -ydiff_z_s[i];
      }

      for(size_t s = 1; s < (n_spec - 1); s++) {
        h_spec_s += ByteOffset(h_spec_stride);
        ydiff_x_s += ByteOffset(ydiffflux_stride);
        ydiff_y_s += ByteOffset(ydiffflux_stride);
        ydiff_z_s += ByteOffset(ydiffflux_stride);
      
        for(size_t i = 0; i < strip_size; i++) {
          int idx = enth_idx[i]++;
          double h_spec = (cpe_data.enth_aa[idx] * in_temp[i] + cpe_data.enth_bb[idx]);
          h_spec_s[i] = h_spec;
          out_heatflux_x[i] += h_spec * ydiff_x_s[i];
          out_heatflux_y[i] += h_spec * ydiff_y_s[i];
          out_heatflux_z[i] += h_spec * ydiff_z_s[i];
          out_ydiffflux_last_x[i] -= ydiff_x_s[i];
          out_ydiffflux_last_y[i] -= ydiff_y_s[i];
          out_ydiffflux_last_z[i] -= ydiff_z_s[i];
        }
      }

      // last species
      h_spec_s += ByteOffset(h_spec_stride);
      for(size_t i = 0; i < strip_size; i++) {
        int idx = enth_idx[i];
        double h_spec = (cpe_data.enth_aa[idx] * in_temp[i] + cpe_data.enth_bb[idx]);
        h_spec_s[i] = h_spec;
        out_heatflux_x[i] += h_spec * out_ydiffflux_last_x[i];
        out_heatflux_y[i] += h_spec * out_ydiffflux_last_y[i];
        out_heatflux_z[i] += h_spec * out_ydiffflux_last_z[i];
      }
      // We're done now
      n_pts = 0;
    }

    in_grad_t_x += FULL_STRIP_SIZE;
    in_grad_t_y += FULL_STRIP_SIZE;
    in_grad_t_z += FULL_STRIP_SIZE;
    in_lambda += FULL_STRIP_SIZE;
    in_temp += FULL_STRIP_SIZE;
    out_h_spec += FULL_STRIP_SIZE;
    in_ydiffflux_x += FULL_STRIP_SIZE;
    in_ydiffflux_y += FULL_STRIP_SIZE;
    in_ydiffflux_z += FULL_STRIP_SIZE;
    out_ydiffflux_last_x += FULL_STRIP_SIZE;
    out_ydiffflux_last_y += FULL_STRIP_SIZE;
    out_ydiffflux_last_z += FULL_STRIP_SIZE;
    out_heatflux_x += FULL_STRIP_SIZE;
    out_heatflux_y += FULL_STRIP_SIZE;
    out_heatflux_z += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
}

/*static*/
bool CalcHeatFluxTask::fast_calc_heatflux_task(RegionAccessor<AccessorType::Generic,double> fa_in_temp,
				      RegionAccessor<AccessorType::Generic,double> fa_in_lambda,
				      RegionAccessor<AccessorType::Generic,double> fa_in_grad_t_x,
				      RegionAccessor<AccessorType::Generic,double> fa_in_grad_t_y,
				      RegionAccessor<AccessorType::Generic,double> fa_in_grad_t_z,
				      PhysicalRegion ra_out_h_spec,
				      PhysicalRegion ra_in_ydiffflux,
				      RegionAccessor<AccessorType::Generic,double> fa_out_heatflux_x,
				      RegionAccessor<AccessorType::Generic,double> fa_out_heatflux_y,
				      RegionAccessor<AccessorType::Generic,double> fa_out_heatflux_z,
				      RegionAccessor<AccessorType::Generic,double> fa_out_ydiffflux_last_x,
				      RegionAccessor<AccessorType::Generic,double> fa_out_ydiffflux_last_y,
				      RegionAccessor<AccessorType::Generic,double> fa_out_ydiffflux_last_z,
				      S3DRank::CPETable& cpe_data,
				      int n_spec,
				      Rect<3> subgrid_bounds)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3], offsets[3];

  const double *in_lambda_ptr = fa_in_lambda.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_lambda_ptr || (subrect != subgrid_bounds)) return false;

  const double *in_temp_ptr = fa_in_temp.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_temp_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_grad_t_x_ptr = fa_in_grad_t_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_t_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
  const double *in_grad_t_y_ptr = fa_in_grad_t_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_t_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
  const double *in_grad_t_z_ptr = fa_in_grad_t_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_t_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_heatflux_x_ptr = fa_out_heatflux_x.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_heatflux_x_ptr || (subrect != subgrid_bounds)) return false;
  double *out_heatflux_y_ptr = fa_out_heatflux_y.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_heatflux_y_ptr || (subrect != subgrid_bounds)) return false;
  double *out_heatflux_z_ptr = fa_out_heatflux_z.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_heatflux_z_ptr || (subrect != subgrid_bounds)) return false;

  double *out_h_spec_ptr = ra_out_h_spec.get_field_accessor(FID_H_SPEC(0)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_h_spec_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;
  ByteOffset h_spec_stride;
for(int i = 1; i < n_spec; i++) {
    double *out_h_spec_i_ptr = ra_out_h_spec.get_field_accessor(FID_H_SPEC(i)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_h_spec_i_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;
    ByteOffset h_spec_i_stride(out_h_spec_i_ptr, out_h_spec_ptr);
    if(i == 1) {
      h_spec_stride = h_spec_i_stride;
    } else {
      if(h_spec_i_stride != (h_spec_stride * i)) {
        printf("h_spec stride mismatch\n");
        return 0;
      }
    }
  }

  double *out_ydiffflux_last_x_ptr = fa_out_ydiffflux_last_x.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_ydiffflux_last_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;
  double *out_ydiffflux_last_y_ptr = fa_out_ydiffflux_last_y.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_ydiffflux_last_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;
  double *out_ydiffflux_last_z_ptr = fa_out_ydiffflux_last_z.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_ydiffflux_last_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *in_ydiffflux_x_ptr = ra_in_ydiffflux.get_field_accessor(FID_YDIFFFLUX_X(0)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_ydiffflux_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
  const double *in_ydiffflux_y_ptr = ra_in_ydiffflux.get_field_accessor(FID_YDIFFFLUX_Y(0)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_ydiffflux_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
  const double *in_ydiffflux_z_ptr = ra_in_ydiffflux.get_field_accessor(FID_YDIFFFLUX_Z(0)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_ydiffflux_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
  ByteOffset ydiffflux_stride;
  for(int i = 1; i < n_spec - 1; i++) {
    for(int d = 0; d < 3; d++) {
      const double *in_ydiffflux_i_ptr = ra_in_ydiffflux.get_field_accessor(FID_YDIFFFLUX(i,d)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
      if(!in_ydiffflux_i_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;
      ByteOffset ydiffflux_i_stride(in_ydiffflux_i_ptr, 
				    ((d == 0) ? in_ydiffflux_x_ptr :
				     (d == 1) ? in_ydiffflux_y_ptr :
				                in_ydiffflux_z_ptr));
      if((i == 1) && (d == 0)) {
	ydiffflux_stride = ydiffflux_i_stride;
      } else {
	if(ydiffflux_i_stride != (ydiffflux_stride * i)) {
	  printf("ydiffflux stride mismatch\n");
	  return 0;
	}
      }
    }
  }

  // are we in a dense layout that we can iterate over blindly?
  if(offsets_are_dense<3, double>(subgrid_bounds, in_offsets) && !offset_mismatch(3, in_offsets, out_offsets)) {
    //printf("dense!\n");
    dense_calc_heatflux_task(subgrid_bounds.volume(), cpe_data, n_spec,
                             in_grad_t_x_ptr, in_grad_t_y_ptr, in_grad_t_z_ptr,
                             in_lambda_ptr, in_temp_ptr,
                             out_h_spec_ptr, h_spec_stride.offset,
                             in_ydiffflux_x_ptr, in_ydiffflux_y_ptr, in_ydiffflux_z_ptr,
                             ydiffflux_stride.offset,
                             out_heatflux_x_ptr, 
                             out_heatflux_y_ptr, 
                             out_heatflux_z_ptr, 
                             out_ydiffflux_last_x_ptr,
                             out_ydiffflux_last_y_ptr,
                             out_ydiffflux_last_z_ptr);
    return true;
  }

  // now that we have all those pointers, we can do the actual work
  ByteOffset in_pos_z, out_pos_z;
  for(int z = subgrid_bounds.lo[2]; z <= subgrid_bounds.hi[2]; z++) {
    ByteOffset in_pos_y = in_pos_z;
    ByteOffset out_pos_y = out_pos_z;
    for(int y = subgrid_bounds.lo[1]; y <= subgrid_bounds.hi[1]; y++) {
      ByteOffset in_pos_x = in_pos_y;
      ByteOffset out_pos_x = out_pos_y;
      for(int x = subgrid_bounds.lo[0]; x <= subgrid_bounds.hi[0]; x++) {
        double grad_t_x = *(in_grad_t_x_ptr + in_pos_x);
        double grad_t_y = *(in_grad_t_y_ptr + in_pos_x);
        double grad_t_z = *(in_grad_t_z_ptr + in_pos_x);
        double lambda = *(in_lambda_ptr + in_pos_x);
        double temp = *(in_temp_ptr + in_pos_x);

        double heatflux_x = -lambda * grad_t_x;
        double heatflux_y = -lambda * grad_t_y;
        double heatflux_z = -lambda * grad_t_z;
        double ydifflast_x = 0.0;
        double ydifflast_y = 0.0;
        double ydifflast_z = 0.0;

        const double *in_ydiffflux_x_ptr_p = in_ydiffflux_x_ptr + in_pos_x;
        const double *in_ydiffflux_y_ptr_p = in_ydiffflux_y_ptr + in_pos_x;
        const double *in_ydiffflux_z_ptr_p = in_ydiffflux_z_ptr + in_pos_x;

        double temp_hi = 3500.0 / T_REF;
        double temp_lo = 250.0 / T_REF;
        assert(cpe_data.num_pts > 0);
        double temp_step = (temp_hi - temp_lo) / (cpe_data.num_pts - 1);

        int bucket = (temp - temp_lo) / temp_step;
        assert((bucket >= 0) && (bucket < cpe_data.num_pts));
        const double *enth_aa = cpe_data.enth_aa + (bucket * n_spec);
        const double *enth_bb = cpe_data.enth_bb + (bucket * n_spec);

        double *out_h_spec_ptr_p = out_h_spec_ptr + in_pos_x;
        for(int i = 0; i < n_spec - 1; i++) {
          double h_spec = (*enth_aa++ * temp + *enth_bb++);
          *out_h_spec_ptr_p = h_spec;  out_h_spec_ptr_p += h_spec_stride;

          double ydiff_x = *in_ydiffflux_x_ptr_p;  in_ydiffflux_x_ptr_p += ydiffflux_stride;
          double ydiff_y = *in_ydiffflux_y_ptr_p;  in_ydiffflux_y_ptr_p += ydiffflux_stride;
          double ydiff_z = *in_ydiffflux_z_ptr_p;  in_ydiffflux_z_ptr_p += ydiffflux_stride;

          heatflux_x += h_spec * ydiff_x;
          heatflux_y += h_spec * ydiff_y;
          heatflux_z += h_spec * ydiff_z;

          ydifflast_x -= ydiff_x;
          ydifflast_y -= ydiff_y;
          ydifflast_z -= ydiff_z;
        }

        // final species
        double h_spec_last = (*enth_aa * temp + *enth_bb);
        *out_h_spec_ptr_p = h_spec_last;
        heatflux_x += h_spec_last * ydifflast_x;
        heatflux_y += h_spec_last * ydifflast_y;
        heatflux_z += h_spec_last * ydifflast_z;
 
        *(out_heatflux_x_ptr + out_pos_x) = heatflux_x;
        *(out_heatflux_y_ptr + out_pos_x) = heatflux_y;
        *(out_heatflux_z_ptr + out_pos_x) = heatflux_z;

        // write out last species' diff flux too
        *(out_ydiffflux_last_x_ptr + out_pos_x) = ydifflast_x;
        *(out_ydiffflux_last_y_ptr + out_pos_x) = ydifflast_y;
        *(out_ydiffflux_last_z_ptr + out_pos_x) = ydifflast_z;

        in_pos_x += in_offsets[0];
        out_pos_x += out_offsets[0];
      }
      in_pos_y += in_offsets[1];
      out_pos_y += out_offsets[1];
    }
    in_pos_z += in_offsets[2];
    out_pos_z += out_offsets[2];
  }

  return true;
}

/*static*/
void CalcHeatFluxTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  // get whatever accessors you need here
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_x = regions[0].get_field_accessor(FID_HEATFLUX_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_y = regions[0].get_field_accessor(FID_HEATFLUX_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_z = regions[0].get_field_accessor(FID_HEATFLUX_Z).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_x = regions[0].get_field_accessor(FID_YDIFFFLUX_X(rank->n_spec - 1)).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_y = regions[0].get_field_accessor(FID_YDIFFFLUX_Y(rank->n_spec - 1)).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_z = regions[0].get_field_accessor(FID_YDIFFFLUX_Z(rank->n_spec - 1)).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_temp = regions[1].get_field_accessor(FID_TEMP).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_lambda = regions[1].get_field_accessor(FID_LAMBDA).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_grad_t_x = regions[2].get_field_accessor(FID_GRAD_T_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_t_y = regions[2].get_field_accessor(FID_GRAD_T_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_t_z = regions[2].get_field_accessor(FID_GRAD_T_Z).typeify<double>();

  if(fast_calc_heatflux_task(fa_temp, fa_lambda, 
                             fa_grad_t_x, fa_grad_t_y, fa_grad_t_z, 
                             regions[3], 
                             regions[2], 
                             fa_heatflux_x, fa_heatflux_y, fa_heatflux_z,
                             fa_ydiffflux_last_x, fa_ydiffflux_last_y, fa_ydiffflux_last_z, 
                             rank->cpe_data, rank->n_spec, subgrid_bounds)) {
    //printf("fast heatflux\n");
    return;
  }

  for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double temp = fa_temp.read(dp);
    double grad_t_x = fa_grad_t_x.read(dp);
    double grad_t_y = fa_grad_t_y.read(dp);
    double grad_t_z = fa_grad_t_z.read(dp);
    double lambda = fa_lambda.read(dp);

    double heatflux_x = -lambda * grad_t_x;
    double heatflux_y = -lambda * grad_t_y;
    double heatflux_z = -lambda * grad_t_z;
    double ydifflast_x = 0.0;
    double ydifflast_y = 0.0;
    double ydifflast_z = 0.0;

    double temp_hi = 3500.0 / T_REF;
    double temp_lo = 250.0 / T_REF;
    assert(rank->cpe_data.num_pts > 0);
    double temp_step = (temp_hi - temp_lo) / (rank->cpe_data.num_pts - 1);

    int bucket = (temp - temp_lo) / temp_step;
    assert((bucket >= 0) && (bucket < rank->cpe_data.num_pts));
    const double *enth_aa = rank->cpe_data.enth_aa + (bucket * rank->n_spec);
    const double *enth_bb = rank->cpe_data.enth_bb + (bucket * rank->n_spec);
    for(int i = 0; i < rank->n_spec - 1; i++) {
      double h_spec = (enth_aa[i] * temp + enth_bb[i]);
      regions[3].get_field_accessor(FID_H_SPEC(i)).typeify<double>().write(dp, h_spec);
      double ydiff_x = regions[2].get_field_accessor(FID_YDIFFFLUX_X(i)).typeify<double>().read(dp);
      double ydiff_y = regions[2].get_field_accessor(FID_YDIFFFLUX_Y(i)).typeify<double>().read(dp);
      double ydiff_z = regions[2].get_field_accessor(FID_YDIFFFLUX_Z(i)).typeify<double>().read(dp);

      heatflux_x += h_spec * ydiff_x;
      heatflux_y += h_spec * ydiff_y;
      heatflux_z += h_spec * ydiff_z;

      ydifflast_x -= ydiff_x;
      ydifflast_y -= ydiff_y;
      ydifflast_z -= ydiff_z;
    }
    // final species
    double h_spec_last = (enth_aa[rank->n_spec - 1] * temp + enth_bb[rank->n_spec - 1]);
    regions[3].get_field_accessor(FID_H_SPEC(rank->n_spec - 1)).typeify<double>().write(dp, h_spec_last);
    heatflux_x += h_spec_last * ydifflast_x;
    heatflux_y += h_spec_last * ydifflast_y;
    heatflux_z += h_spec_last * ydifflast_z;
 
    fa_heatflux_x.write(dp, heatflux_x);
    fa_heatflux_y.write(dp, heatflux_y);
    fa_heatflux_z.write(dp, heatflux_z);

    // write out last species' diff flux too
    regions[0].get_field_accessor(FID_YDIFFFLUX_X(rank->n_spec - 1)).typeify<double>().write(dp, ydifflast_x);
    regions[0].get_field_accessor(FID_YDIFFFLUX_Y(rank->n_spec - 1)).typeify<double>().write(dp, ydifflast_y);
    regions[0].get_field_accessor(FID_YDIFFFLUX_Z(rank->n_spec - 1)).typeify<double>().write(dp, ydifflast_z);
  }
#endif
}

CalcEnthalpyTask::CalcEnthalpyTask(S3DRank *r,
                                   Domain domain,
                                   TaskArgument global_arg,
                                   ArgumentMap arg_map,
                                   Predicate pred,
                                   bool must,
                                   MapperID id,
                                   MappingTagID tag,
                                   bool add_requirements)
 : IndexLauncher(CalcEnthalpyTask::TASK_ID, domain, global_arg, arg_map, pred, must,
                 id, tag | CalcEnthalpyTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_hspec(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    for(int i = 0; i < rank->n_spec; i++)
      rr_hspec.add_field(FID_H_SPEC(i));
    add_region_requirement(rr_hspec);

    RegionRequirement rr_state(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_state.add_field(FID_TEMP);
    add_region_requirement(rr_state);
  }
}

void CalcEnthalpyTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  for(int s = 0; s < rank->n_spec; s++)
    check_field(runtime, ctx, rank->proc_grid_bounds, 
                rank->is_grid, rank->ip_top, rank->lr_int, FID_H_SPEC(s),
                rank, &S3DRank::RHSFArrays::h_spec, s, 0, 1, 0, "h_spec[%d]", s);
}

/*static*/
const char * const CalcEnthalpyTask::TASK_NAME = "calc_enthalpy_task";

static inline void check_temperature(S3DRank *rank, const int idx, 
                                     const double lo, const double hi, const double val)
{
  if (isnan(val))
  {
    printf("NAN temperature value at index %d on rank (%d,%d,%d) at stage %d\n", 
            idx, rank->my_proc_id[0], rank->my_proc_id[1], rank->my_proc_id[2],
            rank->cur_legion_stage);
    assert(false);
  }
  if (isinf(val))
  {
    printf("INF temperature value at index %d on rank (%d,%d,%d) at stage %d\n",
            idx, rank->my_proc_id[0], rank->my_proc_id[1], rank->my_proc_id[2],
            rank->cur_legion_stage);
    assert(false);
  }
  if (val < lo)
  {
    printf("Temperature value %.8g is less than minimum %.8g at index %d on rank (%d,%d,%d) at stage %d\n",
            val, lo, idx, rank->my_proc_id[0], rank->my_proc_id[1], rank->my_proc_id[2],
            rank->cur_legion_stage);
    assert(false);
  }
  if (val > hi)
  {
    printf("Temperature value %.8g is greater than maximum %.8g at index %d on rank (%d,%d,%d) at stage %d\n",
            val, hi, idx, rank->my_proc_id[0], rank->my_proc_id[1], rank->my_proc_id[2],
            rank->cur_legion_stage);
    assert(false);
  }
}

/*static*/
bool CalcEnthalpyTask::dense_calc_enthalpy(S3DRank *rank, const Rect<3> &subgrid_bounds,
                                           RegionAccessor<AccessorType::Generic,double> fa_in_temp,
                                           const std::vector<RegionRequirement> &reqs,
                                           const std::vector<PhysicalRegion> &regions)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], offsets[3];

  const double *in_temp_ptr = fa_in_temp.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_temp_ptr || (subrect != subgrid_bounds) || 
     !offsets_are_dense<3,double>(subgrid_bounds, in_offsets)) return false;

  double *out_h_spec_ptr = regions[0].get_field_accessor(FID_H_SPEC(0)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!out_h_spec_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  off_t h_spec_stride; 
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_spec = regions[0].get_field_accessor(FID_H_SPEC(1)).typeify<double>();
    double *next_spec_ptr = fa_next_spec.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_spec_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
    ByteOffset stride(next_spec_ptr,out_h_spec_ptr);
    h_spec_stride = stride.offset/sizeof(double);
  }

  const double temp_hi = 3500.0 / T_REF;
  const double temp_lo = 250.0 / T_REF;
  assert(rank->cpe_data.num_pts > 0);
  const double temp_step = (temp_hi - temp_lo) / (rank->cpe_data.num_pts - 1);
  const double temp_step_inv = (1.0/temp_step);

  size_t n_pts = subgrid_bounds.volume();
#ifdef BULLDOZER
// Fit in L1 and only consume the first way in each set
#define FULL_STRIP_SIZE 512
#else
#define FULL_STRIP_SIZE 1024
#endif

#if defined(USE_AVX_KERNELS)
  __m256d avx_temp[FULL_STRIP_SIZE/4];
#else
  __m128d sse_temp[FULL_STRIP_SIZE/2];
#endif
  double temp[FULL_STRIP_SIZE];
  int enth_idx[FULL_STRIP_SIZE];

  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined(USE_AVX_KERNELS)
      if (aligned<32>(in_temp_ptr) && aligned<32>(out_h_spec_ptr)) {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          avx_temp[i] = _mm256_load_pd(in_temp_ptr+(i<<2));
          __m128d upper = _mm256_extractf128_pd(avx_temp[i],1);
          __m128d lower = _mm256_extractf128_pd(avx_temp[i],0);
          const double t1 = _mm_cvtsd_f64(lower);
          const double t2 = _mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1));
          const double t3 = _mm_cvtsd_f64(upper);
          const double t4 = _mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1));
          check_temperature(rank, (i<<2), temp_lo, temp_hi, t1);
          check_temperature(rank, (i<<2)+1, temp_lo, temp_hi, t2);
          check_temperature(rank, (i<<2)+2, temp_lo, temp_hi, t3);
          check_temperature(rank, (i<<2)+3, temp_lo, temp_hi, t4);
          int bucket1 = (t1 - temp_lo) * temp_step_inv;
          int bucket2 = (t2 - temp_lo) * temp_step_inv;
          int bucket3 = (t3 - temp_lo) * temp_step_inv;
          int bucket4 = (t4 - temp_lo) * temp_step_inv;
          enth_idx[(i<<2)] = bucket1 * rank->n_spec;
          enth_idx[(i<<2)+1] = bucket2 * rank->n_spec;
          enth_idx[(i<<2)+2] = bucket3 * rank->n_spec;
          enth_idx[(i<<2)+3] = bucket4 * rank->n_spec;
        }

        for (int s = 0; s < rank->n_spec; s++) {
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            int idx1 = enth_idx[(i<<2)]++;
            int idx2 = enth_idx[(i<<2)+1]++;
            int idx3 = enth_idx[(i<<2)+2]++;
            int idx4 = enth_idx[(i<<2)+3]++;
            __m256d aa_val = _mm256_set_pd(rank->cpe_data.enth_aa[idx4],
                                           rank->cpe_data.enth_aa[idx3],
                                           rank->cpe_data.enth_aa[idx2],
                                           rank->cpe_data.enth_aa[idx1]);
            __m256d bb_val = _mm256_set_pd(rank->cpe_data.enth_bb[idx4],
                                           rank->cpe_data.enth_bb[idx3],
                                           rank->cpe_data.enth_bb[idx2],
                                           rank->cpe_data.enth_bb[idx1]);
            __m256d h_spec = _mm256_add_pd(bb_val,_mm256_mul_pd(aa_val,avx_temp[i]));
            _mm256_stream_pd(out_h_spec_ptr+(s*h_spec_stride)+(i<<2),h_spec);
          }
        }
      } else {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          avx_temp[i] = _mm256_loadu_pd(in_temp_ptr+(i<<2));
          __m128d upper = _mm256_extractf128_pd(avx_temp[i],1);
          __m128d lower = _mm256_extractf128_pd(avx_temp[i],0);
          const double t1 = _mm_cvtsd_f64(lower);
          const double t2 = _mm_cvtsd_f64(_mm_shuffle_pd(lower,lower,1));
          const double t3 = _mm_cvtsd_f64(upper);
          const double t4 = _mm_cvtsd_f64(_mm_shuffle_pd(upper,upper,1));
          check_temperature(rank, (i<<2), temp_lo, temp_hi, t1);
          check_temperature(rank, (i<<2)+1, temp_lo, temp_hi, t2);
          check_temperature(rank, (i<<2)+2, temp_lo, temp_hi, t3);
          check_temperature(rank, (i<<2)+3, temp_lo, temp_hi, t4);
          int bucket1 = (t1 - temp_lo) * temp_step_inv;
          int bucket2 = (t2 - temp_lo) * temp_step_inv;
          int bucket3 = (t3 - temp_lo) * temp_step_inv;
          int bucket4 = (t4 - temp_lo) * temp_step_inv;
          enth_idx[(i<<2)] = bucket1 * rank->n_spec;
          enth_idx[(i<<2)+1] = bucket2 * rank->n_spec;
          enth_idx[(i<<2)+2] = bucket3 * rank->n_spec;
          enth_idx[(i<<2)+3] = bucket4 * rank->n_spec;
        }

        for (int s = 0; s < rank->n_spec; s++) {
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            int idx1 = enth_idx[(i<<2)]++;
            int idx2 = enth_idx[(i<<2)+1]++;
            int idx3 = enth_idx[(i<<2)+2]++;
            int idx4 = enth_idx[(i<<2)+3]++;
            __m256d aa_val = _mm256_set_pd(rank->cpe_data.enth_aa[idx4],
                                           rank->cpe_data.enth_aa[idx3],
                                           rank->cpe_data.enth_aa[idx2],
                                           rank->cpe_data.enth_aa[idx1]);
            __m256d bb_val = _mm256_set_pd(rank->cpe_data.enth_bb[idx4],
                                           rank->cpe_data.enth_bb[idx3],
                                           rank->cpe_data.enth_bb[idx2],
                                           rank->cpe_data.enth_bb[idx1]);
            __m256d h_spec = _mm256_add_pd(bb_val,_mm256_mul_pd(aa_val,avx_temp[i]));
            _mm256_storeu_pd(out_h_spec_ptr+(s*h_spec_stride)+(i<<2),h_spec);
          }
        }
      }
#elif defined(USE_SSE_KERNELS)
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        sse_temp[i] = _mm_load_pd(in_temp_ptr+(i<<1));
        const double t1 = _mm_cvtsd_f64(sse_temp[i]);
        const double t2 = _mm_cvtsd_f64(_mm_shuffle_pd(sse_temp[i],sse_temp[i],1));
        check_temperature(rank, (i<<1), temp_lo, temp_hi, t1);
        check_temperature(rank, (i<<1)+1, temp_lo, temp_hi, t2);
        int bucket1 = (t1 - temp_lo) * temp_step_inv;
        int bucket2 = (t2 - temp_lo) * temp_step_inv;
        enth_idx[(i<<1)] = bucket1 * rank->n_spec;
        enth_idx[(i<<1)+1] = bucket2 * rank->n_spec;
      }

      for (int s = 0; s < rank->n_spec; s++) {
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          int idx1 = enth_idx[(i<<1)]++;
          int idx2 = enth_idx[(i<<1)+1]++;
          __m128d aa_val = _mm_set_pd(rank->cpe_data.enth_aa[idx2],
                                      rank->cpe_data.enth_aa[idx1]);
          __m128d bb_val = _mm_set_pd(rank->cpe_data.enth_bb[idx2],
                                      rank->cpe_data.enth_bb[idx1]);
          __m128d h_spec = _mm_add_pd(bb_val,_mm_mul_pd(aa_val,sse_temp[i]));
          _mm_stream_pd(out_h_spec_ptr+(s*h_spec_stride)+(i<<1),h_spec);
        }
      }
#else
      for (int i = 0; i < FULL_STRIP_SIZE; i++) {
        temp[i] = in_temp_ptr[i];
        check_temperature(rank, i, temp_lo, temp_hi, temp[i]);
        int bucket = (temp[i] - temp_lo) * temp_step_inv;
        enth_idx[i] = bucket * rank->n_spec;
      }
      for (int s = 0; s < rank->n_spec; s++) {
        for (int i = 0; i < FULL_STRIP_SIZE; i++) {
          int idx = enth_idx[i]++;
          double h_spec = (rank->cpe_data.enth_aa[idx] * temp[i] + rank->cpe_data.enth_bb[idx]);
          out_h_spec_ptr[s*h_spec_stride+i] = h_spec;
        }
      }
#endif 
      n_pts -= FULL_STRIP_SIZE;
    } else {
      for (size_t i = 0; i < n_pts; i++) {
        temp[i] = in_temp_ptr[i];
        check_temperature(rank, i, temp_lo, temp_hi, temp[i]);
        int bucket = (temp[i] - temp_lo) * temp_step_inv;
        enth_idx[i] = bucket * rank->n_spec;
      }
      for (int s = 0; s < rank->n_spec; s++) {
        for (size_t i = 0; i < n_pts; i++) {
          int idx = enth_idx[i]++;
          double h_spec = (rank->cpe_data.enth_aa[idx] * temp[i] + rank->cpe_data.enth_bb[idx]);
          out_h_spec_ptr[s*h_spec_stride+i] = h_spec;
        }
      }
      n_pts = 0;
    }
    in_temp_ptr += FULL_STRIP_SIZE;
    out_h_spec_ptr += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
}

/*static*/
void CalcEnthalpyTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                     const std::vector<RegionRequirement> &reqs,
                                     const std::vector<PhysicalRegion> &regions,
                                     Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  RegionAccessor<AccessorType::Generic,double> fa_temp = 
    regions[1].get_field_accessor(FID_TEMP).typeify<double>();

  if (dense_calc_enthalpy(rank, subgrid_bounds, fa_temp, reqs, regions))
    return;

  const double temp_hi = 3500.0 / T_REF;
  const double temp_lo = 250.0 / T_REF;
  unsigned idx = 0;
  for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++, idx++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double temp = fa_temp.read(dp);
    
    check_temperature(rank, idx, temp_lo, temp_hi, temp);
    assert(rank->cpe_data.num_pts > 0);
    double temp_step = (temp_hi - temp_lo) / (rank->cpe_data.num_pts - 1);

    int bucket = (temp - temp_lo) / temp_step;
    assert((bucket >= 0) && (bucket < rank->cpe_data.num_pts));
    const double *enth_aa = rank->cpe_data.enth_aa + (bucket * rank->n_spec);
    const double *enth_bb = rank->cpe_data.enth_bb + (bucket * rank->n_spec);

    for (int s = 0; s < rank->n_spec; s++) {
      double h_spec = (enth_aa[s] * temp + enth_bb[s]);
      regions[0].get_field_accessor(FID_H_SPEC(s)).typeify<double>().write(dp, h_spec);
    }
  }
#endif
}

CalcShortHeatFluxTask::CalcShortHeatFluxTask(S3DRank *r,
                                             Domain domain,
                                             TaskArgument global_arg,
                                             ArgumentMap arg_map,
                                             Predicate pred,
                                             bool must,
                                             MapperID id,
                                             MappingTagID tag,
                                             bool add_requirements)
 : IndexLauncher(CalcShortHeatFluxTask::TASK_ID, domain, global_arg, arg_map, pred, must,
                 id, tag | CalcShortHeatFluxTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    for(int d = 0; d < 3; d++) {
      rr_out.add_field(FID_HEATFLUX(d));
      rr_out.add_field(FID_YDIFFFLUX(rank->n_spec - 1, d));
    }
    add_region_requirement(rr_out);

    RegionRequirement rr_state(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_state.add_field(FID_LAMBDA);
    add_region_requirement(rr_state);

    RegionRequirement rr_in(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    for(int d = 0; d < 3; d++) {
      rr_in.add_field(FID_GRAD_T(d));
      for(int i = 0; i < rank->n_spec - 1; i++)
	rr_in.add_field(FID_YDIFFFLUX(i,d));
    }
    add_region_requirement(rr_in);

    RegionRequirement rr_hspec(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    for(int i = 0; i < rank->n_spec; i++)
      rr_hspec.add_field(FID_H_SPEC(i));
    add_region_requirement(rr_hspec);
  }
}

void CalcShortHeatFluxTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  for(int d = 0; d < 3; d++)
    check_field(runtime, ctx, rank->proc_grid_bounds, 
                rank->is_grid, rank->ip_top, rank->lr_int, FID_YDIFFFLUX(rank->n_spec-1, d),
                rank, &S3DRank::RHSFArrays::ydiffflux, d * rank->n_spec + (rank->n_spec - 1), 1e-15, 1e8, 0, 
                "Ydiffflux[%d,%d]", rank->n_spec - 1, d);
  for(int d = 0; d < 3; d++)
    check_field(runtime, ctx, rank->proc_grid_bounds, 
                rank->is_grid, rank->ip_top, rank->lr_int, FID_HEATFLUX(d),
                rank, &S3DRank::RHSFArrays::heatflux, d, 1e-10, 1e8, 0, "heatflux[%d]", d);
}

/*static*/
const char * const CalcShortHeatFluxTask::TASK_NAME = "short_heatflux_task";

/*static*/
bool CalcShortHeatFluxTask::dense_short_heatflux(S3DRank *rank, const Rect<3> &subgrid_bounds,
                                                 RegionAccessor<AccessorType::Generic,double> fa_lambda,
                                                 RegionAccessor<AccessorType::Generic,double> fa_grad_t_x,
                                                 RegionAccessor<AccessorType::Generic,double> fa_grad_t_y,
                                                 RegionAccessor<AccessorType::Generic,double> fa_grad_t_z,
                                                 RegionAccessor<AccessorType::Generic,double> fa_heatflux_x,
                                                 RegionAccessor<AccessorType::Generic,double> fa_heatflux_y,
                                                 RegionAccessor<AccessorType::Generic,double> fa_heatflux_z,
                                                 RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_x,
                                                 RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_y,
                                                 RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_z,
                                                 const std::vector<RegionRequirement> &reqs,
                                                 const std::vector<PhysicalRegion> &regions)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], offsets[3];

  const double *in_lambda_ptr = fa_lambda.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_lambda_ptr || (subrect != subgrid_bounds) ||
     !offsets_are_dense<3,double>(subgrid_bounds, in_offsets)) return false;
  
  const double *in_grad_t_x = fa_grad_t_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_t_x || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_grad_t_y = fa_grad_t_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_t_y || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_grad_t_z = fa_grad_t_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_t_z || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_heatflux_x = fa_heatflux_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_heatflux_x || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_heatflux_y = fa_heatflux_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_heatflux_y || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_heatflux_z = fa_heatflux_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_heatflux_z || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_ydiffflux_last_x = fa_ydiffflux_last_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_ydiffflux_last_x || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_ydiffflux_last_y = fa_ydiffflux_last_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_ydiffflux_last_y || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_ydiffflux_last_z = fa_ydiffflux_last_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_ydiffflux_last_z || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  RegionAccessor<AccessorType::Generic,double> fa_h_spec = regions[3].get_field_accessor(FID_H_SPEC(0)).typeify<double>();
  const double *in_h_spec = fa_h_spec.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in_h_spec || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  off_t h_spec_stride; 
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_spec = regions[3].get_field_accessor(FID_H_SPEC(1)).typeify<double>();
    const double *next_h_spec = fa_next_spec.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_h_spec || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
    ByteOffset stride(next_h_spec,in_h_spec);
    h_spec_stride = stride.offset/sizeof(double);
  }

  RegionAccessor<AccessorType::Generic,double> fa_diffflux = regions[2].get_field_accessor(FID_YDIFFFLUX(0,0)).typeify<double>();
  const double *in_diffflux = fa_diffflux.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_diffflux || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  off_t diffflux_stride;
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_flux = regions[2].get_field_accessor(FID_YDIFFFLUX(0,1)).typeify<double>();
    const double *next_diffflux = fa_next_flux.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_diffflux || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
    ByteOffset stride(next_diffflux,in_diffflux);
    diffflux_stride = stride.offset/sizeof(double);
  }

  size_t n_pts = subgrid_bounds.volume();

#ifdef BULLDOZER
// Fit in L1 and only use first way in each set
#define FULL_STRIP_SIZE 64 
#else
#define FULL_STRIP_SIZE 512
#endif 

#if defined(USE_AVX_KERNELS)
  __m256d avx_heatflux_x[FULL_STRIP_SIZE/4];
  __m256d avx_heatflux_y[FULL_STRIP_SIZE/4];
  __m256d avx_heatflux_z[FULL_STRIP_SIZE/4];
  __m256d avx_ydifflast_x[FULL_STRIP_SIZE/4];
  __m256d avx_ydifflast_y[FULL_STRIP_SIZE/4];
  __m256d avx_ydifflast_z[FULL_STRIP_SIZE/4];
#elif defined(USE_SSE_KERNELS)
  __m128d sse_heatflux_x[FULL_STRIP_SIZE/2];
  __m128d sse_heatflux_y[FULL_STRIP_SIZE/2];
  __m128d sse_heatflux_z[FULL_STRIP_SIZE/2];
  __m128d sse_ydifflast_x[FULL_STRIP_SIZE/2];
  __m128d sse_ydifflast_y[FULL_STRIP_SIZE/2];
  __m128d sse_ydifflast_z[FULL_STRIP_SIZE/2];
#endif
  double heatflux_x[FULL_STRIP_SIZE];
  double heatflux_y[FULL_STRIP_SIZE];
  double heatflux_z[FULL_STRIP_SIZE];
  double ydifflast_x[FULL_STRIP_SIZE];
  double ydifflast_y[FULL_STRIP_SIZE];
  double ydifflast_z[FULL_STRIP_SIZE];
  
  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined(USE_AVX_KERNELS)
      // Check for alignement when doing inputs
      if (aligned<32>(in_lambda_ptr) && aligned<32>(in_grad_t_x) && aligned<32>(in_grad_t_z) &&
          aligned<32>(in_h_spec) && aligned<32>(in_diffflux)) {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d lambda = _mm256_load_pd(in_lambda_ptr+(i<<2));
          __m256d grad_t_x = _mm256_load_pd(in_grad_t_x+(i<<2));
          __m256d grad_t_y = _mm256_load_pd(in_grad_t_y+(i<<2));
          __m256d grad_t_z = _mm256_load_pd(in_grad_t_z+(i<<2));
          // Fast lambda negation
          lambda = _mm256_xor_pd(lambda,_mm256_set1_pd(-0.0));
          avx_heatflux_x[i] = _mm256_mul_pd(lambda,grad_t_x);
          avx_heatflux_y[i] = _mm256_mul_pd(lambda,grad_t_y);
          avx_heatflux_z[i] = _mm256_mul_pd(lambda,grad_t_z);

          avx_ydifflast_x[i] = _mm256_set1_pd(0.0);
          avx_ydifflast_y[i] = _mm256_set1_pd(0.0);
          avx_ydifflast_z[i] = _mm256_set1_pd(0.0);
        }
        for (int s = 0; s < (rank->n_spec-1); s++) {
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d h_spec = _mm256_load_pd(in_h_spec+(s*h_spec_stride)+(i<<2));
            __m256d ydiff_x = _mm256_load_pd(in_diffflux+(s*3*diffflux_stride)+(i<<2));
            __m256d ydiff_y = _mm256_load_pd(in_diffflux+((s*3+1)*diffflux_stride)+(i<<2));
            __m256d ydiff_z = _mm256_load_pd(in_diffflux+((s*3+2)*diffflux_stride)+(i<<2));

            avx_heatflux_x[i] = _mm256_add_pd(avx_heatflux_x[i],_mm256_mul_pd(h_spec,ydiff_x));
            avx_heatflux_y[i] = _mm256_add_pd(avx_heatflux_y[i],_mm256_mul_pd(h_spec,ydiff_y));
            avx_heatflux_z[i] = _mm256_add_pd(avx_heatflux_z[i],_mm256_mul_pd(h_spec,ydiff_z));

            avx_ydifflast_x[i] = _mm256_sub_pd(avx_ydifflast_x[i],ydiff_x);
            avx_ydifflast_y[i] = _mm256_sub_pd(avx_ydifflast_y[i],ydiff_y);
            avx_ydifflast_z[i] = _mm256_sub_pd(avx_ydifflast_z[i],ydiff_z);
          }
        }
      } else {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d lambda = _mm256_loadu_pd(in_lambda_ptr+(i<<2));
          __m256d grad_t_x = _mm256_loadu_pd(in_grad_t_x+(i<<2));
          __m256d grad_t_y = _mm256_loadu_pd(in_grad_t_y+(i<<2));
          __m256d grad_t_z = _mm256_loadu_pd(in_grad_t_z+(i<<2));
          // Fast lambda negation
          lambda = _mm256_xor_pd(lambda,_mm256_set1_pd(-0.0));
          avx_heatflux_x[i] = _mm256_mul_pd(lambda,grad_t_x);
          avx_heatflux_y[i] = _mm256_mul_pd(lambda,grad_t_y);
          avx_heatflux_z[i] = _mm256_mul_pd(lambda,grad_t_z);

          avx_ydifflast_x[i] = _mm256_set1_pd(0.0);
          avx_ydifflast_y[i] = _mm256_set1_pd(0.0);
          avx_ydifflast_z[i] = _mm256_set1_pd(0.0);
        }
        for (int s = 0; s < (rank->n_spec-1); s++) {
          for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d h_spec = _mm256_loadu_pd(in_h_spec+(s*h_spec_stride)+(i<<2));
            __m256d ydiff_x = _mm256_loadu_pd(in_diffflux+(s*3*diffflux_stride)+(i<<2));
            __m256d ydiff_y = _mm256_loadu_pd(in_diffflux+((s*3+1)*diffflux_stride)+(i<<2));
            __m256d ydiff_z = _mm256_loadu_pd(in_diffflux+((s*3+2)*diffflux_stride)+(i<<2));

            avx_heatflux_x[i] = _mm256_add_pd(avx_heatflux_x[i],_mm256_mul_pd(h_spec,ydiff_x));
            avx_heatflux_y[i] = _mm256_add_pd(avx_heatflux_y[i],_mm256_mul_pd(h_spec,ydiff_y));
            avx_heatflux_z[i] = _mm256_add_pd(avx_heatflux_z[i],_mm256_mul_pd(h_spec,ydiff_z));

            avx_ydifflast_x[i] = _mm256_sub_pd(avx_ydifflast_x[i],ydiff_x);
            avx_ydifflast_y[i] = _mm256_sub_pd(avx_ydifflast_y[i],ydiff_y);
            avx_ydifflast_z[i] = _mm256_sub_pd(avx_ydifflast_z[i],ydiff_z);
          }
        }
      }
      // Check for alignment when doing outputs
      if (aligned<32>(out_heatflux_x) && aligned<32>(out_heatflux_y) && aligned<32>(out_heatflux_z) &&
          aligned<32>(out_ydiffflux_last_x) && aligned<32>(out_ydiffflux_last_y) &&
          aligned<32>(out_ydiffflux_last_z) && aligned<32>(in_h_spec)) {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d h_spec_last = _mm256_load_pd(in_h_spec+((rank->n_spec-1)*h_spec_stride)+(i<<2));
          avx_heatflux_x[i] = _mm256_add_pd(avx_heatflux_x[i],_mm256_mul_pd(h_spec_last,avx_ydifflast_x[i]));
          avx_heatflux_y[i] = _mm256_add_pd(avx_heatflux_y[i],_mm256_mul_pd(h_spec_last,avx_ydifflast_y[i]));
          avx_heatflux_z[i] = _mm256_add_pd(avx_heatflux_z[i],_mm256_mul_pd(h_spec_last,avx_ydifflast_z[i]));

          _mm256_stream_pd(out_heatflux_x+(i<<2),avx_heatflux_x[i]);
          _mm256_stream_pd(out_heatflux_y+(i<<2),avx_heatflux_y[i]);
          _mm256_stream_pd(out_heatflux_z+(i<<2),avx_heatflux_z[i]);

          _mm256_stream_pd(out_ydiffflux_last_x+(i<<2),avx_ydifflast_x[i]);
          _mm256_stream_pd(out_ydiffflux_last_y+(i<<2),avx_ydifflast_y[i]);
          _mm256_stream_pd(out_ydiffflux_last_z+(i<<2),avx_ydifflast_z[i]);
        }
      } else {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d h_spec_last = _mm256_loadu_pd(in_h_spec+((rank->n_spec-1)*h_spec_stride)+(i<<2));
          avx_heatflux_x[i] = _mm256_add_pd(avx_heatflux_x[i],_mm256_mul_pd(h_spec_last,avx_ydifflast_x[i]));
          avx_heatflux_y[i] = _mm256_add_pd(avx_heatflux_y[i],_mm256_mul_pd(h_spec_last,avx_ydifflast_y[i]));
          avx_heatflux_z[i] = _mm256_add_pd(avx_heatflux_z[i],_mm256_mul_pd(h_spec_last,avx_ydifflast_z[i]));

          _mm256_storeu_pd(out_heatflux_x+(i<<2),avx_heatflux_x[i]);
          _mm256_storeu_pd(out_heatflux_y+(i<<2),avx_heatflux_y[i]);
          _mm256_storeu_pd(out_heatflux_z+(i<<2),avx_heatflux_z[i]);

          _mm256_storeu_pd(out_ydiffflux_last_x+(i<<2),avx_ydifflast_x[i]);
          _mm256_storeu_pd(out_ydiffflux_last_y+(i<<2),avx_ydifflast_y[i]);
          _mm256_storeu_pd(out_ydiffflux_last_z+(i<<2),avx_ydifflast_z[i]);
        }
      }
#elif defined(USE_SSE_KERNELS)
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d lambda = _mm_stream_load_pd(in_lambda_ptr+(i<<1));
        __m128d grad_t_x = _mm_stream_load_pd(in_grad_t_x+(i<<1));
        __m128d grad_t_y = _mm_stream_load_pd(in_grad_t_y+(i<<1));
        __m128d grad_t_z = _mm_stream_load_pd(in_grad_t_z+(i<<1));
        // Fast lambda negation
        lambda = _mm_xor_pd(lambda,_mm_set1_pd(-0.0));
        sse_heatflux_x[i] = _mm_mul_pd(lambda,grad_t_x);
        sse_heatflux_y[i] = _mm_mul_pd(lambda,grad_t_y);
        sse_heatflux_z[i] = _mm_mul_pd(lambda,grad_t_z);

        sse_ydifflast_x[i] = _mm_set1_pd(0.0);
        sse_ydifflast_y[i] = _mm_set1_pd(0.0);
        sse_ydifflast_z[i] = _mm_set1_pd(0.0);
      }
      for (int s = 0; s < (rank->n_spec-1); s++) {
        for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          __m128d h_spec = _mm_stream_load_pd(in_h_spec+(s*h_spec_stride)+(i<<1));
          __m128d ydiff_x = _mm_stream_load_pd(in_diffflux+(s*3*diffflux_stride)+(i<<1));
          __m128d ydiff_y = _mm_stream_load_pd(in_diffflux+((s*3+1)*diffflux_stride)+(i<<1));
          __m128d ydiff_z = _mm_stream_load_pd(in_diffflux+((s*3+2)*diffflux_stride)+(i<<1));

          sse_heatflux_x[i] = _mm_add_pd(sse_heatflux_x[i],_mm_mul_pd(h_spec,ydiff_x));
          sse_heatflux_y[i] = _mm_add_pd(sse_heatflux_y[i],_mm_mul_pd(h_spec,ydiff_y));
          sse_heatflux_z[i] = _mm_add_pd(sse_heatflux_z[i],_mm_mul_pd(h_spec,ydiff_z));

          sse_ydifflast_x[i] = _mm_sub_pd(sse_ydifflast_x[i],ydiff_x);
          sse_ydifflast_y[i] = _mm_sub_pd(sse_ydifflast_y[i],ydiff_y);
          sse_ydifflast_z[i] = _mm_sub_pd(sse_ydifflast_z[i],ydiff_z);
        }
      }
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d h_spec_last = _mm_stream_load_pd(in_h_spec+((rank->n_spec-1)*h_spec_stride)+(i<<1));
        sse_heatflux_x[i] = _mm_add_pd(sse_heatflux_x[i],_mm_mul_pd(h_spec_last,sse_ydifflast_x[i]));
        sse_heatflux_y[i] = _mm_add_pd(sse_heatflux_y[i],_mm_mul_pd(h_spec_last,sse_ydifflast_y[i]));
        sse_heatflux_z[i] = _mm_add_pd(sse_heatflux_z[i],_mm_mul_pd(h_spec_last,sse_ydifflast_z[i]));

        _mm_stream_pd(out_heatflux_x+(i<<1),sse_heatflux_x[i]);
        _mm_stream_pd(out_heatflux_y+(i<<1),sse_heatflux_y[i]);
        _mm_stream_pd(out_heatflux_z+(i<<1),sse_heatflux_z[i]);

        _mm_stream_pd(out_ydiffflux_last_x+(i<<1),sse_ydifflast_x[i]);
        _mm_stream_pd(out_ydiffflux_last_y+(i<<1),sse_ydifflast_y[i]);
        _mm_stream_pd(out_ydiffflux_last_z+(i<<1),sse_ydifflast_z[i]);
      }
#else
      for (int i = 0; i < FULL_STRIP_SIZE; i++) {
        double lambda = in_lambda_ptr[i];
        double grad_t_x = in_grad_t_x[i];
        double grad_t_y = in_grad_t_y[i];
        double grad_t_z = in_grad_t_z[i];
        heatflux_x[i] = -lambda * grad_t_x;
        heatflux_y[i] = -lambda * grad_t_y;
        heatflux_z[i] = -lambda * grad_t_z;
        ydifflast_x[i] = 0.0;
        ydifflast_y[i] = 0.0;
        ydifflast_z[i] = 0.0;
      }
      for (int s = 0; s < (rank->n_spec-1); s++) {
        for (int i = 0; i < FULL_STRIP_SIZE; i++) {
          double h_spec = in_h_spec[s*h_spec_stride+i];
          double ydiff_x = in_diffflux[(s*3)*diffflux_stride+i];
          double ydiff_y = in_diffflux[(s*3+1)*diffflux_stride+i];
          double ydiff_z = in_diffflux[(s*3+2)*diffflux_stride+i];

          heatflux_x[i] += h_spec * ydiff_x;
          heatflux_y[i] += h_spec * ydiff_y;
          heatflux_z[i] += h_spec * ydiff_z;

          ydifflast_x[i] -= ydiff_x;
          ydifflast_y[i] -= ydiff_y;
          ydifflast_z[i] -= ydiff_z;
        }
      }
      for (int i = 0; i < FULL_STRIP_SIZE; i++) {
        double h_spec_last = in_h_spec[(rank->n_spec-1)*h_spec_stride+i];
        heatflux_x[i] += h_spec_last * ydifflast_x[i];
        heatflux_y[i] += h_spec_last * ydifflast_y[i];
        heatflux_z[i] += h_spec_last * ydifflast_z[i];

        out_heatflux_x[i] = heatflux_x[i];
        out_heatflux_y[i] = heatflux_y[i];
        out_heatflux_z[i] = heatflux_z[i];

        out_ydiffflux_last_x[i] = ydifflast_x[i];
        out_ydiffflux_last_y[i] = ydifflast_y[i];
        out_ydiffflux_last_z[i] = ydifflast_z[i];
      }
#endif
      n_pts -= FULL_STRIP_SIZE;
    } else {
      for (size_t i = 0; i < n_pts; i++) {
        double lambda = in_lambda_ptr[i];
        double grad_t_x = in_grad_t_x[i];
        double grad_t_y = in_grad_t_y[i];
        double grad_t_z = in_grad_t_z[i];
        heatflux_x[i] = -lambda * grad_t_x;
        heatflux_y[i] = -lambda * grad_t_y;
        heatflux_z[i] = -lambda * grad_t_z;
        ydifflast_x[i] = 0.0;
        ydifflast_y[i] = 0.0;
        ydifflast_z[i] = 0.0;
      }
      for (int s = 0; s < (rank->n_spec-1); s++) {
        for (size_t i = 0; i < n_pts; i++) {
          double h_spec = in_h_spec[s*h_spec_stride+i];
          double ydiff_x = in_diffflux[(s*3)*diffflux_stride+i];
          double ydiff_y = in_diffflux[(s*3+1)*diffflux_stride+i];
          double ydiff_z = in_diffflux[(s*3+2)*diffflux_stride+i];

          heatflux_x[i] += h_spec * ydiff_x;
          heatflux_y[i] += h_spec * ydiff_y;
          heatflux_z[i] += h_spec * ydiff_z;

          ydifflast_x[i] -= ydiff_x;
          ydifflast_y[i] -= ydiff_y;
          ydifflast_z[i] -= ydiff_z;
        }
      }
      for (size_t i = 0; i < n_pts; i++) {
        double h_spec_last = in_h_spec[(rank->n_spec-1)*h_spec_stride+i];
        heatflux_x[i] += h_spec_last * ydifflast_x[i];
        heatflux_y[i] += h_spec_last * ydifflast_y[i];
        heatflux_z[i] += h_spec_last * ydifflast_z[i];

        out_heatflux_x[i] = heatflux_x[i];
        out_heatflux_y[i] = heatflux_y[i];
        out_heatflux_z[i] = heatflux_z[i];

        out_ydiffflux_last_x[i] = ydifflast_x[i];
        out_ydiffflux_last_y[i] = ydifflast_y[i];
        out_ydiffflux_last_z[i] = ydifflast_z[i];
      }
      n_pts = 0;
    }
    in_lambda_ptr += FULL_STRIP_SIZE;
    in_grad_t_x += FULL_STRIP_SIZE;
    in_grad_t_y += FULL_STRIP_SIZE;
    in_grad_t_z += FULL_STRIP_SIZE;
    in_h_spec += FULL_STRIP_SIZE;
    in_diffflux += FULL_STRIP_SIZE;
    out_heatflux_x += FULL_STRIP_SIZE;
    out_heatflux_y += FULL_STRIP_SIZE;
    out_heatflux_z += FULL_STRIP_SIZE;
    out_ydiffflux_last_x += FULL_STRIP_SIZE;
    out_ydiffflux_last_y += FULL_STRIP_SIZE;
    out_ydiffflux_last_z += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
}

/*static*/
void CalcShortHeatFluxTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                          const std::vector<RegionRequirement> &reqs,
                                          const std::vector<PhysicalRegion> &regions,
                                          Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_x = regions[0].get_field_accessor(FID_HEATFLUX_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_y = regions[0].get_field_accessor(FID_HEATFLUX_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_z = regions[0].get_field_accessor(FID_HEATFLUX_Z).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_x = regions[0].get_field_accessor(FID_YDIFFFLUX_X(rank->n_spec - 1)).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_y = regions[0].get_field_accessor(FID_YDIFFFLUX_Y(rank->n_spec - 1)).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_z = regions[0].get_field_accessor(FID_YDIFFFLUX_Z(rank->n_spec - 1)).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_lambda = regions[1].get_field_accessor(FID_LAMBDA).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_grad_t_x = regions[2].get_field_accessor(FID_GRAD_T_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_t_y = regions[2].get_field_accessor(FID_GRAD_T_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_t_z = regions[2].get_field_accessor(FID_GRAD_T_Z).typeify<double>();

  if (dense_short_heatflux(rank, subgrid_bounds, fa_lambda, fa_grad_t_x, fa_grad_t_y, fa_grad_t_z,
                           fa_heatflux_x, fa_heatflux_y, fa_heatflux_z,
                           fa_ydiffflux_last_x, fa_ydiffflux_last_y, fa_ydiffflux_last_z,
                           reqs, regions))
    return;

  for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double grad_t_x = fa_grad_t_x.read(dp);
    double grad_t_y = fa_grad_t_y.read(dp);
    double grad_t_z = fa_grad_t_z.read(dp);
    double lambda = fa_lambda.read(dp);

    double heatflux_x = -lambda * grad_t_x;
    double heatflux_y = -lambda * grad_t_y;
    double heatflux_z = -lambda * grad_t_z;
    double ydifflast_x = 0.0;
    double ydifflast_y = 0.0;
    double ydifflast_z = 0.0;

    for(int i = 0; i < rank->n_spec - 1; i++) {
      double h_spec = regions[3].get_field_accessor(FID_H_SPEC(i)).typeify<double>().read(dp);
      double ydiff_x = regions[2].get_field_accessor(FID_YDIFFFLUX_X(i)).typeify<double>().read(dp);
      double ydiff_y = regions[2].get_field_accessor(FID_YDIFFFLUX_Y(i)).typeify<double>().read(dp);
      double ydiff_z = regions[2].get_field_accessor(FID_YDIFFFLUX_Z(i)).typeify<double>().read(dp);

      heatflux_x += h_spec * ydiff_x;
      heatflux_y += h_spec * ydiff_y;
      heatflux_z += h_spec * ydiff_z;

      ydifflast_x -= ydiff_x;
      ydifflast_y -= ydiff_y;
      ydifflast_z -= ydiff_z;
    }
    // final species
    double h_spec_last = regions[3].get_field_accessor(FID_H_SPEC(rank->n_spec-1)).typeify<double>().read(dp);
    heatflux_x += h_spec_last * ydifflast_x;
    heatflux_y += h_spec_last * ydifflast_y;
    heatflux_z += h_spec_last * ydifflast_z;
 
    fa_heatflux_x.write(dp, heatflux_x);
    fa_heatflux_y.write(dp, heatflux_y);
    fa_heatflux_z.write(dp, heatflux_z);

    // write out last species' diff flux too
    regions[0].get_field_accessor(FID_YDIFFFLUX_X(rank->n_spec - 1)).typeify<double>().write(dp, ydifflast_x);
    regions[0].get_field_accessor(FID_YDIFFFLUX_Y(rank->n_spec - 1)).typeify<double>().write(dp, ydifflast_y);
    regions[0].get_field_accessor(FID_YDIFFFLUX_Z(rank->n_spec - 1)).typeify<double>().write(dp, ydifflast_z);
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern 
void gpu_short_heat_flux(const int num_species,
                         const int max_elements,
                         const int h_spec_stride,
                         const int diffflux_stride,
                         const double *h_spec_d,
                         const double *diffflux_d,
                         const double *lambda_d,
                         const double *grad_t_x_d,
                         const double *grad_t_y_d,
                         const double *grad_t_z_d,
                         double *heatflux_x_d,
                         double *heatflux_y_d,
                         double *heatflux_z_d,
                         double *diffflux_last_x_d,
                         double *diffflux_last_y_d,
                         double *diffflux_last_z_d);
#endif

/*static*/
void CalcShortHeatFluxTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                          const std::vector<RegionRequirement> &reqs,
                                          const std::vector<PhysicalRegion> &regions,
                                          Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS

  RegionAccessor<AccessorType::Generic,double> fa_heatflux_x = regions[0].get_field_accessor(FID_HEATFLUX_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_y = regions[0].get_field_accessor(FID_HEATFLUX_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_z = regions[0].get_field_accessor(FID_HEATFLUX_Z).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_x = regions[0].get_field_accessor(FID_YDIFFFLUX_X(rank->n_spec - 1)).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_y = regions[0].get_field_accessor(FID_YDIFFFLUX_Y(rank->n_spec - 1)).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_z = regions[0].get_field_accessor(FID_YDIFFFLUX_Z(rank->n_spec - 1)).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_lambda = regions[1].get_field_accessor(FID_LAMBDA).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_grad_t_x = regions[2].get_field_accessor(FID_GRAD_T_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_t_y = regions[2].get_field_accessor(FID_GRAD_T_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_t_z = regions[2].get_field_accessor(FID_GRAD_T_Z).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_h_spec = regions[3].get_field_accessor(FID_H_SPEC(0)).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_diffflux = regions[2].get_field_accessor(FID_YDIFFFLUX(0,0)).typeify<double>();

  Rect<3> subrect;
  ByteOffset in_offsets[3], offsets[3];

  const double *in_lambda_ptr = fa_lambda.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_lambda_ptr || (subrect != subgrid_bounds) ||
     !offsets_are_dense<3,double>(subgrid_bounds, in_offsets)) assert(false);
  
  const double *in_grad_t_x = fa_grad_t_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_t_x || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const double *in_grad_t_y = fa_grad_t_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_t_y || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const double *in_grad_t_z = fa_grad_t_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_t_z || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  double *out_heatflux_x = fa_heatflux_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_heatflux_x || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  double *out_heatflux_y = fa_heatflux_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_heatflux_y || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  double *out_heatflux_z = fa_heatflux_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_heatflux_z || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  double *out_ydiffflux_last_x = fa_ydiffflux_last_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_ydiffflux_last_x || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  double *out_ydiffflux_last_y = fa_ydiffflux_last_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_ydiffflux_last_y || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  double *out_ydiffflux_last_z = fa_ydiffflux_last_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_ydiffflux_last_z || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const double *in_h_spec = fa_h_spec.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!in_h_spec || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  off_t h_spec_stride; 
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_spec = regions[3].get_field_accessor(FID_H_SPEC(1)).typeify<double>();
    const double *next_h_spec = fa_next_spec.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_h_spec || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
    ByteOffset stride(next_h_spec,in_h_spec);
    h_spec_stride = stride.offset/sizeof(double);
  }

  const double *in_diffflux = fa_diffflux.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_diffflux || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  off_t diffflux_stride;
  {
    RegionAccessor<AccessorType::Generic,double> fa_next_flux = regions[2].get_field_accessor(FID_YDIFFFLUX(0,1)).typeify<double>();
    const double *next_diffflux = fa_next_flux.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!next_diffflux || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
    ByteOffset stride(next_diffflux,in_diffflux);
    diffflux_stride = stride.offset/sizeof(double);
  }

  const int max_elements = subgrid_bounds.volume();

  gpu_short_heat_flux(rank->n_spec, max_elements, h_spec_stride, diffflux_stride,
                      in_h_spec, in_diffflux, in_lambda_ptr,
                      in_grad_t_x, in_grad_t_y, in_grad_t_z,
                      out_heatflux_x, out_heatflux_y, out_heatflux_z,
                      out_ydiffflux_last_x, out_ydiffflux_last_y, out_ydiffflux_last_z);
#endif
#endif
}

CalcTauTask::CalcTauTask(S3DRank *r,
                         Domain domain,
                         TaskArgument global_arg,
                         ArgumentMap arg_map,
                         Predicate pred,
                         bool must,
                         MapperID id,
                         MappingTagID tag,
                         bool add_requirements)
  : IndexLauncher(CalcTauTask::TASK_ID, domain, global_arg, 
                  arg_map, pred, must, id, tag | CalcTauTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    rr_out.add_field(FID_TAU_XX);
    rr_out.add_field(FID_TAU_XY);
    rr_out.add_field(FID_TAU_XZ);
    rr_out.add_field(FID_TAU_YY);
    rr_out.add_field(FID_TAU_YZ);
    rr_out.add_field(FID_TAU_ZZ);
    add_region_requirement(rr_out);

    RegionRequirement rr_state(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_state.add_field(FID_VISCOSITY);
    add_region_requirement(rr_state);

    RegionRequirement rr_in(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    rr_in.add_field(FID_GRAD_VEL_X_X);
    rr_in.add_field(FID_GRAD_VEL_X_Y);
    rr_in.add_field(FID_GRAD_VEL_X_Z);
    rr_in.add_field(FID_GRAD_VEL_Y_X);
    rr_in.add_field(FID_GRAD_VEL_Y_Y);
    rr_in.add_field(FID_GRAD_VEL_Y_Z);
    rr_in.add_field(FID_GRAD_VEL_Z_X);
    rr_in.add_field(FID_GRAD_VEL_Z_Y);
    rr_in.add_field(FID_GRAD_VEL_Z_Z);
    add_region_requirement(rr_in);
  }
}

void CalcTauTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_TAU_XX,
              rank, &S3DRank::RHSFArrays::tau, 0, 1e-10, 1e8, 0, "tau_xx");
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_TAU_XY,
              rank, &S3DRank::RHSFArrays::tau, 1, 1e-10, 1e8, 0, "tau_xy");
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_TAU_XZ,
              rank, &S3DRank::RHSFArrays::tau, 2, 1e-10, 1e8, 0, "tau_xz");
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_TAU_XY,
              rank, &S3DRank::RHSFArrays::tau, 3, 1e-10, 1e8, 0, "tau_yx");
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_TAU_YY,
              rank, &S3DRank::RHSFArrays::tau, 4, 1e-10, 1e8, 0, "tau_yy");
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_TAU_YZ,
              rank, &S3DRank::RHSFArrays::tau, 5, 1e-10, 1e8, 0, "tau_yz");
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_TAU_XZ,
              rank, &S3DRank::RHSFArrays::tau, 6, 1e-10, 1e8, 0, "tau_zx");
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_TAU_YZ,
              rank, &S3DRank::RHSFArrays::tau, 7, 1e-10, 1e8, 0, "tau_zy");
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_TAU_ZZ,
              rank, &S3DRank::RHSFArrays::tau, 8, 1e-10, 1e8, 0, "tau_zz");
}

/*static*/ const char * const CalcTauTask::TASK_NAME = "calc_tau";

/*static*/
bool CalcTauTask::dense_calc_tau_task(RegionAccessor<AccessorType::Generic,double> fa_out_tau_xx,
                                      RegionAccessor<AccessorType::Generic,double> fa_out_tau_xy,
                                      RegionAccessor<AccessorType::Generic,double> fa_out_tau_xz,
                                      RegionAccessor<AccessorType::Generic,double> fa_out_tau_yy,
                                      RegionAccessor<AccessorType::Generic,double> fa_out_tau_yz,
                                      RegionAccessor<AccessorType::Generic,double> fa_out_tau_zz,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_viscosity,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_x_x,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_x_y,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_x_z,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_y_x,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_y_y,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_y_z,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_z_x,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_z_y,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_z_z,
                                      Rect<3> subgrid_bounds)
{
  Rect<3> subrect;
  ByteOffset out_offsets[3], offsets[3];

  double *tau_xx_ptr = fa_out_tau_xx.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if (!tau_xx_ptr || (subrect != subgrid_bounds) || 
      !offsets_are_dense<3,double>(subgrid_bounds, out_offsets)) return false;

  double *tau_xy_ptr = fa_out_tau_xy.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!tau_xy_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *tau_xz_ptr = fa_out_tau_xz.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!tau_xz_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *tau_yy_ptr = fa_out_tau_yy.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!tau_yy_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *tau_yz_ptr = fa_out_tau_yz.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!tau_yz_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *tau_zz_ptr = fa_out_tau_zz.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!tau_zz_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *viscosity_ptr = fa_in_viscosity.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!viscosity_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *grad_vel_xx_ptr = fa_in_grad_vel_x_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!grad_vel_xx_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *grad_vel_xy_ptr = fa_in_grad_vel_x_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!grad_vel_xy_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *grad_vel_xz_ptr = fa_in_grad_vel_x_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!grad_vel_xz_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *grad_vel_yx_ptr = fa_in_grad_vel_y_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!grad_vel_yx_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *grad_vel_yy_ptr = fa_in_grad_vel_y_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!grad_vel_yy_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *grad_vel_yz_ptr = fa_in_grad_vel_y_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!grad_vel_yz_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *grad_vel_zx_ptr = fa_in_grad_vel_z_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!grad_vel_zx_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *grad_vel_zy_ptr = fa_in_grad_vel_z_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!grad_vel_zy_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  const double *grad_vel_zz_ptr = fa_in_grad_vel_z_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if (!grad_vel_zz_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  size_t n_pts = subgrid_bounds.volume();

#ifdef BULLDOZER
#define FULL_STRIP_SIZE 512
#else
#define FULL_STRIP_SIZE 2048
#endif

#if defined(USE_AVX_KERNELS)
  __m256d avx_visc[FULL_STRIP_SIZE/4];
#elif defined(USE_SSE_KERNELS)
  __m128d sse_visc[FULL_STRIP_SIZE/2];
#endif

  double visc[FULL_STRIP_SIZE];

  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined(USE_AVX_KERNELS)
      // Check for alignment on inputs
      if (aligned<32>(tau_xx_ptr) && aligned<32>(tau_xy_ptr) && aligned<32>(tau_xz_ptr) &&
          aligned<32>(tau_yy_ptr) && aligned<32>(tau_yz_ptr) && aligned<32>(tau_zz_ptr) &&
          aligned<32>(grad_vel_xx_ptr) && aligned<32>(grad_vel_xy_ptr) && aligned<32>(grad_vel_xz_ptr) &&
          aligned<32>(grad_vel_yx_ptr) && aligned<32>(grad_vel_yy_ptr) && aligned<32>(grad_vel_yz_ptr) &&
          aligned<32>(grad_vel_zx_ptr) && aligned<32>(grad_vel_zy_ptr) && aligned<32>(grad_vel_zz_ptr) &&
          aligned<32>(viscosity_ptr)) {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          avx_visc[i] = _mm256_load_pd(viscosity_ptr+(i<<2));
          __m256d grad_vel_xx = _mm256_load_pd(grad_vel_xx_ptr+(i<<2));
          __m256d grad_vel_yy = _mm256_load_pd(grad_vel_yy_ptr+(i<<2));
          __m256d grad_vel_zz = _mm256_load_pd(grad_vel_zz_ptr+(i<<2));
          __m256d sum_term = _mm256_add_pd(grad_vel_xx,_mm256_add_pd(grad_vel_yy,grad_vel_zz));
          sum_term = _mm256_mul_pd(sum_term, _mm256_set1_pd(1.0/3.0));
          __m256d tau_xx = _mm256_mul_pd(_mm256_set1_pd(2.0),_mm256_mul_pd(avx_visc[i],_mm256_sub_pd(grad_vel_xx,sum_term)));
          _mm256_stream_pd(tau_xx_ptr+(i<<2), tau_xx);
          __m256d tau_yy = _mm256_mul_pd(_mm256_set1_pd(2.0),_mm256_mul_pd(avx_visc[i],_mm256_sub_pd(grad_vel_yy,sum_term)));
          _mm256_stream_pd(tau_yy_ptr+(i<<2), tau_yy);
          __m256d tau_zz = _mm256_mul_pd(_mm256_set1_pd(2.0),_mm256_mul_pd(avx_visc[i],_mm256_sub_pd(grad_vel_zz,sum_term)));
          _mm256_stream_pd(tau_zz_ptr+(i<<2), tau_zz);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_vel_xy = _mm256_load_pd(grad_vel_xy_ptr+(i<<2));
          __m256d grad_vel_yx = _mm256_load_pd(grad_vel_yx_ptr+(i<<2));
          __m256d tau_xy = _mm256_mul_pd(avx_visc[i],_mm256_add_pd(grad_vel_xy,grad_vel_yx));
          _mm256_stream_pd(tau_xy_ptr+(i<<2), tau_xy);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_vel_xz = _mm256_load_pd(grad_vel_xz_ptr+(i<<2));
          __m256d grad_vel_zx = _mm256_load_pd(grad_vel_zx_ptr+(i<<2));
          __m256d tau_xz = _mm256_mul_pd(avx_visc[i],_mm256_add_pd(grad_vel_xz,grad_vel_zx));
          _mm256_stream_pd(tau_xz_ptr+(i<<2), tau_xz);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_vel_yz = _mm256_load_pd(grad_vel_yz_ptr+(i<<2));
          __m256d grad_vel_zy = _mm256_load_pd(grad_vel_zy_ptr+(i<<2));
          __m256d tau_yz = _mm256_mul_pd(avx_visc[i],_mm256_add_pd(grad_vel_yz,grad_vel_zy));
          _mm256_stream_pd(tau_yz_ptr+(i<<2), tau_yz);
        }
      } else {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          avx_visc[i] = _mm256_loadu_pd(viscosity_ptr+(i<<2));
          __m256d grad_vel_xx = _mm256_loadu_pd(grad_vel_xx_ptr+(i<<2));
          __m256d grad_vel_yy = _mm256_loadu_pd(grad_vel_yy_ptr+(i<<2));
          __m256d grad_vel_zz = _mm256_loadu_pd(grad_vel_zz_ptr+(i<<2));
          __m256d sum_term = _mm256_add_pd(grad_vel_xx,_mm256_add_pd(grad_vel_yy,grad_vel_zz));
          sum_term = _mm256_mul_pd(sum_term, _mm256_set1_pd(1.0/3.0));
          __m256d tau_xx = _mm256_mul_pd(_mm256_set1_pd(2.0),_mm256_mul_pd(avx_visc[i],_mm256_sub_pd(grad_vel_xx,sum_term)));
          _mm256_storeu_pd(tau_xx_ptr+(i<<2), tau_xx);
          __m256d tau_yy = _mm256_mul_pd(_mm256_set1_pd(2.0),_mm256_mul_pd(avx_visc[i],_mm256_sub_pd(grad_vel_yy,sum_term)));
          _mm256_storeu_pd(tau_yy_ptr+(i<<2), tau_yy);
          __m256d tau_zz = _mm256_mul_pd(_mm256_set1_pd(2.0),_mm256_mul_pd(avx_visc[i],_mm256_sub_pd(grad_vel_zz,sum_term)));
          _mm256_storeu_pd(tau_zz_ptr+(i<<2), tau_zz);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_vel_xy = _mm256_loadu_pd(grad_vel_xy_ptr+(i<<2));
          __m256d grad_vel_yx = _mm256_loadu_pd(grad_vel_yx_ptr+(i<<2));
          __m256d tau_xy = _mm256_mul_pd(avx_visc[i],_mm256_add_pd(grad_vel_xy,grad_vel_yx));
          _mm256_storeu_pd(tau_xy_ptr+(i<<2), tau_xy);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_vel_xz = _mm256_loadu_pd(grad_vel_xz_ptr+(i<<2));
          __m256d grad_vel_zx = _mm256_loadu_pd(grad_vel_zx_ptr+(i<<2));
          __m256d tau_xz = _mm256_mul_pd(avx_visc[i],_mm256_add_pd(grad_vel_xz,grad_vel_zx));
          _mm256_storeu_pd(tau_xz_ptr+(i<<2), tau_xz);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d grad_vel_yz = _mm256_loadu_pd(grad_vel_yz_ptr+(i<<2));
          __m256d grad_vel_zy = _mm256_loadu_pd(grad_vel_zy_ptr+(i<<2));
          __m256d tau_yz = _mm256_mul_pd(avx_visc[i],_mm256_add_pd(grad_vel_yz,grad_vel_zy));
          _mm256_storeu_pd(tau_yz_ptr+(i<<2), tau_yz);
        }
      }
#elif defined(USE_SSE_KERNELS)
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        sse_visc[i] = _mm_stream_load_pd(viscosity_ptr+(i<<1));
        __m128d grad_vel_xx = _mm_stream_load_pd(grad_vel_xx_ptr+(i<<1));
        __m128d grad_vel_yy = _mm_stream_load_pd(grad_vel_yy_ptr+(i<<1));
        __m128d grad_vel_zz = _mm_stream_load_pd(grad_vel_zz_ptr+(i<<1));
        __m128d sum_term = _mm_add_pd(grad_vel_xx,_mm_add_pd(grad_vel_yy,grad_vel_zz));
        sum_term = _mm_mul_pd(sum_term,_mm_set1_pd(1.0/3.0));
        __m128d tau_xx = _mm_mul_pd(_mm_set1_pd(2.0),_mm_mul_pd(sse_visc[i],_mm_sub_pd(grad_vel_xx,sum_term)));
        _mm_stream_pd(tau_xx_ptr+(i<<1),tau_xx);
        __m128d tau_yy = _mm_mul_pd(_mm_set1_pd(2.0),_mm_mul_pd(sse_visc[i],_mm_sub_pd(grad_vel_yy,sum_term)));
        _mm_stream_pd(tau_yy_ptr+(i<<1),tau_yy);
        __m128d tau_zz = _mm_mul_pd(_mm_set1_pd(2.0),_mm_mul_pd(sse_visc[i],_mm_sub_pd(grad_vel_zz,sum_term)));
        _mm_stream_pd(tau_zz_ptr+(i<<1),tau_zz);
      }
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d grad_vel_xy = _mm_stream_load_pd(grad_vel_xy_ptr+(i<<1));
        __m128d grad_vel_yx = _mm_stream_load_pd(grad_vel_yx_ptr+(i<<1));
        __m128d tau_xy = _mm_mul_pd(sse_visc[i], _mm_add_pd(grad_vel_xy, grad_vel_yx));
        _mm_stream_pd(tau_xy_ptr+(i<<1), tau_xy);
      }
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d grad_vel_xz = _mm_stream_load_pd(grad_vel_xz_ptr+(i<<1));
        __m128d grad_vel_zx = _mm_stream_load_pd(grad_vel_zx_ptr+(i<<1));
        __m128d tau_xz = _mm_mul_pd(sse_visc[i], _mm_add_pd(grad_vel_xz, grad_vel_zx));
        _mm_stream_pd(tau_xz_ptr+(i<<1), tau_xz);
      }
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d grad_vel_yz = _mm_stream_load_pd(grad_vel_yz_ptr+(i<<1));
        __m128d grad_vel_zy = _mm_stream_load_pd(grad_vel_zy_ptr+(i<<1));
        __m128d tau_yz = _mm_mul_pd(sse_visc[i], _mm_add_pd(grad_vel_yz, grad_vel_zy));
        _mm_stream_pd(tau_yz_ptr+(i<<1), tau_yz);
      }
#else
      for (int i = 0; i < FULL_STRIP_SIZE; i++)
        visc[i] = viscosity_ptr[i];
      for (size_t i = 0; i < n_pts; i++) {
        double sum_term = (grad_vel_xx_ptr[i] + grad_vel_yy_ptr[i] + grad_vel_zz_ptr[i]) * (1.0/3.0);
        tau_xx_ptr[i] = 2.0 * visc[i] * (grad_vel_xx_ptr[i] - sum_term);
        tau_yy_ptr[i] = 2.0 * visc[i] * (grad_vel_yy_ptr[i] - sum_term);
        tau_zz_ptr[i] = 2.0 * visc[i] * (grad_vel_zz_ptr[i] - sum_term);
      }
      for (int i = 0; i < FULL_STRIP_SIZE; i++) {
        tau_xy_ptr[i] = visc[i] * (grad_vel_xy_ptr[i] + grad_vel_yx_ptr[i]);
      }
      for (int i = 0; i < FULL_STRIP_SIZE; i++) {
        tau_xz_ptr[i] = visc[i] * (grad_vel_xz_ptr[i] + grad_vel_zx_ptr[i]);
      }
      for (int i = 0; i < FULL_STRIP_SIZE; i++) {
        tau_yz_ptr[i] = visc[i] * (grad_vel_yz_ptr[i] + grad_vel_zy_ptr[i]);
      }
#endif
      n_pts -= FULL_STRIP_SIZE;
    } else {
      for (size_t i = 0; i < n_pts; i++)
        visc[i] = viscosity_ptr[i];
      for (size_t i = 0; i < n_pts; i++) {
        double sum_term = (grad_vel_xx_ptr[i] + grad_vel_yy_ptr[i] + grad_vel_zz_ptr[i]) * (1.0/3.0);
        tau_xx_ptr[i] = 2.0 * visc[i] * (grad_vel_xx_ptr[i] - sum_term);
        tau_yy_ptr[i] = 2.0 * visc[i] * (grad_vel_yy_ptr[i] - sum_term);
        tau_zz_ptr[i] = 2.0 * visc[i] * (grad_vel_zz_ptr[i] - sum_term);
      }
      for (size_t i = 0; i < n_pts; i++) {
        tau_xy_ptr[i] = visc[i] * (grad_vel_xy_ptr[i] + grad_vel_yx_ptr[i]);
      }
      for (size_t i = 0; i < n_pts; i++) {
        tau_xz_ptr[i] = visc[i] * (grad_vel_xz_ptr[i] + grad_vel_zx_ptr[i]);
      }
      for (size_t i = 0; i < n_pts; i++) {
        tau_yz_ptr[i] = visc[i] * (grad_vel_yz_ptr[i] + grad_vel_zy_ptr[i]);
      }
      n_pts = 0;
    }
    tau_xx_ptr += FULL_STRIP_SIZE;
    tau_xy_ptr += FULL_STRIP_SIZE;
    tau_xz_ptr += FULL_STRIP_SIZE;
    tau_yy_ptr += FULL_STRIP_SIZE;
    tau_yz_ptr += FULL_STRIP_SIZE;
    tau_zz_ptr += FULL_STRIP_SIZE;
    viscosity_ptr += FULL_STRIP_SIZE;
    grad_vel_xx_ptr += FULL_STRIP_SIZE;
    grad_vel_xy_ptr += FULL_STRIP_SIZE;
    grad_vel_xz_ptr += FULL_STRIP_SIZE;
    grad_vel_yx_ptr += FULL_STRIP_SIZE;
    grad_vel_yy_ptr += FULL_STRIP_SIZE;
    grad_vel_yz_ptr += FULL_STRIP_SIZE;
    grad_vel_zx_ptr += FULL_STRIP_SIZE;
    grad_vel_zy_ptr += FULL_STRIP_SIZE;
    grad_vel_zz_ptr += FULL_STRIP_SIZE;
  }
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
#undef FULL_STRIP_SIZE
}

/*static*/
bool CalcTauTask::fast_calc_tau_task(RegionAccessor<AccessorType::Generic,double> fa_in_viscosity,
                          PhysicalRegion ra_in_grad_vel,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_xx,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_xy,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_xz,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_yy,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_yz,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_zz,
                          Rect<3> subgrid_bounds)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3], offsets[3];

  const double *in_viscosity_ptr = fa_in_viscosity.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_viscosity_ptr || (subrect != subgrid_bounds)) return false;

  const double *in_grad_vel_ptr = ra_in_grad_vel.get_field_accessor(FID_GRAD_VEL_X_X).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_grad_vel_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
  ByteOffset grad_vel_stride;
  for(int fid = FID_GRAD_VEL_X_X + 1; fid <= FID_GRAD_VEL_Z_Z; fid++) {
    double *in_grad_vel_i_ptr = ra_in_grad_vel.get_field_accessor(fid).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_grad_vel_i_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
    ByteOffset grad_vel_i_stride(in_grad_vel_i_ptr, in_grad_vel_ptr);
    if(fid == FID_GRAD_VEL_X_X + 1) {
      grad_vel_stride = grad_vel_i_stride;
    } else {
      if(grad_vel_i_stride != (grad_vel_stride * (fid - FID_GRAD_VEL_X_X))) {
        printf("grad_vel stride mismatch\n");
        return 0;
      }
    }
  }

  double *out_tau_xx_ptr = fa_out_tau_xx.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_tau_xx_ptr || (subrect != subgrid_bounds)) return false;

  double *out_tau_xy_ptr = fa_out_tau_xy.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_tau_xy_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *out_tau_xz_ptr = fa_out_tau_xz.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_tau_xz_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *out_tau_yy_ptr = fa_out_tau_yy.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_tau_yy_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *out_tau_yz_ptr = fa_out_tau_yz.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_tau_yz_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *out_tau_zz_ptr = fa_out_tau_zz.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_tau_zz_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  // now that we have all those pointers, we can do the actual work
  ByteOffset in_pos_z, out_pos_z;
  for(int z = subgrid_bounds.lo[2]; z <= subgrid_bounds.hi[2]; z++) {
    ByteOffset in_pos_y = in_pos_z;
    ByteOffset out_pos_y = out_pos_z;
    for(int y = subgrid_bounds.lo[1]; y <= subgrid_bounds.hi[1]; y++) {
      ByteOffset in_pos_x = in_pos_y;
      ByteOffset out_pos_x = out_pos_y;
      for(int x = subgrid_bounds.lo[0]; x <= subgrid_bounds.hi[0]; x++) {
        double viscosity = *(in_viscosity_ptr + in_pos_x);
        double grad_vel_xx = *(in_grad_vel_ptr + in_pos_x);
        double grad_vel_yy = *(in_grad_vel_ptr + in_pos_x + 4 * grad_vel_stride);
        double grad_vel_zz = *(in_grad_vel_ptr + in_pos_x + 8 * grad_vel_stride);

        double sum_term = (grad_vel_xx + grad_vel_yy + grad_vel_zz) / 3.0;

        double tau_xx = 2.0 * viscosity * (grad_vel_xx - sum_term);
        double tau_yy = 2.0 * viscosity * (grad_vel_yy - sum_term);
        double tau_zz = 2.0 * viscosity * (grad_vel_zz - sum_term);

        double grad_vel_xy = *(in_grad_vel_ptr + in_pos_x + 1 * grad_vel_stride);
        double grad_vel_yx = *(in_grad_vel_ptr + in_pos_x + 3 * grad_vel_stride);

        double tau_xy = viscosity * (grad_vel_xy + grad_vel_yx);

        double grad_vel_xz = *(in_grad_vel_ptr + in_pos_x + 2 * grad_vel_stride);
        double grad_vel_zx = *(in_grad_vel_ptr + in_pos_x + 6 * grad_vel_stride);

        double tau_xz = viscosity * (grad_vel_xz + grad_vel_zx);

        double grad_vel_yz = *(in_grad_vel_ptr + in_pos_x + 5 * grad_vel_stride);
        double grad_vel_zy = *(in_grad_vel_ptr + in_pos_x + 7 * grad_vel_stride);

        double tau_yz = viscosity * (grad_vel_yz + grad_vel_zy);

        *(out_tau_xx_ptr + out_pos_x) = tau_xx;
        *(out_tau_xy_ptr + out_pos_x) = tau_xy;
        *(out_tau_xz_ptr + out_pos_x) = tau_xz;
        *(out_tau_yy_ptr + out_pos_x) = tau_yy;
        *(out_tau_yz_ptr + out_pos_x) = tau_yz;
        *(out_tau_zz_ptr + out_pos_x) = tau_zz;

        in_pos_x += in_offsets[0];
        out_pos_x += out_offsets[0];
      }
      in_pos_y += in_offsets[1];
      out_pos_y += out_offsets[1];
    }
    in_pos_z += in_offsets[2];
    out_pos_z += out_offsets[2];
  }

  return true;
}

/*static*/
void CalcTauTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &my_subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  // get whatever accessors you need here
  RegionAccessor<AccessorType::Generic,double> fa_tau_xx = regions[0].get_field_accessor(FID_TAU_XX).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_xy = regions[0].get_field_accessor(FID_TAU_XY).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_xz = regions[0].get_field_accessor(FID_TAU_XZ).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_yy = regions[0].get_field_accessor(FID_TAU_YY).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_yz = regions[0].get_field_accessor(FID_TAU_YZ).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_zz = regions[0].get_field_accessor(FID_TAU_ZZ).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_viscosity = regions[1].get_field_accessor(FID_VISCOSITY).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_grad_vel_x_x = regions[2].get_field_accessor(FID_GRAD_VEL_X_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_vel_x_y = regions[2].get_field_accessor(FID_GRAD_VEL_X_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_vel_x_z = regions[2].get_field_accessor(FID_GRAD_VEL_X_Z).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_vel_y_x = regions[2].get_field_accessor(FID_GRAD_VEL_Y_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_vel_y_y = regions[2].get_field_accessor(FID_GRAD_VEL_Y_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_vel_y_z = regions[2].get_field_accessor(FID_GRAD_VEL_Y_Z).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_vel_z_x = regions[2].get_field_accessor(FID_GRAD_VEL_Z_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_vel_z_y = regions[2].get_field_accessor(FID_GRAD_VEL_Z_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_grad_vel_z_z = regions[2].get_field_accessor(FID_GRAD_VEL_Z_Z).typeify<double>();

  if(dense_calc_tau_task(fa_tau_xx, fa_tau_xy, fa_tau_xz, fa_tau_yy, fa_tau_yz, fa_tau_zz, fa_viscosity,
                         fa_grad_vel_x_x, fa_grad_vel_x_y, fa_grad_vel_x_z,
                         fa_grad_vel_y_x, fa_grad_vel_y_y, fa_grad_vel_y_z,
                         fa_grad_vel_z_x, fa_grad_vel_z_y, fa_grad_vel_z_z, my_subgrid_bounds)) {
    return;
  }

  if(fast_calc_tau_task(fa_viscosity, regions[2],
                        fa_tau_xx, fa_tau_xy, fa_tau_xz, fa_tau_yy, fa_tau_yz, fa_tau_zz,
                        my_subgrid_bounds)) {
    //printf("fast calc tau\n");
    return;
  }

  for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double viscosity = fa_viscosity.read(dp);
    double grad_vel[3][3];
    grad_vel[0][0] = fa_grad_vel_x_x.read(dp);
    grad_vel[0][1] = fa_grad_vel_x_y.read(dp);
    grad_vel[0][2] = fa_grad_vel_x_z.read(dp);
    grad_vel[1][0] = fa_grad_vel_y_x.read(dp);
    grad_vel[1][1] = fa_grad_vel_y_y.read(dp);
    grad_vel[1][2] = fa_grad_vel_y_z.read(dp);
    grad_vel[2][0] = fa_grad_vel_z_x.read(dp);
    grad_vel[2][1] = fa_grad_vel_z_y.read(dp);
    grad_vel[2][2] = fa_grad_vel_z_z.read(dp);

    double sum_term = (grad_vel[0][0] + grad_vel[1][1] + grad_vel[2][2]) / 3.0;

    double tau_xx = 2.0 * viscosity * (grad_vel[0][0] - sum_term);
    double tau_yy = 2.0 * viscosity * (grad_vel[1][1] - sum_term);
    double tau_zz = 2.0 * viscosity * (grad_vel[2][2] - sum_term);

    double tau_xy = viscosity * (grad_vel[0][1] + grad_vel[1][0]);
    double tau_xz = viscosity * (grad_vel[0][2] + grad_vel[2][0]);
    double tau_yz = viscosity * (grad_vel[1][2] + grad_vel[2][1]);

    fa_tau_xx.write(dp, tau_xx);
    fa_tau_xy.write(dp, tau_xy);
    fa_tau_xz.write(dp, tau_xz);
    fa_tau_yy.write(dp, tau_yy);
    fa_tau_yz.write(dp, tau_yz);
    fa_tau_zz.write(dp, tau_zz);
  }
#endif
}

