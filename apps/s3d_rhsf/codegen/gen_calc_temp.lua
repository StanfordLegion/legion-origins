#!/usr/bin/env lua

-- add the script's directory to the module search path
package.path = (arg[0]:gsub('/[^/]+$','/?.lua;')) .. package.path

require 'cklib'
require 'refvals'

method_order = 3 -- default is Halley's method
calc_pressure = true
calc_cpmix = true

local function per_spec_coeffs(ck, refs, s, t, of, pfx, cp_only)
   cp_only = cp_only or false
   -- first, fetch the raw coefficients from the table
   local idx = ((s - 1) * (ck.maxtp - 1) + (t - 1)) * (ck.nthcp + 2)
   local enth_coeffs = { ck.ncaa[idx + ck.nthcp + 1] }
   local cp_coeffs = {}
   local dcp_coeffs = {}
   for i = 1, ck.nthcp do
      enth_coeffs[i + 1] = ck.ncaa[idx + i]
      cp_coeffs[i] = ck.ncaa[idx + i]
      if i > 1 then 
	 dcp_coeffs[i - 1] = ck.ncaa[idx + i]
      end
   end

   -- now scale things - base scaling factor has a few terms, and then things depend on powers of t_ref
   local base_scale = refs.univ_gascon / (0.001 * ck.ncwt[s]) / refs.a_ref / refs.a_ref
   for i = 1, ck.nthcp + 1 do
      enth_coeffs[i] = enth_coeffs[i] * base_scale
      if i > 1 then
	 enth_coeffs[i] = enth_coeffs[i] * refs.t_ref^(i - 1) / (i - 1)
      end
   end
   for i = 1, ck.nthcp do
      cp_coeffs[i] = cp_coeffs[i] * base_scale * refs.t_ref^i
   end
   -- dcp gets an additional 1/2 factor incorporated
   for i = 1, ck.nthcp-1 do
      dcp_coeffs[i] = dcp_coeffs[i] * 0.5 * base_scale * refs.t_ref^(i+1) * i
   end

   -- now incoporate the nRT terms into enth, cp
   if not cp_only then
      local nrt_term = refs.univ_gascon / refs.cp_ref / (0.001 * ck.ncwt[s])
      enth_coeffs[2] = enth_coeffs[2] - nrt_term
      cp_coeffs[1] = cp_coeffs[1] - nrt_term
   end

   if not cp_only then
      of:write(string.format("%senth = (%a", pfx, enth_coeffs[1]))
      for i = 2, #enth_coeffs do
         of:write(string.format(" + (%a * temp%d)", enth_coeffs[i], i - 1))
      end
      of:write ");\n"
   end
   of:write(string.format("%scp = (%a", pfx, cp_coeffs[1]))
   for i = 2, #cp_coeffs do
      of:write(string.format(" + (%a * temp%d)", cp_coeffs[i], i - 1))
   end
   of:write ");\n"
   if not cp_only then
      of:write(string.format("%sdcp = (%a", pfx, dcp_coeffs[1]))
      for i = 2, #dcp_coeffs do
         of:write(string.format(" + (%a * temp%d)", dcp_coeffs[i], i - 1))
      end
      of:write ");\n"
   end
end

local function safename(s)
   return s:gsub('-', '__minus__'):gsub('+','__plus__')
end

local function write_inner_loop(of, idx_expr, gpu)
   if gpu then
      of:write(string.format("    double rho_vx = stream_load(in_rho_vx+%s);\n", idx_expr))
      of:write(string.format("    double rho_vy = stream_load(in_rho_vy+%s);\n", idx_expr))
      of:write(string.format("    double rho_vz = stream_load(in_rho_vz+%s);\n", idx_expr))
      of:write(string.format("    double rho = stream_load(in_rho+%s);\n", idx_expr))
      of:write(string.format("    double rho_e = stream_load(in_rho_e+%s);\n", idx_expr))
   else
      of:write(string.format("    double rho_vx = in_rho_vx[%s];\n", idx_expr))
      of:write(string.format("    double rho_vy = in_rho_vy[%s];\n", idx_expr))
      of:write(string.format("    double rho_vz = in_rho_vz[%s];\n", idx_expr))
      of:write(string.format("    double rho = in_rho[%s];\n", idx_expr))
      of:write(string.format("    double rho_e = in_rho_e[%s];\n", idx_expr))
   end
   of:write "    double c0 = 0.5 * (rho_vx * rho_vx + rho_vy * rho_vy + rho_vz * rho_vz) - rho * rho_e;\n"
   -- don't stream load this read for the gpu
   of:write(string.format("    double temp1 = inout_temp[%s];\n", idx_expr))
   of:write "    double nr_over_v = 0.0;\n";
   of:write "\n"
   of:write "    for(int iter = 1; iter < 4; iter++) {\n"
   of:write "      double err = c0;\n";
   of:write "      double derr = 0.0;\n";
   of:write "      double dderr = 0.0;\n";

   for p = 2, ck.nthcp do
      of:write(string.format("      double temp%d = temp1 * temp%d;\n", p, p - 1))
   end

   -- iterate over species
   local rysl = string.format("rho_yspec_%s", safename(ck.ickk[ck.kk]))
   of:write(string.format("      double %s = rho;\n", rysl))
   of:write(string.format("      const double *rho_yspec_ptr = in_rho_yspec + (%s);\n", idx_expr))
   of:write "      double enth, cp, dcp;\n"
   of:write "\n"
   of:write "      nr_over_v = 0.0;\n"

   for s = 1, ck.kk do
      local rys
      if s < ck.kk then
	 rys = string.format("rho_yspec_%s", safename(ck.ickk[s]))
         if gpu then
            of:write(string.format("      double %s = stream_load(rho_yspec_ptr);\n", rys))
         else
	    of:write(string.format("      double %s = *rho_yspec_ptr;\n", rys))
         end
	 of:write "      rho_yspec_ptr += spec_stride;\n"
      else
	 rys = rysl
      end

      local nrt_term = refs.univ_gascon / refs.cp_ref / (0.001 * ck.ncwt[s])
      of:write(string.format("      nr_over_v += %s * %a;\n", rys, nrt_term))

      for t = 1, ck.maxtp - 2 do
	 of:write(string.format("      if(temp1 < %a) {\n", ck.nctt[(s - 1) * ck.maxtp + t + 1] / refs.t_ref))
	 per_spec_coeffs(ck, refs, s, t, of, "        ")
	 of:write "      } else\n"
      end

      of:write "      {\n"
      per_spec_coeffs(ck, refs, s, ck.maxtp - 1, of, "        ")
      of:write "      }\n"

      if s < ck.kk then
	 of:write(string.format("      %s -= %s;\n", rysl, rys))
      end
      of:write(string.format("      %s *= rho;\n", rys))

      of:write(string.format("      err += %s * enth;\n", rys))
      of:write(string.format("      derr += %s * cp;\n", rys))
      of:write(string.format("      dderr += %s * dcp;\n", rys))
   end

   -- dderr includes 1/2 factor already
   of:write "      double delta_t = (err * derr) / (derr * derr - err * dderr);\n"
   --of:write '      printf("iter: %d %g %.20g %g %g %g\\n", iter, temp1, delta_t, err, derr, dderr);\n'
   of:write "      temp1 -= delta_t;\n";
   of:write "      if(fabs(err) < 1e-10) break;\n"
   of:write "    }\n"
   of:write(string.format("    inout_temp[%s] = temp1;\n", idx_expr))
   if gpu then
      of:write(string.format("    stream_store(out_pressure+%s, temp1 * nr_over_v);\n", idx_expr))
   else
      of:write(string.format("    out_pressure[%s] = temp1 * nr_over_v;\n", idx_expr))
   end
   if calc_cpmix then
      -- a subset of the calculation above generates rho*cpmix, but we
      --  have to redo it as the temperature has changed once more
      of:write("    if(out_cpmix) {\n")
      for p = 2, ck.nthcp-1 do  -- cp doesn't use last power term
         of:write(string.format("      double temp%d = temp1 * temp%d;\n", p, p - 1))
      end
      of:write("      double rho_cpmix = 0;\n")

      -- iterate over species
      local rysl = string.format("rho_yspec_%s", safename(ck.ickk[ck.kk]))
      of:write(string.format("      double %s = rho;\n", rysl))
      of:write(string.format("      const double *rho_yspec_ptr = in_rho_yspec + (%s);\n", idx_expr))
      of:write "      double cp;\n"

      for s = 1, ck.kk do
         local rys
         if s < ck.kk then
	    rys = string.format("rho_yspec_%s", safename(ck.ickk[s]))
            if gpu then
               of:write(string.format("      double %s = stream_load(rho_yspec_ptr);\n", rys))
            else
	       of:write(string.format("      double %s = *rho_yspec_ptr;\n", rys))
            end
	    of:write "      rho_yspec_ptr += spec_stride;\n"
         else
   	    rys = rysl
         end

         for t = 1, ck.maxtp - 2 do
  	    of:write(string.format("      if(temp1 < %a) {\n", ck.nctt[(s - 1) * ck.maxtp + t + 1] / refs.t_ref))
	    per_spec_coeffs(ck, refs, s, t, of, "        ", true)
	    of:write "      } else\n"
         end

         of:write "      {\n"
         per_spec_coeffs(ck, refs, s, ck.maxtp - 1, of, "        ", true)
         of:write "      }\n"

         if s < ck.kk then
	    of:write(string.format("      %s -= %s;\n", rysl, rys))
         end

         of:write(string.format("      rho_cpmix += %s * cp;\n", rys))
      end

      if gpu then
         of:write(string.format("      stream_store(out_cpmix+%s, rho_cpmix / rho);\n", idx_expr))
      else
         of:write(string.format("      out_cpmix[%s] = rho_cpmix / rho;\n", idx_expr))
      end
      of:write("    }\n")
   end
end

local function write_c_output(filename)
   local of = assert(io.open(filename, "w"))

   of:write "#include <stdio.h>\n"
   of:write "#include <sys/types.h>\n"
   of:write "#include <math.h>\n"
   of:write "\n"
   of:write "void calc_temps(const double *in_rho_vx, const double *in_rho_vy, const double *in_rho_vz,\n"
   of:write "                const double *in_rho, const double *in_rho_e, const double *in_rho_yspec,\n"
   of:write "                double *inout_temp,\n"
   if calc_pressure then
      of:write "                double *out_pressure,\n"
   end
   if calc_cpmix then
      of:write "                double *out_cpmix,\n"
   end
   of:write "                size_t count, size_t elem_stride, size_t spec_stride)\n"
   of:write "{\n"
   of:write "  for(size_t i = 0; i < count; i++) {\n"
   write_inner_loop(of, "elem_stride * i", false)
   of:write "  }\n"
   of:write "}\n"
end

local function write_cuda_output(filename)
   local of = assert(io.open(filename, "w"))

   of:write "#include <cuda.h>\n"
   of:write "#include <cuda_runtime.h>\n"
   of:write "#include \"gpu_help.h\"\n"
   of:write "\n"
   of:write "#//#define DEBUG_CUDA\n"
   of:write "\n"
   of:write "#ifdef DEBUG_CUDA\n"
   of:write "#define CUDA_SAFE_CALL(expr) \\\n"
   of:write "  { \\\n"
   of:write "    cudaError_t err = (expr); \\\n"
   of:write "    if (err == cudaSuccess) \\\n"
   of:write "    { \\\n"
   of:write "      printf(\"Cuda error: %s\\n\", cudaGetErrorString(err)); \\\n"
   of:write "      assert(false); \\\n"
   of:write "    } \\\n"
   of:write "  }\n"
   of:write "#else\n";
   of:write "#define CUDA_SAFE_CALL(expr) expr \n"
   of:write "#endif\n";
   of:write "\n"

   of:write "__global__\n"
   of:write "void calc_temps_kernel(const double *in_rho_vx, const double *in_rho_vy, const double *in_rho_vz,\n"
   of:write "                const double *in_rho, const double *in_rho_e, const double *in_rho_yspec,\n"
   of:write "                double *inout_temp,\n"
   if calc_pressure then
      of:write "                double *out_pressure,\n"
   end
   if calc_cpmix then
      of:write "                double *out_cpmix,\n"
   end
   of:write "                size_t count_x, size_t count_y, size_t count_z,\n"
   of:write "                size_t stride_x, size_t stride_y, size_t stride_z, size_t spec_stride)\n"
   of:write "{\n"
   of:write "  int x = (blockIdx.x * blockDim.x) + threadIdx.x;\n"
   of:write "  int y = (blockIdx.y * blockDim.y) + threadIdx.y;\n"
   of:write "  int z = (blockIdx.z * blockDim.z) + threadIdx.z;\n"
   of:write "  if((x >= count_x) || (y >= count_y)) return;\n"
   of:write "  size_t elem_offset = ((x * stride_x) + (y * stride_y) + (z * stride_z));\n"
   of:write "  while(z < count_z) {\n"
   write_inner_loop(of, "elem_offset", true)
   of:write "\n"
   of:write "    z += gridDim.z;\n"
   of:write "    elem_offset += gridDim.z * blockDim.z * stride_z;\n"
   of:write "  }\n"
   of:write "}\n"
   of:write "\n"

   of:write "static size_t div_roundup(size_t num, size_t den)\n"
   of:write "{\n"
   of:write "  return ((num + den - 1) / den);\n";
   of:write "}\n"
   of:write "\n"

   of:write "void calc_temps_gpu(const double *in_rho_vx, const double *in_rho_vy, const double *in_rho_vz,\n"
   of:write "                    const double *in_rho, const double *in_rho_e, const double *in_rho_yspec,\n"
   of:write "                    double *inout_temp,\n"
   if calc_pressure then
      of:write "                    double *out_pressure,\n"
   end
   if calc_cpmix then
      of:write "                double *out_cpmix,\n"
   end
   of:write "                    size_t count_x, size_t count_y, size_t count_z,\n"
   of:write "                    size_t stride_x, size_t stride_y, size_t stride_z, size_t spec_stride)\n"
   of:write "{\n"
   --of:write "  cudaStream_t exec_stream;\n"
   --of:write "  CUDA_SAFE_CALL(cudaStreamCreate(&exec_stream));\n"
   of:write "  dim3 block_size(32, 4, 1); // somewhat arbitrary\n"
   of:write "  dim3 grid_size(div_roundup(count_x, block_size.x),\n"
   of:write "                 div_roundup(count_y, block_size.y),\n"
   of:write "                 8);\n"
   --of:write "  calc_temps_kernel<<<grid_size, block_size, 0, exec_stream>>>(in_rho_vx, in_rho_vy, in_rho_vz,\n"
   of:write "  calc_temps_kernel<<<grid_size, block_size>>>(in_rho_vx, in_rho_vy, in_rho_vz,\n"
   of:write "                                                               in_rho, in_rho_e, in_rho_yspec,\n"
   of:write "                                                               inout_temp,\n"
   if calc_pressure then
      of:write "                                                               out_pressure,\n"
   end
   if calc_cpmix then
      of:write "                                                               out_cpmix,\n"
   end
   of:write "                                                               count_x, count_y, count_z,\n"
   of:write "                                                               stride_x, stride_y, stride_z, spec_stride);\n"
   --of:write "  CUDA_SAFE_CALL(cudaStreamSynchronize(exec_stream));\n"
   --of:write "  CUDA_SAFE_CALL(cudaStreamDestroy(exec_stream));\n"
   of:write "}\n"
end

in_chem = nil
out_c = nil
out_cuda = nul

i = 1
while i <= #arg do
   if arg[i] == "-i" then
      in_chem = arg[i+1]
      i = i + 2
   elseif arg[i] == "-m" then
      method_order = tonumber(arg[i+1])
      i = i + 2
   elseif arg[i] == "-c" then
      out_c = arg[i+1]
      i = i + 2
   elseif arg[i] == "-cuda" then
      out_cuda = arg[i+1]
      i = i + 2
   else 
      print("bad argument", arg[i])
      i = i + 1
   end
end

if in_chem == nil then
   error("must specify input chemistry file with -i!")
end

ck = CkLib:read_chem_asc(in_chem)
refs = RefVals:new()

if out_c ~= nil then
   write_c_output(out_c)
end

if out_cuda ~= nil then
   write_cuda_output(out_cuda)
end
