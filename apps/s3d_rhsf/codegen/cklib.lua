-- CkLib - parse CHEMKIN input file formats

CkLib = {}

CkLib.__index = CkLib

local function read_chem_asc_string(f)
   local l = f:read('*line')
   return l:match "^%s*(.-)%s*$"
end

local function read_chem_asc_number(f)
   local l = f:read('*line')
   return tonumber(l)
end

local function read_chem_asc_strings(f, width, count)
   local l = ""
   repeat
      local l2 = f:read("*line")
      l2 = l2:gsub("[\r\n]", "")
      l = l .. l2
   until l:len() >= (width * count)
   local a = {}
   for i = 1, count do
      a[i] = assert(l:sub((i - 1) * width + 1, i * width):match "^%s*(.-)%s*$")
   end
   return a
end

local function read_chem_asc_numbers(f, width, count)
   local l = ""
   repeat
      local l2 = f:read("*line")
      l2 = l2:gsub("[\r\n]", "")
      l = l .. l2
   until l:len() >= (width * count)
   local a = {}
   for i = 1, count do
      a[i] = assert(tonumber(l:sub((i - 1) * width + 1, i * width)))
   end
   return a
end

function CkLib:read_chem_asc(filename)
   local f = assert(io.open(filename, "r"))

   o = {}
   o.filver = read_chem_asc_string(f)
   o.prvers = read_chem_asc_string(f)
   o.prec = read_chem_asc_string(f)
   o.kerr = read_chem_asc_string(f)
   o.leni, o.lenr, o.lenc = unpack(read_chem_asc_numbers(f, 12, 3))
   o.maxsp, o.maxtb, o.maxtp, o.nthcp, _ = unpack(read_chem_asc_numbers(f, 12, 10))
   o.mm, o.kk, o.ii, _ = unpack(read_chem_asc_numbers(f, 12, 21))
   o.ckmn = read_chem_asc_number(f)

   -- atom names and molwts
   o.icmm = read_chem_asc_strings(f, 16, o.mm)
   o.ncaw = read_chem_asc_numbers(f, 24, o.mm)

   -- molecule names and molwts
   o.ickk = read_chem_asc_strings(f, 16, o.kk)
   o.ncwt = read_chem_asc_numbers(f, 24, o.kk)

   -- atoms in each molecule
   o.icnc = read_chem_asc_numbers(f, 12, o.kk * o.mm)

   -- ??
   o.icch = read_chem_asc_numbers(f, 12, o.kk)
   o.icnt = read_chem_asc_numbers(f, 12, o.kk)
   o.icph = read_chem_asc_numbers(f, 12, o.kk)

   -- temperature ranges for curve fit (per species)
   o.nctt = read_chem_asc_numbers(f, 24, o.kk * o.maxtp)

   -- arrhenius coefficients (for specific heat, entropy)
   o.ncaa = read_chem_asc_numbers(f, 24, o.kk * (o.maxtp - 1) * (o.nthcp + 2))

   -- bunch more stuff that we don't care about yet

   -- bless and return
   setmetatable(o, self)
   return o
end

return CkLib
