-- RefVals - reference units from S3D

RefVals = {}

RefVals.__index = RefVals

function RefVals:new()
   o = {}

   o.mach_no = 0.001
   o.re_real = 100.0
   o.pr = 0.708

   o.g_ref = 1.4
   o.a_ref = 347.2   -- m/s
   o.t_o   = 300.0   -- K
   o.rho_ref = 1.1766  -- Kg/m3
   o.lambda_ref = 26.14e-3  -- W/m-s

   o.re = o.re_real / o.mach_no
   o.univ_gascon = 8.31451                   --J/mol-K
   o.t_ref = o.t_o * ( o.g_ref - 1 )             --K
   o.cp_ref = (o.a_ref ^ 2) / o.t_ref             --J/kg-K
   o.l_ref = o.pr * o.lambda_ref / o.cp_ref * o.re / o.rho_ref / o.a_ref   --m
   o.mu_ref = o.rho_ref * o.l_ref * o.a_ref       --kg/m-s

   o.p_ref = (o.a_ref ^ 2) * o.rho_ref              --Pa
   o.time_ref = o.l_ref / o.a_ref                    --sec

   o.rr_ref = o.rho_ref / o.time_ref  --kg m^-3 s^-1 
   o.hr_ref = o.cp_ref * o.t_ref * o.rr_ref -- J m^-3 s^-1
   o.pres_atm = 1.01325e5 -- in Pa. 

   -- bless and return
   setmetatable(o, self)
   return o
end

return RefVals
