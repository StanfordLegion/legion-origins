
#include "rhsf.h"
#include "calc_scalar.h"
#include "check_field.h"
#include "rank_helper.h"
#include "legion.h"
#include "arrays.h"
#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
#include <x86intrin.h>
#endif
#include <cmath>

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

CalcScalarFluxTask::CalcScalarFluxTask(S3DRank *r,
				       Domain domain,
				       TaskArgument global_arg,
				       ArgumentMap arg_map,
				       Predicate pred,
				       bool must,
				       MapperID id,
				       MappingTagID tag,
				       bool add_requirements)
  : IndexLauncher(CalcScalarFluxTask::TASK_ID, domain, global_arg, 
                  arg_map, pred, must, id, tag | CalcScalarFluxTask::MAPPER_TAG), rank(r)
{
  if (add_requirements) {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    RegionRequirement rr_in(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);

    for(int s = 0; s < rank->n_scalar; s++)
      for(int d = 0; d < 3; d++) {
	rr_out.add_field(FID_SCALAR_FLUX(s, d));
	rr_in.add_field(FID_GRAD_SCALAR(s, d));
      }

    add_region_requirement(rr_out);
    add_region_requirement(rr_in);

    // also need lambda (from state) and cpmix and volume (from int)
    add_region_requirement(RegionRequirement(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state)
			   .add_field(FID_LAMBDA));

    add_region_requirement(RegionRequirement(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int)
			   .add_field(FID_VOLUME)
			   .add_field(FID_CPMIX));
  }
}

void CalcScalarFluxTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // no checks for scalar flux right now
}

/*static*/ const char * const CalcScalarFluxTask::TASK_NAME = "calc_scalar_flux";

template <int DIM, typename PTRTYPE, typename ACCTYPE>
static bool raw_rect_helper(PTRTYPE &ptr, ACCTYPE& acc, const Rect<DIM> &bounds, ByteOffset *offsets, bool first)
{
  Rect<DIM> subrect;
  if(first) {
    ptr = acc.template raw_rect_ptr<DIM>(bounds, subrect, offsets);
  } else {
    ByteOffset offsets2[DIM];
    ptr = acc.template raw_rect_ptr<DIM>(bounds, subrect, offsets2);
    if(offset_mismatch(DIM, offsets, offsets2)) return false;
  }
  return(ptr && (subrect == bounds));
}						 

/*static*/
bool CalcScalarFluxTask::fast_calc_scalar_flux_task(const Rect<3> &subgrid_bounds,
						    int n_scalar,
						    RegionAccessor<AccessorType::Generic,double> fa_in_lambda,
						    RegionAccessor<AccessorType::Generic,double> fa_in_volume,
						    RegionAccessor<AccessorType::Generic,double> fa_in_cpmix,
						    RegionAccessor<AccessorType::Generic,double> *fa_in_grad_scalars,
						    RegionAccessor<AccessorType::Generic,double> *fa_out_fluxes)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3];

  const double *in_lambda_ptr, *in_volume_ptr, *in_cpmix_ptr;
  if(!raw_rect_helper<3>(in_lambda_ptr, fa_in_lambda, subgrid_bounds, in_offsets, true)) return false;
  if(!raw_rect_helper<3>(in_volume_ptr, fa_in_volume, subgrid_bounds, in_offsets, false)) return false;
  if(!raw_rect_helper<3>(in_cpmix_ptr,  fa_in_cpmix,  subgrid_bounds, in_offsets, false)) return false;

  const double **in_grad_scalar_ptrs = (const double **)alloca(n_scalar * 3 * sizeof(double *));
  for(int i = 0; i < n_scalar * 3; i++)
    if(!raw_rect_helper<3>(in_grad_scalar_ptrs[i], fa_in_grad_scalars[i], subgrid_bounds, in_offsets, false)) return false;

  double **out_flux_ptrs = (double **)alloca(n_scalar * 3 * sizeof(double *));
  for(int i = 0; i < n_scalar * 3; i++) {
    if(!raw_rect_helper<3>(out_flux_ptrs[i], fa_out_fluxes[i], subgrid_bounds, out_offsets, (i==0))) return false;
  }

  // now that we have all those pointers, we can do the actual work
  ByteOffset in_pos_z, out_pos_z;
  for(int z = subgrid_bounds.lo[2]; z <= subgrid_bounds.hi[2]; z++) {
    ByteOffset in_pos_y = in_pos_z;
    ByteOffset out_pos_y = out_pos_z;
    for(int y = subgrid_bounds.lo[1]; y <= subgrid_bounds.hi[1]; y++) {
      ByteOffset in_pos_x = in_pos_y;
      ByteOffset out_pos_x = out_pos_y;
      for(int x = subgrid_bounds.lo[0]; x <= subgrid_bounds.hi[0]; x++) {
	double lambda = *(in_lambda_ptr + in_pos_x);
	double volume = *(in_volume_ptr + in_pos_x);
	double cpmix = *(in_cpmix_ptr + in_pos_x);

	// combine the above into the base scaling factor - note the negation compared to the Fortran
	// (we're calculating the true flux instead of its additive inverse here)
	double base_factor = -lambda * volume / cpmix;
#ifdef DEBUG_SCALFLUX
	printf("(%d,%d,%d) factor = -%.12g * %.12g / %.12g = %.12g\n", 
	       x, y, z, lambda, volume, cpmix, base_factor);
#endif

	for(int s = 0; s < n_scalar; s++) {
	  double factor = base_factor;
	  if(s == 1)
	    factor *= (1.0 / 1.61); // hardcoded, just like Fortran version :(

	  // now each of the three directions is just a simple read/scale/write
	  for(int d = 0; d < 3; d++) {
	    *(out_flux_ptrs[s*3+d] + out_pos_x) = factor * *(in_grad_scalar_ptrs[s*3+d] + in_pos_x);
#ifdef DEBUG_SCALFLUX
	    printf("(%d,%d,%d) dir=%d, (%p) %g = %g * %g\n", x, y, z, d, 
		   out_flux_ptrs[s*3+d] + out_pos_x, *(out_flux_ptrs[s*3+d] + out_pos_x), factor, *(in_grad_scalar_ptrs[s*3+d] + in_pos_x));
#endif
	  }
	}

        in_pos_x += in_offsets[0];
        out_pos_x += out_offsets[0];
      }
      in_pos_y += in_offsets[1];
      out_pos_y += out_offsets[1];
    }
    in_pos_z += in_offsets[2];
    out_pos_z += out_offsets[2];
  }

  return true;
}

/*static*/
void CalcScalarFluxTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE

  int num_inouts = reqs[0].privilege_fields.size();
  int n_scalar = num_inouts / 3;

  std::vector<RegionAccessor<AccessorType::Generic, double> > fa_out_fluxes(num_inouts);
  for(int s = 0; s < n_scalar; s++)
    for(int d = 0; d < 3; d++)
      fa_out_fluxes[s*3+d] = regions[0].get_field_accessor(FID_SCALAR_FLUX(s, d)).typeify<double>();

  std::vector<RegionAccessor<AccessorType::Generic, double> > fa_in_grad_scalars(num_inouts);
  for(int s = 0; s < n_scalar; s++)
    for(int d = 0; d < 3; d++)
      fa_in_grad_scalars[s*3+d] = regions[1].get_field_accessor(FID_GRAD_SCALAR(s, d)).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_in_lambda = regions[2].get_field_accessor(FID_LAMBDA).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_volume = regions[3].get_field_accessor(FID_VOLUME).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_cpmix = regions[3].get_field_accessor(FID_CPMIX).typeify<double>();

  // See if we hit the fast path
  if(fast_calc_scalar_flux_task(subgrid_bounds, n_scalar,
				fa_in_lambda, fa_in_volume, fa_in_cpmix,
				&fa_in_grad_scalars[0], &fa_out_fluxes[0]))
    return;

  // no slow path yet
  assert(0);
#endif
}

#ifdef USE_GPU_KERNELS
extern void gpu_scalar_flux(const int max_elements,
			    const double *lambda, const double *volume, const double *cpmix,
			    const double *grad_scal_0x, const double *grad_scal_0y, const double *grad_scal_0z,
			    const double *grad_scal_1x, const double *grad_scal_1y, const double *grad_scal_1z,
			    double *scal_flux_0x, double *scal_flux_0y, double *scal_flux_0z,
			    double *scal_flux_1x, double *scal_flux_1y, double *scal_flux_1z);
#endif

/*static*/
void CalcScalarFluxTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
				       const std::vector<RegionRequirement> &reqs,
				       const std::vector<PhysicalRegion> &regions,
				       Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  int num_inouts = reqs[0].privilege_fields.size();
  int n_scalar = num_inouts / 3;

  std::vector<RegionAccessor<AccessorType::Generic, double> > fa_out_fluxes(num_inouts);
  for(int s = 0; s < n_scalar; s++)
    for(int d = 0; d < 3; d++)
      fa_out_fluxes[s*3+d] = regions[0].get_field_accessor(FID_SCALAR_FLUX(s, d)).typeify<double>();

  std::vector<RegionAccessor<AccessorType::Generic, double> > fa_in_grad_scalars(num_inouts);
  for(int s = 0; s < n_scalar; s++)
    for(int d = 0; d < 3; d++)
      fa_in_grad_scalars[s*3+d] = regions[1].get_field_accessor(FID_GRAD_SCALAR(s, d)).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_in_lambda = regions[2].get_field_accessor(FID_LAMBDA).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_volume = regions[3].get_field_accessor(FID_VOLUME).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_cpmix = regions[3].get_field_accessor(FID_CPMIX).typeify<double>();
  Rect<3> subrect;
  ByteOffset offsets[3];

  const double *in_lambda_ptr, *in_volume_ptr, *in_cpmix_ptr;
  if(!raw_rect_helper<3>(in_lambda_ptr, fa_in_lambda, subgrid_bounds, offsets, true)) assert(0);
  if(!raw_rect_helper<3>(in_volume_ptr, fa_in_volume, subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(in_cpmix_ptr,  fa_in_cpmix,  subgrid_bounds, offsets, false)) assert(0);

  // gpu version is hard-coded for 2 scalar fields
  assert(n_scalar == 2);
  const double *in_grad_scal_0x, *in_grad_scal_0y, *in_grad_scal_0z;
  const double *in_grad_scal_1x, *in_grad_scal_1y, *in_grad_scal_1z;
  if(!raw_rect_helper<3>(in_grad_scal_0x, fa_in_grad_scalars[0], subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(in_grad_scal_0y, fa_in_grad_scalars[1], subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(in_grad_scal_0z, fa_in_grad_scalars[2], subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(in_grad_scal_1x, fa_in_grad_scalars[3], subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(in_grad_scal_1y, fa_in_grad_scalars[4], subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(in_grad_scal_1z, fa_in_grad_scalars[5], subgrid_bounds, offsets, false)) assert(0);

  double *out_scal_flux_0x, *out_scal_flux_0y, *out_scal_flux_0z;
  double *out_scal_flux_1x, *out_scal_flux_1y, *out_scal_flux_1z;
  if(!raw_rect_helper<3>(out_scal_flux_0x, fa_out_fluxes[0], subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(out_scal_flux_0y, fa_out_fluxes[1], subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(out_scal_flux_0z, fa_out_fluxes[2], subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(out_scal_flux_1x, fa_out_fluxes[3], subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(out_scal_flux_1y, fa_out_fluxes[4], subgrid_bounds, offsets, false)) assert(0);
  if(!raw_rect_helper<3>(out_scal_flux_1z, fa_out_fluxes[5], subgrid_bounds, offsets, false)) assert(0);

  // Now we can do the actual math
  size_t n_pts = subgrid_bounds.volume();
  
  gpu_scalar_flux(n_pts, 
		  in_lambda_ptr, in_volume_ptr, in_cpmix_ptr,
		  in_grad_scal_0x, in_grad_scal_0y, in_grad_scal_0z,
		  in_grad_scal_1x, in_grad_scal_1y, in_grad_scal_1z,
		  out_scal_flux_0x, out_scal_flux_0y, out_scal_flux_0z,
		  out_scal_flux_1x, out_scal_flux_1y, out_scal_flux_1z);
#endif
#endif
}

struct Sum6IntegrateArgs {
  double alpha, beta, err;
  int which;
};

ScalarSum6IntegrateTask::ScalarSum6IntegrateTask(Domain domain,
				     TaskArgument global_arg,
				     ArgumentMap arg_map,
				     Predicate pred,
				     bool must,
				     MapperID id,
				     MappingTagID tag)
 : IndexLauncher(ScalarSum6IntegrateTask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | ScalarSum6IntegrateTask::MAPPER_TAG)
{
}

/*static*/ const char * const ScalarSum6IntegrateTask::TASK_NAME = "sum_6_integrate_task";

void ScalarSum6IntegrateTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing
}

/*static*/
void ScalarSum6IntegrateTask::sum_6_integrate_fields(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
						     IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr_in,
						     LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
						     FieldID fid_1, FieldID fid_2, FieldID fid_3, 
						     FieldID fid_4, FieldID fid_5, FieldID fid_6,
						     FieldID fid_out,
						     double alpha, double beta, double err, 
						     Future f_timestep,
						     int which, int stage, int tag)
{
  Sum6IntegrateArgs args;
  args.alpha = alpha;
  args.beta = beta;
  args.err = err;
  args.which = which;

  ScalarSum6IntegrateTask sum_launcher(Domain::from_rect<3>(proc_grid_bounds),
				       TaskArgument(&args, sizeof(args)),
				       ArgumentMap());
  sum_launcher.tag |= tag;

  LogicalPartition lp_in_top = runtime->get_logical_partition(ctx, lr_in, ip_top);
  LogicalPartition lp_q_top = runtime->get_logical_partition(ctx, lr_q, ip_top);
  LogicalPartition lp_qerr_top = runtime->get_logical_partition(ctx, lr_qerr, ip_top);
  LogicalPartition lp_qtmp_top = runtime->get_logical_partition(ctx, lr_qtmp, ip_top);

  RegionRequirement rr_q(lp_q_top, 0, READ_WRITE, EXCLUSIVE, lr_q);
  rr_q.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_q);

  // qerr is only written on the first integation
  RegionRequirement rr_qerr(lp_qerr_top, 0, 
			    ((which == FIRST_INTEGRATION) ? WRITE_DISCARD : READ_WRITE),
			    EXCLUSIVE, lr_qerr);
  rr_qerr.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_qerr);

  // qtmp is only read on the last integration, only written on the first
  RegionRequirement rr_qtmp(lp_qtmp_top, 0, 
			    ((which == FIRST_INTEGRATION) ? WRITE_DISCARD : 
			     (which == LAST_INTEGRATION) ? READ_ONLY :
			     READ_WRITE),
			    EXCLUSIVE, lr_qtmp);
  rr_qtmp.add_field(fid_out);
  sum_launcher.add_region_requirement(rr_qtmp);

  RegionRequirement rr_in(lp_in_top, 0, READ_ONLY, EXCLUSIVE, lr_in);
  rr_in.add_field(fid_1);
  rr_in.add_field(fid_2);
  rr_in.add_field(fid_3);
  rr_in.add_field(fid_4);
  rr_in.add_field(fid_5);
  rr_in.add_field(fid_6);
  sum_launcher.add_region_requirement(rr_in);

  sum_launcher.add_future(f_timestep);

  RankableTaskHelper::dispatch_task(sum_launcher, ctx, runtime, stage);
}

/*static*/
bool ScalarSum6IntegrateTask::fast_sum_6_integrate(RegionAccessor<AccessorType::Generic,double> fa_q,
						   RegionAccessor<AccessorType::Generic,double> fa_qerr,
						   RegionAccessor<AccessorType::Generic,double> fa_qtmp,
						   RegionAccessor<AccessorType::Generic,double> fa_1,
						   RegionAccessor<AccessorType::Generic,double> fa_2,
						   RegionAccessor<AccessorType::Generic,double> fa_3,
						   RegionAccessor<AccessorType::Generic,double> fa_4,
						   RegionAccessor<AccessorType::Generic,double> fa_5,
						   RegionAccessor<AccessorType::Generic,double> fa_6,
						   Rect<3> subgrid_bounds,
						   double alpha, double beta, double err, 
						   int which)
{
  Rect<3> subrect;
  ByteOffset q_offsets[3];
  double *q_ptr = fa_q.raw_rect_ptr<3>(subgrid_bounds, subrect, q_offsets);
  if(!q_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset qerr_offsets[3];
  double *qerr_ptr = fa_qerr.raw_rect_ptr<3>(subgrid_bounds, subrect, qerr_offsets);
  if(!qerr_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset qtmp_offsets[3];
  double *qtmp_ptr = fa_qtmp.raw_rect_ptr<3>(subgrid_bounds, subrect, qtmp_offsets);
  if(!qtmp_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in1_offsets[3];
  double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, in1_offsets);
  if(!in1_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in2_offsets[3];
  double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, in2_offsets);
  if(!in2_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in3_offsets[3];
  double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, in3_offsets);
  if(!in3_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in4_offsets[3];
  double *in4_ptr = fa_4.raw_rect_ptr<3>(subgrid_bounds, subrect, in4_offsets);
  if(!in4_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in5_offsets[3];
  double *in5_ptr = fa_5.raw_rect_ptr<3>(subgrid_bounds, subrect, in5_offsets);
  if(!in5_ptr) return false;
  assert(subrect == subgrid_bounds);
  ByteOffset in6_offsets[3];
  double *in6_ptr = fa_6.raw_rect_ptr<3>(subgrid_bounds, subrect, in6_offsets);
  if(!in6_ptr) return false;
  assert(subrect == subgrid_bounds);

  for(GenericPointInRectIterator<3> pir(Rect<3>(Point<3>::ZEROES(), subgrid_bounds.hi - subgrid_bounds.lo)); pir; pir++) {
    double *qp = q_ptr + pir.p[0] * q_offsets[0] + pir.p[1] * q_offsets[1] + pir.p[2] * q_offsets[2];
    double *qerrp = qerr_ptr + pir.p[0] * qerr_offsets[0] + pir.p[1] * qerr_offsets[1] + pir.p[2] * qerr_offsets[2];
    double *qtmpp = qtmp_ptr + pir.p[0] * qtmp_offsets[0] + pir.p[1] * qtmp_offsets[1] + pir.p[2] * qtmp_offsets[2];
    const double *in1p = in1_ptr + pir.p[0] * in1_offsets[0] + pir.p[1] * in1_offsets[1] + pir.p[2] * in1_offsets[2];
    const double *in2p = in2_ptr + pir.p[0] * in2_offsets[0] + pir.p[1] * in2_offsets[1] + pir.p[2] * in2_offsets[2];
    const double *in3p = in3_ptr + pir.p[0] * in3_offsets[0] + pir.p[1] * in3_offsets[1] + pir.p[2] * in3_offsets[2];
    const double *in4p = in4_ptr + pir.p[0] * in4_offsets[0] + pir.p[1] * in4_offsets[1] + pir.p[2] * in4_offsets[2];
    const double *in5p = in5_ptr + pir.p[0] * in5_offsets[0] + pir.p[1] * in5_offsets[1] + pir.p[2] * in5_offsets[2];
    const double *in6p = in6_ptr + pir.p[0] * in6_offsets[0] + pir.p[1] * in6_offsets[1] + pir.p[2] * in6_offsets[2];
    double sum = *in1p + *in2p + *in3p + (*in4p * (*in5p + *in6p));

    double q = *qp;
    double qerr, qtmp;
    if(which == FIRST_INTEGRATION) {
      qerr = 0;
      qtmp = q;
    } else {
      qerr = *qerrp;
      qtmp = *qtmpp;
    }

    qerr += err * sum;
    q = qtmp + alpha * sum;

    *qerrp = qerr;

    if(which != LAST_INTEGRATION) {
      qtmp = q + beta * sum;
      *qtmpp = qtmp;
    }

    *qp = q;
  }

  return true;
}

/*static*/
void ScalarSum6IntegrateTask::cpu_base_impl(S3DRank *rank, const Task *task,
				      const Rect<3> &my_subgrid_bounds,
				      const std::vector<RegionRequirement> &reqs,
				      const std::vector<PhysicalRegion> &regions,
				      Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  const Sum6IntegrateArgs *args = (const Sum6IntegrateArgs *)(task->args);

  Future f_timestep = task->futures[0];
  double timestep = f_timestep.get_reference<TimestepInfo>().cur_timestep;

  const int num_species = reqs[0].instance_fields.size();

  for (int spec = 0; spec < num_species; spec++)
  {
    // get whatever accessors you need here
    RegionAccessor<AccessorType::Generic,double> fa_q = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qerr = 
      regions[1].get_field_accessor(reqs[1].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qtmp = 
      regions[2].get_field_accessor(reqs[2].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_1 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_2 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_3 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec+2]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_4 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec+3]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_5 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec+4]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_6 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec+5]).typeify<double>();

    if(fast_sum_6_integrate(fa_q, fa_qerr, fa_qtmp, fa_1, fa_2, fa_3, fa_4, fa_5, fa_6, my_subgrid_bounds,
			    args->alpha * timestep, args->beta * timestep, args->err * timestep,
			    args->which)) {
      //printf("fast sum 6\n");
      continue;
    }

    printf("falling back to slow sum_6_integrate\n");

    for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double f1 = fa_1.read(dp);
      double f2 = fa_2.read(dp);
      double f3 = fa_3.read(dp);
      double f4 = fa_4.read(dp);
      double f5 = fa_5.read(dp);
      double f6 = fa_6.read(dp);

      double q = fa_q.read(dp);
      double qerr = ((args->which == FIRST_INTEGRATION) ? 0 : fa_qerr.read(dp));
      double qtmp = ((args->which == FIRST_INTEGRATION) ? q : fa_qtmp.read(dp));

      double sum = f1 + f2 + f3 + (f4 * (f5 + f6));

#ifdef DEBUG_SUM6INTEGRATE
      if(1 && pir.p == my_subgrid_bounds.lo) {
	printf("s6ii: %d %g %g %g %g %g %g = %g\n", 
	       reqs[0].instance_fields[spec], f1, f2, f3, f4, f5, f6, sum);
	printf("s6i: %d %g / %g %g %g %d / %g %g %g\n",
	       reqs[0].instance_fields[spec], sum, args->alpha * timestep, args->beta * timestep, args->err * timestep, args->which,
	       q, qerr, qtmp);
      }
#endif

      qerr += args->err * timestep * sum;
      q = qtmp + args->alpha * timestep * sum; 

      if(args->which != LAST_INTEGRATION) {
	qtmp = q + args->beta * timestep * sum;
	fa_qtmp.write(dp, qtmp);
      }

      fa_q.write(dp, q);
      fa_qerr.write(dp, qerr);
    } 
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern
void gpu_sum_6_integrate_fields(const int num_points,
				const double *in1_ptr,
				const double *in2_ptr,
				const double *in3_ptr,
				const double *in4_ptr,
				const double *in5_ptr,
				const double *in6_ptr,
				double *q_ptr,
				double *qerr_ptr,
				double *qtmp_ptr,
				double alpha, double beta, double err, 
                                int which);
#endif

/*static*/
void ScalarSum6IntegrateTask::gpu_base_impl(S3DRank *rank, const Task *task,
                             const Rect<3> &subgrid_bounds,
                             const std::vector<RegionRequirement> &reqs,
                             const std::vector<PhysicalRegion> &regions,
                             Context ctx, HighLevelRuntime *runtime)
{
#ifdef USE_GPU_KERNELS
#ifndef NO_COMPUTE
  const Sum6IntegrateArgs *args = (const Sum6IntegrateArgs *)(task->args);

  Future f_timestep = task->futures[0];
  double timestep = f_timestep.get_reference<TimestepInfo>().cur_timestep;

  const int num_species = reqs[0].instance_fields.size();

  for (int spec = 0; spec < num_species; spec++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_q = 
      regions[0].get_field_accessor(reqs[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qerr = 
      regions[1].get_field_accessor(reqs[1].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_qtmp = 
      regions[2].get_field_accessor(reqs[2].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_1 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_2 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_3 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec+2]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_4 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec+3]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_5 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec+4]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_6 = 
      regions[3].get_field_accessor(reqs[3].instance_fields[6*spec+5]).typeify<double>();

    Rect<3> subrect;
    ByteOffset out_offsets[3], offsets[3];

    double *q_ptr = fa_q.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
    if (!q_ptr || (subrect != subgrid_bounds) || 
	!offsets_are_dense<3, double>(subgrid_bounds, out_offsets)) assert(false);

    double *qerr_ptr = fa_qerr.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!qerr_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *qtmp_ptr = fa_qtmp.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!qtmp_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in1_ptr = fa_1.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in1_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in2_ptr = fa_2.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in2_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in3_ptr = fa_3.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in3_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in4_ptr = fa_4.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in4_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in5_ptr = fa_5.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in5_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    double *in6_ptr = fa_6.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if (!in6_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

    size_t n_pts = subgrid_bounds.volume();

    gpu_sum_6_integrate_fields(n_pts, in1_ptr, in2_ptr, in3_ptr, in4_ptr, in5_ptr, in6_ptr,
			       q_ptr, qerr_ptr, qtmp_ptr,
			       args->alpha * timestep, args->beta * timestep, args->err * timestep,
			       args->which);
  }
#endif
#else
  assert(0);
#endif
}

