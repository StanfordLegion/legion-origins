
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <assert.h>

#include "cuda.h"
#include "cuda_runtime.h"
#include "gpu_help.h"

#include "gpu_s3d_constants.h"

//#define DEBUG_CUDA

#ifdef DEBUG_CUDA
#define CUDA_SAFE_CALL(expr)                        \
  {                                                 \
    cudaError_t err = (expr);                       \
    if (err != cudaSuccess)                         \
    {                                               \
      printf("Cuda error: %s\n", cudaGetErrorString(err));  \
      assert(false);                                \
    }                                               \
  }
#else
#define CUDA_SAFE_CALL(expr)  expr
#endif

__global__
void calc_source_kernel(size_t n_pts, double dPdt_imposed,
			const double *in_gamma_ptr, const double *in_pressure_ptr,
		       double *out_source_ptr)
{
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  // Check for upper bound
  if (idx >= n_pts) return;  

  //  for(size_t i = 0; i < n_pts; i++) {
  double gamma = in_gamma_ptr[idx];
  double pressure = in_pressure_ptr[idx];
  double source = dPdt_imposed / (gamma * pressure);
  out_source_ptr[idx] = source;
}

__host__
void calc_source_gpu(size_t n_pts, double dPdt_imposed,
		     const double *in_gamma, const double *in_pressure,
		     double *out_source)
{
  const int threads_per_block = 256;
  const int num_blocks = (n_pts + (threads_per_block-1))/threads_per_block;

  calc_source_kernel<<<num_blocks,threads_per_block>>>(n_pts, dPdt_imposed,
						       in_gamma, in_pressure,
						       out_source);
}

__global__
void integrate_source_kernel(size_t n_pts,
			     double *q_ptr,
			     double *qerr_ptr,
			     double *qtmp_ptr,
			     const double *rhs_ptr,
			     const double *src_ptr,
			     const double *src2_ptr,
			     double alpha, double beta, double err, int which)
{
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  if (idx >= n_pts) return;

  double q = stream_load(q_ptr+idx);
  double qerr, qtmp;
  if(which == 0 /* FIRST_INTEGRATION */) {
    qerr = 0;
    qtmp = q;
  } else {
    qerr = stream_load(qerr_ptr+idx);
    qtmp = stream_load(qtmp_ptr+idx);
  }

  double rhs = stream_load(rhs_ptr+idx);
  double src = stream_load(src_ptr+idx);
  double src2 = src2_ptr ? stream_load(src2_ptr+idx) : 0;

  // rhs is augmented with the current state multiplied by the source term
  rhs += (q + src2) * src;

  qerr += err * rhs;
  q = qtmp + alpha * rhs;
  qtmp = q + beta * rhs; // always do add - math is free on GPU

  stream_store(q_ptr+idx, q);
  stream_store(qerr_ptr+idx, qerr);
  if(which != 2 /* LAST_INTEGRATION */)
    stream_store(qtmp_ptr+idx, qtmp);
}

__host__
void gpu_integrate_source(size_t n_pts,
			  double *q_ptr,
			  double *qerr_ptr,
			  double *qtmp_ptr,
			  const double *rhs_ptr,
			  const double *src_ptr,
			  const double *src2_ptr,
			  double alpha, double beta, double err, int which)
{
  const int threads_per_block = 256;
  const int num_blocks = (n_pts + (threads_per_block-1))/threads_per_block;

  integrate_source_kernel<<<num_blocks,threads_per_block>>>(n_pts,
							    q_ptr,
							    qerr_ptr,
							    qtmp_ptr,
							    rhs_ptr,
							    src_ptr,
							    src2_ptr,
							    alpha,
							    beta,
							    err,
							    which);
}
