
#include "rhsf.h"
#include "calc_chem.h"
#include "check_field.h"
#include "legion.h"
#include "arrays.h"

// Include Singe emitted header files
#include "getcoeffs.h"
#include "getrates.h"

// Singe header files for SSE
#ifdef USE_SSE_KERNELS
#include "sse_getcoeffs.h"
#include "sse_getrates.h"
#endif

// Singe header files for AVX
#ifdef USE_AVX_KERNELS
#include "avx_getcoeffs.h"
#include "avx_getrates.h"
#endif

#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
#include <x86intrin.h>
#endif

// Chemistry and physics for the GPU
#ifdef USE_GPU_KERNELS
#include "rhsf_gpu.h"
#endif

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

CalcGetratesTask::CalcGetratesTask(S3DRank *r,
                                   Domain domain,
                                   TaskArgument global_arg,
                                   ArgumentMap arg_map,
                                   Predicate pred,
                                   bool must,
                                   MapperID id,
                                   MappingTagID tag,
                                   bool add_requirements,
                                   bool redundant)
  : IndexLauncher(CalcGetratesTask::TASK_ID, domain, global_arg, 
                  arg_map, pred, must, id, tag | CalcGetratesTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    for(int i = 0; i < rank->n_spec; i++)
      rr_out.add_field(FID_RR(i));
    add_region_requirement(rr_out);

    RegionRequirement rr_state(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_state.add_field(FID_TEMP);
    add_region_requirement(rr_state);

    RegionRequirement rr_in(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    rr_in.add_field(FID_PRESSURE);
#ifdef REDUNDANT_WORK
    if (redundant) {
      rr_in.add_field(FID_AVMOLWT_ALT);
      for(int i = 0; i < rank->n_spec; i++)
        rr_in.add_field(FID_YSPECIES_ALT(i));
    } else {
      rr_in.add_field(FID_AVMOLWT);
      for(int i = 0; i < rank->n_spec; i++)
        rr_in.add_field(FID_YSPECIES(i));
    }
#else
    assert(!redundant);
    rr_in.add_field(FID_AVMOLWT);
    for(int i = 0; i < rank->n_spec; i++)
      rr_in.add_field(FID_YSPECIES(i));
#endif
    add_region_requirement(rr_in);

#ifdef GETRATES_NEEDS_DIFFUSION
    RegionRequirement rr_diff(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    for (int i = 0; i < rank->n_spec; i++)
      rr_diff.add_field(FID_DIFFUSION(i));
    add_region_requirement(rr_diff);
#endif
  }
}

void CalcGetratesTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  for(int s = 0; s < rank->n_spec; s++)
      check_field(runtime, ctx, rank->proc_grid_bounds, 
                  rank->is_grid, rank->ip_top, rank->lr_int, FID_RR(s),
		  rank, &S3DRank::RHSFArrays::rr_r, s, 1e-10, 1e5, 1e-50, "rr_r[%d]", s);
}

/*static*/ const char * const CalcGetratesTask::TASK_NAME = "getrates_task";

/*static*/
bool CalcGetratesTask::fast_cpu_getrates(const Rect<3> &my_subgrid_bounds,
                                         const std::vector<RegionRequirement> &reqs,
                                         const std::vector<PhysicalRegion> &regions,
                                         RegionAccessor<AccessorType::Generic,double> fa_temp,
                                         RegionAccessor<AccessorType::Generic,double> fa_pressure,
                                         RegionAccessor<AccessorType::Generic,double> fa_avmolwt
#ifdef GETRATES_NEEDS_DIFFUSION
					 , double dt
#endif
					 )
{
  Rect<3> subrect;
  ByteOffset offsets[3], offsets2[3];
  const double *in_temp = fa_temp.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets);
  if(!in_temp || (subrect != my_subgrid_bounds) || !offsets_are_dense<3, double>(subrect, offsets)) return false;

  const double *in_yspec = regions[2].get_field_accessor(reqs[2].instance_fields[2]).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_yspec || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;
  size_t in_yspec_stride = (regions[2].get_field_accessor(reqs[2].instance_fields[3]).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2) - in_yspec);

#ifdef GETRATES_NEEDS_DIFFUSION
  const double *in_diff = regions[3].get_field_accessor(FID_DIFFUSION(0)).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_diff || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;
  size_t in_diff_stride = (regions[3].get_field_accessor(FID_DIFFUSION(1)).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2) - in_diff);
  if(in_diff_stride != in_yspec_stride) return false;
#endif

  const double *in_press = fa_pressure.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_press || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;

  const double *in_avmolwt = fa_avmolwt.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_avmolwt || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;

  double *out_rr = regions[0].get_field_accessor(FID_RR(0)).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!out_rr || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;
  size_t out_rr_stride = (regions[0].get_field_accessor(FID_RR(1)).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2) - out_rr);
  if(out_rr_stride != in_yspec_stride) return false;

  size_t num_elmts = my_subgrid_bounds.volume();

#ifdef USE_AVX_KERNELS
  avx_getrates(in_press, in_temp, in_avmolwt, in_yspec,
               num_elmts, in_yspec_stride,
#ifdef GETRATES_NEEDS_DIFFUSION
               in_diff, dt,
#endif
               out_rr);
  _mm256_zeroall();
  return true;
#else
#ifdef USE_SSE_KERNELS
  sse_getrates(in_press, in_temp, in_avmolwt, in_yspec,
               num_elmts, in_yspec_stride,
#ifdef GETRATES_NEEDS_DIFFUSION
               in_diff, dt,
#endif
               out_rr);
  return true;
#else
  return false;
#endif
#endif
}

/*static*/
void CalcGetratesTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE 
  RegionAccessor<AccessorType::Generic,double> fa_temp = regions[1].get_field_accessor(FID_TEMP).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_pressure = regions[2].get_field_accessor(FID_PRESSURE).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_avmolwt = regions[2].get_field_accessor(reqs[2].instance_fields[1]).typeify<double>();

#ifdef GETRATES_NEEDS_DIFFUSION
  // get timestep from a future
  Future f_timestep = task->futures[0];
  double timestep = f_timestep.get_reference<TimestepInfo>().cur_timestep;
  const double timeconv = L_REF / A_REF;
  const double dt = timestep * timeconv;
#endif

  if (!fast_cpu_getrates(subgrid_bounds, reqs, regions, fa_temp, fa_pressure, fa_avmolwt
#ifdef GETRATES_NEEDS_DIFFUSION
			 , dt
#endif
			 ))
  {
    //RegionAccessor<AccessorType::Generic,double> fa_rates = regions[0].get_field_accessor(FID_RR(0)).typeify<double>();
    // Assumes contiguous species values
    //RegionAccessor<AccessorType::Generic,double> fa_species = regions[2].get_field_accessor(FID_YSPECIES(0)).typeify<double>();
#ifdef GETRATES_NEEDS_DIFFUSION
    // Assumes contiguous species values
    //RegionAccessor<AccessorType::Generic,double> fa_diff = regions[3].get_field_accessor(FID_DIFFUSION(0)).typeify<double>();
    double *diffusion = (double*)alloca(rank->n_spec * sizeof(double));
#endif
    
    // We get to ignore avmolwt here since this version of getrates computes it anyway
    double *yspecs = (double *)alloca(rank->n_spec * sizeof(double));
    double *rrs = (double *)alloca(rank->n_spec * sizeof(double));

    for (GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++)
    {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double temperature = fa_temp.read(dp);
      double pressure = fa_pressure.read(dp);
      double avmolwt = fa_avmolwt.read(dp);
      
      for (int i = 0; i < rank->n_spec; i++)
        yspecs[i] = regions[2].get_field_accessor(reqs[2].instance_fields[i+2]).typeify<double>().read(dp);

#ifdef GETRATES_NEEDS_DIFFUSION
      for (int i = 0; i < rank->n_spec; i++)
        diffusion[i] = regions[3].get_field_accessor(FID_DIFFUSION(i)).typeify<double>().read(dp); 
#endif

      getrates(pressure, temperature, avmolwt, yspecs,
#ifdef GETRATES_NEEDS_DIFFUSION
               diffusion, dt,
#endif
               rrs);
      for (int i = 0; i < rank->n_spec; i++)
        regions[0].get_field_accessor(FID_RR(i)).typeify<double>().write(dp, rrs[i]);
    }
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern 
void gpu_check_chemistry(const int max_elements, const double max_rate,
                         const double *rates, const int num_species,
                         const int rank_x, const int rank_y, const int rank_z);
#endif

void CalcGetratesTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#if USE_GPU_KERNELS
  // Assumes contiguous species values
  RegionAccessor<AccessorType::Generic,double> rates = regions[0].get_field_accessor(FID_RR(0)).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> temperature = regions[1].get_field_accessor(FID_TEMP).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> pressure = regions[2].get_field_accessor(FID_PRESSURE).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> avmolwt = regions[2].get_field_accessor(reqs[2].instance_fields[1]).typeify<double>();
  // Assumes contiguous species values
  RegionAccessor<AccessorType::Generic,double> species = regions[2].get_field_accessor(reqs[2].instance_fields[2]).typeify<double>();
#ifdef GETRATES_NEEDS_DIFFUSION
  // get timestep from a future
  Future f_timestep = task->futures[0];
  double timestep = f_timestep.get_reference<TimestepInfo>().cur_timestep;
  const double timeconv = L_REF / A_REF;
  const double dt = timestep * timeconv;
  // Assumes contiguous species values
  RegionAccessor<AccessorType::Generic,double> diff = regions[3].get_field_accessor(FID_DIFFUSION(0)).typeify<double>();
#endif

  Rect<3> bounds = subgrid_bounds;
  Rect<3> subrect;
  ByteOffset rate_offsets[3];
  double *rate_ptr = rates.raw_rect_ptr<3>(bounds, subrect, rate_offsets);
  assert(rate_ptr != NULL);
  assert(subrect == bounds);
  bool dense_offsets = offsets_are_dense<3, double>(subrect, rate_offsets);
  assert(dense_offsets);
  size_t rate_stride = 
    regions[0].get_field_accessor(FID_RR(1)).typeify<double>()
      .raw_rect_ptr<3>(bounds, subrect, rate_offsets) - rate_ptr;
#define CAREFUL_STRIDE_CHECK
#ifdef CAREFUL_STRIDE_CHECK
  for(int i = 2; i < rank->n_spec; i++) {
    size_t s = regions[0].get_field_accessor(FID_RR(i)).typeify<double>()
      .raw_rect_ptr<3>(bounds, subrect, rate_offsets) - rate_ptr;
    assert(s == (rate_stride * i));
  }
#endif
  ByteOffset temp_offsets[3];
  double *temp_ptr = temperature.raw_rect_ptr<3>(bounds, subrect, temp_offsets);
  assert(temp_ptr != NULL);
  assert(subrect == bounds);
  assert(!offset_mismatch(3, rate_offsets, temp_offsets));
  ByteOffset pres_offsets[3];
  double *pres_ptr = pressure.raw_rect_ptr<3>(bounds, subrect, pres_offsets);
  assert(pres_ptr != NULL);
  assert(subrect == bounds);
  assert(!offset_mismatch(3, rate_offsets, pres_offsets));
  ByteOffset avmolwt_offsets[3];
  double *avmolwt_ptr = avmolwt.raw_rect_ptr<3>(bounds, subrect, avmolwt_offsets);
  assert(avmolwt_ptr != NULL);
  assert(subrect == bounds);
  assert(!offset_mismatch(3, rate_offsets, avmolwt_offsets));
  ByteOffset spec_offsets[3];
  double *spec_ptr = species.raw_rect_ptr<3>(bounds, subrect, spec_offsets);
  assert(spec_ptr != NULL);
  assert(subrect == bounds);
  assert(!offset_mismatch(3, rate_offsets, spec_offsets));
  {
    size_t spec_stride =
      regions[2].get_field_accessor(reqs[2].instance_fields[3]).typeify<double>()
        .raw_rect_ptr<3>(bounds, subrect, spec_offsets) - spec_ptr;
    assert(spec_stride == rate_stride);
#ifdef CAREFUL_STRIDE_CHECK
    for(int i = 2; i < rank->n_spec; i++) {
      size_t s = regions[2].get_field_accessor(reqs[2].instance_fields[i+2]).typeify<double>()
	.raw_rect_ptr<3>(bounds, subrect, spec_offsets) - spec_ptr;
      assert(s == (rate_stride * i));
    }
#endif
  }
#ifdef GETRATES_NEEDS_DIFFUSION
  ByteOffset diff_offsets[3];
  double *diff_ptr = diff.raw_rect_ptr<3>(bounds, subrect, diff_offsets);
  assert(diff_ptr != NULL);
  assert(subrect == bounds);
  assert(!offset_mismatch(3, rate_offsets, diff_offsets));
  {
    size_t diff_stride = 0;
    for(int i = 0; i < GETRATES_STIFF_SPECIES; i++) {
      int s = stif_species_indexes[i];
      if(s == 0) continue;  // can't compute stride from the first element
      diff_stride = (regions[3].get_field_accessor(FID_DIFFUSION(s)).typeify<double>()
		     .raw_rect_ptr<3>(bounds, subrect, diff_offsets) - diff_ptr) / s;
      assert(diff_stride == rate_stride);
#ifdef CAREFUL_STRIDE_CHECK
      break; // only need one unless we're paranoid
#endif
    }
  }
#endif

#ifndef GETRATES_NEEDS_DIFFUSION
  run_gpu_getrates(temp_ptr, pres_ptr, spec_ptr, avmolwt_ptr, bounds.dim_size(0),
                   bounds.dim_size(1), bounds.dim_size(2), rate_ptr);
#else
  run_gpu_getrates(temp_ptr, pres_ptr, spec_ptr, avmolwt_ptr, diff_ptr, bounds.dim_size(0),
                   bounds.dim_size(1), bounds.dim_size(2), dt, rate_ptr);
#endif

#ifdef RUN_CHECKS
  gpu_check_chemistry(bounds.volume(), 1e-04, rate_ptr, rank->n_spec,
                      rank->my_proc_id[0], rank->my_proc_id[1], rank->my_proc_id[2]);
#endif

#endif
#endif
}

