
#include "rhsf.h"
#include "rhsf_mapper.h"
#include "arrays.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Arrays;

LegionRuntime::Logger::Category log_mapper("rhsf_mapper");

RHSFMapper::RHSFMapper(Machine machine, HighLevelRuntime *rt, Processor local,
                       const std::vector<FieldID> &all_q,
                       const std::vector<FieldID> &all_temp,
                       const std::vector<FieldID> &all_state,
                       const std::vector<FieldID> &all_gpu,
		       bool _enable_profiling)
  : DefaultMapper(machine, rt, local),
    has_subranks((S3DRank::get_rank(Point<3>::ZEROES(), false)->n_subranks > 0)),
    mixed_gpu(S3DRank::get_mixed_stencils() || S3DRank::get_all_gpu()),
    all_gpu(S3DRank::get_all_gpu()),
    enable_profiling(_enable_profiling)
{
  all_q_fields.insert(all_q.begin(),all_q.end());
  all_temp_fields.insert(all_temp.begin(),all_temp.end());
  all_state_fields.insert(all_state.begin(),all_state.end());
  all_gpu_temp_fields.insert(all_gpu.begin(),all_gpu.end());

  std::set<Processor> all_procs;
  machine.get_all_processors(all_procs);
  for (std::set<Processor>::const_iterator it = all_procs.begin();
        it != all_procs.end(); it++)
  {
#ifndef SHARED_LOWLEVEL
    // Filter out any processors that aren't local (allow differences only
    //  in the 16 LSBs - should work for 32-bit and 64-bit IDs)
    if (((it->id ^ local_proc.id) >> 16) != 0)
      continue;
#endif
    Processor::Kind k = it->kind();
    if (k == Processor::LOC_PROC)
      local_cpus.push_back(*it);
    else if (k == Processor::TOC_PROC)
      local_gpus.push_back(*it);
  }

  local_next_cpu.resize(MAX_NUM_TASK_ID, 0);
  local_next_gpu.resize(MAX_NUM_TASK_ID, 0);
}

RHSFMapper::~RHSFMapper(void)
{
}

void RHSFMapper::set_bounds(const Rect<3>& bounds)
{
  proc_grid_bounds = bounds;
}

void RHSFMapper::assign_point(DomainPoint point, Processor proc)
{
  if (rank_map.find(point) == rank_map.end())
  {
    // If we haven't added anything to this point yet, then do it
    rank_map[point].push_back(proc);
    // Make the next map as big as the number of possible task IDs
    next_map[point].resize(MAX_NUM_TASK_ID, 0);
  }
  else
  {
    // See if we already added this processor. If not, add it.
    std::vector<Processor> &proc_vec = rank_map[point];
    for (std::vector<Processor>::const_iterator it = proc_vec.begin();
          it != proc_vec.end(); it++)
    {
      if ((*it) == proc)
        return;
    }
    proc_vec.push_back(proc);
  }
}

void RHSFMapper::select_task_options(Task *task)
{
  // Default values
  task->inline_task = false;
  task->spawn_task = false;
  task->map_locally = false;
  if ((task->task_id == UPDATE_MAPPERS_TASK_ID) ||
      (task->task_id == CREATE_GHOST_REGIONS_TASK_ID) ||
      (task->task_id == GATHER_BARRIERS_TASK_ID))
    task->map_locally = true;
  task->profile_task = false;
  if (task->tag & RHSF_MAPPER_PRIORITIZE)
    task->task_priority = 1;
  else
    task->task_priority = 0;
  // If it is an index space task then just keep
  // it here since we're going to call slice domain anyway
  if (task->is_index_space)
    task->target_proc = local_proc;
  else
  {
    if (task->tag & RHSF_MAPPER_FORCE_RANK_MATCH)
    {
      // Have a special shortcut here for running
      // rank tasks on our GPUs.  Right now this only
      // works if the distribute-to-rank mapping is
      // one-to-one since we only know about our local GPUs
      if (((all_gpu && (task->tag & (RHSF_MAPPER_ALL_GPU | RHSF_MAPPER_MIXED_GPU))) ||
           (mixed_gpu && (task->tag & RHSF_MAPPER_MIXED_GPU)) ||
           (!has_subranks && (task->tag & RHSF_MAPPER_SINGLE_RANK))) && !local_gpus.empty() && 
          task->variants->has_variant(Processor::TOC_PROC, true/*single*/, true/*index*/)) 
      {
        assert(task->task_id < local_next_gpu.size());  
        unsigned next_proc = local_next_gpu[task->task_id]++;
        if (local_next_gpu[task->task_id] == local_gpus.size())
          local_next_gpu[task->task_id] = 0;
        task->target_proc = local_gpus[next_proc];  
        return;
      }
      const DomainPoint &point = task->index_point;
      assert(rank_map.find(point) != rank_map.end());
      const std::vector<Processor> &proc_vec = rank_map[point];
      std::vector<unsigned> &next_vec = next_map[point];
      if (has_subranks && (task->tag & RHSF_MAPPER_SUBRANKABLE))
      {
        // It has sub-ranks, so no need to worry about balancing it
        // Just send it to the first processor in its list
        task->target_proc = proc_vec.front();
      }
      else if (task->tag & RHSF_MAPPER_BALANCE)
      {
        // No subranks, but needs balance across processors
        // so send it to the next processor for its task ID
        assert(task->task_id < next_vec.size());
        unsigned next_proc = next_vec[task->task_id]++;
        if (next_vec[task->task_id] == proc_vec.size())
          next_vec[task->task_id] = 0;
        assert(next_proc < proc_vec.size());
        task->target_proc = proc_vec[next_proc];
      }
      else
      {
        // Doesn't matter, just send it to the first processor
        // for its assigned rank
        task->target_proc = proc_vec.front();
      }
    }
    else
    {
      // Keep it on one of our local processors   
      // See if we have GPUs and it has a GPU variant
      if (!local_gpus.empty() && 
          task->variants->has_variant(Processor::TOC_PROC, true/*single*/, true/*index*/))
      {
        assert(task->task_id < local_next_gpu.size());  
        unsigned next_proc = local_next_gpu[task->task_id]++;
        if (local_next_gpu[task->task_id] == local_gpus.size())
          local_next_gpu[task->task_id] = 0;
        task->target_proc = local_gpus[next_proc];
      }
      else
      {
        // Put it on one of our CPUs
        assert(task->task_id < local_next_cpu.size());
        unsigned next_proc = local_next_cpu[task->task_id]++;
        if (local_next_cpu[task->task_id] == local_cpus.size())
          local_next_cpu[task->task_id] = 0;
        task->target_proc = local_cpus[next_proc];
      }
    }
  }
}

void RHSFMapper::slice_domain(const Task *task, const Domain &domain,
                              std::vector<DomainSplit> &slices)
{
  // Special cases for startup tasks
  if (task->task_id == CONNECT_MPI_TASK_ID)
  {
    const int DIM = 2;
    std::vector<Processor> proc_list;
    std::set<Processor> all_procs;
    machine.get_all_processors(all_procs);
    for (std::set<Processor>::const_iterator it = all_procs.begin();
          it != all_procs.end(); it++)
    {
      if (it->kind() != Processor::LOC_PROC)
        continue;
      proc_list.push_back(*it);
    }
    assert(proc_list.size() == (size_t)(domain.get_rect<DIM>().hi[0] + 1));
    for (Domain::DomainPointIterator pir(domain); pir; pir++) {
      DomainPoint dp = pir.p;
      Point<DIM> p = dp.get_point<DIM>();
      Processor proc = proc_list[p[0]];
      Mapper::DomainSplit ds(Domain::from_rect<DIM>(Rect<DIM>(p, p)), proc, 
                            false /* recurse */, false /* stealable */);
      slices.push_back(ds);
    }
    return;
  }
  if (task->task_id == UPDATE_MAPPERS_TASK_ID)
  {
    const int DIM = 1;
    assert(domain.get_dim() == DIM);
    std::vector<Processor> proc_list;
    std::set<int> seen_spaces;
    std::set<Processor> all_procs;
    machine.get_all_processors(all_procs);
    for (std::set<Processor>::const_iterator it = all_procs.begin();
          it != all_procs.end(); it++)
    {
      if (it->kind() != Processor::LOC_PROC)
        continue;
      if (seen_spaces.find(it->address_space()) != seen_spaces.end())
        continue;
      proc_list.push_back(*it);
      seen_spaces.insert(it->address_space());
    }
    assert(proc_list.size() == (size_t)(domain.get_rect<DIM>().hi[0] + 1));
    for (Domain::DomainPointIterator pir(domain); pir; pir++) {
      DomainPoint dp = pir.p;
      Point<DIM> p = dp.get_point<DIM>();
      Processor proc = proc_list[p[0]];
      Mapper::DomainSplit ds(Domain::from_rect<DIM>(Rect<DIM>(p, p)), proc, 
                             false /* recurse */, false /* stealable */);
      slices.push_back(ds);
    }
    return;
  }
  // Handle cases where we need to send points to the right places
  if (task->tag & RHSF_MAPPER_FORCE_RANK_MATCH)
  {
    const int DIM = 3;
    assert(domain.get_dim() == DIM);

    // Have a special case here for running rank
    // tasks on the GPU.  We'll only do this if there is only
    // one point in our domain, so we know that we're keeping
    // this local because we don't know about remote GPUs.
    if (((all_gpu && (task->tag & (RHSF_MAPPER_ALL_GPU | RHSF_MAPPER_MIXED_GPU))) ||
         (mixed_gpu && (task->tag & RHSF_MAPPER_MIXED_GPU)) ||
         (!has_subranks && (task->tag & RHSF_MAPPER_SINGLE_RANK))) && 
        (domain.get_volume() == 1) && (!local_gpus.empty()) &&
        task->variants->has_variant(Processor::TOC_PROC, false/*single*/, true/*index*/))
    {
      // Pick our next local GPU for the given task ID
      assert(task->task_id < local_next_gpu.size());
      unsigned next_gpu = local_next_gpu[task->task_id]++;
      if (local_next_gpu[task->task_id] == local_gpus.size())
        local_next_gpu[task->task_id] = 0;
      assert(next_gpu < local_gpus.size());
      Mapper::DomainSplit ds(domain, local_gpus[next_gpu],
                             false/*recurse*/, false/*stealable*/);
      slices.push_back(ds);
      return;
    }

    for (Domain::DomainPointIterator pir(domain); pir; pir++)
    {
      const DomainPoint &dp = pir.p;
      Point<DIM> p = dp.get_point<DIM>();
      assert(rank_map.find(dp) != rank_map.end());
      const std::vector<Processor> &proc_vec = rank_map[dp];
      std::vector<unsigned> &next_vec = next_map[dp];
      if (has_subranks && (task->tag & RHSF_MAPPER_SUBRANKABLE))   
      {
        // It has sub-ranks, so no need to worry about balancing it
        // Just send it to the first processor in its list
        Mapper::DomainSplit ds(Domain::from_rect<DIM>(Rect<DIM>(p, p)), proc_vec.front(),
                               false/*recurse*/, false/*stealable*/);
        slices.push_back(ds);
      }
      else if (task->tag & RHSF_MAPPER_BALANCE)
      {
        // No subranks, but needs balance across processors
        // so send it to the next processor for its task ID
        assert(task->task_id < next_vec.size());
        unsigned next_proc = next_vec[task->task_id]++;
        if (next_vec[task->task_id] == proc_vec.size())
          next_vec[task->task_id] = 0;
        assert(next_proc < proc_vec.size());
        Mapper::DomainSplit ds(Domain::from_rect<DIM>(Rect<DIM>(p, p)), proc_vec[next_proc],
                               false/*recurse*/, false/*stealable*/);
        slices.push_back(ds);
      }
      else
      {
        // Doesn't matter, just send it to the first processor
        // for its assigned rank
        Mapper::DomainSplit ds(Domain::from_rect<DIM>(Rect<DIM>(p, p)), proc_vec.front(),
                               false/*recurse*/, false/*stealable*/);
        slices.push_back(ds);
      }
    }
    return;
  }
  if (task->task_id == CALC_SPECIES_TASK_ID)
  {
    if ((task->tag & (RHSF_MAPPER_MIXED_GPU | RHSF_MAPPER_ALL_GPU)) && !local_gpus.empty() && 
        task->variants->has_variant(Processor::TOC_PROC, false/*single*/, true/*index*/))
    {
      DefaultMapper::decompose_index_space(domain, local_gpus, 1/*splitting factor*/, slices);
    }
    else
    {
      DefaultMapper::decompose_index_space(domain, local_cpus, 1/*splitting factor*/, slices);
    }
    return;
  }
  // Otherwise we're going to distribute it locally, see if we can
  // distribute over GPUs, if not try CPUs
  if (!local_gpus.empty() &&
      task->variants->has_variant(Processor::TOC_PROC, false/*single*/, true/*index*/))
  {
    DefaultMapper::decompose_index_space(domain, local_gpus, 1/*splitting factor*/, slices);
  }
  else
  {
    DefaultMapper::decompose_index_space(domain, local_cpus, 1/*splitting factor*/, slices);
  }
}

bool RHSFMapper::map_task(Task *task)
{
  // when tracking execution progress, get profiling data for the two tasks that
  //  exchange data with MPI
#if 0
  if (enable_profiling && 
      ((task->task_id == AWAIT_MPI_TASK_ID) ||
       (task->task_id == HANDOFF_TO_MPI_TASK_ID)))
    task->profile_task = true;
#endif
  // If this is the create ghost regions task or
  // the ghost regions fixup task then put all of the
  // regions in to the RDMA memory if it exists
  if (task->task_id == UPDATE_MAPPERS_TASK_ID)
  {
    Memory target_mem = 
      machine_interface.find_memory_kind(task->target_proc, Memory::SYSTEM_MEM);
    assert(target_mem.exists());
    for (unsigned idx = 0; idx < task->regions.size(); idx++)
    {
      task->regions[idx].target_ranking.push_back(target_mem);
      task->regions[idx].virtual_map = false;
      task->regions[idx].enable_WAR_optimization = false;
      task->regions[idx].reduction_list = false;
      task->regions[idx].blocking_factor = task->regions[idx].max_blocking_factor;
    }
    return false;
  }
  if ((task->task_id == CREATE_GHOST_REGIONS_TASK_ID) ||
      (task->task_id == GATHER_BARRIERS_TASK_ID) ||
      (task->task_id == GHOST_REGIONS_FIXUP_TASK_ID))
  {
    Memory target_mem = 
      machine_interface.find_memory_kind(task->target_proc, Memory::REGDMA_MEM);
    // If we don't have a REGDMA memory, then put it 
    // in the system memory
    if (!target_mem.exists())
    {
      target_mem = 
        machine_interface.find_memory_kind(task->target_proc, Memory::SYSTEM_MEM);
      assert(target_mem.exists());
    }
    for (unsigned idx = 0; idx < task->regions.size(); idx++)
    {
      task->regions[idx].target_ranking.push_back(target_mem);
      task->regions[idx].virtual_map = false;
      task->regions[idx].enable_WAR_optimization = false;
      task->regions[idx].reduction_list = false;
      task->regions[idx].blocking_factor = task->regions[idx].max_blocking_factor;
    }
    return false;
  }
  if (task->task_id == DISTRIBUTE_TASK_ID)
  {
    // For the distribute task we should always have exactly
    // one valid instance
    for (unsigned idx = 0; idx < task->regions.size(); idx++)
    {
      RegionRequirement &req = task->regions[idx];
      assert(req.current_instances.size() == 1);
      req.target_ranking.push_back(req.current_instances.begin()->first);
      req.virtual_map = false;
      req.enable_WAR_optimization = false;
      req.reduction_list = false;
      req.blocking_factor = req.max_blocking_factor;
    }
    // Don't need any notification
    return false;
  }
  // Add extra fields onto the await MPI task
  if (task->task_id == AWAIT_MPI_TASK_ID)
  {
    for (unsigned idx = 0; idx < task->regions.size(); idx++)
    {
      if ((idx == 0) || (idx == 1))
        task->regions[idx].additional_fields = all_q_fields;
      else if (idx == 2)
        task->regions[idx].additional_fields = all_temp_fields;
      else if (idx == 3)
        task->regions[idx].additional_fields = all_state_fields;
      task->regions[idx].make_persistent = true;
    }
  }
  Processor::Kind target_kind = task->target_proc.kind();
  if (target_kind == Processor::TOC_PROC)
  {
    Memory fb_mem = machine_interface.find_memory_kind(task->target_proc, Memory::GPU_FB_MEM);
    assert(fb_mem.exists());
    for (unsigned idx = 0; idx < task->regions.size(); idx++)
    {
      task->regions[idx].target_ranking.push_back(fb_mem);
      task->regions[idx].virtual_map = false;
      task->regions[idx].enable_WAR_optimization = false;
      task->regions[idx].reduction_list = false;
      task->regions[idx].blocking_factor = task->regions[idx].max_blocking_factor;
      // Keep these GPU instances around so we don't blow though IDs and make
      // the low-level runtime upset for now.
      // TODO: Sean needs to fix instance ID recycling
      task->regions[idx].make_persistent = true;
    }
    // If we're running a calc temp task or a calc diffusion task on the GPU, add aditional gpu fields
    if (task->task_id == CALC_TEMP_TASK_ID)
    {
      if (all_gpu)
      {
        task->regions[0].additional_fields = all_state_fields;
        task->regions[1].additional_fields = all_temp_fields;
        task->regions[0].make_persistent = true;
        task->regions[1].make_persistent = true;
      }
      else
      {
        task->regions[1].additional_fields = all_gpu_temp_fields;
        task->regions[1].make_persistent = true;
      }
    }
    else if (task->task_id == CALC_SPECIES_TASK_ID)
    {
      if (all_gpu)
      {
        task->regions[0].additional_fields = all_temp_fields;
        task->regions[0].make_persistent = true;
      }
      else
      {
        task->regions[0].additional_fields = all_gpu_temp_fields;
        task->regions[0].make_persistent = true;
      }
    }
  }
  else
  {
    Memory system_mem = machine_interface.find_memory_kind(task->target_proc, Memory::SYSTEM_MEM);
    assert(system_mem.exists());
    for (unsigned idx = 0; idx < task->regions.size(); idx++)
    {
      // Don't need to map restricted regions
      if (task->regions[idx].restricted)
        continue;
      task->regions[idx].target_ranking.push_back(system_mem);
      task->regions[idx].virtual_map = false;
      task->regions[idx].enable_WAR_optimization = false;
      task->regions[idx].reduction_list = false;
      task->regions[idx].blocking_factor = task->regions[idx].max_blocking_factor;
    }
    // If this is a sub-task of distribute task, then load balance it
    if ((task->task_id > DISTRIBUTE_TASK_ID) && (local_cpus.size() > 1))
      task->additional_procs.insert(local_cpus.begin(),local_cpus.end());
    // If this is a calc enthalpy task, allow it to break an anti-dependence
    // with imposed pressure tasks
    //if (task->task_id == CALC_ENTHALPY_TASK_ID)
    //  task->regions[0].enable_WAR_optimization = true;
  }
  // Give CEMA tasks lower priority
  if (task->task_id == CALC_CEMA_TASK_ID)
    task->task_priority = -1;
  // Don't need any notification
  return false;
}

void RHSFMapper::select_tasks_to_schedule(const std::list<Task*> &ready_tasks)
{
  // check to see if we have priority and there are no tasks in a previous rhsf
  bool has_priority = false;
  long long priority_id = -1;
  int priority_stage = -1;

  long long smallest_task_id = -1;
  int smallest_task_stage = -1;

  for (std::list<Task*>::const_iterator it = ready_tasks.begin();
        it != ready_tasks.end(); it++)
  {
    long long uid = (*it)->get_unique_task_id();
    if ((*it)->task_priority > 0)
    {
      has_priority = true;
      if ((priority_id == -1) || (uid < priority_id))
      {
        priority_id = uid;
        priority_stage = (*it)->tag & RHSF_MAPPER_STAGE_MASK;
      }
    }
    else
    {
      if ((smallest_task_id == -1) || (uid < smallest_task_id))
      {
        smallest_task_id = uid;
        smallest_task_stage = (*it)->tag & RHSF_MAPPER_STAGE_MASK;
      }
    }
  }
  if (has_priority && (priority_stage == smallest_task_stage))
  {
    for (std::list<Task*>::const_iterator it = ready_tasks.begin();
          it != ready_tasks.end(); it++)
    {
      if (((*it)->task_priority > 0) && 
          ((int)((*it)->tag & RHSF_MAPPER_STAGE_MASK) == smallest_task_stage))
      {
        (*it)->schedule = true;
      }
    }
  }
  else
    // If we didn't find anything with priority then just do the normal thing
    DefaultMapper::select_tasks_to_schedule(ready_tasks);
}

bool RHSFMapper::rank_copy_targets(const Mappable *mappable,
                                   LogicalRegion rebuild_region,
                                   const std::set<Memory> &current_instances,
                                   bool complete,
                                   size_t max_blocking_factor,
                                   std::set<Memory> &to_reuse,
                                   std::vector<Memory> &to_create,
                                   bool &create_one, size_t &blocking_factor)
{
  if (all_gpu && !local_gpus.empty())
  {
    Memory local_fb = machine_interface.find_memory_kind(local_gpus.front(), Memory::GPU_FB_MEM);
    assert(local_fb.exists());
    if (current_instances.find(local_fb) != current_instances.end())
      to_reuse.insert(local_fb);
    else
      to_create.push_back(local_fb);
  }
  else
  {
    Memory local_system = machine_interface.find_memory_kind(local_proc, Memory::SYSTEM_MEM);
    assert(local_system.exists());
    //assert(current_instances.find(local_system) != current_instances.end());
    if (current_instances.find(local_system) != current_instances.end())
      to_reuse.insert(local_system);
    else
      to_create.push_back(local_system);
  }
  //if (current_instances.find(local_system) != current_instances.end())
  //  to_reuse.insert(local_system);
  //else
  //  to_create.push_back(local_system);
  create_one = true;
  blocking_factor = max_blocking_factor;
  // don't make a composite instance
  return false;
}

void RHSFMapper::select_task_variant(Task *task)
{
  typedef std::map<VariantID,TaskVariantCollection::Variant> VariantMap;
  const VariantMap &all_variants = task->variants->get_all_variants();
  if (task->tag & RHSF_MAPPER_FORCE_RANK_MATCH)
  {
    if (((all_gpu && (task->tag & (RHSF_MAPPER_ALL_GPU | RHSF_MAPPER_MIXED_GPU))) ||
         (mixed_gpu && (task->tag & RHSF_MAPPER_MIXED_GPU)) ||
         (!has_subranks && (task->tag & RHSF_MAPPER_SINGLE_RANK))) && 
        (task->target_proc.kind() == Processor::TOC_PROC))
    {
      assert(task->variants->has_variant(RHSF_GPU_LEAF_VARIANT));
      task->selected_variant = RHSF_GPU_LEAF_VARIANT;
      return;
    }
    assert(task->target_proc.kind() == Processor::LOC_PROC);
    // Always pick a CPU variant and only pick an inner
    // one if we're told we can and we have subranks
    if (has_subranks && (task->tag & RHSF_MAPPER_SUBRANKABLE))
    {
      // Make sure it has an inner variant
      assert(all_variants.find(RHSF_INNER_VARIANT) != all_variants.end());
      task->selected_variant = RHSF_INNER_VARIANT;
    }
    else if (all_variants.find(RHSF_CPU_LEAF_VARIANT) != all_variants.end())
    {
      // Use the leaf variant
      task->selected_variant = RHSF_CPU_LEAF_VARIANT;
    }
    else
    {
      // Not something registered with the SubrankableTaskHelper
      // so just find the CPU non-inner variant
      for (VariantMap::const_iterator it = all_variants.begin();
            it != all_variants.end(); it++)
      {
        if (!it->second.inner && (it->second.proc_kind == Processor::LOC_PROC))
        {
          task->selected_variant = it->first;
          return;
        }
      }
      // should never get here
      assert(false);
    }
  }
  else
  {
    if (task->target_proc.kind() == Processor::TOC_PROC)
    {
      // Find the GPU variant
      for (VariantMap::const_iterator it = all_variants.begin();
            it != all_variants.end(); it++)
      {
        if (it->second.proc_kind == Processor::TOC_PROC)
        {
          assert(!it->second.inner);
          task->selected_variant = it->first;
          return;
        }
      }
      // Should never get here
      assert(false);
    }
    else
    {
      // Find the CPU variant that is not an inner task 
      for (VariantMap::const_iterator it = all_variants.begin();
            it != all_variants.end(); it++)
      {
        if (it->first == RHSF_INNER_VARIANT)
          continue;
        if (it->second.inner)
          continue;
        if (it->second.proc_kind == Processor::LOC_PROC)
        {
          task->selected_variant = it->first;
          return;
        }
      }
      // Should never get here
      assert(false);
    }
  }
}

size_t RHSFMapper::select_region_layout(const Task *task, Processor target,
					const RegionRequirement &req, 
					unsigned index, 
					const Memory &chosen_mem, 
					size_t max_blocking_factor)
{
  // ALWAYS choose SOA
  return max_blocking_factor;
}

bool RHSFMapper::map_copy(Copy *copy)
{
  assert(copy->src_requirements.size() == 1);
  // Put all of the current instances into the register memory 
  Memory system_mem = machine_interface.find_memory_kind(local_proc, Memory::REGDMA_MEM);
  // Unless we have no registered memory or we are mapping a big region
  if (!system_mem.exists() || all_gpu)
  {
    system_mem = machine_interface.find_memory_kind(local_proc, Memory::SYSTEM_MEM);
    assert(system_mem.exists());
  }
  RegionRequirement &src_req = copy->src_requirements[0];
  src_req.target_ranking.push_back(system_mem);
  src_req.virtual_map = false;
  src_req.enable_WAR_optimization = false;
  src_req.reduction_list = false;
  src_req.blocking_factor = src_req.max_blocking_factor;
  src_req.make_persistent = true;
  //for (std::map<Memory,bool>::const_iterator it = 
  //      copy->src_requirements[0].current_instances.begin(); it !=
  //      copy->src_requirements[0].current_instances.end(); it++)
  //{
  //  copy->src_requirements[0].target_ranking.push_back(it->first);
  //}
  // There should only be one destination instance and it should be
  // restricted which means that we can ignore it
  assert(copy->dst_requirements.size() == 1);
  assert(copy->dst_requirements[0].restricted);
  // Don't need the result
  return false;
}

bool RHSFMapper::map_inline(Inline *inline_op)
{
  // Get the executing processor
  Processor proc = inline_op->parent_task->current_proc;
  // Get the system memory for it
  Memory target_mem = 
      machine_interface.find_memory_kind(proc, Memory::REGDMA_MEM);
  assert(target_mem.exists());
  inline_op->requirement.target_ranking.push_back(target_mem);
  inline_op->requirement.virtual_map = false;
  inline_op->requirement.enable_WAR_optimization = false;
  inline_op->requirement.reduction_list = false;
  inline_op->requirement.blocking_factor = inline_op->requirement.max_blocking_factor;
  return false;
}

void RHSFMapper::notify_mapping_failed(const Mappable *mappable)
{
  switch (mappable->get_mappable_kind())
  {
    case Mappable::TASK_MAPPABLE:
      {
        Task *task = mappable->as_mappable_task();
        int failed_idx = -1;
        for (unsigned idx = 0; idx < task->regions.size(); idx++)
        {
          if (task->regions[idx].mapping_failed)
          {
            failed_idx = idx;
            break;
          }
        }
        log_mapper.error("Failed task mapping for region %d of "
                               "task %s (%p) on processor " IDFMT " (%s)\n", 
                    failed_idx, task->variants->name, task,
                    task->target_proc.id,
                    (task->target_proc.kind() == Processor::TOC_PROC) ? "GPU" : "CPU"); 
        assert(false);
        break;
      }
    case Mappable::COPY_MAPPABLE:
      {
        Copy *copy = mappable->as_mappable_copy();
        int failed_idx = -1;
        for (unsigned idx = 0; idx < copy->src_requirements.size(); idx++)
        {
          if (copy->src_requirements[idx].mapping_failed)
          {
            failed_idx = idx;
            break;
          }
        }
        for (unsigned idx = 0; idx < copy->dst_requirements.size(); idx++)
        {
          if (copy->dst_requirements[idx].mapping_failed)
          {
            failed_idx = copy->src_requirements.size() + idx;
            break;
          }
        }
        log_mapper.error("Failed copy mapping for region %d of copy (%p)\n", 
                    failed_idx, copy);
        assert(false);
        break;
      }
    default:
      assert(false);
  }
}

bool RHSFMapper::map_must_epoch(const std::vector<Task*> &tasks,
                                const std::vector<MappingConstraint> &constraints,
                                MappingTagID tag)
{
  // For each of our tasks, map our tasks to our registered memory or system
  // memory and then go through and fixup the constraints on the other memories
#ifdef NEW_BARRIER_EXCHANGE
#define NEW_MUST_EPOCH_MAP
#endif
#ifdef NEW_MUST_EPOCH_MAP
  size_t n_tasks = tasks.size();
  //printf("map_must_epoch: %zd tasks\n", n_tasks);
  for(size_t i = 0; i < n_tasks; i++) {
    assert(tasks[i]->task_id == DISTRIBUTE_TASK_ID);
  
    Point<3> p = tasks[i]->index_point.get_point<3>();

    //printf("task %4zd: (%d,%d,%d) proc=" IDFMT " regions=%zd\n",
    //   i, p[0], p[1], p[2], tasks[i]->target_proc.id, tasks[i]->regions.size());

    for(size_t j = 0; j < tasks[i]->regions.size(); j++) {
      RegionRequirement& rr(tasks[i]->regions[j]);
      int tag = rr.tag;

      // default will be to place this data where the task is running
      Point<3> home = p;
      switch(tag) {
      case RHSF_GHOST_PREV_X:
	{
	  if(home.x[0] == proc_grid_bounds.lo[0])
	    home.x[0] = proc_grid_bounds.hi[0];
	  else
	    home.x[0]--;
	  break;
	}

      case RHSF_GHOST_NEXT_X:
	{
	  if(home.x[0] == proc_grid_bounds.hi[0])
	    home.x[0] = proc_grid_bounds.lo[0];
	  else
	    home.x[0]++;
	  break;
	}

      case RHSF_GHOST_PREV_Y:
	{
	  if(home.x[1] == proc_grid_bounds.lo[1])
	    home.x[1] = proc_grid_bounds.hi[1];
	  else
	    home.x[1]--;
	  break;
	}

      case RHSF_GHOST_NEXT_Y:
	{
	  if(home.x[1] == proc_grid_bounds.hi[1])
	    home.x[1] = proc_grid_bounds.lo[1];
	  else
	    home.x[1]++;
	  break;
	}

      case RHSF_GHOST_PREV_Z:
	{
	  if(home.x[2] == proc_grid_bounds.lo[2])
	    home.x[2] = proc_grid_bounds.hi[2];
	  else
	    home.x[2]--;
	  break;
	}

      case RHSF_GHOST_NEXT_Z:
	{
	  if(home.x[2] == proc_grid_bounds.hi[2])
	    home.x[2] = proc_grid_bounds.lo[2];
	  else
	    home.x[2]++;
	  break;
	}

      case RHSF_GHOST_FIRST_NODE:
	{
	  home = proc_grid_bounds.lo;
	  break;
	}

      case RHSF_GHOST_LAST_NODE:
	{
	  home = proc_grid_bounds.hi;
	  break;
	}

      default: break; // no change
      };

      // map a proc grid location to an actual proc
      const std::vector<Processor> &proc_vec = rank_map[DomainPoint::from_point<3>(home)];
      Processor proc = proc_vec[0];

      // favor REGDMA memory on the home proc, if available
      Memory target_mem = machine_interface.find_memory_kind(proc, Memory::REGDMA_MEM);
      if(!target_mem.exists()) {
	Memory target_mem = machine_interface.find_memory_kind(proc, Memory::SYSTEM_MEM);
	assert(target_mem.exists());
      }

      rr.target_ranking.push_back(target_mem);
      rr.blocking_factor = rr.max_blocking_factor;

      // printf(" region %zd: tag=%d target=" IDFMT " current=%zd",
      // 	     j, tag, target_mem.id, rr.current_instances.size());
      // for(std::map<Memory,bool>::const_iterator it = rr.current_instances.begin();
      // 	  it != rr.current_instances.end();
      // 	  it++)
      // 	printf(" " IDFMT "(%d)", it->first.id, it->second);
      // printf("\n");

      if(!rr.current_instances.empty())
	assert(rr.current_instances.count(target_mem));
    }
  }
#else
  for (std::vector<Task*>::const_iterator it = tasks.begin();
        it != tasks.end(); it++)
  {
    Memory target_mem = 
      machine_interface.find_memory_kind((*it)->target_proc, Memory::REGDMA_MEM);
    // If we don't have a REGDMA memory, then put it 
    // in the system memory
    if (!target_mem.exists())
    {
      target_mem = 
        machine_interface.find_memory_kind((*it)->target_proc, Memory::SYSTEM_MEM);
      assert(target_mem.exists());
    }
    for (unsigned idx = 0; idx < (*it)->regions.size(); idx++)
    {
      if (!((*it)->regions[idx].flags & NO_ACCESS_FLAG))
      {
        (*it)->regions[idx].target_ranking.push_back(target_mem);
      }
      // always set the blocking factor
      (*it)->regions[idx].blocking_factor = (*it)->regions[idx].max_blocking_factor;
    }
  }
  // Now go through all the constrains and fix up the difference
  for (std::vector<MappingConstraint>::const_iterator it = constraints.begin();
        it != constraints.end(); it++)
  {
    // Exactly one of these should have been assigned
    if (it->t1->regions[it->idx1].target_ranking.empty())
    {
      assert(!it->t2->regions[it->idx2].target_ranking.empty());
      it->t1->regions[it->idx1].target_ranking = it->t2->regions[it->idx2].target_ranking;
    }
    else
    {
      assert(it->t2->regions[it->idx2].target_ranking.empty());
      it->t2->regions[it->idx2].target_ranking = it->t1->regions[it->idx1].target_ranking;
    }
  }
  // Sanity check that everyone got a target ranking
  for (std::vector<Task*>::const_iterator it = tasks.begin();
        it != tasks.end(); it++)
  {
    for (unsigned idx = 0; idx < (*it)->regions.size(); idx++)
    {
      // If we didn't get an assignement, then this is a wrap around
      // region so we can just map it to our local target
      if ((*it)->regions[idx].target_ranking.empty())
      {
         Memory target_mem = 
          machine_interface.find_memory_kind((*it)->target_proc, Memory::REGDMA_MEM);
        // If we don't have a REGDMA memory, then put it 
        // in the system memory
        if (!target_mem.exists())
        {
          target_mem = 
            machine_interface.find_memory_kind((*it)->target_proc, Memory::SYSTEM_MEM);
          assert(target_mem.exists());
        }   
        (*it)->regions[idx].target_ranking.push_back(target_mem);
      }
    }
  }
#endif
  return false;
}

void RHSFMapper::notify_profiling_info(const Task *task)
{
  log_mapper.info("profile data for %s: %lld %lld (%lld)",
		  task->variants->name,
		  task->start_time, task->stop_time,
		  task->stop_time - task->start_time);
}

int RHSFMapper::get_tunable_value(const Task *task, TunableID tid, MappingTagID tag)
{
  switch (tid)
  {
    case TUNABLE_PROCS_PER_NODE:
      {
        // Assume that this is the same everywhere
        assert(local_cpus.size() > 0);
        return local_cpus.size();
      }
    default:
      assert(false);
  }
  return -1;
}

void RHSFMapper::configure_context(Task *task)
{
  // Only need to do this for the distribute task
  if (task->task_id == DISTRIBUTE_TASK_ID)
  {
    // Make sure we can have at least three frames in flight
    // and be mapping two frames ahead
    task->max_outstanding_frames = 3;
    task->min_frames_to_schedule = 2;
  }
}

