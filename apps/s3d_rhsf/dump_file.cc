#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>

#include "dump_file.h"

DumpFile::DumpFile(const char *_filename)
{
#ifdef USE_ZLIB
  // try filename with .gz on the end first
  char filename_gz[1024];
  sprintf(filename_gz, "%s.gz", _filename);
  f = gzopen(filename_gz, "rb");
  if(!f)
    f = gzopen(_filename, "rb");
#else
  fd = open(_filename, O_RDONLY);
#endif
  cur_pos = 0;
  act_pos = 0;
}

DumpFile::~DumpFile(void)
{
#ifdef USE_ZLIB
  if(f)
    gzclose(f);
#else
  if(fd >= 0)
    close(fd);
#endif
}

DumpedArray *DumpFile::get_next_array(void)
{
  // support up to 8-dim arrays for now
  struct Header {
    int recsize;
    char name[20];
    unsigned rank;
    int dims[8];
  } hdr;

#ifdef USE_ZLIB
  if(!f)
    return 0;
  if(act_pos != gztell(f)) assert(0);
  if(act_pos != cur_pos)
    //printf("gzseek: %d -> %d (%d)\n", act_pos, cur_pos, cur_pos - act_pos);
    gzseek(f, cur_pos, SEEK_SET);
  ssize_t count = gzread(f, &hdr, sizeof(hdr));
#else
  if(fd < 0)
    return 0;
  lseek(fd, cur_pos, SEEK_SET);
  ssize_t count = read(fd, &hdr, sizeof(hdr));
#endif
  if(count == 0) return 0; // eof
  assert(count >= 32); // otherwise need at least enough data for 1-d
  act_pos = cur_pos + count;

  DumpedArray *da = new DumpedArray(this,
				    hdr.name, hdr.rank, hdr.dims);

  if(hdr.recsize > 0) {
    // normal < 2GB record
    da->add_segment(cur_pos + sizeof(int), hdr.recsize);
    cur_pos += 2*sizeof(int); // record size appears at start and end of record
    cur_pos += hdr.recsize;
  } else {
    // negative value indicates >= 2GB record split into pieces
    int rawsize = hdr.recsize;
    while(rawsize < 0) {
      int actsize = -rawsize;
      da->add_segment(cur_pos + sizeof(int), actsize);
      cur_pos += 2*sizeof(int); // record size appears at start and end of record
      cur_pos += actsize;
#ifdef USE_ZLIB
      if(cur_pos != act_pos) {
	//printf("gzseek: %d -> %d (%d)\n", act_pos, cur_pos, cur_pos - act_pos);
	gzseek(f, cur_pos, SEEK_SET);
      }
      count = gzread(f, &rawsize, sizeof(int));
#else
      lseek(fd, cur_pos, SEEK_SET);
      count = read(fd, &rawsize, sizeof(int));
#endif
      assert(count == sizeof(int));
      act_pos = cur_pos + sizeof(int);
    }

    // last piece has a positive size
    da->add_segment(cur_pos + sizeof(int), rawsize);
    cur_pos += 2*sizeof(int); // record size appears at start and end of record
    cur_pos += rawsize;
  }

  return da;
}

void DumpFile::load_bytes(void *dst, off_t offset, size_t bytes)
{
  char *dst_c = (char *)dst;

  if(act_pos != offset) {
#ifdef USE_ZLIB
    //printf("gzseek: %d -> %d (%d)\n", act_pos, offset, offset - act_pos);
    gzseek(f, offset, SEEK_SET);
#else
    lseek(fd, offset, SEEK_SET);
#endif
    act_pos = offset;
  }

  size_t done = 0;
  while(done < bytes) {
#ifdef USE_ZLIB
    ssize_t count = gzread(f, dst_c + done, bytes - done);
#else
    ssize_t count = read(fd, dst_c + done, bytes - done);
#endif
    assert(count > 0);
    done += count;
    act_pos += count;
  }
}
