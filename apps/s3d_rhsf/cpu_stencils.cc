#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <assert.h>

// instructions for non-temporal loads
#ifndef MOVNTDQA
#define MOVNTDQA "movntdqa"
#endif

#ifndef VMOVNTDQA
#ifdef HAVE_256BIT_VMOVNTDQA
#define VMOVNTDQA "vmovntdqa"
#else
#define VMOVNTDQA "vmovdqa"
#endif
#endif

// best I can tell, movntdq is worse than movapd if you're touching the 
//  whole cache line quickly
#ifndef MOVNTDQ
#define MOVNTDQ "movapd"
//define MOVNTDQ "movntdq"
#endif

#ifndef VMOVNTDQ
//define VMOVNTDQ "vmovapd"
#define VMOVNTDQ "vmovntdq"
#endif

template <typename T>
static inline void add_offset(T *& ptr, off_t offset)
{
  ptr = (T *)(((char *)(ptr)) + offset);
}

void slow_cpu_stencil(int dim_1, int dim_2, int dim_3, double ds,
		      const double *minus_ptr, size_t minus_stride_1, size_t minus_stride_2, size_t minus_stride_3,
		      const double *in_ptr, size_t in_stride_1, size_t in_stride_2, size_t in_stride_3,
		      const double *plus_ptr, size_t plus_stride_1, size_t plus_stride_2, size_t plus_stride_3,
		      double *out_ptr, size_t out_stride_1, size_t out_stride_2, size_t out_stride_3)
{
  double ae = 4.0 / 5.0 * ds;
  double be = -1.0 / 5.0 * ds;
  double ce = 4.0 / 105.0 * ds;
  double de = -1.0 / 280.0 * ds;

  for(int z = 0; z < dim_3; z++) {
    const double *minus_y = minus_ptr;
    const double *in_y = in_ptr;
    const double *plus_y = plus_ptr;
    double *out_y = out_ptr;

    for(int y = 0; y < dim_2; y++) {
      const double *minus_x = minus_y;
      const double *in_x = in_y;
      const double *plus_x = plus_y;
      double *out_x = out_y;

      // pencil in "x" direction
      {
	double window[9];

	// pre-fill window
	for(int i = 0; i < 4; i++) {
	  window[i] = *minus_x;
	  add_offset(minus_x, minus_stride_1);
	}

	for(int i = 0; i < 4; i++) {
	  window[i+4] = *in_x;
	  add_offset(in_x, in_stride_1);
	}

	// main loop
	for(int i = 0; i < dim_1 - 4; i++) {
	  window[8] = *in_x;
	  add_offset(in_x, in_stride_1);

	  double deriv = (((ae * (window[5] - window[3])
			    + be * (window[6] - window[2]))
			   + ce * (window[7] - window[1]))
			  + de * (window[8] - window[0]));
	      
	  if(0 && (y == 0) && (z == 0)) {
	    printf("%g %g %g %g %g %g %g %g %g -> %g\n",
		   window[0], window[1], window[2], window[3],
		   window[4], window[5], window[6], window[7],
		   window[8], deriv);
	  }
	  *out_x = deriv; add_offset(out_x, out_stride_1);

	  for(int j = 0; j < 8; j++)
	    window[j] = window[j + 1];
	}

	// final bit with plus data
	for(int i = 0; i < 4; i++) {
	  window[8] = *plus_x;
	  add_offset(plus_x, plus_stride_1);

	  double deriv = (((ae * (window[5] - window[3])
			    + be * (window[6] - window[2]))
			   + ce * (window[7] - window[1]))
			  + de * (window[8] - window[0]));
	      
	  if(0 && (y == 0) && (z == 0)) {
	    printf("%g %g %g %g %g %g %g %g %g -> %g\n",
		   window[0], window[1], window[2], window[3],
		   window[4], window[5], window[6], window[7],
		   window[8], deriv);
	  }
	  *out_x = deriv; add_offset(out_x, out_stride_1);

	  for(int j = 0; j < 8; j++)
	    window[j] = window[j + 1];
	}
      }

      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);
    }

    add_offset(minus_ptr, minus_stride_3);
    add_offset(in_ptr, in_stride_3);
    add_offset(plus_ptr, plus_stride_3);
    add_offset(out_ptr, out_stride_3);
  }
}

#define REGSTENCIL_SHIFT(r0, r1, r2, r3, r4, r5, r6, r7, ldexp, stexp)	\
  /* ae * (window[5] - window[3]) */			 \
  "movapd %%xmm" #r3 ", %%xmm9;"				 \
  "subpd %%xmm" #r5 ", %%xmm9;"				 \
  "mulpd %%xmm12, %%xmm9;"				 \
  /* be * (window[6] - window[2]) */			 \
  "movapd %%xmm" #r2 ", %%xmm8;"				 \
  "subpd %%xmm" #r6 ", %%xmm8;"				 \
  "mulpd %%xmm13, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* ce * (window[7] - window[1]) */			 \
  "movapd %%xmm" #r1 ", %%xmm8;"				 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm14, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* de * (window[8] - window[0]) */			 \
  "movapd %%xmm" #r0 ", %%xmm8;"				 \
  /* manually move registers for now - rotate later */	 \
  "movapd %%xmm" #r1 ", %%xmm" #r0 ";"				 \
  "movapd %%xmm" #r2 ", %%xmm" #r1 ";"			 \
  "movapd %%xmm" #r3 ", %%xmm" #r2 ";"				 \
  "movapd %%xmm" #r4 ", %%xmm" #r3 ";"				 \
  "movapd %%xmm" #r5 ", %%xmm" #r4 ";"				 \
  "movapd %%xmm" #r6 ", %%xmm" #r5 ";"				 \
  "movapd %%xmm" #r7 ", %%xmm" #r6 ";"				 \
  "movddup " ldexp ", %%xmm" #r7 ";"				 \
  /*  bleah */						 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm15, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  "movq %%xmm9, " stexp ";"

#define REGSTENCIL_ROTATE(r0, r1, r2, r3, r4, r5, r6, r7, ldexp, stexp)	\
  /* ae * (window[5] - window[3]) */			 \
  "movapd %%xmm" #r3 ", %%xmm9;"				 \
  "subpd %%xmm" #r5 ", %%xmm9;"				 \
  "mulpd %%xmm12, %%xmm9;"				 \
  /* be * (window[6] - window[2]) */			 \
  "movapd %%xmm" #r2 ", %%xmm8;"				 \
  "subpd %%xmm" #r6 ", %%xmm8;"				 \
  "mulpd %%xmm13, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* ce * (window[7] - window[1]) */			 \
  "movapd %%xmm" #r1 ", %%xmm8;"				 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm14, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* de * (window[8] - window[0]) */			 \
  "movapd %%xmm" #r0 ", %%xmm8;"				 \
  "movddup " ldexp ", %%xmm" #r0 ";"				 \
  "subpd %%xmm" #r0 ", %%xmm8;"				 \
  "mulpd %%xmm15, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  "movlpd %%xmm0, " stexp ";"

// first 5 values are in r0, r1, r2, r3, r4
// reads next value
// interpolates r4 and new value into r5, r6, r7, leaving new value in r0
// calculates differences, destroying ymm8, ymm9, ymm10, ymm11
// stores result
#define REGSTENCIL_ROTATE_1X4_AVX(r0, r1, r2, r3, r4, r5, r6, r7, ldexp, stexp) \
  "vmovapd " ldexp ", %%ymm8;"  \
  "vperm2f128 $33, %%ymm8, %%ymm"#r4", %%ymm"#r6";" \
  "vshufpd    $5, %%ymm"#r6", %%ymm"#r4", %%ymm"#r5";"  \
  "vshufpd    $5, %%ymm8, %%ymm"#r6", %%ymm"#r7";"  \
  "vsubpd     %%ymm8, %%ymm"#r0", %%ymm11;" \
  "vmovapd    %%ymm8, %%ymm"#r0";" \
  "vsubpd     %%ymm"#r7", %%ymm"#r1", %%ymm10;" \
  "vsubpd     %%ymm"#r6", %%ymm"#r2", %%ymm9;" \
  "vsubpd     %%ymm"#r5", %%ymm"#r3", %%ymm8;" \
  "vmulpd     %%ymm15, %%ymm11, %%ymm11;" \
  "vmulpd     %%ymm14, %%ymm10, %%ymm10;" \
  "vmulpd     %%ymm13, %%ymm9, %%ymm9;" \
  "vmulpd     %%ymm12, %%ymm8, %%ymm8;" \
  "vaddpd     %%ymm9, %%ymm8, %%ymm8;" \
  "vaddpd     %%ymm10, %%ymm8, %%ymm8;" \
  "vaddpd     %%ymm11, %%ymm8, %%ymm8;" \
  VMOVNTDQ  " %%ymm8, " stexp ";"
#define REGSTENCIL_ROTATE_1X4_FMA4(r0, r1, r2, r3, r4, r5, r6, r7, ldexp, stexp) \
  "vmovapd " ldexp ", %%ymm8;"  \
  "vperm2f128 $33, %%ymm8, %%ymm"#r4", %%ymm"#r6";" \
  "vshufpd    $5, %%ymm"#r6", %%ymm"#r4", %%ymm"#r5";"  \
  "vshufpd    $5, %%ymm8, %%ymm"#r6", %%ymm"#r7";"  \
  "vsubpd     %%ymm8, %%ymm"#r0", %%ymm11;" \
  "vmovapd    %%ymm8, %%ymm"#r0";" \
  "vsubpd     %%ymm"#r5", %%ymm"#r3", %%ymm8;" \
  "vsubpd     %%ymm"#r6", %%ymm"#r2", %%ymm9;" \
  "vsubpd     %%ymm"#r7", %%ymm"#r1", %%ymm10;" \
  "vmulpd     %%ymm12, %%ymm8, %%ymm8;" \
  "vfmaddpd   %%ymm8, %%ymm13, %%ymm9, %%ymm8;" \
  "vfmaddpd   %%ymm8, %%ymm14, %%ymm10, %%ymm8;" \
  "vfmaddpd   %%ymm8, %%ymm15, %%ymm11, %%ymm8;" \
  VMOVNTDQ  " %%ymm8, " stexp ";"

#define REGSTENCIL_ROTATE_1X2(r0, r1, r2, r3, r4, r5, r6, r7, ldexp, stexp)	\
  "movapd " ldexp ", %%xmm10;"						\
  "movapd %%xmm" #r6 ", %%xmm" #r7 ";"					\
  "shufpd $1, %%xmm10, %%xmm" #r7 ";"					\
  /* ae * (window[5] - window[3]) */			 \
  "movapd %%xmm" #r3 ", %%xmm9;"				 \
  "subpd %%xmm" #r5 ", %%xmm9;"				 \
  "mulpd %%xmm12, %%xmm9;"				 \
  /* be * (window[6] - window[2]) */			 \
  "movapd %%xmm" #r2 ", %%xmm8;"				 \
  "subpd %%xmm" #r6 ", %%xmm8;"				 \
  "mulpd %%xmm13, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* ce * (window[7] - window[1]) */			 \
  "movapd %%xmm" #r1 ", %%xmm8;"				 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm14, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* de * (window[8] - window[0]) */			 \
  "movapd %%xmm" #r0 ", %%xmm8;"				 \
  "subpd %%xmm10, %%xmm8;"				 \
  "movapd %%xmm10, %%xmm" #r0 ";"				 \
  "mulpd %%xmm15, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  MOVNTDQ " %%xmm9, " stexp ";"

#define REGSTENCIL_ROTATE_2X1(r0, r1, r2, r3, r4, r5, r6, r7, ldexp1, ldexp2, stexp1, stexp2) \
  /* ae * (window[5] - window[3]) */			 \
  "movapd %%xmm" #r3 ", %%xmm9;"				 \
  "subpd %%xmm" #r5 ", %%xmm9;"				 \
  "mulpd %%xmm12, %%xmm9;"				 \
  /* be * (window[6] - window[2]) */			 \
  "movapd %%xmm" #r2 ", %%xmm8;"				 \
  "subpd %%xmm" #r6 ", %%xmm8;"				 \
  "mulpd %%xmm13, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* ce * (window[7] - window[1]) */			 \
  "movapd %%xmm" #r1 ", %%xmm8;"				 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm14, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* de * (window[8] - window[0]) */			 \
  "movapd %%xmm" #r0 ", %%xmm8;"				 \
  "movsd " ldexp1 ", %%xmm" #r0 ";"				 \
  "movhpd " ldexp2 ", %%xmm" #r0 ";"				 \
  "subpd %%xmm" #r0 ", %%xmm8;"				 \
  "mulpd %%xmm15, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  "movlpd %%xmm9, " stexp1 ";"				 \
  "movhpd %%xmm9, " stexp2 ";"

#define REGSTENCIL_ROTATE_2X2(r0, r1, r2, r3, r4, r5, r6, r7, ldexp1, ldexp2, stexp1, stexp2) \
  /* ae * (window[5] - window[3]) */			 \
  "movapd %%xmm" #r3 ", %%xmm9;"				 \
  "subpd %%xmm" #r5 ", %%xmm9;"				 \
  "mulpd %%xmm12, %%xmm9;"				 \
  /* be * (window[6] - window[2]) */			 \
  "movapd %%xmm" #r2 ", %%xmm8;"				 \
  "subpd %%xmm" #r6 ", %%xmm8;"				 \
  "mulpd %%xmm13, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* ce * (window[7] - window[1]) */			 \
  "movapd %%xmm" #r1 ", %%xmm8;"				 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm14, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* de * (window[8] - window[0]) */			 \
  "movapd %%xmm" #r0 ", %%xmm8;"				 \
  "movapd " ldexp1 ", %%xmm" #r0 ";" \
  "movapd %%xmm" #r0 ", %%xmm" #r1 ";" \
  "unpcklpd " ldexp2 ", %%xmm" #r0 ";" \
  "unpckhpd " ldexp2 ", %%xmm" #r1 ";" \
  "subpd %%xmm" #r0 ", %%xmm8;"				 \
  "mulpd %%xmm15, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  MOVNTDQ " %%xmm9, " stexp1 ";"				 \
  MOVNTDQ " %%xmm8, " stexp2 ";"

// unused
#if 0
static void stencil_x_row_shift(const double *minus, const double *in, const double *plus,
			 const double *weights, double *out, size_t count)
{
  asm volatile (
// fetch weights into registers
"movddup 0(%3), %%xmm12;"
"movddup 8(%3), %%xmm13;"
"movddup 16(%3), %%xmm14;"
"movddup 24(%3), %%xmm15;"
// preload first eight values
"movddup 0(%0), %%xmm0;"
"movddup 8(%0), %%xmm1;"
"movddup 16(%0), %%xmm2;"
"movddup 24(%0), %%xmm3;"
"movddup 0(%1), %%xmm4;"
"movddup 8(%1), %%xmm5;"
"movddup 16(%1), %%xmm6;"
"movddup 24(%1), %%xmm7;"
"addq $32, %1;"
"movq %5, %%rcx;"
"subq $4, %%rcx;"
"1:"
REGSTENCIL_SHIFT(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%4)")
"addq $8, %1;"
"addq $8, %4;"
"subq $1, %%rcx;"
"jne 1b;"
REGSTENCIL_SHIFT(0, 1, 2, 3, 4, 5, 6, 7, "0(%2)", "0(%4)")
REGSTENCIL_SHIFT(0, 1, 2, 3, 4, 5, 6, 7, "8(%2)", "8(%4)")
REGSTENCIL_SHIFT(0, 1, 2, 3, 4, 5, 6, 7, "16(%2)", "16(%4)")
REGSTENCIL_SHIFT(0, 1, 2, 3, 4, 5, 6, 7, "24(%2)", "24(%4)")
: "+r" (minus), "+r" (in), "+r" (plus), "+r" (weights),
  "+r" (out)
: "r" (count)
: "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

// also unused
#if 0
static void stencil_x_row_rotate(const double *minus, const double *in, const double *plus,
			 const double *weights, double *out, size_t count)
{
  asm volatile (
// fetch weights into registers
"movddup 0(%3), %%xmm12;"
"movddup 8(%3), %%xmm13;"
"movddup 16(%3), %%xmm14;"
"movddup 24(%3), %%xmm15;"
// preload first eight values
"movddup 0(%0), %%xmm0;"
"movddup 8(%0), %%xmm1;"
"movddup 16(%0), %%xmm2;"
"movddup 24(%0), %%xmm3;"
"movddup 0(%1), %%xmm4;"
"movddup 8(%1), %%xmm5;"
"movddup 16(%1), %%xmm6;"
"movddup 24(%1), %%xmm7;"
"addq $32, %1;"
"movq %5, %%rcx;"
"1:"
REGSTENCIL_ROTATE(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%4)")
REGSTENCIL_ROTATE(1, 2, 3, 4, 5, 6, 7, 0, "8(%1)", "8(%4)")
REGSTENCIL_ROTATE(2, 3, 4, 5, 6, 7, 0, 1, "16(%1)", "16(%4)")
REGSTENCIL_ROTATE(3, 4, 5, 6, 7, 0, 1, 2, "24(%1)", "24(%4)")
"subq $8, %%rcx;"
"je 2f;"
REGSTENCIL_ROTATE(4, 5, 6, 7, 0, 1, 2, 3, "32(%1)", "32(%4)")
REGSTENCIL_ROTATE(5, 6, 7, 0, 1, 2, 3, 4, "40(%1)", "40(%4)")
REGSTENCIL_ROTATE(6, 7, 0, 1, 2, 3 ,4, 5, "48(%1)", "48(%4)")
REGSTENCIL_ROTATE(7, 0, 1, 2, 3, 4, 5, 6, "56(%1)", "56(%4)")
"addq $64, %1;"
"addq $64, %4;"
"jmp 1b;"
"2:"
REGSTENCIL_ROTATE(4, 5, 6, 7, 0, 1, 2, 3, "0(%2)", "32(%4)")
REGSTENCIL_ROTATE(5, 6, 7, 0, 1, 2, 3, 4, "8(%2)", "40(%4)")
REGSTENCIL_ROTATE(6, 7, 0, 1, 2, 3 ,4, 5, "16(%2)", "48(%4)")
REGSTENCIL_ROTATE(7, 0, 1, 2, 3, 4, 5, 6, "24(%2)", "56(%4)")
: "+r" (minus), "+r" (in), "+r" (plus), "+r" (weights),
  "+r" (out)
: "r" (count)
: "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

#ifdef USE_FMA4_KERNELS
static void stencil_x_row_rotate4_fma4(const double *minus, const double *in, const double *plus,
				       const double *weights, double *out, size_t count)
{
  asm volatile (
// fetch weights into registers
"vbroadcastsd 0(%3), %%ymm12;"
"vbroadcastsd 8(%3), %%ymm13;"
"vbroadcastsd 16(%3), %%ymm14;"
"vbroadcastsd 24(%3), %%ymm15;"
// preload first four pairs
"vmovapd 0(%0), %%ymm0;"
"vmovapd 0(%1), %%ymm4;"
// shuffles to get intermediate pairs
"vperm2f128 $33, %%ymm4, %%ymm0, %%ymm2;"
"vshufpd    $5, %%ymm2, %%ymm0, %%ymm1;"
"vshufpd    $5, %%ymm4, %%ymm2, %%ymm3;"
"addq $32, %1;"
"movq %5, %%rcx;"
"1:"
REGSTENCIL_ROTATE_1X4_FMA4(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%4)")
"subq $8, %%rcx;"
"je 2f;"
REGSTENCIL_ROTATE_1X4_FMA4(4, 5, 6, 7, 0, 1, 2, 3, "32(%1)", "32(%4)")
"addq $64, %1;"
"addq $64, %4;"
"jmp 1b;"
"2:"
REGSTENCIL_ROTATE_1X4_FMA4(4, 5, 6, 7, 0, 1, 2, 3, "0(%2)", "32(%4)")
: "+r" (minus), "+r" (in), "+r" (plus), "+r" (weights),
  "+r" (out)
: "r" (count)
: "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

#ifdef USE_AVX_KERNELS
static void stencil_x_row_rotate4_avx(const double *minus, const double *in, const double *plus,
				      const double *weights, double *out, size_t count)
{
  asm volatile (
// fetch weights into registers
"vbroadcastsd 0(%3), %%ymm12;"
"vbroadcastsd 8(%3), %%ymm13;"
"vbroadcastsd 16(%3), %%ymm14;"
"vbroadcastsd 24(%3), %%ymm15;"
// preload first four pairs
"vmovapd 0(%0), %%ymm0;"
"vmovapd 0(%1), %%ymm4;"
// shuffles to get intermediate pairs
"vperm2f128 $33, %%ymm4, %%ymm0, %%ymm2;"
"vshufpd    $5, %%ymm2, %%ymm0, %%ymm1;"
"vshufpd    $5, %%ymm4, %%ymm2, %%ymm3;"
"addq $32, %1;"
"movq %5, %%rcx;"
"1:"
REGSTENCIL_ROTATE_1X4_AVX(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%4)")
"subq $8, %%rcx;"
"je 2f;"
REGSTENCIL_ROTATE_1X4_AVX(4, 5, 6, 7, 0, 1, 2, 3, "32(%1)", "32(%4)")
"addq $64, %1;"
"addq $64, %4;"
"jmp 1b;"
"2:"
REGSTENCIL_ROTATE_1X4_AVX(4, 5, 6, 7, 0, 1, 2, 3, "0(%2)", "32(%4)")
: "+r" (minus), "+r" (in), "+r" (plus), "+r" (weights),
  "+r" (out)
: "r" (count)
: "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

#if !defined(USE_AVX_KERNELS)
static void stencil_x_row_rotate2_sse(const double *minus, const double *in, const double *plus,
				      const double *weights, double *out, size_t count)
{
  asm volatile (
// fetch weights into registers
"movddup 0(%3), %%xmm12;"
"movddup 8(%3), %%xmm13;"
"movddup 16(%3), %%xmm14;"
"movddup 24(%3), %%xmm15;"
// preload first four pairs
"movapd 0(%0), %%xmm0;"
"movapd 16(%0), %%xmm2;"
"movapd 0(%1), %%xmm4;"
"movapd 16(%1), %%xmm6;"
// shuffles to get intermediate pairs
"movapd %%xmm0, %%xmm1;"
"shufpd $1, %%xmm2, %%xmm1;"
"movapd %%xmm2, %%xmm3;"
"shufpd $1, %%xmm4, %%xmm3;"
"movapd %%xmm4, %%xmm5;"
"shufpd $1, %%xmm6, %%xmm5;"
"addq $32, %1;"
"movq %5, %%rcx;"
"1:"
REGSTENCIL_ROTATE_1X2(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%4)")
REGSTENCIL_ROTATE_1X2(2, 3, 4, 5, 6, 7, 0, 1, "16(%1)", "16(%4)")
"subq $8, %%rcx;"
"je 2f;"
REGSTENCIL_ROTATE_1X2(4, 5, 6, 7, 0, 1, 2, 3, "32(%1)", "32(%4)")
REGSTENCIL_ROTATE_1X2(6, 7, 0, 1, 2, 3 ,4, 5, "48(%1)", "48(%4)")
"addq $64, %1;"
"addq $64, %4;"
"jmp 1b;"
"2:"
REGSTENCIL_ROTATE_1X2(4, 5, 6, 7, 0, 1, 2, 3, "0(%2)", "32(%4)")
REGSTENCIL_ROTATE_1X2(6, 7, 0, 1, 2, 3 ,4, 5, "16(%2)", "48(%4)")
: "+r" (minus), "+r" (in), "+r" (plus), "+r" (weights),
  "+r" (out)
: "r" (count)
: "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

// also also unused
#if 0
static void stencil_x_row2_rotate(const double *minus1, const double *in1, const double *plus1, double *out1,
			   const double *minus2, const double *in2, const double *plus2, double *out2,
			   const double *weights, size_t count)
{
  asm volatile (
// fetch weights into registers
"movddup 0(%8), %%xmm12;"
"movddup 8(%8), %%xmm13;"
"movddup 16(%8), %%xmm14;"
"movddup 24(%8), %%xmm15;"
// preload first eight values
"movsd 0(%0), %%xmm0;"
"movsd 8(%0), %%xmm1;"
"movsd 16(%0), %%xmm2;"
"movsd 24(%0), %%xmm3;"
"movsd 0(%1), %%xmm4;"
"movsd 8(%1), %%xmm5;"
"movsd 16(%1), %%xmm6;"
"movsd 24(%1), %%xmm7;"
"movhpd 0(%4), %%xmm0;"
"movhpd 8(%4), %%xmm1;"
"movhpd 16(%4), %%xmm2;"
"movhpd 24(%4), %%xmm3;"
"movhpd 0(%5), %%xmm4;"
"movhpd 8(%5), %%xmm5;"
"movhpd 16(%5), %%xmm6;"
"movhpd 24(%5), %%xmm7;"
"addq $32, %1;"
"addq $32, %5;"
"movq %9, %%rcx;"
"1:"
#define ROTATE_2X2
#ifdef ROTATE_2X2
REGSTENCIL_ROTATE_2X2(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%5)", "0(%3)", "0(%7)")
REGSTENCIL_ROTATE_2X2(2, 3, 4, 5, 6, 7, 0, 1, "16(%1)", "16(%5)", "16(%3)", "16(%7)")
#else
REGSTENCIL_ROTATE_2X1(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%5)", "0(%3)", "0(%7)")
REGSTENCIL_ROTATE_2X1(1, 2, 3, 4, 5, 6, 7, 0, "8(%1)", "8(%5)", "8(%3)", "8(%7)")
REGSTENCIL_ROTATE_2X1(2, 3, 4, 5, 6, 7, 0, 1, "16(%1)", "16(%5)", "16(%3)", "16(%7)")
REGSTENCIL_ROTATE_2X1(3, 4, 5, 6, 7, 0, 1, 2, "24(%1)", "24(%5)", "24(%3)", "24(%7)")
#endif
"subq $8, %%rcx;"
"je 2f;"
#ifdef ROTATE_2X2
REGSTENCIL_ROTATE_2X2(4, 5, 6, 7, 0, 1, 2, 3, "32(%1)", "32(%5)", "32(%3)", "32(%7)")
REGSTENCIL_ROTATE_2X2(6, 7, 0, 1, 2, 3 ,4, 5, "48(%1)", "48(%5)", "48(%3)", "48(%7)")
#else
REGSTENCIL_ROTATE_2X1(4, 5, 6, 7, 0, 1, 2, 3, "32(%1)", "32(%5)", "32(%3)", "32(%7)")
REGSTENCIL_ROTATE_2X1(5, 6, 7, 0, 1, 2, 3, 4, "40(%1)", "40(%5)", "40(%3)", "40(%7)")
REGSTENCIL_ROTATE_2X1(6, 7, 0, 1, 2, 3 ,4, 5, "48(%1)", "48(%5)", "48(%3)", "48(%7)")
REGSTENCIL_ROTATE_2X1(7, 0, 1, 2, 3, 4, 5, 6, "56(%1)", "56(%5)", "56(%3)", "56(%7)")
#endif
"addq $64, %1;"
"addq $64, %3;"
"addq $64, %5;"
"addq $64, %7;"
"jmp 1b;"
"2:"
#ifdef ROTATE_2X2
REGSTENCIL_ROTATE_2X2(4, 5, 6, 7, 0, 1, 2, 3, "0(%2)", "0(%6)", "32(%3)", "32(%7)")
REGSTENCIL_ROTATE_2X2(6, 7, 0, 1, 2, 3 ,4, 5, "16(%2)", "16(%6)", "48(%3)", "48(%7)")
#else
REGSTENCIL_ROTATE_2X1(4, 5, 6, 7, 0, 1, 2, 3, "0(%2)", "0(%6)", "32(%3)", "32(%7)")
REGSTENCIL_ROTATE_2X1(5, 6, 7, 0, 1, 2, 3, 4, "8(%2)", "8(%6)", "40(%3)", "40(%7)")
REGSTENCIL_ROTATE_2X1(6, 7, 0, 1, 2, 3 ,4, 5, "16(%2)", "16(%6)", "48(%3)", "48(%7)")
REGSTENCIL_ROTATE_2X1(7, 0, 1, 2, 3, 4, 5, 6, "24(%2)", "24(%6)", "56(%3)", "56(%7)")
#endif
: "+r" (minus1), "+r" (in1), "+r" (plus1), "+r" (out1),
  "+r" (minus2), "+r" (in2), "+r" (plus2), "+r" (out2),
  "+r" (weights)
: "r" (count)
: "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

static void stencil_x(size_t dim_1, size_t dim_2, size_t dim_3,
		      const double *minus_ptr, size_t minus_stride_2, size_t minus_stride_3,
		      const double *in_ptr, size_t in_stride_2, size_t in_stride_3,
		      const double *plus_ptr, size_t plus_stride_2, size_t plus_stride_3,
		      double *out_ptr, size_t out_stride_2, size_t out_stride_3,
		      double ae, double be, double ce, double de)
{
  double weights[12] = { -ae, -be, -ce, -de,
			 -ae, -ae, // these are in case we need pre-vectorized versions
			 -be, -be,
			 -ce, -ce,
			 -de, -de };

  for(size_t z = 0; z < dim_3; z++) {
    const double *minus_y = minus_ptr;
    const double *in_y = in_ptr;
    const double *plus_y = plus_ptr;
    double *out_y = out_ptr;

    for(size_t y = 0; y < dim_2; y++) {
      const double *minus_x = minus_y;
      const double *in_x = in_y;
      const double *plus_x = plus_y;
      double *out_x = out_y;

#if 1
#ifdef USE_FMA4_KERNELS
      stencil_x_row_rotate4_fma4(minus_x, in_x, plus_x, weights, out_x, dim_1);
#else
#ifdef USE_AVX_KERNELS
      stencil_x_row_rotate4_avx(minus_x, in_x, plus_x, weights, out_x, dim_1);
#else
      stencil_x_row_rotate2_sse(minus_x, in_x, plus_x, weights, out_x, dim_1);
#endif
#endif

      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);
#else
      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);

      stencil_x_row2_rotate(minus_x, in_x, plus_x, out_x,
			    minus_y, in_y, plus_y, out_y,
			    weights, dim_1);
      y++;

      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);
#endif
    }

    add_offset(minus_ptr, minus_stride_3);
    add_offset(in_ptr, in_stride_3);
    add_offset(plus_ptr, plus_stride_3);
    add_offset(out_ptr, out_stride_3);
  }
}

#ifdef USE_FMA4_KERNELS
static void stencil_y_row_fma4(const double *in0, const double *in1, const double *in2, const double *in3,
			       const double *in4, const double *in5, const double *in6, const double *in7,
			       const double *in8, const double *weights, double *out, size_t count)
{
  asm volatile (
"xorq %%rax, %%rax;"
"movq %11, %%rcx;"
"vbroadcastsd 24(%9), %%ymm12;"
"vbroadcastsd 16(%9), %%ymm13;"
"vbroadcastsd 8(%9), %%ymm14;"
"vbroadcastsd 0(%9), %%ymm15;"
"1:"
"vmovapd 0(%%rax,%3), %%ymm0;"
"vmovapd 32(%%rax,%3), %%ymm1;"
"vmovapd 64(%%rax,%3), %%ymm2;"
"vmovapd 96(%%rax,%3), %%ymm3;"
"vsubpd 0(%%rax,%5), %%ymm0, %%ymm0;"
"vsubpd 32(%%rax,%5), %%ymm1, %%ymm1;"
"vsubpd 64(%%rax,%5), %%ymm2, %%ymm2;"
"vsubpd 96(%%rax,%5), %%ymm3, %%ymm3;"
"vmulpd %%ymm0, %%ymm15, %%ymm6;"
"vmulpd %%ymm1, %%ymm15, %%ymm7;"
"vmulpd %%ymm2, %%ymm15, %%ymm8;"
"vmulpd %%ymm3, %%ymm15, %%ymm9;"
"vmovapd 0(%%rax,%2), %%ymm0;"
"vmovapd 32(%%rax,%2), %%ymm1;"
"vmovapd 64(%%rax,%2), %%ymm2;"
"vmovapd 96(%%rax,%2), %%ymm3;"
"vsubpd 0(%%rax,%6), %%ymm0, %%ymm0;"
"vsubpd 32(%%rax,%6), %%ymm1, %%ymm1;"
"vsubpd 64(%%rax,%6), %%ymm2, %%ymm2;"
"vsubpd 96(%%rax,%6), %%ymm3, %%ymm3;"
"vfmaddpd %%ymm6, %%ymm0, %%ymm14, %%ymm6;"
"vfmaddpd %%ymm7, %%ymm1, %%ymm14, %%ymm7;"
"vfmaddpd %%ymm8, %%ymm2, %%ymm14, %%ymm8;"
"vfmaddpd %%ymm9, %%ymm3, %%ymm14, %%ymm9;"
"vmovapd 0(%%rax,%1), %%ymm0;"
"vmovapd 32(%%rax,%1), %%ymm1;"
"vmovapd 64(%%rax,%1), %%ymm2;"
"vmovapd 96(%%rax,%1), %%ymm3;"
"vsubpd 0(%%rax,%7), %%ymm0, %%ymm0;"
"vsubpd 32(%%rax,%7), %%ymm1, %%ymm1;"
"vsubpd 64(%%rax,%7), %%ymm2, %%ymm2;"
"vsubpd 96(%%rax,%7), %%ymm3, %%ymm3;"
"vfmaddpd %%ymm6, %%ymm0, %%ymm13, %%ymm6;"
"vfmaddpd %%ymm7, %%ymm1, %%ymm13, %%ymm7;"
"vfmaddpd %%ymm8, %%ymm2, %%ymm13, %%ymm8;"
"vfmaddpd %%ymm9, %%ymm3, %%ymm13, %%ymm9;"
"vmovapd 0(%%rax,%0), %%ymm0;"
"vmovapd 32(%%rax,%0), %%ymm1;"
"vmovapd 64(%%rax,%0), %%ymm2;"
"vmovapd 96(%%rax,%0), %%ymm3;"
"vsubpd 0(%%rax,%8), %%ymm0, %%ymm0;"
"vsubpd 32(%%rax,%8), %%ymm1, %%ymm1;"
"vsubpd 64(%%rax,%8), %%ymm2, %%ymm2;"
"vsubpd 96(%%rax,%8), %%ymm3, %%ymm3;"
"vfmaddpd %%ymm6, %%ymm0, %%ymm12, %%ymm6;"
"vfmaddpd %%ymm7, %%ymm1, %%ymm12, %%ymm7;"
"vfmaddpd %%ymm8, %%ymm2, %%ymm12, %%ymm8;"
"vfmaddpd %%ymm9, %%ymm3, %%ymm12, %%ymm9;"
VMOVNTDQ " %%ymm6, 0(%%rax,%10);"
VMOVNTDQ " %%ymm7, 32(%%rax,%10);"
VMOVNTDQ " %%ymm8, 64(%%rax,%10);"
VMOVNTDQ " %%ymm9, 96(%%rax,%10);"
"addq $128, %%rax;"
"decq %%rcx;"
"jne 1b;"
: /* no outputs */
: "r" (in0), "r" (in1), "r" (in2), "r" (in3), "r" (in4), "r" (in5), "r" (in6), "r" (in7), "r" (in8),
  "r" (weights), "r" (out), "r" (count)
: "rax", "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

#ifdef USE_AVX_KERNELS
static void stencil_y_row_avx(const double *in0, const double *in1, const double *in2, const double *in3,
			      const double *in4, const double *in5, const double *in6, const double *in7,
			      const double *in8, const double *weights, double *out, size_t count)
{
  asm volatile (
"xorq %%rax, %%rax;"
"movq %11, %%rcx;"
"vbroadcastsd 24(%9), %%ymm3;"
"vbroadcastsd 16(%9), %%ymm7;"
"vbroadcastsd 8(%9), %%ymm11;"
"vbroadcastsd 0(%9), %%ymm15;"
"1:"
"vmovapd 0(%%rax,%0), %%ymm0;"
"vmovapd 32(%%rax,%0), %%ymm1;"
"vmovapd 0(%%rax,%1), %%ymm4;"
"vmovapd 32(%%rax,%1), %%ymm5;"
"vmovapd 0(%%rax,%2), %%ymm8;"
"vmovapd 32(%%rax,%2), %%ymm9;"
"vmovapd 0(%%rax,%3), %%ymm12;"
"vmovapd 32(%%rax,%3), %%ymm13;"
"vsubpd 0(%%rax,%5), %%ymm12, %%ymm12;"
"vsubpd 32(%%rax,%5), %%ymm13, %%ymm13;"
"vsubpd 0(%%rax,%6), %%ymm8, %%ymm8;"
"vsubpd 32(%%rax,%6), %%ymm9, %%ymm9;"
"vsubpd 0(%%rax,%7), %%ymm4, %%ymm4;"
"vsubpd 32(%%rax,%7), %%ymm5, %%ymm5;"
"vsubpd 0(%%rax,%8), %%ymm0, %%ymm0;"
"vsubpd 32(%%rax,%8), %%ymm1, %%ymm1;"
"vmulpd %%ymm15, %%ymm12, %%ymm12;"
"vmulpd %%ymm15, %%ymm13, %%ymm13;"
"vmulpd %%ymm11, %%ymm8, %%ymm8;"
"vmulpd %%ymm11, %%ymm9, %%ymm9;"
"vmulpd %%ymm7, %%ymm4, %%ymm4;"
"vmulpd %%ymm7, %%ymm5, %%ymm5;"
"vmulpd %%ymm3, %%ymm0, %%ymm0;"
"vmulpd %%ymm3, %%ymm1, %%ymm1;"
"vaddpd %%ymm8, %%ymm12, %%ymm12;"
"vaddpd %%ymm9, %%ymm13, %%ymm13;"
"vaddpd %%ymm4, %%ymm12, %%ymm12;"
"vaddpd %%ymm5, %%ymm13, %%ymm13;"
"vaddpd %%ymm0, %%ymm12, %%ymm12;"
"vaddpd %%ymm1, %%ymm13, %%ymm13;"
VMOVNTDQ " %%ymm12, 0(%%rax,%10);"
VMOVNTDQ " %%ymm13, 32(%%rax,%10);"
"addq $64, %%rax;"
"decq %%rcx;"
"jne 1b;"
: /* no outputs */
: "r" (in0), "r" (in1), "r" (in2), "r" (in3), "r" (in4), "r" (in5), "r" (in6), "r" (in7), "r" (in8),
  "r" (weights), "r" (out), "r" (count)
: "rax", "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

#if !defined(USE_AVX_KERNELS) && defined(USE_SSE_KERNELS)
static void stencil_y_row_sse(const double *in0, const double *in1, const double *in2, const double *in3,
			      const double *in4, const double *in5, const double *in6, const double *in7,
			      const double *in8, const double *weights, double *out, size_t count)
{
  asm volatile (
"xorq %%rax, %%rax;"
"movq %11, %%rcx;"
"movddup 24(%9), %%xmm3;"
"movddup 16(%9), %%xmm7;"
"movddup 8(%9), %%xmm11;"
"movddup 0(%9), %%xmm15;"
"1:"
"movapd 0(%%rax,%0), %%xmm0;"
"movapd 16(%%rax,%0), %%xmm1;"
"movapd 0(%%rax,%1), %%xmm4;"
"movapd 16(%%rax,%1), %%xmm5;"
"movapd 0(%%rax,%2), %%xmm8;"
"movapd 16(%%rax,%2), %%xmm9;"
"movapd 0(%%rax,%3), %%xmm12;"
"movapd 16(%%rax,%3), %%xmm13;"
"subpd 0(%%rax,%5), %%xmm12;"
"subpd 16(%%rax,%5), %%xmm13;"
"subpd 0(%%rax,%6), %%xmm8;"
"subpd 16(%%rax,%6), %%xmm9;"
"subpd 0(%%rax,%7), %%xmm4;"
"subpd 16(%%rax,%7), %%xmm5;"
"subpd 0(%%rax,%8), %%xmm0;"
"subpd 16(%%rax,%8), %%xmm1;"
"mulpd %%xmm15, %%xmm12;"
"mulpd %%xmm15, %%xmm13;"
"mulpd %%xmm11, %%xmm8;"
"mulpd %%xmm11, %%xmm9;"
"mulpd %%xmm7, %%xmm4;"
"mulpd %%xmm7, %%xmm5;"
"mulpd %%xmm3, %%xmm0;"
"mulpd %%xmm3, %%xmm1;"
"addpd %%xmm8, %%xmm12;"
"addpd %%xmm9, %%xmm13;"
"addpd %%xmm4, %%xmm12;"
"addpd %%xmm5, %%xmm13;"
"addpd %%xmm0, %%xmm12;"
"addpd %%xmm1, %%xmm13;"
MOVNTDQ " %%xmm12, 0(%%rax,%10);"
MOVNTDQ " %%xmm13, 16(%%rax,%10);"
"addq $32, %%rax;"
"decq %%rcx;"
"jne 1b;"
: /* no outputs */
: "r" (in0), "r" (in1), "r" (in2), "r" (in3), "r" (in4), "r" (in5), "r" (in6), "r" (in7), "r" (in8),
  "r" (weights), "r" (out), "r" (count)
: "rax", "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

static void stencil_y(size_t strip_width, size_t strip_height, size_t num_strips,
		      const double *minus_ptr, size_t minus_line_stride, size_t minus_strip_stride,
		      const double *in_ptr, size_t in_line_stride, size_t in_strip_stride,
		      const double *plus_ptr, size_t plus_line_stride, size_t plus_strip_stride,
		      double *out_ptr, size_t out_line_stride, size_t out_strip_stride,
		      double ae, double be, double ce, double de)
{
  double weights[4] = { -ae, -be, -ce, -de };

  // outer loop is over strips
  for(size_t strip_idx = 0; strip_idx < num_strips; strip_idx++) {
    // set up pointers to rows within strip
    const double *row0 = minus_ptr + (0 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row1 = minus_ptr + (1 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row2 = minus_ptr + (2 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row3 = minus_ptr + (3 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row4 = in_ptr + (0 * in_line_stride) + (strip_idx * in_strip_stride);
    const double *row5 = in_ptr + (1 * in_line_stride) + (strip_idx * in_strip_stride);
    const double *row6 = in_ptr + (2 * in_line_stride) + (strip_idx * in_strip_stride);
    const double *row7 = in_ptr + (3 * in_line_stride) + (strip_idx * in_strip_stride);
    double *row_o = out_ptr + (strip_idx * out_strip_stride);

    for(size_t line_idx = 0; line_idx < (strip_height - 4); line_idx++) {
      const double *row8 = row7 + in_line_stride;
#ifdef USE_FMA4_KERNELS
      stencil_y_row_fma4(row0, row1, row2, row3, row4, row5, row6, row7, row8,
			 weights, row_o, strip_width / 16);
#else
#ifdef USE_AVX_KERNELS
      stencil_y_row_avx(row0, row1, row2, row3, row4, row5, row6, row7, row8,
			weights, row_o, strip_width / 8);
#else
      stencil_y_row_sse(row0, row1, row2, row3, row4, row5, row6, row7, row8,
			weights, row_o, strip_width / 4);
#endif
#endif
      if(0 && (strip_idx == 0)) {
	printf("F: %g %g %g %g %g %g %g %g %g -> %g\n",
	       *row0, *row1,  *row2, *row3, *row4, *row5, *row6, *row7, *row8, *row_o);
      }
      row0 = row1;
      row1 = row2;
      row2 = row3;
      row3 = row4;
      row4 = row5;
      row5 = row6;
      row6 = row7;
      row7 = row8;
      row_o = row_o + out_line_stride;
    }

    // last part uses plus data
    for(size_t line_idx = 0; line_idx < 4; line_idx++) {
      const double *row8 = plus_ptr + (line_idx * plus_line_stride) + (strip_idx * plus_strip_stride);
#ifdef USE_FMA4_KERNELS
      stencil_y_row_fma4(row0, row1, row2, row3, row4, row5, row6, row7, row8,
			 weights, row_o, strip_width / 16);
#else
#ifdef USE_AVX_KERNELS
      stencil_y_row_avx(row0, row1, row2, row3, row4, row5, row6, row7, row8,
			weights, row_o, strip_width / 8);
#else
      stencil_y_row_sse(row0, row1, row2, row3, row4, row5, row6, row7, row8,
			weights, row_o, strip_width / 4);
#endif
#endif
      if(0 && (strip_idx == 0)) {
	printf("F: %g %g %g %g %g %g %g %g %g -> %g\n",
	       *row0, *row1,  *row2, *row3, *row4, *row5, *row6, *row7, *row8, *row_o);
      }
      row0 = row1;
      row1 = row2;
      row2 = row3;
      row3 = row4;
      row4 = row5;
      row5 = row6;
      row6 = row7;
      row7 = row8;
      row_o = row_o + out_line_stride;
    }
  }
}

#ifdef NON_CACHE_Z_STENCIL
// "Z" stencil for when the "Y" version won't fit in cache (either because your strip size
//  is too large or because the per-plane stride doesn't play nicely with the L1 associativity
//  (that means you, Bulldozer)
// 'weights4' should have 4 copies of ae, then 4 of be, then 4 of ce, and 4 of de
//
// initial results suggest the problems this approach causes for the prefetcher are worse than
//  cache thrashing...
#define REGSTENCIL_Z_ROTATE(r0, r1, r2, r3, r4, r5, r6, r7, rs, rd, rsinc, rdinc, ae, be, ce, de) \
  "movapd 0(" rs "), %%xmm8;" \
  "addq   " rsinc ", " rs ";" \
  "movapd %%xmm"#r3", %%xmm9;" \
  "subpd  %%xmm"#r5", %%xmm9;" \
  "mulpd  " ae ", %%xmm9;" \
  "movapd %%xmm"#r2", %%xmm10;" \
  "subpd  %%xmm"#r6", %%xmm10;" \
  "mulpd  " be ", %%xmm10;" \
  "addpd  %%xmm10, %%xmm9;" \
  "movapd %%xmm"#r1", %%xmm10;" \
  "subpd  %%xmm"#r7", %%xmm10;" \
  "mulpd  " ce ", %%xmm10;" \
  "addpd  %%xmm10, %%xmm9;" \
  "movapd %%xmm"#r0", %%xmm10;" \
  "movapd %%xmm8, %%xmm"#r0";" \
  "subpd  %%xmm8, %%xmm10;" \
  "mulpd  " de ", %%xmm10;" \
  "addpd  %%xmm10, %%xmm9;" \
  MOVNTDQ " %%xmm9, 0(" rd ");" \
  "addq  " rdinc ", " rd ";"

static void stencil_z_strip(const double *minus, const double *in, const double *plus, double *out,
			    size_t minus_stride, size_t in_stride, size_t plus_stride, size_t out_stride,
			    const double *weights4, size_t count)
{
  asm volatile (
"movapd 0(%8), %%xmm12;"
"movapd 32(%8), %%xmm13;"
"movapd 64(%8), %%xmm14;"
"movapd 96(%8), %%xmm15;"
"leaq   0(%0,%4,2), %%rcx;"
"movapd 0(%0), %%xmm0;"
"movapd 0(%0,%4), %%xmm1;"
"movapd 0(%%rcx), %%xmm2;"
"movapd 0(%%rcx,%4), %%xmm3;"
"leaq   0(%1,%5,2), %%rcx;"
"movapd 0(%1), %%xmm4;"
"movapd 0(%1,%5), %%xmm5;"
"movapd 0(%%rcx), %%xmm6;"
"movapd 0(%%rcx,%5), %%xmm7;"
"leaq   0(%1,%5,4), %%rsi;"
"movq   %9, %%rcx;"
"shrq   $3, %%rcx;"  // doing 8 iterations per loop
"movq   %3, %%rdi;"
"jmp    2f;"
"1:"
REGSTENCIL_Z_ROTATE(4, 5, 6, 7, 0, 1, 2, 3, "%%rsi", "%%rdi", "%5", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
REGSTENCIL_Z_ROTATE(5, 6, 7, 0, 1, 2, 3, 4, "%%rsi", "%%rdi", "%5", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
REGSTENCIL_Z_ROTATE(6, 7, 0, 1, 2, 3, 4, 5, "%%rsi", "%%rdi", "%5", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
REGSTENCIL_Z_ROTATE(7, 0, 1, 2, 3, 4, 5, 6, "%%rsi", "%%rdi", "%5", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
"2:"
REGSTENCIL_Z_ROTATE(0, 1, 2, 3, 4, 5, 6, 7, "%%rsi", "%%rdi", "%5", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
REGSTENCIL_Z_ROTATE(1, 2, 3, 4, 5, 6, 7, 0, "%%rsi", "%%rdi", "%5", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
REGSTENCIL_Z_ROTATE(2, 3, 4, 5, 6, 7, 0, 1, "%%rsi", "%%rdi", "%5", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
REGSTENCIL_Z_ROTATE(3, 4, 5, 6, 7, 0, 1, 2, "%%rsi", "%%rdi", "%5", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
"decq  %%rcx;"
"jne   1b;"
"movq  %2, %%rsi;"
REGSTENCIL_Z_ROTATE(4, 5, 6, 7, 0, 1, 2, 3, "%%rsi", "%%rdi", "%6", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
REGSTENCIL_Z_ROTATE(5, 6, 7, 0, 1, 2, 3, 4, "%%rsi", "%%rdi", "%6", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
REGSTENCIL_Z_ROTATE(6, 7, 0, 1, 2, 3, 4, 5, "%%rsi", "%%rdi", "%6", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
REGSTENCIL_Z_ROTATE(7, 0, 1, 2, 3, 4, 5, 6, "%%rsi", "%%rdi", "%6", "%7", "%%xmm12", "%%xmm13", "%%xmm14", "%%xmm15")
: /* no outputs */
: "r" (minus), "r" (in), "r" (plus), "r" (out),
  "r" (minus_stride), "r" (in_stride), "r" (plus_stride), "r" (out_stride),
  "r" (weights4), "r" (count)
: "rcx", "rsi", "rdi",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}

static void stencil_z(size_t strip_width, size_t strip_height, size_t num_strips,
		      const double *minus_ptr, size_t minus_line_stride, size_t minus_strip_stride,
		      const double *in_ptr, size_t in_line_stride, size_t in_strip_stride,
		      const double *plus_ptr, size_t plus_line_stride, size_t plus_strip_stride,
		      double *out_ptr, size_t out_line_stride, size_t out_strip_stride,
		      double ae, double be, double ce, double de)
{
  double weights4[16] = { -ae, -ae, -ae, -ae,
			  -be, -be, -be, -be,
			  -ce, -ce, -ce, -ce
			  -de, -de, -de, -de };

  // outer loop is over strips
  for(size_t strip_idx = 0; strip_idx < num_strips; strip_idx++) {
    size_t strip_ofs = 0;
    while(strip_ofs < strip_width) {
      stencil_z_strip(minus_ptr + strip_ofs,
		      in_ptr + strip_ofs,
		      plus_ptr + strip_ofs,
		      out_ptr + strip_ofs,
		      minus_line_stride * sizeof(double),
		      in_line_stride * sizeof(double),
		      plus_line_stride * sizeof(double),
		      out_line_stride * sizeof(double),
		      weights4,
		      strip_height);
      strip_ofs += 2;
    }
    minus_ptr += minus_strip_stride;
    in_ptr += in_strip_stride;
    plus_ptr += plus_strip_stride;
    out_ptr += out_strip_stride;
  }
}
#endif

// attempt #2 for Z stencils - bite the bullet and copy to a L1-friendly intermediate buffer
template <typename T>
static void memcpy_stream_load(T *dst, const T *src, size_t count)
{
  // copy cache lines at a time
  static const size_t BLOCK_SIZE = 64;
  count *= sizeof(T);
  assert((count % BLOCK_SIZE) == 0);
  count /= BLOCK_SIZE;
  asm volatile (
"1:"
MOVNTDQA " 0(%1), %%xmm0;"
MOVNTDQA " 16(%1), %%xmm1;"
MOVNTDQA " 32(%1), %%xmm2;"
MOVNTDQA " 48(%1), %%xmm3;"
"addq    $64, %1;"
"movdqa  %%xmm0, 0(%0);"
"movdqa  %%xmm1, 16(%0);"
"movdqa  %%xmm2, 32(%0);"
"movdqa  %%xmm3, 48(%0);"
"addq    $64, %0;"
"decq    %2;"
"jne     1b;"
: "+r" (dst), "+r" (src), "+r" (count)
:
: "xmm0", "xmm1", "xmm2", "xmm3", "memory");
}

template <typename T>
void ALIGN_POINTER(T *& p, size_t amt)
{
  size_t rem = reinterpret_cast<size_t>(p) % amt;
  if(rem) {
    p = reinterpret_cast<T *>(amt * ((reinterpret_cast<size_t>(p) / amt) + 1));
  }
}

// similar to stencil_y_row_sse, except in8 is read using streaming loads and _STORED_ to in0
#if !defined(USE_AVX_KERNELS)
static void stencil_z_row_sse(double *in0, const double *in1, const double *in2, const double *in3,
			      const double *in4, const double *in5, const double *in6, const double *in7,
			      const double *in8, const double *weights, double *out, size_t count)
{
  asm volatile (
"xorq %%rax, %%rax;"
"movq %11, %%rcx;"
"movddup 24(%9), %%xmm3;"
"movddup 16(%9), %%xmm7;"
"movddup 8(%9), %%xmm11;"
"movddup 0(%9), %%xmm15;"
"1:"
MOVNTDQA " 0(%%rax,%8), %%xmm2;"
MOVNTDQA " 16(%%rax,%8), %%xmm6;"
"movapd 0(%%rax,%0), %%xmm0;"
"movapd 16(%%rax,%0), %%xmm1;"
"movapd 0(%%rax,%1), %%xmm4;"
"movapd 16(%%rax,%1), %%xmm5;"
"movapd 0(%%rax,%2), %%xmm8;"
"movapd 16(%%rax,%2), %%xmm9;"
"movapd 0(%%rax,%3), %%xmm12;"
"movapd 16(%%rax,%3), %%xmm13;"
"subpd 0(%%rax,%5), %%xmm12;"
"subpd 16(%%rax,%5), %%xmm13;"
"subpd 0(%%rax,%6), %%xmm8;"
"subpd 16(%%rax,%6), %%xmm9;"
"subpd 0(%%rax,%7), %%xmm4;"
"subpd 16(%%rax,%7), %%xmm5;"
"subpd %%xmm2, %%xmm0;"
"subpd %%xmm6, %%xmm1;"
"movapd %%xmm2, 0(%%rax,%0);"
"movapd %%xmm6, 16(%%rax,%0);"
"mulpd %%xmm15, %%xmm12;"
"mulpd %%xmm15, %%xmm13;"
"mulpd %%xmm11, %%xmm8;"
"mulpd %%xmm11, %%xmm9;"
"mulpd %%xmm7, %%xmm4;"
"mulpd %%xmm7, %%xmm5;"
"mulpd %%xmm3, %%xmm0;"
"mulpd %%xmm3, %%xmm1;"
"addpd %%xmm8, %%xmm12;"
"addpd %%xmm9, %%xmm13;"
"addpd %%xmm4, %%xmm12;"
"addpd %%xmm5, %%xmm13;"
"addpd %%xmm0, %%xmm12;"
"addpd %%xmm1, %%xmm13;"
MOVNTDQ " %%xmm12, 0(%%rax,%10);"
MOVNTDQ " %%xmm13, 16(%%rax,%10);"
"addq $32, %%rax;"
"decq %%rcx;"
"jne 1b;"
: /* no outputs */
: "r" (in0), "r" (in1), "r" (in2), "r" (in3), "r" (in4), "r" (in5), "r" (in6), "r" (in7), "r" (in8),
  "r" (weights), "r" (out), "r" (count)
: "rax", "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

#ifdef USE_AVX_KERNELS
static void stencil_z_row_avx(double *in0, const double *in1, const double *in2, const double *in3,
			      const double *in4, const double *in5, const double *in6, const double *in7,
			      const double *in8, const double *weights, double *out, size_t count)
{
  asm volatile (
"xorq %%rax, %%rax;"
"movq %11, %%rcx;"
"vbroadcastsd 24(%9), %%ymm3;"
"vbroadcastsd 16(%9), %%ymm7;"
"vbroadcastsd 8(%9), %%ymm11;"
"vbroadcastsd 0(%9), %%ymm15;"
"1:"
VMOVNTDQA " 0(%%rax,%8), %%ymm2;"
VMOVNTDQA " 32(%%rax,%8), %%ymm6;"
"vmovapd 0(%%rax,%0), %%ymm0;"
"vmovapd 32(%%rax,%0), %%ymm1;"
"vmovapd 0(%%rax,%1), %%ymm4;"
"vmovapd 32(%%rax,%1), %%ymm5;"
"vmovapd 0(%%rax,%2), %%ymm8;"
"vmovapd 32(%%rax,%2), %%ymm9;"
"vmovapd 0(%%rax,%3), %%ymm12;"
"vmovapd 32(%%rax,%3), %%ymm13;"
"vsubpd 0(%%rax,%5), %%ymm12, %%ymm12;"
"vsubpd 32(%%rax,%5), %%ymm13, %%ymm13;"
"vsubpd 0(%%rax,%6), %%ymm8, %%ymm8;"
"vsubpd 32(%%rax,%6), %%ymm9, %%ymm9;"
"vsubpd 0(%%rax,%7), %%ymm4, %%ymm4;"
"vsubpd 32(%%rax,%7), %%ymm5, %%ymm5;"
"vsubpd %%ymm2, %%ymm0, %%ymm0;"
"vsubpd %%ymm6, %%ymm1, %%ymm1;"
"vmovapd %%ymm2, 0(%%rax,%0);"
"vmovapd %%ymm6, 32(%%rax,%0);"
"vmulpd %%ymm15, %%ymm12, %%ymm12;"
"vmulpd %%ymm15, %%ymm13, %%ymm13;"
"vmulpd %%ymm11, %%ymm8, %%ymm8;"
"vmulpd %%ymm11, %%ymm9, %%ymm9;"
"vmulpd %%ymm7, %%ymm4, %%ymm4;"
"vmulpd %%ymm7, %%ymm5, %%ymm5;"
"vmulpd %%ymm3, %%ymm0, %%ymm0;"
"vmulpd %%ymm3, %%ymm1, %%ymm1;"
"vaddpd %%ymm8, %%ymm12, %%ymm12;"
"vaddpd %%ymm9, %%ymm13, %%ymm13;"
"vaddpd %%ymm4, %%ymm12, %%ymm12;"
"vaddpd %%ymm5, %%ymm13, %%ymm13;"
"vaddpd %%ymm0, %%ymm12, %%ymm12;"
"vaddpd %%ymm1, %%ymm13, %%ymm13;"
VMOVNTDQ " %%ymm12, 0(%%rax,%10);"
VMOVNTDQ " %%ymm13, 32(%%rax,%10);"
"addq $64, %%rax;"
"decq %%rcx;"
"jne 1b;"
: /* no outputs */
: "r" (in0), "r" (in1), "r" (in2), "r" (in3), "r" (in4), "r" (in5), "r" (in6), "r" (in7), "r" (in8),
  "r" (weights), "r" (out), "r" (count)
: "rax", "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

#ifdef USE_FMA4_KERNELS
static void stencil_z_row_fma4(double *in0, const double *in1, const double *in2, const double *in3,
			      const double *in4, const double *in5, const double *in6, const double *in7,
			      const double *in8, const double *weights, double *out, size_t count)
{
  asm volatile (
"xorq %%rax, %%rax;"
"movq %11, %%rcx;"
"vbroadcastsd 24(%9), %%ymm12;"
"vbroadcastsd 16(%9), %%ymm13;"
"vbroadcastsd 8(%9), %%ymm14;"
"vbroadcastsd 0(%9), %%ymm15;"
"1:"
VMOVNTDQA " 0(%%rax,%8), %%ymm4;"
VMOVNTDQA " 32(%%rax,%8), %%ymm5;"
VMOVNTDQA " 64(%%rax,%8), %%ymm10;"
VMOVNTDQA " 96(%%rax,%8), %%ymm11;"
"vmovapd 0(%%rax,%3), %%ymm0;"
"vmovapd 32(%%rax,%3), %%ymm1;"
"vmovapd 64(%%rax,%3), %%ymm2;"
"vmovapd 96(%%rax,%3), %%ymm3;"
"vsubpd 0(%%rax,%5), %%ymm0, %%ymm0;"
"vsubpd 32(%%rax,%5), %%ymm1, %%ymm1;"
"vsubpd 64(%%rax,%5), %%ymm2, %%ymm2;"
"vsubpd 96(%%rax,%5), %%ymm3, %%ymm3;"
"vmulpd %%ymm0, %%ymm15, %%ymm6;"
"vmulpd %%ymm1, %%ymm15, %%ymm7;"
"vmulpd %%ymm2, %%ymm15, %%ymm8;"
"vmulpd %%ymm3, %%ymm15, %%ymm9;"
"vmovapd 0(%%rax,%2), %%ymm0;"
"vmovapd 32(%%rax,%2), %%ymm1;"
"vmovapd 64(%%rax,%2), %%ymm2;"
"vmovapd 96(%%rax,%2), %%ymm3;"
"vsubpd 0(%%rax,%6), %%ymm0, %%ymm0;"
"vsubpd 32(%%rax,%6), %%ymm1, %%ymm1;"
"vsubpd 64(%%rax,%6), %%ymm2, %%ymm2;"
"vsubpd 96(%%rax,%6), %%ymm3, %%ymm3;"
"vfmaddpd %%ymm6, %%ymm0, %%ymm14, %%ymm6;"
"vfmaddpd %%ymm7, %%ymm1, %%ymm14, %%ymm7;"
"vfmaddpd %%ymm8, %%ymm2, %%ymm14, %%ymm8;"
"vfmaddpd %%ymm9, %%ymm3, %%ymm14, %%ymm9;"
"vmovapd 0(%%rax,%1), %%ymm0;"
"vmovapd 32(%%rax,%1), %%ymm1;"
"vmovapd 64(%%rax,%1), %%ymm2;"
"vmovapd 96(%%rax,%1), %%ymm3;"
"vsubpd 0(%%rax,%7), %%ymm0, %%ymm0;"
"vsubpd 32(%%rax,%7), %%ymm1, %%ymm1;"
"vsubpd 64(%%rax,%7), %%ymm2, %%ymm2;"
"vsubpd 96(%%rax,%7), %%ymm3, %%ymm3;"
"vfmaddpd %%ymm6, %%ymm0, %%ymm13, %%ymm6;"
"vfmaddpd %%ymm7, %%ymm1, %%ymm13, %%ymm7;"
"vfmaddpd %%ymm8, %%ymm2, %%ymm13, %%ymm8;"
"vfmaddpd %%ymm9, %%ymm3, %%ymm13, %%ymm9;"
"vmovapd 0(%%rax,%0), %%ymm0;"
"vmovapd 32(%%rax,%0), %%ymm1;"
"vmovapd 64(%%rax,%0), %%ymm2;"
"vmovapd 96(%%rax,%0), %%ymm3;"
"vmovdqa %%ymm4, 0(%%rax,%0);"
"vmovdqa %%ymm5, 32(%%rax,%0);"
"vmovdqa %%ymm10, 64(%%rax,%0);"
"vmovdqa %%ymm11, 96(%%rax,%0);"
"vsubpd %%ymm4, %%ymm0, %%ymm0;"
"vsubpd %%ymm5, %%ymm1, %%ymm1;"
"vsubpd %%ymm10, %%ymm2, %%ymm2;"
"vsubpd %%ymm11, %%ymm3, %%ymm3;"
"vfmaddpd %%ymm6, %%ymm0, %%ymm12, %%ymm6;"
"vfmaddpd %%ymm7, %%ymm1, %%ymm12, %%ymm7;"
"vfmaddpd %%ymm8, %%ymm2, %%ymm12, %%ymm8;"
"vfmaddpd %%ymm9, %%ymm3, %%ymm12, %%ymm9;"
VMOVNTDQ " %%ymm6, 0(%%rax,%10);"
VMOVNTDQ " %%ymm7, 32(%%rax,%10);"
VMOVNTDQ " %%ymm8, 64(%%rax,%10);"
VMOVNTDQ " %%ymm9, 96(%%rax,%10);"
"addq $128, %%rax;"
"decq %%rcx;"
"jne 1b;"
: /* no outputs */
: "r" (in0), "r" (in1), "r" (in2), "r" (in3), "r" (in4), "r" (in5), "r" (in6), "r" (in7), "r" (in8),
  "r" (weights), "r" (out), "r" (count)
: "rax", "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}
#endif

static void stencil_z(size_t strip_width, size_t strip_height, size_t num_strips,
		      const double *minus_ptr, size_t minus_line_stride, size_t minus_strip_stride,
		      const double *in_ptr, size_t in_line_stride, size_t in_strip_stride,
		      const double *plus_ptr, size_t plus_line_stride, size_t plus_strip_stride,
		      double *out_ptr, size_t out_line_stride, size_t out_strip_stride,
		      double ae, double be, double ce, double de)
{
  double weights[4] = { -ae, -be, -ce, -de };
  double *buffer = (double *)alloca(8 * strip_width * sizeof(double) + 64);
  ALIGN_POINTER(buffer, 64);

  // outer loop is over strips
  for(size_t strip_idx = 0; strip_idx < num_strips; strip_idx++) {
    // set up pointers to rows within strip
    double *row0 = buffer;
    memcpy_stream_load(row0, minus_ptr + (0 * minus_line_stride) + (strip_idx * minus_strip_stride), strip_width);
    double *row1 = row0 + strip_width;
    memcpy_stream_load(row1, minus_ptr + (1 * minus_line_stride) + (strip_idx * minus_strip_stride), strip_width);
    double *row2 = row1 + strip_width;
    memcpy_stream_load(row2, minus_ptr + (2 * minus_line_stride) + (strip_idx * minus_strip_stride), strip_width);
    double *row3 = row2 + strip_width;
    memcpy_stream_load(row3, minus_ptr + (3 * minus_line_stride) + (strip_idx * minus_strip_stride), strip_width);
    double *row4 = row3 + strip_width;
    memcpy_stream_load(row4, in_ptr + (0 * in_line_stride) + (strip_idx * in_strip_stride), strip_width);
    double *row5 = row4 + strip_width;
    memcpy_stream_load(row5, in_ptr + (1 * in_line_stride) + (strip_idx * in_strip_stride), strip_width);
    double *row6 = row5 + strip_width;
    memcpy_stream_load(row6, in_ptr + (2 * in_line_stride) + (strip_idx * in_strip_stride), strip_width);
    double *row7 = row6 + strip_width;
    memcpy_stream_load(row7, in_ptr + (3 * in_line_stride) + (strip_idx * in_strip_stride), strip_width);
		       
    const double *row_i = in_ptr + (4 * in_line_stride) + (strip_idx * in_strip_stride);;
    double *row_o = out_ptr + (strip_idx * out_strip_stride);

    for(size_t line_idx = 0; line_idx < (strip_height - 4); line_idx++) {
#ifdef USE_FMA4_KERNELS
      stencil_z_row_fma4(row0, row1, row2, row3, row4, row5, row6, row7, row_i,
			 weights, row_o, strip_width / 16);
#else
#ifdef USE_AVX_KERNELS
      stencil_z_row_avx(row0, row1, row2, row3, row4, row5, row6, row7, row_i,
			weights, row_o, strip_width / 8);
#else
      stencil_z_row_sse(row0, row1, row2, row3, row4, row5, row6, row7, row_i,
			weights, row_o, strip_width / 4);
#endif
#endif
      double *row8 = row0;
      row0 = row1;
      row1 = row2;
      row2 = row3;
      row3 = row4;
      row4 = row5;
      row5 = row6;
      row6 = row7;
      row7 = row8;
      row_i = row_i + in_line_stride;
      row_o = row_o + out_line_stride;
    }

    // last part uses plus data
    row_i = plus_ptr + (strip_idx * plus_strip_stride);
    for(size_t line_idx = 0; line_idx < 4; line_idx++) {
#ifdef USE_FMA4_KERNELS
      stencil_z_row_fma4(row0, row1, row2, row3, row4, row5, row6, row7, row_i,
			 weights, row_o, strip_width / 16);
#else
#ifdef USE_AVX_KERNELS
      stencil_z_row_avx(row0, row1, row2, row3, row4, row5, row6, row7, row_i,
			weights, row_o, strip_width / 8);
#else
      stencil_z_row_sse(row0, row1, row2, row3, row4, row5, row6, row7, row_i,
			weights, row_o, strip_width / 4);
#endif
#endif
      double *row8 = row0;
      row0 = row1;
      row1 = row2;
      row2 = row3;
      row3 = row4;
      row4 = row5;
      row5 = row6;
      row6 = row7;
      row7 = row8;
      row_i = row_i + plus_line_stride;
      row_o = row_o + out_line_stride;
    }
  }
}

void fast_cpu_stencil(int dim_1, int dim_2, int dim_3, double ds,
		      const double *minus_ptr, size_t minus_stride_1, size_t minus_stride_2, size_t minus_stride_3,
		      const double *in_ptr, size_t in_stride_1, size_t in_stride_2, size_t in_stride_3,
		      const double *plus_ptr, size_t plus_stride_1, size_t plus_stride_2, size_t plus_stride_3,
		      double *out_ptr, size_t out_stride_1, size_t out_stride_2, size_t out_stride_3)
{
  double ae = 4.0 / 5.0 * ds;
  double be = -1.0 / 5.0 * ds;
  double ce = 4.0 / 105.0 * ds;
  double de = -1.0 / 280.0 * ds;

  // dense x stencil case
  if((minus_stride_1 == 8) && (in_stride_1 == 8) && (plus_stride_1 == 8) && (out_stride_1 == 8)) {
    //printf("x stencil\n");
    stencil_x(dim_1, dim_2, dim_3,
	      minus_ptr, minus_stride_2, minus_stride_3,
	      in_ptr, in_stride_2, in_stride_3,
	      plus_ptr, plus_stride_2, plus_stride_3,
	      out_ptr, out_stride_2, out_stride_3,
	      ae, be, ce, de);
    return;
  }

  // y stencil case
  if((minus_stride_2 == 8) && (in_stride_2 == 8) && (plus_stride_2 == 8) && (out_stride_2 == 8)) {
    if((minus_stride_3 == (dim_2 * minus_stride_2)) &&
       (in_stride_3 == (dim_2 * in_stride_2)) &&
       (plus_stride_3 == (dim_2 * plus_stride_2)) &&
       (out_stride_3 == (dim_2 * out_stride_2))) {
      // size strips so that 8 rows of temp data consumes 8KB, which should
      //  fit comfortably inside even stupidly-small L1s
      size_t strip_width = 128; //dim_2  * dim_3;
      //printf("z stencil\n");
#ifdef PERF_DEBUG
      {
	const char *s = getenv("Z_STENCIL_STRIP");
	if(s) strip_width = atoi(s);
      }
      if(getenv("FORCE_Y_STENCIL") != 0)
      stencil_y(strip_width, dim_1, dim_2 * dim_3 / strip_width,
		minus_ptr, minus_stride_1 / sizeof(double), strip_width,
		in_ptr, in_stride_1 / sizeof(double), strip_width,
		plus_ptr, plus_stride_1 / sizeof(double), strip_width,
		out_ptr, out_stride_1 / sizeof(double), strip_width,
		ae, be, ce, de); else
#endif
      stencil_z(strip_width, dim_1, dim_2 * dim_3 / strip_width,
		minus_ptr, minus_stride_1 / sizeof(double), strip_width,
		in_ptr, in_stride_1 / sizeof(double), strip_width,
		plus_ptr, plus_stride_1 / sizeof(double), strip_width,
		out_ptr, out_stride_1 / sizeof(double), strip_width,
		ae, be, ce, de);
    } else {
      //printf("y stencil\n");
      stencil_y(dim_2, dim_1, dim_3,
		minus_ptr, minus_stride_1 / sizeof(double), minus_stride_3 / sizeof(double),
		in_ptr, in_stride_1 / sizeof(double), in_stride_3 / sizeof(double),
		plus_ptr, plus_stride_1 / sizeof(double), plus_stride_3 / sizeof(double),
		out_ptr, out_stride_1 / sizeof(double), out_stride_3 / sizeof(double),
		ae, be, ce, de);
    }
    return;
  }

  // not a case we handle
  assert(0);
}
