
#include "calc_physics.h"
#include "check_field.h"

// Include Singe emitted header files
#include "getcoeffs.h"
#include "getrates.h"

// Singe header files for SSE
#ifdef USE_SSE_KERNELS
#include "sse_getcoeffs.h"
#include "sse_getrates.h"
#endif

// Singe header files for AVX
#ifdef USE_AVX_KERNELS
#include "avx_getcoeffs.h"
#include "avx_getrates.h"
#endif

#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
#include <x86intrin.h>
#endif

// Chemistry and physics for the GPU
#ifdef USE_GPU_KERNELS
#include "rhsf_gpu.h"
#endif

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

CalcDiffusionTask::CalcDiffusionTask(S3DRank *r,
                                     Domain domain,
                                     TaskArgument global_arg,
                                     ArgumentMap arg_map,
                                     Predicate pred,
                                     bool must,
                                     MapperID id,
                                     MappingTagID tag,
                                     bool add_requirements,
                                     bool redundant)
 : IndexLauncher(CalcDiffusionTask::TASK_ID, domain, global_arg, arg_map, 
                 pred, must, id, tag | CalcDiffusionTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_state_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_state);
    for (int i = 0; i < rank->n_spec; i++)
      rr_out.add_field(FID_DS_MIXAVG(i));
    add_region_requirement(rr_out);

    RegionRequirement rr_temp(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_temp.add_field(FID_TEMP);
    add_region_requirement(rr_temp);

    RegionRequirement rr_pres(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    rr_pres.add_field(FID_PRESSURE);
    add_region_requirement(rr_pres);

    RegionRequirement rr_spec(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
#ifdef REDUNDANT_WORK
    if (redundant) {
      rr_spec.add_field(FID_MIXMW_ALT);
      for (int i = 0; i < rank->n_spec; i++)
        rr_spec.add_field(FID_YSPECIES_ALT(i));
    } else {
      rr_spec.add_field(FID_MIXMW);
      for (int i = 0; i < rank->n_spec; i++)
        rr_spec.add_field(FID_YSPECIES(i));
    }
#else
    assert(!redundant);
    rr_spec.add_field(FID_MIXMW);
    for (int i = 0; i < rank->n_spec; i++)
      rr_spec.add_field(FID_YSPECIES(i));
#endif
    add_region_requirement(rr_spec);
  }
}

void CalcDiffusionTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // seeing up to 10% error in diffusion because reference code seems to be a bit sloppy
  //  in the name of performance
  for(int s = 0; s < rank->n_spec; s++)
    check_field(runtime, ctx, rank->proc_grid_bounds, rank->is_grid, 
                rank->ip_top, rank->lr_state, FID_DS_MIXAVG(s),
                rank, &S3DRank::RHSFArrays::ds_mixavg, s, 1e-5, 1e1, 0, "ds_mixavg[%d]", s);
}

/*static*/ const char * const CalcDiffusionTask::TASK_NAME = "calc_diffusion";

/*static*/
bool CalcDiffusionTask::dense_diffusion(const Rect<3> &my_subgrid_bounds,
                              const std::vector<RegionRequirement> &reqs,
                              const std::vector<PhysicalRegion> &regions,
                              RegionAccessor<AccessorType::Generic,double> fa_mixmw,
                              RegionAccessor<AccessorType::Generic,double> fa_temp,
                              RegionAccessor<AccessorType::Generic,double> fa_pressure)
{
  Rect<3> subrect;
  ByteOffset offsets[3], offsets2[3];
  const double *in_temp = fa_temp.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets);
  if(!in_temp || (subrect != my_subgrid_bounds) || !offsets_are_dense<3, double>(subrect, offsets)) return false;

  const double *in_press = fa_pressure.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_press || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;

  const double *in_yspec = regions[3].get_field_accessor(reqs[3].instance_fields[1]).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_yspec || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;
  size_t in_yspec_stride = (regions[3].get_field_accessor(reqs[3].instance_fields[2]).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2) - in_yspec);

  const double *in_mixmw = fa_mixmw.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_mixmw || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;

  double *out_diff = regions[0].get_field_accessor(FID_DS_MIXAVG(0)).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!out_diff || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;
  size_t out_diff_stride = (regions[0].get_field_accessor(FID_DS_MIXAVG(1)).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2) - out_diff);
  if (in_yspec_stride != out_diff_stride) return false;

  size_t num_elmts = my_subgrid_bounds.volume();

#ifdef USE_AVX_KERNELS
  avx_diffusion(in_temp, in_press, in_yspec, in_mixmw, num_elmts, in_yspec_stride, out_diff);
  _mm256_zeroall();
  return true;
#else
#ifdef USE_SSE_KERNELS
  sse_diffusion(in_temp, in_press, in_yspec, in_mixmw, num_elmts, in_yspec_stride, out_diff);
  return true;
#else
  return false;
#endif
#endif
}

/*static*/
void CalcDiffusionTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                      const std::vector<RegionRequirement> &reqs,
                                      const std::vector<PhysicalRegion> &regions,
                                      Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  if (S3DRank::get_show_progress())
    printf("%s task\n", "CPU diffusion"); 

  RegionAccessor<AccessorType::Generic,double> fa_mixmw = regions[3].get_field_accessor(reqs[3].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_temp = regions[1].get_field_accessor(FID_TEMP).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_pressure = regions[2].get_field_accessor(FID_PRESSURE).typeify<double>();

  if (!dense_diffusion(subgrid_bounds, reqs, regions, fa_mixmw, fa_temp, fa_pressure))
  {
    double *yspecs = (double *)alloca(rank->n_spec * sizeof(double));
    double *diffusion = (double *)alloca(rank->n_spec * sizeof(double));
    for (GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++)
    {
      DomainPoint dp = DomainPoint::from_point<3>(pir.p); 

      double temperature = fa_temp.read(dp);
      double pressure = fa_pressure.read(dp);

      for (int i = 0; i < rank->n_spec; i++)
        yspecs[i] = regions[3].get_field_accessor(reqs[3].instance_fields[i+1]).typeify<double>().read(dp);

      double mixmw = fa_mixmw.read(dp);

      base_diffusion(pressure, temperature, yspecs, mixmw, diffusion);

      for (int i = 0; i < rank->n_spec; i++)
      {
        regions[0].get_field_accessor(FID_DS_MIXAVG(i)).typeify<double>().write(dp, diffusion[i]);
      }
    }
  }
#endif
}

/*static*/
void CalcDiffusionTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                      const std::vector<RegionRequirement> &reqs,
                                      const std::vector<PhysicalRegion> &regions,
                                      Context ctx, HighLevelRuntime *runtime)
{
#ifdef USE_GPU_KERNELS
#ifndef NO_COMPUTE
  if (S3DRank::get_show_progress())
    printf("%s task\n", "GPU diffusion");

  // Assumes all species values are contiguous
  RegionAccessor<AccessorType::Generic,double> diff = regions[0].get_field_accessor(FID_DS_MIXAVG(0)).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> mixmw = regions[3].get_field_accessor(reqs[3].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> temperature = regions[1].get_field_accessor(FID_TEMP).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> pressure = regions[2].get_field_accessor(FID_PRESSURE).typeify<double>();
  // Assumes all species values are contiguous
  RegionAccessor<AccessorType::Generic,double> species = regions[3].get_field_accessor(reqs[3].instance_fields[1]).typeify<double>();

  Rect<3> bounds = subgrid_bounds;
  Rect<3> subrect;
  ByteOffset diff_offsets[3];
  double *diff_ptr = diff.raw_rect_ptr<3>(bounds, subrect, diff_offsets);
  assert(diff_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset temp_offsets[3];
  double *temp_ptr = temperature.raw_rect_ptr<3>(bounds, subrect, temp_offsets); 
  assert(temp_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset pres_offsets[3];
  double *pres_ptr = pressure.raw_rect_ptr<3>(bounds, subrect, pres_offsets);
  assert(pres_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset spec_offsets[3];
  double *spec_ptr = species.raw_rect_ptr<3>(bounds, subrect, spec_offsets);
  assert(spec_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset mixmw_offsets[3];
  double *mixmw_ptr = mixmw.raw_rect_ptr<3>(bounds, subrect, mixmw_offsets);
  assert(mixmw_ptr != NULL);
  assert(subrect == bounds);

  run_gpu_diffusion(temp_ptr, pres_ptr, spec_ptr, mixmw_ptr, bounds.dim_size(0), 
                    bounds.dim_size(1), bounds.dim_size(2), diff_ptr);
#endif
#endif
}

CalcViscosityTask::CalcViscosityTask(S3DRank *r,
                                     Domain domain,
                                     TaskArgument global_arg,
                                     ArgumentMap arg_map,
                                     Predicate pred,
                                     bool must,
                                     MapperID id,
                                     MappingTagID tag,
                                     bool add_requirements,
                                     bool redundant)
 : IndexLauncher(CalcViscosityTask::TASK_ID, domain, global_arg, 
                 arg_map, pred, must, id, tag | CalcViscosityTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_state_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_state);
    rr_out.add_field(FID_VISCOSITY);
    add_region_requirement(rr_out);

    RegionRequirement rr_temp(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_temp.add_field(FID_TEMP);
    add_region_requirement(rr_temp);

    RegionRequirement rr_spec(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
#ifdef REDUNDANT_WORK
    if (redundant) {
      rr_spec.add_field(FID_MIXMW_ALT);
      for (int i = 0; i < rank->n_spec; i++)
        rr_spec.add_field(FID_YSPECIES_ALT(i));
    } else {
      rr_spec.add_field(FID_MIXMW);
      for (int i = 0; i < rank->n_spec; i++)
        rr_spec.add_field(FID_YSPECIES(i));
    }
#else
    assert(!redundant);
    rr_spec.add_field(FID_MIXMW);
    for (int i = 0; i < rank->n_spec; i++)
      rr_spec.add_field(FID_YSPECIES(i));
#endif
    add_region_requirement(rr_spec);
  }
}

void CalcViscosityTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  check_field(runtime, ctx, rank->proc_grid_bounds, rank->is_grid, 
              rank->ip_top, rank->lr_state, FID_VISCOSITY,
              rank, &S3DRank::RHSFArrays::viscosity, 0, 1e-15, 1e10, 0, "viscosity");
}

/*static*/ const char * const CalcViscosityTask::TASK_NAME = "calc_viscosity";

/*static*/
bool CalcViscosityTask::dense_viscosity(const Rect<3> &my_subgrid_bounds,
                              const std::vector<RegionRequirement> &reqs,
                              const std::vector<PhysicalRegion> &regions,
                              RegionAccessor<AccessorType::Generic,double> fa_viscosity,
                              RegionAccessor<AccessorType::Generic,double> fa_mixmw,
                              RegionAccessor<AccessorType::Generic,double> fa_temp)
{
  Rect<3> subrect;
  ByteOffset offsets[3], offsets2[3];
  const double *in_temp = fa_temp.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets);
  if(!in_temp || (subrect != my_subgrid_bounds) || !offsets_are_dense<3, double>(subrect, offsets)) return false;

  const double *in_yspec = regions[2].get_field_accessor(reqs[2].instance_fields[1]).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_yspec || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;
  size_t in_yspec_stride = (regions[2].get_field_accessor(reqs[2].instance_fields[2]).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2) - in_yspec);

  const double *in_mixmw = fa_mixmw.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_mixmw || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;

  double *out_viscosity = fa_viscosity.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!out_viscosity || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;

  size_t num_elmts = my_subgrid_bounds.volume();

#ifdef USE_AVX_KERNELS
  avx_viscosity(in_temp, in_yspec, in_mixmw, num_elmts, in_yspec_stride, out_viscosity);
  _mm256_zeroall();
  return true;
#else
#ifdef USE_SSE_KERNELS
  sse_viscosity(in_temp, in_yspec, in_mixmw, num_elmts, in_yspec_stride, out_viscosity);
  return true;
#else
  return false;
#endif
#endif
}

/*static*/
void CalcViscosityTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  if (S3DRank::get_show_progress())
    printf("%s task\n", "CPU viscosity"); 

  RegionAccessor<AccessorType::Generic,double> fa_viscosity = regions[0].get_field_accessor(FID_VISCOSITY).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_mixmw = regions[2].get_field_accessor(reqs[2].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_temp = regions[1].get_field_accessor(FID_TEMP).typeify<double>();

  if (!dense_viscosity(subgrid_bounds, reqs, regions, fa_viscosity, fa_mixmw, fa_temp))
  {
    double *yspecs = (double *)alloca(rank->n_spec * sizeof(double));
    for (GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++)
    {
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double temperature = fa_temp.read(dp);

      for (int i = 0; i < rank->n_spec; i++)
        yspecs[i] = regions[2].get_field_accessor(reqs[2].instance_fields[i+1]).typeify<double>().read(dp);

      double mixmw = fa_mixmw.read(dp);

      double viscosity = base_viscosity(temperature, yspecs, mixmw);
      fa_viscosity.write(dp, viscosity);
    }
  }
#endif
}

/*static*/
void CalcViscosityTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  if (S3DRank::get_show_progress())
    printf("%s task\n", "GPU viscosity"); 

  RegionAccessor<AccessorType::Generic,double> viscosity = regions[0].get_field_accessor(FID_VISCOSITY).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> mixmw = regions[2].get_field_accessor(reqs[2].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> temperature = regions[1].get_field_accessor(FID_TEMP).typeify<double>();
  // Assumes species values are contiguous
  RegionAccessor<AccessorType::Generic,double> species = regions[2].get_field_accessor(reqs[2].instance_fields[1]).typeify<double>();

  Rect<3> bounds = subgrid_bounds;
  Rect<3> subrect;
  ByteOffset visc_offsets[3];
  double *visc_ptr = viscosity.raw_rect_ptr<3>(bounds, subrect, visc_offsets);
  assert(visc_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset temp_offsets[3];
  double *temp_ptr = temperature.raw_rect_ptr<3>(bounds, subrect, temp_offsets);
  assert(temp_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset spec_offsets[3];
  double *spec_ptr = species.raw_rect_ptr<3>(bounds, subrect, spec_offsets);
  assert(spec_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset mixmw_offsets[3];
  double *mixmw_ptr = mixmw.raw_rect_ptr<3>(bounds, subrect, mixmw_offsets);
  assert(mixmw_ptr != NULL);
  assert(subrect == bounds);

  run_gpu_viscosity(temp_ptr, spec_ptr, mixmw_ptr, bounds.dim_size(0), 
                    bounds.dim_size(1), bounds.dim_size(2), visc_ptr);
#endif
#endif
}

CalcConductivityTask::CalcConductivityTask(S3DRank *r,
                                           Domain domain,
                                           TaskArgument global_arg,
                                           ArgumentMap arg_map,
                                           Predicate pred,
                                           bool must,
                                           MapperID id,
                                           MappingTagID tag,
                                           bool add_requirements,
                                           bool redundant)
 : IndexLauncher(CalcConductivityTask::TASK_ID, domain, global_arg, 
                 arg_map, pred, must, id, tag | CalcConductivityTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_state_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_state);
    rr_out.add_field(FID_LAMBDA);
    add_region_requirement(rr_out);

    RegionRequirement rr_temp(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_temp.add_field(FID_TEMP);
    add_region_requirement(rr_temp);

    RegionRequirement rr_spec(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
#ifdef REDUNDANT_WORK
    if (redundant) {
      rr_spec.add_field(FID_MIXMW_ALT);
      for (int i = 0; i < rank->n_spec; i++)
        rr_spec.add_field(FID_YSPECIES_ALT(i));
    } else {
      rr_spec.add_field(FID_MIXMW);
      for (int i = 0; i < rank->n_spec; i++)
        rr_spec.add_field(FID_YSPECIES(i));
    }
#else
    assert(!redundant);
    rr_spec.add_field(FID_MIXMW);
    for (int i = 0; i < rank->n_spec; i++)
      rr_spec.add_field(FID_YSPECIES(i));
#endif
    add_region_requirement(rr_spec);
  }
}

void CalcConductivityTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  check_field(runtime, ctx, rank->proc_grid_bounds, rank->is_grid, 
              rank->ip_top, rank->lr_state, FID_LAMBDA,
              rank, &S3DRank::RHSFArrays::lambda, 0, 1e-15, 1e10, 0, "lambda");
}

/*static*/ const char * const CalcConductivityTask::TASK_NAME = "calc_conductivity";

/*static*/
bool CalcConductivityTask::dense_conductivity(const Rect<3> &my_subgrid_bounds,
                                              const std::vector<RegionRequirement> &reqs,
                                              const std::vector<PhysicalRegion> &regions,
                                              RegionAccessor<AccessorType::Generic,double> fa_lambda,
                                              RegionAccessor<AccessorType::Generic,double> fa_mixmw,
                                              RegionAccessor<AccessorType::Generic,double> fa_temp)
{
  Rect<3> subrect;
  ByteOffset offsets[3], offsets2[3];
  const double *in_temp = fa_temp.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets);
  if(!in_temp || (subrect != my_subgrid_bounds) || !offsets_are_dense<3, double>(subrect, offsets)) return false;;

  const double *in_yspec = regions[2].get_field_accessor(reqs[2].instance_fields[1]).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_yspec || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;
  size_t in_yspec_stride = (regions[2].get_field_accessor(reqs[2].instance_fields[2]).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2) - in_yspec);

  const double *in_mixmw = fa_mixmw.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_mixmw || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;

  double *out_lambda = fa_lambda.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!out_lambda || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;

  size_t num_elmts = my_subgrid_bounds.volume();

#ifdef USE_AVX_KERNELS
  avx_conductivity(in_temp, in_yspec, in_mixmw, num_elmts, in_yspec_stride, out_lambda);
  _mm256_zeroall();
  return true;
#else
#ifdef USE_SSE_KERNELS
  sse_conductivity(in_temp, in_yspec, in_mixmw, num_elmts, in_yspec_stride, out_lambda);
  return true;
#else
  return false;
#endif
#endif
}

/*static*/
void CalcConductivityTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  if (S3DRank::get_show_progress())
    printf("%s task\n", "CPU conductivity"); 

  RegionAccessor<AccessorType::Generic,double> fa_lambda = regions[0].get_field_accessor(FID_LAMBDA).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_mixmw = regions[2].get_field_accessor(reqs[2].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_temp = regions[1].get_field_accessor(FID_TEMP).typeify<double>();

  if (!dense_conductivity(subgrid_bounds, reqs, regions, fa_lambda, fa_mixmw, fa_temp))
  {
    double *yspecs = (double *)alloca(rank->n_spec * sizeof(double));
    //const double lambda_ref = RHO_REF * A_REF * A_REF * A_REF * L_REF / T_REF;
    for (GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++)
    {
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double temperature = fa_temp.read(dp);

      for (int i = 0; i < rank->n_spec; i++)
        yspecs[i] = regions[2].get_field_accessor(reqs[2].instance_fields[i+1]).typeify<double>().read(dp);

      double mixmw = fa_mixmw.read(dp);

      double conductivity = base_conductivity(temperature, yspecs, mixmw);

      fa_lambda.write(dp, conductivity);
    }
  }
#endif
}

/*static*/
void CalcConductivityTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &my_subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  if (S3DRank::get_show_progress())
    printf("%s task\n", "GPU conductivity"); 

  RegionAccessor<AccessorType::Generic,double> lambda = regions[0].get_field_accessor(FID_LAMBDA).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> mixmw = regions[2].get_field_accessor(reqs[2].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> temperature = regions[1].get_field_accessor(FID_TEMP).typeify<double>();
  // Assumes species values are contiguous
  RegionAccessor<AccessorType::Generic,double> species = regions[2].get_field_accessor(reqs[2].instance_fields[1]).typeify<double>();

  Rect<3> bounds = my_subgrid_bounds;
  Rect<3> subrect;
  ByteOffset lambda_offsets[3];
  double *lambda_ptr = lambda.raw_rect_ptr<3>(bounds, subrect, lambda_offsets); 
  assert(lambda_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset temp_offsets[3];
  double *temp_ptr = temperature.raw_rect_ptr<3>(bounds, subrect, temp_offsets);
  assert(temp_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset spec_offsets[3];
  double *spec_ptr = species.raw_rect_ptr<3>(bounds, subrect, spec_offsets); 
  assert(spec_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset mixmw_offsets[3];
  double *mixmw_ptr = mixmw.raw_rect_ptr<3>(bounds, subrect, mixmw_offsets);
  assert(mixmw_ptr != NULL);
  assert(subrect == bounds);

  run_gpu_conductivity(temp_ptr, spec_ptr, mixmw_ptr, bounds.dim_size(0), 
                       bounds.dim_size(1), bounds.dim_size(2), lambda_ptr);
#endif
#endif
}

CalcThermalTask::CalcThermalTask(S3DRank *r,
                                 Domain domain,
                                 TaskArgument global_arg,
                                 ArgumentMap arg_map,
                                 Predicate pred,
                                 bool must,
                                 MapperID id,
                                 MappingTagID tag,
                                 bool add_requirements)
 : IndexLauncher(CalcThermalTask::TASK_ID, domain, global_arg, 
                 arg_map, pred, must, id, tag | CalcThermalTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_state_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_state);
    for (int i = 0; i < rank->n_spec; i++)
      rr_out.add_field(FID_RS_THERM_DIFF(i));
    add_region_requirement(rr_out);

    RegionRequirement rr_temp(rank->lp_state_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_state);
    rr_temp.add_field(FID_TEMP);
    add_region_requirement(rr_temp);

    RegionRequirement rr_spec(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    for (int i = 0; i < rank->n_spec; i++)
      rr_spec.add_field(FID_YSPECIES(i));
    rr_spec.add_field(FID_MIXMW);
    add_region_requirement(rr_spec);
  }
}

void CalcThermalTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  for (int s = 0; s < rank->n_spec; s++)
    check_field(runtime, ctx, rank->proc_grid_bounds, rank->is_grid, 
                rank->ip_top, rank->lr_state, FID_RS_THERM_DIFF(s),
                rank, &S3DRank::RHSFArrays::rs_therm_diff, s, 1e-15, 1e10, 0, "rs_therm_diff[%d]", s);
}

/*static*/ const char * const CalcThermalTask::TASK_NAME = "calc_thermal";

/*static*/
bool CalcThermalTask::dense_thermal(const Rect<3> &my_subgrid_bounds,
                                    const std::vector<RegionRequirement> &reqs,
                                    const std::vector<PhysicalRegion> &regions,
                                    RegionAccessor<AccessorType::Generic,double> fa_mixmw,
                                    RegionAccessor<AccessorType::Generic,double> fa_temp)
{
  Rect<3> subrect;
  ByteOffset offsets[3], offsets2[3];
  const double *in_temp = fa_temp.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets);
  if(!in_temp || (subrect != my_subgrid_bounds) || !offsets_are_dense<3, double>(subrect, offsets)) return false;

  const double *in_yspec = regions[2].get_field_accessor(FID_YSPECIES(0)).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_yspec || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;
  size_t in_yspec_stride = (regions[2].get_field_accessor(FID_YSPECIES(1)).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2) - in_yspec);

  const double *in_mixmw = fa_mixmw.raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!in_mixmw || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;

  double *out_therm = regions[0].get_field_accessor(FID_RS_THERM_DIFF(0)).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2);
  if(!out_therm || (subrect != my_subgrid_bounds) || offset_mismatch(3, offsets, offsets2)) return false;
  size_t out_therm_stride = (regions[0].get_field_accessor(FID_RS_THERM_DIFF(1)).typeify<double>().raw_rect_ptr<3>(my_subgrid_bounds, subrect, offsets2) - out_therm);
  if (in_yspec_stride != out_therm_stride) return false;

  size_t num_elmts = my_subgrid_bounds.volume();

#ifdef USE_AVX_KERNELS
  avx_thermal(in_temp, in_yspec, in_mixmw, num_elmts, in_yspec_stride, out_therm);
  _mm256_zeroall();
  return true;
#else
#ifdef USE_SSE_KERNELS
  sse_thermal(in_temp, in_yspec, in_mixmw, num_elmts, in_yspec_stride, out_therm);
  return true;
#else
  return false;
#endif
#endif
}

/*static*/
void CalcThermalTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  if (S3DRank::get_show_progress())
    printf("%s task\n", "CPU thermal"); 

  RegionAccessor<AccessorType::Generic,double> fa_mixmw = regions[2].get_field_accessor(FID_MIXMW).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_temp = regions[1].get_field_accessor(FID_TEMP).typeify<double>();

  if (!dense_thermal(subgrid_bounds, reqs, regions, fa_mixmw, fa_temp))
  {
    double *yspecs = (double *)alloca(rank->n_spec * sizeof(double));
    double *thermal = (double *)alloca(rank->n_spec * sizeof(double));
    for (GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++)
    {
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double temperature = fa_temp.read(dp);

      for (int i = 0; i < rank->n_spec; i++)
        yspecs[i] = regions[2].get_field_accessor(FID_YSPECIES(i)).typeify<double>().read(dp);

      double mixmw = fa_mixmw.read(dp);

      base_thermal(temperature, yspecs, mixmw, thermal);

      for (int i = 0; i < rank->n_spec; i++)
        regions[0].get_field_accessor(FID_RS_THERM_DIFF(i)).typeify<double>().write(dp, thermal[i]);
    }
  }
#endif
}

/*static*/
void CalcThermalTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &my_subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  if (S3DRank::get_show_progress())
    printf("%s task\n", "GPU thermal"); 

  // Assumes contiguous species values
  RegionAccessor<AccessorType::Generic,double> thermal = regions[0].get_field_accessor(FID_RS_THERM_DIFF(0)).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> mixmw = regions[2].get_field_accessor(FID_MIXMW).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> temperature = regions[1].get_field_accessor(FID_TEMP).typeify<double>();
  // Assumes contiguous species values
  RegionAccessor<AccessorType::Generic,double> species = regions[2].get_field_accessor(FID_YSPECIES(0)).typeify<double>();

  Rect<3> bounds = my_subgrid_bounds;
  Rect<3> subrect;
  ByteOffset visc_offsets[3];
  double *therm_ptr = thermal.raw_rect_ptr<3>(bounds, subrect, visc_offsets);
  assert(therm_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset temp_offsets[3];
  double *temp_ptr = temperature.raw_rect_ptr<3>(bounds, subrect, temp_offsets);
  assert(temp_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset spec_offsets[3];
  double *spec_ptr = species.raw_rect_ptr<3>(bounds, subrect, spec_offsets);
  assert(spec_ptr != NULL);
  assert(subrect == bounds);
  ByteOffset mixmw_offsets[3];
  double *mixmw_ptr = mixmw.raw_rect_ptr<3>(bounds, subrect, mixmw_offsets);
  assert(mixmw_ptr != NULL);
  assert(subrect == bounds);

  run_gpu_thermal(temp_ptr, spec_ptr, mixmw_ptr, bounds.dim_size(0), 
                  bounds.dim_size(1), bounds.dim_size(2), therm_ptr);
#endif
#endif
}

