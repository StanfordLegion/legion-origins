
#include "calc_temp.h"
#include "check_field.h"

// Include Singe emitted header files
#include "getcoeffs.h"
#include "getrates.h"

#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
#include <x86intrin.h>
#endif

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
static inline __m128d _mm_stream_load_pd(const double *ptr) 
{
#ifdef __SSE4_1__
  return _mm_castsi128_pd(_mm_stream_load_si128(reinterpret_cast<__m128i *>(const_cast<double *>(ptr))));
#else
  return *(const __m128d *)ptr;
#endif
}
#endif

#ifdef USE_AVX_KERNELS
template<unsigned BOUNDARY>
static inline bool aligned(const void *ptr)
{
  return ((((unsigned long)ptr) & (BOUNDARY-1)) == 0);
}
#endif

CalcTempTask::CalcTempTask(S3DRank *r,
                           Domain domain,
                           TaskArgument global_arg,
                           ArgumentMap arg_map,
                           Predicate pred,
                           bool must,
                           MapperID id,
                           MappingTagID tag,
                           bool add_requirements)
 : IndexLauncher(CalcTempTask::TASK_ID, domain, global_arg, 
                 arg_map, pred, must, id, tag | CalcTempTask::MAPPER_TAG), rank(r)
{
  // FID_CPMIX is neede for scalar transport and for compression
  calc_cpmix = (rank->n_scalar > 0) || (rank->compression != 0);

  if (add_requirements)
  {
    // we'll read/write the temp(erature) field, and write a bunch of others
    RegionRequirement rr_tmp(rank->lp_state_top, 0, READ_WRITE, EXCLUSIVE, rank->lr_state);
    rr_tmp.add_field(FID_TEMP);
    add_region_requirement(rr_tmp);

    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    rr_out.add_field(FID_PRESSURE);
    if(calc_cpmix)
      rr_out.add_field(FID_CPMIX);
    add_region_requirement(rr_out);

    // from q, we need everything!
    RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
    rr_q.add_field(FID_RHO_U);
    rr_q.add_field(FID_RHO_V);
    rr_q.add_field(FID_RHO_W);
    rr_q.add_field(FID_RHO);
    rr_q.add_field(FID_RHO_E);
    for(int i = 0; i < rank->n_spec - 1; i++)
      rr_q.add_field(FID_RHO_Y(i));
    add_region_requirement(rr_q);
  }
}

void CalcTempTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  check_field(runtime, ctx, rank->proc_grid_bounds, rank->is_grid, rank->ip_top, rank->lr_state, FID_TEMP,
              rank, &S3DRank::RHSFArrays::temperature, 0, 1e-10, 1e10, 0, "temp");
  check_field(runtime, ctx, rank->proc_grid_bounds, rank->is_grid, rank->ip_top, rank->lr_int, FID_PRESSURE,
              rank, &S3DRank::RHSFArrays::pressure, 0, 1e-10, 1e10, 0, "pressure");
  if(calc_cpmix)
    check_field(runtime, ctx, rank->proc_grid_bounds, rank->is_grid, rank->ip_top, rank->lr_int, FID_CPMIX,
                rank, &S3DRank::RHSFArrays::cpmix, 0, 1e-10, 1e10, 0, "cpmix");
}

/*static*/ const char * const CalcTempTask::TASK_NAME = "calc_temp";

extern
void calc_temps(const double *in_rho_vx, const double *in_rho_vy, const double *in_rho_vz,
                const double *in_rho, const double *in_rho_e, const double *in_rho_yspec,
                double *inout_temp,
                double *out_pressure,
                double *out_cpmix,
                size_t count, size_t elem_stride, size_t spec_stride);

#ifdef USE_GPU_KERNELS
extern
void calc_temps_gpu(const double *in_rho_vx, const double *in_rho_vy, const double *in_rho_vz,
                    const double *in_rho, const double *in_rho_e, const double *in_rho_yspec,
                    double *inout_temp,
                    double *out_pressure,
                    double *out_cpmix,
                    size_t count_x, size_t count_y, size_t count_z,
                    size_t stride_x, size_t stride_y, size_t stride_z, size_t spec_stride);
#endif

/*static*/
bool CalcTempTask::fast_calc_temp_task(RegionAccessor<AccessorType::Generic,double> fa_in_rho,
                                       RegionAccessor<AccessorType::Generic,double> fa_in_rho_e,
                                       RegionAccessor<AccessorType::Generic,double> fa_in_rho_vel_x,
                                       RegionAccessor<AccessorType::Generic,double> fa_in_rho_vel_y,
                                       RegionAccessor<AccessorType::Generic,double> fa_in_rho_vel_z,
                                       PhysicalRegion ra_in_rho_yspecies,
                                       RegionAccessor<AccessorType::Generic,double> fa_io_temp,
                                       RegionAccessor<AccessorType::Generic,double> fa_out_pressure,
                                       RegionAccessor<AccessorType::Generic,double> fa_out_cpmix,
                                       int n_spec,
                                       Rect<3> subgrid_bounds)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3], offsets[3];

  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds)) return false;

  const double *in_rho_e_ptr = fa_in_rho_e.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_e_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_rho_vel_x_ptr = fa_in_rho_vel_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_vel_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_rho_vel_y_ptr = fa_in_rho_vel_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_vel_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_rho_vel_z_ptr = fa_in_rho_vel_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_vel_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_rho_yspecies_ptr = ra_in_rho_yspecies.get_field_accessor(FID_RHO_Y(0)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_yspecies_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
  ByteOffset rho_yspecies_stride;
  for(int i = 1; i < n_spec - 1; i++) {
    double *in_rho_yspecies_i_ptr = ra_in_rho_yspecies.get_field_accessor(FID_RHO_Y(i)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_rho_yspecies_i_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
    ByteOffset rho_yspecies_i_stride(in_rho_yspecies_i_ptr, in_rho_yspecies_ptr);
    if(i == 1) {
      rho_yspecies_stride = rho_yspecies_i_stride;
    } else {
      if(rho_yspecies_i_stride != (rho_yspecies_stride * i)) {
        printf("yspecies stride mismatch\n");
        return 0;
      }
    }
  }

  double *io_temp_ptr = fa_io_temp.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!io_temp_ptr || (subrect != subgrid_bounds)) return false;

  double *out_pressure_ptr = fa_out_pressure.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_pressure_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  if(!offsets_are_dense<3, double>(subgrid_bounds, in_offsets) ||
       offset_mismatch(3, in_offsets, out_offsets))
      return false;

  double *out_cpmix_ptr = 0;
  if(fa_out_cpmix.valid()) {
    out_cpmix_ptr = fa_out_cpmix.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_cpmix_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

    if(!offsets_are_dense<3, double>(subgrid_bounds, in_offsets) ||
         offset_mismatch(3, in_offsets, out_offsets))
        return false;
  }
  
  calc_temps(in_rho_vel_x_ptr, in_rho_vel_y_ptr, in_rho_vel_z_ptr, 
             in_rho_ptr, in_rho_e_ptr, in_rho_yspecies_ptr,
             io_temp_ptr, out_pressure_ptr, out_cpmix_ptr,
             subgrid_bounds.volume(), 1, rho_yspecies_stride.offset / sizeof(double));

  return true;
}

/*static*/
void CalcTempTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                 const std::vector<RegionRequirement> &reqs,
                                 const std::vector<PhysicalRegion> &regions,
                                 Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  RegionAccessor<AccessorType::Generic,double> fa_temp = regions[0].get_field_accessor(FID_TEMP).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_pressure = regions[1].get_field_accessor(FID_PRESSURE).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_cpmix;
  if(task->regions[1].instance_fields.size() >= 2)
    fa_cpmix = regions[1].get_field_accessor(FID_CPMIX).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_rho = regions[2].get_field_accessor(FID_RHO).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_rho_e = regions[2].get_field_accessor(FID_RHO_E).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_rho_vel_x = regions[2].get_field_accessor(FID_RHO_U).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_rho_vel_y = regions[2].get_field_accessor(FID_RHO_V).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_rho_vel_z = regions[2].get_field_accessor(FID_RHO_W).typeify<double>();

  if(fast_calc_temp_task(fa_rho, fa_rho_e, fa_rho_vel_x, fa_rho_vel_y, fa_rho_vel_z,
                         regions[2],
                         fa_temp, fa_pressure, fa_cpmix,
                         rank->n_spec, subgrid_bounds)) {
    //printf("fast calc temp\n");
    return;
  }

  double *rho_yspecs = (double *)alloca((rank->n_spec - 1) * sizeof(double));

  for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double rho = fa_rho.read(dp);
    double rho_e = fa_rho_e.read(dp);
    double rho_vx = fa_rho_vel_x.read(dp);
    double rho_vy = fa_rho_vel_y.read(dp);
    double rho_vz = fa_rho_vel_z.read(dp);

    for(int i = 0; i < rank->n_spec - 1; i++)
      rho_yspecs[i] = regions[2].get_field_accessor(FID_RHO_Y(i)).typeify<double>().read(dp);

    // read the temp from the previous rhsf step as our starting point
    double temp = fa_temp.read(dp);
    double pressure;
    double cpmix;

    // do a single point
    calc_temps(&rho_vx, &rho_vy, &rho_vz, &rho, &rho_e, rho_yspecs,
               &temp, &pressure, &cpmix, 1, 0, 1);

    fa_temp.write(dp, temp);
    fa_pressure.write(dp, pressure); 
    if(fa_cpmix.valid())
      fa_cpmix.write(dp, cpmix);
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern
void gpu_check_temperature(const int max_elements,
                           const double min_temp,
                           const double max_temp,
                           const double *temp_ptr,
                           const int rank_x,
                           const int rank_y,
                           const int rank_z);
#endif

/*static*/
void CalcTempTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                 const std::vector<RegionRequirement> &reqs,
                                 const std::vector<PhysicalRegion> &regions,
                                 Context ctx, HighLevelRuntime *runtime)
{
#ifdef USE_GPU_KERNELS
#ifndef NO_COMPUTE
  RegionAccessor<AccessorType::Generic,double> fa_io_temp = regions[0].get_field_accessor(FID_TEMP).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_out_pressure = regions[1].get_field_accessor(FID_PRESSURE).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_in_rho = regions[2].get_field_accessor(FID_RHO).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_rho_e = regions[2].get_field_accessor(FID_RHO_E).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_in_rho_vel_x = regions[2].get_field_accessor(FID_RHO_U).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_rho_vel_y = regions[2].get_field_accessor(FID_RHO_V).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_rho_vel_z = regions[2].get_field_accessor(FID_RHO_W).typeify<double>();

  

  PhysicalRegion ra_in_rho_yspecies = regions[2];
  const int n_spec = rank->n_spec;

  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3], offsets[3];

  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds)) assert(false);

  const double *in_rho_e_ptr = fa_in_rho_e.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_e_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const double *in_rho_vel_x_ptr = fa_in_rho_vel_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_vel_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const double *in_rho_vel_y_ptr = fa_in_rho_vel_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_vel_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const double *in_rho_vel_z_ptr = fa_in_rho_vel_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_vel_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

  const double *in_rho_yspecies_ptr = ra_in_rho_yspecies.get_field_accessor(FID_RHO_Y(0)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_yspecies_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
  ByteOffset rho_yspecies_stride;
  for(int i = 1; i < n_spec - 1; i++) {
    double *in_rho_yspecies_i_ptr = ra_in_rho_yspecies.get_field_accessor(FID_RHO_Y(i)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_rho_yspecies_i_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
    ByteOffset rho_yspecies_i_stride(in_rho_yspecies_i_ptr, in_rho_yspecies_ptr);
    if(i == 1) {
      rho_yspecies_stride = rho_yspecies_i_stride;
    } else {
      if(rho_yspecies_i_stride != (rho_yspecies_stride * i)) {
        printf("yspecies stride mismatch\n");
        assert(false);
      }
    }
  }

  double *io_temp_ptr = fa_io_temp.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!io_temp_ptr || (subrect != subgrid_bounds)) assert(false);

  double *out_pressure_ptr = fa_out_pressure.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_pressure_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

  double *out_cpmix_ptr = 0;
  RegionAccessor<AccessorType::Generic,double> fa_cpmix;
  if(task->regions[1].instance_fields.size() >= 2) {
    out_cpmix_ptr = regions[1].get_field_accessor(FID_CPMIX).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_cpmix_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);
  }

  calc_temps_gpu(in_rho_vel_x_ptr, in_rho_vel_y_ptr, in_rho_vel_z_ptr, 
                 in_rho_ptr, in_rho_e_ptr, in_rho_yspecies_ptr,
                 io_temp_ptr, out_pressure_ptr, out_cpmix_ptr,
                 (subgrid_bounds.hi[0] - subgrid_bounds.lo[0] + 1),
                 (subgrid_bounds.hi[1] - subgrid_bounds.lo[1] + 1),
                 (subgrid_bounds.hi[2] - subgrid_bounds.lo[2] + 1),
                 out_offsets[0].offset / sizeof(double),
                 out_offsets[1].offset / sizeof(double),
                 out_offsets[2].offset / sizeof(double),
                 rho_yspecies_stride.offset / sizeof(double));

#ifdef RUN_CHECKS
  const double temp_hi = 3500.0 / T_REF;
  const double temp_lo = 250.0 / T_REF;
  gpu_check_temperature(subgrid_bounds.volume(),
                        temp_lo, temp_hi, io_temp_ptr,
                        rank->my_proc_id[0], rank->my_proc_id[1], rank->my_proc_id[2]);
#endif
  
#endif
#endif
}

///////////////////////////////////////////////////////////////

CalcSpeciesTask::CalcSpeciesTask(S3DRank *r,
                                 Domain domain,
                                 TaskArgument global_arg,
                                 ArgumentMap arg_map,
                                 Predicate pred,
                                 bool must,
                                 MapperID id,
                                 MappingTagID tag,
                                 bool add_requirements,
                                 bool redundant)
 : IndexLauncher(CalcSpeciesTask::TASK_ID, domain, global_arg, arg_map, pred, must,
                 id, tag | CalcSpeciesTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
#ifdef REDUNDANT_WORK
    if (redundant) {
      rr_out.add_field(FID_AVMOLWT_ALT);
      rr_out.add_field(FID_MIXMW_ALT);
      for (int i = 0; i < rank->n_spec; i++)
        rr_out.add_field(FID_YSPECIES_ALT(i));
      this->tag |= (RHSF_MAPPER_ALL_GPU | RHSF_MAPPER_MIXED_GPU);
    } else {
      rr_out.add_field(FID_AVMOLWT);
      rr_out.add_field(FID_MIXMW);
      for (int i = 0; i < rank->n_spec; i++)
        rr_out.add_field(FID_YSPECIES(i));
    }
#else
    assert(!redundant);
    rr_out.add_field(FID_AVMOLWT);
    rr_out.add_field(FID_MIXMW);
    for (int i = 0; i < rank->n_spec; i++)
      rr_out.add_field(FID_YSPECIES(i));
#endif
    add_region_requirement(rr_out);

    RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
    rr_q.add_field(FID_RHO);
    for(int i = 0; i < rank->n_spec - 1; i++)
      rr_q.add_field(FID_RHO_Y(i));
    add_region_requirement(rr_q);
  }
}

void CalcSpeciesTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  for(int s = 0; s < rank->n_spec; s++)
    check_field(runtime, ctx, rank->proc_grid_bounds, 
                rank->is_grid, rank->ip_top, rank->lr_int, FID_YSPECIES(s),
                rank, &S3DRank::RHSFArrays::yspecies, s, 0, 1, 0, "yspecies[%d]", s);
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_AVMOLWT,
              rank, &S3DRank::RHSFArrays::avmolwt, 0, 1e-10, 1e10, 0, "avmolwt");
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_MIXMW,
              rank, &S3DRank::RHSFArrays::mixmw, 0, 1e-10, 1e10, 0, "mixmw");
}

/*static*/
const char * const CalcSpeciesTask::TASK_NAME = "calc_species_task";

/*static*/
bool CalcSpeciesTask::dense_calc_species(S3DRank *rank, const Rect<3> &subgrid_bounds,
                                         RegionAccessor<AccessorType::Generic,double> fa_in_rho,
                                         RegionAccessor<AccessorType::Generic,double> fa_out_avmolwt,
                                         RegionAccessor<AccessorType::Generic,double> fa_out_mixmw,
                                         const std::vector<RegionRequirement> &reqs,
                                         const std::vector<PhysicalRegion> &regions)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3], offsets[3];

  PhysicalRegion ra_in_rho_yspecies = regions[1];
  PhysicalRegion ra_out_yspec = regions[0];

  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds)) return false;

  const double *in_rho_yspecies_ptr = ra_in_rho_yspecies.get_field_accessor(FID_RHO_Y(0)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_yspecies_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
  ByteOffset rho_y_stride;
  for(int i = 1; i < rank->n_spec - 1; i++) {
    double *in_rho_yspecies_i_ptr = ra_in_rho_yspecies.get_field_accessor(FID_RHO_Y(i)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_rho_yspecies_i_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;
    ByteOffset rho_yspecies_i_stride(in_rho_yspecies_i_ptr, in_rho_yspecies_ptr);
    if(i == 1) {
      rho_y_stride = rho_yspecies_i_stride;
    } else {
      if(rho_yspecies_i_stride != (rho_y_stride * i)) {
        printf("yspecies stride mismatch\n");
        return 0;
      }
    }
  }

  double *out_avmolwt_ptr = fa_out_avmolwt.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_avmolwt_ptr || (subrect != subgrid_bounds)) return false;

  double *out_mixmw_ptr = fa_out_mixmw.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_mixmw_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *out_yspec_ptr = ra_out_yspec.get_field_accessor(reqs[0].instance_fields[2]).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_yspec_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;
  ByteOffset yspec_stride;
  for(int i = 1; i < rank->n_spec; i++) {
    double *out_yspec_i_ptr = ra_out_yspec.get_field_accessor(reqs[0].instance_fields[2+i]).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_yspec_i_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;
    ByteOffset yspec_i_stride(out_yspec_i_ptr, out_yspec_ptr);
    if(i == 1) {
      yspec_stride = yspec_i_stride;
    } else {
      if(yspec_i_stride != (yspec_stride * i)) {
        printf("yspec stride mismatch\n");
        return 0;
      }
    }
  }

  if(!offsets_are_dense<3, double>(subgrid_bounds, in_offsets) ||
       offset_mismatch(3, in_offsets, out_offsets))
      return false;

  size_t n_pts = subgrid_bounds.volume();
  const int n_spec = rank->n_spec;

#ifdef BULLDOZER
// Block for L1, only consume the first
// way in each set
#define FULL_STRIP_SIZE 128 
#else
#define FULL_STRIP_SIZE 1024
#endif

#if defined(USE_AVX_KERNELS)
  __m256d vol[FULL_STRIP_SIZE/4];
  __m256d yspec_last[FULL_STRIP_SIZE/4];
  __m256d avmolwt[FULL_STRIP_SIZE/4];
#elif defined(USE_SSE_KERNELS)
  __m128d vol[FULL_STRIP_SIZE/2];
  __m128d yspec_last[FULL_STRIP_SIZE/2];
  __m128d avmolwt[FULL_STRIP_SIZE/2];
#endif
  double full_vol[FULL_STRIP_SIZE];

  double *out_yspec_last = out_yspec_ptr;
  out_yspec_last += ByteOffset((off_t)((n_spec - 1) * yspec_stride.offset));

  while(n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE)
    {
#if defined(USE_AVX_KERNELS)
      if (aligned<32>(out_avmolwt_ptr) && aligned<32>(out_mixmw_ptr) &&
          aligned<32>(out_yspec_ptr) && aligned<32>(in_rho_ptr)) {
        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          vol[i] = _mm256_div_pd(_mm256_set1_pd(1.0),_mm256_load_pd(in_rho_ptr+(i<<2)));
        }

        // loop over species
        const double *in_rho_y_s = in_rho_yspecies_ptr;
        double *out_yspec_s = out_yspec_ptr;

        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d yspec = _mm256_mul_pd(vol[i],_mm256_load_pd(in_rho_y_s+(i<<2)));
          _mm256_stream_pd(out_yspec_s+(i<<2),yspec);
          yspec_last[i] = _mm256_sub_pd(_mm256_set1_pd(1.0),yspec);
          avmolwt[i] = _mm256_mul_pd(yspec,_mm256_set1_pd(recip_molecular_masses[0]));
        }

        for(int s = 1; s < (n_spec - 1); s++) {
          in_rho_y_s += (rho_y_stride);
          out_yspec_s += (yspec_stride);

          for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d yspec = _mm256_mul_pd(vol[i],_mm256_load_pd(in_rho_y_s+(i<<2)));
            _mm256_stream_pd(out_yspec_s+(i<<2),yspec);
            yspec_last[i] = _mm256_sub_pd(yspec_last[i],yspec);
            avmolwt[i] = _mm256_add_pd(avmolwt[i],_mm256_mul_pd(yspec,_mm256_set1_pd(recip_molecular_masses[s])));
          }
        }

        // last species is N2, for which we've derived yspec
        // also write out avmolwt
        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          avmolwt[i] = _mm256_mul_pd(_mm256_set1_pd(1000.0),_mm256_add_pd(avmolwt[i],
                _mm256_mul_pd(yspec_last[i],_mm256_set1_pd(recip_molecular_masses[n_spec-1]))));
          _mm256_stream_pd(out_yspec_last+(i<<2),yspec_last[i]);
          _mm256_stream_pd(out_avmolwt_ptr+(i<<2),avmolwt[i]);
          _mm256_stream_pd(out_mixmw_ptr+(i<<2),_mm256_div_pd(_mm256_set1_pd(1.0),avmolwt[i]));
        }
      }
      else {
        // handle the unaligned case
        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          vol[i] = _mm256_div_pd(_mm256_set1_pd(1.0),_mm256_loadu_pd(in_rho_ptr+(i<<2)));
        }

        // loop over species
        const double *in_rho_y_s = in_rho_yspecies_ptr;
        double *out_yspec_s = out_yspec_ptr;

        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d yspec = _mm256_mul_pd(vol[i],_mm256_loadu_pd(in_rho_y_s+(i<<2)));
          _mm256_storeu_pd(out_yspec_s+(i<<2),yspec);
          yspec_last[i] = _mm256_sub_pd(_mm256_set1_pd(1.0),yspec);
          avmolwt[i] = _mm256_mul_pd(yspec,_mm256_set1_pd(recip_molecular_masses[0]));
        }

        for(int s = 1; s < (n_spec - 1); s++) {
          in_rho_y_s += (rho_y_stride);
          out_yspec_s += (yspec_stride);

          for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
            __m256d yspec = _mm256_mul_pd(vol[i],_mm256_loadu_pd(in_rho_y_s+(i<<2)));
            _mm256_storeu_pd(out_yspec_s+(i<<2),yspec);
            yspec_last[i] = _mm256_sub_pd(yspec_last[i],yspec);
            avmolwt[i] = _mm256_add_pd(avmolwt[i],_mm256_mul_pd(yspec,_mm256_set1_pd(recip_molecular_masses[s])));
          }
        }

        // last species is N2, for which we've derived yspec
        // also write out avmolwt
        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          avmolwt[i] = _mm256_mul_pd(_mm256_set1_pd(1000.0),_mm256_add_pd(avmolwt[i],
                _mm256_mul_pd(yspec_last[i],_mm256_set1_pd(recip_molecular_masses[n_spec-1]))));
          _mm256_storeu_pd(out_yspec_last+(i<<2),yspec_last[i]);
          _mm256_storeu_pd(out_avmolwt_ptr+(i<<2),avmolwt[i]);
          _mm256_storeu_pd(out_mixmw_ptr+(i<<2),_mm256_div_pd(_mm256_set1_pd(1.0),avmolwt[i]));
        }
      }
#elif defined(USE_SSE_KERNELS)
      for(size_t i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        vol[i] = _mm_div_pd(_mm_set1_pd(1.0),_mm_stream_load_pd(in_rho_ptr+(i<<1)));
      }

      // loop over species
      const double *in_rho_y_s = in_rho_yspecies_ptr;
      double *out_yspec_s = out_yspec_ptr;

      for(size_t i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d yspec = _mm_mul_pd(vol[i],_mm_stream_load_pd(in_rho_y_s+(i<<1)));
        _mm_stream_pd(out_yspec_s+(i<<1),yspec);
        yspec_last[i] = _mm_sub_pd(_mm_set1_pd(1.0),yspec);
        avmolwt[i] = _mm_mul_pd(yspec,_mm_set1_pd(recip_molecular_masses[0]));
      }

      for(int s = 1; s < (n_spec - 1); s++) {
        in_rho_y_s += (rho_y_stride);
        out_yspec_s += (yspec_stride);

        for(size_t i = 0; i < (FULL_STRIP_SIZE/2); i++) {
          __m128d yspec = _mm_mul_pd(vol[i],_mm_stream_load_pd(in_rho_y_s+(i<<1)));
          _mm_stream_pd(out_yspec_s+(i<<1),yspec);
          yspec_last[i] = _mm_sub_pd(yspec_last[i],yspec);
          avmolwt[i] = _mm_add_pd(avmolwt[i],_mm_mul_pd(yspec,_mm_set1_pd(recip_molecular_masses[s])));
        }
      }

      // last species is N2, for which we've derived yspec
      // also write out avmolwt
      for(size_t i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        avmolwt[i] = _mm_mul_pd(_mm_set1_pd(1000.0),_mm_add_pd(avmolwt[i],
              _mm_mul_pd(yspec_last[i],_mm_set1_pd(recip_molecular_masses[n_spec-1]))));
        _mm_stream_pd(out_yspec_last+(i<<1),yspec_last[i]);
        _mm_stream_pd(out_avmolwt_ptr+(i<<1),avmolwt[i]);
        _mm_stream_pd(out_mixmw_ptr+(i<<1),_mm_div_pd(_mm_set1_pd(1.0),avmolwt[i]));
      }
#else
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) full_vol[i] = 1.0 / in_rho_ptr[i];

      // loop over species
      const double *in_rho_y_s = in_rho_yspecies_ptr;
      double *out_yspec_s = out_yspec_ptr;

      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
        double yspec = full_vol[i] * in_rho_y_s[i];
        out_yspec_s[i] = yspec;
        out_yspec_last[i] = 1.0 - yspec;
        out_avmolwt_ptr[i] = yspec * recip_molecular_masses[0];
      }

      for(int s = 1; s < (n_spec - 1); s++) {
        in_rho_y_s += (rho_y_stride);
        out_yspec_s += (yspec_stride);

        for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
          double yspec = full_vol[i] * in_rho_y_s[i];
          out_yspec_s[i] = yspec;
          out_yspec_last[i] -= yspec;
          out_avmolwt_ptr[i] += yspec * recip_molecular_masses[s];
        }
      }

      // last species is N2, for which we've derived yspec
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) {
        double yspec_n2 = out_yspec_last[i];
        double avmolwt = 1000.0 * (out_avmolwt_ptr[i] + yspec_n2 * recip_molecular_masses[n_spec - 1]);
        out_avmolwt_ptr[i] = avmolwt;
        out_mixmw_ptr[i] = 1.0 / avmolwt;
      }
#endif
      n_pts -= FULL_STRIP_SIZE;
    }
    else
    {
      // Slow case
      for(size_t i = 0; i < n_pts; i++) full_vol[i] = 1.0 / in_rho_ptr[i];

      // loop over species
      const double *in_rho_y_s = in_rho_yspecies_ptr;
      double *out_yspec_s = out_yspec_ptr;

      for(size_t i = 0; i < n_pts; i++) {
        double yspec = full_vol[i] * in_rho_y_s[i];
        out_yspec_s[i] = yspec;
        out_yspec_last[i] = 1.0 - yspec;
        out_avmolwt_ptr[i] = yspec * recip_molecular_masses[0];
      }

      for(int s = 1; s < (n_spec - 1); s++) {
        in_rho_y_s += (rho_y_stride);
        out_yspec_s += (yspec_stride);

        for(size_t i = 0; i < n_pts; i++) {
          double yspec = full_vol[i] * in_rho_y_s[i];
          out_yspec_s[i] = yspec;
          out_yspec_last[i] -= yspec;
          out_avmolwt_ptr[i] += yspec * recip_molecular_masses[s];
        }
      }

      // last species is N2, for which we've derived yspec
      for(size_t i = 0; i < n_pts; i++) {
        double yspec_n2 = out_yspec_last[i];
        double avmolwt = 1000.0 * (out_avmolwt_ptr[i] + yspec_n2 * recip_molecular_masses[n_spec - 1]);
        out_avmolwt_ptr[i] = avmolwt;
        out_mixmw_ptr[i] = 1.0 / avmolwt;
      }
      n_pts = 0;
    }

    in_rho_ptr += FULL_STRIP_SIZE;
    in_rho_yspecies_ptr += FULL_STRIP_SIZE;
    out_avmolwt_ptr += FULL_STRIP_SIZE;
    out_mixmw_ptr += FULL_STRIP_SIZE;
    out_yspec_ptr += FULL_STRIP_SIZE;
    out_yspec_last += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
}

#ifdef USE_GPU_KERNELS
extern
void calc_species_gpu(size_t n_pts, size_t n_spec,
                      const double *in_rho,
                      const double *in_rho_y,
                      const int rho_y_stride,
                      double *out_avmolwt,
                      double *out_mixmw,
                      double *out_yspec,
                      const int yspec_stride);
#endif

/*static*/
void CalcSpeciesTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                    const std::vector<RegionRequirement> &reqs,
                                    const std::vector<PhysicalRegion> &regions,
                                    Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  RegionAccessor<AccessorType::Generic,double> fa_rho = regions[1].get_field_accessor(FID_RHO).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_avmolwt = regions[0].get_field_accessor(reqs[0].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_mixmw = regions[0].get_field_accessor(reqs[0].instance_fields[1]).typeify<double>();

  if (dense_calc_species(rank, subgrid_bounds, fa_rho, fa_avmolwt, fa_mixmw, reqs, regions))
    return;

  double *rho_yspecs = (double *)alloca((rank->n_spec - 1) * sizeof(double));

  for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double rho = fa_rho.read(dp);

    for(int i = 0; i < rank->n_spec - 1; i++)
      rho_yspecs[i] = regions[1].get_field_accessor(FID_RHO_Y(i)).typeify<double>().read(dp);

    double volume = 1.0 / rho;
    double yspec_n2 = 1.0;
    double avmolwt = 0.0;
    for (int i = 0; i < (rank->n_spec-1); i++) {
      double yspec = volume * rho_yspecs[i];
      regions[0].get_field_accessor(reqs[0].instance_fields[2+i]).typeify<double>().write(dp, yspec);
      yspec_n2 -= yspec;
      avmolwt += yspec * recip_molecular_masses[i];
    }
    regions[0].get_field_accessor(reqs[0].instance_fields[2+rank->n_spec-1]).typeify<double>().write(dp, yspec_n2);

    avmolwt += yspec_n2 * recip_molecular_masses[rank->n_spec-1];
    avmolwt *= 1000.0;
    double mixmw = 1.0 / avmolwt;
    fa_avmolwt.write(dp, avmolwt);
    fa_mixmw.write(dp, mixmw);
  }
#endif
}

/*static*/
void CalcSpeciesTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                    const std::vector<RegionRequirement> &reqs,
                                    const std::vector<PhysicalRegion> &regions,
                                    Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  RegionAccessor<AccessorType::Generic,double> fa_in_rho = regions[1].get_field_accessor(FID_RHO).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_out_avmolwt = regions[0].get_field_accessor(reqs[0].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_out_mixmw = regions[0].get_field_accessor(reqs[0].instance_fields[1]).typeify<double>();

  PhysicalRegion ra_in_rho_yspecies = regions[1];
  const int n_spec = rank->n_spec;

  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3], offsets[3];

  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds)) assert(false);

  const double *in_rho_yspecies_ptr = ra_in_rho_yspecies.get_field_accessor(FID_RHO_Y(0)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_yspecies_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
  ByteOffset rho_yspecies_stride;
  for(int i = 1; i < n_spec - 1; i++) {
    double *in_rho_yspecies_i_ptr = ra_in_rho_yspecies.get_field_accessor(FID_RHO_Y(i)).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_rho_yspecies_i_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);
    ByteOffset rho_yspecies_i_stride(in_rho_yspecies_i_ptr, in_rho_yspecies_ptr);
    if(i == 1) {
      rho_yspecies_stride = rho_yspecies_i_stride;
    } else {
      if(rho_yspecies_i_stride != (rho_yspecies_stride * i)) {
        printf("yspecies stride mismatch\n");
        assert(false);
      }
    }
  }

  double *out_avmolwt_ptr = fa_out_avmolwt.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_avmolwt_ptr || (subrect != subgrid_bounds)) assert(false);

  double *out_mixmw_ptr = fa_out_mixmw.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_mixmw_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);

  double *out_yspec_ptr = regions[0].get_field_accessor(reqs[0].instance_fields[2]).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_yspec_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);
  ByteOffset yspec_stride;
  for(int i = 1; i < n_spec; i++) {
    double *out_yspec_i_ptr = regions[0].get_field_accessor(reqs[0].instance_fields[2+i]).typeify<double>().raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_yspec_i_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) assert(false);
    ByteOffset yspec_i_stride(out_yspec_i_ptr, out_yspec_ptr);
    if(i == 1) {
      yspec_stride = yspec_i_stride;
    } else {
      if(yspec_i_stride != (yspec_stride * i)) {
        printf("yspec stride mismatch\n");
        assert(false);
      }
    }
  }

  calc_species_gpu(subgrid_bounds.volume(), rank->n_spec,
                   in_rho_ptr, in_rho_yspecies_ptr, rho_yspecies_stride.offset / sizeof(double),
                   out_avmolwt_ptr, out_mixmw_ptr, out_yspec_ptr, yspec_stride.offset / sizeof(double));
#endif
#endif
}

///////////////////////////////////////////////////////////////

CalcGammaTask::CalcGammaTask(S3DRank *r,
                                 Domain domain,
                                 TaskArgument global_arg,
                                 ArgumentMap arg_map,
                                 Predicate pred,
                                 bool must,
                                 MapperID id,
                                 MappingTagID tag,
                                 bool add_requirements)
 : IndexLauncher(CalcGammaTask::TASK_ID, domain, global_arg, arg_map, pred, must,
                 id, tag | CalcGammaTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    add_region_requirement(RegionRequirement(rank->lp_int_top, 0, 
					     WRITE_DISCARD, EXCLUSIVE, rank->lr_int)
			   .add_field(FID_GAMMA));

    add_region_requirement(RegionRequirement(rank->lp_int_top, 0,
					     READ_ONLY, EXCLUSIVE, rank->lr_int)
			   .add_field(FID_CPMIX)
			   .add_field(FID_AVMOLWT));
  }
}

void CalcGammaTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_GAMMA,
              rank, &S3DRank::RHSFArrays::gamma, 0, 1e-10, 1e10, 0, "gamma");
}

/*static*/
const char * const CalcGammaTask::TASK_NAME = "calc_gamma_task";

/*static*/
bool CalcGammaTask::dense_calc_gamma(S3DRank *rank, const Rect<3> &subgrid_bounds,
				     RegionAccessor<AccessorType::Generic,double> fa_in_cpmix,
				     RegionAccessor<AccessorType::Generic,double> fa_in_avmolwt,
				     RegionAccessor<AccessorType::Generic,double> fa_out_gamma,
				     const std::vector<RegionRequirement> &reqs,
				     const std::vector<PhysicalRegion> &regions)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3], offsets[3];

  const double *in_cpmix_ptr = fa_in_cpmix.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_cpmix_ptr || (subrect != subgrid_bounds)) return false;

  double *in_avmolwt_ptr = fa_in_avmolwt.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_avmolwt_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_gamma_ptr = fa_out_gamma.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_gamma_ptr || (subrect != subgrid_bounds)) return false;

  if(!offsets_are_dense<3, double>(subgrid_bounds, in_offsets) ||
       offset_mismatch(3, in_offsets, out_offsets))
      return false;

  size_t n_pts = subgrid_bounds.volume();

  const double Ru = 8.31451 / CP_REF;
  for(size_t i = 0; i < n_pts; i++) {
    double cpmix = in_cpmix_ptr[i];
    double avmolwt = in_avmolwt_ptr[i];
    double gamma = cpmix / (cpmix - avmolwt * Ru);
    out_gamma_ptr[i] = gamma;
  }

  return true;
}

#ifdef USE_GPU_KERNELS
extern
void calc_gamma_gpu(size_t n_pts,
		    const double *in_cpmix, const double *in_avmolwt,
		    double *out_gamma);
#endif

/*static*/
void CalcGammaTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
				  const std::vector<RegionRequirement> &reqs,
				  const std::vector<PhysicalRegion> &regions,
				  Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  RegionAccessor<AccessorType::Generic,double> fa_cpmix = regions[1].get_field_accessor(FID_CPMIX).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_avmolwt = regions[1].get_field_accessor(FID_AVMOLWT).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_gamma = regions[0].get_field_accessor(FID_GAMMA).typeify<double>();

  if (dense_calc_gamma(rank, subgrid_bounds, fa_cpmix, fa_avmolwt, fa_gamma, reqs, regions))
    return;

  const double Ru = 8.31451 / CP_REF;
  for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double cpmix = fa_cpmix.read(dp);
    double avmolwt = fa_avmolwt.read(dp);
    double gamma = cpmix / (cpmix - avmolwt * Ru);
    fa_gamma.write(dp, gamma);
  }
#endif
}

/*static*/
void CalcGammaTask::gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                    const std::vector<RegionRequirement> &reqs,
                                    const std::vector<PhysicalRegion> &regions,
                                    Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  RegionAccessor<AccessorType::Generic,double> fa_in_cpmix = regions[1].get_field_accessor(FID_CPMIX).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_avmolwt = regions[1].get_field_accessor(FID_AVMOLWT).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_out_gamma = regions[0].get_field_accessor(FID_GAMMA).typeify<double>();

  Rect<3> subrect;
  ByteOffset offsets[3];
  const double *in_cpmix_ptr = fa_in_cpmix.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  assert(in_cpmix_ptr && (subrect == subgrid_bounds) &&
	 (offsets_are_dense<3, double>(subgrid_bounds, offsets)));

  double *in_avmolwt_ptr = fa_in_avmolwt.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  assert(in_avmolwt_ptr && (subrect == subgrid_bounds) &&
	 (offsets_are_dense<3, double>(subgrid_bounds, offsets)));

  double *out_gamma_ptr = fa_out_gamma.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  assert(out_gamma_ptr && (subrect == subgrid_bounds) &&
	 (offsets_are_dense<3, double>(subgrid_bounds, offsets)));

  size_t n_pts = subgrid_bounds.volume();

  calc_gamma_gpu(n_pts, in_cpmix_ptr, in_avmolwt_ptr, out_gamma_ptr);
#endif
#endif
}
