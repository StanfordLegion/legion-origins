#ifndef GPU_STENCILS_H
#define GPU_STENCILS_H

void fast_gpu_stencil(int dim_1, int dim_2, int dim_3, double ds,
		      const double *minus_ptr, size_t minus_stride_1, size_t minus_stride_2, size_t minus_stride_3,
		      const double *in_ptr, size_t in_stride_1, size_t in_stride_2, size_t in_stride_3,
		      const double *plus_ptr, size_t plus_stride_1, size_t plus_stride_2, size_t plus_stride_3,
		      double *out_ptr, size_t out_stride_1, size_t out_stride_2, size_t out_stride_3);

#endif
