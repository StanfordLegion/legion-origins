
#ifndef _RHSF_H_
#define _RHSF_H_

#define NEW_BARRIER_EXCHANGE
#define REMOVE_OLD_BARRIER_EXCHANGE
#define CEMA_SORT_ON_LAST_NODE

#include "legion.h"
#include "arrays.h"
// Check for GETRATES_NEEDS_DIFFUSION macro
#include "getrates.h"

// Enumeration for Task IDs
enum {
  // rhsf_init tasks
  TOP_LEVEL_TASK_ID,
  CONNECT_MPI_TASK_ID,
  UPDATE_MAPPERS_TASK_ID,
  UPDATE_SUBRANK_BOUNDS_TASK_ID,
  CREATE_GHOST_REGIONS_TASK_ID,
  GATHER_BARRIERS_TASK_ID,
  GHOST_REGIONS_FIXUP_TASK_ID,
  // rhsf_impl tasks
  DISTRIBUTE_TASK_ID,
  GET_INIT_TEMP_TASK_ID,
  AWAIT_MPI_TASK_ID,
  HANDOFF_TO_MPI_TASK_ID,
  // check_field
  CHECK_FIELD_TASK_ID,
  DUMP_FIELD_TASK_ID,
  SUM_FIELD_TASK_ID,
  // calc_stencil
  CALC_STENCIL_1D_TASK_ID,
  // calc_sum
  CALC_SUM_3_TASK_ID,
  CALC_SUM_4_TASK_ID,
  CALC_SUM_3_INTEGRATE_TASK_ID,
  CALC_SUM_4_INTEGRATE_TASK_ID,
  CALC_SCALAR_SUM_6_INTEGRATE_TASK_ID,
  CALC_RHS_VEL_IN_TASK_ID,
  // calc_volume
  CALC_VOLUME_TASK_ID,
  // calc_temp
  CALC_TEMP_TASK_ID,
  CALC_SPECIES_TASK_ID,
  CALC_GAMMA_TASK_ID,
  // calc_physics
  CALC_DIFFUSION_TASK_ID,
  CALC_VISCOSITY_TASK_ID,
  CALC_CONDUCTIVITY_TASK_ID,
  CALC_THERMAL_TASK_ID,
  // calc_flux
  CALC_YDIFFFLUX_TASK_ID,
  CALC_YDIFFFLUX_PER_FIELD_TASK_ID,
  CALC_HEATFLUX_TASK_ID,
  CALC_ENTHALPY_TASK_ID,
  CALC_SHORT_HEATFLUX_TASK_ID,
  CALC_TAU_TASK_ID,
  CALC_SCALAR_FLUX_TASK_ID,
  // calc chem
  CALC_GETRATES_TASK_ID,
  // calc_pre
  CALC_ENERGY_PRECALC_TASK_ID,
  CALC_SPEC_PRECALC_TASK_ID,
  CALC_SPEC_PRECALC_PER_FIELD_TASK_ID,
  CLEAR_FIELD_TASK_ID,
  // impose pressure
  CALC_IMPOSED_PRESSURE_TASK_ID,
  CALC_SOURCE_TERM_TASK_ID,
  CALC_INTEGRATE_SOURCE_TASK_ID,
  // CEMA-related tasks
  SAMPLE_POINTS_TASK_ID,
  CALC_CEMA_TASK_ID,
  SORT_CEMA_TASK_ID,
  // Keep track of the maximum number of task IDs
  MAX_NUM_TASK_ID,
};

// reduction op IDs
enum {
  REDOP_SUM_DOUBLE = LegionRuntime::HighLevel::REDOP_ID_AVAILABLE,
};

// Different trace IDs
enum {
  TRACE_WITH_THERM_COEFFS,
  TRACE_WITHOUT_THERM_COEFFS,
  TRACE_FULL_INTEGRATION,
};

// Tuanble IDs
enum {
  TUNABLE_PROCS_PER_NODE,
};

#define MAX_SPECIES 200

// field IDs used for q and rhs
enum { FID_RHO_U,
       FID_RHO_V,
       FID_RHO_W,
       FID_RHO,
       FID_RHO_E,
       FID_RHO_Y0, // ...

       FID_SCALAR_0 = FID_RHO_Y0 + MAX_SPECIES
};
#define FID_RHO_Y(x) (FID_RHO_Y0 + (x))
#define FID_SCALAR(x) (FID_SCALAR_0 + (x))

// field IDs used for state
enum { FID_TEMP = 10000,
       FID_VISCOSITY,
       FID_LAMBDA, // conductivity
       FID_DIM_TEMP, // dimensionalized temperature
       FID_DS_MIXAVG_0,
       FID_DS_MIXAVG_MAX = FID_DS_MIXAVG_0 + MAX_SPECIES,

       FID_RS_THERM_DIFF_0,
       FID_RS_THERM_DIFF_MAX = FID_RS_THERM_DIFF_0 + MAX_SPECIES,
};
#define FID_DS_MIXAVG(s) (FID_DS_MIXAVG_0 + (s))
#define FID_RS_THERM_DIFF(s) (FID_RS_THERM_DIFF_0 + (s))

// partition IDs, colors
enum { PID_X, PID_Y, PID_Z, PID_SUBRANK };
enum { PCLR_XYZLO, PCLR_XYZHI };

// field IDs used for ghost regions
enum { FID_GHOST_VEL_X = 20000,
       FID_GHOST_VEL_Y,
       FID_GHOST_VEL_Z,
       FID_GHOST_T,
       FID_GHOST_MIXMW,
       FID_GHOST_RHS1,
       FID_GHOST_RHS2,
       FID_GHOST_RHS3,
       FID_GHOST_RHS4,
       FID_GHOST_RHS5,
       FID_GHOST_YSPEC_0,
       FID_GHOST_YSPEC_MAX = FID_GHOST_YSPEC_0 + MAX_SPECIES,
       FID_GHOST_YDIFF_0,
       FID_GHOST_YDIFF_MAX = FID_GHOST_YDIFF_0 + MAX_SPECIES,
       FID_GHOST_RHS6_0,
       FID_GHOST_RHS6_MAX = FID_GHOST_RHS6_0 + MAX_SPECIES,
       FID_GHOST_SCALAR_0,
       FID_GHOST_SCALAR_MAX = FID_GHOST_SCALAR_0 + MAX_SPECIES,
       FID_GHOST_SCALAR_RHS_0,
       FID_GHOST_SCALAR_RHS_MAX = FID_GHOST_SCALAR_RHS_0 + MAX_SPECIES,
};
#define FID_GHOST_YSPEC(s) (FID_GHOST_YSPEC_0 + (s))
#define FID_GHOST_YDIFF(s) (FID_GHOST_YDIFF_0 + (s))
#define FID_GHOST_RHS6(s) (FID_GHOST_RHS6_0 + (s))
#define FID_GHOST_SCALAR(s) (FID_GHOST_SCALAR_0 + (s))
#define FID_GHOST_SCALAR_RHS(s) (FID_GHOST_SCALAR_RHS_0 + (s))

// temporary field ids used within rhsf

// overlay some fields whose lifetimes are strictly non-overlapping
#define OVERLAP_FIELDS

enum { FID_NONE = 0,
       FID_VOLUME = 1000,
       FID_VEL_X,
       FID_VEL_Y,
       FID_VEL_Z,
       FID_CPMIX,
       FID_AVMOLWT,
       FID_MIXMW,
       FID_GAMMA,
       FID_PRESSURE,

#ifdef REDUNDANT_WORK
      FID_AVMOLWT_ALT,
      FID_MIXMW_ALT,
#endif

       FID_DIM_PRESSURE, // dimensionalized pressure

       FID_TAU_XX,
       FID_TAU_XY,
       FID_TAU_XZ,
       FID_TAU_YY,
       FID_TAU_YZ,
       FID_TAU_ZZ,

       FID_SOURCE_TERM,

       FID_RHS_1_DX_IN,
       FID_RHS_1_DX,
       FID_RHS_1_DY_IN,
       FID_RHS_1_DY,
       FID_RHS_1_DZ_IN,
       FID_RHS_1_DZ,
       FID_RHS_2_DX_IN,
       FID_RHS_2_DX,
       FID_RHS_2_DY_IN,
       FID_RHS_2_DY,
       FID_RHS_2_DZ_IN,
       FID_RHS_2_DZ,
       FID_RHS_3_DX_IN,
       FID_RHS_3_DX,
       FID_RHS_3_DY_IN,
       FID_RHS_3_DY,
       FID_RHS_3_DZ_IN,
       FID_RHS_3_DZ,

       FID_RHS_4_DX,
       FID_RHS_4_DY,
       FID_RHS_4_DZ,

       FID_RHS_5_DX_IN,
       FID_RHS_5_DX,
       FID_RHS_5_DY_IN,
       FID_RHS_5_DY,
       FID_RHS_5_DZ_IN,
       FID_RHS_5_DZ,

       FID_RHS_6_DX_IN_0,
       FID_RHS_6_DX_0,
       FID_RHS_6_DY_IN_0,
       FID_RHS_6_DY_0,
       FID_RHS_6_DZ_IN_0,
       FID_RHS_6_DZ_0,
       FID_RHS_6_D_MAX = FID_RHS_6_DX_IN_0 + 6 * MAX_SPECIES,

       FID_GRAD_T_X,
       FID_GRAD_T_Y,
       FID_GRAD_T_Z,
       FID_GRAD_MIXMW_X,
       FID_GRAD_MIXMW_Y,
       FID_GRAD_MIXMW_Z,
       FID_HEATFLUX_X,
       FID_HEATFLUX_Y,
       FID_HEATFLUX_Z,

       FID_YSPECIES_0,
       FID_YSPECIES_MAX = FID_YSPECIES_0 + MAX_SPECIES,

#ifdef REDUNDANT_WORK
       FID_YSPECIES_ALT_0,
       FID_YSPECIES_ALT_MAX = FID_YSPECIES_ALT_0 + MAX_SPECIES,
#endif

       FID_H_SPEC_0,
       FID_H_SPEC_MAX = FID_H_SPEC_0 + MAX_SPECIES,

       FID_GRAD_VEL_X_X,
       FID_GRAD_VEL_X_Y,
       FID_GRAD_VEL_X_Z,
       FID_GRAD_VEL_Y_X,
       FID_GRAD_VEL_Y_Y,
       FID_GRAD_VEL_Y_Z,
       FID_GRAD_VEL_Z_X,
       FID_GRAD_VEL_Z_Y,
       FID_GRAD_VEL_Z_Z,
#ifndef OVERLAP_FIELDS
       // these overlap with FID_RHS_6_D_IN if OVERLAP_FIELDS is defined
       FID_GRAD_YS_0_X,
       FID_GRAD_YS_0_Y,
       FID_GRAD_YS_0_Z,
       FID_GRAD_YS_MAX = FID_GRAD_YS_0_X + 3 * MAX_SPECIES,
#endif

       FID_YDIFFFLUX_0_X,
       FID_YDIFFFLUX_0_Y,
       FID_YDIFFFLUX_0_Z,
       FID_YDIFFFLUX_MAX = FID_YDIFFFLUX_0_X + 3 * MAX_SPECIES,

#ifdef REDUNDANT_WORK 
       FID_YDIFFFLUX_ALT_0_X,
       FID_YDIFFFLUX_ALT_0_Y,
       FID_YDIFFFLUX_ALT_0_Z,
       FID_YDIFFFLUX_ALT_MAX = FID_YDIFFFLUX_ALT_0_X + 3 * MAX_SPECIES,
#endif

       FID_RR_0,
       FID_RR_MAX = FID_RR_0 + MAX_SPECIES,
#ifdef GETRATES_NEEDS_DIFFUSION
       FID_DIFFUSION_0,
       FID_DIFFUSION_MAX = FID_DIFFUSION_0 + MAX_SPECIES,

       FID_DIFF_DX_0,
       FID_DIFF_DY_0,
       FID_DIFF_DZ_0,
       FID_DIFF_D_MAX = FID_DIFF_DX_0 + 3 * MAX_SPECIES,
#endif
       FID_DIM_YSPECIES_0,
       FID_DIM_YSPECIES_MAX = FID_DIM_YSPECIES_0 + MAX_SPECIES,

       FID_GRAD_SCALAR_0_X,
       FID_GRAD_SCALAR_0_Y,
       FID_GRAD_SCALAR_0_Z,
       FID_GRAD_SCALAR_MAX = FID_GRAD_SCALAR_0_X + 3 * MAX_SPECIES,

       FID_SCALAR_FLUX_0_X,
       FID_SCALAR_FLUX_0_Y,
       FID_SCALAR_FLUX_0_Z,
       FID_SCALAR_FLUX_MAX = FID_SCALAR_FLUX_0_X + 3 * MAX_SPECIES,

       FID_SCALAR_RHS_DX_IN_0,
       FID_SCALAR_RHS_DX_0,
       FID_SCALAR_RHS_DY_IN_0,
       FID_SCALAR_RHS_DY_0,
       FID_SCALAR_RHS_DZ_IN_0,
       FID_SCALAR_RHS_DZ_0,
       FID_SCALAR_RHS_D_MAX = FID_SCALAR_RHS_DX_IN_0 + 6 * MAX_SPECIES,

};
#define FID_YSPECIES(s) (FID_YSPECIES_0 + (s))
#define FID_H_SPEC(s) (FID_H_SPEC_0 + (s))
#define FID_GRAD_T(d) (FID_GRAD_T_X + (d))
#define FID_GRAD_MIXMW(d) (FID_GRAD_MIXMW_X + (d))
#define FID_HEATFLUX(d) (FID_HEATFLUX_X + (d))
#define FID_YDIFFFLUX(s,d) (FID_YDIFFFLUX_0_X + 3*(s) + (d))
#define FID_YDIFFFLUX_X(s) (FID_YDIFFFLUX_0_X + 3*(s))
#define FID_YDIFFFLUX_Y(s) (FID_YDIFFFLUX_0_Y + 3*(s))
#define FID_YDIFFFLUX_Z(s) (FID_YDIFFFLUX_0_Z + 3*(s))
#define FID_RR(s) (FID_RR_0 + (s))
#define FID_RHS_6_D_IN(s,d) (FID_RHS_6_DX_IN_0 + 6*(s) + 2*(d))
#define FID_RHS_6_D(s,d) (FID_RHS_6_DX_0 + 6*(s) + 2*(d))
#define FID_RHS_6_DX(s) (FID_RHS_6_DX_0 + 6*(s))
#define FID_RHS_6_DY(s) (FID_RHS_6_DY_0 + 6*(s))
#define FID_RHS_6_DZ(s) (FID_RHS_6_DZ_0 + 6*(s))
#ifdef OVERLAP_FIELDS
#define FID_GRAD_YS(s,d) FID_RHS_6_D_IN(s,d)
#define FID_GRAD_YS_X(s) FID_RHS_6_D_IN(s,0)
#define FID_GRAD_YS_Y(s) FID_RHS_6_D_IN(s,1)
#define FID_GRAD_YS_Z(s) FID_RHS_6_D_IN(s,2)
#else
#define FID_GRAD_YS(s,d) (FID_GRAD_YS_0_X + 3*(s) + (d))
#define FID_GRAD_YS_X(s) (FID_GRAD_YS_0_X + 3*(s))
#define FID_GRAD_YS_Y(s) (FID_GRAD_YS_0_Y + 3*(s))
#define FID_GRAD_YS_Z(s) (FID_GRAD_YS_0_Z + 3*(s))
#endif
#ifdef GETRATES_NEEDS_DIFFUSION
#define FID_DIFFUSION(s) (FID_DIFFUSION_0 + (s))
#define FID_DIFF_DX(s) (FID_DIFF_DX_0 + 3*(s))
#define FID_DIFF_DY(s) (FID_DIFF_DY_0 + 3*(s))
#define FID_DIFF_DZ(s) (FID_DIFF_DZ_0 + 3*(s))
#define FID_DIFF_D(s,d) (FID_DIFF_DX_0 + 3*(s) + (d))
#endif
#define FID_DIM_YSPECIES(s) (FID_DIM_YSPECIES_0 + (s))
#define FID_GRAD_SCALAR(s,d) (FID_GRAD_SCALAR_0_X + 3*(s) + (d))
#define FID_GRAD_SCALAR_X(s) (FID_GRAD_SCALAR_0_X + 3*(s))
#define FID_GRAD_SCALAR_Y(s) (FID_GRAD_SCALAR_0_Y + 3*(s))
#define FID_GRAD_SCALAR_Z(s) (FID_GRAD_SCALAR_0_Z + 3*(s))
#define FID_SCALAR_FLUX(s,d) (FID_SCALAR_FLUX_0_X + 3*(s) + (d))
#define FID_SCALAR_FLUX_X(s) (FID_SCALAR_FLUX_0_X + 3*(s))
#define FID_SCALAR_FLUX_Y(s) (FID_SCALAR_FLUX_0_Y + 3*(s))
#define FID_SCALAR_FLUX_Z(s) (FID_SCALAR_FLUX_0_Z + 3*(s))
#define FID_SCALAR_RHS_D_IN(s,d) (FID_SCALAR_RHS_DX_IN_0 + 6*(s) + 2*(d))
#define FID_SCALAR_RHS_DX_IN(s) (FID_SCALAR_RHS_DX_IN_0 + 6*(s))
#define FID_SCALAR_RHS_DY_IN(s) (FID_SCALAR_RHS_DY_IN_0 + 6*(s))
#define FID_SCALAR_RHS_DZ_IN(s) (FID_SCALAR_RHS_DZ_IN_0 + 6*(s))
#define FID_SCALAR_RHS_D(s,d) (FID_SCALAR_RHS_DX_0 + 6*(s) + 2*(d))
#define FID_SCALAR_RHS_DX(s) (FID_SCALAR_RHS_DX_0 + 6*(s))
#define FID_SCALAR_RHS_DY(s) (FID_SCALAR_RHS_DY_0 + 6*(s))
#define FID_SCALAR_RHS_DZ(s) (FID_SCALAR_RHS_DZ_0 + 6*(s))
#ifdef REDUNDANT_WORK 
#define FID_YSPECIES_ALT(s) (FID_YSPECIES_ALT_0 + (s))
#define FID_YDIFFFLUX_ALT(s,d) (FID_YDIFFFLUX_ALT_0_X + 3*(s) + (d))
#define FID_YDIFFFLUX_ALT_X(s) (FID_YDIFFFLUX_ALT_0_X + 3*(s))
#define FID_YDIFFFLUX_ALT_Y(s) (FID_YDIFFFLUX_ALT_0_Y + 3*(s))
#define FID_YDIFFFLUX_ALT_Z(s) (FID_YDIFFFLUX_ALT_0_Z + 3*(s))
#endif

// field IDs used for CEMA - in addition to ones shared with the above
enum {
  FID_SAMPLE_X = 30000,
  FID_SAMPLE_Y,
  FID_SAMPLE_Z,
  FID_TEXP,
};

#define MACH_NO           (0.001)
#define RE_REAL           (100.0)
#define PARAM_PR          (0.708)
#define G_REF             (1.4)
#define A_REF             (347.2) // m/s
#define T_0               (300.0) // K
#define RHO_REF           (1.1766)  // Kg/m3
#define LAMBDA_REF        (26.14e-3)
#define PARAM_RE          (RE_REAL / MACH_NO)
#define T_REF             (T_0 * (G_REF - 1))
#define CP_REF            ((A_REF * A_REF) / T_REF)
#define L_REF             (PARAM_PR * LAMBDA_REF / CP_REF * PARAM_RE / RHO_REF / A_REF)
#define VIS_REF           (RHO_REF * A_REF * L_REF)
#define DIF_REF           (A_REF * L_REF)
#define P_REF             ((A_REF * A_REF) * RHO_REF * 10.0)

// defined in impose_pressure.h
class CompressionSourceTerm;

// AwaitMPITask returns one of these to be used by other tasks that need timestep info
struct TimestepInfo {
  double cur_time;
  double cur_timestep;
};

// A Helper class for coordinating between MPI and Legion
class ExtLegionHandshake {
public:
  enum { IN_EXT, IN_LEGION };

  typedef LegionRuntime::LowLevel::UserEvent UserEvent;

  ExtLegionHandshake(int init_state, int _ext_queue_depth = 1, int _legion_queue_depth = 1);

  void ext_init(void);

  void legion_init(void);

  void ext_handoff_to_legion(void);

  void ext_wait_on_legion(void);

  void legion_handoff_to_ext(void);
  
  void legion_wait_on_ext(void);
protected:
  int state, ext_queue_depth, legion_queue_depth, ext_count, legion_count;
  UserEvent *ext_queue, *legion_queue;
  pthread_mutex_t sync_mutex;
  pthread_cond_t sync_cond;
};

// The S3D rank task provides the interface
// for passing data back and forth between
// an S3D MPI Rank and the Legion runtime.
class S3DRank {
public:
  S3DRank(const int *local_grid,
	  const int *global_grid,
	  const int *dist_grid,
	  const int *proc_grid,
	  const int *proc_id,
	  const int *p_vary_in_dims,
	  const int *p_nvar_tot,
	  const int *p_n_spec,
	  const int *p_n_reg,
	  const int *p_n_scalar,
	  const int *p_iorder,
	  const int *p_iforder,
	  const int *p_n_stages,
	  const int *p_n_steps,
	  const int *p_n_subranks,
	  const int *p_lagging_switch,
	  const int *p_lag_steps,
          const int *p_i_react,
	  const double *_scale_dims
#ifdef USE_CEMA
	  , const int *p_cema_samples_per_rank
#endif
	  );

  ~S3DRank(void);

  LegionRuntime::Arrays::Point<3> local_grid_size, global_grid_size, dist_grid_size, proc_grid_size, my_proc_id;
  int nvar_tot, n_spec, n_reg, n_scalar, iorder, iforder, n_stages, n_steps, n_subranks;
  int lagging_switch, lag_steps, cur_stage, next_therm_stage;
  int i_react;
  double scale_dims[3];
  LegionRuntime::Arrays::Point<3> vary_in_dims;
  LegionRuntime::Arrays::Rect<3> *subrank_bounds;
  CompressionSourceTerm *compression;

  // Save some of these values for use in building region requirements
  LegionRuntime::HighLevel::IndexSpace is_grid;
  LegionRuntime::HighLevel::IndexPartition ip_top;
  LegionRuntime::HighLevel::LogicalRegion lr_q, lr_rhs, lr_qerr, lr_qtmp, lr_state, lr_int;
  LegionRuntime::HighLevel::LogicalPartition lp_q_top, lp_rhs_top, lp_qerr_top, lp_qtmp_top, lp_state_top, lp_int_top;
  LegionRuntime::Arrays::Rect<3> proc_grid_bounds;

  LegionRuntime::HighLevel::DynamicCollective global_sum_collective;

  struct RHSFArrays {
    RHSFArrays(void);

    void release_memory(void);

    const double *q;
    const double *volume;
    const double *yspecies;
    const double *grad_u;
    const double *grad_T;
    const double *grad_ys;
    const double *temp_in;
    const double *temperature;
    const double *avmolwt;
    const double *cpmix;
    const double *gamma;
    const double *pressure;
    const double *viscosity;
    const double *lambda;
    const double *ds_mixavg;
    const double *rs_therm_diff;
    const double *tau;
    const double *h_spec;
    const double *heatflux;
    const double *diffusion;
    const double *mixmw;
    const double *grad_mixmw;
    const double *ydiffflux;
    const double *rr_r;
    const double *rhs_cmp;
    double *rhs;
    const double *qs;
    double *rhs_scalar;
  };

  struct IntegrationArrays {
    IntegrationArrays(void);

    double *q;
    double *q_err;
    double *qs;
  };

  struct CPETable {
    int num_pts;
    const double *enth_aa;
    const double *enth_bb;
    const double *cp_aa;
    const double *cp_bb;
  };

  RHSFArrays *cur_data;
  IntegrationArrays *cur_integ_data;
  double cur_time, cur_timestep;
  int cur_handback, next_handback; // These set by fortran, read by legion
  int did_last_handback; // Set by legion, read by legion
  ExtLegionHandshake *handshake;
  CPETable cpe_data;
  const double *temp_data;
  int cur_legion_stage; // current number of calls to integrate or top
  int erk_stages;
  double *erk_alpha, *erk_beta, *erk_error;
  double small_value;
#ifdef USE_CEMA
  int cema_samples_per_rank;
  LegionRuntime::HighLevel::LogicalRegion lr_cema_global, lr_cema_subdist, lr_cema_local;
  LegionRuntime::HighLevel::IndexPartition ip_cema_proc;
  LegionRuntime::HighLevel::LogicalPartition lp_cema_proc;
  LegionRuntime::HighLevel::PhaseBarrier cema_ready_barrier, cema_done_barrier;
#endif

  bool is_first_proc(void) const { return((my_proc_id[0] == 0) &&
					  (my_proc_id[1] == 0) &&
					  (my_proc_id[2] == 0)); }

  // used to remember information about ghost region barriers
  void *barrier_data;

  void begin_rhsf_step(RHSFArrays *step_data, double timestep);
  void finish_rhsf_step(void);
  void begin_integration(IntegrationArrays *step_data, double time_accum, double timestep, int handback);
  void finish_integration(void);
  void provide_cpe_data(int num_pts, 
			const double *enth_aa, const double *enth_bb,
			const double *cp_aa, const double *cp_bb);
  void provide_initial_temperature(const double *temperature);
  void configure_erk(int stages, const double *alpha, const double *beta,
                     const double *error, const double *small);
  void complete_configure(void);
public:
  static S3DRank* get_rank(const Point<3> &pt, bool must_match);
#ifdef SHARED_LOWLEVEL
  static bool has_rank(const Point<3> &pt);
#endif
  static void parse_cmdline_args(int argc, const char *argv[]);
public:
  static const int diff_max_errors;
  static const bool show_all_error_counts;
  static bool& get_diff_replace_data(void);
  static bool& get_show_progress(void);
  static bool& get_enable_profiling(void);
  static bool& get_perform_waits(void);
  static bool& get_perform_all_checks(void);
  static bool& get_perform_final_checks(void);
  static bool& get_explicit_ghost_cells(void);
  static bool& get_skip_nonstiff_diffusion(void);
  static bool& get_hoist_subtasks(void);
  static bool& get_mixed_stencils(void);
  static bool& get_all_gpu(void);
  static bool& get_field_centric(void);
  static int& get_fuse_fields(void);
  static int& get_legion_pass(void);
  static int& get_compression_lag(void);
private:
#ifndef SHARED_LOWLEVEL
  static S3DRank*& get_local_rank(void);
#else
  static std::map<Point<3>, S3DRank*, Point<3>::STLComparator>& get_local_ranks(void);
  static pthread_mutex_t& get_local_mutex(void);
#endif
};

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

static inline bool offset_mismatch(int i, const ByteOffset *off1, const ByteOffset *off2)
{
  while(i-- > 0)
    if((off1++)->offset != (off2++)->offset)
      return true;
  return false;
}

template <unsigned DIM, typename T>
  static bool offsets_are_dense(const Rect<DIM>& bounds, const ByteOffset *offset, 
				bool reorder_ok = true)
{
  off_t exp_offset = sizeof(T);
  int used_mask = 0; // keep track of which dimensions we've already matched
  for(unsigned i = 0; i < DIM; i++) {
    bool found = false;
    for(unsigned j = 0; j < DIM; j++)
      if((offset[j].offset == exp_offset) && (((used_mask >> j) & 1) == 0)) {
	found = true;
	if((i != j) && !reorder_ok) return false;
	used_mask |= (1 << j);  // don't reuse this dimension
	exp_offset *= (bounds.hi[j] - bounds.lo[j] + 1);
	break;
      }
    if(!found) 
      return false;
  }
  return true;
}

template <typename T, int DIM>
T *get_dense_ptr(const PhysicalRegion& pr, FieldID fid, const Rect<DIM>& subgrid_bounds,
		 bool failure_ok = false)
{
  RegionAccessor<AccessorType::Generic,T> fa = pr.get_field_accessor(fid).typeify<T>();
  Rect<DIM> subrect;
  ByteOffset offsets[DIM];
  T *ptr = fa.template raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets);
  if(ptr && (subrect == subgrid_bounds) &&
     offsets_are_dense<DIM,T>(subgrid_bounds, offsets, false /*no reordering*/)) {
    return ptr;
  } else {
    assert(failure_ok);
    return 0;
  }
}

template <typename T, int DIM>
T *get_strided_dense_ptrs(const PhysicalRegion& pr, FieldID fid_start, int count,
			  const Rect<DIM>& subgrid_bounds, ByteOffset& stride,
			  bool failure_ok = false)
{
  T *ptr = 0;
  for(int i = 0; i < count; i++) {
    RegionAccessor<AccessorType::Generic,T> fa = pr.get_field_accessor(fid_start+i).typeify<T>();
    Rect<DIM> subrect;
    ByteOffset offsets[DIM];
    T *ptr2 = fa.template raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets);
    if(!ptr2 || (subrect != subgrid_bounds) ||
       !offsets_are_dense<DIM,T>(subgrid_bounds, offsets, false /*no reordering*/)) {
      assert(failure_ok);
      return 0;
    }
    if(i == 0) {
      ptr = ptr2;
    } else {
      ByteOffset s(ptr2, ptr);
      if(i == 1) {
	stride = s;
      } else {
	if(s.offset != (stride.offset * i)) {
	  assert(failure_ok);
	  return 0;
	}
      }
    }
  }
  return ptr;
}

#endif
