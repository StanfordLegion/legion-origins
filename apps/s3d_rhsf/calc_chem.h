
#ifndef _CALC_CHEM_H_
#define _CALC_CHEM_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

class CalcGetratesTask : public IndexLauncher {
public:
  CalcGetratesTask(S3DRank *rank,
                   Domain domain,
                   TaskArgument global_arg,
                   ArgumentMap arg_map,
                   Predicate pred = Predicate::TRUE_PRED,
                   bool must = false,
                   MapperID id = 0,
                   MappingTagID tag = 0,
                   bool add_requirement = true,
                   bool redundant = false);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_GETRATES_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_PRIORITIZE;
protected:
  static bool fast_cpu_getrates(const Rect<3> &my_subgrid_bounds,
                                const std::vector<RegionRequirement> &reqs,
                                const std::vector<PhysicalRegion> &regions,
                                RegionAccessor<AccessorType::Generic,double> fa_temp,
                                RegionAccessor<AccessorType::Generic,double> fa_pressure,
                                RegionAccessor<AccessorType::Generic,double> fa_avmolwt
#ifdef GETRATES_NEEDS_DIFFUSION
				, double dt
#endif
				);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

#endif
