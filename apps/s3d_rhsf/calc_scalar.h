
#ifndef _CALC_SCALAR_H_
#define _CALC_SCALAR_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

// calculates the diffusive "flux" of the scalar fields
// (the Fortran version actually calculates the negative of the flux (del Z instead of -del Z) -
// we calculate it with the correct sign so that we can use the existing code for combining
// advection and diffusion terms in calc_pre.cc
class CalcScalarFluxTask : public IndexLauncher {
public:
  CalcScalarFluxTask(S3DRank *rank, 
		     Domain domain,
		     TaskArgument global_arg,
		     ArgumentMap arg_map,
		     Predicate pred = Predicate::TRUE_PRED,
		     bool must = false,
		     MapperID id = 0,
		     MappingTagID tag = 0,
		     bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
  std::vector<int> species;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SCALAR_FLUX_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_ALL_GPU; 
protected:
  static bool fast_calc_scalar_flux_task(const Rect<3> &subgrid_bounds,
					 int n_scalar,
					 RegionAccessor<AccessorType::Generic,double> fa_in_lambda,
					 RegionAccessor<AccessorType::Generic,double> fa_in_volume,
					 RegionAccessor<AccessorType::Generic,double> fa_in_cpmix,
					  
					 RegionAccessor<AccessorType::Generic,double> *fa_in_grad_scalars,
					 RegionAccessor<AccessorType::Generic,double> *fa_out_fluxes);

public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

// the second scalar field for PRF has a source term of the form: volume * (rr_r[9] + rr_r[10])
// rather than calculating that separately, roll it into the integration
class ScalarSum6IntegrateTask : public IndexLauncher {
public:
  enum { FIRST_INTEGRATION, MIDDLE_INTEGRATION, LAST_INTEGRATION };

  ScalarSum6IntegrateTask(Domain domain,
			  TaskArgument global_arg,
			  ArgumentMap arg_map,
			  Predicate pred = Predicate::TRUE_PRED,
			  bool must = false,
			  MapperID id = 0,
			  MappingTagID tag = 0);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SCALAR_SUM_6_INTEGRATE_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_BALANCE | RHSF_MAPPER_ALL_GPU;
protected:
  static bool fast_sum_6_integrate(RegionAccessor<AccessorType::Generic,double> fa_q,
				   RegionAccessor<AccessorType::Generic,double> fa_qerr,
				   RegionAccessor<AccessorType::Generic,double> fa_qtmp,
				   RegionAccessor<AccessorType::Generic,double> fa_1,
				   RegionAccessor<AccessorType::Generic,double> fa_2,
				   RegionAccessor<AccessorType::Generic,double> fa_3,
				   RegionAccessor<AccessorType::Generic,double> fa_4,
				   RegionAccessor<AccessorType::Generic,double> fa_5,
				   RegionAccessor<AccessorType::Generic,double> fa_6,
				   Rect<3> subgrid_bounds,
				   double alpha, double beta, double err, 
                                   int which);
  static void kernel_sum_6_integrate(size_t n_pts,
				     double *q, double *qerr, double *qtmp,
				     const double *in1, const double *in2,
				     const double *in3, const double *in4,
				     const double *in5, const double *in6,
				     double alpha, double beta, double err, int which);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task,
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task,
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void sum_6_integrate_fields(HighLevelRuntime *runtime, Context ctx, 
				     const Rect<3>& proc_grid_bounds,
				     IndexSpace is_grid, IndexPartition ip_top, 
				     LogicalRegion lr_in,
				     LogicalRegion lr_q, LogicalRegion lr_qerr, LogicalRegion lr_qtmp,
				     FieldID fid_1, FieldID fid_2, FieldID fid_3, 
				     FieldID fid_4, FieldID fid_5, FieldID fid_6, FieldID fid_out,
				     double alpha, double beta, double err, 
                                     Future f_timestep, 
                                     int which, int stage, int tag = 0);
};


#endif
