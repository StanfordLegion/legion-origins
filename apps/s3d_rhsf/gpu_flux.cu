
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <assert.h>

#include "cuda.h"
#include "cuda_runtime.h"
#include "gpu_help.h"

//#define DEBUG_CUDA

#ifdef DEBUG_CUDA
#define CUDA_SAFE_CALL(expr)                        \
  {                                                 \
    cudaError_t err = (expr);                       \
    if (err != cudaSuccess)                         \
    {                                               \
      printf("Cuda error: %s\n", cudaGetErrorString(err));  \
      assert(false);                                \
    }                                               \
  }
#else
#define CUDA_SAFE_CALL(expr)  expr
#endif



__global__
void calc_ydiffflux_kernel(const int num_species, 
                           const int field_stride,
                           const int max_elements,
                           const double *ds_mixavg_ptr,
                           const double *yspecs_ptr,
                           const double *grad_yspecs_ptr,
                           const double *grad_mixmw_ptr,
                           const double *avmolwt_ptr,
                           const double *rho_ptr,
                           double *diff_flux_ptr)
{
  // Offset all our pointers based on our location
  {
    const int idx = blockIdx.x*blockDim.x + threadIdx.x;
    // Check for extra threads
    if (idx >= max_elements) return;
    ds_mixavg_ptr += idx;
    yspecs_ptr += idx;
    grad_yspecs_ptr += idx;
    grad_mixmw_ptr += idx;
    avmolwt_ptr += idx;
    rho_ptr += idx;
    diff_flux_ptr += idx;
  }

  // Load the gradient of the mixmw field in all dimensions
  double grad_mixmw[3];
  #pragma unroll
  for (int d = 0; d < 3; d++)
    grad_mixmw[d] = stream_load(grad_mixmw_ptr+d*field_stride);
  // Load other invariant variables
  const double avmolwt = *avmolwt_ptr;
  const double rho = *rho_ptr;
  // Iterate over the species
  for (int s = 0; s < num_species; s++)
  {
    // Load everything for this species
    const double ds_mixavg = stream_load(ds_mixavg_ptr+s*field_stride);
    const double yspec = stream_load(yspecs_ptr+s*field_stride);
    #pragma unroll
    for (int d = 0; d < 3; d++)
    {
      const double grad_yspec = stream_load(grad_yspecs_ptr+(3*s+d)*field_stride);
      const double ydiffflux = -rho * ds_mixavg * (grad_yspec + yspec * grad_mixmw[d] * avmolwt);
      stream_store(diff_flux_ptr+(3*s+d)*field_stride, ydiffflux);
    }
  }
}

__host__
void gpu_ydiff_flux(const int num_species, 
                    const int field_stride,
                    const int max_elements,
                    const double *ds_mixavg_d,
                    const double *yspecs_d,
                    const double *grad_yspecs_d,
                    const double *grad_mixmw_d,
                    const double *avmolwt_d,
                    const double *rho_d,
                    double *diff_flux_d)
{
  const int threads_per_block = 256;
  const int num_blocks = (max_elements + (threads_per_block-1))/threads_per_block;

  calc_ydiffflux_kernel<<<num_blocks,threads_per_block>>>(
                                                          num_species, 
                                                          field_stride,
                                                          max_elements,
                                                          ds_mixavg_d,
                                                          yspecs_d,
                                                          grad_yspecs_d,
                                                          grad_mixmw_d,
                                                          avmolwt_d,
                                                          rho_d,
                                                          diff_flux_d);
}

__global__
void calc_field_ydiffflux_kernel(const int max_elements,
                                 const double *rho_ptr,
                                 const double *avmolwt_ptr,
                                 const double *yspec_ptr,
                                 const double *ds_mixavg_ptr,
                                 const double *grad_ys_x_ptr,
                                 const double *grad_ys_y_ptr,
                                 const double *grad_ys_z_ptr,
                                 const double *grad_mixmw_x_ptr,
                                 const double *grad_mixmw_y_ptr,
                                 const double *grad_mixmw_z_ptr,
                                 double *diff_flux_x_ptr,
                                 double *diff_flux_y_ptr,
                                 double *diff_flux_z_ptr)
{
  // Offset all our pointers based on our location
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  // Check for extra threads
  if (idx >= max_elements) return;
  double rho = stream_load(rho_ptr+idx);
  double ds_mixavg = stream_load(ds_mixavg_ptr+idx);
  double rho_ds = -rho * ds_mixavg;

  double yspec = stream_load(yspec_ptr+idx);
  double avmolwt = stream_load(avmolwt_ptr+idx);
  double yspec_av = yspec * avmolwt;

  // X direction
  {
    double grad_ys = stream_load(grad_ys_x_ptr+idx);
    double grad_mixmw = stream_load(grad_mixmw_x_ptr+idx);
    double flux = rho_ds * (grad_ys + yspec_av * grad_mixmw);
    stream_store(diff_flux_x_ptr+idx, flux);
  }
  // Y direction
  {
    double grad_ys = stream_load(grad_ys_y_ptr+idx);
    double grad_mixmw = stream_load(grad_mixmw_y_ptr+idx);
    double flux = rho_ds * (grad_ys + yspec_av * grad_mixmw);
    stream_store(diff_flux_y_ptr+idx, flux);
  }
  // Z direction
  {
    double grad_ys = stream_load(grad_ys_z_ptr+idx);
    double grad_mixmw = stream_load(grad_mixmw_z_ptr+idx);
    double flux = rho_ds * (grad_ys + yspec_av * grad_mixmw);
    stream_store(diff_flux_z_ptr+idx, flux);
  }
}

__host__
void gpu_field_ydiff_flux(const int max_elements,
                          const double *rho_d,
                          const double *avmolwt_d,
                          const double *yspec_d,
                          const double *ds_mixavg_d,
                          const double *grad_ys_x_d,
                          const double *grad_ys_y_d,
                          const double *grad_ys_z_d,
                          const double *grad_mixmw_x_d,
                          const double *grad_mixmw_y_d,
                          const double *grad_mixmw_z_d,
                          double *diff_flux_x_d,
                          double *diff_flux_y_d,
                          double *diff_flux_z_d)
{
  const int threads_per_block = 256;
  const int num_blocks = (max_elements + (threads_per_block-1))/threads_per_block;

  calc_field_ydiffflux_kernel<<<num_blocks,threads_per_block>>>(
                                max_elements, rho_d, avmolwt_d, yspec_d,
                                ds_mixavg_d, grad_ys_x_d, grad_ys_y_d,
                                grad_ys_z_d, grad_mixmw_x_d, grad_mixmw_y_d,
                                grad_mixmw_z_d, diff_flux_x_d, diff_flux_y_d,
                                diff_flux_z_d);
}

__global__
void calc_short_heat_flux(const int num_species,
                          const int max_elements,
                          const int h_spec_stride,
                          const int diffflux_stride,
                          const double *h_spec_ptr,
                          const double *diffflux_ptr,
                          const double *lambda_ptr,
                          const double *grad_t_x_ptr,
                          const double *grad_t_y_ptr,
                          const double *grad_t_z_ptr,
                          double *heatflux_x_ptr,
                          double *heatflux_y_ptr,
                          double *heatflux_z_ptr,
                          double *diffflux_last_x_ptr,
                          double *diffflux_last_y_ptr,
                          double *diffflux_last_z_ptr)
{
  // Offset all our pointers based on our location
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  // Check for extra threads
  if (idx >= max_elements) return;

  const double grad_t_x = stream_load(grad_t_x_ptr+idx);
  const double grad_t_y = stream_load(grad_t_y_ptr+idx);
  const double grad_t_z = stream_load(grad_t_z_ptr+idx);
  const double lambda = stream_load(lambda_ptr+idx);

  double heatflux_x = -lambda * grad_t_x;
  double heatflux_y = -lambda * grad_t_y;
  double heatflux_z = -lambda * grad_t_z;

  double ydifflast_x = 0.0;
  double ydifflast_y = 0.0;
  double ydifflast_z = 0.0;

  // Actually (num_species-1) see below
  for (int i = 0; i < num_species; i++) {
    double h_spec = stream_load(h_spec_ptr+i*h_spec_stride+idx);
    double ydiff_x = stream_load(diffflux_ptr+(i*3)*diffflux_stride+idx);
    double ydiff_y = stream_load(diffflux_ptr+(i*3+1)*diffflux_stride+idx);
    double ydiff_z = stream_load(diffflux_ptr+(i*3+2)*diffflux_stride+idx);

    heatflux_x += h_spec * ydiff_x;
    heatflux_y += h_spec * ydiff_y;
    heatflux_z += h_spec * ydiff_z;

    ydifflast_x -= ydiff_x;
    ydifflast_y -= ydiff_y;
    ydifflast_z -= ydiff_z;
  }
  // now handle the final species
  double h_spec_last = stream_load(h_spec_ptr+num_species*h_spec_stride+idx);
  heatflux_x += h_spec_last * ydifflast_x;
  heatflux_y += h_spec_last * ydifflast_y;
  heatflux_z += h_spec_last * ydifflast_z;

  // write out heatflux
  stream_store(heatflux_x_ptr+idx, heatflux_x);
  stream_store(heatflux_y_ptr+idx, heatflux_y);
  stream_store(heatflux_z_ptr+idx, heatflux_z);

  // write out last diff flux
  stream_store(diffflux_last_x_ptr+idx, ydifflast_x);
  stream_store(diffflux_last_y_ptr+idx, ydifflast_y);
  stream_store(diffflux_last_z_ptr+idx, ydifflast_z);
}

__host__
void gpu_short_heat_flux(const int num_species,
                         const int max_elements,
                         const int h_spec_stride,
                         const int diffflux_stride,
                         const double *h_spec_d,
                         const double *diffflux_d,
                         const double *lambda_d,
                         const double *grad_t_x_d,
                         const double *grad_t_y_d,
                         const double *grad_t_z_d,
                         double *heatflux_x_d,
                         double *heatflux_y_d,
                         double *heatflux_z_d,
                         double *diffflux_last_x_d,
                         double *diffflux_last_y_d,
                         double *diffflux_last_z_d)
{
  const int threads_per_block = 256;
  const int num_blocks = (max_elements + (threads_per_block-1))/threads_per_block;

  calc_short_heat_flux<<<num_blocks,threads_per_block>>>(
                          (num_species-1), max_elements, h_spec_stride, diffflux_stride,
                          h_spec_d, diffflux_d, lambda_d, 
                          grad_t_x_d, grad_t_y_d, grad_t_z_d,
                          heatflux_x_d, heatflux_y_d, heatflux_z_d,
                          diffflux_last_x_d, diffflux_last_y_d, diffflux_last_z_d);
}

__global__
void scalar_flux_kernel(const int max_elements,
			const double *lambda_ptr, const double *volume_ptr, const double *cpmix_ptr,
			const double *grad_scal_0x_ptr, const double *grad_scal_0y_ptr, const double *grad_scal_0z_ptr,
			const double *grad_scal_1x_ptr, const double *grad_scal_1y_ptr, const double *grad_scal_1z_ptr,
			double *scal_flux_0x_ptr, double *scal_flux_0y_ptr, double *scal_flux_0z_ptr,
			double *scal_flux_1x_ptr, double *scal_flux_1y_ptr, double *scal_flux_1z_ptr)
{
  // Offset all our pointers based on our location
  const int idx = blockIdx.x*blockDim.x + threadIdx.x;
  // Check for extra threads
  if (idx >= max_elements) return;

  const double lambda = stream_load(lambda_ptr+idx);
  const double volume = stream_load(volume_ptr+idx);
  const double cpmix = stream_load(cpmix_ptr+idx);

  const double grad_scal_0x = stream_load(grad_scal_0x_ptr+idx);
  const double grad_scal_0y = stream_load(grad_scal_0y_ptr+idx);
  const double grad_scal_0z = stream_load(grad_scal_0z_ptr+idx);
  const double grad_scal_1x = stream_load(grad_scal_1x_ptr+idx);
  const double grad_scal_1y = stream_load(grad_scal_1y_ptr+idx);
  const double grad_scal_1z = stream_load(grad_scal_1z_ptr+idx);

  // combine the above into the base scaling factor - note the negation compared to the Fortran
  // (we're calculating the true flux instead of its additive inverse here)
  double base_factor = -lambda * volume / cpmix;

  // scalar field 0 uses the factor directly
  double factor0 = base_factor;
  stream_store(scal_flux_0x_ptr+idx, factor0 * grad_scal_0x);
  stream_store(scal_flux_0y_ptr+idx, factor0 * grad_scal_0y);
  stream_store(scal_flux_0z_ptr+idx, factor0 * grad_scal_0z);

  // scalar field 1 divides out another factor of 1.61 (hardcoded in the Fortran)
  double factor1 = base_factor * (1.0 / 1.61);
  stream_store(scal_flux_1x_ptr+idx, factor1 * grad_scal_1x);
  stream_store(scal_flux_1y_ptr+idx, factor1 * grad_scal_1y);
  stream_store(scal_flux_1z_ptr+idx, factor1 * grad_scal_1z);
}

__host__
void gpu_scalar_flux(const int max_elements,
		     const double *lambda, const double *volume, const double *cpmix,
		     const double *grad_scal_0x, const double *grad_scal_0y, const double *grad_scal_0z,
		     const double *grad_scal_1x, const double *grad_scal_1y, const double *grad_scal_1z,
		     double *scal_flux_0x, double *scal_flux_0y, double *scal_flux_0z,
		     double *scal_flux_1x, double *scal_flux_1y, double *scal_flux_1z)
{
  const int threads_per_block = 256;
  const int num_blocks = (max_elements + (threads_per_block-1))/threads_per_block;

  scalar_flux_kernel<<<num_blocks,threads_per_block>>>(max_elements, lambda, volume, cpmix,
								     grad_scal_0x, grad_scal_0y, grad_scal_0z,
								     grad_scal_1x, grad_scal_1y, grad_scal_1z,
								     scal_flux_0x, scal_flux_0y, scal_flux_0z,
								     scal_flux_1x, scal_flux_1y, scal_flux_1z);
}
