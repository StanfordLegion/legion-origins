
#ifndef _CALC_PRE_H_
#define _CALC_PRE_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

class CalcEnergyTask : public IndexLauncher {
public:
  CalcEnergyTask(S3DRank *rank,
                 Domain domain,
                 TaskArgument global_arg,
                 ArgumentMap arg_map,
                 Predicate pred = Predicate::TRUE_PRED,
                 bool must = false,
                 MapperID id = 0,
                 MappingTagID tag = 0,
                 bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_ENERGY_PRECALC_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_BALANCE;
protected:
  static void dense_energy_precalc_task(size_t n_pts,
                                 /*const*/ double *rho_e, /*const*/ double *vel_x, 
                                 /*const*/ double *vel_y, /*const*/ double *vel_z,
                                 /*const*/ double *pressure, /*const*/ double *heatflux_x, 
                                 /*const*/ double *heatflux_y, /*const*/ double *heatflux_z,
                                 /*const*/ double *tau_xx, /*const*/ double *tau_xy, /*const*/ double *tau_xz,
                                 /*const*/ double *tau_yy, /*const*/ double *tau_yz, /*const*/ double *tau_zz,
                                 double *precalc_x, double *precalc_y, double *precalc_z);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, 
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcSpecTask : public IndexLauncher {
public:
  CalcSpecTask(S3DRank *rank, int spec, int dir,
               Domain domain,
               TaskArgument global_arg,
               ArgumentMap arg_map,
               Predicate pred = Predicate::TRUE_PRED,
               bool must = false,
               MapperID id = 0,
               MappingTagID tag = 0,
               bool add_requirements = true);
  CalcSpecTask(S3DRank *rank, int start_spec, int max_spec, int dir,
               Domain domain,
               TaskArgument global_arg,
               ArgumentMap arg_map,
               Predicate pred = Predicate::TRUE_PRED,
               bool must = false,
               MapperID id = 0,
               MappingTagID tag = 0,
               bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SPEC_PRECALC_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_BALANCE | RHSF_MAPPER_ALL_GPU;
protected:
  static bool dense_spec_precalc_task(const Rect<3> &subgrid_bounds,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_rho_y,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_vel,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_flux,
                                      RegionAccessor<AccessorType::Generic,double> fa_out_precalc);
  static bool fast_spec_precalc_task(RegionAccessor<AccessorType::Generic,double> fa_in_rho_y,
                                     RegionAccessor<AccessorType::Generic,double> fa_in_vel,
                                     RegionAccessor<AccessorType::Generic,double> fa_in_flux,
                                     RegionAccessor<AccessorType::Generic,double> fa_out_precalc,
                                     Rect<3> subgrid_bounds);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, 
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

// the per-field version also handles scalar fields, using negative values for 'spec'
class CalcSpecPerFieldTask : public IndexLauncher {
public:
  CalcSpecPerFieldTask(S3DRank *rank, int spec,
                       Domain domain,
                       TaskArgument global_arg,
                       ArgumentMap arg_map,
                       Predicate pred = Predicate::TRUE_PRED,
                       bool must = false,
                       MapperID id = 0,
                       MappingTagID tag = 0,
                       bool add_requirements = true);
  CalcSpecPerFieldTask(S3DRank *rank, int start_spec, int max_spec,
                       Domain domain,
                       TaskArgument global_arg,
                       ArgumentMap arg_map,
                       Predicate pred = Predicate::TRUE_PRED,
                       bool must = false,
                       MapperID id = 0,
                       MappingTagID tag = 0,
                       bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SPEC_PRECALC_PER_FIELD_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_BALANCE | RHSF_MAPPER_ALL_GPU;
public:
  static bool dense_spec_precalc_task(const Rect<3> &subgrid_bounds,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_rho,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_vel_x,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_vel_y,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_vel_z,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_flux_x,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_flux_y,
                                      RegionAccessor<AccessorType::Generic,double> fa_in_flux_z,
                                      RegionAccessor<AccessorType::Generic,double> fa_out_pre_x,
                                      RegionAccessor<AccessorType::Generic,double> fa_out_pre_y,
                                      RegionAccessor<AccessorType::Generic,double> fa_out_pre_z);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, 
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task,
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

#endif
