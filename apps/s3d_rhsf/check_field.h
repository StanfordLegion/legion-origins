
#ifndef _CHECK_FIELD_H_
#define _CHECK_FIELD_H_

#include "rhsf.h"
#include "legion.h"
#include "arrays.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

// Forward declaration of this function for launching checking tasks
void check_field(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
		 IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr, FieldID fid,
		 const S3DRank *rank, const double * S3DRank::RHSFArrays::* ref_ptr, int plane_number, 
		 double abs_tolerance, double rel_tolerance, double min_difference,
		 const char *fmt, ...);

void check_field_task(const Task *task,
                      const std::vector<PhysicalRegion> &regions,
                      Context ctx, HighLevelRuntime *runtime);

void dump_field(HighLevelRuntime *runtime, Context ctx, const Rect<3>& proc_grid_bounds,
		IndexSpace is_grid, IndexPartition ip_top, LogicalRegion lr, FieldID fid,
		const S3DRank *rank, bool blocking,
		const char *fmt, ...);

void dump_field_task(const Task *task,
		     const std::vector<PhysicalRegion> &regions,
		     Context ctx, HighLevelRuntime *runtime);

#endif
