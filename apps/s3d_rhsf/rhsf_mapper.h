
#ifndef _RHSF_MAPPERS_H_
#define _RHSF_MAPPERS_H_

#include "legion.h"
#include "default_mapper.h"

using namespace LegionRuntime::HighLevel;

enum {
  // Use the first 8 bits for storing the rhsf index
  RHSF_MAPPER_STAGE_MASK       = 0x000000ff,
  RHSF_MAPPER_FORCE_RANK_MATCH = 0x00000100,
  RHSF_MAPPER_PRIORITIZE       = 0x00000200,
  RHSF_MAPPER_BALANCE          = 0x00000400,
  RHSF_MAPPER_SUBRANKABLE      = 0x00000800,
  RHSF_MAPPER_MIXED_GPU        = 0x00001000,
  RHSF_MAPPER_ALL_GPU          = 0x00002000,
  RHSF_MAPPER_SINGLE_RANK      = 0x00004000,
};

enum {
  RHSF_INNER_VARIANT           = 0x00000100,
  RHSF_CPU_LEAF_VARIANT        = 0x00000200,
  RHSF_GPU_LEAF_VARIANT        = 0x00000400,
};

// tags on region requirements help us understand where to put ghost regions
enum {
  RHSF_GHOST_LOCAL = 1,
  RHSF_GHOST_PREV_X,
  RHSF_GHOST_PREV_Y,
  RHSF_GHOST_PREV_Z,
  RHSF_GHOST_NEXT_X,
  RHSF_GHOST_NEXT_Y,
  RHSF_GHOST_NEXT_Z,
  RHSF_GHOST_FIRST_NODE,
  RHSF_GHOST_LAST_NODE,
};

#define RHSF_GHOST_PREV(d) (RHSF_GHOST_PREV_X + (d))
#define RHSF_GHOST_NEXT(d) (RHSF_GHOST_NEXT_X + (d))

class RHSFMapper : public DefaultMapper {
public:
  RHSFMapper(Machine machine, HighLevelRuntime *runtime, Processor local,
	     const std::vector<FieldID> &all_q_fields,
	     const std::vector<FieldID> &all_temp_fields,
	     const std::vector<FieldID> &all_state_fields,
	     const std::vector<FieldID> &all_gpu_temp_fields,
	     bool _enable_profiling);
  virtual ~RHSFMapper(void);
public:
  void set_bounds(const Rect<3>& bounds);
  void assign_point(DomainPoint point, Processor proc);
public:
  virtual void select_task_options(Task *task);
  virtual void slice_domain(const Task *task, const Domain &domain,
                            std::vector<DomainSplit> &slices);
  virtual bool map_task(Task *task);
  virtual void select_tasks_to_schedule(const std::list<Task*> &ready_tasks);
  virtual bool rank_copy_targets(const Mappable *mappable,
                                 LogicalRegion rebuild_region,
                                 const std::set<Memory> &current_instances,
                                 bool complete,
                                 size_t max_blocking_factor,
                                 std::set<Memory> &to_reuse,
                                 std::vector<Memory> &to_create,
                                 bool &create_one, size_t &blocking_factor);
  virtual void select_task_variant(Task *task);
  virtual size_t select_region_layout(const Task *task, Processor target,
				      const RegionRequirement &req, 
				      unsigned index, 
				      const Memory &chosen_mem, 
				      size_t max_blocking_factor);
  virtual bool map_copy(Copy *copy);
  virtual bool map_inline(Inline *inline_op);
  virtual bool map_must_epoch(const std::vector<Task*> &tasks,
                              const std::vector<MappingConstraint> &constraints,
                              MappingTagID tag);
  virtual void notify_mapping_failed(const Mappable *mappable);
  virtual void notify_profiling_info(const Task *task);
  virtual int get_tunable_value(const Task *task, TunableID tid, MappingTagID tag);
  virtual void configure_context(Task *task);
protected:
  std::set<FieldID> all_q_fields;
  std::set<FieldID> all_temp_fields;
  std::set<FieldID> all_state_fields;
  std::set<FieldID> all_gpu_temp_fields;
protected:
  // Mapping from rank points to their sets of Legion processors
  std::map<DomainPoint, std::vector<Processor>, DomainPoint::STLComparator> rank_map;
  // Keep track of the next processor to use for each kind of task ID
  std::map<DomainPoint, std::vector<unsigned>, DomainPoint::STLComparator> next_map;
  // overall processor grid bounds
  Rect<3> proc_grid_bounds;
protected:
  // Keep track of our local CPUs and GPUs on this node
  std::vector<Processor> local_cpus;
  std::vector<Processor> local_gpus;
protected:
  std::vector<unsigned> local_next_cpu;
  std::vector<unsigned> local_next_gpu;
protected:
  const bool has_subranks;
  const bool mixed_gpu;
  const bool all_gpu;
  const bool enable_profiling;
};

#endif
