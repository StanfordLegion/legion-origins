
#ifndef _CALC_TEMP_H_
#define _CALC_TEMP_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

class CalcTempTask : public IndexLauncher {
public:
  CalcTempTask(S3DRank *rank,
               Domain domain,
               TaskArgument global_arg,
               ArgumentMap arg_map,
               Predicate pred = Predicate::TRUE_PRED,
               bool must = false,
               MapperID id = 0,
               MappingTagID tag = 0,
               bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
  bool calc_cpmix;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_TEMP_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_PRIORITIZE;
protected:
  static bool fast_calc_temp_task(RegionAccessor<AccessorType::Generic,double> fa_in_rho,
				  RegionAccessor<AccessorType::Generic,double> fa_in_rho_e,
				  RegionAccessor<AccessorType::Generic,double> fa_in_rho_vel_x,
				  RegionAccessor<AccessorType::Generic,double> fa_in_rho_vel_y,
				  RegionAccessor<AccessorType::Generic,double> fa_in_rho_vel_z,
				  PhysicalRegion ra_in_rho_yspecies,
				  RegionAccessor<AccessorType::Generic,double> fa_io_temp,
				  RegionAccessor<AccessorType::Generic,double> fa_out_pressure,
				  RegionAccessor<AccessorType::Generic,double> fa_out_cpmix,
				  int n_spec,
				  Rect<3> subgrid_bounds);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcSpeciesTask : public IndexLauncher {
public:
  CalcSpeciesTask(S3DRank *rank,
                  Domain domain,
                  TaskArgument global_arg,
                  ArgumentMap arg_map,
                  Predicate pred = Predicate::TRUE_PRED,
                  bool must = false,
                  MapperID id = 0,
                  MappingTagID tag = 0,
                  bool add_requirements = true,
                  bool redundant = false);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SPECIES_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_PRIORITIZE;
protected:
  static bool dense_calc_species(S3DRank *rank, const Rect<3> &subgrid_bounds,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_rho,
                                 RegionAccessor<AccessorType::Generic,double> fa_out_avmolwt,
                                 RegionAccessor<AccessorType::Generic,double> fa_out_mixmw,
                                 const std::vector<RegionRequirement> &reqs,
                                 const std::vector<PhysicalRegion> &regions);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcGammaTask : public IndexLauncher {
public:
  CalcGammaTask(S3DRank *rank,
		Domain domain,
		TaskArgument global_arg,
		ArgumentMap arg_map,
		Predicate pred = Predicate::TRUE_PRED,
		bool must = false,
		MapperID id = 0,
		MappingTagID tag = 0,
		bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_GAMMA_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
protected:
  static bool dense_calc_gamma(S3DRank *rank, const Rect<3> &subgrid_bounds,
			       RegionAccessor<AccessorType::Generic,double> fa_in_rho,
			       RegionAccessor<AccessorType::Generic,double> fa_out_avmolwt,
			       RegionAccessor<AccessorType::Generic,double> fa_out_mixmw,
			       const std::vector<RegionRequirement> &reqs,
			       const std::vector<PhysicalRegion> &regions);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

#endif

