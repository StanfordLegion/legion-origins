
#include "calc_volume.h"
#include "check_field.h"

// Include Singe emitted header files
#include "getcoeffs.h"
#include "getrates.h"

#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
#include <x86intrin.h>
#endif

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
static inline __m128d _mm_stream_load_pd(const double *ptr) 
{
#ifdef __SSE4_1__
  return _mm_castsi128_pd(_mm_stream_load_si128(reinterpret_cast<__m128i *>(const_cast<double *>(ptr))));
#else
  return *(const __m128d *)ptr;
#endif
}
#endif

#ifdef USE_AVX_KERNELS
template<unsigned BOUNDARY>
static inline bool aligned(const void *ptr)
{
  return ((((unsigned long)ptr) & (BOUNDARY-1)) == 0);
}
#endif

CalcVolumeTask::CalcVolumeTask(S3DRank *r,
                               Domain domain,
                               TaskArgument global_arg,
                               ArgumentMap arg_map,
                               Predicate pred,
                               bool must,
                               MapperID id,
                               MappingTagID tag,
                               bool add_requirements)
 : IndexLauncher(CalcVolumeTask::TASK_ID, domain, global_arg, arg_map, 
                 pred, must, id, tag | CalcVolumeTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
    rr_q.add_field(FID_RHO);
    rr_q.add_field(FID_RHO_U);
    rr_q.add_field(FID_RHO_V);
    rr_q.add_field(FID_RHO_W);
    add_region_requirement(rr_q);

    RegionRequirement rr_int(rank->lp_int_top, 0, READ_WRITE, EXCLUSIVE, rank->lr_int);
    rr_int.add_field(FID_VOLUME);
    rr_int.add_field(FID_VEL_X);
    rr_int.add_field(FID_VEL_Y);
    rr_int.add_field(FID_VEL_Z);
    add_region_requirement(rr_int);
  }
}

void CalcVolumeTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  check_field(runtime, ctx, rank->proc_grid_bounds, 
              rank->is_grid, rank->ip_top, rank->lr_int, FID_VOLUME,
              rank, &S3DRank::RHSFArrays::volume, 0, 0, 1, 0, "volume"); 
}

/*static*/ const char * const CalcVolumeTask::TASK_NAME = "calc_volume";

/*static*/
void CalcVolumeTask::dense_calc_volume_task(size_t n_pts,
                                            const double *in_rho, const double *in_rho_u, 
                                            const double *in_rho_v, const double *in_rho_w,
                                            double *out_vol, double *out_vel_x, 
                                            double *out_vel_y, double *out_vel_z)
{
  // need to keep volume, avmolwt in cache - 32K / 24 B/pt -> 1K points
#ifdef BULLDOZER
// Block for L1, only consume the first way of each set
#define FULL_STRIP_SIZE     512 
#else
#define FULL_STRIP_SIZE     1024 
#endif

#if defined(USE_AVX_KERNELS)
  __m256d vol[FULL_STRIP_SIZE/4];
#elif defined(USE_SSE_KERNELS)
  __m128d vol[FULL_STRIP_SIZE/2];
#endif

  while(n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE)
    {
#if defined(USE_AVX_KERNELS)
      if (aligned<32>(out_vol) && aligned<32>(out_vel_x) && aligned<32>(out_vel_y) &&
          aligned<32>(out_vel_z) && aligned<32>(in_rho) && aligned<32>(in_rho_u) &&
          aligned<32>(in_rho_v) && aligned<32>(in_rho_w)) {
        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          vol[i] = _mm256_div_pd(_mm256_set1_pd(1.0),_mm256_load_pd(in_rho+(i<<2)));
          __m256d vel_x = _mm256_mul_pd(vol[i],_mm256_load_pd(in_rho_u+(i<<2)));
          __m256d vel_y = _mm256_mul_pd(vol[i],_mm256_load_pd(in_rho_v+(i<<2)));
          __m256d vel_z = _mm256_mul_pd(vol[i],_mm256_load_pd(in_rho_w+(i<<2)));
          _mm256_stream_pd(out_vol+(i<<2),vol[i]);
          _mm256_stream_pd(out_vel_x+(i<<2),vel_x);
          _mm256_stream_pd(out_vel_y+(i<<2),vel_y);
          _mm256_stream_pd(out_vel_z+(i<<2),vel_z);
        }
      }
      else {
        // handle the unaligned case
        for(size_t i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          vol[i] = _mm256_div_pd(_mm256_set1_pd(1.0),_mm256_loadu_pd(in_rho+(i<<2)));
          __m256d vel_x = _mm256_mul_pd(vol[i],_mm256_loadu_pd(in_rho_u+(i<<2)));
          __m256d vel_y = _mm256_mul_pd(vol[i],_mm256_loadu_pd(in_rho_v+(i<<2)));
          __m256d vel_z = _mm256_mul_pd(vol[i],_mm256_loadu_pd(in_rho_w+(i<<2)));
          _mm256_storeu_pd(out_vol+(i<<2),vol[i]);
          _mm256_storeu_pd(out_vel_x+(i<<2),vel_x);
          _mm256_storeu_pd(out_vel_y+(i<<2),vel_y);
          _mm256_storeu_pd(out_vel_z+(i<<2),vel_z);
        }
      }
#elif defined(USE_SSE_KERNELS)
      for(size_t i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        vol[i] = _mm_div_pd(_mm_set1_pd(1.0),_mm_stream_load_pd(in_rho+(i<<1)));
        __m128d vel_x = _mm_mul_pd(vol[i],_mm_stream_load_pd(in_rho_u+(i<<1)));
        __m128d vel_y = _mm_mul_pd(vol[i],_mm_stream_load_pd(in_rho_v+(i<<1)));
        __m128d vel_z = _mm_mul_pd(vol[i],_mm_stream_load_pd(in_rho_w+(i<<1)));
        _mm_stream_pd(out_vol+(i<<1),vol[i]);
        _mm_stream_pd(out_vel_x+(i<<1),vel_x);
        _mm_stream_pd(out_vel_y+(i<<1),vel_y);
        _mm_stream_pd(out_vel_z+(i<<1),vel_z);
      }
#else
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) out_vol[i] = 1.0 / in_rho[i];

      // now multiply volume through a bunch of things
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) out_vel_x[i] = out_vol[i] * in_rho_u[i];
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) out_vel_y[i] = out_vol[i] * in_rho_v[i];
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) out_vel_z[i] = out_vol[i] * in_rho_w[i];
#endif
      n_pts -= FULL_STRIP_SIZE;
    }
    else
    {
      // Slow case
      for(size_t i = 0; i < n_pts; i++) out_vol[i] = 1.0 / in_rho[i];

      // now multiply volume through a bunch of things
      for(size_t i = 0; i < n_pts; i++) out_vel_x[i] = out_vol[i] * in_rho_u[i];
      for(size_t i = 0; i < n_pts; i++) out_vel_y[i] = out_vol[i] * in_rho_v[i];
      for(size_t i = 0; i < n_pts; i++) out_vel_z[i] = out_vol[i] * in_rho_w[i];

      n_pts = 0;
    }

    in_rho += FULL_STRIP_SIZE;
    in_rho_u += FULL_STRIP_SIZE;
    in_rho_v += FULL_STRIP_SIZE;
    in_rho_w += FULL_STRIP_SIZE;
    out_vol += FULL_STRIP_SIZE;
    out_vel_x += FULL_STRIP_SIZE;
    out_vel_y += FULL_STRIP_SIZE;
    out_vel_z += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
}

/*static*/
bool CalcVolumeTask::fast_calc_volume_task(RegionAccessor<AccessorType::Generic,double> fa_in_rho,
                                           RegionAccessor<AccessorType::Generic,double> fa_in_rho_u,
                                           RegionAccessor<AccessorType::Generic,double> fa_in_rho_v,
                                           RegionAccessor<AccessorType::Generic,double> fa_in_rho_w,
                                           RegionAccessor<AccessorType::Generic,double> fa_out_vol,
                                           RegionAccessor<AccessorType::Generic,double> fa_out_vel_x,
                                           RegionAccessor<AccessorType::Generic,double> fa_out_vel_y,
                                           RegionAccessor<AccessorType::Generic,double> fa_out_vel_z,
                                           Rect<3> subgrid_bounds)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3], offsets[3];

  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds)) return false;

  const double *in_rho_u_ptr = fa_in_rho_u.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_u_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_rho_v_ptr = fa_in_rho_v.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_v_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_rho_w_ptr = fa_in_rho_w.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_rho_w_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_vol_ptr = fa_out_vol.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_vol_ptr || (subrect != subgrid_bounds)) return false;

  double *out_vel_x_ptr = fa_out_vel_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_vel_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *out_vel_y_ptr = fa_out_vel_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_vel_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  double *out_vel_z_ptr = fa_out_vel_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_vel_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, out_offsets, offsets)) return false;

  if(offsets_are_dense<3, double>(subgrid_bounds, in_offsets) && !offset_mismatch(3, in_offsets, out_offsets)) {
    //printf("dense calc_volume!\n");
    dense_calc_volume_task(subgrid_bounds.volume(),
                           in_rho_ptr, in_rho_u_ptr, in_rho_v_ptr, in_rho_w_ptr,
                           out_vol_ptr, out_vel_x_ptr, out_vel_y_ptr, out_vel_z_ptr);
    return true;
  }

  // now that we have all those pointers, we can do the actual work
  ByteOffset in_pos_z, out_pos_z;
  for(int z = subgrid_bounds.lo[2]; z <= subgrid_bounds.hi[2]; z++) {
    ByteOffset in_pos_y = in_pos_z;
    ByteOffset out_pos_y = out_pos_z;
    for(int y = subgrid_bounds.lo[1]; y <= subgrid_bounds.hi[1]; y++) {
      ByteOffset in_pos_x = in_pos_y;
      ByteOffset out_pos_x = out_pos_y;
      for(int x = subgrid_bounds.lo[0]; x <= subgrid_bounds.hi[0]; x++) {
        double rho = *(in_rho_ptr + in_pos_x);
        double volume = 1.0 / rho;

        *(out_vol_ptr + out_pos_x) = volume;

        *(out_vel_x_ptr + out_pos_x) = volume * *(in_rho_u_ptr + in_pos_x);
        *(out_vel_y_ptr + out_pos_x) = volume * *(in_rho_u_ptr + in_pos_x);
        *(out_vel_z_ptr + out_pos_x) = volume * *(in_rho_u_ptr + in_pos_x);

        in_pos_x += in_offsets[0];
        out_pos_x += out_offsets[0];
      }
      in_pos_y += in_offsets[1];
      out_pos_y += out_offsets[1];
    }
    in_pos_z += in_offsets[2];
    out_pos_z += out_offsets[2];
  }

  return true;
}

/*static*/
void CalcVolumeTask::cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                                   const std::vector<RegionRequirement> &reqs,
                                   const std::vector<PhysicalRegion> &regions,
                                   Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  // get whatever accessors you need here
  RegionAccessor<AccessorType::Generic,double> fa_rho = regions[0].get_field_accessor(FID_RHO).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_vol = regions[1].get_field_accessor(FID_VOLUME).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_rho_u = regions[0].get_field_accessor(FID_RHO_U).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_rho_v = regions[0].get_field_accessor(FID_RHO_V).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_rho_w = regions[0].get_field_accessor(FID_RHO_W).typeify<double>();
  
  RegionAccessor<AccessorType::Generic,double> fa_vel_x = regions[1].get_field_accessor(FID_VEL_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_vel_y = regions[1].get_field_accessor(FID_VEL_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_vel_z = regions[1].get_field_accessor(FID_VEL_Z).typeify<double>();
  
  // attempt fast version
  if(fast_calc_volume_task(fa_rho, fa_rho_u, fa_rho_v, fa_rho_w,
                           fa_vol, fa_vel_x, fa_vel_y, fa_vel_z,
                           subgrid_bounds)) {
    //printf("fast calc volume ran!\n");
    return;
  }

  for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);
    
    // do something for the current point
    double rho = fa_rho.read(dp);
    double volume = 1.0 / rho;
    //printf("(%d, %d, %d): %g -> %g\n", pir.p[0], pir.p[1], pir.p[2], rho, volume);
    fa_vol.write(dp, volume);
    
    fa_vel_x.write(dp, volume * fa_rho_u.read(dp));
    fa_vel_y.write(dp, volume * fa_rho_v.read(dp));
    fa_vel_z.write(dp, volume * fa_rho_w.read(dp));  
  }
#endif
}

