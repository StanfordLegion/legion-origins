
#ifndef _CALC_VOLUME_H_
#define _CALC_VOLUME_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

class CalcVolumeTask : public IndexLauncher {
public:
  CalcVolumeTask(S3DRank *rank,
                 Domain domain,
                 TaskArgument global_arg,
                 ArgumentMap arg_map,
                 Predicate pred = Predicate::TRUE_PRED,
                 bool must = false,
                 MapperID id = 0,
                 MappingTagID tag = 0,
                 bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_VOLUME_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
protected:
  static void dense_calc_volume_task(size_t n_pts,
				     const double *in_rho, const double *in_rho_u, 
                                     const double *in_rho_v, const double *in_rho_w,
				     double *out_vol, double *out_vel_x, 
                                     double *out_vel_y, double *out_vel_z);
  static bool fast_calc_volume_task(RegionAccessor<AccessorType::Generic,double> fa_in_rho,
				    RegionAccessor<AccessorType::Generic,double> fa_in_rho_u,
				    RegionAccessor<AccessorType::Generic,double> fa_in_rho_v,
				    RegionAccessor<AccessorType::Generic,double> fa_in_rho_w,
				    RegionAccessor<AccessorType::Generic,double> fa_out_vol,
				    RegionAccessor<AccessorType::Generic,double> fa_out_vel_x,
				    RegionAccessor<AccessorType::Generic,double> fa_out_vel_y,
				    RegionAccessor<AccessorType::Generic,double> fa_out_vel_z,
				    Rect<3> subgrid_bounds);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

#endif

