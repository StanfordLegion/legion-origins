#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#ifdef __MACH__
#include <mach/mach.h>
#include <mach/mach_time.h>
#else
#include <time.h>
#define _POSIX_BARRIERS
#endif
#include <pthread.h>
#include <assert.h>

#define MACH_NO           (0.001)
#define RE_REAL           (100.0)
#define PARAM_PR          (0.708)
#define G_REF             (1.4)
#define A_REF             (347.2) // m/s
#define T_0               (300.0) // K
#define RHO_REF           (1.1766)  // Kg/m3
#define LAMBDA_REF        (26.14e-3)
#define PARAM_RE          (RE_REAL / MACH_NO)
#define T_REF             (T_0 * (G_REF - 1))
#define CP_REF            ((A_REF * A_REF) / T_REF)
#define L_REF             (PARAM_PR * LAMBDA_REF / CP_REF * PARAM_RE / RHO_REF / A_REF)

#include "cpu_stencils.h"

#if 0
// best I can tell, movntdq is worse than movapd if you're touching the 
//  whole cache line quickly
#ifndef MOVNTDQ
#define MOVNTDQ "movapd"
//define MOVNTDQ "movntdq"
#endif

template <typename T>
static inline void add_offset(T *& ptr, off_t offset)
{
  ptr = (T *)(((char *)(ptr)) + offset);
}

void pencil_2(int size,
	      double ae, double be, double ce, double de,
	      const double *minus, size_t minus_stride,
	      const double *in, size_t in_stride,
	      const double *plus, size_t plus_stride,
	      double *out, size_t out_stride)
{
  while(size >= 16) {
    asm volatile (
// "movdqa (%2), %%xmm0;"
// "movdqa 16(%2), %%xmm1;"
// "movdqa 32(%2), %%xmm2;"
// "movdqa 48(%2), %%xmm3;"
// "movdqa 64(%2), %%xmm4;"
// "movdqa 80(%2), %%xmm5;"
// "movdqa 96(%2), %%xmm6;"
// "movdqa 112(%2), %%xmm7;"
// "addq %6, %2;"
"movdqa (%2), %%xmm0;"
"prefetcht2 128(%2);"
"addq %6, %2;"
"movdqa (%2), %%xmm1;"
"prefetcht2 128(%2);"
"addq %6, %2;"
"movdqa (%2), %%xmm2;"
"prefetcht2 128(%2);"
"addq %6, %2;"
"movdqa (%2), %%xmm3;"
"prefetcht2 128(%2);"
"addq %6, %2;"
"movdqa (%2), %%xmm4;"
"prefetcht2 128(%2);"
"addq %6, %2;"
"movdqa (%2), %%xmm5;"
"prefetcht2 128(%2);"
"addq %6, %2;"
"movdqa (%2), %%xmm6;"
"prefetcht2 128(%2);"
"addq %6, %2;"
"movdqa (%2), %%xmm7;"
"prefetcht2 128(%2);"
"addq %6, %2;"
MOVNTDQ " %%xmm0, (%0);"
"addq %4, %0;"
MOVNTDQ " %%xmm1, (%0);"
"addq %4, %0;"
MOVNTDQ " %%xmm2, (%0);"
"addq %4, %0;"
MOVNTDQ " %%xmm3, (%0);"
"addq %4, %0;"
MOVNTDQ " %%xmm4, (%0);"
"addq %4, %0;"
MOVNTDQ " %%xmm5, (%0);"
"addq %4, %0;"
MOVNTDQ " %%xmm6, (%0);"
"addq %4, %0;"
MOVNTDQ " %%xmm7, (%0);"
"addq %4, %0;"
: "+r"(out), "+r"(minus), "+r"(in), "+r"(plus)
: "r"(2*out_stride), "r"(2*minus_stride), "r"(2*in_stride), "r"(2*plus_stride)
: "memory", 
  "%xmm0", "%xmm1", "%xmm2", "%xmm3", "%xmm4", "%xmm5", "%xmm6", "%xmm7");
    size -= 16;
  }
}

void pencil_1_2(int size,
	      double ae, double be, double ce, double de,
	      const double *minus, size_t minus_stride,
	      const double *in, size_t in_stride,
	      const double *plus, size_t plus_stride,
	      double *out, size_t out_stride)
{
  while(size >= 4) {
    asm volatile (
"movq (%2), %%xmm0;"
"addq %6, %2;"
"movq (%2), %%xmm1;"
"addq %6, %2;"
"movq (%2), %%xmm2;"
"addq %6, %2;"
"movq (%2), %%xmm3;"
"addq %6, %2;"
//"movq %%xmm0, (%0);"
//"addq %4, %0;"
"movq %%xmm0, (%0);"
"addq %4, %0;"
"movq %%xmm1, (%0);"
"addq %4, %0;"
"movq %%xmm2, (%0);"
"addq %4, %0;"
"movq %%xmm3, (%0);"
"addq %4, %0;"
: "+r"(out), "+r"(minus), "+r"(in), "+r"(plus)
: "r"(out_stride), "r"(minus_stride), "r"(in_stride), "r"(plus_stride)
: "memory", 
  "%xmm0", "%xmm1", "%xmm2", "%xmm3", "%xmm4", "%xmm5", "%xmm6", "%xmm7");
    size -= 4;
  }
}

void pencil_1_4(int size,
	      double ae, double be, double ce, double de,
	      const double *minus, size_t minus_stride,
	      const double *in, size_t in_stride,
	      const double *plus, size_t plus_stride,
	      double *out, size_t out_stride)
{
  while(size >= 4) {
    asm volatile (
"movq (%2), %%xmm0;"
"addq %6, %2;"
"movq (%2), %%xmm1;"
"addq %6, %2;"
"movq (%2), %%xmm2;"
"addq %6, %2;"
"movq (%2), %%xmm3;"
"addq %6, %2;"
"movq %%xmm0, (%0);"
"addq %4, %0;"
"movq %%xmm1, (%0);"
"addq %4, %0;"
"movq %%xmm2, (%0);"
"addq %4, %0;"
"movq %%xmm3, (%0);"
"addq %4, %0;"
: "+r"(out), "+r"(minus), "+r"(in), "+r"(plus)
: "r"(out_stride), "r"(minus_stride), "r"(in_stride), "r"(plus_stride)
: "memory", 
  "%xmm0", "%xmm1", "%xmm2", "%xmm3", "%xmm4", "%xmm5", "%xmm6", "%xmm7");
    size -= 4;
  }
}

void pencil_1_8(int size,
	      double ae, double be, double ce, double de,
	      const double *minus, size_t minus_stride,
	      const double *in, size_t in_stride,
	      const double *plus, size_t plus_stride,
	      double *out, size_t out_stride)
{
  while(size >= 8) {
    asm volatile (
"movq (%2), %%xmm0;"
"addq %6, %2;"
"movq (%2), %%xmm1;"
"addq %6, %2;"
"movq (%2), %%xmm2;"
"addq %6, %2;"
"movq (%2), %%xmm3;"
"addq %6, %2;"
"movq (%2), %%xmm4;"
"addq %6, %2;"
"movq (%2), %%xmm5;"
"addq %6, %2;"
"movq (%2), %%xmm6;"
"addq %6, %2;"
"movq (%2), %%xmm7;"
"addq %6, %2;"
"movq %%xmm0, (%0);"
"addq %4, %0;"
"movq %%xmm1, (%0);"
"addq %4, %0;"
"movq %%xmm2, (%0);"
"addq %4, %0;"
"movq %%xmm3, (%0);"
"addq %4, %0;"
"movq %%xmm4, (%0);"
"addq %4, %0;"
"movq %%xmm5, (%0);"
"addq %4, %0;"
"movq %%xmm6, (%0);"
"addq %4, %0;"
"movq %%xmm7, (%0);"
"addq %4, %0;"
: "+r"(out), "+r"(minus), "+r"(in), "+r"(plus)
: "r"(out_stride), "r"(minus_stride), "r"(in_stride), "r"(plus_stride)
: "memory", 
  "%xmm0", "%xmm1", "%xmm2", "%xmm3", "%xmm4", "%xmm5", "%xmm6", "%xmm7");
    size -= 8;
  }
}

void slow_stencil(int grid_size, int dim_1, int dim_2, int dim_3,
		  const double *minus_ptr, size_t minus_stride_1, size_t minus_stride_2, size_t minus_stride_3,
		  const double *in_ptr, size_t in_stride_1, size_t in_stride_2, size_t in_stride_3,
		  const double *plus_ptr, size_t plus_stride_1, size_t plus_stride_2, size_t plus_stride_3,
		  double *out_ptr, size_t out_stride_1, size_t out_stride_2, size_t out_stride_3)
{
  double xmax = 1.0;
  double xmin = 0.0;
  double ds = (grid_size - 1) / (((xmax - xmin) /* / (rank->global_grid_size[0] - 1)*/ / 100.0 / L_REF));
  //printf("DS = %g\n", ds);

  double ae = 4.0 / 5.0 * ds;
  double be = -1.0 / 5.0 * ds;
  double ce = 4.0 / 105.0 * ds;
  double de = -1.0 / 280.0 * ds;
  //ae = 1; be = 1; ce = 1; de = 1;
  for(int z = 0; z < dim_3; z++) {
    const double *minus_y = minus_ptr;
    const double *in_y = in_ptr;
    const double *plus_y = plus_ptr;
    double *out_y = out_ptr;

    for(int y = 0; y < dim_2; y++) {
      const double *minus_x = minus_y;
      const double *in_x = in_y;
      const double *plus_x = plus_y;
      double *out_x = out_y;

      // pencil in "x" direction
      {
	double window[9];

	// pre-fill window
	for(int i = 0; i < 4; i++) {
	  window[i] = *minus_x;
	  add_offset(minus_x, minus_stride_1);
	}

	for(int i = 0; i < 4; i++) {
	  window[i+4] = *in_x;
	  add_offset(in_x, in_stride_1);
	}

	// main loop
	for(int i = 0; i < dim_1 - 4; i++) {
	  window[8] = *in_x;
	  add_offset(in_x, in_stride_1);

	  double deriv = (((ae * (window[5] - window[3])
			    + be * (window[6] - window[2]))
			   + ce * (window[7] - window[1]))
			  + de * (window[8] - window[0]));
	      
	  if(0 && (y == 0) && (z == 0)) {
	    printf("%g %g %g %g %g %g %g %g %g -> %g\n",
		   window[0], window[1], window[2], window[3],
		   window[4], window[5], window[6], window[7],
		   window[8], deriv);
	  }
	  *out_x = deriv; add_offset(out_x, out_stride_1);

	  for(int j = 0; j < 8; j++)
	    window[j] = window[j + 1];
	}

	// final bit with plus data
	for(int i = 0; i < 4; i++) {
	  window[8] = *plus_x;
	  add_offset(plus_x, plus_stride_1);

	  double deriv = (((ae * (window[5] - window[3])
			    + be * (window[6] - window[2]))
			   + ce * (window[7] - window[1]))
			  + de * (window[8] - window[0]));
	      
	  if(0 && (y == 0) && (z == 0)) {
	    printf("%g %g %g %g %g %g %g %g %g -> %g\n",
		   window[0], window[1], window[2], window[3],
		   window[4], window[5], window[6], window[7],
		   window[8], deriv);
	  }
	  *out_x = deriv; add_offset(out_x, out_stride_1);

	  for(int j = 0; j < 8; j++)
	    window[j] = window[j + 1];
	}
      }

      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);
    }

    add_offset(minus_ptr, minus_stride_3);
    add_offset(in_ptr, in_stride_3);
    add_offset(plus_ptr, plus_stride_3);
    add_offset(out_ptr, out_stride_3);
  }
}

#define REGSTENCIL_SHIFT(r0, r1, r2, r3, r4, r5, r6, r7, ldexp, stexp)	\
  /* ae * (window[5] - window[3]) */			 \
  "movapd %%xmm" #r3 ", %%xmm9;"				 \
  "subpd %%xmm" #r5 ", %%xmm9;"				 \
  "mulpd %%xmm12, %%xmm9;"				 \
  /* be * (window[6] - window[2]) */			 \
  "movapd %%xmm" #r2 ", %%xmm8;"				 \
  "subpd %%xmm" #r6 ", %%xmm8;"				 \
  "mulpd %%xmm13, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* ce * (window[7] - window[1]) */			 \
  "movapd %%xmm" #r1 ", %%xmm8;"				 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm14, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* de * (window[8] - window[0]) */			 \
  "movapd %%xmm" #r0 ", %%xmm8;"				 \
  /* manually move registers for now - rotate later */	 \
  "movapd %%xmm" #r1 ", %%xmm" #r0 ";"				 \
  "movapd %%xmm" #r2 ", %%xmm" #r1 ";"			 \
  "movapd %%xmm" #r3 ", %%xmm" #r2 ";"				 \
  "movapd %%xmm" #r4 ", %%xmm" #r3 ";"				 \
  "movapd %%xmm" #r5 ", %%xmm" #r4 ";"				 \
  "movapd %%xmm" #r6 ", %%xmm" #r5 ";"				 \
  "movapd %%xmm" #r7 ", %%xmm" #r6 ";"				 \
  "movddup " ldexp ", %%xmm" #r7 ";"				 \
  /*  bleah */						 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm15, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  "movq %%xmm9, " stexp ";"

#define REGSTENCIL_ROTATE(r0, r1, r2, r3, r4, r5, r6, r7, ldexp, stexp)	\
  /* ae * (window[5] - window[3]) */			 \
  "movapd %%xmm" #r3 ", %%xmm9;"				 \
  "subpd %%xmm" #r5 ", %%xmm9;"				 \
  "mulpd %%xmm12, %%xmm9;"				 \
  /* be * (window[6] - window[2]) */			 \
  "movapd %%xmm" #r2 ", %%xmm8;"				 \
  "subpd %%xmm" #r6 ", %%xmm8;"				 \
  "mulpd %%xmm13, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* ce * (window[7] - window[1]) */			 \
  "movapd %%xmm" #r1 ", %%xmm8;"				 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm14, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* de * (window[8] - window[0]) */			 \
  "movapd %%xmm" #r0 ", %%xmm8;"				 \
  "movddup " ldexp ", %%xmm" #r0 ";"				 \
  "subpd %%xmm" #r0 ", %%xmm8;"				 \
  "mulpd %%xmm15, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  "movlpd %%xmm0, " stexp ";"

#define REGSTENCIL_ROTATE_1X2(r0, r1, r2, r3, r4, r5, r6, r7, ldexp, stexp)	\
  "movapd " ldexp ", %%xmm10;"						\
  "movapd %%xmm" #r6 ", %%xmm" #r7 ";"					\
  "shufpd $1, %%xmm10, %%xmm" #r7 ";"					\
  /* ae * (window[5] - window[3]) */			 \
  "movapd %%xmm" #r3 ", %%xmm9;"				 \
  "subpd %%xmm" #r5 ", %%xmm9;"				 \
  "mulpd %%xmm12, %%xmm9;"				 \
  /* be * (window[6] - window[2]) */			 \
  "movapd %%xmm" #r2 ", %%xmm8;"				 \
  "subpd %%xmm" #r6 ", %%xmm8;"				 \
  "mulpd %%xmm13, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* ce * (window[7] - window[1]) */			 \
  "movapd %%xmm" #r1 ", %%xmm8;"				 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm14, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* de * (window[8] - window[0]) */			 \
  "movapd %%xmm" #r0 ", %%xmm8;"				 \
  "subpd %%xmm10, %%xmm8;"				 \
  "movapd %%xmm10, %%xmm" #r0 ";"				 \
  "mulpd %%xmm15, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  MOVNTDQ " %%xmm9, " stexp ";"

#define REGSTENCIL_ROTATE_2X1(r0, r1, r2, r3, r4, r5, r6, r7, ldexp1, ldexp2, stexp1, stexp2) \
  /* ae * (window[5] - window[3]) */			 \
  "movapd %%xmm" #r3 ", %%xmm9;"				 \
  "subpd %%xmm" #r5 ", %%xmm9;"				 \
  "mulpd %%xmm12, %%xmm9;"				 \
  /* be * (window[6] - window[2]) */			 \
  "movapd %%xmm" #r2 ", %%xmm8;"				 \
  "subpd %%xmm" #r6 ", %%xmm8;"				 \
  "mulpd %%xmm13, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* ce * (window[7] - window[1]) */			 \
  "movapd %%xmm" #r1 ", %%xmm8;"				 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm14, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* de * (window[8] - window[0]) */			 \
  "movapd %%xmm" #r0 ", %%xmm8;"				 \
  "movsd " ldexp1 ", %%xmm" #r0 ";"				 \
  "movhpd " ldexp2 ", %%xmm" #r0 ";"				 \
  "subpd %%xmm" #r0 ", %%xmm8;"				 \
  "mulpd %%xmm15, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  "movlpd %%xmm9, " stexp1 ";"				 \
  "movhpd %%xmm9, " stexp2 ";"

#define REGSTENCIL_ROTATE_2X2(r0, r1, r2, r3, r4, r5, r6, r7, ldexp1, ldexp2, stexp1, stexp2) \
  /* ae * (window[5] - window[3]) */			 \
  "movapd %%xmm" #r3 ", %%xmm9;"				 \
  "subpd %%xmm" #r5 ", %%xmm9;"				 \
  "mulpd %%xmm12, %%xmm9;"				 \
  /* be * (window[6] - window[2]) */			 \
  "movapd %%xmm" #r2 ", %%xmm8;"				 \
  "subpd %%xmm" #r6 ", %%xmm8;"				 \
  "mulpd %%xmm13, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* ce * (window[7] - window[1]) */			 \
  "movapd %%xmm" #r1 ", %%xmm8;"				 \
  "subpd %%xmm" #r7 ", %%xmm8;"				 \
  "mulpd %%xmm14, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  /* de * (window[8] - window[0]) */			 \
  "movapd %%xmm" #r0 ", %%xmm8;"				 \
  "movapd " ldexp1 ", %%xmm" #r0 ";" \
  "movapd %%xmm" #r0 ", %%xmm" #r1 ";" \
  "unpcklpd " ldexp2 ", %%xmm" #r0 ";" \
  "unpckhpd " ldexp2 ", %%xmm" #r1 ";" \
  "subpd %%xmm" #r0 ", %%xmm8;"				 \
  "mulpd %%xmm15, %%xmm8;"				 \
  "addpd %%xmm8, %%xmm9;"				 \
  MOVNTDQ " %%xmm9, " stexp1 ";"				 \
  MOVNTDQ " %%xmm8, " stexp2 ";"

void stencil_x_row_shift(const double *minus, const double *in, const double *plus,
			 const double *weights, double *out, size_t count)
{
  asm volatile (
// fetch weights into registers
"movddup 0(%3), %%xmm12;"
"movddup 8(%3), %%xmm13;"
"movddup 16(%3), %%xmm14;"
"movddup 24(%3), %%xmm15;"
// preload first eight values
"movddup 0(%0), %%xmm0;"
"movddup 8(%0), %%xmm1;"
"movddup 16(%0), %%xmm2;"
"movddup 24(%0), %%xmm3;"
"movddup 0(%1), %%xmm4;"
"movddup 8(%1), %%xmm5;"
"movddup 16(%1), %%xmm6;"
"movddup 24(%1), %%xmm7;"
"addq $32, %1;"
"movq %5, %%rcx;"
"subq $4, %%rcx;"
"1:"
REGSTENCIL_SHIFT(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%4)")
"addq $8, %1;"
"addq $8, %4;"
"subq $1, %%rcx;"
"jne 1b;"
REGSTENCIL_SHIFT(0, 1, 2, 3, 4, 5, 6, 7, "0(%2)", "0(%4)")
REGSTENCIL_SHIFT(0, 1, 2, 3, 4, 5, 6, 7, "8(%2)", "8(%4)")
REGSTENCIL_SHIFT(0, 1, 2, 3, 4, 5, 6, 7, "16(%2)", "16(%4)")
REGSTENCIL_SHIFT(0, 1, 2, 3, 4, 5, 6, 7, "24(%2)", "24(%4)")
: "+r" (minus), "+r" (in), "+r" (plus), "+r" (weights),
  "+r" (out)
: "r" (count)
: "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}

void stencil_x_row_rotate(const double *minus, const double *in, const double *plus,
			 const double *weights, double *out, size_t count)
{
  asm volatile (
// fetch weights into registers
"movddup 0(%3), %%xmm12;"
"movddup 8(%3), %%xmm13;"
"movddup 16(%3), %%xmm14;"
"movddup 24(%3), %%xmm15;"
// preload first eight values
"movddup 0(%0), %%xmm0;"
"movddup 8(%0), %%xmm1;"
"movddup 16(%0), %%xmm2;"
"movddup 24(%0), %%xmm3;"
"movddup 0(%1), %%xmm4;"
"movddup 8(%1), %%xmm5;"
"movddup 16(%1), %%xmm6;"
"movddup 24(%1), %%xmm7;"
"addq $32, %1;"
"movq %5, %%rcx;"
"1:"
REGSTENCIL_ROTATE(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%4)")
REGSTENCIL_ROTATE(1, 2, 3, 4, 5, 6, 7, 0, "8(%1)", "8(%4)")
REGSTENCIL_ROTATE(2, 3, 4, 5, 6, 7, 0, 1, "16(%1)", "16(%4)")
REGSTENCIL_ROTATE(3, 4, 5, 6, 7, 0, 1, 2, "24(%1)", "24(%4)")
"subq $8, %%rcx;"
"je 2f;"
REGSTENCIL_ROTATE(4, 5, 6, 7, 0, 1, 2, 3, "32(%1)", "32(%4)")
REGSTENCIL_ROTATE(5, 6, 7, 0, 1, 2, 3, 4, "40(%1)", "40(%4)")
REGSTENCIL_ROTATE(6, 7, 0, 1, 2, 3 ,4, 5, "48(%1)", "48(%4)")
REGSTENCIL_ROTATE(7, 0, 1, 2, 3, 4, 5, 6, "56(%1)", "56(%4)")
"addq $64, %1;"
"addq $64, %4;"
"jmp 1b;"
"2:"
REGSTENCIL_ROTATE(4, 5, 6, 7, 0, 1, 2, 3, "0(%2)", "32(%4)")
REGSTENCIL_ROTATE(5, 6, 7, 0, 1, 2, 3, 4, "8(%2)", "40(%4)")
REGSTENCIL_ROTATE(6, 7, 0, 1, 2, 3 ,4, 5, "16(%2)", "48(%4)")
REGSTENCIL_ROTATE(7, 0, 1, 2, 3, 4, 5, 6, "24(%2)", "56(%4)")
: "+r" (minus), "+r" (in), "+r" (plus), "+r" (weights),
  "+r" (out)
: "r" (count)
: "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}

void stencil_x_row_rotate2(const double *minus, const double *in, const double *plus,
			   const double *weights, double *out, size_t count)
{
  asm volatile (
// fetch weights into registers
"movddup 0(%3), %%xmm12;"
"movddup 8(%3), %%xmm13;"
"movddup 16(%3), %%xmm14;"
"movddup 24(%3), %%xmm15;"
// preload first four pairs
"movapd 0(%0), %%xmm0;"
"movapd 16(%0), %%xmm2;"
"movapd 0(%1), %%xmm4;"
"movapd 16(%1), %%xmm6;"
// shuffles to get intermediate pairs
"movapd %%xmm0, %%xmm1;"
"shufpd $1, %%xmm2, %%xmm1;"
"movapd %%xmm2, %%xmm3;"
"shufpd $1, %%xmm4, %%xmm3;"
"movapd %%xmm4, %%xmm5;"
"shufpd $1, %%xmm6, %%xmm5;"
"addq $32, %1;"
"movq %5, %%rcx;"
"1:"
REGSTENCIL_ROTATE_1X2(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%4)")
REGSTENCIL_ROTATE_1X2(2, 3, 4, 5, 6, 7, 0, 1, "16(%1)", "16(%4)")
"subq $8, %%rcx;"
"je 2f;"
REGSTENCIL_ROTATE_1X2(4, 5, 6, 7, 0, 1, 2, 3, "32(%1)", "32(%4)")
REGSTENCIL_ROTATE_1X2(6, 7, 0, 1, 2, 3 ,4, 5, "48(%1)", "48(%4)")
"addq $64, %1;"
"addq $64, %4;"
"jmp 1b;"
"2:"
REGSTENCIL_ROTATE_1X2(4, 5, 6, 7, 0, 1, 2, 3, "0(%2)", "32(%4)")
REGSTENCIL_ROTATE_1X2(6, 7, 0, 1, 2, 3 ,4, 5, "16(%2)", "48(%4)")
: "+r" (minus), "+r" (in), "+r" (plus), "+r" (weights),
  "+r" (out)
: "r" (count)
: "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}

void stencil_x_row2_rotate(const double *minus1, const double *in1, const double *plus1, double *out1,
			   const double *minus2, const double *in2, const double *plus2, double *out2,
			   const double *weights, size_t count)
{
  asm volatile (
// fetch weights into registers
"movddup 0(%8), %%xmm12;"
"movddup 8(%8), %%xmm13;"
"movddup 16(%8), %%xmm14;"
"movddup 24(%8), %%xmm15;"
// preload first eight values
"movsd 0(%0), %%xmm0;"
"movsd 8(%0), %%xmm1;"
"movsd 16(%0), %%xmm2;"
"movsd 24(%0), %%xmm3;"
"movsd 0(%1), %%xmm4;"
"movsd 8(%1), %%xmm5;"
"movsd 16(%1), %%xmm6;"
"movsd 24(%1), %%xmm7;"
"movhpd 0(%4), %%xmm0;"
"movhpd 8(%4), %%xmm1;"
"movhpd 16(%4), %%xmm2;"
"movhpd 24(%4), %%xmm3;"
"movhpd 0(%5), %%xmm4;"
"movhpd 8(%5), %%xmm5;"
"movhpd 16(%5), %%xmm6;"
"movhpd 24(%5), %%xmm7;"
"addq $32, %1;"
"addq $32, %5;"
"movq %9, %%rcx;"
"1:"
#define ROTATE_2X2
#ifdef ROTATE_2X2
REGSTENCIL_ROTATE_2X2(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%5)", "0(%3)", "0(%7)")
REGSTENCIL_ROTATE_2X2(2, 3, 4, 5, 6, 7, 0, 1, "16(%1)", "16(%5)", "16(%3)", "16(%7)")
#else
REGSTENCIL_ROTATE_2X1(0, 1, 2, 3, 4, 5, 6, 7, "0(%1)", "0(%5)", "0(%3)", "0(%7)")
REGSTENCIL_ROTATE_2X1(1, 2, 3, 4, 5, 6, 7, 0, "8(%1)", "8(%5)", "8(%3)", "8(%7)")
REGSTENCIL_ROTATE_2X1(2, 3, 4, 5, 6, 7, 0, 1, "16(%1)", "16(%5)", "16(%3)", "16(%7)")
REGSTENCIL_ROTATE_2X1(3, 4, 5, 6, 7, 0, 1, 2, "24(%1)", "24(%5)", "24(%3)", "24(%7)")
#endif
"subq $8, %%rcx;"
"je 2f;"
#ifdef ROTATE_2X2
REGSTENCIL_ROTATE_2X2(4, 5, 6, 7, 0, 1, 2, 3, "32(%1)", "32(%5)", "32(%3)", "32(%7)")
REGSTENCIL_ROTATE_2X2(6, 7, 0, 1, 2, 3 ,4, 5, "48(%1)", "48(%5)", "48(%3)", "48(%7)")
#else
REGSTENCIL_ROTATE_2X1(4, 5, 6, 7, 0, 1, 2, 3, "32(%1)", "32(%5)", "32(%3)", "32(%7)")
REGSTENCIL_ROTATE_2X1(5, 6, 7, 0, 1, 2, 3, 4, "40(%1)", "40(%5)", "40(%3)", "40(%7)")
REGSTENCIL_ROTATE_2X1(6, 7, 0, 1, 2, 3 ,4, 5, "48(%1)", "48(%5)", "48(%3)", "48(%7)")
REGSTENCIL_ROTATE_2X1(7, 0, 1, 2, 3, 4, 5, 6, "56(%1)", "56(%5)", "56(%3)", "56(%7)")
#endif
"addq $64, %1;"
"addq $64, %3;"
"addq $64, %5;"
"addq $64, %7;"
"jmp 1b;"
"2:"
#ifdef ROTATE_2X2
REGSTENCIL_ROTATE_2X2(4, 5, 6, 7, 0, 1, 2, 3, "0(%2)", "0(%6)", "32(%3)", "32(%7)")
REGSTENCIL_ROTATE_2X2(6, 7, 0, 1, 2, 3 ,4, 5, "16(%2)", "16(%6)", "48(%3)", "48(%7)")
#else
REGSTENCIL_ROTATE_2X1(4, 5, 6, 7, 0, 1, 2, 3, "0(%2)", "0(%6)", "32(%3)", "32(%7)")
REGSTENCIL_ROTATE_2X1(5, 6, 7, 0, 1, 2, 3, 4, "8(%2)", "8(%6)", "40(%3)", "40(%7)")
REGSTENCIL_ROTATE_2X1(6, 7, 0, 1, 2, 3 ,4, 5, "16(%2)", "16(%6)", "48(%3)", "48(%7)")
REGSTENCIL_ROTATE_2X1(7, 0, 1, 2, 3, 4, 5, 6, "24(%2)", "24(%6)", "56(%3)", "56(%7)")
#endif
: "+r" (minus1), "+r" (in1), "+r" (plus1), "+r" (out1),
  "+r" (minus2), "+r" (in2), "+r" (plus2), "+r" (out2),
  "+r" (weights)
: "r" (count)
: "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}

void stencil_x(size_t dim_1, size_t dim_2, size_t dim_3,
	       const double *minus_ptr, size_t minus_stride_2, size_t minus_stride_3,
	       const double *in_ptr, size_t in_stride_2, size_t in_stride_3,
	       const double *plus_ptr, size_t plus_stride_2, size_t plus_stride_3,
	       double *out_ptr, size_t out_stride_2, size_t out_stride_3,
	       double ae, double be, double ce, double de)
{
  double weights[12] = { -ae, -be, -ce, -de,
			 -ae, -ae, // these are in case we need pre-vectorized versions
			 -be, -be,
			 -ce, -ce,
			 -de, -de };

  for(int z = 0; z < dim_3; z++) {
    const double *minus_y = minus_ptr;
    const double *in_y = in_ptr;
    const double *plus_y = plus_ptr;
    double *out_y = out_ptr;

    for(int y = 0; y < dim_2; y++) {
      const double *minus_x = minus_y;
      const double *in_x = in_y;
      const double *plus_x = plus_y;
      double *out_x = out_y;

#if 1
      stencil_x_row_rotate2(minus_x, in_x, plus_x, weights, out_x, dim_1);

      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);
#else
      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);

      stencil_x_row2_rotate(minus_x, in_x, plus_x, out_x,
			    minus_y, in_y, plus_y, out_y,
			    weights, dim_1);
      y++;

      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);
#endif
    }

    add_offset(minus_ptr, minus_stride_3);
    add_offset(in_ptr, in_stride_3);
    add_offset(plus_ptr, plus_stride_3);
    add_offset(out_ptr, out_stride_3);
  }
}

void stencil_y_row(const double *in0, const double *in1, const double *in2, const double *in3,
		   const double *in4, const double *in5, const double *in6, const double *in7,
		   const double *in8, const double *weights, double *out, size_t count)
{
  asm volatile (
"xorq %%rax, %%rax;"
"movq %11, %%rcx;"
"movddup 24(%9), %%xmm3;"
"movddup 16(%9), %%xmm7;"
"movddup 8(%9), %%xmm11;"
"movddup 0(%9), %%xmm15;"
"1:"
"movapd 0(%%rax,%0), %%xmm0;"
"movapd 16(%%rax,%0), %%xmm1;"
"movapd 0(%%rax,%1), %%xmm4;"
"movapd 16(%%rax,%1), %%xmm5;"
"movapd 0(%%rax,%2), %%xmm8;"
"movapd 16(%%rax,%2), %%xmm9;"
"movapd 0(%%rax,%3), %%xmm12;"
"movapd 16(%%rax,%3), %%xmm13;"
"subpd 0(%%rax,%5), %%xmm12;"
"subpd 16(%%rax,%5), %%xmm13;"
"subpd 0(%%rax,%6), %%xmm8;"
"subpd 16(%%rax,%6), %%xmm9;"
"subpd 0(%%rax,%7), %%xmm4;"
"subpd 16(%%rax,%7), %%xmm5;"
"subpd 0(%%rax,%8), %%xmm0;"
"subpd 16(%%rax,%8), %%xmm1;"
"mulpd %%xmm15, %%xmm12;"
"mulpd %%xmm15, %%xmm13;"
"mulpd %%xmm11, %%xmm8;"
"mulpd %%xmm11, %%xmm9;"
"mulpd %%xmm7, %%xmm4;"
"mulpd %%xmm7, %%xmm5;"
"mulpd %%xmm3, %%xmm0;"
"mulpd %%xmm3, %%xmm1;"
"addpd %%xmm8, %%xmm12;"
"addpd %%xmm9, %%xmm13;"
"addpd %%xmm4, %%xmm12;"
"addpd %%xmm5, %%xmm13;"
"addpd %%xmm0, %%xmm12;"
"addpd %%xmm1, %%xmm13;"
MOVNTDQ " %%xmm12, 0(%%rax,%10);"
MOVNTDQ " %%xmm13, 16(%%rax,%10);"
"addq $32, %%rax;"
"decq %%rcx;"
"jne 1b;"
: /* no outputs */
: "r" (in0), "r" (in1), "r" (in2), "r" (in3), "r" (in4), "r" (in5), "r" (in6), "r" (in7), "r" (in8),
  "r" (weights), "r" (out), "r" (count)
: "rax", "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}

void stencil_y(size_t strip_width, size_t strip_height, size_t num_strips,
	       const double *minus_ptr, size_t minus_line_stride, size_t minus_strip_stride,
	       const double *in_ptr, size_t in_line_stride, size_t in_strip_stride,
	       const double *plus_ptr, size_t plus_line_stride, size_t plus_strip_stride,
	       double *out_ptr, size_t out_line_stride, size_t out_strip_stride,
	       double ae, double be, double ce, double de)
{
  double weights[4] = { -ae, -be, -ce, -de };

  // outer loop is over strips
  for(size_t strip_idx = 0; strip_idx < num_strips; strip_idx++) {
    // set up pointers to rows within strip
    const double *row0 = minus_ptr + (0 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row1 = minus_ptr + (1 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row2 = minus_ptr + (2 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row3 = minus_ptr + (3 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row4 = in_ptr + (0 * in_line_stride) + (strip_idx * in_strip_stride);
    const double *row5 = in_ptr + (1 * in_line_stride) + (strip_idx * in_strip_stride);
    const double *row6 = in_ptr + (2 * in_line_stride) + (strip_idx * in_strip_stride);
    const double *row7 = in_ptr + (3 * in_line_stride) + (strip_idx * in_strip_stride);
    double *row_o = out_ptr + (strip_idx * out_strip_stride);

    for(size_t line_idx = 0; line_idx < (strip_height - 4); line_idx++) {
      const double *row8 = row7 + in_line_stride;
      stencil_y_row(row0, row1, row2, row3, row4, row5, row6, row7, row8,
		    weights, row_o, strip_width / 4);
      if(0 && (strip_idx == 0)) {
	printf("F: %g %g %g %g %g %g %g %g %g -> %g\n",
	       *row0, *row1,  *row2, *row3, *row4, *row5, *row6, *row7, *row8, *row_o);
      }
      row0 = row1;
      row1 = row2;
      row2 = row3;
      row3 = row4;
      row4 = row5;
      row5 = row6;
      row6 = row7;
      row7 = row8;
      row_o = row_o + out_line_stride;
    }

    // last part uses plus data
    for(size_t line_idx = 0; line_idx < 4; line_idx++) {
      const double *row8 = plus_ptr + (line_idx * plus_line_stride) + (strip_idx * plus_strip_stride);
      stencil_y_row(row0, row1, row2, row3, row4, row5, row6, row7, row8,
		    weights, row_o, strip_width / 4);
      if(0 && (strip_idx == 0)) {
	printf("F: %g %g %g %g %g %g %g %g %g -> %g\n",
	       *row0, *row1,  *row2, *row3, *row4, *row5, *row6, *row7, *row8, *row_o);
      }
      row0 = row1;
      row1 = row2;
      row2 = row3;
      row3 = row4;
      row4 = row5;
      row5 = row6;
      row6 = row7;
      row7 = row8;
      row_o = row_o + out_line_stride;
    }
  }
}

void fast_stencil(int grid_size, int dim_1, int dim_2, int dim_3,
		  const double *minus_ptr, size_t minus_stride_1, size_t minus_stride_2, size_t minus_stride_3,
		  const double *in_ptr, size_t in_stride_1, size_t in_stride_2, size_t in_stride_3,
		  const double *plus_ptr, size_t plus_stride_1, size_t plus_stride_2, size_t plus_stride_3,
		  double *out_ptr, size_t out_stride_1, size_t out_stride_2, size_t out_stride_3)
{
  double xmax = 1.0;
  double xmin = 0.0;
  double ds = (grid_size - 1) / (((xmax - xmin) /* / (rank->global_grid_size[0] - 1)*/ / 100.0 / L_REF));
  //printf("DS = %g\n", ds);

  double ae = 4.0 / 5.0 * ds;
  double be = -1.0 / 5.0 * ds;
  double ce = 4.0 / 105.0 * ds;
  double de = -1.0 / 280.0 * ds;
  //ae = 1; be = 1; ce = 1; de = 1;

  // dense x stencil case
  if((minus_stride_1 == 8) && (in_stride_1 == 8) && (plus_stride_1 == 8) && (out_stride_1 == 8)) {
    //printf("x stencil\n");
    stencil_x(dim_1, dim_2, dim_3,
	      minus_ptr, minus_stride_2, minus_stride_3,
	      in_ptr, in_stride_2, in_stride_3,
	      plus_ptr, plus_stride_2, plus_stride_3,
	      out_ptr, out_stride_2, out_stride_3,
	      ae, be, ce, de);
    return;
  }

  // y stencil case
  if((minus_stride_2 == 8) && (in_stride_2 == 8) && (plus_stride_2 == 8) && (out_stride_2 == 8)) {
    if((minus_stride_3 == (dim_2 * minus_stride_2)) &&
       (in_stride_3 == (dim_2 * in_stride_2)) &&
       (plus_stride_3 == (dim_2 * plus_stride_2)) &&
       (out_stride_3 == (dim_2 * out_stride_2))) {
      size_t strip_width = 256; //dim_2  * dim_3;
      //printf("z stencil\n");
      stencil_y(strip_width, dim_1, dim_2 * dim_3 / strip_width,
		minus_ptr, minus_stride_1 / sizeof(double), strip_width,
		in_ptr, in_stride_1 / sizeof(double), strip_width,
		plus_ptr, plus_stride_1 / sizeof(double), strip_width,
		out_ptr, out_stride_1 / sizeof(double), strip_width,
		ae, be, ce, de);
    } else {
      //printf("y stencil\n");
      stencil_y(dim_2, dim_1, dim_3,
		minus_ptr, minus_stride_1 / sizeof(double), minus_stride_3 / sizeof(double),
		in_ptr, in_stride_1 / sizeof(double), in_stride_3 / sizeof(double),
		plus_ptr, plus_stride_1 / sizeof(double), plus_stride_3 / sizeof(double),
		out_ptr, out_stride_1 / sizeof(double), out_stride_3 / sizeof(double),
		ae, be, ce, de);
    }
    return;
  }

  // dead code
  assert(0);
  // x stencil case
  for(int z = 0; z < dim_3; z++) {
    const double *minus_y = minus_ptr;
    const double *in_y = in_ptr;
    const double *plus_y = plus_ptr;
    double *out_y = out_ptr;

    for(int y = 0; y < dim_2; y++) {
      const double *minus_x = minus_y;
      const double *in_x = in_y;
      const double *plus_x = plus_y;
      double *out_x = out_y;

      pencil_2(dim_1,
	       ae, be, ce, de,
	       minus_x, minus_stride_1,
	       in_x, in_stride_1,
	       plus_x, plus_stride_1,
	       out_x, out_stride_1);

      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);
    }

    add_offset(minus_ptr, minus_stride_3);
    add_offset(in_ptr, in_stride_3);
    add_offset(plus_ptr, plus_stride_3);
    add_offset(out_ptr, out_stride_3);
  }
}
#endif

static double now(void)
{
#ifdef __MACH__
  uint64_t time_raw = mach_absolute_time();
  mach_timebase_info_data_t tbinfo;
  mach_timebase_info(&tbinfo);
  return (1e-9 * time_raw) * tbinfo.numer / tbinfo.denom;
#else
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return ((1.0 * ts.tv_sec) + (1e-9 * ts.tv_nsec));
#endif
}

class CacheFlusher {
public:
  CacheFlusher(int _size = 64 * 1024 * 1024)
  {
    size = _size >> 2;
    buffer = new int[size];
    for(int i = 0; i < size; i++) buffer[i] = 0;
  }

  void flush_cache(void)
  {
    for(int i = 0; i < size; i++) buffer[i]++;
  }

protected:
  int size;
  int *buffer;
};

#ifdef _POSIX_BARRIERS
pthread_barrier_t barrier;
#endif

template <typename T>
void ALIGN_POINTER(T *& p, size_t amt)
{
  size_t rem = reinterpret_cast<size_t>(p) % amt;
  if(rem) {
    p = reinterpret_cast<T *>(amt * ((reinterpret_cast<size_t>(p) / amt) + 1));
  }
}

class Thread {
public:
  Thread(int _size, int _ghost, int _grid_size, int _repeat, int _thread_idx, int _num_threads)
    : size(_size), ghost(_ghost), grid_size(_grid_size), repeat(_repeat), thread_idx(_thread_idx), num_threads(_num_threads)
  {
    thread = 0;
  }

  static void *thread_entry(void *data)
  {
    ((Thread *)data)->thread_main();
    return data;
  }

  void report(const char *name, double t_start, double t_stop)
  {
    double elapsed = t_stop - t_start;
    double ns_per_pt = 1e9 * elapsed / size / size / size;
    double bw = sizeof(double) * size * size * (2 * size + 2 * ghost) * num_threads / elapsed / 1e9;
    printf("%s = %g s, %5.1f ns/point, %5.2f GB/s\n", name, elapsed, ns_per_pt, bw);
  }

  void start(void)
  {
    pthread_create(&thread, 0, &thread_entry, (void *)this);
  }

  void finish(void)
  {
    void *dummy;
    pthread_join(thread, &dummy);
  }

  void barrier_wait(void)
  {
    if(thread_idx >= 0) {
#ifdef _POSIX_BARRIERS
      pthread_barrier_wait(&barrier);
#else
      assert(0);
#endif
    }
  }

  void thread_main(void)
  {
    CacheFlusher cf;
    int block_total = size * size * size;
    int edge_total = size * size * ghost;

    int mem_waste = 1;
    double *in = new double[block_total * mem_waste + 4];
    double *out_slow = new double[block_total * mem_waste + 4];
    double *out_fast = new double[block_total * mem_waste + 4];
    double *minus = new double[edge_total + 4];
    double *plus = new double[edge_total + 4];
    ALIGN_POINTER(in, 128);
    ALIGN_POINTER(out_slow, 128);
    ALIGN_POINTER(out_fast, 128);
    ALIGN_POINTER(minus, 128);
    ALIGN_POINTER(plus, 128);
    printf("in = %p, out_slow = %p, out_fast = %p, minus = %p, plus = %p\n",
           in, out_slow, out_fast, minus, plus);
  
    double v = 0.0;
    double step = 1.0;
    for(int i = 0; i < block_total; i++) { in[i] = v; v += step; }
    for(int i = 0; i < edge_total; i++) { minus[i] = v; v += step; }
    for(int i = 0; i < edge_total; i++) { plus[i] = v; v += step; }
    for(int i = 0; i < block_total; i++) { out_slow[i] = v; v += step; }
    for(int i = 0; i < block_total; i++) { out_fast[i] = 0; }

    double xmax = 1.0;
    double xmin = 0.0;
    double ds = (grid_size - 1) / (((xmax - xmin) /* / (rank->global_grid_size[0] - 1)*/ / 100.0 / L_REF));

    // x direction
    if(thread_idx <= 0) printf("X:\n");
    {
      double best_slow = 1e20;
      double best_fast = 1e20;
      for(int r = 0; r < repeat; r++) {
	cf.flush_cache();
	barrier_wait();
	double t_slow_start = now();
	slow_cpu_stencil(size, size, size, ds,
		     minus, sizeof(double), ghost * sizeof(double), ghost * size * sizeof(double),
		     in, sizeof(double), size * sizeof(double), size * size * sizeof(double),
		     plus, sizeof(double), ghost * sizeof(double), ghost * size * sizeof(double),
		     out_slow, sizeof(double), size * sizeof(double), size * size * sizeof(double));
	barrier_wait();
	double t_slow = now() - t_slow_start;
	if (t_slow < best_slow) best_slow = t_slow;
	
	for(int i = 0; i < block_total; i++) { out_fast[i] = 0; }
	cf.flush_cache();
	barrier_wait();
	double t_fast_start = now();
	fast_cpu_stencil(size, size, size, ds,
		     minus, sizeof(double), ghost * sizeof(double), ghost * size * sizeof(double),
		     in, sizeof(double), size * sizeof(double), size * size * sizeof(double),
		     plus, sizeof(double), ghost * sizeof(double), ghost * size * sizeof(double),
		     out_fast, sizeof(double), size * sizeof(double), size * size * sizeof(double));
	barrier_wait();
	double t_fast = now() - t_fast_start;
	if (t_fast < best_fast) best_fast = t_fast;
	int err_count = 0;
	for(int i = 0; i < block_total; i++)
	  if((fabs(out_slow[i] - out_fast[i]) * 1e10) > fabs(out_slow[i])) {
            if(err_count < 10) printf("[%d]: %g %g %g\n", i, out_slow[i], out_fast[i], out_slow[i] - out_fast[i]);
	    err_count++;
          }
	if(err_count) 
	  printf("errors = %d\n", err_count);
      }
      if(thread_idx <= 0) {
	report("slow", 0, best_slow);
	report("fast", 0, best_fast);
      }
    }

    // y direction
    if(thread_idx <= 0) printf("Y:\n");
    {
      double best_slow = 1e20;
      double best_fast = 1e20;
      for(int r = 0; r < repeat; r++) {
	cf.flush_cache();
	barrier_wait();
	double t_slow_start = now();
	slow_cpu_stencil(size, size, size, ds,
		     minus, size * sizeof(double), sizeof(double), ghost * size * sizeof(double),
		     in, size * sizeof(double), sizeof(double), size * size * sizeof(double),
		     plus, size * sizeof(double), sizeof(double), ghost * size * sizeof(double),
		     out_slow, size * sizeof(double), sizeof(double), size * size * sizeof(double));
	barrier_wait();
	double t_slow = now() - t_slow_start;
	if (t_slow < best_slow) best_slow = t_slow;
	
	for(int i = 0; i < block_total; i++) { out_fast[i] = 0; }
	cf.flush_cache();
	barrier_wait();
	double t_fast_start = now();
	fast_cpu_stencil(size, size, size, ds,
		     minus, size * sizeof(double), sizeof(double), ghost * size * sizeof(double),
		     in, size * sizeof(double), sizeof(double), size * size * sizeof(double),
		     plus, size * sizeof(double), sizeof(double), ghost * size * sizeof(double),
		     out_fast, size * sizeof(double), sizeof(double), size * size * sizeof(double));
	barrier_wait();
	double t_fast = now() - t_fast_start;
	if (t_fast < best_fast) best_fast = t_fast;
	int err_count = 0;
	for(int i = 0; i < block_total; i++)
	  if((fabs(out_slow[i] - out_fast[i]) * 1e10) > fabs(out_slow[i])) {
            if(err_count < 10) printf("[%d]: %g %g %g\n", i, out_slow[i], out_fast[i], out_slow[i] - out_fast[i]);
	    err_count++;
          }
	if(err_count) 
	  printf("errors = %d\n", err_count);
      }
      if(thread_idx <= 0) {
	report("slow", 0, best_slow);
	report("fast", 0, best_fast);
      }
    }
#if 0
    // y direction
    if(thread_idx <= 0) printf("Y:\n");
    for(int r = 0; r < repeat; r++) {
      cf.flush_cache();
      barrier_wait();
      double t_slow_start = now();
      slow_cpu_stencil(size, size, size, ds,
		   minus, size * sizeof(double), sizeof(double), ghost * size * sizeof(double),
		   in, size * sizeof(double), sizeof(double), size * size * sizeof(double),
		   plus, size * sizeof(double), sizeof(double), ghost * size * sizeof(double),
		   out_slow, size * sizeof(double), sizeof(double), size * size * sizeof(double));
      double t_slow_stop = now();
      barrier_wait();
      if(thread_idx <= 0)
	report("slow", t_slow_start, t_slow_stop);
      for(int i = 0; i < block_total; i++) { out_fast[i] = 0; }
      cf.flush_cache();
      barrier_wait();
      double t_fast_start = now();
      fast_cpu_stencil(size, size, size, ds,
		   minus, size * sizeof(double), sizeof(double), ghost * size * sizeof(double),
		   in, size * sizeof(double), sizeof(double), size * size * sizeof(double),
		   plus, size * sizeof(double), sizeof(double), ghost * size * sizeof(double),
		   out_fast, size * sizeof(double), sizeof(double), size * size * sizeof(double));
      barrier_wait();
      double t_fast_stop = now();
      if(thread_idx <= 0)
	report("fast", t_fast_start, t_fast_stop);
      int err_count = 0;
      for(int i = 0; i < block_total; i++)
	if(out_slow[i] != out_fast[i])
	  err_count++;
      if(err_count) 
	printf("errors = %d\n", err_count);
    }
#endif
    // z direction
    if(thread_idx <= 0) printf("Z:\n");
    {
      double best_slow = 1e20;
      double best_fast = 1e20;
      for(int r = 0; r < repeat; r++) {
	cf.flush_cache();
	barrier_wait();
	double t_slow_start = now();
	slow_cpu_stencil(size, size, size, ds,
		     minus, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		     in, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		     plus, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		     out_slow, size * size * sizeof(double), sizeof(double), size * sizeof(double));
	barrier_wait();
	double t_slow = now() - t_slow_start;
	if (t_slow < best_slow) best_slow = t_slow;
	
	for(int i = 0; i < block_total; i++) { out_fast[i] = 0; }
	cf.flush_cache();
	barrier_wait();
	double t_fast_start = now();
	fast_cpu_stencil(size, size, size, ds,
		     minus, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		     in, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		     plus, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		     out_fast, size * size * sizeof(double), sizeof(double), size * sizeof(double));
	barrier_wait();
	double t_fast = now() - t_fast_start;
	if (t_fast < best_fast) best_fast = t_fast;
	int err_count = 0;
	for(int i = 0; i < block_total; i++)
	  if((fabs(out_slow[i] - out_fast[i]) * 1e10) > fabs(out_slow[i])) {
            if(err_count < 10) printf("[%d]: %g %g %g\n", i, out_slow[i], out_fast[i], out_slow[i] - out_fast[i]);
	    err_count++;
          }
	if(err_count) 
	  printf("errors = %d\n", err_count);
      }
      if(thread_idx <= 0) {
	report("slow", 0, best_slow);
	report("fast", 0, best_fast);
      }
    }
#if 0
    // Z direction
    if(thread_idx <= 0) printf("Z:\n");
    for(int r = 0; r < repeat; r++) {
      cf.flush_cache();
      barrier_wait();
      double t_slow_start = now();
      slow_stencil(grid_size, size, size, size,
		   minus, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		   in, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		   plus, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		   out_slow, size * size * sizeof(double), sizeof(double), size * sizeof(double));
      double t_slow_stop = now();
      barrier_wait();
      if(thread_idx <= 0)
	report("slow", t_slow_start, t_slow_stop);
      for(int i = 0; i < block_total; i++) { out_fast[i] = 0; }
      cf.flush_cache();
      barrier_wait();
      double t_fast_start = now();
      fast_stencil(grid_size, size, size, size,
		   minus, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		   in, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		   plus, size * size * sizeof(double), sizeof(double), size * sizeof(double),
		   out_fast, size * size * sizeof(double), sizeof(double), size * sizeof(double));
      barrier_wait();
      double t_fast_stop = now();
      if(thread_idx <= 0)
	report("fast", t_fast_start, t_fast_stop);
      int err_count = 0;
      for(int i = 0; i < block_total; i++)
	if(out_slow[i] != out_fast[i])
	  err_count++;
      if(err_count) 
	printf("errors = %d\n", err_count);
    }
#endif
  }

protected:
  int size, ghost, grid_size, repeat, thread_idx, num_threads;
  pthread_t thread;
};

int main(int argc, const char *argv[])
{
  int size = 64;
  int ghost = 4;
  int grid_size = size * 16;
  int repeat = 5;
  int threads = 0;

  for(int i = 1; i < argc; i++) {
    if(!strcmp(argv[i], "-s")) {
      size = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-g")) {
      ghost = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-r")) {
      repeat = atoi(argv[++i]);
      continue;
    }

    if(!strcmp(argv[i], "-t")) {
      threads = atoi(argv[++i]);
      continue;
    }
  }

  if(threads > 0) {
    Thread **t = new Thread *[threads];
#ifdef _POSIX_BARRIERS
    pthread_barrier_init(&barrier, 0, threads);
#else
    if(threads > 1)
      printf("warning: no barriers available!\n");
#endif
    for(int i = 0; i < threads; i++)
      t[i] = new Thread(size, ghost, grid_size, repeat, i, threads);
    for(int i = 0; i < threads; i++)
      t[i]->start();
    for(int i = 0; i < threads; i++)
      t[i]->finish();
    for(int i = 0; i < threads; i++)
      delete t[i];
    delete t;
  } else {
    Thread t(size, ghost, grid_size, repeat, -1, 1);
    t.thread_main();
  }
}
