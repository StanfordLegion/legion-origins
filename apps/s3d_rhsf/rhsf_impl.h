
#ifndef _RHSF_IMPL_H_
#define _RHSF_IMPL_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"
#include "arrays.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Arrays;

struct RhsfDistributeArgs {
  Point<3> dist_proc_pt;
  IndexSpace is_grid, is_subdist;
  IndexPartition ip_proc;
  Rect<3> dist_proc_subgrid, full_proc_subgrid;
  DynamicCollective global_sum_collective;
#ifdef NEW_BARRIER_EXCHANGE
  PhaseBarrier barrier_exchange_barrier;
#endif
#ifdef USE_CEMA
  LogicalRegion lr_cema_global;
  IndexSpace is_cema;
  PhaseBarrier cema_ready_barrier, cema_done_barrier;
#endif
  FieldSpace fs_ghost;
  LogicalRegion lr_ghost[0];
  // don't put anything after this!
};

void distribute_task(const Task *task,
                     const std::vector<PhysicalRegion> &regions,
                     Context ctx, HighLevelRuntime *runtime);

void get_init_temp_task(const Task *task,
                        const std::vector<PhysicalRegion> &regions,
                        Context ctx, HighLevelRuntime *runtime);

void clear_field_task(const Task *task,
		      const std::vector<PhysicalRegion> &regions,
		      Context ctx, HighLevelRuntime *runtime);

void calc_stencil_1d_cpu_task(const Task *task,
                              const std::vector<PhysicalRegion> &regions,
                              Context ctx, HighLevelRuntime *runtime);

#ifdef USE_GPU_KERNELS
void calc_stencil_1d_gpu_task(const Task *task,
                              const std::vector<PhysicalRegion> &regions,
                              Context ctx, HighLevelRuntime *runtime);
#endif

class AwaitMPITask : public IndexLauncher {
public:
  AwaitMPITask(S3DRank *rank,
               Domain domain,
               TaskArgument global_arg,
               ArgumentMap arg_map,
               Predicate pred = Predicate::TRUE_PRED,
               bool must = false,
               MapperID id = 0,
               MappingTagID tag = 0,
               bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = AWAIT_MPI_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_FORCE_RANK_MATCH;
public:
  static TimestepInfo cpu_base_impl(S3DRank *rank, const Task *task, 
				    const Rect<3> &subgrid_bounds,
				    const std::vector<RegionRequirement> &reqs,
				    const std::vector<PhysicalRegion> &regions,
				    Context ctx, HighLevelRuntime *runtime);
};

class HandoffToMPITask : public IndexLauncher {
public:
  HandoffToMPITask(S3DRank *rank,
                   Domain domain,
                   TaskArgument global_arg,
                   ArgumentMap arg_map,
                   Predicate pred = Predicate::TRUE_PRED,
                   bool must = false,
                   MapperID id = 0,
                   MappingTagID tag = 0,
                   bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = HANDOFF_TO_MPI_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_FORCE_RANK_MATCH;
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, 
                            const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

#endif
