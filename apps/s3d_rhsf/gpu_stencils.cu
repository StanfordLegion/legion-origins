#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <assert.h>

#include "cuda.h"
#include "cuda_runtime.h"
#include "gpu_help.h"

//#define DEBUG_CUDA

#ifdef DEBUG_CUDA
#define CUDA_SAFE_CALL(expr)                        \
  {                                                 \
    cudaError_t err = (expr);                       \
    if (err != cudaSuccess)                         \
    {                                               \
      printf("Cuda error: %s\n", cudaGetErrorString(err));  \
      assert(false);                                \
    }                                               \
  }
#else
#define CUDA_SAFE_CALL(expr)  expr
#endif

template <typename T>
static inline __host__ __device__ void add_offset(T *& ptr, off_t offset)
{
  ptr = (T *)(((char *)(ptr)) + offset);
}

#if 0
void slow_cpu_stencil(int dim_1, int dim_2, int dim_3, double ds,
		      const double *minus_ptr, size_t minus_stride_1, size_t minus_stride_2, size_t minus_stride_3,
		      const double *in_ptr, size_t in_stride_1, size_t in_stride_2, size_t in_stride_3,
		      const double *plus_ptr, size_t plus_stride_1, size_t plus_stride_2, size_t plus_stride_3,
		      double *out_ptr, size_t out_stride_1, size_t out_stride_2, size_t out_stride_3)
{
  double ae = 4.0 / 5.0 * ds;
  double be = -1.0 / 5.0 * ds;
  double ce = 4.0 / 105.0 * ds;
  double de = -1.0 / 280.0 * ds;

  for(int z = 0; z < dim_3; z++) {
    const double *minus_y = minus_ptr;
    const double *in_y = in_ptr;
    const double *plus_y = plus_ptr;
    double *out_y = out_ptr;

    for(int y = 0; y < dim_2; y++) {
      const double *minus_x = minus_y;
      const double *in_x = in_y;
      const double *plus_x = plus_y;
      double *out_x = out_y;

      // pencil in "x" direction
      {
	double window[9];

	// pre-fill window
	for(int i = 0; i < 4; i++) {
	  window[i] = *minus_x;
	  add_offset(minus_x, minus_stride_1);
	}

	for(int i = 0; i < 4; i++) {
	  window[i+4] = *in_x;
	  add_offset(in_x, in_stride_1);
	}

	// main loop
	for(int i = 0; i < dim_1 - 4; i++) {
	  window[8] = *in_x;
	  add_offset(in_x, in_stride_1);

	  double deriv = (((ae * (window[5] - window[3])
			    + be * (window[6] - window[2]))
			   + ce * (window[7] - window[1]))
			  + de * (window[8] - window[0]));
	      
	  if(0 && (y == 0) && (z == 0)) {
	    printf("%g %g %g %g %g %g %g %g %g -> %g\n",
		   window[0], window[1], window[2], window[3],
		   window[4], window[5], window[6], window[7],
		   window[8], deriv);
	  }
	  *out_x = deriv; add_offset(out_x, out_stride_1);

	  for(int j = 0; j < 8; j++)
	    window[j] = window[j + 1];
	}

	// final bit with plus data
	for(int i = 0; i < 4; i++) {
	  window[8] = *plus_x;
	  add_offset(plus_x, plus_stride_1);

	  double deriv = (((ae * (window[5] - window[3])
			    + be * (window[6] - window[2]))
			   + ce * (window[7] - window[1]))
			  + de * (window[8] - window[0]));
	      
	  if(0 && (y == 0) && (z == 0)) {
	    printf("%g %g %g %g %g %g %g %g %g -> %g\n",
		   window[0], window[1], window[2], window[3],
		   window[4], window[5], window[6], window[7],
		   window[8], deriv);
	  }
	  *out_x = deriv; add_offset(out_x, out_stride_1);

	  for(int j = 0; j < 8; j++)
	    window[j] = window[j + 1];
	}
      }

      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);
    }

    add_offset(minus_ptr, minus_stride_3);
    add_offset(in_ptr, in_stride_3);
    add_offset(plus_ptr, plus_stride_3);
    add_offset(out_ptr, out_stride_3);
  }
}


static void stencil_x(size_t dim_1, size_t dim_2, size_t dim_3,
		      const double *minus_ptr, size_t minus_stride_2, size_t minus_stride_3,
		      const double *in_ptr, size_t in_stride_2, size_t in_stride_3,
		      const double *plus_ptr, size_t plus_stride_2, size_t plus_stride_3,
		      double *out_ptr, size_t out_stride_2, size_t out_stride_3,
		      double ae, double be, double ce, double de)
{
  double weights[12] = { -ae, -be, -ce, -de,
			 -ae, -ae, // these are in case we need pre-vectorized versions
			 -be, -be,
			 -ce, -ce,
			 -de, -de };

  for(int z = 0; z < dim_3; z++) {
    const double *minus_y = minus_ptr;
    const double *in_y = in_ptr;
    const double *plus_y = plus_ptr;
    double *out_y = out_ptr;

    for(int y = 0; y < dim_2; y++) {
      const double *minus_x = minus_y;
      const double *in_x = in_y;
      const double *plus_x = plus_y;
      double *out_x = out_y;

#if 1
      stencil_x_row_rotate2(minus_x, in_x, plus_x, weights, out_x, dim_1);

      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);
#else
      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);

      stencil_x_row2_rotate(minus_x, in_x, plus_x, out_x,
			    minus_y, in_y, plus_y, out_y,
			    weights, dim_1);
      y++;

      add_offset(minus_y, minus_stride_2);
      add_offset(in_y, in_stride_2);
      add_offset(plus_y, plus_stride_2);
      add_offset(out_y, out_stride_2);
#endif
    }

    add_offset(minus_ptr, minus_stride_3);
    add_offset(in_ptr, in_stride_3);
    add_offset(plus_ptr, plus_stride_3);
    add_offset(out_ptr, out_stride_3);
  }
}

static void stencil_y_row(const double *in0, const double *in1, const double *in2, const double *in3,
			  const double *in4, const double *in5, const double *in6, const double *in7,
			  const double *in8, const double *weights, double *out, size_t count)
{
  asm volatile (
"xorq %%rax, %%rax;"
"movq %11, %%rcx;"
"movddup 24(%9), %%xmm3;"
"movddup 16(%9), %%xmm7;"
"movddup 8(%9), %%xmm11;"
"movddup 0(%9), %%xmm15;"
"1:"
"movapd 0(%%rax,%0), %%xmm0;"
"movapd 16(%%rax,%0), %%xmm1;"
"movapd 0(%%rax,%1), %%xmm4;"
"movapd 16(%%rax,%1), %%xmm5;"
"movapd 0(%%rax,%2), %%xmm8;"
"movapd 16(%%rax,%2), %%xmm9;"
"movapd 0(%%rax,%3), %%xmm12;"
"movapd 16(%%rax,%3), %%xmm13;"
"subpd 0(%%rax,%5), %%xmm12;"
"subpd 16(%%rax,%5), %%xmm13;"
"subpd 0(%%rax,%6), %%xmm8;"
"subpd 16(%%rax,%6), %%xmm9;"
"subpd 0(%%rax,%7), %%xmm4;"
"subpd 16(%%rax,%7), %%xmm5;"
"subpd 0(%%rax,%8), %%xmm0;"
"subpd 16(%%rax,%8), %%xmm1;"
"mulpd %%xmm15, %%xmm12;"
"mulpd %%xmm15, %%xmm13;"
"mulpd %%xmm11, %%xmm8;"
"mulpd %%xmm11, %%xmm9;"
"mulpd %%xmm7, %%xmm4;"
"mulpd %%xmm7, %%xmm5;"
"mulpd %%xmm3, %%xmm0;"
"mulpd %%xmm3, %%xmm1;"
"addpd %%xmm8, %%xmm12;"
"addpd %%xmm9, %%xmm13;"
"addpd %%xmm4, %%xmm12;"
"addpd %%xmm5, %%xmm13;"
"addpd %%xmm0, %%xmm12;"
"addpd %%xmm1, %%xmm13;"
MOVNTDQ " %%xmm12, 0(%%rax,%10);"
MOVNTDQ " %%xmm13, 16(%%rax,%10);"
"addq $32, %%rax;"
"decq %%rcx;"
"jne 1b;"
: /* no outputs */
: "r" (in0), "r" (in1), "r" (in2), "r" (in3), "r" (in4), "r" (in5), "r" (in6), "r" (in7), "r" (in8),
  "r" (weights), "r" (out), "r" (count)
: "rax", "rcx",
  "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
  "xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
  "memory"
		);
}

static void stencil_y(size_t strip_width, size_t strip_height, size_t num_strips,
		      const double *minus_ptr, size_t minus_line_stride, size_t minus_strip_stride,
		      const double *in_ptr, size_t in_line_stride, size_t in_strip_stride,
		      const double *plus_ptr, size_t plus_line_stride, size_t plus_strip_stride,
		      double *out_ptr, size_t out_line_stride, size_t out_strip_stride,
		      double ae, double be, double ce, double de)
{
  double weights[4] = { -ae, -be, -ce, -de };

  // outer loop is over strips
  for(size_t strip_idx = 0; strip_idx < num_strips; strip_idx++) {
    // set up pointers to rows within strip
    const double *row0 = minus_ptr + (0 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row1 = minus_ptr + (1 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row2 = minus_ptr + (2 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row3 = minus_ptr + (3 * minus_line_stride) + (strip_idx * minus_strip_stride);
    const double *row4 = in_ptr + (0 * in_line_stride) + (strip_idx * in_strip_stride);
    const double *row5 = in_ptr + (1 * in_line_stride) + (strip_idx * in_strip_stride);
    const double *row6 = in_ptr + (2 * in_line_stride) + (strip_idx * in_strip_stride);
    const double *row7 = in_ptr + (3 * in_line_stride) + (strip_idx * in_strip_stride);
    double *row_o = out_ptr + (strip_idx * out_strip_stride);

    for(size_t line_idx = 0; line_idx < (strip_height - 4); line_idx++) {
      const double *row8 = row7 + in_line_stride;
      stencil_y_row(row0, row1, row2, row3, row4, row5, row6, row7, row8,
		    weights, row_o, strip_width / 4);
      if(0 && (strip_idx == 0)) {
	printf("F: %g %g %g %g %g %g %g %g %g -> %g\n",
	       *row0, *row1,  *row2, *row3, *row4, *row5, *row6, *row7, *row8, *row_o);
      }
      row0 = row1;
      row1 = row2;
      row2 = row3;
      row3 = row4;
      row4 = row5;
      row5 = row6;
      row6 = row7;
      row7 = row8;
      row_o = row_o + out_line_stride;
    }

    // last part uses plus data
    for(size_t line_idx = 0; line_idx < 4; line_idx++) {
      const double *row8 = plus_ptr + (line_idx * plus_line_stride) + (strip_idx * plus_strip_stride);
      stencil_y_row(row0, row1, row2, row3, row4, row5, row6, row7, row8,
		    weights, row_o, strip_width / 4);
      if(0 && (strip_idx == 0)) {
	printf("F: %g %g %g %g %g %g %g %g %g -> %g\n",
	       *row0, *row1,  *row2, *row3, *row4, *row5, *row6, *row7, *row8, *row_o);
      }
      row0 = row1;
      row1 = row2;
      row2 = row3;
      row3 = row4;
      row4 = row5;
      row5 = row6;
      row6 = row7;
      row7 = row8;
      row_o = row_o + out_line_stride;
    }
  }
}
#endif


#define STENCIL_X_THREADS  384
#define STENCIL_X_ITERS     32

template<int ITERATIONS>
__global__ void 
__launch_bounds__(384,4)
stencil_x_kernel(size_t dim_1, size_t dim_2, size_t dim_3,
                 const double *minus_ptr, size_t minus_stride_2, size_t minus_stride_3,
                 const double *in_ptr, size_t in_stride_2, size_t in_stride_3,
                 const double *plus_ptr, size_t plus_stride_2, size_t plus_stride_3,
                 double *out_ptr, size_t out_stride_2, size_t out_stride_3,
                 const double ae, const double be, const double ce, const double de)
{
  int pos_x = threadIdx.x;
  int pos_y = threadIdx.y + blockIdx.y * blockDim.y;
  int pos_z =               blockIdx.z;

  // quit if we're not really in the grid for some reason
  if((pos_x >= dim_1) || (pos_y >= dim_2) || (pos_z >= dim_3)) return;

  extern __shared__ double shared_base[];

  // buffer in shared memory is padded by ghost cell size
  double *s_ptr = shared_base + (threadIdx.y * (8 + dim_1)) + pos_x + 4;

  const double *ghost_ptr;
  size_t     ghost_stride;
  int global_ghost_offset;
  int shared_ghost_offset;
  if (pos_x < (blockDim.x/2)) {
    ghost_ptr = minus_ptr;
    add_offset(ghost_ptr,                          minus_stride_2 * pos_y + minus_stride_3 * pos_z);
    ghost_stride = minus_stride_3 * gridDim.z;
    global_ghost_offset = (pos_x % 4);
    shared_ghost_offset = -4;
  } else {
    ghost_ptr = plus_ptr;
    add_offset(ghost_ptr,                           plus_stride_2 * pos_y + plus_stride_3 * pos_z);
    ghost_stride = plus_stride_3 * gridDim.z; 
    global_ghost_offset = (pos_x % 4);
    shared_ghost_offset = 4;
  }
  add_offset(in_ptr,    sizeof(double) * pos_x + in_stride_2 * pos_y + in_stride_3 * pos_z);
  add_offset(out_ptr,   sizeof(double) * pos_x + out_stride_2 * pos_y + out_stride_3 * pos_z);

  if (ITERATIONS > 0) {
    for (int i = 0; i < ITERATIONS; i++) {

      double my_ghost, my_in;
      my_in = stream_load(in_ptr);
      // Duplicate reads are no problem since they'll all hit in the cache
      my_ghost = stream_load(ghost_ptr+global_ghost_offset);

      __syncthreads();

      s_ptr[0] = my_in;
      if ((pos_x < 4) || (pos_x >= (dim_1-4))) s_ptr[shared_ghost_offset] = my_ghost;

      __syncthreads();

      double deriv = (((ae * (s_ptr[1] - s_ptr[-1])
                        + be * (s_ptr[2] - s_ptr[-2]))
                       + ce * (s_ptr[3] - s_ptr[-3]))
                      + de * (s_ptr[4] - s_ptr[-4]));

      stream_store(out_ptr, deriv);

      add_offset(in_ptr, in_stride_3 * gridDim.z);
      add_offset(ghost_ptr, ghost_stride);
      add_offset(out_ptr, out_stride_3 * gridDim.z);

      pos_z += gridDim.z;
    }
  } else {
    while (pos_z < dim_3) {

      double my_ghost, my_in;
      my_in = stream_load(in_ptr);
      // Duplicate reads are no problem since they'll all hit in the cache
      my_ghost = stream_load(ghost_ptr+global_ghost_offset);

      __syncthreads();

      s_ptr[0] = my_in;
      if ((pos_x < 4) || (pos_x >= (dim_1-4))) s_ptr[shared_ghost_offset] = my_ghost;

      __syncthreads();

      double deriv = (((ae * (s_ptr[1] - s_ptr[-1])
                        + be * (s_ptr[2] - s_ptr[-2]))
                       + ce * (s_ptr[3] - s_ptr[-3]))
                      + de * (s_ptr[4] - s_ptr[-4]));

      stream_store(out_ptr, deriv);

      add_offset(in_ptr, in_stride_3 * gridDim.z);
      add_offset(ghost_ptr, ghost_stride);
      add_offset(out_ptr, out_stride_3 * gridDim.z);

      pos_z += gridDim.z;
    }
  }
}

#if 0
__global__ void stencil_x_kernel(size_t dim_1, size_t dim_2, size_t dim_3,
				 const double *minus_ptr, size_t minus_stride_2, size_t minus_stride_3,
				 const double *in_ptr, size_t in_stride_2, size_t in_stride_3,
				 const double *plus_ptr, size_t plus_stride_2, size_t plus_stride_3,
				 double *out_ptr, size_t out_stride_2, size_t out_stride_3,
				 const double ae, const double be, const double ce, const double de)
{
  int pos_x = threadIdx.x;
  int pos_y = threadIdx.y + blockIdx.y * blockDim.y;
  int pos_z =               blockIdx.z;

  // quit if we're not really in the grid for some reason
  if((pos_x >= dim_1) || (pos_y >= dim_2) || (pos_z >= dim_3)) return;

  extern __shared__ double shared_base[];

  // update our pointers (minus and plus don't get the x offset)
  add_offset(minus_ptr,                          minus_stride_2 * pos_y + minus_stride_3 * pos_z);
  add_offset(in_ptr,    sizeof(double) * pos_x + in_stride_2 * pos_y + in_stride_3 * pos_z);
  add_offset(plus_ptr,                           plus_stride_2 * pos_y + plus_stride_3 * pos_z);
  add_offset(out_ptr,   sizeof(double) * pos_x + out_stride_2 * pos_y + out_stride_3 * pos_z);

  // buffer in shared memory is padded by ghost cell size
  double *s_ptr = shared_base + (threadIdx.y * (8 + dim_1)) + pos_x + 4;

  while(pos_z < dim_3) {
    // load data
    double my_ghost, my_in;
    my_in = stream_load(in_ptr);
    if(pos_x < 4)            my_ghost = stream_load(minus_ptr+pos_x);
    if(pos_x >= (dim_1 - 4)) my_ghost = stream_load(plus_ptr+pos_x + 4 - dim_1);

    syncthreads();

    s_ptr[0] = my_in;
    if(pos_x < 4)            s_ptr[-4] = my_ghost;
    if(pos_x >= (dim_1 - 4)) s_ptr[ 4] = my_ghost;

    syncthreads();

#if 1
    double deriv = (((ae * (s_ptr[1] - s_ptr[-1])
		      + be * (s_ptr[2] - s_ptr[-2]))
		     + ce * (s_ptr[3] - s_ptr[-3]))
		    + de * (s_ptr[4] - s_ptr[-4]));
#else
    double deriv = __dadd_rz(__dadd_rz(__dadd_rz(__dmul_rz(ae, __dadd_rz(s_ptr[1], -s_ptr[-1])),
						 __dmul_rz(be, __dadd_rz(s_ptr[2], -s_ptr[-2]))),
				       __dmul_rz(ce, __dadd_rz(s_ptr[3], -s_ptr[-3]))),
			     __dmul_rz(de, __dadd_rz(s_ptr[4], s_ptr[-4])));
#endif

    stream_store(out_ptr, deriv);

    add_offset(minus_ptr, minus_stride_3 * gridDim.z);
    add_offset(in_ptr, in_stride_3 * gridDim.z);
    add_offset(plus_ptr, plus_stride_3 * gridDim.z);
    add_offset(out_ptr, out_stride_3 * gridDim.z);
    
    pos_z += gridDim.z;
  }
}
#endif

#define STRIP_WIDTH 256

template<int HEIGHT>
__global__ void 
__launch_bounds__(STRIP_WIDTH,4)
stencil_y_kernel(size_t strip_width, size_t strip_height, size_t num_strips,
				 const double *minus_ptr, size_t minus_line_stride, size_t minus_strip_stride,
				 const double *in_ptr, size_t in_line_stride, size_t in_strip_stride,
				 const double *plus_ptr, size_t plus_line_stride, size_t plus_strip_stride,
				 double *out_ptr, size_t out_line_stride, size_t out_strip_stride,
				 const double ae, const double be, const double ce, const double de)
{
  int strip_pos = threadIdx.x + blockDim.x * blockIdx.x;
  int line_pos = threadIdx.y + blockDim.y * blockIdx.y;

  // position our pointers within our strip/line
  add_offset(minus_ptr, sizeof(double) * strip_pos + minus_strip_stride * line_pos);
  add_offset(in_ptr, sizeof(double) * strip_pos + in_strip_stride * line_pos);
  add_offset(plus_ptr, sizeof(double) * strip_pos + plus_strip_stride * line_pos);
  add_offset(out_ptr, sizeof(double) * strip_pos + out_strip_stride * line_pos);

  double w0, w1, w2, w3, w4, w5, w6, w7, w8;

  // read minus data and first four rows of in data
  w0 = stream_load(minus_ptr);  add_offset(minus_ptr, minus_line_stride);
  w1 = stream_load(minus_ptr);  add_offset(minus_ptr, minus_line_stride);
  w2 = stream_load(minus_ptr);  add_offset(minus_ptr, minus_line_stride);
  w3 = stream_load(minus_ptr);  add_offset(minus_ptr, minus_line_stride);
  w4 = stream_load(in_ptr);     add_offset(in_ptr,    in_line_stride);
  w5 = stream_load(in_ptr);     add_offset(in_ptr,    in_line_stride);
  w6 = stream_load(in_ptr);     add_offset(in_ptr,    in_line_stride);
  w7 = stream_load(in_ptr);     add_offset(in_ptr,    in_line_stride);

  // all but 4 rows are the same
  #pragma unroll 8  
  for(int i = 0; i < (HEIGHT - 4); i++) {
    w8 = stream_load(in_ptr);     add_offset(in_ptr,    in_line_stride);
    
    double deriv = (((ae * (w5 - w3)
		      + be * (w6 - w2))
		     + ce * (w7 - w1))
		    + de * (w8 - w0));

    stream_store(out_ptr, deriv); add_offset(out_ptr,   out_line_stride);
    w0 = w1; w1 = w2; w2 = w3; w3 = w4; w4 = w5; w5 = w6; w6 = w7; w7 = w8;
  }

  // last 4 rows use plus data
  for(int i = 0; i < 4; i++) {
    w8 = stream_load(plus_ptr);     add_offset(plus_ptr,    plus_line_stride);
    
    double deriv = (((ae * (w5 - w3)
		      + be * (w6 - w2))
		     + ce * (w7 - w1))
		    + de * (w8 - w0));

    stream_store(out_ptr, deriv); add_offset(out_ptr,   out_line_stride);
    w0 = w1; w1 = w2; w2 = w3; w3 = w4; w4 = w5; w5 = w6; w6 = w7; w7 = w8;
  }
}

void fast_gpu_stencil(int dim_1, int dim_2, int dim_3, double ds,
		      const double *minus_ptr, size_t minus_stride_1, size_t minus_stride_2, size_t minus_stride_3,
		      const double *in_ptr, size_t in_stride_1, size_t in_stride_2, size_t in_stride_3,
		      const double *plus_ptr, size_t plus_stride_1, size_t plus_stride_2, size_t plus_stride_3,
		      double *out_ptr, size_t out_stride_1, size_t out_stride_2, size_t out_stride_3)
{
  double ae = 4.0 / 5.0 * ds;
  double be = -1.0 / 5.0 * ds;
  double ce = 4.0 / 105.0 * ds;
  double de = -1.0 / 280.0 * ds;

  // dense x stencil case
  if((minus_stride_1 == 8) && (in_stride_1 == 8) && (plus_stride_1 == 8) && (out_stride_1 == 8)) {
    //printf("x stencil\n");
    assert((STENCIL_X_THREADS % dim_1) == 0);
    int rows_per_cta = STENCIL_X_THREADS / dim_1;
    dim3 block_size(dim_1, rows_per_cta);
    size_t shared_size = sizeof(double) * (8 + dim_1) * rows_per_cta;
#ifdef K20_ARCH
    CUDA_SAFE_CALL(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte)); 
#endif
    if ((dim_3 % STENCIL_X_ITERS) == 0) {
      dim3 grid_size(1, (dim_2 + rows_per_cta - 1) / rows_per_cta, dim_3 / STENCIL_X_ITERS); 
      stencil_x_kernel<STENCIL_X_ITERS><<< grid_size, block_size, shared_size>>>(dim_1, dim_2, dim_3,
                                                                 //stencil_x_kernel<<< 1, 1, shared_size >>>(dim_1, dim_2, dim_3,
                                                                 minus_ptr, minus_stride_2, minus_stride_3,
                                                                 in_ptr, in_stride_2, in_stride_3,
                                                                 plus_ptr, plus_stride_2, plus_stride_3,
                                                                 out_ptr, out_stride_2, out_stride_3,
                                                                 ae, be, ce, de);
    } else {
      dim3 grid_size(1, (dim_2 + rows_per_cta - 1) / rows_per_cta, 
                    (dim_3 > STENCIL_X_ITERS) ? dim_3 / STENCIL_X_ITERS: 1);
      stencil_x_kernel<-1><<< grid_size, block_size, shared_size>>>(dim_1, dim_2, dim_3,
                                                                 //stencil_x_kernel<<< 1, 1, shared_size >>>(dim_1, dim_2, dim_3,
                                                                 minus_ptr, minus_stride_2, minus_stride_3,
                                                                 in_ptr, in_stride_2, in_stride_3,
                                                                 plus_ptr, plus_stride_2, plus_stride_3,
                                                                 out_ptr, out_stride_2, out_stride_3,
                                                                 ae, be, ce, de);
    }
    return;
  }

  // y stencil case
  if((minus_stride_2 == 8) && (in_stride_2 == 8) && (plus_stride_2 == 8) && (out_stride_2 == 8)) {
    if((minus_stride_3 == (dim_2 * minus_stride_2)) &&
       (in_stride_3 == (dim_2 * in_stride_2)) &&
       (plus_stride_3 == (dim_2 * plus_stride_2)) &&
       (out_stride_3 == (dim_2 * out_stride_2))) {
      size_t num_strips = dim_2 * dim_3 / STRIP_WIDTH;
      dim3 block_size(STRIP_WIDTH, 1);
      dim3 grid_size(1, num_strips);
      size_t shared_size = 0;
      //printf("z stencil (%d,%d) (%d,%d)\n", grid_size.x, grid_size.y, block_size.x, block_size.y);
      switch (dim_1)
      {
        case 32:
          stencil_y_kernel<32><<< grid_size, block_size, shared_size>>>( STRIP_WIDTH, dim_1, dim_2 * dim_3 / STRIP_WIDTH,
                                                                         minus_ptr, minus_stride_1, STRIP_WIDTH* sizeof(double),
                                                                         in_ptr, in_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         plus_ptr, plus_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         out_ptr, out_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         ae, be, ce, de);
          break;
        case 48:
          stencil_y_kernel<48><<< grid_size, block_size, shared_size>>>( STRIP_WIDTH, dim_1, dim_2 * dim_3 / STRIP_WIDTH,
                                                                         minus_ptr, minus_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         in_ptr, in_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         plus_ptr, plus_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         out_ptr, out_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         ae, be, ce, de);
          break;
        case 64:
          stencil_y_kernel<64><<< grid_size, block_size, shared_size>>>( STRIP_WIDTH, dim_1, dim_2 * dim_3 / STRIP_WIDTH,
                                                                         minus_ptr, minus_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         in_ptr, in_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         plus_ptr, plus_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         out_ptr, out_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         ae, be, ce, de);
          break;
        case 96:
          stencil_y_kernel<96><<< grid_size, block_size, shared_size>>>( STRIP_WIDTH, dim_1, dim_2 * dim_3 / STRIP_WIDTH,
                                                                         minus_ptr, minus_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         in_ptr, in_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         plus_ptr, plus_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         out_ptr, out_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         ae, be, ce, de);
          break;
        case 128:
          stencil_y_kernel<128><<< grid_size, block_size, shared_size>>>( STRIP_WIDTH, dim_1, dim_2 * dim_3 / STRIP_WIDTH,
                                                                         minus_ptr, minus_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         in_ptr, in_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         plus_ptr, plus_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         out_ptr, out_stride_1, STRIP_WIDTH * sizeof(double),
                                                                         ae, be, ce, de);
          break;
        default:
          printf("ERROR: Unsupported dimension size %d.  You can instantiate it in gpu_stencils.cu\n", dim_1);
          assert(false);
      }
    } else {
      size_t num_strips = dim_3;
      dim3 block_size(dim_2, 1);
      dim3 grid_size(1, num_strips);
      size_t shared_size = 0;
      //printf("y stencil (%d,%d) (%d,%d)\n", grid_size.x, grid_size.y, block_size.x, block_size.y);
      switch (dim_1)
      {
        case 32:
          stencil_y_kernel<32><<< grid_size, block_size, shared_size>>>(dim_2, dim_1, dim_3,
                    minus_ptr, minus_stride_1, minus_stride_3,
                    in_ptr, in_stride_1, in_stride_3,
                    plus_ptr, plus_stride_1, plus_stride_3,
                    out_ptr, out_stride_1, out_stride_3,
                    ae, be, ce, de);
          break;
        case 48:
          stencil_y_kernel<48><<< grid_size, block_size, shared_size>>>(dim_2, dim_1, dim_3,
                    minus_ptr, minus_stride_1, minus_stride_3,
                    in_ptr, in_stride_1, in_stride_3,
                    plus_ptr, plus_stride_1, plus_stride_3,
                    out_ptr, out_stride_1, out_stride_3,
                    ae, be, ce, de);
          break;
        case 64:
          stencil_y_kernel<64><<< grid_size, block_size, shared_size>>>(dim_2, dim_1, dim_3,
                    minus_ptr, minus_stride_1, minus_stride_3,
                    in_ptr, in_stride_1, in_stride_3,
                    plus_ptr, plus_stride_1, plus_stride_3,
                    out_ptr, out_stride_1, out_stride_3,
                    ae, be, ce, de);
          break;
        case 96:
          stencil_y_kernel<96><<< grid_size, block_size, shared_size>>>(dim_2, dim_1, dim_3,
                    minus_ptr, minus_stride_1, minus_stride_3,
                    in_ptr, in_stride_1, in_stride_3,
                    plus_ptr, plus_stride_1, plus_stride_3,
                    out_ptr, out_stride_1, out_stride_3,
                    ae, be, ce, de);
          break;
        case 128:
          stencil_y_kernel<128><<< grid_size, block_size, shared_size>>>(dim_2, dim_1, dim_3,
                    minus_ptr, minus_stride_1, minus_stride_3,
                    in_ptr, in_stride_1, in_stride_3,
                    plus_ptr, plus_stride_1, plus_stride_3,
                    out_ptr, out_stride_1, out_stride_3,
                    ae, be, ce, de);
          break;
        default:
          printf("ERROR: Unsupported dimension size %d.  You can instantiate it in gpu_stencils.cu\n", dim_1);
          assert(false);
      }
    }
    return;
  }

  // not a case we handle
  assert(0);
}
