
#ifndef _CALC_FLUX_H_
#define _CALC_FLUX_H_

#include "rhsf.h"
#include "rhsf_mapper.h"
#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

class CalcYDiffFluxTask : public IndexLauncher {
public:
  CalcYDiffFluxTask(S3DRank *rank,
                    Domain domain,
                    TaskArgument global_arg,
                    ArgumentMap arg_map,
                    Predicate pred = Predicate::TRUE_PRED,
                    bool must = false,
                    MapperID id = 0,
                    MappingTagID tag = 0,
                    bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_YDIFFFLUX_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_ALL_GPU;
protected:
  static bool dense_calc_ydiffflux_task(S3DRank *rank, const Rect<3> &subgrid_bounds,
                                        const std::vector<RegionRequirement> &reqs,
                                        const std::vector<PhysicalRegion> &regions);

  static bool fast_calc_ydiffflux_task(RegionAccessor<AccessorType::Generic,double> fa_in_ds_mixavg,
                                RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys,
                                RegionAccessor<AccessorType::Generic,double> fa_in_yspecies,
                                RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw,
                                RegionAccessor<AccessorType::Generic,double> fa_in_avmolwt,
                                RegionAccessor<AccessorType::Generic,double> fa_in_rho,
                                RegionAccessor<AccessorType::Generic,double> fa_out_ydiffflux,
                                Rect<3> subgrid_bounds);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcYDiffFluxFieldTask : public IndexLauncher {
public:
  CalcYDiffFluxFieldTask(S3DRank *rank, int spec,
                         Domain domain,
                         TaskArgument global_arg,
                         ArgumentMap arg_map,
                         bool redundant,
                         Predicate pred = Predicate::TRUE_PRED,
                         bool must = false,
                         MapperID id = 0,
                         MappingTagID tag = 0,
                         bool add_requirements = true);   
  CalcYDiffFluxFieldTask(S3DRank *rank, 
                         const std::vector<int> &specs,
                         Domain domain,
                         TaskArgument global_arg,
                         ArgumentMap arg_map,
                         bool redundant,
                         Predicate prec = Predicate::TRUE_PRED,
                         bool must = false,
                         MapperID id = 0,
                         MappingTagID tag = 0,
                         bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
  std::vector<int> species;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_YDIFFFLUX_PER_FIELD_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_PRIORITIZE | RHSF_MAPPER_ALL_GPU; 
protected:
  static bool dense_calc_ydiffflux_task(const Rect<3> &subgrid_bounds,
                      RegionAccessor<AccessorType::Generic,double> fa_in_rho,
                      RegionAccessor<AccessorType::Generic,double> fa_in_avmolwt,
                      RegionAccessor<AccessorType::Generic,double> fa_in_yspec,
                      RegionAccessor<AccessorType::Generic,double> fa_in_ds_mixavg,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw_x,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw_y,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_mixmw_z,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys_x,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys_y,
                      RegionAccessor<AccessorType::Generic,double> fa_in_grad_ys_z,
                      RegionAccessor<AccessorType::Generic,double> fa_out_flux_x,
                      RegionAccessor<AccessorType::Generic,double> fa_out_flux_y,
                      RegionAccessor<AccessorType::Generic,double> fa_out_flux_z);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcHeatFluxTask : public IndexLauncher {
public:
  CalcHeatFluxTask(S3DRank *rank,
                   Domain domain,
                   TaskArgument global_arg,
                   ArgumentMap arg_map,
                   Predicate pred = Predicate::TRUE_PRED,
                   bool must = false,
                   MapperID id = 0,
                   MappingTagID tag = 0,
                   bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_HEATFLUX_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
protected:
  static void dense_calc_heatflux_task(size_t n_pts,
				       S3DRank::CPETable& cpe_data,
				       size_t n_spec,
				       const double *in_grad_t_x,
				       const double *in_grad_t_y,
				       const double *in_grad_t_z,
				       const double *in_lambda,
				       const double *in_temp,
				       double *out_h_spec, 
                                       off_t h_spec_stride,
				       const double *in_ydiffflux_x,
				       const double *in_ydiffflux_y,
				       const double *in_ydiffflux_z,
				       off_t ydiffflux_stride,
				       double *out_heatflux_x,
				       double *out_heatflux_y,
				       double *out_heatflux_z,
				       double *out_ydiffflux_last_x,
				       double *out_ydiffflux_last_y,
				       double *out_ydiffflux_last_z);
  static bool fast_calc_heatflux_task(RegionAccessor<AccessorType::Generic,double> fa_in_temp,
				      RegionAccessor<AccessorType::Generic,double> fa_in_lambda,
				      RegionAccessor<AccessorType::Generic,double> fa_in_grad_t_x,
				      RegionAccessor<AccessorType::Generic,double> fa_in_grad_t_y,
				      RegionAccessor<AccessorType::Generic,double> fa_in_grad_t_z,
				      PhysicalRegion ra_out_h_spec,
				      PhysicalRegion ra_in_ydiffflux,
				      RegionAccessor<AccessorType::Generic,double> fa_out_heatflux_x,
				      RegionAccessor<AccessorType::Generic,double> fa_out_heatflux_y,
				      RegionAccessor<AccessorType::Generic,double> fa_out_heatflux_z,
				      RegionAccessor<AccessorType::Generic,double> fa_out_ydiffflux_last_x,
				      RegionAccessor<AccessorType::Generic,double> fa_out_ydiffflux_last_y,
				      RegionAccessor<AccessorType::Generic,double> fa_out_ydiffflux_last_z,
				      S3DRank::CPETable& cpe_data,
				      int n_spec,
				      Rect<3> subgrid_bounds);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcEnthalpyTask : public IndexLauncher {
public:
  CalcEnthalpyTask(S3DRank *rank,
                   Domain domain,
                   TaskArgument global_arg,
                   ArgumentMap arg_map,
                   Predicate pred = Predicate::TRUE_PRED,
                   bool must = false,
                   MapperID id = 0,
                   MappingTagID tag = 0,
                   bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_ENTHALPY_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
protected:
  static bool dense_calc_enthalpy(S3DRank *rank, const Rect<3> &subgrid_bounds,
                                  RegionAccessor<AccessorType::Generic,double> fa_in_temp,
                                  const std::vector<RegionRequirement> &reqs,
                                  const std::vector<PhysicalRegion> &regions);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcShortHeatFluxTask : public IndexLauncher {
public:
  CalcShortHeatFluxTask(S3DRank *rank,
                        Domain domain,
                        TaskArgument global_arg,
                        ArgumentMap arg_map,
                        Predicate pred = Predicate::TRUE_PRED,
                        bool must = false,
                        MapperID id = 0,
                        MappingTagID tag = 0,
                        bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_SHORT_HEATFLUX_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const bool GPU_BASE_LEAF = true;
  static const int MAPPER_TAG = RHSF_MAPPER_ALL_GPU;
protected:
  static bool dense_short_heatflux(S3DRank *rank, const Rect<3> &subgrid_bounds,
                                   RegionAccessor<AccessorType::Generic,double> fa_lambda,
                                   RegionAccessor<AccessorType::Generic,double> fa_grad_t_x,
                                   RegionAccessor<AccessorType::Generic,double> fa_grad_t_y,
                                   RegionAccessor<AccessorType::Generic,double> fa_grad_t_z,
                                   RegionAccessor<AccessorType::Generic,double> fa_heatflux_x,
                                   RegionAccessor<AccessorType::Generic,double> fa_heatflux_y,
                                   RegionAccessor<AccessorType::Generic,double> fa_heatflux_z,
                                   RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_x,
                                   RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_y,
                                   RegionAccessor<AccessorType::Generic,double> fa_ydiffflux_last_z,
                                   const std::vector<RegionRequirement> &reqs,
                                   const std::vector<PhysicalRegion> &regions);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
  static void gpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

class CalcTauTask : public IndexLauncher {
public:
  CalcTauTask(S3DRank *rank,
              Domain domain,
              TaskArgument global_arg,
              ArgumentMap arg_map,
              Predicate pred = Predicate::TRUE_PRED,
              bool must = false,
              MapperID id = 0,
              MappingTagID tag = 0,
              bool add_requirements = true);
public:
  void launch_check_fields(Context ctx, HighLevelRuntime *runtime);
public:
  S3DRank *rank;
public:
  static const char * const TASK_NAME;
  static const int TASK_ID = CALC_TAU_TASK_ID;
  static const bool CPU_BASE_LEAF = true;
  static const int MAPPER_TAG = 0;
protected:
  static bool dense_calc_tau_task(RegionAccessor<AccessorType::Generic,double> fa_out_tau_xx,
                                 RegionAccessor<AccessorType::Generic,double> fa_out_tau_xy,
                                 RegionAccessor<AccessorType::Generic,double> fa_out_tau_xz,
                                 RegionAccessor<AccessorType::Generic,double> fa_out_tau_yy,
                                 RegionAccessor<AccessorType::Generic,double> fa_out_tau_yz,
                                 RegionAccessor<AccessorType::Generic,double> fa_out_tau_zz,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_viscosity,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_x_x,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_x_y,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_x_z,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_y_x,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_y_y,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_y_z,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_z_x,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_z_y,
                                 RegionAccessor<AccessorType::Generic,double> fa_in_grad_vel_z_z,
                                 Rect<3> subgrid_bounds);
  static bool fast_calc_tau_task(RegionAccessor<AccessorType::Generic,double> fa_in_viscosity,
                          PhysicalRegion ra_in_grad_vel,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_xx,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_xy,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_xz,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_yy,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_yz,
                          RegionAccessor<AccessorType::Generic,double> fa_out_tau_zz,
                          Rect<3> subgrid_bounds);
public:
  static void cpu_base_impl(S3DRank *rank, const Task *task, const Rect<3> &subgrid_bounds,
                            const std::vector<RegionRequirement> &reqs,
                            const std::vector<PhysicalRegion> &regions,
                            Context ctx, HighLevelRuntime *runtime);
};

#endif
