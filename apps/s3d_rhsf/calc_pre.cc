
#include "rhsf.h"
#include "calc_pre.h"
#include "legion.h"
#include "arrays.h"
#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
#include <x86intrin.h>
#endif

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using namespace LegionRuntime::Arrays;

#if defined(USE_SSE_KERNELS) || defined(USE_AVX_KERNELS)
static inline __m128d _mm_stream_load_pd(const double *ptr) 
{
#ifdef __SSE4_1__
  return _mm_castsi128_pd(_mm_stream_load_si128(reinterpret_cast<__m128i *>(const_cast<double *>(ptr))));
#else
  return *(const __m128d *)ptr;
#endif
}
#endif

#ifdef USE_AVX_KERNELS
template<unsigned BOUNDARY>
static inline bool aligned(const void *ptr)
{
  return ((((unsigned long)ptr) & (BOUNDARY-1)) == 0);
}
#endif

CalcEnergyTask::CalcEnergyTask(S3DRank *r,
                               Domain domain,
                               TaskArgument global_arg,
                               ArgumentMap arg_map,
                               Predicate pred,
                               bool must,
                               MapperID id,
                               MappingTagID tag,
                               bool add_requirements)
 : IndexLauncher(CalcEnergyTask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | CalcEnergyTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    rr_out.add_field(FID_RHS_5_DX_IN);
    rr_out.add_field(FID_RHS_5_DY_IN);
    rr_out.add_field(FID_RHS_5_DZ_IN);
    add_region_requirement(rr_out);

    RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
    rr_q.add_field(FID_RHO_E);
    add_region_requirement(rr_q);

    RegionRequirement rr_int(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    rr_int.add_field(FID_VEL_X);
    rr_int.add_field(FID_VEL_Y);
    rr_int.add_field(FID_VEL_Z);
    rr_int.add_field(FID_PRESSURE);
    rr_int.add_field(FID_TAU_XX);
    rr_int.add_field(FID_TAU_XY);
    rr_int.add_field(FID_TAU_XZ);
    rr_int.add_field(FID_TAU_YY);
    rr_int.add_field(FID_TAU_YZ);
    rr_int.add_field(FID_TAU_ZZ);
    rr_int.add_field(FID_HEATFLUX_X);
    rr_int.add_field(FID_HEATFLUX_Y);
    rr_int.add_field(FID_HEATFLUX_Z);
    add_region_requirement(rr_int);
  }
}

/*static*/ const char * const CalcEnergyTask::TASK_NAME = "calc_energy_task";

void CalcEnergyTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing
}

/*static*/
void CalcEnergyTask::dense_energy_precalc_task(size_t n_pts,
			       /*const*/ double *rho_e, /*const*/ double *vel_x, 
                               /*const*/ double *vel_y, /*const*/ double *vel_z,
			       /*const*/ double *pressure, /*const*/ double *heatflux_x, 
                               /*const*/ double *heatflux_y, /*const*/ double *heatflux_z,
			       /*const*/ double *tau_xx, /*const*/ double *tau_xy, /*const*/ double *tau_xz,
			       /*const*/ double *tau_yy, /*const*/ double *tau_yz, /*const*/ double *tau_zz,
			       double *precalc_x, double *precalc_y, double *precalc_z)
{
#ifdef BULLDOZER
// Block for L1, only consume the 
// first way in each set.
#define FULL_STRIP_SIZE 64 
#else
  // Size for L1 cache
  // 7 arrays * 8B * 512 = 28K bytes
#define FULL_STRIP_SIZE 512 
#endif

#if defined (USE_AVX_KERNELS)
  __m256d rho_pres[FULL_STRIP_SIZE/4];
  __m256d local_vel_x[FULL_STRIP_SIZE/4];
  __m256d local_vel_y[FULL_STRIP_SIZE/4];
  __m256d local_vel_z[FULL_STRIP_SIZE/4];
  __m256d local_tau_xy[FULL_STRIP_SIZE/4];
  __m256d local_tau_xz[FULL_STRIP_SIZE/4];
  __m256d local_tau_yz[FULL_STRIP_SIZE/4];
#elif defined(USE_SSE_KERNELS)
  __m128d rho_pres[FULL_STRIP_SIZE/2];
  __m128d local_vel_x[FULL_STRIP_SIZE/2];
  __m128d local_vel_y[FULL_STRIP_SIZE/2];
  __m128d local_vel_z[FULL_STRIP_SIZE/2];
  __m128d local_tau_xy[FULL_STRIP_SIZE/2];
  __m128d local_tau_xz[FULL_STRIP_SIZE/2];
  __m128d local_tau_yz[FULL_STRIP_SIZE/2];
#endif
  // intermediate buffers we hope will stay in the L1
  double rho_e_plus_pressure[FULL_STRIP_SIZE];

  while(n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined(USE_AVX_KERNELS)
      // Load our values into the cache
      if (aligned<32>(rho_e) && aligned<32>(pressure) && aligned<32>(vel_x) &&
          aligned<32>(vel_y) && aligned<32>(vel_z) && aligned<32>(tau_xy) &&
          aligned<32>(tau_xz) && aligned<32>(tau_yz)) {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) 
          rho_pres[i] = _mm256_add_pd(_mm256_load_pd(rho_e+(i<<2)),_mm256_load_pd(pressure+(i<<2)));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_vel_x[i] = _mm256_load_pd(vel_x+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_vel_y[i] = _mm256_load_pd(vel_y+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_vel_z[i] = _mm256_load_pd(vel_z+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_tau_xy[i] = _mm256_load_pd(tau_xy+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_tau_xz[i] = _mm256_load_pd(tau_xz+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_tau_yz[i] = _mm256_load_pd(tau_yz+(i<<2));
      } else {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) 
          rho_pres[i] = _mm256_add_pd(_mm256_loadu_pd(rho_e+(i<<2)),_mm256_loadu_pd(pressure+(i<<2)));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_vel_x[i] = _mm256_loadu_pd(vel_x+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_vel_y[i] = _mm256_loadu_pd(vel_y+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_vel_z[i] = _mm256_loadu_pd(vel_z+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_tau_xy[i] = _mm256_loadu_pd(tau_xy+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_tau_xz[i] = _mm256_loadu_pd(tau_xz+(i<<2));
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++)
          local_tau_yz[i] = _mm256_loadu_pd(tau_yz+(i<<2));
      }
      if (aligned<32>(precalc_x) && aligned<32>(precalc_y) && aligned<32>(precalc_z) &&
          aligned<32>(tau_xx) && aligned<32>(tau_yy) && aligned<32>(tau_zz) &&
          aligned<32>(heatflux_x) && aligned<32>(heatflux_y) && aligned<32>(heatflux_z)) {
        // X direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d local_x = _mm256_xor_pd(local_vel_x[i],_mm256_set1_pd(-0.0));
          local_x = _mm256_mul_pd(local_x,_mm256_sub_pd(rho_pres[i],_mm256_load_pd(tau_xx+(i<<2))));
          local_x = _mm256_sub_pd(local_x,_mm256_load_pd(heatflux_x+(i<<2)));
          local_x = _mm256_add_pd(local_x,_mm256_mul_pd(local_vel_y[i],local_tau_xy[i]));
          local_x = _mm256_add_pd(local_x,_mm256_mul_pd(local_vel_z[i],local_tau_xz[i]));
          _mm256_stream_pd(precalc_x+(i<<2),local_x);
        }

        // Y direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d local_y = _mm256_xor_pd(local_vel_y[i],_mm256_set1_pd(-0.0));
          local_y = _mm256_mul_pd(local_y,_mm256_sub_pd(rho_pres[i],_mm256_load_pd(tau_yy+(i<<2))));
          local_y = _mm256_sub_pd(local_y,_mm256_load_pd(heatflux_y+(i<<2)));
          local_y = _mm256_add_pd(local_y,_mm256_mul_pd(local_vel_x[i],local_tau_xy[i]));
          local_y = _mm256_add_pd(local_y,_mm256_mul_pd(local_vel_z[i],local_tau_yz[i]));
          _mm256_stream_pd(precalc_y+(i<<2),local_y);
        }
        
        // Z direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d local_z = _mm256_xor_pd(local_vel_z[i],_mm256_set1_pd(-0.0));
          local_z = _mm256_mul_pd(local_z,_mm256_sub_pd(rho_pres[i],_mm256_load_pd(tau_zz+(i<<2))));
          local_z = _mm256_sub_pd(local_z,_mm256_load_pd(heatflux_z+(i<<2)));
          local_z = _mm256_add_pd(local_z,_mm256_mul_pd(local_vel_x[i],local_tau_xz[i]));
          local_z = _mm256_add_pd(local_z,_mm256_mul_pd(local_vel_y[i],local_tau_yz[i]));
          _mm256_stream_pd(precalc_z+(i<<2),local_z);
        }
      } else {
        // handle the unaligned case

        // X direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d local_x = _mm256_xor_pd(local_vel_x[i],_mm256_set1_pd(-0.0));
          local_x = _mm256_mul_pd(local_x,_mm256_sub_pd(rho_pres[i],_mm256_loadu_pd(tau_xx+(i<<2))));
          local_x = _mm256_sub_pd(local_x,_mm256_loadu_pd(heatflux_x+(i<<2)));
          local_x = _mm256_add_pd(local_x,_mm256_mul_pd(local_vel_y[i],local_tau_xy[i]));
          local_x = _mm256_add_pd(local_x,_mm256_mul_pd(local_vel_z[i],local_tau_xz[i]));
          _mm256_storeu_pd(precalc_x+(i<<2),local_x);
        }

        // Y direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d local_y = _mm256_xor_pd(local_vel_y[i],_mm256_set1_pd(-0.0));
          local_y = _mm256_mul_pd(local_y,_mm256_sub_pd(rho_pres[i],_mm256_loadu_pd(tau_yy+(i<<2))));
          local_y = _mm256_sub_pd(local_y,_mm256_loadu_pd(heatflux_y+(i<<2)));
          local_y = _mm256_add_pd(local_y,_mm256_mul_pd(local_vel_x[i],local_tau_xy[i]));
          local_y = _mm256_add_pd(local_y,_mm256_mul_pd(local_vel_z[i],local_tau_yz[i]));
          _mm256_storeu_pd(precalc_y+(i<<2),local_y);
        }
        
        // Z direction
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d local_z = _mm256_xor_pd(local_vel_z[i],_mm256_set1_pd(-0.0));
          local_z = _mm256_mul_pd(local_z,_mm256_sub_pd(rho_pres[i],_mm256_loadu_pd(tau_zz+(i<<2))));
          local_z = _mm256_sub_pd(local_z,_mm256_loadu_pd(heatflux_z+(i<<2)));
          local_z = _mm256_add_pd(local_z,_mm256_mul_pd(local_vel_x[i],local_tau_xz[i]));
          local_z = _mm256_add_pd(local_z,_mm256_mul_pd(local_vel_y[i],local_tau_yz[i]));
          _mm256_storeu_pd(precalc_z+(i<<2),local_z);
        }
      }
#elif defined(USE_SSE_KERNELS)
      // Load our values into the cache
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) 
        rho_pres[i] = _mm_add_pd(_mm_stream_load_pd(rho_e+(i<<1)),_mm_stream_load_pd(pressure+(i<<1)));
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++)
        local_vel_x[i] = _mm_stream_load_pd(vel_x+(i<<1));
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++)
        local_vel_y[i] = _mm_stream_load_pd(vel_y+(i<<1));
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++)
        local_vel_z[i] = _mm_stream_load_pd(vel_z+(i<<1));
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++)
        local_tau_xy[i] = _mm_stream_load_pd(tau_xy+(i<<1));
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++)
        local_tau_xz[i] = _mm_stream_load_pd(tau_xz+(i<<1));
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++)
        local_tau_yz[i] = _mm_stream_load_pd(tau_yz+(i<<1));

      // X direction
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d local_x = _mm_xor_pd(local_vel_x[i],_mm_set1_pd(-0.0));
        local_x = _mm_mul_pd(local_x,_mm_sub_pd(rho_pres[i],_mm_stream_load_pd(tau_xx+(i<<1))));
        local_x = _mm_sub_pd(local_x,_mm_stream_load_pd(heatflux_x+(i<<1)));
        local_x = _mm_add_pd(local_x,_mm_mul_pd(local_vel_y[i],local_tau_xy[i]));
        local_x = _mm_add_pd(local_x,_mm_mul_pd(local_vel_z[i],local_tau_xz[i]));
        _mm_stream_pd(precalc_x+(i<<1),local_x);
      }

      // Y direction
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d local_y = _mm_xor_pd(local_vel_y[i],_mm_set1_pd(-0.0));
        local_y = _mm_mul_pd(local_y,_mm_sub_pd(rho_pres[i],_mm_stream_load_pd(tau_yy+(i<<1))));
        local_y = _mm_sub_pd(local_y,_mm_stream_load_pd(heatflux_y+(i<<1)));
        local_y = _mm_add_pd(local_y,_mm_mul_pd(local_vel_x[i],local_tau_xy[i]));
        local_y = _mm_add_pd(local_y,_mm_mul_pd(local_vel_z[i],local_tau_yz[i]));
        _mm_stream_pd(precalc_y+(i<<1),local_y);
      }
      
      // Z direction
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d local_z = _mm_xor_pd(local_vel_z[i],_mm_set1_pd(-0.0));
        local_z = _mm_mul_pd(local_z,_mm_sub_pd(rho_pres[i],_mm_stream_load_pd(tau_zz+(i<<1))));
        local_z = _mm_sub_pd(local_z,_mm_stream_load_pd(heatflux_z+(i<<1)));
        local_z = _mm_add_pd(local_z,_mm_mul_pd(local_vel_x[i],local_tau_xz[i]));
        local_z = _mm_add_pd(local_z,_mm_mul_pd(local_vel_y[i],local_tau_yz[i]));
        _mm_stream_pd(precalc_z+(i<<1),local_z);
      }
#else
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) rho_e_plus_pressure[i] = rho_e[i] + pressure[i];

      // accumulate value in several passes in hopes this makes CPU prefetcher happier
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) 
	precalc_x[i] = -vel_x[i] * (rho_e_plus_pressure[i] - tau_xx[i]) - heatflux_x[i];
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) 
	precalc_x[i] += vel_y[i] * tau_xy[i];
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) 
	precalc_x[i] += vel_z[i] * tau_xz[i];

      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) 
	precalc_y[i] = -vel_y[i] * (rho_e_plus_pressure[i] - tau_yy[i]) - heatflux_y[i];
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) 
	precalc_y[i] += vel_x[i] * tau_xy[i];
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) 
	precalc_y[i] += vel_z[i] * tau_yz[i];

      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) 
	precalc_z[i] = -vel_z[i] * (rho_e_plus_pressure[i] - tau_zz[i]) - heatflux_z[i];
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) 
	precalc_z[i] += vel_x[i] * tau_xz[i];
      for(size_t i = 0; i < FULL_STRIP_SIZE; i++) 
	precalc_z[i] += vel_y[i] * tau_yz[i];
#endif
      n_pts -= FULL_STRIP_SIZE;
    } else {
      for(size_t i = 0; i < n_pts; i++) rho_e_plus_pressure[i] = rho_e[i] + pressure[i];

      // accumulate value in several passes in hopes this makes CPU prefetcher happier
      for(size_t i = 0; i < n_pts; i++) 
	precalc_x[i] = -vel_x[i] * (rho_e_plus_pressure[i] - tau_xx[i]) - heatflux_x[i];
      for(size_t i = 0; i < n_pts; i++) 
	precalc_x[i] += vel_y[i] * tau_xy[i];
      for(size_t i = 0; i < n_pts; i++) 
	precalc_x[i] += vel_z[i] * tau_xz[i];

      for(size_t i = 0; i < n_pts; i++) 
	precalc_y[i] = -vel_y[i] * (rho_e_plus_pressure[i] - tau_yy[i]) - heatflux_y[i];
      for(size_t i = 0; i < n_pts; i++) 
	precalc_y[i] += vel_x[i] * tau_xy[i];
      for(size_t i = 0; i < n_pts; i++) 
	precalc_y[i] += vel_z[i] * tau_yz[i];

      for(size_t i = 0; i < n_pts; i++) 
	precalc_z[i] = -vel_z[i] * (rho_e_plus_pressure[i] - tau_zz[i]) - heatflux_z[i];
      for(size_t i = 0; i < n_pts; i++) 
	precalc_z[i] += vel_x[i] * tau_xz[i];
      for(size_t i = 0; i < n_pts; i++) 
	precalc_z[i] += vel_y[i] * tau_yz[i];
      n_pts = 0;
    }

    rho_e += FULL_STRIP_SIZE;
    vel_x += FULL_STRIP_SIZE;
    vel_y += FULL_STRIP_SIZE;
    vel_z += FULL_STRIP_SIZE;
    pressure += FULL_STRIP_SIZE;
    heatflux_x += FULL_STRIP_SIZE;
    heatflux_y += FULL_STRIP_SIZE;
    heatflux_z += FULL_STRIP_SIZE;
    tau_xx += FULL_STRIP_SIZE;
    tau_xy += FULL_STRIP_SIZE;
    tau_xz += FULL_STRIP_SIZE;
    tau_yy += FULL_STRIP_SIZE;
    tau_yz += FULL_STRIP_SIZE;
    tau_zz += FULL_STRIP_SIZE;
    precalc_x += FULL_STRIP_SIZE;
    precalc_y += FULL_STRIP_SIZE;
    precalc_z += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
}

typedef void (*dense_kernelptr_17d)(size_t n_pts, 
				    double *f1, double *f2, double *f3, double *f4, double *f5,
				    double *f6, double *f7, double *f8, double *f9, double *f10,
				    double *f11, double *f12, double *f13, double *f14, double *f15,
				    double *f16, double *f17);

template <unsigned DIM, dense_kernelptr_17d KPTR>
bool attempt_dense_17d(const Rect<DIM>& subgrid_bounds,
		       PhysicalRegion r1, FieldID fid1,
		       PhysicalRegion r2, FieldID fid2,
		       PhysicalRegion r3, FieldID fid3,
		       PhysicalRegion r4, FieldID fid4,
		       PhysicalRegion r5, FieldID fid5,
		       PhysicalRegion r6, FieldID fid6,
		       PhysicalRegion r7, FieldID fid7,
		       PhysicalRegion r8, FieldID fid8,
		       PhysicalRegion r9, FieldID fid9,
		       PhysicalRegion r10, FieldID fid10,
		       PhysicalRegion r11, FieldID fid11,
		       PhysicalRegion r12, FieldID fid12,
		       PhysicalRegion r13, FieldID fid13,
		       PhysicalRegion r14, FieldID fid14,
		       PhysicalRegion r15, FieldID fid15,
		       PhysicalRegion r16, FieldID fid16,
		       PhysicalRegion r17, FieldID fid17)
{
  Rect<DIM> subrect;
  ByteOffset offsets[DIM], offsets2[DIM];

  double *p1 = r1.get_field_accessor(fid1).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets);
  if(!p1 || (subrect != subgrid_bounds) || !offsets_are_dense<DIM,double>(subgrid_bounds, offsets))
    return false;

#define CHECK_FIELD(n) \
  double *p ## n = r ## n.get_field_accessor(fid ## n).typeify<double>().raw_rect_ptr<DIM>(subgrid_bounds, subrect, offsets2); \
  if(!p ## n || (subrect != subgrid_bounds) || offset_mismatch(DIM, offsets, offsets2)) \
    return false

  CHECK_FIELD(2);
  CHECK_FIELD(3);
  CHECK_FIELD(4);
  CHECK_FIELD(5);
  CHECK_FIELD(6);
  CHECK_FIELD(7);
  CHECK_FIELD(8);
  CHECK_FIELD(9);
  CHECK_FIELD(10);
  CHECK_FIELD(11);
  CHECK_FIELD(12);
  CHECK_FIELD(13);
  CHECK_FIELD(14);
  CHECK_FIELD(15);
  CHECK_FIELD(16);
  CHECK_FIELD(17);

  KPTR(subgrid_bounds.volume(), p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17);
  return true;
#undef CHECK_FIELD
}

/*static*/
void CalcEnergyTask::cpu_base_impl(S3DRank *rank, const Task *task,
                                   const Rect<3> &my_subgrid_bounds,
                                   const std::vector<RegionRequirement> &reqs,
                                   const std::vector<PhysicalRegion> &regions,
                                   Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  if (S3DRank::get_show_progress())
    printf("%s task, pt = (%d, %d, %d)\n", "energy_precalc", 
        task->index_point.get_point<3>()[0], 
        task->index_point.get_point<3>()[1], 
        task->index_point.get_point<3>()[2]);

  if(attempt_dense_17d<3, dense_energy_precalc_task>(my_subgrid_bounds,
						     regions[1], FID_RHO_E,
						     regions[2], FID_VEL_X,
						     regions[2], FID_VEL_Y,
						     regions[2], FID_VEL_Z,
						     regions[2], FID_PRESSURE,
						     regions[2], FID_HEATFLUX_X,
						     regions[2], FID_HEATFLUX_Y,
						     regions[2], FID_HEATFLUX_Z,
						     regions[2], FID_TAU_XX,
						     regions[2], FID_TAU_XY,
						     regions[2], FID_TAU_XZ,
						     regions[2], FID_TAU_YY,
						     regions[2], FID_TAU_YZ,
						     regions[2], FID_TAU_ZZ,
						     regions[0], FID_RHS_5_DX_IN,
						     regions[0], FID_RHS_5_DY_IN,
						     regions[0], FID_RHS_5_DZ_IN)) {
    //printf("dense energy precalc!\n");
    return;
  }

  // get whatever accessors you need here
  RegionAccessor<AccessorType::Generic,double> fa_precalc_x = regions[0].get_field_accessor(FID_RHS_5_DX_IN).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_precalc_y = regions[0].get_field_accessor(FID_RHS_5_DY_IN).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_precalc_z = regions[0].get_field_accessor(FID_RHS_5_DZ_IN).typeify<double>();

  RegionAccessor<AccessorType::Generic,double> fa_rho_e = regions[1].get_field_accessor(FID_RHO_E).typeify<double>();
  
  RegionAccessor<AccessorType::Generic,double> fa_vel_x = regions[2].get_field_accessor(FID_VEL_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_vel_y = regions[2].get_field_accessor(FID_VEL_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_vel_z = regions[2].get_field_accessor(FID_VEL_Z).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_pressure = regions[2].get_field_accessor(FID_PRESSURE).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_x = regions[2].get_field_accessor(FID_HEATFLUX_X).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_y = regions[2].get_field_accessor(FID_HEATFLUX_Y).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_heatflux_z = regions[2].get_field_accessor(FID_HEATFLUX_Z).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_xx = regions[2].get_field_accessor(FID_TAU_XX).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_xy = regions[2].get_field_accessor(FID_TAU_XY).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_xz = regions[2].get_field_accessor(FID_TAU_XZ).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_yy = regions[2].get_field_accessor(FID_TAU_YY).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_yz = regions[2].get_field_accessor(FID_TAU_YZ).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_tau_zz = regions[2].get_field_accessor(FID_TAU_ZZ).typeify<double>();

  for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
    // accessors use DomainPoint's for now
    DomainPoint dp = DomainPoint::from_point<3>(pir.p);

    double rho_e = fa_rho_e.read(dp);
    double vel_x = fa_vel_x.read(dp);
    double vel_y = fa_vel_y.read(dp);
    double vel_z = fa_vel_z.read(dp);
    double pressure = fa_pressure.read(dp);
    double heatflux_x = fa_heatflux_x.read(dp);
    double heatflux_y = fa_heatflux_y.read(dp);
    double heatflux_z = fa_heatflux_z.read(dp);
    double tau_xx = fa_tau_xx.read(dp);
    double tau_xy = fa_tau_xy.read(dp);
    double tau_xz = fa_tau_xz.read(dp);
    double tau_yy = fa_tau_yy.read(dp);
    double tau_yz = fa_tau_yz.read(dp);
    double tau_zz = fa_tau_zz.read(dp);

    double precalc_x = (-vel_x * (rho_e + pressure - tau_xx) +
			vel_y * tau_xy +
			vel_z * tau_xz -
			heatflux_x);
    double precalc_y = (vel_x * tau_xy -
			vel_y * (rho_e + pressure - tau_yy) +
			vel_z * tau_yz -
			heatflux_y);
    double precalc_z = (vel_x * tau_xz +
			vel_y * tau_yz -
			vel_z * (rho_e + pressure - tau_zz) -
			heatflux_z);

    fa_precalc_x.write(dp, precalc_x);
    fa_precalc_y.write(dp, precalc_y);
    fa_precalc_z.write(dp, precalc_z);
  }
#endif
}

CalcSpecTask::CalcSpecTask(S3DRank *r, int spec, int dir,
                           Domain domain,
                           TaskArgument global_arg,
                           ArgumentMap arg_map,
                           Predicate pred,
                           bool must,
                           MapperID id,
                           MappingTagID tag,
                           bool add_requirements)
 : IndexLauncher(CalcSpecTask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | CalcSpecTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    rr_out.add_field(FID_RHS_6_D_IN(spec, dir));
    add_region_requirement(rr_out);

    RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
    rr_q.add_field(FID_RHO_Y(spec));
    add_region_requirement(rr_q);

    RegionRequirement rr_int(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    rr_int.add_field(FID_VEL_X + dir);
    rr_int.add_field(FID_YDIFFFLUX(spec, dir));
    add_region_requirement(rr_int);
  }
}

CalcSpecTask::CalcSpecTask(S3DRank *r, int start_spec, int max_spec, int dir,
                           Domain domain,
                           TaskArgument global_arg,
                           ArgumentMap arg_map,
                           Predicate pred,
                           bool must,
                           MapperID id,
                           MappingTagID tag,
                           bool add_requirements)
 : IndexLauncher(CalcSpecTask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | CalcSpecTask::MAPPER_TAG), rank(r)
{
  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    for (int spec = start_spec; spec < max_spec; spec++)
      rr_out.add_field(FID_RHS_6_D_IN(spec, dir));
    add_region_requirement(rr_out);

    RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
    for (int spec = start_spec; spec < max_spec; spec++)
      rr_q.add_field(FID_RHO_Y(spec));
    add_region_requirement(rr_q);

    RegionRequirement rr_int(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    rr_int.add_field(FID_VEL_X + dir);
    for (int spec = start_spec; spec < max_spec; spec++)
      rr_int.add_field(FID_YDIFFFLUX(spec, dir));
    add_region_requirement(rr_int);
  }
}


/*static*/ const char * const CalcSpecTask::TASK_NAME = "spec_precalc_task";

void CalcSpecTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing
}

/*static*/
bool CalcSpecTask::dense_spec_precalc_task(const Rect<3> &subgrid_bounds,
                                           RegionAccessor<AccessorType::Generic,double> fa_in_rho_y,
                                           RegionAccessor<AccessorType::Generic,double> fa_in_vel,
                                           RegionAccessor<AccessorType::Generic,double> fa_in_flux,
                                           RegionAccessor<AccessorType::Generic,double> fa_out_precalc)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], offsets[3];

  const double *in_rho_y_ptr = fa_in_rho_y.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_rho_y_ptr || (subrect != subgrid_bounds) || 
     !offsets_are_dense<3,double>(subgrid_bounds, in_offsets)) return false;

  const double *in_vel_ptr = fa_in_vel.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_vel_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_flux_ptr = fa_in_flux.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_flux_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_precalc_ptr = fa_out_precalc.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_precalc_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  // Pick a large enough strip size to get the prefetchers going
  // but not so large that we can't handle common cases with the fast path
#define FULL_STRIP_SIZE 1024

  size_t n_pts = subgrid_bounds.volume();
  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined(USE_AVX_KERNELS)
      if (aligned<32>(in_rho_y_ptr) && aligned<32>(in_vel_ptr) &&
          aligned<32>(in_flux_ptr) && aligned<32>(out_precalc_ptr)) {
        // Aligned case
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d rho_y = _mm256_load_pd(in_rho_y_ptr+(i<<2));
          __m256d vel = _mm256_load_pd(in_vel_ptr+(i<<2));
          __m256d flux = _mm256_load_pd(in_flux_ptr+(i<<2));
          // Fast negation of rho
          rho_y = _mm256_xor_pd(rho_y,_mm256_set1_pd(-0.0));
          __m256d precalc = _mm256_sub_pd(_mm256_mul_pd(rho_y,vel),flux);
          _mm256_stream_pd(out_precalc_ptr+(i<<2),precalc);
        }
      } else {
        // Unaligned case
        // We'll use the indirection of our static functions to handle it
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d rho_y = _mm256_loadu_pd(in_rho_y_ptr+(i<<2));
          __m256d vel = _mm256_loadu_pd(in_vel_ptr+(i<<2));
          __m256d flux = _mm256_loadu_pd(in_flux_ptr+(i<<2));
          // Fast negation of rho
          rho_y = _mm256_xor_pd(rho_y,_mm256_set1_pd(-0.0));
          __m256d precalc = _mm256_sub_pd(_mm256_mul_pd(rho_y,vel),flux);
          _mm256_storeu_pd(out_precalc_ptr+(i<<2),precalc);
        }
      }
#elif defined(USE_SSE_KERNELS)
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d rho_y = _mm_load_pd(in_rho_y_ptr+(i<<1));
        __m128d vel = _mm_load_pd(in_vel_ptr+(i<<1));
        __m128d flux = _mm_load_pd(in_flux_ptr+(i<<1));
        // Fast negation of rho
        rho_y = _mm_xor_pd(rho_y,_mm_set1_pd(-0.0));
        __m128d precalc = _mm_sub_pd(_mm_mul_pd(rho_y,vel),flux);
        _mm_stream_pd(out_precalc_ptr+(i<<1),precalc);
      }
#else
      for (int i = 0; i < FULL_STRIP_SIZE; i++) {
        double rho_y = in_rho_y_ptr[i];
        double vel = in_vel_ptr[i];
        double flux = in_flux_ptr[i];
        double precalc = -rho_y * vel - flux;
        out_precalc_ptr[i] = precalc;
      }
#endif
      n_pts -= FULL_STRIP_SIZE;
    } else {
      for (size_t i = 0; i < n_pts; i++) {
        double rho_y = in_rho_y_ptr[i];
        double vel = in_vel_ptr[i];
        double flux = in_flux_ptr[i];
        double precalc = -rho_y * vel - flux;
        out_precalc_ptr[i] = precalc;
      }
      n_pts = 0;
    }
    in_rho_y_ptr += FULL_STRIP_SIZE;
    in_vel_ptr += FULL_STRIP_SIZE;
    in_flux_ptr += FULL_STRIP_SIZE;
    out_precalc_ptr += FULL_STRIP_SIZE;
  }
#undef FULL_STRIP_SIZE
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
}

/*static*/
bool CalcSpecTask::fast_spec_precalc_task(RegionAccessor<AccessorType::Generic,double> fa_in_rho_y,
                                          RegionAccessor<AccessorType::Generic,double> fa_in_vel,
                                          RegionAccessor<AccessorType::Generic,double> fa_in_flux,
                                          RegionAccessor<AccessorType::Generic,double> fa_out_precalc,
                                          Rect<3> subgrid_bounds)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], out_offsets[3], offsets[3];

  const double *in_rho_y_ptr = fa_in_rho_y.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_rho_y_ptr || (subrect != subgrid_bounds)) return false;

  const double *in_vel_ptr = fa_in_vel.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_vel_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_flux_ptr = fa_in_flux.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_flux_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_precalc_ptr = fa_out_precalc.raw_rect_ptr<3>(subgrid_bounds, subrect, out_offsets);
  if(!out_precalc_ptr || (subrect != subgrid_bounds)) return false;

  // now that we have all those pointers, we can do the actual work
  ByteOffset in_pos_z, out_pos_z;
  for(int z = subgrid_bounds.lo[2]; z <= subgrid_bounds.hi[2]; z++) {
    ByteOffset in_pos_y = in_pos_z;
    ByteOffset out_pos_y = out_pos_z;
    for(int y = subgrid_bounds.lo[1]; y <= subgrid_bounds.hi[1]; y++) {
      ByteOffset in_pos_x = in_pos_y;
      ByteOffset out_pos_x = out_pos_y;
      for(int x = subgrid_bounds.lo[0]; x <= subgrid_bounds.hi[0]; x++) {
	double rho_y = *(in_rho_y_ptr + in_pos_x);
	double vel = *(in_vel_ptr + in_pos_x);
	double flux = *(in_flux_ptr + in_pos_x);

	double precalc = -rho_y * vel - flux;

	*(out_precalc_ptr + out_pos_x) = precalc;

	in_pos_x += in_offsets[0];
	out_pos_x += out_offsets[0];
      }
      in_pos_y += in_offsets[1];
      out_pos_y += out_offsets[1];
    }
    in_pos_z += in_offsets[2];
    out_pos_z += out_offsets[2];
  }

  return true;
}

/*static*/
void CalcSpecTask::cpu_base_impl(S3DRank *rank, const Task *task, 
                                 const Rect<3> &my_subgrid_bounds,
                                 const std::vector<RegionRequirement> &reqs,
                                 const std::vector<PhysicalRegion> &regions,
                                 Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  if (S3DRank::get_show_progress())
    printf("%s task, pt = (%d, %d, %d)\n", "spec_precalc", 
        task->index_point.get_point<3>()[0], 
        task->index_point.get_point<3>()[1], 
        task->index_point.get_point<3>()[2]);

  // get whatever accessors you need here
  

  RegionAccessor<AccessorType::Generic,double> fa_vel = 
    regions[2].get_field_accessor(task->regions[2].instance_fields[0]).typeify<double>();

  const int num_species = task->regions[0].instance_fields.size(); 
  for (int spec = 0; spec < num_species; spec++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_out = 
      regions[0].get_field_accessor(task->regions[0].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_rho_y = 
      regions[1].get_field_accessor(task->regions[1].instance_fields[spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_flux = 
      regions[2].get_field_accessor(task->regions[2].instance_fields[spec+1]).typeify<double>();

    if (dense_spec_precalc_task(my_subgrid_bounds, fa_rho_y, fa_vel, fa_flux, fa_out))
      continue;

    if(fast_spec_precalc_task(fa_rho_y, fa_vel, fa_flux, fa_out, my_subgrid_bounds)) {
      //printf("fast spec precalc\n");
      continue;
    }
    
    for(GenericPointInRectIterator<3> pir(my_subgrid_bounds); pir; pir++) {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double rho_y = fa_rho_y.read(dp);
      double vel = fa_vel.read(dp);
      double flux = fa_flux.read(dp);

      double precalc = -rho_y * vel - flux;

      fa_out.write(dp, precalc);
    }
  }
#endif
}

CalcSpecPerFieldTask::CalcSpecPerFieldTask(S3DRank *r, int spec,
                       Domain domain,
                       TaskArgument global_arg,
                       ArgumentMap arg_map,
                       Predicate pred,
                       bool must,
                       MapperID id,
                       MappingTagID tag,
                       bool add_requirements)
 : IndexLauncher(CalcSpecPerFieldTask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | CalcSpecPerFieldTask::MAPPER_TAG), rank(r) 
{
  if (add_requirements)
  {
    if(spec >= 0) {
      // actual chemical species
      RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
      for (int d = 0; d < 3; d++)
	rr_out.add_field(FID_RHS_6_D_IN(spec, d));
      add_region_requirement(rr_out);

      RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
      rr_q.add_field(FID_RHO_Y(spec));
      add_region_requirement(rr_q);

      RegionRequirement rr_int(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
      for (int d = 0; d < 3; d++)
	rr_int.add_field(FID_VEL_X + d);
      for (int d = 0; d < 3; d++)
	rr_int.add_field(FID_YDIFFFLUX(spec, d));
      add_region_requirement(rr_int);
    } else {
      // scalar field
      int scal_idx = -1 - spec;

      add_region_requirement(RegionRequirement(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int)
			     .add_field(FID_SCALAR_RHS_DX_IN(scal_idx))
			     .add_field(FID_SCALAR_RHS_DY_IN(scal_idx))
			     .add_field(FID_SCALAR_RHS_DZ_IN(scal_idx)));

      add_region_requirement(RegionRequirement(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q)
			     .add_field(FID_SCALAR(scal_idx)));

      add_region_requirement(RegionRequirement(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int)
			     .add_field(FID_VEL_X)
			     .add_field(FID_VEL_Y)
			     .add_field(FID_VEL_Z)
			     .add_field(FID_SCALAR_FLUX_X(scal_idx))
			     .add_field(FID_SCALAR_FLUX_Y(scal_idx))
			     .add_field(FID_SCALAR_FLUX_Z(scal_idx)));
    }
  }
}

CalcSpecPerFieldTask::CalcSpecPerFieldTask(S3DRank *r, int start_spec, int max_spec,
                                           Domain domain,
                                           TaskArgument global_arg,
                                           ArgumentMap arg_map,
                                           Predicate pred,
                                           bool must,
                                           MapperID id,
                                           MappingTagID tag,
                                           bool add_requirements)
 : IndexLauncher(CalcSpecPerFieldTask::TASK_ID, domain, global_arg, arg_map, pred,
                 must, id, tag | CalcSpecPerFieldTask::MAPPER_TAG), rank(r) 
{
  // fused version doesn't support scalar fields
  assert(start_spec >= 0);

  if (add_requirements)
  {
    RegionRequirement rr_out(rank->lp_int_top, 0, WRITE_DISCARD, EXCLUSIVE, rank->lr_int);
    for (int spec = start_spec; spec < max_spec; spec++)
    {
      for (int d = 0; d < 3; d++)
        rr_out.add_field(FID_RHS_6_D_IN(spec, d));
    }
    add_region_requirement(rr_out);

    RegionRequirement rr_q(rank->lp_q_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_q);
    for (int spec = start_spec; spec < max_spec; spec++)
      rr_q.add_field(FID_RHO_Y(spec));
    add_region_requirement(rr_q);

    RegionRequirement rr_int(rank->lp_int_top, 0, READ_ONLY, EXCLUSIVE, rank->lr_int);
    for (int d = 0; d < 3; d++)
      rr_int.add_field(FID_VEL_X + d);
    for (int spec = start_spec; spec < max_spec; spec++)
    {
      for (int d = 0; d < 3; d++)
        rr_int.add_field(FID_YDIFFFLUX(spec, d));
    }
    add_region_requirement(rr_int);
  }
}

/*static*/
const char * const CalcSpecPerFieldTask::TASK_NAME = "spec_precalc_per_field_task";

void CalcSpecPerFieldTask::launch_check_fields(Context ctx, HighLevelRuntime *runtime)
{
  // Do nothing
}

/*static*/
bool CalcSpecPerFieldTask::dense_spec_precalc_task(const Rect<3> &subgrid_bounds,
                          RegionAccessor<AccessorType::Generic,double> fa_in_rho,
                          RegionAccessor<AccessorType::Generic,double> fa_in_vel_x,
                          RegionAccessor<AccessorType::Generic,double> fa_in_vel_y,
                          RegionAccessor<AccessorType::Generic,double> fa_in_vel_z,
                          RegionAccessor<AccessorType::Generic,double> fa_in_flux_x,
                          RegionAccessor<AccessorType::Generic,double> fa_in_flux_y,
                          RegionAccessor<AccessorType::Generic,double> fa_in_flux_z,
                          RegionAccessor<AccessorType::Generic,double> fa_out_pre_x,
                          RegionAccessor<AccessorType::Generic,double> fa_out_pre_y,
                          RegionAccessor<AccessorType::Generic,double> fa_out_pre_z)
{
  Rect<3> subrect;
  ByteOffset in_offsets[3], offsets[3];

  const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
  if(!in_rho_ptr || (subrect != subgrid_bounds) || 
     !offsets_are_dense<3,double>(subgrid_bounds, in_offsets)) return false;

  const double *in_vel_x_ptr = fa_in_vel_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_vel_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_vel_y_ptr = fa_in_vel_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_vel_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_vel_z_ptr = fa_in_vel_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_vel_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_flux_x_ptr = fa_in_flux_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_flux_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_flux_y_ptr = fa_in_flux_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_flux_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  const double *in_flux_z_ptr = fa_in_flux_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!in_flux_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_precalc_x_ptr = fa_out_pre_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_precalc_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_precalc_y_ptr = fa_out_pre_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_precalc_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

  double *out_precalc_z_ptr = fa_out_pre_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
  if(!out_precalc_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) return false;

#define FULL_STRIP_SIZE 1024

#if defined(USE_AVX_KERNELS)
  __m256d avx_rho[FULL_STRIP_SIZE/4];
#elif defined(USE_SSE_KERNELS)
  __m128d sse_rho[FULL_STRIP_SIZE/2];
#endif
  double rho[FULL_STRIP_SIZE];

  size_t n_pts = subgrid_bounds.volume();
  while (n_pts > 0) {
    if (n_pts >= FULL_STRIP_SIZE) {
#if defined(USE_AVX_KERNELS)
      if (aligned<32>(in_rho_ptr) && aligned<32>(in_vel_x_ptr) && aligned<32>(in_vel_y_ptr) &&
          aligned<32>(in_vel_z_ptr) && aligned<32>(in_flux_x_ptr) && aligned<32>(in_flux_y_ptr) &&
          aligned<32>(in_flux_z_ptr) && aligned<32>(out_precalc_x_ptr) && 
          aligned<32>(out_precalc_y_ptr) && aligned<32>(out_precalc_z_ptr)) {
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          avx_rho[i] = _mm256_load_pd(in_rho_ptr+(i<<2));
          // Fast negation
          avx_rho[i] = _mm256_xor_pd(avx_rho[i],_mm256_set1_pd(-0.0));
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d vel = _mm256_load_pd(in_vel_x_ptr+(i<<2));
          __m256d flux = _mm256_load_pd(in_vel_x_ptr+(i<<2));
          __m256d spec = _mm256_sub_pd(_mm256_mul_pd(avx_rho[i],vel),flux);
          _mm256_stream_pd(out_precalc_x_ptr+(i<<2),spec);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d vel = _mm256_load_pd(in_vel_y_ptr+(i<<2));
          __m256d flux = _mm256_load_pd(in_vel_y_ptr+(i<<2));
          __m256d spec = _mm256_sub_pd(_mm256_mul_pd(avx_rho[i],vel),flux);
          _mm256_stream_pd(out_precalc_y_ptr+(i<<2),spec);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d vel = _mm256_load_pd(in_vel_z_ptr+(i<<2));
          __m256d flux = _mm256_load_pd(in_vel_z_ptr+(i<<2));
          __m256d spec = _mm256_sub_pd(_mm256_mul_pd(avx_rho[i],vel),flux);
          _mm256_stream_pd(out_precalc_z_ptr+(i<<2),spec);
        }
      } else {
        // Unaligned AVX code
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          avx_rho[i] = _mm256_loadu_pd(in_rho_ptr+(i<<2));
          // Fast negation
          avx_rho[i] = _mm256_xor_pd(avx_rho[i],_mm256_set1_pd(-0.0));
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d vel = _mm256_loadu_pd(in_vel_x_ptr+(i<<2));
          __m256d flux = _mm256_loadu_pd(in_vel_x_ptr+(i<<2));
          __m256d spec = _mm256_sub_pd(_mm256_mul_pd(avx_rho[i],vel),flux);
          _mm256_storeu_pd(out_precalc_x_ptr+(i<<2),spec);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d vel = _mm256_loadu_pd(in_vel_y_ptr+(i<<2));
          __m256d flux = _mm256_loadu_pd(in_vel_y_ptr+(i<<2));
          __m256d spec = _mm256_sub_pd(_mm256_mul_pd(avx_rho[i],vel),flux);
          _mm256_storeu_pd(out_precalc_y_ptr+(i<<2),spec);
        }
        for (int i = 0; i < (FULL_STRIP_SIZE/4); i++) {
          __m256d vel = _mm256_loadu_pd(in_vel_z_ptr+(i<<2));
          __m256d flux = _mm256_loadu_pd(in_vel_z_ptr+(i<<2));
          __m256d spec = _mm256_sub_pd(_mm256_mul_pd(avx_rho[i],vel),flux);
          _mm256_storeu_pd(out_precalc_z_ptr+(i<<2),spec);
        }
      } 
#elif defined(USE_SSE_KERNELS)
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        sse_rho[i] = _mm_load_pd(in_rho_ptr+(i<<1));
        // Fast negation
        sse_rho[i] = _mm_xor_pd(sse_rho[i],_mm_set1_pd(-0.0));
      }
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d vel = _mm_load_pd(in_vel_x_ptr+(i<<1));
        __m128d flux = _mm_load_pd(in_flux_x_ptr+(i<<1));
        __m128d spec = _mm_sub_pd(_mm_mul_pd(sse_rho[i],vel),flux);
        _mm_stream_pd(out_precalc_x_ptr+(i<<1),spec);
      }
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d vel = _mm_load_pd(in_vel_y_ptr+(i<<1));
        __m128d flux = _mm_load_pd(in_flux_y_ptr+(i<<1));
        __m128d spec = _mm_sub_pd(_mm_mul_pd(sse_rho[i],vel),flux);
        _mm_stream_pd(out_precalc_y_ptr+(i<<1),spec);
      }
      for (int i = 0; i < (FULL_STRIP_SIZE/2); i++) {
        __m128d vel = _mm_load_pd(in_vel_z_ptr+(i<<1));
        __m128d flux = _mm_load_pd(in_flux_z_ptr+(i<<1));
        __m128d spec = _mm_sub_pd(_mm_mul_pd(sse_rho[i],vel),flux);
        _mm_stream_pd(out_precalc_z_ptr+(i<<1),spec);
      }
#else
      for (int i = 0; i < FULL_STRIP_SIZE; i++)
        rho[i] = -in_rho_ptr[i];
      for (int i = 0; i < FULL_STRIP_SIZE; i++)
        out_precalc_x_ptr[i] = rho[i] * in_vel_x_ptr[i] - in_flux_x_ptr[i];
      for (int i = 0; i < FULL_STRIP_SIZE; i++)
        out_precalc_y_ptr[i] = rho[i] * in_vel_y_ptr[i] - in_flux_y_ptr[i];
      for (int i = 0; i < FULL_STRIP_SIZE; i++)
        out_precalc_z_ptr[i] = rho[i] * in_vel_z_ptr[i] - in_flux_z_ptr[i];
#endif
      n_pts -= FULL_STRIP_SIZE;
    } else {
      for (size_t i = 0; i < n_pts; i++)
        rho[i] = -in_rho_ptr[i];
      for (size_t i = 0; i < n_pts; i++)
        out_precalc_x_ptr[i] = rho[i] * in_vel_x_ptr[i] - in_flux_x_ptr[i];
      for (size_t i = 0; i < n_pts; i++)
        out_precalc_y_ptr[i] = rho[i] * in_vel_y_ptr[i] - in_flux_y_ptr[i];
      for (size_t i = 0; i < n_pts; i++)
        out_precalc_z_ptr[i] = rho[i] * in_vel_z_ptr[i] - in_flux_z_ptr[i];
      n_pts = 0;
    }
    in_rho_ptr += FULL_STRIP_SIZE;
    in_vel_x_ptr += FULL_STRIP_SIZE;
    in_vel_y_ptr += FULL_STRIP_SIZE;
    in_vel_z_ptr += FULL_STRIP_SIZE;
    in_flux_x_ptr += FULL_STRIP_SIZE;
    in_flux_y_ptr += FULL_STRIP_SIZE;
    in_flux_z_ptr += FULL_STRIP_SIZE;
    out_precalc_x_ptr += FULL_STRIP_SIZE;
    out_precalc_y_ptr += FULL_STRIP_SIZE;
    out_precalc_z_ptr += FULL_STRIP_SIZE;
  }
#ifdef USE_AVX_KERNELS
  _mm256_zeroall();
#endif
  return true;
#undef FULL_STRIP_SIZE
}

/*static*/
void CalcSpecPerFieldTask::cpu_base_impl(S3DRank *rank, const Task *task,
                                    const Rect<3> &subgrid_bounds,
                                    const std::vector<RegionRequirement> &reqs,
                                    const std::vector<PhysicalRegion> &regions,
                                    Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
  RegionAccessor<AccessorType::Generic,double> fa_vel_x = 
    regions[2].get_field_accessor(task->regions[2].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_vel_y = 
    regions[2].get_field_accessor(task->regions[2].instance_fields[1]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_vel_z = 
    regions[2].get_field_accessor(task->regions[2].instance_fields[2]).typeify<double>();

  const int num_species = (task->regions[0].instance_fields.size()/3);

  for (int spec = 0; spec < num_species; spec++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_out_x = 
      regions[0].get_field_accessor(task->regions[0].instance_fields[3*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_out_y = 
      regions[0].get_field_accessor(task->regions[0].instance_fields[3*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_out_z = 
      regions[0].get_field_accessor(task->regions[0].instance_fields[3*spec+2]).typeify<double>();

    RegionAccessor<AccessorType::Generic,double> fa_rho_y = 
      regions[1].get_field_accessor(task->regions[1].instance_fields[spec]).typeify<double>();

    RegionAccessor<AccessorType::Generic,double> fa_flux_x = 
      regions[2].get_field_accessor(task->regions[2].instance_fields[3*spec+3]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_flux_y = 
      regions[2].get_field_accessor(task->regions[2].instance_fields[3*spec+4]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_flux_z = 
      regions[2].get_field_accessor(task->regions[2].instance_fields[3*spec+5]).typeify<double>();
   
    if (dense_spec_precalc_task(subgrid_bounds, fa_rho_y, 
                                fa_vel_x, fa_vel_y, fa_vel_z,
                                fa_flux_x, fa_flux_y, fa_flux_z,
                                fa_out_x, fa_out_y, fa_out_z))
      continue;

    for(GenericPointInRectIterator<3> pir(subgrid_bounds); pir; pir++) {
      // accessors use DomainPoint's for now
      DomainPoint dp = DomainPoint::from_point<3>(pir.p);

      double rho_y = fa_rho_y.read(dp);
      
      // X direction
      {
        double vel_x = fa_vel_x.read(dp);
        double flux_x = fa_flux_x.read(dp);
        double spec_x = -rho_y * vel_x - flux_x; 
        fa_out_x.write(dp, spec_x);
      }

      // Y direction
      {
        double vel_y = fa_vel_y.read(dp);
        double flux_y = fa_vel_y.read(dp);
        double spec_y = -rho_y * vel_y - flux_y;
        fa_out_y.write(dp, spec_y);
      }

      // Z direction
      {
        double vel_z = fa_vel_z.read(dp);
        double flux_z = fa_flux_z.read(dp);
        double spec_z = -rho_y * vel_z - flux_z;
        fa_out_z.write(dp, spec_z);
      }
    }
  }
#endif
}

#ifdef USE_GPU_KERNELS
extern
void gpu_field_spec_precalc(const size_t max_elements,
                            const double *rho_d,
                            const double *vel_x_d,
                            const double *vel_y_d,
                            const double *vel_z_d,
                            const double *flux_x_d,
                            const double *flux_y_d,
                            const double *flux_z_d,
                            double *pre_x_d,
                            double *pre_y_d,
                            double *pre_z_d);
#endif

/*static*/
void CalcSpecPerFieldTask::gpu_base_impl(S3DRank *rank, const Task *task,
                                    const Rect<3> &subgrid_bounds,
                                    const std::vector<RegionRequirement> &reqs,
                                    const std::vector<PhysicalRegion> &regions,
                                    Context ctx, HighLevelRuntime *runtime)
{
#ifndef NO_COMPUTE
#ifdef USE_GPU_KERNELS
  RegionAccessor<AccessorType::Generic,double> fa_in_vel_x = 
    regions[2].get_field_accessor(task->regions[2].instance_fields[0]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_vel_y = 
    regions[2].get_field_accessor(task->regions[2].instance_fields[1]).typeify<double>();
  RegionAccessor<AccessorType::Generic,double> fa_in_vel_z = 
    regions[2].get_field_accessor(task->regions[2].instance_fields[2]).typeify<double>();

  const int num_species = (task->regions[0].instance_fields.size()/3);
  const size_t n_pts = subgrid_bounds.volume();

  for (int spec = 0; spec < num_species; spec++)
  {
    RegionAccessor<AccessorType::Generic,double> fa_out_pre_x = 
      regions[0].get_field_accessor(task->regions[0].instance_fields[3*spec]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_out_pre_y = 
      regions[0].get_field_accessor(task->regions[0].instance_fields[3*spec+1]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_out_pre_z = 
      regions[0].get_field_accessor(task->regions[0].instance_fields[3*spec+2]).typeify<double>();

    RegionAccessor<AccessorType::Generic,double> fa_in_rho = 
      regions[1].get_field_accessor(task->regions[1].instance_fields[spec]).typeify<double>();

    RegionAccessor<AccessorType::Generic,double> fa_in_flux_x = 
      regions[2].get_field_accessor(task->regions[2].instance_fields[3*spec+3]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_in_flux_y = 
      regions[2].get_field_accessor(task->regions[2].instance_fields[3*spec+4]).typeify<double>();
    RegionAccessor<AccessorType::Generic,double> fa_in_flux_z = 
      regions[2].get_field_accessor(task->regions[2].instance_fields[3*spec+5]).typeify<double>();
   
    Rect<3> subrect;
    ByteOffset in_offsets[3], offsets[3];

    const double *in_rho_ptr = fa_in_rho.raw_rect_ptr<3>(subgrid_bounds, subrect, in_offsets);
    if(!in_rho_ptr || (subrect != subgrid_bounds) || 
       !offsets_are_dense<3,double>(subgrid_bounds, in_offsets)) assert(false);

    const double *in_vel_x_ptr = fa_in_vel_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_vel_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    const double *in_vel_y_ptr = fa_in_vel_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_vel_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    const double *in_vel_z_ptr = fa_in_vel_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_vel_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    const double *in_flux_x_ptr = fa_in_flux_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_flux_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    const double *in_flux_y_ptr = fa_in_flux_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_flux_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    const double *in_flux_z_ptr = fa_in_flux_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!in_flux_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    double *out_precalc_x_ptr = fa_out_pre_x.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_precalc_x_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    double *out_precalc_y_ptr = fa_out_pre_y.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_precalc_y_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    double *out_precalc_z_ptr = fa_out_pre_z.raw_rect_ptr<3>(subgrid_bounds, subrect, offsets);
    if(!out_precalc_z_ptr || (subrect != subgrid_bounds) || offset_mismatch(3, in_offsets, offsets)) assert(false);

    gpu_field_spec_precalc(n_pts, in_rho_ptr, in_vel_x_ptr, in_vel_y_ptr, in_vel_z_ptr,
                           in_flux_x_ptr, in_flux_y_ptr, in_flux_z_ptr,
                           out_precalc_x_ptr, out_precalc_y_ptr, out_precalc_z_ptr);
  }
#endif
#endif
}
