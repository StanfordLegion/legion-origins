#!/usr/bin/python

import os
import sys
import getopt
import re
import tempfile
import shutil
import subprocess

opts, args = getopt.getopt(sys.argv[1:], "w:o:m:x:l:g:p:b:t:k:s:H:e:fhr:")
envvars = [ x[1] for x in opts if x[0] == '-e' ]
opts = dict(opts)

def usage(msg):
    if msg:
        sys.stderr.write(msg + "\n")
    exit(1)

def parse_dim3(s):
    m1 = re.match(r'^[0-9]+$', s)
    if m1:
        return (int(s), int(s), int(s))

    m2 = re.match(r'^([0-9]+)x([0-9]+)x([0-9]+)$', s)
    if m2:
        return (int(m2.group(1)), int(m2.group(2)), int(m2.group(3)))

    usage("Invalid grid size: " + s)

if "-h" in opts:
    usage()
basedir = opts.get("-b") or os.environ.get("S3DROOT") or "."
workdir = opts.get("-w") or os.environ.get("WORK") or "."
keepdir = opts.get("-k")
outfile = opts.get("-o") or "-"
mechanism = opts.get("-m")
print opts
exe = opts.get("-x") or usage("Must specify executable via -x!")
lib = opts.get("-l")
local_grid = parse_dim3(opts.get("-g", "32"))
proc_grid = parse_dim3(opts.get("-p", "1"))
timesteps = int(opts.get("-t", "10"))
spawn = opts.get("-s", "local")
hosts = opts.get("-H")
no_lagging = "-f" in opts
node_file = opts.get("-r")

if not mechanism:
    # try to guess mechanism from binary
    with open(exe, "rb") as f:
        data = f.read()
        m = re.search(r'Using chemical mechanism: ([A-Za-z0-9_]+)', data)
        if not m:
            print "Not able to guess mechanism yet!"
            exit(1)
        mechanism = m.group(1)
        print "Mechanism appears to be:", mechanism

# create a temporary directory for stuff to live in (or use what we're told)
if keepdir:
    tdir = keepdir
    if not os.path.isdir(keepdir):
        os.mkdir(keepdir)
else:
    tdir = tempfile.mkdtemp(dir = workdir)
tdir = os.path.abspath(tdir)

# need three subdirectories
idir = os.path.join(tdir, "input")
ddir = os.path.join(tdir, "data")
rdir = os.path.join(tdir, "run")

if not os.path.isdir(idir):
    os.mkdir(idir)
if not os.path.isdir(ddir):
    os.mkdir(ddir)
if not os.path.isdir(rdir):
    os.mkdir(rdir)

# copy over input files
indep_dir = os.path.join(basedir, "input")
chemdep_dir = os.path.join(basedir, "input", "chemistry_dependent", mechanism)
if not os.path.exists(chemdep_dir):
    print "Error: %s does not exist - typo in mechanism?" % chemdep_dir
    exit(1)

def copy_files(src, tgt):
    for f in os.listdir(src):
        fullpath = os.path.join(src, f)
        if os.path.isfile(fullpath):
            shutil.copy(fullpath, tgt)

copy_files(indep_dir, idir)
copy_files(chemdep_dir, idir)

# copy over executable and shared lib, if it exists
shutil.copy(exe, tdir)
exe = os.path.join(tdir, os.path.basename(exe))
if node_file <> None:
    shutil.copy(node_file, tdir)

if lib:
    # library always gets copied in as librhsf.so
    tlib = os.path.join(tdir, "librhsf.so")
    shutil.copy(lib, tlib)
    lib = tlib
    #print os.environ["LD_LIBRARY_PATH"]
    os.environ["LD_LIBRARY_PATH"] = tdir + ":" + os.environ["LD_LIBRARY_PATH"]
    #print os.environ["LD_LIBRARY_PATH"]

# now generate the right s3d.in
with open(os.path.join(idir, "s3d.in"), "w") as f:
    f.write("==========================================================================================\n")
    f.write("MODE\n")
    f.write("==========================================================================================\n")
    f.write("0                   - 0 = run DNS code, 1 = post-process DNS results     (mode)\n")
    f.write("==========================================================================================\n")
    f.write("GRID DIMENSION PARAMETERS\n")
    f.write("==========================================================================================\n")
    f.write("%-6d                 - global number of grid points in the x-direction    (nx_g)\n" % (local_grid[0] * proc_grid[0]))
    f.write("%-6d                 - global number of grid points in the y-direction    (ny_g)\n" % (local_grid[1] * proc_grid[1]))
    f.write("%-6d                 - global number of grid points in the z-direction    (nz_g)\n" % (local_grid[2] * proc_grid[2]))
    f.write("%-6d                 - number of processors in x-direction                (npx)\n" % proc_grid[0])
    f.write("%-6d                 - number of processors in y-direction                (npy)\n" % proc_grid[1])
    f.write("%-6d                 - number of processors in z-direction                (npz)\n" % proc_grid[2])
    f.write("==========================================================================================\n")
    f.write("RUN-TIME PARAMETERS\n")
    f.write("==========================================================================================\n")
    f.write("0                   - 0 for write output to screen, 1 for write to file  (i_write)\n")
    f.write("0                   - 0 for new run, 1 for restart                       (i_restart)\n")
    f.write("%-6d                - ending time step                                   (i_time_end)\n" % timesteps)
    f.write("1000                - frequency to save fields in restart files          (i_time_save)\n")
    f.write("1.0e+5              - time period to save fields in restart files        (time_save_inc)\n")
    f.write("==========================================================================================\n")
    f.write("GEOMETRY PARAMETERS\n")
    f.write("==========================================================================================\n")
    f.write("bomb                - title of run, sets initialiation of flow field     (run_title)\n")
    f.write("1                   - 0 for no x-direction dependence, 1 for so          (vary_in_x)\n")
    f.write("1                   - 0 for no y-direction dependence, 1 for so          (vary_in_y)\n")
    f.write("1                   - 0 for no z-direction dependence, 1 for so          (vary_in_z)\n")
    f.write("1                   - 0 for non-periodic in x-direction, 1 for periodic  (periodic_x)\n")
    f.write("1                   - 0 for non-periodic in y-direction, 1 for periodic  (periodic_y)\n")
    f.write("1                   - 0 for non-periodic in z-direction, 1 for periodic  (periodic_z)\n")
    f.write("1                   - 0 for stretched edges in x-dir, 1 for uniform      (unif_grid_x)\n")
    f.write("1                   - 0 for stretched edges in y-dir, 1 for uniform      (unif_grid_y)\n")
    f.write("1                   - 0 for stretched edges in z-dir, 1 for uniform      (unif_grid_z)\n")
    f.write("40.0e-6             - minimum grid spacing for streching in x direction  (min_grid_x)\n")
    f.write("40.0e-6             - minimum grid spacing for streching in y direction  (min_grid_y)\n")
    f.write("40.0e-6             - minimum grid spacing for streching in z direction  (min_grid_z)\n")
    f.write("0                   - 0 for no turbulence, 1 for isotropic turbulence    (i_turbulence)\n")
    f.write("1                   - BC at x=0 boundary; 1 nonreflecting, 0 hard inflow (nrf_x0)\n")
    f.write("1                   - BC at x=L boundary; 1 nonreflecting, 0 hard inflow (nrf_xl)\n")
    f.write("1                   - BC at y=0 boundary; 1 nonreflecting, 0 hard inflow (nrf_y0)\n")
    f.write("1                   - BC at y=L boundary; 1 nonreflecting, 0 hard inflow (nrf_yl)\n")
    f.write("1                   - BC at z=0 boundary; 1 nonreflecting, 0 hard inflow (nrf_z0)\n")
    f.write("1                   - BC at z=L boundary; 1 nonreflecting, 0 hard inflow (nrf_zl)\n")
    f.write("0.2                 - fix factor for pressure drift                      (relax_ct)\n")
    f.write("==========================================================================================\n")
    f.write("PHYSICAL PARAMETERS\n")
    f.write("==========================================================================================\n")
    f.write("0.0                 - minimum value of grid in x-direction in cm         (xmin)\n")
    f.write("0.0                 - minimum value of grid in y-direction in cm         (ymin)\n")
    f.write("0.0                 - minimum value of grid in z-direction in cm         (zmin)\n")
    f.write("1.0                 - maximum value of grid in x-direction in cm         (xmax)\n")
    f.write("1.0                 - maximum value of grid in y-direction in cm         (ymax)\n")
    f.write("1.0                 - minimum value of grid in z-direction in cm         (zmax)\n")
    f.write("0.001               - Mach number where re_real/mach_no = re_acoustic    (mach_no)\n")
    f.write("100.0               - real convective Reynolds number                    (re_real)\n")
    f.write("0.708               - Prandtl number                                     (pr)\n")
    f.write("==========================================================================================\n")
    f.write("NUMERICS PARAMETERS\n")
    f.write("==========================================================================================\n")
    f.write("1                   - 0 for no reaction, 1 for reaction                    (i_react)\n")
    f.write("8                   - order of spatial derivatives: 6th or 8th only        (iorder)\n")
    f.write("10                  - frequency to monitor min/max and active              (i_time_mon)\n")
    f.write("-1                  - frequency to check spatial resolution                (i_time_res)\n")
    f.write("-1                  - frequency to write tecplot file                      (i_time_tec)\n")
    f.write("10                  - order of spatial filter                              (i_filter)\n")
    f.write("10                  - frequency to filter solution vector                  (i_time_fil)\n")
    f.write("==========================================================================================\n")
    f.write("REQUIRED REFERENCE VALUES\n")
    f.write("==========================================================================================\n")
    f.write("1.4                 - reference ratio of specific heats                    (g_ref)\n")
    f.write("347.2               - reference speed of sound (m/s)                       (a_ref)\n")
    f.write("300.0               - freestream temperature (K)                           (to)\n")
    f.write("1.1766              - reference density (kg/m^3)                           (rho_ref)\n")
    f.write("26.14e-3            - reference thermal conductivity (W/m-s)               (lambda_ref) \n")
    f.write("==========================================================================================\n")
    f.write("flag to enable/disable tracer\n")
    f.write("==========================================================================================\n")
    f.write("0                   - tracer control                                       (tracer_ctrl)\n")
    f.write("==========================================================================================\n")
    f.write("flag to enable/disable MPI I/O\n")
    f.write("==========================================================================================\n")
    f.write("1                   - I/O method: 0:Fortran I/O, 1:MPI-IO, 2:PnetCDF, 3:HDF5\n")

with open(os.path.join(idir, "mixavg.in"), "w") as f:
    f.write("\n")
    f.write("F           F = no baro-diffusion,     T = on        (baro_switch)\n")
    f.write("F           F = no thermal diffusion,  T = on        (thermDiff_switch)\n")
    f.write("\n")
    f.write("%s           F = no lagging of coefficients,  T = on  (lagging_switch)\n" % ("F" if no_lagging else "T"))
    f.write("1           number of steps lagged                   (lag_steps)\n")
    f.write("1           0 for default diffflux calc, other for johnmc routine\n")
    f.write("1           0 for default heatflux calc, other for johnmc routine\n")
    f.write("1           0 for default mcavis calc, other for johnmc routine\n")

# now we can actually run it!
n_ranks = proc_grid[0] * proc_grid[1] * proc_grid[2]
if spawn == "local":
    assert n_ranks == 1
    if outfile == "-":
        outf = sys.stdout
    else:
        outf = open(outfile, "w")
    #print exe, rdir
    # apply environment variables locally
    for e in envvars:
        v = e.split('=', 1)
        if len(v) == 1:
            os.environ[v[0]] = '1'
        else:
            os.environ[v[0]] = v[1]

    subprocess.call([ exe ], shell = False, cwd = rdir, stdin = None, stdout = outf, stderr = outf)
    if not keepdir:
        # nuke temp tree
        shutil.rmtree(tdir)

elif spawn == "mpi":
    cmdline = [ "mpirun", "-n", str(n_ranks) ]
    if hosts:
        cmdline.extend([ "-H", hosts ])

    cmdline.append("--report-bindings")

    if lib:
        cmdline.extend([ "-x", "LD_LIBRARY_PATH", "--pernode", "--bind-to", "none" ])
    else:
        cmdline.extend([ "--bind-to-core", "--bysocket" ])

    for e in envvars:
        cmdline.extend([ "-x", e ])

    #if outfile != "-":
    #    cmdline.extend([ "--output-filename", os.path.abspath(outfile) ])
    cmdline.append(exe)

    if outfile == "-":
        outf = sys.stdout
    else:
        outf = open(outfile, "w")
    print cmdline
    subprocess.call(cmdline, cwd = rdir, stdin = None, stdout = outf, stderr = outf)
    if not keepdir:
        # nuke temp tree
        shutil.rmtree(tdir)

elif spawn == "aprun":
    #cmdline = [ "aprun", "-n", str(n_ranks), "-N", "1", "-d", "16", "-cc", "none" ]
    cmdline = [ "aprun", "-n", str(n_ranks), "-N", "1", "-cc", "none" ]
    if node_file <> None:
        node_file = os.path.join(tdir, node_file)
        cmdline.extend([ "-l", node_file ])
    #if lib:
	#print os.environ["LD_LIBRARY_PATH"]
        #cmdline.extend([ "-e", "LD_LIBRARY_PATH=$LD_LIBRARY_PATH" ])

    #tlib = os.path.join(tdir, "libgasnet-gemini-par.so")
    #shutil.copy("/ccs/home/mebauer/gasnet/lib/libgasnet-gemini-par.so", tlib)

    for e in envvars:
        cmdline.extend([ "-e", e ])

    cmdline.append(exe)

    if outfile == "-":
        outf = sys.stdout
    else:
        outf = open(outfile, "w")

    print cmdline
    subprocess.call(cmdline, cwd = rdir, stdin = None, stdout = outf, stderr= outf)
    if not keepdir:
        shutil.rmtree(tdir)

else:
    assert False

