
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <algorithm>
#include <cmath>

#include "legion.h"

#define NO_DEBUG_POINTERS

using namespace LegionRuntime::HighLevel;

using namespace LegionRuntime::Accessor;

using namespace LegionRuntime::Arrays;

enum {
  TOP_LEVEL_TASK_ID,
};

enum {
  FIELDID_V = 0,
};

template <typename T>
struct Add {
  typedef T LHS;
  typedef T RHS;

  template <bool EXCL> static void apply(T& lhs, T rhs) { lhs += rhs; }
  template <bool EXCL> static void fold(T& rhs1, T rhs2) { rhs1 += rhs2; }
  static const T identity;
};

template <typename T>
/*static*/ const T Add<T>::identity = 0;

static ReductionOpID REDOP_ADD_FLOAT = 1234;

template <typename T>
struct Subtract {
  typedef T LHS;
  typedef T RHS;

  template <bool EXCL> static void apply(T& lhs, T rhs) { lhs -= rhs; }
  // fold is still add
  template <bool EXCL> static void fold(T& rhs1, T rhs2) { rhs1 += rhs2; }
  static const T identity = 0;
};

template <typename T>
class LMatrix {
public:
  LMatrix(void);
  LMatrix(const LMatrix<T>& other);
  LMatrix(int _rows, int _cols, int _rblock, int _cblock, HighLevelRuntime *runtime, Context ctx);

  LMatrix<T>& operator=(const LMatrix<T>& rhs);

  void clear(T clear_val, HighLevelRuntime *runtime, Context ctx, bool wait = false);
  void randomize(int seed, HighLevelRuntime *runtime, Context ctx, bool wait = false);
  void show(const char *name, HighLevelRuntime *runtime, Context ctx, bool wait = false);
  static void gemm(T alpha, const LMatrix<T>& A, const LMatrix<T>& B,
		   T beta, const LMatrix<T>& C,
		   HighLevelRuntime *runtime, Context ctx, bool wait = false);

  int rows, cols, rblock, cblock;
  IndexSpace ispace;
  FieldSpace fspace;
  Blockify<2> blkify;
  IndexPartition ipart;
  LogicalRegion region;
};

template <typename T>
LMatrix<T>::LMatrix(void) {}

template <typename T>
LMatrix<T>::LMatrix(const LMatrix<T>& other)
  : rows(other.rows), cols(other.cols), rblock(other.rblock), cblock(other.cblock),
    ispace(other.ispace), fspace(other.fspace), blkify(other.blkify), ipart(other.ipart),
    region(other.region)
{}

template <typename T>
LMatrix<T>& LMatrix<T>::operator=(const LMatrix<T>& rhs)
{
  rows = rhs.rows;
  cols = rhs.cols;
  rblock = rhs.rblock;
  cblock = rhs.cblock;
  ispace = rhs.ispace;
  fspace = rhs.fspace;
  blkify = rhs.blkify;
  ipart = rhs.ipart;
  region = rhs.region;
}

template <typename T>
LMatrix<T>::LMatrix(int _rows, int _cols, int _rblock, int _cblock,
		 HighLevelRuntime *runtime, Context ctx)
  : rows(_rows), cols(_cols), rblock(_rblock), cblock(_cblock)
{
  assert((rows % rblock) == 0);
  assert((cols % cblock) == 0);

  Point<2> lo = make_point(0, 0);
  Point<2> hi = make_point(rows-1, cols-1);
  Point<2> blk = make_point(rblock, cblock);
  Rect<2> extent(lo, hi);

  ispace = runtime->create_index_space(ctx, Domain::from_rect<2>(extent));
  blkify = Blockify<2>(blk);
  ipart = runtime->create_index_partition(ctx, ispace, blkify, 0);

  fspace = runtime->create_field_space(ctx);
  {
    FieldAllocator fa = runtime->create_field_allocator(ctx, fspace);
    fa.allocate_field(sizeof(T), FIELDID_V);
  }

  region = runtime->create_logical_region(ctx, ispace, fspace);
}

template <typename T>
class ClearMatrixTask : public IndexLauncher {
public:
  struct TaskArgs {
    LMatrix<T> matrix;
    T clear_val;
  };

  ClearMatrixTask(Domain domain,
		  TaskArgument global_arg,
		  ArgumentMap arg_map,
		  Predicate pred = Predicate::TRUE_PRED,
		  bool must = false,
		  MapperID id = 0,
		  MappingTagID tag = 0);

  static int TASKID;

  static void register_tasks(void);

public:
  static void cpu_task(const Task *task,
		       const std::vector<PhysicalRegion> &regions,
		       Context ctx, HighLevelRuntime *runtime);
};

template <typename T> /*static*/ int ClearMatrixTask<T>::TASKID;

template <typename T>
ClearMatrixTask<T>::ClearMatrixTask(Domain domain,
				    TaskArgument global_arg,
				    ArgumentMap arg_map,
				    Predicate pred /*= Predicate::TRUE_PRED*/,
				    bool must /*= false*/,
				    MapperID id /*= 0*/,
				    MappingTagID tag /*= 0*/)
  : IndexLauncher(TASKID, domain, global_arg, arg_map, pred, must, id, tag)
{
}

template <typename T>
/*static*/ void ClearMatrixTask<T>::register_tasks(void)
{
  TASKID = HighLevelRuntime::register_legion_task<ClearMatrixTask<T>::cpu_task >(AUTO_GENERATE_ID,
										 Processor::LOC_PROC, 
										 false, true);
  printf("registered as task id %d\n", TASKID);
}

template <typename T>
void ClearMatrixTask<T>::cpu_task(const Task *task,
				  const std::vector<PhysicalRegion> &regions,
				  Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(TaskArgs));
  const TaskArgs *args = (const TaskArgs *)(task->args);
  //printf("clear val = %f\n", args->clear_val);

  Point<2> p = task->index_point.get_point<2>();
  //printf("point = (%d, %d)\n", p[0], p[1]);

  int rb = args->matrix.rblock;
  int cb = args->matrix.cblock;

  Rect<2> bounds, subrect;
  bounds.lo.x[0] = p[0] * rb;
  bounds.lo.x[1] = p[1] * cb;
  bounds.hi.x[0] = (p[0] + 1) * rb - 1;
  bounds.hi.x[1] = (p[1] + 1) * cb - 1;
  ByteOffset offsets[2];
  T *base = regions[0].get_field_accessor(FIELDID_V).template typeify<T>().template raw_rect_ptr<2>(bounds, subrect, offsets);
  assert(subrect == bounds);
#ifdef DEBUG_POINTERS
  printf("ptr = %p (%d, %d)\n", base, offsets[0].offset, offsets[1].offset);
#endif

  for(int ri = 0; ri < args->matrix.rblock; ri++)
    for(int ci = 0; ci < args->matrix.cblock; ci++)
      *(base + ri * offsets[0] + ci * offsets[1]) = args->clear_val;
}

template <typename T>
void LMatrix<T>::clear(T clear_val, HighLevelRuntime *runtime, Context ctx, bool wait /*= false*/)
{
  LogicalPartition lpart = runtime->get_logical_partition(ctx, region, ipart);

  Domain d = runtime->get_index_partition_color_space(ctx, ipart);

  typename ClearMatrixTask<T>::TaskArgs args;
  args.matrix = *this;
  args.clear_val = clear_val;

  ClearMatrixTask<T> launcher(runtime->get_index_partition_color_space(ctx, ipart),
			      TaskArgument(&args, sizeof(args)),
			      ArgumentMap());
  launcher.add_region_requirement(RegionRequirement(lpart, 0, WRITE_DISCARD, EXCLUSIVE, region).
				  add_field(FIELDID_V));

  FutureMap fm = runtime->execute_index_space(ctx, launcher);

  if(wait)
    fm.wait_all_results();
}

template <typename T>
class RandomizeMatrixTask : public IndexLauncher {
public:
  struct TaskArgs {
    LMatrix<T> matrix;
    int seed;
  };

  RandomizeMatrixTask(Domain domain,
		  TaskArgument global_arg,
		  ArgumentMap arg_map,
		  Predicate pred = Predicate::TRUE_PRED,
		  bool must = false,
		  MapperID id = 0,
		  MappingTagID tag = 0);

  static int TASKID;

  static void register_tasks(void);

public:
  static void cpu_task(const Task *task,
		       const std::vector<PhysicalRegion> &regions,
		       Context ctx, HighLevelRuntime *runtime);
};

template <typename T> /*static*/ int RandomizeMatrixTask<T>::TASKID;

template <typename T>
RandomizeMatrixTask<T>::RandomizeMatrixTask(Domain domain,
				    TaskArgument global_arg,
				    ArgumentMap arg_map,
				    Predicate pred /*= Predicate::TRUE_PRED*/,
				    bool must /*= false*/,
				    MapperID id /*= 0*/,
				    MappingTagID tag /*= 0*/)
  : IndexLauncher(TASKID, domain, global_arg, arg_map, pred, must, id, tag)
{
}

template <typename T>
/*static*/ void RandomizeMatrixTask<T>::register_tasks(void)
{
  TASKID = HighLevelRuntime::register_legion_task<RandomizeMatrixTask<T>::cpu_task >(AUTO_GENERATE_ID,
										 Processor::LOC_PROC, 
										 false, true);
  printf("registered as task id %d\n", TASKID);
}

template <typename T>
void RandomizeMatrixTask<T>::cpu_task(const Task *task,
				  const std::vector<PhysicalRegion> &regions,
				  Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(TaskArgs));
  const TaskArgs *args = (const TaskArgs *)(task->args);

  Point<2> p = task->index_point.get_point<2>();
  //printf("point = (%d, %d)\n", p[0], p[1]);

  int rb = args->matrix.rblock;
  int cb = args->matrix.cblock;

  Rect<2> bounds, subrect;
  bounds.lo.x[0] = p[0] * rb;
  bounds.lo.x[1] = p[1] * cb;
  bounds.hi.x[0] = (p[0] + 1) * rb - 1;
  bounds.hi.x[1] = (p[1] + 1) * cb - 1;
  ByteOffset offsets[2];
  T *base = regions[0].get_field_accessor(FIELDID_V).template typeify<T>().template raw_rect_ptr<2>(bounds, subrect, offsets);
  assert(subrect == bounds);
#ifdef DEBUG_POINTERS
  printf("ptr = %p (%d, %d)\n", base, offsets[0].offset, offsets[1].offset);
#endif

  unsigned short xsubi[3];
  xsubi[0] = args->seed;
  xsubi[1] = p[0];
  xsubi[2] = p[1];
  for(int i = 0; i < 10; i++) nrand48(xsubi);  // scramble PRNG a little

  for(int ri = 0; ri < args->matrix.rblock; ri++)
    for(int ci = 0; ci < args->matrix.cblock; ci++)
      *(base + ri * offsets[0] + ci * offsets[1]) = /*ri*10+ci;*/ erand48(xsubi) - 0.5;
}

template <typename T>
void LMatrix<T>::randomize(int seed, HighLevelRuntime *runtime, Context ctx, bool wait /*= false*/)
{
  LogicalPartition lpart = runtime->get_logical_partition(ctx, region, ipart);

  Domain d = runtime->get_index_partition_color_space(ctx, ipart);

  typename RandomizeMatrixTask<T>::TaskArgs args;
  args.matrix = *this;
  args.seed = seed;

  RandomizeMatrixTask<T> launcher(runtime->get_index_partition_color_space(ctx, ipart),
			      TaskArgument(&args, sizeof(args)),
			      ArgumentMap());
  launcher.add_region_requirement(RegionRequirement(lpart, 0, WRITE_DISCARD, EXCLUSIVE, region).
				  add_field(FIELDID_V));

  FutureMap fm = runtime->execute_index_space(ctx, launcher);

  if(wait)
    fm.wait_all_results();
}

template <typename T>
class ShowMatrixTask : public TaskLauncher {
public:
  struct TaskArgs {
    LMatrix<T> matrix;
    char name[20];
  };

  ShowMatrixTask(TaskArgument global_arg,
		 Predicate pred = Predicate::TRUE_PRED,
		 MapperID id = 0,
		 MappingTagID tag = 0);

  static int TASKID;

  static void register_tasks(void);

public:
  static void cpu_task(const Task *task,
		       const std::vector<PhysicalRegion> &regions,
		       Context ctx, HighLevelRuntime *runtime);
};

template <typename T> /*static*/ int ShowMatrixTask<T>::TASKID;

template <typename T>
ShowMatrixTask<T>::ShowMatrixTask(TaskArgument arg,
				  Predicate pred /*= Predicate::TRUE_PRED*/,
				  MapperID id /*= 0*/,
				  MappingTagID tag /*= 0*/)
  : TaskLauncher(TASKID, arg, pred, id, tag)
{
}

template <typename T>
/*static*/ void ShowMatrixTask<T>::register_tasks(void)
{
  TASKID = HighLevelRuntime::register_legion_task<ShowMatrixTask<T>::cpu_task >(AUTO_GENERATE_ID,
										 Processor::LOC_PROC, 
										 true, true);
  printf("registered as task id %d\n", TASKID);
}

template <typename T>
void ShowMatrixTask<T>::cpu_task(const Task *task,
				 const std::vector<PhysicalRegion> &regions,
				 Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(TaskArgs));
  const TaskArgs *args = (const TaskArgs *)(task->args);

  Rect<2> bounds, subrect;
  bounds.lo.x[0] = 0;
  bounds.lo.x[1] = 0;
  bounds.hi.x[0] = args->matrix.rows - 1;
  bounds.hi.x[1] = args->matrix.cols - 1;
  ByteOffset offsets[2];
  T *base = regions[0].get_field_accessor(FIELDID_V).template typeify<T>().template raw_rect_ptr<2>(bounds, subrect, offsets);
  assert(subrect == bounds);
#ifdef DEBUG_POINTERS
  printf("ptr = %p (%d, %d)\n", base, offsets[0].offset, offsets[1].offset);
#endif

  for(int ri = 0; ri < args->matrix.rows; ri++) {
    printf("%s[%d, %d:%d] = [", args->name, ri, 0, args->matrix.cols - 1);
    for(int ci = 0; ci < args->matrix.cols; ci++)
      printf(" %f", *(base + ri * offsets[0] + ci * offsets[1]));
    printf(" ]\n");
  }
}

template <typename T>
void LMatrix<T>::show(const char *name, HighLevelRuntime *runtime, Context ctx, bool wait /*= false*/)
{
  typename ShowMatrixTask<T>::TaskArgs args;
  args.matrix = *this;
  strcpy(args.name, name);

  ShowMatrixTask<T> launcher(TaskArgument(&args, sizeof(args)));
  launcher.add_region_requirement(RegionRequirement(region, READ_ONLY, EXCLUSIVE, region).
				  add_field(FIELDID_V));

  Future f = runtime->execute_task(ctx, launcher);

  if(wait)
    f.get_void_result();
}

template <typename T>
class GemmTask : public TaskLauncher {
public:
  struct TaskArgs {
    LMatrix<T> A, B, C;
    T alpha, beta;
    int m, n, k;
  };

  GemmTask(TaskArgument global_arg,
	   Predicate pred = Predicate::TRUE_PRED,
	   MapperID id = 0,
	   MappingTagID tag = 0);

  static int TASKID;

  static void register_tasks(void);

public:
  static void cpu_task(const Task *task,
		       const std::vector<PhysicalRegion> &regions,
		       Context ctx, HighLevelRuntime *runtime);
};

template <typename T> /*static*/ int GemmTask<T>::TASKID;

template <typename T>
GemmTask<T>::GemmTask(TaskArgument arg,
		      Predicate pred /*= Predicate::TRUE_PRED*/,
		      MapperID id /*= 0*/,
		      MappingTagID tag /*= 0*/)
  : TaskLauncher(TASKID, arg, pred, id, tag)
{
}

template <typename T>
/*static*/ void GemmTask<T>::register_tasks(void)
{
  TASKID = HighLevelRuntime::register_legion_task<GemmTask<T>::cpu_task >(AUTO_GENERATE_ID,
										 Processor::LOC_PROC, 
										 true, true);
  printf("registered as task id %d\n", TASKID);
}

template <typename T>
void GemmTask<T>::cpu_task(const Task *task,
			   const std::vector<PhysicalRegion> &regions,
			   Context ctx, HighLevelRuntime *runtime)
{
  assert(task->arglen == sizeof(TaskArgs));
  const TaskArgs *args = (const TaskArgs *)(task->args);

  Rect<2> bounds, subrect;
  bounds.lo.x[0] = args->m;
  bounds.lo.x[1] = args->n;
  bounds.hi.x[0] = bounds.lo.x[0] + args->C.rblock - 1;
  bounds.hi.x[1] = bounds.lo.x[1] + args->C.cblock - 1;
  ByteOffset c_offsets[2];
#if 0
  T *c_base = regions[0].get_field_accessor(FIELDID_V).template typeify<T>().template raw_rect_ptr<2>(bounds, subrect, c_offsets);
#else
  T *c_base = regions[0].get_accessor().template typeify<T>().template raw_rect_ptr<2>(bounds, subrect, c_offsets);
#endif
  assert(subrect == bounds);
#ifdef DEBUG_POINTERS
  printf("c ptr = %p (%d, %d)\n", c_base, c_offsets[0].offset, c_offsets[1].offset);
#endif

  bounds.lo.x[0] = args->m;
  bounds.lo.x[1] = args->k;
  bounds.hi.x[0] = bounds.lo.x[0] + args->A.rblock - 1;
  bounds.hi.x[1] = bounds.lo.x[1] + args->A.cblock - 1;
  ByteOffset a_offsets[2];
  T *a_base = regions[1].get_field_accessor(FIELDID_V).template typeify<T>().template raw_rect_ptr<2>(bounds, subrect, a_offsets);
  assert(subrect == bounds);
#ifdef DEBUG_POINTERS
  printf("a ptr = %p (%d, %d)\n", a_base, a_offsets[0].offset, a_offsets[1].offset);
#endif

  bounds.lo.x[0] = args->k;
  bounds.lo.x[1] = args->n;
  bounds.hi.x[0] = bounds.lo.x[0] + args->B.rblock - 1;
  bounds.hi.x[1] = bounds.lo.x[1] + args->B.cblock - 1;
  ByteOffset b_offsets[2];
  T *b_base = regions[2].get_field_accessor(FIELDID_V).template typeify<T>().template raw_rect_ptr<2>(bounds, subrect, b_offsets);
  assert(subrect == bounds);
#ifdef DEBUG_POINTERS
  printf("b ptr = %p (%d, %d)\n", b_base, b_offsets[0].offset, b_offsets[1].offset);
#endif

  // iterate N^3 products within the tile
  for(int mm = 0; mm < args->C.rblock; mm++)
    for(int nn = 0; nn < args->C.cblock; nn++)
      for(int kk = 0; kk < args->A.cblock; kk++) {
	T prod = (*(a_base + mm * a_offsets[0] + kk * a_offsets[1]) *
		  *(b_base + kk * b_offsets[0] + nn * b_offsets[1]));
	*(c_base + mm * c_offsets[0] + nn * c_offsets[1]) += args->alpha * prod;
      }
}

template <typename T>
/*static*/ void LMatrix<T>::gemm(T alpha, const LMatrix<T>& A, const LMatrix<T>& B,
				 T beta, const LMatrix<T>& C,
				 HighLevelRuntime *runtime, Context ctx, bool wait /*= false*/)
{
  std::vector<Future> wait_on;

  // iterator over blocks ourselves
  assert(C.rows == A.rows);
  assert(A.cols == B.rows);
  assert(B.cols == C.cols);
  assert(C.rblock == A.rblock);
  assert(A.cblock == B.rblock);
  assert(B.cblock == C.cblock);

  typename GemmTask<T>::TaskArgs args;
  args.A = A;
  args.B = B;
  args.C = C;
  args.alpha = alpha;
  args.beta = beta;
  for(int m = 0; m < C.rows; m += C.rblock)
    for(int n = 0; n < C.cols; n += C.cblock)
      for(int k = 0; k < A.cols; k += A.cblock) {
	args.m = m;
	args.n = n;
	args.k = k;
	GemmTask<T> launcher(TaskArgument(&args, sizeof(args)));
	LogicalRegion c_tile = runtime->get_logical_subregion(ctx, 
							      runtime->get_logical_partition(ctx, C.region, C.ipart),
							      runtime->get_index_subspace(ctx,
											  C.ipart,
											  make_point(m / C.rblock, n / C.cblock)));
#if 0
	launcher.add_region_requirement(RegionRequirement(c_tile, READ_WRITE, EXCLUSIVE, C.region)
					.add_field(FIELDID_V));
#else
	launcher.add_region_requirement(RegionRequirement(c_tile, REDOP_ADD_FLOAT, EXCLUSIVE, C.region)
					.add_field(FIELDID_V));
#endif

	LogicalRegion a_tile = runtime->get_logical_subregion(ctx, 
							      runtime->get_logical_partition(ctx, A.region, A.ipart),
							      runtime->get_index_subspace(ctx,
											  A.ipart,
											  make_point(m / A.rblock, k / A.cblock)));
	launcher.add_region_requirement(RegionRequirement(a_tile, READ_ONLY, EXCLUSIVE, A.region)
					.add_field(FIELDID_V));

	LogicalRegion b_tile = runtime->get_logical_subregion(ctx, 
							      runtime->get_logical_partition(ctx, B.region, B.ipart),
							      runtime->get_index_subspace(ctx,
											  B.ipart,
											  make_point(k / B.rblock, n / B.cblock)));
	launcher.add_region_requirement(RegionRequirement(b_tile, READ_ONLY, EXCLUSIVE, B.region)
					.add_field(FIELDID_V));

	Future f = runtime->execute_task(ctx, launcher);
	if(wait)
	  wait_on.push_back(f);
      }

  for(std::vector<Future>::iterator it = wait_on.begin();
      it != wait_on.end();
      it++)
    (*it).get_void_result();
}

// computes z = alpha * x + y
struct MainArgs {
  unsigned num_blocks;
  unsigned num_elems;
  float alpha;
  IndexSpace ispace;
  FieldSpace fspace;
  Blockify<1> blkify;
  IndexPartition ipart;
  //Domain blk_domain;
  LogicalRegion r_x, r_y, r_z;
};

float get_rand_float() {
  return (((float)2*rand()-RAND_MAX)/((float)RAND_MAX));
}

void top_level_task(const void *args, size_t arglen,
		    const std::vector<RegionRequirement> &reqs,
		    const std::vector<PhysicalRegion> &regions,
		    Context ctx, HighLevelRuntime *runtime) {
  InputArgs *inputs = (InputArgs*)args;
  int M = 4;
  int N = 4;
  int K = 4;
  int MB = 2;
  int NB = 2;
  int KB = 2;
  int seed = 55;

#define INTARG(s, v) if(!strcmp(inputs->argv[i], (s))) { (v) = atoi(inputs->argv[++i]); continue; }
  for (int i = 1; i < inputs->argc; i++) {
    INTARG("-M", M);
    INTARG("-N", N);
    INTARG("-K", K);
    INTARG("-MB", MB);
    INTARG("-NB", NB);
    INTARG("-KB", KB);
    INTARG("-seed", seed);
  }

  LMatrix<float> A(M, K, MB, KB, runtime, ctx);
  LMatrix<float> B(K, N, KB, NB, runtime, ctx);
  LMatrix<float> C(M, N, MB, NB, runtime, ctx);

  A.show("A", runtime, ctx, true /*wait*/);

  A.randomize(seed + 1, runtime, ctx, true /*wait*/);
  B.randomize(seed + 2, runtime, ctx, true /*wait*/);
  C.clear(0, runtime, ctx, true /*wait*/);

  A.show("A", runtime, ctx, true /*wait*/);
  B.show("B", runtime, ctx, true /*wait*/);
  C.show("C", runtime, ctx, true /*wait*/);

  LMatrix<float>::gemm(1.0, A, B, 1.0, C, runtime, ctx, true /*wait*/);

  C.show("C", runtime, ctx, true /*wait*/);
}

void create_mappers(Machine *machine, HighLevelRuntime *runtime,
                    const std::set<Processor> &local_procs) {
  //runtime->replace_default_mapper(new TestMapper(machine, runtime, local));
}

int main(int argc, char **argv) {
  srand(time(NULL));

  HighLevelRuntime::set_registration_callback(create_mappers);
  HighLevelRuntime::set_top_level_task_id(TOP_LEVEL_TASK_ID);
  HighLevelRuntime::register_single_task<top_level_task>(TOP_LEVEL_TASK_ID, Processor::LOC_PROC, false, "top_level_task");

  ClearMatrixTask<float>::register_tasks();
  RandomizeMatrixTask<float>::register_tasks();
  ShowMatrixTask<float>::register_tasks();
  GemmTask<float>::register_tasks();

  HighLevelRuntime::register_reduction_op<Add<float> >(REDOP_ADD_FLOAT);

  return HighLevelRuntime::start(argc, argv);
}
