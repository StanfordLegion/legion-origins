
#ifndef __LEGION_VECTOR__
#define __LEGION_VECTOR__

#include <cassert>

#include "legion.h"
#include "accessor.h"

namespace LegionStd {
  template<typename T, bool DEBUG = false>
  class LegionVector {
  private:
    // Some typedefs to make typing easier
    typedef LegionRuntime::HighLevel::HighLevelRuntime Runtime;
    typedef LegionRuntime::HighLevel::Context Context;
    typedef LegionRuntime::HighLevel::LogicalRegion LogicalRegion;
    typedef LegionRuntime::HighLevel::PhysicalRegion PhysicalRegion;
    typedef LegionRuntime::LowLevel::ElementMask ElementMask;
    typedef LegionRuntime::LowLevel::ElementMask::Enumerator Enumerator;
    typedef LegionRuntime::HighLevel::IndexSpaceRequirement IndexSpaceRequirement;
    typedef LegionRuntime::HighLevel::RegionRequirement RegionRequirement;
    typedef LegionRuntime::HighLevel::FieldID FieldID;
    typedef LegionRuntime::HighLevel::IndexSpace IndexSpace;
    typedef LegionRuntime::HighLevel::FieldSpace FieldSpace;
    typedef LegionRuntime::HighLevel::FieldAllocator FieldAllocator;
    typedef LegionRuntime::HighLevel::PrivilegeMode PrivilegeMode;
    typedef LegionRuntime::HighLevel::CoherenceProperty CoherenceProperty;
    typedef LegionRuntime::HighLevel::AllocateMode AllocateMode;
    typedef LegionRuntime::HighLevel::MapperID MapperID;
    typedef LegionRuntime::HighLevel::MappingTagID MappingTagID;
    typedef LegionRuntime::HighLevel::IndexAllocator IndexAllocator;
    typedef LegionRuntime::Accessor::RegionAccessor<LegionRuntime::Accessor::AccessorType::AOS<sizeof(T)>,T> RegionAccessor;
  public:
    //-------------------------------------------------------------------------
    LegionVector(void)
      : handle(LogicalRegion::NO_REGION), fid(0), mapped(false) { }
    //-------------------------------------------------------------------------
    LegionVector(Runtime *rt, Context ctx, LogicalRegion h, PhysicalRegion d, FieldID f) 
      : handle(h), fid(f), mapped(true), data(d)
    //-------------------------------------------------------------------------
    {
      alloc = rt->create_index_allocator(ctx, handle.get_index_space());
      data.wait_until_valid();
      acc = LegionRuntime::Accessor::extract_accessor<LegionRuntime::Accessor::AccessorType::AOS<sizeof(T)>,T>(data);
    }
    //-------------------------------------------------------------------------
    LegionVector(Runtime *rt, Context ctx, size_t max_elements = 1024)
      : mapped(false)
    //-------------------------------------------------------------------------
    {
      IndexSpace is = rt->create_index_space(ctx, max_elements);
      FieldSpace fs = rt->create_field_space(ctx); 
      FieldAllocator falloc = rt->create_field_allocator(ctx, fs);
      fid = falloc.allocate_field(sizeof(T)); 
      handle = rt->create_logical_region(ctx, is, fs);
    }
  public:
    //-------------------------------------------------------------------------
    inline void map(Runtime *rt, Context ctx, PrivilegeMode priv, CoherenceProperty coher, 
                    MapperID mapper_id = 0, MappingTagID tag = 0)
    //-------------------------------------------------------------------------
    {
      if (DEBUG)
        assert(!mapped);
      RegionRequirement req(handle, priv, coher, handle, tag); 
      req.add_field(fid);
      data = rt->map_region(ctx, req, mapper_id, tag);
      alloc = rt->create_index_allocator(ctx, handle.get_index_space());
      data.wait_until_valid();
      mapped = true;
      acc = LegionRuntime::Accessor::extract_accessor<LegionRuntime::Accessor::AccessorType::AOS<sizeof(T)>,T>(data);
    }
    //-------------------------------------------------------------------------
    inline void unmap(Runtime *rt, Context ctx)
    //-------------------------------------------------------------------------
    {
      if (DEBUG)
        assert(mapped); 
      rt->unmap_region(ctx, data);
      alloc = IndexAllocator(); // frees up reference
      mapped = false;
      data = PhysicalRegion();
    }
    //-------------------------------------------------------------------------
    inline void destroy(Runtime *rt, Context ctx)
    //-------------------------------------------------------------------------
    {
      if (mapped)
        unmap(rt, ctx);
      rt->destroy_logical_region(ctx,handle);
      rt->destroy_index_space(ctx,handle.get_index_space());
      rt->destroy_field_space(ctx,handle.get_field_space());
      handle = LogicalRegion::NO_REGION;
    }
    //-------------------------------------------------------------------------
    inline RegionRequirement get_region_requirement(PrivilegeMode priv, CoherenceProperty coher, bool virt = false)
    //-------------------------------------------------------------------------
    {
      RegionRequirement result(handle, priv, coher, handle);
      result.add_field(fid, !virt);
      return result;
    }
    //-------------------------------------------------------------------------
    inline IndexSpaceRequirement get_index_requirement(AllocateMode mode)
    //-------------------------------------------------------------------------
    {
      IndexSpaceRequirement result(handle.get_index_space(), mode, handle.get_index_space());
      return result;
    }
  public:
    //-------------------------------------------------------------------------
    inline ptr_t push_back(const T &elem)
    //-------------------------------------------------------------------------
    {
      if (DEBUG)
        assert(mapped);
      ptr_t new_loc = alloc.alloc();
      if (DEBUG)
        assert(new_loc != ptr_t::nil());
      acc.write(new_loc, elem);
      return new_loc;
    }
    //-------------------------------------------------------------------------
    inline void pop_back(void)
    //-------------------------------------------------------------------------
    {
      ptr_t last = handle.get_index_space().get_valid_mask().last_enabled();
      alloc.free(last);
    }
    //-------------------------------------------------------------------------
    inline T& back(void)
    //-------------------------------------------------------------------------
    {
      ptr_t last = handle.get_index_space().get_valid_mask().last_enabled();
      return acc.ref(last);
    }
    //-------------------------------------------------------------------------
    inline T& operator[](ptr_t ptr)
    //-------------------------------------------------------------------------
    {
      return acc.ref(ptr);
    }
    //-------------------------------------------------------------------------
    const T& operator[](ptr_t ptr) const
    //-------------------------------------------------------------------------
    {
      return acc.ref(ptr);
    }
  public:
    //-------------------------------------------------------------------------
    inline bool empty(void) const { return (handle.get_index_space().get_valid_mask().pop_count() == 0); }
    //-------------------------------------------------------------------------
    inline size_t size(void) const { return handle.get_index_space().get_valid_mask().pop_count(); }
    //-------------------------------------------------------------------------
  public:
    class iterator {
    public:
      iterator(void) : ptr(ptr_t::nil()), finished(true), remaining(0),
                      enumerator(NULL), owner(NULL) { } // make a null iterator
    protected:
      friend class LegionVector<T>;
      iterator(LegionVector<T> *own, LogicalRegion h)
        : enumerator(h.get_index_space().get_valid_mask().enumerate_enabled()), owner(own)
      {
        int pos;
        finished = !(enumerator->get_next(pos,remaining));
        remaining--;
        if (finished)
          ptr = ptr_t::nil();
        else
          ptr = pos;
      }
    public:
      //-------------------------------------------------------------------------
      inline iterator operator++(void) { advance(); return *this; }
      //-------------------------------------------------------------------------
      inline iterator operator++(int) { advance(); return *this; }
      //-------------------------------------------------------------------------
      inline T& operator*(void) { return (*owner)[ptr]; }
      //-------------------------------------------------------------------------
      inline bool operator==(const iterator &rhs) const { return (ptr == rhs.ptr); }
      //-------------------------------------------------------------------------
      inline bool operator!=(const iterator &rhs) const { return (ptr != rhs.ptr); }
      //-------------------------------------------------------------------------
    private:
      inline void advance(void)
      {
        if (remaining > 0)
        {
          ptr++;
          remaining--;
        }
        else
        {
          int pos;
          finished = !(enumerator->get_next(pos,remaining));
          remaining--;
          if (finished)
            ptr = ptr_t::nil();
          else
            ptr = pos;
        }
      }
    public:
      ptr_t ptr;
    private:
      bool finished;
      int remaining;
      Enumerator *enumerator;
      LegionVector *owner;
    };
    //-------------------------------------------------------------------------
    inline iterator begin(void) const { return iterator(const_cast<LegionVector<T>*>(this), handle); }
    //-------------------------------------------------------------------------
    inline iterator end(void) const { return iterator(); }
    //-------------------------------------------------------------------------
  private:
    LogicalRegion handle;
    FieldID fid;
    bool mapped;
    PhysicalRegion data;
    RegionAccessor acc;
    IndexAllocator alloc;
  };
};

#endif // __LEGION_VECTOR__

// EOF

