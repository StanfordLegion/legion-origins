
/**
 * Graph algorithms.
 */

"use strict";
var graph = (function () {
  var me = {};

  /** Computes the k longest weighted paths (nodes have weights) starting
    * at 'start' to all other nodes.
    */
  me.k_longest_paths = function(start, k, nodes) {

    // expected layout of nodes (just check for first node)
    assert(start != null);
    assert("id" in start);
    assert("succs" in start);
    assert("preds" in start);

    var order = graph.topological_sort(start, nodes);
    var info = {};

    for (var key in order) {
      var n = order[key];
      info[n.id] = [];
    }

    // in info[n.id] we store the information about the longest paths
    // found so far for node n.  we store a list of at most k entries,
    // each consisting of {w, (pred_node, pred_path_idx)}, where w is the
    // weighted length of the path to this node, pred_node is the predecessor
    // node in the path, and pred_path_idx is the index into the list of
    // paths at that node (stored at info[pred_node.id]) for which path to
    // follow.
    // to indicate the end of a path, we store null as the predecessor node,
    // and any value for the index.
    // the i-th entry is the i-th longest path to this node. if there are
    // fewer than k entries, then there aren't k paths to this node.

    info[start.id] = [{
      weight: start.weight(),
      pred_node: null,
      pred_path_idx: -1,
    }];

    order = order.slice(1); // the first node does not need to be processed
    for (var key in order) {
      var cur = order[key];

      // we look at all possible paths from all predecessors to 'cur', and
      // pick the k longest ones
      var candidates = [];
      for (var key in cur.preds) {
        var pred = cur.preds[key];
        if (!(pred.id in info)) continue; // ignore unconnected nodes.
        var pred_info = info[pred.id];
        for (var i = 0; (i < k) && (i < pred_info.length); i++) {
          var pii = pred_info[i];
          candidates.push({
            weight: pii.weight + cur.weight(),
            pred_node: pred,
            pred_path_idx: i,
          });
        }
      }
      candidates.sort(function (a,b) { return b.weight - a.weight; });
      if (candidates.length > k) {
        candidates = candidates.slice(0, k);
      }
      info[cur.id] = candidates;
    }

    return info;
  }

  /** Returns a map from node id's to the number of paths ending in 'end'
   * (according to 'info') go through that node.  Assumes that 'info' is
   * the result of a call to graph.k_longest_paths.
   */
  me.node_frequency_on_paths = function (start, end, info, nodes) {
    var res = [];
    var allnodes = nodes || graph.all_nodes(start);
    for (var key in allnodes) {
      res[allnodes[key].id] = 0;
    }
    // initialize the number of paths thad end in 'end'
    res[end.id] = info[end.id].length;

    var follow_path = function (path) {
      var pred = path.pred_node;
      if (pred != null) {
        assert(pred.id in res);
        res[pred.id] += 1
        var next_path = info[pred.id][path.pred_path_idx];
        follow_path(next_path);
      }
    };
    // follow all paths backwards, and update 'res'
    for (var key in info[end.id]) {
      var path = info[end.id][key];
      follow_path(path);
    }
    return res;
  };

  /** Returns the longest path from 'start' to 'end'.  Assumes that 'info' is
   * the result of a call to graph.k_longest_paths.
   */
  me.longest_path = function (start, end, info) {
    var res = [];
    res.push(end);

    var follow_path = function (path) {
      var pred = path.pred_node;
      if (pred != null) {
        res.push(pred);
        var next_path = info[pred.id][path.pred_path_idx];
        follow_path(next_path);
      }
    };
    follow_path(info[end.id][0]);
    return res.reverse();
  };

  /**
   * Computes a topological sorting of all nodes reachable from the start node.
   */
  me.topological_sort = function (start, nodes) {
    // expected layout of nodes (just check for first node)
    assert(start != null);
    assert("id" in start);
    assert("succs" in start);
    assert("preds" in start);
    assert(start.preds.length == 0);

    var all;
    if (nodes != null) {
      all = util.object_to_array(nodes);
    } else {
      all = graph.all_nodes(start);
    }
    var in_degree = {};
    all.map(function (n) { in_degree[n.id] = n.preds.filter(function(x){return all.indexOf(x) != -1}).length; });
    var res = [];
    var zero_in_degree = [];
    all.map(function (n) { if (in_degree[n.id] == 0) zero_in_degree.push(n); });

    while (zero_in_degree.length != 0) {
      var cur = zero_in_degree[0];
      zero_in_degree = zero_in_degree.slice(1);
      res.push(cur);
      for (var key in cur.succs) {
        var succ = cur.succs[key];
        in_degree[succ.id] -= 1;
        if (in_degree[succ.id] == 0) {
          zero_in_degree.push(succ);
        }
      }
    }
    assert(res.length == all.length);

    return res;
  }

  /** Returns all nodes reachable from a given node in a graph. */
  me.all_nodes = function (start) {
    return util.object_to_array(graph.all_nodes_object(start));
  }

  /** Returns all nodes reachable from a given node in a graph. */
  me.all_nodes_object = function (start) {
    var res = {};
    var worklist = [start];
    while (worklist.length != 0) {
      var cur = worklist.pop();
      if (cur.id in res) continue;
      res[cur.id] = cur;
      for (var key in cur.succs) {
        if (!(cur.succs[key].id in res))
          worklist.push(cur.succs[key]);
      }
    }
    return res;
  }

  /** Returns the number of edges. */
  me.n_edges = function (nodes) {
    var res = 0;
    for (var key in nodes) {
      res += nodes[key].succs.length;
    }
    return res;
  }

  /** Computes the transitive reduction of a graph. */
  me.transitive_reduction = function (start) {
    var sorted = graph.topological_sort(start);
    var lookup = {};
    for (var i = 0; i < sorted.length; i++) {
      var node = sorted[i];
      transitive_reduction_helper(sorted, i, node);
    }
  }

  function transitive_reduction_helper(sorted, index, start) {
    var curlength = graph.all_nodes_object(start);
    for (var key in curlength) {
      curlength[key] = 0;
    }
    var dont_retain = {}

    for (var i = index+1; i < sorted.length; i++) {
      var cur = sorted[i];
      for (var key in cur.preds) {
        var pred = cur.preds[key];
        if (!(pred.id in curlength)) continue; // ignore unreachable nodes
        var pred_length = curlength[pred.id];
        if (pred_length > 0) {
          dont_retain[cur.id] = true;
        }
        curlength[cur.id] = 1;
      }
    }
    // remove any edge (start,n) where n is in dont_retain
    var succs = start.succs.slice(0);
    for (var key in succs) {
      var succ = succs[key];
      if (succ.id in dont_retain) {
        remove(succ, start.succs);
        remove(start, succ.preds);
      }
    }
  }

  /** Computes the transitive reduction of a graph. */
  me.transitive_reduction = function (start, allnodes) {
    var nodes = allnodes || graph.all_nodes(start);
    var sorted = graph.topological_sort(start, nodes);
    // build an index to quickly decide if two nodes are connected
    var lookup = {};
    for (var key in nodes) {
      var node = nodes[key];
      var nid = node.id;
      var l = {};
      for (var key in node.succs) {
        var succ = node.succs[key];
        l[succ.id] = true;
      }
      lookup[nid] = l;
    }

    for (var key in nodes) {
      var node = nodes[key];
      var nid = node.id;
      var succs = node.succs.slice(0);
      for (var key1 in succs) {
        var succ1 = succs[key1];
        // see if (node -> succ1) is redundant by finding a succ2, such that
        // (node, succ2) and (succ2, succ1)
        for (var key2 in node.succs) {
          var succ2 = node.succs[key2];
          if (succ1.id in lookup[succ2.id]) {
            remove(succ1, node.succs);
            remove(node, succ1.preds);
            break;
          }
        }
      }
    }
  }

  function remove(item, array) {
    var i = array.indexOf(item);
    assert(i != -1);
    array.splice(i, 1);
  }

  /** Returns the set of strongly connected components. */
  me.scc = function (nodes) {
    // based on tarjan's algorithm
    var index = 0;
    var S = [];
    var indices = {};
    var lowlink = {};
    var sccs = [];

    var strongconnect = function (node) {
      indices[node.id] = index;
      lowlink[node.id] = index;
      index += 1;
      S.push(node);

      for (var key in node.succs) {
        var succ = node.succs[key];
        if (!(succ.id in indices)) {
          strongconnect(succ);
          lowlink[node.id] = Math.min(lowlink[node.id], lowlink[succ.id]);
        } else if (S.indexOf(succ) != -1) {
          lowlink[node.id] = Math.min(lowlink[node.id], indices[succ.id]);
        }
      }

      if (indices[node.id] == lowlink[node.id]) {
        var scc = [];
        var w = null;
        while (w != node) {
          w = S.pop();
          scc.push(w);
        }
        sccs.push(scc);
      }
    };

    for (var key in nodes) {
      var node = nodes[key];
      if (!(node.id in indices)) {
        strongconnect(node);
      }
    }
    return sccs;
  }

  me.is_cyclic = function (nodes) {
    return graph.scc(nodes).map(function(x){return x.length}).filter(function(d){return d>1}).length > 0
  }

  return me;
}());
