
/**
 * Code to visualize the execution as a timeline.
 */

"use strict";
var timeline = (function () {
  var me = {};
  var s = logparser;
  var v = {};

  me.v = v;

  // ---------------------------------------------------------------------------
  // --- Initialization of visualization
  // ---------------------------------------------------------------------------

  me.visualize = function () {

    me.update_model();
    set_general_sizes();
    
    v.vis_div = d3.select("#vis");
    v.svg = v.vis_div.append("svg").attr("id", "vis-svg");
    /*$("#vis-svg").append(util.parse_svg(util.hereDoc(function() {
/*
<defs>
<filter id="dropglow">
    <feGaussianBlur stdDeviation="10.5" result="coloredBlur"/>
    <feMerge>
        <feMergeNode in="coloredBlur"/>
        <feMergeNode in="coloredBlur"/>
        <feMergeNode in="coloredBlur"/>
        <feMergeNode in="SourceGraphic"/>
    </feMerge>
</filter>
</defs>
/
})))*/

    v.graph = new ExecGraphVis(s.graph);

    v.xscale_startdomain = [0, d3.max(s.graph.map(function (n) { return n.end; }))];
    v.xscale = d3.scale.linear()
      .domain(v.xscale_startdomain);
    set_x_sizes();

    // decide what processors to display
    v.active_procs = {};
    s.graph.foreach(function(node) {
      v.active_procs[node.processor.id] = node.processor;
    })
    v.active_procs = util.object_to_array(v.active_procs)
      .sort(function (a,b) {
        if (a.id * b.id < 0) {
          // all negative id's should come after positive id's
          return b.id - a.id;
        }
        return a.id - b.id;
      });
    v.procscale = d3.scale.ordinal()
      .domain(v.active_procs.map(function(d){return d.id}))
      .range(d3.range(v.active_procs.length));

    v.current_category = function() {
      return v.model.current_category
    }

    v.svg.append("rect")
     .attr("class", "pane");

    set_sizes();

    v.svg.selectAll("text.processor")
      .data(v.active_procs)
      .enter()
      .append("text")
      .attr("class", "processor")
      .attr("x", function (d) {return v.margin.left;})
      .attr("y", function (d) {
        return v.margin.top + (v.procscale(d.id))*(v.row.height) - v.rowheader.offset;
      })
      .text(function (d) {
        if (d.id >= 0) return d.kind + " (ID " + util.as_hex(d.id) + ")";
        return d.kind;
      })

    v.xAxis = d3.svg.axis()
      .scale(v.xscale)
      .orient("bottom")
      .tickFormat(formatTime)
      .ticks(5);

    v.svg.append("g")
      .attr("class", "x axis");

    v.svg.append("g")
      .attr("id", "block_highlight");

    v.svgrects = v.svg.append("g")
      .attr("id", "rect-nodes");

    // add buttons
    common.add_buttons(v, [
      new RepeatableButton(">", function(){ move_x_axis(1); }, "Scroll visualization to the right"),
      new RepeatableButton("<", function(){ move_x_axis(-1); }, "Scroll visualization to the left"),
      new RepeatableButton("-", function(){ navigation_step(v.zoom * 1.5, v.translation); }, "Zoom out"),
      new RepeatableButton("+", function(){ navigation_step(v.zoom / 1.5, v.translation); }, "Zoom in"),
      new Button("Reset zoom", reset, null),
    ]);
    /*b.append("input")
      .attr("type", "number")
      .attr("id", "button_many_crit_k")
      .attr("value", 100)
      .attr("style", "width: 60px; margin-left: 10px");
    b.append("button")
      .attr("type", "button")
      .attr("id", "button_many_crit_go")
      .text("Recompute path")
      .on("click", function() { recolor(); });*/

    // initialize zooming and navigation control
    v.zoom = 1;
    v.translation = 0;
    v.undo_buffer = [];
    v.redo_buffer = [];

    if (get_error_count() > 0) {
      print("ERROR-SUMMARY: There were " + get_error_count() + " errors.")
      var cats = "Error categories:\n";
      for (var key in global_error_categories) {
        cats += key + ": " + global_error_categories[key] + "\n";
      }
      $("#errors").attr("title", "There have been " + get_error_count() + " errors (open javascript console to see them).  This suggest a bug in the logging code in Legion, or this visualization tool.\n\n" + cats)
      $("#errors-num").text(get_error_count())
      util.show("#errors")
    }
    /*printhtml(dot.graph_to_dot(s.graph.get_node_array().concat([s.start_node, s.end_node])))
    //,
    //  function (d) {
    //    if (d.id in s.longest_path) return {color: "red"};
    //    return {};
    //  }));
    printhtml(dot.event_graph_to_dot(s.events));*/

    // selection
    v.selection = v.svg.append("rect")
      .attr("class", "h")
      .attr("id", "selection");
    v.cover = v.svg.append("rect")
      .attr("class", "h")
      .attr("id", "cover");
    $("#cover")
      .on('mouseup', finish_selection)
      .on('mousemove', move_selection);
    v.pane = $(".pane")
      .on('mousedown', init_selection)

    // mouse wheel zoom
    $(".pane").on("mousewheel DOMMouseScroll", mouse_wheel_zoom);

    // undo/redo button initialization
    $("#undo").click(undo_navigation_step);
    $("#redo").click(redo_navigation_step);

    // key navigation
    $(document).bind('keydown', 'left', function(){
      move_x_axis(-1);
      return false;
    });
    $(document).bind('keydown', 'right', function(){
      move_x_axis(1);
      return false;
    });
    $(document).bind('keydown', 'up', function(){
      navigation_step(v.zoom / 1.5, v.translation);
      return false;
    });
    $(document).bind('keydown', 'down', function(){
      navigation_step(v.zoom * 1.5, v.translation);
      return false;
    });
    $(document).bind('keydown', 'ctrl+z', function(){
      undo_navigation_step();
      return false;
    });
    $(document).bind('keydown', 'ctrl+y', function(){
      redo_navigation_step();
      return false;
    });
    $(document).bind('keydown', 'esc', function(){
      cancel_selection();
      return false;
    });

    // priority inference buttons
    $("#prio-critical").click(function(){
      run_priority_inference("critical");
    });
    $("#prio-lbd").click(function(){
      run_priority_inference("lbd");
    });
    $("#prio-close").click(function() {
      util.hide("#priorities");
    });
    $("#prio-inference").click(function() {
      $("#priority-log").text("");
      util.show("#priorities");
    });

    // buttons in info view
    $("#info").draggable();
    $("#info-close").bind("click", close_info_view);
    $("#info-button-backwards").click(function() {
      var node = v.current_node;
      v.deps = node.transitive_preds();
      v.dep_node = node;
      document.getElementById("button_category_deps").checked = true;
      recolor();
    })
    $("#info-button-forwards").click(function() {
      var node = v.current_node;
      v.deps = node.transitive_succs();
      v.dep_node = node;
      document.getElementById("button_category_deps").checked = true;
      recolor();
    })
    $("#info-button-latest-backwards").click(function() {
      var node = v.current_node;
      v.deps = node.transitive_latest_preds();
      v.dep_node = node;
      document.getElementById("button_category_deps").checked = true;
      recolor();
    })
    $("#info-button-center").click(function() {
      var node = v.current_node;
      var block = v.graph.node2block[node.id];
      var start = block.begin;
      var end = block.end;
      var block_width = end - start;
      var width = 2*(block_width + 1);
      var middle = (end+start)*1/2;
      rezoom(middle - width/2, middle + width/2);
    })
    $("#info-button-log").click(function() {
      var node = v.current_node;
      print(node);
    })

    if (false) {
      // this code calculates the length of the interesting part of an execution in s3d, where part of the execution is not relevant and should not be measured.
      var s3d_start = 0;
      var s3d_end = 0;
      s.graph.foreach(function (n) {
        if (n.kind instanceof TaskInstanceNodeKind) {
          if (n.kind.instance.operation.rawdata.task.name == "await_mpi_task") {
            s3d_start = n.end;
          }
          if (n.kind.instance.operation.rawdata.task.name == "handoff_to_mpi_task") {
            s3d_end = n.begin;
          }
        }
      })
      print(s3d_end - s3d_start);
    }

    resize();
    window.onresize = resize;

    util.show("#timeline");
  };

  // ---------------------------------------------------------------------------
  // --- Updating size and color in the graph
  // ---------------------------------------------------------------------------

  me.recolor = recolor;
  function recolor() {
    me.update_model();
    // add legend and decide on colors
    if (["many_crit", "crit_path", "deps"].indexOf(v.current_category()) == -1) {
      var items = {};
      s.graph.foreach(function(val) {
        var d = val.category(v);
        items[d.id] = true;
      })
      var colordomain = [];
      for (var id in items) {
        colordomain.push(id);
      }
      v.colors = d3.scale.ordinal()
        .domain(colordomain)
        .range(common.colors);

      items = {};
      s.graph.foreach(function(val) {
        var d = val.category(v);
        items[d.id] = new LegendItem(d.legend_label, v.colors(d.id));
      })

      common.add_legend(v, util.object_to_array(items), v.current_category());
    } else {
      common.add_legend(v, []);
    }

    v.svgrects.selectAll("rect.node")
      //.transition()
      //.duration(first ? 0 : common.default_duration)
      //.attr("fill", "none")
      .attr("fill", function (d) {
        if (["many_crit", "crit_path"].indexOf(v.current_category()) != -1) {
          return common.colors[1];
        }
        if (v.current_category() == "deps") {
          if (d.node == v.dep_node) return common.colors[0];
          return common.colors[1];
        }
        return v.colors(d.category(v).id);
      })
      .attr("opacity", function (d) {
        var min = 0.2;
        if (v.current_category() == "many_crit") {
          if (!d.node_in(v.nodefreq)) return min;
          var freq = v.nodefreq[d.node.id];
          return min + (freq / v.nodefreq[s.end_node.id])*(1-min);
        }
        if (v.current_category() == "crit_path") {
          if (d.node_in(v.longest_path)) return 1;
          return min;
        }
        if (v.current_category() == "deps") {
          if (d.node_in(v.deps) || d.node == v.dep_node) return 1;
          return min;
        }
        return 1;
      })
      //.attr("style", "filter: url(#dropglow);")
  }

  me.resize = resize;
  function resize() {
    set_sizes();
    me.update_model();
    v.graph.rebuild();

    v.svg.attr("width", v.fullwidth)
      .attr("height", v.fullheight);
    v.vis_div.attr("style", "width:" + v.fullwidth + "px");

    v.pane
      .attr("width", v.fullwidth)
      .attr("height", v.fullheight);
    v.cover
      .attr("width", v.fullwidth)
      .attr("height", v.fullheight);

    $("#info").width(v.fullwidth-50);

    v.svg.select("g.x.axis")
      .attr("transform", "translate(" + v.margin.left + "," + (v.height + v.margin.top - v.xaxis.height) + ")");

    var selection = v.svgrects.selectAll("rect.node")
      .data(v.graph.visible_blocks, function(d) { return d.id; });
    selection.exit().remove();
    selection.enter()
      .append("rect")
      .attr("id", function(d) {return "block_"+d.id;})
      .attr("class", "node")
      .on("mousewheel", function(){mouse_wheel_zoom(d3.event)})
      .on("DOMMouseScroll", function(){mouse_wheel_zoom(d3.event)})
      .on("click", function(d) {
        d3.event.stopPropagation();
        initialize_info_view(d.node);
      })
      .append("svg:title")
      .text(function (d) {return d.get_title() })

    recolor();

    v.svg.select("g.x.axis").call(v.xAxis);
    v.svgrects.selectAll("rect.node")
      .attr("width", function (d) { return v.xscale(d.end) - v.xscale(d.begin); })
      .attr("height", v.barHeight)
      .attr("x", function (d) {return v.margin.left + v.xscale(d.begin);})
      .attr("y", function (d) {
        return v.margin.top + v.rowheader.height + (v.procscale(d.processor.id))*(v.row.height);
      });

    // current view overview
    var total = v.fullwidth;
    var execution = 0.75*total;
    var left_offset = (total-execution)/2;
    $("#overview-box").width(total);
    $("#overview-execution")
      .attr("style", "margin: 0 "+ (left_offset) + "px");
    var domain = v.xscale.domain();
    var domain_total = domain[1] - domain[0];
    var orig_domain_total = v.xscale_startdomain[1]-v.xscale_startdomain[0];
    var left = Math.max(0, left_offset + domain[0]/orig_domain_total*execution);
    var w = Math.min(total-left, domain_total/orig_domain_total*execution);
    w = Math.max(1, w);
    $("#overview-current").attr("style", "left:"+left+"px;width:"+w+"px");

    // highlight the current block
    update_current_block_highlight()

    // update undo/redo buttons
    $("#redo").toggleClass("undo-disabled", v.redo_buffer.length == 0);
    $("#undo").toggleClass("undo-disabled", v.undo_buffer.length == 0);
  }

  function update_current_block_highlight() {
    $("#block_highlight").empty();
    if (v.current_node != null) {
      var block = v.graph.node2block[v.current_node.id];
      assert(!block.is_hidden());
      var blockrect = $("#block_" + block.id);
      if (blockrect.length == 0) {
        resize();
      }
      var hdata = {
        x: parseFloat(blockrect.attr("x")),
        y: parseFloat(blockrect.attr("y")),
        h: parseFloat(blockrect.attr("height")),
        w: parseFloat(blockrect.attr("width")),
        m: 4,
        n: 0,
      }
      d3.select("#block_highlight")
        .selectAll("line")
        .data([
          // vertical
          {x1: hdata.x-hdata.m, y1: 0, x2: hdata.x-hdata.m, y2: hdata.y-hdata.n},
          {x1: hdata.x+hdata.w+hdata.m, y1: 0, x2: hdata.x+hdata.w+hdata.m, y2: hdata.y-hdata.n},
          {x1: hdata.x-hdata.m, y1: hdata.y+hdata.h+hdata.n, x2: hdata.x-hdata.m, y2: v.fullheight},
          {x1: hdata.x+hdata.w+hdata.m, y1: hdata.y+hdata.h+hdata.n, x2: hdata.x+hdata.w+hdata.m, y2: v.fullheight},
          // horizontal
          {x1: 0, y1: hdata.y-hdata.m, x2: hdata.x-hdata.n, y2: hdata.y-hdata.m},
          {x1: 0, y1: hdata.y+hdata.h+hdata.m, x2: hdata.x-hdata.n, y2: hdata.y+hdata.h+hdata.m},
          {x1: hdata.x+hdata.w+hdata.n, y1: hdata.y-hdata.m, x2: v.fullwidth, y2: hdata.y-hdata.m},
          {x1: hdata.x+hdata.w+hdata.n, y1: hdata.y+hdata.h+hdata.m, x2: v.fullwidth, y2: hdata.y+hdata.h+hdata.m},
        ])
        .enter()
        .append("line")
        .attr("x1", function(d){return d.x1})
        .attr("y1", function(d){return d.y1})
        .attr("x2", function(d){return d.x2})
        .attr("y2", function(d){return d.y2})
        .attr("style", "stroke:#aaa;stroke-width:1;");
    }
  }

  // ---------------------------------------------------------------------------
  // --- Helper for computing sizes
  // ---------------------------------------------------------------------------

  function set_sizes() {
    set_x_sizes();
    set_y_sizes();
  }

  function set_general_sizes() {
    v.barHeight = 20;
    v.barPadding = 5;
    v.rowheader = {height: 30, offset: -22};
    v.row = {height: v.rowheader.height + v.barHeight + v.barPadding}
    v.legend = {width: 200};
    v.margin = {top: 30, right: 30, bottom: 30, left: 30};
    v.xaxis = {height: 20, margin: 20}
  }

  function set_x_sizes() {
    set_general_sizes();
    v.width = document.body.clientWidth - v.margin.left - v.margin.right - (v.legend.width + 10);
    v.fullwidth = v.width + v.margin.left + v.margin.right;
    recompute_x_axis();
  }

  function set_y_sizes() {
    set_general_sizes();
    v.height = v.active_procs.length * (v.row.height) + v.xaxis.height + v.xaxis.margin;
    v.fullheight = v.height + v.margin.top + v.margin.bottom;
  }

  function move_x_axis(plus) {
    var old = v.xscale.domain();
    var length = old[1] - old[0];
    var vector = Math.round(plus*(length/10));
    navigation_step(v.zoom, v.translation+vector);
  }

  function recompute_x_axis() {
    v.xscale.range([0, v.width]);
    var w = v.xscale_startdomain[1] - v.xscale_startdomain[0];
    var middle = v.xscale_startdomain[0] + 1/2*w + v.translation;
    v.xscale.domain([middle - 1/2*v.zoom*w, middle + 1/2*v.zoom*w]);
  }

  // ---------------------------------------------------------------------------
  // --- Helpers for zooming and navigation
  // ---------------------------------------------------------------------------

  function reset() {
    navigation_step(1, 0);
  }

  function rezoom(left, right) {
    var width = right-left;
    var w = v.xscale_startdomain[1] - v.xscale_startdomain[0];
    var middle = (left+right)*1/2;
    // we just solve the equations from recompute_x_axis for zoom and translation
    navigation_step(width / w, middle - (v.xscale_startdomain[0] + 1/2*w));
  }

  function mouse_wheel_zoom(e) {
    e.preventDefault();
    var delta = 0;
    e = e.originalEvent || e;
    if ('detail' in e) delta = e.detail * -1;
    if ('wheelDelta' in e) delta = e.wheelDelta;
    delta  = Math[ delta  >= 1 ? 'floor' : 'ceil' ](delta  / Math.abs(delta));
    var relClientX = e.clientX - v.pane.offset().left;
    var mouse_pos = v.xscale.invert(relClientX);

    var domain = v.xscale.domain();
    var left = domain[0];
    var right = domain[1];
    var middle = (left + right)/2;
    var width = right - left;
    var newright, newleft;
    if (delta < 0) {
      newright = mouse_pos + (right - mouse_pos) * 1.5;
      newleft = mouse_pos - (mouse_pos - left) * 1.5;
    } else if (delta > 0) {
      newright = mouse_pos + (right - mouse_pos) / 1.5;
      newleft = mouse_pos - (mouse_pos - left) / 1.5;
    } else {
      return;
    }
    rezoom(newleft, newright);
  }

  function init_selection(e) {
    v.selection_data = {
      start: e.clientX - v.pane.offset().left,
    };
    v.selection
      .attr("x", v.selection_data.start)
      .attr("width", 0)
      .attr("y", v.margin.top)
      .attr("height", v.height - v.xaxis.height)
      .classed("h", false);
    v.cover.classed("h", false);
    e.preventDefault();
  }

  function move_selection(e) {
    var relClientX = e.clientX - v.pane.offset().left;
    var w = relClientX - v.selection_data.start;
    var x = v.selection_data.start;
    if (w < 0) {
      w = -w;
      x -= w;
    }
    if (relClientX < v.margin.left) {
      w = w + (x - v.margin.left);
      x = v.margin.left;
    }
    else if (relClientX > v.margin.left + v.width) {
      w = v.margin.left + v.width - x;
    }
    v.selection
      .attr("width", w)
      .attr("x", x)
    return {x: x, w: w};
  }

  function finish_selection(e) {
    if (v.selection_data == null) return; // has the selection already been canceled?
    var r = move_selection(e);
    if (r.w == 0) return;
    var pos_to_time = function (p) {
      return v.xscale.invert(p - v.margin.left);
    }
    rezoom(pos_to_time(r.x), pos_to_time(r.x+r.w));
    v.selection.classed("h", true);
    v.cover.classed("h", true);
    v.selection_data = null;
  }

  function cancel_selection() {
    v.selection_data = null;
    v.selection.classed("h", true);
    v.cover.classed("h", true);
  }

  function navigation_step(zoom, translation) {
    v.undo_buffer.push({
      zoom: v.zoom,
      translation: v.translation,
    })
    v.redo_buffer = [];
    v.zoom = zoom;
    v.translation = translation;
    resize();
  }

  function undo_navigation_step() {
    if (v.undo_buffer.length == 0) return;
    v.redo_buffer.push({
      zoom: v.zoom,
      translation: v.translation,
    })
    var last = v.undo_buffer.pop();
    v.zoom = last.zoom;
    v.translation = last.translation;
    resize();
  }

  function redo_navigation_step() {
    if (v.redo_buffer.length == 0) return;
    v.undo_buffer.push({
      zoom: v.zoom,
      translation: v.translation,
    })
    var last = v.redo_buffer.pop();
    v.zoom = last.zoom;
    v.translation = last.translation;
    resize();
  }

  // ---------------------------------------------------------------------------
  // --- Miscellaneous helpers
  // ---------------------------------------------------------------------------

  me.update_model = function () {
    v.model = {
      hidden_eps: d3.select("#input_hidden_small").property("value"),
      current_category: d3.select("input[name=\"category\"]:checked").attr("id").substr(16),
    }
  }

  me.radiobutton = function (name) {
    if (name == "crit_path" || name == "many_crit") {
      compute_long_paths(name);
    }
    setTimeout(function() {
      // for some reason, clicking on the button prevents changing the checked status
      document.getElementById("button_category_"+name).checked = true;
      recolor();
    }, 0);
  }

  function update_current_node(node, noredraw) {
    v.current_node = node;
    if (!noredraw) update_current_block_highlight();
  }

  // ---------------------------------------------------------------------------
  // --- Priority inference
  // ---------------------------------------------------------------------------

  function run_priority_inference(mode) {
    if (mode == "lbd") {
      var path = util.object_to_array(s.top_level_instance.last_node.transitive_latest_preds());
      priority_inference_helper(path, mode);
    } else if (mode == "critical") {
      var f = [{
        name: "priority inference",
        func: function() {
          var path = graph.longest_path(s.start_node, s.end_node, v.long_paths);
          priority_inference_helper(path, mode);
        },
      }];
      compute_long_paths(null, f);
    } else {
      assert(false);
    }
  }

  function priority_inference_helper(path, mode) {
    var l = path.filter(function(n) {
      return n.kind instanceof TaskInstanceNodeKind && n.stage == "exec";
    }).map(function(n) {
      return n.kind.instance.operation;
    })
    var ids = util.distinct_obj(l.map(function(t){return {id:t.task_id,tag:t.tag,name:t.task_name};}));
    var task_names = util.distinct(ids.map(function(d){return d.name}));
    var r = [];
    for (var key in ids) {
      var id = ids[key];
      r.push("task->task_id == " + id.id + " && task->tag == " + id.tag + " /* " + id.name + " */")
    }
    var code = "";
    code += "// The following tasks have been selected to have higher priority (using mode " + mode + "):";
    code += "\n// " + task_names.join(", ")
    code += "\n\ntask->task_priority = 0;"
    code += "\nif (" + r.join(" ||\n    ") + ") {"
    code += "\n  task->task_priority += 1;"
    code += "\n}";
    $("#priority-log").text(code);
  }

  // ---------------------------------------------------------------------------
  // --- Compute longest paths
  // ---------------------------------------------------------------------------

  function compute_long_paths(category, more_funcs) {
    if (v.long_paths != null) {
      // result already computed
      more_funcs.map(function(d){d.func();}) // still execute more_funcs (not asynchronously though)
      return;
    }
    more_funcs = more_funcs || [];
    var n = s.graph.n_nodes();
    if (n > 20000) {
      if (!confirm("The execution graph contains " + n + " nodes.  It might take a while to perform this operation.  Continue?")) {
        return;
      }
    }
    var funcs = [];
    if (!v.computed_transitive_reduction) {
      funcs.push({name: "computing transitive reduction", func: function() {
        graph.transitive_reduction(s.start_node, s.graph.nodes);
      }});
      funcs.push({name: "merging redundant nodes", func: function() {
        lgraph.merge_nodes(s.graph);
      }});
    }
    funcs.push({name: "computing k longest paths", func: function() {
      $("#input_n_long_paths").attr("disabled", "disabled")
      v.n_long_paths = d3.select("#input_n_long_paths").property("value");
      v.long_paths = graph.k_longest_paths(s.start_node, v.n_long_paths, s.graph.nodes);
      v.nodefreq = graph.node_frequency_on_paths(s.start_node, s.end_node, v.long_paths, s.graph.nodes);
      v.longest_path = util.array_to_object(graph.longest_path(s.start_node, s.end_node, v.long_paths));

      var last_info = v.long_paths[s.top_level_instance.last_node.id];
      print("Found " + last_info.length + " paths, while looking for " + v.n_long_paths + ".")
      print("Difference between shortest/longest: " + last_info[Math.min(v.n_long_paths-1, last_info.length-1)].weight / last_info[0].weight);
      print("Longest: " + last_info[0].weight)
      print("Execution length: " + (s.end_node.end - s.start_node.begin))
      v.computed_transitive_reduction = true;
    }});
    if (category != null) {
      funcs.push({name: "updating visualization", func: function() {
        document.getElementById("button_category_"+category).checked = true;
        recolor();
      }});
    }
    funcs = funcs.concat(more_funcs);
    util.execute_async(funcs);
  }

  // ---------------------------------------------------------------------------
  // --- Info view
  // ---------------------------------------------------------------------------

  var info_view_initialized = false;
  me.initialize_info_view = function(node){initialize_info_view(node)}
  function initialize_info_view(node) {
    var unconnected = node.kind instanceof VisualizationOnlyNodeKind;
    close_info_view(true); // clear any potentially still visible artifacts
    update_current_node(node);
    var block = v.graph.node2block[node.id];
    // position
    if (!info_view_initialized) {
      var pos = $("#info-box-spacer").position();
      $("#info").css("left", pos.left).css("top", pos.top);;
    }
    info_view_initialized = true;
    // content
    $("#info > p.box-title").text(node.toString());
    $("#info-content").html("");
    var add = function (content) { $("#info-content").append(content) }
    if (block.is_hidden()) {
      add("<p style=\"color:gray; font-size: 11px;\">(not currently visible, because it is "+block.hidden_reason()+")</p>")
    }
    add("<p>Duration: " + format_time(node.end-node.begin) + " (from " + format_time(node.begin) + " to " + format_time(node.end) + ")</p>");
    if (!unconnected) {
      add("<p>Predecessors: " + node.preds.length + "</p>");
      add(successor_list_in_info_view(node.preds));
      add("<p>Successors: " + node.succs.length + "</p>");
      add(successor_list_in_info_view(node.succs));
      $("#info-button-backwards").removeClass("h")
      $("#info-button-forwards").removeClass("h")
      $("#info-button-latest-backwards").removeClass("h")
    } else {
      $("#info-button-backwards").addClass("h")
      $("#info-button-forwards").addClass("h")
      $("#info-button-latest-backwards").addClass("h")
    }
    // statistics
    add_node_statistics(node);
    // show box
    util.show("#info");
  }

  /** List of successors (or predecessors) for a given node. */
  function successor_list_in_info_view(succs) {
    var res = $(document.createElement("ol"));
    var i;
    for (i = 0; i < 3+(4==succs.length) && i < succs.length; i++) {
      var a = $(document.createElement("a"));
      var li = $(document.createElement("li")).append(a);
      res.append(li);
      a.text(succs[i].toString());
      a.attr("href", "/");
      a.click(initialize_info_view_func(succs[i]))
    }
    if (i < succs.length) {
      var li = $(document.createElement("li")).text("...");
      res.append(li);
    }
    return res;
  }

  function initialize_info_view_func(n) {
    return function(e) { initialize_info_view(n); return false; };
  }

  function close_info_view(noredraw) {
    update_current_node(null, noredraw);
    util.hide("#info");
    $("#block_highlight").empty();
    $("#info-stats").empty();
  }

  function add_node_statistics(node) {
    var container = $("#info-stats");
    var procedure = node.kind.node_kind_parent;
    compute_statistics(procedure);
    container.append("<p class=\"box-title\">Statistics of " + procedure_string(procedure) + " <i>" + procedure.toString() + "</i></p>");
    add_statistics_table(container, procedure.statistics);

    var overall_instance_stats = function(procedure, task_operation) {
      var task_id = task_operation.task_id;
      var task_name = task_operation.rawdata.task.name;
      var other_ops = [];
      var instances = [];
      for (var key in s.task_operation) {
        var top = s.task_operation[key];
        if (top.task_id == task_id) {
          other_ops.push(top);
          instances = instances.concat(top.instances);
        }
      }
      assert(other_ops.length >= 1);
      if (other_ops.length > 0+(procedure==task_operation)) {
        // aggregate statistics of all instances for this particular task operation
        var stats = instances.map(function(i){
          compute_statistics(i);
          return i.statistics;
        })
        assert(stats.length > 0);
        var aggstats = aggregate_statistics(stats);
        container.append("<p class=\"box-title\">Average statistics of all task instances of the task " + task_name + "</p>");
        container.append("Number of overall instances: " + stats.length);
        add_statistics_table(container, aggstats);
      } else {
        container.append("<p>This is the only " + procedure_string(procedure) + " of the task " + task_name + ".</p>")
      }
    }

    if (procedure instanceof TaskInstance) {
      if (!procedure.is_single()) {
        container.append("<p class=\"box-title\">More details</p>");
        var a = $(document.createElement("a"));
        var p = $(document.createElement("p")).append(a);
        container.append(p);
        a.text("Go to corresponding task operation");
        a.attr("href", "#");
        a.click(initialize_info_view_func(procedure.operation.first_node))
      }
      overall_instance_stats(procedure, procedure.operation);
    }
    if (procedure instanceof TaskOperation && !procedure.is_single()) {
      // aggregate statistics of all instances for this particular task operation
      var stats = procedure.instances.map(function(i){
        compute_statistics(i);
        return i.statistics;
      })
      assert(stats.length > 0);
      var aggstats = aggregate_statistics(stats);
      container.append("<p class=\"box-title\">Average statistics of all task instances of " + procedure.toString() + "</p>");
      container.append("Number of instances: " + stats.length);
      add_statistics_table(container, aggstats);
      overall_instance_stats(procedure, procedure);
    }
  }

  function aggregate_statistics(stats) {
    var helper = function(stats, datafield, keys, agg_fields, keep_fields) {
      var res = {};
      for (var key in keys) {
        key = keys[key];
        res[key] = {};
        var set = false;
        for (var f in agg_fields) {
          f = agg_fields[f];
          var mean = d3.mean(stats.map(function(s){if (s[datafield][key] == null) return null; return s[datafield][key][f]}));
          if (mean === undefined) continue;
          set = true;
          res[key][f] = mean;
          res[key][f+"_std"] = Math.sqrt(d3.mean(stats.map(function(s){if (s[datafield][key] == null) return null; var x = s[datafield][key][f]-res[key][f];return x*x;})))
        }
        if (!set) delete res[key];
      }
      return res;
    }
    return {
      timings: helper(stats, "timings", stats_stages, ["n_nodes", "time"]),
      waits: helper(stats, "waits", ["inline_waits", "future_waits"], ["n", "time"]),
    };
  }

  var stats_stages = ["copy", TIME_DEPENDENCE_ANALYSIS, TIME_PREMAPPING, TIME_MAPPING, TIME_SLICING, TIME_EXEC, TIME_COMPLETE_POINT, TIME_GC];
  function add_statistics_table(container, statistics) {
    var timings = statistics.timings;
    var is_agg = false;
    var table = $(document.createElement("table"));
    container.append(table);
    table.addClass("simple-table")
    for (var i = 0; i < stats_stages.length; i++) {
      var stage = stats_stages[i];
      if (stage in timings) {
        var t = timings[stage];
        is_agg = "time_std" in t;
        if (!is_agg && stage == "exec") stage = "exec (without waiting)"
        var res = "<tr><td style='padding-right:20px'>" + util.capitalize(stage) + "</td>";
        res += "<td>" + format_time(t.time);
        if (is_agg) res += " (σ=" + format_time(t.time_std) + ")"
        if (t.n_nodes > 1) {
          var n_nodes = t.n_nodes;
          if (is_agg) n_nodes += " (σ=" + t.n_nodes_std + ")"
          res += " (accumulated over " + n_nodes + " nodes)";
        }
        res += "</td>";
        res += "</tr>";
        table.append(res);
      }
    }
    for (var key in statistics.waits) {
      var wait = statistics.waits[key];
      var w = "Waiting for inline mappings";
      if (wait.kind == "future_waits") w = "Waiting for children";
      var res = "<tr>";
      res += "<td style='padding-right:20px'>" + w + "</td>";
      res += "<td>" + format_time(wait.time);
      res += " (sum over " + wait.n + " intervals)";
      res += "</td>";
      res += "</tr>";
      table.append(res);
    }
  }

  function procedure_string(procedure) {
    if (procedure instanceof TaskInstance) return "task instance";
    if (procedure instanceof TaskOperation) return "task operation";
    if (procedure instanceof Operation) return "operation";
    if (procedure instanceof Slice) return "slice";
    if (procedure instanceof LowlevelCopy) return "";
    if (procedure instanceof MetaKindParent) return "";
    assert(false);
  }

  function compute_statistics(procedure) {
    if (procedure.statistics != null) return;
    var timings = {};
    for (var key in procedure.nodes) {
      var node = procedure.nodes[key];
      var stage = node.stage;
      assert(stage != null);
      if (timings[stage] == null) {
        timings[stage] = {
          stage: stage,
          n_nodes: 1,
          time: node.end - node.begin,
        }
      } else {
        timings[stage].time += node.end-node.begin;
        timings[stage].n_nodes++;
      }
    }
    var waits = {};
    if (procedure instanceof TaskInstance) {
      var rawwaits = procedure.inline_waits.concat(procedure.future_waits)
      for (var key in rawwaits) {
        var wait = rawwaits[key];
        if (wait.end - wait.begin == 0) continue;
        if (waits[wait.kind] == null) {
          waits[wait.kind] = {
            time: wait.end - wait.begin,
            n: 1,
            kind: wait.kind,
          }
        } else {
          waits[wait.kind].n++;
          waits[wait.kind].time += wait.end - wait.begin;
        }
      }
    }
    procedure.statistics = {
      timings: timings,
      waits: waits,
    };
  }

  // ---------------------------------------------------------------------------
  // --- Formating time values
  // ---------------------------------------------------------------------------

  /**
   * Computes a good formating of the time axis.  All values on the axis are
   * in the same unit (us, ms or s; based on the absolute maximum value on the
   * axis) and the precision of the values shown (how many values after the
   * comma) depends on the range of values shown.
   * Basically it does always what you want. :)
   */
  function formatTime(t) {
    var range = v.xscale.domain();
    var max = Math.max(Math.abs(range[0]), Math.abs(range[1]));
    range = Math.abs(range[1] - range[0]);
    if (max >= 1000000) {
      var newt = t/1000000;
      if (newt.toString().indexOf(".") != -1) {
        var precision = 6 - Math.round(log10(range)) + 1;
        if (precision > 20) precision = 20;
        newt = newt.toFixed(precision);
      }
      return newt + " s";
    }
    if (max >= 1000) {
      var newt = t/1000;
      if (newt.toString().indexOf(".") != -1) {
        var precision = 3 - Math.round(log10(range)) + 1;
        if (precision > 20) precision = 20;
        newt = newt.toFixed(precision);
      }
      return newt + " ms";
    }
    return t + " us";
  }

  function format_time(t) {
    if (t >= 1000000) {
      var newt = t/1000000;
      if (newt.toString().indexOf(".") != -1) {
        newt = newt.toFixed(2);
      }
      return newt + " s";
    }
    if (t >= 1000) {
      var newt = t/1000;
      if (newt.toString().indexOf(".") != -1) {
        newt = newt.toFixed(2);
      }
      return newt + " ms";
    }
    if (t.toString().indexOf(".") != -1) {
      t = t.toFixed(2);
    }
    return t + " us";
  }

  function log10(val) {
    return Math.log(val) / Math.LN10;
  }

  return me;
}());
