
/**
 * Status information.
 */

"use strict";
var stat = (function () {
  var me = {};

  me.hide = function () {
    d3.select("#loading").classed("h", true);
  }

  me.show = function (o) {
    util.show("#loading");
  }

  var last_status = null;
  me.update_status = function (s) {
    var now = new Date().getTime();
    if (last_status != null) {
      var finish_msg = "Finished " + last_status.text + ". [" + (now - last_status.time) + "ms]";
      print("STATUS: " + finish_msg)
      me.push_console(finish_msg)
    }
    var news = util.capitalize(s) + "...";
    print("STATUS: " + news)
    d3.select("#console-status").text(news);
    last_status = {
      time: now,
      text: s,
    }
  }

  me.finish_status = function () {
    me.update_status("done");
    last_status = null;
  }

  me.finish_status_with_error = function () {
    me.update_status("error");
    last_status = null;
  }

  me.init = function () {
    assert(last_status == null);
    d3.select("#console-status").text("");
    d3.select("#console-log").text("");
    me.show();
  }

  me.push_console = function (s) {
    var old = d3.select("#console-log").property("value");
    d3.select("#console-log").text(s + "\n" + old);
  }

  me.show_error = function (message) {
    stat.hide();
    util.show("#error")
    $("#error-message").text(message);
  }

  return me;
}());
