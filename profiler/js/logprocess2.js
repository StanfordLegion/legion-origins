
/**
 * Code to process the parsed information from a log file.
 * Builds a graph of operations, copies, etc. and connects the appropriate nodes.
 */

"use strict";
var logprocess2 = (function () {
  var me = {};
  var s = logparser;

  var task_categories = [];

  me.process = function () {
    s.graph = new ExecGraph();

    // for all task instances, add all relevant nodes
    var nonexec_stages = [TIME_DEPENDENCE_ANALYSIS, TIME_PREMAPPING, TIME_MAPPING, TIME_SLICING];
    for (var key in s.task_instance) {
      var val = s.task_instance[key];
      
      // all stages but the execution stage
      var prev_node = null;
      nonexec_stages.map(function (x) {
        var n = add_node_pipeline_stage(val, x);
        // link the nodes in sequence of their pipeline stages
        if (n != null) {
          if (val.first_node == null) {
            val.first_node = n;
          }
          if (prev_node != null) {
            prev_node.add_succ(n, "task instance pipeline sequence");
          }
          prev_node = n;
        }
      });

      // add execution stage, but split as necessary
      assert(prev_node != null);
      var begin = val.tmptimes[TIME_EXEC].time;
      var exec_proc = val.tmptimes[TIME_EXEC].processor;
      var interrupts = val.get_all_exec_interupts();
      var prev_interrupt = null;
      var first_exec_node = null;
      for (var key in interrupts) {
        var interrupt = interrupts[key];
        if (begin != interrupt.begin) {
          var n = task_instance_node(val, TIME_EXEC, begin, interrupt.begin, exec_proc);
          if (first_exec_node == null) first_exec_node = n;
          prev_node.add_succ(n, "task instance execution stage nodes [1/2]");
          prev_node = n;
        }
        if (prev_interrupt != null) prev_interrupt.next_node = prev_node;
        assert(prev_node != null);
        interrupt.prev_node = prev_node;
        begin = interrupt.end;
        prev_interrupt = interrupt;
      }
      var end_exec = val.tmptimes[TIME_END_EXEC].time;
      if (begin != end_exec) {
        var n = task_instance_node(val, TIME_EXEC, begin, end_exec, exec_proc);
        if (first_exec_node == null) first_exec_node = n;
        prev_node.add_succ(n, "task instance execution stage nodes [2/2]");
        prev_node = n;
        if (prev_interrupt != null) prev_interrupt.next_node = n;
      }
      val.last_exec_node = prev_node;
      val.first_exec_node = first_exec_node;

      // add complete node (we assume its time is very small, and use 1ns)
      var complete_time = val.tmptimes[TIME_COMPLETE_POINT].time;
      var complete_node = task_instance_node(val, "complete", complete_time, complete_time+1, val.tmptimes[TIME_COMPLETE_POINT].processor);
      prev_node.add_succ(complete_node, "task instance last execution node to completion node");

      val.last_node = complete_node;
    }

    // compute which low-level copies can go on the same line when displayed
    var llcopies = util.object_to_array(s.llcopies).sort(function (a,b) {
      if (a.time_begin == b.time_begin) return a.time_end - b.time_end;
      return a.time_begin - b.time_begin;
    });
    var line_start = [];
    for (var key in llcopies) {
      var llcopy = llcopies[key];
      var found = false;
      for (var i = 0; i < line_start.length && !found; i++) {
        // hack for ll-copies without a time
        if (llcopy.time_begin < 0) {
          llcopy.line_id = i+1;
          found = true;
        } else
        if (line_start[i] < llcopy.time_begin) {
          line_start[i] = llcopy.time_end;
          llcopy.line_id = i+1;
          found = true;
        }
      }
      if (!found) {
        line_start.push(llcopy.time_end);
        llcopy.line_id = line_start.length;
      }
    }

    // nodes for low-level copies
    for (var key in s.llcopies) {
      var llcopy = s.llcopies[key];
      var node = lowlevel_copy_node(llcopy);
      llcopy.last_node = node;
      llcopy.first_node = node;
    }

    // nodes for things we only show, but don't link
    for (var key in s.timings_only) {
      var val = s.timings_only[key];
      visualization_only_node(val.kind, val.begin, val.end, val.processor);
    }

    // code to handle the linking of slicing phases
    // the code produces nodes for the slicing stage in 'slicing_procedure',
    // and links these nodes correctly with the slicing or mapping stage
    // of all child_procedures (must be task instances or slicings), and then
    // recursively continues for child_procedures if these are slicings.
    // the process is bottom-up.
    var handle_slicing = function(slicing_procedure, premapping_node, child_procedures) {
      assert(child_procedures.length > 0);
      var first_child = child_procedures[0];
      var child_stage = TIME_SLICING;
      if (first_child instanceof TaskInstance) {
        child_stage = TIME_MAPPING;
      } else {
        assert(first_child instanceof Slice);
      }
      var end_child_stage = "end_" + child_stage;
      // recursively handle children if necessary
      if (first_child instanceof Slice) {
        // skip this level if there is no slicing going on by collecting all child slices of all
        // children and continue there.  if there are no children, go to the task instances
        // directly.
        if (first_child.tmptimes[TIME_SLICING] == null) {
          var children = [];
          for (var key in child_procedures) {
            var child_procedure = child_procedures[key];
            children = children.concat(child_procedure.slices);
          }
          if (children.length == 0) {
            var instances = child_procedures.map(function(s){return s.instance});
            return handle_slicing(slicing_procedure, premapping_node, instances);
          } else {
            return handle_slicing(slicing_procedure, premapping_node, children);
          }
        }

        for (var key in child_procedures) {
          var child_procedure = child_procedures[key];
          assert(child_procedure.slices.length > 0)
          handle_slicing(child_procedure, premapping_node, child_procedure.slices)
        }
      }

      // we might need to split the slicing stage, as child slicings or child task
      // instance mappings might happen "inline".
      var interrupts = [];
      var noninterrupts = [];
      var slicing_prococessor = slicing_procedure.tmptimes[TIME_SLICING].processor.id;
      var slicing_begin = slicing_procedure.tmptimes[TIME_SLICING].time;
      var slicing_end = slicing_procedure.tmptimes[TIME_END_SLICING].time;
      for (var key in child_procedures) {
        var child_procedure = child_procedures[key];
        var child_begin = child_procedure.tmptimes[child_stage];
        var child_end = child_procedure.tmptimes[end_child_stage];
        assert(child_begin.time >= slicing_begin);
        var child_stage_processor = child_begin.processor.id;
        if (child_begin.time < slicing_end && child_stage_processor == slicing_prococessor) {
          assert(child_end.time <= slicing_end);
          assert(child_begin.node != null);
          interrupts.push({
            begin: child_begin.time,
            end: child_end.time,
            node: child_begin.node,
          });
        } else {
          // nothing special to be done, this happens later or somewhere else
          noninterrupts.push(child_begin.node);
        }
      }
      var begin = slicing_begin;
      var prev_interrupt = null;
      var first_node = null;
      for (var i = 0; i < interrupts.length; i++) {
        var interrupt = interrupts[i];
        assert(interrupt.begin != begin)
        var n = slicing_node(slicing_procedure, begin, interrupt.begin);
        if (first_node == null) first_node = n;
        if (i == 0) premapping_node.add_succ(n, "premapping to slicing");
        n.add_succ(interrupt.node, "slicing to slicing/mapping (inplace)");
        if (prev_interrupt != null) {
          // link mapping back to slicing
          prev_interrupt.node.add_succ(n, "slicing/mapping back to slicing [1/2]");
        }
        begin = interrupt.end;
        prev_interrupt = interrupt;
      }
      assert(begin != slicing_end)
      var n = slicing_node(slicing_procedure, begin, slicing_end);
      if (first_node == null) first_node = n;
      slicing_procedure.last_slicing_node = n;
      if (prev_interrupt != null) {
        // link mapping back to slicing
        prev_interrupt.node.add_succ(n, "slicing/mapping back to slicing [2/2]");
      }
      // now link child nodes that do not happen inline off the premapping node
      for (var key in noninterrupts) {
        var child_node = noninterrupts[key];
        // for now we link these from the previous pipeline stage
        // TODO: fix this so mappings of instances are linked of
        // (the correct point) of slicing of the task operation.
        premapping_node.add_succ(child_node, "slicing to slicing/mapping (elsewhere)");
      }
      return {
        first_node: first_node,
        last_node: n,
      }
    }

    // nodes for (task and non-task (like close/mapping/..)) operations
    var nodes_for_operation = function(ops, all_stages, create_node_fnct) {
      for (var key in ops) {
        var op = ops[key];

        // check assumption about data
        var end_all_stages = all_stages.map(function(t) {return "end_" + t})
        for (var t in op.tmptimes) {
          if ([TIME_COMMIT_POINT, TIME_RESOLVE_SPEC_POINT, TIME_COMPLETE_POINT].indexOf(t) != -1) {
            continue; // these don't have end-events
          }
          assert(all_stages.indexOf(t) != -1 || end_all_stages.indexOf(t) != -1, "Unexpected stage found: " + t)
        }

        // add nodes for all stages
        var nodes = [];
        for (var key in all_stages) {
          var stage = all_stages[key];
          if (stage in op.tmptimes) {
            if (stage == TIME_SLICING && op instanceof TaskOperation) {
              var res = handle_slicing(op, op.tmptimes[TIME_PREMAPPING].node, op.slices);
              if (res.first_node != res.last_node) {
                nodes.push(res.first_node);
              }
              nodes.push(res.last_node);
            } else {
              var n = create_node_fnct(op, stage);
              op.tmptimes[stage].node = n;
              nodes.push(n);
            }
          }
        }
        // handle slicing if this task operation is not sliced, but it has a slice
        if (!(TIME_SLICING in op.tmptimes) && op instanceof TaskOperation && op.slices.length > 0) {
          assert(op.slices.length == 1);
          var res = handle_slicing(op.slices[0], op.tmptimes[TIME_PREMAPPING].node, op.slices[0].slices);
          if (res.first_node != res.last_node) {
            nodes.push(res.first_node);
          }
          nodes.push(res.last_node);
        }

        // add complete node (we assume its time is very small, and use 1ns)
        // we don't add complete nodes for operations, only task operations.
        // single task operations don't have a complete node, only their instance does.
        if (op instanceof TaskOperation && !op.is_single()) {
          var complete_time = op.tmptimes[TIME_COMPLETE_POINT].time;
          var complete_node = create_node_fnct(op, TIME_COMPLETE_POINT, complete_time, complete_time+1);
          nodes.push(complete_node);
        }

        // finish up nodes
        nodes.sort(util.sort_by_begin_then_end);
        var prev_node = null;
        for (var key in nodes) {
          var n = nodes[key];
          if (op.first_node == null) {
            op.first_node = n;
          }
          if (prev_node != null) {
            prev_node.add_succ(n, "operation/task operation pipeline node linking");
          }
          prev_node = n;
        }
        op.last_node = prev_node;
      }
    };
    var all_op_stages = [TIME_DEPENDENCE_ANALYSIS, TIME_PREMAPPING, TIME_MAPPING, TIME_SLICING, TIME_EXEC];
    nodes_for_operation(s.operation, all_op_stages, operation_node);
    nodes_for_operation(s.task_operation, all_op_stages, task_operation_node);

    // link all slicings to their instances
    for (var key in s.slices) {
      var slice = s.slices[key];
      if ("last_slicing_node" in slice && slice.slices.length == 0) {
        slice.last_slicing_node.add(slice.instance.first_node, "end of slicing of slice to instance");
      }
    }

    // link parents and subtask when a task is launched
    // link parents and operations when an operation is launched
    for (var key in s.task_instance) {
      var instance = s.task_instance[key];
      if (instance.is_top_level()) {
        assert(s.top_level_instance == null);
        s.top_level_instance = instance;
      }
      // link all launch nodes of this node with the first node of every child launched
      var launches = instance.launch_timings;
      for (var key in launches) {
        var launch = launches[key];
        var launched_op = s.task_operation[launch.task_op_id];
        if (launched_op.is_single()) {
          launched_op = launched_op.instance;
        }
        launch.prev_node.add_succ(launched_op.first_node, "launch of task to task operation/instance");
      }

      // link all op launch nodes of this node with the first node of the operation launched
      var oplaunches = instance.op_launch_timings;
      for (var key in oplaunches) {
        var launch = oplaunches[key];
        launch.prev_node.add_succ(launch.op.first_node, "launch of operation to its first node");
      }
    }
    // as a side-effect, we also set the top-level instance
    assert(s.top_level_instance != null);

    // link the completion of tasks with their parents
    // link the completion of instances with their task operation
    for (var key in s.task_operation) {
      var task_op = s.task_operation[key];
      assert(task_op.context.last_node != null);
      if (task_op.is_single()) {
        assert(!(TIME_COMPLETE_POINT in task_op.tmptimes));
        if (!task_op.is_top_level()) {
          task_op.instance.last_node.add_succ(task_op.context.last_node, "completion of single instance to context completion")
        } else {
          // top-level task does not have a parent
        }
      } else {
        // link task instance completions with their task operation completion
        assert(TIME_COMPLETE_POINT in task_op.tmptimes);
        assert(task_op.last_node != null);
        for (var key in task_op.instances) {
          var instance = task_op.instances[key];
          instance.last_node.add_succ(task_op.last_node, "completion of index instance to completion of task operation");
        }
        // link the task operation's completion with the parent's completion
        task_op.last_node.add_succ(task_op.context.last_node, "index task operation completion with context completion");
      }
    }

    // link parents that wait on subtask to finish
    for (var key in s.task_instance) {
      var instance = s.task_instance[key];
      var waits = instance.future_waits;
      for (var key in waits) {
        var wait = waits[key];
        assert("next_node" in wait);
        assert(wait.next_node != null);
        for (var key in wait.wait_on.instances) {
          var child = wait.wait_on.instances[key];
          child.last_exec_node.add_succ(wait.next_node, "waiting on subtask to finish");
        }
      }
    }

    // link dependency analysis nodes for tasks in the same context (must occur in program order)
    {
      // find all dependency analysis nodes
      var dep_analyses = [];
      for (var key in s.task_operation) {
        var top = s.task_operation[key];
        if (TIME_DEPENDENCE_ANALYSIS in top.stage_nodes) {
          assert(top.stage_nodes[TIME_DEPENDENCE_ANALYSIS].length == 1);
          dep_analyses.push(top.stage_nodes[TIME_DEPENDENCE_ANALYSIS][0]);
        }
      }
      // group by context
      var contexts = [];
      for (var key in dep_analyses) {
        var da = dep_analyses[key];
        var top = da.kind.task_operation;
        var context_id = top.context.id;
        if (context_id in contexts) {
          contexts[context_id].push(da);
        } else {
          contexts[context_id] = [da];
        }
      }
      // sort by start time and link sequentially
      for (var key in contexts) {
        var context = contexts[key];
        context.sort(function(a,b){return a.begin - b.begin})
        var prev = null;
        for (var key in context) {
          var cur = context[key];
          if (prev != null) {
            prev.add_succ(cur);
          }
          prev = cur;
        }
      }
    }

    // link based on mapping dependencies
    for (var key in s.mapping_dependence) {
      var dep = s.mapping_dependence[key];
      var prev = logprocess.get_timed_operation(dep.previous_id)
      var prev_mapping = prev.stage_nodes[TIME_MAPPING]
      var next = logprocess.get_timed_operation(dep.next_id)
      var next_mapping = next.stage_nodes[TIME_MAPPING]

      if (prev instanceof Operation && prev.kind == "deletion" ||
        next instanceof Operation && next.kind == "deletion") {
        // for now we ignore deletions, they don't affect performance
      } else {
        if (prev_mapping == null && prev instanceof TaskOperation) {
          prev_mapping = [];
          for (var key in prev.instances) {
            var mappings = prev.instances[key].stage_nodes[TIME_MAPPING];
            if (mappings.lenght == 1) {
              // avoid copy in common case
              prev_mapping.push(mappings[0]);
            } else {
              prev_mapping = prev_mapping.concat(mappings)
            }
          }
        }
        if (next_mapping == null && next instanceof TaskOperation) {
          next_mapping = next.stage_nodes[TIME_PREMAPPING];
        }
        if (prev_mapping == null || next_mapping == null) {
          error("unexpected mapping dependency", "Did not understand the mapping dependence between " + prev.toString() + " and " + next.toString());
        } else {
          for (var key in prev_mapping) {
            var prev_node = prev_mapping[key];
            for (var key in next_mapping) {
              var next_node = next_mapping[key];
              prev_node.add_succ(next_node);
            }
          }
        }
      }
    }

    // link close/deletions with completion of their context
    for (var key in s.operation) {
      var op = s.operation[key];
      if ([DELETION_OPERATION, CLOSE_OPERATION].indexOf(op.kind) != -1) {
        op.last_node.add_succ(op.context.last_node, "close/deletion to completion of context");
      }
    }

    // process event-operation relationships
    for (var key in s.operation_events) {
      var val = s.operation_events[key];
      var op = logprocess.get_timed_operation(val.unique_op_id);
      if ("begin_event" in val) {
        op.begin_event = val.begin_event;
        val.begin_event.belongs_to.push(op);
      }
      if ("end_event" in val) {
        op.end_event = val.end_event;
        val.end_event.belongs_to.push(op);
      }
    }

    // link nodes based on events
    [s.task_instance, s.task_operation, s.operation, s.llcopies].map(function(ops) {
      for (var key in ops) {
        var op = ops[key];
        var first_node = op.first_node;
        if (op instanceof TaskInstance) {
          first_node = op.first_exec_node;
        }
        if ("begin_event" in op) {
          var preds = pred_nodes_of_event(op.begin_event);
          for (var key in preds) {
            var pred = preds[key];
            pred.add_succ(first_node, "link based on begin_event of " + op.toString());
          }
        }
        if ("end_event" in op) {
          var succs = succ_nodes_of_event(op.end_event);
          for (var key in succs) {
            var succ = succs[key];
            op.last_node.add_succ(succ, "link based on end_event of " + op.toString());
          }
        }
      }
    })

    // link inline-waits correctly with what is being waited on
    for (var key in s.task_instance) {
      var instance = s.task_instance[key];
      for (var key in instance.inline_waits) {
        var wait = instance.inline_waits[key];
        assert("next_node" in wait);
        assert("event" in wait);
        var waiting_ons = pred_nodes_of_event(wait.event);
        for (var key in waiting_ons) {
          var waiting_on = waiting_ons[key];
          if (waiting_on.kind instanceof OperationNodeKind) {
            // if we are waiting on ourselves, don't record a dependency
            var context = waiting_on.kind.operation.context;
            if (context.id == instance.id && waiting_on.succs.length > 0) {
              continue;
            }
          }
          waiting_on.add_succ(wait.next_node, "inline wait link");
        }
      }
    }

    s.start_node = s.top_level_instance.first_node;
    s.end_node = s.top_level_instance.last_node;
  };

  function already_connected(from, to) {
    print(from.toString() + " === " + to.toString)
    for (var key in from.succs) {
      var mid = from.succs[key];
      if (mid.succs.indexOf(to) != -1) {
        return true;
      }
    }
    return false;
  }

  me.check_consistency = function() {
    for (var key in s.task_instance) {
      var instance = s.task_instance[key]
      assert("begin_event" in instance);
      assert("end_event" in instance);
    }
    for (var key in s.task_operation) {
      var operation = s.task_operation[key]
      // TODO: enable this check again
      //assert("begin_event" in operation);
      //assert("end_event" in operation);
    }
    for (var key in s.operation) {
      var operation = s.operation[key]
      // TODO: enable this check again
      //assert("begin_event" in operation);
      //assert("end_event" in operation);
    }
    for (var key in s.events) {
      var val = s.events[key];
    }

    // see if nodes on a given processor are overlapping
    var node_per_proc = {}
    s.graph.foreach(function(node) {
      if (!(node.processor.id in node_per_proc)) {
        node_per_proc[node.processor.id] = [];
      }
      node_per_proc[node.processor.id].push(node)
    })
    for (var key in node_per_proc) {
      if (key < 0) continue; // ignore copies
      var nodes = node_per_proc[key];
      nodes.sort(util.sort_by_begin_then_end);
      for (var i = 0; i < nodes.length-1; i++) {
        var n1 = nodes[i];
        var n2 = nodes[i+1];
        // TODO: right now we only output a warning in the console, no assertion
        if (n1.end > n2.begin) {
          error("overlapping on processor", "overlapping on processor " + s.processor[key].kind + " " + key + ":",
            "  " + n1.toString() + " (" + n1.begin + " to " + n1.end + ")",
            "  " + n2.toString() + " (" + n2.begin + " to " + n2.end + ")")
        }
      }
    }

    // no cycles
    if (false) { // disabled for performance
      var cycles = graph.scc(s.graph.get_node_array()).filter(function(d) {return d.length > 1});
      if (cycles.length > 0) {
        assert(false);
      }
    }

    s.graph.foreach(function(node) {
      // check for empty preds/succs
      if (node.preds.length == 0 && node.id != s.start_node.id && !(node.kind instanceof VisualizationOnlyNodeKind)) {
        error("missing predecessors", "No preds: " + node.dotlabel);
      }
      if (node.succs.length == 0 && node.id != s.end_node.id && !(node.kind instanceof VisualizationOnlyNodeKind)) {
        error("missing successors", "No succs: " + node.dotlabel);
      }
      // check that there are no links "back in time"
      for (var key in node.succs) {
        var succ = node.succs[key];
        if (node.end > succ.begin) {
          error("edge between nodes back in time", "link that goes back in time:",
            node.dotlabel + " (" + node.begin + " - " + node.end + ")",
            succ.dotlabel + " (" + succ.begin + " - " + succ.end + ")",
            "reason for link: " + get_node_link_origin(node, succ));
        }
      }
    })
  }

  /**
    * Returns a list of Nodes
    * that are direct predecessors of this event (direct w.r.t. nodes, not events).
    */
  function pred_nodes_of_event(ev, helper) {
    var res = helper || [];
    // add all nodes that have this event as their end-event
    for (var key in ev.belongs_to) {
      var belongs_to = ev.belongs_to[key];
      if (belongs_to.end_event == ev) {
        assert("last_node" in belongs_to);
        res.push(belongs_to.last_node);
      } else if (belongs_to.begin_event == ev) {
        // add all predecessors of the first node
        var first_node = belongs_to.first_node;
        for (var key in first_node.preds) {
          res.push(first_node.preds[key]);
        }
      } else {
        assert(false);
      }
    }
    // go through predecessors
    for (var key in ev.preds) {
      var pred = ev.preds[key];
      pred_nodes_of_event(pred, res);
    }
    return res;
  }

  /**
    * Returns a list of Nodes
    * that are direct successors of this event (direct w.r.t. nodes, not events).
    */
  function succ_nodes_of_event(ev, helper) {
    var res = helper || [];
    // add all nodes that have this event as their begin-event
    for (var key in ev.belongs_to) {
      var belongs_to = ev.belongs_to[key];
      if (belongs_to.begin_event == ev) {
        assert("first_node" in belongs_to);
        var first_node = belongs_to.first_node;
        if (belongs_to instanceof TaskInstance) {
          first_node = belongs_to.first_exec_node;
        }
        res.push(first_node);
      } else if (belongs_to.end_event == ev) {
        // add all successors of the last node
        var last_node = belongs_to.last_node;
        for (var key in last_node.succs) {
          res.push(last_node.succs[key]);
        }
      } else {
        assert(false);
      }
    }
    // go through successors
    for (var key in ev.succs) {
      var succ = ev.succs[key];
      succ_nodes_of_event(succ, res);
    }
    return res;
  }

  function add_node_pipeline_stage(op, stage) {
    if (stage in op.tmptimes) {
      var endstage = "end_" + stage;
      assert(endstage in op.tmptimes);
      var n = task_instance_node(op, stage, op.tmptimes[stage].time, op.tmptimes[endstage].time, op.tmptimes[stage].processor);
      op.tmptimes[stage].node = n;
      return n;
    }
    return null;
  }

  /** Create a node for a given stage of a task instance (with specified begin/end times). */
  function task_instance_node(instance, stage, begin, end, processor) {
    var kind = new TaskInstanceNodeKind(instance, stage);
    var node = new Node(begin, end, stage, task_instance_categories(instance, stage), kind, processor);
    s.graph.add_node(node);
    return node;
  }

  function visualization_only_node(name, begin, end, processor) {
    var kind = new VisualizationOnlyNodeKind(name);
    var c = new Category("__" + name, name);
    var categories = {
      "task": c,
      "kind": c,
    };
    var node = new Node(begin, end, name, categories, kind, processor);
    s.graph.add_node(node);
    return node;
  }

  /** Create a node for a given stage of a (non-task) operation. */
  function operation_node(operation, stage) {
    var kind = new OperationNodeKind(operation, stage);
    var endstage = "end_" + stage;
    var categories = {
      "task": task_instance_categories(operation.context, null).task,
      "kind": new Category(stage, stage),
    };
    var node = new Node(operation.tmptimes[stage].time, operation.tmptimes[endstage].time, stage, categories, kind, operation.tmptimes[stage].processor);
    s.graph.add_node(node);
    return node;
  }

  /** Create a node for a given stage of a task operation. */
  function task_operation_node(operation, stage, manual_begin, manual_end) {
    var kind = new TaskOperationNodeKind(operation, stage);
    var endstage = "end_" + stage;
    var categories = {
      "task": new Category(operation.rawdata.task.task_id, operation.rawdata.task.name),
      "kind": new Category(stage, stage),
    };
    var begin = manual_begin || operation.tmptimes[stage].time;
    var end = manual_end || operation.tmptimes[endstage].time;
    var node = new Node(begin, end, stage, categories, kind, operation.tmptimes[stage].processor);
    s.graph.add_node(node);
    return node;
  }

  function slicing_node(procedure, manual_begin, manual_end) {
    var stage = TIME_SLICING;
    if (procedure instanceof TaskOperation) {
      return task_operation_node(procedure, stage, manual_begin, manual_end)
    }
    var kind = new SliceNodeKind(procedure, stage);
    var endstage = "end_" + stage;
    var categories = {
      "task": new Category(procedure.task_operation.rawdata.task.task_id, procedure.task_operation.rawdata.task.name),
      "kind": new Category(stage, stage),
    };
    var begin = manual_begin || procedure.tmptimes[stage].time;
    var end = manual_end || procedure.tmptimes[endstage].time;
    var node = new Node(begin, end, stage, categories, kind, procedure.tmptimes[stage].processor);
    s.graph.add_node(node);
    return node;
  }

  /** Create a node for a given lowlevel copy. */
  function lowlevel_copy_node(llcopy) {
    var kind = new LowlevelCopyNodeKind(llcopy);
    var c = new Category("__lowlevel_copy__", "low-level copy");
    var categories = {
      "task": c,
      "kind": c,
    };
    var processor = {
      id: -llcopy.line_id,
      kind: "Low-level copies"
    }; // no processor associated with copies
    var node = new Node(llcopy.time_begin, llcopy.time_end, "copy", categories, kind, processor);
    s.graph.add_node(node);
    return node;
  }

  /** The categories for a task instance. */
  function task_instance_categories(instance, stage) {
    return {
      "task": new Category(instance.operation.rawdata.task.task_id, instance.operation.rawdata.task.name),
      "kind": new Category(stage, stage),
    };
  }

  return me;
}());
