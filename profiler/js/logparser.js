
/**
 * Code to parse a Legion log file.
 */

"use strict";
var logparser = (function () {
  var me = {};

  me.processor = [];
  me.memory = [];
  me.proc_mem_affinity = [];
  me.mem_mem_affinity = [];
  me.task_collection = [];
  me.task_variant = [];
  me.top_level_task = [];
  me.task_operation = [];
  me.operation = [];
  me.task_instance_variant = [];
  me.index_slice = [];
  me.slice_slice = [];
  me.point_point = [];
  me.slice_point = [];
  me.lowlevel_copy = [];
  me.operation_timing = [];
  me.event_timing = [];
  me.future_wait = [];
  me.inline_wait = [];
  me.top_index_space = [];
  me.index_partition = [];
  me.index_subspace = [];
  me.field_space = [];
  me.field_creation = [];
  me.top_region = [];
  me.logical_requirement = [];
  me.requirement_fields = [];
  me.mapping_dependence = [];
  me.task_instance_requirement = [];
  me.operation_events = [];
  me.event_dependency = [];
  me.physical_instance = [];
  me.physical_user = [];
  me.nodeinfo = [];

  // set by logprocess2.process
  me.top_level_instance = null;

  me.min_time = Number.MAX_VALUE;
  me.max_time = -1;

  me.parse = function (log) {
    var lines = log.match(/[^\r\n]+/g);
    var n = 0;
    for (var idx in lines) {
      var line = lines[idx];
      var split = line.match(/\[[0-9]+ - [0-9a-f]+\] \{\w+\}\{legion_logging\}: ([\w_]+): (.*)/);
      if (split != null) {
        n++;
        var kind = split[1];
        var message = JSON.parse(split[2]);
        message.type = kind;
        var keybased = ["operation", "processor", "memory", "task_collection", "task_operation", "operation", "nodeinfo"];
        
        // record min/max time (i.e. start/end of application)
        if ("time" in message) {
          var t = message.time;
          assert(t > 0);
          if (t < me.min_time) me.min_time = t;
          if (t > me.max_time) me.max_time = t;
        }

        // store element with it's ID as index
        if (keybased.indexOf(kind) != -1) {
          var key = null;
          for (key in message) {
            if (key == "id" || key.indexOf("_id") != -1) break;
            if (kind == "nodeinfo") {
              key = "address_space";
              break;
            }
          }
          assert(key != null);
          me[kind][message[key]] = message;
        } else {
          me[kind].push(message);
        }
      }
    }
    if (n == 0) {
      stat.show_error("Did not find any legion_logging log messages (did you compile with -DLEGION_LOGGING?)")
      return false;
    }
    return true;
  };

  me.all_messages = function () {
    var all = [];
    var process = function (arr) {
      for (var key in arr) {
        all.push(arr[key]);
      }
    }
    process(me.processor);
    process(me.memory);
    process(me.proc_mem_affinity);
    process(me.mem_mem_affinity);
    process(me.task_collection);
    process(me.task_variant);
    process(me.top_level_task);
    process(me.task_operation);
    process(me.operation);
    process(me.task_instance_variant);
    process(me.index_slice);
    process(me.slice_slice);
    process(me.point_point);
    process(me.slice_point);
    process(me.lowlevel_copy);
    process(me.operation_timing);
    process(me.event_timing);
    process(me.future_wait);
    process(me.inline_wait);
    process(me.top_index_space);
    process(me.index_partition);
    process(me.index_subspace);
    process(me.field_space);
    process(me.field_creation);
    process(me.top_region);
    process(me.logical_requirement);
    process(me.requirement_fields);
    process(me.mapping_dependence);
    process(me.task_instance_requirement);
    process(me.operation_events);
    process(me.event_dependency);
    process(me.physical_instance);
    process(me.physical_user);
    process(me.nodeinfo);
    return all;
  };

  return me;
}());
