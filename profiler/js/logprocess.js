
/**
 * Code to process the parsed information from a log file.
 * Resolves id's and adds timing information to the right objects. Also
 * builds the event graph based on event dependencies (but does not
 * associate events with operations)
 */

"use strict";
var logprocess = (function () {
  var me = {};
  var s = logparser;

  me.process = function () {
    // parse processor kinds
    for (var key in s.processor) {
      var p = s.processor[key];
      p.kind = parse_processor_kind(p.kind);
      assert(p.address_space in s.nodeinfo);
      p.init_time = s.nodeinfo[p.address_space].init_time;
    }

    // parse memory id's
    for (var key in s.memory) {
      s.memory[key].kind = parse_memory_kind(s.memory[key].kind);
    }

    // resolve processors
    // make time relative to init_time
    // init events
    var all = s.all_messages();
    s.events = {};
    for (var key in all) {
      var val = all[key];
      if ("processor" in val) {
        var id = val.processor;
        if (!(id in s.processor)) {
          assert(val.type == "event_timing")
          parse_ev_timing_kind(val.kind); // should not fail
        } else {
          val.processor = s.processor[id];
        }
      }
      if ("time" in val) {
        if ("address_space" in val) {
          var init_time = s.nodeinfo[val.address_space].init_time;
        } else {
          var init_time = val.processor.init_time;
        }
        // time is measured in nano-seconds from the start of the runtime (or profiling, at least)
        assert(init_time > 0);
        val.time -= init_time;
        assert(val.time > 0);
      }
      ["begin_event", "end_event", "event", "event1", "event2"].map(function (event_name) {
        if (event_name + "_id" in val) {
          var id = val[event_name + "_id"];
          var gen = val[event_name + "_gen"];
          assert(id !== null && gen !== null);
          // TODO: we should not see NO_EVENTs here
          if (id != 0 && gen != 0) {
            var ev = new Event(id, gen);
            if (ev in s.events) {
              ev = s.events[ev]; // make sure we only use one object per event
            } else {
              s.events[ev] = ev;
            }
            val[event_name] = ev;
          } else {
            // at least the NO_EVENTs don't show up in random places
            assert(val.type == "inline_wait" || val.type == "operation_events" || val.type == "event_dependency")
          }
        }
      })
    }

    // process event dependencies
    for (var key in s.event_dependency) {
      var val = s.event_dependency[key];
      if (val.event1 == null || val.event2 == null) {
        error("no_event in log_event_dependency", "Found NO_EVENT in event dependency: " + val.event1 + " -> " + val.event2)
        continue;
      }
      val.event1.add_succ(val.event2);
    }

    s.task_instance = []
    // resolve task_id
    for (var key in s.task_operation) {
      var to = s.task_operation[key];
      assert(to.task_id in s.task_collection);
      to.task = s.task_collection[to.task_id];

      // build correct instance
      s.task_operation[key] = new TaskOperation(s.task_operation[key]);

      // build single launch instance
      if (to.is_individual) {
        var t = new TaskInstance(s.task_operation[key], null/* slice */, to.unique_op_id, null, to.processor);
        s.task_instance[t.id] = t;
      }
    }

    // build index task instances (build maps for operation -> slice(s) -> point(s) first)
    var id2taskop = [];
    var id2point = [];
    var oldid2newid = [];
    s.slices = {};
    var build_map = function (map, indirection, old_key, new_key, slices) {
      for (var key in indirection) {
        var val = indirection[key];
        var id = val[new_key];
        var parent_id = val[old_key];
        assert(parent_id in map);
        if (slices) {
          assert(parent_id in s.slices, "Expected " + parent_id + " to occur in s.slices.");
          var parent_slice = s.slices[parent_id];
          s.slices[id] = new Slice(parent_slice, parent_slice.task_operation, id);
        }
        map[id] = map[parent_id];
      }
    };
    var init_map = function (map, indirection, key, init) {
      for (var k in indirection) {
        var val = indirection[k];
        var id = val[key];
        var initval = init(val);
        if (initval instanceof TaskOperation) {
          s.slices[id] = new Slice(initval, initval, id);
        }
        map[val[key]] = initval;
      }
    };
    init_map(id2taskop, s.index_slice, "slice_id", function (v) {
      assert(v.index_id in s.task_operation);
      return s.task_operation[v.index_id];
    });
    build_map(id2taskop, s.slice_slice, "slice_parent", "slice_subslice", true);
    build_map(id2taskop, s.slice_point, "slice_id", "point_id", true);
    build_map(id2taskop, s.point_point, "orig_point", "new_point", true);
    init_map(id2point, s.slice_point, "point_id", function (v) {
      return new Point(v.dim, v.d0, v.d1, v.d2);
    });
    build_map(id2point, s.point_point, "orig_point", "new_point", false);
    init_map(oldid2newid, s.slice_point, "point_id", function (v) {
      return v;
    });
    build_map(oldid2newid, s.point_point, "new_point", "orig_point", false);

    for (var key in s.slice_point) {
      var val = s.slice_point[key];
      var p = id2point[val.point_id];
      assert(p instanceof Point);
      var op = id2taskop[val.point_id];
      assert(op instanceof TaskOperation);
      assert(val.point_id in s.slices);
      var slice = s.slices[val.point_id];
      var t = new TaskInstance(op, slice, oldid2newid[val.point_id].point_id, p, oldid2newid[val.point_id].processor);
      s.task_instance[t.id] = t;
    }

    // operations
    for (var key in s.operation) {
      var val = s.operation[key];
      val.kind = window[val.kind];
      assert(val.kind != null);
      s.operation[key] = new Operation(val);
    }

    // add timing information to tasks
    var timings_only_tmp_list = [];
    for (var key in s.operation_timing) {
      var val = s.operation_timing[key];
      val.kind = parse_op_timing_kind(val.kind);
      if (val.kind == TIME_SCHEDULING || val.kind == TIME_END_SCHEDULING) continue;
      if (val.kind == TIME_WINDOW_WAIT || val.kind == TIME_END_WINDOW_WAIT) continue; // this is handeled later
      if (val.kind == TIME_GC || val.kind == TIME_POST_EXEC ||
          val.kind == TIME_END_GC || val.kind == TIME_END_POST_EXEC) {
        timings_only_tmp_list.push(val);
        continue;
      }
      var op = me.get_timed_operation(val.unique_op_id);
      assert(!(val.kind in op.tmptimes));
      op.tmptimes[val.kind] = {
        "time": val.time,
        "processor": val.processor,
      };
    }
    var begin_wait = null;
    for (var key in s.operation_timing) {
      var val = s.operation_timing[key];
      if (val.kind == TIME_WINDOW_WAIT || val.kind == TIME_END_WINDOW_WAIT) {
        assert(val.unique_op_id in s.task_instance);
        var instance = s.task_instance[val.unique_op_id];
        if (val.kind == TIME_WINDOW_WAIT) {
          assert(begin_wait == null);
          begin_wait = val.time;
        } else {
          assert(begin_wait != null);
          var end_wait = val.time;
          assert(end_wait > begin_wait);
          instance.window_waits.push({
            begin: begin_wait,
            end: end_wait,
          })
          begin_wait = null;
        }
      }
    }
    assert(begin_wait == null);

    // go through sorted tmp timings only
    var timings_only_tmp = {};
    s.timings_only = [];
    timings_only_tmp_list = timings_only_tmp_list.sort(function(a,b){
      if (a.time==b.time)
        return b.kind.localeCompare(a.kind);
      return a.time - b.time;
    });
    timings_only_tmp_list.map(function(val) {
      var id = val.processor.id;
      if (id in timings_only_tmp) {
        timings_only_tmp[id].push(val);
      } else {
        timings_only_tmp[id] = [val];
      }
    })
    // merge overlapping garbage collection
    for (var pid in timings_only_tmp) {
      var tmp = timings_only_tmp[pid];
      var opened = 0; // count how many TIME_GC we have seen, that have not been closed by TIME_END_GC
      for (var i = 0; i < tmp.length; i++) {
        var val = tmp[i];
        if (val.kind == TIME_GC) {
          if (opened == 0) {
            var start = val.time;
          }
          opened++;
        } else if (val.kind == TIME_END_GC) {
          assert(opened > 0);
          opened--;
          if (opened == 0) {
            s.timings_only.push({
              begin: start,
              end: val.time,
              kind: TIME_GC,
              processor: val.processor,
            })
          }
        } else {
          // TODO: also handle post execution
        }
      }
      assert(opened == 0);
    }

    // operation launches
    for (var key in s.operation) {
      var val = s.operation[key];
      assert(val.rawdata.context in s.task_instance);
      var instance = s.task_instance[val.rawdata.context];
      val.context = instance;
      instance.op_launch_timings.push({
        begin: val.rawdata.time,
        end: val.rawdata.time,
        op_id: val.id,
        op: val,
        processor: instance.tmptimes[TIME_EXEC].processor,
        kind: "op_launch",
      });
    }

    // process low-level copies
    s.llcopies = {};
    for (var key in s.lowlevel_copy) {
      var rawdata = s.lowlevel_copy[key];
      var ll = new LowlevelCopy(rawdata);
      s.llcopies[rawdata.end_event] = ll;
      rawdata.begin_event.belongs_to.push(ll);
      rawdata.end_event.belongs_to.push(ll);
    }

    // add timing to low-level copies
    for (var key in s.event_timing) {
      var val = s.event_timing[key];
      val.kind = parse_ev_timing_kind(val.kind);
      if (!(val.event in s.llcopies)) {
        //error("missing copy", "missing copy for", val.event.toString())
        //continue;
        val.event._gen += 1;
      }
      assert(val.event in s.llcopies);
      var llcopy = s.llcopies[val.event];
      if (val.kind == TIME_COPY_INIT) llcopy.time_init = val.time;
      if (val.kind == TIME_COPY_READY) llcopy.time_ready = val.time;
      if (val.kind == TIME_COPY_BEGIN) llcopy.time_begin = val.time;
      if (val.kind == TIME_COPY_END) llcopy.time_end = val.time;
    }

    // add inline_wait and future_wait to tasks
    var process_waits = function (logmessages, task_field) {
      var tmpwait = [];
      for (var key in logmessages) {
        var val = logmessages[key];
        var id = val.context;
        val.kind = parse_wait_kind(val.kind);
        assert(id in s.task_instance);
        var op = s.task_instance[id];
        var wait = null;
        if (val.kind == WAIT_BEGIN) {
          assert(!(op.id in tmpwait));
          tmpwait[op.id] = val.time;
        } else if (val.kind == WAIT_END) {
          assert(op.id in tmpwait);
          var start = tmpwait[op.id];
          tmpwait.splice(op.id, 1);
          assert(start < val.time);
          wait = {
            "begin": start, "end": val.time,
            "processor": val.processor,
             kind: task_field,
          };
        } else {
          assert(!(op.id in tmpwait));
          assert(val.kind == WAIT_NOWAIT);
          //continue;
          wait = {
            "begin": val.time, "end": val.time,
            "processor": val.processor,
            kind: task_field,
          };
        }
        // add additional information
        if (wait != null) {
          if ("event" in val) {
            wait.event = val.event;
            val.event.meta_data.push(task_field)
          } else if ("wait_on" in val) {
            assert(val.wait_on in s.task_operation);
            wait.wait_on = s.task_operation[val.wait_on];
          } else {
            continue;
            // TODO: enable this when possible
            //assert(false)
          }
          op[task_field].push(wait);
        }
      }
    }
    process_waits(s.inline_wait, "inline_waits");
    process_waits(s.future_wait, "future_waits");

    // resolve parent context and add task launch timings
    for (var key in s.task_operation) {
      var val = s.task_operation[key];
      var context_id = val.rawdata.context;
      if (context_id in s.task_operation) {
        assert(context_id in s.task_operation);
        var context = s.task_operation[context_id];
        var processor = context.rawdata.processor;
        assert(context.is_single());
        val.context = context.instance;
      } else {
        assert(context_id in s.task_instance);
        var context = s.task_instance[context_id];
        var processor = context.processor;
        val.context = context;
      }

      if (!val.is_top_level()) { // the top-level task does not get launched
        val.context.launch_timings.push({
          begin: val.rawdata.time,
          end: val.rawdata.time,
          task_op_id: val.id,
          processor: processor,
          kind: "launch",
        });
      }
    }
  };

  me.check_consistency = function() {
    for (var key in s.task_instance) {
      var val = s.task_instance[key];
      assert(TIME_MAPPING in val.tmptimes);
      assert(TIME_END_MAPPING in val.tmptimes);
      assert(TIME_LAUNCH_POINT in val.tmptimes);
      assert(TIME_EXEC in val.tmptimes);
      assert(TIME_END_EXEC in val.tmptimes);
      if (val.is_single()) {
        assert(TIME_PREMAPPING in val.tmptimes);
        assert(TIME_END_PREMAPPING in val.tmptimes);
      }
      var begin = val.tmptimes[TIME_EXEC].time;
      var end = val.tmptimes[TIME_END_EXEC].time;
      var interrupts = val.get_all_exec_interupts();
      for (var k2 in interrupts) {
        var v2 = interrupts[k2];
        var error = v2.kind + " (" + v2.begin + "-" + v2.end + ") did not happen inside of exec stage (" + begin + "-" + end + ")"
        assert(v2.begin >= begin, error);
        assert(v2.end <= end, error);
      }
    }

    // check slice consistency
    for (var key in s.slices) {
      var slice = s.slices[key];
      assert(slice.parent != null);
      if (slice.instance == null) {
        assert(slice.slices.length > 0);
      } else {
        assert(slice.slices.length == 0);
      }
    }

    var get_keys = function (a) {
      var res = [];
      for (var key in a) res.push(key);
      return res.sort().toString();
    }

    // check slide timing consistency
    for (var key in s.task_operation) {
      var top = s.task_operation[key];
      if (top.slices.length > 0) {
        // we expect all slices "on the same level" to have the same timing information
        var first = top.slices[0];
        var keys = get_keys(first.tmptimes);
        for (var i = 1; i < top.slices.length; i++) {
          var slice = top.slices[i];
          assert(keys == get_keys(slice.tmptimes), get_keys(first.tmptimes) + " - " + get_keys(slice.tmptimes))
        }
      }
    }

    for (var key in s.llcopies) {
      var c = s.llcopies[key];
      assert(c.time_begin > 0, "Invalid start time for low-level copy ("+c.toString()+"): " + c.time_begin);
      assert(c.time_end > 0, "Invalid end time for low-level copy ("+c.toString()+"): " + c.time_end);
    }
  }

  me.get_timed_operation = function(id) {
    if (id in s.task_instance) return s.task_instance[id];
    if (id in s.operation) return s.operation[id];
    if (id in s.task_operation) return s.task_operation[id];
    if (id in s.slices) return s.slices[id];
    assert(false, "did not find operation id " + id);
  }

  function parse_memory_kind(kind) {
    if (kind == 0) return "GLOBAL";
    if (kind == 1) return "SYSTEM";
    if (kind == 2) return "PINNED";
    if (kind == 3) return "SOCKET";
    if (kind == 4) return "ZEROCOPY";
    if (kind == 5) return "GPUFRAMEBUFFER";
    if (kind == 6) return "L3CACHE";
    if (kind == 7) return "L2CACHE";
    if (kind == 8) return "L1CACHE";
    assert(false);
  }

  function parse_processor_kind(kind) {
    if (kind == 0) return "GPU";
    if (kind == 1) return "CPU";
    if (kind == 2) return "Utility CPU";
    assert(false, "invalid processor kind " + kind);
  }

  function parse_op_timing_kind(kind) {
    var kinds = {
      0: TIME_DEPENDENCE_ANALYSIS,
      1: TIME_END_DEPENDENCE_ANALYSIS,
      2: TIME_PREMAPPING,
      3: TIME_END_PREMAPPING,
      4: TIME_MAPPING,
      5: TIME_END_MAPPING,
      6: TIME_SLICING,
      7: TIME_END_SLICING,
      8: TIME_EXEC,
      9: TIME_END_EXEC,
      10: TIME_LAUNCH_POINT,
      11: TIME_RESOLVE_SPEC_POINT,
      12: TIME_COMPLETE_POINT,
      13: TIME_COMMIT_POINT,
      14: TIME_WINDOW_WAIT,
      15: TIME_END_WINDOW_WAIT,
      16: TIME_SCHEDULING,
      17: TIME_END_SCHEDULING,
      22: TIME_GC,
      23: TIME_END_GC,
      24: TIME_POST_EXEC,
      25: TIME_END_POST_EXEC,
    }
    assert(kind in kinds);
    return kinds[kind];
  }

  function parse_ev_timing_kind(kind) {
    var kinds = {
      18: TIME_COPY_INIT,
      19: TIME_COPY_READY,
      20: TIME_COPY_BEGIN,
      21: TIME_COPY_END, 
    }
    assert(kind in kinds);
    return kinds[kind];
  }

  function parse_wait_kind(kind) {
    var kinds = {
      0: WAIT_BEGIN,
      1: WAIT_END,
      2: WAIT_NOWAIT,
    }
    assert(kind in kinds, "unknown wait kind " + kind);
    return kinds[kind];
  }

  return me;
}());
