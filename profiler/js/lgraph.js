
/**
 * Legion graph algorithms.
 */

"use strict";
var lgraph = (function () {
  var me = {};

  /** Merge any two nodes n1 and n2 that are connected, of the same kind,
    * where n1.succs.length == n2.pred.length == 0 as well as
    * n1.end_time == n2.begin_time.
    */
  me.merge_nodes = function(graph) {
    assert(graph instanceof ExecGraph);

    graph.foreach(function(node) {
      for (var key in node.succs) {
        var succ = node.succs[key];
        if (node.end == succ.begin
          && node.succs.length == 1 && succ.preds.length == 1
          && node.kind.equals(succ.kind)) {

          // delete succ and relink node to all successors of succ
          node.remove_succ(succ);
          var succ_succs = succ.succs.slice(0);
          for (var key in succ_succs) {
            var succ_succ = succ_succs[key];
            succ.remove_succ(succ_succ);
            node.add_succ(succ_succ);
          }

          // make node bigger
          node.end = succ.end;

          // remove succ from graph
          graph.remove_node(succ);
        }
      }
    })
  }

  return me;
}());
