
/**
 * Common functionality for visualizations
 */

"use strict";
var common = (function () {
  var me = {};

  me.default_duration = 350;

  me.colors = [
    // got colors from here: http://www.tinygorilla.com/Easter_eggs/PallateHex.html
    "#3BB878", // Light Green
    "#5574B9", // Light Blue
    "#F26C4F", // Light Red
    "#FBAF5C", // Light Yellow Orange
    "#855FA8", // Light Violet
    "#FFF467", // Light Yellow
    "#ACD372", // Light Pea Green
    "#F26D7D", // Light Magenta Red
    "#1ABBB4", // Light Green Cyan
    "#00BFF3", // Light Cyan
    "#F06EA9", // Light Magenta
    "#438CCA", // Light Cyan Blue
    "#605CA8", // Light Blue Violet
    "#A763A8", // Light Violet Magenta
    "#F68E55", // Light Red Orange
    "#7CC576", // Light Yellow Green
  ];

  me.pastelcolors = [
    "#F9AD81", // Pastel Red
    "#F7977A", // Pastel Red Orange 
    "#FDC68A", // Pastel Yellow Orange
    "#FFF79A", // Pastel Yellow
    "#C4DF9B", // Pastel Pea Green
    "#A2D39C", // Pastel Yellow Green
    "#82CA9D", // Pastel Green
    "#7BCDC8", // Pastel Green Cyan
    "#6ECFF6", // Pastel Cyan
    "#7EA7D8", // Pastel Cyan Blue
    "#8493CA", // Pastel Blue
    "#8882BE", // Pastel Blue Violet
    "#A187BE", // Pastel Violet
    "#BC8DBF", // Pastel Violet Magenta
    "#F49AC2", // Pastel Magenta
    "#F6989D", // Pastel Magenta Red
  ];

  me.add_buttons = function (v, buttons) {
    var b = d3.select("#vis").selectAll("button")
      .data(buttons);
    
    var undo = 60;
    b.exit().remove();
    b.enter()
      .append("button")
      .text(function (d) { return d.label; })
      .attr("style", function (d,i) {
        return "position: absolute; top: " + (v.margin.top - 10) + "px; right: " + (v.margin.right + undo + i*35) + "px";
      })
      .attr("title", function (d) { return d.title })
      .on("click", function (d) { if (d instanceof Button) return d.action(); })
      .attr("id", function(d){return "button_" + d.id;})
      .attr("type", "button");
    buttons.map(function(button) {
      if (button instanceof RepeatableButton) {
        me.make_repeatable(document.getElementById("button_"+button.id), button.action, 200);
      }
    })
  }

  me.add_legend = function (v, items, category) {
    if (items.length == 0) {
      util.hide("#legend");
      return;
    }
    util.show("#legend");
    d3.select("#legend").selectAll("div.legenditem").remove();
    var legenditems = d3.select("#legend").selectAll("div.legenditem")
      .data(items, function (d) {return v.current_category() + d.name;});

    legenditems.enter()
      .append("div")
      .attr("class", "legenditem");

    d3.select("#legend")
      .style("width", (v.legend.width-20) + "px");
    
    legenditems.append("span")
      .style("background-color", function (d) { return d.color; });
    if (category != "task") {
      legenditems.append("p")
        .text(function (d) { return d.name; });
    } else {
      legenditems.append("p")
        .attr("class", "link-like")
        .attr("title", function(d) {
          var s = logparser;
          if (d.name == "low-level copy") {
            return "Jump to a low-level copy node."
          } else {
            return "Jump to a node of this task (e.g., to see statistics)"
          }
        })
        .on("click", function(d) {
          var s = logparser;
          if (d.name == "low-level copy") {
            for (var key in s.llcopies) {
              timeline.initialize_info_view(s.llcopies[key].first_node);
              return;
            }
          }
          var top = null;
          for (var key in s.task_operation) {
            if (s.task_operation[key].rawdata.task.name == d.name) {
              top = s.task_operation[key];
              break;
            }
          }
          assert(top != null);
          if (top.is_single()) {
            timeline.initialize_info_view(top.instance.first_node);
          } else {
            timeline.initialize_info_view(top.first_node);
          }
          return false;
        })
        .text(function (d) { return d.name; });
    }
  }

  me.make_repeatable = function(button, f, delay){
    // inspired by http://stackoverflow.com/questions/702165/how-do-i-implement-press-and-hold-button-javascript
    var timer_handle, timer_is_running = false;
    var update = function(){
      var first = !timer_is_running;
      timer_is_running = true;
      if(first){
        // When the function is first called, it puts an onmouseup handler on the whole document 
        // that stops the process when the mouse is released. This is important if the user moves
        // the cursor off of the button.
        document.onmouseup = function(){
          clearTimeout(timer_handle);
          document.onmouseup = null;
          timer_is_running = false;
        }
      }
      var t1 = (new Date()).getTime();
      f();
      // run asynchronously to make sure we see the update to timer_is_running
      setTimeout(function () {
        var t2 = (new Date()).getTime();
        if (timer_is_running) {
          timer_handle = setTimeout(update, Math.max(0, delay-(t2-t1)));
        }
      }, 0);
    }
    button.onmousedown = update;
  }

  return me;
}());

/** Represents a button, calling the function 'action' when clicked */
var button_id_counter = 0;
function Button(label, action, title) {
  this.id = button_id_counter++;
  this.label = label;
  this.action = action;
  this.title = title;
};

/** Represents a button, calling the function 'action' when clicked.  Holding down the mouse repeats the action. */
function RepeatableButton(label, action, title) {
  this.id = button_id_counter++;
  this.label = label;
  this.action = action;
  this.title = title;
};

/** Represents one item in a legend. */
function LegendItem(name, color) {
  this.name = name;
  this.color = color;
}

/** Represents the category of a node in a visualization, which decides
  * what color to use, and which legend item it belongs to.
  */
function Category(id, legend_label) {
  this.id = id;
  this.legend_label = legend_label;
}

/** A visualization of an execution graph. */
function ExecGraphVis(graph) {
  this.graph = graph;
  this.blocks = {};
  this.visible_blocks = [];
  this.hidden_reasons = {};
  this.node2block = {};
}
ExecGraphVis.prototype.rebuild = function() {
  this.blocks = {};
  this.node2block = {};
  var nodes = this.graph.get_node_array();
  for (var key in nodes) {
    var n = nodes[key];
    var b = new VisBlock(this, n);
    this.blocks[b.id] = b;
    this.node2block[n.id] = b;
  }
  this.visible_blocks = this.get_visible_blocks();
}
var HIDDEN_NOT_HIDDEN = 0;
var HIDDEN_TOO_SMALL = -1;
var HIDDEN_OUTOFBOUNDS = -2;
var t = true;
ExecGraphVis.prototype.get_visible_blocks = function() {
  var hr = {};
  this.hidden_reasons = hr;
  [HIDDEN_NOT_HIDDEN, HIDDEN_TOO_SMALL, HIDDEN_OUTOFBOUNDS]
    .map(function (x) { hr[x] = 0; });
  var res = [];
  var v = timeline.v;
  for (var key in this.blocks) {
    var block = this.blocks[key];
    var hidden = block.is_hidden();
    hr[hidden] += 1;
    if (hidden != HIDDEN_NOT_HIDDEN) continue;
    res.push(block);
  }
  d3.select("#stats-total-nodes").text(logparser.graph.n_nodes());
  d3.select("#stats-nodes-in-vis").text(hr[HIDDEN_NOT_HIDDEN]);
  d3.select("#stats-hidden-small").text(hr[HIDDEN_TOO_SMALL]);
  d3.select("#stats-hidden-oob").text(hr[HIDDEN_OUTOFBOUNDS]);
  return res;
}
ExecGraphVis.prototype.map = function(f) {
  var res = [];
  for (var key in this.visible_blocks) {
    res.push(f(this.visible_blocks[key]));
  }
  return res;
}
ExecGraphVis.prototype.foreach = function(f) {
  for (var key in this.visible_blocks) {
    f(this.visible_blocks[key]);
  }
}

/** A node in the visualization, corresponding to one or more real nodes. */
var __vis_block_id = 1;
function VisBlock(graph, node) {
  this.id = __vis_block_id;
  __vis_block_id++;
  this.graph = graph;
  this.node = node;
  this.kind = node.kind;
  this.processor = node.processor;
  this.category = function(v) { return node.category.call(node, v); };
  this.begin = this.node.begin;
  this.end = this.node.end;
}
/** Returns 0 if the node is not hidden, or a number to indicate the reason why it is hidden */
VisBlock.prototype.is_hidden = function() {
  var v = timeline.v;
  var range = v.xscale.range();
  var xmin = range[0] - v.margin.left;
  var xmax = range[1] + v.margin.right;
  var node = this.node;
  var xend = v.xscale(this.end);
  var xstart = v.xscale(this.begin);
  // never hide the current node
  if (node == v.current_node) {
    return HIDDEN_NOT_HIDDEN;
  }
  // nodes that are out of bounds
  if (xmin > xend || xmax < xstart) {
    return HIDDEN_OUTOFBOUNDS;
  }
  // hide nodes that are too small to see
  if (xend - xstart < v.model.hidden_eps) {
    return HIDDEN_TOO_SMALL;
  }
  return HIDDEN_NOT_HIDDEN;
}
VisBlock.prototype.hidden_reason = function() {
  var r = this.is_hidden();
  var lookup = [];
  lookup[HIDDEN_TOO_SMALL] = "too small";
  lookup[HIDDEN_OUTOFBOUNDS] = "out of bounds";
  assert(r in lookup);
  return lookup[r];
}
/** Returns a list of nodes that are predecessors of this node. */
VisBlock.prototype.get_preds = function() {
  var res = {};
  var node = this.node;
  for (var key in node.preds) {
    var pred = node.preds[key];
    res[this.graph.node2block[pred.id].id] = true;
  }
  return res;
}
/** Returns a description to be used in the visualization as title. */
VisBlock.prototype.get_title = function() {
  return this.node.toString();
}
/** Returns true if any node id appears in lookup. */
VisBlock.prototype.node_in = function(lookup) {
  if (this.node.id in lookup) return true;
  return false;
}
