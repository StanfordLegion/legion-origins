
/**
 * Utility functionality.
 */

"use strict";
var util = (function () {
  var me = {};

  /** Capitalizes the first character of a string. */
  me.capitalize = function (s) {
    return s.charAt(0).toUpperCase() + s.slice(1);
  }

  /** Converts an object into an array, by taking all its property values. */
  me.object_to_array = function (o) {
    var res = [];
    for (var key in o) {
      res.push(o[key]);
    }
    return res;
  }

  /** Converts an array into an object, using the field f as they key (defaults to 'id'). */
  me.array_to_object = function (arr, f) {
    f = f || "id";
    var res = {};
    for (var key in arr) {
      var val = arr[key];
      res[val[f]] = val;
    }
    return res;
  }

  /** Takes an array of objects with a field id, and turns it into a lookup-table
    * for that index.
    */
  me.lookup_table = function (array) {
    var res = {};
    for (var key in array) {
      var val = array[key];
      res[val.id] = val;
    }
    return res;
  }

  me.hereDoc = function (f) {
    return f.toString().
        replace(/^[^\/]+\/\*!?/, '').
        replace(/\*\/[^\/]+$/, '');
  }

  /** comparison function to sort elements by their begin, and if equal, by their end. */
  me.sort_by_begin_then_end = function(a,b) {
    if (a.begin == b.begin) return a.end - b.end;
    return a.begin - b.begin;
  }

  /** A function to add successors to some kind of node. Avoids duplicates. */
  me.add_succ = function(other) {
    assert(other != null);
    if (this.succs.indexOf(other) != -1) return;
    this.succs.push(other);
    other.preds.push(this);
  }

  /** A function to remove successors to some kind of node. */
  me.remove_succ = function(succ) {
    assert(succ != null);
    assert(this.succs.indexOf(succ) != -1);
    this.succs = this.succs.filter(function(n) { return n != succ; })
    var t = this;
    succ.preds = succ.preds.filter(function(n) { return n != t; })
  }

  /** Map over all keys in an object. */
  me.object_map = function(obj, f) {
    var res = {};
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        res[key] = f(obj[key]);
      }
    }
    return res;
  }

  /** Map over all keys in an object, resulting in an array. */
  me.object_array_map = function(obj, f) {
    var res = [];
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        res.push(f(obj[key]));
      }
    }
    return res;
  }

  me.hide = function (id) {
    $(id).addClass("h");
  }
  me.show = function (id) {
    $(id).removeClass("h");
  }

  me.execute_async = function (funcs) {
    stat.init();
    me.execute_async_noinit(funcs);
  }

  me.execute_async_noinit = function (funcs) {
    if (funcs.length == 0) return;
    stat.update_status(funcs[0].name);
    exec_async_helper(funcs);
  }

  function exec_async_helper(funcs) {
    var f = funcs[0];
    setTimeout(function(){
      var r = f.func();
      if (typeof r == "boolean" && r === false) {
        stat.finish_status_with_error();
        stat.hide();
        return;
      }
      if (funcs.length > 1) {
        stat.update_status(funcs[1].name);
        exec_async_helper(funcs.slice(1));
      } else {
        stat.finish_status();
        stat.hide();
      }
    },0);
  }

  me.as_hex = function (v) {
    return (+v).toString(16);
  }

  me.format_size = function (n) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (n == 0) return '0 Bytes';
    var i = parseInt(Math.floor(Math.log(n) / Math.log(1000)));
    return (Math.round((n / Math.pow(1000, i)) * 100)/100) + " " + sizes[i];
  }

  me.parse_svg = function (s) {
    var div= document.createElementNS('http://www.w3.org/1999/xhtml', 'div');
    div.innerHTML= '<svg xmlns="http://www.w3.org/2000/svg">'+s+'</svg>';
    var frag= document.createDocumentFragment();
    while (div.firstChild.firstChild)
      frag.appendChild(div.firstChild.firstChild);
    return frag;
  }

  me.get_param = function (param) {
    return decodeURI(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURI(param).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
  }

  me.has_param = function (param) {
    return window.location.search.match(new RegExp("^(?:.*[&\\?]" + encodeURI(param).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?).*$", "i")) !== null;
  }

  /** Returns all the unique entries in an array. */
  me.distinct = function(arr) {
    var unique = arr.filter(function(elem, pos, self) {
      return self.indexOf(elem) == pos;
    });
    return unique;
  }

  /** Returns all the unique entries in an array. */
  me.distinct_obj = function(arr) {
    var unique = [];
    for (var i = 0; i < arr.length; i++) {
      var o = arr[i];
      var found = false;
      for (var j = 0; !found && j < unique.length; j++) {
        var o2 = unique[j];
        if ($.compare(o, o2)) found = true;
      }
      if (found) continue;
      unique.push(o);
    }
    return unique;
  }

  /** Is a time inside an interval (allowing for some imprecision in measuring time). */
  me.most_inside = function (time, interval) {
    return time + TIME_ERR >= interval[0] && time - TIME_ERR <= interval[1];
  }

  return me;
}());

/** Number of time units that measurements can be off. */
var TIME_ERR = 20;

function assert(condition, message) {
    if (!condition) {
        throw message || "Assertion failed";
    }
}

/** Log some error to the console (but don't fail like an assertion). */
var show_errors = true;
function error() {
  global_error_count += 1;
  if (!show_errors) return;
  var category = arguments[0];
  if (category in global_error_categories) {
    global_error_categories[category]++;
  } else {
    global_error_categories[category] = 1;
  }
  for (var i = 1; i < arguments.length; i++) {
    if (i == 1) {
      print("ERROR: " + arguments[i]);
    }
    else {
      print("  " + arguments[i]);
    }
  }
}
var global_error_count = 0;
var global_error_categories = [];
function get_error_count() {
  return global_error_count;
}

function print() {
  return console.log.apply(console, arguments);
};

function printhtml(a) {
  d3.select("body")
    .append("p")
    .append("pre")
    .text(a);
}

/** Pairs */
function Pair(a, b) {
  this.a = a;
  this.b = b;
}
Pair.prototype.toString = function() {
  return "(" + this.a.toString() + "," + this.b.toString() + ")";
}
Pair.prototype.equals = function(o) {
  if (!(o instanceof Pair)) return false;
  return this.a.equals(o.a) && this.b.equals(o.b);
}



// taken from http://stackoverflow.com/questions/1773069/using-jquery-to-compare-two-arrays (2013-12-06)
jQuery.extend({
    compare : function (a,b) {
        var obj_str = '[object Object]',
            arr_str = '[object Array]',
            a_type  = Object.prototype.toString.apply(a),
            b_type  = Object.prototype.toString.apply(b);

            if ( a_type !== b_type) { return false; }
            else if (a_type === obj_str) {
                return $.compareObject(a,b);
            }
            else if (a_type === arr_str) {
                return $.compareArray(a,b);
            }
            return (a === b);
        }
});

jQuery.extend({
    compareArray: function (arrayA, arrayB) {
        var a,b,i,a_type,b_type;
        // References to each other?
        if (arrayA === arrayB) { return true;}

        if (arrayA.length != arrayB.length) { return false; }
        // sort modifies original array
        // (which are passed by reference to our method!)
        // so clone the arrays before sorting
        a = jQuery.extend(true, [], arrayA);
        b = jQuery.extend(true, [], arrayB);
        a.sort(); 
        b.sort();
        for (i = 0, l = a.length; i < l; i+=1) {
            a_type = Object.prototype.toString.apply(a[i]);
            b_type = Object.prototype.toString.apply(b[i]);

            if (a_type !== b_type) {
                return false;
            }

            if ($.compare(a[i],b[i]) === false) {
                return false;
            }
        }
        return true;
    }
});

jQuery.extend({
    compareObject : function(objA,objB) {

        var i,a_type,b_type;

        // Compare if they are references to each other 
        if (objA === objB) { return true;}

        if (Object.keys(objA).length !== Object.keys(objB).length) { return false;}
        for (i in objA) {
            if (objA.hasOwnProperty(i)) {
                if (typeof objB[i] === 'undefined') {
                    return false;
                }
                else {
                    a_type = Object.prototype.toString.apply(objA[i]);
                    b_type = Object.prototype.toString.apply(objB[i]);

                    if (a_type !== b_type) {
                        return false; 
                    }
                }
            }
            if ($.compare(objA[i],objB[i]) === false){
                return false;
            }
        }
        return true;
    }
});
