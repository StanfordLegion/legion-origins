
/**
 * Functionality to perform timing of operations.
 */

var timing = (function () {
  var me = {};

  var running = [];
  var timings = [];

  /** Start measuring time. */
  me.start = function (name) {
    running.push({
      start: new Date().getTime(),
      name: name,
    })
  }

  /** Stop measuring and store the value internally */
  me.stop = function (name) {
    assert(running.length > 0);
    var start = running.pop();
    var end = new Date().getTime();
    if (name != null) {
      assert(name == start.name);
    }
    timings.push({
      duration: end - start.start,
      name: start.name,
    });
    return end - start.start;
  }

  me.print = function () {
    print("Timing information:")
    for (var key in timings) {
      var t = timings[key];
      print("  " + t.name + ": " + t.duration + "ms")
    }
  }

  return me;
}());
