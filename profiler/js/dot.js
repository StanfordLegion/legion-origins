
/**
 * Output a graph as DOT to be visualized by GraphViz.
 */
 
"use strict";
var dot = (function () {
  var me = {};

  me.graph_to_dot = function(nodes, node_labels) {
    var res = "";
    var edges = "";
    res += "digraph {\n";
    res += "  node [shape=rectangle];\n\n";
    node_labels = node_labels || function (d) { return {}; }

    // expected layout of nodes (just check for first node)
    assert(nodes[0] != null);
    assert("id" in nodes[0]);
    assert("succs" in nodes[0]);
    assert("dotlabel" in nodes[0]);

    for (var key in nodes) {
      var node = nodes[key];
      for (var key in node.succs) {
        var succ = node.succs[key];
        edges += "  " + node.id + " -> " + succ.id + "\n";
      }
    }

    for (var key in nodes) {
      var n = nodes[key];
      var labels = node_labels(n);
      var labelstr = ""
      for (var key in labels) {
        labelstr += ", " + key + "=" + labels[key];
      }
      var weight = "";
      if ("weight" in n) weight = " (" + n.weight + ")";
      res += "  " + n.id + " [label=\"" + n.dotlabel + weight + " [" + n.id + "]" + "\"" + labelstr + "]\n";
    }

    res += edges;
    res += "}\n";

    return res;
  }

  me.connected_graph_to_dot = function(start) {
    var res = "";
    var edges = "";
    res += "digraph {\n";
    res += "  node [shape=rectangle];\n\n";

    // expected layout of nodes (just check for first node)
    assert(start != null);
    assert("id" in start);
    assert("succs" in start);
    assert("dotlabel" in start);

    var cur = start;
    var nodes = [];
    var worklist = [];
    var done = {};
    while (cur != null) {
      nodes.push(cur);
      done[cur] = true;
      for (var key in cur.succs) {
        var succ = cur.succs[key];
        if (!done[succ.id]) {
          worklist.push(succ);
          done[succ.id] = true;
        }
        edges += "  " + cur.id + " -> " + succ.id + "\n";
      }

      if (worklist.length == 0) {
        cur = null;
      } else {
        cur = worklist[0];
        worklist = worklist.slice(1);
      }
    }

    for (var key in nodes) {
      var n = nodes[key];
      res += "  " + n.id + " [label=\"" + n.dotlabel + "\"]\n";
    }

    res += edges;
    res += "}\n";

    return res;
  }

  me.event_graph_to_dot = function(events) {
    var res = "";
    var edges = "";
    res += "digraph {\n";
    res += "  node [shape=rectangle];\n\n";

    var nodes = [];
    for (var key in events) {
      var ev = events[key];
      for (var key in ev.belongs_to) {
        var op = ev.belongs_to[key];
        assert(op.id != null);
        nodes[op.id] = op;
      }
      for (var key in ev.succs) {
        var succ = ev.succs[key];
        edges += "  " + ev.id + " -> " + succ.id + "\n";
      }
      var meta = "";
      if (ev.meta_data.length > 0) {
        meta = " (" + ev.meta_data  + ")"
      }
      res += "  " + ev.id + " [label=\"" + ev.toString() + meta + "\", shape=circle]\n";
    }

    for (var key in nodes) {
      var n = nodes[key];
      var id = n.id;
      if (n instanceof LowlevelCopy) id = n.legionnodeid;
      res += "  " + id + " [label=\"" + n.toString() + "\"]\n";
      if ("end_event" in n) edges += "  " + id + " -> " + n.end_event.id + "\n";
      if ("begin_event" in n) edges += "  " + n.begin_event.id + " -> " + id + "\n";
    }

    res += edges;
    res += "}\n";

    return res;
  }

  return me;
}());
