
/**
 * Definitions for Legion-related objects.
 */

"use strict";

/** Counter used to give IDs to things that don't have an ID (like events or low-level copies). */
var legionnodecounter = 1;

function TaskInstance(operation, slice, id, point, processor) {
  this.id = id;
  this.operation = operation;
  this.slice = slice; // slice is null for is_single instances
  if (!this.is_single()) {
    assert(slice instanceof Slice, "Unexpected slice: " + slice);
    slice.instance = this;
  }
  this.processor = processor;
  assert(operation instanceof TaskOperation);
  if (this.operation.is_single()) {
    this.operation.instance = this;
  }
  this.operation.instances.push(this);
  // only set for single task instances (not index task launches)
  this.point = point;
  // used to store timing information before it is nicely processed and stored more semantically
  this.tmptimes = [];
  // wait timings (each element has begin/end fields)
  this.inline_waits = [];
  this.future_waits = [];
  // timing when this instance launched another task (each element has begin/end/task_op_id fields)
  this.launch_timings = [];
  // timings when this instance launched operations
  this.op_launch_timings = [];
  // all nodes
  this.nodes = [];
  this.stage_nodes = {};
  // all window waits
  this.window_waits = [];
}
TaskInstance.prototype.toString = function() {
  if (this.point != null)
    return this.operation.rawdata.task.name + " (ID " + this.id + " at " + this.point.toString() + ")";
  else
    return this.operation.rawdata.task.name + " (ID " + this.id + ")";
};
TaskInstance.prototype.is_single = function() {
  return this.operation.id == this.id;
};
TaskInstance.prototype.is_top_level = function() {
  return this.operation.id == this.operation.context.id;
};
TaskInstance.prototype.get_processor = function() {
  return this.processor;
}
/**
 * Returns a list of reasons the execution of this instance got "interrupted"
 * (or a point where something important happened).
 *
 * At the very least, each interrupt has the following fields:
 * begin/end (timing information, might be both the same for time points),
 * kind (what kind of interrupt), processor.
 * There can be additional fields with more meta-information (depending on
 * the kind).
 */
TaskInstance.prototype.get_all_exec_interupts = function() {
  var interrupts = [];
  interrupts = interrupts.concat(this.inline_waits);
  interrupts = interrupts.concat(this.future_waits);
  interrupts = interrupts.concat(this.launch_timings);
  interrupts = interrupts.concat(this.op_launch_timings);
  interrupts = interrupts.concat(this.window_waits);
  interrupts.sort(function (a,b) {
    if (a.begin == b.begin) return a.end - b.end;
    return a.begin - b.begin;
  });
  return interrupts;
}

function TaskOperation(operation) {
  this.id = operation.unique_op_id;
  this.task = operation.task;
  this.task_id = operation.task.task_id;
  this.task_name = operation.task.name;
  this.rawdata = operation;
  this.tag = operation.tag;
  // see TaskInstance for an explanation
  this.tmptimes = [];
  // list of instances
  this.instances = [];
  // list of child slices
  this.slices = [];
  // all nodes
  this.nodes = [];
  this.stage_nodes = {};
}
TaskOperation.prototype.toString = function() {
  return this.rawdata.task.name + " (Task Operation ID " + this.id + ")";
};
TaskOperation.prototype.get_processor = function () {
  return this.rawdata.processor;
}
TaskOperation.prototype.is_single = function() {
  return this.rawdata.is_individual;
};
TaskOperation.prototype.is_top_level = function() {
  return this.id == this.context.id;
};

function Point(dim, d0, d1, d2) {
  this.dim = dim;
  this.data = (function () {
    if (dim == 1) return [d0];
    if (dim == 2) return [d0, d1];
    if (dim == 3) return [d0, d1, d2];
  })()
}
Point.prototype.toString = function() {
  return "Point(" + this.data + ")";
};

function Operation(rawdata) {
  this.id = rawdata.unique_op_id;
  this.rawdata = rawdata;
  this.kind = rawdata.kind;
  // see TaskInstance for an explanation
  this.tmptimes = [];
  // to be set later
  this.context;
  // all nodes
  this.nodes = [];
  this.stage_nodes = {};
}
Operation.prototype.toString = function () {
  return this.rawdata.kind + " (ID " + this.id + ") of " + this.context.toString();
};
Operation.prototype.get_processor = function () {
  return this.rawdata.processor;
}

var MAPPING_OPERATION = "mapping";
var COPY_OPERATION = "copy";
var FENCE_OPERATION = "fence";
var DELETION_OPERATION = "deletion";
var CLOSE_OPERATION = "close";

function Event(id, gen) {
  this.id = -legionnodecounter;
  legionnodecounter += 1;
  this._id = id;
  this._gen = gen;
  this.succs = [];
  this.preds = [];
  this.belongs_to = [];
  this.meta_data = []; // strings describing what this event is about/what we know about it
}
Event.prototype.toString = function() {
  return "E(" + util.as_hex(this._id) + "," + this._gen + ")";
}
Event.prototype.equals = function(o) {
  if (!(o instanceof Event)) return false;
  return this.id == o.id && this.gen == o.gen;
}
Event.prototype.add_succ = util.add_succ;

function LowlevelCopy(rawdata) {
  this.legionnodeid = -legionnodecounter;
  legionnodecounter += 1;
  this.id = rawdata.end_event;
  this.rawdata = rawdata;
  // timing information
  this.time_init = -1;
  this.time_ready = -1;
  this.time_begin = -1;
  this.time_end = -1;
  this.begin_event = rawdata.begin_event;
  this.end_event = rawdata.end_event;
  // all nodes
  this.nodes = [];
  this.stage_nodes = {};
}
LowlevelCopy.prototype.timings = function() {
  var res = [];
  if (this.time_init != -1) res.push(this.time_init);
  if (this.time_ready != -1) res.push(this.time_ready);
  if (this.time_begin != -1) res.push(this.time_begin);
  if (this.time_end != -1) res.push(this.time_end);
  return res;
};
LowlevelCopy.prototype.toString = function() {
  return "Lowlevel copy (" + this.id.toString() + ")";
};

function Slice(parent, task_operation, id) {
  this.id = id;
  this.parent = parent;
  assert(parent instanceof TaskOperation || parent instanceof Slice, "expected Slice or TaskOperation, but got: " + parent);
  this.task_operation = task_operation;
  assert(task_operation instanceof TaskOperation, "Expected task operation, but got: " + task_operation);
  parent.slices.push(this);
  this.instance; // null or the task instance this slice belongs to
  // used to store timing information before it is nicely processed and stored more semantically
  this.tmptimes = [];
  // list of child slices
  this.slices = [];
  // all nodes
  this.nodes = [];
  this.stage_nodes = {};
}
Slice.prototype.toString = function() {
  return "Slice (ID " + this.id + ") of " + this.task_operation.toString();
};


// convention:
// - x and end_x belong together and form an interval (x does not contain "point")
// - x_point marks a time-point
var TIME_DEPENDENCE_ANALYSIS = "dependence_analysis";
var TIME_END_DEPENDENCE_ANALYSIS = "end_dependence_analysis";
var TIME_PREMAPPING = "premapping";
var TIME_END_PREMAPPING = "end_premapping";
var TIME_MAPPING = "mapping";
var TIME_END_MAPPING = "end_mapping";
var TIME_SLICING = "slicing";
var TIME_END_SLICING = "end_slicing";
var TIME_EXEC = "exec";
var TIME_END_EXEC = "end_exec";
var TIME_LAUNCH_POINT = "launch_point";
var TIME_RESOLVE_SPEC_POINT = "resolve_spec";
var TIME_COMPLETE_POINT = "complete";
var TIME_COMMIT_POINT = "commit";
var TIME_WINDOW_WAIT = "window_wait";
var TIME_END_WINDOW_WAIT = "end_window_wait";
var TIME_SCHEDULING = "scheduling";
var TIME_END_SCHEDULING = "end_scheduling";
var TIME_COPY_INIT = "copy_init";
var TIME_COPY_READY = "copy_ready";
var TIME_COPY_BEGIN = "copy_begin";
var TIME_COPY_END = "copy_end";
var TIME_GC = "gargabe_collection";
var TIME_END_GC = "end_gargabe_collection";
var TIME_POST_EXEC = "post_exec";
var TIME_END_POST_EXEC = "end_post_exec";

var WAIT_BEGIN = "wait_begin"
var WAIT_END = "wait_end"
var WAIT_NOWAIT = "wait_nowait"

// ---- definitions for graph nodes

var nodeids = 0;
var record_node_link_origin = true;
var node_link_origin = {};
function get_node_link_origin(from, to) {
  var edge = new Pair(from.id, to.id);
  return node_link_origin[edge];
}
function Node(begin, end, stage, categories, kind, processor) {
  this.id = nodeids;
  nodeids += 1;
  this.begin = begin;
  this.end = end;
  this.kind = kind;
  this.stage = stage;
  assert(this.weight() >= 0, "Expected positive weight, but got begin=" + begin + " and end=" + end + " for " + kind.toString() + ".");
  this.categories = categories;
  this.succs = [];
  this.preds = [];
  this.dotlabel = kind.toString();
  this.processor = processor;
  // set by logprocess2 to indicate the first and last node when creating
  // the execution graph
  this.first_node;
  this.last_node;
  kind.node_kind_parent.nodes.push(this);
  if (kind.stage in kind.node_kind_parent.stage_nodes) {
    kind.node_kind_parent.stage_nodes[kind.stage].push(this);
  } else {
    kind.node_kind_parent.stage_nodes[kind.stage] = [this];
  }
}
Node.prototype.category = function(v) {
  return this.categories[v.current_category()];
}
Node.prototype.toString = function() {
  return this.dotlabel;
}
Node.prototype.add_succ = function(succ, origin) {
  if (record_node_link_origin) {
    var edge = new Pair(this.id, succ.id);
    node_link_origin[edge] = origin;
  }
  util.add_succ.call(this, succ);
}
Node.prototype.remove_succ = util.remove_succ;
Node.prototype.transitive_preds = function() {
  var res = {};
  node_transitive_helper(res, "preds", this);
  return res;
}
Node.prototype.transitive_succs = function() {
  var res = {};
  node_transitive_helper(res, "succs", this);
  return res;
}
Node.prototype.weight = function () {
  return this.end - this.begin;
}
function node_transitive_helper(res, field, node) {
  var queue = node[field].slice(0);
  while (queue.length > 0) {
    var x = queue.pop();
    var t = x.id in res;
    res[x.id] = x;
    if (!t) {
      for (var key in x[field]) {
        queue.push(x[field][key]);
      }
    }
  }
}
Node.prototype.transitive_latest_preds = function() {
  var cur = this;
  var res = {};
  while (cur.preds.length > 0) {
    var latest = null;
    var latest_time = -1;
    for (var key in cur.preds) {
      var pred = cur.preds[key];
      if (pred.end > latest_time) {
        latest_time = pred.end;
        latest = pred;
      }
    }
    res[latest.id] = latest;
    cur = latest;
  }
  return res;
}

/** The execution graph. */
function ExecGraph() {
  this.nodes = {};
  this.node_count = 0;
  this.node_array = null;
}
ExecGraph.prototype.add_node = function(node) {
  assert("id" in node);
  assert(!(node.id in this.nodes));
  this.nodes[node.id] = node;
  this.node_count += 1;
  this.node_array = null;
}
ExecGraph.prototype.remove_node = function(node) {
  assert("id" in node);
  assert(node.id in this.nodes);
  delete this.nodes[node.id];
  this.node_count -= 1;
  this.node_array = null;
}
ExecGraph.prototype.n_nodes = function() {
  return this.node_count;
}
ExecGraph.prototype.n_edges = function() {
  var res = 0;
  for (var key in this.nodes) {
    var node = this.nodes[key];
    res += node.succs.length;
  }
  return res;
}
ExecGraph.prototype.get_node = function(id) {
  assert(id in this.nodes);
  return this.nodes[id];
}
ExecGraph.prototype.map = function(f) {
  var res = [];
  for (var key in this.nodes) {
    res.push(f(this.nodes[key]));
  }
  return res;
}
ExecGraph.prototype.foreach = function(f) {
  for (var key in this.nodes) {
    f(this.nodes[key]);
  }
}
ExecGraph.prototype.get_node_array = function() {
  if (this.node_array == null) {
    this.node_array = util.object_to_array(this.nodes);
  }
  return this.node_array;
}


/*
We use a kind to associate nodes with the thing that they originate from (e.g.
a pipeline stage from a task instance).  Every kind must implement the
following methods:
- equals(o): Is this kind the same as some other kind o
- toString: Description of the the kind
*/

function TaskInstanceNodeKind(instance, stage) {
  assert(instance instanceof TaskInstance);
  this.instance = instance;
  this.stage = stage;
  this.node_kind_parent = instance;
}
TaskInstanceNodeKind.prototype.toString = function() {
  return util.capitalize(this.stage) + " of " + this.instance.toString();
}
TaskInstanceNodeKind.prototype.equals = function(o) {
  if (!(o instanceof TaskInstanceNodeKind)) return false;
  return o.instance.id == this.instance.id && this.stage == o.stage;
}
function TaskOperationNodeKind(task_operation, stage) {
  assert(task_operation instanceof TaskOperation);
  this.task_operation = task_operation;
  this.stage = stage;
  this.node_kind_parent = task_operation;
}
TaskOperationNodeKind.prototype.toString = function() {
  return util.capitalize(this.stage) + " of " + this.task_operation.toString();
}
TaskOperationNodeKind.prototype.equals = function(o) {
  if (!(o instanceof TaskOperationNodeKind)) return false;
  return o.task_operation.id == this.task_operation.id && this.stage == o.stage;
}
function OperationNodeKind(operation, stage) {
  assert(operation instanceof Operation);
  this.operation = operation;
  this.stage = stage;
  this.node_kind_parent = operation;
}
OperationNodeKind.prototype.toString = function() {
  return util.capitalize(this.stage) + " of " + this.operation.toString();
}
OperationNodeKind.prototype.equals = function(o) {
  if (!(o instanceof OperationNodeKind)) return false;
  return o.operation.id == this.operation.id && this.stage == o.stage;
}
function LowlevelCopyNodeKind(lowlevel_copy) {
  assert(lowlevel_copy instanceof LowlevelCopy);
  this.lowlevel_copy = lowlevel_copy;
  this.node_kind_parent = lowlevel_copy;
  this.stage = "copy"
}
LowlevelCopyNodeKind.prototype.toString = function() {
  return this.lowlevel_copy.toString();
}
LowlevelCopyNodeKind.prototype.equals = function(o) {
  if (!(o instanceof LowlevelCopyNodeKind)) return false;
  return o.lowlevel_copy.id.equals(this.lowlevel_copy.id);
}
function SliceNodeKind(slice, stage) {
  assert(slice instanceof Slice);
  this.slice = slice;
  this.stage = stage;
  this.node_kind_parent = slice;
}
SliceNodeKind.prototype.toString = function() {
  return util.capitalize(this.stage) + " of " + this.slice.toString();
}
SliceNodeKind.prototype.equals = function(o) {
  if (!(o instanceof SliceNodeKind)) return false;
  return o.slice.id == this.slice.id && this.stage == o.stage;
}
function VisualizationOnlyNodeKind(stage) {
  this.stage = stage;
  this.node_kind_parent = meta_kind_parent[stage];
}
VisualizationOnlyNodeKind.prototype.toString = function() {
  return util.capitalize(this.stage);
}
VisualizationOnlyNodeKind.prototype.equals = function(o) {
  if (!(o instanceof VisualizationOnlyNodeKind)) return false;
  return o.stage == this.stage;
}
function MetaKindParent(kind) {
  this.kind = kind;
  // all nodes
  this.nodes = [];
  this.stage_nodes = {};
}
MetaKindParent.prototype.toString = function() {
  return util.capitalize(this.kind);
}
var meta_kind_parent = {
  gargabe_collection: new MetaKindParent("gargabe_collection"),
}
