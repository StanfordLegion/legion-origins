#!/bin/bash

## Configuration
server=sapling.stanford.edu
tmpdir=/www/htdocs/profiler
##

#if [[ "`hg id`" == *+* ]]
#then
#    echo ERROR: hg not clean
#    exit
#fi

# update configuration
./update-config.sh

# create temporary folder
ssh $server rm -rf $tmpdir/*

# copy all files
scp -r lib $server:$tmpdir
scp -r js $server:$tmpdir
scp -r img $server:$tmpdir
scp config.js $server:$tmpdir
scp index.html $server:$tmpdir

