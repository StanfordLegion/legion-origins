#!/bin/bash

#id=`hg id -i`
id=`git log -n 1 --pretty=format:%H`
date=`date "+%Y-%m-%d %H:%M (%Z)"`
sed -e "s/<hgid>/$id/g" -e "s/<date>/$date/g" <config.tpl.js >config.js

