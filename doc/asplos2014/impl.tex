
\section{\Realm Implementation}
\label{sec:impl}
There are currently two implementations of \Realm:
one for shared memory machines and
another for  clusters of both CPUs
and GPUs.  The shared-memory implementation uses 
the POSIX threads library\cite{PTHREADS} (Pthreads)
and is primarily for debugging.  The 
implementation for heterogeneous clusters uses Pthreads as 
well as the CUDA runtime library for GPUs\cite{CUDA} 
and the GASNet cluster API\cite{GASNET07}.  

The heterogeneous implementation models a cluster as
having two kinds of processors 
and the four kinds of memory discussed in Section~\ref{sec:circuit_ex}.  
Every CPU core is presented 
as a different {\tt CPU} processor and every GPU is 
a {\tt GPU} processor, matching the scheduling 
granularity available in the Pthreads and CUDA APIs,
respectively.  

%The first kind of memory is the 
%system memory that is accessible by every CPU core on 
%a given node.  The second is the framebuffer memory on 
%each GPU (and accessible only by that GPU).  The third 
%kind of memory is system memory on a node that has been 
%made accessible to the GPU(s) on that node as well as the 
%CPU cores, commonly referred to as {\em zero-copy memory}.
%The final kind of memory is the portion of system memory on 
%each node that has been registered with the GASNet runtime 
%to allow {\em remote direct memory access} (RDMA) by other 
%nodes in the cluster.  We refer to this memory 
%as {\em GASNet memory}.

Implementation of all runtime features requiring communication
rely on GASNet's {\em active messages}.  Active messages consist 
of a command and payload sent by one node to 
another without any other coordination.  Upon arrival
at a destination node, a handler routine is invoked to 
process the message\cite{vonEicken92}.

Due to space constraints, we only describe the implementation
of \Realm features that are novel technical contributions.
We cover in detail the implementation of events,
\reservations, and reduction instances.

\subsection{Events}
\label{subsec:eventimpl}
Events are created on demand and are {\em owned} by the creating node.
The space of event handles is divided across the nodes at start-up
time, allowing each node to assign handles to new events without
conflicting with another node's assignments and without inter-node
communication.  The static division of event handles also permits any
node to determine the owning node of an event without communication.

When a new event $e$ is created, the owning node allocates a 
data structure to track $e$'s state ({\em triggered} 
or {\em untriggered}) and $e$'s list of {\em waiters}:
dependent operations (e.g., copy operations and task launches) 
on the same node.  The first operation 
dependent on $e$ from a remote node $n$ allocates the same 
data structure on $n$.  An {\em event subscription} 
active message is then sent to $e$'s owner indicating that 
node $n$ should be informed when event $e$ triggers.  
Arbitrarily many dependent operations on the subscribing node 
can be added to the list of waiters without additional 
communication.  When $e$ triggers, the owner node 
notifies all local waiters and sends an {\em event trigger} 
active message to each node from which it has seen an event 
subscription to notify remote waiters.  In the case where an 
event triggers while a subscription message is in flight from 
a remote node, the owner node immediately responds with a 
trigger message upon receipt of the subscription message.

The actual triggering of an event may occur on any node.  
If it occurs on a node other than the owning node, an 
{\em event trigger} active message is sent from the triggering 
node back to the owning node, which forwards that message 
to all the other subscribed nodes.  The triggering node  
notifies its waiters and no message is sent from 
the owning node back to the triggering node.  While a remote 
trigger of an event can result in the latency of a triggering 
operation being at least two active message flight times, it 
bounds the number of active messages required per event to 
$2N-2$ where $N$ is the number of nodes monitoring the event
(which is generally a small fraction of the total number of nodes
in the machine).  
An alternative is to share the subscriber list 
so that the triggering node can notify all interested nodes 
directly.  However, such an algorithm is both more complicated 
(due to race conditions) and requires $O(N^2)$ active messages.  
Any algorithm super-linear in the node count will not 
scale well, and as we show in Section~\ref{subsec:eventmicro},
the latency of a single event trigger active message is very small. 

The data structure used to track an event cannot be freed until 
all operations on that event have been performed.  Creation and 
triggering can each happen only once, but there can be an 
arbitrary number of operations that are dependent on an event.
Furthermore, some operations may not even be requested until long after 
the event has triggered.  Other systems incorporating events 
address this issue by reference counting event 
handles\cite{Khronos:OpenCL}, but such reference counting 
adds both client and runtime overhead even when limited to a 
single node; further overhead and complexity would be expected 
for a cluster-level reference-counting approach.

Instead of attempting to free event data structures, our 
implementation aggressively recycles them.  Compared to a 
reference counting scheme our implementation requires fewer 
total event data structures and has no client/runtime overhead.  
The key observation is that one {\em generational event} data 
structure can efficiently capture the state of one untriggered 
event and a very large number (e.g., $2^{32}-1$) of already-triggered 
events.  We extend each event handle to 
include its generation number as well as the identifier for its 
generational event.  Each generational event remembers how many 
previous generations have already been triggered.  Any new operation 
that is dependent on a generation that is known to already be 
triggered can immediately be executed.  A generational event can be 
reused for a new generation as soon as the current generation 
has triggered. To create a new event, a node finds a generational 
event in the triggered state, increases the generation by one, 
and sets the generational event's state to untriggered.  As before, 
this can be done with no inter-node communication.

Nodes also maintain generational event data structures for
remote events they have observed.  These data structures 
record the most recent generation known to have triggered as well
as the generation of the most recent subscription message sent 
(if any).  The distributed nature of the system allows remote 
generational events to perform an interesting optimization.  If
a remote generational event receives a request to wait 
on a later generation than its current generation, it is safe 
to infer that all generations up to the requested generation have 
triggered, because a new generation of the event was 
already created by the event's owner.  All local waiters can 
be notified even before receiving the event trigger message 
for the current generation.

\subsection{\Reservations}
\label{subsec:reservationimpl}
Like events, \reservations are created on demand, using a space 
of handles divided across the nodes.  
\Reservation creation requires no communication
and the handle is sufficient to determine 
the creating node.  However, whereas event ownership 
is static, \reservation ownership may migrate; the creating node 
is the initial owner, but ownership can be transferred to other nodes.
Since any node may at some point own a \reservation $r$, 
all nodes use the same data structure to track the state of $r$:

\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
\item {\em owner node} - the most recently known owner of $r$.  
If the current node is the owner, this
information is always accurate.  If not, this information may 
be stale, but by induction the recorded owner is guaranteed to 
know the actual owner or the next node to query about ownership.

\item {\em \reservation status} - records whether $r$ is 
currently held; valid only on the current owner.

\item {\em local waiters} - a list of pending local acquire 
requests.  This data is always valid on all nodes.

\item {\em remote waiters} - a bitmask of which other nodes have 
pending acquire requests;  valid only on the current owner.

\item {\em local payload pointer and size} - the local node's 
copy of $r$'s payload
\end{itemize}

Each time an acquire request is made, a new event is created 
to track when the grant occurs.  The current node then
examines its copy of the \reservation data structure to determine 
if it is the owner.  If the current node is the owner and the 
\reservation isn't held, the acquire request is granted immediately 
and the event is triggered.  If the \reservation is held, the 
event is added to the list of local waiters.  Note that the 
event associated with the acquire request is the only data that 
must be stored.  If the current node isn't the owner, a 
{\em \reservation acquire} active message is sent to the most 
recently known owner.  If the receiver of an acquire request  
message is no longer the owner, it forwards the message on to 
the node it has recorded as the owner.  If the current owner's 
status shows the \reservation is currently held, the bit for 
the requesting node is set in the remote waiters bitmask.  If 
the \reservation is not held, the ownership of the \reservation 
is given to the requesting node via a {\em \reservation transfer} 
active message, which includes the bitmask of remote waiters and 
an up-to-date copy of the \reservation's payload.  The inclusion 
of the payload in the active message is the reason for the 
4KB size limit specified on payloads in Section~\ref{subsec:reservations}.

Similarly, a release request is first checked against the local 
node's \reservation state.  If the local node is not the owner, 
a {\em release} active message is sent to the most recently 
known owner, which is forwarded if necessary.  Once the release 
request is on the \reservation's current owning node, the local 
waiter list is examined.  If the list is non-empty, the \reservation 
remains in the acquired state and the first acquire grant
event is pulled off the local waiter list and triggered.  If 
instead the local waiter list is empty, the acquire state is 
changed to not-acquired, and the bitmask of remote waiters 
is examined.  If there are remote waiters, then one of them is 
chosen and the corresponding node becomes the new owner via a 
\reservation transfer active message.

The unfairness that results from a \reservation favoring local 
waiters over remote waiters is intentional.  When the contention 
on a \reservation is high (the only time the question of fairness 
is relevant), the latencies involved in transferring a \reservation 
between nodes can be the limiter on throughput.  By minimizing the number
of \reservation transfers, the throughput on the \reservation is maximized.  
This effect will be demonstrated in Section~\ref{subsec:acquiremicro}.

\subsection{Reduction Instances}
\label{subsec:reducimpl}

\Realm supports {\em reduction-only} physical region instances that differ in two 
important ways from normal instances.  First, the per-element storage in reduction-only
instances is sized to hold the ``right-hand side'' of the reduction
operation (e.g., the $v$ in $struct.field\ \text{+=}\ v$).
Second, individual reductions are applied (atomically) to the local
instance, which is then sent as a batched reduction to
the target instance.  When multiple reductions are made to the same element,
they are {\em folded} together locally, further reducing 
inter-node communication. Note the folding operation
is not always identical to the reduction operation.  For example,
if the reduction operation is exponentiation, the corresponding fold
operation is multiplication:
$$(r[i]\ \text{**=}\ a)\ \text{**=}\ b \quad \Leftrightarrow 
\quad r[i]\ \text{**=}\ (a * b)$$
The client registers all reduction and corresponding fold operations with \Realm at system
startup (this part of the interface is omitted from Figure~\ref{fig:runtimeapi}).
As mentioned in Section~\ref{subsec:phyreg}, reduction instances can be folded into other reduction instances to
build hierarchical bulk reductions matching the memory 
hierarchy.  For example, in the circuit simulation, when there are multiple
GPUs per node each GPU reduces into a local reduction instance in zero-copy memory,
which are then folded together into a reduction instance for each node, and finally 
reduced across all nodes into GASNet memory.

%
% Alex: Need to make it clear back in section on physical regions that they are close to arrays.
%
\Realm supports two classes of reduction-only instances.  
A {\em reduction fold instance} is similar
to a normal physical region in that it is implemented as an array 
indexed by the same element indices.
The difference is that each instance element is a value of the reduction's
right-hand-side type, which is often smaller than the 
region's element type (the left-hand-side type).  When the
client requests a reduction operation to a particular location, 
the supplied right-hand-side value is folded in to the corresponding 
location in the array.  Any number of reductions can be 
folded into each location.  When the reduction fold instance $p$ is reduced
back to a normal instance $r$, first $p$
is copied to the location of $r$, where  
\Realm automatically applies 
$p$ to $r$ by invoking the 
reduction function once per location via a cache-friendly 
linear sweep over the memory.

The second kind of reduction instance is a {\em reduction list instance},
where the instance is implemented as a list of reductions rather than an array.
This representation can be better if updates are sparse (see
Section~\ref{subsec:reducmicro}).  A reduction list instance keeps a log of
every reduction operation requested (the pointer location and
right-hand-side value).  When the reduction list instance $p$ is reduced back
to a normal physical region $r$, $p$ is transferred and replayed
at $r$'s location.  In cases where the list of reductions
is smaller than the number of elements in $r$
the reduction in data transferred can yield better performance.
