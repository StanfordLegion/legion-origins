
\section{Related Work}
\label{sec:related}

MPI is the current industry standard for programming 
super\-computers\cite{MPI}.  While MPI supports asynchronous
communication there is no mechanism for fully
deferred execution.  GASNet is another interface for programming
large clusters based on active messages\cite{GASNET07}.  GASNet
is part of the heterogeneous implementation of \Realm, but like
MPI does not support composition of asynchronous active messages with
other constructs.  Co-array Fortran, UPC, and Titanium
are array-based languages that implement bulk-synchronous
programming models similar to MPI that allow asynchronous
data exchange\cite{COARRAY_FORTRAN,UPC99,JV:Yel98}.  Like
MPI however, none of these languages allow for a general deferred
execution model with non-blocking tasks, communication, and synchronization.

Both POSIX threads and OpenMP\cite{OPENMP98} are used for intra-node parallel
programming on large machines, but neither support asynchronous
operations.  CUDA\cite{CUDA} and OpenCL\cite{Khronos:OpenCL} support
the composition of asynchronous kernel launches and asynchronous copies
between a host node and a single GPU.  However, the only synchronization
options available in both interfaces are blocking operations on either
a stream or work queue.  OpenCL has events similar to \Realm for
expressing ordering, but has no synchronization primitive comparable to \reservations
for expressing more relaxed properties such as atomicity.
OpenCL events are valid only within a single GPU context and cannot be used
globally.

Cilk demonstrated the power of
asynchronous function calls\cite{CILK95}.  The Cilk {\em work-first}
principle provides a compelling theoretical argument for 
interfaces such as \Realm that optimize for throughput at the potential
expense of adding additional latency to the critical 
dependence path\cite{Frigo98}.

The Threaded Abstract Machine (TAM) is a programming model designed to
make it easy to port dataflow programs onto a parallel machine\cite{CullerGSvE93}.  
Conceptually \Realm is similar to the dataflow languages TAM supports.
However, the implementation of TAM is designed for a class of much smaller machines
and therefore is based on heavier communication and threading mechanisms.
While TAM supports composable asynchronous computation and communication, its
synchronization mechanisms still require blocking operations. TinyOS\cite{PowerLock}
provides {\em power locks} which provide non-blocking requests and callbacks on lock
acquisition, but only to mediate access to hardware resources in a single-threaded
environment.

The design of coarse-grained dataflow languages such as Lucid \cite{Lucid95}
and CGD \cite{CGD09} are orthogonal to the design of \Realm.  We view
\Realm as a potential implementation target for many
higher-level programming systems, including coarse-grained
dataflow programming models.  The implementation of many dataflow languages
have features related to \Realm, but are not implemented
for distributed memory machines.  I-structures from the Id language \cite{Arvind89}
have similar semantics to \Realm events, but also have a much simpler implementation
because they are limited to shared address spaces.

Chapel\cite{Chamberlain:Chapel} and X10\cite{X1005} are high-level parallel
programming languages that support asynchronous operations.  The constructs
introduced in these languages are higher-level and have complex 
semantics.  We view \Realm as providing a target
for implementing these higher-level asynchronous operations in an efficient
manner similar to Legion\cite{Legion12}.

Physical regions in \Realm are related to
arrays from the Sequoia runtime interface\cite{Houston08} in that 
both systems support specialized operations on data because they are 
aware of the structure of the data.  Sequoia's runtime interface supports
asynchronous tasks and copies, but does not permit composition of
asynchronous operations.

The implementation of \Realm shares some similarities with large
distributed systems.  Many distributed systems implement a publish/subscribe
abstraction for supporting communication that is similar in some ways to how
we manage events and event waiters\cite{Aguilera99,Carzaniga01}.  Work has
also been done on using object-oriented languages to build event-based
distributed systems\cite{Eugster01,Harrison97,Chang91}.  In these cases
callbacks are registered to run when event operations are triggered
remotely.  Events in these systems are much heavier weight
and often carry large data payloads.  In many distributed event systems
the focus is on resiliency instead of performance\cite{Ostrowski09}.
