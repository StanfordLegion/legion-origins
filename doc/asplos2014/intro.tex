
\section{Introduction}
\label{sec:intro}

All current, widely-used programming systems for large-scale parallel
programming of supercomputers provide some variant of the {\em
bulk-synchronous} execution model\cite{Valiant90}.  Bulk-synchronous
execution partitions executions into {\em phases} of computation,
data communication, and synchronization.  While bulk-synchronous
execution eases reasoning about parallel execution, it relies on
blocking constructs that were designed for machines many orders of
magnitude smaller than today's platforms
\cite{MPI,COARRAY_FORTRAN,UPC99}. On heterogeneous
supercomputers, the large, variable, and growing latencies between
components are exposed by these blocking constructs.

The standard mechanism for masking latency is to use non-blocking {\em
asynchronous operations}, thereby enabling other useful work to be
done while the asynchronous operations complete.  
Bulk-synchronous models have gradually added asynchronous operations;
for example, MPI has non-blocking {\em send} and {\em receive}
for overlapping communication with computation \cite{MPI}.
Unfortunately, the application must still explicitly wait on a
non-blocking operation before its results can be used (or its inputs
modified), effectively synchronizing the application with the operation.
Although the programmer may choose where to place these
synchronization points, correct placement is difficult in practice,
as it may rely on input data, algorithmic decisions,
the presence of hard or soft errors, and the underlying
hardware architecture.
Inaccurate placement results in exposure to some or all of the latency
that the non-blocking operations were intended to hide.
Consequently, programs written in this style tend to have neither
predictable nor portable performance.

An alternative to a bulk-synchronous model with asynchronous
extensions is a fully asynchronous {\em deferred execution} model.  In
deferred execution, a request for an operation returns immediately to the caller,
while the actual operation is {\em deferred} until its dependencies are satisfied and
it is scheduled for execution.  The caller continues requesting
further operations, even those dependent on previously-requested operations.
This hides the latency of the operations (and any necessary communication) from
the caller, and gives the underlying runtime system the freedom to execute the operations
in any order that obeys the specified dependences between operations.
One well-known domain that typically uses a deferred execution model is
graphics rendering.  Both the OpenGL\cite{OpenGL99} and Direct3D\cite{D3D10}
graphics libraries provide an {\em immediate mode} in which the illusion of
immediacy (despite the long latency of modern graphics pipelines) is achieved
through deferred execution.

We present \Realm, a general-purpose, low-level
interface for deferred execution 
on distributed memory supercomputers, which 
provides primitives for parallelism, data movement,
and synchronization.  To express 
dependences between operations, \Realm uses a light-weight 
{\em event} primitive.  
%Events provide a mechanism
%for naming a point in the future when an asynchronous operation
%(computation, data copy, or synchronization) completes.  
Every \Realm operation $p$ returns immediately
with an event that {\em triggers} when $p$ completes.
Furthermore, every \Realm operation $p$ takes as input
an event that must trigger before $p$ can begin.
%Events can be combined into new events and an unlimited number 
%of operations can use the same event as a precondition.  
Using events, clients can construct arbitrary graphs of dependent
operations, which the runtime schedules to optimize for throughput.

Section~\ref{sec:circuit_ex} gives a motivating
example of a real-world application that benefits from  deferred 
execution.  Subsequent sections describe our contributions:

\begin{itemize} \itemsep1pt \parskip1pt \parsep1pt
\item We introduce the \Realm deferred execution interface for
parallel computation, data movement and synchronization (Section~\ref{sec:interface}).

\item To support synchronization in a deferred execution model we
introduce {\em \reservations}, a novel synchronization primitive
for a fully  asynchronous environment 
(Section~\ref{subsec:reservations}).

\item We describe a {\em physical region} system that provides structural
information about data layout necessary for deferring data movement
operations (Section~\ref{subsec:phyreg}).

\item We cover the novel features of a \Realm implementation including
the use of {\em generational event} data structures for efficiently
representing light-weight events in a distributed memory architecture
(Section~\ref{sec:impl}).

\item We report on micro-benchmarks that stress-test our implementation
of \Realm and show that performance approaches the performance of
the underlying hardware (Section~\ref{sec:micro}).

\item We demonstrate that \Realm's low-level interface can
support a deferred execution implementation of Legion \cite{Legion12}, a
high-level programming system.  Furthermore, we demonstrate that
applications using a bulk-synchronous Legion implementation
incur performance penalties between 22-135\% compared to a
Legion implementation using \Realm.
\end{itemize}

%Section~\ref{sec:related} describes related work and
%Section~\ref{sec:conclusion} concludes.

