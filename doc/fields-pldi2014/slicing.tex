
\section{Structure Slicing}
\label{sec:slicing}
In this section we give a precise treatment of structure slicing and describe
how it can be applied to identify 
task parallelism in applications.  We believe our
approach is compatible with a variety of programming models and with 
the Legion programming model in particular.
We begin by describing requirements for the underlying data model
on which we wish to add structure slicing  
(Section~\ref{subsec:model}).  We then discuss the
dynamic allocation and deallocation of fields and the resulting benefits  
(Section~\ref{subsec:alloc}).  Finally, we cover the annotation of application tasks with
field usage information and how it is used to infer non-interference
of tasks (Section~\ref{subsec:implicit}).

\subsection{Data Model}
\label{subsec:model}

As the name suggests, structure slicing assumes that there
is some available structure in the application data to slice.  In 
particular, we assume that any data that is shared between tasks in an
application is grouped into one or more {\em collections}.  There are
no requirements on the size or layout of these collections. 
%nor do we insist that collections be made up of distinct objects.  
In C or Fortran, a collection might correspond to a static or dynamically 
allocated array, or to the elements of a linked list.
In Legion, the collections are logical regions.

We require that each collection in our application be associated with a
{\em field space}, which is a set of names of all of the fields that might
exist on any object in the collection.  A field might not exist for a particular
object if the underlying programming model supports mixed collections, but the
problem of an attempted access to a non-existent field is left to the 
model---we are interested in cases where a field will not be accessed for any object
in the collection.  There is one very important constraint on field spaces.  We
require that the field spaces be {\em disjoint}---i.e., no piece of data may be accessible
through two different names in a field space.  This does not completely disallow overlaid
data structures such as C's {\tt union} type, but it does require that the entire union
be contained in a single field in the field space.

Legion's logical regions do not already record the fields of objects,
so we introduce an explicit {\tt FieldSpace} construct 
and require one to be specified as part of the creation of a logical
region.  (Existing Legion code can use a default {\tt FieldSpace} with
one large field, but will not get the benefit of structure slicing.)

\subsection{Dynamic Field Spaces}
\label{subsec:alloc}

The set of field names in a field space need not be fixed.  The most obvious reason
is to support dynamically typed languages, or even statically typed languages that
allow dynamic loading of compiled code (e.g., Java).  A less obvious case applies to
fully static languages like C and is common in scientific computing applications:
when the names of all the fields are statically known but the liveness
of the data in those fields is variable.

Consider a simple computation which computes a temporary for every
element in a collection and then performs a stencil computation
over that temporary before summing the result with another field.
The need for maintaining temporary fields is ubiquitous in scientific
computing.  In many cases both C and Fortran programmers will allocate
{\em scratch} fields in static data types that are re-used throughout
long computations like S3D's {\tt rhsf} function.

There are two problems with these scratch fields.  First, they consume
memory at all times.  Programmers often address this memory bloat by
reusing a single scratch field for several (hopefully non-overlapping)
temporary variables, at a significant cost to code maintainability.
However, reuse of scratch fields can introduce false dependencies between
two otherwise independent tasks that happen to reuse the same
scratch field.  Second, scratch fields add overhead to data transfers.
The scratch fields of a structure in C or Fortran are copied along
with all the other fields, whether they have live data or not.  For
applications such as S3D that use a large number of scratch fields and
are constrained by system bandwidth, the costs of copying unused scratch
fields can be large.

By allowing the application to add a field to the field set when it
becomes live and remove it when it becomes dead, false dependencies
can be eliminated and, if the programming model supports it,
memory footprint and transfer costs can be reduced.  In Section~\ref{sec:impl}
we describe our support for this feature in Legion.

\subsection{Field-Based Non-Interference}
\label{subsec:implicit}
To use structure slicing to identify parallelism in an application, we
assume that the application is decomposed into a set of {\em tasks}, which 
may be functions (in the case of Legion),
or loops (in the case of OpenMP or OpenACC), or any other unit of
computation understood by a programming system.  We also are
given a partial ordering of the tasks specifying the constraints on the
observable execution order.  (In a language without explicit parallelism
constructs, this will usually be a total ordering that matches program 
order.)  The goal is to identify implicit parallelism: pairs of tasks $t_1$ and $t_2$ for which
an ordering constraint was specified (i.e., $t_1 < t_2$), but the two tasks
are {\em non-interfering} (written $t_1 \# t_2$) and can therefore be executed in either
order (or concurrently) without causing a difference in the observed
state of memory after their execution.

There are a number of sufficient conditions for the non-interference
of two tasks.  Structure slicing adds a field-based non-interference condition. If
two tasks can be shown to use disjoint subsets of fields, or the same fields with
compatible privileges, then they cannot interfere
with each other and may be run in parallel.  
%As with all analyses of this form,
%it can be performed statically, dynamically, speculatively, or even some combination.

In our structure slicing extension to Legion, we make use of Legion's
existing privilege annotations on tasks (recall
Section~\ref{subsec:legion}), which specify which logical regions
(i.e.,  collections) they access and whether they perform
reads, writes, or commutative reduction operations.  By augmenting
these privilege declarations to also include the subset of the
collection's field space to which accesses will be contained, we are
able to incorporate structure slicing into the Legion runtime's
existing non-interference checks in a disciplined way that preserves
the safety guarantees shown in \cite{LegionTypes13}.  Following the
methodology of that paper, we first define a precise
non-interference test based on the actual execution of two tasks, and
then show a sound approximation of that test that can be performed
using our augmented privileges.

\makeatletter
\newcommand{\nonint}[1][]{\#\@ifmtarg{#1}{}{_{\!\!#1}}}
\makeatother

A {\em memory operation} $\epsilon$ is a triple $(l,op,v)$ where $l$ is a memory location,
 $op$ is the operation performed on $l$ (read, write, or reduce with a particular reduction
operator), and $v$ is the value (the value read, written or folded into $l$ using the named
reduction operator).  In \cite{LegionTypes13}, the non-interference of two memory operations $\epsilon_1$ and
$\epsilon_2$ is defined as follows:
$$
\begin{array}{l@{ }l}
\epsilon_1\ \nonint\ \epsilon_2 \Leftrightarrow & (op_1 = read \wedge op_2 = read)\ \vee \\
& (op_1 = reduce_{id_1} \wedge op_2 = reduce_{id_2} \wedge id_1 = id_2) \vee \\
& l_1 \not= l_2
\end{array}
$$

The first two conditions capture the non-interference of two read
operations or two reduction operations (using the same reduction
operator), and we will use those as is.  The final condition looks for
accesses to different memory locations $l_1$ and $l_2$.  If we let
$o_1$ and $o_2$ be the base address of the objects accessed by
$\epsilon_1$ and $\epsilon_2$ and let $f_1$ and $f_2$ be the names of
the fields being accessed, we can refine the notion of accessing the
same location to the access of the same field within the same object
$$
l_1 = l_2 \Leftrightarrow (o_1 = o_2) \wedge (f_1 = f_2)
$$

\noindent and then rewrite the non-interference test as
$$
\begin{array}{l@{ }l}
\epsilon_1\ \nonint\ \epsilon_2 \Leftrightarrow & (op_1 = read \wedge op_2 = read)\ \vee \\
& (op_1 = reduce_{id_1} \wedge op_2 = reduce_{id_2} \wedge id_1 = id_2)\ \vee \\
& \bm{o_1 \not= o_2\ \vee} \\
& \bm{f_1 \not= f_2}
\end{array}
$$

This splits the original different-location test into two tests: one that
identifies data parallelism from accesses to different objects, and
one that identifies task parallelism from accesses to different
fields.  

The Legion non-interference test is a (sound) approximation that works at
the level of entire regions rather than individual memory operations.
Let task $t_i$ access fields $\mbox{\em fields}_i$ of region
$r_i$ with the privilege $priv_i$.
The test for non-interference of tasks $t_1$ and $t_2$ is extended to include fields
as follows\footnote{
The non-interference test is performed in the context of $M$, the runtime mapping of logical
regions to physical instances.  In \cite{LegionTypes13}, it is shown that this approximation is
sound for any valid mapping decisions.}:

$\begin{array}{@{}l@{}l@{}}
\multicolumn{2}{@{}l}{priv_1(r_1,\bm{\mathit{fields}_1}) \nonint[M] priv_2(r_2,\bm{\mathit{fields}_2}) \Leftrightarrow} \\
\hspace*{0.5cm} & (priv_1 = \text{reads} \wedge priv_2 = \text{reads})\ \vee \\
& (priv_1 = \text{reduces}_{id_1} \wedge priv_2 = \text{reduces}_{id_2} \wedge id_1 = id_2)\ \vee \\
& (r_1 * r_2)\ \vee \\
& (M(r_1) \cap M(r_2) = \emptyset)\ \vee \\
& \bm{(\mathit{fields}_1 \cap \mathit{fields}_2 = \emptyset)}
\end{array}$

The existing Legion runtime non-interference checks (which extract
other forms of parallelism) are left unaffected, and a single new
sufficient condition is added, which checks whether the sets of fields
accessed by two tasks for a given privilege are disjoint.  
This additional check is performed
dynamically (due to our desire to support dynamic field sets), and we
discuss an efficient implementation in the next section.
Finally, using this definition, the theorems proving
the soundness of the privilege system \cite{LegionTypes13} can be extended to cover
structure slicing.   
