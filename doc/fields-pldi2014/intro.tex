\section{Introduction}
\label{sec:intro}
%Satisfying the demand for high-performance computing capacity has led modern
%parallel architectures to evolve deep memory hierarchies with heterogeneous 
%processors.  With each new architecture has come new software for extracting 
%performance on the target hardware.  Consequently, current supercomputers are 
%programmed using an eclectic mix of programming models and tools, each with 
%their own (often conflicting) assumptions.  

%To compose these programming systems, most programmers rely on a tiered 
%approach to avoid violating conflicting assumptions.  For example, many programmers
%first write MPI applications, and then use either Pthreads or 
%OpenMP within a node to extract either task  or data level 
%parallelism respectively.  However, few programmers will use both Pthreads and 
%OpenMP in the same program to avoid threads from each programming system 
%competing for time on a node's cores.  Similar statements can be made between the 
%use of CUDA/OpenCL and OpenACC for GPUs, as well as between Pthreads and 
%OpenMP on Xeon Phi devices.  As a result, many programmers pigeon-hole 
%themselves into only being able to exploit task  or data level parallelism 
%in their applications purely by their choice of programming system at each tier.  

Modern supercomputers have evolved to incorporate deep memory hierarchies
and heterogeneous processors to meet the demand for increasing performance
from the computational science community.  Accompanying each new architecture
has been new software for extracting performance on the target 
hardware\cite{CUDA,OPENMP98,Khronos:OpenCL,OpenACC,IntelMPSS}.  Consequently, 
modern supercomputers are programmed using an eclectic mix of systems, each
with different programming models.

In practice most computational scientists select purely data parallel
programming systems such as MPI and OpenMP to minimize programmer
burden.  Ideally, {\em weak scaling} of applications would make
this data parallel approach sustainable, as the input problem size can
be chosen to fill the machine and give good performance.  However, we
are now at a point where this approach to scaling fails.  A filling
problem size on many current machines would provide a very high
fidelity simulation for most applications.  Unfortunately, high fidelity simulations
necessitate excessively small time steps in order to maintain the
stability of existing explicit numerical methods\cite{Hamming86}.\footnote{This does not apply to implicit methods.  However, implicit methods 
introduce the separate problem of difficult to scale communication patterns.}
At this point {\em strong scaling} becomes necessary: the total problem size is
fixed at the highest reasonable 
fidelity and further attempts to scale on larger machines using data parallelism 
requires assigning each processor smaller units of work. This approach often leads to
one or both of the following bottlenecks:
\begin{itemize}
\item Start-up cost: the overhead of starting a computation grows to exceed its
execution time.  For example, the cost of moving data over the PCI-E bus 
to a GPU or warming-up a cache on a CPU dominates the total computation time.
\item Communication cost: As the communication-to-computation ratio 
grows with smaller per-processor problem sizes, applications become
limited by network bandwidth or latency.
\end{itemize}
There is a natural path to mitigate these problems: move more of
the computation onto a single node, as both the available memory and number
of processors per node grow with Moore's law.  Initially, this both reduces
communication-to-computation ratios and amortizes start-up costs with
larger data sizes.  However, even at the node level,
strong scaling is needed, as the number of processors
(and their compute power) will grow faster than the available memory.  
Thus, pure data parallelism
is insufficient to strong-scale an application on current architectures.
Instead, we must leverage both the data and task parallelism available within
applications to make full use of all of a machine's resources.

\begin{figure}
\begin{center}
\includegraphics[scale=0.40]{figs/Field_Slicing}
\end{center}
\caption{Across-Cell and Across-Field Phases in S3D. \label{fig:s3d_phases}}
\vspace{-0.5cm}
\end{figure}

Consider, for example, the combustion simulation S3D\cite{S3D09}, which
was one of the six applications used for acceptance testing of
Titan\cite{S3DACC12}, the number two supercomputer in the
world\cite{Top500}.  S3D models both the physics of turbulent gas
dynamics as well as the chemistry of combustion via an explicit
numerical method.  Physical space is discretized into a grid of cells
and each cell maintains more than a thousand values or {\em fields}
corresponding to different physical and chemical properties (e.g.,
temperature, pressure, etc.).  S3D contains many computational phases,
each of which is primarily a physics or chemistry phase, as shown in
Figure~\ref{fig:s3d_phases}.  Physical computation phases consist
mainly of {\em stencils} which require only a few field values from
each cell, but need the same field value from many neighboring cells.
Chemistry phases require many fields, but always from a single cell.
In every time step of S3D there routinely exists more than 100-way
parallelism between both kinds of phases using different subsets of
the more than one thousand fields, but existing implementations of S3D
ignore this task parallelism.

Many scientific computing applications share
S3D's structure:  there is a large distributed data
structure (regular grid, irregular mesh, graph) and each element within
this data structure holds many tens, hundreds, or even thousands of field
values.  Furthermore, all computations on these data structures go through
phases operating on different subsets of these fields, with task parallelism
existing between phases using {\em non-interfering} sets of fields.  
Successfully extracting this field-level task parallelism would make strong
scaling within modern supercomputing nodes possible.

In this paper we present {\em structure slicing} as an extension to existing
data parallel programming models for extracting field-level task parallelism
between phases of a computation.  Structure slicing requires each data parallel
computation to be annotated with the names of all the fields being
accessed by that computation.  Armed with this information, a programming
system can perform an analysis to extract task parallelism from the
application.  To demonstrate the benefits of structure slicing,
we describe both the interface and implementation of structure slicing
within the existing Legion programming model\cite{Legion12}.

The rest of this paper is organized as follows. In Section 
\ref{sec:background} we motivate our design by giving a concrete example
of how S3D can benefit from structure slicing.  Each of the remaining sections 
contains one of our core technical contributions:
\begin{itemize}
\item We describe the semantics of the general structure slicing extension, 
      including the need for dynamic field allocation, and give a 
      formal definition of non-interference based on fields.  We also detail
      the incorporation of structure slicing into Legion's definition of
      non-interference (Section~\ref{sec:slicing}).
\item We extend Legion's dynamic dependence analysis algorithm to
      implicitly discover field-level parallelism using structure slicing.
      We also present optimizations for making dynamic dependence analysis
      using fields more efficient (Section~\ref{sec:impl}).
\item We port the full S3D application into our extended version
      of Legion to evaluate the performance benefits of structure slicing.
%      both within a single node and across nodes.  
      We also examine the 
      performance benefits of the optimizations presented for 
      dynamic dependence analysis (Section~\ref{sec:eval}).  
\end{itemize}
Section~\ref{sec:related} describes related work and Section~\ref{sec:conclusion}
concludes.


%The demand for high-performance computing capacity is steadily increasing, and the 
%design of systems that meet this demand have been growing steadily more complicated.  A limit
%on a single processor's speed led to systems with first tens, then hundreds, and now millions
%of processors.  A limit on the ability to connect these processors led to increasingly
%hierarchical network and memory structures.  And a limit on the power consumption of these
%systems has led to heterogeneity in the types of processors with ``low-latency'' cores for
%more serial code and ``high-throughput'' cores for more efficienct handling of regular parallel
%code.  A good example is Titan\cite{Titan}, which consists of over 18,000 nodes, each of which
%contains 16 CPU cores arranged in a non-uniform memory architecture (NUMA) along with a 
%Kepler K20X GPU\cite{Kepler} with 2688 CUDA cores.

%Applications are also becoming more complicated, often incorporating different kinds of 
%computations to solve more interesting problems.  For example, S3D\cite{S3D}, one of the six
%acceptance applications for Titan, attempts to model both the physics of
%turbulent flows of gases as well as the chemistry of how those gases react with each other.

%The job of mapping these increasinly complicated applications onto these increasingly complicated
%systems falls on the programmer, ideally with a lot of help from the programming model.
%Existing parallel programming models are generally either {\em task parallel} or {\em data
%parallel}.  As the names suggest, task parallelism attempts to identify different computations
%that can be performed concurrently, while data parallelism focuses on situations where the
%same computation is being performed on elements of an array or other data structure.

%When available, data parallelism is usually exploited first.  There are several reasons for this.
%The most obvious reason is that high-performance computing applications are typically run on 
%large data sets with millions or even billions of elements, whereas even very complicated 
%applications are unlikely to have more than one hundred different tasks.  Other reasons
%are related to efficiency, both for the programmer and for the hardware.  Data parallelism can
%be expressed very succintly in programming
%models such as OpenMP\cite{OpenMP} and CUDA\cite{CUDA} or even inferred by a compiler\cite{?}.
%And although they include support for
%divergent code, today's high-throughput processors are designed for applying the same operation
%to large {\em vectors} of data.\cite{XeonPhi,Kepler}.
%Data parallelism has some limits in practice though.  Although one could theoretically distribute
%single array elements to different processors, the overhead of coordinating and synchronizing 
%all of the processors can dominate the runtime.  Additionally, computations that require
%values of nearby elements as inputs (often called {\em stencil} operations) become communication
%limited if the distribution is too fine.

%Task parallelism provides different benefits and tradeoffs.  Perhaps the largest benefit is in
%its ability to efficiently use heterogenous processors.  Some computations run better on a 
%CPU, while others run better on a GPU.  Instead of having to split each
%across the different processors (load-balancing each computation differently or, more commonly,
%running the computation entirely on the more
%suitable processor type and letting the other sit idle), task parallelism can be used to run
%CPU-friendly tasks on the CPUs while running the GPU-friendly tasks on the GPUs.  Another 
%important benefit of task parallelism is that different computations will often use different
%subsets of the application data.  For example, the {\em transport} part of S3D uses the
%current momentum and related physical constants while the {\em kinematics} part uses the 
%current molar concentrations and a large number of reaction coefficients.  High-throughput cores
%are often limited by relatively small memories and poor communication bandwidth.  By breaking up
%these operations into different tasks, the working set of a single task can be reduced, minimizing
%data transfer between the CPUs and GPUs and allowing the GPU to operate on larger blocks of 
%elements at a time.

%While some existing programming models include both task and data parallel
%constructs,\cite{OpenMP,Chapel} they do so in a ``tiered'' model in which an application is split up
%into tasks that may then use data parallel constructs.  The Legion programming model\cite{Legion}
%is attractive because of its notions of {\em locality} and {\em independence} that capture
%both task and data parallelism and its ability to handle an arbitrary hierarchy of decomposition.
%Unfortunately, although Legion supports objects that may be structures with multiple fields, it
%is not expressive enough to describe the locality and independence of different subsets of those
%structures.

%We propose adding this capability to Legion in the form of {\em structure slicing}.  By allowing
%Legion's per-task {\em privilege} declarations to name particular fields of a structure, we
%can expose more of an application's parallelism and do it in a way that maps better to any given
%machine.

%We begin with some background, briefly describing the S3D application (Section~\ref{sec:s3d}) and
%the existing Legion programming model (Section~\ref{sec:legion}).  We then describe our
%{\em structure slicing} extensions to the programming model (Section~\ref{sec:slicing}) and
%the modifications to the Legion runtime that enable efficient tracking of dependencies and
%data coherence at the granularity of fields (Section~\ref{sec:impl}).

%We evaluate the performance of the structure sliced version of S3D (Section~\ref{sec:eval})
%and conclude with a discussion of related work (Section~\ref{sec:related}).
