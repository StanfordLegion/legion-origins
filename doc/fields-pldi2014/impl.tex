
\section{Structure Slicing Implementation}
\label{sec:impl}
We now describe an implementation of structure
slicing in Legion.  Structure
slicing collections are implemented using Legion's logical
regions.  While logical regions were previously created using
static compound types, dynamic logical region creation with
structure slicing uses field spaces to specify the available
fields on a logical region.  Legion supports
the dynamic allocation and deallocation of fields via the level 
of indirection that exists between Legion's logical regions and physical 
instances.  The dynamic allocation of a field $f$
on a logical region $r$ guarantees that $f$ is
available on future physical instances of $r$.  
Similarly, the destruction of a field $f$ on $r$
ensures that the field need not be allocated
as a part of future physical instances.

We also extend Legion tasks to support the necessary information
for structure slicing.  Previously, Legion tasks statically declared
the logical regions that they would access along with
the privileges on each region\cite{Legion12}.
In our structure slicing version of Legion, instead of specifying
privileges on whole logical regions, tasks must specify privileges
on individual fields within regions.  
%This guarantees
%that our extension to the Legion programming model complies with
%the requirements that any structure slicing model specify the
%fields and privileges accessed by any computation.

We extend the previous implementation of the Legion runtime
described in \cite{Legion12} to support the necessary operations for the
creation and deletion of field spaces, creation and deletion
of fields, and launching of tasks with privileges on individual
fields.  We describe the interesting aspects of our system.
Section~\ref{subsec:noninter} covers the details of
the non-interference tests necessary to allow a Legion implementation
to extract task parallelism from the information provided by
structure slicing.  Section~\ref{subsec:accel} introduces
optimizations for mitigating the extra overhead
of the field non-interference tests.

\subsection{Dynamic Non-Interference Tests}
\label{subsec:noninter}

\begin{figure}
\centering
\includegraphics[scale=0.45]{figs/field_disjoint}
\caption{Example Legion Non-Interference Test\label{fig:noninter}}
\label{fig:nonintex}
\end{figure}

%
% ALEX
% this isn't quite right.  It's not all previously issued tasks that must be
% checked because of the hierarchical scheduling --- this paragraph makes the problem
% sound worse than it is and makes Legion sound possibly unrealistic.  But we don't
% want to explain hierarchical scheduling.  I don't see an easy fix besides leaving it
% alone.
%
The primary challenge in extending a Legion implementation to
incorporate structure slicing information is performing dynamic
non-interference tests at runtime to extract implicit parallelism 
in a way that is unobtrusive to application performance. 
In Legion, subtasks launched within a parent task are issued
in program order to the runtime system.  For each subtask $t$ the
runtime must perform non-interference tests between $t$ and any
unfinished subtasks launched within the same parent task\cite{Legion12}.  
(Note only tasks launched within the same parent task must be 
considered because of Legion's provably safe hierarchical
scheduling algorithm\cite{LegionTypes13}.)  A poor implementation
could put this sequence of non-interference tests on an application's
critical path, so it is important that the non-interference
tests be implemented efficiently.

%The existing Legion implementation
%hides the cost of dynamic non-interference tests using a deferred
%execution model\cite{Legion12}.  However, structure slicing adds 
%an additional dimension to the non-interference test which, if 
%implemented poorly, could result in significant runtime overhead.  
%It is therefore imperative that care be taken to minimize the 
%additional testing overhead of structure slicing.

With the addition of structure slicing to Legion, a pair of tasks 
can be proven to be non-interfering in one of three dimensions:
\begin{itemize}
\item independent sets of fields, 

\item independent sets of regions, 

\item or non-interfering privileges.  
\end{itemize}
Implementing non-interference testing requires fixing
an order on these three tests.  Since non-interference is 
disjunctive, proving non-interference on the first dimension allows
testing the remaining dimensions to be skipped.  Therefore
the order chosen can have a significant impact on performance.

\begin{figure*}
\centering

\subfigure[Field Tree for 4-bit masks.]{
\includegraphics[scale=0.36]{figs/field_tree}
\label{fig:field_tree}
} \qquad
\subfigure[Insertion of $t_8$ with mask $0100$.]{
\includegraphics[scale=0.36]{figs/field_tree_new_node}
\label{fig:new_node}
} \qquad
\subfigure[Insertion of $t_9$ with mask $1100$.]{
\includegraphics[scale=0.36]{figs/field_tree_no_node}
\label{fig:no_node}
}
\vspace{-2mm}
\caption{Field Tree Examples. \label{fig:field_ex}}
\vspace{-4mm}
\end{figure*}

The order we use for the dimensions is
region independence, field independence, and finally non-interference of privileges.
This ordering stems from an important observation about
Legion programs: it is common for programs, 
such as S3D, to express data parallelism at several different 
granularities both across nodes and within nodes.  It is therefore 
most likely that two tasks will be proven to be non-interfering 
based on the independence of the logical regions that they access.  
After this, field set independence is most likely.  Finally, 
privilege non-interference is a more complicated test, and is 
consequently placed last where it is least likely to be performed.  
While it is possible to write Legion applications that
perform better with a different test ordering, it has been our 
experience that the performance of this ordering is sufficient for 
achieving high non-interference test throughput.  In 
Section~\ref{sec:eval}, we quantify the throughput of Legion 
non-interference tests to determine its impact on total performance.

Having fixed an ordering on dimensions for our non-interference tests,
we now build on the Legion non-interference test from
\cite{Legion12,LegionTypes13}.  At every point in time, the runtime
system tracks which already-issued tasks are using which regions by
annotating regions of the region tree with task and privilege
information.  An example is given in Figure~\ref{fig:nonintex} where
task $T_0$ has {\em read-write} privileges on region $r_p$, $T_1$ has
{\em read-write} privileges on subregion $r_2$, and $T_2$ has {\em
  read} privileges on subregion $r_1$. (Also recorded is the partial
order in which these issued tasks can execute, but this information is
not required for the non-interference test.)  Now assume that the system
issues a new task $T_3$ with {\em read-write} privileges for $r_1$.
The runtime system walks the path in the region tree from the root to
the target region $r_1$, testing all tasks encountered along the way
for non-interference; since all of these regions contain $r_1$, task $T_3$
potentially interferes with any such task.  (Tasks in regions off of this path,
such as $T_1$ in the example, are already known to be non-interfering because
they use regions disjoint from $r_1$.)   In this case $T_3$ interferes
with both $T_0$ and $T_2$.  Task $T_3$ is recorded in the node for region $r_1$
and the system also records the fact that $T_3$ cannot execute until $T_0$ and $T_2$ complete.

To extend this algorithm to check field non-interference, we store the
set of fields used in addition to the privilege information with each
task in the region tree.  So, for example, if $T_0$ reads and writes
field $f$, $T_2$ reads field $g$ and $T_3$ reads and writes fields $\{
g, h \}$, then the runtime system determines that $T_0$ and $T_3$ do
not interfere (based on field usage), while $T_2$ and $T_3$ still do
interfere.

Whether two tasks overlap on region arguments is determined by the
nodes visited during the walk of the region tree.  For each task
recorded at a node we may need to perform independence tests
between many large sets of fields; to
make these tests fast we implement sets of fields as bit masks.  For
every region and privilege that a task requests, a bit mask is
inserted into the region tree summarizing the fields used for the
specific region and privilege.  Using bit-wise operators, fast tests
for both disjointness and intersection can be performed.  To further
improve the performance of bit mask disjointness testing we place a
compile-time upper bound on the number of fields that can be allocated
in a bit mask, which allows fixed storage to be allocated for each bit
mask.  The runtime dynamically maps field allocations for a field
space into indices in the bit mask, and frees up indices when fields
are deallocated.  While this does place a restriction on the total
number of fields in a field space, it does not limit the total number
of fields in a program, as our implementation supports an unbounded
number of fields spaces.  Additionally, the upper bound can be
programmer controlled, and increasing the upper bound simply increases
the cost of the dynamic non-interference analysis.

\subsection{Accelerating Non-Interference Tests}
\label{subsec:accel}
As expected, adding an additional dimension to the non-interference test
introduces additional overhead in dependence analysis.  To mitigate
this additional overhead we designed two optimizations for improving the
performance of non-interference testing, one which reduces 
asymptotic complexity and another which reduces constant
overheads.  We describe the two optimizations in this section and quantify
their performance improvements in Section~\ref{subsec:dep_opt}.

The first optimization attacks the linear time cost of testing each
task stored at a node in the region tree for field disjointness.
Ideally, we can prune large groups
of tasks from consideration with a single field independence test.  To
achieve this effect we introduce a {\em field-tree} data structure with
the following properties:
\begin{itemize}
\item Every node in the tree maintains a bit mask for a set of
      fields and set of tasks whose bit masks are identical
      or a subset of the node's field mask.  A {\em precise} bit
      tracks whether all the tasks stored at this node have identical
      bit masks to the node's bit mask or not.
\item A parent node can have sub-nodes whose bit masks are subsets 
      of the parent node's mask.  The bit masks between child nodes
      may overlap but may never dominate each other.
\end{itemize}
Figure~\ref{fig:field_tree} shows an example field tree for bit masks
with a maximum of four fields.  Field tree data structures support a
traversal operation which takes a field mask argument and traverses
all the nodes that overlap with the given field mask and examines all
the tasks which have overlapping fields within the field tree.  This
operation is straightforward and starts at the root (node with a bit
mask of all ones) and traverses all sub-nodes which have a non-empty
intersection with the mask.  At each node all the tasks are examined.
If a node is precise, we know all tasks must be examined without
requiring any field independence tests.  If a node is not precise,
each task at this node must undergo a field independence test to
determine if the task should be examined.  Before traversing a
sub-tree, we test for a non-empty intersection with the child node's mask which serves
as an upper bound on the possible masks in the sub-tree.  If our traversal mask
is independent of the sub-mask then we can skip the entire sub-tree.

The other operation supported by field trees is insertion, which has a less 
obvious implementation.  Insertion traverses the tree until it finds a node
with the same bit mask as the task being inserted and then appends the task
to the list in the node.  There are two cases where such a node will not be found.  
First, the node may not exist, in which case a new node is created and added
as a child of an existing node.  This situation is illustrated in 
Figure~\ref{fig:new_node} when task $t_8$ with the bit mask $0100$ is added
to the field tree data structure.  In the second case, there may be two 
sub-nodes of the last traversed node which both dominate the mask.
To avoid duplicating tasks in the field tree 
(which could exponentially increase the number of tasks stored in the field 
tree), we instead place the task at the most precise node which dominates 
the mask and has no siblings which also dominate the mask.  We then mark 
the node where the task was added as no longer being precise.  This situation is
shown in Figure~\ref{fig:no_node} when task $t_9$ is added with the bit
mask $1100$.  
%It is important to note that the field tree data shape is
%driven by both the masks that appear and their order.  
% Something about complexity here?

The second optimization we implemented is designed to reduce the constant 
overheads associated with performing field independence tests on large 
bit masks.  The key insight is that fast field independence tests rely 
primarily on bit mask intersection.  In cases like the heptane mechanism 
with an upper bound of 2048 fields, the bit mask is stored as thirty-two 
64-bit words.  Testing for intersection requires performing bit-wise 
intersections on all thirty-two words.  In practice, most bit masks represent 
between 1 and 64 fields (although heptane does require several masks with 
hundreds of fields).  To avoid performing intersection tests
on all words for bit masks with just a few set bits, we introduce a
two-level bit mask.

A two-level bit mask stores a single word summary of all the other words in
the bit mask.  If a bit at index $i$ is set in any of the words in the bit
mask, then the bit at index $i$ is set in the summary mask.  When performing 
an intersection test, the two summary masks are first intersected.  If the 
result is empty, then full intersection is also empty, allowing us to skip
the test on the full set of words.  Otherwise, we simply perform the full 
intersection test.  In practice, most bit masks only have a  few set bits 
and maintaining a summary mask significantly improves performance of field 
independence tests.

