
\section{Performance Evaluation}
\label{sec:eval}
In this section we evaluate the performance of our
implementation of structure slicing as well as the benefits
of using structure slicing in a production 
application.  
%We deviate from standard programming
%systems methodology in evaluating our work.  
Instead of writing 
a suite of representative application benchmarks using structure
slicing, we have instead ported the full S3D application, including
more than 100K lines of hand-written Fortran and 250K lines of
per-mechanism generated Fortran, to use our structure slicing
version of Legion written in C++.  This process forced us to 
ensure that structure slicing worked under many different 
conditions and was capable of scaling to complete production 
applications.  The full S3D application also exercised 
performance bottlenecks that we have never observed in any of
the many parallel benchmark suites commonly used for academic
research\cite{PARSEC09,LONESTAR09,NASPAR,Mantevo08,RODINA09}.
Consequently we feel that our evaluation more accurately 
characterizes the benefits that can be expected from using
structure slicing on production codes.

While our version of S3D written in Legion is capable of running
on an arbitrary number of nodes, we focus our performance analysis 
on performance within a node for two reasons.
First, performance within a node dictates the performance
of the full S3D application, as the explicit Runge-Kutta solver 
only requires communication between adjacent nodes.  Second, 
structure slicing is specifically designed to impact performance 
within a node and we wish to accurately characterize the performance
benefits directly attributable to structure slicing.

We used the Keeneland supercomputer\cite{Keeneland} to conduct
all of our experiments.  Each Keeneland node contains two
8-core Intel Sandy Bridge processors with support for 256-bit AVX 
vector instructions, 3 NVIDIA 
M2090 Fermi GPUs with 512 CUDA cores and 6 GB of memory, and 24 GB of 
DRAM spread across two NUMA domains.  Our baseline for comparison is
the Fortran version of S3D\cite{S3D09} compiled with the Intel Fortran
compiler, using AVX vector optimizations.
(The Intel Fortran compiler yielded significantly higher performance 
than the PGI or GNU Fortran compilers.)  We also attempted to compare 
against the OpenACC version of S3D\cite{S3DACC12}, but were unable 
to compile and run their code due to variations in the target architecture and/or
compiler (i.e. Cray OpenACC compiler on Titan vs. PGI OpenACC compiler on 
Keeneland).  We compare with their published results in 
Section~\ref{subsec:openacc}. 
%but are continuing to try to get a 
%compiled version on which to run our experiments directly.

\subsection{Dependence Test Optimizations}
\label{subsec:dep_opt}

\begin{figure}
\includegraphics[scale=0.45]{figs/field_overheads}
\caption{Structure Slicing Non-Interference Performance\label{fig:overhead}}
\end{figure}

Our first experiment is designed to measure the performance overheads
associated with the dynamic dependence analysis required by structure
slicing for extracting implicit parallelism.  To stress the dependence
analysis, we experimented with the heptane mechanism (our current
largest) and selected a small problem size of $32^3$ grid points on
a single node.  The heptane mechanism launches the most tasks and
therefore creates the largest possible number of dependences which
must be evaluated.  By selecting a small problem size, we minimize
the amount of work available for the runtime to use when hiding
the latency of the dependence analysis.

We configured our experiment to test four different versions of the
dependence analysis.  In the first configuration, we ran our new
dependence analysis algorithm without any of the optimizations
described in Section~\ref{subsec:accel}.  We also ran each experiment
with the optimizations enabled in isolation and then with both
optimizations enabled simultaneously.  We ran for several time steps
and measured the average amount of time spent performing dependence
analysis for an invocation of the {\tt rhsf} function. 
Figure~\ref{fig:overhead} shows the total time spent performing 
dependence analysis for an average {\tt rhsf} call over a range of 
logical region subgrids in the lowest-level partition in the region tree.

When using both CPUs and GPUs, the total execution time for an
invocation of {\tt rhsf} for the heptane mechanism is approximately
450 milliseconds (ms).  In the worst case the dependence analysis
without any optimizations for 16 subgrids requires 241 ms, which is a
significant portion of the total execution time.  The Legion runtime,
however, overlaps the dependence analysis with other useful work
(i.e., tasks that can execute) as much as possible, so the real
question is whether the dependence analysis affects overall running
time.  A critical path analysis reveals that while the latency of a
majority of the non-interference tests are successfully hidden, a
small fraction of them appear on the application's limiting path of
execution.  Any improvement in performance on the critical path 
improves overall application performance, motivating the need for the
optimizations described in Section~\ref{subsec:accel}.

Figure~\ref{fig:overhead} also shows the benefits introduced by the 
two optimizations described in Section~\ref{subsec:accel}.  Both
optimizations dramatically speed up the field disjointness test (by
up to 9X), but because the test on fields is only a part of the
dependence analysis and not even always executed, the overall impact
is less but still significant.  The field tree algorithm improves
dependence analysis by up 12\% while the two-level bit mask improves
performance up to 22\%.  The two-level bit mask algorithm offers the
most benefit due to the extremely large 2048-bit masks required for
the heptane mechanism, while the field tree actually grows large due
to the variety of bit masks generated by S3D.  Interestingly, combined
the two optimizations are less effective as the two-level bit mask
adds overhead to the field tree algorithms with minimal gain.  
We expect both optimizations to be useful in the future
depending on the size of bit masks required, the number of bit masks
generated, and the frequency with which they occur in different
applications.

%
% ALEX
% Is the following statement correct?  We don't say it anywhere but it is
% the logical follow-up to the critical path discussion of the unoptimized version.
%
Either optimization is sufficient to remove dependence testing completely
from the critical path of execution in S3D, showing the importance of the 
improvements, especially for small problem sizes.  In larger problem sizes 
the cost of dependence analysis does not change as the dependence analysis 
is purely a function of the number of tasks launched.  Thus, on larger problem 
sizes that fill an entire node, dependence analysis overheads are less than 
1\% of total execution time.  On average, non-interference analysis of an 
S3D task requires 82 microseconds, which is significantly less than the tens to 
hundreds of milliseconds required to execute the tasks for full node problem 
sizes.  This suggests that true cost of the dynamic dependence analysis is the 
loss in throughput resulting from the use of a dedicated core by the Legion
runtime to perform dependence analysis, among other runtime responsibilities.  
On Keeneland this translates to a $\frac{1}{16}$ drop in performance in some 
configurations, which we observe in Section~\ref{subsec:rhsfperf}.    


\subsection{Effects of Problem Size}
\label{subsec:problemsize}

\begin{figure}
\includegraphics[scale=0.45]{figs/psize_hept.pdf}
\caption{Performance Dependence on Problem Size\label{fig:psizehept}}
\end{figure}

Our next experiment is designed to illustrate one of the effects we 
claimed in the introduction as motivation for structure slicing.  In many
cases, kernels require a significant amount of work in order to overcome
the start-up costs associated with their execution.  To illustrate this
effect we show the impact that problem size has on performance for both
the baseline Fortran version of S3D as well as both a CPU-only and
hybrid CPU-GPU version of our Legion S3D implementation.

We run several time steps of S3D over a range of problem sizes and measure
the total throughput in terms of cells updated per second.  We use
only a single CPU core (or a single GPU in the hybrid case) to focus on
the best-case performance per processor without contention for shared resources
such as memory bandwidth or locks.  Figure~\ref{fig:psizehept} shows
the results when the experiment is run with the heptane mechanism.
Other mechanisms differ in the absolute throughput values, but not in
the shape of the curves.

The static partitioning of work across vector units guarantees that the
Fortran implementation maintains constant throughput.  In the case of Legion,
we recognized that several kernels were memory bandwidth bound, and blocked
them for caches on the CPU.  As a result these kernels are faster on larger
problem sizes, but slower than the Fortran code on smaller problem sizes where
the extra overhead of moving data into a scratch buffer adds latency to
the overall computation.  The effect of start-up costs is even more 
pronounced on the Legion GPU kernels used in the hybrid version as large 
problem sizes ensure a more highly utilized GPU. 

There are two important conclusions to draw from this experiment.  First,
while static scheduling of data parallelism can often achieve significant
performance, it is not always the right decision.  In some cases, like S3D,
it is often better to leverage task parallelism afforded by structure slicing
to send different tasks to different processors.  If large enough problem
sizes can be handed to each processor, the total throughput may ultimately
be higher.  This explains the improved throughput of the Legion CPU-only
version when compared to the Fortran version.  It is also important to note
that this effect will only be magnified as the amount of available memory per
node continues to grow.

The second important conclusion from this experiment deals with ability of
structure slicing to increase available problem sizes in hierarchical 
memory.  While the Legion-hybrid version cannot be directly compared to the 
CPU-only versions, it illustrates the large variability that exists in 
performance on accelerators as a function of problem sizes (over 3X difference
between $32^3$ and $96^3$).  The only reason we are able to fit $96^3$ points
into a 6 GB GPU framebuffer is because structure slicing allows our Legion 
implementation to know exactly which fields needed to be moved to the GPU
for a given task.  Structure slicing made it possible for our Legion 
implementation to only move the necessary fields for GPU computations.
This both reduced communication latency since less data movement was required
through the memory hierarchy, and increased overall processor throughput
as a larger problem size could be fit into a limited capacity memory.

%This effect is even more pronounced for the Legion GPU
%kernels used in the hybrid version.  The performance of the hybrid version cannot be 
%directly compared to the CPU-only versions as the number of CPU cores generally exceeds
%the number of GPUs in a node, but the large variability (over a 3x difference between
%problem sizes of $32^3$ and $96^3$) will remain.  When considering applications (or individual
%kernels) to re-write in CUDA for a GPU, the ability to extract task parallelism (e.g. via
%structure slicing) can help keep the individual problem sizes larger and greatly improve
%the result of that effort.

\subsection{Single-Node RHSF Performance}
\label{subsec:rhsfperf}

\begin{figure}
\centering
\subfigure[DME Mechanism]{
\label{fig:scaledme}
\includegraphics[scale=0.45]{figs/scale_dme.pdf}
}
\subfigure[Heptane Mechanism]{
\label{fig:scalehept}
\includegraphics[scale=0.45]{figs/scale_hept.pdf}
}
\caption{Performance Scaling\label{fig:scale}}
\end{figure}

%
% ALEX
% I find the description of varying along different curves confusing --- not entirely
% sure what that means.
%
In our second experiment, we fix the problem size for a single node at $96^3$ and
vary the number of CPUs available for parallel execution.  For CPU-only
runs we start at a single core and scale up to using all 16 cores on a node.  For
hybrid runs, each curve corresponds to a fixed number of GPUs while
varying the number of available CPUs.  Performance is again measured in
number of cell updates per second.  We show results for both the
DME and heptane mechanisms in Figure~\ref{fig:scaledme} and 
Figure~\ref{fig:scalehept} respectively.

As is common for MPI applications, the Fortran implementation of S3D uses 
only the data parallelism in S3D to divide the $96^3$ cells over a
separate MPI rank per core.  Although this results in progressively smaller 
problem sizes (down to a $24\!\times\!48\!\times\!48$ grid), we observed in
Section~\ref{subsec:problemsize} that the Fortran code performance does not vary
with problem size.  In this experiment, the Fortran performance scales nearly linearly
with the number of CPU cores, achieving approximately 13X speedup using 16 cores
on both mechanisms.

The CPU-only Legion implementation, using structure slicing, performs better than
the Fortran implementation at nearly all core counts.  Structure slicing enables
the Legion version to exploit task parallelism and hand larger problem
sizes to kernels optimized for larger inputs, resulting in higher throughput.  However,
the Legion CPU-only version does not scale linearly for three reasons.
First, the kernels in the Legion implementation
saturate shared system resources (e.g. socket memory bandwidth) with fewer
cores, causing performance to tail off with larger core counts.  Second, although
structure slicing extracts significant task parallelism from S3D, there are several 
phases in S3D where the critical dependence path passes through a single
data parallel task.  In these cases Legion operates less efficiently as cores
are delegated small parts of the overall grid, resulting in the reduced Legion
performance on smaller problem sizes as shown in Section~\ref{subsec:problemsize}.  
Finally, on 16 cores the baseline Fortran code passes the CPU-only version of 
Legion due to the loss of a dedicated core for the Legion runtime, as discussed in 
Section~\ref{subsec:dep_opt}.

%The Legion implementation, when using only its CPU kernels, performs better than
%the Fortran implementation at nearly all core counts, but deviates much more from
%the ideal linear scaling as core count increases.  There are three reasons for this.
%First, faster code will runs into bottlenecks from shared resources (e.g. system
%memory bandwidth) sooner.  Second, although structure slicing allows our implementation
%to use task parallelism in many cases, there are a few parts of {\tt rhsf} where
%only data parallelism is available, forcing the code to operate less efficiently on
%smaller problem sizes.  In this case, the efficiency loss is small enough that increasing
%the core count is a net win, but as we'll see below, this is not always the case.
%The final bottleneck on performance at large core counts is the Legion runtime itself.
%Legion uses a number of background threads for task scheduling and data movement, and
%these can interfere with application work if the cores are oversubscribed.  As a result,
%the standard practice for Legion applications is to reserve a few CPU cores for runtime
%use.

The hybrid CPU-GPU Legion implementation significantly outperforms both CPU-only
versions by up to 1.89X.  Using the task parallelism discovered by structure slicing
the five most computationally expensive computations were offloaded from the CPUs
onto one or more GPUs.  The performance 
of the hybrid Legion implementation illustrates a number of interesting effects.  
First, and somewhat surprisingly, the performance of the hybrid implementation
is more strongly correlated with the number of CPU cores than with the number
of GPUs used.  This is primarily an artifact of Amdahl's Law: the kernels on the GPU
are sufficiently fast that the remaining CPU work dictates performance.  Another
interesting effect can be observed at both higher CPU and GPU counts.  The hybrid
implementation has a much stronger dependence on problem size per processor due
to the optimized Legion kernels described in Section~\ref{subsec:problemsize}.  As a
result, it is often better to exploit a limited amount of data parallelism, and
rely on the benefits of task parallelism provided by structure slicing to 
achieve high performance.


%First, and somewhat surprisingly at first glance, the performance of the hybrid implementation
%is correlated much more strongly with the number of CPU core than the number of GPU cores.
%For smaller CPU core counts, this is primarily due to Amdahl's Law (the kernels on the GPU
%are so fast that the remaining CPU work dictates the performance).  At higher GPU counts and
%especially CPU core counts,
%we again see the effect of the decreased problem size that results from splitting the same
%amount of work over more cores.  Since the hybrid implementation has a much stronger dependence
%on problem size, increases in the core count eventually hurt efficiency so much that the net
%effect of the added cores is negative.  Dividing all of the work evenly over all the cores will
%still achieve better performance than the CPU-only implementations, but a careful choice
%(e.g. 2 GPU and 8 CPUs) beats the best CPU-only performance by 89\% for DME and 69\% for
%heptane.

\begin{figure}
\centering
\includegraphics[width=\columnwidth]{figs/legion_prof_crop.pdf}
\caption{Execution Profile of Hybrid Version\label{fig:prof}}
\end{figure}

We now call attention to one last interesting optimization enabled by a 
combination of structure slicing and Legion's dynamic mapper 
interface\cite{Legion12}.  Figure~\ref{fig:prof} shows the execution
of one call to the {\tt rhsf} function in the hybrid Legion implementation
generated by a Legion performance profiling tool.  Each box represents a 
separate task.  This particular run used
4 CPUs (running green, blue, and purple tasks in middle) and 
1 GPU (running the magenta tasks on the bottom) along with a single thread
(along the top) dedicated to running Legion runtime meta-tasks (such as testing non-interference).  Although
there is some room for improvement, this profile shows significant
overlap of the CPU and GPU work, including the data transfer operations (not illustrated)
between the CPU and GPU memories (the GPU is continuously busy despite
the sum of all tasks' data consuming more memory than exists in a framebuffer).
This is possible because structure slicing discovered enough task
parallelism for a Legion mapper to generate a sequence of different tasks
to be sent to the GPU.  As a result, the latency
of transferring data both to and from the GPU could be hidden as the GPU
was always performing useful work in parallel with asynchronous data copies.
Thus structure slicing made it possible to effectively implement the
hierarchical memory software pipelining algorithm from \cite{Knight07} using
task parallelism instead of data parallelism.

\subsection{Comparison to OpenACC Implementation}
\label{subsec:openacc}
As discussed above, we have code for the OpenACC implementation of S3D, but were
unable to compile it on Keeneland.  Consequently, our comparisons are limited to 
the numbers published in \cite{S3DACC12} and inspection of the code.  
As they describe in their paper, their code only supports and is heavily
optimized for the heptane mechanism; they further detail the
extensive manual code transformations performed to merge many
computations into large loops that are either parallelized across the CPU
cores with OpenMP directives or offloaded to the GPU with OpenACC directives.
The majority of these loops are parallelized using OpenACC which primarily
keeps the GPUs busy and the CPUs idle.  Since most tasks are performed on
the GPU, their implementation is limited to a problem size of $48^3$ in
order to fit all the necessary data in the 6 GB GPU framebuffer.  In contrast,
structure slicing allows our implementation to run problem sizes of 
$96^3$ (8X larger) and use less than 5 GB of GPU framebuffer memory.
Running on the Titan supercomputer,
they report the time to calculate one time step as 4.5 seconds when using a 
CPU-only approach and 1.8 seconds when using the GPU, which corresponds to
throughputs of 24.5K and 61K cell updates respectively.  With the obvious caveat 
that these  results were obtained on different architectures, the Legion 
versions of S3D obtain throughputs of 71.5K for the CPU-only and 127.4K for the 
hybrid CPU-GPU implementations which correspond to 2.91X and 2.09X 
improvements in throughput respectively.

Although the difference in performance on the CPU-only version can be 
directly attributed to architectural differences, the hybrid CPU-GPU difference
is primarily an illustration of the benefits conferred by structure slicing.
Consider that a Titan node has a single 16-core AMD Interlagos which uses
a single socket with 4 DRAM channels, while a Keeneland node contains
2 sockets for a total of 8 DRAM channels.  The available memory bandwidth
on a Keeneland node is approximately twice as high and results in 2X
improvements in the majority of S3D's memory bandwidth limited kernels.
The combination of improved memory bandwidth along with faster Sandy Bridge 
processors with wider vectors (256 bit versus 128 bit) is enough to
explain a 2.91X discrepancy in performance in the CPU-only experiment.

However, in the case of the hybrid CPU-GPU implementations, a Titan node 
maintains a significant advantage over a Keeneland node.  Each Titan node 
possesses an NVIDIA Kepler K20x GPU with twice the throughput
and twice the memory bandwidth of the one M2090 GPU used in our best
Legion run.  Despite this limitation in hardware, structure slicing
allows our Legion implementation to exceed the performance of the
OpenACC version by 2.09X.  

There are two factors that contribute to structure slicing outperforming
OpenACC even with inferior hardware.  First, code on a GPU is not
uniformly faster than code on a CPU.  The OpenACC implementation moves
as many computations as possible to the GPU, even when the computation
may not be ideally suited for the GPU.  Leveraging structure slicing,
our implementation is able to use task parallelism to move natural GPU 
tasks to the GPU while leaving more natural CPU tasks on the CPU.  By
using both CPU and GPU processors in parallel, we significantly
improve overall throughput.  Second, with the majority of the OpenACC
version of S3D running on the GPU, all of the scratch fields used 
by {\tt rhsf} must also be allocated on the GPU.  As a result, the
maximum problem size that can fit on a Titan node is $48^3$.
It is likely that the OpenACC kernels encounter the degraded performance 
of smaller problem size observed in Section~\ref{subsec:problemsize}.
The result is that structure slicing more than makes up for the deficit
of inferior hardware.

Overall we believe that this comparison is indicative of 
current trends in high performance computing.  While data parallelism
provides an easy way to fill the steadily increasing number of processors
on a node, it encounters diminishing returns as smaller problem sizes
reduce the efficiency of individual processors.  Furthermore, for
systems with heterogeneous processors and hierarchical memory, 
data parallelism alone is unable to make simultaneous use of all the 
computational resources of a node and to hide the growing latencies 
of data movement.  Structure slicing provides programmers with a 
powerful tool for describing the task parallelism necessary to 
address the challenge of programming both modern and 
future architectures.


%a 16-core AMD Interlagos CPU with 4
%DRAM channels, while a Keeneland node contains 2 8-core Intel Sandy Bridge
%processors with 
%is related to architectural variations, we 
%believe the rest is related to benefits conferred from structure slicing.  
%When comparing the CPU-only versions of the code, Titan nodes contain
%16-core AMD Interlagos processors.  While
%
%With nearly all of the {\tt rhsf} computation moved into these loops, the
%computation is restricted to only running CPU computations or GPU computations
%at a time due to the synchronization models of OpenMP and OpenACC.
%With the majority of the computation running on the GPU, all of the 
%scratch fields used by {\tt rhsf} must also be allocated on the GPU, limiting 
%their implentation to work on problem sizes of $48^3$.  As a result,
%it is likely their GPU kernels encounter the effects of smaller problem 
%sizes discussed in Section~\ref{subsec:problemsize}.
%
%
%They report the time to 
%calculate one full S3D timestep of roughly 4.5 seconds when using the CPUs and 
%just under 2 seconds when using the GPU kernels.  This corresponds to 
%approximately 25,000 and 60,000 cell updates per second, respectively.  
%The numbers are reported on Titan, which uses 16 core AMD Interlagos CPUs and 
%a single NVIDIA Kepler K20x GPU per node.  Although both systems have 16
%CPU cores, the Keeneland CPUs have twice the vector width (256 bit vs. 128 bit) 
%and twice the memory bandwidth per core (8 shared DRAM channels vs. 4).  
%The difference in architectures, accounts for 2X.
%
%This discrepancy, along with difference in compilers, explains most of the 
%3X difference in performance on the CPU-only versions.  Their GPU accelerated results are roughly
%half of what we were able to achieve on Keeneland, despite using a GPU with twice the theoretical
%double-precision throughput (K20x vs. M2090).  There are likely three factors contributing to
%this result.
%
%The first difference is likely due to the style in which
%the GPU kernels are coded.  The OpenACC model maps individual loop iterations to separate CUDA threads,
%but this does not take advantage of the ability of CUDA threads within the same block to work
%together.  Our GPU kernels were written in CUDA and made heavy use of shared memory to use multiple
%threads to work on a single cell, better fitting the workload into the constraint register files and
%caches on GPUs.
%
%Code on a GPU is not uniformly faster than the corresponding code on a CPU.
%The OpenACC implementation moves all the computation to the GPU, whereas our implementation used
%GPU kernels just for the computations we were confident would map well to the GPU.  By leaving
%more CPU-suited computations on the CPUs and using the task parallelism from structure slicing to 
%run them in parallel with the GPU, the GPU is able to spend its cycles on computations that do
%map well to the GPU, increasing throughput.  
%
%The third likely contributor to the performance difference between the OpenACC implementation and our
%hybrid Legion implementation is again the effect of problem size.  Because it can only fit a problem
%size of $48^3$ per node, then OpenACC is almost certainly operating at a lower point on the performance
%curve than our implementation.  With more careful selection of which tasks to map to the GPU, 
%structure slicing would be able to reduce the per-cell memory footprint on the GPU, allowing the
%OpenACC implementation to operate on larger problem sizes and achieve increased performance.

%Figure~\ref{fig:prof} shows the execution of one call to {\tt rhsf} in the hybrid Legion
%implementation, generated by the Legion performance profiling tool.    Although there is clearly some room for further improvement, this profile shows
%significant overlap of the CPU and GPU work.  This simultaneous use of heterogenous computational
%resources was primarily due to task parallelism, obtained through structure slicing.  In a few
%cases, such as between the early GPU work and the larger blue boxes on the CPU, judicious use of
%data parallelism allowed a ``pipelining'' optimization where one large task is able
%to start working
%on parts of the input while the rest is still being computed by the previous task.


%% Compare against baseline S3D
%% Talk about what is better and worse
%% Show performance scaling with number of curves
%% Talk about tradeoffs in computational overheads
%% effects of different mechanisms
%% effects of problem size
%% benefits of structure slicing

%% \subsection{Performance vs Accuracy Tradeoffs}
%% \label{subsec:tradeoffs}

%% \begin{figure}
%% \centering
%% \includegraphics[width=\columnwidth]{figs/lag_hept.pdf}
%% \caption{Performance Benefit of Lagging Switch\label{fig:lagswitch}}
%% \end{figure}

%% Figure~\ref{fig:lagswitch} examines the performance benefit of enabling the
%% {\em lagging switch} in S3D, which reuses the results of an expensive computation
%% for multiple {\tt rhsf} calls, at the cost of some accuracy.  This particular
%% computation gets much faster in the hybrid implementation, and it's important to
%% reconsider such performance/accuracy tradeoffs, but this really feels like a 
%% digression for this paper.

%% talk about performance with heterogeneous nodes
%% describe mapping strategy including how we can
%% move tasks back and forth between cpu and gpu
%% describe benefits of using both cpu and gpu
%% describe hiding transfer latency with task pipelining
%% made possible by structure slicing
