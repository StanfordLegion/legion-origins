
\section{Warp-Specialized Code Generation}
\label{sec:codegen}
In this section we describe techniques for emitting high-performance
warp-specialized code.  The primary obstacle to overcome when generating
warp-specialized code is the performance of the GPU's instruction cache.
GPUs are built assuming all threads run the same code with minimal 
stretches of control divergence.  The na\"{i}ve code generation strategy of 
using a top-level switch statement on the warp ID to send each warp to a 
different block of code violates this assumption and results in
severe performance degradation.  Figure~\ref{fig:naivespec} 
(on previous page) shows a comparison 
of na\"{i}ve warp-specialized code with code emitted by \Singe for a DME 
viscosity kernel over a range of warps per CTA.  The na\"{i}ve approach begins thrashing the instruction
cache at six different warp code paths, while the code emitted by \Singe
continues to improve with peaks for warp counts that evenly divide the
number of species in the DME mechanism.  It is therefore imperative that 
common code paths be maintained across warps and that branching
on warp IDs be done at a fine enough granularity to avoid thrashing the
instruction cache.  
%In the rest of this section we outline the set
%of general techniques necessary for emitting high-performance 
%warp-specialized code.

%For concreteness we assume that the compiler maintains an abstract syntax tree (AST)
%intermediate representation for the code assigned to each warp.  
%Any other form of intermediate code would work just as well for the transformations
%we describe.  

%In the case of \Singe, each warp maintains an abstract syntax tree (AST)
%as the IR for the code assigned to each warp.  Code may be transformed
%by mutating the AST and code is emitted by traversing the AST nodes.

\lstset{
  captionpos=b,
  language=C++,
  basicstyle=\scriptsize,
  numbers=left,
  numberstyle=\tiny,
  columns=fullflexible,
  stepnumber=1,
  escapechar=\#,
  keepspaces=true,
  belowskip=-15pt,
  morekeywords={\_\_shared\_\_,\_\_constant\_\_,\_\_global\_\_,\_\_device\_\_},
}
\begin{lstlisting}[float={t},label={lst:bitmask},caption={Overlaid bit-mask code emitted by Singe.}]
const int warp_id = (threadIdx.x >> 5);
const int lane_id = (threadIdx.x & 0x1f);
...
if ((1 << warp_id) & 0x000005D7) {
  int troe_idx = troe_index_0[0+step*step_stride][warp_id];
  double fcent = scratch[TROE_OFFSET+troe_idx][lane_id];
  double flogpr = log10(pr) - 0.4 - 0.67 * fcent;
  double fdenom = 0.75 - 1.27 * fcent - 0.14 * flogpr;
  double fquan = flogpr / fdenom;
  fquan = fcent / (1.0 + fquan * fquan);
  rr_f_0 = rr_kinf * pr/(1.0 + pr) * exp(fquan * DLn10);
} else {
  rr_f_0 = rr_kinf * pr/(1.0 + pr);
}
\end{lstlisting}

\subsection{Overlaying Computation}
\label{subsec:overlay}
In order to minimize instruction cache thrashing, a warp-specializing
compiler must {\em overlay} code from different warps whenever warps
are performing similar computations.  To achieve this goal
we modify the standard approach to generating code from an AST.  Code generation
from an AST traditionally involves traversing the AST from top to bottom, emitting
code at a node and then emitting code for each of a node's children in
program order.  In a warp-specializing compiler, code must be generated from
a forest of ASTs with each AST describing the code to be executed for a different warp.
To generate code from this forest of ASTs, a warp-specializing compiler traverses
all the ASTs simultaneously.  As long as the AST nodes across all warps are
identical (with the exception of different constant values and indexing
offsets, discussed in Sections~\ref{subsec:dedup} and \ref{subsec:indexing}), 
the compiler emits a single instance of code for all the warps in the kernel 
to execute\footnote{Care must also be taken to standardize variable names wherever
possible between different warps to avoid creating false AST differences.}.  When the structure 
of the ASTs differs, the compiler uses branches dependent on warp ID to 
differentiate code blocks for warps.

\Singe uses two different approaches to branching on warp ID.
In the first case, if there are no similarities between
the code required for each warp, \Singe emits an indirect branch
dependent on warp ID to jump to the correct block of code for a given warp.  
For cases with longer sequences of instructions, the single indirect branch
statement is fissioned into multiple indirect branch statements.
In our experience this approach does not degrade performance provided the 
regions of code along each path of an indirect branch are less than a few 
hundred instructions long.  Under these circumstance the instruction cache 
prefetching mechanism is capable of handling the inter-warp divergence.  
%For cases with longer
%sequences of instructions, multiple indirect branches are used.  To avoid
%placing additional register pressure on the kernel, temporaries from different
%warps that are live across indirect branch statements are aliased.

In cases where there is still some structure shared among a subset
of the warps, \Singe uses bit-masks to indicate
which warps should enter a block of code.  Bit-masks are constructed
using one-hot encoding with each bit in the mask indicating whether a
warp should take the branch or not.  Listing~\ref{lst:bitmask} shows an
example of overlaid code generated by \Singe for computing Laundau-Teller 
and Lindermann reaction rates simultaneously in the chemistry kernel.  
By overlaying code with bit-mask warp filters, \Singe 
minimizes the number of different paths warps can take during execution,
thereby improving performance of the GPU instruction cache.

In general we have found that branching many ways for a few hundred
instructions or less, or only branching a few ways for longer stretches of code, is necessary to 
avoid thrashing the GPU's instruction cache.  This is consistent with
earlier results on using warp specialization that only contained two or three
different warp-specialized code paths\cite{CudaDMA11}.  The penalty for thrashing
the instruction cache is routinely performance degradation of an order of
magnitude or worse.  Therefore it is crucial that any 
warp-specializing compiler overlay warp-specialized code on current GPUs.

\subsection{Constant Arrays and Constant Deduplication}
\label{subsec:dedup}

\begin{lstlisting}[float={t},label={lst:fermi_broadcast},caption={Example Fermi constant broadcasts.}]
__shared__ volatile double real_mirror[NUM_WARPS];
const int warp_id = (threadIdx.x >> 5);
const int lane_id = (threadIdx.x & 0x1f);
...
if (lane_id == 3)
  real_mirror[warp_id] = real_constants[0];
double arrhenius = real_mirror[warp_id] * vlntemp;
if (lane_id == 4)
  real_mirror[warp_id] = real_constants[0];
arrhenius = __fma_rn(real_mirror[warp_id], ortc, arrhenius);
\end{lstlisting}

\begin{lstlisting}[float={t},label={lst:kepler_broadcast},caption={Example Kepler constant broadcasts.}]
int hi_part, lo_part;
hi_part = __shfl(__double2hiint(real_constants[0]),3,32);
lo_part = __shfl(__double2loint(real_constants[0]),3,32);
double arrhenius = __hiloint2double(hi_part,lo_part) * vlntemp;
hi_part = __shfl(__double2hiint(real_constants[0]),4,32);
lo_part = __shfl(__double2loint(real_constants[0]),4,32);
arrhenius = __fma_rn(__hiloint2double(hi_part,lo_part), ortc, arrhenius);
\end{lstlisting}

One of the challenges in generating overlaid warp-specialized code
is that often warps require different constant values.  We describe
a technique that avoids branching on different constant values.

After a computation has been partitioned for warp specialization, each
warp requires only a subset of the total constants needed for the full
computation.  The DSL compiler can then allocate an array for storing
the constants needed by each warp that is as large as the largest
number of constants needed by any warp.  The compiler emits code
so that all warps access the same locations in the constant array at
all times.  After code generation is complete, the compiler lays out
constants in memory to ensure the right values are in the correct
locations of the array for each warp.  In some cases for divergent
code, this may involve emitting padding values into the array for some
warps.  However, the cost of loading a few padded values is small
compared to the cost of dynamic branching.

In practice, we have discovered that these constant arrays are 
often quite large for a single warp and can consume more than
an entire thread's worth of registers.  We therefore have developed
another optimization for deduplicating constants between the threads within
a warp.  All the threads within a warp require the same set of constants and a single
thread can broadcast a constant value to the other threads within
a warp with no synchronization.  Instead of each thread holding all 
the constants required for computation, the constants for a warp
are statically striped across the threads within a warp, requiring
each thread hold only $\frac{1}{32}$ of all the constants.
Whenever a constant is needed, it is broadcast from the owning thread
to the other threads within the warp.

This broadcast takes different forms depending on the architecture.
For Fermi GPUs, the broadcast is performed using an allocation of
shared memory with one location per warp.  One thread writes data
into shared memory and then the other threads in the warp read
the value; no explicit synchronization is required because
all the threads within a warp execute in lock step.
Listing~\ref{lst:fermi_broadcast} shows example code emitted
by \Singe employing this technique.

On Kepler GPUs the broadcast is performed more efficiently using
{\em shuffle instructions}.  Shuffle instructions allow an exchange of
32 bits between all threads in a warp.  By breaking double
precision constants into their upper and lower halves they can be
broadcast from the owning thread and then re-assembled.  The two
shuffle instructions are faster on Kepler because they do not stall
pipelined loads in the shared memory pipeline, which happens during the shared memory
write for the Fermi broadcast.  Listing~\ref{lst:kepler_broadcast}
shows an example of exchanging constants using shuffle instructions.

Figure~\ref{fig:constant_regs} shows the number of registers required 
per thread for storing constants values across both mechanisms on
the Kepler architecture.  Note that they are small enough
to allow the majority of registers to remain free for general purpose
computation while still holding more constants on chip than can even
be addressed by the constant cache, let alone fit in its 
working set\cite{CUDA}.

This approach to storing constant values in registers can yield
further performance gains when coupled with a streaming execution
model.  When multiple sets of points are mapped onto a single CTA,
the CTA performs a loop to handle all of its points.  By hoisting
the loads for the constants outside of this loop, a DSL compiler
can amortize the cost of loading constants into registers, resulting
in very low overhead constant access.

\begin{figure}
\centering
{\small
  \begin{tabular}{c|ccc}
  Mechanism & Viscosity & Diffusion & Chemistry \\ \hline
  DME     &  8 & 18 & 6 \\
  Heptane &  28 & 28 & 8 \\ 
  \end{tabular}
}
\caption{Constant registers per thread on Kepler.\label{fig:constant_regs}}
\vspace{-0.5cm}
\end{figure}

\subsection{Warp Indexing}
\label{subsec:indexing}
For applications with irregular memory access patterns, warp specialization
can result in warps needing to access different locations in memory.  One of the
many examples of this occurring in \Singe is in the stiffness computations for 
the chemistry kernel described in Section~\ref{subsec:chem}.  Each of the 
different warps needs to load different diffusion rates from global memory and 
different molar fractions from shared memory.  The need to access different memory 
locations by different warps runs counter to the DSL compiler's goal of overlaying 
as much code as possible.  To support code overlay without dynamic branching, a 
compiler can use {\em warp indexing} constants when doing memory accesses.

Warp indexing is a technique where each warp stores integer offset values for
indexing into memory that are specific to that warp.  All warps can then
perform their address calculations using the same index variable even though
the variable stores different values for different warps.  As with the constant arrays, this extra level
of indirection enables the compiler to overlay warp-specialized code without
requiring dynamic branching for irregular memory access patterns.

Similar to constant deduplication, if the number of warp indexing constants
is large, they can be deduplicated by striping them across the threads within a
warp using the same approach described in Section~\ref{subsec:dedup}.  The only 
difference is that the index constants are not directly involved in computation, but
are instead only used for address indexing.
Listing~\ref{lst:warp_index} shows an example of warp indexing on Kepler
from the stiffness computation where the same index is used to load a diffusion 
value from global memory and a molar fraction from shared memory.

\begin{lstlisting}[float={t},label={lst:warp_index},caption={Stiffness warp indexing on Kepler.}]
__shared__ volatile double scratch[192][32];
const int lane_id = threadIdx.x & 0x1f;
...
int index = __shfl(index_constants[0],1,32);
asm volatile("ld.global.nc.cg.f64 %0, [%1];" : "=d"(stif_diffusion_0) :
  "l"(diffusion_array+index*spec_stride) : "memory");
stif_mole_frac_0 = scratch[index][lane_id];
\end{lstlisting}

One important caveat to warp indexing is that it only applies to data that
is stored in dynamically indexable GPU memories, specifically global, shared, 
and constant memories.  Arrays allocated in registers are not dynamically indexable 
and any attempt to index them with a dynamic variable causes  the CUDA 
compiler to spill the array to much slower local memory.  Ideally future 
GPUs will allow dynamic indexing of the register file as well.

