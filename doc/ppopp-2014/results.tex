
\section{Experimental Results}
\label{sec:results}
In this section we quantify the performance gains that
result from applying warp specialization in \Singe.
We compare against baseline versions of the kernels emitted
by an earlier version of \Singe in the traditional CUDA data-parallel programming 
model.  The baseline versions of the kernels were fully
optimized using a combination of domain- and architecture-specific
optimizations, including the use of logarithmic-space
computations, exposure of additional instruction level parallelism,
and the use of LDG texture loads using inline PTX for higher memory bandwidth
on the Kepler architecture.  In addition, a brute-force autotuner 
exhaustively explored the space of occupancy-register tradeoffs
on all architectures.  Consequently, the baseline CUDA kernels were 
already up to 2X faster than the OpenACC versions currently used by 
the production version of S3D\cite{S3DACC12}.
The same set of optimizations were also applied to the warp-specialized
versions of the kernels to ensure that all performance gains are
directly attributable to warp specialization.

All experiments were run on two different architectures.
The first was an NVIDIA Tesla C2070 Fermi GPU with 14 SMs, a 1147 MHz
SM clock frequency, and a 1494 MHz DRAM clock frequency.  The second
architecture was a Tesla K20c Kepler GPU with 13 SMs, a 705 MHz SM
clock frequency, and a 2600 MHz DRAM clock frequency.  ECC was disabled
on both architectures to make the memory-bound baseline kernels perform
as well as possible against the kernels emitted by \Singe.  \Singe kernels 
are primarily limited by on-chip resources and therefore achieve even larger 
speedups relative to the baseline kernels with ECC enabled.  All kernels were 
compiled with the default version of {\tt nvcc} from CUDA 5.0 and were run with
version 304.54 of the CUDA driver.

For all experiments, we report throughput for three different
grid problem sizes $32^3$, $64^3$, and $128^3$ to illustrate any 
scaling effects.  Absolute times for each experiment can be computed
by dividing the number of points in each grid by the reported throughput.  In
all cases the reported throughput is computed using the harmonic
mean of the throughputs for twenty iterations of each experiment.
In addition to reporting performance results for each kernel, we also
analyze the underlying SASS machine code for each kernel to determine the limiting
factor for each of the different kernels.

\begin{figure}
\centering
\subfigure[Fermi]{
\includegraphics[width=0.38\textwidth]{figs/visc_fermi_dme}
\label{fig:viscfermidme}
}
\subfigure[Kepler]{
\includegraphics[width=0.38\textwidth]{figs/visc_kepler_dme}
\label{fig:visckeplerdme}
}
\caption{Viscosity performance results for DME.\label{fig:viscdme}}
\vspace{-0.5cm}
\end{figure}

\begin{figure}
\centering
\subfigure[Fermi]{
\includegraphics[width=0.38\textwidth]{figs/visc_fermi_heptane}
\label{fig:viscfermiheptane}
}
\subfigure[Kepler]{
\includegraphics[width=0.38\textwidth]{figs/visc_kepler_heptane}
\label{fig:visckeplerheptane}
}
\caption{Viscosity performance results for heptane.\label{fig:vischeptane}}
\vspace{-0.5cm}
\end{figure}

\subsection{Viscosity}
\label{subsec:viscres}

Figures~\ref{fig:viscdme} and \ref{fig:vischeptane} show performance 
results comparing warp-specialized versions of the viscosity kernel
to the baseline versions for the DME and heptane mechanisms respectively.
Speedups for the warp specialization kernels ranged from 1.2X to 3.75X over the
baseline kernels.  Significantly larger speedups over the baseline versions 
were achieved on the Kepler architecture.  

To understand the underlying reasons for these results we investigated
the underlying SASS machine code generated for both the baseline
and warp-specialized versions of the code.  From the SASS we were able
to determine the total number of double precision floating point
operations required per warp for both the baseline and warp-specialized
versions on Fermi and Kepler.  For the DME mechanism on Fermi
the baseline and warp-specialized kernels achieved 197.9 and 257.3
billion double precision floating point operations per second (GFLOPS)
respectively; on Kepler they achieved 220.6 and 617.7 GFLOPS respectively.

Using these numbers along with an understanding of the Fermi and Kepler
architectures we were able to discern the limiting factors for each
kernel.  In theory, a Fermi GPU can issue 1 DFMA instruction per
SM every other clock cycle.  For the C2070 GPU in these experiments this
yields a theoretical math throughput of 513 GFLOPS.  In practice, 
optimized Fermi kernels such as DGEMM can reach around 300 GFLOPS\cite{CUDA}.
Achieving 257.3 GFLOPS on the warp-specialized viscosity kernel is 
near optimal math throughput on Fermi when the overhead of dynamic
branching and shared memory accesses are included.  The baseline CUDA
version however does not come as close to the practical peak math
throughput for two reasons.  First, the working set of molar fractions
and per-species viscosities does not fit in registers and is spilled to
local memory.  Second, the large number of constants do not fit in
the constant cache.  Both these issues add latency 
and account for the slowdown relative to the warp-specialized kernels.

For the Kepler architecture, the theoretical math throughput is
significantly higher.  On Kepler each {\em quad} of an SM can
issue one DFMA every other cycle, yielding a theoretical throughput
of 1173 GFLOPS on a K20c with 13 SMs.  The 617.7 GFLOPS achieved by the warp-specialized
viscosity kernel is more than half of theoretical peak.  We hypothesized
that this kernel is actually limited by the throughput of DFMA operations
whose third operand is loaded from the constant cache, which is the case
for the 12 DFMA operations in the Taylor series expansion of the exponential
function which dominates viscosity performance.  To verify this hypothesis
we modified \Singe to emit an incorrect exponential function that instead
relied on constants stored in registers.  Experiments showed these kernels
were capable of performance near 750 GFLOPS, indicating that our 
warp-specialized viscosity kernels were also compute-bound 
on Kepler.  The baseline CUDA version improved marginally from Fermi, but 
still suffered from severe register spilling and constant cache misses.  
The highest performing baseline CUDA version used the larger on-chip 
register file on Kepler to avoid spilling and not to increase occupancy 
which meant that the latency of loading constants was still exposed.

These results demonstrate that warp specialization enables \Singe
to partition the viscosity kernel in a way that better maps
onto both Fermi and Kepler GPUs.  Both warp-specialized kernels
approach the limits of math throughput on their architectures.
The higher potential math throughput on Kepler explains why
the Kepler warp-specialized kernel performs significantly
better than the Fermi warp-specialized kernel relative to the baseline.

\begin{figure}
\centering
\subfigure[Fermi]{
\includegraphics[width=0.38\textwidth]{figs/diff_fermi_dme}
\label{fig:difffermidme}
}
\subfigure[Kepler]{
\includegraphics[width=0.38\textwidth]{figs/diff_kepler_dme}
\label{fig:diffkeplerdme}
}
\caption{Diffusion performance results for DME.\label{fig:diffdme}}
\vspace{-0.5cm}
\end{figure}

\begin{figure}
\centering
\subfigure[Fermi]{
\includegraphics[width=0.38\textwidth]{figs/diff_fermi_heptane}
\label{fig:difffermiheptane}
}
\subfigure[Kepler]{
\includegraphics[width=0.38\textwidth]{figs/diff_kepler_heptane}
\label{fig:diffkeplerheptane}
}
\caption{Diffusion performance results for heptane.\label{fig:diffheptane}}
\vspace{-0.5cm}
\end{figure}

\subsection{Diffusion}
\label{subsec:diffres}

Figures~\ref{fig:diffdme} and \ref{fig:diffheptane} show performance
results for the diffusion kernels for the DME and heptane mechanisms
respectively.  Speedups for the warp-specialized kernels ranged from
1.33X to 2.58X over the baseline CUDA kernels.  Again larger speedups were
achieved on the Kepler architecture than the Fermi architecture due
to the higher ceiling of math instruction throughput.

To confirm that the performance of the warp-specialized kernels was
again limited by math throughput, we again analyzed the SASS machine
code.  We were surprised to discover that for the DME mechanism the
warp-specialized version of the kernel was only achieving 212.8 GFLOPS
on Fermi and 526.6 GFLOPS on Kepler.  After examining the SASS, we 
determined that the degradation in performance was caused by the additional named
barrier calls that were needed to synchronize access to the partial
sums stored in shared memory.  A larger number of barriers resulted
in excessive cycles waiting for straggler warps to arrive at the barrier
which consequently reduced math throughput.  Unsafely removing the 
barriers resulted in performance around 250 GFLOPS on Fermi and 625 
GFLOPS on Kepler, which is consistent with the actual peak math 
throughput observed in Section~\ref{subsec:viscres}.

Another interesting effect can be observed in Figures~\ref{fig:diffkeplerdme}
and \ref{fig:diffkeplerheptane}.  For smaller problem sizes the speedup
over the baseline kernels is smaller.  Due to the large number of constants
required for the diffusion kernel, the cost of loading these constants into
registers is significant.  However, for larger problem sizes it is more
easily amortized.  This effect is only noticeable on Kepler where the math
performance is much higher and exposes the constant loading phase.  A problem
size of at least $64^3$ is therefore required to fully amortize the cost of
loading the constants into registers.

\begin{figure}
\centering
\subfigure[Fermi]{
\includegraphics[width=0.38\textwidth]{figs/chem_fermi_dme}
\label{fig:chemfermidme}
}
\subfigure[Kepler]{
\includegraphics[width=0.38\textwidth]{figs/chem_kepler_dme}
\label{fig:chemkeplerdme}
}
\caption{Chemistry performance results for DME.\label{fig:chemdme}}
\vspace{-0.5cm}
\end{figure}

\begin{figure}
\centering
\subfigure[Fermi]{
\includegraphics[width=0.38\textwidth]{figs/chem_fermi_heptane}
\label{fig:chemfermiheptane}
}
\subfigure[Kepler]{
\includegraphics[width=0.38\textwidth]{figs/chem_kepler_heptane}
\label{fig:chemkeplerheptane}
}
\caption{Chemistry performance results for heptane.\label{fig:chemheptane}}
\vspace{-0.5cm}
\end{figure}

\subsection{Chemistry}
\label{subsec:chemres}

Figures~\ref{fig:chemdme} and \ref{fig:chemheptane} show performance results
for the chemistry kernels for both the DME and heptane mechanisms respectively.
Speedups for the warp-specialized kernels ranged from 1.01X to 1.50X over 
the baseline CUDA kernels.  Unlike the viscosity and diffusion kernels, the
performance characteristics of the chemistry kernels are very different.  The
very large working set places extreme pressure on the architectural resources
of any GPU.  Using warp specialization \Singe was able to emit code using an 
alternative allocation of resources which yielded performance gains.

The baseline CUDA chemistry kernels spill significant amounts of memory due
to the large working sets required.  For the heptane mechanism 8736 bytes
are spilled per thread on Fermi and 8500 bytes per thread are spilled on Kepler.  With enough
occupancy all this latency can be hidden, but results in a memory bandwidth
limited kernel.  After analyzing the SASS for the baseline CUDA kernels, we
measured memory bandwidths of 85 GB/s on Fermi and 100 GB/s on Kepler, which are consistent
with actual peak memory bandwidths for these architectures\footnote{We have
measured read bandwidths of 165 GB/s on a Kepler K20c with five GDDR5 memory partitions
when using LDG texture loads, but this path is not available to local memory since 
local memory is not constant.  100 GB/s load bandwidth is consistent with measured load bandwidths 
through the much smaller pipe in the L1 cache with only 13 SMs.}.

Alternatively, the warp-specialized kernels emitted by \Singe for heptane spill 
only 276 bytes on Fermi and 44 bytes on Kepler (mostly infrequently accessed 
pointer data).  Almost the entire working set of the computation including all 
forward and reverse reaction rates remain in registers at the cost that they 
must be repeatedly exchanged through shared memory.  Neither the Fermi nor the Kepler 
warp-specialized kernels have enough warps to hide the 30 cycles of shared memory
access latency with only 20 and 16 warps per SM respectively.  As a result, the warp-specialized
versions of the kernel are shared memory latency limited.  However, the performance
results demonstrate that this is significantly better than being global memory bandwidth limited.

%Using warp specialization \Singe was able to find an alternative allocation of
%resources that resulted in higher performance.  Additionally, since the warp-specialized
%kernels are limited by shared memory latency, they are better positioned for future
%GPU architectures with larger on-chip memories that will support additional
%occupancy for latency hiding.  Since on-chip memory sizes are growing faster than
%global memory bandwidth, the \Singe kernels will scale better than the data-parallel
%kernels. Warp
%specialization made it possible for \Singe to better explore the use of GPU resources 
%through computation partitioning, resulting in the interesting design point epitomized
%by the improved chemistry kernels.


