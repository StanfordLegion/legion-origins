
\section{Warp-Specialized Partitioning}
\label{sec:kernels}
Warp-specialized partitioning provides a useful mechanism for DSL compilers 
when grappling with computations that exhibit both irregularity
and large working sets.  While warp-specialized partitioning is a useful
tool, it is important to note that the particular method for partitioning
a computation into specialized warps relies on both domain specific knowledge
and the target architecture.  Therefore the partitioning strategy
must vary with each DSL compiler and we cannot provide a general
partitioning algorithm.  Instead, we provide a case study of coupling
domain specific information with warp specialization to address performance
problems in the domain of large and complex combustion applications.  Subsequent
sections will cover generally applicable techniques for mapping and
scheduling computations after they have been partitioned using domain specific
knowledge.

We begin with a brief overview of the combustion domain in 
Section~\ref{subsec:over}.  We then describe how warp-specialized
partitioning is used by \Singe to address the performance challenges inherent
in the generation of three expensive combustion kernels:
viscosity (Section~\ref{subsec:visc}), diffusion (Section~\ref{subsec:diff})
and chemistry (Section~\ref{subsec:chem}).

\subsection{Combustion Chemistry}
\label{subsec:over}
Combustion simulations are described by {\em chemical mechanisms}
which consist of a set of {\em reactions} and the
{\em species} involved in those reactions.  Chemical species 
range from single elements to very large and complex
hydro-carbons.  Table~\ref{fig:mechanisms} shows the characteristics
of the Dimethyl Ether (DME) and n-Heptane mechanisms 
used in this paper.  These mechanisms were chosen for
their relevance in current combustion research\cite{LU09}.

\begin{figure}
  \centering
{\small
  \begin{tabular}{c|cccc}
  Mechanism & Reactions & Species & QSSA & Stiff \\ \hline 
  DME       &  175      &  39     &  9   &  22   \\ 
  Heptane   &  283      &  68     &  16  &  27   \\ 
  \end{tabular}
}
\caption{Chemical Mechanisms\label{fig:mechanisms}}
\vspace{-0.5cm}
\end{figure}

Combustion mechanisms are described by a declarative data DSL 
based on the CHEMKIN\cite{CHEMKIN96} standard.  A 
mechanism in this DSL is described by three input files:
\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
  \item CHEMKIN file: a list of chemical reactions with
    stoichiometric coefficients and reaction models (see Figure~\ref{fig:input})
  \item TRANSPORT file: a table of diffusion and viscosity
    coefficients for all chemical species
  \item THERMO file: a table of thermodynamic coefficients
    for all chemical species
\end{itemize}
\Singe parses these files and emits CUDA code for each of the kernels
necessary to simulate combustion of the specified chemical mechanism.  \Singe may
also take an optional fourth input file 
describing the set of {\em quasi-steady-state approximation}
(QSSA) and {\em stiffness} (Stiff) species.  QSSA species arise out
of techniques that reduce the total number of species across all
phases of the simulation at the expense of additional computation during the
chemistry phase of the application\cite{LU09}.  For example, in 
the heptane mechanism, the 16 QSSA species are removed from 
the original group of 68 so that only a total of 52 species 
must be simulated, while requiring a complex QSSA computation
be performed in the chemistry kernel.  Stiffness species allow the simulation 
to take longer time steps, but require additional computations
be performed in the chemistry kernel.  We describe how warp 
specialization handles both the QSSA and stiffness computations 
in Section~\ref{subsec:chem}.

Most combustion simulations operate on a three dimensional cartesian
grid.  Each point in the grid has an associated set of {\em fields}
with each field laid out contiguously in a separate array to ensure
coalescing of global memory loads.  In most GPU combustion
kernels\cite{S3DACC12}, each
thread is responsible for a single point in the cartesian
grid, which conforms to the traditional data-parallel
GPU programming model.

\subsection{Viscosity}
\label{subsec:visc}
The viscosity kernel computes a viscosity coefficient for each point
in the cartesian grid as a function of the temperature and molar
fractions for each species.  Per-species viscosities are first
computed by taking the exponent of a per-species third order 
polynomial dependent on temperature ($T$):
\begin{equation*}
vis_i(T) = e^{{\eta}_{i0} + {\eta}_{i1}*T + {\eta}_{i2}*T^2 + {\eta}_{i3}*T^3}
\end{equation*}
The final viscosity output $\nu$ for each cartesian grid point is then
computed as an interaction of the viscosity of each species with 
every other species given by the following equation:
\begin{equation*}
\nu = \sqrt{8} * \sum_{k=1}^N \left[\begin{array}{l}\\
  \frac{x_k*vis_k}{\displaystyle\sum\limits_{j=1}^N x_j * 
  \frac{\left(1+\sqrt{\frac{vis_k}{vis_j}*\sqrt{\frac{m_j}{m_k}}}\right)^2}
    {\sqrt{1+\frac{m_k}{m_j}}}}\end{array}\right]
\end{equation*}
where $N$ is the number of species and $x_i$ and $m_i$ are the
molar fraction and molecular mass of species $i$ respectively.
In an optimized CUDA implementation, this computation is performed
in logarithmic space to reduce the dependency on square root and
divide operations that are implemented using Newton's method in
the absence of dedicated hardware on GPUs.  After constant folding,
for each of the $N^2$ pairs of species the viscosity kernel requires 
that 2 double precision constants be loaded and that 2 double precision adds,
2 double precision multiplies, and 10 double precision fused
multiply-add (DFMA) operations be performed.

The viscosity computation is embarrassingly parallel as each point
in the grid can be computed independently, but it places a
significant strain on several GPU resources.  First, 
the working set for a cartesian grid point is difficult to fit on chip into a
single thread's registers.  For the heptane mechanism with 52 species,
storing the molar fractions and per-species viscosity values requires
104 double precision values, which would require 208 registers on an
NVIDIA GPU.  Fermi GPUs only support 64 registers per thread, while Kepler
GPUs support 256 but at the cost of extremely low warp occupancy and
under-utilized math units.  In this scenario, a data parallel GPU
programming model forces the DSL compiler to choose between low-occupancy
or spilling registers, which adds additional memory latency to the kernel.

\begin{figure}
\begin{verbatim}
!1
  ch3+h(+m) = ch4(+m) 2.138e+15  -0.40  0.000E+00
           low / 3.310E+30 -4.00 2108. /
    troe/0.0  1.E-15  1.E-15  40./ h2/2/ h2o/5/ 
!2
  ch4+h  =  ch3+h2  1.727E+04  3.00  8.224E+03
    rev /  6.610E+02  3.00  7.744E+03 /
!3
  ch4+oh  =  ch3+h2o  1.930E+05  2.40  2.106E+03
    rev /  3.199E+04  2.40  1.678E+04 /
...
\end{verbatim}
\caption{Example CHEMKIN input file to \Singe.\label{fig:input}}
\vspace{-0.70cm}
\end{figure}

A second problem encountered by the viscosity kernel has to do with
the large number of constant values required for the computation.  Every
pair of species requires two different double precision constants.  GPU 
architectures include constant caches, but their working set sizes are 
small.  GPUs only have 8 KB of on-chip constant 
cache\cite{CUDA}.  The DME and Heptane mechanisms require 13.9 and 42.4 KB 
of constants respectively.  Loading constants for the viscosity 
kernel is therefore expensive since they are unlikely to hit in the constant 
cache.  The problem of hiding these long-latency constant loads is exacerbated 
by the low occupancy of the kernel caused by the large working set 
described earlier.

\Singe uses warp-specialized partitioning to solve both of these problems.  
The outer sum over the set of chemical species is broken into individual
computations each of which is mapped to a different warp using the algorithm
described in Section~\ref{subsec:mapping}.  Unlike data-parallel
CUDA where each thread handles a single point, all of the warps in a CTA cooperate 
on a set of 32 points.  A thread in lane $l$ of warp $w$ handles the per-species computations 
assigned to warp $w$ for the $l$-th point.  Warp specialization necessitates the
sharing of data between warps, therefore the molar fractions and per-species
viscosities are moved into shared memory.  These values fit easily because each 
CTA is only handling 32 grid points at a time.

Partitioning the computation for warp specialization also provides a solution
to the problem of storing the large number of constant values in on-chip
memory.  Since each warp is only performing a subset of the computation, it
only requires a subset of the constant values.  Furthermore, moving the working
set to shared memory makes the registers available for storing constants.
Using the constant deduplication optimization described in Section~\ref{subsec:dedup},
warp specialization enables all the constant values to be stored in on-chip registers.
At the end of the computation, all the warps reduce their values through shared
memory and the threads in warp 0 perform the write of the resulting values
for each point.  Using warp specialization, \Singe is able to partition the
viscosity computation so that values better fit into the on-chip memories
which we will show leads to significant performance gains in Section~\ref{sec:results}.

\subsection{Diffusion}
\label{subsec:diff}
The diffusion computation shares some characteristics with the viscosity computation
which leads to similar challenges, but requires a more complex warp-specialized
partitioning scheme.  For every pair of species $i$ and $j$, a diffusive constant
is computed from the exponent of a third order polynomial dependent on the temperature
$T$ where $\delta$ is a $N$x$N$x4 matrix of coefficients.
\begin{equation*}
d_{ij}(T) = e^{{\delta}_{ij0} + {\delta}_{ij1}*T + {\delta}_{ij2}*T^2 + {\delta}_{ij3}*T^3}
\end{equation*}
The $N$x$N$ matrix of these diffusive constants is then used in computing the
per-species diffusion outputs $\Delta_i$:
\begin{eqnarray*}
&& mass = \sum_{j=1}^N m_j*x_j \\
&& clamp_i = max(\epsilon,x_i) \\
&& \Delta_i = \frac{P_{atmos}}{P} * \frac{-clamp_i*m_i + \sum_{j=1}^N clamp_j*m_j}
                    {mass * \sum_{j=1}^N clamp_j * d_{ij}}
\end{eqnarray*}
where $N$ is the number of species, $P$ is the pressure, $P_{atmos}$ is 
atmospheric pressure, $\epsilon$ is the minimum molar fraction, 
and $x_i$ and $m_i$ are the molar fraction and molecular mass of species 
$i$ respectively.  Note that unlike the viscosity computation, which computed
a single output value per point, the diffusion kernel computes one output value 
per species per point.  Furthermore the $d_{ij}$ matrix is symmetric with zeros
along the diagonal which implies that less than half of the matrix must actually 
be computed.  However, each $d_{ij}$ must contribute to both $\Delta_i$ and $\Delta_j$.

Like viscosity, diffusion suffers from the same large working set
and constant problems.  We again solve them using warp specialization, but
with a different partitioning strategy.  The $N$x$N$ matrix of $d_{ij}$
values is partitioned by column.  Columns are offset by one from each other
and only a subset of values in each column need be computed because of
the symmetric nature of the matrix.  For odd numbers of species, each
column must compute $\lfloor \frac{N}{2}\rfloor$ $d_{ij}$ values; for even
numbers of species the first $\frac{N}{2}$ columns compute $\frac{N}{2}$
$d_{ij}$ values and the second $\frac{N}{2}$ columns compute $\frac{N}{2}-1$
$d_{ij}$ values.  Figure~\ref{fig:diff_part} shows the assignment
of $d_{ij}$ value computations for matrices with $N=4$ and $N=5$ for
two warps.  `X' values indicate that the point need not be computed
because its symmetric point about the diagonal has already been computed.  

\begin{figure}
\begin{center}
\includegraphics[scale=0.50]{figs/Diffusion_Partition}
\end{center}
\caption{Diffusion partitioning between two warps for $N$=4,5. \label{fig:diff_part}}
\vspace{-0.5cm}
\end{figure}

Warps are assigned adjacent columns to maximize locality.  Each warp traverses
its set of columns and maintains a partial sum for the species in each of its
columns.  Additional partial sums for each species are also maintained 
in shared memory.  For a given row, the warp computes the $d_{ij}$ values 
and reduces them into the partial sums stored in registers for each column.  
The warp also computes a row partial sum and reduces it into the location for 
the corresponding row species in shared memory.  To avoid data races when 
accessing shared memory, named barriers are used to synchronize access to 
different regions of shared memory.

At the end of the computation, each warp reads the partial sums out of
shared memory for the species that it owns and sums the results with
the partial sums stored in the warp's registers.  The pressure scaling
ratio is then applied and each warp writes out results for its owned
species.  In this scenario, the warp-specialized partitioning of the 
diffusion computation results in a hybrid storage of the working set 
using both shared memory and registers that enables additional 
parallelism to be extracted.  We describe the algorithm for placing 
data in further detail in Section~\ref{subsec:mapping}.

\subsection{Chemistry}
\label{subsec:chem}
The chemistry computation is the most complex kernel that \Singe emits because it 
requires multiple phases, some of which can execute in parallel.  The first 
phase computes forward and reverse reaction
rates for every reaction using an Arrhenius, Lindermann,
or Landau-Teller reaction model\cite{CHEMKIN96}, requiring between 6 and 15 double precision
constants per reaction.  The second phase computes the QSSA 
scaling factors and then applies them
to all reactions involving a QSSA species.  A similar computation is
performed in the third phase for the stiff species.  The output phase
sums the contributions from each reaction and computes the resulting rate of 
change for each species based on stoichiometric coefficients.  

\begin{figure}
\begin{center}
\includegraphics[scale=0.50]{figs/Warp_Chemistry}
\end{center}
\caption{Warp specialization for chemistry kernel.\label{fig:warp_chem}}
\vspace{-0.5cm}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=0.50]{figs/Warp_QSSA}
\end{center}
\caption{Example partitioning of heptane QSSA computation.\label{fig:warp_qssa}}
\vspace{-0.5cm}
\end{figure}

While the chemistry kernel could be fissioned 
into separate kernels, each such kernel would read and write a 
forward and reverse reaction rate for every reaction at every point.  
Kernels for the heptane mechanism would each require 566 double precision 
reads and 566 double precision writes per cartesian grid point,  
which would be memory bandwidth limited and therefore slow.

\begin{figure*}
\centering
\includegraphics[scale=0.6]{figs/WarpSpecializingCompiler}
\caption{Architecture of a warp-specializing compiler.\label{fig:compiler}}
\vspace{-0.5cm}
\end{figure*}

Instead we employ warp-specialized partitioning to break apart the computation
and the working set so it fits in on-chip memory.  However, unlike viscosity 
and diffusion, for all interesting mechanisms the number of reaction rates is too large to store 
in shared memory.  Instead we partition the reaction computations 
across warps and store the resulting forward and reverse reaction rates in each warp's
registers.  When reaction rates are needed for computations, they are exchanged
in passes by writing subsets of the rates into shared memory and having the needed 
values read out before progressing to the next pass.  While this adds latency to 
the kernel, it is significantly less latency than going to off-chip global memory 
and well within the bandwidth limits of shared memory.

The chemistry kernel also suffers from another performance challenge: the QSSA phase 
is both computationally expensive and difficult to parallelize.  \Singe uses 
warp specialization at two different granularities to address this problem.  First,
the QSSA computation usually requires between half and two-thirds of the reaction
rates.  \Singe assigns these reactions to warps first.  A subset of the warps are
then siphoned off to perform the QSSA computation while the remaining warps complete
the reaction computations.  Figure~\ref{fig:warp_chem} shows an example of this 
partitioning of the warps for the chemistry kernel.  A producer-consumer named barrier 
is used to indicate to the QSSA warps when the needed values from the non-QSSA warps 
have been written into shared memory.  Note that because this barrier is non-blocking
warp specialization enables the QSSA computation to be overlapped with the remaining 
reaction rate computations, which cannot be done in the data-parallel version of the kernel.

Warp specialization is also used to further parallelize the QSSA computation.  The QSSA 
computation performs many divide operations and consequently requires between 20 
and 60 DFMA operations for each QSSA species.  Data dependences exist between species
further complicating parallelization.  \Singe uses a heuristic for partitioning
the directed acyclic graph (DAG) of a QSSA computation across a set of QSSA warps.
For every edge that crosses a warp boundary, a producer-consumer named barrier
is allocated.  Figure~\ref{fig:warp_qssa} shows an example of one such partitioning
for the QSSA computation for the heptane mechanism across two warps.  In 
Section~\ref{subsec:warpsync} we describe a general algorithm for synchronizing 
and scheduling these dataflow DAGs that avoids deadlock.

