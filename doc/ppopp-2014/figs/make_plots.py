#!/usr/bin/python

import subprocess
import sys, os, shutil
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages
#matplotlib.rcParams['ps.useafm'] = True
#matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

tableau1 = (0.968,0.714,0.824)
tableau2 = (0.882,0.478,0.470)
tableau3 = (0.565,0.663,0.792)
tableau4 = (0.635,0.635,0.635)
tableau5 = (0.678,0.545,0.788)
tableau6 = (1.000,0.620,0.290)
tableau7 = (0.859,0.859,0.553)
tableau8 = (0.769,0.612,0.580)
tableau9 = (0.478,0.757,0.424)
tableau10= (0.427,0.800,0.855)
tableau11= (0.929,0.592,0.792)
tableau12= (0.929,0.400,0.364)
tableau13= (0.447,0.620,0.808)
tableau14= (0.780,0.780,0.780)
tableau15= (0.773,0.690,0.835)
tableau16= (0.882,0.616,0.353)
tableau17= (0.804,0.800,0.365)
tableau18= (0.659,0.471,0.431)
tableau18= (0.404,0.749,0.361)
tableau19= (0.137,0.122,0.125)

problem_sizes = [32, 64, 128]

chem_heptane_slow_fermi = [17.533, 131.087, 1024.134]
chem_heptane_fast_fermi = [17.489, 119.156, 891.556]

chem_dme_slow_fermi = [13.706, 100.989, 791.364]
chem_dme_fast_fermi = [10.761, 71.865, 532.288]

chem_heptane_slow_kepler = [10.266, 76.705, 604.489]
chem_heptane_fast_kepler = [8.110, 54.656, 431.607]

chem_dme_slow_kepler = [7.224, 53.244, 417.500]
chem_dme_fast_kepler = [7.110, 35.565, 285.339]

#visc_heptane_slow_fermi = [39.061, 294.219, 2337.22]
visc_heptane_slow_fermi = [23.773, 177.352, 1389.982]
visc_heptane_fast_fermi = [18.093, 144.043, 1152.149]

#visc_dme_slow_fermi = [10.709, 80.176, 637.000]
visc_dme_slow_fermi = [7.316, 55.653, 434.026]
visc_dme_fast_fermi = [5.612, 44.351, 354.263]

#visc_heptane_slow_kepler = [37.798, 296.651, 2360.502]
visc_heptane_slow_kepler = [21.437, 169.311, 1352.180]
visc_heptane_fast_kepler = [5.715, 45.311, 361.073]

#visc_dme_slow_kepler = [8.828, 66.531, 527.591]
visc_dme_slow_kepler = [5.579, 43.242, 345.877]
visc_dme_fast_kepler = [2.666, 15.871, 122.922]

diff_heptane_slow_fermi = [16.766, 127.566, 1001.288]
diff_heptane_fast_fermi = [11.752, 93.688, 749.120]

diff_dme_slow_fermi = [5.162, 39.438, 314.163]
diff_dme_fast_fermi = [2.907, 22.951, 183.428]

diff_heptane_slow_kepler = [10.901, 85.941, 686.327]
diff_heptane_fast_kepler = [7.810, 33.276, 265.900]

diff_dme_slow_kepler = [2.631, 20.150, 160.216]
diff_dme_fast_kepler = [1.889, 11.203, 84.074]

visc_prf_slow_fermi = [160.887,1194.587,9528.406]
visc_prf_fast_fermi = [96.922,772.808,6175.588]

visc_prf_slow_kepler = [161.01,1255.867,9965.88]
visc_prf_fast_kepler = [35.084,280.126,2240.33]

diff_prf_slow_fermi = [78.556,606.808,4839.142]
diff_prf_fast_fermi = [82.503,659.269,5269.659]

diff_prf_slow_kepler = [51.186,384.986,3025.627]
diff_prf_fast_kepler = [28.336,225.956,1806.965]

def autolabel(rects, ax, max_height):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x()+rect.get_width()/2.0,
                1.05*height, '%.3f' % height,
                ha='center', va='bottom')
        if (1.20*height) > max_height:
            max_height = 1.20*height
    return max_height

def make_plot(slow, fast, expr, arch, mech, save):
    N = 3
    ind = np.arange(N)
    width = 0.4
    fig = plt.figure()
    ax = plt.gca()
    slow_through = list()
    fast_through = list()
    for idx in range(len(problem_sizes)):
        p = problem_sizes[idx]
        vol = p*p*p
        slow_through.append(float(vol)/(slow[idx]*1e3))
        fast_through.append(float(vol)/(fast[idx]*1e3))
    rects1 = ax.bar(ind, slow_through, width, color = tableau18)
    rects2 = ax.bar(ind+width, fast_through, width, color=tableau10)
    ax.set_ylabel('Throughput (Mpoints/s)')
    ax.set_xticks(ind+width)
    ax.set_xticklabels( ('32x32x32','64x64x64','128x128x128') )
    #ax.set_title(expr+" "+arch+" "+mech)
    ax.legend( (rects1[0], rects2[0]), ('Data-Parallel CUDA','Warp-Specialized'), loc=4)
    max_height = 0
    max_height = autolabel(rects1,ax,max_height)
    max_height = autolabel(rects2,ax,max_height)
    ax.set_ylim(ymax=max_height)
    if save:
        fig.savefig(expr+"_"+arch+"_"+mech+".pdf", 
                    format="pdf", bbox_inches="tight")

def make_line_plot(save):
    fig = plt.figure()
    warps = range(1,17)
    naive = [2.270,4.116,5.984,6.937,7.835,8.140,7.560,7.472,5.609,5.533,5.939,5.566,5.041,5.419,3.804,4.018]
    fast  = [2.154,4.252,6.143,7.093,10.338,12.828,12.305,15.097,14.115,17.073,13.842,13.822,12.922,13.426,17.061,16.341]
    plt.plot(warps,fast,label='Singe Warp Specialization', linestyle='-', color=tableau10, marker='s')
    plt.plot(warps,naive,label='Naive Warp Specialization', linestyle='-', color=tableau18, marker='o')
    plt.xlabel('Warps/CTA (with 3 CTAs/SM)')
    plt.ylabel('Throughput (Mpoints/s)')
    plt.legend(loc=2)
    plt.xlim(xmin=0,xmax=17)
    plt.ylim(ymin=0,ymax=20)
    if save:
        fig.savefig('naivespec.pdf',format="pdf",bbox_inches="tight")

def make_plots(show = True, save = True):
    make_plot(chem_heptane_slow_fermi,
              chem_heptane_fast_fermi,"chem","fermi","heptane",save)
    make_plot(chem_dme_slow_fermi,
              chem_dme_fast_fermi,"chem","fermi","dme",save)
    make_plot(chem_heptane_slow_kepler,
              chem_heptane_fast_kepler,"chem","kepler","heptane",save)
    make_plot(chem_dme_slow_kepler,
              chem_dme_fast_kepler,"chem","kepler","dme",save)
    make_plot(visc_heptane_slow_fermi,
              visc_heptane_fast_fermi,"visc","fermi","heptane",save)
    make_plot(visc_dme_slow_fermi,
              visc_dme_fast_fermi,"visc","fermi","dme",save)
    make_plot(visc_heptane_slow_kepler,
              visc_heptane_fast_kepler,"visc","kepler","heptane",save)
    make_plot(visc_dme_slow_kepler,
              visc_dme_fast_kepler,"visc","kepler","dme",save)
    make_plot(diff_heptane_slow_fermi,
              diff_heptane_fast_fermi,"diff","fermi","heptane",save)
    make_plot(diff_dme_slow_fermi,
              diff_dme_fast_fermi,"diff","fermi","dme",save)
    make_plot(diff_heptane_slow_kepler,
              diff_heptane_fast_kepler,"diff","kepler","heptane",save)
    make_plot(diff_dme_slow_kepler,
              diff_dme_fast_kepler,"diff","kepler","dme",save)
    make_plot(visc_prf_slow_fermi,
              visc_prf_fast_fermi,"visc","fermi","prf",save)
    make_plot(visc_prf_slow_kepler,
              visc_prf_fast_kepler,"visc","kepler","prf",save)
    make_plot(diff_prf_slow_fermi,
              diff_prf_fast_fermi,"diff","fermi","prf",save)
    make_plot(diff_prf_slow_kepler,
              diff_prf_fast_kepler,"diff","kepler","prf",save)
    make_line_plot(save)
    if show:
        plt.show()

if __name__ == "__main__":
    make_plots(not("-s" in sys.argv), True)
