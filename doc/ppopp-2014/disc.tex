
\section{Discussion}
\label{sec:disc}
While we have shown that warp specialization is effective on NVIDIA GPUs, we 
believe that similar benefits could be realized on other wide-vector SIMD architectures,
such as Intel's Xeon Phi.  Conceptually, a warp is a single instruction stream
issuing 32-wide vector instructions, which is not far removed from a thread
on a Xeon Phi issuing 8-wide double-precision vector instructions.  Instead
of partitioning and mapping computations onto warps, computations could be 
mapped onto vectorized threads.  Identical to warp specialization, the
techniques presented in this paper could be used to fit large working sets 
into on-chip memories and handle irregularity.  There are currently two absent 
hardware features on the Xeon Phi that inhibit this approach.  First, there are 
no fast hardware synchronization primitives equivalent to named barriers present 
(heavy-weight software mutexes are currently necessary).  Second, there are no 
on-chip software-managed memories to be used for fast data exchange buffers.  
Instead the hardware-managed L1 cache must be used which is subject to 
interference and eviction by other data movement operations.

The presence of hardware-managed caches on GPUs also hinders the performance
of warp-specializing compilers like \Singe.  Hardware-managed caches consume
transistors which could have been used to fit larger working sets in on-chip 
software-managed memories.  Hardware-managed caches are also difficult resources
for compilers to reason about.  An earlier version of \Singe attempted to store
the large number of constant values in the GPUs hardware-managed L2 cache.
However, writes from other threads kept evicting constants off-chip despite
\Singe annotating all global writes with the PTX cache-streaming 
qualifier {\tt cs}.  The current \Singe compiler completely ignores the L2
cache which wastes significant on-chip memory (768 KB on Fermi and 1536 KB on Kepler).  
Finally, hardware-managed caches add overhead to the kernels emitted by \Singe 
both in terms of performance (tag look-ups on all memory accesses) and power 
(unnecessary data movement between cache levels).  Ideally, future GPUs and 
other wide-vector architectures will consist primarily of software-managed 
caches which will enable compilers like \Singe to directly orchestrate 
data movement leading to both higher performance and power efficiency.

The poor performance of current GPU instruction caches makes warp 
specialization infeasible without overlaying code paths.  Instruction
caches designed for handling many divergent warps in future GPUs would
remove the need for overlaying code using the techniques described in
Section~\ref{sec:codegen}.  This would improve the performance of
\Singe kernels by significantly reducing the number of necessary
warp-specific branch instructions.  Additionally, it would greatly 
reduce the complexity of the code generation stage for warp-specializing
compilers.  It would also make writing warp-specialized code practical 
for human programmers.

Finally, a common misconception regarding warp specialization is that it
will be made obsolete by Moore's law because more transistors in future chips 
will allow current large working sets to fit on-chip.  Ideally, more on-chip storage 
could lead to higher occupancy and would only require a simple 
data-parallel programming model to achieve high performance.  Unfortunately, 
this view fails to consider that many computational science domains, 
including combustion, cosmology, and molecular dynamics, all limit the 
scope of their simulations in order to execute efficiently on current hardware.  
For example, the combustion mechanisms described in this paper are 
considered {\em reduced} mechanisms because they only simulate tens of chemical 
species instead of the hundreds and thousands of species in real mechanisms\cite{LU09}.  
In our experience, computational scientists will always use additional hardware 
to perform higher fidelity but more computationally expensive simulations instead
of making current simulations perform better.  As machines become more powerful, 
scientists will devise more complex simulations that place intense pressure on 
both programming systems and hardware.  Under such conditions,
data-parallel programming models such as CUDA, OpenCL, and especially OpenACC
will struggle to take full advantage of future hardware.  Ultimately, programming models
such as warp specialization will become essential for managing 
large working sets, handling irregularity, and exploiting task-level parallelism 
to fully leverage forthcoming architectures for general purpose computing.

