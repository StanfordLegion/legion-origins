
\section{Warp Specialization}
\label{sec:warpspec}
While there are 
several APIs for programming GPUs, they all implement variations of
the same programming model.  We use CUDA as a proxy for the standard 
GPU programming model as it is the only interface that currently supports the 
fine-grain synchronization primitives necessary for warp specialization.

CUDA launches grids of thread blocks or {\em cooperative thread arrays}
(referred to as CTAs for the remainder of the paper) on the GPU.  This abstraction gives 
the hardware considerable flexibility when executing a CUDA application.  
In current GPUs, the threads within a CTA are broken into groups of 
32 threads called {\em warps}.  All threads within a CTA 
(and therefore also within a warp) execute the same program.  If 
the threads within a warp {\em diverge} on a branch instruction, the 
streaming multiprocessor (SM) on which the warp is executing first 
executes the warp with all the threads not taking the branch masked
off.  After the taken branch is handled, the warp is re-executed
for the not-taken branch with the complementary set of threads masked 
off from executing.  Divergence is severely detrimental to program
performance because it serializes potentially parallel thread execution within a warp.
%Divergence can significantly reduce performance because 
%not all the threads of the warp can execute in parallel.

The crucial insight for warp specialization is that while
control divergence within a warp results in performance
degradation, divergence between warps does not.  A 
warp-specialized kernel is one in which dynamic branches,
dependent on each thread's warp ID, create explicit 
inter-warp control divergence for the purpose of executing
different code in each warp.  As long as all threads
within a warp execute the same instruction stream then the only
execution overhead is the cost of the 
warp-specific branch instructions.

%The crucial insight for warp specialization is that control
%divergenece between warps does not incur a performance penalty.
%As long as all threads within a warp execute the same code path,
%then the only overhead is the cost of the branch instruction.
%A warp-specialized kernel is one in which dynamic branches,
%dependent on each thread's warp ID, are used to cause warps to
%execute different code.  As long as all the threads within
%a warp execute the same instructions, there is minimal 
%execution overhead.

Warp-specialized programs also require more expressive
synchronization mechanisms.
In traditional CUDA programs synchronization is performed using
barriers between all the threads within a CTA.  However, by using 
inline PTX statements, a CUDA program has access to a more expressive set of 
intra-CTA synchronization primitives referred to as {\em named barriers}\cite{PTX}.  
Named barriers provide two operations: {\em arrive} and {\em sync}.  
Arrive is a non-blocking operation which registers that a warp has 
arrived at a barrier and then continues execution.  Sync is a blocking
operation that waits until all the necessary warps have arrived
or synced on the barrier.  

\begin{figure}
\begin{center}
\includegraphics[width=0.45\textwidth]{figs/named_barriers}
\caption{Producer-consumer named barriers example.\label{fig:namedbarriers}}
\vspace{-0.5cm}
\end{center}
\end{figure}

Using arrive and sync operations, programmers can encode producer-consumer 
relationships in warp-specialized programs.  Figure~\ref{fig:namedbarriers} 
illustrates using two named barriers to coordinate
movement of data from a producer warp (red) to a consumer warp (blue)
through a buffer in shared memory.  The producer warp first waits for 
a signal from the consumer warp that the buffer is empty.  The
consumer warp signals the buffer is ready by performing a non-blocking
arrive operation.  Since the arrive is non-blocking, the consumer warp
is free to perform additional work while waiting for the buffer to be
filled.  At some point the consumer warp blocks on the second named barrier
waiting for the buffer to be full.  The producer warp signals when
the buffer is full using a non-blocking arrive operation on the second
named barrier.  It is important to note that named barriers support
synchronization between arbitrary subsets of warps within a CTA, including
allowing synchronization between a single pair of warps as in this example.

