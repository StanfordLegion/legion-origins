
\newtheorem{thm}{Theorem}

\section{Named Barrier Placement}
\label{sec:warpsync}
Traditional data-parallel GPU programming models present a very
simple coarse-grain barrier for synchronization within a CTA.  Named
barriers provide a finer granularity of synchronization between
individual warps, but can lead to poor performance and deadlock if
placed incorrectly.  In this section we describe an algorithm for
placing named barrier synchronization calls that guarantees a 
deadlock-free schedule.  We also present a class of transformations
on the schedule that can safely be performed by the compiler
during optimization passes.

We assume an input similar to the QSSA computation DAG shown in 
Figure~\ref{fig:warp_qssa} with nodes representing computations to be
performed and arcs indicating data dependences that must be observed between
the computations.  The DSL compiler partitions the graph by assigning
computations to warps using domain specific knowledge.  We then perform 
the following actions to generate a deadlock-free schedule for the 
operations in each warp.  
\begin{enumerate} \itemsep1pt \parskip0pt \parsep0pt
  \item Tag every data dependence between two nodes assigned to different
        warps as requiring a {\em synchronization point}, with the producer warp 
        needing to perform an arrival and the consumer warp needing to perform a wait.
  \item Construct a partial order of the synchronization points based on
        their transitive data dependences.
  \item Linearize the partial order of all synchronization points and then number
        each synchronization point, defining a total order on synchronization points.
  \item For each warp, create a static schedule of all the operations in that warp
        that obeys operational data dependences and obeys the total ordering on
        synchronization points so that an operation with a lower numbered synchronization
        point comes before an operation with a higher numbered synchronization point.
\end{enumerate}
We now prove that using this algorithm guarantees the existence of at least one
schedule that is deadlock-free.
\begin{thm}
\label{thm:schedule}
\rm
A schedule for each warp that obeys the initial data dependences and a total
ordering on synchronization points exists and is deadlock-free.
\end{thm}
\begin{proof}
Initially the graph of operations is a DAG which defines a partial ordering on
the operations.  For every operation which requires a synchronization point we
add an additional edge to every operation that requires a synchronization point
with a larger number.  By construction, these edges also obey the partial order
of the original DAG since the total order on synchronization points was constrained
by the original data dependences.  Therefore a partial order still exists on
the graph which guarantees that the graph is still a DAG.  From any partial order,
there exists at least one total ordering on all the operations.  An initial
schedule for a warp can be then be found by removing the operations assigned
to each warp in order from any total ordering of all operations.  The DAG nature
of the resulting graph ensures the absence of any cycles on control resources
which could result in deadlock and therefore the resulting schedule is deadlock-free.
\end{proof}

After an initial deadlock-free schedule has been generated for each warp, there
are several re-ordering transformations permitted that do not invalidate 
Theorem~\ref{thm:schedule}.
\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
  \item Operations arriving at a synchronization point can be re-ordered up the
        schedule arbitrarily far, or can be moved down the schedule if they
        are not re-ordered with respect to an operation that waits on a higher-numbered 
	synchronization point.
  \item Operations waiting at a synchronization point can be re-ordered up the schedule
        if they are not re-ordered with respect to a lower-numbered 
        synchronization point, or can be re-ordered down the schedule
        if they are not re-ordered with respect to operations on a higher-numbered 
	synchronization point.
\end{itemize}
Optimizing a schedule is an NP-hard problem that also depends on other factors
such as the computational cost of each of the operations.  By outlining the constraints
placed on each operation we have given a DSL compiler several degrees of freedom with
which to optimize the scheduling of operations without needing to be concerned with deadlock.

\begin{figure}
\centering
\subfigure[Fermi]{
\includegraphics[width=0.38\textwidth]{figs/visc_fermi_dme}
\label{fig:viscfermidme}
}
\subfigure[Kepler]{
\includegraphics[width=0.38\textwidth]{figs/visc_kepler_dme}
\label{fig:visckeplerdme}
}
\caption{Viscosity performance results for DME.\label{fig:viscdme}}
\vspace{-0.5cm}
\end{figure}

\begin{figure}
\centering
\subfigure[Fermi]{
\includegraphics[width=0.38\textwidth]{figs/visc_fermi_heptane}
\label{fig:viscfermiheptane}
}
\subfigure[Kepler]{
\includegraphics[width=0.38\textwidth]{figs/visc_kepler_heptane}
\label{fig:visckeplerheptane}
}
\caption{Viscosity performance results for heptane.\label{fig:vischeptane}}
\vspace{-0.5cm}
\end{figure}

After the schedule for each warp has been finalized, the DSL compiler must map the set
of synchronization points onto the set of 16 named barriers per SM available on both
Fermi and Kepler architectures\footnote{If the desired occupancy is more than one CTA
per SM, then the maximum number of named barriers per CTA is 16 divided by the number of 
desired CTAs per SM.}.  A named barrier can only be re-used if there exists transitive 
dependences from all prior uses of the named barrier.  The problem of mapping synchronization
points onto named barriers is isomorphic to the problem of register allocation for static
single-assignment (SSA) code.  This problem is known to be NP-complete, but there are
many good heuristics in the compiler literature for addressing it\cite{PurpleDragon}.
\Singe uses a simple greedy algorithm relying on an interference graph
to assign synchronization points to named barriers.  In practice, only the heptane chemistry
kernel with 4 QSSA warps has required the use of all 16 named barriers.


