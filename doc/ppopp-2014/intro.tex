
\section{Introduction}
\label{sec:intro}

Current GPU programming models, such 
as OpenCL\cite{Khronos:OpenCL} and CUDA\cite{CUDA},
support data-parallel computations where
all threads execute the same instruction stream on arrays of data.
However, the expansion of GPUs into general purpose computing
has uncovered many applications which exhibit properties
which make them challenging to map onto traditional data-parallel 
GPU programming models.  For example, consider the domain of combustion
science, which includes applications such as S3D\cite{S3D09,S3DACC12}.
The physics and chemistry kernels for these combustion applications have
three characteristics that make them difficult to optimize for GPUs:
%In practice, however, there are two common classes of computations that
%are not well-supported by a pure data parallel model:  first, irregular computations in
%which parallel threads execute divergent instruction streams, make divergent memory
%references, or both; and
%second, applications that, even if they are data parallel, have working sets
%that are too large to fit on-chip and so become memory bound.
%One example domain that meets these criteria is 
%combustion simulation, which includes applications such as
%S3D\cite{S3D09,S3DACC12} and more general CHEMKIN solvers\cite{CHEMKIN96}.  
%The physics and chemistry kernels for these combustion
%applications have three characteristics that make them
%difficult to run well on GPUs:
\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
  \item Large working sets: combustion kernels routinely
    require hundreds of live double-precision variables per 
    physical point in discretized space.  In a data-parallel
    model these working sets commonly exceed the small on-chip
    memory capacity allotted to each thread, resulting in register
    spilling, low occupancy, and under-utilization of math units.
  \item Irregular computations: combustion kernels often contain 
    multiple phases with different computational characteristics.
    The large number of temporaries between phases necessitates
    that the phases be fused together into a single kernel.  While some 
    of these phases could be run in parallel, current data-parallel 
    GPU programming models serialize these phases.
  \item Irregular data accesses: data accesses in combustion kernels are dependent
    both on the properties of the chemical mechanism as well as 
    runtime values.  Under many circumstances it is impossible for
    a data-parallel model to avoid memory divergence and shared
    memory bank conflicts.
\end{itemize}

\begin{figure}
\centering
\subfigure[Data-Parallel]{
\includegraphics[width=0.45\textwidth]{figs/DataParallel}
\label{fig:dataparallel}
}
\qquad
\subfigure[Warp Specialization]{
\includegraphics[width=0.45\textwidth]{figs/WarpSpecialization}
\label{fig:warpspec}
}
\caption{Contrasting GPU programming models.\label{fig:compare}}
\vspace{-0.5cm}
\end{figure}

In practice, many different scientific computing domains demonstrate some
or all of the same characteristics as combustion.  Achieving peak performance
for these applications mandates finding alternative approaches to 
programming GPUs.

In this work we describe how {\em warp specialization} can be used as
an alternative programming model for mapping irregular and
large working set applications onto GPUs.  Figure~\ref{fig:compare}
illustrates the differences between the core data-parallel abstraction
of current GPU programming models and warp specialization.  In the data-parallel
model, a collection of threads within a {\em thread block} all execute the same 
program over independent elements from arrays of input data.  On the hardware, however, a thread block 
is broken into {\em warps} consisting of (typically) 32 threads which serve as 
the unit of scheduling.  Warp specialization exploits the division of a thread block
into warps to partition computations into sub-computations such that each sub-computation 
is executed by a different warp within a thread block.  Carefully structured
programs can handle irregularity by grouping threads into warps such that threads 
within a warp have good data-parallel behavior, even if threads in different warps 
do not.  Warp specialization can also be used to partition extremely large working
sets across multiple threads in different warps, keeping data on-chip and dramatically 
reducing the register pressure on an individual thread.
%Previous work on warp specialization focuses on breaking algorithms into data- 
%and compute-intensive components using a library abstraction\cite{CudaDMA11}.

We describe the design and implementation of a Domain Specific Language (DSL)
compiler capable of using warp specialization in conjunction with domain
specific knowledge to better map challenging kernels onto GPU architectures.
Our approach has been implemented in \Singe, a DSL compiler for general 
combustion simulations. While \Singe targets combustion specifically, the
techniques described in this paper are general and can be adapted
to other domains.  We show that using these techniques, \Singe
generates warp-specialized code that achieves speedups up to
3.75X over heavily optimized but purely data-parallel combustion kernels.

In Section~\ref{sec:warpspec} we give a brief introduction to
the mechanics of warp specialization.  Each of the following 
sections describe one of our primary contributions.
\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
  \item We present a case study of how \Singe uses warp specialization
    to partition the three most expensive kernels in a real-world
    combustion application.  We elucidate how warp-specialized partitioning
    addresses the critical performance bottlenecks in these kernels (Section~\ref{sec:kernels}).
  \item We describe the architecture of a warp-specializing compiler including
    the necessary algorithms for managing data placement, communication, and
    synchronization for general warp-specialized kernels (Section~\ref{sec:arch}).
  \item We cover three general techniques for generating high performance 
    warp-specialized code that are essential to avoiding instruction cache
    thrashing.  We give code examples of how \Singe employs 
    these techniques (Section~\ref{sec:codegen}).
  %\item We present an algorithm for performing fine-grain
  %  synchronization in warp-specialized programs and prove that
  %  it is deadlock-free (Section~\ref{sec:warpsync}).
  \item We investigate the performance advantages conferred
    by warp specialization by examining the performance of
    the kernels emitted by \Singe on both Fermi and Kepler
    GPUs for two different chemical mechanisms (Section~\ref{sec:results}).
\end{itemize}
Section~\ref{sec:disc} discusses implications of warp specialization,
Section~\ref{sec:related} describes related work, 
and Section~\ref{sec:conclusion} concludes.


%To alleviate this problem, we propose using {\em warp specializaton} 
%as an alternative mechanism for implementing DSL compilers 
%that generate code for GPUs.  Warp specialization is an existing 
%technique\cite{CudaDMA11} for partitioning computations into
%sub-computations which are then executed by different sub-sets
%of threads within a thread block.  While previous work on
%warp specialization has focused on breaking computations into 
%data-intensive and memory-intensive components\cite{CudaDMA11},
%we instead describe an approach that encourages decomposing
%computations into tasks which are executed by different warps.

%Task-based warp specialization confers several benefits to the
%DSL compiler.  First different warps can employ different
%addressing schemes for managing irregular data access patterns.
%Second, task-based warp specialization can leverage task-parallelism
%by scheduling different computations to run on parallel on 
%different warps.  Finally, in the case of applications with very
%large working sets, computations can be divided and assigned to
%different warps to keep the working set entirely on-chip.

%The rest of the paper is organized as follows: in Section~\ref{sec:warpspec}
%we give a brief introduction to the mechanics of how warp specialization
%is implemented.  The remaining sections each describe one of our contributions:  
%\begin{itemize}
%\item Section~\ref{sec:kernels} how we apply warp specialization in the
%  \Singe DSL compiler to generate high performance GPU kernels for
%  a combustion chemistry application.
%\item In Section~\ref{sec:opt} we describe four optimizations made possible
%  by warp specialization that are 
%\end{itemize}

