%sri

\section{Introduction}
A failure during a large-scale execution of any application on an extreme-scale
system leads to loss of time and money, and can cause a nightmare to any
application developer. While failures could be due to application bugs,
soft failures are becoming more prevelant.  For example, Geist states that such
failures are a common occurrence on Oak Ridge National Laboratories leadership
class machines~\cite{errors_ecc}, and Schroeder and Gibson state that a large
number of CPU and memory failures were from parity errors after tracking a
five-year log of hardware replacements for a $765$ node high-performance
computing cluster~\cite{schroeder_gibson}.  %Echoing the sentiment of Snir et
%al.~\cite{snir}, we believe that it is critical to overcome the effect of such
%failures through a productive and performant resilience interface in the
%parallel programming model so that a developer can avail the computing power on
%extreme- and upcoming exa-scale parallel systems.
A well-known solution to address soft failures is checkpoint-restart. In this
solution, periodic global checkpoints of the application's control and data are
preserved and used for recovery in the event of a failure. However, this solution
will not be able to handle a high rate of soft failures. This is because most of
the execution time will be spent in checkpointing/recovering without making much 
application progress~\cite{cds}. Therefore, fine-grained recovery of failures
within a running that doesn't rely on checkpoint/restart is the most pressing
aspect of resilience.

%- But its extremely difficult (if not impossible) to design a universal
%fine-grained recovery mechanism because applications are all different - Hence
%we need a flexible infrastructure for fine-grained soft-fault resilience (e.g.
%resilience a la carte) that allows applications to specify what should be
%protected (policy) and the system provides the implementation (mechanism) that
%does that caries out the policy with minimal programmer effort - Here's how we
%can make this a reality in Legion...

\subsection{Motivation}

\begin{figure*}[t]
\centering
\captionsetup{justification=centering}
\includegraphics[width=.72\textwidth]{images/soleil_x_particlepush.png}
\caption{A portion of the dataflow graph resulting from the execution of
Soleil-X. The dataflow graph was produced by Legion Spy, where different colored
nodes represent different tasks. Edges between nodes represent data and control
dependences.} 
\label{soleil_x_particlepush}
\end{figure*}

Consider the dataflow graph resulting from an execution of Soleil-X, a
turbulence/particle/radiation solver written in Legion~\cite{soleilx}, shown in
Figure 
~\ref{soleil_x_particlepush}. The nodes in the graph represent tasks in the
application. The edges indicate the flow of data from one task to another. In this
execution, if the highlighted task, `particles\_pushall' fails, then ensuring
resilience requires answers to the following questions:
\begin{enumerate}
\item Who detects the failure, stops it from propagating, and triggers the
recovery mechanism ? programmer or the runtime ?
\item Which tasks need to be restarted in the event of a failure ? Based on
the availability of data checkpoints, a runtime can determine the tasks that
need to be restarted. In contrast, a programmer can choose to identify the tasks
that need to be restarted, e.g., in the X10 programming model, a programmer
chooses what-to do in the \texttt{catch} block for a 
\texttt{DeadPlaceException}~\cite{x10}.
\item Which variant of a task needs to be restarted, CPU or GPU or co-processor
variant ? 
\item Whether to take a snapshot of the inputs to a task or not ? 
\item Who ensures the sequential semantics in the event of a failure ? 
\end{enumerate}

%- But its extremely difficult (if not impossible) to design a universal
%fine-grained recovery mechanism because applications are all different - Hence
%we need a flexible infrastructure for fine-grained soft-fault resilience (e.g.
%resilience a la carte) that allows applications to specify what should be
%protected (policy) and the system provides the implementation (mechanism) that
%does that caries out the policy with minimal programmer effort - Here's how we
%can make this a reality in Legion...

% for fine-grained soft-fault resilience (e.g.
%resilience a la carte) that 
A productive flexible resilience infrastructure for fine-grained soft-failures 
allows applications to specify what should be protected (policy, questions 3\&4
above) and the system
provides the implementation (mechanism, questions 1,2\&5 above) that carries out the policy
with minimal programmer effort. We claim that this policy and mechanism based
resilience infrastructure is more progressive than the ones supported in state-of-the-art parallel models, e.g.,
(a) X10 specifies the semantics of failure recovery alone~\cite{x10}, which is
insufficient. It defines the semantics of resilient finish blocks, but delegates
what- and how-to recover to the programmer, and (b) PARSEC allows programmers to
specify the tasks to restart in the event of a failure~\cite{parsec}, but no
capability to manage memory and compute resources during recovery.  

\subsection{Contributions}
In this report, 
\begin{itemize}
\item We develop a productive resilience infrastructure for the Legion
programming system. 
\item Our resilience infrastructure also allows for garbage collection of
preserved regions. It does so by employing a novel scheme that allows for
detection and reclamation of preserved regions when the task execution wavefront
is past the point where the regions will be needed for recovery.
\end{itemize}

\section{Resilience in Legion}

The theme behind the resilience infrastructure in Legion is to provide
\textbf{the programmer with the same progressive execution control that is available
for a failure-free Legion execution} to when an execution suffers from task 
failures. Inline with this theme, our resilience policy provides complete
control to the programmer over the preservation of regions and the mapping
decisions for tasks and regions during recovery from a failure. The following
sections detail the resilience policy and mechanisms in Legion.   

\subsection{Policy}

In our resilience design, the programmer specifies when to preserve the region
inputs to a task. By doing so, the programmer is marking specific tasks at
which a restart can begin in the event of a failure in a task sub-DAG rooted 
at that task. The Mapper API to perserve a physical instance associated with a
region is as follows:
\begin{center}
\texttt{ 
void MapperRuntime::preserve\_physical\_instance(\\
           MapperContext ctx, unsigned region\_idx,\\ 
           const PhysicalInstance \&instance) const\\
}
\end{center}

The motivation for doing this is as follows: the choice of where to  
preserve is a challenging problem that often benefits of application or
domain-specific knowledge. Therefore, we believe that programmers, not runtime,
are more capable of developing domain-specific metapolicies that can help
identify the tasks whose input regions need to be preserved. For example, a
programmer can trade-off the cost of recomputing the input regions to a task to
the cost of preserving the input regions to a task as one such metapolicy.

In our resilience design, programmer specifies where to map the tasks and
regions during recovery from a failure. In the event of a failure, a
non-progressive resilience scheme would re-map the regions and re-execute the
tasks at the same placements as the programmer chose in the failed run. However,
this is unproductive, re-execution at the same place might cause the failure to
re-occur. Consider a scenario where the failed execution of a task occurred on
the GPU. A programmer would want the flexibility to launch a CPU-variant of the
task during recovery to avoid a repeat of the failure.   

\paragraph{Illustration of the Resilience Policy}
Consider a dataflow sub-graph of an application illustrated in
Figure~\ref{fig:failure_scenario}. The subgraph consists of tasks
\texttt{A,B,C,D} represented by nodes in the graph, and each task's input 
and output regions. The instances for the different regions are used as follows:

\begin{figure}
\begin{minipage}{.5\textwidth}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.72\textwidth]{images/failure_scenario.png}
\caption{Example dataflow graph with a failure.}
\label{fig:failure_scenario}
\end{minipage}
%\end{figure}
%\begin{figure}
\begin{minipage}{.5\textwidth}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.77\textwidth]{images/implementation.png}
\caption{Implementation of resilience}
\label{fig:resilience_implementation}
\end{minipage}
\end{figure}


\begin{itemize}
\item \texttt{A} reads and writes regions \texttt{R1} and \texttt{R3}. To do so,
\texttt{A} maps regions \texttt{R1} and \texttt{R3} to physical instances
$\delta$ and $\epsilon$, while $\alpha$ and $\gamma$ were the instances
containing the latest data of \texttt{R1} and \texttt{R3}. In this illustration,
the inputs to \texttt{A} are preserved by the programmer using the API presented
above. The result of this preservation is the creation of a copy of $\alpha,
\beta, \gamma$ via a Legion runtime \texttt{copy} operation. 
\item \texttt{B} reads the \texttt{R1} output of \texttt{A} and in the process
maps \texttt{R1} to $\tau$.   
\item \texttt{C} reads the \texttt{R3} output of \texttt{A} and in the process
maps \texttt{R3} to $\omega$. \texttt{C} additionally has dependences on other
tasks, i.e., consumes other regions, that are not indicated in this illustration. 
\item \texttt{D} consumes both \texttt{R1:}$\tau$ and \texttt{R3:}$\omega$. 
\end{itemize}  
In this illustration, by preserving the region inputs to \texttt{A}, any failure
of tasks \texttt{A, B, C, D} causes an automatic restart of re-mapping and
re-execution by the Legion runtime at \texttt{A}. 


\subsection{Mechanisms}
In the event of a task failure, the Legion runtime automatically captures the
failure, identifies the tasks in the dataflow graph to restart, and restarts the
execution of these tasks. These steps are detailed in the following paragraphs. 

\paragraph{Capture the failure of a task}
In the Legion programming system, the execution of a task is divided into
multiple pipeline stages such as logical dependence analysis, mapping, physical
dependence analysis, execution, completion and commit\cite{bauer_thesis}. The
execution of these stages follows a deferred execution model where each stage of
a task is scheduled, in contrast to being invoked, and predicated on an event 
based on the successful execution of the previous stage. Each stage is also
predicated on the successful completion of the same stage in the task's
predecessors in the dataflow graph, e.g., task \texttt{B}'s mapping stage is
predicated on the success of the mapping stage of task \texttt{A}. When a task
fails during execution:
\begin{itemize}
\item the Realm layer of the Legion programming system, a low-level runtime
system targetted by the Legion runtime~\cite{treichler_thesis}, poisons the
output event generated from the execution of the failed task. Further, all tasks
predicated on the successful execution of the failed task, i.e., by consuming
the output of the failed task, will be poisoned. This poison propagation ensures
that no task that is directly or transitively dependent on the completion of a
failed task is executed. 
\item the runtime layer of the Legion programming system checks the return value
of a task's execution when analyzing the task's profiling
response~\cite{bauer_thesis} and recognizes the failure. The runtime then 
initiates additional recovery mechanisms as discussed in the
next two paragraphs. 
\end{itemize}

\paragraph{Rollback of the execution wavefront due to a task failure}
In the event of a failure, the failed task can be restarted provided the input
physical region instances~\cite{bauer_thesis} are available, i.e., not garbage
collected, and unmodified, i.e., contain the same data as when the failed task
began execution.  The data in the region's physical instance is deemed as
modified if another task has mapped the same physical instance and with
\texttt{read, write} permissions. If the inputs are not available or are
modified, the runtime has to traverse upstream from the point of failure in the
dataflow graph to identify tasks that can help generate these inputs. The upstream
traversal terminates with tasks whose inputs satisfy the previously mentioned
criteria or we have reached tasks whose input regions' physical instances have
been preserved. This step is illustrated in the
Figure~\ref{fig:resilience_implementation}. The runtime traverses upstream from
\texttt{B} to determine tasks whose inputs are available and unmodified, and
finds \texttt{A}, since \texttt{A} preserved its region inputs.

\paragraph{Activation, re-mapping and re-execution of tasks}
Before the restart of tasks, referred to as restart\_set, in response to a
failure, the runtime has to activate the tasks along the path of recovery to
ensure that they get re-mapped and re-executed in the correct context. There is
an alternate choice here: One can skip the process of traversing the dataflow
graph and identifying tasks to re-map and re-execute. In contrast, one can
regenerate the task graph from the restart\_set. But, this involves a
sophisticated approach where task launches leave behind continuations. For
example, in Figure~\ref{fig:recovery_path}, if we have to rediscover
\texttt{B,C,D}, and they were not launched from \texttt{A} but a task
\texttt{E}, then we need to have continuations in \texttt{E} that enable us to
do so. Hence, we did not choose this option. 

\begin{figure}
\begin{minipage}{.5\textwidth}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.72\textwidth]{images/recovery_path.png}
\caption{Recovery path.}
\label{fig:recovery_path}
\end{minipage}
%\end{figure}
%\begin{figure}
\begin{minipage}{.5\textwidth}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.72\textwidth]{images/remapping_memory_consistency.png}
\caption{remapping and ensuring sequential semantics}
\label{fig:remapping_memory_consistency}
\end{minipage}
\end{figure}

Our design is as follows: traverse downstream from the point of failed task in
the dataflow graph and include tasks that were either directly or transitively
dependent on the failed task. Additionally, traverse upstream from the failed
task to the restart\_set while including all the tasks found along this
path.  Once we identify all the tasks that need to be restarted, we activate them
ensuring that they re-map following the successful re-mapping and re-execute
following the successful re-execution of their predecessor tasks in the dataflow 
graph.

\paragraph{Garbage collection of preserved regions}
In our resilience design, we introduce a novel scheme to garbage collect
preserved physical instances of regions. A preserved physical instance of a
region is no longer needed when the preservation is post-dominated in the
dataflow graph by preservations to the same region. Similarly, a task never  
needs to be restarted if its post-dominated by a set of tasks who have their
inputs preserved, thereby ensuring that the restart wavefront never reaches the
task in question. We implement this scheme by following a standard backward
all-paths dataflow analysis technique. 

\section{Sequential Semantics}
In our resilience design, the Legion runtime ensures sequential semantics, i.e.,
an execution that involves a restart of a subset of tasks will be as though all
the tasks in the program were run in a sequential order. In order to do so, the
legion runtime limits the physical instances that can be mapped during a
\texttt{map\_task}~\cite{bauer_thesis} operation while in recovery from a
failure. The limitation is necessary since during a recovery, transiently, the
latest physical instance mapped by a task is not necessarily the one that
contains the latest data for that logical region. This is best illustrated by
Figure~\ref{fig:remapping_memory_consistency}. To recover from the failure of
task \texttt{B}, tasks \texttt{A,B,D} are re-mapped. If we focus on region
\texttt{R3}, during recovery task \texttt{D} cannot map \texttt{R3} to the
physical instance created by task \texttt{A} for \texttt{R3}, namely
$\epsilon\prime$. Instead, it can choose either to map \texttt{R3} to $\omega$,
which was created by task \texttt{C} or create a new physical instance for
\texttt{R3}. If a new instance was created, then the runtime inserts a
\texttt{copy} from the physical instance, $\omega$, to the newly created
instance ensuring sequential semantics. The runtime prevents \texttt{D} mapping to
$\epsilon\prime$ by allowing the programmer to query the validity of mapping
that instance through a flag available on that instance object during a restart.

\section{Implementation Details}

In this section, we detail the prominent Legion runtime changes needed to
achieve the mechanisms detailed in the previous section.
%\begin{itemize}
\subsection{Re-establishing mapping dependences}
In the event of a task failure, the Legion runtime performs a downward sweep of
the dataflow graph from the point of failure and resets the tasks along the
traversal to the mapping stage. In this process, it creates a new
\texttt{mapped\_event} for each of these tasks, and updates the mapping
dependences of the dependent tasks with the new \texttt{mapped\_event}s.
 
\paragraph{Choice for establishing mapping dependencies}
There are two choices by which the runtime can update the mapping dependences
for a task, say \texttt{D}, as illustrated in
Figure~\ref{fig:remapping_memory_consistency}: (a) the runtime can merge all the
\texttt{mapped\_event}s of the predecessor tasks of \texttt{D} into a single
event on which \texttt{D}'s mapping is predicated, or (b) the runtime can merge
the \texttt{mapped\_event} of \texttt{D} itself with the newly generated
\texttt{mapped\_event}s of only the predecessors of \texttt{D} that are going to
be restarted. Currently, we pursue the former strategy, since the later choice
forces the completion of any earlier mapping operations issued for \texttt{D}
before the latest mapping operation, which might reduce the effectiveness of the
deferred execution model.  

%\begin{figure}
%%\begin{minipage}{.5\textwidth}
%\centering
%%\captionsetup{justification=centering}
%\includegraphics[width=.50\textwidth]{images/impl_detail3.png}
%\caption{choice for mapped event}
%\label{mapping_choice}
%%\end{minipage}
%\end{figure}

\subsection{Restart Generation of a task}
Our resilience design can handle multiple concurrent failures among the
application tasks. A key aspect of the design that helps us achieve this is the
\texttt{RestartGenID} associated with each task. The \texttt{RestartGenID} is
used as a unique identifier to determine whether a specific stage of the task ,
e.g., mapping, should be completed or not; we compare the restart generation
identifier captured when the mapping stage was scheduled against the
restart generation that the task is in when the mapping stage is
executed. This is illustrated in Figure~\ref{task_tuple_state}. 

%\end{itemize}
\begin{figure}
\begin{minipage}{.5\textwidth}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.82\textwidth]{images/impl_detail2.png}
\caption{task tuple state}
\label{task_tuple_state}
\end{minipage}
\begin{minipage}{.5\textwidth}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.82\textwidth]{images/impl_detail1.png}
\caption{preserve implementation}
\label{preserve_impl}
\end{minipage}
\end{figure}


\subsection{\texttt{preserve\_physical\_instance} implementation}
The Legion runtime handles the preservation of physical instances by
scheduling a runtime \texttt{copy} operation from the physical instance. This 
provides the freedom to the
programmer to utilize the preserved physical instances as needed in later tasks.
This is illustrated in Figure~\ref{preserve_impl}. If $\alpha$ is modified by a
later task, \texttt{A} can still be restarted in the event of a failure since
there is a copy of $\alpha$ available. 

\section{Current Status}
We are working on the implementation of the resilience design in the Legion
runtime with S3D~\cite{s3d, s3d2}, a parallel direct numerical solver from
Sandia National Laboratories, and Soleil-X~\cite{soleilx}, a turbulence,
particle and radiation solver from Stanford University, as the primary
application drivers. We also are working on:
\begin{itemize}
\item Evolving the design to handle node and runtime failures and handle
failures in tasks employing phase barriers\\
The strategy to handle node failures is to lazily update a copy of the legion
runtime state and application's region and task structures on another node in
the parallel system, and utilize this state footprint to recover form a node
failure. For phase barriers, we are strengthening the advancement of a phase
with the runtime guarantee that all tasks issued prior-to and in-that phase will
never need to be restarted.
\item Develop Regent language-level support for resilience
The current API for preserving regions is supported in the \texttt{C++}
interface to Legion. We anticipate developing such extensions in the
higher-level Regent language as well. 
%\item Handle a reliability hierarchy, e.g, Containment Domains, by associating
%instance preservation with a hierarchy level
\end{itemize}

%\section{Introduction}
%
%The traditional solutions to address failures during large-scale parallel
%executions involves the application developer: (a) manually takes periodic
%checkpoints, and manually controls the rollback and restart from specific
%checkpoints~\cite{checkpointrestart}, (b) employs a semi-automatic approach, 
%where the developer employs programming constructs that have well defined 
%semantics in the event of failures, e.g, \texttt{finish} blocks in X10~\cite{x10}.
%Here, the parallel programming model runtime automatically corrects itself to 
%handle further execution in the event of a failure but limited to the beginning
%of these specific programming constructs. (c) employs a sophistica
%In x10, use programming language constructs such as resilient finish blocks to
%capture their completing along with useful semnatic guarantees. 
%Autocorrecting, where the programmer labels computations which form~\cite{parsec}.
%Migratable objects~\cite{charm++}.
%We believe these solutions are good, but not flexible to demand the
%complexities of current extreme-scale and upcoming exa-scale systems. Its is
%not at one point, every mapping needs to make a decision, support for checking
%whther there is a tradeoff between recomputing the dag vs .. this has to be
%dynamic. We also believe that these decision should be in a deferred execution
%state, not the whole computation stopping.
%
%
%To drive this point, consider the figure . A simple computation task graph, X10
%would allow this, PARSEC would allow this, Charm++ would focus on the stores of
%alpha, x and y. However, we believe that this solution.  soemtimes you have a
%resilient store where you map alpha, x but sometimes not. Think here.
%
%The right vehicle to demonstrate such a dynamic decision for checkpoints, along
%with a deferred execution model is the Legion programming model. The strengths
%of this include a decoupled scheduling, mapping, and execution analysis stages
%that drives this solution.
%
%\begin{figure}
%\centering
%\captionsetup{justification=centering}
%\includegraphics[width=.42\textwidth]{images/spectrum_x10_parsec_legion_policies.png}
%\caption{A spectrum of resilience mechanism supported by different
%state-of-the-art parallel runtimes along with the proposed resilience strategy
%for Legion.} 
%\end{figure}
%
%Our contributions are as follows: 
%\begin{itemize} 
%\item the most flexible resilience mechanism 
%\item dynamic decisions for checkpointing 
%\item auto rollback and recovery 
%\item garbage collector that will also collect previous 
%checkpoints based on a novel post-dominator algorithm.  
%\item a discussion of the semantics of regions when there is rollback 
%\end{itemize}
%
%The rest of the paper is organized as follows:
%
%%With the advent of productive programming models like Legion~\cite{legion},
%%X10~\cite{x10}, PARSEC~\cite{parsec}, CHARM++~\cite{charm++}, and others,
%%programming extreme-scale systems is not as significant a challenge as
%%addressing other 
%%
%%how is it different from x10, parsec, charm++\\
%%	- they also allow tasks to be marked as resilience\\
%%	- x10 allows finish blocks, exception semantics.\\
%%	- what are the exception semantics that we are providing\\
%%
%%what about local vs global recovery\\
%%	- can we recover from a node failure\\
%%	- can we recover from an exception, what are the semantics that we provide
%%	  here\\
%%	- can we recover from ECC errors, \\
%%	- can we fix these errors ?\\
%%	- can we recover from I/O errors ?\\ 
%%	- can we recover from non-SingleTasks, what is recovery for index space
%%	  tasks, must epoch tasks\\
%%
%%
%%experiments: circuit, miniAero, Soleil-X, stencil, (S3D ?)\\
%%
