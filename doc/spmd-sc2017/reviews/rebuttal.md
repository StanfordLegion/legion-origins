<!-- ############################################################ -->
# Overview
<!-- ############################################################ -->

We thank the reviewers for their detailed and insightful feedback.

Some reviewers voiced concerns about the novelty of our approach. To
the best of our knowledge, we are the first to achieve practical
levels of scalability for several important classes of applications,
such as simulations on unstructured meshes, when written in an
implicitly parallel programming model. We are also the first to use
language-level support for partitioning in a compile-time analysis or
optimization to achieve such a result.

As noted by Reviewer 4, it is reasonable to ask how to define implicit
parallelism. For the purposes of this paper, an implicitly parallel
programming model is one that provides sequential semantics, even when
executing on a parallel (and possibly distributed) machine. Regent
follows this definition, as do High Performance Fortran (HPF), the
data parallel subset of Chapel, Spark, and MapReduce. OpenMP is
discussed in our section responding to Reviewer 4's questions. A nice
property of these systems is that they avoid by construction the
pitfalls of explicitly parallel programming, such as data races and
deadlocks. In addition, because they avoid any need to manage
execution resources or communication in a distributed-memory machine,
they are also substantially easier to use compared to explicitly
parallel programming models.

Clearly, programmers have been writing unstructured mesh codes for a
long time and have achieved high performance on modern
supercomputers. But these results have used explicitly parallel
programming models and paid the associated price. Among the implicitly
parallel systems, only two we are aware of have attempted to address
this issue in the context of distributed-memory machines: the
inspector/executor compiler (IEC) and the work for OpenMP by Kwon et
al. These achieved scalability to 256 and 64 cores, respectively. HPF
and Chapel have been focused mostly on structured applications and
either haven't implemented, or haven't demonstrated scalable and
efficient performance for unstructured programs. Spark and MapReduce
are intended for very different workloads and struggle to execute
efficiently the class of high-performance applications we consider
here. Among implicitly parallel programming models, we are the first
to achieve practical levels of performance and scalability for
important classes of applications such as simulations on unstructured
meshes.

Existing techniques for compile-time analysis and optimization have
focused on very limited domains such as affine loops. Within such
domains, techniques such as the polyhedral method have been very
successful. However, for the classes of applications of interest here,
it is simply impossible to apply these techniques as the program data
structures may dynamic and possibly input-dependent. Thus some amount
of dynamic analysis is mandatory.

However, a pure dynamic analysis, in the absense of any higher-level
program structure, can also be prohitively expensive. For example, IEC
takes as input unannotated sequential C programs. Because the source
programs contain no higher-level structure (i.e. there are no tasks),
IEC must perform instrumentation and analysis at the level of the
memory accesses of individual loop iterations. This imposes a cost in
both time and space that can quickly become prohibitive at practical
scales.

Another alternative is a hybrid analysis. Kwon et al. take such an
approach for OpenMP programs. However, the source program again
contain no higher-level structure. To avoid the cost of a per-element
dynamic analysis, Kwon et al. record a summary of the accesses of each
loop iteration. But this summary loses precision, hurting the ultimate
performance of the technique.
