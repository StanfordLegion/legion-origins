# Summary and High Level Discussion

This paper presents a technique called control replication, whose
purpose is to transform implicitly parallel programs (those written
with constructs like parallel for loops) into SPMD programs (a la
MPI). The key here is that control tasks (such as executing code
between parallel for loops, allocating operations to threads, etc.)
now need to be replicated across multiple threads of control, each
running on a different node. Hence, "control replication."

The key challenges here are figuring out how to distribute tasks among
threads of control and how to communicate data that different threads
of control might need access to. This is accomplished using a
Legion-like region system: data is partitioned into different regions,
with programmer-specified aliasing properties. This information can be
used to efficiently determine what data might need to be communicated
(using disjointness information to soundly guarantee when data won't
need to be communicated), with precise communication being determined
at run-time by performing explicit intersection tests between regions.

Overall, the idea is nice (though not completely novel, as the authors
themselves note) and well-implemented, and the results are very
compelling.

Strengths:
+ Very good results
+ Nice way to leverage region annotations to simplify the task of
  determining what data needs to be communicated.
+ Good optimizations to help with efficiency

Weaknesses:
- Novelty is a little middling -- as a high level concept, not that
  different than what, say, HPF does, or what Kwon et al do.

# Comments for Rebuttal

It appears that in the current implementation, the runtime region
intersection tests (for communication) are computed just once, at the
beginning of execution. Is that correct? Can you handle situations
where region definitions change at runtime?

# Detailed Comments for Authors

Overall, I thought this was a very nice paper. Well-written, clear, a
good idea, with very good results. The general problem of transforming
implicitly parallel programs to SPMD versions is clearly an important
one, and this approach seems like a good, if sort of straightforward,
way to do it. The proposed system leverages Regent/Legion's region
information very well.

I appreciated the simple running example used to illustrate the
transformation steps, but I think it might have been nice to see a
slightly more complicated example, one that demonstrated the use of
the two-level intersection test, for example
