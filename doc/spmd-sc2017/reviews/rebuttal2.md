<!-- ############################################################ -->
# Overview
<!-- ############################################################ -->

Some reviewers voiced concerns about the novelty of our approach. We
believe, to the best of our knowledge, that control replication
represents a fundamentally new approach to achieving scalability with
implicitly parallel codes. In particular, we are the first to
demonstrate that the use of general-purpose high-level abstractions for data
partitioning enable a substantially simpler and more robust hybrid
static/dynamic program analysis, and more effective code generation,
leading to better scalability on challenging classes of
applications such as simulations on unstructured meshes.

To be clear, we are not the first to attack the problem of achieving
scalability on unstructured mesh codes. But all the work that we know
of either (a) makes use of an explicitly parallel programming model
such as MPI, (b) is limited to domain-specific structures, or (c)
suffers from limited scalability.

Reviewer 4 asks how we define implicit parallelism. For the purposes
of this paper, an implicitly parallel programming model achieves
parallel (and possibly distributed) execution while maintaining
apparently sequential semantics. This definition captures an intuition
which we find appealing: namely, that explicitly parallel models by
definition permit (and usually require) programmers to violate
sequential semantics and thus expose them to one or more of the
pitfalls traditionally associated with explicit parallelism: data
races, deadlocks, etc. Implicitly parallel programming models avoid
these by construction.

Reviewer 3 is correct that aliasing in the memory access patterns of
an application can be an obstacle to achieving scalability in Regent.
The same is true for any implicitly parallel programming model, where
the underlying system is responsible for managing communication in a
distributed-memory machine, and that communication is a function of
how the application is written. Regent simply provides abstractions
for describing this aliasing soundly and precisely (via regions and
partitioning). Regent programmers use the ability to express necessary
aliasing precisely to minimize data movement and communication,
enabling high performance and scalability.

It is worth taking a moment to consider how one would implement
implicit parallelism in the absence of Regent's abstractions. For
simulations on unstructured meshes and other applications with dynamic
or input-dependent behavior, the required analysis is simply
infeasible at compile time. Dynamic or hybrid analysis can be used,
but in the absence of higher-level abstractions this analysis must be
performed at the level of much finer-grained operations such as
individual loop iterations and memory references and can easily become
prohibitively expensive.

The best examples of this approach are the inspector/executor compiler
(IEC) and the work by Kwon et al. IEC records and compares read and
write sets at the level of individual loop iterations to determine
what data must be communicated. While the resulting communication is
precise, the time and space costs of the analysis can be prohibitive
at large node counts. Kwon et al. avoid some of this cost by
summarizing these accesses via array sections, but lose precision in
the set of elements communicated, resulting in degraded scalability
overall.

In contrast, control replication leverages higher-level abstractions
for data partitioning. This means that control replication is able to
achieve precise communication without any analysis of the read and
write sets of individual loop iterations. In the presence of
user-specified disjointness (such as in the two-level partitioning
example) control replication can limit analysis to only those elements
actually involved in communication in some way. This is an algorithmic
improvement: the control replication analysis is proportional to the
surface area rather than the volume of the simulation
domain. Furthermore, because partitioning is under explicit programmer
control, the outcome of control replication is transparent and
predictable, in contrast to many traditional compiler optimizations
which may be sensitive to small perturbations to the code. Finally, the
implementation of control replication itself is made particularly
straightforward by the use of these abstractions.

While investigating the answer to Reviewer 2's question about Stencil
performance, we found a typo in our spreadsheet with Stencil timing
data where we scaled the Regent execution time by the wrong number of
timesteps (60 instead of 50). We have fixed the typo and verified the
new result by rerunning the single-node MPI and Regent codes. We will
release the corrected numbers if the paper is accepted. With the
correction, Regent absolute performance is 1-2% worse than MPI at all
node counts (instead of 15% better as erroneously reported in the
submission). Regent's parallel efficiency is still as reported in the
paper (as the bug was just a scaling difference): we achieve 99%
parallel efficiency at 1024 nodes.

We wish to thank the reviewers again for their detailed and insightful
feedback. We will make all requested changes to the text. In the
interest of responding to all reviewers' concerns, we include answers
to any remaining questions below, grouped by reviewer.

<!-- ############################################################ -->
# Detailed Responses to Reviewer 1's Questions
<!-- ############################################################ -->

The dynamic computation of intersections is performed as early as
possible, but no earlier. In our experiments, the intersections are
performed immediately after the creation and partitioning of the
regions and before the start of the main simulation loop. However,
control replication can also be applied to situations where the
partitioning is performed periodically during execution; this simply
limits the scope to which control replication can be applied. Of
course for control replication to be profitable any repartitioning of
data should occur relatively infrequently (e.g. every ten or hundred
time steps).

We will add an example of two-level partitioning.

<!-- ############################################################ -->
# Detailed Responses to Reviewer 2's Questions
<!-- ############################################################ -->

In response to the reviewer's high-level summary: thread creation is
one of the overheads which is alleviated by control replication, but
is not the only one. Implicitly parallel implementations for
distributed-memory machines must perform analysis to determine the
exact sets of elements to be communicated between nodes, and this can
easily be much more expensive than the creation of threads
themselves. This is in contrast to, for example, implementations of
OpenMP for shared-memory machines where the primary concern is the
thread creation overhead alone, as data movement is managed by the
shared-memory hardware.

Regarding the two cited papers:

The tiling and stencil papers are examples of techniques that show how
to choose an ordering of loop iterations, and partitioning of
iteration spaces into tasks, to achieve various properties such as
cache locality or parallelism. These techniques require a precise
compile-time analysis of loop dependencies which is generally only
achievable for very limited classes of codes such as affine loop
nests. In contrast, control replication uses a different division of
labor where the programmer is responsible for annotating
coarse-grained tasks and regions, while control replication is
responsible for efficient execution and communication. The ghost areas
under control replication are a function of the intersections of
regions, rather than of the memory access patterns of individual loop
iterations. This approach makes it straightforward to apply control
replication to problems which are challenging for traditional compiler
techniques.

The unstructured grid paper is an example of an implementation written
in an explicitly parallel programming model. We have discussed the
benefits of implicit parallelism over explicit parallelism
above. Control replication can be seen as increasing the performance
range of the implicit style, allowing more whole codes and subsystems
of codes to be written in this more productive and more easily
maintained style.

The reviewer is correct that the precise synchronization, just as with
the precise communication, cannot be determined until runtime. In fact
the two are isomorphic: the set of nodes that require synchronization
is exactly the set of nodes that require communication. The code that
is generated by the compiler inspects the dynamically computed region
intersections to determine what set of point-to-point synchronization
operations to perform.

Any outer control flow is permitted in control replicated
programs. Any statements are simply replicated and executed on all
processors. For example, an outer while loop might be used to
determine if the simulation has reached the desired maximum simulation
time. This capability is used in the PENNANT example in our
experiments.

We used the following number of timesteps in the experiments: 50
(Stencil), 30 (MiniAero), 30 (PENNANT), and 100 (Circuit). Note that
in production applications the number of timesteps is typically on the
order or thousands or millions.

We performed different experiments at different times with different
versions of the software. LLVM is neither forwards nor backwards
compatible between releases and thus we were forced to use different
LLVM versions on different experiments. While all codes run and
achieve the same performance on the newest version of the software
(and LLVM), we did not have the node hour allocation to perform reruns
of full-scale experiments for the paper deadline. We are expecting to
receive additional allocation and will include updated numbers if the
paper is accepted.

We will apply the suggested changes.

<!-- ############################################################ -->
# Detailed Responses to Reviewer 3's Questions
<!-- ############################################################ -->

We will be happy to add additional detail to sections 3.4 and 3.5.

The reviewer is correct that only innermost loops must be free of
loop-carried dependencies. The code surrounding those loops is
permitted to use arbitrary control flow.

TF may read as well as write PB. See the privileges declared for TF in
Figure 2 line 2. It is true that if the privileges included write and
not read then, in combination with an analysis to prove that TF
overwrites all elements of PB, the initialization of PB could be
skipped. The initialization of QB can only be skipped if PB covers the
entire region B. Note that knowing the definition of QB is not
sufficient, as QB may contain different elements from PB. That is,
it's possible for the following to be true:

\exists j \in QB, i \in PB. j = h(i) \and \forall k \in PB. k \neq j

We will clarify this in the text.

The reviewer is correct that Figure 4c is an intermediate step on the
way to 4d. The synchronization is correct but only becomes necessary
in 4d. We feel that the presentation as two small steps rather than
one large step is better for reader understanding, but we will add
some clarifying comments in the text.

In Figure 4d, the iteration space I has been partitioned between the
shards such that each shard owns a subset SI. Each shard will issue
tasks for each i in SI. This means that the flow of tasks visible to a
given shard include a write to PB[i] for each i in SI, and a read to
each QB[i], but not necessarily all QB[j] which might overlap with the
written PB[i]. This is where communication is required, and where the
synchronization must be specified. We will clarify this in the text.

Certain elements in Figure 4d are written in an informal way to
streamline the presentation. However, the actual output of control
replication is valid Regent code and can be written in proper Regent
syntax.

We will apply all suggested changes.

<!-- ############################################################ -->
# Detailed Responses to Reviewer 4's Questions
<!-- ############################################################ -->

The applications used in the experiments exchange halo cells in a way
that is similar to a stencil. Note that in some cases (PENNANT,
Circuit), the exchanges are over partial sums of values (such as
forces) to be reduced into the elements on the far side. In a typical
MPI implementation, these would require the use and coordination of
master/slave cells. This complexity is not present in the Regent
versions, which simply use Regent's reduction privilege to express
these computations. Note also that PENNANT uses a scalar reduction
(the equivalent of MPI_Allreduce) to compute the value of dt for the
next timestep.

The algorithmic complexity of launching N tasks can be O(log N) in
cases where the set of N tasks are sufficiently similar to one
another. This is true for the set of threads launched in an OpenMP
parallel for, or for the set of ranks created as part of the initial
launch of an MPI program. However, note that in Regent the arguments
to tasks are permitted to be arbitrary expressions and thus when N
tasks are launched from a single node the algorithmic complexity is in
the worst case O(N). We will clarify this point in the text.

We take the reviewer's point about the tone of the introduction and
will revise the text accordingly.

The reviewer is correct that the 256-core figure quoted in the
submission is in reference to the inspector/executor compiler, which
is the best comparable system we know of.

Regarding the other systems listed by the reviewer:

UPC and Titanium are PGAS languages and expose explicitly SPMD
semantics. In many ways they improve upon the state of practice with
MPI. However, while data may be visible everywhere, it is not local
everywhere, and an attempt to access non-local data will either be an
error or slow depending on the semantics of the language. Furthermore,
because the languages provide SPMD semantics the user is generally
still exposed to pitfalls such as data races and deadlocks. Therefore
these languages are not implicitly parallel.

MapReduce, Hadoop and Spark scale well and do qualify as implicitly
parallel by our definition. However, these systems are intended for
very different workloads: MapReduce and Hadoop assume that the data
begins and ends on disk. Spark does as well though it goes to more
effort to keep intermediate data in memory. Our experience is first
that these systems perform poorly on HPC hardware because
supercomputers are typically configured to use a parallel filesystem
such as Lustre which runs on a set of nodes which is distinct from the
compute nodes in the machine. And second, the systems themselves are
not optimized for the granularity of tasks and types of communication
patterns that occur in HPC application such as those considered in
this paper. We are not aware of positive performance results on the
class of applications we consider with these systems.

We will add citations for DSLs. These are often implicitly parallel
and some, such as Liszt, have been demonstrated to scale
well. However, by design these systems operate via inherent knowledge
of the domain, and thus have limited applicability. Control
replication is a much more general technique.

HPF, ZPL and Chapel are implicitly parallel and do scale. HPF was
mostly intended for use with dense arrays; while the spec offered an
optional extension for certain classes of unstructured computations we
are not aware of any implementation of it, or any performance
results.

ZPL provides a very general remap operator. Efficient communication
with this operator requires the use of bulk data transfers. By nature,
remap is an element-wise operator and the compiler is responsible for
identifying and exploiting opportunities for bulk transfers. If
compile-time analysis fails then the system falls back to an
inspector/executor approach with tradeoffs similar to IEC. However,
all of this is opaque to the programmer; there is no way to specify
higher-level properties, making it challenging to debug or correct the
behavior of the system when something goes wrong. Note that benchmarks
presented in the ZPL paper do not stress the worst case: the FFT is of
course structured and CG uses a CSR encoding of a sparse matrix. It is
unclear how ZPL would perform on e.g. an unstructured mesh code.

Chapel offers a more general concept of a domains, which are
expressive enough to describe unstructured meshes. The effectiveness
of these in practice depends on the domain map used to distribute the
elements of the domain around the machine. Chapel provides
user-extensible domain maps, but we are not aware of any work on
domain maps for unstructured application such as those considered in
this paper, or any large scale performance results for such
applications.

X10 is explicitly parallel. While deadlocks are not possible in the
language, async calls may be execute in any order, resulting in the
possibility of data races when the programmer is not
vigilant. Furthermore, data is distributed between places and accesses
to non-local data require an async call. As a result, communication is
explicit in the program.

In Regent, tasks denote a decomposition of the program into
appropriate units of execution. However, the execution of a sequence
of tasks is always consistent with sequential execution. Both
deadlocks and data races are impossible by construction within the
language.

The reviewer is correct that any number of tasks with reduction
privileges may be executed in parallel, as the partial results of the
reductions are simply buffered. However, when multiple reduction
operators are used, the application of reductions with different
operators must be serialized in order to preserve sequential
semantics. We will clarify this point in the text.

Regarding inner versus outer parallelism: in Regent, the analysis to
find parallelism between tasks generally runs ahead of the execution
of the program in order to expose as much parallelism as possible in
the application. To the extent that tasks within subsequent iterations
of an outer loop do not depend on the current iteration, those tasks
are permitted to execute in parallel. This property is preserved by
control replication.

Under Regent's type system, two regions indexed by the same constant
are the same region (i.e. they will both have the same type). We will
clarify this in the text.

Please see our response to Reviewer 2's question about LLVM versions.

The Regent implementation of Stencil supports arbitrary grid sizes,
and we used this support to run the Regent code on both even and odd
powers of 2 sizes.

The primary challenge of using GPUs is that one must generate
efficient GPU kernels for the desired application, which can require
non-trivial programmer effort. (Note that the second challenge
traditionally associated with GPUs, of managing data movement to and
from the GPU, is already solved by Legion.) Control replication is
compatible with the use of GPUs.

In Chapel, when creating an domain, a domain map (which maps elements
to memory) must be specified at the time the domain is created. In
order to change the domain map, a new domain must be created, the
elements copied, and the old one destroyed. This may be prohibitively
expensive, depending on the use case.

In Regent, a given region may be partitioned multiple times without
destroying the previous partitions. Data is only moved when a task
actually requests access to a subregion. This feature is used in all
of the benchmarks used in our experiments, in order to express ghost
cells. In a simple partitioning scheme, two partitions may be used:
one to name what HPF would call the domain decomposition, and one (or
more) to name the various ways that ghost cells will be accessed. As
the application executes, it may switch at will between these, without
destroying previous instances of the data, and while moving only that
data which actually needs to be updated to construct an up-to-date
copy of the requested subregion.

Regent can describe arbitrary task graphs. Control replication
operates on a collections of parallel loops and optionally bulk or
scalar reductions (which can be used as a form of broadcast).

We will apply all suggested edits.
