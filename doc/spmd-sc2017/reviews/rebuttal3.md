Some reviewers voiced concerns about novelty. We believe control
replication represents a new approach to achieving scalability with
implicitly parallel codes. We are the first to demonstrate that
general-purpose high-level abstractions for data partitioning enable a
substantially simpler, robust hybrid static/dynamic program analysis,
and more effective code generation, leading to better scalability on
challenging classes of applications.

We are not the first to attack achieving scalability on unstructured
meshes. Previous work uses explicitly parallelism, is limited to
domain-specific structures, or has limited scalability.

We take Reviewer 4's point about the tone of the introduction and will
revise accordingly.

Reviewer 4 asks how we define implicit parallelism. For our purposes,
an implicitly parallel programming model achieves parallel execution
while maintaining sequential semantics. This definition captures an
appealing intuition: explicitly parallel models require programmers to
violate sequential semantics and thus expose them to well-known
pitfalls (races, deadlocks, etc.). Implicitly parallel programming
models avoid these by construction.

Reviewer 3 is correct that aliasing patterns can be an obstacle to
scalability in any implicitly parallel programming model, where the
underlying system manages communication. Regent has the advantage that
programmers can precisely describe necessary aliasing and thereby
minimize run-time communication.

Consider implementing implicit parallelism without Regent's
abstractions. For applications with input-dependent behavior, the
required analysis is infeasible at compile-time. Dynamic or hybrid
analysis can be used, but without higher-level abstractions this
analysis is at the fine-grain level of loop iterations and memory
references and can become prohibitively expensive.

The best examples of this approach are the inspector/executor compiler
(IEC) and the work by Kwon. IEC records and compares read and write
sets of individual loop iterations to determine communicated data. The
resulting communication is precise, but the time/space cost of
analysis can be prohibitive at large node counts. Kwon avoids some
cost by summarizing accesses via array sections, but loses precision
resulting in degraded scalability.

In contrast, control replication leverages higher-level abstractions
for data partitioning. Control replication achieves precise
communication without analysis of the read/write sets of loop
iterations. In the presence of user-specified disjointness control
replication limits analysis to elements actually involved in
communication. This is an algorithmic improvement: the analysis is
proportional to the surface area, not the volume of the simulation
domain. Furthermore, because partitioning is under programmer control,
the outcome is predictable, in contrast to many traditional compiler
optimizations that are sensitive to small code perturbations. Finally,
the implementation of control replication is made straightforward by
using these abstractions.

Region intersections are computed as early as possible, but no
earlier. Every non-empty intersection corresponds to a point-to-point
synchronization.

Regarding papers cited by Reviewer 2:

The tiling and stencil papers require a precise compile-time analysis
of loop iterations and memory references, which is difficult. Control
replication instead leverages user-specified coarse-grained tasks and
regions. This approach makes control replication straightforward in
situations that are challenging for traditional compiler techniques.

The unstructured grid paper is an example of an explicitly parallel
implementation.



Reviewer 3 is correct that Figure 4c is a step towards 4d. The
synchronization is correct but only becomes necessary in 4d.

In Figure 4d, the iteration space I has been partitioned: each shard
owns a subset SI and issues tasks for each i in SI. The tasks visible
to a given shard include, for each i in SI, a write to PB[i] and a
read to QB[i], but not necessarily all QB[j] which might overlap with
the written PB[i]. This is where communication and synchronization are
required.



Our experiments exchange halo cells similarly to a stencil. In some
cases, the exchanges are partial sums to be reduced into elements on
the far side. MPI implementations master/slave cells. This complexity
is absent in the Regent versions. Some experiments also use scalar
reductions.


In Chapel, a domain map is specified when a domain is created. To
change domain maps, a new domain is created, the elements copied, and
the old one destroyed. This may be expensive, depending on the use
case.

In Regent, a given region may be partitioned multiple times without
destroying previous partitions. Data is only moved when a task
actually requests access to a subregion. This feature is used in all
our benchmarks. In a simple partitioning scheme, one partition names
what HPF calls the domain decomposition, and one (or more) partitions
name how ghost cells are accessed. The application may switch between
these partitions, without destroying previous instances of the data,
and while moving only data needed to construct up-to-date copies of
regions.


We will make all requested changes.
