\section{Related Work}
\label{sec:related}

% FIXME: Should these be added?
% RDIR: Sequoia, dataflow?
% Analysis: DPJ
% Languages: Copperhead

Several approaches have been considered for compiling sequential, shared-memory parallel, and/or
data-parallel programs for execution on distributed-memory machines.

%% Inspector/Executor

Inspector/executor (I/E) methods have been used to compile a class of
sequential programs with affine loops and irregular accesses for
distributed memory~\cite{InspectorExecutorIrregular12,
  InspectorExecutorMixed15}. As in control replication, a necessary
condition for I/E methods is that the memory access patterns are fixed
within the loop, so that the inspector need only be run once. Use of
an inspector allows the read/write sets of program statements to be
determined dynamically when the necessary static analysis is
infeasible in the underlying programming language, enabling
distributed, parallel execution of codes written in conventional
languages. This approach has been demonstrated to scale to 256
cores. However, the time and space requirements of the inspector limit
scalability at very large node counts. Also, the I/E
approach relies on generic partitioning algorithms such as automatic
graph partitioning~\cite{ParMetis02, Patoh11}.

%% OpenMP for MPI

Kwon et al. describe a technique for compiling OpenMP programs with
regular accesses to MPI code~\cite{OpenMPDistributedHybrid12}. A
hybrid static/dynamic analysis is used to determine the set of
elements accessed by each parallel loop. For efficiency, the dynamic
analysis maintains a bounded list of rectangular section fragments at
communication points. As a result, non-affine accesses cause
analysis imprecision that results in replicated data, increased
communication, and limited scalability. The approach has been
demonstrated to scale to 64 cores.

Like the two approaches above, control replication uses a combined
static/dynamic analysis to obtain precise information about access
patterns. At a high level, the key difference is that control
replication leverages a programming model with explicit support for
coarse-grain operations (tasks), data partitioning (of regions into
subregions), and the simultaneous use of multiple partitions of the
same data. Control replication performs analysis at this coarsened
level rather than at the level of individual loop iterations,
resulting in a more efficient dynamic analysis and in-memory
representation of the access patterns of each loop without any loss of
precision. Furthermore, hierarchically nested partitions enable
control replication to skip analysis at runtime for data elements not
involved in communication (further reducing memory usage for the
analysis). Finally, explicit language support for partitioning allows
control replication to leverage application-specific partitioning
algorithms, which are often more efficient and yield better results
than generic algorithms. As a result, control replication is able to
support more complex access patterns more efficiently, resulting in
better scalability.

%% Control replication, in
%% contrast, leverages a programming language with explicit support for
%% coarse-grained operations (tasks) and data partitioning (of regions
%% into subregions); this allows the use of application-specific
%% partitioning algorithms, which are often more efficient and yield
%% better results than generic algorithms. The choice of language also
%% enables a tractable static analysis of that partitioning to avoid
%% runtime analysis on data elements not involved in communication,
%% reducing the time and space complexity of dynamic analysis (resulting
%% in a corresponding improvement to scalability).

%% OpenMP for DSM

A number of efforts to support OpenMP on distributed-memory machines
target software distributed shared-memory (DSM)
systems~\cite{ClusterOpenMP01, IntelClusterOpenMP06,
  OpenMPDistributed07}. These approaches have limited scalability due
to the limitations of page-based DSM systems.

%% HPF

Efforts in data-parallel languages such as High Performance
Fortran (HPF)~\cite{HPF96, HPF07} pioneered compilation techniques for
a variety of machines, including distributed-memory. In HPF, a single
(conceptual) thread of control creates implicit data-parallelism by
specifying operations over entire arrays in a manner similar to
traditional Fortran. This data parallelism is then mapped to a
distributed-memory system via explicit user-specified data
distributions of the arrays---though the compiler is still responsible
for inferring \emph{shadow regions} (i.e.\ halos that must be
communicated) from array accesses. Several implementations of HPF
achieve good scalability on structured applications~\cite{Impact3D02,
  HPFJA02}.

%% ZPL

ZPL, an implicitly parallel array language, provides a very general
array remap operator~\cite{ZPLRemap03} that permits the redistribution
of data via (conceptually) scatters and gathers. The ZPL compiler
optimizes cases of the remap operator that commonly appear in
structured programs to avoid paying the cost of the general
case. However, when these optimizations fail, ZPL falls back to an
inspector/executor approach similar to the one described above, with
similar tradeoffs.

% FIXME: Sequoia

%% The Sequoia language and compiler~\cite{SequoiaLanguage06,
%%   SequoiaCompiler07}

The Chapel~\cite{Chapel07} language supports a variety of styles of
parallelism, including implicit data parallelism and explicit
PGAS-style parallelism. This \emph{multiresolution} design reduces the
burden placed on the compiler to optimize Chapel's data parallel
subset because users can incrementally switch to other forms of
parallelism as needed. However, use of Chapel's explicitly parallel
features expose users to the hazards of traditional explicitly
parallel programming.

Compared to Regent, Chapel's data parallel subset (which is most
similar to Regent's implicit parallelism) only supports a single
distribution of elements for the lifetime of a given array, and
limited task parallelism. Regent's
support for multiple and hierarchical partitions are critical to
enabling control replication to optimize implicitly parallel
programs for efficient execution on distributed memory machines.

%% More recent high-level parallel languages such as
%% Chapel~\cite{Chapel07} have avoided reliance on the compiler's ability
%% to optimize high-level language features by adding support for
%% low-level constructs that can be used when necessary.

The multiresolution approach is
also taken in Legion and Regent, which support both a straightforward
implicitly parallel style of programming that scales to modest numbers
of nodes as well as more involved explicit communication constructs
that can be used to scale to very large node counts
\cite{LegionFields14}. In particular, the explicit approach can be
time-consuming and error-prone, and was identified in a recent
study~\cite{SandiaReportManyTaskRuntimes} as a challenge for this
class of programming systems. Our work can be seen as greatly
increasing the performance range of the implicit style, allowing more
whole codes and subsystems of codes to be written in this more
productive and more easily maintained style.

MapReduce~\cite{MapReduce04} and Spark~\cite{Spark10} support
implicitly parallel execution of pure functions composed via
data-parallel operators such as \emph{map} and \emph{reduce}. Both
MapReduce and Spark use a centralized scheduler that becomes a
bottleneck at large node counts with tasks on the order of
milliseconds to tens of milliseconds. Originally these systems were
intended for use in
I/O-intensive applications where the input is initially read from
disk, such that the overhead of centralized scheduling was not a
concern. More recently, improvements in the efficiency of these codes
have resulting in more fine-grained tasks. The use of execution
templates to reduce control overhead~\cite{Nimbus17} has been explored
as a way to improve the scalability of a centralized scheduler.

Liszt~\cite{Liszt11} is an implicitly parallel domain-specific
language for solving partial differential equations on unstructured
meshes. By leveraging domain-specific knowledge, the Liszt compiler is
able to generate optimized distributed code that scales to large
numbers of nodes. However, the domain-specific reasoning used in the
compiler limits the applicability of the approach. In contrast,
control replication is general-purpose and not limited to a specific
domain.

X10~\cite{X1005} is an explicitly parallel programming language with
places and hierarchical tasks. Flat X10~\cite{FlatX10} is a subset of
this language that restricts programs to a two-level task hierarchy
where the top level consists of forall-style parallel loops. A
compiler for Flat X10 is able to transform the program into a
SPMD-style X10 program with explicit synchronization between
tasks. However, as the original Flat X10 program already contains
explicit communication, the compiler need not make changes to the
structure of communication in the program. In contrast, control
replication is able to automatically generate efficient explicit
communication for an implicitly parallel program with implicit data
movement.
