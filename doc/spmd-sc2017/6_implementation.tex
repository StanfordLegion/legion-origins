\section{Implementation}
\label{sec:impl}

We have implemented control replication as a compiler plug-in for Regent
that adds control replication as an additional optimization
pass to the main compiler. As with Regent itself, control
replication is implemented in the Terra~\cite{Terra13} and Lua~\cite{Lua}
languages. This combination gives Regent (and our plugin) the
ability to drive compilation with a high-level scripting language,
while generating efficient low-level code via LLVM~\cite{LLVM}.

%% In discussing control replication, we have been largely concerned with
%% a limited subset of a Regent features. Most additional features of
%% Regent have been straightforward to implement within control
%% replication, though a couple warrant special attention.

\subsection{Runtime Support}
\label{subsec:impl-runtime}

Regent targets Legion, a C++ runtime system for hierarchical task
parallelism~\cite{Legion12}. In non-control replicated Regent
programs, Legion discovers parallelism between tasks by computing a
dynamic dependence graph over the tasks in an executing
program. Control replication removes the need to analyze inter-shard
parallelism, but Legion
is still responsible for parallelism within a shard as well as any
parallelism in the code outside of the scope of control replication.

A notable feature of Legion is its deferred execution model. All
operations (tasks, copies, and even synchronization) execute
asynchronously in the Legion runtime. This is an important requirement
for supporting task parallelism, as it guarantees that the main
thread of execution does not block and is subsequently able to expose
as much parallelism as possible to the runtime system.

Legion targets Realm, a low-level runtime that supports execution on a
wide variety of machines~\cite{Realm14}. Realm uses
GASNet~\cite{GASNET07} for active messages and data transfer.

\subsection{Mapping Tasks to Processors}
\label{subsec:impl-mapping}

All tasks in Regent, including shard tasks, are processed through the
Legion \emph{mapping interface}~\cite{Legion12}. This interface allows
the user to define a \emph{mapper} that controls the assignment of
tasks to physical processors. (At the user's discretion, these
decisions may be delegated to a library implementation. Legion
provides a default mapper which provides sensible defaults for many
applications.) A typical strategy is to assign one shard to each node,
and then to distribute the tasks assigned to that shard among the
processors of the node. However, substantially more sophisticated
mapper implementations are also possible; in general mappers are
permitted to be stateful and/or dynamic in their decision making. The
techniques described in this paper are agnostic to the mapping used.

\subsection{Region Reductions}
\label{subsec:impl-reductions}

Control replication permits loop-carried dependencies resulting from
the application of associative and commutative reductions to region
arguments of tasks. These reductions require special care in an
implementation of control replication.

The partial results from the reductions must be stored separately to
allow them to be folded into the destination region, even in the
presence of aliasing. To accomplish this, the compiler generates a
temporary region to be used as the target for the reduction and
initializes the contents of the temporary to the identity value (e.g.,
0 if the reduction operator is addition). The compiler then issues
special \emph{reduction copies} to apply the partial results to any
destination regions which require the updates.

\subsection{Scalar Reductions}
\label{subsec:impl-scalars}

In control replication, scalar variables are normally replicated as well. This
ensures, for example, that control flow constructs behave identically
on all shards in a SPMD-style program. Assignments to scalars are
restricted to preserve this property; for example, scalars cannot be
assigned within an innermost loop (as the iterations of this loop will
be distributed across shards during control replication).

However, it can be useful to perform reductions
on scalars, for example, to compute the next timestep in a code with
dynamic time stepping. To accommodate this, control replication permits
reductions to scalars within inner loops. Scalars
are accumulated into local values that are then reduced across the
machine with a Legion \emph{dynamic collective}, an asynchronous
collective operation that supports a dynamically determined number of
participants. The result is then broadcast to all shards.

\subsection{Hierarchical Region Trees}
\label{subsec:hierarchical-region-trees}

\input{f7_hierarchical_region_tree}

Regent permits recursive partitioning of regions. Among many other uses, this feature enables
a common idiom in which the programmer constructs a top-level
partition of a region into two subsets of elements: those which are
guaranteed to never be involved in communication, and those which may need to be communicated.
 This design pattern,
in combination with the region tree analysis described in
Section~\ref{subsec:region-trees}, enables an important communication
optimization that reduces data movement for
distributed-memory execution, and also substantially reduces the cost
of the dynamic computation of intersections described in
Section~\ref{subsec:trans-intersect}.

Figure~\ref{fig:hierarchical-region-tree} shows a possible
modification to the region tree from Figure~\ref{fig:region-tree} that
uses this optimization. The top-level region \texttt{B} has been
partitioned into two subregions that represent all the \emph{private}
elements (i.e.\ those never involved in communication) and \emph{ghost}
elements (i.e.\ those that are involved in communication). The new
partition \texttt{SB} represents the subset of elements of the
original \texttt{PB} partition involved in communication. Similarly,
the new \texttt{PB} and \texttt{QB} partitions have been intersected
with the regions \texttt{all\_private} and \texttt{all\_ghost}.

Notably, the top-level partition in this new region tree is disjoint,
and thus by consulting the region tree the compiler is able to prove
that the partition \texttt{PB} is disjoint from \texttt{QB} and
\texttt{SB}. As a result, the compiler is able to prove that the
subregions of \texttt{PB} are not involved in communication (as they
are provably disjoint from all other subregions), and can avoid
issuing copies for \texttt{PB}. Additionally, because \texttt{PB} has
been excluded from the set of partitions involved in communication,
the compiler is able to skip any intersection tests with \texttt{PB}
and other partitions. As in most scalable applications the set of
elements involved in communication is usually much smaller than those
not involved in communication, so placing the private data in its own
disjoint subregion can reduce the runtime cost of computing intersections.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "spmd"
%%% End: 
