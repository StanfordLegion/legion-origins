# Instructions for Building Paper

Install a recent version of TeXLive (tested with 2016 and 2017).

Then run:

```
make
```

**DO NOT** attempt to build the paper by directly invoking `pdflatex`
or similar. There are multiple steps in the build that must be
sequenced properly for the build to work.
