\section{Control Replication}
\label{sec:trans}

\input{fA_trans_code_regent}

In this section we describe the program transformations that comprise
the control replication optimization. The optimization proceeds in
phases, first inserting communication, then synchronization, and
finally replicating the control flow to produce long-running shard
tasks to reduce runtime overhead.

Consider a subregion $S$ and its parent region $P$.  Semantically, $S$
is literally a subset of $P$: an update to an element of $S$ also updates
the corresponding element of $P$.
There are two natural ways to implement this region semantics.  In the
\emph{shared memory implementation} the memory allocated to $S$ is simply the
corresponding subset of the memory allocated to $P$.
In the \emph{distributed memory implementation}, $S$ and $P$
have distinct storage and the implementation must explicitly manage
data coherence.  For example, if a task writes to region $S$, then the
implementation must copy $S$ (or at least the elements that changed)
to the corresponding memory locations of $P$ so that subsequent tasks
that use $P$ see those updates; synchronization may also be needed to
ensure these operations happen in the correct order.  Intuitively,
control replication begins with a shared memory program and converts
it to an equivalent distributed memory implementation, with all
copies and synchronization made explicit by the compiler.

\subsection{Data Replication}
\label{subsec:trans-data}




\texcomment{
Normally, Regent programs execute with sequential semantics and thus
regions obey a form of shared-memory semantics. Subregions are views
onto a subset of the elements of a parent region; i.e.\ they contain no
logically separate storage and their consistency (under distributed
execution) is maintained by the underlying runtime system. Under
control replication, it is necessary exchange this for explicit
distributed-memory semantics as the compiler is taking control of
managing the consistency of regions.

In this distributed-memory semantics, subregions now represent
logically distinct storage from parent regions. In order to maintain
consistency, the compiler must insert explicit copy operations to
update the equivalent elements present in previously aliased
regions.
}

The first stage of control replication is to rewrite the program so
that every region and subregion has its own storage, inserting copies
between regions where necessary for correctness.  We use the shorthand
$R_1 \leftarrow R_2$ for an assignment between two regions: $R_1$ is
updated with the values of $R_2$ on the elements $R_1 \cap R_2$ they
have in common.  Figure~\ref{fig:trans_code4a} shows the core of the
program in Figure~\ref{fig:code2} after three sets of copies have been
inserted.  Immediately before the code to which the optimization is
applied (lines 7-11), the various partitions are initialized from the contents of
the parent regions (lines 2-4).  Symmetrically, any partitions written
in the body of the transformed code must be copied back to their
respective parent regions at the end (lines 14-15). Finally, inside
the transformed code, writes to partitions must be copied to any
aliased partitions that are also used within the transformed code.
Here \texttt{PB} and \texttt{QB} are aliased (i.e.\ subregions of
\texttt{PB} may overlap subregions of \texttt{QB}), so \texttt{PB} must be
copied to \texttt{QB} on line 9 following the write to \texttt{PB} on
line 8. Note that \texttt{PA} is also written (on line 10) but can be
proven to be disjoint from \texttt{PB} and \texttt{QB} using the
region tree analysis described in Section~\ref{subsec:region-trees},
thus no additional copies are required.

\subsection{Copy Placement}
\label{subsec:trans-copy}

The placement of the copies in 
Figure~\ref{fig:trans_code4a} happens to be optimal, but in general 
the algorithm described in Section~\ref{subsec:trans-data} 
may introduce redundant copies and place those copies suboptimally.
To improve copy placement, we employ variants of partial redundancy elimination
 and loop invariant code motion.
%These optimizations ensure that copies are not issued redundantly in the case
%of repeated writes or reads to a partition. 
The modifications required to the textbook descriptions of these optimizations 
are minimal. Loops such as lines 8-10 of
Figure~\ref{fig:trans_code4a} are viewed as operations on partitions. For
example, line 8 is seen as writing the partition \texttt{PB} and
reading \texttt{PA} (summarizing the reads and writes to individual
subregions).
Note that the use of standard compiler techniques is only
possible because of the problem formulation.  In particular,
aliasing between partitions is removed by the data replication transformation in
Section~\ref{subsec:trans-data}, and program statements operate on
partitions which hide the details of individual memory accesses.

\subsection{Copy Intersection Optimization}
\label{subsec:trans-intersect}

Copies are issued between pairs of source and destination regions, but
only the intersections of the regions must actually be
copied. The number, size and extent of such intersections are unknown
at compile time; this is an aspect of the analysis that is deferred
until runtime. For a large class of high-performance scientific
applications, the number of such intersections per region is
$\mathcal{O}(1)$ in the size of the overall problem and thus for these
codes an optimization to skip copies for empty intersections is able
to reduce the complexity of the loop on Figure~\ref{fig:trans_code4a}
line 9 from $\mathcal{O}(N^2)$ to
$\mathcal{O}(N)$. Figure~\ref{fig:trans_code4b} shows the
code following this optimization; note the changes on lines 5 and
10. For clarity of presentation the intersections on line 5 are
written in pseudocode. In the implementation the compiler generates
equivalent Regent code.

To avoid an $\mathcal{O}(N^2)$ startup cost in comparing all
pairs of subregions in the computation of intersections at line 5 in
Figure~\ref{fig:trans_code4b}, we apply an additional optimization
(not shown in the figure). The computation of intersections
proceeds in two phases. First, we compute a shallow intersection to
determine which pairs of regions overlap (but not the extent of the
overlap). For unstructured regions, an interval tree acceleration data
structure makes this operation
$\mathcal{O}(N \operatorname{log} N)$. For
structured regions, we use a bounding volume hierarchy for this
purpose. Second, we compute the exact set of overlapping elements between
known-intersecting regions. Following the creation of shard tasks in
Section~\ref{subsec:trans-control} these operations are performed
inside the individual shards, making them $\mathcal{O}(M^2)$ where $M$ is the
number of non-empty intersections for regions owned by that shard.

Note that while shallow intersections are initially placed immediately
prior to the transformed code, the placement may subsequently be
altered by standard optimizations such as loop-invariant code
motion. In the applications tested in Section~\ref{sec:evaluation},
the shallow intersections were all lifted up to the beginning of the
program execution.

In practice, at 1024 nodes, the impact of intersection computations on total
running time is negligible, especially for long-running
applications. Section~\ref{subsec:intersection-times} reports running times for
the intersection operations of the evaluated applications.

\subsection{Synchronization Insertion}
\label{subsec:trans-sync}

When moving to multiple shards, it is necessary to synchronize on
copies performed between shards. Shards are created in
Section~\ref{subsec:trans-control}, and thus the inserted
synchronization is initially redundant, but becomes necessary in the
final transformed code.

A naive version
of this synchronization is shown in Figure~\ref{fig:trans_code4c}. The
copy operations on line 11 are issued by the producer of the
data. Therefore, on the producer's side only, copies follow Regent's
normal sequential semantics. Explicit synchronization is therefore
only required for the consumer. Two barriers are used in
Figure~\ref{fig:trans_code4c} on lines 10 and 12. The first barrier on
line 10 preserves write-after-read dependencies and ensures that the
copy does not start until all previous consumers of \texttt{QB}
(i.e.\ \texttt{TG} tasks from the previous iteration of the outer loop)
have completed. The second barrier on line 12 preserves
read-after-write dependencies and ensures that subsequent consumers of
\texttt{QB} (i.e.\ subsequent \texttt{TG} tasks) do not start until the
copy has completed.

As an additional optimization (not shown), these barriers are
replaced with point-to-point synchronization. In particular, the tasks
which require synchronization are exactly those with non-empty intersections computed
in Section~\ref{subsec:trans-intersect}, thus the sets of tasks that
much synchronize are computed dynamically from the intersections
above. The placement of synchronization operations in the transformed
code is determined as follows. A simple dataflow analysis
determines all consumers of \texttt{QB} preceding the copy on line 11
and all those following; these tasks synchronize with copies on line
11 as determined by the non-empty intersections computed in
\texttt{IQPB}. This form of synchronization in Regent has the
additional benefit that the point-to-point synchronization operators
can be added as a direct precondition or postcondition to a task and
therefore do not block the main thread of control as would a
traditional barrier.

\subsection{Creation of Shards}
\label{subsec:trans-control}

In the final stage of the transformation, control flow itself is
replicated by creating a set of shard tasks that distribute the
control flow of the original program. Figure~\ref{fig:trans_code4d}
shows the code after the completion of the following steps.

First, the iterations of the inner loops for \texttt{TF} and
\texttt{TG} must be divided among the shards. Note this division does not 
determine the mapping of a task to a processor for execution, which is discussed
in Section~\ref{subsec:impl-mapping}. This
simply determines ownership of tasks for the purposes of runtime
analysis and control flow. The assignment is decided by a simple
block partition of the iteration space into \texttt{NS} roughly even blocks on line 14.
Second, the compiler transforms the loops so that the innermost loops
are now over iterations owned by each shard, while the new outermost
loop on line 15 iterates over shards.

Third, the compiler extracts the body of the shard into a new task on
lines 2-10. This task is called from the main loop on line 18.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "spmd"
%%% End: 
