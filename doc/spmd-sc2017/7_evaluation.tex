\section{Evaluation}
\label{sec:evaluation}

We evaluate performance and scalability of control replication in the
context of Regent with four applications: a stencil benchmark on a
regular grid; MiniAero, an explicit solver of the compressible
Navier-Stokes equations on a 3D unstructured mesh; PENNANT, a
Lagrangian hydrodynamics simulation on a 2D unstructured mesh; and a
circuit simulation on a sparse unstructured graph. For each
application we consider a Regent implementation with and without
control replication and when available a reference implementation
written in MPI or a flavor of MPI+X.

For each application, we report weak scaling performance on up to 1024
nodes of the Piz Daint supercomputer~\cite{PizDaint}, a Cray XC50
system. Each node has an Intel Xeon E5-2690 v3 CPU (with 12 physical
cores) and 64 GB of memory. Legion was compiled with GCC 5.3.0. The
reference codes were compiled with the Intel C/C++ compiler
17.0.1. Regent used LLVM for code generation: version 3.8.1 for
Stencil and PENNANT and 3.6.2 for MiniAero and Circuit.

%% FIXME: Explain why different LLVM versions were used

Finally, we report the running times of the dynamic region
intersections for each of the applications at 64 and 1024 nodes.

\subsection{Stencil}
\label{subsec:stencil}

\input{e4_stencil}

% Efficiency (updated 2017-04-01):
%   1024 nodes:
%     58.620652 / 59.173497 = 0.990657
%   2048 nodes:
%     58.620652 / 59.756428 = 0.980993

% Problem size (updated 2017-04-01):
%   40k * 40k cells per node
%   60 iterations

% Configuration (updated 2017-04-01):
%   MPI: 10 MPI per node
%   MPI+OpenMP: 1 ranks per node, 10 OpenMP threads per rank
%   Regent: 10 application cores per node
% Memory-bound, so additional cores don't help or even hurt

Stencil is a 2D structured benchmark from the Parallel Research
Kernels (PRK)~\cite{PRK14, PRKRuntimes16}. The code performs a stencil
of configurable shape and radius over a regular grid. Our experiments
used a radius-2 star-shaped stencil on a grid of double-precision
floating point values with $\mathrm{40k}^2$ grid points per node. We
compared against the MPI and MPI+OpenMP reference codes provided by
PRK. Both reference codes require square inputs and thus were run only
at node counts that were even powers of two.

As noted in Section~\ref{subsec:target-programs}, all analysis for
control replication was performed at the task and region
level. Control replication was able to optimize code containing affine
access patterns, without requiring any specific support for affine
reasoning in the compiler.

Figure~\ref{fig:stencil} shows weak scaling performance for Stencil up
to 1024 nodes. (In the legend control replication is abbreviated as
CR.) Control replication achieved 99\% parallel efficiency at 1024
nodes, whereas Regent without control replication rapidly drops in
efficiency when the overhead of launching an increasing number of subtasks
begins to dominate the time to execute those subtasks, as discussed
in Section~\ref{sec:intro}.

\subsection{MiniAero}
\label{subsec:miniaero}

\input{e2_miniaero}

% Problem size (updated 2017-04-04):
%   256k cells/node (256 x 512 x 4)

% Efficiency (updated 2017-04-01):
%   1024 nodes:
%     11.50868 / 11.353638 = 1.013655

MiniAero is a 3D unstructured mesh proxy application from the Mantevo
suite~\cite{Mantevo} developed at Sandia National Laboratories,
implementing an explicit solver for the compressible Navier-Stokes
equations. The mini-app is written in a hybrid style, using MPI for
inter-node communication and Kokkos for intra-node
parallelism. (Kokkos is a portability layer for C++ that compiles down
to pthreads (on CPUs), and is also developed at Sandia~\cite{Kokkos}.) We ran
the reference in two configurations: one MPI rank per core, and
one MPI rank per node (using Kokkos support for intra-node
parallelism).

Figure~\ref{fig:miniaero} shows weak scaling absolute performance for
the various implementations of MiniAero on a problem size of 512k
cells per node. As demonstrated in \cite{Regent15}, Regent-based codes
out-perform the reference MPI+Kokkos implementations of MiniAero on a single
node, mostly by leveraging the improved hybrid data layout features of
Legion~\cite{LegionFields14}.

Control replication achieves slightly over 100\% parallel efficiency
at 1024 nodes due to variability in the performance of individual
nodes; as before, Regent without control replication struggles to
scale beyond a modest number of nodes. Although the rank per node
configuration of the MPI+Kokkos reference provides initial benefits to
single-node performance, performance eventually drops to the level of
the rank per core configuration.

\subsection{PENNANT}
\label{subsec:pennant}

\input{e3_pennant}

% Problem size (updated 2017-04-01):
%   7.4M zones/node (320 x 23040, leblanc_long4 problem size)

% Efficiency (updated 2017-04-01):
%   1024 nodes:
%     12.643584 / 14.49727 = 0.872135

PENNANT is a 2D unstructured mesh proxy application from Los Alamos
National Laboratory simulating Lagrangian
hydrodynamics~\cite{PENNANT}. The application represents a subset of
the functionality that exists in FLAG, a larger production code used
at the lab~\cite{FLAG}.

The reference PENNANT implementation applies a cache blocking
optimization that substantially improves the computational intensity
of the overall application. This optimization impacts even the data
structure layouts, as the (otherwise unordered) mesh elements are
grouped into chunks to be processed together. In spite of this,
control replication applied seamlessly to the code, as the details of
the cache blocking optimization are limited to the structure of the region tree
(which subsumes the chunk structure of the original code) and the
bodies of tasks (whose details are accurately summarized by the
privileges declared in the task declaration).

Figure~\ref{fig:pennant} shows weak scaling performance for PENNANT on
up to 1024 nodes, using a problem size of 7.4M zones per
node. The single-node performance of the Regent implementation is less
than the reference because the underlying Legion runtime requires a core be
dedicated to analysis of tasks. This effect is noticeable on PENNANT
because, due to the cache blocking optimization above, PENNANT is
mostly compute-bound.

However, this performance gap closes at larger
node counts as Regent is better able to achieve asynchronous execution
to hide the latency of the global scalar reduction to compute the $dt$
in the next timestep of the application. At 1024 nodes, control
replication achieves 87\% parallel efficiency, compared to 82\% for
MPI and 64\% for MPI+OpenMP.

\subsection{Circuit}
\label{subsec:circuit}

\input{e1_circuit}

% Problem size (updated 2017-04-01):
%   2.5k circuit nodes/piece, 10k wires/piece
%   10 pieces per node
%   25k circuit nodes/node, 100k wires/node

% Efficiency (updated 2017-04-01):
%   1024 nodes:
%     28.19835 / 28.787816 = 0.979523
%   2048 nodes:
%     28.19835 / 28.783989 = 0.979654

We developed a sparse circuit simulation based on \cite{Legion12} to
measure weak scaling performance on unstructured graphs. The
implicitly parallel version from \cite{Legion12} was already shown to
be substantially communication bound at 32 nodes and would not have
scaled to significantly more nodes, regardless of the implementation
technique. The input for this problem was a randomly generated sparse
graph with 100k edges and 25k vertices per compute node; the
application was otherwise identical to the original.

Figure~\ref{fig:circuit} shows weak scaling performance for the
simulation up to 1024 nodes. Regent with control replication achieves
98\% parallel efficiency at 1024 nodes. As with the other applications,
Regent without control replication matches this performance at small node counts
(in this case up to 16 nodes) but then efficiency begins to drop rapidly as
the overhead of having a single master task launching many subtasks becomes dominant.

\subsection{Dynamic Intersections}
\label{subsec:intersection-times}

As described in Section~\ref{subsec:trans-intersect}, dynamic region
intersections are computed prior to launching a set of shard tasks
in order to identify the communication patterns and
precise data movement required for control-replicated
execution. Table~\ref{tab:intersections} reports the running times of the
intersection operations measured during the above experiments while
running on 64 and 1024 nodes. Shallow intersections are performed on a
single node to determine the approximate communication pattern (but
not the precise sets of elements that require communication); these
required at most 259~ms at 1024 nodes (15~ms at 64 nodes). Complete
intersections are then performed in parallel on each node to determine
the precise sets of elements that must be communicated with other
nodes; these took at most 124~ms.
% FIXME: We've got a bug in complete intersections, they are NOT constant
%% (Running times for complete
%% intersections were nearly constant across node counts.)
Both times are
much less than the typical running times of the applications
themselves, which are often minutes to hours.

\begin{table}
\centering
\begin{tabular}{r c | c c}
Application & Nodes & Shallow (ms) & Complete (ms) \\
\hline

\multirow{2}{*}{Circuit} & 64 & 7.8 & 2.7 \\
& 1024 & 143 & 4.7 \\
\hline

\multirow{2}{*}{MiniAero} & 64 & 15 & 17  \\
& 1024 & 259 & 43 \\
\hline

\multirow{2}{*}{PENNANT} & 64 & 6.8 & 14 \\
& 1024 & 125 & 124 \\
\hline

\multirow{2}{*}{Stencil} & 64 & 2.7 & 0.4 \\
& 1024 & 78 & 1.3
\end{tabular}
\caption{Running times for region intersections on each application at 64 and 1024 nodes.}
\label{tab:intersections}
\end{table}

% Shallow Intersection (updated 2017-04-04):

% At 1024 nodes:
%%   * circuit
%%     list_cross_product: 143 ms
%%     list_cross_product_complete:
%%   * miniaero
%%     list_cross_product: 259 ms
%%     list_cross_product_complete:
%%   * pennant
%%     list_cross_product: 125 ms
%%     list_cross_product_complete:
%%   * stencil
%%     list_cross_product: 78 ms
%%     list_cross_product_complete:

% At 64 nodes:
%%   * circuit
%%     list_cross_product: 7.9  ms
%%     list_cross_product_complete:
%%   * miniaero
%%     list_cross_product: 15.2 ms
%%     list_cross_product_complete:
%%   * pennant
%%     list_cross_product: 6.8 ms
%%     list_cross_product_complete:
%%   * stencil
%%     list_cross_product: 2.7 ms
%%     list_cross_product_complete:


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "spmd"
%%% End:
