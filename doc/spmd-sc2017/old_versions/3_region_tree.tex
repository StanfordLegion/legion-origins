\section{Region Tree Analysis}
\label{sec:rtree}

\input{f4_grammar}

Control replication requires intimate understanding of the data
access patterns and aliasing in an application. For every pair of
statements, the
compiler must determine whether the pair may potentially access the
same data (i.e., may access the same or aliased regions), as this
implies communication within the program. In Figure~\ref{fig:code2a},
for example, the tasks called on lines 8 and 12 access subregions
\texttt{ghost[i]} and \texttt{master[j]} respectively. (For clarity,
the loops on lines 7 and 11 have been given distinguishing loop
variables \texttt{i} and \texttt{j}.) Because the partitions
\texttt{ghost} and \texttt{master} derive from the same region
\texttt{private\_vs\_ghost[1]} (lines 4-5), these accesses may
overlap; thus there is communication implied between the loops.

%% FIMXE: Or array accesses? At compile time?

Broadly speaking, this problem is isomorphic to the alias analysis
problem for pointers. In a programming language where all aliasing
must be inferred at the granularity of individual pointer references,
such as in C/C++, this is a well-known hard problem at compile
time. Our situation is different, however, as the Regent programming
model captures explicit aliasing information at the coarser
granularity of the region hierarchy, which is both much easier to
reason about statically and defined by the programmer precisely to
describe the important subsets of the data and their relationships. We
present an efficient analysis
that determines which regions a statement may access and identifies any
potential overlap between pairs of regions.

To facilitate this discussion, Figure~\ref{fig:regent-core} presents a
grammar for \emph{Core Regent}, a fragment of the complete Regent
language that focuses on the key features relevant to this
analysis. (Core Regent follows semantics described in
\cite{LegionTypes13}, though the surface syntax differs somewhat.)
Here we are concerned with analyzing the data access patterns of code
fragments such as those in Figure~\ref{fig:code2a}, containing
sequences of simple parallel task launches.

\subsection{Target Tasks}
\label{sec:rtree-target-tasks}

Control replication is a local optimization that transforms a single
target task. While control replication places restrictions on the form
of the target task, no restrictions are placed on caller or callee
tasks. The compiler analysis for control replication need not be
concerned with the contents of called tasks because the behavior of a
task is completely described by the privileges in the task's
declaration.

The target task must contain a set of loops of task calls with no
loop-carried dependencies except for those resulting from reductions
to region arguments or scalar variables. The loops must use the same
loop bounds. The set of loops may be surrounded with any arbitrary
control flow and statements over scalar variables.

The region arguments of any called tasks must be of the form
\texttt{p[f(i)]} where \texttt{p} is a partition, \texttt{i} is the
loop index, and \texttt{f} is a pure function. Any accesses with a
non-trivial function \texttt{f} will be transformed into the form
\texttt{q[i]} with a new partition \texttt{q} such that \texttt{q[i]}
is \texttt{p[f(i)]}. Note here that we make essential use of Regent's
ability to define multiple partitions of the same data.

\subsection{Region Trees}
\label{sec:rtree-region-trees}

%% Regent employs a type system which makes data and data access patterns
%% explicit~\cite{LegionTypes13}. Regent's primary data structures are
%% \emph{regions}, which are simply containers for elements. Accesses to
%% regions are tracked explicitly by Regent's type system, enabling a
%% tractable static analysis of data access patterns in the application.

%% Regions can be \emph{partitioned} into
%% \emph{subregions}. Figure~\ref{fig:code2a} includes the partitioning
%% for heat diffusion, where owned and ghost define partitions of all
%% cells following the access patterns shown in
%% Figure~\ref{fig:partitioning}. (The details of how specific mesh
%% elements are included in specific subregions are not important for our
%% purposes and are omitted.) Note that subregions of owned cells (line
%% 2) are non-overlapping, or \emph{disjoint}, whereas subregions of
%% ghost cells (line 3) are overlapping, or \emph{aliased}, (and do not
%% even cover the entire mesh).

%% Regions may be recursively decomposed into subregions, so it is
%% natural to view the overall organization of data as a \emph{region
%%   tree}. Figure~\ref{fig:region-tree} shows the tree for the example
%% above.

Regions can be partitioned into subregions. Recursive application of
partitioning results in a hierarchy of regions, so it is natural to
view the overall organization of data as a \emph{region
  tree}. Figure~\ref{fig:region-tree} shows the region tree for the
code in Figure~\ref{fig:code2a}. Note that regions in this formulation
are \emph{symbolic}, that is, the indices used to identify subregions
are either constants or unevaluated loop variables. A dynamic evaluation
of this program would result in an expansion of this tree for the
various iterations of the loops (resulting in e.g. \texttt{private[0]},
\texttt{private[1]}, \ldots, \texttt{private[N-1]} under the
\texttt{private} partition), as seen in \cite{Legion12}. However, the
number of iterations is not available at compile-time, making the
symbolic version necessary.

The region tree is convenient because it provides a natural test to
determine whether any two regions may potentially alias: For any
regions $R$ and $S$, find the least common ancestor $A$ with immediate
children $R'$ and $S'$ (along the path to $R$ and $S$,
respectively). If $A$ is a disjoint partition and $R'$ and $S'$ are
indexed by constants, then $R$ and $S$ are guaranteed to be disjoint
regions at runtime; otherwise they may alias.

Region trees can be constructed by walking the program source from top
to bottom. Each newly created region becomes the root of a fresh region
tree. Partitions are inserted under the region they partition, and
expressions that access subregions of partitions result in the
corresponding subregion nodes, tagged with the index expression used.

\input{f5_annotated_code_pennant}

\input{f6_region_tree}

\input{f7_rdir_partial}

\input{f8_rdir_complete}

\subsection{Recording Region Usage}
\label{sec:rtree-region-usage}

The first step in our method for converting an implicit parallel
program into SPMD form requires the compiler to annotate each program statement with the
regions (and partitions) that statement uses, as well as how the
region (partition) is used: Does the statement read from (\emph{R}),
write to (\emph{W}) or reduce to (\emph{Red}) a given field of the
region?  For most statements inferring the \emph{privileges}
(\emph{R}, \emph{W}, or \emph{Red}) is easy. The only non-obvious case
is where the privileges are obtained for task calls, but tasks in Regent
must declare their privileges on region arguments. Region usage for a
statement is recorded as a list of tuples of the form
\emph{privilege(region.field)}.

Using privileges, and knowledge of the region tree, the compiler can
analyze the data usage for loops as follows. Loops summarize the
privileges of their bodies, dropping loop indices from any region
index expressions to indicate that multiple (and not just one)
subregions may be accessed. For example, in the first inner loop (lines
7-10), the compiler records accesses \emph{RW}(\texttt{private[i].f})
and \emph{Red+}(\texttt{ghost[i].f}) for the call inside the loop, and
\emph{RW}(\texttt{private.f}) and \emph{Red+}(\texttt{ghost.f}) for
the loop itself. Figure~\ref{fig:code2a} shows the PENNANT program
with region access annotations.
