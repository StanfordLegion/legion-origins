\section{Regent}
\label{sec:regent}

Regent is a programming language with support for both implicit and
explicit parallelism, making it possible to describe the control
replication transformation entirely within one system. In particular,
Regent's support for multiple partitions of data collections
enables a particularly straightforward analysis of data movement
required for efficient SPMD code generation. In this section, we
discuss the relevant features of Regent and a number of preliminary
steps to control replication.

\subsection{Data and Execution Model}
\label{subsec:regent-model}

\input{f2_code_regent}

A central concern of the Regent programming language is the management
and partitioning of data. Data in Regent is stored in
\emph{regions}. A region is a (structured or unstructured) collection
of objects and may be \emph{partitioned} into subregions that name
subsets of the elements of the parent region.

Figure~\ref{fig:code2} shows a Regent version of the program in
Figure~\ref{fig:tasks_code1a}. The two inner loops with calls to point
functions \texttt{F} and \texttt{G} have been extracted into tasks
\texttt{TF} and \texttt{TG} on lines 1-6 and 8-13, respectively, and
the new main simulation loop is preceded by an explicit partitioning
of the data on lines 16-22.

Lines 18 and 19 declare two regions \texttt{A} and \texttt{B} that
correspond to the arrays by the same name in the original
program. These regions contains elements of some data type indexed
from 0 to $\mathtt{N}-1$. (The element data type does not matter for
the purposes of this paper.) The declaration of the \emph{index space} \texttt{U} on line 16
gives a name to the set of indices for the regions; symbolic
names for sets of indices are helpful because in general regions may be
structured or unstructured, and optionally sparse. In Regent, memory
allocation for regions
is decoupled from their declaration. No actual memory allocation occurs at lines 18-19. Instead
the program proceeds to partition the regions into subregions so that
the eventual memory allocations are distributed across the machine.

Lines 20-22 contain calls to partitioning operators. The first two of
these, on lines 20 and 21, declare block partitions of the regions
\texttt{A} and \texttt{B} into roughly equal-sized subregions numbered
0 to $\mathtt{NT}-1$. (As before, a variable \texttt{I} is declared on
line 17 to name this set of indices.) The variables \texttt{PA} and
\texttt{PB} name
the sets of subregions created in the respective partitioning
operations. For convenience, we name the object which represents a set
of subregions a \emph{partition}.

Line 22 declares a second partition \texttt{QB} of the region
\texttt{B} based on the image of the function \texttt{h} over
\texttt{PB}. That is, for every index \texttt{b} in region
\texttt{B}'s index space (\texttt{U}), $\texttt{h(b)} \in \texttt{QB[i]}$ if $\texttt{b} \in
\texttt{PB[i]}$. This partition describes exactly the set of elements
that will be read inside the task \texttt{TG} on line~11.  Importantly,
there are no restrictions on the form or semantics of \texttt{h}. As a
result, \texttt{QB} may not be a partition in the mathematical sense;
i.e.\ the subregions of \texttt{QB} are not required to be disjoint,
and the union of subregions need not cover the entire region
\texttt{B}. In practice this formulation of partitioning is extremely
useful for naming the sets of elements involved in e.g.\ halo
exchanges.

Regent supports a number of additional operators as part of an
expressive sub-language for partitioning
\cite{DependentPartitioning16}. In the general case, Regent partitions
are extremely flexible and may divide regions into subregions
containing arbitrary subsets of elements. For the purposes of this
paper, the only property of partitions that is necessary to analyze
statically is the disjointness of partitions. A partition object is
said to be \emph{disjoint} if the subregions can be statically proven
to be non-overlapping, otherwise the partition is \emph{aliased}. For
example, the block partition operators on lines 20-21 produce disjoint
partitions as the subregions are always guaranteed to be
non-overlapping. For the image operator on line 22, the function
\texttt{h} is unconstrained and thus Regent assumes that the
subregions may contain overlaps, causing the resulting partition to be
considered aliased.

The main simulation loop on lines 23-30 then executes a sequence of
task calls with the appropriate subregions as arguments. Tasks declare
privileges on their region arguments (\emph{read}, \emph{write}, or
\emph{reduce} on an associative and commutative operator). Execution
of tasks is apparently sequential: two tasks may execute in parallel
only if they operate on disjoint regions, or with compatible
privileges (e.g.\ both read, or both reduce with the same
operator). Regent programs are typically written such that the inner
loop can execute in parallel; in this case the loops on lines 24-26
and 27-29 both execute in parallel.

Note that in Regent, unlike in the fork-join parallel execution of
Figure~\ref{fig:tasks1a}, there is not an implicit global
synchronization point at the end of each inner loop. Instead, Regent
computes the dependencies directly between pairs of tasks (as
described above) and thus tasks from different inner loops may execute
in parallel if doing so preserves sequential semantics.

An important property of Regent tasks is that privileges are
\emph{strict}. That is, a task may only call another task if its own
privileges are a superset of those required by the other
task. Similarly, any reads or writes to elements of a region must
conform to the privileges specified by the task. As a result, a
compile-time analysis such as control replication need not consider
the code inside of a task. All of the analysis for control replication
will be at the level of tasks, privileges declared for tasks, region
arguments to tasks, and the disjointness or aliasing of region
arguments to tasks.

\subsection{Target Programs}
\label{subsec:target-programs}

In this paper we consider programs containing forall-style loops of
task calls such as those on lines 24-26 and 27-29 of
Figure~\ref{fig:code2}. Control replication is a local optimization
and need not be applied to an entire program to be effective. The
optimization is applied automatically to the largest set of statements
that meet the requirements described below. In the example, control
replication will be applied to lines 23-30 of the program.

Control replication applies to loops of task calls with no
loop-carried dependencies except for those resulting from reductions
to region arguments or scalar variables. Arbitrary control flow is
permitted outside of these loops, as are statements over scalar
variables.

No restrictions are placed on caller or callee tasks; control
replication is fully composable with nested parallelism in the
application. The compiler analysis for control replication need not be
concerned with the contents of called tasks because the behavior of a
task is soundly approximated by the privileges in the task's
declaration (a property enforced by Regent's type system). Similarly, any caller task is completely agnostic to the
application of control replication because any possible transformation
of the code must be consistent with the task's privileges.

The region arguments of any called tasks must be of the form
\texttt{p[f(i)]} where \texttt{p} is a partition, \texttt{i} is the
loop index, and \texttt{f} is a pure function. Any accesses with a
non-trivial function \texttt{f} are transformed into the form
\texttt{q[i]} with a new partition \texttt{q} such that \texttt{q[i]}
is \texttt{p[f(i)]}. Note here that we make essential use of Regent's
ability to define multiple partitions of the same data.

\subsection{Region Trees}
\label{subsec:region-trees}

\input{f6_region_tree}

The semantics of Regent enables a straightforward analysis of aliasing
based on the relationships between regions and partitions. To
determine whether two regions may alias, the compiler constructs a
\emph{region tree} that describes these relationships. This tree is a
compile-time adaptation of the runtime data structure described in
\cite{Legion12}.

Figure~\ref{fig:region-tree} shows the region tree for the code in
Figure~\ref{fig:code2}. Note that regions in this formulation are
\emph{symbolic}, that is, the indices used to identify subregions are
either constants or unevaluated loop variables. A dynamic evaluation
of this program would result in an expansion of this tree for the
various iterations of the loops (resulting in e.g.\ \texttt{PA[0]},
\texttt{PA[1]}, \ldots, \texttt{PA[NT-1]} under the \texttt{PA}
partition). However, the number of iterations is not available at
compile-time, making the symbolic version necessary.

The region tree is convenient because it provides a natural test to
determine whether any two regions may alias: For any pair
of regions $R$ and $S$, find the least common ancestor $A$ with
immediate children $R'$ and $S'$ (along the path to $R$ and $S$,
respectively). If $A$ is a disjoint partition and $R'$ and $S'$ are
indexed by constants, then $R$ and $S$ are guaranteed to be disjoint
regions at runtime; otherwise they may alias.

Region trees can be constructed by walking a task's body from top
to bottom. Each newly created region becomes the root of a fresh region
tree. Partitions are inserted under the region they partition, and
expressions that access subregions of partitions result in the
corresponding subregion nodes, tagged with the index expression used.
