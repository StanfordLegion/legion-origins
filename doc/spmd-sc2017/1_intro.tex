%% \eject % Hack: Copyright block causes section header to flow to next page.

%% \vspace*{-0.65cm}

\section{Introduction}
\label{sec:intro}

\input{f1_tasks}

Programs with sequential semantics are easy to read, understand,
debug, and maintain, compared to explicitly parallel codes. In certain
cases, sequential programs also lend themselves naturally to parallel
execution. Consider the code in
Figure~\ref{fig:tasks_code1a}. Assuming there are no loop carried
dependencies, the
iterations of each of the two inner loops can be executed in parallel
on multiple processors in a straightforward fork-join style.  As
illustrated in Figure~\ref{fig:tasks1a}, a main thread launches a
number of worker threads for the first loop, each of which executes
one (or more) loop iterations. There is a synchronization point at the
end of the loop where control returns to the main thread; the second
loop is executed similarly.  Because the second loop can have a completely
different data access pattern than the first (indicated by the arbitrary
function $\mathtt{h}$ in $\mathtt{B[h(j)]}$), complex
algorithms can be expressed.
With considerable variation, this \emph{implicitly parallel} style is the
basis of many parallel programming models.  Classic parallelizing
compilers~\cite{Polaris95, SUIF96, PIPS91}, HPF~\cite{HPF96, HPF07}
and the data parallel subset of Chapel~\cite{Chapel07} are canonical
examples; other examples
include MapReduce~\cite{MapReduce04} and Spark~\cite{Spark10} and
task-based models such as Sequoia~\cite{SequoiaLanguage06}, Legion
\cite{Legion12}, StarPU\cite{StarPU11}, and PaRSEC~\cite{PARSEC13}.

In practice, programmers don't write highly scalable high performance
codes in the implicitly parallel style of Figure~\ref{fig:tasks_code1a}.  Instead, they
write the program in Figure~\ref{fig:tasks_code1b}.  Here the
launching of a set of worker threads happens once, at program start,
and the workers run until the end of the computation.  We can see in
Figures~\ref{fig:tasks1a} and \ref{fig:tasks1b} that conceptually the
correspondence between the programs is simple.  Where
Figure~\ref{fig:tasks_code1a} launches $N$ workers in the first loop
and then $N$ workers in the second loop, Figure~\ref{fig:tasks_code1b}
launches $N$ long-lived threads that act as workers across iterations
of the inner loops of the program.  This \emph{single program multiple
data}, or SPMD, programming style is the basis of MPI~\cite{MPI},
UPC~\cite{UPCSTANDARD} and Titanium~\cite{TITANIUMSTANDARD}, and also
forms a useful subset of Chapel, X10 \cite{X1005} and Legion.

While Figure~\ref{fig:tasks_code1a} and Figure~\ref{fig:tasks_code1b}
are functionally equivalent, they have very different scalability and
programmer productivity properties.  Figure~\ref{fig:tasks_code1b} is
much more scalable, and not just by a constant factor.  
To see this, consider what happens in
Figure~\ref{fig:tasks_code1a} as the number of workers $N$ (the
``height'' of the execution graph in Figure~\ref{fig:tasks1a}) increases.
Under weak scaling, the time to execute each worker task (e.g., $\mathtt{F(A[i])}$ in
the first loop) remains constant, but the main control thread does
$\mathcal{O}(N)$ work to launch $N$ workers. (In some cases, a
broadcast tree can be used to reduce this overhead to
$\mathcal{O}(\operatorname{log} N)$.)  Thus, for some $N$, the runtime
overhead of launching workers exceeds the individual worker's execution time
and the program ceases to scale.  While
the exact scalability in practice always depends on how
long-running the parallel worker tasks are, our experience is that
many implicitly parallel programs don't scale beyond 10 to 100 nodes
when task granularities are on the order of milliseconds to tens of
milliseconds.  In
contrast, the SPMD program in
Figure~\ref{fig:tasks_code1b}, while it still 
must launch $N$ workers, does so only once and in particular the launch
overhead is not incurred in every time step (the \texttt{T} loop).  Programs written in SPMD style
can scale to thousands or tens of thousands of nodes.
 
On the other hand, the implicitly parallel program in
Figure~\ref{fig:tasks_code1a} is much easier to write and maintain
than the program in Figure~\ref{fig:tasks_code1b}.  While it is not
possible to give precise measurements, it is clear that the difference in productivity is large: 
In our experience an implicitly parallel
program that takes a day to write will require roughly a week to code in SPMD style.  
The extra programming cost is incurred
because the individual workers in Figure~\ref{fig:tasks_code1b} each
compute only a piece of the first loop of
Figure~\ref{fig:tasks_code1a}, and thus explicit synchronization is
required to ensure that all fragments of the first loop in all workers
finish before dependent parts of the second loop begin.  Furthermore,
because the access patterns of the two loops in
Figure~\ref{fig:tasks_code1a} need not be the same, data movement is
in general also needed to ensure that the values written by the
various distributed pieces of the first loop are communicated to the
threads that will read those values in the distributed pieces of the
second loop.  In most SPMD models (and specifically in MPI) this
data movement must be explicitly written and optimized by the
programmer.  The synchronization and the data movement are by far the most
difficult and time consuming parts of SPMD programs to get right, and these
are exactly the parts that are not required in implicitly parallel programs.

This paper presents \emph{control replication}, a technique for
generating high-performance and scalable SPMD code from implicitly
parallel programs with sequential semantics. The goal of control
replication is to both ``have our cake and eat it'', to allow programmers
to write in the productive implicitly parallel style and to use
a combination of compiler optimizations and runtime analysis to
automatically produce the scalable (but much more complex) SPMD equivalent.

Control replication works by generating a set of \emph{shards}, or long-running
tasks, from the control flow of the original program, to amortize
overhead and enable efficient execution on large numbers of
processors.  Intuitively, a control thread of an implicitly
parallel program is \emph{replicated} across the shards, with each shard 
maintaining enough state to mimic the decisions of the 
original control thread.  An important feature of control replication
is that it is a local transformation, applying to a single collection
of loops. Thus, it need not be applied only at the top level, and can in fact be 
applied independently to different parts of a program and at multiple
different scales of nested parallelism.

As suggested above, the heart of the control replication transformation depends
on the ability to analyze the implicitly parallel program with sufficient precision
to generate the needed synchronization and data movement between shards.  Similar analyses
are known to be very difficult in traditional programming languages.  Past approaches
that have attempted optimizations with comparable goals to control replication have
relied on either very sophisticated, complex and therefore unpredictable static analysis (e.g., HPF) or
have relied much more heavily on dynamic analysis with associated run-time overheads (e.g.,
inspector-executor systems \cite{InspectorExecutorIrregular12}).  

A key aspect of our work is that we leverage recent advances in
parallel programming model design that greatly simplify and make
reliable and predictable the static analysis component of control
replication.  Many parallel programming models allow programmers to
specify a \emph{partition} of the data, to name different subsets of
the data on which parallel computations will be carried out.  Recent proposals
allow programmers to define and use \emph{multiple} partitions of
the same data \cite{Legion12, Bocchino09}.  For example,
returning to our abstract example in Figure~\ref{fig:tasks_code1a},
one loop may be accessing a matrix partitioned by columns while the
other loop accesses the same matrix partitioned by rows.  Control
replication relies on the programmer to declare the data partitions
of interest (e.g., rows and columns).  The static analysis is carried
out only at the granularity of the partitions and
determines which partitions may share elements and therefore might require
communication between shards. The dynamic analysis optimizes the communication
at runtime by computing exactly which elements they share.

An important property of this approach is that the control replication transformation is
guaranteed to succeed for any programmer-specified partitions of
the data, even though the partitions can be arbitrary.  Partitions
name program access patterns, and control replication
reasons at the level of those coarser collections and their possible overlaps.
This situation contrasts with the static analysis of programs where the access patterns must be inferred
from individual memory references; current techniques, such as
polyhedral analyses, work very well for affine index expressions
\cite{AffineDistributedMemory13}, but do not address programs with
more complex accesses.

This paper makes the following contributions:

\begin{itemize}
\item We describe the design and implementation of control replication
  in the context of the Regent programming
  language \cite{Regent15}. As noted above, the critical feature of
  Regent for control replication is support for multiple partitions;
  the technique should be applicable to any language with this feature.

\item To the best of our knowledge, we are the first to demonstrate
  the impact of programming model support for multiple partitions on a
  compiler analysis and transformation.  We show that this feature
  can be leveraged to provide both good productivity and scalability.

\item We evaluate control replication using four codes: a circuit
  simulation on an unstructured graph, an explicit solver for the
  compressible Navier-Stokes equations on a 3D unstructured mesh, a
  Lagrangian hydrodynamics proxy application on a 2D unstructured
  mesh, and a stencil benchmark on a regular grid. Our implementation
  of control replication achieves up to 99\% parallel efficiency on
  1024 nodes (12288 cores) on the Piz Daint
  supercomputer~\cite{PizDaint} with absolute performance comparable
  to hand-written MPI(+X) codes.

\texcomment{
\item For the class of applications considered, our technique is the
  first (to the best of our knowledge) to provide practical levels of
  scalability for implicitly parallel programs.  We demonstrate
  scaling to 48x more cores than the previous best known techniques
  (12288 vs. 256 cores) while maintaining performance competitive with
  hand-written MPI(+X) codes.
}

\end{itemize}

In the following section, we describe the relevant features of Regent
for control replication and give a motivating example. We then
describe the transformation, and an evaluation of our implementation,
before discussing related work and concluding.

\texcomment{
A common approach to address this issue is to rearrange the code to
increase the running time of tasks~\cite{ForkJoinSPMD90}. Such a
modified program is shown in 
initially that the indexing function \texttt{h} (on line 5) is the
identity, the dependencies between the original loop iterations are
preserved in the modified program, allowing iterations of the new
outer loop to be executed in parallel. Figure~\ref{fig:tasks1b}
illustrates the execution of this code.  There is still overhead in
launching $\mathcal{O}(\mathtt{N})$ tasks from the main thread, but
the launches are done
once at the beginning and the parallel tasks run for the duration, so
the launch overhead is in fact the minimum possible and asymptotically
better than the overhead of the first program ($\mathcal{O}(\mathtt{N})$
instead of $\mathcal{O}(\mathtt{TN})$).  Primarily because of its
scalability on large supercomputers, 


There are, however, two problems.  First, there is a built-in
scalability issue. There is a sequential component, namely the time
spent in the main thread to create and later destroy the parallel work
at the beginning and end of the parallel loops.  This overhead is
proportional to the number of parallel \emph{tasks} (or units of work)
to be executed, divided by the time per task.  As the number of tasks
grows as a function of $\mathcal{O}(\mathtt{TN})$ and the amount of work for each task
stays either constant or decreases, the overhead comes to dominate
useful work at some problem size.



The second problem is well-known: aliasing. Returning to the example
in Figure~\ref{fig:tasks_code1a}, what if \texttt{h} is non-trivial?
Even if the two loops on lines 2 and 5 are individually data parallel,
and so have no dependencies that prevent straightforward
parallelization of the respective loops, the use of the potentially
complex indexing function \texttt{h(j)} introduces non-trivial data
dependencies between the first and second loop---specifically,
whenever $\mathtt{i} = \mathtt{h(j)}$. As before, the sequential
semantics is clear. The fork-join parallel interpretation in
Figure~\ref{fig:tasks1a} is
also fine, because of the synchronization point enforced by the main
thread between the two loops. But when \texttt{h} is non-trivial, then
the SPMD version requires inter-task synchronization (e.g., a global
barrier at the comment on line 4 in Figure~\ref{fig:tasks_code1b}) to
enforce that iterations of the second loop that refer to data touched
in the first loop execute in the required order.  On a distributed
memory machine, data movement may also be necessary to ensure that
updates from calls to \texttt{F} are visible when executing calls to
\texttt{G}, as shown by the dashed edges in Figure~\ref{fig:tasks1b}.

This need for synchronization and data movement is the primary
drawback of writing SPMD programs: this is explicit parallel
programming, with all of its well-known pitfalls for program
complexity, debugging and maintenance. And thus, this is an area where
a compiler, if it were able to generate this code automatically, would
be a substantial boon to the programmer.

Much of the challenge in this approach flows from the complexity of
the function \texttt{h}. Certain classes of functions, such as affine
functions, are known to be amenable to static analysis in a
compiler. Traditional polyhedral optimizations leverage this property
and have been demonstrated to be effective for distributed-memory code
generation \cite{AffineDistributedMemory13}. However, many notable
classes of high-performance applications require more complex indexing
functions, and are unable to take advantage of these approaches. In
particular, computations on unstructured meshes typically use indexing
schemes which are functions of the dynamic problem input, making any
purely static compile-time analysis impossible.

A common alternative in SPMD programming systems is to allow the
programmer to specify a \emph{partition} of the data across the
machine. Critically, this partitioning information gives the compiler
or runtime system coarse-grain, high-level knowledge, and specifically
above the level of trying to reason about individual array or pointer
dereferences, of how the program uses the data.  Static or dynamic
analysis (or both) that leverages the partition information is then
used to simplify programming, improve performance, or both, as in HPF,
UPC, Titanium, X10 and Chapel. In many programming systems, this
feature is called a data \emph{distribution} due to an implicit
limitation that there be only a single, disjoint partition of a given
collection of data, and this distribution maps each data element to a
physical memory location for the entire duration of the lifetime of
the data. In our example, this approach helps with the compilation and
execution of one of the two views of the shared data, say the first
loop with accesses through \texttt{B[i]}, but because there is no way
to describe the second, aliased, access pattern \texttt{B[h(j)]} to
the programming system, the compiler must fall back to a direct
analysis of the function \texttt{h} and thus the performance of the
second loop with accesses through \texttt{B[h(j)]} is in general
compromised. In our experience, there are many applications,
especially irregular applications such as simulations on unstructured
meshes~\cite{FLAG, PENNANT}, where it is extremely useful to have
multiple, aliased views of program data; and this need is becoming
increasingly urgent with the recent trend towards multi-physics
applications in which different access patterns are employed at
different points in the program execution.

Thus it is extremely useful to be able to leverage explicit
programming model support for multiple, potentially aliased partitions
of a given collection of data. Compiler transformations targeting
these models completely avoid analysis of individual data accesses,
and thus achieve improved reliability over traditional techniques as
changes to the indexing function (as with \texttt{h} in the example)
do not impact the compiler's ability to perform analysis and
optimization.
}
