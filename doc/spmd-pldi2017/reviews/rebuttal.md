# Overview

We thank the reviewers for their detailed and insightful feedback.

The technique in this paper, control replication, enables a class of
programs with sequential semantics to scale efficiently to large
distributed-memory machines. *Replication* refers to the method by
which this transformation is achieved: the sequential control flow of
the original program is replicated (slices of the program are executed
concurrently) on each shard of the distributed program. Although prior
approaches have explored this class of technique, there has always
been a fundamental limiter preventing those approaches from achieving
either sufficient generality or sufficient scalability to be
useful. In particular, when a set of loop nests is sliced to be
executed on distributed memory, the memory accesses in those loops
must be analyzed so that the data dependencies (RAW, WAW, etc.) can be
properly observed. For the class of affine programs, this can be
solved via the polyhedral method. However, in the presence of any form
of input-dependent or dynamic aliasing between elements or any
complicated indexing expressions, this problem is very difficult to
solve in a static analysis for a general purpose programming
language. Attempts at solving this problem by recording dynamic traces
of memory accesses, such as in the inspector/executor (I/E) method,
are able to generalize to more dynamic applications but pay a
substantial penalty in the cost of analysis time and memory. In a
follow-up to their original I/E paper, Ravishankar et al. state: "In the
worst case, such traces exhaust the available memory and make it
impossible to run the application." (Ravishankar PPoPP'15) As such,
there is no general and effective solution to this problem in the
state of the art.

Our core contribution is a practical solution to this problem for a
useful class of sequential programs with scalability and performance
comparable to handwritten, manually optimized, explicitly parallel
and distributed codes.

Our technique leverages features of the Regent programming language,
particularly data partitioning. This is an essential, not incidental,
aspect of our approach. If you consider similar approaches (such as
Kwon PPoPP'12 for OpenMP and Ravishankar SC'12 and PPoPP'15 for I/E), the
critical limiting factor preventing efficient distributed execution is
the lack of precise information about data partitioning. If the
analysis to determine data partitioning is imprecise, as in Kwon
PPoPP'12, then some data must be replicated, leading to a loss in
efficiency in both the memory and communication required. (Similarly,
a pure static analysis is unlikely to be sufficiently precise for the
class of programs of interest.) On the other hand, if a precise
dynamic analysis is used, as in I/E, the time and memory requirements
of the analysis itself can be prohibitive. Only a carefully designed
hybrid static/dynamic analysis can achieve the required precision
without prohibitive analysis costs. A contribution of our paper is the
demonstration that the data partitioning features of Regent are
sufficient to enable such a hybrid analysis.

Two features of the Regent programming language are critical
here. First, support for data partitioning allows the
language to directly capture information about disjointness. Even
partial disjointness information is sufficient to allow a static
analysis to guide the application of dynamic analysis, so as to limit
such dynamic analysis to cases which have a potential for
aliasing. Second, because the elements of regions are intended to be
used together, the region annotations in the language enable an
in-memory representation of sets of region elements which is much more
compact than previous approaches while maintaining full precision. As
a result of these factors, the memory requirements of the hybrid
analysis for control replication are substantially reduced, permitting
scaling to large numbers of nodes.

While it is out of scope for this paper to consider the applicability
of control replication to other programming models and/or languages,
we do believe control replication to be broadly applicable. The
features that would need to be added are the data partitioning
operators described above. In fact, a goal of our work is to
motivate the adoption of such features in a broader context.

We wish to thank the reviewers again for their feedback. We will make
all requested changes to the text.

# Responses to Reviewer Questions

In the interest of responding to reviewers' concerns, we address any
remaining questions below.

## Reviewer A

  * The disjoint/aliased tags in Regent are currently trusted at
    compile time, though the compiler can insert a dynamic check to
    verify these. However, there is work elsewhere in the community on
    a sublanguage of restricted partitioning operations which can
    enable a more precise static analysis of partitions (Treichler
    OOPSLA'16).

  * Our technique is independent of any optimizations applied to the
    leaf tasks (kernels) in the computation. Regent does
    support an auto-vectorization pass similar in spirit to the
    auto-vectorizers in C/C++ compilers. Vectorization has a
    substantial impact on the performance of the Circuit and Stencil
    applications but is either challenging and/or does not have a
    substantial impact on Pennant and MiniAero. For Stencil, we used
    kernels defined in C++ to ensure that the implementations were as
    similar as possible.

  * Please note that in all cases the entries labeled "Regent (with
    CR)" and "Regent (w/o CR)" in the experiments refer to exactly the same source code
    running with control replication enabled or disabled
    (respectively).

  * In other cases (the various MPI codes, and the manual SPMD-style
    Regent implementation), we used the best available
    implementations of the various codes. At any rate, our goal is not
    to compare the single-node performance (as this is largely
    orthogonal to scalability) but to consider how the different
    approaches scale to large node counts. In our experiments, Regent
    with control replication achieved scalability comparable to the
    best alternative implementations.

## Reviewer B

  * Control replication as presented in this paper operates at the
    level of task calls and is thus independent of optimizations
    performed within individual tasks. Regent tasks may include
    multiple *variants* for various kinds of processors (CPU, GPU,
    etc.). Variants may be either handwritten or, in some cases,
    automatically generated by Regent. At runtime, Regent programs
    execute on the Legion runtime. Various features of the runtime
    (the ability to map tasks to different processors, regions to
    different memories and different formats) can have a significant
    impact on single-node performance, but are largely orthogonal to
    scalability. For this paper we focus our evaluations on the
    latter.

  * The OmpSs programming model provides tasks (functions with
    arguments) and privileges (in, out, and inout). Arguments to tasks
    can either be scalars or array sections; array sections must
    completely overlap, i.e. partial overlaps between array sections
    are not allowed. This is effectively equivalent to Regent's data
    partitioning model if you allow only a single disjoint partition
    for each region and exclude aliased, multiple and nested
    partitions. Control replication could certainly be applied within
    this restricted domain, though many of the benefits of the
    approach would not be fully realized.

  * The Regent versions of three of the four applications are largely
    unchanged from the codes evaluated in the original Regent paper,
    which includes a lines of code analysis (Slaughter SC'15). The
    Regent codes were generally 25-50% shorter by lines of code than
    the respective references. The original C++ reference codes vary
    from about 2000 to 4000 lines. The main changes in the versions of
    those codes evaluated in this paper were to parallelize the
    initialization phase of the applications, which has no impact on
    steady state performance but (if not done properly) inflates total
    running time at very large node counts.

  * All applications except Circuit were validated for correctness
    with the provided test suites from the original
    implementations. (The original Circuit application did not include
    a validation test suite. We manually verified the correctness of
    the Circuit implementation.)

  * Galois discovers parallelism at runtime via speculative execution,
    and relies heavily on shared memory. When a task begins to
    execute, it is impossible in general to know the set of elements
    that will be accessed by the task. Regent requires data
    partitioning be performed explicitly, though the sets of elements
    assigned to subregions may be chosen dynamically, and thus the set
    of elements to be accessed by a task are known at the latest at
    the point where the tasks is called. An advantage of Regent's
    approach is that the system can schedule data movement in bulk for
    a task. A disadvantage of Regent is that for inherently dynamic
    applications, partitions may need to be recomputed frequently,
    which may hurt performance. However, such applications are
    challenging to execute on distributed memory regardless of the
    programming model.

  * Regarding Hierarchical Overlapped Tiling: Such an optimization
    could certainly be performed prior to applying control replication
    to a program. One question is how the redundant computation would
    be communicated to subsequent passes. As an initial observation,
    please note that redundant computations by nature do not follow
    sequential semantics (i.e. they involve concurrent writes to
    aliased memory locations, though the values being written are the
    same). It is possible to describe such computations in Regent with
    *relaxed coherence modes*, which permit a relaxation of the
    sequential semantics for a given region argument (Treichler
    OOPSLA'13). Thus one possible approach would be to extend control
    replication with support for coherence modes to allow such
    applications to be written directly (or by a previous optimization
    pass in the compiler). In this paper we consider only input
    programs with sequential semantics and thus exclude any
    consideration of relaxed coherence modes.

  * Regarding the semantics of data in RDIR: Conceptually, RDIR can be
    seen as a conservative static approximation of the dynamic
    dataflow graph Legion computes at runtime. Legion is responsible
    for the merging of region state (where gather nodes appear in
    RDIR). To do this, Legion maintains a version number for each
    instance of a region along with other metadata.

  * With respect to the generation of the RDIR graph: The order in
    which statements are initially translated into a set of nodes does
    not matter. The subsequent insertion of gather/scatter nodes must
    follow program order to preserve the original data dependencies.

  * There are a number of options available to reduce the overhead of
    Legion runtime analysis. The analysis can be run inline in the
    control task, avoiding the need to dedicate a core to the runtime;
    however, this is usually not a win in practice. Alternatively, the
    analysis can be allowed to run not on a dedicated core but instead
    on a hyperthread associated with an application core; again, this
    might or might not be an advantage depending on the application
    characteristics. Fundamentally, Legion is a runtime that performs
    dynamic program analysis, and that analysis must be performed
    somewhere, although there is some flexibility in where and when it
    is run.

## Reviewer C

  * We thank the reviewer for the detailed comments and will apply all
    suggested corrections.

  * Regarding Figure 3: The original code on which this is based uses
    a dynamic time stepping loop (i.e. `t += dt` where `dt` changes on
    each loop iteration). We will update the example to make it
    internally consistent.

  * Compound privileges such as `reads reduces+` are allowed in Regent
    but are implicitly upgraded to `reads writes` (as the `reads`
    permission allows the result of the reduction to be
    observed). Similarly it is a notational convenience to write $RW$
    as a privilege that subsumes all other compound privileges.

  * The rule regarding the ordering of writes and reductions is
    similar to the following code snippet:

        int A[4];
        for (int i = 0; i < 4; i++) A[i] = 0;
        for (int i = 0; i < 4; i++) A[i] += 10;

    It would be nonsensical to rearrange the second and third lines in
    this snippet because then the reductions would apply to an
    uninitialized state (and subsequently be erased). Similarly, in
    RDIR, a write provides the initial state of a data node, and the
    reductions modify that state before it is read.

## Reviewer D

  * As noted above, previous approaches either substantially limit the
    space of inputs (e.g. to affine loops) or rely on dynamic analysis
    to determine access patterns. The latter approach incurs
    substantial time and space costs, and in previous work simply has
    not been demonstrated to scale past 64 nodes. Control replication
    is the first approach for this class of programs which scales to
    512 nodes.

  * Our technique does leverage features of the Regent programming
    language. This is a key aspect of our contribution: the additional
    language constructs enable a superior hybrid static/dynamic
    analysis making a practical solution possible. While the
    application of these features to other languages is out of scope
    for this paper, we do believe that these techniques can be adopted
    in other systems and languages.

  * Although we place restrictions on the programming model, please
    note that (a) partitions themselves can contain arbitrary subsets
    of elements, and (b) the bodies of tasks can contain arbitrary
    code. As such, the class of programs covered by this approach is
    in some ways much broader than the class of programs handled by
    e.g. the inspector/executor method (Ravishankar SC'12, PPoPP'15).

  * Also, please note that the work by Kwon et al. (PPoPP'12) was on
    compiling OpenMP programs to MPI. The codes marked "MPI+OpenMP" in
    the experiments were instead handwritten in that programming
    model.

  * The dynamic analysis component of our technique is lifted as early
    in the program as possible, but no earlier. If a program is truly
    dynamic, i.e. the partitions are computed on each loop iteration,
    the dynamic analysis will performed there and control replication
    applied to the interior of the loop. Of course performance might
    suffer in such a case. However, in scientific computing it is
    common to see problems where the mesh topology is updated every
    $N$ timesteps for some value of $N>1$. In such cases control
    replication would likely be profitable.

## Reviewer E

  * We thank the reviewer for the detailed comments. We will apply the
    suggestions to smooth out the exposition of the paper.

  * Partitions in Regent can name any arbitrary subsets of the
    elements of a region. This exposed in the language via a
    *coloring* object, which is a map from colors (small integers) to
    sets of elements. In Regent these are computed by the user in any
    way they see fit and then passed to the `partition` operator to
    create a partition. In the code examples, these colorings have
    been elided, as their construction is quite verbose and does not
    convey any particular insight. However, we agree that current
    presentation is also confusing and we will work to clarify this
    point.

  * Conditionals can be decomposed into loops (i.e. a statement `if c`
    can be represented as `for i = 0, c` where `c` is 0 or 1, and an
    `else` statement can be decomposed into the negation of the
    conditional). This is not how actual programs are written but is
    sufficient for a core language.

  * Accesses to different subregions become accesses to different
    partitions at the loop level as per the description in the last
    paragraphs of Sections 3.1 and 3.3. The remainder of the
    transformation reasons at the level of these partitions.

  * In Regent without control replication, the main control task
    exposes what is conceptually a stream of tasks to the underlying
    runtime, Legion. The runtime computes dependencies between tasks
    to form a dependence graph. Thus there are no global barriers and
    no join points in the execution of a program without control
    replication. Fork-join parallelism is a conceptual and pedagogical
    shortcut, but does not reflect how the programs actually
    execute. Thus, even without control replication, the dependencies
    and data movement in the program are precise. The problem in this
    approach is the cost of analysis, and the cost of launching tasks,
    which both occur on a single node and thus cost $O(N)$ in the
    number of nodes. Control replication replicates the control task,
    and thus the analysis. Because each node is now responsible only
    for tasks being executed on that node, the analysis is reduced to
    $O(1)$ on each node and the system is able to scale out to a large
    number of nodes. We will make this clear in the text.

  * In all cases, the "Regent (with CR)" and "Regent (w/o CR)" lines
    in the experiments refer to exactly the same source code, with the
    only difference being the presence or absence of the control
    replication optimization. Section 7.2 refers to a difference in
    the approach taken in the manually hand-optimized SPMD-style
    Regent code. In general, we used the best available
    implementations to compare against in our experiments.

  * The goal of the experiments is to demonstrate scalability, not
    single-node performance. The single-node performance results are
    not new anyway, but the scalability results are novel. A flat line
    in a weak scaling chart demonstrates that the runtime analysis and
    any communication costs are indeed $O(1)$ with respect to the number
    of nodes.

  * Single-node performance is mostly orthogonal to
    scalability. (Mostly, because if single-node performance is very
    poor, then scaling is trivial. However in all our experiments the
    single-node performance was comparable to the reference
    versions.) In a weak scaling experiment, work per node stays
    constant, so there is no interaction with scaling unless there is
    some non-$O(1)$ communication or analysis cost.

  * We will address the related work.
