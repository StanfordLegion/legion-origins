===========================================================================
                           PLDI '17 Review #295E
---------------------------------------------------------------------------
Paper #295: Control Replication: Compiling Implicit Parallelism to
            Efficient SPMD with Logical Regions
---------------------------------------------------------------------------

                 Reviewer expertise: Y. Knowledgeable
                      Overall merit: C. Weak paper, though I will not fight
                                        strongly against it

                           ===== Strengths =====

Converting fork-join programs with a single initial/final thread of
control into SPMD programs is an elegant way to make programming easier.

                          ===== Weaknesses =====

The paper has an unneveness of detail where it explains the blow-by-blow of
examples (at length), but doesn't clearly present the algorithmic insights that
enable control replication.  It's not clear why control replication is
difficult.  The evaluation looks at scaling but not at other aspects, and it
includes one methodological flaw.

                      ===== Comments to authors =====

This paper seems quite promising, but reading it in its current form has its
frustrations.  First, at the very outset, we are given an abstract example
(doubly nested loop, F,G), and shown a loop fusion and interchange.  But it's
hard to build much intuition from this without knowing what kinds of effects F
and G may perform.  The refresher on SPMD is nice, but there is no list of
contribution bullets.

Indeed, the paper dives right into a motivating example, which begins a style of
exposition common throughout the paper: extremely detailed blow-by-blow
descriptions of what is happening to which nodes and which variables at which
lines and which edges.  This grows tedious and the prose needs to pause to
deliver the key insights in a digestible form.

The motivating example of section 2 (and much of the paper), suffers from being
more complicated than the example of section 1, but still quite abstract.  What
made private_vs_ghost have exactly two subregions?  Something inside the elided
arguments of "partition" on line 2 of figure 5?  Many bits of this code are
redacted, and types are simplified to the point of "int" or "region" (region of
what?).  It would be great to have an example that's concrete and simple at the
same time.

Likewise, the grammar of figure 4 is abstract, in fact, it's been abstracted too
much.  What does the third argument of "partition(_,_,_)" do?  It's hard to tell
because it was redacted from Figure 3 and called only "expr" in Figure 4.
Furthermore, Core Regent is so simplified that it doesn't include conditionals.

Explaining what happens to the branches of conditionals, however, is important.
The end of section 3.2 states: "expressions that access subregions of partitions
result in the corresponding subregion nodes, tagged with the index expression
used."  So what happens when two branches of a conditional access different
subregions?  We were told that symbolic indices only include constants and
variables.

Section 4 and 5 could benefit from formalization that would explain the
transformations in a much more precise fashion.  One assumption that should be
stated up front is that the scalar language is restricted to be deterministic.
This way the state of the replicas do not diverge.  That's a substantial
restriction, as it prevents RNG, prevents FFI, requires data-race freedom, etc.

Through section 4 and 5 there is some fishy ordering where a topic will begin to
be rolled out but then the exposition stops and forward references an
explanation (e.g. to sec 5.3).  Section 5.2 describes "two global barriers", but
in the very next subsection these are explained to be, actually, point-to-point
synchronization.  Why the bait and switch?  The first explanation is not easier
to understand than the second.

In section 7.1 it would be good to have a reminder of what exactly Regent
without CR is doing, e.g., how do *all* parallel loop join points require a
global barrier, or does it handle nested parallelism more efficiently than that?
Likewise how does the non-CR version distribute load?  The explanation at the
end of 7.1 points to dynamic analysis as the reason for non-CR's poor scaling.
But does the CR version perform dynamic analysis too?  What makes the latter so
much cheaper?

Ultimately, the primary benefit is actually the increase in ease of programming
of Regent (with CR) versus Regent with manual SPMD.  To that end it would be
good to quantify at least the lines of code of these different variants, and to
compare consistently against "Regent (manual SPMD)".

The biggest objection to the current evaluation section is at the end of 7.2.
Here it turns out that figure 11 is actually an apples-to-oranges comparison,
because the with CR version was manually optimized in a way that "w/o CR" was
not.  Finally, the speedups over competitors appear to be previously published,
which makes this seem like a comparatively thin new contribution in this paper.

In related work, I expected an extensive discussion of how this relates to work
on the polyhedral model targeting distributed memories.

