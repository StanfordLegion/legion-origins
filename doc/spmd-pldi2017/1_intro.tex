\section{Introduction}
\label{sec:intro}

Consider the following code.

\begin{lstlisting}[language=Simple,basicstyle=\small,belowskip=5pt]
for t = 0, T do
  for i = 0, N by K do
    F(A[i..i+K])
  end
  for j = 0, N by K do
    G(B[j..j+K])
  end
end
\end{lstlisting}

% Same thing in a figure.
%% \input{f0_code_intro}

\input{f1_tasks}

This program has desirable properties: it is sequential and thus easy
to read, understand, debug and maintain.  It is also easily
parallelized.  Assuming there are no loop carried dependencies, the
iterations of each of the two inner loops can be executed in parallel
on multiple processors in a straightforward fork-join style.  As
illustrated in Figure~\ref{fig:tasks1a}, a main thread launches a
number of worker threads for the first loop, each of which executes
one or more loop iterations. There is a synchronization point at the
end of the loop where control returns to the main thread; the second
loop is executed similarly.  With considerable variation, this
\emph{global view} style is the basis of many parallel programming
models.  Classic parallelizing compilers~\cite{Polaris95, SUIF96,
  PIPS91}, HPF~\cite{HPF96, HPF07} and the data parallel subset of
Chapel~\cite{Chapel07} are canonical examples; more recent examples
include Spark~\cite{Spark10} and TensorFlow~\cite{TensorFlow15} and
task-based models such as Sequoia~\cite{SequoiaLanguage06,
  SequoiaCompiler07} and Legion \cite{Legion12}.

There are, however, two problems.  First, there is a built-in
scalability issue. There is a sequential component, namely the time
spent in the main thread to create and later destroy the parallel work
at the beginning and end of the parallel loops.  This overhead is
proportional to the number of parallel \emph{tasks} (or units of work)
to be executed, divided by the time per task.  As the number of tasks
\texttt{N/K} grows and the amount of work \texttt{K} for each task
stays either constant or decreases, the overhead comes to dominate
useful work at some problem size.  A common approach to address this
issue is to rearrange the code to increase the running time of tasks
as follows:

\begin{lstlisting}[language=Simple,basicstyle=\small,belowskip=5pt]
for i = 0, N by K do
  for t = 0, T do
    F(A[i..i+K])
    G(B[i..i+K])
  end
end
\end{lstlisting}

In the modified program, iterations of the new outer loop can be
executed in parallel.  Figure~\ref{fig:tasks1b} illustrates the
execution of this code.  There is still overhead in launching
\texttt{N/K} tasks from the main thread, but the launches are done
once at the beginning and the parallel tasks run for the duration, so
the launch overhead is in fact the minimum possible and asymptotically
better than the overhead of the first program (${\cal O}({\tt N/K})$
instead of ${\cal O}({\tt TN/K})$).  Primarily because of its
scalability on large supercomputers, this programming style is the
basis of SPMD programming models such as MPI~\cite{MPI},
UPC~\cite{UPCSTANDARD} and Titanium~\cite{TITANIUMSTANDARD} and also
forms a useful subset of more expressive programming systems such as
Chapel, X10 \cite{X1005} and Legion.

The second problem is well-known: aliasing.  Returning to the first
example, even if the two loops are data parallel, and so have no
dependencies that prevent straightforward parallelization of the
individual loops, what if \texttt{A} and \texttt{B} are aliased, i.e.,
they share at least some array elements in common?  The sequential
semantics is clear.  The parallel interpretation in
Figure~\ref{fig:tasks1a} is also fine, because of the synchronization
point enforced by the main thread between the two loops.  But if
\texttt{A} and \texttt{B} can overlap, then the SPMD version requires
inter-task synchronization (e.g., a global barrier between lines 3 and
4 in the modified code) to enforce that iterations of the second loop
that refer to data touched in the first loop execute in the required
order.  On a distributed memory machine, data movement may also be
necessary to ensure that when tasks are executing calls to
\texttt{G()} that any updates performed by calls to \texttt{F()} are
visible.

This need for synchronization and data movement (illustrated in
Figure~\ref{fig:tasks1b}) is the primary drawback of writing SPMD
programs: this is explicit parallel programming, with all of its
well-known pitfalls for program complexity, debugging and maintenance.
Different programming models find various ways of helping the
programmer cope.  A common approach in SPMD programming systems is to
allow the programmer to specify a \emph{partition} of the data across
the machine.  Critically, this partitioning information gives the
compiler or runtime system coarse-grain, high-level knowledge, and
specifically above the level of trying to reason about individual
array or pointer dereferences, of how the program uses the data.
Static or dynamic analysis (or both) that leverages the partition
information is then used to simplify programming, improve performance,
or both, as in HPF, UPC, Titanium, X10 and Chapel.  In our example,
this approach helps with the compilation and execution of one of the two
views of the shared data, say the first loop with accesses through
\texttt{A}, but because there is only one partition there is no way to
describe aliasing to the programming system, and the performance of the second
loop with accesses through \texttt{B} is in general compromised.  In
our experience, there are many applications, especially irregular
applications such as simulations on unstructured meshes~\cite{FLAG,
  PENNANT}, where it is extremely useful to have multiple, aliased
views of program data and to be able to write programs in the
\emph{implicitly parallel} style of our first example.  We would then
like to automatically transform these programs into the explicitly
parallel SPMD style of our second example for scalability and
performance.

This paper presents \emph{control replication}, a technique that
transforms an implicitly parallel, fork-join style program into
efficient SPMD code. The goal of control replication is to generate a
set of \emph{shards}, or long-running tasks, from the control flow of
the original program, to amortize overhead and enable efficient
execution on large numbers of processors. In our experiments, code
optimized by control replication achieves up to 96\% parallel
efficiency on 512 nodes. We describe
our work using and implement our approach in Regent~\cite{Regent15}, a
high-level programming language built on the Legion runtime.  Key to
the design of control replication and our experimental results is
leveraging static and dynamic information from the Regent data model,
which allows multiple, possibly aliased partitions of data.
