\section{Evaluation}
\label{sec:evaluation}

We evaluate scalability and parallel efficiency of control replication
in the context of Regent with three unstructured proxy applications
and one structured benchmark: a circuit simulation on a sparse
unstructured graph; MiniAero, an explicit solver of the compressible
Navier-Stokes equations on a 3D unstructured mesh; PENNANT, a
Lagrangian hydrodynamics simulation on a 2D unstructured mesh; and a stencil
benchmark on a regular grid.

For each application, we report weak scaling performance on up to 512
nodes of the Piz Daint supercomputer~\cite{PizDaint}, a Cray XC30
system. Each node has an Intel Xeon E5-2670 CPU (8 physical cores) and
32 GB of memory. All applications were compiled with GCC
4.8.2, and Regent used LLVM 3.5.2 for code generation.

Finally, we report the running times of the dynamic region
intersections for each of the applications at 64 and 512 nodes.

\subsection{Circuit}
\label{subsec:circuit}

% Problem size: 15k circuit nodes/node, 60k wires/node

% Circuit efficiency at 256 nodes:
%   13.088623500000002 / 13.1580345 = 0.9947248200329618
% And at 512 nodes:
%   13.088623500000002 / 13.679076909090911 = 0.9568352884470953

We developed a sparse circuit simulation based on \cite{Legion12} to
measure weak scaling performance on unstructured graphs. The
implicitly parallel version from \cite{Legion12} was already shown to
be substantially communication bound at 32 nodes and would not have
scaled to significantly more nodes, regardless of the implementation
technique. The input for this problem was a randomly generated sparse
graph with 60k edges and 15k vertices per compute node; the application
was otherwise identical to the original.

Figure~\ref{fig:circuit} shows weak scaling performance for the
simulation up to 512 nodes. (In the legend control replication is
abbreviated as CR.) Regent without control replication matches
this performance up to 16 nodes; however, after this point the
overhead of the dynamic analysis in the Legion runtime begins to
exceed the running time of the application, causing efficiency to drop
rapidly.

\input{e1_circuit}

\subsection{MiniAero}
\label{subsec:miniaero}

% Problem size: 256k cells/node (256 x 256 x 4)

% MiniAero efficiency at 512 nodes:
%   9.6980843 / 10.439066100000002 = 0.9290183822094965

MiniAero is a 3D unstructured mesh proxy application from the Mantevo suite~\cite{Mantevo} developed at Sandia National
Laboratories, implementing an explicit solver for the compressible
Navier-Stokes equations. The mini-app is written in a hybrid style,
using MPI for inter-node communication and Kokkos for intra-node
parallelism. (Kokkos is a portability layer for C++ that compiles
down to OpenMP (on CPUs), also developed at Sandia~\cite{Kokkos}.)

In this experiment we compare two distinct Regent programs. One
version uses Regent's implicitly parallel style (which we test with
and without control replication). We also evaluate a second version
that directly uses Regent's support for hand-written SPMD-style programming.

Figure~\ref{fig:miniaero} shows weak scaling absolute performance for
the various implementations of MiniAero on a problem size of 256k
cells per node. Control replication achieves 93\% parallel efficiency
at 512 nodes; as before, Regent without control replication struggles
to scale beyond a modest number of nodes. The manual SPMD version of
the Regent code also scales, though with worse overall
performance. This was due in part to an early design decision to split
ghost regions in the application one per neighbor per submesh instead
of just one per submesh. The automated nature of control replication
allowed us to iterate quickly on the implicit parallel version and
reverse this decision to achieve better final performance with less
overall development effort.

As demonstrated in \cite{Regent15}, Regent-based codes out-perform the
MPI+Kokkos implementation of MiniAero on a single node, mostly by
leveraging the improved hybrid data layout features of
Legion~\cite{LegionFields14}.

\input{e2_miniaero}

\subsection{PENNANT}
\label{subsec:pennant}

% Problem size: 1.8M zones/node (320 x 5760)

% PENNANT efficiency at 128 nodes:
%   6.7707223999999995 / 7.1951036 = 0.9410180556677459
% And at 512 nodes:
%   6.7707223999999995 / 8.854033833333334 = 0.7647048257834568

PENNANT is a 2D unstructured mesh proxy application from Los Alamos
National Laboratory simulating Lagrangian hydrodynamics~\cite{PENNANT},
implemented in MPI+OpenMP. The application represents a subset of the
functionality that exists in FLAG, a larger production code used at
the lab~\cite{FLAG}. (Details on the computation performed by PENNANT
and the problem decomposition used are discussed in
Section~\ref{sec:example}.)

The original PENNANT source code applies a cache blocking optimization
which substantially improves the computational intensity of the
overall application. This optimization impacts even the data structure
layouts, as the (otherwise unordered) mesh elements are grouped into
chunks to be processed together. In spite of this, control replication
applied seamlessly to the code, as the details of the optimization are
limited to the structure of the region tree (which subsumes the chunk
structure of the original code) and the bodies of tasks (whose details
are accurately summarized by the privileges declared in the task
declaration).

Figure~\ref{fig:pennant} shows weak scaling performance for PENNANT on
up to 512 nodes, using a problem size of 1.8M zones per node. Control
replication maintains 94\% parallel efficiency up to 128 nodes; after
this efficiency begins to drop as the Legion runtime's implementation
of dynamic collectives (used in control replication to implement
scalar reductions) begin to take more time in comparison to the MPI
collective implementation. Without control replication, the overhead
of the implicitly parallel version very quickly exceeds the short
running time of the individual PENNANT tasks.

%% FIXME: add: by 16 nodes the parallel efficiency has droped to YY%

As noted in \cite{Regent15}, Regent's single-node performance on
PENNANT matches that of MPI+OpenMP when applying a similar number of
cores to application-level tasks. However, the Legion runtime requires
one core to perform runtime analysis on tasks, and this prevents
Legion-based applications from fully utilizing the machine for
compute-bound applications (as PENNANT is after the cache blocking
optimization described above). In these experiments, Regent runs with
7 application cores per node, with 1 core dedicated to the Legion
runtime; MPI+OpenMP achieves its best performance when using 16
hyperthreads on all 8 physical cores.

\input{e3_pennant}

\subsection{Stencil}
\label{subsec:stencil}

% Stencil efficiency at 512 nodes:
%   40.6527577 / 42.21325829999999 = 0.9630329270270996

% Characteristics:
%   20k * 40k cells per node
%   50 iterations
%   8 MPI ranks per node
%   8 application cores per node for Regent

Stencil is a 2D structured benchmark from the Parallel Research
Kernels~\cite{PRK14, PRKRuntimes16}. The code performs a stencil of
configurable shape and radius over a regular grid. Our experiments
used a radius-2 star-shaped stencil on a grid of double-precision
floating point values with $\mathrm{20k} \times \mathrm{40k}$ grid
points per node. (This size was chosen to be square at odd powers of 2
because the original MPI code requires a square input.)

As with the previous experiments, all compiler analysis was performed
at the task and region level. Control replication was able to optimize
code containing affine access patterns, without requiring any specific
support for affine reasoning in the compiler.

Figure~\ref{fig:stencil} shows weak scaling performance for Stencil up
to 512 nodes. Control replication achieved 96\% parallel efficiency at
512 nodes, whereas Regent without control replication rapidly drops in
efficiency.

\input{e4_stencil}

\subsection{Dynamic Intersections}
\label{subsec:intersection-times}

Dynamic region intersections are computed at application startup
(after initialization) to identify the communication patterns and
precise data movement required for an application
run. Table~\ref{tab:intersections} reports the running times of the
intersection operations measured during the above experiments while
running on 64 and 512 nodes. Shallow intersections are performed on a
single node to determine the approximate communication pattern (but
not the precise sets of elements that require communication); these
required at most 254 ms at 512 nodes (31 ms at 64 nodes). Complete
intersections are then performed in parallel on each node to determine
the precise sets of elements that must be communicated with other
nodes; these took at most 21 ms.  (Running times for complete
intersections were nearly constant across node counts.) Both times are
much less than the typical running times of the applications
themselves, which are often minutes to hours.

\begin{table}
\centering
\begin{tabular}{r c | c c}
Application & Nodes & Shallow (ms) & Complete (ms) \\
\hline
\multirow{2}{*}{Circuit} & 64 & 5.7 & 0.4 \\
& 512 & 84 & 0.4 \\
\hline
\multirow{2}{*}{MiniAero} & 64 & 31 & 20  \\
& 512 & 254 & 21 \\
\hline
\multirow{2}{*}{PENNANT} & 64 & 5.7 & 0.2 \\
& 512 & 78 & 0.2 \\
\hline
\multirow{2}{*}{Stencil} & 64 & 4.7 & 0.2 \\
& 512 & 105 & 0.1
\end{tabular}
\caption{Running times for region intersections on each application at 64 and 512 nodes.}
\label{tab:intersections}
\end{table}

% At 512 nodes:
%%   * circuit
%%     list_cross_product: 84 ms
%%     list_cross_product_complete: 0.4 ms
%%   * miniaero
%%     list_cross_product: 254 ms
%%     list_cross_product_complete: 21 ms
%%   * pennant
%%     list_cross_product: 78 ms
%%     list_cross_product_complete: 0.2 ms
%%   * stencil
%%     list_cross_product: 105 ms
%%     list_cross_product_complete: 0.13 ms

% At 64 nodes:
%%   * circuit
%%     list_cross_product: 5.7  ms
%%     list_cross_product_complete: 0.4 ms
%%   * miniaero
%%     list_cross_product: 31 ms
%%     list_cross_product_complete: 20 ms
%%   * pennant
%%     list_cross_product: 5.7 ms
%%     list_cross_product_complete: 0.2 ms
%%   * stencil
%%     list_cross_product: 4.7 ms
%%     list_cross_product_complete: 0.15 ms


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "spmd"
%%% End:
