\section{Program Transformation}
\label{sec:replication}

The program can now be transformed to produce \emph{shards}:
long-running tasks with explicit communication and synchronization to
be executed in SPMD fashion. The transformation
replaces the explicit gather and scatter nodes in the RDIR
graph with \emph{copy} nodes for point-to-point communication
(along with the appropriate
synchronization). This is sufficient to satisfy the implicitly
parallel semantics of the program. However, to construct an
efficient SPMD program, care must be taken to ensure that the copies
are not only sufficient, but precise. In particular, it is not
desirable to collect the results of a gather of distributed subregions
all on a single machine node,
as this creates both a sequential bottleneck and limits the problem
sizes that can effectively run.

Instead, we perform a dynamic analysis, comparing regions for overlap
at runtime. This has two advantages: first, the program is able to
(dynamically) discover a precise communication graph for the
application, so that nodes are only required to communicate with other
nodes that hold relevant pieces of data. And second, this dynamic
analysis identifies precise overlaps for regions, minimizing the data
that must actually be copied between those nodes.

Figure~\ref{fig:rdir-replicated} shows the results of the control
replication on the outer loop of the application. This graph now
represents the code that will be executed by each shard in the SPMD
execution of the program. Notably, the control flow in the program has
been largely kept intact, although the inner loops have been replaced
by their respective task calls. As a further optimization (not shown),
the compiler replaces each individual task called with a set of tasks,
so that a shard may be responsible for execution of tasks on multiple
processors (e.g. to enable use of a single shard per node).

%% FIXME: How is the assignment from iterations to shards decided?
%% Really, this is a mapping decision. But describing the mapper here
%% might not be the best idea.

%% Control replication can be applied to any tree where the leaves
%% (innermost loops in the nested control flow) are loops with analyzable
%% bounds, and the loops in question have no loop-carried
%% dependencies. For simplicity of implementation, we also require the
%% bounds of all innermost loops to be identical.

%% Operations outside of innermost loops are permitted to have
%% side-effects, such as incrementing a time-step variable or computing
%% the next value of $dt$, but are not permitted to access regions or
%% partitions. Applications which require, for example, access to mesh
%% parameters in order to compute parameters for the next time step, can
%% do so by reducing values into scalar variables, which will be
%% automatically broadcast to all shards.

\subsection{Insert Copies}
\label{subsec:copies}

Following the conversion to RDIR format, the graph contains gather and
scatter nodes wherever communication is implied in the original
program (as shown in Figure~\ref{fig:rdir-complete}). These nodes must
be replaced with explicit communication and synchronization for SPMD execution.

To generate the necessary communication, the compiler replaces each
gather node with one or more explicit copy nodes, and elides the
scatter node and intermediate data node for the parent region. In
Figure~\ref{fig:rdir-replicated}, for example, the compiler inserts a
single copy node from \texttt{ghost.f} to \texttt{master.f}.

Copies resulting from the application of reductions
require additional care. The partial results from the
reductions must be stored separately to allow them to be
folded into the destination region, even in the event of aliasing. To
accomplish this, the compiler generates a temporary field $f'$ and
initializes the contents of the new field to the identity value (in this case, 0). The
compiler then issues reduction copies to apply the partial results to
any destination regions which require the updates.

Copies are issued between pairs of source and destination regions, but
only the \emph{intersections} of the regions must actually be
copied. The number, size and extent of such intersections are unknown
at compile time; this is an aspect of the analysis that is deferred
until runtime. The compiler is able to ensure, however, that this
analysis is performed during program initialization. The analysis of
region intersections is addressed in
Section~\ref{subsec:intersections}.

\subsection{Insert Synchronization}
\label{subsec:sync}

The copy operations introduced by control replication are always
issued by the shard producing the data. Therefore, on the
producer's side only, copies follow the normal semantics of data
dependencies implied by the RDIR format. Explicit synchronization is
required to ensure that copies synchronize correctly with the
consumer.

Each copy involves two points of synchronization. A copy must not
start until the previous consumer of the data has completed to avoid
overwriting data currently in use. Similarly, a copy must finish before
the consumer of the data can begin. Because each shard is potentially
both a producer and a consumer of data, in
Figure~\ref{fig:rdir-replicated} we use two global barriers to enforce
proper synchronization. When copies have completed, the \texttt{full}
barrier is signaled to indicate data is available for
consumption. Once a shard has consumed its data, it signals the
\texttt{empty} barrier to indicate that it is safe for other shards to
produce more updates.  Note that the copy operations wait for the
\texttt{empty} barrier to trigger before pushing more data to
consumers, and the \texttt{adv\_pos\_full} operation waits on the
\texttt{full} barrier before consuming data produced by other
nodes. The argument to a barrier names which iteration of the loop
to which that instance of the barrier refers; the advance operation increments
the argument value, naming the barrier to be used in the next
iteration of the loop. The compiler inserts code to initialize the
barriers on entry to the loop, shown as read dependencies for the loop
on the left-hand side of Figure~\ref{fig:rdir-replicated}.

\subsection{Dynamic Analysis of Intersections}
\label{subsec:intersections}

The precise intersections of copied regions cannot be known until
runtime. Control replication resolves this issue by delaying this
analysis until runtime, when the intersections can be precisely
computed.

While issuing copies, the compiler records pairs of source and
destination partitions that require copies. For example, \texttt{ghost}
and \texttt{master} is such a pair in
Figure~\ref{fig:rdir-replicated}. The data that must be copied from
\texttt{ghost} to \texttt{master} corresponds to the set of intersections
between all pairs of subregions of the two partitions.

To avoid comparing all pairs of subregions in the computation of intersections,
we apply a two-stage approach. First, the main thread (before
launching the SPMD shard tasks) computes a shallow intersection to
determine which pairs of regions overlap (but not the extent of the
overlap). An interval tree acceleration data structure makes this
operation $O(N \operatorname{log} N)$. Second, each shard begins its
execution by computing a complete intersection between these
known-intersecting regions. This operation is $O(M^2)$ where $M$ is on
the order of the number of nodes each node must communicate
with.

As a further optimization, the global barriers described in
Section~\ref{subsec:sync} are replaced by point-to-point
synchronization between a node and only those nodes that it produces
data for and consumes data from. This approach minimizes
synchronization overhead and avoids having all tasks wait at barriers
even when the conditions for some of those tasks to proceed may have
already been satisfied.

%% FIXME: and M is usually much less than N

In practice, at 512 nodes, the impact of intersection computations on total
running time is negligible, especially for long-running
applications. Section~\ref{subsec:intersection-times} reports running times for
the intersection operations of the evaluated applications.

%% \subsection{Duplicate Aliased Partitions}
%% \label{subsec:dup}

%% Regions may be partition multiple ways to describe an arbitrary number
%% of access patterns. However, because these may potentially alias with
%% each other, the compiler must disambiguate them to avoid conflicts at
%% runtime. To accomplish this, the compiler picks at most one disjoint
%% partition of each region---this partition, and only this partition may
%% be used as-is. For all other partitions, the compiler duplicates every
%% subregion of the partition to give a distinct logical name to the data
%% covered by that region.

%% \subsection{Must Epoch Launch}
%% \label{subsec:epoch}

%% The standard semantics for a task launch allow but do not require
%% tasks to run in parallel; however, with a SPMD-style program, a
%% failure to run in parallel would result in deadlock (due to explicit
%% synchronization). Regent provides a \emph{must-parallel epoch}
%% construct which launches a set of tasks which are required to run in
%% parallel for specifically this purpose.

%% The arguments to the tasks within the epoch include any regions to be
%% used directly by the shards themselves, in addition to any regions
%% which overlap regions used directly. Because this results in
%% interference between tasks (because tasks might use the same region
%% with read-write privileges), \emph{simultaneous} coherence is enabled
%% to relax the constraints of execution ordering to permit parallel
%% execution.
