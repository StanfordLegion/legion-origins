#!/bin/bash

echo "X Time Perf" > DATA_BASELINE_7
CNT=1
for i in 1 2 4 8 16 32 64 128 256 512
do
  AVG_TIME=`grep "hydro cycle run" baseline/out_"$i"x7* | awk '{print $5}' | avg.rb`
  PERF=`ruby -e "puts (320*5760*30/$AVG_TIME/1000000.0)"`
  echo "$CNT $AVG_TIME $PERF" >> DATA_BASELINE_7
  CNT=$((CNT + 1))
done

echo "X Time Perf" > DATA_BASELINE
CNT=1
for i in 1 2 4 8 16 32 64 128 256 512
do
  AVG_TIME=`grep "hydro cycle run" baseline/out_"$i"x16* | awk '{print $5}' | avg.rb`
  PERF=`ruby -e "puts (320*5760*30/$AVG_TIME/1000000.0)"`
  echo "$CNT $AVG_TIME $PERF" >> DATA_BASELINE
  CNT=$((CNT + 1))
done

echo "X Time Perf" > DATA_REGENT
CNT=1
for i in 1 2 4 8 16 32 64 128 256 512
do
  AVG_TIME=`../summarize.py regent/out_"$i"x* | grep -v ERROR | awk '{print $4}' | avg.rb`
  PERF=`ruby -e "puts (320*5760*30/$AVG_TIME/1000000.0)"`
  echo "$CNT $AVG_TIME $PERF" >> DATA_REGENT
  CNT=$((CNT + 1))
done

echo "X Time Perf" > DATA_REGENT_NO_SPMD
CNT=1
for i in 1 2 4 8 16 32 64 128 256
do
  AVG_TIME=`../summarize.py regent_no_spmd/out_"$i"x* | grep -v ERROR | awk '{print $4}' | avg.rb`
  PERF=`ruby -e "puts (320*5760*30/$AVG_TIME/1000000.0)"`
  echo "$CNT $AVG_TIME $PERF" >> DATA_REGENT_NO_SPMD
  CNT=$((CNT + 1))
done
