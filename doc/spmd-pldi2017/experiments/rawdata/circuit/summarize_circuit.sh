#!/bin/bash

echo "X Time Perf"
CNT=1
for i in 1 2 4 8 16 32 64 128 256 512
do
  AVG_TIME=`../summarize.py "$1"_"$i"x* | grep -v ERROR | awk '{print $4}' | avg.rb`
  PERF=`ruby -e "puts (6*10000*5/$AVG_TIME/1000.0)"`
  echo "$CNT $AVG_TIME $PERF"
  CNT=$((CNT + 1))
done
