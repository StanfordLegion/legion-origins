\section{Motivating Example}
\label{sec:example}

\input{f3_partitioning}
\input{f2_code_pennant}

As a more realistic motivating example, consider PENNANT, a Lagrangian
hydrodynamics proxy application from Los Alamos National
Laboratory~\cite{PENNANT}. PENNANT's core data structure is a 2D
unstructured mesh with fundamental constituents in 0, 1 and 2
dimensions called \emph{points}, \emph{edges} and \emph{zones}, as
well as \emph{sides} representing the triangular area between an edge
and the center of a zone. The application simulates multiple types of
physics; individual steps in the simulation loop over mesh elements
and read or write fields representing various physical properties of
the mesh. Steps in the simulation may require access to neighboring
elements---for example, to update the center of a zone, the code must
read the positions of the neighboring points. In a parallel
implementation of PENNANT, the read sets of such steps may
\emph{alias} data written by another processor; and in a
distributed-memory environment, the other processor might be on a remote node,
requiring communication. A central concern in the design of an
implementation of PENNANT is the management of the communication
implied by such aliased access patterns.

A straightforward implementation of PENNANT in Regent would exploit
Regent's sequential semantics to maintain the consistency of a single
(conceptually shared) copy of the mesh. A Regent program achieves
parallelism by dividing the computation into \emph{tasks}. In this
case, the program might consist of a task per step of the simulation
per chunk of the mesh. Objects such as elements of the mesh are stored in
containers called \emph{regions}; regions may be \emph{partitioned}
into \emph{subregions} containing arbitrary subsets of the original
elements. Subregions are views or aliases of the parent region. If an
object is contained in multiple overlapping subregions, any update to
that object is visible to any task that follows in program order and
that references the object through any other aliased
subregion. Typically, subregions are chosen to correspond to the read
and write sets of tasks. Figure~\ref{fig:partitioning} shows an
example mesh colored to indicate a potential partitioning of zones,
sides and points into subregions to enable parallel, distributed
execution.

Figure~\ref{fig:code1a} shows an excerpt from
an implementation of PENNANT that follows this strategy. In the code,
forces are accumulated onto points and used to update the velocity and
position of the points themselves. For simplicity, and because only
points are involved in communication, the code only shows data usage
for points, and only for a single \emph{field} \texttt{f} representing
the force exerted on each point.

In Regent, a task may only access data in the task's region
arguments, and then only in the manner prescribed
by the \emph{privileges}---\emph{read}, \emph{write}, and
\emph{reduce} (using an associative and commutative operator)---specified
in the task's declaration. (For regions of user-defined data types, a
privilege may optionally name a specific field within the
elements of the region.) Thus a task's data access patterns are
completely described by the arguments passed to the task and
the declared privileges. This means, for example, that the
unstructured data accesses present in the implementations of the tasks
\texttt{calc\_\allowbreak{}forces} and \texttt{adv\_pos\_full} (not shown) present no
obstacle to analysis and optimization.

The collection of subregions produced via partitioning a region is
called a \emph{partition}. Partitions are first-class values and serve
a variety of purposes. For example,
line 9 of Figure~\ref{fig:code1a} declares a \emph{disjoint} partition
\texttt{private} of points not involved in communication (the
solid-shaded points in Figure~\ref{fig:partitioning}). Since
\texttt{private} is disjoint, tasks with \emph{read-write} privileges
may execute in parallel on the subregions as the data accesses are
guaranteed to not overlap. The code to determine the contents of the
partition itself is omitted, as the disjoint property is sufficient to
enable the required analysis. The \texttt{master} partition (hatched
points in Figure~\ref{fig:partitioning} bottom left) serves a similar
purpose for points involved in communication. Line 11 declares an
\emph{aliased} partition \texttt{ghost} (Figure~\ref{fig:partitioning}
bottom right), meaning that the subregions are not necessarily
disjoint. Because the \texttt{ghost} subregions may share elements,
tasks may only execute in parallel on the subregions if they all request
\emph{read} privilege, or \emph{reduce} with all tasks using the same
reduction operator.

The partition \texttt{private\_vs\_ghost} on line 8 serves a different
purpose. Only points at the boundaries between portions of the mesh
require communication; points in the interiors remain local to the
tasks operating on that portion of the mesh. As an optimization, these
points are partitioned separately from those involved in
communication, resulting in two intermediate subregions, one
consisting of the union of the regions of solid-shaded points from
Figure~\ref{fig:partitioning}, and one consisting of the hatched
points. This reduces communication and is also leveraged by
control replication to reduce the extent of dynamic analysis
required during execution.

Any optimization attempting to transform this program into SPMD style
(resulting in the code in Figure~\ref{fig:code1c}) must respect the
implicit communication between the inner loops from \texttt{master} to
\texttt{ghost}. First, the analysis needs to detect the presence of
aliasing between the subregions of \texttt{master} and \texttt{ghost}---this
is aided substantially by the implementation's knowledge of the tree
structure of regions and subregions. Second,
to determine where explicit communication is required, the compiler
must discover the producer-consumer relationships implied by accesses
to aliased regions. Third and finally, efficient communication
requires knowledge not simply of the presence of aliasing, but of
precisely which points are shared between each pair of \texttt{master}
and \texttt{ghost} subregions. It is not possible to determine the
precise intersections of regions at compile time. Thus, we delay
analysis of region intersections until runtime when they can be
precisely resolved. If the program's region structure is invariant
inside of the main control loop (which is, fortunately, extremely
common in practice), the compiler is able to lift these queries into
the initialization phase of the program and avoid any expensive checks
during the execution of the main loop.

Our implementation of control replication for Regent is composed of
three stages, following the steps outlined above:

\begin{itemize}
\item First, a \emph{region tree analysis} identifies overlapping data
  access patterns in the application (Section~\ref{sec:rtree}).
\item Second, a \emph{region-based dataflow intermediate representation}
  makes explicit all the implied communication in the program
  (Section~\ref{sec:rdir}).
\item Third, the compiler restructures the program into \emph{shards},
  long-running tasks that each do a portion of the work of each of a
  sequence of tasks in the original program. The compiler modifies the
  communication operations in the second step to do direct
  shard-to-shard copies and adds synchronization between tasks
  (Section~\ref{sec:replication}).
\end{itemize}

We present an implementation of control replication for the Regent
language in Section~\ref{sec:implementation}. We evaluate this
implementation in Section~\ref{sec:evaluation} using four codes: a
circuit simulation on an unstructured graph, an explicit solver for
the compressible Navier-Stokes equations on a 3D unstructured mesh,
PENNANT (described above), and a stencil benchmark on a regular
grid. Our implementation of control replication achieves up to 96\%
parallel efficiency on 512 nodes on the Piz Daint
supercomputer~\cite{PizDaint}. Section~\ref{sec:related} discusses
related work, and Section~\ref{sec:conclusion} concludes.
