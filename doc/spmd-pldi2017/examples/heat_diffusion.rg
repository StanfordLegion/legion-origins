import "regent"

fspace cell {
  a : double,
  b : double,
}

task do_physics(cells : region(cell))
where reads writes(cells) do
end

task do_stencil(cells : region(cell), ghost_cells : region(cell))
where reads writes(cells.a), reads(cells.b, ghost_cells.b) do
end

task main()
  var cells = region(ispace(ptr, 10), cell)

  var N = 3
  var private = partition(equal, cells, ispace(int1d, N))
  var ghost = partition(equal, cells, ispace(int1d, N)) -- should be aliased

  var T = 5
  __demand(__spmd)
  for t = 0, T do
    for i = 0, N do
      do_physics(private[i])
    end
    for i = 0, N do
      do_stencil(private[i], ghost[i])
    end
  end
end
