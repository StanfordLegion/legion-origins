\section{Evaluation}
\label{sec:eval}

We evaluate the performance of the Regent
implementation, and the effectiveness of the optimizations performed
by the Regent compiler, on three applications: a circuit simulation;
PENNANT, a Lagrangian hydrodynamics code; and
MiniAero, an explicit solver for the compressible Navier-Stokes
equations on an unstructured mesh.
The experiments were done on the Certainty
supercomputer~\cite{StanfordHPCC}. Each node has two sockets with an
Intel Xeon X5650 per socket for a total of 12 physical cores per
node (24 threads with hyperthreading). Nodes are connected with Mellanox
QDR Infiniband. The Legion runtime, along with all three C++ reference codes,
have been compiled with GCC 4.9.2. Terra (and therefore Regent) uses LLVM 3.5.

We perform several experiments on the Regent and reference
implementations of each application. First, we compare Regent absolute
performance against the reference on the target machine.

Next, to demonstrate the impact of the compiler optimizations performed by
Regent, we perform \emph{knockout experiments} for each application,
disabling each optimization presented in
Section~\ref{sec:optimizations} in turn. In addition, we perform
double knockout experiments, measuring performance with all possible
pairs of two optimizations disabled, and call out a few interesting
combinations. As several of the optimizations impact the achieved parallelism, we evaluate each
configuration in a parallel configuration and compare against the best sequential performance achieved by Regent. The labels for the
various optimizations are described in Figure~\ref{fig:knockout-key}.
Pointer check elision has been previously demonstrated to have a
significant impact on performance~\cite{LegionTypes13} and has been
left out of the knockout to reduce clutter.

Finally, we evaluate the productivity of Regent by comparing the
number of lines of codes in each Regent implementation against each
reference. Figure~\ref{fig:loc} summarizes the
results. Application-specific details are described along with each
application below.

\begin{figure}[t]
\centering
\begin{tabular}{l | l l l l}
Key & Optimization \\
\hline
map & Mapping Elision \\
leaf & Leaf Task Optimization \\
idx & Index Launch Optimization \\
fut & Future Optimization \\
dbr & Dynamic Branch Elision \\
vec & Vectorization \\
all & All of the Optimizations Above
\end{tabular}
\caption{Legend key for knockout experiments.}
\label{fig:knockout-key}
\end{figure}

\begin{figure}[t]
\centering
\begin{tabular}{l | l l l l}
Application & Regent & Reference \\
\hline
Circuit & 825 & 1701 \\
PENNANT & 1789 & 2416 \\
MiniAero & 2836 & 3993
\end{tabular}
\caption{Lines of code (non-comment, non-blank) for Regent and reference implementations.}
\label{fig:loc}
\end{figure}

\pgfplotsset{
  cycle list={%
    {red, mark=*}, {blue,mark=square*},
  }
}
\begin{figure*}[t!]
  \begin{subfigure}[t!]{0.5\textwidth}
    \centering
    \pgfplotsset{width=\textwidth,compat=1.5.1}
    \begin{tikzpicture}
      \begin{axis}[
        xlabel=Total CPUs,
        xtick={1,10,20,...,80},
        ylabel=GFLOPS,
        legend style={at={(0.05, 0.8)}, anchor=south west, legend columns=1}
        ]
        \addplot
        table[x=Cpus,y=Gflops]
        {experiments/rawdata/circuit/DATA_I1_F1_N1_L1_V1};
        \addplot
        table[x=Cpus,y=Gflops]
        {experiments/rawdata/circuit/DATA_BASELINE};
        \legend{Regent,Legion}
      \end{axis}
    \end{tikzpicture}
    \caption{Circuit performance vs Legion.}
    \label{fig:performance-circuit}
  \end{subfigure}
  \begin{subfigure}[t!]{0.5\textwidth}
    \centering
    \pgfplotsset{width=\textwidth,compat=1.5.1}
    \begin{tikzpicture}
      \begin{axis}[
        xlabel=Total CPUs,
        % xtick=data,
        xtick={1, 4, 8, ..., 24}, ylabel=Zones per second (in millions),
        legend style={at={(0.05, 0.8)}, anchor=south west, legend columns=1}
        ]

        \addplot table[x=Threads,y=MZonesPerSecond]
        {experiments/rawdata/pennant/singlenode/PENNANT_DATA_I1_F1_N1_L1_V1_D1};
        \addplot table[x=Threads,y=MZonesPerSecond]
        {experiments/rawdata/pennant/singlenode/PENNANT_DATA_BASELINE};
        \legend{Regent,OpenMP}
      \end{axis}
    \end{tikzpicture}
    \caption{PENNANT performance vs OpenMP.}
    \label{fig:performance-pennant}
  \end{subfigure}

  \begin{subfigure}[t!]{\textwidth}\vspace{13pt}\end{subfigure}

  \begin{subfigure}[t!]{0.5\textwidth}
    \centering
    \pgfplotsset{width=\textwidth,compat=1.5.1}
    \begin{tikzpicture} 
      \begin{axis}[
        y tick label style={
          /pgf/number format/.cd,
          fixed,
          fixed zerofill,
          precision=1,
          /tikz/.cd
        },
        xlabel=Total CPUs,
        xtick=data,
        ylabel=Cells per second (in millions),
        legend style={at={(0.05, 0.8)}, anchor=south west, legend columns=1}
        ]

        \addplot
        table[x=Cpus,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/singlenode_power2/MINIAERO_DATA_I1_F1_N1_L1_V1};
        \addplot
        table[x=Cpus,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/singlenode_power2/MINIAERO_DATA_BASELINE};
        \legend{Regent,MPI+Kokkos}
      \end{axis}
    \end{tikzpicture}
    \caption{MiniAero single-node performance vs MPI+Kokkos.}
    \label{fig:performance-miniaero-singlenode}
  \end{subfigure}
  \begin{subfigure}[t!]{0.5\textwidth}
    \centering
    \pgfplotsset{width=\textwidth,compat=1.5.1}
    \begin{tikzpicture} 
      \begin{axis}[
        y tick label style={
          /pgf/number format/.cd,
          fixed,
          fixed zerofill,
          precision=1,
          /tikz/.cd
        },
        xlabel=Total Nodes (8 CPUs per Node),
        xtick=data,
        ylabel=Cells per second (in millions),
        legend style={at={(0.05, 0.8)}, anchor=south west, legend columns=1}
        ]

        \addplot
        table[x=Nodes,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/multinode_power2/MINIAERO_DATA_I1_F1_N1_L1_V1};
        \addplot
        table[x=Nodes,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/multinode_power2/MINIAERO_DATA_BASELINE};
        \legend{Regent,MPI+Kokkos}
      \end{axis}
    \end{tikzpicture}
    \caption{MiniAero multi-node performance vs MPI+Kokkos.}
    \label{fig:performance-miniaero-multinode}
  \end{subfigure}
  \vspace{7pt}
  \caption{Performance evaluations.}
\end{figure*}

\subsection{Circuit}
\label{subsec:eval-circuit}

Circuit, introduced in \cite{Legion12}, is a distributed circuit
simulation, operating over an arbitrary graph of nodes and
wires. While in principle the topology of the graph can be arbitrary,
Circuit is concerned primarily with topologies with interconnected
dense subgraphs. Such graphs allow Circuit to achieve some level of
scalability, though that scalability is ultimately limited by the
global all-to-all communication pattern between the subgraphs.

We compare the performance of Regent against a hand-tuned and manually
vectorized CPU implementation written to the C++ Legion API. We
evaluate both implementations on a graph with 800K wires connecting
200K nodes. Figure~\ref{fig:performance-circuit} shows the performance
of Regent against the baseline C++ Legion implementation running on up
to 8 nodes on Certainty. Notably, the fully-optimized Regent
im\-ple\-men\-ta\-tion---which is written in a straightforward way with no use
of explicit vectors or vector intrinsics, and is less than half the
total number of lines of code---achieves performance comparable to the
manually vectorized C++ code, exceeding the performance that can be
achieved by using the LLVM 3.5 vectorizer alone.

Figure~\ref{fig:knockout-circuit} demonstrates the impact of disabling
individual and pairs of optimizations on the performance of the Regent
implementation of Circuit. Certain optimizations impact the
parallelism available in the application; index launch optimization and mapping elision
are two such optimizations. When both are disabled simultaneously, the
code runs sequentially. As
described in Section~\ref{subsec:inline}, the Legion runtime, in the
absence of the map and unmap calls placed by the compiler, must copy
back the results of each task execution before returning control to
caller. This creates an effective barrier between consecutive tasks,
but the effect is not noticeable as long as index launch optimization
is able to parallelize the task launches. Disabling both optimizations
serializes the code.  But if either optimization is disabled by
itself, the application continues to run in parallel at somewhat
reduced throughput.

Dynamic branch elision does not have a significant impact on the
performance of Circuit and has been omitted to reduce clutter.

\subsection{PENNANT}
\label{subsec:eval-pennant}

PENNANT~\cite{PENNANT} is a mini-app for Lagrangian
hydrodynamics representing a subset of the
functionality of FLAG~\cite{FLAG}, a LANL production code.
Figure~\ref{fig:performance-pennant} evaluates Regent against an
OpenMP implementation of PENNANT on a problem containing
approximately 2.6M zones. Implementation details of the
Regent version are discussed in Section~\ref{sec:examples}.

%% Regent at 10 cores: 8.894852
%% OpenMP at 12 cores: ???
%% OpenMP at 24 cores: ???
%% (execution time; smaller is better)

Regent performs better than OpenMP for all core counts up to
10, surpassing OpenMP by 8\% at 10 cores. Starting at 12 cores, Regent
performance degrades because the
additional compute threads interfere with threads Legion uses for
dynamic dependence analysis and data movement. The Legion runtime is
also unable to exclusively allocate physical cores for each thread and
abandons pinning altogether, leading to increased interference between
application threads.

PENNANT is largely memory-bound, and is thus significantly impacted by
the NUMA architecture of the machine. OpenMP performance was
substantially impacted by CPU affinity, and a manual assignment of
threads to cores was needed for optimal performance. Regent
automatically binds threads to cores when possible and round robins
threads between NUMA domains, thus performing well with minimal manual
tuning.

Regent achieves good performance despite an overall decrease in lines of
code. The numbers listed in Figure~\ref{fig:loc} exclude a number of
routines, shared between both implementations, for generating the
input mesh and exporting the output of the simulation to files.

Figure~\ref{fig:knockout-pennant} shows that the Regent implementation
of PENNANT exhibits varied behavior with certain combinations of
optimizations disabled. As with Circuit, the combination of index
launches and mapping elision causes the application to execute
sequentially. However, PENNANT offers a different response to the
combination of index and leaf optimizations. PENNANT's pattern of task
launches is such that when leaf optimization is disabled, the Legion
runtime must stall for mapping to complete in order to ensure that all
the dependencies are correctly captured. Circuit is structured
differently from PENNANT and is therefore not impacted significantly
by the leaf optimization (in combination with index launches or
otherwise).

% Results from:
% experiments/rawdata/pennant/knockout/PENNANT_DATA_I1_F1_N1_L1_V1_D0
% experiments/rawdata/pennant/knockout/PENNANT_DATA_I1_F1_N1_L1_V1_D1

PENNANT also shows the most benefit from eliminating dynamic
branches. In contrast to Circuit and MiniAero which are generally
compute or memory bound, certain performance critical kernels in
PENNANT contain long chains of dependent math instructions, which in
turn depend on conditional memory accesses (when dynamic branch
elision is not enabled). At 10 cores, throughput improves by 15\% if
dynamic branches can be eliminated.

\begin{figure*}[t!]
  %\begin{subfigure}[t!]{0.5\textwidth}
  \begin{subfigure}[t!]{0.33\textwidth}
    \centering
    \pgfplotsset{width=\textwidth,height=6cm,compat=1.5.1}
    \begin{tikzpicture}
      \begin{axis}[
        ylabel=GFLOPS,
        ybar,
        ymin=0,
        xmin=0,
        xmax=10,
        xtick={1,2,3,4,5,6,7,8,9},
        xticklabels from table={experiments/rawdata/circuit/knockout/label}{Type},
        major x tick style = transparent,
        x tick label style={rotate=90,anchor=east},
        bar width=9pt
        ]
        
        \addplot+[forget plot, fill=red, draw=black]
        table[x=X,y=Gflops]
        {experiments/rawdata/circuit/knockout/DATA_I0_F0_N0_L0_V0};
        \addplot+[forget plot,fill=lime, draw=black]
        table[x=X,y=Gflops]
        {experiments/rawdata/circuit/knockout/DATA_I0_F1_N0_L1_V1};
        \addplot+[forget plot, fill=orange, draw=black]
        table[x=X,y=Gflops]
        {experiments/rawdata/circuit/knockout/DATA_I0_F1_N1_L0_V1};
        \addplot+[forget plot, fill=cyan, draw=black]
        table[x=X,y=Gflops]
        {experiments/rawdata/circuit/knockout/DATA_I0_F1_N1_L1_V1};
        \addplot+[forget plot, fill=teal, draw=black]
        table[x=X,y=Gflops]
        {experiments/rawdata/circuit/knockout/DATA_I1_F0_N1_L1_V1};
        \addplot+[forget plot, fill=magenta, draw=black]
        table[x=X,y=Gflops]
        {experiments/rawdata/circuit/knockout/DATA_I1_F1_N0_L1_V1};
        \addplot+[forget plot, fill=yellow, draw=black]
        table[x=X,y=Gflops]
        {experiments/rawdata/circuit/knockout/DATA_I1_F1_N1_L0_V1};
        \addplot+[forget plot, fill=brown, draw=black]
        table[x=X,y=Gflops]
        {experiments/rawdata/circuit/knockout/DATA_I1_F1_N1_L1_V0};
        \addplot+[forget plot, fill=blue, draw=black]
        table[x=X,y=Gflops]
        {experiments/rawdata/circuit/knockout/DATA_I1_F1_N1_L1_V1};
        \addplot+[forget plot, sharp plot, color=red, thick]
        coordinates { (0, 10.963) (10, 10.963) };
      \end{axis}
    \end{tikzpicture}
    \caption{Circuit (10 CPUs)}
    \label{fig:knockout-circuit}
  \end{subfigure}
  %\begin{subfigure}[t!]{0.5\textwidth}
  \begin{subfigure}[t!]{0.33\textwidth}
    \centering
    %\pgfplotsset{width=10cm,height=9cm,compat=1.5.1}
    \pgfplotsset{width=\textwidth,height=6cm,compat=1.5.1}
    \begin{tikzpicture}
      \begin{axis}[
        ymin=0,
        xmin=0,
        xmax=10,
        xtick={1,2,3,4,5,6,7,8,9},
        xticklabels from table={experiments/rawdata/miniAero/knockout/label}{Type},
        major x tick style = transparent,
        x tick label style={rotate=90,anchor=east},
        ybar,
        bar width=9pt,
        ylabel=Cells per second (in millions)
        ]

        \addplot+[forget plot, fill=red, draw=black]
        table[x=X,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/knockout/MINIAERO_DATA_I0_F0_N0_L0_V0};
        \addplot+[forget plot,fill=lime, draw=black]
        table[x=X,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/knockout/MINIAERO_DATA_I0_F1_N0_L1_V1};
        \addplot+[forget plot, fill=orange, draw=black]
        table[x=X,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/knockout/MINIAERO_DATA_I0_F1_N1_L0_V1};
        \addplot+[forget plot, fill=cyan, draw=black]
        table[x=X,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/knockout/MINIAERO_DATA_I0_F1_N1_L1_V1};
        \addplot+[forget plot, fill=teal, draw=black]
        table[x=X,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/knockout/MINIAERO_DATA_I1_F0_N1_L1_V1};
        \addplot+[forget plot, fill=magenta, draw=black]
        table[x=X,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/knockout/MINIAERO_DATA_I1_F1_N0_L1_V1};
        \addplot+[forget plot, fill=yellow, draw=black]
        table[x=X,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/knockout/MINIAERO_DATA_I1_F1_N1_L0_V1};
        \addplot+[forget plot, fill=brown, draw=black]
        table[x=X,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/knockout/MINIAERO_DATA_I1_F1_N1_L1_V0};
        \addplot+[forget plot, fill=blue, draw=black]
        table[x=X,y=MCellsPerSecond]
        {experiments/rawdata/miniAero/knockout/MINIAERO_DATA_I1_F1_N1_L1_V1};
        \addplot+[sharp plot,color=red,thick]
        coordinates { (0, 0.10963522117634678) (10, 0.10963522117634678) };
      \end{axis}
    \end{tikzpicture}
    \caption{MiniAero (8 CPUs)}
    \label{fig:knockout-miniaero}
  \end{subfigure}
  %\begin{subfigure}[t!]{0.5\textwidth}
  \begin{subfigure}[t!]{0.33\textwidth}
    \centering
    \pgfplotsset{width=\textwidth,height=6cm,compat=1.5.1}
    \begin{tikzpicture} 
      \begin{axis}[
        ymin=0,
        xmin=0,
        xmax=11,
        xtick={1,2,3,4,5,6,7,8,9,10},
        xticklabels from table={experiments/rawdata/pennant/knockout/label}{Type},
        major x tick style = transparent,
        x tick label style={rotate=90,anchor=east},
        ybar,
        bar width=9pt,
        ylabel=Zones per second (in millions)
        ]

        \addplot+[forget plot, fill=red, draw=black]
        table[x=X,y=MZonesPerSecond]
        {experiments/rawdata/pennant/knockout/PENNANT_DATA_I0_F0_N0_L0_V0_D0};
        \addplot+[forget plot,fill=lime, draw=black]
        table[x=X,y=MZonesPerSecond]
        {experiments/rawdata/pennant/knockout/PENNANT_DATA_I0_F1_N0_L1_V1_D1};
        \addplot+[forget plot, fill=orange, draw=black]
        table[x=X,y=MZonesPerSecond]
        {experiments/rawdata/pennant/knockout/PENNANT_DATA_I0_F1_N1_L0_V1_D1};
        \addplot+[forget plot, fill=cyan, draw=black]
        table[x=X,y=MZonesPerSecond]
        {experiments/rawdata/pennant/knockout/PENNANT_DATA_I0_F1_N1_L1_V1_D1};
        \addplot+[forget plot, fill=teal, draw=black]
        table[x=X,y=MZonesPerSecond]
        {experiments/rawdata/pennant/knockout/PENNANT_DATA_I1_F0_N1_L1_V1_D1};
        \addplot+[forget plot, fill=magenta, draw=black]
        table[x=X,y=MZonesPerSecond]
        {experiments/rawdata/pennant/knockout/PENNANT_DATA_I1_F1_N0_L1_V1_D1};
        \addplot+[forget plot, fill=yellow, draw=black]
        table[x=X,y=MZonesPerSecond]
        {experiments/rawdata/pennant/knockout/PENNANT_DATA_I1_F1_N1_L0_V1_D1};
        \addplot+[forget plot, fill=brown, draw=black]
        table[x=X,y=MZonesPerSecond]
        {experiments/rawdata/pennant/knockout/PENNANT_DATA_I1_F1_N1_L1_V0_D1};
        \addplot+[forget plot, fill=pink, draw=black]
        table[x=X,y=MZonesPerSecond]
        {experiments/rawdata/pennant/knockout/PENNANT_DATA_I1_F1_N1_L1_V1_D0};
        \addplot+[forget plot, fill=blue, draw=black]
        table[x=X,y=MZonesPerSecond]
        {experiments/rawdata/pennant/knockout/PENNANT_DATA_I1_F1_N1_L1_V1_D1};
        \addplot+[forget plot, sharp plot, color=red, thick]
        coordinates { (0, 0.5260291685819759) (11, 0.5260291685819759) };
      \end{axis}
    \end{tikzpicture}
    \caption{PENNANT (10 CPUs)}
    \label{fig:knockout-pennant}
  \end{subfigure}
  \vspace{7pt}
  \caption{Knockout experiments. The red line in each graph shows the best sequential Regent performance.}
\end{figure*}


\subsection{MiniAero}
\label{subsec:eval-miniaero}

% From Janine:

%% Mini Aero is a three-dimensional, unstructured, finite-volume,
%% computation fluid dynamics code that uses Runge-Kutta fourth-order
%% time marching.  It has options for first or second order spatial
%% discretization, and includes inviscid Roe and Newtonian viscous
%% fluxes.  The baseline application is approximately 3800 lines of C++
%% code, written with MPI and Kokkos, a Sandia-based performance
%% portability layer.

MiniAero~\cite{Mantevo} is a computational fluid dynamics mini-app
that uses a Runge-Kutta fourth-order time marching scheme to solve the
compressible Navier-Stokes equations on a 3D unstructured mesh. The
baseline version of MiniAero is implemented as a hybrid code, using
MPI for inter-node communication and Trilinos Kokkos~\cite{Kokkos} for
intra-node
parallelism. Figures~\ref{fig:performance-miniaero-singlenode} and
\ref{fig:performance-miniaero-multinode} compare performance between a
Regent implementation and the baseline MPI+Kokkos version on a problem
size with 4M cells and 13M faces running on up to 4 nodes on Certainty.
The Regent implementation for
MiniAero is approximately 30\% shorter by lines of code than the
reference.

% Results from:
% experiments/rawdata/miniAero/singlenode_power2/MINIAERO_DATA_BASELINE
% experiments/rawdata/miniAero/singlenode_power2/MINIAERO_DATA_I1_F1_N1_L1_V1

Regent outperforms MPI+Kokkos on 8 cores by a factor of 2.8X
through the use of a hybrid SOA-AOS data layout, an approach similar
to that taken in \cite{LegionFields14}. The improved data layout
substantially boosts cache reuse and improves utilization of memory bandwidth.%%  and enables Regent to continue
%% scaling through multiple nodes.

%% Regent matches MPI+Kokkos performance up through 4 nodes but falls off
%% slightly at 8 nodes. Regent performance falls off for two
%% reasons. First, increased runtime overhead relative to task size at
%% higher node counts makes it more challenging for the Legion runtime to
%% stay ahead of the application. Second, imprecision in the
%% communication patterns in the application, mostly due to the choice of
%% partitioning scheme, cause Legion to slightly over-approximate the
%% amount of data that needs to transfered between nodes. Neither of
%% these issues is fundamental to the Regent programming model.

Figure~\ref{fig:knockout-miniaero} demonstrates that MiniAero is
sensitive to the combination of index launches and mapping
elision. When both optimizations are disabled, the code runs
serially. MiniAero is otherwise relatively resilient to the choice of
optimizations performed. MiniAero is not significantly impacted by
dynamic branch elision (not shown).

\subsection{Limitations}

While the Regent compiler significantly simplifies aspects of writing
to the Legion programming model, our current implementation does have
limitations.  The most important one is that Legion's support for {\em simultaneous}
access to regions by tasks is not currently
implemented in Regent \cite{Legion12}.  Briefly, simultaneous access
allows Legion programs to implement SPMD style patterns, such as are
typically used in UPC~\cite{UPC99,UPCSTANDARD}, Titanium~\cite{Tit98} and MPI~\cite{MPI}
programs, where multiple concurrently executing tasks share access to
the same region, mediated by explicit synchronization operations.
Simultaneous access exists in Legion to address a problem that any
dynamic task-based model with subtasks (i.e., not just Legion) faces.
The performance of a task that launches substasks depends on the
number of subtasks launched and the duration of those subtasks; the
runtime overhead incurred is proportional to the number of subtasks
launched, and so the more subtasks that are launched by a task, the
longer those subtasks must run to keep the ratio of useful work to
runtime overhead low.  When launching very large numbers of tasks, say
across a petascale supercomputer, the runtime overheads will dominate
the useful computation of the tasks unless the tasks run for a long
time. For applications in which the bulk of the available parallelism
comes from repeated and regular data-parallel task launches, the SPMD
structure allows a very large number of long-running
tasks to be launched at application start, and then be further
subdivided using the functional-style task calls we have shown in our
examples in this paper (where each task has {\em exclusive} access to
its region arguments \cite{Legion12}).

In our experience, such applications should be written in the
SPMD syle at the outermost level of control if they are to scale past
somewhere between 10 and 100 nodes, depending on the application
domain.  Because of that, and because the optimizations described in
this paper are effective even on a single node, we have focused on
experiments on one or a small number of nodes.  There is no technical
barrier to adding support for simultaneous access to Regent as it
already exists in Legion.  This approach is known to provide
scalability to more than 10K nodes in Legion~\cite{Bauer14}, and it may be the
solution we eventually adopt, but simultaneous access does add another
dimension of complexity to the programming model that we would like to
avoid in Regent if possible.  As future work we are interested in
whether it is possible to provide scalability to large numbers of
nodes in Regent for applications with regular (and repetitive) data
parallel tasks by compiler transformations that target Legion's
simultaneous coherence without exposing that feature to the Regent
programmer.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "compiler"
%%% End: 
