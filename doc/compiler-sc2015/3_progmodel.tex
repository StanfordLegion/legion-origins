\section{Programming Model}
\label{sec:progmodel}

Before we discuss the Regent programming model in more detail, we
explain more about Legion so that the reader can appreciate the
differences between the two systems.  Legion, the runtime which Regent
targets, is implemented as a {\em software out-of-order processor}~\cite{Legion12}.  Tasks are issued to the Legion runtime in program
order, but the underlying Legion scheduler may reorder tasks and
execute them in parallel if it can prove that it is safe to do so.
The Legion runtime analyzes each task's privileges for its region
arguments to identify when tasks are using the same regions in ways
that either allow parallel execution or require that the tasks be
serialized in the order they were issued.

Besides managing the tasks, the Legion runtime also manages the
regions.  Tasks are written using {\em logical regions}, which simply
name collections of objects.  During execution each logical region may
correspond to any number of {\em physical instances}, which are actual
allocated copies of the data.  Separating the logical and physical
levels allows important patterns, such as having multiple copies of
read-only data, to be expressed directly.

The Legion abstractions allow programmers to write efficient task-based
programs that run out-of-order, asyn\-chro\-nously, and in a distributed
fashion. However, because Legion is embedded in C++, which does not
understand the semantics of tasks and regions, the Legion API is
forced to expose functionality beyond the logical layer of the
programming model. Programmers must generally write Legion programs
with some awareness of both the logical and physical levels.

Regent exposes only the logical
level. The lower-level, physical details of the Legion execution model
are hidden from the programmer. For a naive implementation, this would be
disastrous for performance. However, with the support of static
analysis and compiler optimizations, Regent is able to close the gap
between logical and physical constructs and provide a seamless
abstraction to the programmer.

In the rest of this section we present the Regent and Legion
programming models in more detail, examine the differences between
the two, and demonstrate how a Regent
compiler is able to translate between them. In
Section~\ref{sec:optimizations} we consider the optimizations
performed by the compiler that enable this translation to be efficient.

\subsection{Tasks}
\label{subsec:tasks}

Tasks are the fundamental unit of control in both Regent and Legion.
Tasks are issued in program order, exactly as they are written in the
text, and every possible program execution is guaranteed to be
indistinguishable from serial execution. As discussed in
Section~\ref{sec:examples}, tasks specify the regions they use and
their permissions for those regions (whether the task performs reads,
writes, or reductions to each region).  In addition, tasks declare
which {\em fields} of the objects in the region the task accesses.
Together, the declaration that specific fields of a region are
accessed with certain privileges is called a {\em region requirement}.

Whenever two tasks
are {\em non-interfering}, accessing either disjoint regions, different fields
of the same region, or the same fields with compatible permissions (e.g., both
tasks only read the field or only perform the same reduction to the field), 
Regent allows those tasks to run in
parallel. Wherever two tasks interfere, Regent
inserts the appropriate synchronization and copy operations to ensure
that the data dependence is handled properly.  In addition to regions,
tasks can also take partition arguments and specify partition requirements.

When writing (or compiling) to the Legion C++ interface, several
additional aspects of the Legion runtime implementation are exposed,
requiring additional user effort. Legion's dynamic dependence analysis
imposes a cost with every task launched. To ensure that this overhead
stays off the critical path, the Legion runtime is itself asynchronous
and parallel~\cite{Legion12}.  The goal is for the runtime to run ahead
of the application, issuing tasks and analyzing task interference in
advance of when those tasks can actually run.  Pipeline stalls, blocking
operations, and excessive analysis costs can all cause the runtime to fall
behind and hurt the performance of the application. Legion mitigates
these issues by providing more sophisticated abstractions which can
result in higher performance, but also have more complex semantics.

\subsubsection{Avoiding Pipeline Stalls}
\label{subsubsec:stalls}

Task execution in Legion is pipelined.  In general, a task must
complete a pipeline stage before it passes to the next stage.  If a
given stage stalls for any reason, that task and any task that depends
on it also stalls.  {\em Mapping}, described in greater detail in
Section~\ref{subsec:mapping}, is
one pipeline stage. When a task is mapped, a processor is
selected to execute the task and memories are chosen to hold the
physical instances of each of its region arguments.

Because tasks can execute subtasks, Legion must wait for all subtasks
to map before it can consider a parent task to have completed
mapping. In general the only way to know that a parent task cannot
issue more subtasks is that the parent task has terminated, which can
result in unnecessary pipeline stalls when the task in question never
intended to launch any subtasks.

Legion allows users to annotate tasks as \emph{leaf} tasks if they
launch no subtasks, a mechanism inherited from
Sequoia~\cite{Fatahalian06}. In Legion, the runtime considers the
mapping of a leaf task to be complete once the task itself is mapped,
avoiding unnecessary pipeline stalls for dependent operations.


\subsubsection{Avoiding Blocking Operations}
\label{subsubsec:blocking}

Tasks can produce results in one of
two ways: direct return values, or as a side-effect on a region
argument.
In Legion, operations can block whenever a parent task consumes a
result produced by one of its child tasks. 
The Legion runtime provides ways of avoiding blocking on both kinds of task results.

When tasks produce direct return values, Legion wraps those values in
\emph{futures}. Users can block to obtain the value of a future, but
Legion also supports passing futures as inputs to other tasks without blocking.
In this way, the programmer can describe the flow of values between tasks 
without blocking, allowing the runtime to run further ahead and hide runtime analysis
costs. Futures are visible in the C++ sample codes in
Listing~\ref{lst:code3} lines 18-19 and Listing~\ref{lst:code4} line
23.

When a parent task needs to read the results of a region written by a
child task, unless the parent task has explicitly indicated otherwise,
the Legion runtime must conservatively assume that the parent task may
attempt to access the regions used by the child task as soon as the
child returns. To preserve sequential semantics, the runtime blocks
the parent while the child task is in flight to ensure the child's results
are available before the parent continues. To avoid blocking, parents
must declare to the runtime that the region data is not required by
\emph{unmapping} (releasing) the physical instance of the region prior
to calling the child.

\subsubsection{Reducing Analysis Costs}
\label{subsubsec:costs}

Even when execution does not stall in the runtime or block in the application, 
the cost of dynamic analysis itself can cause
the runtime to fall behind. One approach for reducing runtime
overheads is to use \emph{index space task launches}.
Conceptually, index launches simply represent a loop of task
launches. Listing~\ref{lst:code2} lines 16-18 shows an example of a
Regent loop that can be transformed into an index launch (with
corresponding C++ code in Listing~\ref{lst:code4}). However, the
Legion runtime places several restrictions on index launches
to ensure that they are well-behaved:

\begin{enumerate}
\item Arguments to all tasks in the index launch must be
  computed outside the launch, guaranteeing that arguments are available and
  that no arguments depend on side-effects from tasks within the
  launch.
\item Futures, if any, are added to the launch as a whole, not to
  individual tasks.
\item Requirements can be in one of two forms:
  \begin{itemize}
  \item Individual region requirements add a single region to all
    tasks in the launch.
  \item Partition requirements add a subregion of the partition for each task.
  \end{itemize}
  Legion supports user-defined projection functions to allow the
  programmer to dynamically select subregions for each type of region
  requirement. 
  %In practice, the built-in identity projection function
  %often suffices.
\item  Because an index launch implies parallel execution, all the tasks must
  be non-interfering.
\item If tasks within the launch return a value, then the launch as a
  whole is allowed to either return a map with all the resulting
  futures, or to reduce the futures into a single value.
\end{enumerate}

When executing an index launch, the runtime still performs dynamic
checks to ensure that the tasks within the launch are
non-interfering. However, Legion is able to amortize these checks
across the entire index launch instead of performing them
individually.

\subsection{Regions}
\label{subsec:regions}

Logical regions are created as the cross product of an \emph{index
  space} (set of indices) and a \emph{field space} (set of
fields). Logical regions can be compared to arrays of objects or
structs, though this analogy falls short in several ways. In
particular, as discussed previously, because a logical region may have multiple
physical instances, there is no one-to-one mapping
between a logical region and its representation in memory.

\subsubsection{Physical Instances}
\label{subsubsec:instances}

In Legion, a logical region may, at any given point in time, map to
zero or more physical instances. Access to each field of a physical 
instance is mediated through a
field-specific \emph{accessor}. In the Legion C++ interface, the programmer
must manage distinct
\lstinline{LogicalRegion}, \lstinline{PhysicalRegion}, and
\lstinline{Accessor} types.

In Regent, these differences disappear because the compiler manages the
mapping from logical to physical regions (and physical regions to
accessors) transparently for the programmer. Field spaces can be
constructed concisely from struct types, and nested structs are
automatically expanded into their component fields. Accessors are
created automatically for whatever fields the programmer declared in the
privileges for the task. These differences are illustrated in the difference
between Listing~\ref{lst:code1} and Listing~\ref{lst:code3}. In
contrast to users of the Legion C++ API, Regent programmers can
usually pretend that regions are simply arrays of structs or objects.

Physical instances in Legion may be stored in one of a number of
layouts. Examples of common layouts include array-of-structs and
struct-of-arrays, while more esoteric layouts may include arrays
blocked for vectorized CPU instructions. Legion provides explicit
accessor objects in order to constant-fold compile-time information
about instance layouts for efficient access, as seen in
Listing~\ref{lst:code3} lines 6-17. Regent manages accessors, along
with instances, on behalf of the programmer.

Regent also manages the creation of region requirements for
each task. For each task, Regent flattens the fields, groups them by
privilege, and issues a region requirement for each privilege and set
of fields; see the correspondence between
Listing~\ref{lst:code2} line 17 and Listing~\ref{lst:code4} lines 6-22.

\subsubsection{Partitions}
\label{subsubsec:partitions}

Partitioning a region using the Legion C++ interface happens in two
steps. First, the user creates an \emph{index partition} of the index
space to specify how the sets of indices are subdivided
between the spaces. Second, the user applies the index partition to a
logical region created using that same index space to obtain a
corresponding \emph{logical partition}. In Regent, these operations
are combined, as the correspondence between logical regions and index spaces is
managed for the programmer.

\subsection{Mapping}
\label{subsec:mapping}

As briefly described previously, mapping is the process of selecting a processor to run each
task and a memory (and data layout) for each logical region. Mapping is under the control of
the application, though  Regent provides a
\emph{default mapper} with sensible settings to allow users to get up
and running quickly. %% Regent also provides a \emph{correctness
%%   guarantee} for mappers, so that no decision a mapper can make will
%% impact the correctness of the application.

Legion also provides a mapping interface, but because of the distinction
between physical and logical constructs exposed in Legion, this
mapping process is more involved.

In Legion, logical regions must be mapped to physical instances in a
specific memory before they can be used.  By default, at the start of a
task Legion automatically maps each region used by the task,  and when the
task ends each of those regions is unmapped.
Before launching a subtask a parent task must
also unmap any region that the child task needs to use.  By default, the
Legion runtime unmaps all of the parent's regions before calling a
child and remaps them when the child terminates.  
While this default behavior
guarantees correct execution, if the parent and child have interfering
privileges for a region (e.g., both can write the region) then
the parent will most likely block until the child terminates,
as the parent cannot remap the region until the child unmaps it
(recall the discussion in Section~\ref{subsubsec:blocking}).


For higher performance, Legion programmers can explicitly manage
region mappings themselves through explicit \emph{map} and
\emph{unmap} calls provided by the Legion interface. By unmapping a
region, the programmer notifies the runtime that the data in that region is
not required by the parent task until a corresponding map call is
issued. In typical usage, programmers unmap all regions before entering a
main loop, and remap all regions once the loop completes, which
ensures that the runtime can avoid blocking when issuing tasks within
that loop. An example of such an unmap call can be seen in
Listing~\ref{lst:code4} line 1.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "compiler"
%%% End: 
