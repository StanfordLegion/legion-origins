\section{Introduction}
\label{sec:intro}

Modern supercomputers feature distributed memory architectures with
deep memory hierarchies. Currently, the state of the art in
programming this class of machines is the MPI+X hybrid programming
model. While MPI+X codes achieve good performance, they do so at a
cost to programmer productivity and performance portability. Users of
MPI+X must explicitly manage data movement and synchronization within
and between nodes.  Furthermore, they must also explicitly overlap communication
with computation for optimal performance, a task made difficult by the
need to interface with two disparate programming models. In addition, the
degree to which communication and computation must be overlapped
to achieve good performance depends on machine-specific factors, resulting in poor performance
portability in aggressively hand-tuned codes.

An alternative that is receiving considerable attention is writing
programs for {\em task-based} runtimes.  While there is
significant variation among the current approaches~\cite{Legion12,OCR14,StarPU11,Uintah,Charmpp}, the common element is a graph
of tasks to be executed, where the graph's edges capture
ordering dependencies between tasks.  The advantage of the task-based
approach is that the computation is expressed at a higher level than
MPI+X, which allows for both more aggressive optimization by the
programming model's implementation and correspondingly less effort by
programmers to express the same optimizations by hand (as well as better
portability).  A disadvantage of all the current task-based models is
that they are runtime systems (i.e., libraries) embedded in a host
language that does not understand the task-based model's higher-level
semantics.  Programmers must do additional work to
maintain important invariants across calls to the runtime system,
resulting in a programming interface that is more complex and verbose
than a true programming language implementation could provide.  Furthermore, important optimizations
that require static analysis to be feasible are simply beyond the
scope of dynamic runtime systems.

To address these challenges we present Regent, a high-productivity
programming language for high performance computing. Regent features
two key abstractions: \emph{tasks} and \emph{logical regions}. Regent
programs look like ordinary sequential programs with calls to {\em
  tasks}, which are functions that the programmer has marked as
eligible for parallel execution. Regent guarantees that any parallel
execution is consistent with the sequential execution of a Regent
program. Internally, dependencies between tasks are inferred automatically,
freeing the user from the need to explicitly synchronize or manage data
movement around the machine. Regent programs are also trivially
deadlock-free and avoid a number of classes of mistakes possible in
lower level distributed programming.

Logical regions~\cite{Legion12, LegionTypes13, LegionFields14}, or
simply regions, are collections of structured objects. Regions have no
fixed location in the memory hierarchy---for example, because they may
be striped across nodes---and no fixed layout in memory---for example,
because different tasks or processors may prefer array-of-structs or
struct-of-arrays layouts. Regions can be recursively partitioned to
match the hierarchical structure of memory, and to facilitate parallel
execution on subsets of data. Regions can also be partitioned multiple
times to express sophisticated communications patterns involving
multiple views of the data, including ones with aliasing between
views.

We describe an optimizing compiler for Regent that translates Regent
programs into efficient implementations for Legion, a dynamic,
task-based asynchronous runtime system with native support for tasks and logical
regions~\cite{Legion12}. Regent simplifies the Legion programming model. Many details
of programming to the Legion runtime system can be managed statically
by the Regent compiler, resulting in Regent programs that are both
written at a higher level and with fewer lines of code than the
corresponding Legion programs.  Several novel optimizations allow the
Regent compiler to achieve performance equivalent to hand-tuned codes
written directly to the Legion C++ API.

To motivate the Regent programming model, we present excerpts from a
Regent implementation of PENNANT~\cite{PENNANT}, a Lagrangian
hydrodynamics code. Each of the following
sections discusses one of our contributions:

\begin{itemize}
\item Section~\ref{sec:progmodel} presents the Regent programming
  model and provides a detailed comparison with Legion.
\item Section~\ref{sec:optimizations} presents compiler
  optimizations that are important for achieving high performance in Regent programs.
\item Section~\ref{sec:impl} discusses the implementation of the
  Regent compiler.
\item Section~\ref{sec:eval} evaluates the performance of three
  applications written in Regent.
\end{itemize}

Section~\ref{sec:related} discusses related work, and
Section~\ref{sec:conclusion} concludes.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "compiler"
%%% End: 
