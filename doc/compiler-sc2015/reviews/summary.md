Reviewer 1: Weak Accept

  * "The performance comparison [...] is thorough and well-explained."
  * Does not understand that not all benchmarks are OpenMP.

Reviewer 2: Strong Reject

  * "Overall, the performance benefit on a multi-node environment is
    not gained [...]."
  * "They need to provide advantages over related work on high
    productive programming languages mentioned in Section 7 such as
    X10, UPC, etc."
  * Confused about why we used Kokkos in MiniAero instead of OpenMP.

Reviewer 3: Weak Accept

  * "Add MPI and MPI+OpenMP results for the PENNANT example."
  * Slightly confused about the nature of OpenMP vs MPI+OpenMP. (For
    the configurations we tested, OpenMP is strictly better than
    MPI and MPI+OpenMP.)

Reviewer 4: Weak Accept

  * Missed our comparison to Legion C++. (We do, in Circuit.)
  * CUDA? (We don't autogen, but we could use hand-written kernels.)
  * PENNANT Vectorization: Does Intel's icc make a difference?
  * "Performance evaluation is done on a relatively old system
    [...]. It is also fairly small, just up to 80 CPUs."
  * Confused about impact of some optimizations.
