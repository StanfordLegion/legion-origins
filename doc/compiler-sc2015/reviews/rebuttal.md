The authors would like to thank all reviewers for their detailed and
valuable feedback.

There seems to have been some confusion regarding the
benchmarks. Recall that Regent was evaluated on three applications
written in three distinct programming models.

 1. A Legion/C++ implementation of Circuit.
 2. An OpenMP implementation of PENNANT.
 3. An MPI+Kokkos implementation of MiniAero.

Each application represents a distinct point of comparison in our
evaluation.

Circuit represents our comparison to traditional, hand-written
Legion/C++ applications. Circuit represents a best practices approach
to programming Legion applications in C++, and has been hand-tuned for
performance by experts with intimate knowledge of the Legion
runtime. We demonstrate modest performance gains over Legion/C++ on 8
nodes using 10 cores per node (the 2 remaining cores are used by the
Legion runtime for dynamic analysis). In response to Reviewer 4, we
feel that this demonstrates our competitiveness against hand-tuned
Legion applications, in both single and multi-node configurations.

PENNANT represents a comparison to best practices with regard to codes
written in OpenMP. In response to Reviewer 3, comparisons between the
Regent and MPI(+OpenMP) versions of PENNANT on multiple nodes are not
possible at this time due to a performance issue in the Legion
runtime's handling of unstructured mesh partitions. However, for the
configurations in which we tested PENNANT (varying core counts on a
single node), OpenMP is strictly the best performing. MPI generally
performs 5-10% worse in these cases, and MPI+OpenMP is typically 1-2%
behind pure OpenMP. Keep in mind that PENNANT is mostly memory-bound,
leaving little room on the table for performance gains once memory
bandwidth is saturated.

In response to Reviewer 4, vectorization is difficult (within key
performance-critical kernels) because of long chains
of data-dependent instructions. We have observed modest (~20%)
performance improvements on a single core with the Intel C++ compiler, but
we believe these to be the result of improved usage of the memory
hierarchy rather than due to improved vectorization. However, these
benefits disappear at higher core counts as memory bandwidth becomes
saturated anyway. GCC slightly outperforms Intel at 24 cores.

MiniAero represents a comparison to best practices hybrid codes; in
this case, MPI+Kokkos. Kokkos is a C++ portability layer developed at
Sandia National Labs and is used in production codes at the labs. In
response to Reviewer 2, Kokkos does in fact use OpenMP as its
underlying parallelization layer. Kokkos provides higher-level
abstractions with a focus on performance portability, but which
ultimately compile down to high-performance OpenMP. Beyond this, the
MPI+Kokkos implementation in question had been independently
hand-tuned by a team of domain experts and performance programmers at
Sandia National Labs.

We believe that these benchmarks cover a broad spectrum of
possibilities, from existing task-based models to the current state of
practice as represented by OpenMP and MPI+X. Responding to Reviewer 2,
we decided to focus our comparisons on the current state of practice,
choosing the implementation of each benchmark with best absolute
performance.

With respect to GPUs, Regent provides the same degree of support as
Legion. Tasks may be written for the GPU, but the kernels must be
provided by the user. (Automatically generating efficient GPU kernels
is generally understood to be a challenging problem for a non-domain
specific language.) Because Regent already generates optimal control
code (i.e. for the tasks which launch subtasks), the presence or absence
of GPU kernels is irrelevant, from Regent's perspective.

In response to Reviewer 1, leaf tasks and futures are pre-existing
Legion features. Regent automates the usage of such features, avoiding
the need for users to program with them manually.

In response to Reviewer 4, dynamic branch elision is indeed important
for PENNANT, and can be seen to make about a 10-20% different in the
Figure 5. Future optimization can be more meaningfully shown to make a
difference in combination with index launch optimization, because of
the calculations needed to compute dt for the next timestep. However,
multiple reviewers have noted, the performance graphs are already
difficult to read and adding another line would clutter things
further.

We will make the changes requested by Reviewer 1 to clarify the
motivating example and apply the suggestions from multiple reviewers
to clean up the performance graphs.
