# Review of pap326s2 by Reviewer 1

## Summary and High Level Discussion

This paper presents a programming language (Regent) and a
corresponding optimizing compiler. Regent embraces the move to
higher-level representations of computations as task-based graphs. The
specific contributions of this work include:

(1) The Regent programming model,
(2) compiler optimizations for this model, and
(3) a compiler for Regent.

Regent compiles to Legion, an existing task-based programming model
and runtime system. Regent is a higher-level language that is able to
hide lower-level physical details of the Legion execution model.

The performance comparison between Regent implementations and
reference OpenMP implementations is thorough and well-explained.

## Comments for Rebuttal

The motivating example is difficult to understand without the
background provided in section 3.  Is it possible to swap these
sections, or provide some background to the reader earlier?

In section three the distinction between Legion and Regent is blurred.
Is the workaround for pipeline stalls(3.1.1) implemented in Legion or
Regent? Similarly futures seem to have been defined and implemented in
Legion; is that new work?

## Detailed Comments for Authors

The descriptions of Figures 4-7 are a bit terse. The reader would
benefit from a summary of the punchline for each figure. For instance,
for Figure 4: The Region implementation of Circuit outperforms the
baseline OpenMP at higher CPU counts. The optimizations have a large
impact on scaling performance. The impact can most clearly be seen
when the vectorization and idx-map optimizations are removed.

# Review of pap326s2 by Reviewer 2

## Summary and High Level Discussion

- The authors presents a high-productivity programming language for
HPC called Region.

- Regent features two key abstractions: tasks and logical regions.
Regent programs look like ordinary sequential programs with calls to
tasks, which are functions that the programmer has marked as eligible
for parallel execution.

- They also describe an optimizing compiler for Regent that translates
Regent programs into efficient implementations for Legion, a dynamic,
task-based asynchronous runtime system with native support for tasks
and logical regions

- Overall, the performance benefit on a multi-node environment is not
gained by the proposed Region framework and implementation. This is
not really attractive for the SC community.

- They need to provide advantages over related work on high productive
programming languages mentioned in Section 7 such as X10, UPC, etc.

## Comments for Rebuttal

(None)

## Detailed Comments for Authors

In 6.1, # of CPUs are not clear. It mentions they use up to 8 nodes,
but Figure shows up to 80 CPUs in total. Each node has 12 physical
cores, so it is not clear what “CPU” means here in the figures. It
sounds like that they use 80 CPU cores and 10 cores per node of 12
cores.

- In 6.3, it is not clear why they use Kokkos for intra-node
parallelism rather than OpenMP. They need to mention the reason.

- Overall, the performance benefit on a multi-node environment is not
gained by the proposed Region framework and implementation. This is
not really attractive for the SC community.

- They need to provide advantages over related work on high productive
programming languages mentioned in Section 7 such as X10, UPC, etc.

# Review of pap326s2 by Reviewer 3

## Summary and High Level Discussion

The paper details a high level language and compiler (REGENT)
targeting the Legion runtime system.  The advantage of REGENT over
hand coding in Legion is that in Legion the developer has to know
about both the logical and physical levels but REGENT only exposes the
logical level making development and porting simpler. The paper covers
some of the different optimizations implemented in REGENT and there
affect on performance/scalability of 3 different
benchmarks/mini-apps. In the results they turn on and off the
optimizations to show their affect. The paper compares the performance
of the benchmarks to a baseline which was either Legion hand coded,
OpenMP, or MPI+Kokkos.

## Comments for Rebuttal

Please fix fig. 4-6 to make the baseline clearer. Also add MPI and
MPI+OpenMP results for the PENNANT example so one can see scaling
results for more optimal configuration.

## Detailed Comments for Authors

The paper is well written with details on the different
optimizations. The results breakdown the affects of the different
optimizations and do a good job to explain why the affect on
performance was the way it was. The comparison to LOC of the 3
benchmarks to the reference was a good way to understand some of the
added benefit of the high-level language. It was difficult to read the
graphs due to so many different curves. For example in figure 4 the
baseline and the multiple overlapping curves across the bottom look
similar. Meaning the overlapping make it look like the baseline is on
the bottom. Making the baseline stand out more by maybe having a
thicker solid line or a triangle symbol which is different than all
other figures would be better. In addition adding in the MPI-only and
MPI+OpenMP results for the PENNANT data would make this stronger. It
is clear that just OpenMP isn't scaling well for that dataset. Showing
the MPI and MPI+OpenMP where the number of OpenMP and MPI tasks is set
optimally for the input would be a better baseline to compare against.

# Review of pap326s2 by Reviewer 4

## Summary andHigh Level Discussion

This paper presents Regent, a new task-based parallel programming
language. Regent is created to simplify programming for the Legion
runtime system, which was also previously proposed by the
authors. Legion is a task-parallel runtime with explicit control over
memory hierarchy. While it's a powerful runtime system, its
library-based programming interface is rather complicated and
difficult to program with. Regent addresses the problem of programming
complexity for the Legion runtime system by providing much
higher-level of abstractions, and it is designed such that the cost of
abstractions can be virtually eliminated by static compilation. The
experimental results show promising performance results with up to 80
CPUs.

Strengths:

- The proposed new language allows for much simpler programming for an
advanced task-parallel runtime such as Legion.

Weaknesses:

- The evaluation does not directly assess the major questions on the
design and implementation of Regent, namely productivity and
performance when compared to the Legion C++ interface since some of
the reference versions are indeed written in other programming
languages and libraries, so that's not apples-to-apples comparison.
Although the current comparison is itself important, it does not
directly answer whether Regent can achieve comparable performance as
those written in the C++ library interface.

- The paper does not discuss accelerator offloading, although the
underlying Legion seems to support CUDA. The basic mechanisms
presented in this paper seems to be applicable when CUDA is used too,
but there could be other performance issues that are unique to
accelerator usage.

## Comments for Rebuttal

How does Regent support accelerators?

In PENNANT, the vectorization does not seem to have much performance
impact. Is it technically impossible to use vector instructions for
the code? Or is it due to the compiler for the reference code? What
does the performance look like if other compilers such as the Intel
compiler are used?

## Detailed Comments for Authors

As mentioned above, the biggest concern on this paper is that the
paper seems to fail to demonstrate its main claim, i.e., "Several
novel optimizations allow the Regent compiler to achieve performance
equivalent to hand-tuned codes written directly to the Legion C++
API."

Another concern is that the performance evaluation is done on a
relatively old system (an Infiniband cluster with Xeon X5650). It is
also fairly small, just up to 80 CPUs. No evaluation is done using
accelerators, although Legion supports CUDA.

Minor comments:

In Figure 5, the same symbol appears to be used for both "baseline"
and "none".

Some of the optimizations do not seem to have much performance impact.
The paper mentions that dynamic branch elision is not effective and so
not shown in the graphs for the Circuit and MiniAero cases. Is it also
the case for PENNANT? Its graph (Figure 5) does not show results when
dynamic branch elision is moved out, so I got an impression that it
made much difference, but unlike the other two cases the paper does
not explicitly say whether it was effective or not. Also, the future
optimization does not seem to make much difference in performance
either. Whether hiding the performance results showing the
effectiveness of those optimizations is rather subjective, but it
would be more important to analyze why those were not effective and
discuss what cases would be necessary.
