#include "legion.h"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;
using std::max;

typedef AccessorType::SOA<0> SOA;

template<typename ET>
using Accessor = RegionAccessor<SOA, ET>;

enum {
  ADV_POS_FULL,
  SIMULATE,
};

enum {
  PX0_X,
  PX0_Y,
  PX_X,
  PX_Y,
  PU0_X,
  PU0_Y,
  PU_X,
  PU_Y,
  PF_X,
  PF_Y,
  PMASWT,
};

template<typename ET>
inline RegionAccessor<SOA, ET> get_accessor(const PhysicalRegion &region, FieldID fid) 
{
  return region.get_field_accessor(fid).template typeify<ET>().template convert<SOA>();
}

void adv_pos_full(const Task *task,
                  const std::vector<PhysicalRegion> &regions,
                  Context ctx, HighLevelRuntime *runtime)
{
  PhysicalRegion rp0 = regions[0];
  Accessor<double> rp_px0_x = get_accessor<double>(rp0, PX0_X);
  Accessor<double> rp_px0_y = get_accessor<double>(rp0, PX0_Y);
  Accessor<double> rp_pu0_x = get_accessor<double>(rp0, PU0_X);
  Accessor<double> rp_pu0_y = get_accessor<double>(rp0, PU0_Y);
  Accessor<double> rp_pf_x = get_accessor<double>(rp0, PF_X);
  Accessor<double> rp_pf_y = get_accessor<double>(rp0, PF_Y);
  Accessor<double> rp_pmaswt = get_accessor<double>(rp0, PMASWT);
  PhysicalRegion rp1 = regions[1];
  Accessor<double> rp_px_x = get_accessor<double>(rp1, PX_X);
  Accessor<double> rp_px_y = get_accessor<double>(rp1, PX_Y);
  Accessor<double> rp_pu_x = get_accessor<double>(rp1, PU_X);
  Accessor<double> rp_pu_y = get_accessor<double>(rp1, PU_Y);

  Future f0 = task->futures[0];
  double dt = f0.get_result<double>();

  double fuzz = 1e-99;
  double dth = 0.5 * dt;

  IndexIterator it(rp0.get_logical_region().get_index_space());
  while (it.has_next()) {
    size_t count;
    ptr_t start = it.next_span(count);
    ptr_t end(start.value + count);
    for (ptr_t p = start; p < end; p++) {
      double frac = (1.0 / max(rp_pmaswt.read(p), fuzz));
      double pap_x = frac * rp_pf_x.read(p);
      double pap_y = frac * rp_pf_y.read(p);
      double pu_x = rp_pu0_x.read(p) + dt * pap_x;
      double pu_y = rp_pu0_y.read(p) + dt * pap_y;
      rp_pu_x.write(p, pu_x);
      rp_pu_y.write(p, pu_y);
      rp_px_x.write(p, rp_px0_x.read(p) + dth*(pu_x + rp_pu0_x.read(p)));
      rp_px_y.write(p, rp_px0_y.read(p) + dth*(pu_y + rp_pu0_y.read(p)));
    }
  }
}

struct config {
  int npieces;
};

void simulate(const Task *task,
              const std::vector<PhysicalRegion> &regions,
              Context ctx, HighLevelRuntime *runtime)
{
  config conf;
  LogicalRegion rp_all_private;
  PhysicalRegion pr_rp_all_private;
  LogicalPartition rp_all_private_p;
  Future dt;

  runtime->unmap_region(ctx, pr_rp_all_private);
  Domain domain = Domain::from_rect<1>(
    Rect<1>(Point<1>(0), Point<1>(conf.npieces - 1)));
  IndexLauncher launcher(ADV_POS_FULL, domain,
                         TaskArgument(), ArgumentMap());

  launcher.add_region_requirement(
    RegionRequirement(rp_all_private_p, 0 /* projection */,
                      READ_ONLY, EXCLUSIVE, rp_all_private));
  launcher.add_field(0, PX0_X);
  launcher.add_field(0, PX0_Y);
  launcher.add_field(0, PU0_X);
  launcher.add_field(0, PU0_Y);
  launcher.add_field(0, PF_X);
  launcher.add_field(0, PF_Y);
  launcher.add_field(0, PMASWT);

  launcher.add_region_requirement(
    RegionRequirement(rp_all_private_p, 0 /* projection */,
                      READ_WRITE, EXCLUSIVE, rp_all_private));
  launcher.add_field(1, PX_X);
  launcher.add_field(1, PX_Y);
  launcher.add_field(1, PU_X);
  launcher.add_field(1, PU_Y);

  launcher.add_future(dt);

  runtime->execute_index_space(ctx, launcher);
}

int main()
{
  return 0;
}
