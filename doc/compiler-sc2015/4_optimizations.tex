\section{Optimizations}
\label{sec:optimizations}

As illustrated in Section~\ref{sec:progmodel}, Regent simplifies the
Legion programming model and provides a higher level of abstraction
that is concerned only with logical, rather than physical,
constructs. The Regent compiler is able to manage the correspondences
between logical and physical constructs in a way that achieves
significantly better
performance than a naive implementation.  This section describes a
number of optimizations that together allow the Regent compiler to
achieve performance comparable to hand-tuned code written to the
Legion C++ API.

\subsection{Mapping Elision}
\label{subsec:inline}

% FIXME (Alex): Can you more precisely define in what sense the Regent
% analysis is optimal?  Are the spans where regions are unmapped
% statically maximized?

Regent frees programmers of the burden of managing physical instances
of regions by statically computing correct and optimal placements of
map and unmap calls. The Regent type system guarantees that the
compiler has complete information about what regions can be accessed
within any task. The compiler uses this information to perform a
flow-sensitive analysis over the AST to determine the spans over which
regions are used and inserts the map and unmap calls at the boundaries
of spans when switching between usage in a parent and a child task.
In the case where a region is not used at all within a task, the
compiler issues a single unmap call at the top of the task and leaves
the region unmapped for the entire duration of the task's
execution. In contrast, the Legion runtime, in the absence of manually
placed calls to map and unmap, is forced to continue to map and unmap
the region throughout the task's execution.

\subsection{Leaf Tasks}
\label{subsec:leaf}

As discussed in Section~\ref{subsubsec:stalls}, correctly identifying
leaf tasks is an important optimization for Legion programs, as
otherwise the Legion runtime must consider the mapping of a task still
in progress until it can be certain all child tasks have mapped.  Regent
automatically infers at compile time which tasks are leaf tasks.  The
compiler knows all call targets and is therefore able to determine,
using a flow-insensitive analysis, whether a given task calls any
subtasks. These annotations are guaranteed to be correct and precise,
in contrast to the user-provided leaf task annotations in Legion.

\subsection{Index Launches}
\label{subsec:launch}

Whenever possible, the Regent compiler transforms loops of task
launches into index space task launches. The analysis for this optimization
proceeds in multiple phases:

\begin{enumerate}
\item The compiler begins with a structural analysis of the code to
  determine whether the loops in question are eligible for
  transformation into an index space launch. Currently all simple
  loops containing single task launches are considered eligible.
\item For each loop, the compiler determines whether the body of
  the loop (aside from the task call itself) is side-effect free. In
  particular, the loop body must not read or modify data that the
  task itself might read or modify. Doing so would introduces a loop-carried
  dependence and shows the loop is not fully parallelizable.
\item For each argument to the task launch, the compiler 
  determines whether the argument in question is eligible to be
  transformed into an argument for an index task launch. Arguments
  must be one of:
  \begin{itemize}
  \item a non-region value;
  \item a region value that is provably loop invariant;
  \item a region value that is provably an analyzable function of the loop index;
    i.e., it is an expression such as a partition access $p[i]$
    indexed by the loop variable $i$.
  \end{itemize}
\item The compiler then performs a static variant of Legion's dynamic
  non-interference analysis. For each region-typed
  argument, the compiler determines
  whether it is statically non-interfering with other region-typed
  arguments. As with the dynamic analysis, the compiler has several
  dimensions along which to prove non-interference:
  \begin{itemize}
  \item disjointness, either because the region types are incompatible, or
    because the compiler can statically prove disjointness through the
    static tree of region partitions;
  \item field disjointness, because the arguments use different fields; or
  \item privileges, because both arguments use compatible privileges
    (e.g.\ both read-only, or both reductions with the same reduction
    operator).
  \end{itemize}
%  These checks must be performed for each region-typed argument
%  against previous region-typed arguments, and also for each
%  region-typed argument against itself (conceptually, the same
%  argument in different iterations).
\end{enumerate}

If the analysis determines that a task launch is eligible for
optimization, the compiler emits the code to perform the index task
launch.

It is worth noting that while this optimization looks similar in
principle to forall-style constructs in other languages and
programming models, it behaves quite differently in many
respects. In particular, when index launch optimization fails (because
any of the properties above cannot be established), that does not imply
the resulting code runs sequentially. The Legion runtime will
perform its standard dynamic analysis, and will
parallelize all tasks that are dynamically non-interfering,
regardless of whether the compiler performs the optimization or
not. This optimization simply allows the runtime to amortize the
dynamic analysis costs in cases where the loops can be analyzed
statically. Thus, Regent has a much more forgiving fallback
for when static analysis is insufficient than language implementations
that rely solely on static analysis.


\subsection{Futures}
\label{subsec:futures}

In Legion, tasks can return futures, which can be passed to other tasks
without blocking, allowing applications to build chains of
asynchronous operations ahead of the actual computation.
The Regent compiler can automatically lift variables
and simple operations to futures to take advantage of these
benefits. This optimization has three phases:
\begin{itemize}

\item The compiler first performs a flow-insensitive analysis to
determine which variables are assigned to futures at any point within
each task. Any such variables are automatically promoted to hold
futures.

\item The compiler then issues calls to automatically wrap and unwrap
futures when storing a concrete value into a future-typed variable, or
when reading a future-typed value because a concrete value is
required. Tasks do not require arguments to be
concrete, and can therefore be issued in advance of when the concrete
arguments are ready.

\item Finally, the compiler emits tasks to allow simple side-effect free
operations (such as arithmetic) to be performed directly on futures.
\end{itemize}

\subsection{Pointer Checks Elision}
\label{subsec:pointer}

As noted in \cite{LegionTypes13}, static type checking of Legion
programs allows certain classes of pointer checks to be elided. Regent
preserves all the properties of the type system which make this
possible. In particular, all pointer types in Regent explicitly
contain one or more regions that they point into. Regent checks
these annotations to ensure correctness at compile time, and elides
the dynamic pointer checks, which are often prohibitively expensive at
runtime.

\subsection{Dynamic Branch Elision}
\label{subsec:branch}

In addition, Regent is able to elide certain classes of dynamic
branches when accessing pointers in Legion. Pointers that can point
into multiple different regions (e.g., private or ghost points in PENNANT) carry
some dynamic tag bits encoding the region the pointer currently points to. In some
cases, however, the memory for the two regions is actually co-located
at runtime (e.g.\ because of a decision to map both
regions to the same memory), allowing the dynamic branches on the tag
bits to be elided. The compiler emits code that
automatically detects such cases at runtime and selects the fast path
when it is available.

\subsection{Vectorization}
\label{subsec:vectorization}

Regent leaf tasks frequently feature loops over regions. In many
cases, the Regent compiler is able to vectorize these loops
automatically, often exceeding performance provided by traditional
autovectorizers.

Regent performs runtime code generation to LLVM~\cite{LLVM} via
Terra~\cite{Terra13}. While LLVM provides an autovectorizer, the low
level of abstraction of the LLVM IR means that the vectorizer
frequently misses vectorization opportunities or chooses the wrong
optimization strategies for its vector code. Regent's native
understanding of regions allows the vectorizer to make these decisions
with improved precision. Regent uses Terra's built-in vector types to
produce explicit vector instructions for LLVM, resulting in
significant performance gains in many cases.

Regent derives this advantage in precision from two sources. First,
Regent has improved information about aliasing through type, field,
and region-based analysis. In particular:

\begin{itemize}
\item While accesses for composite types are ultimately expressed as
  array accesses to fundamental types (integers, double-precision
  floating point, etc.), Regent is able to compare the original
  types to determine if there is potential for aliasing.
\item Furthermore, even for identical types, Regent knows which
  fields are accessed and may be able to use this information to
  prove independence.
\item Finally, when two accesses are to different regions,
  Regent may be able to use its knowledge of region disjointness to
  prove that accesses are independent.
\end{itemize}

Beyond this, Regent has access to implicit information about the costs
of potential vectorization opportunities through regions. Regions are
hierarchical and distributed data structures intended to provide
opportunities for parallelism. Therefore, when Regent sees an outer
loop over a region, and an inner loop (over something other than a
region), Regent can infer with high confidence that the outer
loop is the better opportunity for vectorization. In some cases,
largely because it lacks comparable information for its cost model, LLVM chooses to
vectorize the inner rather than outer loop, resulting in degraded
performance.
