#!/usr/bin/env bash

TOTAL_CELLS=1166400 # 1080 * 1080
function extract_results {

  local num_cycles=30
  local index=$1
  local future=$2
  local inline=$3
  local leaf=$4
  local vectorize=$5
  local branch=$6

  DATA_FILE="singlenode/PENNANT_DATA_I$index""_F$future""_N$inline""_L$leaf""_V$vectorize""_D$branch"
  echo "Threads TaskTime MZonesPerSecond" > "$DATA_FILE"

  for cpu in 1 2 4 6 8 10 12 24
  do
    LOG_FILE="regent_new/node_1_cpu_$cpu""_I$index""_F$future""_N$inline""_L$leaf""_V$vectorize""_D$branch"
    echo $LOG_FILE
    TIME=`grep Elapsed "$LOG_FILE"_* | awk '{print $4}' | ./avg.rb`
    ZPS=`ruby -e "print $TOTAL_CELLS*$num_cycles/1000000.0/$TIME"`
    if [ $TIME != "NaN" ]; then
      echo "$cpu $TIME $ZPS" >> "$DATA_FILE"
    fi

  done
}

function extract_results_baseline {

  local num_cycles=30
  DATA_FILE="singlenode/PENNANT_DATA_BASELINE"
  echo "Threads TaskTime MZonesPerSecond" > "$DATA_FILE"

  for cpu in 1 2 4 6 8 10 12 24
  do
    LOG_FILE="baseline_new/node_1_cpu_$cpu""_cpp"
    echo $LOG_FILE
    TIME=`grep "run time" "$LOG_FILE" | awk '{print $5}' | ./avg.rb`
    ZPS=`ruby -e "print $TOTAL_CELLS*$num_cycles/1000000.0/$TIME"`
    if [ $TIME != "NaN" ]; then
      echo "$cpu $TIME $ZPS" >> "$DATA_FILE"
    fi

  done
}

#index future inline leaf vectorize
# for config in 2#11111 2#01111 2#11011 2#11110 2#01011 2#00000 2#10000 2#10100 2#00001 2#00011 2#00100 2#00101
for config in 2#111111 2#101111 2#011111 2#110111 2#111011 2#010111 2#000000 2#011011 2#111101 2#111110 2#001111 2#110011
do
  branch=$((config >> 0 & 1))
  vectorize=$((config >> 1 & 1))
  leaf=$((config >> 2 & 1))
  inline=$((config >> 3 & 1))
  future=$((config >> 4 & 1))
  index=$((config >> 5 & 1))

  extract_results $index $future $inline $leaf $vectorize $branch
done
extract_results_baseline
