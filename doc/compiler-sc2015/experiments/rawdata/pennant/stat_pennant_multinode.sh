#!/usr/bin/env bash

TOTAL_CELLS=1166400 # 1080 * 1080
function extract_results {

  local num_cycles=29
  local index=$1
  local future=$2
  local inline=$3
  local leaf=$4
  local vectorize=$5

  DATA_FILE="multinode/PENNANT_DATA_I$index""_F$future""_N$inline""_L$leaf""_V$vectorize"
  echo "Nodes TaskTime MZonesPerSecond" > "$DATA_FILE"

  for n in 1 2 4 #8 16
  do
    if [ $n -eq 1 ]; then
      cpus="1 2 4 6 8 10 12 24"
    else
      cpus="10 12 24"
    fi

    local MAX_ZPS=0.0
    local MIN_TIME=0.0
    for cpu in $cpus
    do
      LOG_FILE="regent_new/node_$n""_cpu_$cpu""_I$index""_F$future""_N$inline""_L$leaf""_V$vectorize"
      echo $LOG_FILE
      ELAPSED_TIMES=`grep Elapsed "$LOG_FILE"_* 2>/dev/null`
      if [ $? -eq 0 ]; then
        AVG_TIME=`echo $ELAPSED_TIMES | awk '{print $4}' | ./avg.rb`
        ZPS=`ruby -e "print $TOTAL_CELLS*$num_cycles/1000000.0/$AVG_TIME"`

        if [ `echo "$ZPS > $MAX_ZPS" | bc` == 1 ]; then
          MAX_ZPS=$ZPS
          MIN_TIME=$AVG_TIME
        fi
      fi
    done
    echo "$n $MIN_TIME $MAX_ZPS" >> "$DATA_FILE"
  done
}

function extract_results_baseline {

  local num_cycles=29
  DATA_FILE="multinode/PENNANT_DATA_BASELINE"
  echo "Nodes TaskTime MZonesPerSecond" > "$DATA_FILE"

  for n in 1 2 4 8 16
  do
    if [ $n -eq 1 ]; then
      cpus="1 2 4 6 8 10 12 24"
    else
      cpus="10 12 24"
    fi

    local MAX_ZPS=0.0
    local MIN_TIME=0.0
    for cpu in $cpus
    do
      LOG_FILE="baseline_new/node_$n""_cpu_$cpu""_cpp"
      echo $LOG_FILE
      ELAPSED_TIMES=`grep "run time" "$LOG_FILE" 2>/dev/null`
      if [ $? -eq 0 ]; then
        AVG_TIME=`echo $ELAPSED_TIMES | awk '{print $5}' | ./avg.rb`
        ZPS=`ruby -e "print $TOTAL_CELLS*$num_cycles/1000000.0/$AVG_TIME"`

        if [ `echo "$ZPS > $MAX_ZPS" | bc` == 1 ]; then
          MAX_ZPS=$ZPS
          MIN_TIME=$AVG_TIME
        fi
      fi
    done
    echo "$n $MIN_TIME $MAX_ZPS" >> "$DATA_FILE"
  done
}

#index future inline leaf vectorize
# for config in 2#11111 2#01111 2#11011 2#11110 2#01011 2#00000 2#10000 2#10100 2#00001 2#00011 2#00100 2#00101
for config in 2#11111 2#10111 2#01111 2#11011 2#11101 2#01011 2#00000 2#01101 2#11110
do
  vectorize=$((config >> 0 & 1))
  leaf=$((config >> 1 & 1))
  inline=$((config >> 2 & 1))
  future=$((config >> 3 & 1))
  index=$((config >> 4 & 1))

  extract_results $index $future $inline $leaf $vectorize
done
extract_results_baseline
