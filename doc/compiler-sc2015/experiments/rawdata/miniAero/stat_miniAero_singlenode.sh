#!/usr/bin/env bash

TOTAL_CELLS=4194304
function extract_results {

  local num_iterations=3
  local index=$1
  local future=$2
  local inline=$3
  local leaf=$4
  local vectorize=$5

  DATA_FILE="singlenode/MINIAERO_DATA_I$index""_F$future""_N$inline""_L$leaf""_V$vectorize"
  echo "Cpus TaskTime TotalTime MCellsPerSecond" > "$DATA_FILE"

  for cpu in 1 2 4 6 8 10 12
  do
    LOG_FILE="regent/node_1_cpu_$cpu""_I$index""_F$future""_N$inline""_L$leaf""_V$vectorize"
    echo $LOG_FILE
    TIME=`grep Elapsed "$LOG_FILE"_* | awk '{print $4}' | ./avg.rb`
    SETUP_TIME=`grep Setup "$LOG_FILE"_* | awk '{print $4}' | ./avg.rb`
    TOTAL_TIME=`ruby -e "print $TIME+$SETUP_TIME"`
    CPS=`ruby -e "print $TOTAL_CELLS*$num_iterations/1000000.0/$TIME"`
    if [ $TIME != "NaN" ]; then
      echo "$cpu $TIME $TOTAL_TIME" "$CPS" >> "$DATA_FILE"
    fi
  done
}

function extract_results_kokkos {

  local num_iterations=3
  DATA_FILE="singlenode/MINIAERO_DATA_BASELINE"
  echo "Cpus TaskTime TotalTime MCellsPerSecond" > "$DATA_FILE"

  for cpu in 1 2 4 6 8 10 12
  do
    NUM_CPUS=$((n * cpu))

    LOG_FILE="baseline/node_1_cpu_$cpu""_kokkos"
    echo $LOG_FILE
    TIME=`grep Device "$LOG_FILE" | awk '{print $5}' | ./avg.rb`
    TOTAL_TIME=`grep 'Total elapsed' "$LOG_FILE" | awk '{print $5}' | ./avg.rb`
    CPS=`ruby -e "print $TOTAL_CELLS*$num_iterations/1000000.0/$TIME"`
    if [ $TIME != "NaN" ]; then
      echo "$cpu $TIME $TOTAL_TIME" "$CPS" >> "$DATA_FILE"
    fi

  done
}

#index future inline leaf vectorize
# for config in 2#11111 2#01111 2#11011 2#11110 2#01011 2#00000 2#10000 2#10100 2#00001 2#00011 2#00100 2#00101
for config in 2#11111 2#01111 2#11011 2#11101 2#01011 2#00000 2#01101 2#10111 2#11110
do
  vectorize=$((config >> 0 & 1))
  leaf=$((config >> 1 & 1))
  inline=$((config >> 2 & 1))
  future=$((config >> 3 & 1))
  index=$((config >> 4 & 1))

  extract_results $index $future $inline $leaf $vectorize
done
extract_results_kokkos
