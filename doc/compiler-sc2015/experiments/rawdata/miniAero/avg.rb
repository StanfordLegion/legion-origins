#!/usr/bin/env ruby

file = STDIN

sum = 0.0
cnt = 0
file.each do |line|
    sum = sum + line.to_f
    cnt = cnt + 1
end
puts "#{sum / cnt}"

