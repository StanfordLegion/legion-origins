#!/usr/bin/env bash

function extract_results {

  local index=$1
  local future=$2
  local inline=$3
  local leaf=$4
  local vectorize=$5

  DATA_FILE="DATA_I$index""_F$future""_N$inline""_L$leaf""_V$vectorize"
  echo "Cpus Time Gflops" > "$DATA_FILE"

  for n in 1 2 4 8
  do

    if [ $n -eq 1 ]; then
      cpus="1 2 4 6 8 10"
    else
      cpus="10"
    fi

    for cpu in $cpus
    do
      NUM_CPUS=$((n * cpu))

      LOG_FILE="regent_new/node_$n""_cpu_$cpu""_I$index""_F$future""_N$inline""_L$leaf""_V$vectorize"
      echo $LOG_FILE
      TIME=`grep ELAPSED "$LOG_FILE"_* | awk '{print $4}' | ./avg.rb`
      GFLOPS=`grep GFLOPS "$LOG_FILE"_* | awk '{print $3}' | ./avg.rb`
      if [ $TIME != "NaN" ]; then
        echo "$NUM_CPUS $TIME $GFLOPS" >> "$DATA_FILE"
      fi

    done
  done
}

function extract_results_cpp {

  DATA_FILE="DATA_BASELINE"
  echo "Cpus Time Gflops" > "$DATA_FILE"

  for n in 1 2 4 8
  do

    if [ $n -eq 1 ]; then
      cpus="1 2 4 6 8 10"
    else
      cpus="10"
    fi

    for cpu in $cpus
    do
      NUM_CPUS=$((n * cpu))

      LOG_FILE="baseline_new/node_$n""_cpu_$cpu""_cpp"
      echo $LOG_FILE
      TIME=`grep ELAPSED "$LOG_FILE" | awk '{print $4}' | ./avg.rb`
      GFLOPS=`grep GFLOPS "$LOG_FILE" | awk '{print $3}' | ./avg.rb`
      if [ $TIME != "NaN" ]; then
        echo "$NUM_CPUS $TIME $GFLOPS" >> "$DATA_FILE"
      fi

    done
  done
}

#index future inline leaf vectorize
# for config in 2#11111 2#01111 2#11011 2#11110 2#01011 2#00000 2#10000 2#10100 2#00001 2#00011 2#00100 2#00101
for config in 2#11111 2#01111 2#11011 2#11101 2#01011 2#00000 2#01101 2#10111 2#11110
do
  vectorize=$((config >> 0 & 1))
  leaf=$((config >> 1 & 1))
  inline=$((config >> 2 & 1))
  future=$((config >> 3 & 1))
  index=$((config >> 4 & 1))

  extract_results $index $future $inline $leaf $vectorize
done
extract_results_cpp
