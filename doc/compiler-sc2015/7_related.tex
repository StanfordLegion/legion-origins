\section{Related Work}
\label{sec:related}

Legion~\cite{Legion12}, though implemented as a runtime
system, is designed to be a programming model for which many
properties could in principle be statically checked. In particular,
there is a static type system for key Legion abstractions~\cite{LegionTypes13}. 
Regent extends that work into a
complete language and compiler, with a focus on productivity and
performance. Among other things, partitions in Regent are first-class
and can be passed to subtasks, facilitating certain interesting design
patterns impossible in the initial type system.

Legion is itself the spiritual successor of
Sequoia~\cite{Fatahalian06}. Most notably, Sequoia, in contrast to
Legion, was entirely static: the application, machine specification,
and mapping from application to machine were all given as inputs to
the compiler, which produced an optimized (but entirely static)
executable. Legion grew out of the difficulties encountered in  adapting Sequoia
to irregular, and thus more dynamic,  applications~\cite{BCSA11}. Regent itself differs from the
Sequoia compiler~\cite{Ren08} in that the compiler plays a different
role with respect to the runtime system, facilitating the efficient
operation of the runtime, rather than the other way around. As a
result, the optimization needs of each system differ.

Deterministic Parallel Java
(DPJ)~\cite{Bocchino09,Bocchino11} is a parallel extension to the
Java programming language adding support for regions. Regions in DPJ
also express locality, as they do in Regent. However, DPJ is a fully
static system, while Regent is a hybrid system with a static type
system and compiler optimizations but also an aggressively optimizing
dynamic runtime. As a result, while Regent's semantic safety properties must be
enforced at compile time, Regent is free to discover parallelism at
runtime, giving it the ability to exploit dynamic information about the
application when insufficient static information is available.

An older but related language is Jade~\cite{Rinard98}. Jade provides an
apparently-serial programming model that implicitly parallelizes
through the use of a dynamic dataflow graph. Jade differs
substantially in design and implementation from Legion because of the
characteristics of the hardware at the time, when processors were much
slower relative to networks and it was plausible to track dynamic
dataflow on a per-object basis. Regent addresses these challenges by
aggregating data with logical regions and providing language
constructs for hierarchical decomposition of those regions. Regent's
design also allows it to employ a hierarchical, distributed scheduling algorithm~\cite{LegionTypes13}.

Swift/T~\cite{Wozniak13,Armstrong14} (not to be confused with Apple
Swift) is a more recent effort focusing on high-productivity scripted
parallel workflows. Both Swift/T and Regent employ an implicitly
parallel programming model, and both
achieve parallelism through dataflow. Swift/T differs from Regent in
that it is purely functional, while Regent allows tasks to have
side-effects on regions, but serializes execution when tasks have the
potential to interfere. Unlike regions, Swift/T's aggregate data types
do not have a first-class concept of partitioning, as in
Regent. Beyond this, while both Swift/T and Regent optimize for
parallelism, the Swift/T makes no attempt to generate efficient
sequential code while Regent is able to match the performance of
hand-tuned and manually vectorized kernels.

Other runtime systems with support for task-based programming include
Charm++~\cite{Charmpp}, Uintah~\cite{Uintah}, StarPU~\cite{StarPU11},
and OCR~\cite{OCR14}. While these systems differ substantially in the
details, they all provide a common abstraction of a task as a
fundamental unit of execution, with a graph of dependencies between
tasks providing communication and synchronization. These systems
differ from Regent in that, as dynamic runtime systems, they impose a
small but potentially significant runtime overhead which must be
overcome to achieve performance. Beyond this, these systems
do not provide the ability to partition data multiple ways and to
migrate data dynamically between these views as the application moves
between different phases of computation. This makes several design
patterns more straightforward to implement in Regent compared to the
above systems.

Another related class of work is represented by PGAS
and related models, such as
UPC~\cite{UPC99,UPCSTANDARD}, Titanium~\cite{Tit98}, Chapel~\cite{Chamberlain:Chapel,CHAPEL11},
X10~\cite{X1005,X1008,X1011} and HPX~\cite{Kaiser2014}. Regent
is similar to PGAS programming models in so far as pointers created in
Regent are valid everywhere in the machine. However, Regent pointers
can only be dereferenced when the task in question has privileges to the
region that it points into. This property allows Regent to preserve a number of
other important properties important for performance and correctness, such as guaranteeing
non-interference of tasks executing in parallel. 
%As a result, Regent programs
%are free of explicit synchronization and data movement.
