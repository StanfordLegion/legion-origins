\section{Evaluation}
\label{sec:evaluation}

We evaluate scalability and parallel efficiency of control replication in the
context of Regent with three unstructured mini-apps: a circuit
simulation on a sparse unstructured graph; MiniAero, an explicit
solver of the compressible Navier-Stokes equations on a 3D
unstructured mesh; and PENNANT, a Lagrangian hydrodynamics simulation
on a 2D unstructured mesh.

For each application, we measure weak scaling performance on up to 512
nodes of the Piz Daint supercomputer\cite{PizDaint}, a Cray XC30
system. Each node has an Intel Xeon E5-2670 CPU (8 physical cores) and
32 GB of memory. All applications were compiled with GCC
4.8.2, and Regent used LLVM 3.5.2 for code generation.

\subsection{Circuit}
\label{subsec:circuit}

% Circuit efficiency at 256 nodes:
%   13.088623500000002 / 13.1580345 = 0.9947248200329618
% And at 512 nodes:
%   13.088623500000002 / 13.679076909090911 = 0.9568352884470953

We developed a sparse circuit simulation based on \cite{Legion12} to
measure weak scaling performance on unstructured graphs. The
implicitly parallel version from \cite{Legion12} was already shown to
be substantially communication bound at 32 nodes and would not have
scaled to significantly more nodes, regardless of the implementation
technique. The input for this problem was a randomly generated sparse
graph; the application was otherwise structured identically to the
original.

Figure~\ref{fig:circuit} shows weak scaling performance for the
simulation up to 512 nodes. Control replication achieves 96\%
parallel efficiency at 512 nodes. Regent without control replication matches
this performance up to 16 nodes; however, after this point the
overhead of the dynamic analysis in the Legion runtime begins to
exceed the running time of the application, causing efficiency to drop
rapidly.

\pgfplotsset{
  cycle list={%
    {red, mark=*}, {blue,mark=square*},
    {teal, mark=diamond*}, {orange,mark=triangle*},
  }
}
\begin{figure}[t]
\centering
\pgfplotsset{width=0.5\textwidth,height=7.5cm,compat=1.5.1}
\begin{tikzpicture}
  \small
  \begin{axis}[
    xlabel=Total Nodes,
    xtick={1,2,...,10},
    xticklabels from table={experiments/rawdata/label}{Nodes},
    ylabel=Throughput per node ($10^3$wires/s),
    legend style={draw=none, at={(0.5, 1.1)}, anchor=north, legend columns=2}
    ]
    \addplot
    table[x=X,y=Perf]
    {experiments/rawdata/circuit/DATA_REGENT};
    \addplot
    table[x=X,y=Perf]
    {experiments/rawdata/circuit/DATA_BASELINE};
    \legend{Regent (with CR), Regent (w/o CR)}
  \end{axis}
\end{tikzpicture}
%\includegraphics[scale=0.3]{experiments/circuit.png}
\caption{Weak scaling for Circuit.}
\label{fig:circuit}
\end{figure}

\subsection{MiniAero}
\label{subsec:miniaero}

% MiniAero efficiency at 512 nodes:
%   9.6980843 / 10.439066100000002 = 0.9290183822094965

MiniAero is a 3D unstructured mesh mini-app from Sandia National
Laboratories, implementing an explicit solver for the compressible
Navier-Stokes equations. The mini-app is written in a hybrid style,
using MPI for inter-node communication and Kokkos for intra-node
parallelism. (Kokkos is a portability layer for C++ that compiles
down to OpenMP (on CPUs), also developed at Sandia\cite{Kokkos}.)

In this experiment we compare two distinct Regent programs. One
version uses Regent's implicitly parallel style (which we test with
and without control replication). We also evaluate a second version
that directly uses Regent's support for hand-written SPMD-style programming.

Figure~\ref{fig:miniaero} shows weak scaling absolute performance for
the various implementations of MiniAero. Control replication achieves
93\% parallel efficiency at 512 nodes; as before, Regent without control
replication struggles to scale beyond a modest number of nodes. The
manual SPMD version of the Regent code also scales, though with worse
overall performance. This was due in part to an early design decision
to split ghost regions in the application one per neighbor per submesh
instead of just one per submesh. The automated nature of control
replication allowed us to iterate quickly on the implicit parallel version and
reverse this decision to achieve better final performance with less
overall development effort.

As demonstrated in \cite{Regent15}, Regent-based codes out-perform the
MPI+Kokkos implementation of MiniAero on a single node, mostly by
leveraging the improved hybrid data layout features of
Legion\cite{LegionFields14}.

%% \begin{figure}[t]
%% \centering
%% \includegraphics[scale=0.3]{experiments/miniaero_v_manual.png}
%% \caption{Weak scaling for MiniAero (vs Manual SPMD).}
%% \label{fig:miniaero-v-manual}
%% \end{figure}

\begin{figure}[t]
\centering
\pgfplotsset{width=0.5\textwidth,height=7cm,compat=1.5.1}
\begin{tikzpicture}
  \small
  \begin{axis}[
    xlabel=Total Nodes,
    xtick={1,2,...,10},
    xticklabels from table={experiments/rawdata/label}{Nodes},
    ymin=0,
    ylabel=Throughput per node ($10^3$cells/s),
    legend style={draw=none, at={(0.5, 1.2)}, anchor=north, legend columns=2}
    ]
    \addplot
    table[x=X,y=Perf]
    {experiments/rawdata/miniaero/DATA_REGENT};
    \addplot
    table[x=X,y=Perf]
    {experiments/rawdata/miniaero/DATA_REGENT_NO_SPMD};
    \addplot
    table[x=X,y=Perf]
    {experiments/rawdata/miniaero/DATA_REGENT_EXPLICIT};
    \addplot
    table[x=X,y=Perf]
    {experiments/rawdata/miniaero/DATA_BASELINE};
    \legend{Regent (with CR), Regent (w/o CR),
      Regent (manual SPMD), MPI+Kokkos}
  \end{axis}
\end{tikzpicture}
%\includegraphics[scale=0.3]{experiments/miniaero_v_kokkos.png}
\caption{Weak scaling for MiniAero.}
\label{fig:miniaero}
\end{figure}

\subsection{PENNANT}
\label{subsec:pennant}

% PENNANT efficiency at 128 nodes:
%   6.7707223999999995 / 7.1951036 = 0.9410180556677459
% And at 512 nodes:
%   6.7707223999999995 / 8.854033833333334 = 0.7647048257834568

PENNANT is a 2D unstructured mesh proxy application from Los Alamos National
Laboratory simulating Lagrangian hydrodynamics\cite{PENNANT},
implemented in MPI+OpenMP. The application represents a subset of the
functionality that exists in FLAG, a larger production code used at
the lab\cite{FLAG}.

Figure~\ref{fig:pennant} shows weak scaling performance for PENNANT on
up to 512 nodes. Control replication maintains 94\% parallel efficiency up to
128 nodes; after this efficiency begins to drop as the Legion
runtime's implementation of dynamic collectives (used in control
replication to implement scalar reductions) begin to take more time in
comparison to the MPI collective implementation. Without control
replication, the overhead of the implicitly parallel version very
quickly exceeds the short running time of the individual PENNANT
tasks.

As noted in \cite{Regent15}, Regent's single-node performance on
PENNANT matches that of MPI+OpenMP when applying a similar number of
cores to application-level tasks. However, the Legion runtime requires
one core to perform runtime analysis on tasks, and this prevents
Regent from fully utilizing the machine for compute-bound
applications. In these experiments, Regent runs with 7 application
cores per node, with 1 core dedicated to the Legion runtime;
MPI+OpenMP achieves its best performance when using 16 hyperthreads on
all 8 physical cores.

\begin{figure}[t]
\centering
\pgfplotsset{width=0.5\textwidth,height=7cm,compat=1.5.1}
\begin{tikzpicture}
  \small
  \begin{axis}[
    xlabel=Total Nodes,
    xtick={1,2,...,10},
    xticklabels from table={experiments/rawdata/label}{Nodes},
    ylabel=Throughput per node ($10^6$zones/s),
    ymin=0,
    legend style={draw=none, at={(0.5, 1.2)}, anchor=north, legend columns=2}
    ]
    \addplot
    table[x=X,y=Perf]
    {experiments/rawdata/pennant/DATA_REGENT};
    \addplot
    table[x=X,y=Perf]
    {experiments/rawdata/pennant/DATA_REGENT_NO_SPMD};
    \addplot
    table[x=X,y=Perf]
    {experiments/rawdata/pennant/DATA_BASELINE_7};
    \addplot
    table[x=X,y=Perf]
    {experiments/rawdata/pennant/DATA_BASELINE};
    \legend{Regent (with CR), Regent (w/o CR),
      MPI+OpenMP (7T), MPI+OpenMP (16T)}
  \end{axis}
\end{tikzpicture}
%\includegraphics[scale=0.3]{experiments/pennant.png}
\caption{Weak scaling for PENNANT.}
\label{fig:pennant}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "spmd"
%%% End:
