\section{Program Transformation}
\label{sec:replication}

The program can now be transformed to produce \emph{shards} of control
to be executed independently in SPMD fashion. The transformation
replaces the explicit gather and scatter nodes in the RDIR
graph with copy nodes (along with the appropriate
synchronization). This is sufficient to satisfy the implicitly
parallel semantics of the program. However, in order to construct an
efficient SPMD program, care must be taken to ensure that the copies
are not only sufficient, but precise. In particular, it is not at all
desirable to collect the data on a single node, as this creates both a
sequential bottleneck and limits the problem sizes that can
effectively run.

Instead, we perform a dynamic analysis, comparing regions for overlap
at runtime. This has two advantages: first, the program is able to
(dynamically) discover a precise communication graph for the
application, so that nodes are only required to communicate with other
nodes that hold relevant pieces of data. And second, this dynamic
analysis identifies precise overlaps for regions, minimizing the data
that must actually be copied between those nodes.

Figure~\ref{fig:rdir-replicated} shows the results of the control
replication on the outer loop of the application. This graph now
represents the code that will be executed by each shard in the SPMD
execution of the program. Notably, the control flow in the program has
been largely kept intact, although the bounds on the inner loops have
changed. In particular, the inner loops now execute only the subset of
iterations they are responsible for on a particular node.

%% Control replication can be applied to any tree where the leaves
%% (innermost loops in the nested control flow) are loops with analyzable
%% bounds, and the loops in question have no loop-carried
%% dependencies. For simplicity of implementation, we also require the
%% bounds of all innermost loops to be identical.

%% Operations outside of innermost loops are permitted to have
%% side-effects, such as incrementing a time-step variable or computing
%% the next value of $dt$, but are not permitted to access regions or
%% partitions. Applications which require, for example, access to mesh
%% parameters in order to compute parameters for the next time step, can
%% do so by reducing values into scalar variables, which will be
%% automatically broadcast to all shards.

\subsection{Insert Copies}
\label{subsec:copies}

Following the conversion to RDIR format, the graph contains gather and
scatter nodes wherever communication is implied in the original
program (as shown in Figure~\ref{fig:rdir-complete}. These nodes must
be replaced with explicit communication and synchronization in order
to allow SPMD execution.

To generate the necessary communication, the compiler replaces each
gather node with one or more explicit copy nodes, and elides the
scatter node and intermediate data node. In the figure above, for
example, the compiler inserts a single copy node from \emph{own.b}
to \emph{ghost.b}.

Care must be taken to ensure that the privileges of the source node of
the copy are obeyed properly. Reductions, in particular, may be
applied to aliased regions in parallel, and must be properly
accumulated before the results can be read.

Copies are issued between pairs of source and destination regions, but
only the \emph{intersections} of the regions must actually be
copied. The number, size and extent of such intersections are unknown
at compile time; this is an aspect of the analysis that is deferred
to runtime. The compiler is able to ensure, however, that the dynamic
intersection tests are amortized over the execution of the
program. The analysis of region intersections is addressed in
Section~\ref{subsec:intersections}.

\subsection{Insert Synchronization}
\label{subsec:sync}

The copy operations introduced by control replication are always
issued by the shard which produced the data. Therefore, on the
producer's side only, copies follow the normal semantics of data
dependencies implied by the RDIR format. Additional synchronization is
required to ensure that copies synchronize correctly with the
consumer.

Each copy involves two points of synchronization. A copy must not
start until the previous consumer of the data has completed (to avoid
overwriting data currently in use); this synchronization is indicated
by the \emph{empty} (\emph{in} and \emph{out}) nodes above. Similarly,
a copy must finish before the consumer of the data can begin, this is
synchronized via the \emph{full} (\emph{in} and \emph{out})
nodes. Because each shard is potentially both a producer (of data
consumed by other shards) and a consumer (of data produced by other
shards), each shard synchronizes four times: twice on the producer
side (\emph{out}) and twice on the consumer side (\emph{in}).

Synchronization is point-to-point, and is performed without blocking
the main thread. Two new kinds edges in
Figure~\ref{fig:rdir-replicated} mark the synchronization points where
operations either wait for a synchronizing event to occur (\emph{wait}
edges), or trigger the synchronization in question (\emph{signal}
edges).

\subsection{Dynamic Analysis of Intersections}
\label{subsec:intersections}

The precise intersections of copied regions cannot be known until
runtime. Control replication resolves this issue by delaying this
analysis until runtime, when the intersections can be precisely
computed.

While issuing copies, the compiler records pairs of source and
destination regions that require copies. For example, \emph{own}
and \emph{ghost} is such a pair in
Figure~\ref{fig:rdir-replicated}. The data that must be copied from
\emph{own} to \emph{ghost} corresponds to the set of intersections
between all pairs of subregions of the two partitions.

To avoid an $O(N^2)$ complexity in the computation of intersections,
we apply a two-stage approach. First, the main thread (before
launching the SPMD shard tasks) computes a shallow intersection to
determine which pairs of regions overlap (but not the extent of the
overlap). An interval tree acceleration data structure makes this
operation $O(N \operatorname{log} N)$. Second, each shard begins its
execution by computing a complete intersection between these
known-intersecting regions. This operation is $O(N^2)$ but with $N$ on
the order of the number of nodes each node needs to communicate with,
as opposed to the total number of nodes in the launch.

In practice, at 512 nodes, we have observed that the shallow
intersection operation takes at most 250ms to run, and the complete
intersection takes at most 20ms on each node. Both operations
frequently require much less time.

% At 512 nodes:
%%   * circuit
%%     list_cross_product: 84 ms
%%     list_cross_product_complete: 0.4 ms
%%   * miniaero
%%     list_cross_product: 254 ms
%%     list_cross_product_complete: 21 ms
%%   * pennant
%%     list_cross_product: 78 ms
%%     list_cross_product_complete: 0.2 ms

%% \subsection{Duplicate Aliased Partitions}
%% \label{subsec:dup}

%% Regions may be partition multiple ways to describe an arbitrary number
%% of access patterns. However, because these may potentially alias with
%% each other, the compiler must disambiguate them to avoid conflicts at
%% runtime. To accomplish this, the compiler picks at most one disjoint
%% partition of each region---this partition, and only this partition may
%% be used as-is. For all other partitions, the compiler duplicates every
%% subregion of the partition to give a distinct logical name to the data
%% covered by that region.

%% \subsection{Must Epoch Launch}
%% \label{subsec:epoch}

%% The standard semantics for a task launch allow but do not require
%% tasks to run in parallel; however, with a SPMD-style program, a
%% failure to run in parallel would result in deadlock (due to explicit
%% synchronization). Regent provides a \emph{must-parallel epoch}
%% construct which launches a set of tasks which are required to run in
%% parallel for specifically this purpose.

%% The arguments to the tasks within the epoch include any regions to be
%% used directly by the shards themselves, in addition to any regions
%% which overlap regions used directly. Because this results in
%% interference between tasks (because tasks might use the same region
%% with read-write privileges), \emph{simultaneous} coherence is enabled
%% to relax the constraints of execution ordering to permit parallel
%% execution.
