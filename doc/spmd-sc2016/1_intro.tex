\section{Introduction}
\label{sec:intro}

SPMD programming systems, including MPI\cite{MPI},
Titanium~\cite{TITANIUMSTANDARD} and UPC\cite{UPCSTANDARD} among many
others, are broadly considered to achieve best-in-class performance
and scalability on today's distributed-memory machines. SPMD codes
execute in a manner inherently suited to the
available hardware, with independent control contexts executing
simultaneously in an asynchronous manner and explicit user control
over synchronization and communication. (Note that SPMD codes need not
be bulk-synchronous.) However, this distributed control comes at a
cost to programmer productivity, as the user is responsible for all
synchronization and communication in the system and is exposed to the
pitfalls those mechanisms entail.

In contrast, implicitly parallel programming models, including
Spark\cite{Spark10} and Legion\cite{Legion12}, use a single control
context to launch parallel work. Programs written in such models are
easy to read, write and reason about, and because data movement is
also implicit, they avoid potential pitfalls (such as deadlocks) that
can occur with explicit synchronization.

For machines with small numbers of nodes, or applications with large
units of parallel work, the implicitly parallel approach works
well. However, the sequential bottleneck of a single control context
becomes apparent at larger node counts---generally, the overhead
incurred by such systems is proportional to the number of units of
parallel work to be executed, divided by the granularity of the work
to be performed. This leads to challenges when attempting to scale
typical HPC applications to large machines. This
problem is severe enough that Legion, for example, provides additional
features to support explicit communication and synchronization,
allowing programmers to write codes that scale to large machines,
albeit with more programming effort \cite{LegionFields14}.

In this paper we pursue a best-of-both-worlds approach by compiling
implicitly parallel programs into efficient SPMD codes, as shown in
Figure~\ref{fig:tasks1}. We present an implementation for
Regent\cite{Regent15}, a language that supports both implicitly parallel and
SPMD styles of programming, allowing exploration of these trade-offs
within a single programming system.

\begin{figure}[t]
\begin{minipage}{0.5\textwidth}\centering
\subfloat[Original implicitly parallel program.\label{fig:tasks1a}]{\includegraphics[scale=0.5]{examples/program_before.pdf}}
\end{minipage}

\vspace{0.3cm}

\begin{minipage}{0.5\textwidth}\centering
\subfloat[Program after control replication.\label{fig:tasks1b}]{\includegraphics[scale=0.5]{examples/program_after.pdf}}
\end{minipage}

\caption{Execution of a program before and after control replication.\label{fig:tasks1}}
\end{figure}

Our primary contribution is \emph{control
  replication}, a technique that distributes the work of the
sequential control context between multiple distributed shards of
control. A key insight of our technique is that generating efficient
SPMD code requires an intimate understanding of the patterns of
communication used in the application; thus it is critical that the
source programming model be amenable to automatic analysis of these
communication patterns. We present a combined static and dynamic
analysis that is able to compute a precise communication graph for
the application and insert the necessary communication and
synchronization for SPMD execution. We evaluate control replication on
three non-trivial unstructured applications and demonstrate that the technique
results in code that scales well, achieving
up to 96\% efficiency on 512 nodes.

\begin{figure}[t]
\begin{lrbox}{\mybox}
\begin{lstlisting}[language=Simple]
task do_physics(own : region)
  where reads writes(own.{a, b}) do #\ldots# end
task do_stencil(own : region, ghost : region)
  where reads(own.{a, b}, ghost.b), writes(own.a) do #\ldots# end

-- inside the main simulation task:
for t = 0, T do
  parallel for i = 0, N do
    do_physics(own[i])
  end
  -- implied communication from own to ghost
  parallel for i = 0, N do
    do_stencil(own[i], ghost[i])
  end
end
\end{lstlisting}
\end{lrbox}
\subfloat[Original heat diffusion program.\label{fig:code1a}]{\usebox\mybox}

\begin{lrbox}{\mybox}
\begin{lstlisting}[language=Simple]
-- do_physics and do_stencil defined as above
task shard(T : int, N : int, own : region, ghost : region)
  where reads writes(own.{a, b}, ghost.b) do
  for t = 0, T do
    do_physics(own)
    for j = 0, N do
      copy(own, ghost[j]) -- synchronization elided
    end
    do_stencil(own, ghost)
  end
end

-- inside the main simulation task:
parallel for i = 0, N do
  shard(T, N, own[i], ghost[i])
end
\end{lstlisting}
\end{lrbox}
\subfloat[After control replication.\label{fig:code1c}]{\usebox\mybox}

\vspace{0.05cm}
\caption{Heat diffusion code before and after control replication.\label{fig:code1}}
\end{figure}

As a motivating example, consider the code in Figure~\ref{fig:code1a},
a simulation of heat diffusion on an unstructured mesh that exhibits a
communication pattern representative of many HPC applications. The
first loop performs a pointwise computation over owned cells, reading
the \emph{fields} of each cell computed in the previous timestep
(represented above as a single field \emph{a}) and writing fields for the beginning of
the current timestep (field \emph{b}). The second loop performs a stencil,
where each cell reads from a set of neighboring cells to update its
final value for the timestep (field \emph{a}).

A straightforward implementation on an unstructured mesh is to define
a set of ghost cells (the neighbors around each set of owned cells),
and to simply have the ghost cells alias with the corresponding cells
of the mesh, as shown in Figure~\ref{fig:partitioning}. If the mesh is
distributed over a large machine, then in the first loop some node X
will update \emph{own[1]} and in the second loop a node Y will perform the
read of \emph{ghost[2]}. If X and Y are different nodes, then the mesh
elements that lie in both \emph{own[1]} and \emph{ghost[2]} (i.e., their
intersection) must be
communicated after \emph{own[1]} is updated by the first loop but before
\emph{ghost[2]} is read by the second loop. Thus, implicit communication is
required as the application alternates between these two aliased views
of the mesh.

A transformation of this program into SPMD style (shown in
Figure~\ref{fig:code1c}) is complicated by this implicit communication
between the inner loops. First, the analysis needs to detect the
presence of aliasing in the data access patterns of the owned and
ghost cells. Second, to determine where explicit communication is
required, the compiler must discover the producer-consumer
relationships implied by aliased data access patterns. Third and
finally, efficient communication requires knowing not just that there
may be aliasing, but the precise intersection of the source and
destination cells.

\begin{figure}[t]
\includegraphics[scale=0.6]{figs/heat/partitioning.pdf}
\caption{Example data access patterns for heat diffusion.\label{fig:partitioning}}
\end{figure}

While the determination of potential aliasing is tractable for static
analysis in a carefully formulated programming language, the precise
intersection calculation needed by the final step is not possible
to carry out accurately at compile time. Thus, we delay analysis of
aliasing patterns until runtime when they can be precisely
resolved. Fortunately, the compiler is able to lift these queries into
the initialization phase of the program and avoid any expensive checks
during the execution of the main loop.

Our implementation of control replication for Regent is composed of
three stages, following the steps outlined above:

\begin{itemize}
\item First, a \emph{region tree analysis} identifies overlapping data
  access patterns in the application (Section~\ref{sec:rtree}).
\item Second, a \emph{region-based dataflow intermediate representation}
  captures the locations of all implied communication in the program
  (Section~\ref{sec:rdir}).
\item Finally, the compiler inserts explicit communication and
  synchronization, and includes code to dynamically discover the
  precise communication graph of the program at runtime
  (Section~\ref{sec:replication}).
\end{itemize}

We present an implementation of control replication for the Regent
language in Section~\ref{sec:implementation}. We evaluate this
implementation in Section~\ref{sec:evaluation} on three unstructured
applications: a circuit simulation on an unstructured graph, an
explicit solver for the compressible Navier-Stokes equations on a 3D
unstructured mesh, and a Lagrangian hydrodynamics simulation on a 2D
unstructured mesh. Our implementation of control replication achieves
up to 96\% parallel efficiency on 512 nodes on the Piz Daint
supercomputer\cite{PizDaint}. Section~\ref{sec:related} discusses
related work, and Section~\ref{sec:conclusion} concludes.
