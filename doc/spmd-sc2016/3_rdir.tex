\section{Region-based Dataflow IR}
\label{sec:rdir}

In Figure~\ref{fig:code2a}, we can see from the annotations that line
6 reads and writes the subregion \emph{own[i]} and that line 10 reads
the subregion \emph{ghost[j]}. As discussed in
Section~\ref{sec:intro}, this situation requires communicating any
updates to \emph{own[i]} performed by line 6 to the reads of
\emph{ghost[j]} on line 10.  And, again, only the mesh elements in the
intersection of \emph{own[i]} and \emph{ghost[j]} need to be
communicated.  Thus, we need to first \emph{gather} (i.e., read) the
subset of elements of \emph{own[i]} that are in both subregions, and
then \emph{scatter} (i.e., write) them into the appropriate locations
in \emph{ghost[j]}. In the region tree of Figure~\ref{fig:code2b}, this
process can be visualized as writing back the updated elements of
\emph{own[i}] to the \emph{cells} parent region, from which elements are then
scattered into \emph{ghost[j]}.

%% Communication in an implicitly parallel program is implied when two
%% statements access the same data. The region tree analysis presented in
%% the previous section identifies the data access patterns of each
%% statement and determines any potential aliasing between the data
%% accessed by each. Note, however, that although aliasing implies
%% overlap, it does not follow that aliased accesses will be exactly
%% equivalent. Thus the data movement implied by the program is not a
%% direct copy, but a \emph{gather} up to a common ancestor in the region
%% tree followed by a \emph{scatter} down to the regions to be accessed.

Control replication requires that these gather and scatters be made
explicit, as they mark the program points where explicit communication
will be inserted into the final program. We use a \emph{region-based
  dataflow intermediate representation} (RDIR) to make gathers and
scatters explicit. RDIR is a hierarchical graph format, with nodes for
operations (rectangles), data (ellipses), gathers and scatters
(triangles), and edges labeled with privileges (read, write,
reduce). Because data can be updated, the same name can appear
multiple times in the graph, representing a different version of the
data on each occurrence. For clarity, different occurrences of the
same data node in Figures~\ref{fig:rdir-partial} and
\ref{fig:rdir-complete} are given a distinguishing integer label.

Figure~\ref{fig:rdir-complete} shows the RDIR graph generated from the
body of the outermost loop of Figure~\ref{fig:code2a}. The two inner
loops from lines 5-8 and 9-12 appear in the graph as the two operation
nodes (the rectangles). For example, the first inner loop, which was
annotated with the usage \emph{RW\textup{(}own.\{a, b\}\textup{)}}
appears along with data nodes representing the data accessed.  (Here,
data nodes are split by field so that \emph{own.a} and \emph{own.b}
appear separately.)  The implicit communication between the loops
(from \emph{own.b} to \emph{ghost.b}) is explicitly represented by a
pair of gather and scatter nodes in the RDIR graph.

\subsection{Execution Model}
\label{subsec:execution-model}

Execution of a program in RDIR form starts at the outermost subgraph
(corresponding to the body of a task). Nodes execute as soon as all
inputs are complete, enforcing a topological order of execution. Loop
nodes execute their contained subgraphs zero or more times as
determined by the loop bounds. Each edge triggers only once for a
given execution of a subgraph.

As mentioned above, each data node represents a distinct version of
the data named. The data stored in a data node becomes valid as soon
as the node receives a write; the data node itself completes
immediately with no effect.

Conceptually, each instance of a data node maintains distinct storage
that is written once when the data is defined and never changed. This
means that, for example, write-after-read dependencies are implicit in
the graph. It is usually desirable, however, to preserve the locality
of the data by overwriting a single storage location each time a new
version of data is defined. This can be achieved by augmenting the
RDIR graph with happens-before edges from readers to subsequent
writers of each data node; we omit these edges for clarity, as they
may be recovered automatically.

\begin{figure*}[t]
\centering
\includegraphics[scale=0.48]{examples/rdir_replicated.pdf}
\caption{RDIR with explicit copies and synchronization.}
\label{fig:rdir-replicated}
\end{figure*}

\subsection{Translation from Regent AST to RDIR}
\label{subsec:ast-to-rdir}

The compiler converts Regent ASTs to the RDIR format in several
steps. First, the compiler walks statements in the program and
constructs an operation node for each statement and data nodes for the
data accessed by the statement. Second, the compiler connects aliased
data nodes between statements by inserting gather and scatter nodes
(Figure~\ref{fig:rdir-partial}). And finally, the compiler optimizes
gather and scatter nodes to eliminate redundant communication
(Figure~\ref{fig:rdir-complete}).

\subsubsection{Translation of Statements}
\label{subsubsec:ast-to-rdir-statements}

To generate the initial structure of the RDIR graph, the compiler
walks the program AST and converts statements one at a time. Blocks of
statements, such as in loop bodies, form distinct subgraphs. For each
subgraph, the compiler inserts two sets of data nodes representing the
initial and final state of the loop. Statements within a block are
added to the graph, with their inputs and outputs determined by the
privileges recorded by the region tree analysis.

For example, the first inner loop from Figure~\ref{fig:code2a} on
lines 5-8 produces the operation node (rectangle) on the left side of
Figure~\ref{fig:rdir-partial}. The recorded privileges
\emph{RW\textup{(}own.\{a,~b\}\textup{)}} result in four attached data
nodes: two nodes (the ones numbered 2) for the
reads of \emph{own} fields \emph{a} and \emph{b}, respectively, and
two more nodes (\#3) for the writes of the same fields.

\subsubsection{Insertion of Gathers and Scatters}
\label{subsubsec:ast-to-rdir-communication}

The compiler connects aliased data nodes with gather and scatter
nodes, resulting in a form where data movement is explicit.

The compiler iterates over the statements in the original program order,
connecting each in turn to the growing connected subgraph of preceding
statements. For each data node in a statement, the compiler finds the
frontier set of aliased nodes most recently used in the
graph. (Initially, the frontier contains the initial set of input and
output data nodes
created for the subgraph.) After all statements have been inserted,
the compiler connects the final set of data nodes for the subgraph.

For example, the input to the second inner loop \emph{own.b} (number
5) is previously written by the first inner loop (number
3). Similarly, the input \emph{ghost.b} (number 6) to the second loop
aliases \emph{own.b} (number 3) from the first loop. Aliased nodes are
inserted together, with a single set of intermediate gather and
scatter nodes.

Gather and scatter nodes must copy data through a region that
contains all the elements of both the source and destination
regions. Thus, the compiler consults
the region tree (Figure~\ref{fig:code2b}) and inserts the least common
ancestor into the graph between the gather and scatter.

\subsubsection{Optimization of Gathers and Scatters}
\label{subsubsec:ast-to-rdir-optimization}

Finally, the compiler optimizes gather and scatter nodes to remove
redundant communication. Any gather or scatter where the source and
destination are the same can be elided. Also, a gather preceded by a
scatter (ignoring intervening data nodes) can be elided. Finally, if a
pair of identical regions is connected only by gathers, scatters and
data nodes (with no intervening operations), the second region in the
pair can be elided and all edges moved to the first occurrence in the
graph. Figure~\ref{fig:rdir-complete} shows the resulting graph when
these optimizations are applied.

%% \begin{figure}[t]
%% \begin{tabular}{r l}
%% Node Type & Summary \\
%% \hline
%% Data & Holds version of data stored at a location \\
%% Compute & Encapsulates a computation \\
%% Control & Executes a subgraph zero or more times \\
%% Open & Scatters data to children \\
%% Close & Gathers data from children
%% \end{tabular}
%% \caption{RDIR node types.}
%% \label{fig:rdir-outer-loop}
%% \end{figure}

%% \begin{figure}[t]
%% \centering
%% \includegraphics[scale=0.75]{examples/application_order.pdf}
%% \caption{Data nodes allow one write followed by zero or more
%%   reductions.}
%% \label{fig:rdir-application-order}
%% \end{figure}
