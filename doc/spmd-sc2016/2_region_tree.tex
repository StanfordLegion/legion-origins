\section{Region Tree Analysis}
\label{sec:rtree}

Control replication requires an intimate understanding of the data
access patterns in an application. For every pair of statements, the
compiler must determine whether the pair may potentially access the
same data, as this implies communication within the program. In
Figure~\ref{fig:code2a}, for example, the loops on lines 5-8 and 9-12
access owned and ghost cells respectively; and because these two sets
of cells overlap with each other (Figure~\ref{fig:partitioning}),
there is communication implied between them.

In this section we consider an analysis to answer these questions in
the context of Regent. Data in Regent is stored in \emph{regions}
(collections of objects). Thus, the goal of the analysis is to
determine what regions a statement may access, and to discover any
potential overlap between pairs of regions.

To facilitate this discussion, Figure~\ref{fig:regent-core} presents a
grammar for \emph{Core Regent}, a fragment of the complete Regent
language that focuses on the key features relevant to this
analysis. (Core Regent follows semantics described in
\cite{LegionTypes13}, though the surface syntax differs somewhat.)
Here we are concerned with analyzing the data access patterns of code
fragments such as those in Figure~\ref{fig:code2a}, containing
sequences of simple parallel task launches.

%% Extensions to this
%% analysis for coverage of the entire Regent language are discussed in
%% Section~\ref{sec:implementation}.

\begin{figure}[t]
\small
\begin{tabular}{r@{ }c@{ }l}
\emph{pgrm} & ::= & \emph{task}+ \\
\emph{task} & ::= & \textbf{task} \emph{id}\textbf{(}[\emph{param},]*\textbf{)} [\textbf{where} \emph{priv}* \textbf{do}] \emph{stmt}* \textbf{end} \\
\emph{param} & ::= & \emph{id} \textbf{:} \emph{type} \\
\emph{priv} & ::= & \emph{mode}* \textbf{(}\emph{id}\textbf{.}\emph{fields}\textbf{)} \\
\emph{mode} & ::= & \textbf{reads} \textbar{} \textbf{writes} \textbar{} \textbf{reduces} \emph{op} \\
\emph{op} & ::= & \textbf{+} \textbar{} \ldots \\
\emph{fields} & ::= & \emph{id} \textbar{} \textbf{\{} [\emph{id,}]* \textbf{\}} \\
\emph{stmt} & ::= & \textbf{var} \emph{id} \textbf{=} \emph{init} \\
            & \textbar & \textbf{for} \emph{id} \textbf{=} \emph{expr}, \emph{expr} \textbf{do} \emph{stmt} \textbf{end} \\
            & \textbar & \textbf{parallel for} \emph{id} \textbf{=} \emph{expr}, \emph{expr} \textbf{do} \emph{call} \textbf{end} \\
\emph{init} &  ::= & \emph{expr} \\
            & \textbar & \textbf{region()} \\
            & \textbar & \textbf{partition(}\textbf{disjoint}, \emph{id}\textbf{)} \\
            & \textbar & \textbf{partition(}\textbf{aliased}, \emph{id}\textbf{)} \\
\emph{call} & ::= & \emph{id}\textbf{(}[\emph{expr},]*\textbf{)} \\
\emph{expr} & ::= & \emph{val} \textbar{} \emph{id} \textbar{} \emph{expr}\textbf{[}\emph{expr}\textbf{]} \\
\emph{val} & ::= & 0 \textbar{} 1 \textbar{} \ldots \\
\\
\emph{type} & ::= & \emph{base} \textbar{} \textbf{region} \\
\emph{base} & ::= & \textbf{int} \textbar{} \ldots
\end{tabular}
\caption{Core Regent grammar.}
\label{fig:regent-core}
\end{figure}

% Arithmetic? I think this isn't a good idea because it allows complex indexes
%% \textbar{} \emph{expr} \textbf{+} \emph{expr}

\subsection{Region Trees}
\label{sec:rtree-region-trees}

%% Regent employs a type system which makes data and data access patterns
%% explicit\cite{LegionTypes13}. Regent's primary data structures are
%% \emph{regions}, which are simply containers for elements. Accesses to
%% regions are tracked explicitly by Regent's type system, enabling a
%% tractable static analysis of data access patterns in the application.

Regions can be \emph{partitioned} into
\emph{subregions}. Figure~\ref{fig:code2a} includes the partitioning
for heat diffusion, where owned and ghost define partitions of all
cells following the access patterns shown in
Figure~\ref{fig:partitioning}. (The details of how specific mesh
elements are included in specific subregions are not important for our
purposes and are omitted.) Note that subregions of owned cells (line
2) are non-overlapping, or \emph{disjoint}, whereas subregions of
ghost cells (line 3) are overlapping, or \emph{aliased}, (and do not
even cover the entire mesh).

Regions may be recursively decomposed into subregions, so it is
natural to view the overall organization of data as a \emph{region
  tree}. Figure~\ref{fig:code2b} shows the tree for the example
above. The formulation is convenient because it proves a natural way
to determine whether any two regions may potentially alias: For any
regions \emph{R} and \emph{S}, find the least common ancestor
\emph{A}. If \emph{A} is a disjoint partition (and \emph{R} and
\emph{S} are indexed by constants), then the two regions are disjoint;
otherwise they may alias.

Region trees can be constructed by walking the program source from top
to bottom. Each newly created region becomes the root of a fresh region
tree. Partitions are inserted under the region they partition, and
expressions that access subregions of partitions result in the
corresponding subregion nodes, tagged with the index expression used.

\begin{figure}[t]
\begin{lrbox}{\mybox}
\begin{lstlisting}[language=Simple]
var cells = region()
var own = partition(disjoint, cells)
var ghost = partition(aliased, cells)
for t = 0, T do #\codecomment{RW\textup{(}own.a, cells.b\textup{)}}#
  parallel for i = 0, N do #\codecomment{RW\textup{(}own.\{a, b\}\textup{)}}#
    do_physics( #\codecomment{RW\textup{(}own[i].\{a, b\}\textup{)}}#
      own[i])
  end
  parallel for j = 0, N do #\codecomment{RW\textup{(}own.a\textup{)}, R\textup{(}own.b, ghost.b\textup{)}}#
    do_stencil( #\codecomment{RW\textup{(}own[j].a\textup{)}, R\textup{(}own[j].b, ghost[j].b\textup{)}}#
      own[j], ghost[j])
  end
end
\end{lstlisting}
\end{lrbox}
\usebox\mybox
\caption{Heat diffusion code with data access annotations.\label{fig:code2a}}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[scale=0.7]{examples/partitions.pdf}
\caption{Region tree for heat diffusion. Filled boxes are disjoint partitions.\label{fig:code2b}}
\end{figure}

\begin{figure*}[t]
\centering
\includegraphics[scale=0.355]{examples/rdir_partial.pdf}
\caption{Initial RDIR graph for outer loop after insertion of statements and scatter and gather nodes.}
\label{fig:rdir-partial}
\end{figure*}

\begin{figure*}[t]
\centering
\includegraphics[scale=0.48]{examples/rdir_complete.pdf}
\caption{Final RDIR graph for outer loop after optimization of scatter and gather nodes.}
\label{fig:rdir-complete}
\end{figure*}

\subsection{Recording Region Usage}
\label{sec:rtree-region-usage}

The first step in our method for converting an implicit parallel
program to an SPMD program is to annotate each program statement with
the regions (and partitions) that statement uses, as well as how the
region (partition) is used: does the statement read from (R), write to
(W) or reduce to (Red) a given field of the region?  For most statements inferring the
\emph{privileges} (R, W, or Red) is easy. The only non-obvious case is
where the privileges are obtained for task calls; tasks in Regent must
declare their privileges on region arguments. Region usage for a
statement is recorded as a list of tuples of the form
\emph{privilege(region.fields)}.

Using privileges, and knowledge of the region tree, the compiler can
analyze the data usage for loops as follows. Loops summarize the
privileges of their bodies, dropping loop indices from any region
index expressions to indicate that multiple (and not just one)
subregion may be accessed. For example, in the first inner loop (lines
5-8), the call to do\_physics records the access
\emph{RW\textup{(}own[i].\{a, b\}\textup{)}}, while the loop itself
records \emph{RW\textup{(}own.\{a,
  b\}\textup{)}}. Figure~\ref{fig:code2a} shows the heat diffusion
program with region access annotations.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "spmd"
%%% End:
