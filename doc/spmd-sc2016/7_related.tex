\section{Related Work}
\label{sec:related}

% FIXME
% RDIR: Sequoia, dataflow?
% Analysis: DPJ
% Languages: Copperhead

The compiler literature has a long history of optimizing programs for
parallel performance. The general case---where the compiler attempts
to automatically parallelize a traditional sequential program written
without any regard to parallelism---has proven a very difficult
problem. Instead, most approaches have focused on optimizing programs
that include some degree of explicit annotation to guide parallel
execution, with the goal of providing an overall experience that is
cleaner and easier to use than fully explicit distributed-memory
programming models.

In the context of OpenMP, there has been significant interest in
methods for compiling shared-memory programs to run on NUMA and
distributed-memory systems. Several efforts implemented OpenMP over
software distributed memory systems\cite{ClusterOpenMP01,
  IntelClusterOpenMP06, DistributedOpenMP07}. Others investigated
direct translation to MPI\cite{DistributedOpenMP07}, including
optimizations for irregular programs that leveraged runtime inspection
of data to perform loop
reordering\cite{IrregularDistributedOpenMP06}. These approaches have
been demonstrated to scale to modest core counts, with further
scalability limited not so much by the model of parallelism as the
difficulty of performing the needed analysis of communication
patterns.

%% Others sought incremental improvements via source-to-source
%% transformation of programs. Array privitization for
%% OpenMP\cite{ArrayPrivitizationOpenMP03} was shown to improve
%% performance on multiple cores of a single-node shared-memory NUMA
%% machine, with some user input, and for structured applications.

% Is this really related?

%% Bamboo\cite{Bamboo12} demonstrated
%% improved latency-tolerance of annotated MPI codes by retargetting the
%% program to an independent runtime system---though scalability was
%% never a question because the original program was written in MPI.

Another line work of focused on data-parallel languages for
arrays, especially High Performance Fortran (HPF)\cite{HPF96,
  HPF07}. In HPF, a single (conceptual) thread of control creates
implicit data-parallelism by specifying operations over entire arrays
in a manner similar to traditional Fortran. This data parallelism is
then mapped to a distributed memory system via explicit user-specified
data distributions of the arrays---though the compiler is still
responsible for inferring \emph{shadow regions} (i.e. halos that must
be communicated) from array accesses. Several versions of HPF provided
different sets of features, mostly focusing on structured
applications.

One of the best demonstrations of performance in HPF was achieved with
IMPACT-3D, a 3D fluid simulation on a structured
mesh\cite{Impact3D02}. IMPACT-3D leveraged the HPF/JA extensions to
enable explicit user control over shadow regions and
communication\cite{HPFJA02}. To our knowledge, similar extensions were
not investigated for unstructured applications---in fact, HPF/JA
removed support for several HPF 2.0 approved extensions intended for
use in unstructured codes, to reduce the complexity of the compiler
implementation.

One effort did attempt to add support for unstructured applications to
HPF\cite{HPFUnstructured95}. However, the changes required radically
altered the programming model: the standard HPF notion of sequential
control was exchanged for what was effectively a SPMD programming
model, complete with low-level message passing constructs.

More recent high-level parallel languages such as
Chapel\cite{Chapel07} have avoided reliance on the compiler's ability
to optimize high-level language features by adding support for
low-level constructs that can be used when necessary. This approach is
also taken in Legion and Regent, which support both a straightforward
implicitly parallel style of programming that scales to modest numbers
of nodes as well as more involved explicit communication constructs
that can be used to scale to very large node counts
\cite{LegionFields14}. Our work can be seen as greatly increasing the
performance range of the implicit style, allowing more whole codes and
subsystems of codes to be written in this more productive and more
easily maintained style. We are able to make more progress on these
problems even for unstructured codes compared to the earlier efforts
discussed above only because the Legion/Regent data model of
explicitly named regions and subregions is much more amenable to
compiler analysis than languages with less structure in how
programmers organize and refer to data, such as C, C++ and
Fortran. Control replication is the first approach to provide
sufficiently powerful analyses of implicitly parallel unstructured
programs, and the first to demonstrate that efficient SPMD codes can be
automatically generated for distributed-memory machines.


%% PGAS languages such as UPC\cite{UPCSTANDARD},
%% Titanium\cite{TITANIUMSTANDARD} and X10\cite{X1005} completely
%% side-step the issue of automatic parallelism by providing explicit
%% distributed control and non-uniform memory access.


Control replication is further distinguished from previous approaches
in its ability to reason about multiple distributions of the data
simultaneously, even when one or more of those distributions is
internally overlapping. Preserving this information throughout the
toolchain allows control replication to perform well on unstructured
applications without the need for additional annotations or
instrinsics.

Finally, control replication reaps substantial performance benefits
from targeting Regent\cite{Regent15} and Legion\cite{Legion12}, a
language and runtime system with native support for hierarchical task
parallelism and deferred execution. Control replication is able to
ignore the details of computations inside leaf tasks, thus simplifying
the implementation and focusing the compiler's effort on areas were it
can have the most leverage.
