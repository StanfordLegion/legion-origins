\section{Implementation}
\label{sec:implementation}

We have implemented dynamic tracing in Legion, a C++ runtime system for task parallelism~\cite{Legion12}. 
%
Legion has a dependence analysis pipeline similar to the one described in Section~\ref{sec:model} and builds a task graph using Realm~\cite{Realm14}, a low-level system for building and executing distributed task graphs.
%
We augment Legion's existing dependence analysis to generate graph calculus programs for traces.
%
Graph calculus is implemented as a set of commands that internally call the Realm API to construct task graphs.

In the rest of this section, we briefly discuss the ways in which our implementation for Legion extends the algorithm presented in this paper.

\subsection{Overlapping Regions}

For simplicity of exposition, we have assumed that regions and region instances are unique and each region instance represents only one region.
%
In the Legion programming model, regions can be partitioned into subregions, and thus can overlap with each other in non-trivial ways.
%
This complicates the dependence analysis, but has no fundamental impact on how dynamic tracing generates graph calculus commands.

\subsection{Fills and Reductions}

Legion provides {\em fills}, which are lazy copies from a constant value. 
%
We incorporate fills as another kind of operation that can be used in the $\mtt{op}$ command.

As discussed in Section 3, Legion tasks can request reduction permission on regions.
%
Reduction tasks are parallelized by summarizing the update from each task into a temporary instance and lazily aggregating such instances to compute the final value when it is requested in subsequent tasks.

\subsection{Parallel Dependence Analysis}
\label{subsec:par-dep}

A task can launch subtasks, and thus tasks that run in parallel can generate their own streams of tasks.
%
The programming model guarantees the children of independent tasks are also independent and thus concurrent task streams are independent of each other~\cite{Legion12}.
%
We have extended dynamic tracing to support concurrent, distributed task streams by taking separate recordings per stream and replaying them independently.

The Legion runtime also pipelines dependence analysis of task streams.
%
The recording procedure of graph calculus commands is divided into several steps, one for each pipeline phase.

\input{fig_parallel_replay}

\subsection{Parallel Trace Replay}
\label{subsec:par-replay}

As sequential replay of a trace can become a performance bottleneck, we implemented parallel replay of a trace.
%
Figure~\ref{fig:parallel-replay} illustrates the key transformation for parallel replay.
%
A trace is split into {\em slices}.
%
Commands appear in slices in the same order as the original trace and any events that are created in one slice and referenced in other slices are connected using the graph calculus extension shown in the figure.
%
A command $\mtt{e := event}$ creates a new untriggered event and assigns it to an event variable $\mtt{e}$.
%
A command $\mtt{trigger(e_1, e_2)}$ registers an event dependence such that event $\mtt{e_1}$ is notified as soon as $\mtt{e_2}$ is triggered, which simply corresponds to adding an edge between the operations represented by $\mtt{e_2}$ and $\mtt{e_1}$.
%
Slices generated from a trace can be replayed in parallel.

Minimizing events that ``cross'' the slice boundary is important for reducing the number of intermediate events for parallel replay, for which we exploit the implicit knowledge encoded in an application's task mappings:
%
We put tasks mapped to the same processor in the same slice as much as possible because in a well-mapped program they are more likely to have dependences on one another.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
