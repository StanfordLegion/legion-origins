# Reviewer 1

* The replay overhead is a big concern.
* The dynamic tracing is not sufficiently evaluated.
* The paper does not discuss how much overhead for dynamic dependence analysis is reduced by dynamic tracing.

# Reviewer 2

* "the connection to real-world problems and use cases seemed tenuous to me, leaving me to wonder how academic vs. practical this paper was."
* It is not clear how much the benchmarks sutdied in the paper required dynamic tasking (or whether they can be counted as "applications" or just simple kernels as with the case of PRK stencil.)
* Experimental results section is confusing in several respects.

# Reviewer 3

* Experiment section lacks details to get a good sense of teh applicability of the approach
* "I suppose, you need to check the graph is the same (isomorphic) as a previous graph."
* "I generally like to have a better idea about how expensive is it to analyze the graph and whether it is parallelized."

# Reviewer 4

* "The notion of memoizing results of analyses for repetitive tasks is similar, in spirit, to inspector-executor models. The paper should cite relevant prior work on this topic."
* "Can you give more information on the tracing aspects of the empirical evaluation? How big are the trace sizes? Also, how many distinct traces are identified (due to distinct pre/post-conditions) per region of code traced?"

# Reviewer 5

* "Specialization of task graphs is a very broad claim that the authors use in the title and initial sections, when really what they are trying to do is just a record-replay model."
* "it will be good for the authors to discuss what scalability range they are targetting so the readers have an idea as to where to place them with respect to the scale of the machines they are targetting."
