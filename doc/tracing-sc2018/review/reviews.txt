# Review of pap490s2 by Reviewer 1

Summary and High Level Discussion

    This paper introduces a dynamic tracing technique to capture
    dynamic dependence of a trace. The dynamic tracing technique can
    dynamically generate a task graph and replay it. The key insight
    of the work is that some results of the dynamic dependence
    analysis can be memorized and reused to reduce runtime overhead.

    The dynamic tracing introduces several optimization techniques,
    such as fence elision, optimizing replay using idempotent
    recordings. However, the effectiveness of those techniques are not
    well understood. The runtime overhead of the dynamic tracing is
    still a big concern.

    Strengths:

    1. The paper uses example codes throughout the paper to explain
       the technique. The example codes are very helpful to understand
       the paper.

    2. The dynamic tracing must handle data coherence in distributed
       task graphs.

    3. The dynamic tracing introduces graph calculus commands to
       construct task graphs. The graph calculus commands have good
       expressiveness to specify task dependencies. Based on the graph
       calculus commands, several optimization techniques are possible.

    Weaknesses:

    1. The runtime overhead (replay overhead) of dynamic tracing is a
       big concern. Depending on how long a trace is and depending on the
       existence of certain program constructs (e.g., the predicate
       condition in PENNAT), the runtime overhead can vary greatly and
       cannot be easily hidden.

    2. The dynamic tracing is not sufficiently evaluated. The
       effectiveness of the proposed optimization techniques is not well
       understood.

    3. Reducing the overhead of dynamic dependency analysis is a key
       to the success of dynamic tracing. Unfortunately, the paper does
       not discuss it sufficiently. Does the dynamic tracing heavily rely
       on the overlapping between runtime dependence analysis and
       application to hide the analysis cost? Is the memoization of
       dynamic dependency analysis results effective to hide cost? Is the
       overlapping or memorization more effective to hide cost?


Comments for Revision/Rebuttal

    (1) How often does the memorization technique work? Does it
        effectively reduce the overhead?

    (2) The paper claims that some operations may not be recorded,
        while a fence can capture the operation’s dependence on those
        operations that are not recorded. What operations are not
        recorded? Shouldn’t all operations be recorded for program
        correctness?

    (3) It is not clear how effective the proposed memoization
        technique is. I think this technique is the major selling
        point. Can you quantify how often the memoization happens?

    (4) Can you quantify the performance benefit of dynamic tracing
        over static tracing?

    (5) Execution template [7] caches blocks of frequently executed
        tasks. Is the method in nature similar to using graph calculus
        commands to replace the dependency analysis?


Detailed Comments for Authors

    (1) The runtime overhead of the dynamic tracing is a big
        concern. Sometimes, the performance is bad, when the trace is too
        long or the application is not friendly to overlap dynamic
        analysis with application execution. For those cases, how about
        dynamically disabling dynamic tracing to avoid runtime overhead.
        Please note that the performance of dynamic tracing is only 74% of
        the reference, which seems not good enough.

    (2) The paper can be significantly improved with more
        evaluations. For example, we can compare the performance of
        dynamic tracing with static tracing. We can also selectively
        disable optimization techniques to examine performance variance
        and show performance benefits of those techniques.

    (3) What does “specialize” mean? Does it mean the user can
        customize the generation of task graphs?

    (4) How does the dependence analysis handle overlapped regions?
        What does it mean by “finding the minimal set of valid instances
        that cover the current instance”?

    (5) Why are the only three applications evaluated with MPI
        reference implementation? Why not try the other two applications?


# Review of pap490s2 by Reviewer 2

Summary and High Level Discussion

    This paper describes an online system for recording and replaying task
    graphs with dynamic dependencies in order to enable the opportunity to
    reuse a previously computed task graph at lower overhead when an
    opportunity to do so presents itself. The paper describes the system
    using a number of detailed examples. The results are measured for
    five benchmarks/applications on up to 256 nodes of Piz Daint using
    strong scaling for modest problem sizes and demonstrate speedups over
    the same system without the dynamic tracing capability. Comparisons
    to reference versions are provided in some cases and vary in
    competitiveness.

    The strength of this paper for me is the presentation of the approach
    which, through the use of detailed examples, is refreshingly clear and
    approachable compared to most SC papers that I've reviewed.

    The main weakness of this paper for me was its motivation which
    (overstating it a bit for emphasis), seemed almost to be "lots of
    people are studying dynamic tasking, but it's bad in certain ways, so
    let's make it better." While applications of dynamic tasking were
    given, the connection to real-world problems and use cases seemed
    tenuous to me, leaving me to wonder how academic vs. practical this
    paper was. I also wasn't certain about the degree to which the
    benchmarks studied required dynamic tasking systems like this one (and
    the answer to that wasn't obvious to me in the paper). I also found
    the experimental results section to be confusing in several respects
    which I'll outline below.


Comments for Revision/Rebuttal

    * A theme of my comments above and below relate to motivation and the
    degree to which the benchmarks you studied rely on dynamic
    dependencies in their task graphs, rather than simply static ones.
    E.g., PRK Stencil is the main one of the five benchmarks I'm
    familiar with, and I would guess that it does not rely on dynamic
    dependencies? Do the others? If not, does that suggest that the
    things you're evaluating aren't good uses of the technology that
    you're proposing? If so, I think you should say more in the paper
    about what kinds of dynamic dependencies they contain and why your
    system is well-suited for them (better suited than MPI?)

    * I'd be curious for comments on my sec 2, par 5 comment below (and it
    may relate to the previous question).

    * Why the use of different compilers for the reference versions vs.
    yours?

    * Can you comment on the challenge of putting begin/end_trace calls
    into your study benchmarks or not?

    * Why are there no comparisons to reference versions for Circuit and
    Soleil-X?

    * If space permits, please feel free to address any other implied
    questions or misunderstandings in my detailed comments.


Detailed Comments for Authors

    * general: It seems to me like a big question in this approach is
    where begin/end_trace calls should be inserted into a user's program
    and how difficult it is to identify those points. Unless I missed
    it, the paper says very little about the sensitivity to the
    insertion of these calls nor the level of effort required to place
    them correctly. Yet I'm guessing the quality of the results would
    depend greatly on how well/poorly they are positioned?

    * sec 1, contribution bullet 3: Compared to tasking systems I'm most
    familiar with, execution times of hundreds of microseconds seem
    remarkably short-lived. Is this the typical granularity of
    Legion/Realm tasks? What about those of competing technologies?
    Could one takeaway from the paper be that compared to, say, MPI
    programming models, effort must be spent to reduce the management
    overhead of these short-lived tasks to make them competitive with
    conventional HPC technologies?

    * sec 2, par 5: "Suppose now that during trace k the choices of nodes
    ... are swapped" -- It's difficult for me to understand what could
    cause this in practice; can you provide a motivating scenario?
    I.e., it would seem unfortunate to have this swap occur, so
    presumably a smart user or runtime wouldn't [want to] do it. Are
    things sufficiently stochastic in the Legion/Realm runtime that
    things like this might just happen from one iteration to the next,
    or is this scenario not actually realistic? I guess I would have
    liked the paper to motivate what might cause the choice of nodes to
    be swapped like this because it seemed like a bad thing but one
    whose underlying cause wasn't clear to me and caused me to guess
    (greedy scheduling? random placement? lack of awareness of
    affinity in the runtime?).

    Somewhat related, looking at the three task graphs in Fig 1c-e, it
    seemed like the 1e graph was most attractive/ideal, and it made me
    wonder whether anything in the system would cause 1e to be more
    likely to be used than 1c, and if not, what factors would influence
    the system's choice between 1c and 1e, and the mapping change in 1d.
    Do the benchmarks you evaluate have behaviors more like 1c or like
    1e? And do they vary in how things are mapped to nodes over time,
    or are they relatively static in their patterns?

    If the benchmarks studied here are relatively static, it made me
    wonder whether they support the motivation for using a tasking
    system with dynamic dependencies in the first place. E.g., does the
    indirect indexing used as one of the motivating examples for
    requiring dynamic dependences show up in these benchmarks? Which
    ones?

    * sec 4D, par 2: "systems like Realm and OCR" -- Does Legion not? Or
    should I consider Realm and Legion to be one and the same for the
    purposes of this statement?

    * alg 1: I found myself wondering whether the algorithm should make
    some sort of statement about what the start state of the variables
    were -- e.g., I think ST == DEP and TR is nil at the start of time,
    is that right? (Ditto for alg 2)

    * fig 8e: I had a hard time parsing this caption and kept trying to
    change "users" into "user's" or "users'" Ultimately, I decided that
    it was referring to the insertion of merges with the e' variables
    from the first trace, but the caption didn't help me get there at
    all -- only the text of the paper and some guesswork. "Replace the
    second fence with per-operation merges with the corresponding
    results of operations in the first trace" is how I understand it.
    "with the users of the first trace" didn't say this to me.
    Searching the paper, I see that the term "users" is used
    consistently w.r.t. this figure, but that term still seems odd to
    me, as it suggests "the human programmer". Perhaps a different term
    would avoid this confusion? "uses"? "consumers"? "references"?

    * sec 5, par 1: As someone familiar with (but not particularly expert
    in) Legion, and much less familiar with Realm, the relationship
    between Leigion and Realm was not particularly clear to me here.
    Both Legion and Realm seem to be runtimes that support tasking. Is
    the idea to use Legion's tasks but to use Realm's task graph
    capabilities, but not its actual tasks? What is the goal / benefit
    of mixing these two systems together rather than just working in one
    or the other? (Realm, I'd presume).

    * sec 5: In general, section 5 seemed quite high-level compared to the
    previous sections which is perhaps unavoidable given the page
    budget, but felt a little unfortunate from the perspective of
    reproducibility. It felt as though the issues enumerated here were
    important ones for any real deployment of this work, yet with so
    little time spent describing each of them, it was hard to get much
    from this section other than a general sense of "more must be done
    in practice."

    * sec 6, par 1: "have competitive or better performance than the
    reference implementations" -- for me, this sentence begged the
    question "in cases where the Regent versions were better, what led
    to them being better?" to save readers from needing to go back and
    read citation [25] to have the context for your experiments.

    * sec 6: It wasn't obvious to me why, if the benchmarks were
    competitive or better than reference implementations in [25], the
    "no tracing" versions reported on here seemed to generally be
    underperforming compared to the reference versions (i.e., the Regent
    versions didn't seem "competitive or better" in this paper). One
    part of the answer might be strong vs. weak scaling, and you also
    mentioned choosing the smallest problem size that would drop off at
    32-64 nodes, but I would've liked the paper to indicate why the
    results seem different than [25] rather than requiring me/the reader
    to guess.

    * The previous point and the next bring up a frustration I had with
    this paper's results which is that the graphs are sufficiently
    compressed that it's challenging to know what the values of the data
    points in the graphs are, particularly given the log-log scale.
    This really made me want to see an appendix with the raw numbers in
    it so that I could do my own computations and comparisons and draw
    better conclusions.

    * Figure 9f: I ended up feeling very confused by the "improvement"
    column in this paper. I understand that it is the ratio of the max
    speedup tracking / no tracing entries of the previous columns, but I
    wasn't particularly clear on where those numbers came from
    themselves. I'd assumed (incorrectly now, I think) that they were
    the speedup values of the 256-node runs for the blue and red numbers
    respectively, and so was confused about why the Circuit and Soleil-X
    graphs seemed to be qualitatively similar (visually speaking), yet
    had such different factors of improvement. Having spent some time
    with the graphs and juggling possibilities, I now think that I
    understand that they're the max of _any_ of the no tracing points
    relative to the max of any of the tracing points (where each value
    could use a different number of nodes), so Circuit fares better
    because its 64-node no tracing timing reaches a higher peak than
    Soleil-X's -- is that right? If so, I think you ought to be saying
    more to clarify the sources of these numbers because it's not clear
    -- particularly since fig 9 a-e show absolute performance rather
    than speedup, so even the speedup numbers "130" and "58" are
    difficult for the reader to match to any of the results, not knowing
    what the 1-node values are (see my previous bullet). Put another
    way, I had to put in a great deal of effort to see if I understood
    what these numbers are and believed them to be correct, and think
    you should be leading readers by the hand better so that they don't
    have to make the same leaps that I did.

    * sec 6, par 1: Why are gcc and LLVM used for the Legion/Regent
    results and Intel for the MPI implementations? This seems like an
    unnecessary apples-to-oranges decision to make in the paper and
    seems definitely worth rationalizing/justifying to readers.

    * sec 6, general: Of the five codes you study, PRK Stencil is the only
    one I'm familiar with, and I'd be hard-pressed to call it an
    "application" rather than simply a benchmark or kernel. This makes
    me wonder whether I'd consider the others to be applications or not
    if I were more familiar with them. In either case, I'd suggest
    adjusting your term(s) to not overstate the size / complexity of the
    Stencil case (and others if they're similar in scope).

    * sec 6, general: The paper states that there are only reference
    comparisons given for the first three benchmarks, but doesn't say
    why there aren't for the other two. My assumption is that these are
    codes that you only have Regent versions for, but I shouldn't need
    to assume this. I also find myself wondering if they're
    computations that are somehow more natural / suitable for Regent
    and/or less so for MPI (e.g., are they more dynamic? more
    unpredictable? more indicative of the kinds of things that Regent
    and Legion was designed for than PRK Stencil?), but don't think the
    paper said so (and think it should if this is the case).

    * sec 6, general: I remain curious about why the Regent versions of
    MiniAero blow away the reference version. This seems like an
    important outlier in your studies but doesn't get an explanation.
    I'm not saying that I don't believe it, I'm just saying that anytime
    a proposed new technology beats a well-established one, it seems
    important to state what advantage permits it to do so.

    * sec 6, 2nd-to-last par: "The strong scalability is competitive with
    the reference for PENNANT and MiniAero" -- Stencil appears to be
    similarly competitive (or non- at 256 nodes), making me wonder
    whether I misunderstand what your definition of competitive is?

    * fig 9f: I think this table would be easier to read if the rows
    preserved the same order as fig9 a-e and table 1.



    Minor formatting / typographical comments:

    * It looks like you're using a capital "X" in your "3.1X", "6.5X",
    etc. notations throughout the paper — I expect a "times" symbol
    would look better.

    * sec 1, par 5: "for the commands to correctly to replay" -- I think
    the second "to" is a stray?

    * sec 2, par 3: "trace 1" -- I'd use a non-breaking space here to
    keep this noun all on one line.


# Review of pap490s2 by Reviewer 3

Summary and High Level Discussion

    This paper describes a task-based runtime system that generates
    and analyzes task graphs and reuses the result of a dependence
    analysis, thereby reducing the overhead of dependence analysis and
    improving the scalability of task-based runtime.

    It successfully improves scalability of five applications, in an
    experiment up to 256 nodes.

    Strengths

    * it is a solid work on an important topic
    * it shows good results on a distributed memory machine up to 256 nodes

    Weakness

    * it lacks a description of the benchmark programs and generate
      task graphs, making it a bit difficult to interpret the results
      (see below)
    * details are at times lacking to get a good sense of the
      applicability of the approach (see below)


Comments for Revision/Rebuttal

    I may be missing something, but there are several details I could
    not get to put the work in perspective.

    * How do you exactly judge if a previous analysis can be reused?
      The paper says it checks "preconditions", but more basically, I
      suppose, you need to check the graph is the same (isomorphic) as
      a previous graph. How do you do that and what is its time
      complexity (for a problem that is, in general, NP complete)?

    * Are you willing to provide the benchmark programs and the
      generate graphs, so that others can study them (graph size,
      granularity of each task, etc.) and better interpret the results
      in the paper? I think it's important to provide these data to
      help interested readers better understand the accomplishment.

    * How are generated graphs actually used by the runtime, other
      than for the obvious purse of determining when each task can get
      started? Are they used for, for example, determining the mapping
      of tasks to resources (i.e., automatic load balancing)?

    * You mentioned "the minimum average task granularity that can be
      efficiently supported is reduced from 598us without dynamic
      tracing to 216us with dynamic tracing," in the introductory part
      of the paper, but if I didn't miss something, the statement is
      not explained in the evaluation part of the paper. How did you
      draw this conclusion? When you say something like this, it
      suggests that the analysis overhead is constant per node (that
      is, the analysis time for a graph is linear to the number of
      nodes), and it is as parallelizable as the application. Combined
      with the previous questions, I generally like to have a better
      idea about how expensive is it to analyze the graph and whether
      it is parallelized.


Detailed Comments for Authors

    I think this is an interesting work. I view this paper as trying
    to extend the applicability of the approaches based on generating
    and analyzing task graphs at runtime, by reusing the results of
    previous analysis and thereby amortizing their cost. The paper is
    a solid and an in-depth attempt toward this goal.

    As such, I think it's important to give more information with
    which we can better understand the boundary of task graphs for
    which it is applicable (e.g., as stated in the paper, it is
    obviously not practically applicable if a task granularity is too
    small; there should be other conditions). I asked many of them
    above, but to rephrase:

    * What is the baseline cost of the graph analysis? When a reuse
      does not occur, how a graph is generated and analyzed, and how
      expensive is the analysis? Even more basic question is what the
      analysis exactly calculates. Is it just used to determine "when"
      a task can get executed? Or is it used for automatic load
      balancing, for example?

    * Is the analysis parallelized, so that it does not become a
      bottleneck with more nodes?

    * What is the cost of judging if a previous result is reusable? If
      I understand, it entails, at least in the most general case, a
      graph isomorphism problem. How do you reduce the cost?

    * Overall, I like to understand the property of task graphs (e.g.,
      the size of the graph or task granularity) for which the
      proposed approach works efficiently in practice. You left a
      statement difficult to interpret or validate in the introductory
      part of the paper (598us vs 216us). I generally do not believe
      the right characterization is as simple as this.

    * I feel Dynamic Tracing is somewhat a misnomer. What this paper
      proposes is really about reusing the previous analysis result,
      if I understand correctly. You might mean something like this
      with this terminology, but "dynamic tracing" seems to me almost
      like a synonym for dynamic graph analysis (without any emphasis
      on reusing analysis results). Along the same line, labels of
      graphs in Fig 9 (tracing vs. no tracing) are confusing, as "no
      tracing" apparently means there are no overhead associated with
      dynamic graph analysis. If I am correct, what "no tracing"
      really refers to is a version analyzing the graph every time,
      whereas "tracing" a version reusing a previous analysis result.


# Review of pap490s2 by Reviewer 4

Summary and High Level Discussion

    This paper considers the optimization of task-parallel programs
    scheduled using dynamic dependence analysis. Such analysis is
    performed online and can increase the overheads associated task
    execution, limiting the minimum granularity of tasks that can be
    efficiently handled. This paper observes that tasks produced by
    regions of code be traced, their computation dags analyzed, and the
    analysis reused across execution of the region of code.

    The key contribution of the paper is the efficient tracing of tasks in
    code regions and identifications of conditions under which a previous
    trace can be reused. Additional optimizations are presented to further
    improve performance. Evaluation clearly shows the benefits of the
    tracing strategy.

    Overall, the idea of identifying repetitive structures to track and
    reuse in terms of dependence analysis is novel in this context. It is
    also shown to clearly improve performance.


Comments for Revision/Rebuttal

    I found the explanations in Section 4 verbose and difficult to
    follow. Could these be better presented as algorithms or as semantic
    rules?

    Does the results include control replication?

    The notion of memoizing results of analyses for repetitive tasks is
    similar, in spirit, to inspector-executor models. The paper should
    cite relevant prior work on this topic.

    In Section 4E, second paragraph: in the sentence "the precondition of
    an idempotent recording sill still satisfy the precondition" should
    the first "precondition" be "postcondition"?

    Does the optimization for idempotent recordings (Section 4E) rely on
    the fact that any operations between repeated executions of a region
    be efficiently detected? Is this detection done at compile-time or
    runtime?

    Section 5c talks about parallel dependence analysis on a per-stream
    basis. However, streams are not discussed in the paper. Can you
    explain streams and what role they play in this context?

    Can you give more information on the tracing aspects of the empirical
    evaluation? How big are the trace sizes? Also, how many distinct
    traces are identified (due to distinct pre/post-conditions) per region
    of code traced?

    For the applications considered, how many regions are tracked? How big
    do the read sets get for different regions?


Detailed Comments for Authors

    Please see comments above.


# Review of pap490s2 by Reviewer 5

Summary and High Level Discussion

    This paper presents an approach for dynamically tracking task
    dependency chains, storing them and replaying them if they recur, thus
    reducing dependency tracking overheads. The authors present the
    various details of their design and showcase, through experimental
    evaluation, that their approach can help several applications scale
    better than what they otherwise would.


Comments for Revision/Rebuttal

    Please see the detailed comments.


Detailed Comments for Authors

    Task-based runtime systems are becoming increasingly important and in
    that sense, this work is quite timely for the community. Dealing with
    task dependencies that are setup at runtime (e.g., based on the input
    data) is important and this paper improves the state of art in that
    area.

    The writeup of the paper can use some improvement, though. I had to
    read the paper a few times to understand exactly what the authors are
    proposing to do. Specialization of task graphs is a very broad claim
    that the authors use in the title and initial sections, when really
    what they are trying to do is just a record-replay model. That's an
    interesting idea, but I would have preferred if the authors had been
    upfront about it and discussed the complexities in doing that.

    As a somewhat of a side comment, the general area of the work is on
    applications that have two properties: (1) their task dependencies are
    irregular and data dependent, and (2) the tasks are very small.
    Broadly speaking, such applications are generally not scalable and
    would need algorithmic improvements to make them more scalable. So,
    are the authors trying to take applications that are fundamentally not
    scalable and trying to push their range of scalability? If they are
    trying to do that, that's a fine goal too (i.e., not every application
    needs to be an Exascale application). However, it will be good for
    the authors to discuss what scalability range they are targetting so
    the readers have an idea as to where to place them with respect to the
    scale of the machines they are targetting.

    The evaluation section gives almost no detail on the applications.
    The authors say that some applications have longer traces than others.
    Why? What are the characteristics of the applications? What makes
    the traces longer?

    In summary: interesting idea, but the writeup needs more work to (1)
    first give a simple high-level picture of what you are trying to do
    without overpromising and then go into the details, (2) give an
    indication of what applications/systems you are targetting, and (3)
    give some idea of what kind of performance we can expect to see for
    each application based on its characteristics.
