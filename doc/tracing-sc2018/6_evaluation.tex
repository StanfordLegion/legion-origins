% Evaluation (1-1.5 page)

\section{Evaluation}
\label{sec:eval}

\input{fig_program_size}

We evaluate dynamic tracing on five programs ranging from quite small and regular and static, to full applications that are complex and irregular:
%
Stencil~\cite{PRK14}, a 9-point stencil benchmark on 2D grids;
%
Circuit~\cite{Regent17}, a circuit simulator for unstructured circuit graphs;
%
\Pennant~\cite{PENNANT} and MiniAero~\cite{Mantevo}, proxy applications for unstructured meshes from Los Alamos and Sandia National Laboratories;
%
and \Soleil~\cite{PSAAP2,SoleilX}, a compressible fluid solver on 3D grids developed to study turbulent fluid flow in channels.
%
All programs are written in Regent~\cite{Regent15}, a high-level programming language that targets Legion, and were run with {\it control replication}~\cite{Regent17}, an optimization that is orthogonal to dynamic tracing.
%
These programs have competitive or better weak scaling performance than reference implementations~\cite{Slaughter17}, where reference implementations are available.
%
Table~\ref{tab:size} shows benchmark metrics, the number of the tasks and copies each node must analyze per iteration.
%
Programs using unstructured meshes have indirect indexing on regions, which require dependences be resolved dynamically.
%
For the two biggest programs, MiniAero and \Soleil, Legion's dynamic task scheduling plays a crucial role in overlapping communication with computation as their tasks have parallelism due to field non-interference~\cite{LegionFields14}; i.e., a task accessing field $f$ of a region can run in parallel with a copy for field $g$ of the same region initiated by another task.
%
For three programs (Stencil, \Pennant, and MiniAero), we are able to compare with publicly available reference MPI versions.

Due to their iterative nature, all five programs have a ``main'' loop where they spend most of their execution time.
%
For Stencil, Circuit, and \Pennant, we annotate the body of this main loop.
%
For MiniAero and Soleil-X, which implement a fourth-order Runge-Kutta time marching scheme, we set the annotation on the body of this time marching loop nested within the main loop.
%
Each application has only one trace because there is no change in the task mapping and dynamic tracing is able to find one idempotent recording of the trace.
%
Identifying loops that merit annotation was trivial for these programs and could easily be automated.

We measured performance when the program reached steady state; i.e., the state where the program starts replaying a recording repeatedly.

We use GCC 5.3 to compile the Legion runtime and the MPI reference implementations.
%
Regent uses LLVM for code generation; we use LLVM 3.8.
%
We report performance for each application on up to 256 nodes of the Piz Daint supercomputer~\cite{PizDaint}, a Cray XC50 system;
%
nodes are connected by an Aries interconnect and each node has 64 GB of memory and one Intel Xeon E5-2690 CPU with 12 physical cores.

\input{fig_performance}

For strong scaling measurements, we chose problem sizes for which runs stop scaling ideally without dynamic tracing at 32 or fewer nodes.
%
Table~\ref{tab:performance} summarizes the results as throughput normalized by single node throughput without dynamic tracing.
%
Dynamic tracing improves the speedup of applications by 4.2\mytimes{} or more, except for \Pennant{}, which is improved by 2.8\mytimes.
%
Unlike the other programs, the main loop in \Pennant{} is guarded by a convergence predicate that in turn prevents a replay of the trace until the condition is resolved.
%
A trace replay overlaps with tasks only for 25\% or less of the time per iteration, which explains an improvement that is 4\mytimes{} off of the improvement in the runtime overhead.
%
Circuit shows the biggest discrepancy between the improvement in the runtime overhead and strong scaling simply because the runs did not reach a point where they are limited by the replay overhead.

To study the effect of optimizations for idempotent recordings, we also measure the performance of runs where dynamic tracing is used without those optimizations (column \noopt).
%
The use of idempotent recordings improves performance by an average of 5\% and a maximum of 19\%.
%
More importantly, dynamic tracing without optimizations sometimes perform worse than the run without dynamic tracing because of spurious dependences introduced by fences, which means fence elision is crucial.
%
The only program immune to the absence of optimizations is Circuit, which has all-to-all dependences between tasks on each node, which results in sightly longer sequences of graph calculus commands after fence elision.

For Stencil, \Pennant, and MiniAero, we also compare performance with expert-written MPI reference versions (column MPI);
%
these applications are static and well suited to MPI-style programming.
%
Note that the MPI versions of Stencil and PENNANT are 21-26\% faster than the Legion versions.
% 
Legion requires resources for its runtime system to make dynamic decisions (e.g., about tracing).
%
In these experiments the Legion runtime is configured to use 3 CPUs (out of 12) per node.
%
When the MPI versions use the same number of application processors as Regent counterparts (column 9R), MPI Stencil performs worse than the Regent version and MPI \Pennant{} is slower up to 128 nodes and becomes 6\% better on 256 nodes.
%
The MPI reference of MiniAero, which only allows the number of ranks to be a power of 2, starts 3\mytimes{} slower than the Regent version, which is consistent with~\cite{Regent17}, and loses scalability earlier.

\input{fig_granularity}

Lastly, we use MiniAero and Soleil-X to calculate the average task granularity supported by dynamic tracing.
%
In these two applications, tasks are almost completely overlapped with the runtime overhead and copies, which make them suitable for studying task granularity.
%
Table~\ref{tab:granularity} shows the minimum time per iteration and the number of tasks each processor runs, from which we derive the average task granularity.
%
The task granularity for MiniAero is half that for \Soleil{} because \Soleil{} has roughly twice as many regions per task as MiniAero, leading to twice as many copies on average to replay per task (5.4 regions per task on average vs. 3.1).

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
