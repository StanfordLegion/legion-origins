\newpage
\appendix

\input{fig_synthetic_benchmark}

\input{fig_synthetic_benchmark_task_graph}

\input{fig_runtime_overhead_synthetic}

In this appendix, we evaluate how much dynamic tracing reduces the cost of dynamic dependence analysis by measuring the runtime overhead with and without dynamic tracing.
%
We use the synthetic benchmark program in Figure~\ref{fig:synthetic-benchmark}, which has two desirable properties.
%
First, the program performs no actual computation so we can count all execution time as runtime overhead.
%
Second, the program exhibits a simple pattern of task dependencies, which allows to compute a bound on the possible improvement from dynamic tracing.
%
Each iteration of the outer most loop launches $\tt{N}$ parallel tasks $\tt{S}$ times where $\tt{N}$ is the number of CPUs remaining after allocating some for the runtime.
% 
The tasks form $\tt{N}$ chains of dependent tasks, where the $\tt{i}$th chain consists of $\tt{S}$ tasks that read and write region $\tt{A[i]}$.
%
Figure~\ref{fig:synthetic-benchmark-task-graph} illustrates the task graph of the synthetic benchmark program.

We place the tracing annotation on the outer $\tt{for}$ loop (lines 4 and 10) and vary the value of $\tt{S}$ to study the effect of trace size ($\tt{S} \cdot \tt{N}$) on the reduction of runtime overhead.
%
We also run the program with different numbers of runtime threads to measure the benefit of parallel replay.

\input{fig_diminishing_return}

Figure~\ref{fig:improvement-synthetic} shows the improvement in the runtime overhead for four configurations of parallel replay.
%
The legend shows the number of runtime threads being allocated for parallel dynamic dependence analysis and trace replay, and also the corresponding value of $\tt{N}$.
%
In all four plots, a longer trace leads to a greater improvement in the runtime overhead as it better amortizes the constant overhead of initializing every trace replay.

The plots also show that increasing the number of runtime threads has diminishing returns, which occurs for two reasons.
%
First, dynamic tracing only reduces the runtime overhead for dependence analysis and there are several other steps in Legion's task processing pipeline.
%
Second, the performance of parallel dependence analysis and trace replay scale sub-linearly in the number of runtime threads, because both parallel dependence analysis and trace replay have portions that run sequentially;
%
Legion performs a sequential preliminary analysis on tasks for parallelizing the subsequent dependence analysis and dynamic tracing sequentially initializes crossing events for parallel trace replay.
%
To better understand how these two factors incur diminishing returns, we use the following model $O_{\ms{dep}}(T)$ of runtime overhead when the number of runtime threads is $T$:
%
\begin{equation*}
  O_{\ms{dep}}(T) = C_{\ms{dep}} \cdot s(T) + \frac{C_{\ms{pipe}}}{T},
\end{equation*}
%
where $C_{\ms{dep}}$ denotes the dependence analysis overhead with one runtime thread, $C_{\ms{pipe}}$ is all the cost of Legion's task processing pipeline except for dependence analysis, and $s(T)$ models the sub-linear speedup governed by Amdahl's law; i.e.,
\begin{equation*}
  s(T) = \frac{1}{(1 - p) + p / T},
\end{equation*}
%
where $p$ is the proportion of dependence analysis that is parallelized ($0 < p < 1$).
%
In the model, we assume the cost $C_{\ms{pipe}}$ of Legion's task pipeline except for dependence analysis can be perfectly parallelized across $T$ threads as they are embarrassingly parallel.
%
The model $O_{\ms{replay}}(T)$ of the trace replay overhead when the number of runtime threads is $T$ is the same as $O_{\ms{dep}}(T)$ except that the dependence analysis overhead is replaced with the parallel trace replay overhead $C_{\ms{replay}} \cdot s(T)$:
%
\begin{equation*}
  O_{\ms{replay}}(T) = C_{\ms{replay}} \cdot s(T) + \frac{C_{\ms{pipe}}}{T},
\end{equation*}
%
where $C_{\ms{replay}}$ denotes the trace replay overhead with one runtime thread.
%
(We use the same $s(T)$ to model the sub-linearity of both parallel dependence analysis and trace replay, to simplify the analysis, though using two different models does not change the result.)
%
The improvement $I(T)$ in runtime overhead is a ratio of $O_{\ms{dep}}(T)$ to $O_{\ms{replay}}(T)$:
%
\begin{equation*}
  \displaystyle I(T) = \frac{O_{\ms{dep}}(T)}{O_{\ms{replay}}(T)} =
  \frac{C_{\ms{dep}} + C_{\ms{pipe}} / (s(T) \cdot T)}{C_{\ms{replay}} + C_{\ms{pipe}} / (s(T) \cdot T)}.
\end{equation*}
%
Note that as $T$ increases, $I(T)$ approaches asymptote $I = C_{\ms{dep}} / C_{\ms{replay}}$;
%
this means that the improvement in the dependence analysis overhead becomes a dominant component in $I(T)$.
%
Finally, the return $R(T) = I(T + 1) - I(T)$ of using an additional runtime thread when there are $T$ threads reaches $0$ as $T$ goes to infinity (i.e., $\lim_{T \to \infty}R(T) = 0$), which implies that $R(T)$ is diminishing as $T$ increases.
%
The plot of $R(T)$ in Figure~\ref{fig:diminishing-return} also clearly shows the trend of diminishing returns.
%
(For the plot, we fit our model to the experiment results by assuming that dependence analysis is 10$\times$ heavier than the rest of analysis pipeline, that 90\% of parallel dependence analysis and trace replay is perfectly parallelized, and that dynamic tracing eliminates 85\% of the dependence analysis overhead; i.e., $10C_{\ms{pipe}} = C_{\ms{dep}}$, $C_{\ms{replay}} = 0.15C_{\ms{dep}}$, and $p = 0.9$.)

Figure~\ref{fig:overhead-per-task-synthetic} shows the average runtime overhead per task with dynamic tracing.
%
Average overhead per task decreases as trace size increases and eventually saturates once the overhead for initializing trace replay is sufficiently amortized.
%
The plots exhibit a similar trend of diminishing returns as those in Figure~\ref{fig:improvement-synthetic}, but because of Amdahl's law;
%
the $C_{\ms{replay}} \cdot s(T)$ term becomes dominant in $O_{\ms{replay}}(T)$ as $T$ increases.

\input{fig_runtime_overhead}

Next, we also evaluate how much dynamic tracing reduces the runtime overhead for five applications used in Section~\ref{sec:eval}.
%
To isolate the runtime overhead from application work or communication, we apply the same methodology used for the synthetic benchmark: We modify applications to only launch tasks and run no actual computations, and we count their execution time as runtime overhead.
%
We allocate three runtime threads, the configuration used in the strong scaling runs.
%
Table~\ref{tab:runtime-overhead} summarizes the measured runtime overhead per trace.
%
In all five applications, dynamic tracing reduces the runtime overhead by more than 7\mytimes.
%
Circuit and \Pennant{} enjoy noticeably greater improvement than the others because they have reduction tasks and copies that make dynamic dependence analysis more expensive.
%
Table~\ref{tab:runtime-overhead} also shows the one-time cost for trace optimization, which is just a few milliseconds even for the longest trace.

The improvement in runtime overhead gives an upper bound on the possible improvement in strong scaling performance.
%
The actual strong scaling improvement in Section~\ref{sec:eval} is influenced by many factors (such as inter-node communication) of which runtime overhead is just one, though it is often the most important one.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
