\subsection{Recording Dependence Analysis}
\label{subsec:record}

Dynamic tracing starts with the {\it recorder} recording the dependence analysis of a trace.
%
A recording for a trace is initiated in two cases: when a trace has appeared for the first time, or when no recording of a trace passes the precondition check described in Section~\ref{subsec:replay}.

The recorder uses {\it graph calculus}, whose syntax is shown in Figure~\ref{fig:syntax}, to express task graphs.
%
Graph calculus uses {\it events} that signal the termination of operations.
%
An $\mtt{op}$ command has the form $e_2 := \mtt{op}(o,e_1)$.
% 
The operation $o$ begins execution after the event $e_1$ triggers, and the event $e_2$ triggers when $o$ terminates.   
%
To express multiple predecessors for an operation, the $\mtt{merge}$ command merges a set of events into an event that is triggered when the events being merged are all triggered.
%
A $\mtt{fence}$ command creates a {\it fence}, an operation that finishes only after all preceding operations terminate.
%
Fences \DIFdelbegin \DIFdel{make }\DIFdelend \DIFaddbegin \DIFadd{allow }\DIFaddend graph calculus commands \DIFdelbegin \DIFdel{self-contained for a single trace because they allow the commands to refer to operations that are }\DIFdelend \DIFaddbegin \DIFadd{to work correctly with earlier untraced parts of the execution, as the previous dependent operations potentially include operations }\DIFaddend not in the trace\DIFdelbegin \DIFdel{without knowing their events}\DIFdelend .
%
Finally, the calculus has command sequencing.

The recorder generates graph calculus commands from a dependence analysis of a trace as follows.
%
Each trace operation $o$ has a corresponding command $e_2 := \mtt{op}(o,e_1)$.
%
The termination event $e_2$ is unique (is not used on the left-hand side of any other $\mtt{op}$ command).
%
The event $e_1$ is the merge (using a $\mtt{merge}$ command) of the termination events of $o$'s dependence predecessors in the trace.
%
For example, in Figure~\ref{fig:recording}, task instance $\mtt{T_2(R^{\nodeb}, S^{\nodea})}$ has two predecessors $\mtt{T_1(R^{\nodea}, S^{\nodea})}$ and $\cp{R^{\nodea}}{R^{\nodeb}}$, whose events $\mtt{e_2}$ and $\mtt{e_3}$ are merged into $\mtt{e_4}$.
%
If there is no predecessor (e.g., because this is the first operation of the trace), a fence is introduced to safely capture \DIFdelbegin \DIFdel{the operation's }\DIFdelend \DIFaddbegin \DIFadd{any }\DIFaddend dependences on those operations that are not recorded.
%
Task instance $\mtt{T_1(R^{\nodea}, S^{\nodea})}$ in Figure~\ref{fig:recording} uses fence $\mtt{e_1}$ as it has no predecessor in the trace.

When the recorder reaches the end of the trace, the recorder inserts an $\mtt{op}$ statement for a {\it summary} operation, \DIFdelbegin \DIFdel{treated as }\DIFdelend a task instance that writes to all region instances used in the trace\DIFdelbegin \DIFdel{, providing in effect a trace-local fence operation using graph calculus commands;
}\DIFdelend \DIFaddbegin \DIFadd{.
}\DIFaddend %
\DIFdelbegin \DIFdel{the key distinction }\DIFdelend \DIFaddbegin \DIFadd{The key difference }\DIFaddend between a fence and a summary operation is that a fence waits on all the preceding operations, both within and out of the current trace, whereas the summary operation has dependences only on operations within the trace.
%
Any subsequent operation that has dependences on any of the replayed operations can safely catch the dependences transitively through the summary operation.

The recorder also computes the \DIFdelbegin \DIFdel{precondition and postcondition }\DIFdelend \DIFaddbegin {\em \DIFadd{precondition}} \DIFadd{and }{\em \DIFadd{postcondition}} \DIFaddend of recorded commands, which are used in the replaying stage;
%
the precondition is a set of region instances that must be valid for recorded commands to replay the same subgraph as the original dependence analysis;
%
the postcondition is a set of region instances that become valid after recorded commands replay a subgraph.
%
The precondition and postcondition are computed by processing \DIFdelbegin \DIFdel{the }\DIFdelend trace operations in order, beginning with \DIFdelbegin \DIFdel{an }\DIFdelend empty pre and postconditions, and applying the following rules:
% 
\begin{itemize}
\item If rule \Ra{} was applied to the region instance and the region instance is not in the postcondition,
  that region instance is added to the precondition.
\item If rule \Rb{} was applied to the region instance and the source instance of the copy is not in the postcondition, that source instance is added to the precondition.
  % 
  The target instance of the copy is added to the postcondition\DIFdelbegin \DIFdel{unconditionally}\DIFdelend .
\item If rule \Wa{} was applied to the region instance, the postcondition of that region is cleared and that region instance is added to the postcondition.
\end{itemize}
\DIFdelbegin %DIFDELCMD < 

%DIFDELCMD < %%%
\DIFdelend %%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
