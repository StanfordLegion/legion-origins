\DIFdelbegin %DIFDELCMD < \input{fig_dep_analysis}
%DIFDELCMD < 

%DIFDELCMD < %%%
\DIFdelend \subsection{Baseline Dependence Analysis}
\label{sec:dep-analysis}

Dynamic tracing can specialize any {\it correct} dependence analysis that generates a task graph as its result.
%
A correct dependence analysis satisfies the following two conditions.

First, a task graph from a correct dependence analysis of tasks captures all dependences between them.
%
Specifically, if task instances $T_1$ and $T_2$ are dependent and $T_2$ is issued after $T_1$, there must be at least one path from $T_1$ to $T_2$ in the task graph.
%
However, task graphs may have edges for {\it transitive} dependences, i.e, dependences that are transitively expressed by other dependences.
%
For example, the task graph is permitted to have an edge between task instances $T_1$ and $T_3$ even if it already has edges between $T_1$ and $T_2$, and between $T_2$ and $T_3$.
%
Transitive dependences are not harmful for parallelism, because they impose no additional constraints, and dependence analysis algorithms often include them in the result as additional analysis would be required to remove them.

Second, a correct dependence analysis guarantees coherence.
%
Each region instance used in a task instance must be {\it valid}, i.e., containing the latest updates to the region.
%
We assume that a correct dependence analysis algorithm keeps track of valid instances of each region using the following rules:
%
\begin{itemize}

\item[\Ra] If the task has read permission on the region and the region instance is already valid, no data movement is necessary for coherent read access.

\item[\Rb] If the task has read permission on the region and the region instance $r$ is not valid, then $r$ is made valid by issuing a copy from an existing valid instance to $r$.
  %
  The issued copy is treated as a task that reads from the source instance and writes to $r$, except it does not invalidate valid instances.

\item[\Wa] If the task has write permission on the region, all \DIFaddbegin \DIFadd{other }\DIFaddend valid instances are invalidated and the \DIFaddbegin \DIFadd{written }\DIFaddend region instance becomes the sole valid instance of the region.

\end{itemize}

Figure~\ref{fig:dep-analysis} illustrates one possible dependence analysis of three task instances.
%
For each task instance, the figure shows changes in the list of valid instances and the resulting task graph.
%
The line under each valid instance denotes the coherence rule applied.
%
Note that task graphs \DIFdelbegin \DIFdel{in the figure are still correct even though they }\DIFdelend contain edges for transitive dependences between $\mtt{T_1(R^{\nodea}, S^{\nodea})}$ and  $\mtt{T_2(R^{\nodeb}, S^{\nodea})}$, and between $\mtt{T_1(R^{\nodea}, S^{\nodea})}$ and $\mtt{T_3(R^{\nodea}, S^{\nodea})}$.
\zap{
%
This can be due to an algorithm analyzing each region instance of a task instance separately, which is common in practice.
}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
