\section{Programming Model}
\label{sec:model}

% Legion programming model primer
% tasks, regions, region instances
% mapping

We consider a task-based programming model where a program is decomposed into {\it tasks}.
%
A task is a unit of execution that runs to completion once scheduled on a processor.
%
Tasks store data in {\it regions};
%
a region is simply a named collection of data used by a task.
%
For dynamic tracing the concept of a region is flexible and can be used to name any arbitrary collection of data including, but not limited to, opaque serialized data, an array, an arena, a relation, etc. 
%
The example in Figure~\ref{fig:ex-program} has four regions: $\mtt{A[0]}$, $\mtt{A[1]}$, $\mtt{B[0]}$, and $\mtt{B[1]}$.
%
Tasks declare {\it permissions} on regions (line 1--2 in Figure~\ref{fig:ex-program}), which describe how they access data.
%
For simplicity, we consider only {\it read} and {\it write} permissions, though some systems~\cite{Legion12,SuperGlue15} provide a reduction permission for updates with commutative and associative operators.

A region can be represented by multiple {\it region instances} in different memories.
%
In Figure~\ref{fig:ex-program}, region $\mtt{A[0]}$ has two region instances $\mtt{A[0]}^{\nodea}$ and $\mtt{A[0]}^{\nodeb}$.
%DIF < 
\DIFdelbegin \DIFdel{We assume that region instances are all unique and each region instance represents one region.
}\DIFdelend \DIFaddbegin \zap{
%
We assume that region instances are all unique and each region instance represents one region.}
\DIFaddend 

When a program is executed, it makes a sequence of task calls, each of which goes through a standard pipeline of phases \cite{Legion12,StarPU16,PARSEC17}.
%
First, regions are {\em mapped} to region instances (assigned to physical memories).
%
An invocation of a task whose regions are mapped is \DIFdelbegin \DIFdel{called }\DIFdelend a {\it task instance}.
%
The mapping does not change during execution of a task instance, but can be different in different task instances of the same task.
%
In Figure~\ref{fig:stream}, region $\mtt{A[1]}$ for task $\mtt{G(A[1])}$ is mapped to $\mtt{A[1]}^{\nodea}$ in trace $k - 1$, whereas it is mapped to $\mtt{A[1]}^{\nodeb}$ in trace $k$.
%
The {\em mapper} is the pipeline stage that makes mapping decisions for tasks according to some (possibly dynamic) policy.

\zap{
Tasks can have only benign aliasing of regions;
%
if a task gets passed the same region multiple times, none of them can have write permission.
%
We call a task {\it well-defined} if it has only benign aliasing of regions.
%
For example, task $\mtt{T(R, R)}$ is not well-defined if it has write permission on both inputs, whereas $\mtt{T(R, S)}$ is well-defined.
%
Task instances of a well-defined task are also well-defined.
}

The next stage in processing a task is {\em dependence analysis}.
%
Two task instances have a {\em dependence} when they access the same region instance and at least one of them has write permissions on the region.
%
In Figure~\ref{fig:tracekp1}, task instances $\mtt{F(A[0]^{\nodea})}$ and $\mtt{G(A[0]^{\nodea})}$ are dependent because both write to the same region instance $\mtt{A[0]}^{\nodea}$, while $\mtt{F(A[0]^{\nodea})}$ and $\mtt{F(A[1]^{\nodeb})}$ are independent, and thus can run in parallel, as they write to two different region instances.

Any access to a region in a task must be {\it coherent}.
%
If a task instance updates a region instance, any subsequent task instances reading region instances of the same region must see the update.
%
\DIFdelbegin \DIFdel{We assume in our model that this }\DIFdelend \DIFaddbegin \DIFadd{In our model maintaining coherence }\DIFaddend is the responsibility of the system.
%
The program specifies what data is to be used, and the programming system manages coherence by automatically generating copies and inserting synchronization to ensure the data is \DIFdelbegin \DIFdel{kept }\DIFdelend current when and where it is needed.

Once dependence analysis is complete for a task instance, the task instance and any required copies are inserted into the {\em task graph}, a DAG where nodes are {\em operations} (task instances and copies) and edges are dependences between operations.
%
The runtime's execution of the graph is concurrent with the graph generation.
% 
The runtime finds operations that have no predecessors in the task graph, and schedules their execution on processors.
%
The mapper's choice of instances for regions constrain a task instance to only run on processors that are able to directly access those instances.

\DIFdelbegin %DIFDELCMD < \input{fig_annotated_program}
%DIFDELCMD < 

%DIFDELCMD < %%%
\DIFdelend We assume that traces are explicitly delimited in a program.
%
A trace is a sequence of task instances that are issued between a \ttb{begin\_trace} and a matching \ttb{end\_trace} statement.
%
\DIFdelbegin \DIFdel{Figure~2 shows a trace annotation for the program in }\DIFdelend \DIFaddbegin \DIFadd{In our experience it is obvious where to add tracing commands, such as around important loops.
%DIF > 
Consider the following example from }\DIFaddend Figure~\ref{fig:ex-program}, which delimits all traces in Figure~\ref{fig:stream}\DIFdelbegin \DIFdel{.
}\DIFdelend \DIFaddbegin \DIFadd{:
%DIF > 
}\begin{Verbatim}[numbers=left,commandchars=\\\{\},xleftmargin=3mm,fontsize=\small,firstnumber=4]
\textbf{\DIFadd{while}} \DIFadd{* }\textbf{\DIFadd{do}}
  \colorbox{lightgray}{\textbf{begin_trace}\zap{\m{\tt (0)}}}
  \textbf{\DIFadd{for}} \m{\tt i} \DIFadd{= }\m{\tt 0}\DIFadd{,}\m{\tt 2} \textbf{\DIFadd{do}} \m{\tt F(A[i])} \textbf{\DIFadd{end}}
  \textbf{\DIFadd{for}} \m{\tt i} \DIFadd{= }\m{\tt 0}\DIFadd{,}\m{\tt 2} \textbf{\DIFadd{do}} \m{\tt G(A[h(i)])} \textbf{\DIFadd{end}}
  \colorbox{lightgray}{\textbf{end_trace}\zap{\m{\tt (0)}}}
\textbf{\DIFadd{end}}
\end{Verbatim}
\DIFaddend %
%No nested trace annotations are permitted as demonstrated in Figure~\ref{fig:invalid-annotation}.
%
%Figure~\ref{fig:invalid-annotation} shows two additional examples of invalid trace annotation.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
