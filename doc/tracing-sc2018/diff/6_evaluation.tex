%DIF >  Evaluation (1-1.5 page)
\DIFaddbegin 

\section{\DIFadd{Evaluation}}
\label{sec:eval}

\input{fig_runtime_overhead}
\input{fig_performance}

\subsection{\DIFadd{Experimental Setup}}

\DIFadd{We evaluate dynamic tracing on five programs ranging from quite small and regular and static, to full applications that are complex and irregular:
%DIF > 
Stencil~\mbox{%DIFAUXCMD
\cite{PRK14}}\hspace{0pt}%DIFAUXCMD
, a 9-point stencil benchmark on 2D grids;
%DIF > 
Circuit~\mbox{%DIFAUXCMD
\cite{Regent17}}\hspace{0pt}%DIFAUXCMD
, a circuit simulator for unstructured circuit graphs;
%DIF > 
}\DIFadd{\Pennant}\DIFadd{~\mbox{%DIFAUXCMD
\cite{PENNANT} }\hspace{0pt}%DIFAUXCMD
and MiniAero~\mbox{%DIFAUXCMD
\cite{Mantevo}}\hspace{0pt}%DIFAUXCMD
, proxy applications for unstructured meshes from Los Alamos and Sandia National Laboratories;
%DIF > 
and }\DIFadd{\Soleil}\DIFadd{~\mbox{%DIFAUXCMD
\cite{PSAAP2,SoleilX}}\hspace{0pt}%DIFAUXCMD
, a compressible fluid solver on 3D grids developed to study turbulent fluid flow in channels.
%DIF > 
All programs are written in Regent~\mbox{%DIFAUXCMD
\cite{Regent15}}\hspace{0pt}%DIFAUXCMD
, a high-level programming language that targets Legion, and were run with }{\it \DIFadd{control replication}}\DIFadd{~\mbox{%DIFAUXCMD
\cite{Regent17}}\hspace{0pt}%DIFAUXCMD
, an optimization that is orthogonal to dynamic tracing.
%DIF > 
These programs have competitive or better weak scaling performance than reference implementations~\mbox{%DIFAUXCMD
\cite{Slaughter17}}\hspace{0pt}%DIFAUXCMD
, where reference implementations are available.
%DIF > 
Table~\ref{tab:size} shows benchmark metrics, the number of the tasks and copies each node must analyze per iteration.
%DIF > 
Programs using unstructured meshes have indirect indexing on regions, which require dependences be resolved dynamically.
%DIF > 
For the two biggest programs, MiniAero and }\DIFadd{\Soleil}\DIFadd{, Legion's dynamic task scheduling plays a crucial role in overlapping communication with computation as their tasks have parallelism due to field non-interference~\mbox{%DIFAUXCMD
\cite{LegionFields14}}\hspace{0pt}%DIFAUXCMD
; i.e., a task accessing field $f$ of a region can run in parallel with a copy for field $g$ of the same region initiated by another task.
%DIF > 
For three programs (Stencil, }\DIFadd{\Pennant}\DIFadd{, and MiniAero), we are able to compare with publicly available reference MPI versions.
}

\DIFadd{Due to their iterative nature, all five programs have a ``main'' loop where they spend most of their execution time.
%DIF > 
For Stencil, Circuit, and }\DIFadd{\Pennant}\DIFadd{, we annotate the body of this main loop.
%DIF > 
For MiniAero and Soleil-X, which implement a fourth-order Runge-Kutta time marching scheme, we set the annotation on the body of this time marching loop nested within the main loop.
%DIF > 
Each application has only one trace because there is no change in the task mapping and dynamic tracing is able to find one idempotent recording of the trace.
%DIF > 
Identifying loops that merit annotation was trivial for these programs and could easily be automated.
}

\DIFadd{We measured performance when the program reached steady state; i.e., the state where the program starts replaying a recording repeatedly.
}\zap{
%
We separately report time to optimize the recorded graph calculus commands.}

\DIFadd{We use GCC 5.3 to compile the Legion runtime and the MPI reference implementations.
%DIF > 
Regent uses LLVM for code generation; we use LLVM 3.8.
%DIF > 
We report performance for each application on up to 256 nodes of the Piz Daint supercomputer~\mbox{%DIFAUXCMD
\cite{PizDaint}}\hspace{0pt}%DIFAUXCMD
, a Cray XC50 system;
%DIF > 
nodes are connected by an Aries interconnect and each node has 64 GB of memory and one Intel Xeon E5-2690 CPU with 12 physical cores.
}

\subsection{\DIFadd{Runtime Overhead}}

\DIFadd{We first measure how much dynamic tracing reduces the cost of dynamic dependence analysis.
%DIF > 
We measure the runtime overhead with and without dynamic tracing with applications modified to only launch tasks and run no actual computation and then counting all execution time as runtime overhead.
%DIF > 
We allocate three runtime threads for parallel dynamic dependence analysis and trace replay, the configuration used in the strong scaling runs as well.
}

\DIFadd{Table~\ref{tab:runtime-overhead} summarizes the measured runtime overhead per trace.
%DIF > 
In all five applications, dynamic tracing reduces the runtime overhead by more than 7}\mytimes\DIFadd{.
%DIF > 
Circuit and }\DIFadd{\Pennant{} }\DIFadd{enjoy noticeably greater improvement than the others because they have reduction tasks and copies that make dynamic dependence analysis more expensive.
%DIF > 
Table~\ref{tab:runtime-overhead} also shows the one-time cost for trace optimization, which is just a few milliseconds even for the longest trace.
}

\DIFadd{The improvement in runtime overhead gives an upper bound on the possible improvement in strong scaling performance.
%DIF > 
The actual strong scaling improvement is influenced by many factors, of which runtime overhead is just one.
}

\subsection{\DIFadd{Strong Scaling Performance}}

\DIFadd{For strong scaling measurements, we chose problem sizes for which runs stop scaling ideally without dynamic tracing at 32 or fewer nodes.
%DIF > 
Table~\ref{tab:performance} summarizes the results as throughput normalized by single node throughput without dynamic tracing.
%DIF > 
Dynamic tracing improves the speedup of applications by 4.2}\mytimes{} \DIFadd{or more, except for }\DIFadd{\Pennant{}}\DIFadd{, which is improved by 2.8}\mytimes\DIFadd{.
%DIF > 
Unlike the other programs, the main loop in }\DIFadd{\Pennant{}} \DIFadd{is guarded by a convergence predicate that in turn prevents a replay of the trace until the condition is resolved.
%DIF > 
A trace replay overlaps with tasks only for 25\% or less of the time per iteration, which explains an improvement that is 4}\mytimes{} \DIFadd{off of the improvement in the runtime overhead.
%DIF > 
Circuit shows the biggest discrepancy between the improvement in the runtime overhead and strong scaling simply because the runs did not reach a point where they are limited by the replay overhead.
}

\DIFadd{To study the effect of optimizations for idempotent recordings, we also measure the performance of runs where dynamic tracing is used without those optimizations (column }\noopt\DIFadd{).
%DIF > 
The use of idempotent recordings improves performance by an average of 5\% and a maximum of 19\%.
%DIF > 
More importantly, dynamic tracing without optimizations sometimes perform worse than the run without dynamic tracing because of spurious dependences introduced by fences, which means fence elision is crucial.
%DIF > 
The only program immune to the absence of optimizations is Circuit, which has all-to-all dependences between tasks on each node, which results in sightly longer sequences of graph calculus commands after fence elision.
}

\DIFadd{For Stencil, }\DIFadd{\Pennant}\DIFadd{, and MiniAero, we also compare performance with expert-written MPI reference versions (column MPI);
%DIF > 
these applications are static and well suited to MPI-style programming.
%DIF > 
Note that the MPI versions of Stencil and \Pennant{} are 21-26\% faster than the Legion versions.
%DIF >  
Legion requires resources for its runtime system to make dynamic decisions (e.g., about tracing).
%DIF > 
In these experiments the Legion runtime is configured to use 3 CPUs (out of 12) per node.
%DIF > 
When the MPI versions use the same number of application processors as Regent counterparts (column 9R), MPI Stencil performs worse than the Regent version and MPI }\DIFadd{\Pennant{}} \DIFadd{is slower up to 128 nodes and becomes 6\% better on 256 nodes.
%DIF > 
The MPI reference of MiniAero, which only allows the number of ranks to be a power of 2, starts 3}\mytimes{} \DIFadd{slower than the Regent version, which is consistent with~\mbox{%DIFAUXCMD
\cite{Regent17}}\hspace{0pt}%DIFAUXCMD
, and loses scalability earlier.
}

\input{fig_granularity}

\DIFadd{Lastly, we use MiniAero and \Soleil{} to calculate the average task granularity supported by dynamic tracing.
%DIF > 
In these two applications, tasks are almost completely overlapped with the runtime overhead and copies, which make them suitable for studying task granularity.
%DIF > 
Table~\ref{tab:granularity} shows the minimum time per iteration and the number of tasks each processor runs, from which we derive the average task granularity.
%DIF > 
The task granularity for MiniAero is half that for \Soleil{} because \Soleil{}} \DIFadd{has roughly twice as many regions per task as MiniAero (5.4 regions per task on average vs. 3.1).
}

\DIFaddend %%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
