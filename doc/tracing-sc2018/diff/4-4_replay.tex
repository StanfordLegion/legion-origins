\subsection{Replaying Dependence Analysis}
\label{subsec:replay}

The next component of dynamic tracing is to replay dependence analysis for a trace.
%
Figure~\ref{fig:replay} illustrates how the {\it replayer} replays dependence analysis for the second appearance of trace $\mtt{T_1(R^{\nodea}, S^{\nodea});T_2(R^{\nodeb}, S^{\nodea});T_3(R^{\nodea}, S^{\nodea})}$ using a recording from the first appearance of the trace.
%
First, the replayer checks that each region instance in the precondition is currently valid (Step 1).
%
If any region instance in the precondition is not valid, the replayer cannot reuse recorded commands, because the original dependence analysis of the trace would issue a copy to make that region instance valid, which is not replayed by the commands.
%
If all recordings fail to pass the precondition check, the replayer stops the current replay and the recorder starts a new recording session.
%
Otherwise, the replayer proceeds with \DIFdelbegin \DIFdel{the }\DIFdelend \DIFaddbegin \DIFadd{a }\DIFaddend recording whose precondition is satisfied.
%
In Figure~\ref{fig:replay}, the set of valid instances after task instance $\mtt{T_2(R^{\nodec},S^{\nodea})}$ is analyzed subsumes the precondition and therefore the recording can be replayed.

Next, the replayer runs recorded commands to reconstruct a subgraph (Step 2).
%
\DIFdelbegin \DIFdel{Graph calculus has very weak requirements for support from the underlying runtime system.
%DIF < 
}\DIFdelend Any explicitly parallel runtime system that supports \DIFdelbegin %DIFDELCMD < {\em %%%
\DIFdel{coordinated}%DIFDELCMD < } %%%
\DIFdel{execution of computations and data movement operations can be used to implement graph calculus.
%DIF < 
Coordinated execution implies that }\DIFdelend a synchronization primitive \DIFdelbegin \DIFdel{, like }\DIFdelend \DIFaddbegin \DIFadd{such as }\DIFaddend an event or \DIFdelbegin \DIFdel{a stream , exists to mediate dependences between computations }\DIFdelend \DIFaddbegin \DIFadd{stream that can be used to express dependences between tasks }\DIFaddend and data movement operations \DIFaddbegin \DIFadd{can implement graph calculus}\DIFaddend .
%
\DIFdelbegin \DIFdel{This is crucial for enabling graph calculuscommands to be replayed efficiently as the synchronization primitives allow us to asynchronously reconstruct the graph well in advance of when it will execute, thereby hiding any overhead.
%DIF < 
}\DIFdelend Many common runtime APIs support the requirements for graph calculus.
%
For example, both CUDA~\cite{CUDA} and OpenCL~\cite{Khronos:OpenCL} can support graph calculus via their use of streams and events respectively to mediate dependences between kernels and copy operations.
%
Furthermore, for distributed memory cases, systems like Realm~\cite{Realm14} and OCR~\cite{OCR14} have event primitives that can be used on any node to handle distributed execution of graph calculus commands for computation and data movement.

When replaying a trace, graph calculus commands execute sequentially to construct a subgraph equivalent to the one produced by the original dependence analysis.
%
The semantics of graph calculus commands is straightforward, except for the fence command.
%
A fence command creates a new fence with dependences on all operations that use any region instance used by commands in the trace.
%
However, the fence is not connected to operations that do not access any region instances used in the trace.
%
This is to prevent those operations, which are independent of the replayed subgraph, from being unnecessarily blocked by that fence.
%
In Figure~\ref{fig:replay}, all users of region instances $\mtt{R^{\nodea}}$, $\mtt{R^{\nodeb}}$, and $\mtt{S^{\nodea}}$, which are the ones used in the recorded commands, are connected to the new fence $\mtt{fence}$.
%
Note that the replayed subgraph does not contain transitive dependences between $\mtt{T_1(R^{\nodea},S^{\nodea})}$ and $\mtt{T_2(R^{\nodeb},S^{\nodea})}$, and between $\mtt{T_1(R^{\nodea},S^{\nodea})}$ and $\mtt{T_3(R^{\nodea},S^{\nodea})}$, unlike the subgraph for the first trace, due to the optimizations in Section~\ref{subsec:optimizations}.

Finally, the replayer updates the list of valid instances using the postcondition (Step 3).
%
The \DIFdelbegin \DIFdel{list of }\DIFdelend \DIFaddbegin \DIFadd{known }\DIFaddend valid instances after a replay \DIFdelbegin \DIFdel{may be inconsistent with the subgraph that was built because operations that are replayed by graph calculus }\DIFdelend \DIFaddbegin \DIFadd{of a subgraph may be incorrect because the replayed }\DIFaddend commands are not analyzed again by dependence analysis.
%
The replayer \DIFdelbegin \DIFdel{recovers consistency by tagging all }\DIFdelend \DIFaddbegin \DIFadd{ensures the system has the correct set of valid instances after replay by tagging }\DIFaddend region instances in the postcondition as valid and invalidating \DIFdelbegin \DIFdel{those that are not in the postcondition}\DIFdelend \DIFaddbegin \DIFadd{all other instances}\DIFaddend .
%
In Figure~\ref{fig:replay}, region instance $\mtt{R^{\nodec}}$ is invalidated after the replay.

Before \DIFdelbegin \DIFdel{resuming the }\DIFdelend \DIFaddbegin \DIFadd{restarting }\DIFaddend dependence analysis, the replayer \DIFdelbegin \DIFdel{needs to register the summary operation with dependence analysis }\DIFdelend \DIFaddbegin \DIFadd{reinitializes the dependence analysis state using the summary operation}\DIFaddend .
%
This \DIFdelbegin \DIFdel{is to make the replayed operations, which are not visible to dependence analysis , indirectly visible through the summary operation}\DIFdelend \DIFaddbegin \DIFadd{makes the dependence analysis aware of the net effect of the replayed operations}\DIFaddend ;
%
any subsequent operation can catch its dependences on any of the replayed operations transitively through this summary operation.
%
For example, the dependence between task instance $\mtt{T_3(R^{\nodea},S^{\nodea})}$ in the replayed graph and the subsequent copy $\ncp{R^{\nodea}}{R^{\nodec}}{3}$ is captured by those between $\mtt{T_3(R^{\nodea},S^{\nodea})}$ and the summary operation $\mtt{T_{summary}(R^{\nodeb},R^{\nodea},S^{\nodea})}$, and between $\mtt{T_{summary}(R^{\nodeb},R^{\nodea},S^{\nodea})}$ and $\ncp{R^{\nodea}}{R^{\nodec}}{3}$.

\DIFdelbegin %DIFDELCMD < \input{alg_tracing}
%DIFDELCMD < 

%DIFDELCMD < %%%
\DIFdelend Algorithm~\ref{alg:tracing} shows the complete dynamic tracing algorithm.
%
The algorithm has two modes: analysis mode ($\ms{DEP}$) and tracing mode ($\ms{TRACE}$).
%
If it is in analysis mode, the algorithm maps each task call to a task instance that goes through the normal dependence analysis.
%
Otherwise, the algorithm builds a trace of task instances until it hits the end of that trace (line 11), and it either records or replays the trace ($\mtt{RecordOrReplay}$), based on the criteria described in this section.
%
The algorithm changes from analysis mode to tracing mode when it sees the beginning of a trace (line 9), and from tracing mode to analysis mode once it finishes either a recording or a replay (line 13).
\DIFdelbegin %DIFDELCMD < 

%DIFDELCMD < \input{alg_tracing_optimized}
%DIFDELCMD < %%%
\DIFdelend 

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
