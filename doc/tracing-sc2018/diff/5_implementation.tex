\section{Implementation}
\label{sec:implementation}

We have implemented dynamic tracing in Legion, a C++ runtime system for task parallelism~\cite{Legion12}. 
%
Legion has a dependence analysis pipeline similar to the one described in Section~\ref{sec:model} and builds a task graph using Realm~\cite{Realm14}, \DIFdelbegin \DIFdel{an event-based explicitly parallel runtime for distributed memory systems}\DIFdelend \DIFaddbegin \DIFadd{a low-level system for building and executing distributed task graphs}\DIFaddend .
%
We augment Legion's existing dependence analysis to generate graph calculus programs for traces.
%
Graph calculus is implemented as a set of commands that internally call the Realm API to construct task graphs.

\zap{
In Legion, traces are delimited simply by runtime calls that are manually inserted at the beginning and the end of a sequence of tasks that the programmer wants to trace.
%
Furthermore, we require that two traces that are assigned the same name be isomorphic.
}
\zap{
%
This allows us to start replaying the command without capturing the whole trace, and also makes the replay incremental and even parallel, which we discuss in Section~\ref{subsec:par-dep}.
}

In the rest of this section, we briefly discuss the ways in which our implementation for Legion extends the algorithm presented in this paper.

\subsection{Overlapping Regions}

For simplicity of exposition, we have assumed that regions and region instances are unique and each region instance represents only one region.
%
In the Legion programming model, regions can be partitioned into subregions, and thus can overlap with each other in non-trivial ways.
%
This complicates the dependence analysis, but has no fundamental impact on how dynamic tracing generates graph calculus commands.
%
\DIFdelbegin \DIFdel{Legion's dependence analysis finds the minimal set of valid instances that cover the current instance that a task wants to make valid and each valid instance in this set introduces one copy to the target instance, which simply corresponds to one graph calculus command to be recorded.
%DIF < 
}\DIFdelend %The Legion runtime has already implemented {\em composite instances} to describe a set of valid instances that match a region and we only needed to augment this mechanism.

\subsection{Fills and Reductions}

Legion provides {\em fills}, which are lazy copies from a constant value. 
%
We incorporate fills as another kind of operation that can be used in the $\mtt{op}$ command.

As discussed in Section 3, Legion tasks can request reduction permission on regions.
%
Reduction tasks are parallelized by summarizing the update from each task into a temporary instance and lazily aggregating such instances to compute the final value when it is requested in subsequent tasks.
%DIF < 
\DIFdelbegin \DIFdel{Temporary instances are initialized with an identity value before they are passed to the tasks.
%DIF < 
For reductions we also record the initialization of their temporary instances using fills.
}\DIFdelend 

\DIFdelbegin %DIFDELCMD < \input{fig_performance}
%DIFDELCMD < 

%DIFDELCMD < %%%
\DIFdelend \subsection{Parallel Dependence Analysis}
\label{subsec:par-dep}

\DIFdelbegin \DIFdel{The Legion runtime handles concurrent, distributed task streams.
%DIF < 
In the Legion programming model, a task }\DIFdelend \DIFaddbegin \DIFadd{A task }\DIFaddend can launch subtasks, and thus tasks that run in parallel can generate their own streams of tasks.
%
\DIFdelbegin \DIFdel{Fortunately, each stream of sub-tasks launched by a parent task is analyzed independently and requires no synchronization with other streams, as the }\DIFdelend \DIFaddbegin \DIFadd{The }\DIFaddend programming model guarantees \DIFdelbegin \DIFdel{any }\DIFdelend \DIFaddbegin \DIFadd{the }\DIFaddend children of independent tasks \DIFdelbegin \DIFdel{to be independent as well }\DIFdelend \DIFaddbegin \DIFadd{are also independent }\DIFaddend and thus concurrent task streams are \DIFdelbegin \DIFdel{also }\DIFdelend independent of each other\DIFaddbegin \DIFadd{~\mbox{%DIFAUXCMD
\cite{Legion12}}\hspace{0pt}%DIFAUXCMD
}\DIFaddend .
%
We have extended dynamic tracing to support concurrent, distributed task streams by taking separate recordings per stream and replaying them independently\DIFdelbegin \DIFdel{within each stream}\DIFdelend \DIFaddbegin \DIFadd{.
}

\DIFadd{The Legion runtime also pipelines dependence analysis of task streams.
%DIF > 
The recording procedure of graph calculus commands is divided into several steps, one for each pipeline phase.
}

\input{fig_parallel_replay}
\input{fig_program_size}

\subsection{\DIFadd{Parallel Trace Replay}}
\label{subsec:par-replay}

\DIFadd{As sequential replay of a trace can become a performance bottleneck, we implemented parallel replay of a trace.
%DIF > 
Figure~\ref{fig:parallel-replay} illustrates the key transformation for parallel replay.
%DIF > 
A trace is split into }{\em \DIFadd{slices}}\DIFadd{.
%DIF > 
Commands appear in slices in the same order as the original trace and any events that are created in one slice and referenced in other slices are connected using the graph calculus extension shown in the figure.
%DIF > 
A command $\mtt{e := event}$ creates a new untriggered event and assigns it to an event variable $\mtt{e}$.
%DIF > 
A command $\mtt{trigger(e_1, e_2)}$ registers an event dependence such that event $\mtt{e_1}$ is notified as soon as $\mtt{e_2}$ is triggered, which simply corresponds to adding an edge between the operations represented by $\mtt{e_2}$ and $\mtt{e_1}$.
%DIF > 
Slices generated from a trace can be replayed in parallel.
}

\DIFadd{Minimizing events that ``cross'' the slice boundary is important in reducing the number of intermediary events for parallel replay, for which we exploit users' knowledge of task mappings;
%DIF > 
we put tasks mapped to the same processor in the same slice as much as possible because they likely have dependences on one another}\DIFaddend .

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
