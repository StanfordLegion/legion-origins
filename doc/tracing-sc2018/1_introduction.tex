\section{Introduction}

\input{fig_example}

% Programming big machines is hard
Programming large distributed systems is a challenging problem in both high performance computing (HPC) and data center environments today. 
% Task graphs are good at programming big machines
To address this problem, there has recently been a resurgence of programming systems that target these machines by expressing computations as task graphs~\cite{Legion12,Realm14,StarPU11,PARSEC17,TensorFlow15,Nimbus17,CnC14}. 
% This is what a task graph is
The nodes in a task graph represent opaque computations and edges represent either control or data dependences constraining the execution order of the graph.
% Task graphs are very useful
Many problems in parallel programming such as mapping~\cite{Legion12}, scheduling~\cite{OmpSs15}, load balancing~\cite{Charm11}, fault tolerance~\cite{Kurt14}, and resource allocation~\cite{Legion12,CnC14} have been successfully addressed using task graphs.

% Task graphs are hard for end users
While tasks graphs are useful tools for programming systems, they can be difficult to work with directly for application developers as they can be challenging to correctly generate and compose by hand.
% Therefore there is a translation layer that does dependence analysis
For this reason, many systems provide higher-level abstractions and rely on an additional translation layer to perform a dependence analysis that constructs an underlying task graph~\cite{Uintah10,Legion12,StarPU11,TensorFlow15,PARSEC17}.
% Dependence analysis is easy when iterative
In some cases the task graph is static and can be constructed once at the beginning of an application \cite{TensorFlow15,Nimbus17,CnC14}.
% Not everything is iterative
However, in more dynamic applications, the computation can be dependent on application data and therefore dependence analysis and the shape of the task graph must also be computed at runtime.
% Therefore more dynamic systems exist
Systems such as Legion~\cite{Legion12}, StarPU~\cite{StarPU11}, and PaRSEC~\cite{PARSEC17} are built around a continuously running dependence analysis that constructs a task graph on the fly, enabling them to adapt to applications with changing requirements.

% Dependence analysis is expensive
While a fully dynamic dependence analysis is very flexible, it also incurs runtime overhead that can limit performance.
% For any program there is a minimum granularity of task
The cost of dynamic dependence analysis can be hidden (by running the analysis in parallel with the application) only if the cost of analyzing a task is on average less than the task's execution time~\cite{Legion12}. 
% Minimum granularity of a task
Therefore the cost of dynamic dependence analysis places a lower bound on the granularity of tasks that can be handled efficiently and how well applications strong scale. 
% Importance of scaling
Consequently dynamic dependence analysis must be as efficient as possible to avoid limiting system performance.

% But we shouldn't have to pay for it all the time
The crucial insight of this work is that, while some applications require a fully dynamic dependence analysis, they often have {\em traces} of repetitive tasks for which we can memoize the results of the dynamic dependence analysis and therefore reduce the overhead of executing tasks in a trace.
% Analogy to tracing JIT
While similar in spirit to trace-based JIT-compilation systems~\cite{Gal09,Chang09,Tracing10,SPUR10,Tracing11,TCAI14}, specializing dynamic dependence analysis for parallel and distributed systems raises new correctness and performance issues, because unlike programming systems for shared-memory machines, distributed task graphs must express both parallelism and the {\it coherence} of data.
%
For example, dynamic dependence analyses for distributed systems can generate different subgraphs for the same trace based on the location(s) of the most recent version of data. 
%
Therefore every replay of a specialized trace needs to maintain the coherence of data, an issue that does not arise in shared-memory environments where data coherence is maintained by the underlying hardware.

% What our system is 
To address these issues, we present {\em dynamic tracing}, a technique to efficiently and correctly memoize a dynamic dependence analysis and generate a task graph semantically equivalent to (but also often syntactically different from) the original.
% How does it work
Dynamic tracing achieves this goal in three steps.
% Capture graphs
First, it records the analysis of a trace as a sequence of {\em graph calculus} commands;
% What is graph calculus
graph calculus is a simple imperative language with commands that directly construct task graphs.
%
The recorded graph calculus commands are associated with a {\em precondition} that must be satisfied for the commands to correctly replay the task graph, and a {\em postcondition} that must be applied to make the dependence analysis state consistent with the replayed graph.
%
Second, it optimizes the commands to minimize the cost of replay and eliminate unnecessary synchronizations in the replayed subgraph.
% Replay graphs
Third, whenever a previously recorded trace appears during program execution, the recorded graph calculus commands are replayed to replace the dependence analysis as long as the trace's precondition is satisfied.

This paper makes three contributions:

\begin{itemize}
\item  To the best of our knowledge, dynamic tracing is the first technique to just-in-time specialize task graphs in distributed task-based runtimes with dynamic dependence analysis.
  %
  We present a complete design of dynamic tracing with several key optimizations.

\item We describe an implementation of dynamic tracing embedded in the Legion runtime system.

\item For five already optimized applications, we demonstrate that dynamic tracing improves strong scaling performance up to 7.0\mytimes, and by 4.9\mytimes{} on average, when running on up to 256 nodes.
\end{itemize}

The rest of this paper is organized as follows.
%
In Section~\ref{sec:overview}, we give an informal overview of dynamic tracing.
%
Section~\ref{sec:model} describes our programming model and defines basic concepts.
%
Then, we present dynamic tracing in Section \ref{sec:tracing}.
%
Section~\ref{sec:implementation} discusses the implementation of dynamic tracing in Legion and Section~\ref{sec:eval} presents experiment results.
%
We survey related work in Section~\ref{sec:related} and conclude in Section~\ref{sec:conclusion}.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
