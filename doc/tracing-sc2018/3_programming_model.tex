\section{Programming Model}
\label{sec:model}

We consider a task-based programming model where a program is decomposed into {\it tasks}.
%
A task is a unit of computation that runs to completion once scheduled on a processor.
%
Tasks store data in {\it regions};
%
a region is simply a named collection of data used by a task.
%
For dynamic tracing the concept of a region is flexible and can be used to name any arbitrary collection of data including, but not limited to, opaque serialized data, an array, an arena, a relation, etc. 
%
The example in Figure~\ref{fig:ex-program} has four regions: $\mtt{A[0]}$, $\mtt{A[1]}$, $\mtt{B[0]}$, and $\mtt{B[1]}$.
%
Tasks declare {\it permissions} on regions (line 1--2 in Figure~\ref{fig:ex-program}), which describe how they access data.
%
For simplicity, we consider only {\it read} and {\it write} permissions, though some systems~\cite{Legion12,SuperGlue15} provide a reduction permission for updates with commutative and associative operators.

A region can be represented by multiple {\it region instances} in different memories.
%
In Figure~\ref{fig:ex-program}, region $\mtt{A[0]}$ has two region instances $\mtt{A[0]}^{\nodea}$ and $\mtt{A[0]}^{\nodeb}$.

When a program is executed, it makes a sequence of task calls, each of which goes through a standard pipeline of phases \cite{Legion12,StarPU16,PARSEC17}.
%
First, regions are {\em mapped} to region instances (assigned to physical memories).
%
An invocation of a task whose regions are mapped is a {\it task instance}.
%
The mapping does not change during execution of a task instance, but can be different in different task instances of the same task.
%
In Figure~\ref{fig:stream}, region $\mtt{A[1]}$ for task $\mtt{G(A[1])}$ is mapped to $\mtt{A[1]}^{\nodea}$ in trace $k - 1$, whereas it is mapped to $\mtt{A[1]}^{\nodeb}$ in trace $k$.
%
The {\em mapper} is the pipeline stage that makes mapping decisions for tasks according to some (possibly dynamic) policy.

The next stage in processing a task is {\em dependence analysis}.
%
Two task instances have a {\em dependence} when they access the same region instance and at least one of them has write permissions on the region.
%
In Figure~\ref{fig:tracekp1}, task instances $\mtt{F(A[0]^{\nodea})}$ and $\mtt{G(A[0]^{\nodea})}$ are dependent because both write to the same region instance $\mtt{A[0]}^{\nodea}$, while $\mtt{F(A[0]^{\nodea})}$ and $\mtt{F(A[1]^{\nodeb})}$ are independent, and thus can run in parallel, as they write to two different region instances.

Any access to a region in a task must be {\it coherent}.
%
If a task instance updates a region instance, any subsequent task instances reading region instances of the same region must see the update.
%
In our model maintaining coherence is the responsibility of the system.
%
The program specifies what data is to be used, and the programming system manages coherence by automatically generating copies and inserting synchronization to ensure the data is current when and where it is needed.

Once dependence analysis is complete for a task instance, the task instance and any required copies are inserted into the {\em task graph}, a DAG where nodes are {\em operations} (task instances and copies) and edges are dependences between operations.
%
The runtime's execution of the graph is concurrent with the graph generation.
% 
The runtime finds operations that have no predecessors in the task graph, and schedules their execution on processors.
%
The mapper's choice of instances for regions constrain a task instance to only run on processors that are able to directly access those instances.

We assume that traces are explicitly delimited in a program.
%
A trace is a sequence of task instances that are issued between a \ttb{begin\_trace} and a matching \ttb{end\_trace} statement.
%
At least some of the places that tracing can be beneficial are obvious, such as around important loops.
%
Consider the following example from Figure~\ref{fig:ex-program}, which delimits all traces in Figure~\ref{fig:stream}:
%
\begin{Verbatim}[numbers=left,commandchars=\\\{\},xleftmargin=3mm,fontsize=\small,firstnumber=4]
\textbf{while} * \textbf{do}
  \colorbox{lightgray}{\textbf{begin_trace}}
  \textbf{for} \m{\tt i} = \m{\tt 0},\m{\tt 2} \textbf{do} \m{\tt F(A[i])} \textbf{end}
  \textbf{for} \m{\tt i} = \m{\tt 0},\m{\tt 2} \textbf{do} \m{\tt G(A[h(i)])} \textbf{end}
  \colorbox{lightgray}{\textbf{end_trace}}
\textbf{end}
\end{Verbatim}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
