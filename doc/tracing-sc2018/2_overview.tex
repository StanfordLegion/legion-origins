\section{Overview}
\label{sec:overview}

We briefly motivate the need for dynamic tracing with a small example designed to illustrate the salient issues.
%
The program in Figure~\ref{fig:ex-program} issues four tasks $\mtt{F(A[0])}$, $\mtt{F(A[1])}$, $\mtt{G(A[h(0)])}$, $\mtt{G(A[h(1)])}$ for every iteration of the while loop.
%
Static dependence analyses will give imprecise results because of the indices computed using the opaque function $\mtt{h}$.
%
In contrast, precise dynamic dependence analysis is straightforward.
%
For example, if $\mtt{h(0)}$ = 1 and $\mtt{h(1)}$ = 0, dynamic dependence analysis shows there are dependences between $\mtt{F(A[0])}$ and $\mtt{G(A[h(1)])}$, and between $\mtt{F(A[1])}$ and $\mtt{G(A[h(0)])}$.

In a distributed system, data dependences may require data movement.
%
For example, if $\mtt{F(A[0])}$ and $\mtt{G(A[0])}$ execute on different nodes of the machine, and since $\mtt{F(A[0])}$ writes to $\mtt{A[0]}$, the updated value of $\mtt{A[0]}$ must be copied to the node where $\mtt{G(A[0])}$ will run.
%
We use node identifiers $\nodea$, $\nodeb$, etc. as superscripts to data elements to distinguish different {\em instances} of the same data on different nodes.
%
In Figure \ref{fig:trace1}, for example, the upper left operation copies an instance of $\mtt{A[1]}$ on node $\nodea$ to an instance of A[1] on node $\nodeb$.
%
Tasks execute on the node where their arguments are placed.

Figure~\ref{fig:stream} shows some {\em traces}, which are sequences of tasks issued by the program.
%
The dependence analysis of \mbox{trace $1$}, which corresponds to the while loop's second iteration, generates the task graph in Figure~\ref{fig:trace1}.
%
To ensure correctness, copy operations are added to the task graph where necessary.
%
During task graph generation, dynamic tracing memoizes the task graph using graph calculus commands (discussed in Section~\ref{subsec:record}) so the graph can be regenerated for later executions of the same trace.

Dynamic tracing detects when recorded commands can be reused using two criteria.
%
First, the subsequent trace must be exactly the same as the one from the second iteration;
% 
this requires that the tasks have the same data dependences and the choice of data placement is the same so that the set of required copies is the same.
%
Second, the set of instances that hold the most recent version of input data to the trace must be the same; in this example $\mtt{A[1]}^\nodea$ and $\mtt{A[0]}^\nodeb$ are the input data during trace $1$ when the graph is captured.
%
For this example, dynamic tracing can replace the dynamic dependence analysis for each trace from iteration $2$ to $k-1$ using the graph calculus commands captured during trace $1$.

Suppose now that during trace $k$ the choices of nodes for the data of tasks $\mtt{G(A[h(0)])}$ and $\mtt{G(A[h(1)])}$ are swapped as shown in Figure~\ref{fig:stream}.
%
(While this change of data placement is not a realistic scenario, it illustrates the issues that arise in real applications.)
%
Trace $k$ then looks different from the first $k-1$ traces because the location of the instances for tasks $\mtt{G(A[h(0)])}$ and $\mtt{G(A[h(1)])}$ have changed, leading dynamic tracing to reject replaying the capture of trace $1$ and instead capture a new trace. 
%
However, there is an important subtlety that occurs when dynamic tracing encounters trace $k+1$.
%
Dynamic tracing cannot replay the commands from trace $k$ for trace $k+1$ because the location of the input instances are different; trace $k$ has input instances $\mtt{A[1]}^\nodea$ and $\mtt{A[0]}^\nodeb$ while trace $k+1$ has input instances $\mtt{A[1]}^\nodeb$ and $\mtt{A[0]}^\nodea$ necessitating input copy operations.
%
Therefore dynamic tracing will also need to capture commands for trace $k+1$ and will be able to replay them starting with trace $k+2$, assuming the instance placement remains stable.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
