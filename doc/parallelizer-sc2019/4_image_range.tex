\section{Generalizing Image and Preimage}

\newbox\spmv
\begin{lrbox}{\spmv}
\small
\begin{lstlisting}
for (i in Y):
  range = Ranges[i]
  for (k in range):
    Y[i] += Mat[k].val * X[Mat[k].ind]
\end{lstlisting}
\end{lrbox}

\newbox\dplspmv
\begin{lrbox}{\dplspmv}
  \small
  \begin{lstlisting}
$\partsym_1$ = equal(Y, N)
$\partsym_2$ = image($\partsym_1$, $\id$, Ranges)
$\partsym_3$ = IMAGE($\partsym_2$, Ranges$[\cdot]$, Mat)
$\partsym_4$ = image($\partsym_3$, Mat$[\cdot]$.ind, X)
\end{lstlisting}
\end{lrbox}

\begin{figure}[t]
\small
\begin{tabular}{@{}c@{}}
  \begin{minipage}[t]{\linewidth}
    {\usebox\spmv}
    \subcaption{SpMV code\label{fig:spmv-code}}
  \end{minipage}
  \\
  \begin{minipage}[t]{\linewidth}
    {\usebox\dplspmv}
    \subcaption{Synthesized DPL code\label{fig:dpl-code}}
  \end{minipage}
\end{tabular}
  \caption{SpMV example\label{fig:ex-spmv}}
\end{figure}

Some programs have loops where the iteration space is determined by values of a region, typically arising in sparse matrix algorithms.
%
The SpMV code using Compressed Sparse Row (CSR) format in Figure~\ref{fig:spmv-code} is one such example.
%
In this code, the matrix is represented by the region \ttt{Mat} where the field \ttt{val} contiguously stores the non-zero values of the matrix and the field \ttt{ind} stores the column indices of those non-zeros.
%
The inner loop at line 3 then iterates over columns of the \ttt{i}th row in the matrix using the value \ttt{Ranges[i]}, a pair of lower and upper bounds of indices in \ttt{Mat}.

These loops with {\em data dependent} iteration spaces require partitioning operators that derive partitions using functions from indices to {\em sets} of indices.
%
In Figure~\ref{fig:spmv-code}, the region \ttt{Ranges} maps each iteration of the outer loop to a set of iterations of the inner loop, and thus partitions for regions accessed in this inner loop, such as \ttt{Mat} and \ttt{X}, must be constructed by collecting (and flattening) the image of this map.
%
We can define such DPL operators \IMAGE{} and \PREIMAGE{} as follows:
%
\begin{center}
  $
  \begin{array}{@{}r@{\ }c@{\ }l@{}}
  \IMAGE{}(\expr,F,\regionsym)[i] & \triangleq & \mset{\big(l, v'\big) \in \regionsym \mid (k, v) \in \expr[i] \land l \in F(k)}
  \\
  \PREIMAGE{}(\regionsym,F,\expr)[i] & \triangleq & \mset{\big(l, v'\big) \in \regionsym \mid (k, v) \in \expr[i] \land k \in F(l)}
  \end{array}
  $
\end{center}   
%
The \image{} and \preimage{} operators in Section~\ref{sec:inference} are a special case of these operators;
%
for example, with a lifting $\funsym_{\uparrow}$ of a function $\funsym$ on indices, where $\funsym_{\uparrow}(x) = \mset{\funsym(x)}$, we have that
%
\begin{center}
  $\image{}(\expr,\funsym,\regionsym) = \IMAGE{}(\expr,\funsym_{\uparrow},\regionsym)$.
\end{center}

Our framework can handle \IMAGE{} and \PREIMAGE{} just like \image{} and \preimage{} with the following minor modifications:
%
\begin{itemize}

\item Algorithm~\ref{alg:inference} now handles inner loops with data dependent iteration spaces (which are handled similarly to assignments).

\item Lemmas \tsc{L12} and \tsc{L14} in Figure~\ref{fig:lemmas} do not hold for \IMAGE{} and \PREIMAGE{}.

\end{itemize}
%
The DPL code synthesized for the SpMV code is shown in Figure~\ref{fig:dpl-code}.

Note that the partitioning strategy in Figure~\ref{fig:dpl-code} can lead to suboptimal performance when the the number of non-zeros in each row is uneven, because the partition of the matrix is derived from an \equal{} partition of \ttt{Ranges}.
%
In this case the user can construct a balanced partition of \ttt{Ranges} using, for example, a graph partitioning heuristic, such as the one proposed by Ravishankar et al. \cite{InspectorExecutorIrregular12}, and provide it as an external constraint.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "parallelizer"
%%% End:
