\section{Introduction}
\label{sec:introduction}

Data partitioning is an essential step to exploit parallelism on distributed memory systems.
%
For example, for a data parallel loop, parallelism can be realized by partitioning the data into {\em subregions} (subcollections of the original data) and running the set of loop iterations accessing a particular subregion.
%
In general, for a given set of parallel tasks, only some partitions of the data are legal, because at a minimum the data accessed by a task must be included in that task's subregion arguments.
%
Furthermore, as programs generally have multiple data access patterns in different loops, the possible legal partitions are those satisfying all the constraints of all loops, while the performant partitions are a subset of the legal partitions.
%
In this view of program parallelization, the crux of parallelizing for distributed execution is identifying and satisfying these data partitioning constraints.

In this paper, we present a {\em constraint-based} approach to data partitioning.
%
We first extract {\em partitioning constraints} under which data partitions will preserve program execution semantics.
%
Partitioning constraints are inferred automatically from data accesses in programs and serve as a specification for implementations of data partitioning.
%
Many different partitioning strategies may satisfy a system of partitioning constraints.
%
To find implementations that match the specification, we employ a constraint solver that synthesizes partitioning code in DPL, the Dependent Partitioning Language~\cite{DependentPartitioning16}, a domain-specific language for data partitioning (we give an overview of DPL below).

The constraint solver can also exploit externally provided invariants on partitions to discharge some or all partitioning constraints
%
--- this is the mechanism by which users or other systems (e.g., external libraries) can provide additional information to the automatic partitioning algorithm about the environment in which the parallelized code will execute.
%
This feature is particularly appealing in scenarios where applying auto-parallelization to the whole program is infeasible or inefficient.
%
For example, this approach naturally handles the common case where the code to be parallelized must accept input from another program component with a fixed data partitioning.
%
Partitioning constraints serve as an interface conveying information about existing partitions from previously or manually parallelized parts to our automated data partitioning algorithm.

\input{fig_example_intro}

\subsection{Overview}
\label{subsec:overview}

We illustrate our constraint-based approach using the program in Figure~\ref{subfig:ex-loops}, which showcases a common pattern of using indirect accesses to establish relationships between different physical entities.
%
This program is excerpted from a larger program written in Regent~\cite{Regent15}, which is the language we use for all code examples and for our implementation.
%
Regent provides a sophisticated set of data partitioning primitives, and so is a natural vehicle for our work.

The program in Figure~\ref{subfig:ex-loops} stores properties of particles and cells in {\em regions} \ttt{Particles} and \ttt{Cells}.
%
A region is a collection of values.
%
All elements of a region have the same type, and every element has a unique index.
%
The values in a region may have {\em fields}, such as the \ttt{cell}, \ttt{vel}, and \ttt{acc} fields used in Figure~\ref{subfig:ex-loops}.
%
Regent provides typical looping constructs over the elements of regions.
%
The first loop iterates over \ttt{Particles} to update the position of each particle \ttt{p}.
%
The index \ttt{c} of the cell where each particle resides is stored in \ttt{Particles[p].cell} (line 2).
%
The change in each particle's position is then computed using the velocity of the cell at \ttt{c} and its neighbor \ttt{h(c)} (line 3).
%
The second loop updates the velocity of each cell similarly (line 5).

The program in Figure~\ref{subfig:ex-parallel-loops} parallelizes the loops in Figure~\ref{subfig:ex-loops} using first-class data partitions;
%
partitions are a primitive concept in Regent.
%
Each {\em partition} $\partsym_i$ is an array of {\em subregions} (i.e., subsets of a region) and each \ttb{parallel} \ttb{for} loop launches {\em tasks} for subregions in the partition, each of which runs a subset of the original loop iterations.
%
Subregions are names for subsets of collections and subregions can be recursively partitioned into subregions themselves.
%
As is standard, tasks are designated functions that can be run asynchronously (in parallel). 
%
Tasks in Regent can take regions, partitions or scalars as arguments.
\zap{
%
The Regent implementation detects and enforces any data dependencies between tasks (by analyzing the relationships between region and partition arguments and results of tasks).
%
Synchronization and data movement between tasks is automatically resolved by a runtime system~\cite{Legion12} or compiler transformations~\cite{Regent17,AffineDistributedMemory13}.}

The parallel program in Figure~\ref{subfig:ex-parallel-loops} preserves the semantics of the sequential program in Figure~\ref{subfig:ex-loops} provided the partitions $\partsym_1$, $\dots$, $\partsym_5$ are {\em legal}, i.e., when they make the following indirect accesses safe:
%
\begin{itemize}
\item \ttt{Cells1[c].vel} at line 4;
%
\item \ttt{Cells2[h(c)].vel} at line 4; and
%
\item \ttt{Cells4[h(c)].acc} at line 7.
\end{itemize}

The space of legal partitions can be expressed by {\em partitioning constraints} that are inferred automatically from programs.
%
Figure~\ref{subfig:ex-constraints} shows the partitioning constraints that capture the conditions under which $\partsym_1$, $\dots$, $\partsym_5$ in Figure~\ref{subfig:ex-parallel-loops} are legal.
%
Each node in the graph corresponds to a partition.
%
The node labeled with $\partsym_1$ denotes a partition of \ttt{Particles}, whereas the others are (potentially different) partitions of \ttt{Cells}.
%
Shaded nodes represent partitions that must be {\em complete}; a partition is complete when its subregions include all elements of the region.
%
Nodes for partitions $\partsym_1$ and $\partsym_4$ are shaded because they must cover the iteration space of the loops at lines 1 and 4.
%
Edges between nodes specify constraints on partitions.
%
The edge from $\partsym_2$ to $\partsym_3$, labeled with the function \ttt{h}, requires that each subregion $\partsym_3[j]$ contain the image of $\partsym_2[j]$ under \ttt{h}, that is, $\forall (k, v) \in \partsym_2[j].\ \exists v'.\ (\ttt{h}(k), v') \in \partsym_3[j]$.
%
The edge from $\partsym_4$ to $\partsym_5$ describes the same constraint but on $\partsym_4$ and $\partsym_5$.
%
The other edge between $\partsym_1$ and $\partsym_2$ is interpreted similarly:
%
\begin{center}
  $\forall (k, v) \in \partsym_1[i].\ \exists v'.\ \big(\ttt{Particles}[k]\ttt{.cell}, v' \big) \in \partsym_2[i]$
\end{center}

\input{fig_dpl_intro}

Figure~\ref{fig:ex-dpl-intro} gives two partitioning strategies satisfying the constraints in Figure~\ref{subfig:ex-constraints}, expressed as DPL programs that construct partitions using the high-level partitioning operators~\ttb{equal}, \ttb{image}, and \ttb{preimage}.
%
DPL is the partitioning sublanguage of Regent that computes partitions of regions at runtime.
%
DPL has additional operators but these are the most commonly used.
%
The main idea in DPL is that some partitioning operators, such as \equal{}, create partitions of regions directly, while others compute a new partition as a function of an existing partition (thus the name {\em dependent} partitioning language).
%
Sophisticated data partitions can be constructed by composing the small set of primitive DPL operators.

\input{fig_dpl_ops_intro}

In Figure~\ref{fig:ex-dpl-intro}, program A derives $\partsym_2$, $\partsym_3$, and $\partsym_5$ from \ttb{equal} partitions of $\partsym_1$ and $\partsym_4$;
%
the \ttb{equal} operator creates a complete partition of a region with (approximately) equal size subregions.
%
Partitions $\partsym_2$, $\partsym_3$ and $\partsym_5$ use \image{} partitions to satisfy the partitioning constraints.
%
The $\image{}$ operator uses an existing partition and a function to define a compatible partition of a region.
%
For example, if $\partsym_2 = \mangle{r_1, \ldots, r_n}$, then the statement $\partsym_3 = \image{}(\partsym_2,\ttt{h},\ttt{Cells})$ creates $\partsym_3 = \mangle{\ttt{h}(r_1), \ldots, \ttt{h}(r_n)}$, where $\ttt{h}(r_i) \subseteq \ttt{Cells}$.
%
Figure~\ref{subfig:ex-image} gives a visual representation of the \image{} operator: The region on the left is already partitioned into two subregions, indicated by the sets of light and dark elements.
%
The image of function \ttt{f} mapping elements of the lefthand region to elements of the righthand region then defines two subregions of the righthand region.

Program B implements a different strategy, first creating an \ttb{equal} partition of \ttt{Cells} for both $\partsym_2$ and $\partsym_4$.
%
Note that $\partsym_2$ is assigned a complete partition even though the partitioning constraint does not require it to be complete;
%
as long as $\partsym_2$ contains the image of \ttt{Particles}$[\cdot]$\ttt{.cell}, it can have extra elements.
%
To construct the partition $\partsym_1$ from the already defined $\partsym_2$, program B uses the \preimage{} operator.
%
As illustrated in Figure~\ref{subfig:ex-preimage}, \preimage{} takes an existing partition of the region on the righthand side and constructs a compatible partition using the preimage of the function; i.e., if the provided partition is $\mangle{r_1,\ldots, r_n}$ and the function is $\ttt{h}$, then the computed partition is $\mangle{\ttt{h}^{-1}(r_1),\ldots, \ttt{h}^{-1}(r_n)}$.
%
Finally, $\partsym_3$ and $\partsym_5$ are computed using the image of $\partsym_2$ under \ttt{h}.

Without any prior knowledge about \ttt{Cells} and \ttt{Particles}, it is not clear whether the partitioning strategy in Figure~\ref{subfig:ex-dpl-strategy1} or Figure~\ref{subfig:ex-dpl-strategy2} is better.
%
Program A has an additional pair of partitions of \ttt{Cells}, which is not necessarily worse than program B if communication due to the extra partitions is justified;
%
in a scenario where spatial distribution of the particles is significantly skewed, program B can suffer from load imbalance in the first loop in Figure~\ref{subfig:ex-parallel-loops}, whereas program A is immune to this issue because the subregions of $\partsym_1$ have equal size.
%
On the other hand, if the particles in each subregion of $P_1$ are spread throughout the domain, each subregion of $\partsym_2$ can be as big as \ttt{Cells}, leading to excessive communication.

\input{fig_usercons_intro}

Given that we cannot identify an optimal DPL program that satisfies the constraints at compile-time, we use heuristics to guide the constraint resolution process.
%
For example, when a given set of partitioning constraints admit multiple DPL programs, our constraint solver chooses the one with the fewest partitions (program B in this example).
%
This approach does not always produce satisfactory solutions when important information about the execution context is missing.
%
Programs also often have parts that are hard to auto-parallelize well, or may not be possible to auto-parallelize at all.
%
Our approach can gracefully handle these situations by allowing programmers to provide additional constraints encoding knowledge of which strategies are best and/or existing partitions used outside the scope of auto-parallelization.
%
For example, if the loops in Figure~\ref{subfig:ex-loops} were embedded in an outer loop where particles' pointers to cells are updated every iteration, the DPL program in Figure~\ref{subfig:ex-dpl-strategy2} would need to repartition \ttt{Particles} every iteration to reflect the updates.
%
If only a few particles change cells on each iteration, then repartitioning the entire \ttt{Particles} region is wasteful.
%
A simple way to mitigate this inefficiency is to exchange particles manually;
%
the pseudo-code in Figure~\ref{fig:ex-usercons-intro} sends a particle to the right ``owner'' whenever the cell to which the particle moves belongs to a subregion different from the current one.
%
The most important part in this pseudo code is the assertion at line 10 specifying the invariant on \ttt{pParticles} and \ttt{pCells}, i.e., that the subregion \ttt{pCells}$[i]$ contains all the cells pointed to by the particles in \ttt{pParticles}$[i]$.
%
The constraint solver uses this assertion to discharge all partitioning constraints in Figure~\ref{subfig:ex-constraints} except those on $\partsym_3$ and $\partsym_5$, for which the solver emits the following DPL program using \ttt{pCells} to derive $\partsym_3$ and $\partsym_5$:
\begin{center}
  $\partsym_3\ttt{ = }\partsym_5\ttt{ = \image(pCells, h, Cells)}$
\end{center}
%
This example demonstrates the key benefit of constraint-based approaches that separate specification from implementation~\cite{Aiken99};
%
as long as the manual particle exchange code maintains the invariant, the entire program mixing parts that are parallelized by different means is correct.
%
Furthermore, by providing constraints the user has a high-level but precise interface for informing the auto-parallelization process, and writing these interface constraints is much less work than parallelizing the entire code manually.

This paper makes the following contributions:
\begin{itemize}

\item We present the design of a static analysis that automatically infers partitioning constraints from programs.

\item We design a constraint solver that synthesizes DPL code from partitioning constraints.

\item We evaluate the implementation of our constraint-based approach using the Regent compiler~\cite{Regent15}.
  % 
  For a set of Regent programs that are already hand-optimized for distributed memory execution~\cite{Regent17,Legion18}, their sequential counterparts auto-parallelized in our approach achieved comparable performance (within 5\%).

\zap{
\item \Soleil{}
}

\end{itemize}

In the following sections, we design the static analysis for constraint inference (Section~\ref{sec:inference}) and the constraint solver (Section~\ref{sec:solver}).
%
We then describe key optimizations on DPL programs (Section~\ref{sec:optimization}), and we present experimental results (Section~\ref{sec:eval}).


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "parallelizer"
%%% End:
