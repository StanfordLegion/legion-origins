\input{fig_performance}

\section{Evaluation}
\label{sec:eval}
%
We have implemented our constraint-based approach in Regent, a high-level programming language for HPC applications with first-class support for data partitions~\cite{Regent15}.
%
Regent detects and enforces data dependencies between tasks by analyzing the relationships between region and partition arguments of tasks.
%
Data movement between data partitions is automatically resolved by Legion~\cite{Legion12}, the runtime system for Regent.
%
Regent also provides all DPL operators in Figure~\ref{fig:syntax}~\cite{DependentPartitioning16}.
%
The constraint inference algorithm and solver are implemented as an optimization pass in the Regent compiler.

As Regent is a task-based programming language, the inference algorithm examines parallelizable loops in tasks.
%
When parallelizable loops are nested, the outermost loop is chosen as the target of parallelization.
%
The final stage of auto-parallelization is a source-to-source transformation that converts the original program into a form that uses subregions in all region accesses, as illustrated in Figure~\ref{subfig:ex-parallel-loops}.
%
All parallelizable loops are also amenable to CUDA code generation supported by the Regent compiler.

Guards introduced by the optimizations in Section~\ref{sec:optimization} can be expensive to check when the subregions are sparse.
%
To amortize the cost the compiler replaces them with a cache that remembers values of guard conditions.
%
Alternatively, we could {\em split} the loop to statically disambiguate accesses to multiple regions, as Koelbel and Mehrotra~\cite{Koelbel91} and Adve and Mellor{-}Crummey~\cite{Adve98} distinguished accesses to local data from those to non-local data.
%
We decided not to use this transformation because of the potential combinatorial explosion of cases in the output program.

We evaluate our implementation using the SpMV code in Figure~\ref{fig:ex-spmv} as well as four larger Regent programs: Stencil~\cite{PRK14}, MiniAero~\cite{Mantevo}, Circuit~\cite{Regent17}, and \Pennant{}~\cite{PENNANT}.
%
For the latter Regent programs, we measure weak scaling performance of auto-parallelized versions of these programs and compare them with hand-optimized counterparts.
%
All programs have a ``main'' loop where they spend most of the execution time, and this main loop consists only of parallelizable loops.
%
The hand-optimized versions have already been optimized for scalability in previous work~\cite{Regent17,Legion18}.

All experiments were performed on Piz Daint~\cite{PizDaint}, a Cray X50 system;
%
each compute node is equipped with one Intel Xeon E5-2690 CPU with 12 physical cores, one NVIDIA Tesla P100, and 64GB of system memory.

\input{tab_compile_time}

Table~\ref{tab:compile-time} presents a breakdown of compilation time for benchmark programs.
%
The table also shows the size of each program in terms of the number of auto-parallelized loops and total compilation times of hand-optimized counterparts as a baseline.
%
The constraint inference and solver algorithms and the rewriting to parallel code constitute less than 10 percent of the total compilation time, and the binary code generation is a dominant component.
%
Note that the baseline does not strictly match the time for generating a binary from the auto-parallelized code, because the auto-parallelizer produces a program that is different from the hand-optimized counterpart.

Figure~\ref{fig:performance} summarizes the weak scaling performance of benchmark programs.
%
Performance numbers in plots were measured once programs reached a steady state.
%
All computation tasks running within the measurement window used only GPUs.

\subsection{SpMV Microbenchmark}

Figure~\ref{fig:performance-spmv} shows weak scaling performance of the SpMV code in Figure~\ref{fig:ex-spmv}.
%
In the experiments, we use a diagonal matrix where each row has a fixed number of non-zeros.
%
With this balanced synthetic matrix the auto-parallelized SpMV code achieved 99\% parallel efficiency on 256 nodes.

\subsection{Stencil}
\label{subsec:stencil}

Stencil is a 9-point stencil program for a 2D grid.
%
The stencil consists of a center and eight neighbor points, two for each direction in 2D space.
%
The uncentered access for each neighbor point corresponds to a distinct subset constraint, for which the constraint solver synthesizes an \image{} partition of an affine function.

Figure~\ref{fig:performance-stencil} shows performance of the hand-optimized code and the auto-parallelized code.
%
The auto-parallelized version achieves 93\% parallel efficiency on 256 nodes, whereas the parallel efficiency of the hand-optimized version is 98\%.
%
In terms of absolute performance, the auto-parallelized version is slower than the hand-optimized version by 3\% on average.
%
The discrepancy is due to an optimization for communication manually applied to the hand-optimized version:
%
The code maintains a copy of the halo part in its own region, which consolidates inter-node data movement for halo exchanges in each direction into a single transfer, while the eight partitions used by the auto-parallelized version require two data transfers per direction.
\zap{
%
A similar consolidation optimization would be possible in our approach given that uncentered accesses use affine functions, but not pursued.
}

\subsection{MiniAero}
\label{subsec:miniaero}

MiniAero is a proxy application that solves the Navier-Stokes equation for compressible flows.
%
MiniAero uses a 3D hexahedron mesh with faces shared between neighboring hexahedron cells.
%
The simulation calculates flux between cells pointed to by each face.
%
All tasks in the simulation loop read face properties and update cell properties via uncentered reductions using pointers in each face, similar to Figure~\ref{subfig:ex-relaxation-original};
%
the optimizer applies the optimization in Section~\ref{subsec:relaxation} to these reductions to eliminate reduction buffers completely.

\let\oldbrokenpenalty\brokenpenalty
\brokenpenalty=0

Figure~\ref{fig:performance-miniaero} shows performance of hand-optimized and auto-parallelized versions of MiniAero.
%
Both achieve 98\% parallel efficiency on 256 nodes, but the auto-parallelized version is 2\% slower on average.
%
This difference is explained by different mesh generators used in the two versions:
%
The mesh generator in the hand-optimized code duplicates faces when they point to cells from two different subregions so that faces surrounding cells in each subregion can be contiguously indexed.
%
On the other hand, because the auto-parallelized code uses a mesh generated for sequential execution, faces in each face subregion can be non-contiguously indexed, leading to a small performance degradation in CUDA kernels generated by the Regent compiler.

\brokenpenalty=\oldbrokenpenalty

\subsection{Circuit}
\label{subsec:circuit}

Circuit simulates electric currents along wires in an unstructured circuit graph.
%
Each wire has pointers to incoming and outgoing nodes, which are used for uncentered read accesses and reductions to the region of nodes.
%
Circuit graphs are randomly generated in a way that circuit nodes form clusters;
%
a maximum of 20\% of wires connect nodes in two different clusters.

To showcase the ability to exploit external constraints, we use the existing parallel circuit graph generator that produces inputs to Circuit and auto-parallelize computation tasks with and without a user constraint describing the initial partition of nodes produced by the generator.
%
Figure~\ref{fig:performance-circuit} compares these configurations (\tsc{Auto+Hint} and \tsc{Auto}) with the hand-optimized code (\tsc{Manual}).
%
Without the user constraint, the auto-parallelized code uses an equal partition of circuit nodes, which makes the code match the hand-optimized one within 5\% only up to eight nodes.
%
The circuit generator is designed to simulate sparsely connected components, and thus assigns the first 1\% of entries in the region of circuit nodes to those connected to nodes in other clusters.
%
As a result, the equal partition of the region of nodes puts all these ``shared'' nodes in one subregion, making the task using this subregion a communication bottleneck.

To fix this performance issue, we give the solver an interface constraint describing the externally computed circuit partition.
%
The parallel circuit generator uses two partitions of the region \ttt{rn} of circuit nodes, $\ttt{pn\_private}$ for private nodes and $\ttt{pn\_shared}$ for shared nodes, and the union of these partitions is a disjoint, complete partition of \ttt{rn}, as expressed by the following user constraint:
%
\begin{center}
  \small
  $\disjoint(\ttt{pn\_private} \cup \ttt{pn\_shared})
  \land  \complete(\ttt{pn\_private} \cup \ttt{pn\_shared}, \ttt{rn})$
 \end{center}
%
With this user constraint, the performance of the auto-parallelized code stays within 5\% of the hand-optimized code on 256 nodes and shows better performance up to 64 nodes.
%
The latter is due to the fact that the hand-optimized code always requests reduction buffers for the entire subset reserved for shared circuit nodes even when only a few nodes in this subset are shared, whereas the auto-parallelized code computes tight private sub-partitions to reduce the size of reduction buffers for uncentered reductions.\zap{ (using the construction in Section~\ref{subsec:private-subpartition}).}

\subsection{\Pennant{}}
\label{subsec:pennant}

\Pennant{} is a proxy application for Lagrangian hydrodynamics on 2D meshes.
%
Each polygonal {\em zone} in the mesh consists of triangular {\em sides}; each pair of sides share two {\em points}.
%
Each side has five pointers used in uncentered accesses: two pointers to the previous and next neighbor sides in the same zone, one to the zone, and the last two to points at the vertices of the zone.

\let\oldbrokenpenalty\brokenpenalty
\brokenpenalty=0

Similar to the random circuit generator in Circuit, \Pennant{}'s mesh generator separates points shared by sides in different subregions from those owned by a single subregion of sides.
%
Shared points reside in the initial entries in the region of points.
%
Because of this separation, performance of the auto-parallelized code without any user constraint (\tsc{Auto} in Figure~\ref{fig:performance-pennant}) keeps up with the hand-optimized one (\tsc{Manual}) only up to four nodes and then drops due to the communication bottleneck.

\brokenpenalty=\oldbrokenpenalty

After adding an external constraint describing the partitioning of points, the auto-parallelized code matches the hand-optimized one within 6\% up to 32 nodes (\tsc{Auto+Hint1}).
%
The auto-parallelized code still struggles to scale beyond 64 nodes, but for a different reason:
%
the partitions constructed by the synthesized DPL code exhibit sparsity patterns inefficiently handled by the underlying runtime system, even though they are equivalent to those used in the hand-optimized one in terms of induced inter-node communication.
%
We circumvent this issue by providing additional constraints to guide the solver to synthesize simpler DPL code as follows:
% 
\begin{itemize}

\item We reused the existing disjoint, complete partitions \ttt{rs\_p} and \ttt{rz\_p} of sides and zones, respectively.
  %
  The parallel mesh generator guarantees that zones pointed to by the sides in the $i$th subregion of \ttt{rs\_p} are all contained in the $i$th subregion of
  \ttt{rz\_p} (i.e.,{\small $\image(\ttt{rs\_p}, \ttt{rs}[\cdot]\ttt{.mapsz}, \ttt{rz}) \subseteq \ttt{rz\_p}$}).

\item Additionally, each side $s$ has all its neighbor sides accessed via \ttt{rs}$[s]$\ttt{.mapss3} and \ttt{rs}$[s]$\ttt{.mapss4} in the same subregion:
  % 
  \begin{center}
    \small
    $\begin{array}{@{}l@{\ }l@{}}
       & \image(\ttt{rs\_p}, \ttt{rs}[\cdot]\ttt{.mapss3}, \ttt{rs}) \subseteq \ttt{rs\_p}
       \\
       \land & \image(\ttt{rs\_p}, \ttt{rs}[\cdot]\ttt{.mapss4}, \ttt{rs}) \subseteq \ttt{rs\_p}
     \end{array}$
   \end{center}
   \noindent
   Although these constraints are recursive, the solver can still check the consistency as long as a satisfying partition (\ttt{rs\_p}) is provided.

 \item Finally, the mesh generator creates a partition \ttt{rp\_p\_private} of private points, which can be used as a private sub-partition for uncentered reductions using \ttt{rs}$[\cdot]$\ttt{.mapsp1}:
   \begin{center}
     \small
     $\preimage(\ttt{rs}, \ttt{rs}[\cdot]\ttt{.mapsp1}, \ttt{rp\_p\_private}) \subseteq \ttt{rs\_p}$
   \end{center}
\end{itemize}
%
With these additional user constraints there is no noticeable difference between the auto-parallelized and hand-optimized versions (\tsc{Auto+Hint2}).
%
This example shows the constraint interfaces' ability to provide extra information to gracefully deal with cases where the auto-parallelizers' heuristics do not quite match reality.
%
Writing the additional constraints is still much easier than parallelizing the code by hand, and preserves the option of parallelizing the code in a different way in a different context or after further improvements in the underlying runtime system.

 %%% Local Variables:
 %%% mode: latex
 %%% TeX-master: "parallelizer"
 %%% End:
