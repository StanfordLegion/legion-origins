#!/bin/bash

for f in `ls *_RAW`; do
  xsv cat columns ../NODES "$f" | sed s/","/" "/g > `echo "$f" | sed s/"_RAW"/""/g`
done
