\section{Constraint Inference}
\label{sec:inference}

\input{fig_syntax}

Figure~\ref{fig:syntax} shows the syntax of the partitioning constraint language.
%
Ground terms are regions (using symbol $\regionsym$) and partitions (using symbol $\partsym$);
%
recall that a region is an indexed set of a fixed type (possibly with fields) and a partition is an indexed set of subregions of a region.
%
A partitioning constraint is a conjunction of subset constraints and predicates on partitions.
%
A predicate $\partition(\expr, \regionsym)$ means that $\expr$ is a partition of the region $\regionsym$;
%
i.e., each subregion $\expr[i]$ must be a subset of the region $\regionsym$:
%
\begin{center}
  $\partition(\expr, \regionsym) \triangleq \forall i. \expr[i] \subseteq \regionsym$.
\end{center}
%
A predicate $\disjoint(\expr)$ requires $\expr$ to be a disjoint partition and a $\complete(\expr, \regionsym)$ requires $\expr$ to be a complete partition of $\regionsym$:
%
\begin{center}
  $\begin{array}{r@{\ }c@{\ }l}
    \disjoint(\expr) & \triangleq & \expr[i] \cap \expr[j] = \varnothing\text{ when }i \ne j
    \\
    \complete(\expr, \regionsym) & \triangleq & \bigcup_i\expr[i] = \regionsym
  \end{array}$
\end{center}
%
A subset constraint $\expr_1 \subseteq \expr_2$ denotes that each subregion $\expr_2[i]$ contains the corresponding subregion $\expr_1[i]$:
%
\begin{center}
  $\expr_1 \subseteq \expr_2 \triangleq \forall i.\ \expr_1[i] \subseteq \expr_2[i]$
\end{center}
%
This relation implicitly requires that the set of indices of $\expr_2$ subsume that of $\expr_1$.
%
The subset constraint is anti-symmetric, i.e.,
\begin{center}
  $\expr_1 = \expr_2 \triangleq \expr_1 \subseteq \expr_2 \land\expr_2 \subseteq \expr_1$.
\end{center}

Expressions are DPL operators that construct partitions;
%
we use DPL operators in constraints to syntactically describe partitions of interest.
%
The union, intersection, and difference operators on partitions are applied subregion-wise;
%
for example, a union of two partitions results in a partition whose $i$th subregion is a union of the $i$th subregions of the operands:
%
\begin{center}
  $(\expr_1 \diamond \expr_2)[i] \triangleq \expr_1[i] \diamond \expr_2[i]
  \qquad
  \text{where\ \ }\diamond \in  \mset{\cup, \cap, -}$
\end{center}
%
The \image{} operator creates a partition of a function's range from an existing partition of the function's domain;
%
the expression \\ $\image{}(\expr,\funsym,\regionsym)$ is a partition of $\regionsym$ derived from $\expr$ using $\funsym$ as follows:
%
\begin{center}
  $\image{}(\expr,\funsym,\regionsym)[i] \triangleq \mset{\big(\funsym(k), v'\big) \in \regionsym \mid (k, v) \in \expr[i]}$.
 \end{center}   
% 
(Note that the function $\funsym$ takes the indices of each subregion as arguments, not its values.)
%
The \preimage{} operator is an inverse of \image{}, i.e., deriving a partition of a function's domain from an existing partition of the function's range;
%
the expression $\preimage{}(\regionsym,\funsym,\expr)$ is a partition of $\funsym$'s domain $\regionsym$ derived from $\expr$ as follows:
%
\begin{center}
  $\preimage{}(\regionsym,\funsym,\expr)[i] \triangleq \mset{(k, v') \in \regionsym \mid \big(\funsym(k), v\big) \in \expr[i]}$.
\end{center}
%
Again, Figure~\ref{fig:ex-image-preimage} visualizes the \image{} and \preimage{} operators for an example function \ttt{f}.
%
\zap{
Nested $\image{}$ and $\preimage{}$ expressions satisfy the following properties:
%
\begin{center}
  \small
  $\begin{array}{r@{\ }c@{\ }l}
     \image(\image(\partsym, \funsym_1, \regionsym), \funsym_2, \regionsym) & =
     &
       \image(\partsym, \funsym_2 \circ \funsym_1, \regionsym)
     \\
     \preimage(\regionsym, \funsym_1, \preimage(\regionsym, \funsym_2, \partsym)) & =
     &
       \preimage(\regionsym, \funsym_1 \circ \funsym_2, \partsym)
     \\
  \end{array}$
\end{center}
}
%
Lastly, the \equal{} operator creates partitions without using any other partitions;
%
the expression $\equal(\regionsym)$ creates a partition of $\regionsym$ with approximately equal size subregions.
%
(Integer arguments denoting the number of subregions are elided in partitioning constraints because they do not affect constraint solving.)

% Parallelizable loops

\input{alg_inference}

Algorithm~\ref{alg:inference} shows the constraint inference algorithm, which takes a loop and produces a system of partitioning constraints.
%
The algorithm is concerned only with {\em parallelizable} loops.
%
A loop is parallelizable when values defined in one loop iteration are never consumed by other iterations of the same loop.
%
For brevity, we characterize parallelizable loops syntactically as follows.
%
\begin{itemize}
\item Region accesses are either {\em centered} or {\em uncentered}.
  % 
  A region access \ttt{R[e]} is centered when \ttt{e} is the loop variable (or an alias), and is uncentered otherwise.

\item An uncentered access is {\em admissible} only when it has an index expression derived from another region access (e.g., \ttt{R[S[e]]}) or it has the form \ttt{R[f(i)]} where \ttt{i} is the loop variable.

\item A parallelizable loop is an outermost loop of the form
  \begin{center}
    \ttt{\ttb{for} (i \ttb{in} R): $\dots$}
  \end{center}
  for some region \ttt{R} (the {\em iteration space} of the loop), which satisfies these conditions:

  \begin{itemize}
  \item All write accesses to regions are centered.
    % 
    (A centered reduction is considered a centered read access followed by a centered write access.)

  \item A region with an uncentered reduction (e.g., \ttt{R[S[e]] += $\dots$}) does not have any other read access or a reduction with a different operator.
    
  \item A region with an uncentered read (e.g., $\dots$ = \ttt{R[S[e]]}) does not have any other write access.

  \end{itemize}
\end{itemize}
%
This syntactic definition is sound but incomplete;
%
i.e., there are loops that a more sophisticated analysis, such as polyhedral analysis~\cite{AffineDistributedMemory13}, can prove parallelizable but our syntactic check cannot.

At the highest level, our method for constraint-based partitioning has three components:
%
\begin{itemize}

  \item Initially a separate partition (represented by a unique partition variable) is assigned to every region access in a parallelizable loop.
    %
    Another unique partition variable is associated with the loop index.  For each of these variables, we generate constraints that guarantee the partition will have all the elements needed to execute correctly.   

  \item We solve the constraints by rewriting them into an equivalent form where each remaining constraint corresponds to a concrete dependent partitioning operation; the partitioning code can be read directly from the resolved form of the constraints.

  \item Allowing a separate partition for every region access admits the widest possible range of partitioning strategies, but can result in solutions with multiple equivalent partitions.
    %
    We unify partition variables with isomorphic constraints to reduce the final number of partitions that need to be created.
\end{itemize}

\input{fig_inference_ex1}

The following example illustrates the constraint inference steps for the first loop in Figure~\ref{subfig:ex-loops}.
%
\begin{ex}
  \label{ex:inference}
  Algorithm~\ref{alg:inference} first conjoins the following predicates on a partition symbol $\partsym_1$ for the iteration space \ttt{Particles} (line 8):
  %
  \begin{center}
    $\partition(\partsym_1, \ttt{Particles}) \land \complete(\partsym_1, \ttt{Particles})$
  \end{center}
  %
  The algorithm also maintains an environment that maps each variable to a lambda function that returns an \image{} expression of a region argument.
  % 
  The initial environment at line 7 has a mapping of the loop variable \ttt{p} to a function $\lambda r.\image(\partsym_1, \id, r)$.
  %
  For the region access \ttt{Particles[p].cell}, the algorithm introduces a partition symbol $\partsym_2$ and generates the following constraints (lines 11-13):
  % 
  \begin{center}
    $\partition(\partsym_2, \ttt{Particles}) \land \partsym_1 \subseteq \partsym_2$
  \end{center}
  %
  Note that the expression $\image(\partsym_1, \id, \ttt{Particles})$ is simplified to $\partsym_1$.
  %
  Since the value of this region access is assigned to the variable \ttt{c}, the algorithm updates the environment (lines 14-15), which then becomes the following:
  %
  \begin{center}
    $\mset{\ttt{p} \mapsto \lambda r.\image(\partsym_1, \id, r), \ttt{c} \mapsto \lambda s.\image(\partsym_1, \funsym_1, s)}$,
  \end{center}
  where $\funsym_1 = \ttt{Particles}[\cdot]\ttt{.cell}$.
  %
  For the uncentered region access \ttt{Cells[c].vel}, the algorithm infers the following constraints on a new partition symbol $\partsym_3$ (lines 11-13), where the subset constraint has an \ttb{image} expression in the lower bound:
  % 
  \begin{center}
    $\partition(\partsym_3, \ttt{Cells}) \land \image(\partsym_1, \funsym_1, \ttt{Cells}) \subseteq \partsym_3$
  \end{center}
  %
  Finally, the centered reduction \ttt{Particles[p].pos += }$\ldots$ is handled similarly to other centered accesses, resulting in the partitioning constraint in Figure~\ref{fig:ex-inference}.
\end{ex}

\input{fig_inference_ex2}

Note that the partitioning constraint in Figure~\ref{fig:ex-inference} does not have a disjointness predicate on the partition of the iteration space.
%
If the final solution uses a non-disjoint partition of the iteration space, there is redundant computation because some loop iterations are executed multiple times.
%
This redundancy is useful in cases (as demonstrated by Zhou et al.~\cite{HOT12}) when recomputing loop iterations on separate nodes is cheaper then the internode communication the redundant computation replaces.
%
In Section~\ref{sec:optimization}, we discuss how we can optimize communication from uncentered reductions using an {\em aliased} (non-disjoint) partition of the iteration space.

However, we do need a disjoint partition of the iteration space when the loop has an uncentered reduction access (lines 16-17 in Algorithm~\ref{alg:inference}).
%
Figure~\ref{fig:ex-inference-disjoint} shows an example where an uncentered reduction on the region \ttt{S} imposes a disjointness constraint on the partition $\partsym_1$ of the iteration space \ttt{R}.
%
To see why disjointness is mandatory in this case, we need to understand how uncentered reductions are typically handled in distributed memory systems~\cite{Legion12,Uintah,InspectorExecutorMixed15}.
%
Unlike centered reductions, which are applied immediately, uncentered reductions require two steps.
%
First, each distributed task allocates a temporary buffer to keep the reduction contribution from each iteration that it owns.
%
Then, temporary buffers are merged, either eagerly or lazily, back to the partitions that the subsequent read accesses use.
%
Because this merge step aggregates all contributions in temporary buffers, each contribution must be counted exactly once to preserve the original semantics.
%
Therefore, the iteration space must be partitioned disjointly in this case.

Algorithm~\ref{alg:inference} runs in linear time in the size of the program and produces partitioning constraints sound with respect to the semantics of parallelizable loops.
%
These partitioning constraints always have at least one trivial solution, obtained by replacing each subset constraint with an equality.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "parallelizer"
%%% End:
