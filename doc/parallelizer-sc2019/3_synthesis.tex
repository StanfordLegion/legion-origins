\section{Constraint Solver}
\label{sec:solver}

In this section, we describe a constraint solver that takes partitioning constraints as input and produces DPL programs as solutions.
%
A {\em DPL statement} $P = E$ is expressible in the constraint language of Figure~\ref{fig:syntax}, and a {\em DPL program} is just a sequence of DPL statements.

Our constraint solver transforms the input partitioning constraint into a {\em resolved} form, the constraint conjoined with exactly one equality $\partsym_i = \expr_i$ for each partition symbol $\partsym_i$.
%
Once the partitioning constraint is solved, the added equalities form one solution program.
%
In the rest of this section, we explain the algorithm to resolve partitioning constraints and the heuristics to minimize the number of partitions constructed by the output program.

\input{fig_lemmas}

\zap{
\begin{figure}[b]
  \centering
  \small
  \begin{tikzpicture}
    \node[ellipse,fill=black!20,minimum height=0.6cm,minimum width=0.8cm,inner sep=0pt] at (0, 0)
    (e1) {$\expr_1[i]$};

    \node[ellipse,draw,minimum height=1.2cm,minimum width=1.4cm,
    label={[inner sep=5pt]below:$\preimage(\regionsym_1, \funsym, \expr_2)[i]$}] at (0, 0)
    (pre) {};

    \node[ellipse,fill=black!20,minimum height=1.2cm,minimum width=2.2cm,inner sep=0pt] at (4, 0)
    (img) {$\image(\expr_1, \funsym, \regionsym_2)[i]$};

    \node[ellipse,draw,minimum height=1.8cm,minimum width=4.0cm,
    label=below:{$\expr_2[i]$}] at (4, 0) (e2) {};


    \draw[dotted] (e1.north) -- (img.north);

    \draw[dotted] (e1.south) -- (img.south);

    \draw (pre.north) -- (e2.north);

    \draw (pre.south) -- (e2.south);

    \draw[->] (pre.east) -- node[label={[inner sep=-1pt]above:$\funsym$}] {} (e2.west);


  \end{tikzpicture}
  \caption{Lemma \tsc{L14}}
  \label{fig:lemma-14}
\end{figure}
}

\subsection{Resolution}

Conceptually, a partitioning constraint $C$ can be resolved by the following procedure:
%
\begin{enumerate}

\item Synthesize expressions $\expr_1, \dots, \expr_n$ for all partition symbols $\partsym_1, \dots, \partsym_n$ in $C$.

\item Check consistency of the strengthened constraint
  \begin{center}
    $C \land \partsym_1 = \expr_1 \land \dots \land \partsym_n = \expr_n$.
\end{center}
  
\item If the consistency check fails, go to (1) and synthesize different expressions.

\end{enumerate}
%
The consistency check in step (2) verifies that each predicate in the constraint is entailed by other predicates or known lemmas of DPL operators, shown in Figure~\ref{fig:lemmas}.
%
Any set of expressions that pass this check is a solution that satisfies the input constraint.

All lemmas in Figure~\ref{fig:lemmas} are direct consequences from definitions of the DPL operators and properties of sets.
%
The first four lemmas enumerate all possible cases where partitions of a region $\regionsym$ can be constructed.
%
Lemmas \tsc{L5-7} (resp. lemmas \tsc{L8-12}) state when the completeness (resp. disjointness) of a partition is propagated to others.
\zap{
%
Figure~\ref{fig:lemma-14} visualizes lemma~\tsc{L14}.
}

Algorithm~\ref{alg:solver} shows the constraint solving algorithm tailored to partitioning constraints inferred by Algorithm~\ref{alg:inference}.
%
This algorithm tries to minimize backtracking due to adding an equation that causes the constraint system to become inconsistent (have no solutions).
%
The solver picks promising candidates using the following insights based on the lemmas in Figure~\ref{fig:lemmas}:
%
\begin{enumerate}

\item If a partition symbol $\partsym$ has subset constraints $\expr_1 \subseteq \partsym$, $\dots$, $\expr_k \subseteq \partsym$ where each $\expr_i$ is {\em closed}, i.e., contains no partition symbol, the union $\expr_1 \cup \dots \cup \expr_k$ of these expressions is a good candidate for $\partsym$ (lemma \tsc{L13}).

\item The only way to create a fresh disjoint partition is the $\equal{}$ operator (lemma \tsc{L1}) and only intersection, difference, and $\preimage{}$ operators preserve the disjointness of operands (lemmas \tsc{L9}, \tsc{L10}, and \tsc{L12}).
  %
  Therefore, a partition symbol with a $\disjoint{}$ predicate must be created using only these operators.
  %
  Likewise, complete partitions can be expressed only by $\equal{}$ (lemma \tsc{L1}), union (lemma \tsc{L6}), and $\preimage{}$ (lemma \tsc{L7}), or combinations of these operators.

\item For a subset constraint $\expr_1 \subseteq \expr_2$, disjointness ``flows'' from right to left (lemma \tsc{L8}).
  %
  When both sides of a subset predicate $\expr_1 \subseteq \expr_2$ must be disjoint, the solver resolves all symbols in the expression $\expr_2$ and then derives $\expr_1$.

\item The $\preimage{}$ operator can produce partitions that satisfy subset constraints containing $\image{}$ (lemma \tsc{L14}).
  % 
  Combined with observation (2), these lemmas imply that the solver must use a $\preimage{}$ partition to discharge a subset constraint of the form $\image(\expr_1, ...) \subseteq \expr_2$ when both $\expr_1$ and $\expr_2$ must be disjoint.

\end{enumerate}

\input{alg_solver}

Algorithm~\ref{alg:solver} can always solve partitioning constraints generated by Algorithm~\ref{alg:inference}:
%
Because Algorithm~\ref{alg:inference} introduces a fresh partition symbol for the RHS of each added subset constraint, the subset constraints never form a cycle.
%
Thus, the solver can always find a trivial solution that uses $\equal{}$ partitions for iteration spaces and has equalities strengthened from all subset constraints.
%
However, this na\"ive solution is inefficient because it does not reuse partitions from one parallelizable loop in the others.
%
To maximize the partition reuse in the solution, the constraint solver performs {\em unification} of partition symbols, which is the topic of the next subsection.

The following examples demonstrate how Algorithm~\ref{alg:solver} resolves partitioning constraints.

\begin{ex}
  \label{ex:ex1}
  Suppose we have this partitioning constraint from Figure~\ref{fig:ex-inference-disjoint}:
  %
  \begin{center}
    $\begin{array}{l@{\ }l}
       & \partition(\partsym_1, \ttt{R}) \land \complete(\partsym_1, \ttt{R}) \land \disjoint(\partsym_1)
       \land \partition(\partsym_2, \ttt{S})
       \\
       \land &
       \image{}(\partsym_1, \ttt{g}, \ttt{S}) \subseteq \partsym_2 \land
       \partition(\partsym_3, \ttt{R}) \land \partsym_1 \subseteq \partsym_3.
     \end{array}$
   \end{center}
   %
   Because $\partsym_1$ has a $\disjoint$ predicate, the solver uses an $\equal$ partition for $\partsym_1$ (line 22).
   % 
   After substituting $\partsym_1$ with $\equal(\ttt{R})$, the original constraint simplifies to:
   % 
  \begin{center}
    $\begin{array}{l@{\ }l}
       &\partition(\partsym_2, \ttt{S}) \land
       \image{}(\equal(\ttt{R}), \ttt{g}, \ttt{S}) \subseteq \partsym_2
       \\
       \land &
       \partition(\partsym_3, \ttt{R}) \land \equal(\ttt{R}) \subseteq \partsym_3.
     \end{array}$
   \end{center}
   %
   Since $\partsym_2$ and $\partsym_3$ have closed expressions on the LHS of their subset constraints, the solver simply strengthens them into equalities (line 17) and produces the following solution (after performing common subexpression elimination):
   %
  \begin{center}
    $\begin{array}{l}
       \partsym_1 = \equal(\ttt{R})
       \quad
       \partsym_2 = \image{}(\partsym_2, \ttt{g}, \ttt{S})
       \quad
       \partsym_3 = \partsym_1
     \end{array}$
   \end{center}
\end{ex}

\begin{ex}
  \label{ex:ex2}
  Suppose now we have an extra predicate $\disjoint(\partsym_2)$ in the partitioning constraint as follows:
  %
  \begin{center}
    $\begin{array}{l@{\ }l}
       &\partition(\partsym_1, \ttt{R}) \land \complete(\partsym_1, \ttt{R}) \land \disjoint(\partsym_1)
       \land \partition(\partsym_2, \ttt{S})
       \\
       \land &
       \image{}(\partsym_1, \ttt{g}, \ttt{S}) \subseteq \partsym_2 \land \highlight{\disjoint(\partsym_2)} \land
       \partition(\partsym_3, \ttt{R}) \land \partsym_1 \subseteq \partsym_3.
     \end{array}$
   \end{center}
   %
   Then, the solver notices that $\partsym_2$, the RHS of the subset constraint $\image{}(\partsym_1, \ttt{g}, \ttt{S}) \subseteq \partsym_2$, must be disjoint, and creates an $\equal$ partition for $\partsym_2$ (line 22) and a $\preimage$ partition for $\partsym_1$ (line 14):
   % 
   \begin{center}
     $\partsym_2 = \equal(\ttt{S}) \quad \partsym_1 = \preimage(\ttt{R}, \ttt{g}, \partsym_2)$.
   \end{center}
   %
   The partition symbol $\partsym_3$ is resolved similarly to Example~\ref{ex:ex1}.
\end{ex}

\input{alg_unification}

\subsection{Unification}
\label{subsec:unification}

A single unification step strengthens the original constraint by conjoining an equality between {\em unifiable} partition symbols.\zap{:
%
\begin{center}
  $\begin{array}{l}
     \partition(\partsym_1, R) \land \partition(\partsym_2, R) \land \dots \strengthen
     \\\quad
     \partition(\partsym_1, R) \land \partition(\partsym_2, R) \land
     \highlight{\partsym_1 = \partsym_2} \land \dots
  \end{array}$
\end{center}}
%
Partition symbols are unifiable only when they represent partitions of the same region.
%
The constraint after unification can be further simplified by replacing one of the unified symbols with the other.
%
\begin{ex}
  The partition symbols $\partsym_1$, $\partsym_2$, and $\partsym_4$ in Figure~\ref{fig:ex-inference} can be unified as follows:
  % 
  \begin{center}
    $\begin{array}{l@{\ }l}
       & \partition(\partsym_1, \ttt{Particles}) \land \complete(\partsym_1, \ttt{Particles})
       \\
       \land &
       \partition(\partsym_3, \ttt{Cells}) \land
       \image{}(\partsym_1, \funsym_1, \ttt{Cells}) \subseteq \partsym_3.
     \end{array}$
   \end{center}
\end{ex}
\noindent
Because unification can introduce equalities inconsistent with the original constraint, the constraint after unification might not have any solution.
%
For example, unification can make some subset constraints recursive as follows:
%
\begin{center}
  $\begin{array}{l}
     \partition(\partsym_1, \regionsym) \land \partition(\partsym_2, \regionsym) \land
     \image{}(\partsym_1, \funsym, \regionsym) \subseteq \partsym_2
     \strengthen
     \\
     \quad     
     \partition(\partsym_1, \regionsym) \land
     \colorbox{lightgray}{$\image{}(\partsym_1, \funsym, \regionsym) \subseteq \partsym_1$} \land
     \partsym_1 = \partsym_2.
  \end{array}$
\end{center}
%
This recursive constraint can be satisfied only by constructing a fixpoint of the function $\funsym$, which is not expressible in our constraint language.
%
Therefore, the goal of unification is to find a maximal set of unifications that preserves consistency of the partitioning constraint.

\input{fig_unification}

Finding all viable unifications requires an exhaustive search in the general case.
%
To make the search efficient, we focus on unifications that reduce the number of subset constraints;
%
intuitively, if unification between partition symbols eliminates some subset constraints, the constraint after unification is no more difficult to resolve than the original one.
%
Such unifications manifest as isomorphic subgraphs in a graph that represents a partitioning constraint.
%
In this {\em constraint graph} each node corresponds to a partition symbol, an unlabeled edge from $\partsym_1$ to $\partsym_2$ represents the subset constraint $\partsym_1 \subseteq \partsym_2$, and an edge labeled with a function symbol $\funsym$ encodes the subset constraint $\image(\partsym_1, \funsym, \regionsym) \subseteq \partsym_2$.
%
(Other cases need not be expressed by this graph, because the inference algorithm only generates subset constraints of the two forms.)
%
Isomorphic subgraphs in this graph correspond to partition symbols connected by the same subset constraints (after renaming symbols).
%
Thus, unifying partition symbols in these isomorphic subgraphs also merges multiple subset constraints, one from each subgraph, into one.

\begin{ex}
  \label{ex:unification}
  Figure~\ref{subfig:ex-unification-common} shows a constraint graph for the following constraint (predicates are elided):
  % 
  \begin{center}
    $\begin{array}{r@{\ }l}
       \dots \land & \image(\partsym_1, \ttt{Particles}[\cdot]\ttt{.cells}, \ttt{Cells})
                     \subseteq \partsym_2
       \\
       \land & \image(\partsym_2, \ttt{h}, \ttt{Cells})
               \subseteq \partsym_3
               \land
               \image(\partsym_4, \ttt{h}, \ttt{Cells})
               \subseteq \partsym_5.
     \end{array}$
   \end{center}
   % 
   In Figure~\ref{subfig:ex-unification-common}, the subgraph of $\partsym_2$ and $\partsym_3$ is isomorphic to that of $\partsym_4$ and $\partsym_5$.
   % 
   The solver unifies $\partsym_2$ and $\partsym_4$ and $\partsym_3$ and $\partsym_5$, with the result shown in Figure~\ref{subfig:ex-unification-result}.
\end{ex}

Algorithm~\ref{alg:unification} shows the constraint solver algorithm with unification.
%
The algorithm uses Algorithm~\ref{alg:solver} to check if the system of constraints after unification is still solvable (line 13).
%
Although finding the largest common subgraph in a constraint graph (line 7) is known to be NP-complete~\cite{GareyJ79}, in practice unification is not a significant cost as constraint graphs are small and we do not attempt to find the absolutely maximal common subgraph.
%
Furthermore, the algorithm greedily tries to unify the first few largest subgraphs in a constraint graph (line 3), based on the observation that these subgraphs often contain other smaller subgraphs.
%
In the average case, common subgraphs can be identified simply by constructing a product graph of constraint graphs.
%
Assuming the unification succeeds in a constant number of trials, the asymptotic time complexity of this greedy algorithm is $O(NM^2)$, where $N$ is the number of constraints to unify and $M$ is the number of graph nodes.

\subsection{External Constraints}

As seen in Section~\ref{sec:introduction}, programmers often have invariants on existing partitions used in manually parallelized parts.
%
The constraint solver can exploit these invariants by adding them to the partitioning constraint for a program and holding their partition symbols fixed (no expressions are synthesized for external constraints).
%
\begin{ex}
  The program in Figure~\ref{fig:ex-usercons-intro} specifies an invariant on partitions \ttt{pCells} and \ttt{pParticles}, which can be added to the partitioning constraint from Example~\ref{ex:unification} as follows:
  \begin{center}
    \small
    $\begin{array}{r@{\ }l}
       \dots \land & \image(\partsym_1, \ttt{Particles}[\cdot]\ttt{.cells}, \ttt{Cells})
                     \subseteq \partsym_2
       \\
       \land & \image(\partsym_2, \ttt{h}, \ttt{Cells})
               \subseteq \partsym_3
               \land
               \image(\partsym_4, \ttt{h}, \ttt{Cells})
               \subseteq \partsym_5
       \\
       \land & \highlight{\image(\ttt{pParticles}, \ttt{Particles}[\cdot]\ttt{.cells}, \ttt{Cells})
               \subseteq \ttt{pCells}}
     \end{array}$
   \end{center}
   %
   The solver finds unifications between $\partsym_1$ and \ttt{pParticles}, $\partsym_2$, \ttt{pCells}, and $\partsym_4$, and $\partsym_3$ and $\partsym_5$, yielding the following constraint:
   %
  \begin{center}
    \small
    $\begin{array}{r@{\ }l}
       \dots \land & \image(\ttt{pParticles}, \ttt{Particles}[\cdot]\ttt{.cells}, \ttt{Cells})
                     \subseteq \ttt{pCells}
       \\
       \land & \image(\ttt{pCells}, \ttt{h}, \ttt{Cells})
               \subseteq \partsym_3.
     \end{array}$
   \end{center}
   %
   Since the LHS of the subset constraint on $\partsym_3$ is closed, the solver strengthens it to an equality and eventually produces this solution:
   %
   \begin{center}
     \small
     $\begin{array}{l}
       \partsym_1 = \ttt{pParticles} \qquad \partsym_2 = \partsym_4 = \ttt{pCells}
       \\
       \partsym_3 = \partsym_5 = \image(\ttt{pCells}, \ttt{h}, \ttt{Cells}).
     \end{array}$
   \end{center}
\end{ex}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "parallelizer"
%%% End:
