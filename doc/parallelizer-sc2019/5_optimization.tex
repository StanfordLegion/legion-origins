\section{Optimizations}
\label{sec:optimization}

% 1.5 pages
% 
% Maximal private partition extraction
% Reindexing
% 

As described in Section~\ref{sec:inference}, uncentered reductions on distributed memory systems are implemented using temporary buffers, because different tasks can make changes to the same element, and these changes must be reconciled to ensure the result is correct.
%
Distributed runtime systems, such as Legion~\cite{Legion12}, require programs to specify which partitions need these buffers.
%
However, using a reduction buffer of the size of the whole partition is often inefficient because the buffering is required only on the part that is accessed by multiple parallel processes.
%
Furthermore, if the partition for uncentered reductions is disjoint, which means each location in the region is updated only by one process, no reduction buffer is necessary.
%
In the rest of this section, we describe two optimizations in the solver to minimize the size of reduction buffers.

\subsection{Relaxing Disjointness Requirements for Iteration Spaces}
\label{subsec:relaxation}

One strategy to synthesize a disjoint partition for uncentered reductions (thereby eliminating the reduction buffer) is to use an $\equal$ partition for these reductions and derive a $\preimage$ partition for the iteration space as in Example~\ref{ex:ex2}:
%
The solver requires $\partsym_2$ (the partition symbol for the uncentered reduction in Figure~\ref{fig:ex-inference-disjoint}) to be a disjoint partition by introducing an extra predicate $\disjoint(\partsym_2)$, and the resolution algorithm produces a solution where $\partsym_2$ is assigned to $\equal(\ttt{S})$ and $\partsym_1$ to a $\preimage$ partition derived from $\partsym_2$.
%
With this solution, the loop in Figure~\ref{fig:ex-inference-disjoint} need not request a reduction buffer to parallelize its uncentered reductions.

\input{fig_relaxation}

This strategy does not work when a loop has multiple uncentered reductions using different functions.
%
If the solver uses an $\equal$ partition for these uncentered reductions, then the partition of the iteration space, which must be disjoint because of the uncentered reductions, must contain all preimages of those different functions and the solver cannot prove it to be disjoint using the resolution lemmas.
%
The following example illustrates this issue with multiple uncentered reductions.
%
\begin{ex}
Figure~\ref{subfig:ex-relaxation-original} shows the partitioning constraint for a loop with two uncentered reductions.
%
Using an $\equal{}$ partition for both $\partsym_2$ and $\partsym_3$ would lead the solver to an assignment of $\partsym_1$ to a union of preimages $\preimage(\ttt{R}, \ttt{f}, ..)$ and $\preimage(\ttt{R}, \ttt{g}, ..)$, which cannot satisfy the predicate $\disjoint(\partsym_1)$.
\end{ex}

\input{fig_execution}

The obvious alternative of assigning a disjoint partition to only one of the uncentered reductions would still require a reduction buffer for the other uncentered reduction.
%
However, the disjointness requirement can be lifted completely by rewriting the loop into a {\em relaxed} form, shown in Figure~\ref{subfig:ex-relaxation-relaxed}.
%
This loop has a guard for each uncentered reduction.
%
In a serial execution these guards are trivial (always true), but when regions used in these guards are replaced by partitions (shown in Figure~\ref{subfig:ex-relaxation-parallel}), the guards prevent contributions in the original loop from being applied multiple times.
%
Therefore, the solver no longer needs a $\disjoint$ predicate on the iteration space partition and can use the union of preimages, which was not viable before the relaxation.
%
Figure~\ref{fig:ex-relaxation-execution} shows how guard conditions work;
%
even though some iteration space elements appear in more than one subregion of the iteration space partition, each iteration contributes to each reduction only once.

This relaxation is not always beneficial, because it introduces redundant computation and extra communication due to overlap among subregions of the iteration space partition, and is not always applicable.
%
We heuristically relax loops only when all loops using the same region as the iteration space can be relaxed.

\subsection{Finding Private Sub-Partitions}
\label{subsec:private-subpartition}

In cases when the relaxation is not applied, the optimizer tries to subtract a {\em private sub-partition} from the reduction partition.
%
A private sub-partition of a partition $\partsym$ is a disjoint partition $\partsym_p$ that satisfies $\partsym_p \subseteq \partsym$.
%
Since the private sub-partition is disjoint, the program need not request a reduction buffer.
%
However, the parallel loop must be modified to account for the fact that now the reduction partition is divided into two parts;
%
if the original reduction partition $\partsym$ is divided into a private sub-partition $\partsym_p$ and the rest $\partsym_s = \partsym - \partsym_p$, then the original parallel loop:
%
\begin{center}
  \small
  $\begin{array}{l}
     \ttt{\ttb{parallel for} (j \ttb{in} }\partsym'\ttt{):}
     \\
     \ttt{\ \ \ttb{for} (i \ttb{in} }\partsym'\ttt{[j]):}
     \\
     \ttt{\ \ \ \ }\partsym\ttt{[j][g(i)] += }\partsym'\ttt{[j][i]}
  \end{array}$
\end{center}
%
must be rewritten to:
%
\begin{center}
  \small
  $\begin{array}{l}
     \ttt{\ttb{parallel for} (j \ttb{in} }\partsym'\ttt{):}
     \\
     \ttt{\ \ \ttb{for} (i \ttb{in} }\partsym'\ttt{[j]):}
     \\
     \ttt{\ \ \ \ \ttb{if} (g(i) \ttb{in} }\partsym_p\ttt{[j]):}
     \ttt{\ }\partsym_p\ttt{[j][g(i)] += }\partsym'\ttt{[j][i]}
     \\
     \ttt{\ \ \ \ \ttb{else:}\qquad\qquad\qquad\ \ \ \,\,}\partsym_s\ttt{[j][g(i)] += }\partsym'\ttt{[j][i]}
  \end{array}$
\end{center}

Although there is no general construction of private sub-partitions for a partition, we can use the following theorem when the partition is derived by the $\image$ operator from another disjoint partition.
%
\begin{theorem}
  \label{thm:privatization}
  Let $\funsym_{\regionsym}(\partsym)$ and $\funsym^{-1}_{\regionsym}(\partsym)$ be defined as follows:
  %
  \begin{center}
    $\funsym_{\regionsym}(\partsym) \triangleq \image(\partsym, \funsym, \regionsym)
    \qquad
    \funsym^{-1}_{\regionsym}(\partsym) \triangleq \preimage(\regionsym, \funsym, \partsym)$
  \end{center}
  %
  For a disjoint partition $\partsym$ of a region $\regionsym$, the following expression constructs a private sub-partition of $\funsym_{S}(\partsym)$ for any $\funsym$ and $S$:
  %
  \begin{center}
    $\funsym_S(\partsym) - \funsym_S(\funsym_{\regionsym}^{-1}(\funsym_S(\partsym)) - \partsym)$.
  \end{center}

\end{theorem}
\input{fig_privatization}
\begin{proof}(Sketch) 
  Each image subregion $\funsym_S(\partsym)[i]$ contains all elements pointed to by those in $\partsym[i]$.
  % 
  Then, the sub-expression $\funsym_{\regionsym}^{-1}(\funsym_S(\partsym))$ extends each subregion $\partsym[i]$ with the elements from the other subregions $\partsym[j]$ ($j \ne i$) that also point to the subregion $\funsym_S(\partsym)[i]$.
  %
  Subtracting $\partsym$ from this expanded partition leaves each subregion with only the elements originally from other subregions.
  %
  Therefore, its image (i.e. $\funsym_S(\funsym_{\regionsym}^{-1}(\funsym_S(\partsym)) - \partsym)$) represents the shared part in the original image partition $\funsym_S(\partsym)$, and thus its complement is a private sub-partition.
\end{proof}
\noindent
Figure~\ref{fig:privatization} illustrates the private sub-partition construction in Theorem~\ref{thm:privatization}.

Once the solver identifies a private sub-partition from a partition, a reduction buffer needs to be allocated only for the shared part.
%
This construction can be generalized to cases when the reduction partition consists of multiple image partitions, for which the solver simply takes an intersection of all private sub-partitions in individual image partitions.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "parallelizer"
%%% End:
