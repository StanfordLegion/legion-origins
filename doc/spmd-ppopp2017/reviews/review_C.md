===========================================================================
                          PPoPP 2017 Review #49C
---------------------------------------------------------------------------
Paper #49: Control Replication: Compiling Implicit Parallelism to Efficient
           SPMD with Logical Regions
---------------------------------------------------------------------------

                 Reviewer expertise: 4. I know a lot about this area

                         ===== Paper Summary =====

Languages with implicit parallelism make it convenient to program. Ensuring that these programs in these languages are compiled into efficient codes is an important challenge. SPMD is a popular target for deriving high levels of performance from parallel programs. This paper presents a scheme to compile implicit parallel programs to efficient SPMD programs with logical regions.

Definitely relevant to PPoPP

                           ===== Strengths =====

The paper takes a complete language and has a seemingly reasonable implementation with good evaluation. The presented techniques look correct.

                          ===== Weaknesses =====

1. Comparisons to many prior works are missing. This makes it hard to justify their claims of novelty.
2. The paper is not self contained.
3. I would like to see a general translation scheme (not just focusing on the constructs of the running example).

                            Novelty: 2. Incremental improvement

                      ===== Comments for author =====

Here are a list of comments (not in any particular order).

o- In the introduction multiple claims require citations. For example: "SPMD programming systems ... achieve best-in-class performance and scalability.", or "overhead incurred by such systems is proportional to the number of units of parallel work .. divided by ..."

o- It would be really good to give a brief intro to Regent language. May be for the constructs used. Please explain what is a shard in Regent. 

o- The motivation with Figure 2, is interesting. But I had hard time mapping the same to Figure 3. [ minor] And the font mis-matches between the figures and explanations do not help.

o- You seem to indicate the same partition command in Line 8 and 9 (Figure 3a) behave differently? The first one seems to create two partitions and the second one many? How?

o- I think it will really help the paper to provide a formal translation scheme (an algorithm or a translation rules of the form S1 -> S2, instead of plain English). Otherwise, it is not clear how all the corner cases are handled or the correctness of the said approach. Talking about that, an argument about correctness is a must. It is very easy to miss some corner cases and generate incorrect code.

o- How do the authors handle futures, recursive tasks/futures, nested loops with conditionally invoked tasks etc.

o- Section 5.3: What if the regions of computation cannot be computed in the first stage?

o- Section 6.3: Can #shards > #nodes? Or it is fixed to #nodes?

o- Give a brief set of static characterization of the benchmarks used. 

o- The authors have omitted many SPMD related papers:
a) Saman P. Amarasinghe and Monica S. Lam. Communication optimization and code generation for distributed memory machines. In Proceedings of the ACM SIGPLAN 1993 conference on Programming language design and implementation, pages 126–138. ACM, 1993

b) Ron Cytron, Jim Lipkis, and Edith Schonberg. A compiler-assisted approach to SPMD execution. In Proceedings of the ACM/IEEE conference on Supercomputing, pages 398–406. IEEE Computer Society, 1990.

c) Edwin M. Paalvast, Arjan J. van Gemund, and Henk J. Sips. A method for parallel program generation with an application to the BOOSTER language. SIGARCH Comput. Archit. News, 18(3b):457–469, 1990.

d) Chau-Wen Tseng. Compiler optimizations for eliminating barrier synchronization. In Proceedings of the ACM SIGPLAN symposium on Principles and practice of parallel programming, pages 144–155. ACM, 1995.

e) Ganesh Bikshandi, Jose G. Castanos, Sreedhar B. Kodali, V. Krishna Nandivada, Igor Peshansky, Vijay A. Saraswat, Sayantan Sur, Pradeep Varma, Tong Wen, Efficient, portable implementation of asynchronous multi-place programs, Proceedings of the 14th ACM SIGPLAN symposium on Principles and practice of parallel programming, February 14-18, 2009, Raleigh, NC, USA 

o- Figure 4 gives a grammar of the language they focus on. But they do not explain the main constructs there in. Further they do not explain anywhere how they extend their techniques to the rest of the language.


o- I think it is a good idea to give the possible future works.
