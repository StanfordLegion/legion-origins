We thank the reviewers for their insightful feedback.

In response to reviewer E:

  * OpenMP and Regent share some common properties. Please note
    however that there are a number of key differences:

     1. Regent does not place an implicit barrier after every
        loop. This is because Regent's sequential semantics enable a
        compiler and runtime analysis which is able to determine a
        precise dependence graph for tasks. As a result, Regent is
        able to take advantage of task parallelism (tasks in loops
        that are entirely independent may execute in parallel) and
        complex dependencies in data parallelism (tasks are required
        to wait only on tasks on which they directly depend, which
        means tasks from subsequent loops may begin to execute even
        before a previous loop has completed).

     2. Regent's semantics allow a compiler and runtime to
        automatically catch and prevent several classes of bugs such
        as data races or attempts to read a pointer to remote memory.

  * Shallow intersections are computed on a single node due to a
    limitation of the Legion runtime that requires metadata for
    regions used by a task to be stored on the node executing said
    task. (The shard tasks are launched from a common root task; thus
    the metadata must be collected to the root node even if it is
    computed in parallel.) This is in no way fundamental to the
    technique presented in this paper. If a future version of the
    Legion runtime supports distributed metadata for regions, then
    control replication can easily be adapted to compute shallow
    intersections on multiple nodes. We will clarify this point in the
    paper.

    In the current implementation, memory required to compute shallow
    intersections is determined by the sizes of the index sets for the
    regions being intersected. For structured regions, these are
    represented as dense rectangles (and thus memory usage is
    inconsequential). For unstructured regions, the index set is
    recorded as a bitmask with one bit per element in the region. The
    total approximate memory usage of bitmasks in the reported
    experiments at 512 nodes was 11 MB, 190 MB and 1.6 GB for Circuit,
    MiniAero and PENNANT, respectively. The total memory usage at 512
    nodes was 3.2 GB, 115 GB and 1.1 TB for Circuit, MiniAero and
    PENNANT, respectively.

  * The reviewer is correct in noting that implicitly parallel Regent
    programs rely on the programmer to explicitly partition regions
    into subregions, while communication is implicit in the program
    and determined automatically by the system. Please note however
    that partitioning in Regent is substantially more flexible and
    easier to use than in MPI. While a region may be partitioned and
    the subregions distributed across the machine, the indexing of
    elements inside regions is still global, and pointers to elements
    in a region may be stored on any node even the data is not
    resident on the local node. A region may be partitioned multiple
    times to express different access patterns used in the
    application; Regent's sequential semantics ensure that these
    partitions are maintained automatically on behalf of the user. As a
    result, it is relatively easy to describe applications with
    extensive task parallelism in Regent.

    We would rank the approximate programmer effort required from
    least to greatest:

     1. either one of:  
         a. an implicitly parallel Regent code with minimal partitioning  
         b. an OpenMP code without any effort at data-locality enhancement
     2. an implicitly parallel Regent code with partitioning appropriate for distributed memory
     3. explicitly parallel Regent code
     4. MPI code

    The difference between (1a) and (2) is that (1a) only requires a
    simple disjoint partitioning of data written by tasks. Such
    partitions are very easy to write in Regent. Data read (or
    reduced) by tasks need not be partitioned on a shared-memory
    system. A program can be incrementally converted into the form (2)
    by additionally partitioning read-only (or reduced) regions to
    more tightly bound the read (reduce) sets of the tasks.

In response to reviewer F:

  * The reviewer is correct that control replication is enabled by the
    design of the Regent programming language and particularly its use
    of regions and partitioning to describe the structure of
    data. While the approach proposed here may seem like a small
    extension to Regent (and this is how it in fact appears to
    programmers), the optimization techniques required to
    make it work are new and are our primary contribution. Practically
    speaking, the greater than order of magnitude improvement in
    scalability for straightforwardly written implicitly parallel
    programs is a big step forward for Regent programmers.

We will make appropriate clarifications in the text and apply all
suggestions.
