===========================================================================
                          PPoPP 2017 Review #49D
---------------------------------------------------------------------------
Paper #49: Control Replication: Compiling Implicit Parallelism to Efficient
           SPMD with Logical Regions
---------------------------------------------------------------------------

                 Reviewer expertise: 3. I know the material, but am not an
                                        expert

                         ===== Paper Summary =====

This paper presents a combined static/dynamic technique for transforming programs with "implicit parallelism" (roughly, programs written in terms of things like parallel for loops) into SPMD programs (e.g., MPI programs). The starting point is a program written in Regent, a Legion-like programming language that specifies parallel tasks with each task accessing some region of memory. The static analysis computes an over approximation of which tasks might communicate what data between each other (i.e., it determines which regions overlap) and inserts explicit communication to share the data in the resulting SPMD program. A dynamic analysis determines at runtime what data actually needs to be communicated, making up for analysis imprecision and reducing the total amount of data transmitted.

                           ===== Strengths =====

+ Generating SPMD programs from implicitly-parallel specifications is a worthy, challenging problem.
+ Impressive performance results.

                          ===== Weaknesses =====

- Not clear if the techniques here really represent a big delta over those in Kwon et al. PPoPP  2012 (see detailed comments)
- Most benefit over that prior work seems to be coming from better annotations in Regent, so not clear that this is really an advantage of the technique specific to this paper.

                            Novelty: 2. Incremental improvement

                      ===== Comments for author =====

Overall, I think the motivation for this work is sound: converting implicitly parallel programs to SPMD programs definitely has the potential to improve scalability, and the performance numbers bear this out.

My chief concern is that the novelty of the *techniques* presented here doesn't seem that high. The context in which the techniques are applied -- the Regent language -- makes the techniques effective, but the basic idea of combining what amounts to a region-based dataflow analysis (instead of an array-based dataflow analysis) coupled with a dynamic analysis to increase precision, seems like what Kwon et al. do in their PPoPP 2012 paper. Their approach winds up being less precise overall due to imprecisions in the dataflow analysis, but it seems like this is largely because doing good Array DFA is harder than doing DFA in the presence of Regent's fairly rich region annotations.

                     ===== Questions for Authors =====

Can you characterize the difference between the DFA + dynamic analysis done in this work and that done in Kwon et al.? Are the differences in precision coming from anything other than the extra semantic information provided by region annotations and/or the difference in benchmark programs?
