===========================================================================
                          PPoPP 2017 Review #49E
                     Updated 24 Oct 2016 1:56:01am EDT
---------------------------------------------------------------------------
Paper #49: Control Replication: Compiling Implicit Parallelism to Efficient
           SPMD with Logical Regions
---------------------------------------------------------------------------

                 Reviewer expertise: 3. I know the material, but am not an
                                        expert

                         ===== Paper Summary =====

The paper develops compiler support for transforming a more convenient but less efficient form of a parallel program (called implicitly parallel) to a more efficient (called explicitly parallel)  form that would be much more tedious and error-prone for a user to write. The implementation is in the context of the Regent programming model that allows both forms of programming. The implicitly parallel form is analogous to OpenMP parallel programming, where a sequence of parallel loops is executed, with barrier synchronization in between the parallel loops. The explicitly parallel form is analogous to MPI programming, with long-lived SPMD processes that perform explicit communication of data.  A difference between OpenMP and the implicit programming model (sequence of parallel loops) in Regent is that data affinity can be specified through the concept of regions and sub-regions.

The paper describes a compiler transformation based on region analysis of a sequence of implicitly parallel loops to a collection of long-lived tasks (shards) that collectively execute the same set of computations, but with greatly reduced communication/synchronization overhead because only the needed "intersected" data between shards is explicitly communicated.

                           ===== Strengths =====

(+) Addresses the very important topic of achieving high performance and high programmer productivity

(+) System is evaluated using a number of significant benchmarks

(+) Very good performance results

(+) Well written paper

                            Novelty: 3. New contribution

                     ===== Questions for Authors =====

1) Why are shallow intersections performed on a single node? What are the implications of such single node processing on the space requirements on each node, relative to the total memory required for the application?

2) Two significant differences that make MPI more tedious than OpenMP are 1) the explicit partitioning of data among processes, and 2) explicit communication of data between processes. It seems that in order to achieve comparable performance to an MPI code from an implicitly parallel Regent program, comparable effort will be required regarding the explicit partitioning of data, but no effort will be needed on specifying explicit data communication. Is this correct? For the examples codes in the paper, can you approximately quantify the relative programmer effort to create i) an OpenMP version without any effort at data-locality enhancement, ii) implicitly parallel Regent code, iii) explicitly parallel Regent code, and iv) MPI code?
