===========================================================================
                          PPoPP 2017 Review #49F
                     Updated 24 Oct 2016 2:14:30am EDT
---------------------------------------------------------------------------
Paper #49: Control Replication: Compiling Implicit Parallelism to Efficient
           SPMD with Logical Regions
---------------------------------------------------------------------------

                 Reviewer expertise: 4. I know a lot about this area

                         ===== Paper Summary =====

The paper describes a new technique called 'control replication' that allows programmers to write implicitly parallel code and get SPMD code automatically. The technique is implemented on top of the Regent programming language. In Regent, programmers express parallelism by dividing the computation into tasks. The tasks describe the privileges over shared data, which gives the compiler full knowledge of data access patterns. 

One of the key analysis problems in going from implicit parallelism to SPMD is identifying which statements are potentially accessing aliased memory regions. In Regent, this is easier than the general alias analysis problem in C/C++ because the region hierarchy in the language captures all the necessary sharing information.


The system translates the original programs into an intermediate representation (Region-based dataflow intermediate representation) that makes explicit the gathers and scatters that correspond to explicit data movement. From the RDIR the complier can code generate the code for the shards with explicit point-to-point communication. 

The generated code also incorporates dynamic analysis of communication patterns in order to optimize communication and avoid extensive unnecessary copying.

                           ===== Strengths =====

Strong performance results.
I like the idea of leveraging high-level information about data sharing to make the application distributed.

                          ===== Weaknesses =====

A little incremental over the Regent work.

                            Novelty: 2. Incremental improvement

                      ===== Comments for author =====


The performance results are quite impressive. They very clearly show the value of control replication when scaling to large core counts, and they show that they can be competitive with hand-crafted MPI, in some cases even surpassing it thanks to the ability to try implementation ideas more quickly.

The paper builds heavily on prior work on Regent, and it feels that many of the performance benefits stem directly from design decisions in the original Regent. That said, the paper makes it clear that the optimizations developed in this work are crucial to scalability of Regent beyond a few dozen cores.
