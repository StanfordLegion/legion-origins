===========================================================================
                          PPoPP 2017 Review #49A
---------------------------------------------------------------------------
Paper #49: Control Replication: Compiling Implicit Parallelism to Efficient
           SPMD with Logical Regions
---------------------------------------------------------------------------

                 Reviewer expertise: 3. I know the material, but am not an
                                        expert

                         ===== Paper Summary =====

This authors describe compiler and runtime modifications to the Regent programming language, which enables parallel execution on distributed shared memory systems with low control overhead.  The resulting parallel code features a distributed control system ('control replication') which eases a bottleneck found in the default Regent codes.  Ultimately, they produce code that scales as well as custom MPI / OpenMP code with simpler and more streamlined programming.

                           ===== Strengths =====

The authors present innovative techniques for inferring and dealing with data alias hazards and have provided a high-performance compilation / runtime system that implements those techniques.  The results are impressive, the system looks practical, and the paper is clear and well-written.

                          ===== Weaknesses =====

A few typos here and there - overall, nothing to really complain about.

                            Novelty: 3. New contribution

                      ===== Comments for author =====

Great paper.  I'd like to see a little more summary of your eval results in the abstract / intro, especially as compared to MPI / OpenMP.  Also, I'd like to see some ideas for future work and some frank comments of the limitations of your own work in the conclusion.
