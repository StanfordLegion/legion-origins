===========================================================================
                          PPoPP 2017 Review #49B
---------------------------------------------------------------------------
Paper #49: Control Replication: Compiling Implicit Parallelism to Efficient
           SPMD with Logical Regions
---------------------------------------------------------------------------

                 Reviewer expertise: 4. I know a lot about this area

                         ===== Paper Summary =====

```
Pattern: my language feature X 	 
         enables optimization Y
	 with impact Z
```

In this paper, the langauge features are explicit partitioning of data
and data flow effects annotations on functions expressed in terms of
these partitions. This enables a more effective translations from
high-level to scalable SPMD implementation. They show improved weak
scaling on a small set of benchmarks.

                           ===== Strengths =====

The problem the paper is tackling is important and not adequately solved.

                          ===== Weaknesses =====

The paper does not adequately put the proposed langauge extensions in
context of prior work.

The use of weak scaling, limited benchmarking, and very limited
comparison to alternative systems makes the impact not very
compelling.

                            Novelty: 2. Incremental improvement

                      ===== Comments for author =====

# my language feature X

Page 1, column 1: the parenthetical about bulk-synchronous is out of context.

Figure 1 (a): the caption suggestsions this is "implicitly" parallel
but visually it appears parallel but using a fork-join style. This
figure accurately captures your transformation but the use of
"implicit parallelism" is not accurate.

The paper talks about "implicit parallelism" but does not really
explain that. In particular, an implicitly parallel programs typically
have as-if-sequential semantics. To acheive parallelism, some action
must be taken to interrupt chains of data flow at partition
boundaries. The paper fails to discuss how this is done in general. In
the specific case of the example of Figure 3, the programmer must
intervene to identify data updates which are "reductions" and hence
relax the as-if-sequential semantics by allowing the updates to be
done in undefined (and hence non-detemrinistic) order. 

On page three, top of first column: "A Region program acheives
parallelism by dividing the computation int tasks". This does not
sound implicit at all dispite the use of the word in the title.  The
Chapel/X10 projects have focused on the phrase ``global view'' as an
alternative to explicit communication which is a more accurate way to
describe this work. If it is implicit because there is no syntax
related to where parallelism is intended then this is a weak notion of
implicit which does the developer no service since you cannot provide
diagnostics about where the intent is is not correctly supported by
the structure of the application.

The "partition" primitive is under-specified. I understand it is quite
general in its implementation but the interface should be well
defined. What is the returned object? What are the 'disjoint' and
'aliased' parameters? They appear like values but seem to be used like
types. Is it correcet to think of "region" as a generic collection of
some element type? Are the element type the scalar primitives like a
single "point" or does the region hold collections of such points
under programmer control?

The name "master" does not help me understand the role of that
partition. Is there a reason to divide first private/ghost and the
partition into indidual chunks instead of first partitioning into
chunks and then inside a chunk dividing between private and ghost? How
does a programmer choose? You talk on page 3 about a communication
optimization that is, at this point, a bit magical. Again how does the
developer know?  What happens if different phases of the computation
need different strategies?

Figure 4 is incomplete, informal, and adds little value to the
presentation. Also 'call" is not mentioend on any rhs. 

It appears that the region tree structure is expected to be a static
property of the program available at all program point. In particular,
the second argument to "partition" must not be an expression but
rather a (possibly subscripted) reference to another region
variable. Is that correct? If so, this is a signiciant limitation as
it interacts with function abstraction so strongly.


Treating a region access type as "reduction" seems to strict. I would
think you would want to treat it as "transactional" to capture the
idea that operations may be applied atomically any in any
order. Reduction is a special case.

# enables optimization Y

Is there a base execution model that can be applied to the input
program without the "optimization" of control replication?

I think it is worth commenting when discussing Figures 7 and 8 that
the input edges into "gh.f_10" from before the loop are not edges into
the loop. The implication being that execution of the loop need not be
delayed on for that input.

In section 4.2.2, there is no discussion of control flow beyond loops
treated as atoms. This impacts the concept of "most recently used
nodes" mentioned in that seciont. Or do you assume all control flow is
structured so can be handled hierarchically?

In section 5.2, the discussion of "copies" appears to assume that you
have the ability to have single-sided remote writes similar to PGAS
models instead of two-sided messages common in MPI. Please clarify
this aspect and discuss motivations for your choice.

# with impacts Z

It is a great weakness of this kind of paper which reports on a small
and ad hoc set of benchmarks that the selection criteria for those
benchmarks is not reported. In particular, the reader has no way to
understand how broadly applicable the technique proposed is. 

I think the paper is incomplete without also describing strong scaling
results. It is too easy to bury problems under weak scaling.

It is also a weakness to not have best-of-breed baseline comparisons
written in MPI for all of the benchmarks. Your claim of impact is
predicated on performance so that needs to be demonstrated.

In section 7.5, part of the intersection problem is solved on a single
node. How much data is required for that? Is it a potential limit to
size of problem which can be handled? Table one should be expanded to
include the running time of the application rather than making a
descriptive claim in the text.

The related work should include X10 as a scale out, global-view
language like Chapel. Also, Deterministic Parallel Java also aims to
support safe parallelism through time annotations.

The discussion of Chapel completely misses the mark in terms of its
focus. While yes there are low-level features, its goals are
essentially the same as here: to allow scalable parallel programs to
expressed at a high-level without low-level details. The presence of a
lower-level interface reflects the reality of needing to gain
experience and unblock potential users and is separate from other
research goals.

The conclusions of the paper are stronger than the data presented.
