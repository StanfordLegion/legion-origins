We thank the reviewers for their detailed and insightful feedback.

In response to reviewer B:

  * The Regent language provides sequential
    semantics. Tasks denote bulk operations of an appropriate
    granularity for parallelism, and region arguments to tasks
    indicate the data to be used by said operations. Discovery of
    parallelism is the responsibility of the compiler and runtime.
    Execution is guaranteed to be consistent with that of the
    sequential program. Updates to regions are visible in any
    overlapping regions at subsequent program points; users do not
    need to be concerned with synchronization points or
    communication. Reductions enable additional parallelism but follow
    the same sequential semantics as writes (in that the updated value
    is observable at subsequent program points).

  * X10 and Chapel support a variety of parallel constructs. If you
    consider the purely data parallel subset, which is the most
    similar to Regent's sequential semantics, X10 and Chapel are
    more limited. Both support only a single, static
    distribution of data (compared to Regent's multiple, dynamic and
    hierarchical partitioning), forall-style constructs (vs tasks), and
    limited task parallelism.

    If you include the more expressive parallel constructs (e.g. X10's
    places, asynchronous activities, etc.) then the subset no longer
    provides a sequential semantics.

    Thus, there is not a meaningful comparison with X10 or Chapel with
    optimization techniques presented here; either the language is
    less expressive (for the data parallel subsets) or very different
    (for the full languages).

  * The goal of the weak scaling experiments in this paper is to
    demonstrate scalability to large node counts. The overhead per node imposed
    by the underlying system must be O(1) with respect to the node
    count N in order to achieve ideal weak scaling (as the work
    performed per node is by definition O(1)). Our experiments provide
    empirical evidence that the overhead (after control replication)
    is roughly O(1), enabling good weak scaling to large node
    counts. In contrast, without control replication this overhead is
    O(N) (because analysis is performed on a single node) leading to a
    dramatic loss in parallel efficiency at higher node counts. Our
    weak scaling results are more than an order of magnitude better
    than the previous best efforts in related papers, and are good
    enough to be useful in practice. Finally, we agree that weak
    scaling can be used to hide problems, but the benchmarks in the
    paper cover the spectrum from compute-bound to memory-bandwidth
    bound; we would be happy to include measures of computational
    intensity.

  * With respect to strong scaling, given an extra page, we would be
    happy to include strong scaling results in the final paper.

  * Regarding the choice of applications used in the experiments:
    three of the four codes used are mini-apps created by
    third-party developers and chosen to test meaningful subsets of the
    functionality of larger real-world applications (or classes of
    applications). These mini-apps range from 1700 to 4000 lines of
    code and contain a variety of kernels with different
    characteristics to simulate the requirements of real codes. For
    each application we used the best performing reference
    implementation of the original code. In most cases the provided
    reference was written in MPI or MPI+X by the original
    developers. Again, we would be more than happy to provide more
    information about the characteristics of the benchmarks.

In response to reviewer C:

  * We will include a more substantial Regent introduction.

  * With respect to adding a formal semantics and statement of the
    correctness of the optimizations, we did consider that but
    determined that it would vastly expand the paper beyond its
    current size, and that the most important things to demonstrate
    were that the transformations were possible and useful. Within the
    page constraints we would be very happy to improve the
    clarity/precision of the transformation description.

  * We thank the reviewer for the additional citations. Please note
    that all of the cited papers consider programs with affine
    indexing, and that (b), (d) and (e) are concerned with translating
    explicitly parallel programs. In contrast the techniques presented
    in this paper apply to a language with sequential semantics and
    programs with unstructured data access patterns.

In response to reviewer D:

  * Kwon et al. (PPoPP12) focus on regular (i.e. affine) access
    patterns. Their static array data flow analysis is inherently
    conservative for non-affine accesses. For efficiency, the dynamic
    analysis maintains a bounded list of rectangular section fragments
    for each communication point. As a result, non-affine accesses
    cause analysis imprecision that results in increased communication
    and replicated data. In contrast, Regent's ability to reason
    explicitly about multiple simultaneous partitions of a region
    enables a simpler and improved static analysis, and an efficient
    and fully precise dynamic analysis, resulting in better
    performance and scalability.

We will apply the reviewers' suggestions and add the suggested citations.

In the interest of responding to the reviewers' concerns we include
additional material below.

In response to reviewer C:

  * A shard is a Regent task that uses Regent's features for explicit
    communication and synchronization to coordinate in a SPMD-like
    manner. A shard can be considered analogous to a rank in MPI.

  * Figure 3 only concerns a subset of the mesh data structure,
    specifically the points in the mesh---zones and sides are omitted
    for brevity. In Figure 3a, lines 1-4 declare leaf kernels, lines
    7-11 declare the data structures (including partitioning), and
    lines 12-20 specify the main loop.

  * The partitioning calls (lines 8-11) omit the coloring parameter
    for brevity. This is described in detail in Treichler, et
    al. (OOPSLA 2013). We will make this more clear in the paper. A
    coloring is a map from colors (small integers) to sets of region
    elements. When passed to a partitioning operator, each color
    denotes a subregion in the resulting partition containing the
    elements included in the colored set. Please note that while
    colorings are important for the correctness and performance of
    application codes, the techniques described in this paper are
    entirely agnostic to the choice of the coloring itself.

  * A partition may divide a region into an arbitrary number of
    subregions. The number of subregions in question is determined by
    the coloring argument as described above.

  * Futures are not an explicit part of the Regent programming
    language but are generated by a compiler optimization described in
    Slaughter, et al. (SC 2015). The transformation described in this
    paper is performed prior to that optimization. Thus, at the point
    where the transformation is applied, any futures appear as scalar
    variables in the program. In most cases these scalars do not need
    to be touched. In the presence of scalar reductions these
    variables can be optimized as described in Section 6.1.

  * The transformation described in this paper applies to a single
    task at a time, any calls to subtasks are left as they are. The
    Legion runtime system allows multiple functionally equivalent
    *variants* of a single task to exist. The transformation merely
    provides a new variant of the task (the control-replicated
    variant). The original task variant continues to exist in its
    unmodified form.

    In the case of a recursive task, the compiler would transform the
    the task without explicitly considering any recursion, treating
    any recursive calls as normal calls to subtasks. At runtime, the
    Legion runtime (as directed by the user-specified mapper) would
    select dynamically between the control-replicated and original
    versions of the task at each call site. The user could, for
    example, select the control-replicated task for the first level of
    recursion, then select the normal task for subsequent levels.

  * The translation described in Section 4.2 is performed recursively
    on the AST and produces a nested dataflow graph per nested
    control-flow construct. The algorithms 4 and 5 apply regardless of
    the number of levels of nesting.

  * The intersections described in Section 5.3 must be computed at the
    latest immediately prior to the launch of the control-replicated
    shards. If the regions are computed dynamically (for example, in
    an adaptive computation), then the intersections cannot be lifted
    into the initialization. However, the intersections will be
    computed only once for a given execution of the shard
    tasks. Typically, the shard tasks themselves contain the main
    timestep loop where nearly all of the execution time is spent,
    amortizing the cost of the work performed.

  * At this time #shards must be <= #processors because the Legion
    runtime does not provide preemption and the shards would otherwise
    deadlock. This property is enforced by the runtime. However, the
    #shards may be > #nodes (if there is more than one processor per
    node); this might be useful on machines with strong NUMA
    effects.

  * The benchmarks are as follows:

      * Circuit is an electrical simulation on a sparsely-connected
        random unstructured graph. The kernels are compute-bound and
        require access to the values of neighboring graph vertices at
        each time step. Reductions are applied to neighboring graph
        vertices once per time step.

      * MiniAero is an aerodynamics simulation on a 3D unstructured
        mesh. The kernels are memory-bound and require access to
        neighboring mesh elements each time step.

      * PENNANT is a Lagrangian hydrodynamics simulation on an
        unstructured 2D mesh. The kernels are relatively
        compute-intensive and require access to neighboring mesh
        elements each time step; reductions are also applied to
        neighboring mesh elements. There is a single global scalar
        reduction each time step to compute dt for the next time step.

      * Stencil is a structured stencil kernel on a 2D grid. The
        stencil is a star-shaped pattern with radius 2. The kernel is
        memory-bound.

In response to reviewer B:

  * A partition subdivides a region into a set of subregions. If the
    subregions are overlapping, the partition is called aliased,
    otherwise disjoint. The aliased or disjoint property of the
    partition is captured in the type system. Each region in the set
    is assigned a distinguishing integer called a color. The returned
    partition object contains the set of regions and is able to
    retrieve regions by their associated color. Partitions are
    described in detail in Treichler, et al. (OOPSLA 2013).

  * Regions contain elements which may be simple primitive types (int,
    float, boolean, etc.), pointers to other region elements, structs
    (as in C), or fixed-size arrays of the above (int[3], etc.). If
    the user desires to build more sophisticated data structures, they
    can construct an appropriate region and partition it as desired.

  * The top-level partition private_vs_ghost separates "private"
    elements (which are not at the boundary of a submesh and therefore
    not involved in communication) from "ghost" elements that are
    involved in communication in some way. The ghost partition (Figure
    2 bottom right) is similar to the halo cells that would be
    required in an MPI implementation of this algorithm, i.e. the
    portion of the read set that overlaps data owned by remote
    nodes. The master partition is most similar to a master/slave
    configuration in MPI, i.e. when data elements that are contained
    in a remote halo region require updates, the master is responsible
    for applying those updates. The partition is disjoint because
    there can only be one node responsible for updating each data
    element.

    A similar scheme is described and motivated in Treichler, et
    al. (OOPSLA 2013).

  * If two regions R1 and R2 overlap, and tasks T1 and T2 execute on
    nodes N1 and N2 with Read-Write privilege on R1 and R2, then there
    is communication required between N1 and N2 (for the overlap
    between R1 and R2) before task T2 can execute.

    Similarly, if two partitions P1 and P2 contain subregions that
    overlap, and sets of tasks U1, ... Un and V1, ... Vn are executing
    on nodes N1 ... Nn (so that Ui and Vi execute on Ni), then
    generally speaking there is communication required from Ni to Nj
    if the ith subregion of P1 overlaps with the jth subregion of
    P2. This is a direct consequence of the programming model, and the
    communication is required of any implementation. Control
    replication does not alter this communication pattern, so it does
    not require special reasoning on the part of the programmer.

  * If different phases of the application require different
    communication patterns, the programmer will presumably want to
    consider how the application data moves globally as well as in
    each phase. To the extent that any data remains private, it is
    valuable to partition that data separately as noted above. Any
    number of partitions can be used to describe the required
    communication patterns. Neither the Regent programming language
    nor control replication places any restriction on the number of
    partitions used.

  * It is convenient to write region trees as if they are valid at all
    program points, but the regions are defined in a given program
    scope and not valid elsewhere. The compiler tracks the valid scope
    of each region. We will clarify this in the paper.

  * Regent programs themselves have sequential semantics. The RDIR
    graph in its original form can be transformed back into an AST
    (which will again obey sequential semantics).

  * Regent provides structured control flow (there is no goto
    statement in the language).

  * The copies in Section 5.2 are a feature of the Legion programming
    model and is not specific to the techniques presented in this
    paper. They are implemented with one-sided GASNet active
    messages. The Legion runtime uses of active messages as the
    underlying implementation in order to promote task
    parallelism. This is described in detail in Treichler et al. (PACT
    2014).

  * Intersections are computed from the index sets of regions. For
    structured regions, these are dense rectangles. For unstructured
    regions, the index set is recorded as a bitmask with one bit per
    element in the region. The total approximate memory usage of
    bitmasks in the reported experiments at 512 nodes was 11 MB, 190
    MB, and 1.6 GB for Circuit, MiniAero and PENNANT, respectively.
