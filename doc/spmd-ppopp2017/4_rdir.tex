\section{Region-based Dataflow IR}
\label{sec:rdir}

In Figure~\ref{fig:code2a}, we can see from the annotations that line
8 performs reductions onto the subregion \texttt{ghost[i]} and that line
12 reads the subregion \texttt{master[j]}. As discussed in
Section~\ref{sec:example}, this situation requires communicating any
updates to \texttt{ghost[i]} performed by line 8 to the reads of
\texttt{master[j]} on line 12.  And, again, only the mesh elements in
the intersection of \texttt{ghost[i]} and \texttt{master[j]} need to be
communicated.  Thus, we need to first \emph{gather} (i.e., read) the
subset of elements of \texttt{ghost[i]} that are in both subregions, and
then \emph{scatter} (i.e., write) them into the appropriate locations
in \texttt{master[j]}. In the region tree of
Figure~\ref{fig:region-tree}, this process can be visualized as
writing back the updated elements of \texttt{ghost[i}] to the
\texttt{private\_vs\_ghost[1]} parent region, from which elements are
then scattered into \texttt{master[j]}.

%% Communication in an implicitly parallel program is implied when two
%% statements access the same data. The region tree analysis presented in
%% the previous section identifies the data access patterns of each
%% statement and determines any potential aliasing between the data
%% accessed by each. Note, however, that although aliasing implies
%% overlap, it does not follow that aliased accesses will be exactly
%% equivalent. Thus the data movement implied by the program is not a
%% direct copy, but a \emph{gather} up to a common ancestor in the region
%% tree followed by a \emph{scatter} down to the regions to be accessed.

Control replication requires that these gather and scatters be made
explicit, as they mark the program points where communication
will be inserted into the final program. We use a \emph{region-based
  dataflow intermediate representation} (RDIR) to make gathers and
scatters explicit. RDIR is a hierarchical graph format, with nodes for
operations (rectangles), data (ellipses), gathers and scatters
(triangles), and edges labeled with privileges (read, write,
reduce). Operation nodes for control-flow constructs such as loops
contain nested graphs representing block of operations to be executed
zero or more times.

Because the contents of regions are mutable, data nodes with the same
name can appear multiple times in the graph, representing a different
version of the data on each occurrence. For clarity, data nodes in
Figures~\ref{fig:rdir-partial} and \ref{fig:rdir-complete} are given
distinguishing integer labels, and the names \texttt{private},
\texttt{master}, \texttt{ghost} and \texttt{private\_vs\_ghost} have been
abbreviated as \texttt{pr}, \texttt{ma}, \texttt{gh}, and \texttt{p\_v\_g},
respectively.

Figure~\ref{fig:rdir-partial} shows the RDIR graph generated from the
body of the outermost loop of Figure~\ref{fig:code2a}. The two inner
loops from lines 7-10 and 11-14 appear in the graph as the two
operation nodes (the rectangles). For example, the first inner loop,
which was annotated with the usage \emph{RW}(\texttt{private.f}) and
\emph{Red+}(\texttt{ghost.f}) appears along with data nodes
representing the data accessed. The implicit communication between the
loops (from \texttt{ghost.f} to \texttt{master.f}) is explicitly
represented by a pair of gather and scatter nodes in the RDIR graph
(connected through the least common ancestor node, numbered
11).

The initial transformation is mechanical and results in a number of
redundant gather and scatter nodes in the graph. The compiler then
optimizes the graph to remove these redundant nodes, as shown in
Figure~\ref{fig:rdir-complete}.

\subsection{Execution Model}
\label{subsec:execution-model}

Execution of a program in RDIR form starts at the outermost graph in the hierarchy
(corresponding to the body of a task). Nodes execute as soon as all
inputs are complete, enforcing a topological order of execution. Loop
nodes execute their contained graphs zero or more times as
determined by the loop bounds. Each edge triggers only once for a
given execution of a graph.

As mentioned above, each data node represents a distinct version of
the data named. The data stored in a data node becomes valid as soon
as the node receives a write; the data node itself completes
immediately with no effect.

Conceptually, each instance of a data node maintains distinct storage
that is written once when the data is defined and never changed. This
means that, for example, write-after-read dependencies are implicit in
the graph. It is usually desirable, however, to preserve the locality
of the data by overwriting a single storage location each time a new
version of data is defined. This can be achieved by augmenting the
RDIR graph with happens-before edges from readers to subsequent
writers of each data node; we omit these edges for clarity, as they
may be recovered automatically.

\input{f9_rdir_replicated}

\subsection{Translation from Regent AST to RDIR}
\label{subsec:ast-to-rdir}

The compiler converts Regent ASTs to the RDIR format in several
steps. First, the compiler walks statements in the program and
constructs an operation node for each statement and data nodes for the
data accessed by the statement. Second, the compiler connects aliased
data nodes between statements by inserting gather and scatter nodes
(Figure~\ref{fig:rdir-partial}). And finally, the compiler optimizes
gather and scatter nodes to eliminate redundant communication
(Figure~\ref{fig:rdir-complete}).

\subsubsection{Translation of Statements}
\label{subsubsec:ast-to-rdir-statements}

To generate the initial structure of the RDIR graph, the compiler
walks the annotated AST from Figure~\ref{fig:code2a} and converts
statements one at a time. Blocks of statements, such as loop
bodies, form distinct graphs in the hierarchy. For each
graph, the compiler inserts two sets of data nodes representing the
initial and final state of the loop. Statements within a block are
added to the graph, with their inputs and outputs determined by the
privileges recorded by the region tree analysis.

For example, the first inner loop from Figure~\ref{fig:code2a} on
lines 7-10 produces the operation node (rectangle) on the left side of
Figure~\ref{fig:rdir-partial}. The recorded privileges
\emph{RW}(\texttt{pri\-vate.f}) and
\emph{Red+}(\texttt{ghost.f}) result in three attached data
nodes: two nodes (numbered 2 and 3) for the read and write of
\texttt{private} and one more nodes (\#10) for the reductions onto
\texttt{ghost}.

\subsubsection{Insertion of Gathers and Scatters}
\label{subsubsec:ast-to-rdir-communication}

The compiler connects aliased data nodes with gather and scatter
nodes, resulting in a form of explicit data movement.

The compiler iterates over the statements in the original program
order, connecting each in turn to the growing connected subgraph of
preceding statements. For each data node in a statement, the compiler
finds the frontier set of aliased nodes most recently used in the
graph. Initially, the frontier contains an initial set of input data
nodes inserted into the graph to represent the summarized region usage
of the containing control flow construct. Similarly, after all
statements have been inserted, the compiler connects a final set of
data nodes for the usage of the containing node.

For example, in Figure~\ref{fig:rdir-partial} the input to the second
inner loop \texttt{private.f} (\#5) is previously written by the first
inner loop (\#3). Similarly, the input \texttt{master.f} (\#12) to the
second loop aliases \texttt{ghost.f} (\#10) from the first
loop. Aliased nodes are inserted together, with a single set of
intermediate gather and scatter nodes.

Gather and scatter nodes must copy data through a region that contains
all the elements of both the source and destination regions. Thus, the
compiler consults the region tree (Figure~\ref{fig:region-tree}) and
inserts the least common ancestor into the graph between the gather
and scatter. This results in an apparent inefficiency as data moves
through a potentially large ancestor node; rather than
materialize this node, a subsequent optimization (described in
Section~\ref{sec:replication}) transforms gather and scatter nodes
into efficient point-to-point communication.

\subsubsection{Optimization of Gathers and Scatters}
\label{subsubsec:ast-to-rdir-optimization}

%% FIXME: concrete examples and why it's safe (if not immediately clear)

Finally, the compiler optimizes gather and scatter nodes to remove
redundant communication. Any gather or scatter where the source and
destination are the same can be elided. Also, a gather preceded by a
scatter (ignoring intervening data nodes) can be elided. Finally, if a
pair of identical regions is connected only by gathers, scatters and
data nodes (with no intervening operations), the second region in the
pair can be elided and all edges moved to the first occurrence in the
graph. Figure~\ref{fig:rdir-complete} shows the resulting graph after
these optimizations are applied.

%% \begin{figure}[t]
%% \begin{tabular}{r l}
%% Node Type & Summary \\
%% \hline
%% Data & Holds version of data stored at a location \\
%% Compute & Encapsulates a computation \\
%% Control & Executes a subgraph zero or more times \\
%% Open & Scatters data to children \\
%% Close & Gathers data from children
%% \end{tabular}
%% \caption{RDIR node types.}
%% \label{fig:rdir-outer-loop}
%% \end{figure}

%% \begin{figure}[t]
%% \centering
%% \includegraphics[scale=0.75]{examples/application_order.pdf}
%% \caption{Data nodes allow one write followed by zero or more
%%   reductions.}
%% \label{fig:rdir-application-order}
%% \end{figure}
