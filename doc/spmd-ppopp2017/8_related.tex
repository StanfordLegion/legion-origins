\section{Related Work}
\label{sec:related}

% FIXME
% RDIR: Sequoia, dataflow?
% Analysis: DPJ
% Languages: Copperhead

The benefits of simple programming models are well-known. Several
approaches have considered compiling sequential, shared-memory parallel, and/or
data-parallel programs for execution on distributed-memory machines.

%% Inspector/Executor

Inspector/executor (I/E) methods have been used to compile a class of
sequential programs with affine loops and irregular accesses for
distributed memory\cite{InspectorExecutorIrregular12,
  InspectorExecutorMixed15}. Use of an inspector allows the read/write
sets of program statements to be determined dynamically when the
necessary static analysis is infeasible in the underlying programming
language, enabling distributed, parallel execution of codes written in
conventional languages. However, the time and space requirements of
the inspector can limit scalability at very large node counts. Also,
the I/E approach relies on generic partitioning algorithms such as
automatic graph partitioning\cite{ParMetis02, Patoh11}. Control replication, in contrast,
leverages a programming language with explicit support for
coarse-grained operations (tasks) and data partitioning (of regions
into subregions); this allows the use of application-specific
partitioning algorithms, which are often more efficient and yield
better results than generic algorithms. The choice of language also
enables a tractable static analysis of data partitioning to avoid
runtime analysis on data elements not involved in communication,
reducing the time and space complexity of dynamic analysis (resulting
in a corresponding improvement to scalability).

%% Inspector/executor (I/E) methods have been used to compile a class of
%% sequential programs with affine loops and irregular accesses for
%% distributed memory\cite{InspectorExecutorIrregular12,
%%   InspectorExecutorMixed15}. The compiler has three responsibilities:
%% to select tasks (at an appropriate granularity to maximize parallelism
%% while minimizing load imbalance and other overheads), to partition data
%% (to maximize locality and minimize data movement), and to schedule
%% communication and synchronization for efficient execution on
%% the machine. An I/E compiler emits two distinct
%% programs: an inspector and an executor. The inspector traces execution
%% of the program to determine the read and write sets of every loop
%% iteration. The resulting hypergraph is partitioned with an automatic
%% graph partitioner to produce tasks with minimal load imbalance. Data
%% partitioning and communication patterns simply follow from the read
%% and write sets of the chosen tasks. The executor uses this information
%% to drive communication and synchronization for execution of the full
%% program.

%% The advantage of the I/E approach is that it can work with codes
%% written in conventional languages, providing a path to distributed
%% parallel codes without introducing new language technology. The
%% downsides of the I/E model are expected consequences of its primary
%% advantage: the execution time and memory requirements of the inspector
%% can become a challenge at very large node counts. Also, to be fully
%% automatic, the technique relies on automatic graph partitioning, and
%% while the heuristics used in partitioners generally work well, in
%% cases where they do not there is little recourse for the programmer.

%% Control replication provides a principled approach to allowing
%% programmer input on the selection of tasks and data partitioning, by
%% leveraging a language where these decisions are explicit. The compiler
%% is able to reason directly about tasks and data partitioning. As a
%% result, control replication is applicable regardless of the code
%% contained in the tasks themselves. Additionally, static reasoning
%% about data partitioning enables control replication to avoid analysis
%% on data elements not involved in communication, reducing the time and
%% space complexity of dynamic analysis (resulting in a corresponding
%% improvement to scalability).

%% OpenMP for DSM and/or MPI

In the context of OpenMP, there has been significant interest in
compiling shared-memory programs for distributed-memory
machines. Initial efforts targeted software distributed shared-memory
(DSM)\cite{ClusterOpenMP01, IntelClusterOpenMP06,
  OpenMPDistributed07}. These approaches have limited scalability due to the
limitations of page-based DSM systems. Better scalability has been
demonstrated by compiling directly to MPI\cite{OpenMPDistributed07,
  OpenMPDistributedIrregular06, OpenMPDistributedHybrid12}. However,
in these approaches some data continues to be replicated among nodes,
limiting the ultimate scalability.

Earlier efforts in data-parallel languages such as High Performance
Fortran (HPF)\cite{HPF96, HPF07} pioneered compilation techniques for
a variety of machines, including distributed-memory. In HPF, a single
(conceptual) thread of control creates implicit data-parallelism by
specifying operations over entire arrays in a manner similar to
traditional Fortran. This data parallelism is then mapped to a
distributed-memory system via explicit user-specified data
distributions of the arrays---though the compiler is still responsible
for inferring \emph{shadow regions} (i.e. halos that must be
communicated) from array accesses. Several implementations of HPF
achieved good scalability on structured application\cite{Impact3D02,
  HPFJA02}.

%% Several versions of HPF provided
%% different sets of features, mostly focusing on structured
%% applications.

%% Another line work of focused on data-parallel languages for
%% arrays, especially High Performance Fortran (HPF)\cite{HPF96,
%%   HPF07}. In HPF, a single (conceptual) thread of control creates
%% implicit data-parallelism by specifying operations over entire arrays
%% in a manner similar to traditional Fortran. This data parallelism is
%% then mapped to a distributed memory system via explicit user-specified
%% data distributions of the arrays---though the compiler is still
%% responsible for inferring \emph{shadow regions} (i.e. halos that must
%% be communicated) from array accesses. Several versions of HPF provided
%% different sets of features, mostly focusing on structured
%% applications.

%% One of the best demonstrations of performance in HPF was achieved with
%% IMPACT-3D, a 3D fluid simulation on a structured
%% mesh\cite{Impact3D02}. IMPACT-3D leveraged the HPF/JA extensions to
%% enable explicit user control over shadow regions and
%% communication\cite{HPFJA02}. To our knowledge, similar extensions were
%% not investigated for unstructured applications---in fact, HPF/JA
%% removed support for several HPF 2.0 approved extensions intended for
%% use in unstructured codes, to reduce the complexity of the compiler
%% implementation.

%% One effort did attempt to add support for unstructured applications to
%% HPF\cite{HPFUnstructured95}. However, the changes required radically
%% altered the programming model: the standard HPF notion of sequential
%% control was exchanged for what was effectively a SPMD programming
%% model, complete with low-level message passing constructs.

% FIXME: Sequoia

%% The Sequoia language and compiler\cite{SequoiaLanguage06,
%%   SequoiaCompiler07}

More recent high-level parallel languages such as
Chapel\cite{Chapel07} have avoided reliance on the compiler's ability
to optimize high-level language features by adding support for
low-level constructs that can be used when necessary. This approach is
also taken in Legion and Regent, which support both a straightforward
implicitly parallel style of programming that scales to modest numbers
of nodes as well as more involved explicit communication constructs
that can be used to scale to very large node counts
\cite{LegionFields14}. In particular, the explicit approach can be
time-consuming and error-prone, and was identified in a recent
study\cite{SandiaReportManyTaskRuntimes} as a challenge for this
class of programming systems. Our work can be seen as greatly
increasing the performance range of the implicit style, allowing more
whole codes and subsystems of codes to be written in this more
productive and more easily maintained style.

%% We are able to make more progress on these
%% problems even for unstructured codes compared to the earlier efforts
%% discussed above only because the Legion/Regent data model of
%% explicitly named regions and subregions is much more amenable to
%% compiler analysis than languages with less structure in how
%% programmers organize and refer to data, such as C, C++ and
%% Fortran. Control replication is the first approach to provide
%% sufficiently powerful analyses of implicitly parallel unstructured
%% programs, and the first to demonstrate that efficient SPMD codes can be
%% automatically generated for distributed-memory machines.

%% Control replication is further distinguished from previous approaches
%% in its ability to reason about multiple distributions of the data
%% simultaneously, even when one or more of those distributions is
%% internally overlapping. Preserving this information throughout the
%% toolchain allows control replication to perform well on unstructured
%% applications without the need for additional annotations or
%% instrinsics.

%% Finally, control replication reaps substantial performance benefits
%% from targeting Regent\cite{Regent15} and Legion\cite{Legion12}, a
%% language and runtime system with native support for hierarchical task
%% parallelism and deferred execution. Control replication is able to
%% ignore the details of computations inside leaf tasks, thus simplifying
%% the implementation and focusing the compiler's effort on areas were it
%% can have the most leverage.
