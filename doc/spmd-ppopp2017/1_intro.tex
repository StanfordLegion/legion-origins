\section{Introduction}
\label{sec:intro}

SPMD programming systems, including MPI\cite{MPI},
Titanium~\cite{TITANIUMSTANDARD} and UPC\cite{UPCSTANDARD} among many
others, are broadly considered to achieve best-in-class performance
and scalability on today's distributed-memory machines. SPMD codes
execute in a manner inherently suited to the
available hardware, with independent control contexts executing
simultaneously in an asynchronous manner and explicit user control
over synchronization and communication. (Note that SPMD codes need not
be bulk-synchronous.) However, this explicit control comes at a
cost to programmer productivity, as the user is responsible for
synchronization and communication and is exposed to the
pitfalls those mechanisms entail (such as deadlocks and data races).

In contrast, implicitly parallel programming models, including
Spark\cite{Spark10} and Legion\cite{Legion12}, use a single control
context to launch parallel work. Programs written in such models are
easy to read, write, and reason about because both data movement and
synchronization are implicit and handled automatically by the system.

\input{f1_tasks}

%% FIXME: "Legion, for example, provides additional" => could be more explicit (i.e. Legion *requires* the use of additional features in order to achieve performance)

For machines with small numbers of nodes, or applications with large
units of parallel work, the implicit approach works
well. However, the sequential bottleneck of a single control context
becomes apparent at larger node counts---generally, the overhead
incurred by such systems is proportional to the number of units of
parallel work to be executed, divided by the granularity of the work
to be performed. This leads to challenges when attempting to scale
high-performance scientific applications, where work granularity is generally
constrained by the required communication, to large machines. This
problem is severe enough that Legion, for example, provides additional
features to support explicit communication and synchronization for
running on very large numbers of nodes
\cite{LegionFields14}. To achieve scalability, a programmer must
manually transform their implicitly parallel code to use long-running
tasks, called \emph{shards}, with explicit communication and
synchronization. Executions of a program before and after this
transformation are shown in Figure~\ref{fig:tasks1}.

%% FIXME: Explain Figure 1. (Also, rephrase captions to avoid use of control replication)

In this paper we seek to combine the advantages of both approaches by compiling
implicitly parallel programs into efficient SPMD codes. We present an
implementation for Regent\cite{Regent15}, a language that supports
both implicit and explicit styles of programming, allowing
exploration of these trade-offs within a single programming system.

Our primary contribution is \emph{control replication}, a technique
that distributes the work of a sequential control context between
a set of shards. A key insight of our technique
is that generating efficient SPMD code requires an intimate
understanding of the patterns of communication used in the
application; and that specific features of Regent make such an
analysis particularly straightforward and precise, resulting in
predictable and highly scalable SPMD code. A second insight is that
control replication is most effective if divided into a static and a
dynamic component; we present a combined static and dynamic analysis
that is able to compute a precise communication graph for the
application and insert the necessary communication and synchronization
for SPMD execution. We evaluate control replication on four
codes---three unstructured proxy applications and one structured
benchmark---and demonstrate that the technique results in SPMD code
that scales well, achieving up to 96\% efficiency on 512 nodes in our
experiments.
