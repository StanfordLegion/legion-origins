
\lstset{
  captionpos=b,
  language=C++,
  basicstyle=\scriptsize,
  numbers=left,
  numberstyle=\tiny,
  columns=fullflexible,
  stepnumber=1,
  escapechar=\#,
  keepspaces=true,
  literate={<}{{$\langle$}}1 {>}{{$\rangle$}}1,
  showlines=true,
  belowskip=-5pt,
  numbersep=2pt,
}

\section{Motivation}
\label{sec:background}
To motivate our design, we begin by presenting a small code
example from S3D that illustrates the need for structure slicing.  
Listing~\ref{lst:code} shows a short pseudo-code excerpt from the 
{\em right-hand side function} {\tt rhsf} of S3D (lines 57-66).  The
{\tt rhsf} function evaluates the values on the right-hand
side of the Navier-Stokes partial differential equations and
is parameterized to operate across a range of chemical
mechanisms.  The {\tt rhsf} function is invoked multiple 
times per time step on each node by an explicit Runge-Kutta 
solver and routinely consumes in excess of 97\% of the execution 
time of an S3D run.  In this section we focus on the computation 
performed by the {\tt rhsf} function on each node. 

\begin{lstlisting}[float={t},label={lst:code},caption={S3D Right-Hand Side Function.}]
struct Cell { 
  double avmolwt, mixmw, temp, viscosity, lambda, pressure,
    yspec_h2, yspec_o2, yspec_o, yspec_oh, yspec_h2, 
    yspec_h20, yspec_h, spec_h02, yspec_oh,
    ds_mixarg_h2, ds_mixarg_o2, ds_mixarg_o,
    ds_mixarg_oh, ds_mixarg_h20, ds_mixarg_h, ds_mixarg_h02,
    ds_mixarg_h202, ds_mixarg_n2, grad_vel_x_x, 
    grad_vel_x_y, grad_vel_x_z, grad_vel_y_x, grad_vel_y_y, 
    grad_vel_y_z grad_vel_z_x, grad_vel_z_y, grad_vel_z_z,
    grad_yy_h2, grad_yy_o2, grad_yy_o, grad_yy_oh, grad_yy_h20,
    grad_yy_h, grad_yy_h02, grad_yy_h202, grad_yy_n2,
    tau_x_x, tau_x_y, tau_x_z, tau_y_y, tau_y_z, tau_z_z, ...
};

/* Example across-cell computation: gradient molar species */
/* Only one direction of stencil shown */
#\##define STENCIL_1D(x, field, ae, be, ce, de) \
  (ae * (cells[x+1].field - cells[x-1].field)) + \
  (be * (cells[x+2].field - cells[x-2].field)) + \
  (ce * (cells[x+3].field - cells[x-3].field)) + \
  (de * (cells[x+4].field - cells[x-4].field))

void calc_grad_yy(Cell *cells, int num_cells, double dim_size) {
  double ae = 4.0 / 5.0 * dim_size;
  double be = -1.0 / 5.0 * dim_size;
  double ce = 4.0 / 105.0 * dim_size;
  double de = -1.0 / 280.0 * dim_size;
  for (int i = 0; i #$<$# num_cells; i++) {
    cells[i].grad_yy_h2 = STENCIL_1D(i, yspec_h2, ae, be, ce, de);
    cells[i].grad_yy_o2 = STENCIL_1D(i, yspec_o2, ae, be, ce, de);
    /* ... */
    cells[i].grad_yy_n2 = STENCIL_1D(i, yspec_n2, ae, be, ce, de);
  }
}

/* Example across-field computation: stress tensor */
void calc_tau(Cell *cells, int num_cells) {
  for (int i = 0; i #$<$# num_cells; i++) {
    double sum_term = cells[i].grad_vel_x_x +
      cells[i].grad_vel_y_y + cells[i].grad_vel_z_z;
    cells[i].tau_x_x = 2.0 * cells[i].viscosity *
              (cells[i].grad_vel_x_x - sum_term);
    cells[i].tau_y_y = 2.0 * cells[i].viscosity * 
              (cells[i].grad_vel_y_y - sum_term);
    cells[i].tau_z_z = 2.0 * cells[i].viscosity *
              (cells[i].grad_vel_z_z - sum_term);
    cells[i].tau_x_y = cells[i].viscosity * 
              (cells[i].grad_vel_x_y + cells[i].grad_vel_y_x);
    cells[i].tau_x_z = cells[i].viscosity *
              (cells[i].grad_vel_x_z + cells[i].grad_vel_z_x);
    cells[i].tau_y_z = cells[i].viscosity *
              (cells[i].grad_vel_y_z + cells[i].grad_vel_z_y);
  }
}

/* Right-Hand Side Function (RHSF) */
void rhsf(Cell *cells, int num_cells) {
  calc_volume(cells, num_cells);
  calc_temperature(cells, num_cells);
  calc_thermal_coefficients(cells, num_cells);
  calc_grad_yy(cells, num_cells);
  calc_tau(cells, num_cells);
  calc_diffusion_flux(cells, num_cells);
  calc_reaction_rates(cells, num_cells);
  /* ... */
}
\end{lstlisting}

The {\tt rhsf} function operates on an array of cells.  The {\tt Cell}
type (declared on lines 1-13) shows the first 42 fields of the 139
field struct for H2, the smallest chemical mechanism.  Interesting
mechanisms used in real research, such as dimethyl ether
(DME) and heptane \cite{S3D09}, require 548 and 1046 fields per cell
respectively. In the original Fortran, these fields are implicitly
encoded as a fourth dimension on every array, with the ordering of 
dimensions fixing the layout of the data.  In both C and Fortran
versions of the code, modifying the data layout would require
changes to a large fraction of the 200K lines of code in S3D.

In Section~\ref{sec:slicing} we demonstrate how Legion allows
data layout to be decoupled from the specification of program data.
In a heterogeneous environment, decoupling of data specification from
layout using structure slicing facilitates using different data layouts 
for tasks executed on different kinds of processors.  For example, tasks 
run on GPU processors will likely perform best with a {\em struct-of-arrays}
data layout for memory coalescing, while tasks run on CPU processors will 
often perform best with {\em array-of-structs} or hybrid \cite{pharr2012} data
layouts to leverage cache prefetch engines.

The {\tt rhsf} function invokes two different kinds of functions
on the array of cells in Figure~\ref{fig:s3d_phases}.  
The {\tt calc\_grad\_yy} function on lines 17-34 of Listing~\ref{lst:code} 
(which computes the gradient of the molar fractions for each species using 
a stencil computation) is an example of an across-cells function.
In contrast, {\tt calc\_tau} on lines 37-54 (which computes the stress 
tensor for each cell using other fields within the same cell) is an 
example of an across-fields function. In S3D there is no function 
that requires access to all of the fields.

While the implementation of {\tt rhsf} portrayed in this example is
a sequential function, there exists significant field-level task
parallelism among its subroutines.  For example, it is safe to execute
the adjacent functions {\tt calc\_grad\_yy} and {\tt calc\_tau} in
parallel because they access independent sets of fields and are
therefore {\em non-interfering},  even though one function operates 
across fields and the other operates across cells.  
(We formally define non-interference
in Section~\ref{sec:slicing}.)  
In practice, it is common for there to exist in excess
of 100-way task parallelism between the hundreds to thousands of fields 
in the {\tt Cell} data type for larger mechanisms in S3D.  We show in
Section~\ref{sec:slicing} how structure slicing enables Legion to
implicitly infer field-level non-interference between tasks.

Structure slicing also enables two important data movement optimizations.
First, on machines with hierarchical memory, structure 
slicing permits Legion to know exactly which fields must be moved 
for computations to run on an accelerator.  For example, off-loading 
the data parallel {\tt calc\_tau} task onto a GPU requires only copying 
data from the {\tt viscosity} and {\tt grad\_vel} fields into the GPU's 
framebuffer.  Since these are a small subset of the total fields in a 
{\tt Cell}, there is a significant improvement in performance by
only moving the needed field data.  
We note that a version of S3D
using OpenACC \cite{S3DACC12} can perform a similar operation, under 
the condition that data is laid out in system memory using a 
struct-of-arrays format so individual field data is dense, allowing
OpenACC to copy individual dimensions of the array.  Entangling 
layout with data movement optimizations in the OpenACC code results
in code that is difficult to modify when exploring different mapping 
strategies and tuning for new architectures.

%Second, since fewer fields are needed, more cells can fit in the 
%target memory.   We will also
%show in Section~\ref{sec:eval} that the additional memory freed
%up by structure slicing makes possible a task-level 
%software-pipelining algorithm\cite{Knight07} for hiding the
%transfer latencies in hierarchical memories.

The second data movement optimization enabled by structure slicing
is copy elimination.  Consider two instances of the {\tt grad\_vel} 
task being performed for the same species in different dimensions.
Both of these tasks require access to the {\tt yspec} field
for a given species.  In the case where both tasks are mapped
onto an accelerator, a structure slicing Legion implementation will
dynamically detect the redundancy in data movement and ensure that the 
data is only moved once. This optimization can also be done in an 
OpenACC version of the code, provided the mapping decisions are known 
statically, but it is a tedious and error prone transformation to 
perform by hand in the presence of the thousands of fields in each S3D 
chemical mechanism.  To compound matters, the optimization is implicit
in the code, reducing code maintainability, and potentially introducing
bugs under code refactoring. We cover the details of how Legion automatically 
performs both data movement optimizations in a safe way in Section~\ref{sec:impl}.

%The second optimization enabled by structure slicing is the 
%ability to leverage all the processors within a node simultaneously.
%While many computations perform better on accelerators, some
%functions have complex locality patterns or irregular memory accesses
%that perform better using the large caches on CPUs.  In S3D,
%the data parallel {\tt calc\_tau} task is better suited for an 
%accelerator, while the {\tt calc\_grad\_yy} task stencil works
%better on the CPU where it has access to remote nodes through one-sided
%RDMA operations.  Structure slicing makes it possible to discover the 
%parallelism between these different tasks, thereby allowing a 
%programming system to keep more of a node's processors (both CPUs 
%and accelerators) active.

%\subsection{Data Parallelism and Partitioning in Legion}
%\label{subsec:legion}
%To evaluate structure slicing we have implemented it within
%an existing data parallel system.  Legion is a data-centric 
%programming model designed
%for describing how data is partitioned and placed in complex
%memory hierarchies\cite{Legion12}.  Legion introduces 
%{\em logical regions} as an abstraction for describing collections 
%of typed data such as the grid of cells in S3D.  Logical regions
%have no implied placement or layout within the memory hierarchy,
%which enables programmers to specify partitioning schemes that
%are independent of any one architecture.  Legion supports the
%hierarchical creation of multiple, dynamic partitions of any
%logical region\cite{LegionTypes13}, enabling 
%partitioning schemes both across nodes and recursively within an individual 
%node.  To target a specific architecture, a {\em mapping 
%interface} gives programmers explicit control over the placement
%and movement of {\em physical instances} of logical regions within
%the memory hierarchy\cite{Legion12}.  There may be zero, one, or multiple
%physical instances of a logical region at any point in time (e.g., multiple
%physical instances may be created in different parts of the machine to 
%improve access to a logical region with read-only data).

%\begin{figure}
%\begin{center}
%\includegraphics[scale=0.60]{figs/s3d_partition}
%\end{center}
%\caption{Logical Region Tree for Data in S3D. \label{fig:s3d_partition}}
%\vspace{-0.5cm}
%\end{figure}

%Figure~\ref{fig:s3d_partition} shows the four-level partitioning
%scheme in our implementation of S3D in Legion.  There is a single
%top-level region which encompasses the grid of all the cells in S3D.\footnote{A physical instance of this logical region is never manifested, as
%it could never fit in any single memory of the machine.}
%The region of
%all cells is first partitioned into disjoint (indicated by the $*$
%symbol) logical sub-regions corresponding to clusters of nodes within
%the machine.  This partitioning supports topology-aware Legion mappings 
%on machines with 3D-torus and dragonfly network topologies where small 
%groups of nodes are better connected.  The second level of partitioning
%further decomposes the set of cells for each cluster into the cells
%owned by each node.  The third level of partitioning breaks each node's
%cells into its set of {\em ghost cells} in each of the three dimensions
%which are used for exchanging halo data for stencil computations between
%adjacent cells.  Finally, a fourth {\em subgrid} partition offers a
%further decomposition of a node's cells which can be used for extracting
%data parallelism or describing workloads for multiple accelerators
%within a node.  The resulting data structure is called a {\em region tree}
%and allows the Legion programming system to reason about the structure
%of the program's data.

%Computation in Legion is organized around a hierarchical tree of {\em tasks}
%which often mirrors, but is not tied to, the partitioning of logical regions.
%Tasks explicitly name the regions that they
%access along with the {\em privileges} on those regions.
%The possible region access privileges are {\em read}, {\em write}, and {\em reduce}
%with a specific reduction operator.  For example, the {\tt rhsf} task in our
%Legion implementation of S3D both reads and writes its logical region
%of owned cells.  Legion extracts parallelism implicitly from a sequence
%of task invocations by leveraging its knowledge of both privileges and 
%the shape of region trees and their partitions.  If Legion can prove that 
%two tasks are {\em non-interfering} because they either access
%disjoint logical regions or have privileges which 
%permit concurrent execution, then the tasks can be run in parallel.  
%For example, if {\tt calc\_tau} launched subtasks each looping over
%logical regions in the subgrid partition for a node, then Legion
%would infer that those subtasks could be run in parallel because
%they access disjoint regions.

%Legion, like other data parallel systems, cannot prove that {\tt calc\_grad\_yy}
%and {\tt calc\_tau} are non-interfering because both tasks use the same logical
%region with read-write privileges.  Structure slicing adds a 
%third dimension for proving non-interference: two tasks are non-interfering
%when accessing disjoint sets of fields or accessing overlapping fields
%with non-interfering privileges.  In Section~\ref{sec:slicing} we give a precise 
%definition  of non-interference on fields, and in Section~\ref{sec:impl} we
%describe how to make this test efficient for discovering implicit 
%parallelism dynamically in Legion.

%\subsection{Structure Slicing in S3D}
%\label{subsec:rhsf}
%In S3D the {\tt rhsf} task launches many subtasks as part of its
%execution.  In the traditional Legion programming model each of these
%phases would be serialized as the Legion dependence analysis would
%see that each subtask was both reading and writing all the cells 
%owned by the {\tt rhsf} task.  It is important to note that this
%problem is not unique to Legion.  Both the OpenMP\cite{S3D09}
%and OpenACC\cite{S3DACC12} versions of S3D suffer from the same 
%problem as both versions synchronize all threads or the GPU after
%each loop over all the cells.  As a result, none of these programming 
%systems can observe all of the available parallelism in 
%the {\tt rhsf} task.

%Structure slicing directly addresses this problem by allowing
%computations to explicitly name the fields that they will access along
%with the privileges with which they access it.  In Legion this is done
%by having each task name the fields on each logical region that it
%will access.  Lines ----- show prototypes for each of the subtasks
%in {\tt rhsf} which explicitly name the fields they access in each
%region.  Since these field names are associated with privileges, the
%Legion dependence analysis will know exactly how each field in each
%region is being accessed.  As a result, when Legion sees the stream
%of subtask calls in {\tt rhsf} it now has field non-interference as
%third mechanism for proviing two tasks are safe to run in parallel.

%Structure slicing also provides two additional benefits in S3D with
%respect to hierarchical memory.  First, by explicitly naming the 
%fields accessed by a subtask, structure slicing enables the Legion 
%runtime to no longer move the entire region if a task is mapped onto 
%a remote processor or an accelerator with its own address space 
%(e.g. a GPU).  Instead, only fields that will be accessed must
%be moved, both saving bandwidth and increasing the problem size that
%can fit on accelerator.  Second (something about joint task and data parallelism)

%Productivity, stencil computation

