\section{Introduction}
\label{sec:intro}
Modern supercomputers have evolved to incorporate deep memory
hierarchies and heterogeneous processors to meet the demands of
performance and power efficiency from the computational science
community.  Each new architecture has been accompanied by new software
for extracting performance on the target
hardware \cite{OpenACC,CUDA,IntelMPSS,OPENMP98,Khronos:OpenCL}.  While
these programming systems contain many ways for describing
parallelism, they offer little support to programmers for managing
data.  Consequently, the responsibility for orchestrating all
data movement, placement, and layout both within and across nodes
falls primarily on the programmer.  At the same time, data
movement has become increasingly complex and expensive and now
dominates the performance of most applications.

To aid programmers in managing program data, we previously introduced Legion, 
a data-centric programming model based on {\em logical regions} \cite{Legion12,LegionTypes13,Realm14}.  
Logical regions are typed collections that can be recursively partitioned 
into logical sub-regions, thereby allowing applications to name 
the subsets of data used by different sub-computations.  Logical regions
further benefit applications by decoupling the specification of which data
is accessed by computations from where that data is placed within the memory
hierarchy.  Using a {\em mapping interface}, Legion applications 
separately control where instances of logical regions are placed in the
machine \cite{Legion12}.   %independent of the expression of the algorithm being  

\begin{figure}
\begin{center}
\includegraphics[scale=0.40]{figs/Field_Slicing}
\end{center}
\caption{Across-Cell and Across-Field Phases in S3D. \label{fig:s3d_phases}}
\vspace{-0.5cm}
\end{figure}

Despite these advantages, our experience writing Legion programs
over the last two years has convinced us that a fundamental dimension
was missing from the design: describing compound data types with
multiple {\em fields}.
Consider, for example, the combustion simulation S3D \cite{S3D09},
which was one of the six applications used for acceptance testing of
Titan \cite{S3DACC12}, the current number two supercomputer \cite{Top500}.
S3D models both the physics of turbulent gas dynamics
as well as the chemistry of combustion via direct numerical
simulation.  Physical space is discretized into a grid of cells and
each cell maintains more than a thousand intermediate values
or fields corresponding to different physical and chemical properties
(e.g., temperature, pressure, etc.).  Each cell can therefore be
considered a structure with numerous field values.  S3D contains many 
computational phases, each of which is primarily a physics or chemistry phase, as
shown in Figure~\ref{fig:s3d_phases}.  Physics phases
are mainly {\em stencils} that access neighboring cells, but require only
a few fields from each cell.  Chemistry phases require many fields, but 
always from a single cell.

In practice, many scientific computing applications share S3D's structure:
there are one or more large distributed data structures (regular grid, irregular mesh, 
graph) and each element within these data structures holds many tens, 
hundreds, or even thousands of field values.  Furthermore, all 
computations on these data structures go through phases operating 
on different subsets of these fields.  The crucial insight is that few,
if any, computations in an application require access to all of the 
fields.  In this paper, we present {\em structure slicing} as an 
extension to Legion which allows computations to explicitly slice
logical regions to name the subsets of fields they will access.
Armed with this information, a field-aware Legion implementation
can automatically derive several important performance benefits.

\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
\item {\em Task Parallelism}: Legion can automatically infer implicit task
      parallelism between computations using disjoint sets of fields.
\item {\em Decoupled Data Layout}: The specification of data usage for
      computations can be decoupled from the actual data layout by
      making data layout a Legion mapping decision.
\item {\em Reduced Data Movement}: By knowing precisely which fields are
      required for tasks, Legion can automatically determine the 
      minimum subset of data that needs to be communicated when computations
      are moved to new processors on remote nodes or accelerators
      with their own address spaces.
\end{itemize}

The rest of this paper is organized as follows. In
Section~\ref{sec:background} we motivate our design by giving concrete
examples of how S3D benefits from structure slicing.  Each of the
remaining sections presents one of our technical contributions:
\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
\item We describe the semantics of structure slicing, 
      including the need for dynamic field allocation.  We 
      detail the incorporation of fields into Legion's definition of
      non-interference and show how Legion's mapping interface
      decouples data layout from data usage (Section~\ref{sec:slicing}).
\item We describe the implementation of structure slicing in Legion, 
      including the necessary changes to Legion's dynamic dependence
      analysis as well as the implementation of data movement routines
      for managing field data (Section~\ref{sec:impl}).
\item We port several applications, including a full implementation of S3D,
      to use a structure slicing Legion implementation.  We demonstrate
      that the performance benefits conferred by structure slicing enable
      both strong and weak scaling (Section~\ref{sec:eval}).
\end{itemize}
Section~\ref{sec:related} describes related work and Section~\ref{sec:conclusion}
concludes.

