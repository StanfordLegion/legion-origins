#!/usr/bin/python

import subprocess
import sys, os, shutil
import string, re
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
matplotlib.rcParams['text.usetex'] = True

tableau1 = (0.968,0.714,0.824)
tableau2 = (0.882,0.478,0.470)
tableau3 = (0.565,0.663,0.792)
tableau4 = (0.635,0.635,0.635)
tableau5 = (0.678,0.545,0.788)
tableau6 = (1.000,0.620,0.290)
tableau7 = (0.859,0.859,0.553)
tableau8 = (0.769,0.612,0.580)
tableau9 = (0.478,0.757,0.424)
tableau10= (0.427,0.800,0.855)
tableau11= (0.929,0.592,0.792)
tableau12= (0.929,0.400,0.364)
tableau13= (0.447,0.620,0.808)
tableau14= (0.780,0.780,0.780)
tableau15= (0.773,0.690,0.835)
tableau16= (0.882,0.616,0.353)
tableau17= (0.804,0.800,0.365)
tableau18= (0.659,0.471,0.431)
tableau18= (0.404,0.749,0.361)
tableau19= (0.137,0.122,0.125)

node_list=[1,2,4,8,16,32,64,128,256,512,1024]
node_small=[1,2,4,8,16,32,64,128]

cray_48=[11.6,12.0,11.9,12.2,12.3,13.7,13.4,13.7,15.5,15.3,18.2]
cray_64=[14.8,14.9,15.0,15.1,15.3,16.4,15.9,16.0,17.9,17.6,21.2]
cray_96=[31.4,31.6,31.8,31.9,31.9,32.1,32.6,32.3,33.4,34.2,37.0]

ifort_48=[7.6,8.0,8.2,8.4,8.4,8.5,9.2,9.7]
ifort_64=[8.8,9.2,9.6,9.0,9.6,10.6,10.2,10.6]
ifort_96=[18.8,19.5,19.4,18.9,20.1,20.7,20.0,22.2]

openacc_48=[86.6, 84.2, 83.4, 83.5, 82.6, 83.5, 89.5, 96.3, 94.7, 94.6, 110.3]
openacc_64=[191.6, 187.4, 185.4, 178.9, 176.5, 178.0, 188.2, 197.8, 195.3, 194.3, 224.3]

titan_48=[61.0, 62.3, 62.9, 61.2, 61.6, 63.4, 63.7, 63.8, 64.3, 64.2, 64.4]
titan_64=[105.9, 108.6, 111.7, 106.2, 106.7, 116.4, 118.5, 118.3, 118.9, 119.7, 119.8]
titan_96=[689.5, 691.6, 688.5, 661.1, 662.0, 698.9, 700.6, 696.6, 690.5, 653.7, 633.6]

keeneland_48=[7.3, 7.9, 7.9, 8.3, 8.6, 8.5, 8.4, 9.3]
keeneland_64=[10.6, 10.8, 10.7, 11.1, 11.2, 11.7, 13.5, 12.1]
keeneland_96=[35.4, 36.3, 35.8, 34.0, 34.8, 38.5, 40.7, 38.1]

def plot_experiment(nodes, experiments, col, mark, lab):
    assert len(experiments) == len(nodes)
    plt.plot(nodes,experiments,'--',color=col,label=lab,
              linestyle='dashed',markersize=14,marker=mark,markerfacecolor=col,linewidth=2.0)

def get_s3d_throughput(latencies, size, timesteps):
    puts = []
    for l in latencies:
        tput = 1.0 * size * size * size * timesteps / l
        puts.append(tput)
    return puts

def plot_titan(show = True, save = True):
    fig = plt.figure(figsize = (7,5))
    plt.semilogx(basex=2)
    plot_experiment(node_list, get_s3d_throughput(cray_48, 48, 10),
                    tableau2,'o','Fortran - $48^3$')
    plot_experiment(node_list, get_s3d_throughput(cray_64, 64, 5),
                    tableau2,'s','Fortran - $64^3$')
    plot_experiment(node_list, get_s3d_throughput(cray_96, 96, 3),
                    tableau2,'p','Fortran - $96^3$')
    plot_experiment(node_list, get_s3d_throughput(titan_48, 48, 100),
                    tableau3,'v','Legion $48^3$')
    plot_experiment(node_list, get_s3d_throughput(titan_64, 64, 100),
                    tableau3,'^','Legion $64^3$')
    plot_experiment(node_list, get_s3d_throughput(titan_96, 96, 100),
                    tableau3,'d','Legion $96^3$')
    plot_experiment(node_list, get_s3d_throughput(openacc_48, 48, 100),
                    tableau9,'<','OpenACC $48^3$')
    plot_experiment(node_list, get_s3d_throughput(openacc_64, 64, 100),
                    tableau9,'>','OpenACC $64^3$')

    plt.ylim(ymin=0.0,ymax=275000)
    plt.xlim(xmin=1/1.1,xmax=1024*1.1)
    #l = plt.legend(bbox_to_anchor=(1.0,0.8),loc=4)
    l = plt.legend(loc=4, ncol=3, fontsize=18)
    plt.setp(l.get_title(), fontsize = 18)
    plt.xlabel('Nodes', fontsize=18)
    plt.ylabel('Throughput Per Node (Points/s)', fontsize=18)
    plt.xticks(node_list,node_list,fontsize=18)
    plt.yticks(fontsize=18)
    plt.grid(True)

    if show:
        plt.show()

    if save:
        print "Saving figure to dme_titan.pdf"
        fig.savefig('dme_titan.pdf',format='pdf',bbox_inches='tight')

def plot_keeneland(show = True, save = True):
    fig = plt.figure(figsize = (7,5))
    plt.semilogx(basex=2)
    plot_experiment(node_small, get_s3d_throughput(ifort_48, 48, 10),
                    tableau2,'o','Fortran - $48^3$')
    plot_experiment(node_small, get_s3d_throughput(ifort_64, 64, 5),
                    tableau2,'s','Fortran - $64^3$')
    plot_experiment(node_small, get_s3d_throughput(ifort_96, 96, 3),
                    tableau2,'p','Fortran - $96^3$')
    plot_experiment(node_small, get_s3d_throughput(keeneland_48, 48, 10),
                    tableau3,'v','Legion $48^3$')
    plot_experiment(node_small, get_s3d_throughput(keeneland_64, 64, 10),
                    tableau3,'^','Legion $64^3$')
    plot_experiment(node_small, get_s3d_throughput(keeneland_96, 96, 10),
                    tableau3,'d','Legion $96^3$')

    plt.ylim(ymin=0.0,ymax=275000)
    plt.xlim(xmin=1/1.1,xmax=128*1.1)
    #l = plt.legend(bbox_to_anchor=(1.0,0.8),loc=4)
    l = plt.legend(loc=4, ncol=2)
    plt.setp(l.get_title(), fontsize = 16)
    plt.xlabel('Nodes', fontsize=14)
    plt.ylabel('Throughput Per Node (Points/s)', fontsize=14)
    plt.xticks(node_small,node_small)
    plt.grid(True)

    if show:
        plt.show()

    if save:
        print "Saving figure to dme_knl.pdf"
        fig.savefig('dme_knl.pdf',format='pdf',bbox_inches='tight')

def print_speedup(title,nodes,base,legion,scale_base=1.0,scale_legion=1.0):
    assert len(nodes) == len(base)
    assert len(nodes) == len(legion)
    print "SPEEDUPS FOR: "+str(title)
    for idx in range(len(nodes)):
        speedup = float(base[idx])*float(scale_base)/(float(legion[idx])*float(scale_legion))
        print "Nodes "+str(nodes[idx])+": "+str(speedup)
    print ""

if __name__ == "__main__":
    plot_keeneland(("-s" not in sys.argv), ("-w" in sys.argv))
    plot_titan(("-s" not in sys.argv), ("-w" in sys.argv))
    print_speedup("Keeneland 48",node_small,ifort_48,keeneland_48)
    print_speedup("Keeneland 64",node_small,ifort_64,keeneland_64,2)
    print_speedup("Keeneland 96",node_small,ifort_96,keeneland_96,10,3)
    print_speedup("Titan 48",node_list,cray_48,titan_48,10)
    print_speedup("Titan 64",node_list,cray_64,titan_64,20)
    print_speedup("Titan 96",node_list,cray_96,titan_96,100,3)
    print_speedup("OpenACC 48",node_list,openacc_48,titan_48)
    print_speedup("OpenACC 64",node_list,openacc_64,titan_64)


