#!/usr/bin/python

import subprocess
import sys, os, shutil
import string, re
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
matplotlib.rcParams['text.usetex'] = True

tableau1 = (0.968,0.714,0.824)
tableau2 = (0.882,0.478,0.470)
tableau3 = (0.565,0.663,0.792)
tableau4 = (0.635,0.635,0.635)
tableau5 = (0.678,0.545,0.788)
tableau6 = (1.000,0.620,0.290)
tableau7 = (0.859,0.859,0.553)
tableau8 = (0.769,0.612,0.580)
tableau9 = (0.478,0.757,0.424)
tableau10= (0.427,0.800,0.855)
tableau11= (0.929,0.592,0.792)
tableau12= (0.929,0.400,0.364)
tableau13= (0.447,0.620,0.808)
tableau14= (0.780,0.780,0.780)
tableau15= (0.773,0.690,0.835)
tableau16= (0.882,0.616,0.353)
tableau17= (0.804,0.800,0.365)
tableau18= (0.659,0.471,0.431)
tableau18= (0.404,0.749,0.361)
tableau19= (0.137,0.122,0.125)

node_list=[1,2,4,8,16,32,64,128,256,512,1024]
node_small=[1,2,4,8,16,32,64,128]

cray_48=[21.7,22.0,22.2,22.6,22.9,25.3,22.8,23.7,30.0,27.8,32.7]
cray_64=[27.3,27.5,27.8,27.8,28.1,30.1,29.1,29.1,32.5,32.0,36.3]
cray_96=[58.0,58.3,58.6,58.8,58.9,58.9,59.5,59.9,63.0,62.2,67.6]

openacc_48=[129.5,125.6,124.1,124.2,123.1,124.5,134.2,146.2,143.2,144.1,170.1]
openacc_64=[295.2,287.6,283.5,270.4,267.2,270.2,287.2,310.4,303.8,304.2,370.4]

titan_48=[78.8,83.6,86.1,89.5,90.5,91.2,92.8,88.9,93.1,95.1,97.1]
titan_64=[156.2,169.8,166.9,173.6,173.4,180.3,181.1,180.9,183.5,191.6,197.0]
titan_96=[1070.4,1073.0,1083.0,1083.1,1094.6,1166.7,1109.8,1108.4,1116.2,1112.3,1072.4]

keeneland_48=[9.6,11.4,11.5,10.4,10.5,11.5,11.5,12.5]
keeneland_64=[17.1,18.6,18.6,17.6,17.6,17.8,19.9,21.5]
keeneland_96=[40.0,42.2,53.5,47.2,48.0,45.1,45.9,47.3]

ifort_48=[14.2,14.2,14.5,13.5,14.7,14.3,16.3,16.3]
ifort_64=[15.1,16.6,17.1,15.7,16.9,19.1,20.1,17.7]
ifort_96=[32.8,34.2,35.1,33.4,34.8,38.2,42.2,39.3]

def plot_experiment(nodes, experiments, col, mark, lab):
    assert len(experiments) == len(nodes)
    plt.plot(nodes,experiments,'--',color=col,label=lab,
              linestyle='dashed',markersize=14,marker=mark,markerfacecolor=col,linewidth=2.0)

def get_s3d_throughput(latencies, size, timesteps):
    puts = []
    for l in latencies:
        tput = 1.0 * size * size * size * timesteps / l
        puts.append(tput)
    return puts

def plot_titan(show = True, save = True):
    fig = plt.figure(figsize = (7,5))
    plt.semilogx(basex=2)
    plot_experiment(node_list, get_s3d_throughput(cray_48, 48, 10),
                    tableau2,'o','Fortran - $48^3$')
    plot_experiment(node_list, get_s3d_throughput(cray_64, 64, 5),
                    tableau2,'s','Fortran - $64^3$')
    plot_experiment(node_list, get_s3d_throughput(cray_96, 96, 3),
                    tableau2,'p','Fortran - $96^3$')
    plot_experiment(node_list, get_s3d_throughput(titan_48, 48, 100),
                    tableau3,'v','Legion $48^3$')
    plot_experiment(node_list, get_s3d_throughput(titan_64, 64, 100),
                    tableau3,'^','Legion $64^3$')
    plot_experiment(node_list, get_s3d_throughput(titan_96, 96, 100),
                    tableau3,'d','Legion $96^3$')
    plot_experiment(node_list, get_s3d_throughput(openacc_48, 48, 100),
                    tableau9,'<','OpenACC $48^3$')
    plot_experiment(node_list, get_s3d_throughput(openacc_64, 64, 100),
                    tableau9,'>','OpenACC $64^3$')

    plt.ylim(ymin=0.0,ymax=175000)
    plt.xlim(xmin=1/1.1,xmax=1024*1.1)
    #l = plt.legend(bbox_to_anchor=(1.0,0.8),loc=4)
    l = plt.legend(loc=4, ncol=3, fontsize=18)
    plt.setp(l.get_title(), fontsize=18)
    plt.xlabel('Nodes', fontsize=18)
    plt.ylabel('Throughput Per Node (Points/s)', fontsize=18)
    plt.xticks(node_list,node_list,fontsize=18)
    plt.yticks(fontsize=18)
    plt.grid(True)

    if show:
        plt.show()

    if save:
        print "Saving figure to dme_titan.pdf"
        fig.savefig('hept_titan.pdf',format='pdf',bbox_inches='tight')

def plot_keeneland(show = True, save = True):
    fig = plt.figure(figsize = (7,5))
    plt.semilogx(basex=2)
    plot_experiment(node_small, get_s3d_throughput(ifort_48, 48, 10),
                    tableau2,'o','Fortran - $48^3$')
    plot_experiment(node_small, get_s3d_throughput(ifort_64, 64, 5),
                    tableau2,'s','Fortran - $64^3$')
    plot_experiment(node_small, get_s3d_throughput(ifort_96, 96, 3),
                    tableau2,'p','Fortran - $96^3$')
    plot_experiment(node_small, get_s3d_throughput(keeneland_48, 48, 10),
                    tableau3,'v','Legion $48^3$')
    plot_experiment(node_small, get_s3d_throughput(keeneland_64, 64, 10),
                    tableau3,'^','Legion $64^3$')
    plot_experiment(node_small, get_s3d_throughput(keeneland_96, 96, 10),
                    tableau3,'d','Legion $96^3$')

    plt.ylim(ymin=0.0,ymax=240000)
    plt.xlim(xmin=1/1.1,xmax=128*1.1)
    #l = plt.legend(bbox_to_anchor=(1.0,0.8),loc=4)
    l = plt.legend(loc=4, ncol=2)
    plt.setp(l.get_title(), fontsize = 16)
    plt.xlabel('Nodes', fontsize=14)
    plt.ylabel('Throughput Per Node (Points/s)', fontsize=14)
    plt.xticks(node_small,node_small)
    plt.grid(True)

    if show:
        plt.show()

    if save:
        print "Saving figure to dme_knl.pdf"
        fig.savefig('hept_knl.pdf',format='pdf',bbox_inches='tight')

def print_speedup(title,nodes,base,legion,scale_base=1.0,scale_legion=1.0):
    assert len(nodes) == len(base)
    assert len(nodes) == len(legion)
    print "SPEEDUPS FOR: "+str(title)
    for idx in range(len(nodes)):
        speedup = float(base[idx])*float(scale_base)/(float(legion[idx])*float(scale_legion))
        print "Nodes "+str(nodes[idx])+": "+str(speedup)
    print ""

if __name__ == "__main__":
    plot_keeneland(("-s" not in sys.argv), ("-w" in sys.argv))
    plot_titan(("-s" not in sys.argv), ("-w" in sys.argv))
    print_speedup("Keeneland 48",node_small,ifort_48,keeneland_48)
    print_speedup("Keeneland 64",node_small,ifort_64,keeneland_64,2)
    print_speedup("Keeneland 96",node_small,ifort_96,keeneland_96,10,3)
    print_speedup("Titan 48",node_list,cray_48,titan_48,10)
    print_speedup("Titan 64",node_list,cray_64,titan_64,20)
    print_speedup("Titan 96",node_list,cray_96,titan_96,100,3)
    print_speedup("OpenACC 48",node_list,openacc_48,titan_48)
    print_speedup("OpenACC 64",node_list,openacc_64,titan_64)


