# helper stuff for plots the way we use them in papers

import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages
#matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# tableau colors
TABLEAU = [ (0, 0, 0),
            (0.882,0.478,0.470),
            (0.565,0.663,0.792),
            (0.478,0.757,0.424),
            (0.427,0.800,0.855),
            (0.635,0.635,0.635),
            (0.678,0.545,0.788),
            (1.000,0.620,0.290),
            (0.859,0.859,0.553),
            (0.769,0.612,0.580),
            (0.929,0.592,0.792),
            (0.929,0.400,0.364),
            (0.447,0.620,0.808),
            (0.780,0.780,0.780),
            (0.773,0.690,0.835),
            (0.882,0.616,0.353),
            (0.804,0.800,0.365),
            (0.659,0.471,0.431),
            (0.404,0.749,0.361),
            (0.137,0.122,0.125),
            (0.968,0.714,0.824),
            ];

class Plot(object):
    def __init__(self, title, xlabel, ylabel, legend = 3, legend_cols = 1,
                 xlog = None, ylog = None, 
                 xlim = None, ylim = None,
                 xtics = None, ytics = None,
                 marksize = 12):
        self.dims = (8, 5)
        self.title = title
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.legend = legend
        self.legend_cols = legend_cols
        self.xlog = xlog
        self.ylog = ylog
        self.xlim = xlim
        self.ylim = ylim
        self.xtics = xtics
        self.ytics = ytics
        self.lines = []
        self.colors = list(xrange(1, len(TABLEAU)+1))
        self.markers = list(('o', 'v', '^', '*', '<', '>', 'd', 's', 'x', '+'))
        self.marksize = marksize

    def pick_color(self, c):
        if c:
            # use the color, but remove it from the list if present
            self.colors = [ x for x in self.colors if x <> c ]
        else:
            # take the first unused color
            c = self.colors.pop(0)

        # if it's just an int, assume it's a tableau color index
        if type(c) is int:
            c = TABLEAU[c]
        return c

    def pick_marker(self, m):
        if m:
            # use the marker, but remove it from the list if present
            self.markers = [ x for x in self.markers if x <> m ]
            return m
        else:
            # take the first unused marker
            return self.markers.pop(0)

    def line(self, label, pts, color = None, marker = None, lstyle = 'dashed', lwidth = 1.0):
        self.lines.append(dict(label = label,
                               pts = pts,
                               color = self.pick_color(color),
                               marker = self.pick_marker(marker),
                               lstyle = lstyle,
                               lmarkersize = self.marksize,
                               lwidth = lwidth))
        #print self.lines[-1]
        return self

    def spacer(self):
        self.lines.append(None)
        return self

    def gen_fig(self):
        fig = plt.figure(figsize = self.dims)
        if self.title:
            plt.title(self.title)
        if self.xlabel:
            plt.xlabel(self.xlabel)
        if self.ylabel:
            plt.ylabel(self.ylabel)

        if self.xlog:
            if self.xtics:
                plt.xscale('log', basex=10**10, subsx=[]) # get rid of existing ticks
            else:
                plt.xscale('log', self.xlog)
        if self.xtics:
            tv = []
            tl = []
            for t in self.xtics:
                try:
                    v, l = t
                except:
                    v = t
                    l = '$' + str(t) + '$'
                tv.append(v)
                tl.append(l)
            plt.xticks(tv, tl)

        legend_entries = []

        for l in self.lines:
            if l is None:
                r = matplotlib.patches.Rectangle((0,0), 1, 1, fill=False, edgecolor='none',
                                                 visible=False)
                legend_entries.append((r,' '))
            else:
                a = plt.plot(*(zip(*l['pts'])), label = l['label'], color = l['color'],
                              marker = l['marker'], markerfacecolor = l['color'],
                              linestyle = l['lstyle'], linewidth = l['lwidth'],
                              markersize= l['lmarkersize'])
                legend_entries.append((a[0],l['label']))

        if self.xlim:
            plt.xlim(self.xlim)

        plt.ylim(ymin = 0)

        if self.legend:
            plt.legend(*zip(*legend_entries), loc = self.legend, ncol = self.legend_cols, prop={'size':10})

        return fig

    def save_file(self, outfile):
        fig = self.gen_fig()
        fig.savefig(outfile, format = "pdf", bbox_inches = "tight")
