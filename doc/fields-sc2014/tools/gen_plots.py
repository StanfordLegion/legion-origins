#!/usr/bin/python

from plot import Plot
import re

def get_s3d_throughput(nodes, path, size, scale = 1):
    points = []
    for n in nodes:
        try:
            with open((path % n), 'r') as f:
                tsteps = None
                timetaken = None
                for l in f:
                    m = re.search(r'writing save    files for: i_time =\s+(\d+)', l)
                    if m is not None:
                        tsteps = int(m.group(1))

                    m = re.search(r'S3D Time taken:\s+(\d+\.\d+) s', l)
                    if m is not None:
                        timetaken = float(m.group(1))

                    #print l
                tput = 1.0 * size * size * size * tsteps / timetaken
                #print tsteps, timetaken, n, size, tput
                points.append((n, tput * scale))
        except:
            #raise
            pass
        #break
    return points


# S3D Titan DME
def dme_titan():
    nodes = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]
    p = Plot(title = None, xlabel = "Nodes", ylabel = "Throughput (pts/sec/node)", legend_cols = 3,
             xlog = True, xtics = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024], xlim = [ 0.9, 1100 ])
    p.line("Fortran - $48^3$", get_s3d_throughput(nodes, 'data/s3d/titan/cray_dme_48_%d.out', 48))
    p.line("Fortran - $64^3$", get_s3d_throughput(nodes, 'data/s3d/titan/cray_dme_64_%d.out', 64))
    p.line("Fortran - $96^3$", get_s3d_throughput(nodes, 'data/s3d/titan/cray_dme_96_%d.out', 96))

    p.line("OpenACC - $48^3$", get_s3d_throughput(nodes, 'data/s3d/titan/openacc_dme_48_%d.out', 48))
    p.line("OpenACC - $64^3$", get_s3d_throughput(nodes, 'data/s3d/titan/openacc_dme_64_%d.out', 64))
    p.spacer()

    p.line("Legion - $48^3$", get_s3d_throughput(nodes, 'data/s3d/titan/leg_dme_48_%d.out', 48))
    p.line("Legion - $64^3$", get_s3d_throughput(nodes, 'data/s3d/titan/leg_dme_64_%d.out', 64))
    p.line("Legion - $96^3$", get_s3d_throughput(nodes, 'data/s3d/titan/leg_dme_96_%d.out', 96))

    p.save_file('figs/dme_titan.pdf')

# S3D Titan heptane
def hept_titan():
    nodes = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]
    p = Plot(title = None, xlabel = "Nodes", ylabel = "Throughput (pts/sec/node)", legend_cols = 3,
             xlog = True, xtics = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024], xlim = [ 0.9, 1100 ])
    p.line("Fortran - $48^3$", get_s3d_throughput(nodes, 'data/s3d/titan/cray_hept_48_%d.out', 48))
    p.line("Fortran - $64^3$", get_s3d_throughput(nodes, 'data/s3d/titan/cray_hept_64_%d.out', 64))
    p.line("Fortran - $96^3$", get_s3d_throughput(nodes, 'data/s3d/titan/cray_hept_96_%d.out', 96))

    p.line("OpenACC - $48^3$", get_s3d_throughput(nodes, 'data/s3d/titan/openacc_hept_48_%d.out', 48))
    p.line("OpenACC - $64^3$", get_s3d_throughput(nodes, 'data/s3d/titan/openacc_hept_64_%d.out', 64))
    p.spacer()

    p.line("Legion - $48^3$", get_s3d_throughput(nodes, 'data/s3d/titan/leg_hept_48_%d.out', 48))
    p.line("Legion - $64^3$", get_s3d_throughput(nodes, 'data/s3d/titan/leg_hept_64_%d.out', 64))
    p.line("Legion - $96^3$", get_s3d_throughput(nodes, 'data/s3d/titan/leg_hept_96_%d.out', 96))

    p.save_file('figs/hept_titan.pdf')

# S3D Keeneland DME
def dme_knl():
    nodes = [1, 2, 4, 8, 16, 32] #, 64, 128]
    p = Plot(title = None, xlabel = "Nodes", ylabel = "Throughput (pts/sec/node)", legend_cols = 2,
             xlog = True, xtics = [1, 2, 4, 8, 16, 32], xlim = [ 0.9, 35 ])
    p.line("Fortran - $48^3$", get_s3d_throughput(nodes, 'data/s3d/knl/ifort_dme_48_%d.out', 48))
    p.line("Fortran - $64^3$", get_s3d_throughput(nodes, 'data/s3d/knl/ifort_dme_64_%d.out', 64))
    p.line("Fortran - $96^3$", get_s3d_throughput(nodes, 'data/s3d/knl/ifort_dme_96_%d.out', 96))

    p.line("Legion - $48^3$", get_s3d_throughput(nodes, 'data/s3d/knl/leg_dme_48_%d_all1.out', 48))
    p.line("Legion - $64^3$", get_s3d_throughput(nodes, 'data/s3d/knl/leg_dme_64_%d_all1.out', 64))
    p.line("Legion - $96^3$", get_s3d_throughput(nodes, 'data/s3d/knl/leg_dme_96_%d_mixed1.out', 96))

    p.save_file('figs/dme_knl.pdf')

# S3D Keeneland heptane
def hept_knl():
    nodes = [1, 2, 4, 8, 16, 32] #, 64, 128]
    p = Plot(title = None, xlabel = "Nodes", ylabel = "Throughput (pts/sec/node)", legend_cols = 2,
             xlog = True, xtics = [1, 2, 4, 8, 16, 32], xlim = [ 0.9, 35 ])
    p.line("Fortran - $48^3$", get_s3d_throughput(nodes, 'data/s3d/knl/ifort_hept_48_%d.out', 48))
    p.line("Fortran - $64^3$", get_s3d_throughput(nodes, 'data/s3d/knl/ifort_hept_64_%d.out', 64))
    p.line("Fortran - $96^3$", get_s3d_throughput(nodes, 'data/s3d/knl/ifort_hept_96_%d.out', 96))

    p.line("Legion - $48^3$", get_s3d_throughput(nodes, 'data/s3d/knl/leg_hept_48_%d_all1.out', 48))
    p.line("Legion - $64^3$", get_s3d_throughput(nodes, 'data/s3d/knl/leg_hept_64_%d_all1.out', 64))
    p.line("Legion - $96^3$", get_s3d_throughput(nodes, 'data/s3d/knl/leg_hept_96_%d_mixed1.out', 96))

    p.save_file('figs/hept_knl.pdf')

def ckt_perf():
    p = Plot(title = None, xlabel = "Nodes", ylabel = "Throughput (GFLOPS)", legend = 2,
             xlog = False, xtics = [1, 2, 4, 6, 8], xlim = [ 0.5, 8.5 ])
    #p.line("Structure Slicing", [(1,363.902), (2,650.439), (4,875.321), (8,909.604)])
    #p.line("No Slicing", [(1,288.278), (2,514.403), (4,788.617), (8,687.052)])
    p.line("Keeneland - Slicing", [(1,361.35), (2,644.269), (3,779.208), (4,947.330), (6, 918.957), (8,1231.263)])
    p.line("Keeneland - No Slicing", [(1,298.626),(2,518.537), (3,627.269), (4,787.52), (6,803.188), (8,1037.061)])
    #p.line("Keeneland - Old (SC2012)", [(1,39.21), (2,77.761), (4,154.756), (8,306.340)])

    p.line("Titan - Slicing", [(1,597.284),(2,978.318),(4,1193.491),(6,1117.766),(8,898.599)])
    p.line("Titan - No Slicing", [(1,289.243),(2,378.326),(4,333.68),(8,334.867)])

    p.save_file('figs/ckt_perf.pdf')

def fluid_large():
    p = Plot(title = None, xlabel = "Nodes", ylabel = "Throughput (Mparticles/sec)", legend = 2,
             xlog = False, xtics = [1, 2, 4, 6, 8], xlim = [ 0.5, 8.5 ])

    p.line("No Slicing", zip([1,3,4,6,8], map(lambda x: 8.1*4/x, [15.668, 8.457, 6.709, 6.655, 7.690])))
    p.line("Slicing (SOA)", zip([1,3,4,6,8], map(lambda x: 8.1*4/x, [11.601, 5.351, 4.407, 5.156, 8.959])))
    p.line("Slicing (AOS)", zip([1,3,4,6,8], map(lambda x: 8.1*4/x, [12.872, 117.268, 158.847, 40.384, 34.548])))
    p.line("Slicing (Mixed)", zip([1,3,4,6,8], map(lambda x: 8.1*4/x, [12.679, 7.755, 4.780, 5.509, 8.221])))
    #p.line("No Slicing", zip([1,2,3,4,6,8], map(lambda x: 8.1*4/x, [15.668, 14.165, 8.457, 6.709, 6.655, 7.690])))
    #p.line("Slicing (SOA)", zip([1,2,3,4,6,8], map(lambda x: 8.1*4/x, [11.601, 11.755, 5.351, 4.407, 5.156, 8.959])))

    p.save_file('figs/fluid_large.pdf')

def fluid_small():
    p = Plot(title = None, xlabel = "Nodes", ylabel = "Throughput (Mparticles/sec)", legend = 2,
             xlog = False, xtics = [1, 2, 4, 6, 8], xlim = [ 0.5, 8.5 ])

    p.line("No Slicing", zip([1,2,4,6,8], map(lambda x: 2.4*4/x, [7.514, 4.018, 3.634, 3.434, 3.343])))
    p.line("Slicing (SOA)", zip([1,2,4,6,8], map(lambda x: 2.4*4/x, [8.223, 5.032, 4.662, 4.584, 4.509])))
    p.line("Slicing (AOS)", zip([1,2,4,6,8], map(lambda x: 2.4*4/x, [6.538, 4.631, 5.151, 4.908, 4.944])))
    p.line("Slicing (Mixed)", zip([1,2,4,6,8], map(lambda x: 2.4*4/x, [6.543, 3.834, 3.259, 3.290, 2.991])))

    p.save_file('figs/fluid_small.pdf')

def fluid_working_set():
    import matplotlib.pyplot as plt

    color1 = (0.404,0.749,0.361)
    color2 = (0.427,0.800,0.855)

    # calculate values based on dimensions and split into 8 planes
    real_cells_per_core = 140 * 194 * 140 / 64.0
    edge_cells_per_core = ((140.0/8.0 + 2) * (140.0/8.0 + 2) * (194 + 2)) - ((140.0 / 8.0) * (140.0 / 8.0) - 194)

    old_wsets = [ 860 * (real_cells_per_core + edge_cells_per_core),
                  860 * (real_cells_per_core + 2 * edge_cells_per_core),
                  860 * (real_cells_per_core + 2 * edge_cells_per_core),
                  860 * (real_cells_per_core + edge_cells_per_core) ]
    new_wsets = [ (64 * 9 + 4) * real_cells_per_core + (64 * 9 + 4) * edge_cells_per_core,
                  (64 * 9 + 4) * real_cells_per_core + 2 * (64 * 9 + 4) * edge_cells_per_core,
                  (64 * 4 + 4) * real_cells_per_core + (64 * 4 + 4) * edge_cells_per_core,
                  (64 * 13 + 4) * real_cells_per_core + (64 * 7 + 4) * edge_cells_per_core ]

    # traffic is based on 7 splitting planes
    cells_exchanged = 7 * 2 * 140 * 194
    old_traffic = [ 860 * cells_exchanged,
                    860 * cells_exchanged,
                    860 * cells_exchanged,
                    860 * cells_exchanged ]
    new_traffic = [ (64 * 9 + 4) * cells_exchanged,
                    (64 * 9 + 4) * cells_exchanged,
                    (64 * 3 + 4) * cells_exchanged,
                    64 * cells_exchanged ]

    # scale to MB
    old_wsets = [ x / 1048576.0 for x in old_wsets ]
    new_wsets = [ x / 1048576.0 for x in new_wsets ]
    old_traffic = [ x / 1048576.0 for x in old_traffic ]
    new_traffic = [ x / 1048576.0 for x in new_traffic ]

    for o, n in zip(old_wsets, new_wsets):
        print "a", 1.0 - n/o
    print sum(new_traffic) / sum(old_traffic)

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize = (8,5))

    ax1.set_xlim([0.5,12.5])
    ax1.set_ylim([0,200])
    ax1.bar([1,4,7,10], old_wsets, 1, color = color1)
    ax1.bar([2,5,8,11], new_wsets, 1, color = color2)
    ax1.set_ylabel('Working Set per Core (MB)')
    ax1.set_xlabel('Phase')
    ax1.set_xticks([2,5,8,11])
    ax1.set_xticklabels(['IC','RR','SD','GF'])
    ax2.set_xlim([0.5,12.5])
    ax2.set_ylim([0,400])
    r1 = ax2.bar([1,4,7,10], old_traffic, 1, color = color1)
    r2 = ax2.bar([2,5,8,11], new_traffic, 1, color = color2)
    ax2.yaxis.tick_right()
    ax2.yaxis.set_label_position("right")
    ax2.set_ylabel('Network Traffic (MB)')
    ax2.set_xlabel('Phase')
    ax2.set_xticks([2,5,8,11])
    ax2.set_xticklabels(['IC','RR','SD','GF'])
    ax2.legend((r1[0], r2[0]), ("No Slicing", "Structure Slicing"),
               bbox_to_anchor = (-0.2, 0.78, 0.5, 0.2), prop={'size':10} )


    fig.savefig('figs/fluid_wset.pdf', format = 'pdf', bbox_inches = 'tight')


# Disabling these since they no longer are where the plots come from
#dme_titan()
#hept_titan()
#dme_knl()
#hept_knl()

ckt_perf()

fluid_small()
fluid_large()
fluid_working_set()
