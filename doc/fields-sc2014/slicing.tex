
\section{Structure Slicing}
\label{sec:slicing}
In this section we present extensions to the Legion programming
model for structure slicing.  We describe how
fields are added to logical regions (Section~\ref{subsec:fields}),
how field information is used to infer non-interference
of tasks (Section~\ref{subsec:implicit}), and extensions 
to the Legion mapping interface for controlling
data layout (Section~\ref{subsec:mapping}).
\subsection{Logical Regions with Fields}
\label{subsec:fields}

In the original Legion design \cite{Legion12} (which for brevity we will call {\em original Legion}),
 logical regions are a
cross product of an {\em index space} $I$ \cite{Chamberlain:Chapel,ZPL04} and a 
type $T$. An index space is an abstract set which defines the set
of entries in a logical region (e.g., a set of opaque pointers, or
a set of points in a Cartesian grid of arbitrary dimensions).  
For each point in $I$, there is a corresponding object of type $T$ 
in the logical region. The type $T$ is not restricted and can be either
a base data type or a compound data type.
Logical regions can be subdivided by dynamic {\em partitioning} operations,
which subset the index space to define subregions.  
%In simple cases where
%the index space is a dense set of points in $n$-space, logical regions correspond
%to $n$-dimensional arrays with element type $T$ and partitioning corresponds
%to array blocking, but with no implied placement of the subregions in the
%memory hierarchy.  Like all other programming models we know of,
%original Legion provides no mechanism for subdividing the collection by fields
%of the type $T$ in cases where $T$ is a structure type.

To extend logical regions to support structure slicing, we introduce
{\em field spaces}.  Instead of using statically defined types,
logical regions are created at runtime by taking the cross product of
an index space $I$ and a field space $F$.  
%(Existing Legion code can
%use a default field space with one large field, but will not receive
%the benefits of structure slicing.)  
Each field $f \in F$ has a type $T_f$.  A pair $\langle i, f \rangle$ where $i \in I$ 
and $f \in F$ uniquely identifies an entry of type $T_f$ in the logical region.

%As the name suggests, structure slicing assumes that there
%is some available structure in the application data to slice.  In 
%particular, we assume that any data that is shared between tasks in an
%application is grouped into one or more {\em collections}.  There are
%no requirements on the size or layout of these collections. 
%nor do we insist that collections be made up of distinct objects.  
%In C or Fortran, a collection might correspond to a static or dynamically 
%allocated array, or to the elements of a linked list.
%In Legion, the collections are logical regions.

%We require that each collection in our application be associated with a
%{\em field space}, which is a set of names of all of the fields that might
%exist on any object in the collection.  A field might not exist for a particular
%object if the underlying programming model supports mixed collections, but the
%problem of an attempted access to a non-existent field is left to the 
%model---we are interested in cases where a field will not be accessed for any object
%in the collection.  There is one very important constraint on field spaces.  We
%require that the field spaces be {\em disjoint}---i.e., no piece of data may be accessible
%through two different names in a field space.  This does not completely disallow overlaid
%data structures such as C's {\tt union} type, but it does require that the entire union
%be contained in a single field in the field space.

%Legion's logical regions do not already record the fields of objects,
%so we introduce an explicit {\tt FieldSpace} construct 
%and require one to be specified as part of the creation of a logical
%region.  (Existing Legion code can use a default {\tt FieldSpace} with
%one large field, but will not get the benefit of structure slicing.)

%\subsection{Dynamic Field Spaces}
%\label{subsec:alloc}

Fields can be dynamically added to and removed from logical regions.
%The most obvious reason
%is to support dynamically typed languages, or even statically typed languages that
%allow dynamic loading of compiled code (e.g., Java).  A less obvious case applies to
%fully static languages like C and is common in scientific computing applications:
%when the names of all the fields are statically known but the liveness
%of the data in those fields is variable.
To understand the need for dynamically allocated fields, 
consider a program that computes a temporary for every
element in a collection and then performs a stencil computation
over that temporary before summing the result with another field.
The need for such temporary fields is common, and in many cases programmers 
will allocate {\em scratch} fields in static data types that are re-used 
throughout long computations like S3D's {\tt rhsf} function.

There are two problems with how scratch fields are allocated and used in
current programs.  First, scratch fields consume
memory at all times.  Programmers often address this memory bloat by
reusing a single scratch field for several (hopefully non-overlapping)
temporary variables, at a significant cost to code maintainability.
However, reuse of scratch fields can introduce false dependencies between
two otherwise independent tasks that happen to reuse the same
scratch field.  Second, scratch fields add overhead to data transfers:
The scratch fields of a structure in C or Fortran are copied along
with all the other fields, whether they have data that will be used
by the receiving computation or not.  For
applications such as S3D that use a large number of scratch fields and
are constrained by system bandwidth, the costs of copying unused scratch
fields can be large.

By allowing the applications to add fields to a field space when needed
and remove them after the last use, false dependencies
can be eliminated and memory footprint and transfer costs reduced.  
%In Section~\ref{sec:impl} we describe our support for this feature in Legion.

\subsection{Field-Based Non-Interference}
\label{subsec:implicit}

A Legion application is decomposed into a hierarchy of {\em tasks}.
Informally, a pair of tasks $t_1$ and $t_2$ is {\em non-interfering} 
(written $t_1 \# t_2$) if executing them in either
order or concurrently cannot cause a difference in their behavior or the observed state of 
memory after both tasks have finished.  The basic semantic guarantee
of Legion is that if two tasks can interfere, then they will be
executed in the original program order, thus giving Legion a default
sequential semantics that is easy to understand and aids programmer reasoning.

To extract parallelism it is desirable for the Legion runtime
to prove as many pairs of tasks are non-interfering as possible.  To facilitate 
reasoning about non-interference, Legion requires that each task name the logical
regions it will access.  If two tasks access disjoint regions, for example, the runtime can prove the
tasks are non-interfering and safely execute them in parallel.  To enable finer grain reasoning when
two tasks access the same
region, Legion also requires tasks declare their {\em privileges} on region arguments
(e.g., read-only, read-write, reduce).  Thus, as another example, when two tasks access
the same region $r$ but with read-only privileges, the two tasks are non-interfering on $r$.

%To use structure slicing to identify parallelism in an application, we
%assume that the application is decomposed into a set of {\em tasks}, which 
%may be functions (in the case of Legion),
%or loops (in the case of OpenMP or OpenACC), or any other unit of
%computation understood by a programming system.  We also are
%given a partial ordering of the tasks specifying the constraints on the
%observable execution order.  (In a language without explicit parallelism
%constructs, this will usually be a total ordering that matches program 
%order.)  The goal is to identify implicit parallelism: pairs of tasks $t_1$ and $t_2$ for which
%an ordering constraint was specified (i.e., $t_1 < t_2$), but the two tasks
%are {\em non-interfering} (written $t_1 \# t_2$) and can therefore be executed in either
%order (or concurrently) without causing a difference in the observed
%state of memory after their execution.

%There are a number of sufficient conditions for the non-interference
%of two tasks.  Structure slicing adds a field-based non-interference condition. If
%two tasks can be shown to use disjoint subsets of fields, or the same fields with
%compatible privileges, then they cannot interfere
%with each other and may be run in parallel.  
%As with all analyses of this form,
%it can be performed statically, dynamically, speculatively, or even some combination.

%In our structure slicing extension to Legion, we make use of Legion's
%existing privilege annotations on tasks (recall
%Section~\ref{subsec:legion}), which specify which logical regions
%(i.e.,  collections) they access and whether they perform
%reads, writes, or commutative reduction operations.  By augmenting
%these privilege declarations to also include the subset of the
%collection's field space to which accesses will be contained, we are
%able to incorporate structure slicing into the Legion runtime's
%existing non-interference checks in a disciplined way that preserves
%the safety guarantees shown in \cite{LegionTypes13}.  Following the
%methodology of that paper, we first define a precise
%non-interference test based on the actual execution of two tasks, and
%then show a sound approximation of that test that can be performed
%using our augmented privileges.

\makeatletter
\newcommand{\nonint}[1][]{\#\@ifmtarg{#1}{}{_{\!\!#1}}}
\makeatother

A full treatment of non-interference in original Legion can be found
in \cite{LegionTypes13}, which derives a sound approximation of the
non-interference test that is efficient enough to be performed at run
time. It also shows how the functional nature of Legion tasks with
effects on logical regions enables a hierarchical scheduling algorithm
and permits distributed non-interference tests to be performed on
different nodes without communication, which is crucial for scaling
Legion to thousands of nodes.

We now extend this framework to include non-interference on fields.
Following the methodology of \cite{LegionTypes13}, we first define a precise
non-interference test based on the actual
execution of two tasks and then show a sound approximation that 
can be efficiently computed using region field privileges.
In \cite{LegionTypes13}, a {\em memory operation} $\epsilon$ is a triple $(l,op,v)$ where $l$ is a memory location,
 $op$ is the operation performed on $l$ ({\em read}, {\em write}, or $\rm\it reduce_o$ with a particular reduction
operator $o$), and $v$ is the value (the value read, written or folded into $l$ using the named
reduction operator).  Non-interference of two memory operations $\epsilon_1$ and
$\epsilon_2$ is then defined as follows:
$$
\begin{array}{l@{ }l}
\epsilon_1\ \nonint\ \epsilon_2 \Leftrightarrow & (op_1 = read \wedge op_2 = read)\ \vee \\
& (op_1 = reduce_{id_1} \wedge op_2 = reduce_{id_2} \\
& \qquad \wedge id_1 = id_2) \vee \\
& l_1 \not= l_2
\end{array}
$$

The first two conditions capture the non-interference of two read
operations or two reduction operations (using the same reduction
operator); these conditions are still sufficient for proving 
non-interference even under fields.  The final condition looks for
accesses to different memory locations $l_1$ and $l_2$.  If we let
$o_1$ and $o_2$ be the base address of the objects accessed by
$\epsilon_1$ and $\epsilon_2$ and let $f_1$ and $f_2$ be the names of
the fields being accessed, we can refine the notion of accessing the
same location to the access of the same field within the same object
$$
l_1 = l_2 \equiv (o_1 = o_2) \wedge (f_1 = f_2)
$$

\noindent and then rewrite the non-interference test as
$$
\begin{array}{l@{ }l}
\epsilon_1\ \nonint\ \epsilon_2 \Leftrightarrow & (op_1 = read \wedge op_2 = read)\ \vee \\
& (op_1 = reduce_{id_1} \wedge op_2 = reduce_{id_2} \\
& \qquad \wedge id_1 = id_2)\ \vee \\
& \bm{o_1 \not= o_2\ \vee} \\
& \bm{f_1 \not= f_2}
\end{array}
$$

This reformulation splits the original different-location test into two tests, one that
identifies data parallelism from accesses to different objects, and
one that identifies task parallelism from accesses to different
fields.  

The non-interference test used in the original Legion runtime is an approximation that works
at the granularity of privileges and regions rather than individual memory
operations.  This coarsening of the test is what makes it practical,
as the cost of a single non-interference test is amortized over many
accesses to the region's data.  A further optimization is to perform
the test based on logical regions rather than {\em physical instances},
the actual physical location(s) where data in that logical region is currently stored.
(Multiple physical instances are permitted for a logical region when data 
has been replicated for improved access.)
The analysis in \cite{LegionTypes13} abstracts this translation of logical regions
to physical instances in a mapping $M$ and shows that a valid mapping $M$
chosen by an application mapper preserves the soundness of the region-based
non-interference test.

To incorporate structure slicing into the non-interference test, we augment the privilege
declarations on tasks to also name the fields on each logical region that a task may access.
Let task $t_i$ access the set of fields $\mbox{\em fields}_i$ of region
$r_i$ with the privilege $priv_i$, and let $M$ be the mapping of logical
regions to physical instances.  The test for non-interference 
of tasks $t_1$ and $t_2$, extended to include structure slicing, is given below.
Here $r_1 * r_2$ is true if $r_1$ and $r_2$ are disjoint logical regions;
$M(r_1) \cap M(r_2)$ is empty if the physical instances $M(r_1)$ and $M(r_2)$ share
no memory locations.
$$
\begin{array}{@{}l@{}l@{}}
\multicolumn{2}{@{}l}{priv_1(r_1,\bm{\mathit{fields}_1}) \nonint[M] priv_2(r_2,\bm{\mathit{fields}_2}) \Leftrightarrow} \\
\hspace*{0.5cm} & (priv_1 = \text{reads} \wedge priv_2 = \text{reads})\ \vee \\
& (priv_1 = \text{reduces}_{id_1} \wedge priv_2 = \text{reduces}_{id_2} \wedge id_1 = id_2)\ \vee \\
& (r_1 * r_2)\ \vee \\
& (M(r_1) \cap M(r_2) = \emptyset)\ \vee \\
& \bm{(\mathit{fields}_1 \cap \mathit{fields}_2 = \emptyset)}
\end{array}
$$

This test is nearly identical to the one in \cite{LegionTypes13}.
The existing Legion runtime non-interference checks (which extract
other forms of parallelism) are left unaffected, and a single new
sufficient condition is added, which checks whether the sets of fields
accessed by two tasks for a given privilege are disjoint.  
This additional check is performed
dynamically (to support dynamic field spaces); we
discuss an efficient implementation in Section~\ref{sec:impl}.
The similarity of the test allows a straightforward extension of the theorems
proving the soundness of the approximate test and its suitability as the
basis for scalable hierarchical scheduling.

\subsection{Field-Based Mapping}
\label{subsec:mapping}

Legion features a dynamic mapping interface
that decouples writing applications from
tuning performance on specific target architectures \cite{Legion12}.  
Programmers work with logical regions with no implied layout or location
in the memory hierarchy.  During program execution the Legion runtime queries a 
{\em mapper object} about how the application should be mapped to the target
architecture. Prior to the incorporation of structure slicing, mapping
a task $t$ in Legion required a mapper to respond to the following
queries from the Legion runtime:

\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
\item Select the target processor for the task $t$
\item For each logical region $r$ that $t$ has privileges, select a
      target memory in which to create or reuse a physical instance of $r$
\end{itemize}

Initially applications use the Legion default mapper which answers these
queries using basic heuristics.  Legion applications are then tuned by
customizing mappers based on application-specific and/or architecture-specific
knowledge. Most importantly, mapping decisions made by mappers cannot 
impact the correctness of Legion applications, which is necessary for
easy porting and tuning of Legion code \cite{Legion12}.

To incorporate structure slicing into the Legion mapping interface
we extend mapper objects to specify the layout of fields within 
physical instances. For each logical region requested for a task, 
Legion mappers are now queried to select a {\em layout schema}  
constraining how the region's index space and fields 
should be linearized in memory when creating a physical instance.  
For example, layout schemas can encode that fields should be laid
out in struct-of-arrays (SOA) format, with data for each field
compactly stored.  Alternatively, data can be laid
out in array-of-structs (AOS) format, or a hybrid format \cite{pharr2012}
that allows several values for each field to be stored 
compactly for use with vectorized SSE or AVX loads
and stores.  Layout specifications also require mappers 
to specify the ordering of points within the index
space for a logical region. Ordering can be done by 
dimensions (e.g., C or Fortran array ordering) or alternatively
with more flexible functions such as Morton space
filling curves. Finally, layout schemas are flexible 
enough to interleave field data with different subsets of 
index spaces. 

\begin{figure}
\centering
\includegraphics[scale=0.45]{figs/layout.pdf}
\vspace{-2mm}
\caption{Example physical region layouts for a 2-D stencil.\label{fig:layouts}}
\vspace{-3mm}
\end{figure}

As an illustrative example, consider a simple 2-D stencil
computation done over a 3-D grid with two different fields: 
A and B. Figure~\ref{fig:layouts} shows three different layouts 
that could be selected for the physical instance with elements of
field A shown in red and elements of field B shown in blue. The first
layout shows the standard AOS layout with the grid serialized
based on a C-ordering of the dimensions. This layout would be
well suited to CPU kernels which perform both stencils simultaneously.
Alternatively, the second layout depicts the standard SOA layout
of the fields, with the array for each field serialized using
C-ordering of the dimensions.  The last layout in Figure~\ref{fig:layouts}
shows how fields can be interleaved with index space dimensions, with
2-D slices of different fields alternating in memory and each
slice laid out with a Fortran-ordering on the grid dimensions.
Such a layout would be extremely useful for describing locality
if both fields are necessary for performing the stencil, but
coalescing of memory accesses are necessary for use on a vectorized
or GPU processor.

In order to maintain the invariant that mapping decisions are independent
of the correctness of Legion applications, we need to update our Legion
implementation to handle various layout specifications. In addition to
handling the necessary data movement operations, a Legion implementation
also needs to apply the necessary transformations when moving data between
physical instances with different data layouts.  As we 
show in Section~\ref{sec:impl}, the cost of transforming data
to support different layouts is often minimal compared to the cost
of data movement itself and any overhead is quickly made up by gains in
memory system performance when executing tasks.

