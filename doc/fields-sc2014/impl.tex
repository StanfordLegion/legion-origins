
\section{Structure Slicing Implementation}
\label{sec:impl}
We now describe an implementation of structure slicing
in Legion.  We first summarize necessary
extensions to the Legion runtime interface 
(Section~\ref{subsec:extension}).  We then present the
three primary modifications to the runtime
system: support for dynamic field non-interference
tests (Section~\ref{subsec:noninter}), modifications to
the mapping interface (Section~\ref{subsec:meta}),
and extensions to data movement routines
(Section~\ref{subsec:movement}).

\subsection{Interface Extensions}
\label{subsec:extension}
To support structure slicing, we extend
the original Legion interface to 
support the dynamic creation and deletion of field spaces
as well as the dynamic creation and deletion of fields.
As discussed in Section~\ref{sec:slicing},
dynamic logical region creation with structure
slicing uses field spaces to specify the available fields on
a logical region.  To support dynamic allocation and 
deallocation of fields, we exploit the natural level of 
indirection between Legion's logical regions and 
physical instances.  
%As discussed in Section~\ref{sec:slicing},physical instances
%are allocations of memory with fixed data layouts in specific 
%locations of the memory hierarchy that contain data for logical
%regions.  
The dynamic allocation of a field $f$ on a logical 
region $r$ guarantees that $f$ is available on future physical 
instances of $r$.  Similarly, the destruction of a field $f$ on $r$
ensures that the field need not be allocated
as a part of future physical instances.

We also extend the task launching mechanism to encode
the necessary information for structure slicing.
Previously, Legion tasks declared
the logical regions they could access along with
any privileges \cite{Legion12}.
In our structure slicing version of Legion, instead of specifying
privileges on entire regions, tasks request privileges
on individual fields of logical regions.  

\subsection{Dynamic Field Non-Interference Tests}
\label{subsec:noninter}

%
% ALEX
% this isn't quite right.  It's not all previously issued tasks that must be
% checked because of the hierarchical scheduling --- this paragraph makes the problem
% sound worse than it is and makes Legion sound possibly unrealistic.  But we don't
% want to explain hierarchical scheduling.  I don't see an easy fix besides leaving it
% alone.
%
The primary challenge in extending a Legion implementation to
incorporate structure slicing information is performing efficient dynamic
non-interference tests.
% at runtime to extract implicit parallelism 
%in a way that is unobtrusive to application performance. 
The introduction of an additional dimension of non-interference
adds dynamic analysis overheads; however, we show 
in Section~\ref{sec:eval} that the cost of this analysis can 
be minimized and often pays for itself by discovering additional
task and data movement parallelism.

In Legion, subtasks launched within a parent task are issued
in program order to the runtime system.  For each subtask $t$ the
runtime must perform non-interference tests between $t$ and any
unfinished subtasks launched within the same parent task \cite{Legion12}.
Recall from Section~\ref{subsec:implicit} that the hierarchical nature
of the non-interference test obviates the need to test against any tasks
launched by another parent \cite{LegionTypes13}.
Extracting parallelism
from this stream of tasks is an inherently sequential process.  
A poor implementation could easily place this analysis on an 
application's critical path, so it is important that the 
non-interference tests be implemented efficiently.

In Section~\ref{subsec:implicit} we defined the three  
disjunctive conditions for demonstrating the 
non-interference of two tasks:
\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
\item access to independent sets of fields, 
\item access to independent sets of regions, 
\item or non-interfering privileges.  
\end{itemize}
These three conditions may be tested in any order.
If any one of the three tests evaluates to {\em true}
then the tasks are non-interfering and any remaining tests can be skipped.
Consequently the choice of test order can have a significant performance impact.

The order we use is region independence, 
field independence, and finally non-interference of privileges.
This ordering stems from the  observation 
that Legion programs commonly express data parallelism at several different 
granularities both across nodes and within nodes.  It is therefore 
most likely that two tasks will be proven to be non-interfering 
using the independence of their logical regions.
After this, field set independence is most likely.  Finally, 
privilege non-interference is a more complicated test and is 
therefore placed last where it is least likely to be performed.  
While it is possible to write Legion applications that
perform better with a different test ordering, it has been our 
experience that the performance of this ordering is sufficient for 
achieving high non-interference test throughput.  We justify this
choice of ordering by showing the breakdown of non-interference
tests for several real applications in Section~\ref{subsec:tests}.

\begin{figure}
\centering
\includegraphics[scale=0.5]{figs/field_disjoint}
\caption{Example Legion Non-Interference Test\label{fig:nonintex}}
\end{figure}

We now build on the Legion non-interference test from
\cite{Legion12,LegionTypes13}.  A {\em region tree} consists of two kinds
of nodes, regions and partitions.  Each region may have one or more child partitions,
and each partition has a number of subregions.  An example region tree is 
shown in Figure~\ref{fig:nonintex}, where boxes are regions and horizontal lines are 
partitions (the $\ast$ notation on a partition indicates the subregions 
of the partition are disjoint).

At every point in time, the Legion runtime
tracks which already-issued tasks are using which regions by
annotating the logical regions in region trees with task and privilege
information.  To extend this algorithm to check field non-interference,
we store the set of fields used in addition to the privilege information
with each task in the region tree, as illustrated in Figure~\ref{fig:nonintex}.
Each task stored in the region tree has already performed its non-interference
test and recorded itself on the regions it is using along with its field and privilege information.
To perform the needed non-interference tests for a new task $T_4$, we begin
by traversing the region tree from the root to where the task has 
requested privileges, in this case the logical region $R_3$.  We only need to 
perform non-interference tests with tasks along this path because regions off of
this path (such as $R_1$ in Figure~\ref{fig:nonintex}), are already known to be disjoint
with $R_3$.  The tasks $T_1$ and $T_3$ can therefore be inferred to be 
non-interfering on logical region usage.  Performing this traversal 
implements the first dimension of our non-interference tests, which examines region usage.

For all tasks that are encountered along the path, we apply the second
and third dimensions of the non-interference test in order.  We first check
for non-interference on fields.  Task $T_4$ is non-interfering with both $T_0$ 
and $T_2$ on fields $a$ and $c$, but interferes with both tasks on field $b$.
For the interfering field $b$, we then perform privilege non-interference
tests.  In this case $T_4$ interferes with $T_0$ because $T_0$ is writing
field $b$ and $T_4$ is reading field $b$, resulting in a true data 
dependence.  However, because both $T_2$ and $T_4$ are reading field $b$,
task $T_4$ is non-interfering with $T_2$ and can potentially run in parallel.

For applications such as S3D with large numbers of fields, it is 
challenging to implement non-interference tests on sets of fields 
efficiently.  To make these tests fast we implement sets of fields 
as bit masks.  For every region and privilege that a task requests, 
a bit mask is inserted into the region tree summarizing the fields used 
for the specific region and privilege.  Using bit-wise operators, fast 
tests for both disjointness and intersection can be performed.  

To further improve the performance of bit mask disjointness testing 
we place a compile-time upper bound on the number of fields that can 
be allocated in a bit mask, which allows fixed storage to be allocated 
for each bit mask. The runtime dynamically maps field allocations for a field
space onto indices in the bit mask, and frees up indices when fields
are deallocated.  While this does place a restriction on the total
number of fields in a field space, it does not limit the total number
of fields in a program, as our implementation supports an unbounded
number of field spaces.  Additionally, the upper bound on fields can 
be programmer controlled, and increasing the upper bound simply 
increases the cost of the dynamic non-interference analysis.
 
By fixing an upper bound on the number of fields in a field space,
we can optimize the representation of the bits in the bit mask. In
the general case, bit masks are represented with unsigned 64-bit
integers.  However, when the hardware supports it, 128-bit SSE
and 256-bit AVX vector data types are used along with the corresponding
vectorized instruction for performing logical bit manipulation.

The fixed upper bound on the number of bits in a bit mask also permits another 
important optimization.  The most common operations on bit masks
are testing for disjointness, intersection, and testing emptiness
(e.g., whether any bits are set. For cases where the upper bound on 
the number of bits is large (e.g., more than a 1K bits, which is common 
in S3D) all three of these operations can be accelerated by maintaining 
a single 64-bit word summary mask that represents the logical 
union of all 64-bit words in the bit mask. The test for disjointness,
intersection, and emptiness can be accelerated by performing
them on the summary masks first, and only performing the full
test if needed. These {\em two-level bit masks} are extremely
important for applications such as S3D where the upper bound on
the number of fields in a field space is large, but most
tasks only request privileges on a few fields.

\subsection{Field-Aware Mapping}
\label{subsec:meta}

In addition to modifying the Legion runtime interface, we also extend
the original Legion mapping interface. The original
Legion mapping interface requires mappers to specify on which
processors tasks should be run and the memory in which to place each
physical instance of a requested logical region.  To support structure slicing, 
we also require mappers to specify the layout of data in each physical 
instance using the layout schemas described in Section~\ref{subsec:mapping}.

To support this feature, the Legion runtime stores additional
meta-data for physical instances.  In addition to tracking the
memory location for a physical instance and whether it contains valid data,
the Legion runtime also tracks which fields contain valid data as well
as the layout schema for the physical instance.  To aid mappers in
making intelligent mapping decisions, this meta-data is also made
available through the mapping interface to allow mappers to know
where current instances reside and their data layout.  Using this
information, mappers can either choose to use an existing physical
instance with a given data layout, or create a new instance and
specify the desired layout.

Supporting dynamically chosen data layouts challenges an important 
principle of the Legion programming model.  Legion guarantees that 
mapping decisions cannot impact the correctness of an application.
To maintain this property, the runtime provides generic {\em accessor} 
objects which introduce the necessary level of indirection to mediate 
the reading and writing of physical instances with arbitrarily chosen 
layouts.  Accessors come in two flavors: {\em generic} and 
{\em specialized}. Generic accessors work for all layouts, while
specialized accessors only work for a subset of layouts.  Applications 
can register multiple functionally equivalent task variants using
combinations of generic and specialized accessors.  The Legion 
runtime will automatically select a specialized task variant if a compatible 
one exists for the specified physical instances, or choose a variant with 
generic accessors for handling arbitrary data layouts if no 
specialized variant exists.

%% However, Legion tasks are currently statically compiled functions with
%% assumed data layouts for their region arguments.  Since these static 
%% functions cannot react to dynamically selected physical instance
%% layouts, Legion allows for multiple, functionally equivalent variants
%% of a task to be registered along with their expected data layouts.
%% To maintain the invariant that Legion programs are correct under
%% all mapping decisions, the Legion runtime automatically selects
%% the correct variant of a task to run based on data layout decisions
%% (or report that no such variant exists).  
%While all task variants
%must currently be registered at start-up, an in-progress version
%of Legion will allow them to be generated on demand using staged
%meta-programming task generators\cite{Terra13}.

\subsection{Field-Aware Data Movement}
\label{subsec:movement}

As part of its analysis, the Legion runtime tracks the physical 
instances that contain valid data for each logical region in a
Legion program.  To support structure slicing, we extend our
implementation of the Legion runtime to store data specifically
regarding which fields contain valid data in each physical 
instance.  At different times, different subsets of the fields
within an instance may contain valid data while other fields are
invalid.  In the case when a mapper chooses to re-use an existing
physical instance, the Legion runtime can automatically determine
which fields contain valid data, and which fields require copies
to acquire valid data.  As mentioned in Section~\ref{sec:background},
this knowledge permits Legion to automatically perform copy
elimination and determine when data in individual fields 
within physical instances can safely be reused by multiple tasks.

While Legion can easily infer the necessary copies between physical 
instances, actually performing the copies is more challenging.  
In our previous version of Legion, data movement between physical 
instances could be performed by linear copies of segments of memory \cite{Realm14}.  
By allowing physical instances to contain multiple fields and maintain 
different data layouts for those fields, the problem of moving data 
between physical instances is greatly complicated.  For movement between 
instances with the same layout, we can still use linear copies, however, 
for movement between different layouts additional logic is required for 
transforming data layout.

Fortunately, while the costs of these transformations are relatively
large in shared memory machines, they are small compared to
the cost of moving data in machines with hierarchical memory.  For
example, the cost of moving data over a PCI-E bus or between nodes
will always be higher than for transposing data.  Since most data 
movement in high performance Legion applications is between
distinct memories, the additional cost of transforming data layouts is minimal.

\begin{figure}
\centering
\includegraphics[scale=0.5]{figs/DataMovement}
\caption{Legion Data Movement Pathways\label{fig:pathways}}
\end{figure}

In practice, our field-aware version of the Legion runtime fuses
together both data movement and transformation within its data
movement pathways.  Figure~\ref{fig:pathways} shows several 
examples of data movement pathways in Legion both within a node as
well as between nodes.  To support these data movement
pathways, Legion maintains temporary buffers that 
are registered with various low-level programming APIs (e.g. uGNI, 
IBVerbs, CUDA).  The memory for these buffers is pinned to facilitate 
direct access by the hardware DMA engines in both GPUs and network
interface cards (NICs).
Legion explicitly gathers and scatters data to and from these buffers 
to support bulk data movement operations for higher performance.
This is a common, but tedious, optimization performed by hand in many
applications, and one that happens automatically in Legion.  Our
Legion implementation further leverages the natural level of 
indirection afforded by these gather and scatter operations to 
automatically perform data transformation as part of 
data movement, rendering the cost of transforming data between
physical instances with different layouts virtually free.

To perform the gather and scatter routines, we dedicate a CPU core 
per node to act as a DMA engine.  The DMA core operates on a queue 
of requested data movement operations and knows how to perform fast data 
movement and transformation routines between all pairs of memories
in our system for various data set sizes.  This includes the use 
of fast, in-cache transpose routines and offloading data movement
operations to hardware DMA engines when possible (e.g., using {\tt cudaMemcpy2D}
for gathering and scattering to and from GPU framebuffer memory).
While the decision to dedicate an entire core on each node for
DMA operations may seem excessive, it is reasonable in an 
environment where most applications, such as S3D, are dominated by
the cost of data movement.  If current scaling trends continue,
more applications will fall into this category, likely precipitating
even more exotic hardware DMA engines for moving data.  As these
new features become available, Legion is well positioned
to incorporate them in a way that is transparent to application 
developers and will require no changes to existing Legion
application code.

%\subsection{Accelerating Non-Interference Tests}
%\label{subsec:accel}
%As expected, adding an additional dimension to the non-interference test
%introduces additional overhead in dependence analysis.  To mitigate
%this additional overhead we designed two optimizations for improving the
%performance of non-interference testing, one which reduces 
%asymptotic complexity and another which reduces constant
%overheads.  We describe the two optimizations in this section and quantify
%their performance improvements in Section~\ref{subsec:dep_opt}.

%The first optimization attacks the linear time cost of testing each
%task stored at a node in the region tree for field disjointness.
%Ideally, we can prune large groups
%of tasks from consideration with a single field independence test.  To
%achieve this effect we introduce a {\em field-tree} data structure with
%the following properties:
%\begin{itemize}
%\item Every node in the tree maintains a bit mask for a set of
%      fields and set of tasks whose bit masks are identical
%      or a subset of the node's field mask.  A {\em precise} bit
%      tracks whether all the tasks stored at this node have identical
%      bit masks to the node's bit mask or not.
%\item A parent node can have sub-nodes whose bit masks are subsets 
%      of the parent node's mask.  The bit masks between child nodes
%      may overlap but may never dominate each other.
%\end{itemize}
%Figure~\ref{fig:field_tree} shows an example field tree for bit masks
%with a maximum of four fields.  Field tree data structures support a
%traversal operation which takes a field mask argument and traverses
%all the nodes that overlap with the given field mask and examines all
%the tasks which have overlapping fields within the field tree.  This
%operation is straightforward and starts at the root (node with a bit
%mask of all ones) and traverses all sub-nodes which have a non-empty
%intersection with the mask.  At each node all the tasks are examined.
%If a node is precise, we know all tasks must be examined without
%requiring any field independence tests.  If a node is not precise,
%each task at this node must undergo a field independence test to
%determine if the task should be examined.  Before traversing a
%sub-tree, we test for a non-empty intersection with the child node's mask which serves
%as an upper bound on the possible masks in the sub-tree.  If our traversal mask
%is independent of the sub-mask then we can skip the entire sub-tree.
%
%The other operation supported by field trees is insertion, which has a less 
%obvious implementation.  Insertion traverses the tree until it finds a node
%with the same bit mask as the task being inserted and then appends the task
%to the list in the node.  There are two cases where such a node will not be found.  
%First, the node may not exist, in which case a new node is created and added
%as a child of an existing node.  This situation is illustrated in 
%Figure~\ref{fig:new_node} when task $t_8$ with the bit mask $0100$ is added
%to the field tree data structure.  In the second case, there may be two 
%sub-nodes of the last traversed node which both dominate the mask.
%To avoid duplicating tasks in the field tree 
%(which could exponentially increase the number of tasks stored in the field 
%tree), we instead place the task at the most precise node which dominates 
%the mask and has no siblings which also dominate the mask.  We then mark 
%the node where the task was added as no longer being precise.  This situation is
%shown in Figure~\ref{fig:no_node} when task $t_9$ is added with the bit
%mask $1100$.  
%%It is important to note that the field tree data shape is
%%driven by both the masks that appear and their order.  
%% Something about complexity here?
%
%The second optimization we implemented is designed to reduce the constant 
%overheads associated with performing field independence tests on large 
%bit masks.  The key insight is that fast field independence tests rely 
%primarily on bit mask intersection.  In cases like the heptane mechanism 
%with an upper bound of 2048 fields, the bit mask is stored as thirty-two 
%64-bit words.  Testing for intersection requires performing bit-wise 
%intersections on all thirty-two words.  In practice, most bit masks represent 
%between 1 and 64 fields (although heptane does require several masks with 
%hundreds of fields).  To avoid performing intersection tests
%on all words for bit masks with just a few set bits, we introduce a
%two-level bit mask.
%
%A two-level bit mask stores a single word summary of all the other words in
%the bit mask.  If a bit at index $i$ is set in any of the words in the bit
%mask, then the bit at index $i$ is set in the summary mask.  When performing 
%an intersection test, the two summary masks are first intersected.  If the 
%result is empty, then full intersection is also empty, allowing us to skip
%the test on the full set of words.  Otherwise, we simply perform the full 
%intersection test.  In practice, most bit masks only have a  few set bits 
%and maintaining a summary mask significantly improves performance of field 
%independence tests.
%
