
\section{Performance Evaluation}
\label{sec:eval}

To evaluate the benefits of structure slicing, we consider
three Legion applications on two heterogeneous
supercomputing systems.  In addition to examining both strong and 
weak scaling scenarios, we use data made available by Legion's
profiling tools to highlight several interesting points.

Two of our applications, Circuit and Fluid, are based on the original
Legion versions described in \cite{Legion12}.  The third is S3D, a production
application \cite{S3D09}. There is no original Legion version of S3D; we compare 
with existing vectorized Fortran and OpenACC implementations.
% exists - as described in 
%Section~\ref{sec:background}, an implementation that could not use structure 
%slicing would not be able to expose enough parallelism to utilize all the 
%processing power available in modern supercomputers.

We ran experiments on Keeneland \cite{Keeneland} and Titan \cite{Top500}.  
Both machines are high performance supercomputers with
a mix of x86 CPUs and NVIDIA GPUs.  However, there is a considerable
difference between the computing power of various processors within these
machines.  The Keeneland Full Scale (KFS) system is an upgraded version of
the machine used for the original Legion experiments \cite{Legion12}.
The CPUs have been upgraded to 16-core Intel Sandy Bridge CPUs with 
support for AVX instructions, and the Infiniband interconnect has been 
upgraded from QDR to FDR.  The three M2090 Fermi GPUs were not upgraded,
but make use of a faster PCI-Express bus.

Titan is a larger system with over 18,000 nodes.  Each node has an
AMD Interlagos CPU with 16 first-generation Bulldozer cores, which perform
poorly compared to the Sandy Bridge cores on Keeneland.  Each Titan node
does possess a single Kepler K20X GPU with nearly equivalent performance 
to all three M2090 GPUs on a Keeneland node.  Titan is connected by a 
Cray Gemini interconnect with a 3D torus topology.

As evidenced by these two machines, heterogeneity is often present in more than
just processor kinds.  By having components from different technology generations,
there are different ratios between compute throughput of different
processors.  On Keeneland, CPU and GPU throughput is evenly matched, 
while on Titan there is a severe imbalance.  As we will see, these
differences necessitate exploring different mapping strategies
on each architecture.

\subsection{Circuit}
\label{subsec:circuit}

The first Legion application we examine is Circuit, which simulates 
the behavior of an electrical circuit, defined as an arbitrary mesh 
in which edges are electronic components and nodes are the points at 
which they connect.  We have two versions: a field-aware version
and a baseline version that differs only
in using a single field for entire structures, eliminating any
benefit from structure slicing.

\begin{figure}
\includegraphics[scale=0.5]{figs/ckt_perf.pdf}
\caption{Circuit Performance\label{fig:ckt_perf}}
\end{figure}

The performance of the Circuit application was measured on both Keeneland
and Titan and is shown in Figure~\ref{fig:ckt_perf}.  The size of the circuit 
being simulated is kept constant as the number of nodes 
is increased to demonstrate strong scaling.  Several of the leaf computation tasks
in the application have been optimized compared to the version that was tested in \cite{Legion12}.
While this greatly improved single-node 
performance, the reduction in computation time
increases the relative cost of Legion runtime and communication 
overhead, which is visible in the relatively poor strong 
scaling of the baseline, especially on Titan.  There is no task 
parallelism to make use of in Circuit, but structure 
slicing is able to reduce the network traffic by 
only sending fields that are needed or have been updated
by another task, and allows the use of the GPU-preferred SOA layout of data.
This results in performance improvements of
up to 19\% on Keeneland and 257\% on Titan, which is much more sensitive to data layout.

\subsection{Fluid}
\label{subsec:fluid}

Fluid is a Legion port of the {\em fluidanimate} 
simulation from the PARSEC \cite{PARSEC09} suite.  Although 
the original PARSEC version of Fluid was designed for a single node,
the original Legion version achieves modest performance improvements 
and scaling on multiple nodes by replacing the fine-grain mutexes used in the PARSEC 
implementation with coarser-grain scheduling using logical regions \cite{Legion12}.

\begin{figure}
\includegraphics[scale=0.45]{figs/fluid_wset.pdf}
\caption{Fluid: Working Set Sizes and Network Traffic\label{fig:fluid_phases}}
\end{figure}

A single simulation time step in Fluid consists of four phases.  For both 
the original Legion version of Fluid and the new structure 
slicing version, Figure~\ref{fig:fluid_phases} shows
the working set and the amount of network traffic between each of the
four phases: initialize cells (IC), rebuild reduce (RR), 
scatter densities (SD), and gather forces (GF).  Structure slicing allows 
the Legion runtime to reduce working sets by 27-80\% and the total network 
traffic per time step by 59\%.

The relatively small problem sizes used in this benchmark present
an interesting challenge.  For multi-node runs, communication costs 
quickly dominate execution.  The reduction in bytes of network traffic
required due to structure slicing helps, but only if the data layout 
is one that can be efficiently transferred by the NIC.  A 
structure-of-arrays (SOA) layout addresses this problem, while an 
array-of-structures (AOS) layout results in many sparse transfers for
individual fields.  However, an AOS layout can yield higher performance
for CPU tasks if the working set fits in the CPU's cache.

\begin{figure}
\includegraphics[scale=0.45]{figs/fluid_small.pdf}
\caption{Fluid Performance - Small Data Set\label{fig:fluid_small}}
\end{figure}

\begin{figure}
\includegraphics[scale=0.45]{figs/fluid_large.pdf}
\vspace*{-2mm}
\caption{Fluid Performance - Large Data Set\label{fig:fluid_large}}
\vspace*{-3mm}
\end{figure}

There is therefore a tension between the SOA layout preferred by the NIC 
and the AOS layout preferred by the CPU.  In existing programming models, 
the programmer would be forced to hard code a single decision into the source.
The Legion mapping interface solves this problem by permitting experimentation
with multiple different mapping strategies in order to select the best one.  
We try three different
strategies for laying out physical instances: all AOS, all SOA, and a mixed
strategy that uses AOS layout for instances local to a node while using SOA 
for instances shared between nodes.  Figures~\ref{fig:fluid_small} and~\ref{fig:fluid_large} show
performance results for each of these three strategies on Titan on two
different problem sizes.  For the small problem size, the mixed mapping
strategy works best by matching AOS performance on a single node
(where no data is shared) and exceeding the SOA-based performance 
for larger node counts.  On the larger problem size, data no longer fits
in cache and therefore the full SOA mapping strategy performs best.  In 
contrast, the volume of sparse inter-node transfers that result from the AOS mapping
strategy can no longer be handled by the runtime's data layout transformation buffers, and
the performance of AOS drops precipitously.
The Legion mapping interface makes it possible to explore the tradeoffs
inherent in mapping decisions seamlessly with no code modifications to 
find the optimal performance points.

\subsection{Non-Interference Tests}
\label{subsec:tests}

\newcommand{\appnonint}[7]{
\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
\node (n1) at (0,0) {\footnotesize\begin{tabular}{c}no tests \\ $0.0 \%$\end{tabular}};
\node (n2) at (2,1) [minimum width=0.4in] {\footnotesize\begin{tabular}{c}regions \\ $#1 \%$\end{tabular}};
\node (n3) at (2,0) [minimum width=0.4in] {\footnotesize\begin{tabular}{c}fields \\ $#2 \%$\end{tabular}};
\node (n4) at (2,-1) [minimum width=0.4in] {\footnotesize\begin{tabular}{c}privs \\ $#3 \%$\end{tabular}};
\draw [->] (n1.north east) -- (n2.west);
\draw [->] (n1) -- (n3);
\draw [->] (n1.south east) -- (n4.west);
\node (n5) at (4.5,1) [minimum width=0.7in] {\footnotesize\begin{tabular}{c}regions+fields \\ $#4 \%$\end{tabular}};
\node (n6) at (4.5,0) [minimum width=0.7in] {\footnotesize\begin{tabular}{c}regions+privs \\ $#5 \%$\end{tabular}};
\node (n7) at (4.5,-1) [minimum width=0.7in] {\footnotesize\begin{tabular}{c}fields+privs \\ $#6 \%$\end{tabular}};
\draw [->] (n2) -- (n5);
\draw [->] (n2) -- (n6);
\draw [->] (n3) -- (n5);
\draw [->] (n3) -- (n7);
\draw [->] (n4) -- (n6);
\draw [->] (n4) -- (n7);
\node (n8) at (7,0) {\footnotesize\begin{tabular}{c}all tests \\ $#7 \%$\end{tabular}};
\draw [->] (n5.east) -- (n8.north west);
\draw [->] (n6) -- (n8);
\draw [->] (n7.east) -- (n8.south west);
\end{tikzpicture}
}

\begin{figure}
\centering
\subfigure[Circuit]{
\appnonint{69.0}{43.6}{25.7}{81.1}{76.3}{60.5}{85.5}
}
\subfigure[Fluid]{
\appnonint{99.2}{43.2}{20.5}{99.4}{99.7}{54.1}{99.8}
}
\subfigure[S3D]{
\appnonint{33.1}{98.4}{32.2}{98.6}{52.7}{99.4}{99.5}
}
\caption{Non-Interference Test Success Rates by Application\label{fig:nonint_venn}}
\end{figure}

As described in Section~\ref{sec:impl}, there are three
dimensions to non-interference tests in Legion.  We now
provide empirical evidence for our chosen ordering of these tests.
Figure~\ref{fig:nonint_venn} shows decision diagrams for
potential orderings of non-interference dimensions tests
for each application.  At each node, we show the percentage
of all non-interference tests that would succeed with that subset of
the tests.  The percentage at the
end shows the overall percentage of non-interference tests 
that succeed for a given application.

The goal is to minimize the overall cost of the tests, which
favors the early use of cheaper and/or more effective tests.
Although there is considerable variability between applications,
region non-interference is the most effective test overall.
As discussed in Section~\ref{subsec:noninter}, the Legion runtime's
region tree data structure makes this test inexpensive, and 
it is the clear choice for the first test.
The combination of fast bit-mask tests with
the benefit of finding significant parallelism in applications
with many fields such as S3D justifies performing field
non-interference second.  Finally, the
more expensive privilege non-interference test is placed
last to minimize the number of invocations.

\subsection{S3D}
\label{subsec:s3d}

%% \begin{figure}
%% \footnotesize
%% \begin{tabular}{cll}
%% & Keeneland & Titan \\
%% \hline
%% Fortran & ifort 12.1.5 & crayftn 8.2.2 (-O2) \\
%% \vspace*{0.1in}
%% &  (-O3 -xT -prefetch -ip) & \\
%% OpenACC & \quad - & crayftn 8.2.2 \\
%% & & (-hipa1 -hacc\_model=fast\_addr \\
%% \vspace*{0.1in}
%% & & \ -hacc\_model=auto\_async\_all) \\
%% Legion & icc 12.1.5 (-O2 -march=native) & gcc 4.8.2 (-O3 -march=bdver1) \\
%% & nvcc 5.5 (-O2) & nvcc 5.5 (-O2)
%% \end{tabular}
%% \caption{S3D: Compiler Versions and Flags Used\label{fig:s3d_flags}}
%% \end{figure}

\begin{figure*}[ht!]
\centering
\subfigure[Keeneland]{
\label{fig:dme_knl}
\includegraphics[scale=0.33]{figs/dme_knl}
}
\subfigure[Titan]{
\label{fig:dme_titan}
\includegraphics[scale=0.23]{figs/dme_titan}
}
\vspace{-2mm}
\caption{S3D Performance - DME}
\vspace{-1mm}
\end{figure*}

\begin{figure*}[ht!]
\centering
\subfigure[Keeneland]{
\label{fig:hept_knl}
\includegraphics[scale=0.33]{figs/hept_knl.pdf}
}
\subfigure[Titan]{
\label{fig:hept_titan}
\includegraphics[scale=0.23]{figs/hept_titan.pdf}
}
\vspace{-2mm}
\caption{S3D Performance - Heptane}
\vspace{-1mm}
\end{figure*}

The final Legion application we use in our evaluation is a Legion 
port of S3D \cite{S3D09}, a production combustion simulation code
initially written in more than 200K lines of Fortran.  We compare against 
two versions of S3D: a CPU-only version and an improved hybrid version
from \cite{S3DACC12} that uses OpenACC.
%% The compilers and flags used
%% to compile each of these versions on both Keeneland and Titan are
%% shown in Figure~\ref{fig:s3d_flags}.

In practice, the S3D algorithm was designed for weak scaling to support
higher fidelity simulations when running on larger machines.
In these experiments, we demonstrate weak scaling by holding the 
problem size per node constant (at either $48^3$, $64^3$, or $96^3$ grid points per node)
as we scale out to larger node counts.  Our graphs show the throughput per node averaged
over 100 time steps, with a flat line representing perfect weak scaling.  We show 
results for two different chemical mechanisms: DME and heptane.  Results for the 
DME mechanism are in Figure~\ref{fig:dme_knl} for Keeneland and 
Figure~\ref{fig:dme_titan} for Titan, while the heptane mechanism results are in 
Figures~\ref{fig:hept_knl} for Keeneland
and~\ref{fig:hept_titan} for Titan.

S3D consists of hundreds of Legion tasks that are launched every time step. 
The most obvious challenge is discovering the best way
to map all of these tasks onto the available processors for both Titan and
Keeneland.  To address this problem, we developed several Legion mappers that
support various mapping strategies designed to both balance work and minimize
data movement.  The two most successful mappers are a {\em mixed} mapping
strategy that balances work between CPUs and GPUs, and a {\em GPU-centric} mapping
strategy that keeps most work on GPU processors to minimize data movement between 
the system and framebuffer memories over the PCI-Express bus.

%\begin{figure}[t]
%\centering
%\includegraphics[scale=0.37]{figs/dme_knl.pdf}
%\vspace*{-2mm}
%\caption{S3D Performance on Keeneland - DME\label{fig:dme_knl}}
%\vspace*{-1mm}
%\end{figure}

%\begin{figure}[t]
%\centering
%\includegraphics[scale=0.25]{figs/dme_titan.pdf}
%\vspace*{-2mm}
%\caption{S3D Performance on Titan - DME\label{fig:dme_titan}}
%\vspace*{-1mm}
%\end{figure}

On Keeneland, the Legion version of S3D ranges from marginally faster than the
AVX-vectorized Fortran version for the smallest problem size on the smaller DME mechanism to
up to 3.06X faster for the largest problem size on the larger heptane mechanism.
The variability is due to the bottleneck of the PCI-Express connections between the GPUs and 
the CPUs and the corresponding changes in the mapping strategy used in the Legion 
implementation.  For the smallest problem, the optimal mapping is to place all work on a single
GPU and avoid the latency cost of moving data across the PCI-Express bus.  A Sandy Bridge CPU
and a Fermi M2090 are roughly equal in their achievable double-precision performance, so
parity in S3D performance is expected.  For larger problems, the latency of PCI-Express transfers
can be better hidden, and mapping strategies that spread work across multiple GPUs and the CPU cores
results in significant improvement compared to the CPU-only Fortran implementation.

\begin{figure*}[t!]
\centering
\subfigure[DME]{
\centering
\begin{tabularx}{\columnwidth}{X|XXX}
        & $48^3$ & $64^3$ & $96^3$ \\
\midrule
Fortran & 63\% & 70\% & 84\% \\
OpenACC & 78\% & 85\% & N/A \\
Legion  & 94\% & 88\% & 98\% \\ 
\end{tabularx}
\label{fig:dmeeff}
}
\subfigure[Heptane]{
\begin{tabularx}{\columnwidth}{X|XXX}
        & $48^3$ & $64^3$ & $96^3$ \\ 
\midrule
Fortran & 66\% & 75\% & 86\% \\
OpenACC & 76\% & 79\% & N/A \\
Legion  & 81\% & 79\% & 99\% \\
\end{tabularx}
\label{fig:hepteff}
}
\vspace{-2mm}
\caption{Parallel Efficiency at 1024 Nodes.}
\vspace{-3mm}
\end{figure*}

The results are much different on Titan, due to the extreme disparity in floating-point
capability between the K20X GPU and the Bulldozer CPU cores.  For the $48^3$ and $64^3$
problem sizes, where the GPU-centric mapping strategy is possible, the Legion implementation
outperforms the CPU-only Fortran implementation by factors up to 3.54X for the DME mechanism
and 3.68X for the heptane mechanism.  For the $96^3$ problem sizes, the GPU's limited memory 
capacity forces a mixed mapping strategy that uses both CPU cores and the GPU. The structure 
slicing implementation of Legion automatically infers that only a subset of fields need to be 
moved to the GPU allowing the working set to fit in the constrained framebuffer memory.
The overall performance is greatly reduced due to the significantly slower Bulldozer cores, 
but still exceeds the CPU-only Fortran implementation by up to 1.94X for the DME 
mechanism and 2.10X for the heptane mechanism.

The speedup of the Legion implementation compared to the OpenACC implementation on Titan is
between 31-88\% for the $48^3$ and $64^3$ problem sizes and can be attributed to two factors.
First, the Legion implementation is able to use the computational power of both the 
Bulldozer cores and the K20X GPU, whereas the OpenACC code in many cases leaves the CPU cores idle 
while performing work solely on the GPU.  Second, the latency of transferring data between 
the GPU's memory and the main system memory can be significant.  The extra task and data
movement parallelism discovered by structure slicing enables Legion to run tasks
on the GPU for some fields while data transfers for other fields
are in progress.  No comparison is possible for the $96^3$ problem size.  The OpenACC 
version cannot fit the necessary data in the GPU's framebuffer memory, and modifying
it to employ an alternate mapping strategy that uses the CPU as well would 
involve significant code refactoring.

%\begin{figure}[t]
%\centering
%\includegraphics[scale=0.37]{figs/hept_knl.pdf}
%\vspace*{-2mm}
%\caption{S3D Performance on Keeneland - Heptane\label{fig:hept_knl}}
%\vspace*{-3mm}
%\end{figure}

%\begin{figure}[t]
%\centering
%\includegraphics[scale=0.25]{figs/hept_titan.pdf}
%\vspace*{-2mm}
%\caption{S3D Performance on Titan - Heptane\label{fig:hept_titan}}
%\vspace*{-3mm}
%\end{figure}

%% The experiments on Titan were performed during a period of heavy load, and large
%% the scalability of all three S3D implementations on Titan were impacted by both their
%% own communication overheads and the overhead of communication being performed by
%% other applications running through the same part of the Gemini interconnect.
%% Although better scalability numbers might be achieved if the system load were lower,
%% all implementations were run under the same conditions, so comparisons are still possible.

%\begin{figure}[t!]
%\centering
%\begin{tabular*}{\columnwidth}{@{\extracolsep{\stretch{1}}}c|ccc}
%        & $48^3$ & $64^3$ & $96^3$ \\ 
%\midrule
%Fortran & 63\% & 70\% & 84\% \\
%OpenACC & 78\% & 85\% & N/A \\
%Legion  & 94\% & 88\% & 98\% \\ 
%\end{tabular*}
%\vspace{-2mm}
%\caption{DME Parallel Efficiency at 1024 Nodes.\label{fig:dmeeff}}
%\vspace{-3mm}
%\end{figure}

%\begin{figure}[t!]
%\centering
%\begin{tabular*}{\columnwidth}{@{\extracolsep{\stretch{1}}}c|ccc}
%        & $48^3$ & $64^3$ & $96^3$ \\ \hline
%Fortran & 66\% & 75\% & 86\% \\
%OpenACC & 76\% & 79\% & N/A \\
%Legion  & 81\% & 79\% & 99\% \\
%\end{tabular*}
%\vspace{-2mm}
%\caption{Heptane Parallel Efficiency at 1024 Nodes.\label{fig:hepteff}}
%\vspace{-3mm}
%\end{figure}

The weak scaling of the Legion implementation is also better 
than both the Fortran and OpenACC versions. Figures~\ref{fig:dmeeff}
and \ref{fig:hepteff} show the parallel efficiency of the DME and
heptane mechanisms respectively at 1024 nodes. As expected, 
in all cases parallel efficiency increases with both larger 
problem sizes and larger mechanisms (recall heptane simulates 52 
chemical species while DME simulates only 30 species). Both larger 
problem sizes and larger mechanisms provide additional work which can
be used to better hide communication latencies. Not surprisingly,
Legion confers the largest performance gains at scale relative to Fortran
and OpenACC on the smallest problem size ($48^3$ DME) because 
structure slicing enables Legion to discover additional work and
better hide communication latencies with computation.

%% \begin{figure}
%% \footnotesize
%% \centering\begin{tabular}{c|ll|ll}
%% Problem & \multicolumn{2}{c|}{Keeneland} & \multicolumn{2}{c}{Titan} \\
%% Size & DME & heptane & DME & heptane \\
%% \hline
%% $48^3$ & foo & foo & foo & foo \\
%% $64^3$ & foo & foo & foo & foo \\
%% $96^3$ & foo & foo & foo & foo 
%% \end{tabular}
%% \caption{S3D: Mapping strategies\label{fig:s3d_mapping}}
%% \end{figure}

%% Figure~\ref{fig:s3d_mapping} shows the optimal mapping strategy for 
%% each system, mechanism, and problem size.  We see that {\bf 5?} of 
%% the 6 possible mapping algorithms are used.  Embedding all of these
%% mapping decisions directly in the application code would make it
%% unmaintainable.  However, Legion decouples mapping decisions from
%% the functional specification, allowing programmers to explore
%% mapping strategies such as data layout with minimal effort, leading
%% to both higher performance and more maintainable code.

