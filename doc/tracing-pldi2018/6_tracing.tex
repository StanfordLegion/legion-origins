% Dynamic Tracing (2 pages)
\section{Dynamic Tracing}
\label{sec:tracing}

Programs often contain traces that are identical except for task identifiers.
%
For example, in a program $t_1; t_2; t_3; t_4$ where
\begin{equation*}
  \begin{array}{ll}
    t_1 = (1, \mset{(r^1, \ms{wr})}),
    &
    t_2 = (2, \mset{(r^1, \ms{rd})}),
    \\
    t_3 = (3, \mset{(r^1, \ms{wr})}),\text{ and }
    &
    t_4 = (4, \mset{(r^1, \ms{rd})}),
  \end{array}
\end{equation*}
%
two traces $t_1; t_2$ and $t_3; t_4$ differ only in their identifiers.
%
We call such traces {\it isomorphic traces}.
%
\begin{df}[Isomorphic Traces]
  We write $\mi{tr}_1 \sim \mi{tr}_2$ to mean that traces $\mi{tr}_1$ and $\mi{tr}_2$ are isomorphic; i.e., there exists a bijection $b : \mi{Id} \to \mi{Id}$ whose lifting $b_{\uparrow}$ to a function on traces satisfies $b_{\uparrow}(\mi{tr}_1) = \mi{tr}_2$.
  %
  We write $\mi{tr}_1 \stackrel{b}{\sim} \mi{tr}_2$ when $b$ is the minimal bijection that makes $\mi{tr}_1$ and $\mi{tr}_2$ isomorphic.
\end{df}

Isomorphic traces are indistinguishable to the dependence analysis:
%
the dependence analysis of isomorphic traces are isomorphic.
%
\begin{df}[Isomorphic Task Graphs]
  We write $\mangle{O_1, D_1} \sim \mangle{O_2, D_2}$ to mean that task graphs $\mangle{O_1, D_1}$ and $\mangle{O_2, D_2}$ are isomorphic; i.e., there exists a bijection $b : \mi{Id} \to \mi{Id}$ whose lifting $b_{\uparrow}$ to a function on operations satisfies $\mangle{O_1', D_1'} = \mangle{O_2, D_2}$, where $O_1' = \mset{b_{\uparrow}(\mi{op}) \mid \mi{op} \in O_1}\text{ and}$
  \begin{center}
    $
    \begin{array}{c}
    D_1' = \mset{\mangle{b_{\uparrow}(\mi{op}), b_{\uparrow}(\mi{op}')} \mid \mangle{\mi{op}, \mi{op}'} \in D_1}.
    \end{array}
    $
  \end{center}
  %
  We write $G_1 \stackrel{b}{\sim} G_2$ when $b$ is the minimal bijection that makes $G_1$ and $G_2$ isomorphic.
\end{df}
%
\begin{lem}
  Let $\mi{tr}_1$ and $\mi{tr}_2$ be isomorphic traces.
  %
  Then, the dependence analyses of $\mi{tr}_1$ and $\mi{tr}_2$ are also isomorphic, i.e.,
  % 
  \begin{equation*}
    \begin{array}{l}
      (E, P, V, G, \mi{tr}_1) \tdep^{*} (E', P', V', G', \epsilon)
      \implies \exists G''. G' \sim G''
      \\
      \quad\land\ 
      (E, P, V, G, \mi{tr}_2) \tdep^{*} (E'', P'', V'', G'', \epsilon).
    \end{array}
  \end{equation*}
  \label{lem:iso-trace}
\end{lem}

We want to run dependence analysis only once for a set of isomorphic traces, but Lemma~\ref{lem:iso-trace} states that we only get isomorphic task graphs when isomorphic traces are analyzed in the same state, which can never be satisfied for different traces of the same program.
%
However, if we relax our goal to just attaining any valid task graph, we can repurpose this property by isolating the analysis of a trace.
%
The idea is to record a command that captures the dependence analysis of that trace, and replay it for isomorphic traces that appear later in the program.
%
We call this approach {\it dynamic tracing}.

First, we can record a command for a trace and execute it to reconstruct the trace's dependence graph.
%
\begin{lem}
  The recording of a trace yields a subgraph that the dependence analysis would have generated for that trace.
  \begin{equation*}
    \begin{array}{l}
      (E, P, V, G, \mi{tr}) \augsep (\bullet, \epsilon) \trec^{*}
      (E', P', V', G', \epsilon) \augsep (\mc{E}, c)
      \\
      \qquad \implies \sem{c} = G' - G
    \end{array}
  \end{equation*}
  \label{lem:rec-subgraph}
\end{lem}

Second, the dependence analysis yields isomorphic subgraphs for isomorphic traces if the same valid instance list is used in both analyses.
%
\begin{lem}
  Let $\mi{tr}_1$ and $\mi{tr}_2$ be isomorphic traces.
  %
  Then, their dependence analyses generate isomorphic subgraphs, i.e.,
  \begin{equation*}
    \begin{array}{l}
      (E_1, P_1, V, G_1, \mi{tr}_1) \tdep^{*} (E_1', P_1', V_1', G_1', \epsilon)
      \implies
      \\
      \qquad \exists G_2'.(E_2, P_2, V, G_2, \mi{tr}_2) \tdep^{*} (E_2', P_2', V_2', G_2', \epsilon)
      \\
      \qquad \land\ (G_1' - G_1) \sim (G'_2 - G_2)
    \end{array}
  \end{equation*}
  \label{lem:iso-subgraph}
\end{lem}

\input{fig_prepost}
\input{fig_tracing}

Third, a trace recording can be replayed if the valid instance list satisfies its {\em precondition}.
%
The precondition of a command is a set of instances that the command expects to be valid.
%
Figure~\ref{fig:pre-post} gives the rules to find the pre- and postcondition of a command, which mirror how the valid instances are managed in the dependence analysis.
%
\begin{df}
  We write $\ms{pre}(c)$ and $\ms{post}(c)$ to denote the pre- and the postcondition of command $c$, respectively, which satisfy $\bullet, \bullet|-_{\ms{cnd}} c \rhd \ms{pre}(c), \ms{post}(c)$.
\end{df}
%
\begin{df}
  Valid instance list $V$ subsumes another valid instance list $V'$ iff $\forall r. V'(r) \subseteq V(r)$.
  %
  We write $V |- V'$ when $V$ subsumes $V'$.
\end{df}
%
\begin{lem}
  Let $c$ be a recording of trace $\mi{tr}$.
  %
  Then, for any valid instance list that subsumes $\ms{pre}(c)$, there exists a task graph $G'$ that satisfies the following condition:
  \begin{equation*}
    \begin{array}{l}
      (E, P, V, G, \mi{tr}) \tdep^{*}
      (E', P', V', G', \epsilon) \land \sem{c} = G' - G 
    \end{array}
  \end{equation*}
\end{lem}

Combining all these properties, we can now show that a trace recording can simulate the dependence analysis of isomorphic traces.
%
\begin{lem}
  Let $\mi{tr}_1$ and $\mi{tr}_2$ be traces, where $\mi{tr}_1 \stackrel{b}{\sim} \mi{tr}_2$.
  %
  Suppose we have a recording $c$ of $\mi{tr}_1$ and a valid instance list $V$, where $V |- \ms{pre}(c)$.
  %
  Then, we can use $c$ to simulate the dependence analysis of $\mi{tr}_2$:
  %
  \begin{center}
    $
    \begin{array}{l}
      \exists G'.(E, P, V, G, \mi{tr}_2) \tdep^{*} (E', P', V', G', \epsilon) \land\ 
      \\
      \qquad
      \sem{G + \sem{b_{\uparrow}(c)}} \subseteq \sem{G'}
    \end{array}
    $
  \end{center}
  % 
  where $b_{\uparrow}(c)$ denotes the command $c$ after substitution using the bijection $b$ on identifiers.
  \label{lem:tracing}
\end{lem}
\noindent
%
An important property that makes Lemma~\ref{lem:tracing} true is that the task graph before the transition $\tdep$ is a {\it prefix} of the task graph after the transition;
%
\begin{df}
  We write $\mangle{O_1, D_1} \sqsubseteq \mangle{O_2, D_2}$ when task graph $\mangle{O_1, D_1}$ is a prefix of task graph $\mangle{O_2, D_2}$, i.e.,
  % 
  \begin{equation*}
    \begin{array}{l}
      O_1 \subseteq O_2 \land D_1 \subseteq D_2\ \land\
      \\
      \qquad
      \forall \mi{op}_1 \in O_1. \not \exists \mi{op}_2 \in O_2 - O_1. \mangle{\mi{op}_2, \mi{op}_1} \in D_2 \land
      \\
      \qquad
      \forall \mi{op}_1, \mi{op}_2 \in O_1. \mangle{\mi{op}_1, \mi{op}_2} \in D_2 \implies
      \mangle{\mi{op}_1, \mi{op}_2} \in D_1
    \end{array}
  \end{equation*}
\end{df}
%
\begin{lem}
  \begin{equation*}
    (E, P, V, G, p) \tdep (E', P', V', G', p') \implies G \sqsubseteq G'
  \end{equation*}
\end{lem}
\noindent
Then, any prefix of a task graph satisfies the following property:
%
\begin{lem}
  \begin{equation*}
    G_1 \sqsubseteq G_2 \implies \sem{G_1 + (G_2 - G_1)} \subseteq \sem{G_2}
  \end{equation*}
\end{lem}

As stated in Lemma~\ref{lem:tracing}, one consequence of replaying traces is that the task graph from the replay describes a subset of schedules that the task graph from the original dependence analysis expresses, because the concatenation of two graphs introduces all-to-all connections on the boundary between the graphs, which can create artificial dependences.
%
We discuss below how we can overcome this issue for certain common traces.

Figure~\ref{fig:tracing} shows the complete system for dynamic tracing.
%
At any point, the system is doing one of four operations:
%
\begin{itemize}

\item[{\sf (DEP)}] Analyzing dependences of a task.

\item[{\sf (REC)}] Recording a command for a trace.

\item[{\sf (RPL)}] Replaying a trace.

\item[{\sf (CAP)}] Collecting a task to construct a trace.

\end{itemize}
%
Each mode of operation is denoted by the first component of the state.
%
\begin{center}
  $
  \begin{array}{ll}
    \text{Modes} & \mi{mode} \in \mset{\ms{DEP}, \ms{REC}, \ms{RPL}, \ms{CAP}}
  \end{array}
  $
\end{center}
%
The three next components $S_{\ms{dep}}$, $S_{\ms{rec}}$, and $S_{\ms{exp}}$ of the state are used for dependence analysis, recording, and replaying, respectively.
%
The component $\mc{T}$ is a trace list, which is a list of pairs of traces and commands.
%
\begin{center}
  $
  \begin{array}{ll}
    \text{Trace Lists} & \mc{T} \in \wp(\mi{Trace} \times \mi{Command})
  \end{array}
  $
\end{center}
%
Finally, the last component $\mi{tr}$ is the trace that is being collected.

Identifying the traces in a program that are beneficial to record and replay is beyond the scope of this work.
%
We assume that traces in a program are already identified by the programmer and delimited by the markers $\ms{begin}$ and $\ms{end}$.
%
We also require the program to be {\it well-formed}; i.e., the program has no nested traces and no unbounded traces.

Since the rules that transition to the same mode are straightforward, we explain the rules that change the mode of operation.
%
The rule (TRec2Dep) ends the recording for a trace and adds the recorded command to the trace list.
%
Then, the rules (TCap2Rec) and (TCap2Rpl) try to retrieve a command from this trace list using the function $\ms{replayable}$:
%
\begin{equation*}
  \begin{array}{l}
    \ms{replayable}(\mc{T}, V, \mi{tr}) \triangleq
    \\
    \quad \left\{
    \begin{array}{lr}
      \mangle{V\cup\ms{post}(c), b_{\uparrow}(c)} & \text{ when } \mangle{\mi{tr}', c} \in \mc{T} \land \mi{tr}' \stackrel{b}{\sim} \mi{tr}
      \\
                                                & \land\ V |- \ms{pre}(c)
      \\
      \varnothing & \text{otherwise}
    \end{array}
                    \right.
  \end{array}
\end{equation*}
%
The function chooses a trace that is isomorphic to the given trace and whose precondition is subsumed by the given valid instance list.
%
It also applies the postcondition of the chosen trace to the current valid instance list to make the state consistent for the rest of analysis.
%
When $\ms{replayable}$ finds a replayable trace, the rule (TCap2Rpl) transitions to a replay state, otherwise the rule (TCap2Rec) starts the recording of the current trace.

\begin{figure}[b]
  \centering
  \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
    \tikzstyle{every state}=[rounded corners,rectangle,fill=none,draw=black,text=black]
    \node[state] at (0, 0)    (dep) {$\ms{DEP}$};
    \node[state] at (0, -1.3) (cap) {$\ms{CAP}$};
    \node[state] at (3, 0.3)  (rec) {$\ms{REC}$};
    \node[state] at (-3, 0.3) (rpl) {$\ms{RPL}$};

    \path (dep) edge [loop above]       node {(TDep)}     (dep)
    edge [left]             node {(TDep2Cap)} (cap);
    \path (cap) edge [bend right,right] node {(TCap2Rec)} (rec)
    edge [bend left]        node {(TCap2Rpl)} (rpl)
    edge [loop below]       node {(TCap)}     (cap);
    \path (rec) edge [loop above]       node {(TRec)}     (rec)
    edge [bend right,above] node {(TRec2Dep)} (dep);
    \path (rpl) edge [loop above]       node {(TRpl)}     (rpl)
    edge [bend left,above]  node {(TRpl2Dep)} (dep);
  \end{tikzpicture}
  \caption{State diagram}
  \label{fig:state-diagram}
\end{figure}

One issue with the replay mechanism is that it does not maintain the epoch list and privilege, as that would add back in some of the overhead tracing is trying to remove.
%
To resolve this issue, the rule (TRpl2Dep) ``flushes the pipeline'' by inserting a dummy operation, which we call a {\it fence}, to all current epochs.
%
Although the fence is treated as an operation that updates all instances, it has no effect on the valid instance list, and thus does not taint the data flow between instances.
%
In rule (TRpl2Dep), the fence $f$ is signified by a task with no instances, which in normal dependence analysis would have been simply ignored as it could not introduce dependences.
%
The function $\ms{fence}$ inserts the fence to the current epochs as follows:
%
\begin{center}
$
  \begin{array}{l}
    \ms{fence}(E, f)(r^i) \triangleq \mangle{\mset{f}, \varnothing}.
  \end{array}
$
\end{center}
%
The rule inserts a fence in the task graph, which manifests as a join point for all tasks in the trace and a precondition for all that come after it.

Figure~\ref{fig:state-diagram} summarizes state transitions in Figure~\ref{fig:tracing} in a state diagram;
%
the label on each edge corresponds to the rule that makes the transition.

The following theorem proves that dynamic tracing finds a valid task graph for a program.
%
\begin{thm}
  Let $S_{\ms{dep}}^{\iota}$, $S_{\ms{rec}}^{\iota}$, and $S_{\ms{exp}}^{\iota}$ denote the components of the initial state for program $p$, where
  \begin{equation*}
    \begin{array}{l}
      S_{\ms{dep}}^{\iota} = (\bullet, \bullet, \bullet, \emptyg, p),
      S_{\ms{rec}}^{\iota} = (\bullet, \epsilon), \text{ and }
      S_{\ms{exp}}^{\iota} = (\bullet, \emptyg, \epsilon).
    \end{array}
  \end{equation*}
  % 
  Then, task graph $G$ is valid for $p$ if it satisfies the following condition:
  %
  \begin{equation*}
    \begin{array}{l}
      \mbbr{\ms{DEP}, S_{\ms{dep}}^{\iota}, S_{\ms{rec}}^{\iota}, S_{\ms{exp}}^{\iota}, \bullet, \epsilon} -->^{*}
      \\
      \qquad\qquad\qquad \mbbr{\ms{DEP}, (E, V, P, G, \epsilon), S_{\ms{rec}}, S_{\ms{exp}}, \mc{T}, \epsilon}.
    \end{array}
  \end{equation*}
  %
\end{thm}

\paragraph{Idempotent Traces}

A trace is idempotent when its analysis does not change the valid instance list.
%
\begin{df}
  Trace $\mi{tr}$ is idempotent to valid instance list $V$ if it satisfies the following condition:
  %
  \begin{center}
    $
    \begin{array}{c}
      (E, P, V, G, \mi{tr}) \tdep^{*} (E', P', V, G', \epsilon)
    \end{array}
    $
  \end{center}
\end{df}
\noindent
Idempotent traces have two benefits in dynamic tracing.
%
First, they require no updates to the valid instance list after being replayed.
%
Second, when a program repeats the same idempotent trace consecutively, they all become replayable right after the first one is replayed.
%
Furthermore, we can eliminate the fence between every two replays.
%
For brevity, we do not discuss the extension of the system in Figure~\ref{fig:tracing} to exploit idempotence of traces.
%
Our implementation still incorporates this feature as a performance optimization.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
