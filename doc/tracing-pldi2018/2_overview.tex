\section{Overview}
\label{sec:overview}

We briefly describe dynamic tracing using an example.

The program in Figure~\ref{fig:ex-program} issues four tasks \texttt{F(A[0])},  \texttt{F(A[1])}, \texttt{G(A[h(0)])}, \texttt{G(A[h(1)])} for every iteration of the while loop.
%
Static dependence analyses will give imprecise results because of the indices computed using the opaque function {\tt h}.
%
In contrast, precise dynamic dependence analysis is straightforward.
%
Assuming \texttt{h(0)} = 1 and \texttt{h(1)} = 0, dynamic dependence analysis shows there are dependences between \texttt{F(A[0])} and \texttt{G(A[h(1)])}, and between \texttt{F(A[1])} and \texttt{G(A[h(0)])}.

In a distributed system, data dependences may require data movement.
%
For example, if \texttt{F(A[0])} and \texttt{G(A[0])} execute on different nodes of the machine, and if \texttt{F(A[0])} writes to \texttt{A[0]}, the updated value of \texttt{A[0]} must be copied to the node where \texttt{G(A[0])} will run.
%
We use node identifiers $\alpha$, $\beta$, etc.\ as superscripts to data elements to distinguish different {\em instances} of the same data on different nodes.
%
In Figure \ref{fig:trace1}, for example, the upper left operation copies an instance of \texttt{A[1]} on node $\alpha$ to an instance of A[1] on node $\beta$.
%
Tasks execute on the node where their argument is placed.

Figure ~\ref{fig:stream} shows some {\em traces}, which are sequences of tasks issued by the program.
%
The dependence analysis of trace $1$, which corresponds to the while loop's second iteration, generates the task graph in Figure~\ref{fig:trace1}.
%
To ensure correctness, copy operations are included in the task graph where necessary.
%
While generating this task graph, dynamic tracing will memoize the generated graph using graph calculus commands (discussed in Section~\ref{sec:explicit}) so the graph can be regenerated for later traces.

Dynamic tracing detects when recorded commands can be reused using two criteria.
%
First, the subsequent trace must be exactly the same as the one from the second iteration;
% 
this requires that the tasks have the same data dependences and the choice of data placement is the same so that the set of required copies are the same.
%
Second, the set of instances that hold the most recent version of input data to the trace must be the same; in this example $\mtt{A[1]}^\nodea$ and $\mtt{A[0]}^\nodeb$ are the input data during trace $1$ when the graph is captured.
%
For this example, dynamic tracing can replace the dynamic dependence analysis for each trace from iteration $2$ to $k-1$ using the graph calculus commands captured during trace $1$.

Suppose now that during trace $k$ the choice of nodes for the data of tasks \texttt{G(A[h(0)])} and \texttt{G(A[h(1)])} are swapped as shown in Figure~\ref{fig:stream}.
%
Trace $k$ then looks different from the first $k-1$ traces because the location of the instances for tasks \texttt{G(A[h(0)])} and \texttt{G(A[h(1)])} have changed, leading dynamic tracing to reject replaying the capture of trace $1$ and instead capture a new trace. 
%
However, something subtle occurs when dynamic tracing encounters trace $k+1$.
%
Again, dynamic tracing will not be able to replay the commands from trace $k$ for trace $k+1$ because the location of the input instances are different; trace $k$ has input instances $\mtt{A[1]}^\nodea$ and $\mtt{A[0]}^\nodeb$ while trace $k+1$ has input instances $\mtt{A[1]}^\nodeb$ and $\mtt{A[0]}^\nodea$ necessitating input copy operations.
%
Therefore dynamic tracing will also need to capture commands for trace $k+1$ and will be able to replay them starting with trace $k+2$ assuming the choice of instance placement remains stable.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
