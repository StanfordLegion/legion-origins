% Evaluation (1-1.5 page)
\section{Evaluation}
\label{sec:eval}

\begin{table}[t!]
  \centering
  \begin{tabular}{c|p{18em}}
    Name & Description
    \\
    \hline
    Stencil & 9-point stencil benchmark on a 2D structured grid~\cite{PRK14}
    \\
    Circuit & Circuit simulation for an unstructured circuit graph~\cite{Regent17}
    \\
    Soleil-X & Turbulent fluid solver on a 3D structured grid~\cite{PSAAP2,SoleilX}
    \\
    \Pennant & Lagrangian hydrodynamics proxy application on a 2D unstructured mesh~\cite{PENNANT}
    \\
    MiniAero & Compressible fluid solver on a 3D unstructured mesh~\cite{Mantevo}
  \end{tabular}
  \caption{Benchmark Regent applications}
  \label{tab:benchmark}
\end{table}

In this section, we evaluate dynamic tracing using the five applications in Table~\ref{tab:benchmark}.
%
The applications are written in Regent~\cite{Regent15}, a high-level programming language that targets Legion.
%
They have already been heavily optimized and have competitive or better performance than reference implementations~\cite{Regent17}.
%
We use GCC 5.3 to compile Legion and LLVM 3.9 to generate application binaries.
%
We report performance for each application on up to 256 nodes of the Quartz cluster~\cite{Quartz}.
%
Nodes are connected by an Omni-Path interconnect and each node has 128 GB of memory and two Intel Xeon E5-2695 CPUs with 18 physical cores each.

We demonstrate that dynamic tracing improves strong scaling performance for all five applications.
%
In strong scaling, the total problem size is held fixed while the size of the parallel machine is increased.
%
Every doubling of the number of nodes halves the granularity of tasks and eventually hits a point where the runtime overhead per task exceeds the task granularity.
%
We show that dynamic tracing achieves better strong scaling by reducing the runtime overhead to support finer-grained tasks.

% Overall evaluation of advantages
Figure~\ref{fig:performance} summarizes our results.
%
For all applications, dynamic tracing scales to approximately 4X more nodes than Legion without tracing, because dynamic tracing can sustain up to 4.9x smaller tasks than dynamic dependence analysis, as shown in Figure~\ref{fig:granularity}.

There are a couple of interesting effects that can be observed in the results.
%
First, \Pennant{}'s strong scaling is the most improved, even though MiniAero has a longer trace;
%
since dynamic tracing amortizes the replaying overhead across tasks in a trace, a longer trace often leads to a smaller average overhead per task.
% 
(In Figure~\ref{fig:granularity}, the trace size is the number of tasks each node analyzes.)
%
The reason \Pennant{} improves more than  MiniAero is that the execution of \Pennant{}'s trace is guarded by a predicate condition that prevents analysis and task graph generation of the next trace while a previous trace is running.
%
This exposes runtime overhead and puts it on the critical path of the application.
%
Thus, any reduction in the overhead is more effective in \Pennant{} than in the other applications where JIT graph generation from the trace is already mostly overlapped with useful application work.

Another interesting result is that the average granularity of tasks for Soleil-X is the smallest.
%
Tasks in Soleil-X are parallelized using OpenMP, which makes the overhead to handle them constant in the number of cores on a node.
%
On the other hand, tasks in the other applications are individually assigned to cores and therefore incur overhead linear in the number of cores.% on a node.

To quantify how close the performance of dynamic tracing comes to the best possible, we compare the dynamic tracing to a direct Realm implementation of Stencil.
%
Note that dynamic tracing cannot yield better performance than this Realm implementation, because tracing also depends on Realm to execute commands.
%
Dynamic tracing matches the performance of the Realm implementation up to 64 nodes, but starts diverging at 128 nodes.
%
The difference between dynamic tracing and Realm at 256 nodes is 38\%.
%
This discrepancy is because our implementation of dynamic tracing cannot remove the base overhead of launching and running tasks in the Legion task pipeline.
%
Though not shown in the graph, performance of the Realm implementation saturates at 256 nodes; i.e., tracing is no more than 1.4X from a lower bound for this application.

\zap{;
%
for example, each task launch stage in Legion incurs about 50us of overhead in addition to the overhead using the Realm task launch.}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
