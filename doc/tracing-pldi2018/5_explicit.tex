% Graph Calculus (2 pages)
\section{Graph Calculus}
\label{sec:explicit}

The dependence analysis in Section~\ref{sec:implicit} ``interprets'' a program to generate a task graph.
%
For each task in a program, this interpreter analyses its dependences on previous tasks and updates the graph.
%
If instead our goal is to build a specific graph, we can specialize the interpreter's analysis to a process that builds just that one graph.
%
To describe this specialization we introduce {\em graph calculus}, a simple, imperative language for task graph construction.

\subsection{Syntax}

\begin{figure}[t!]
  \centering
  $
  \begin{array}{p{4.2em}rcl}
    \text{Commands} & c & \in & \mi{Command}
    \\
                    & c &\!\!\!\!::=\!\!\!\!&\mtt{x := op(\mi{op}, x)}
                                              \mid \mtt{x := merge(\overline{x})}
                                              \mid c; c \mid \epsilon
  \end{array}
  $
  \caption{Syntax of graph calculus}
  \label{fig:syntax}
\end{figure}

Figure~\ref{fig:syntax} shows the syntax of graph calculus.
%
The calculus uses variables to name and connect operations.
%
The $\mtt{op}$ statement takes an operation and a name of the incoming operation in the graph, and assigns the operation to a new name.
%
To express multiple incoming operations to a task or a copy, the $\mtt{merge}$ statement assigns a new name to a set of operations.
%
Finally, the calculus has statement sequencing and an empty statement.

The special name $\none$ is used when there is no incoming operation, which is needed to express a {\it closed} command with no free variables.
%
The function $\ms{FV}$ in the following finds the set of free variables in a command.
%
\begin{equation*}
  \begin{array}{l}
    \ms{FV}(\mtt{x := op(\mi{op}, y)}) \triangleq \mset{\mtt{y}}
    \quad
    \ms{FV}(\mtt{x := merge(\overline{y})}) \triangleq \mset{\mtt{\overline{y}}}
    \\
    \ms{FV}(c_1; c_2) \triangleq \ms{FV}(c_1) \cup (\ms{FV}(c_2) - \ms{BV}(c_1))
    \quad
    \ms{FV}(\epsilon) \triangleq \varnothing
    \\
    \ms{BV}(\mtt{x := op(\mi{op}, y)}) \triangleq \mset{\mtt{x}}
    \quad
    \ms{BV}(\mtt{x := merge(\overline{y})}) \triangleq \mset{\mtt{x}}
    \\
    \ms{BV}(c_1; c_2) \triangleq \ms{BV}(c_1) \cup \ms{BV}(c_2)
    \quad
    \ms{BV}(\epsilon) \triangleq \varnothing
  \end{array}
\end{equation*}
% 
Command $c$ is closed if $\ms{FV}(c) = \mset{\none}$.

\subsection{Operational Semantics}

\begin{figure}[b]
  \centering
  $
  \begin{array}{c}
    \zap{
    \multicolumn{1}{l}{\framebox{$(\mc{X}, G, c) \texp (\mc{X}, G, c)$}}
    \\
    \\
    }
    \inference[(EI)]
    {
    \mc{X}' = \mc{X}[\mtt{x} \mapsto \mset{\mi{op}}]
    \quad
    G' = \ms{insert}(G, \mc{X}(\mtt{y}), \mi{op})
    }
    {
    (\mc{X}, G, \mtt{x := op(\mi{op}, y)})
    \texp
    (\mc{X}', G', \epsilon)
    }
    \\
    \\
    \inference[(EM)]
    {
    \mc{X}' = \mc{X}[\mtt{x} \mapsto \cup_i \mc{X}(y_i)]
    }
    {(\mc{X}, G, \mtt{x := merge(\overline{y_i})}) \texp (\mc{X}', G, \epsilon)}
    \\
    \\
    \inference[(ES1)]
    {(\mc{X}, G, c_1) \texp (\mc{X}', G', c_1')}
    {(\mc{X}, G, c_1;c_2) \texp (\mc{X}', G', c_1';c_2)}
    \\
    \\
    \inference[(ES2)]
    {}
    {(\mc{X}, G, \epsilon;c_2) \texp (\mc{X}, G, c_2)}
  \end{array}
  $
  \caption{Operational semantics of graph calculus}
  \label{fig:semantics}
\end{figure}

Figure~\ref{fig:semantics} shows the operational semantics of graph calculus.
%
The relation $\texp$ describes the execution of a command;
%
The meaning of each rule is straightforward.
%
The rules use an {\it environment} $\mc{X}$, which maps variables to a set of operations.
%
\begin{equation*}
  \begin{array}{ll}
    \text{Environments}
    &
    \mc{X} : \mi{Variable} \to \wp(\mi{Op})
    \\
    & \mc{X} ::= \bullet \mid \mc{X}[\mtt{x} \mapsto O]
  \end{array}
\end{equation*}
%
An empty environment $\bullet$ maps $\none$ to an empty set of operations.
%
The semantics of a command is the task graph reachable from the initial state via $\texp$.
%
\begin{df}
  We write $\sem{c}$ for the semantics of command $c$, which is a task graph $G$ that satisfies the following condition.
  \begin{equation*}
    (\bullet, \emptyg, c) \texp^{*} (\mc{X}, G, \epsilon)
  \end{equation*}
\end{df}
\noindent
Closed commands do not get stuck and always terminate.
%
\begin{lem}
  For any closed command $c$, there exists task graph $G$ such that $(\bullet, \emptyg, c) \texp^{*} (\mc{X}, G, \epsilon)$.
\end{lem}
\noindent
The execution of commands is deterministic and yields an acyclic graph.
%
\begin{lem}
  \begin{equation*}
    \begin{array}{l}
      (\bullet, \emptyg, c) \texp^{*} (\mc{X}, G_1, \epsilon) \land
      (\bullet, \emptyg, c) \texp^{*} (\mc{X}, G_2, \epsilon)
      \\
      \qquad \implies G_1 = G_2
    \end{array}
  \end{equation*}
\end{lem}
%
\begin{lem}
  Let $\mangle{O, D}$ be the semantics of command $c$. Then, the graph is acyclic, i.e.,
  $\not \exists \mi{op} \in O. \mangle{\mi{op}, \mi{op}} \in D^{+}$.
\end{lem}

\subsection{Generating Graph Calculus From Dependence Analysis}
\label{subsec:rec}

We now consider how to capture a trace and produce a corresponding graph calculus program.
%
The gray highlighting in Figure~\ref{fig:rec-analysis} augments the dependence analysis with recording.
%
With recording, the rules maintain an {\it operation table} $\mc{E}$ that maps operations to their names.
%
\begin{equation*}
  \begin{array}{ll}
    \text{Operation Tables}
    &
    \mc{E} : \mi{Op} -> \mi{Variable}
    \\
    & \mc{E} ::= \bullet \mid \mc{E}[\mi{op} \mapsto \mtt{x}]
  \end{array}
\end{equation*}
%
The $\ms{lookup}$ function retrieves the names of a set of operations.
\begin{center}
$
  \begin{array}{l}
    \ms{lookup}(\mc{E}, O) \triangleq
    \\
    \qquad
    \left\{
    \begin{array}{l}
      \none \hfill \qquad \text{when } \mi{dom}(\mc{E}) \cap O = \varnothing
      \\
      \mset{\mc{E}(\mi{op}) \mid \mi{op} \in \mi{dom}(\mc{E}) \cap O} \hfill \qquad\qquad \text{otherwise}
    \end{array}
    \right.
  \end{array}
$
\end{center}

The most important property of the recording is that the final command generates the same graph as the dependence analysis.
%
\begin{thm}
  Let $c$ be a recording of program $p$, where
  \begin{equation*}
    (\bullet, \bullet, \bullet, \emptyg, p) \augsep (\bullet, \epsilon)
    \trec^{*}
    (E, P, V, G, \epsilon) \augsep (\mc{E}, c)
  \end{equation*}
  for some $E$, $P$, $V$, $G$, and $\mc{E}$.
  %
  Then, the semantics of $c$ is the same as the task graph $G$, i.e., $\sem{c} = G$.
\end{thm}

\begin{ex}
  The analysis in Figure~\ref{fig:rec-analysis} records the following command for the program in Example~\ref{ex:dep}.
  %
  \begin{center}
    \begin{tabular}{cc}
      \begin{minipage}{0.4\linewidth}
        $
        \begin{array}{l}
          \mtt{x_1 := merge(\none)};
          \\
          \mtt{x_2 := merge(\none)};
          \\
          \mtt{x_3 := merge(x_2, \none)};
          \\
          \mtt{x_4 := merge(x_1, x_3)};
          \\
          \mtt{x_5 := op(\mi{t_1}, x_4)};
          \\
          \mtt{y_1 := merge(x_5)};
          \\
          \mtt{y_2 := merge(y_1, \none)};
        \end{array}
        $
      \end{minipage}
      &
      \begin{minipage}{0.4\linewidth}
        $
        \begin{array}{l}
          \mtt{y_3 := op(\mi{t_2}, y_2)};
          \\
          \mtt{z_1 := merge(x_5)};
          \\
          \mtt{z_2 := merge(z_1, \none)};
          \\
          \mtt{z_3 := merge(y_3)};
          \\
          \mtt{z_4 := merge(z_3, \none)};
          \\
          \mtt{z_5 := merge(z_2, z_4)};
          \\
          \mtt{z_6 := op(\mi{t_3}, z_5)};\epsilon
        \end{array}
        $
      \end{minipage}
    \end{tabular}
  \end{center}
  \label{ex:rec}
\end{ex}

\subsection{Optimizing Graph Calculus Commands}

A command is suboptimal when there exists a shorter command that produces the same graph.
%
In particular, multiple merge statements can often be combined into one merge statement.
%
Since the names of operations are constants, we can use constant propagation to coalesce or even eliminate multiple merge statements.
%
Commands may also introduce transitive dependence edges.
%
We can safely remove the transitively redundant edges and still obtain a graph that expresses the same set of schedules as the one generated by the original command.
%
These optimizations can be done after or even during the dependence analysis.
%
For example, optimizing the graph of Example~\ref{ex:rec} yields the graph of Example~\ref{ex:dep} without the redundant transitive edge.
%
\begin{ex}
  The command in Example~\ref{ex:rec} can be optimized into the following command.
  \begin{equation*}
    \mtt{x_5 := op(\mi{t_1}, \none)};\mtt{y_3 := op(\mi{t_2}, x_5)};\mtt{z_6 := op(\mi{t_3}, y_3)};
  \end{equation*}
  % 
  This program creates the following graph.
  \begin{center}
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
      \node at (0, 0) (t1) {$t_1$};
      \node at (1, 0) (t2) {$t_2$};
      \node at (2, 0) (t3) {$t_3$};

      \draw (t1) -- (t2);
      \draw (t2) -- (t3);
    \end{tikzpicture}
  \end{center}
  %
  Note that unlike Example~\ref{ex:dep}, this graph does not contain transitive edges.
  \label{ex:opt}
\end{ex}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
