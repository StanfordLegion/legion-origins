# Reviewer 1 - Weak Accept (Knowledgable)

* "I would have liked a more detailed description of the implementation of legion. ...
   I was curious to see how the idempotent traces were handled efficiently at run-time."

* "I was missing the actual proofs from the paper."

* "can the optimisations in §5.4 be stated formally?"

* "What would be needed to integrate dynamic tracing without programmer cooperation?"

# Reviewer 2 - Weak Reject (Some familiarity)

* "unfortunately the problem addressed by this paper does not seem very complicated."

* "Another problem with the paper is that parts of it are quite terse, and could be improved with more explanatory text and less rules."

# Reviewer 3 - Weak Accept (Some familiarity)

* "There is a lack of formal guarantees between the formalization and its implementation."

* "The proof efforts and extensibility have not been well discussed."

* "This paper also mentions that such kind of optimizations could be performed during different phases of the analysis. Have you considered any of the alternatives? Do they incur additional complexity regarding the soundness proof?"

* "does performance improvement reported in Section 8 depends on the way a program is annotated?"

# Reviewer 4 - Weak Accept (Expert)

* "no asymptotic analysis of the improvements (unlike much of the prior related work on scheduling)"

* "I think the current framing of the contribution over-reaches. ... many parallel programming models generate task graphs directly, ... The model treated in this paper is a more narrow one exemplified by Legion and related systems."

* "for a task-graph based system, the biggest challenge to directly adopting tracing techniques would be identifying isomorphic subgraphs ...  It's very practical to sidestep this problem in some way, by having the programmer do extra work.  But the exact way in which the problem
was sidestepped in this paper is not shown in much detail, and it leaves the reader with the impression that perhaps so much is done manually by the programmer here that what is automated is actually not that challenging.

* "More details on the implementation and more examples would help."

* "One thing the evaluation does not establish, however, is whether the previous (no tracing) Legion results were competitive with reference implementations not just at 1-16 nodes, but at the 256 node scale as well."

* "Why model tasks as already having specific data instances as their targets?"

* "It would help to have some intuition about the runtime handling of task streams.  Is there a single node that has to process the sequential task stream?"
