PLDI 2018 Paper #2 Reviews and Comments
===========================================================================
Paper #167 Dynamic Tracing: Just-In-Time Specialization of Task Graphs for
Dynamic Task-based Parallel Runtimes


Review #167A
===========================================================================

Overall merit
-------------
B. I support accepting this paper but will not champion it.

Reviewer expertise
------------------
Y. Knowledgeable

Paper summary
-------------
The paper formally presents _dynamic tracing_, an optimisation
technique for task graphs for parallel run-times. By tracing
sequences of tasks and recording their dependency analysis result,
it is possible to recall those results if and when idempotent
traces occur later during execution. The paper describes formally
task graphs and their dependency analysis, as a prelude to an
imperative graph calculus language for task graph construction.
This builds the case for dynamic tracing -- which is then defined
in terms of the preceding definitions.

An implementation of dynamic tracing is discussed briefly in the
context of the Legion run-time system. Tracing happens as a result
of explicit being/end markers inserted by the programmer. The key
differences between Legion and the system formalised in the paper
are described briefly. Finally, Legion with dynamic tracing is
compared to Legion without dynamic tracing, showing significant
strong scaling improvements, and in particular a much reduced task
granularity.

Comments for author
-------------------
I found the paper to be quite dense, and I had to frequently flip
back and forth between definitions. There are many moving pieces,
but the paper does an excellent job of building up the system
piecemeal!

I would have liked a more detailed description of the
implementation of legion. For example, as I was reading §6, I was
curious to see how the idempotent traces were handled efficiently
at run-time. I appreciate that there are many details that must be
covered in the formal description, which is already a bit dense,
so I suspect that regardless of what subset of the implementation
one chooses to cover, there will always be readers disagreeing
with that choice. However, it might be more useful to readers to
focus more on the particular design choices when going from a
formalism (which e.g., includes non-deterministic choice) to an
implementation, rather than highlighting additional aspects of the
Legion system.

I was missing the actual proofs from the paper. They would have
been quite helpful for the understanding, or even just a sketch of
a few sentences. I think that for a paper listing proofs as the
2nd contribution, this is to be expected -- or at least provide
additional details in supplemental material.

To remove any opportunities for misunderstanding, can the
optimisations in §5.4 be stated formally?

The word "chooses" on line 1011 needs to be untangled.

Nitpick: "Lemma 8 states that we only get..." Is the use of _only_
warranted here?

There seems to be a typo in the last rule of Fig. 6
$V_{pre}[r\to V_{pre} \cup \{r^i}]$ should be
$V_{pre}[r\to V_{pre}(r) \cup \{r^i}]$ and
$V_{post}[r\to V_{post} \cup \{r^i}]$ should be
$V_{post}[r\to V_{post}(r) \cup \{r^i}]$.

The 2nd paragraph of §7 states that tracing needs explicit
begin/end markers inserted by programmers, and that there are some
naming issues. What would be needed to integrate dynamic tracing
without programmer cooperation? Are there any particular source
markers or similar that could be used as input to tracing?



Review #167B
===========================================================================

Overall merit
-------------
C. I would not accept this paper but will not argue strongly against
   accepting it.

Reviewer expertise
------------------
Z. Some familiarity

Paper summary
-------------
This paper deals with optimizing task-based programming systems, where task graphs are generated to specify computations to be run on parallel, distributed machines. The paper describes a method to efficiently capture a trace of a dynamic dependency analysis that generates a task graph, and to soundly replay it. The method is implemented on an existing system (Legion) and leads to multiple-factor scaling improvements.

Comments for author
-------------------
This paper is quite thorough in formalizing the capture and replay computations of task graphs via dynamic dependency analyses. Following the single-writer-multiple-reader invariant, the progress of the computation is divided into epochs, with dependence edges between epochs. Also included is the notion that there can be multiple instances of a task that operate on the same logical data in different nodes, and thus in addition to tracking read/write and write/write dependencies, operations that "copy" data to maintain coherence also need to be tracked.

Despite these details, unfortunately the problem addressed by this paper does not seem very complicated. Programs in the so-called graph calculus used to describe commands that construct task graphs are basically just lists of simple graph operations, and could be advertised as such.

Another problem with the paper is that parts of it are quite terse, and could be improved with more explanatory text and less rules. Section 6 should be the main focus of the paper, but I found it hard to follow and got lost in a series of new concepts introduced and used in this section. Particularly unclear is the process of replaying and the associated roles of fences and idempotent traces.



Review #167C
===========================================================================

Overall merit
-------------
B. I support accepting this paper but will not champion it.

Reviewer expertise
------------------
Z. Some familiarity

Paper summary
-------------
This paper tackles the problem of runtime generation of task graphs and formally describes a technique called dynamic tracing, which records the dependence analysis while processing previous traces, and reuses it whenever a ``similar" trace is identified. Central to this technique is the utilization of graph calculus, which describes operations on the task graph and could be replayed to construct an equivalent graph deterministically. This paper proves the soundness of the recording of graph calculus commands, and also proves that dynamic tracing will always generate a valid task graph w.r.t. the given program. The authors implement this technique in Legion, and report a 3.8X average improvement on the minimal granularity of tasks.

Comments for author
-------------------
                        ===== Paper strengths =====
* The paper presents a clean formalization of the task-graph-based system and its analysis.
* The graph calculus that enables memoization technique of dynamic tracing is novel.
* Experiments demonstrate the improvement in the strong scaling performance.


                        ===== Paper weaknesses =====
* There is a lack of formal guarantees between the formalization and its implementation.
* The proof efforts and extensibility have not been well discussed.


                      ===== Comments for author =====
Dynamic dependence analysis of task graphs is a challenging problem. This paper compares the idea of dynamic tracing to trace-based JIT compilation and points out that the main difference in this work is that data must be correctly copied between machines to remain valid input, which is not an issue in the single-machine setting. This subtle problem is elegantly resolved by utilizing the SWMR invariant, dividing the task sequence into epochs w.r.t. each instance, and in the task graph drawing an edge between tasks belonging to adjacent previous and current epochs.

The major contribution listed in the paper is the formalization of the task-graph-based system. But the proofs presented in the submission are not detailed enough and no supplementary materials are provided. Most of the lemmas used in the paper are simply stated without explaining how the proof is done. I suggest authors including more formal details in the appendix and may even construct mechanized proofs in a proof assistant like Coq. This will dramatically increase the confidence of this work.

The frontend program as in figure 1(a) is not clearly explained. As defined in section 3, a program consists of a stream of tasks, which operate on instances, indicating that instance assignment is given before the dependence analysis. However, the assignment of instances is not shown, and it is unclear how to translate this program into task sequences as in 1(b).

It is quite interesting that the graph calculus could be used for optimization on its own (shown in Section 5.4). This paper also mentions that such kind of optimizations could be performed during different phases of the analysis. Have you considered any of the alternatives? Do they incur additional complexity regarding the soundness proof?

Section 7 mentions that traces are delimited manually by developers. As a result, does performance improvement reported in Section 8 depends on the way a program is annotated?



Review #167D
===========================================================================

Overall merit
-------------
B. I support accepting this paper but will not champion it.

Reviewer expertise
------------------
X. Expert

Paper summary
-------------
This paper considers the class of implicitly parallel programs that
expressed as a sequential stream of tasks, which are labeled with the
data items (regions) they reference.  An implementation challenge for
these programs is to dynamically determine which tasks can run in
parallel, i.e. have non-conflicting data access, and generate a
schedule.  This paper proposes a tracing capability which caches the
same set of tasks

Comments for author
-------------------
Strengths:

 + combines a solid combination of formalization and compelling
   empirical results

 + addresses a problem in Legion and similar systems

 + formalizes a reasonable algorithm for dependenc graph generation,
   including elimination of unneeded transitive-dependence edges 

Weaknesses:

 - result is narrow, and somewhat Legion-centric

 - formalization is perfectly reasonable, but not a major source of
   insight in this case

 - no asymptotic analysis of the improvements (unlike much of the
   prior related work on scheduling)

 - task-graph construction is amortized, but the tracing capability
   could probably amortize other overheads as well that would apply
   to a broader range of systems

This paper makes a solid contribution on the topic of Legion
implementation, with applicability to related systems.  The paper is
well written and was a pleasure to read.  I especially liked the
recognition and treatment of idempotent traces.  I would have liked to
know more about how the "implementation still incorporates this
feature as a performance optimization".

This paper is solid, but the contribution is in some ways narrow, and
so it could be made even better.

First, I think the current framing of the contribution over-reaches.
At the outset of the paper, it sounds like this is a contribution to
all task-graph based systems (which is a great many).  But many
parallel programming models generate task graphs directly, thus the
work of constructing a dependence graph from a sequential list of
tasks (the work being memoized here) wouldn't exist in the first
place.  The model treated in this paper is a more narrow one
exemplified by Legion and related systems.

Second, for a task-graph based system, the biggest challenge to
directly adopting tracing techniques would be identifying isomorphic
subgraphs (without solving the NP-complete problem at runtime).
It's very practical to sidestep this problem in some way, by having
the programmer do extra work.  But the exact way in which the problem
was sidestepped in this paper is not shown in much detail, and it
leaves the reader with the impression that perhaps so much is done
manually by the programmer here that what is automated is actually not
that challenging.

More details on the implementation and more examples would help.
In its current form, the paper is heavily focused on the formalism of
tasks and dependence graphs; the only example code shown is in figure
2(a).  This is a fine example program, but we never see what a
*runnable* program looks like -- i.e. including the calls that
explicitly mark the begin/end trace region, and including the details
of naming traces, as described on line 1096: "we require that two
traces that are assigned the same name be isomorphic".

I believe some of the straightforward parts of the formalism could be
relegated to an appendix, which would make more room to explain the
implementation and give examples.

The formal work of this paper establishes important safety properties
of the memoization process.  As with any incremental computing, the
memoized version should behave the same as the original.  The
development is somewhat straightforward, and when formalizing
something that is straightforward (but still valuable!), it would be
great to see a mechanized proof.

Finally, I think there are some missed opportunities here to widen the
scope of overheads amortized by the tracing process.  While
task-stream->DAG conversion may be unique to Legion and similar
models, surely there are some overheads that would apply to a broader
range of systems that could be addressed here?  That is, establishing
isomorphic task graphs (and data placements) on two iterations is a
very strong guarantee that should yield all kinds of potential
dividends.  Nevertheless, for Legion specifically, clearly task graph
handling was a bottleneck to strong scaling, because the results show
a marked improvement in scaling across the benchmark applications.

One thing the evaluation does not establish, however, is whether the
previous (no tracing) Legion results were competitive with reference
implementations not just at 1-16 nodes, but at the 256 node scale as
well.  Including results from other reference implementations in
Figure 9 would help establesh that Legion's scaling was already
reasonable, and that this optimization makes it excellent.
(Otherwise, the reader must entertain the hypothesis that Legion had a
scaling bottleneck which other systems did not, and that this work
only addresses a Legion-specific scaling problem.)

Minor:

l 439 - I'm having trouble type checking $\in op$ here, as $op$ is
defined as a constant-sized task or copy operation, not a set.

l 993 - this is a bit of a let-down.  Thus it may be good to clarify
this limitation in the introduction.  That way, it is clear that a
fully automatic policy for tracing is not part of the paper.

l 1194 - it is mentioned briefly that Soleil-X uses OpenMP fork
task-parallelism.  It is unclear how this Legion+OpenMP hybrid works,
so some clarification would be appreciated.

l 1290 - figure 9(f): these are rather coarse-grained tasks.  It would
be good to establish expectations for this early: i.e. distributed
schedulers need coarser grained tasks than node-local shared-memory
ones like OpenMP.

Questions for Authors
---------------------
Why model tasks as already having specific data instances as their
targets?  That is, it seems that the more natural place to start is
that tasks depend on logical data ($r$ rather than $r^i$), and then a
distribution/mapping is applied, which refines the task stream to
refer to instances rather than regions.

It would help to have some intuition about the runtime handling of
task streams.  Is there a single node that has to process the
sequential task stream?  (Does not seem scalable.)
