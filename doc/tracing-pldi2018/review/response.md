We thank the reviewers for their detailed and insightful feedback. We will incorporate suggestions in the final paper.

The proofs can be found here: https://tinyurl.com/dynamictracing. 

Responses to reviewers' individual questions and comments.

* Reviewer 1

The optimisations in §5.4 are constant propagation and transitive reduction, both of which are standard dataflow analyses and straightforward to formalize.

To integrate dynamic tracing into a system it is necessary to implement trace discovery, which amounts to finding repeating patterns in a stream of tasks that are beneficial to capture and replay. We think this is an important problem, but one that is orthogonal to the dynamic tracing mechanism.
 
* Reviewer 2

As the reviewer points out, graph calculus on its own consists of only simple graph operations. However, recording graph calculus commands for subgraphs and recognizing the contexts in which they can be replayed are non-trivial to formally design and implement.  This is our main technical contribution.

* Reviewer 3

In the case of Legion and some task-based systems the assignment of instances is done by user-defined mappers, but dynamic tracing is general enough to handle other cases including mapping decisions made by heuristics in the runtime system.

We considered optimizing portions of graph calculus commands at each step in the analysis, but we found that this only complicates the proof of correctness as we then need to consider that the command does not generate a graph identical to the one that was recorded, but an equivalent one.

As noted, performance improvements from tracing depend on how many tasks in a program can be traced. However, for applications used in Section 8, we could easily capture traces of the body of the main loops where programs spend the majority of their time.

* Reviewer 4

We agree with reviewer 4 that dynamic tracing’s ability to memoize the dynamic dependence analysis primarily benefits Legion and related systems. We believe there will be many more such systems in the future, as dynamic generation of task graphs becomes more widespread. We also believe that the even more important and general aspect of dynamic tracing is its ability to amortize the cost of “lowering” task graphs from a machine-independent representation to a specific hardware architecture. Almost all task graph programming frameworks perform this lowering today to handle the heterogeneity in modern architectures. Currently most systems perform lowering at compile-time or JIT-time. However, more and more systems are moving to dynamically specialize task graphs at runtime to deal with issues such as load balancing and fault tolerance. This is most evident today with machine learning frameworks like PyTorch and Chainer beginning to support dynamic graph generation and specialization. As this practice becomes more prevalent, it also becomes necessary to reduce the overheads of lowering the task graph to a target machine and dynamic tracing will provide a way to achieve this both correctly and efficiently.

We agree that identifying isomorphic subgraphs, or more precisely when a program generates isomorphic subgraphs, is a challenge. In programming models that directly generate task graphs, this challenge is equivalent to proving semantic equivalence between different parts of a program. However, a key observation in our setting is that when task graphs are constructed from a task stream, isomorphic traces can generate isomorphic subgraphs. One of the contributions of our paper is to formally demonstrate when isomorphic traces actually lead to isomorphic subgraphs, which is when they see the same valid instances for their access set. Thus, our paper addresses the problem of identifying isomorphic subgraphs extracted from task streams, and the only manual work the formalism assumes is the demarcation of traces. That said, our paper does not address the issue of finding traces that are beneficial to trace, which we believe is an important, but orthogonal issue.

We will include results from the reference implementations in the final paper.  All the Legion applications already had competitive or better weak scaling performance than the reference codes; this work addresses the strong scalability.

Considering mappings in the formalism only adds complexity without changing the essence that our model captures. The modifications would be 1) tasks are annotated with regions, instead of instances, and 2) rules query an extra mapping function M which maps regions in tasks to instances. These only change how task streams are specified and not actually how the rules find, capture, and replay dependencies between tasks. However, our implementation does record and replay mappings because the Legion runtime consults with user-defined mappers for instance assignment.

Legion handles concurrent, distributed task streams. Each task stream can be analyzed independently and requires no synchronization with other streams, as the programming model guarantees any children of independent tasks to be independent as well and thus concurrent task streams are also independent of each other.
