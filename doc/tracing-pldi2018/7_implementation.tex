% Implementation (0.5-1 page)
\section{Implementation}
\label{sec:implementation}

We have implemented dynamic tracing in Legion, a C++ runtime system for task parallelism~\cite{Legion12}. 
%
Legion runs a dependence analysis similar to the one in Figure~\ref{fig:rec-analysis} and builds a task graph using Realm~\cite{Realm14}, an event-based explicitly parallel runtime for distributed memory systems.
%
We augment Legion's existing dependence analysis to generate graph calculus programs for traces.
%
Graph calculus is implemented as a set of commands that internally call the Realm API to construct task graphs.

In Legion, traces are delimited simply by runtime calls that are manually inserted at the beginning and the end of a sequence of tasks that the programmer wants to trace.
%
Furthermore, we require that two traces that are assigned the same name be isomorphic.
\zap{
%
This allows us to start replaying the command without capturing the whole trace, and also makes the replay incremental and even parallel, which we discuss in Section~\ref{subsec:par-dep}.
}

In the rest of this section, we briefly discuss the ways in which our implementation differs.

\subsection{Overlapping Regions}

The biggest difference between Legion and our abstract dependence analysis is that regions can be partitioned into subregions, and thus can overlap with each other in non-trivial ways.
%
This complicates the dependence analysis, but has no fundamental impact on how we generate graph calculus commands.
%
We can extend the system in Figure~\ref{fig:rec-analysis} to support overlapping regions by allowing instances to belong to multiple regions that overlap each other, and by finding the minimal set of valid instances that cover the current instance that a task wants to make valid in the rule (DR2).
%
Then, each valid instance in that set introduces one copy to the target instance, which corresponds to one graph calculus command to be recorded.
%
%The Legion runtime has already implemented {\em composite instances} to describe a set of valid instances that match a region and we only needed to augment this mechanism.

\subsection{Reductions and Fills}

Another difference with Legion is that tasks can request {\em reduction} privileges on regions.
%
A reduction is an update through a commutative, associative operator with an identity.
%
Reduction tasks are parallelized by summarizing the update from each task into a temporary instance and lazily aggregating such instances to compute the final value when it is requested in subsequent tasks.
%
Temporary instances are initialized with an identity of the reduction operator before they are passed to the tasks.
%
Therefore, for reductions we must record not only tasks themselves, but also the initialization of their temporary instances.

Legion also provides {\em fills}, which are lazy copies from a constant value. 
%
We incorporate fills as another kind of graph calculus operation.
%
We also had to modify graph calculus to include recorded fills in the set of previous operations for tasks that perform reductions.

\floatstyle{plain}
\restylefloat{figure}

\input{fig_performance}

\zap{
\subsection{Control Replication}

We use {\em control replication}~\cite{Regent17} to improve the scalability of our system.
%
Control replication is a compiler transformation that optimizes programs that launch a single stream of tasks into an SPMD-style program where multiple nodes each launch a subset of the tasks.
%
The control replication optimization needs to ensure that two tasks that were dependent in the original single stream but are now initiated on different nodes are still executed in the correct order and with the correct data; we had to similarly augment dynamic tracing to handle dependent operations split across multiple nodes.

\subsection{Parallel Dependence Analysis}
\label{subsec:par-dep}

Our formalism assumes that dependence analysis runs sequentially on a sequence of tasks.
%
The Legion runtime actually parallelizes dependence analysis by splitting it into {\em logical} and {\em physical} phases.
%
In the logical analysis, tasks are sequentially analyzed based on their region usages.
%
The the physical analysis, which takes instances into account, can be done in parallel for tasks that are discovered to be independent.
%
The replay of a trace can be parallelized similarly;
%
instead of running the whole command for a trace, we can split it into commands for individual tasks and replay those for logically independent tasks in parallel.
%
We also memoize logical dependences between tasks to avoid running the logical analysis repeatedly.
}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
