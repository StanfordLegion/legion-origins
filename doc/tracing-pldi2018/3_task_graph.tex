\floatstyle{boxed}
\restylefloat{figure}

\section{Background}
\label{sec:background}

In dynamic task-based programming models, a program generates a stream of tasks that are parallelized at runtime.
%
We abstract the program as the sequence of tasks it generates.
%
We call a subsequence of tasks in a program a {\it trace}.
%
\begin{center}
  $
  \begin{array}{ll}
    \text{Programs} & p \in \mi{Program} = \mi{Task}^{*} 
    \qquad p ::= \epsilon \mid t; p
    \\
    \text{Tasks} & t \in \mi{Task}
    \\
    \text{Traces} & \mi{tr} \in \mi{Trace} = \mi{Task}^{*}
  \end{array}
  $
\end{center}

A task has a unique identifier and is described by pairs of instances and privileges.
%
We use $\wp(X)$ for the powerset of X.
%
\begin{equation*}
  \mi{Task} = \mi{Id} \times \wp(\mi{Instance} \times \mi{Privilege})
\end{equation*}
%
We write $\ms{id}(t)$ to mean an identifier of task $t$.
%
An {\em instance} represents the memory that holds a task argument.
%
Tasks can read from or write to an instance when they have read ($\ms{rd}$) or write ($\ms{wr}$) {\it privilege}, respectively.
%
\begin{center}
  $
  \begin{array}{ll}
    \text{Privileges} & \mi{pv} \in \mi{Privilege} = \mset{\ms{rd}, \ms{wr}}
  \end{array}
  $
\end{center}
%
For simplicity, we assume that when a task writes an instance, it writes all data in the instance (equivalently, that instances hold only a single datum).

\subsection{Regions and Coherence}

In distributed memory systems, one logical datum may be represented by multiple instances in different memories.
%
For example, two tasks that use the same data but run on different nodes will each have their own instance in a local memory.
%
A {\em region} is a set of instances corresponding to the same data.
%
For simplicity, we assume each instance is in only one region.
%
\begin{center}
  $
  \begin{array}{lrl}
    \text{Regions} & r, s & \in \mi{Region} = \wp(\mi{Instance})
    \\
    \text{Instances} & r^1, r^2, s^1, s^2, \dots & \in \mi{Instance}
  \end{array}
  $
\end{center}
%
Superscripts name instances in a region; for example $r^1$ and $r^2$ are two distinct instances of region $r$.
%
We use $I$ for a set of instances.

If a task updates an instance, any subsequent tasks reading instances of the same region must see the update.
%
We call this property {\it coherence}.
%
Since a program does not describe how data should be propagated from one instance to another, copies must be added to the program to guarantee coherence.

To describe an execution of a program, we use a {\it schedule}, which is a sequence of tasks and copies.
%
A {\it copy} is a pair of a source and a destination instance, along with an identifier.
%
Copies are valid only when their source and destination instances belong to the same region.
%
\begin{center}
$
  \begin{array}{ll}
    \text{Schedules} & \mi{sc} \in \mi{Schedule} = \mi{Op}^{*}
                       \hfill \qquad sc ::= \epsilon \mid \mi{op}; \mi{sc}
    \\
    \text{Operations} & \mi{op} \in \mi{Op} = \mi{Task} + \mi{Copy}
                        \hfill \qquad \mi{op} ::= t \mid \mi{cp}
    \\
    \text{Copies} & \mi{cp} \in \mi{Copy} = \mi{Id} \times \mi{Instance} \times \mi{Instance}
  \end{array}
$
\end{center}
%
A coherent schedule of program $p$ is a schedule that satisfies the rules in Figure~\ref{fig:sc-check}.
%
\begin{df}
  Schedule $\mi{sc}$ is a coherent schedule of program $p$ iff $\bullet, p |-_{\ms{co}} \mi{sc}$.
\end{df}

\begin{figure}[t]
  \centering
  $
  \begin{array}{c}
    \zap{
    \multicolumn{1}{l}{\framebox{$V, p |-_{\ms{co}} \mi{sc}$}}
    \\
    }
    \inference[(S1)]
    {}
    {V, \epsilon |-_{\ms{co}} \epsilon}
    \quad
    \inference[(S2)]
    {
    \mi{cp} = (\mi{id}, r^i, r^j)
    \quad
    r^i \in V(r)
    \\
    V[r \mapsto V(r) \cup \mset{r^j}], p |-_{\ms{co}} \mi{sc}
    }
    {V, p |-_{\ms{co}} \mi{cp}; \mi{sc}}
    \\
    \\
    \inference[(S3)]
    {
    \forall (r^i, \ms{rd}) \in t. r^i \in V(r)
    \quad
    V[t], p |-_{\ms{co}} \mi{sc}
    }
    {V, t; p |-_{\ms{co}} t; \mi{sc}}
  \end{array}
  $
  \caption{Coherence check}
  \label{fig:sc-check}
\end{figure}

The rules in Figure~\ref{fig:sc-check} check that each task in a schedule reads from {\em valid} instances.
%
A valid instance list $V$ keeps a set of valid instances for each region.
%
\begin{center}
  $
  \begin{array}{ll}
    \text{Valid Instance Lists} & V : \mi{Region} \to \wp(\mi{Instance})
    \\
                                & V ::= \bullet \mid V[r \mapsto I]
  \end{array}
  $
\end{center}
%
A copy of a valid instance is valid (Rule S2) and when a valid instance is written that becomes the only valid instance (Rule S3).
%
The notation $(r^i, \mi{pv}) \in \mi{op}$ means that $\mi{op}$ has privilege $\mi{pv}$ on instance $r^i$.
%
We define $V[t]$ as follows.
%
\begin{equation*}
  V[t](r) = \left\{
    \begin{array}{lr}
      r^j & \text{when } (r^j, \ms{wr}) \in t
      \\
      V(r) & \text{otherwise}
    \end{array}
  \right.
\end{equation*}
%
The rules additionally require tasks to appear in program order.
%
Therefore, the following lemma holds for every coherent schedule.
%
\begin{lem}
  We write $s |- a \succ b$ if $b$ immediately follows $a$ in sequences $s$, and $s |- a \succ^{+} b$ if $b$ occurs somewhere after $a$ in $s$.  
  %
  A coherent schedule $\mi{sc}$ of program $p$ satisfies the following property:
  \begin{center}
    $
    \begin{array}{c}
    \forall t_1, t_2. p |- t_1 \succ t_2 \implies \mi{sc} |- t_1 \succ^{+} t_2.
    \end{array}
    $
  \end{center}
\end{lem}

We require that the results of tasks be well-defined by permitting tasks to update at most one instance of a region.
%
\begin{df}
  Task $t$ is well-defined when it satisfies the following condition:
  \begin{center}
    $
    \begin{array}{c}
      \forall r^i, s^j. (r^i, \ms{wr}) \in t \land (s^j, \ms{wr}) \in t \implies r \ne s.
    \end{array}
    $
  \end{center}
\end{df}

\subsection{Dependences and Valid Schedules}

Operations are {\em dependent} when they access the same instance and at least one can write to it.

\begin{df}
  We write $\mi{op}_1 \dep \mi{op}_2$ when operations $\mi{op}_1$ and $\mi{op}_2$ are dependent on each other, i.e.,
  \begin{equation*}
    \begin{array}{l}
      \exists r^i, \mi{pv}_1, \mi{pv}_2.
      (r^i, \mi{pv}_1) \in \mi{op}_1 \land (r^i, \mi{pv}_2) \in \mi{op}_2
      \\
      \qquad \land\ (\mi{pv}_1 = \ms{wr} \lor \mi{pv}_2 = \ms{wr}).
    \end{array}
  \end{equation*}
\end{df}
\noindent
We use $(r^i, \ms{rd}) \in \mi{cp}$ when $r^i$ is the source of copy $\mi{cp}$ and $(r^i, \ms{wr}) \in \mi{cp}$ when $r^i$ is the destination.

A {\em valid schedule} is any permutation of a coherent schedule that preserves the ordering of dependent operations.
%
\begin{df}
  Schedule $\mi{sc}$ is valid for program $p$ iff it satisfies the following condition for a coherent schedule $\mi{sc}_{\ms{co}}$ of $p$:
  \begin{center}
    $
    \begin{array}{l}
      \forall \mi{op}_1, \mi{op}_2. \mi{sc}_{\ms{co}} |- \mi{op}_1 \succ^{+} \mi{op}_2
      \land \mi{op}_1 \dep \mi{op}_2 \implies
      \\
      \qquad \mi{sc} |- \mi{op}_1 \succ^{+} \mi{op}_2.
    \end{array}
    $
  \end{center}
\end{df}
%
\begin{ex}
  Consider a program $p = t_1;t_2;t_3;t_4$, where
  \begin{center}
    $
    \begin{array}{l}
      t_1 = (1, \mset{(r^1, \ms{wr})}),
      t_2 = (2, \mset{(r^2, \ms{wr})}),
      \\
      t_3 = (3, \mset{(r^3, \ms{rd})}), \text{ and }
      \quad
      t_4 = (4, \mset{(r^1, \ms{rd})}).
    \end{array}
    $
  \end{center}
  %
  In a coherent schedule, tasks $t_3$ and $t_4$ should see the updates from task $t_2$.
  %
  Thus, a schedule $t_2;(4, r^2, r^3);t_3;t_1;(5, r^2, r^1);t_4$ is valid for $p$, while another schedule
  $t_2;t_1;(4, r^1, r^3);t_3;t_4$ is not.
\end{ex}

\subsection{Task Graphs}

For the purpose of discovering valid schedules, parallel programming systems construct a {\it task graph}, which concisely represents a set of schedules.
%
Independent tasks in a program are mutually unreachable from one another in this graph.

A task graph is a directed acyclic graph of tasks and copies.
%
An edge between nodes constrains the order in which they can be scheduled.
%
\begin{equation*}
  \begin{array}{llcl}
    \text{Task Graphs} & G = \mangle{O, D} & \in & \mi{TaskGraph}
    \\
                       & & & = \wp(\mi{Op}) \times \wp(\mi{Op} \times \mi{Op})
  \end{array}
\end{equation*}
%
We write $\emptyg$ to denote an empty task graph $\mangle{\varnothing, \varnothing}$.
%
\begin{df}
  Schedule $\mi{sc}$ of task graph $\mangle{O, D}$ satisfies the following property:
  \begin{equation*}
    \forall \mi{op}_1, \mi{op}_2 \in O. \mangle{\mi{op}_1, \mi{op}_2} \in D \implies
    \mi{sc} |- \mi{op}_1 \succ^{+} \mi{op}_2.
  \end{equation*}
\end{df}
\noindent
Any topological ordering of a task graph is a schedule.
%
We write $\sem{G}$ for the set of schedules represented by a task graph $G$.
%
\begin{df}
  Task graph $G$ is valid for a program iff any schedule $\mi{sc}$ in $\sem{G}$ is valid for the program.
 \end{df}

\zap{
There can be multiple valid task graphs for a program and we want the most ``flexible'' one, i.e., the one that expresses the biggest set of valid schedules.
%
\begin{df}
  We write $G_1 \sqsubseteq G_2$ when task graph $G_2$ is {\em more relaxed} than $G_1$, i.e.,
  $\sem{G_1} \subseteq \sem{G_2}$.
\end{df}
}
\zap{
We write $G_{r^i}$ to denote $r^i$-connected subgraph of task graph $G$, which has only the operations that use instance $r^i$.
%
\begin{df}
  The $r^i$-connected subgraph $\mangle{O_{r^i}, D_{r^i}}$ of task graph $\mangle{O, D}$ satisfies the following conditions:
  \begin{equation*}
    \begin{array}{l}
      O_{r^i} = \mset{\mi{op} \in O \mid (r^i, \mi{pv}) \in \mi{op}}
      \\
      D_{r^i} = \mset{\mangle{\mi{op}_1, \mi{op}_2} \in D \mid \mi{op}_1, \mi{op}_2 \in O_{r^i}}
    \end{array}
  \end{equation*}
\end{df}
}
Concatenation and subtraction of task graphs are also used in our development.
%
Subtraction between two task graphs is defined as follows:
\begin{df}
  \begin{equation*}
    \begin{array}{l}
      \mangle{O_1, D_1} - \mangle{O_2, D_2} \triangleq \mangle{O, D}, \text{ where }
      \\
      \quad
      O = O_1 - O_2, \text{ and }
      D = \mset{(\mi{op}_1, \mi{op}_2) \in D_1 \mid \mi{op}_1, \mi{op}_2 \in O}
    \end{array}
  \end{equation*}
\end{df}
%
The definition of concatenation is more involved as it requires notions of the {\em origin} and the {\em frontier}.
%
Origins are nodes with no predecessors and frontiers are nodes with no successors.
%
Concatenation of two task graphs connects all frontiers in the first graph to all origins in the second graph.
%
\begin{df}
  \begin{equation*}
    \begin{array}{l}
      \ms{origin}(\mangle{O, D}) \triangleq \mset{\mi{op} \in O \mid \not \exists \mi{op}'.
      \mangle{\mi{op}', \mi{op}} \in D}
      \\
      \ms{front}(\mangle{O, D}) \triangleq \mset{\mi{op} \in O \mid \not \exists \mi{op}'.
      \mangle{\mi{op}, \mi{op}'} \in D}
    \end{array}
  \end{equation*}
\end{df}
%
\begin{df}
  \begin{equation*}
    \begin{array}{l}
      \mangle{O_1, D_1} + \mangle{O_2, D_2} \triangleq \mangle{O, D}, \text{ where } O = O_1 \cup O_2 \text{ and }
      \\
      \quad D = D_1 \cup D_2\ \cup
      \\
      \quad
      \mset{\mangle{\mi{op}_1, \mi{op}_2} \mid
      \mi{op}_1 \in \ms{front}(\mangle{O_1, D_1}) \land \ms{origin}(\mangle{O_2, D_2})}
    \end{array}
  \end{equation*}
\end{df}
%
\zap{
\noindent
The following lemma captures the intuition that given schedules for two task graphs, we can construct a schedule in the concatenated task graph.
%
\begin{lem}
  \begin{equation*}
    \mi{sc}_1 \in \sem{G_1} \land \mi{sc}_2 \in \sem{G_2} \implies \mi{sc}_1; \mi{sc}_2 \in \sem{G_1 + G_2}
  \end{equation*}
\end{lem}
}
\zap{
Among the task graphs whose schedules are the same, some task graphs are more succinct than the others when they have a smaller number of edges.
%
Especially, if a task graph has no edges between nodes that are already connected transitively by other edges, the graph is called {\it minimal} because no other graphs with a smaller set of edges can express the same set of schedules.
%
\begin{equation*}
  \begin{array}{l}
    \mangle{T, D}\text{ is minimal} \triangleq \forall t_1, t_2 \in T.
    \\
    \quad\qquad  (t_1, t_2) \in D \implies
    \not \exists t_3. (t_1, t_3) \in D \land (t_3, t_2) \in D^{+}
  \end{array}
\end{equation*}
%
We are interested in a valid task graph that is minimal, but still the most relaxed one for a given program.
}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
