\input{fig_example}

\section{Introduction}

% Programming big machines is hard
Programming large distributed systems is a challenging problem in both high performance computing (HPC) and data center environments today. 
% Task graphs are good at programming big machines
To address this problem, there has recently been a resurgence in programming systems that target these machines by expressing computations as task graphs~\cite{Legion12,Realm14,StarPU11,PARSEC17,TensorFlow15,MXNet15,Nimbus17,CnC14}. 
% This is what a task graph is
The nodes in a task graph represent opaque computations and edges represent either control or data dependences constraining the execution of the graph.
% Task graphs are very useful
Many problems in parallel programming such as mapping~\cite{Legion12}, scheduling~\cite{OmpSs15}, load balancing~\cite{Charm11}, fault tolerance~\cite{Kurt14}, and resource allocation~\cite{Legion12,CnC14} have been successfully addressed using task graphs.

% Task graphs are hard for end users
While tasks graphs are useful tools for programming systems, they can be difficult to work with directly for application developers as they can be challenging to correctly generate and compose.
% Therefore there is a translation layer that does dependence analysis
For this reason, many systems provide higher-level abstractions and rely on an additional translation layer to perform a dependence analysis that constructs an underlying task graph~\cite{Uintah10,Legion12,StarPU11,TensorFlow15,MXNet15,PARSEC17}.
% Dependence analysis is easy when iterative
In some cases the task graph is static and can be constructed once at the beginning of an application \cite{TensorFlow15,MXNet15,Nimbus17,CnC14}.
% Not everything is iterative
However, in more dynamic applications, the computation being performed can be dependent on application data and therefore dependence analysis and the shape of the task graph must also be computed at runtime.
% Therefore more dynamic systems exist
Systems like Legion~\cite{Legion12} and StarPU~\cite{StarPU11} are built around a continuously running dependence analysis that constructs a task graph on the fly, enabling them to adapt to the changing needs of an application. 

% Dependence analysis is expensive
While a fully dynamic dependence analysis is very flexible, it also incurs runtime overhead that can limit performance.
% For any program there is a minimum granularity of task
In most cases the cost of dynamic dependence analysis can be hidden if the cost of the analysis for a task is on average smaller than the time it takes to execute that task~\cite{Legion12}. 
% Minimum granularity of a task
Therefore the cost of dynamic dependence analysis places a lower bound on the granularity of tasks that can be handled efficiently and how well applications strong scale. 
% Importantance of scaling
Consequently dynamic dependence analysis must be as efficient as possible to avoid limiting system performance.

% But we shouldn't have to pay for it all the time
The crucial insight of this work is that, while some applications require a fully dynamic dependence analysis, they often have {\em traces} of repetitive tasks for which we can memoize the results of the dynamic dependence analysis and therefore significantly reduce the overhead of executing tasks in a trace.
% Analogy to tracing JIT
While similar in spirit to trace-based JIT-compilation systems~\cite{Gal09,Chang09,Tracing10,SPUR10,Tracing11,TCAI14}, specializing dynamic dependence analysis for parallel and distributed systems raises new correctness and performance issues.

% What our system is 
To address these issues, we present {\em dynamic tracing}, a technique to efficiently and correctly memoize an abstract dependence analysis and soundly JIT regenerate a task graph equivalent to the original.  
% How does it work
Dynamic tracing achieves this goal in two steps.
% Capture graphs
First, it records the analysis of a trace as a sequence of {\em graph calculus} commands;
% What is graph calculus
graph calculus is a simple imperative calculus with commands that directly construct task graphs.
% Replay graphs
Second, whenever previously captured traces appear in the program, the recorded graph calculus commands are replayed to replace the dependence analysis.

This paper makes four contributions:

\begin{itemize} \itemsep0pt \parskip0pt \parsep0pt
\item  To the best of our knowledge, our paper presents the first formal treatment of dynamic dependence analysis and tracing, including formalizing the handling of data dependences in a distributed setting.

\item We prove the soundness of the translation from dependence analysis to graph calculus.

\item We describe an implementation of dynamic tracing embedded in the Legion runtime system.

\item For five already optimized applications, we demonstrate that dynamic tracing improves strong scaling performance up to 4.9X, and by 3.8X on average, when running on up to 256 nodes.
\end{itemize}

The rest of this paper is organized as follows.
%
In Section~\ref{sec:overview}, we give an informal overview of dynamic tracing.
%
Section~\ref{sec:background} defines basic notation and concepts.
%
Then, we present our formalism for dependence analysis, graph calculus, and dynamic tracing in Sections \ref{sec:implicit}-\ref{sec:tracing}.
%
Section~\ref{sec:implementation} discusses the implementation of dynamic tracing in Legion and Section~\ref{sec:eval} presents experiment results.
%
We survey related work in Section~\ref{sec:related} and conclude in Section~\ref{sec:conclusion}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
