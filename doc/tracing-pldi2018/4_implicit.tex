% Generating Task Graph using Dependence Analysis (2 pages)
\section{Dependence Analysis}
\label{sec:implicit}

\input{fig_dep_analysis}

In this section, we present a system that generates a valid task graph from a program.
%
\zap{The system guarantees that it also finds the most relaxed task graph.}

An issue in generating a task graph is avoiding adding unnecessary dependences, which can happen if some effort is not made to avoid adding transitive dependence edges.
%
We exploit the Single-Writer, Multiple-Reader (SWMR) invariant~\cite{SWMR} to safely ignore most transitive dependences between  tasks that use the same instance.
%
The SWMR invariant states that in any given {\em epoch} of tasks, there can be only one writer task or multiple reader tasks for one instance.
%
When a new task arrives, there are only two possible cases:
%
\begin{itemize}
\item[Case 1.] If both the current epoch and the new task are readers of the instance, then the new task joins the current epoch.
\item[Case 2.] If either the current epoch or the new task is a writer of the instance, then the new task starts a new epoch and the current epoch becomes the previous epoch.
\end{itemize}
%
In both cases, the new task gets dependences on any tasks in the previous epoch.

Figure~\ref{fig:rec-analysis} ignoring the gray highlighting formalizes the dependence analysis based on the SWMR invariant.
%
The relation $\tdep$ describes one step of the dependence analysis for the first task in a program.
%
For each task, we  first analyze the instances with read privilege and then those with write privilege.
%
We write $\mi{op}(\mi{pv})$ to denote a set of instances in operation $\mi{op}$ whose privilege is $\mi{pv}$, i.e.,
\begin{center}
  $
  \begin{array}{c}
  \mi{op}(\mi{pv}) \triangleq \mset{r^i \mid (r^i, \mi{pv}) \in \mi{op}}.
  \end{array}
  $
\end{center}

The rules use an {\it epoch list} $E$ and an {\em epoch privilege} $P$ to maintain the SWMR invariant.
%
The epoch list is a map from instances to pairs of the current and previous epochs and the epoch privilege tracks whether the current epoch of an instance is of a writer ($\ms{wr}$) or of readers ($\ms{rd}$).
%
\begin{equation*}
  \begin{array}{ll}
    \text{Epoch Lists}
    &
    E : \mi{Instance} \to \wp(\mi{Op}) \times \wp(\mi{Op})
    \\
    & E ::= \bullet \mid E[r^i \mapsto \mangle{O, O}]
    \\
    \text{Epoch Privileges}
    &
    P : \mi{Instance} \to \mi{Privilege}
    \\
    &
    P ::= \bullet \mid P[r^i \mapsto \mi{pv}]
  \end{array}
\end{equation*}
%
An empty epoch list $\bullet$ maps every instance to a pair of empty epochs and an empty epoch privilege $\bullet$ returns $\ms{wr}$ for all instances.
%
The epoch list and epoch privilege are valid only when they actually maintain the SWMR invariant.
%
\begin{df}
  Epoch list $E$ and epoch privilege $P$ are valid iff they satisfy the following conditions.
  \begin{enumerate}
  \item The previous and current epoch for an instance contain only the operations for that instance.
    \begin{equation*}
      \forall r^i. \forall \mi{op} \in E(r^i).1 \cup E(r^i).2. \exists \mi{pv}. (r^i, \mi{pv}) \in \mi{op}
    \end{equation*}
  \item There cannot be multiple writers.
    \begin{equation*}
      \begin{array}{rl}
        \forall r^i. |E(r^i).1| > 1 \implies \!\!\!&\!\! \forall \mi{op} \in E(r^i).1. (r^i, \ms{wr}) \not \in \mi{op}
        \\
        \ \land\     |E(r^i).2| > 1 \implies \!\!\!&\!\! \forall \mi{op} \in E(r^i).2. (r^i, \ms{wr}) \not \in \mi{op}
      \end{array}
    \end{equation*}
  \item The epoch privilege represents the privilege of the current epochs.
    \begin{equation*}
      \begin{array}{rl}
        \forall r^i. P(r^i) = \ms{rd} \implies \!\!\!&\!\! \forall \mi{op} \in E(r^i).1. r^i \in \mi{op}(\ms{rd}) - \mi{op}(\ms{wr})
        \\
        \lor\  P(r^i) = \ms{wr} \implies \!\!\!&\!\! \forall \mi{op} \in E(r^i).1. r^i \in \mi{op}(\ms{wr})
      \end{array}
    \end{equation*}
  \item If the current epoch for an instance is of readers, the previous epoch should be of a writer.
    \begin{equation*}
      \forall r^i. P(r^i) = \ms{rd} \implies \forall \mi{op} \in E(r^i).2. (r^i, \ms{rd}) \not \in \mi{op}
    \end{equation*}
  \end{enumerate}
\end{df}

The function $\ms{swmr}$ describes how a new task is added to the epochs:
%
\begin{center}
  $
  \begin{array}{l}
    \ms{swmr}(\mangle{O_1, O_2}, \mi{pv}_1, \mi{op}, \mi{pv}_2) \triangleq
    \\
    \qquad
    \left\{
    \begin{array}{lr}
      \mangle{O_1 \cup \mset{\mi{op}}, O_2} & \text{when }\mi{pv}_1 = \mi{pv}_2 = \ms{rd}
      \\
      \mangle{\mset{\mi{op}}, O_1} & \text{otherwise}
    \end{array}
    \right.
  \end{array}
  $
\end{center}

Once the epochs are adjusted, the dependences between a new task and those in the previous epoch (written as $E'(r^i).2$ in the rules) are registered to the task graph $G$ using the function $\ms{insert}$:
%
\begin{center}
  $
  \begin{array}{l}
    \ms{insert}(\mangle{O, D}, O', \mi{op}) \triangleq
    \\
    \qquad
    \mangle{O \cup \mset{\mi{op}},
    D \cup \mset{\mangle{\mi{op}', \mi{op}} \mid \mi{op}' \in O' \cap O}}
  \end{array}
  $
\end{center}
%
Note that the task being inserted is removed as a possible source of a dependence to avoid a self-dependence.
%
This case happens when a task requests both read and write privilege on the same instance.

The rules also use a valid instance list $V$ to check if an instance read by a task is valid.
%
The rule $\text{(DR2)}$ shows a case when the instance is not valid, and thus needs a copy from the other valid instance.
%
The rule treats copies similarly to tasks; i.e., it first recurses for the source instance and then for the destination instance.
%
However, the key difference between copies and tasks is that copies do not invalidate the existing valid instances, as shown in the rule $\text{(DW2)}$.

We define the consistency of the dependence analysis state as follows.
%
\begin{df}
  Dependence analysis state $(E, P, V, \mangle{O, D})$ is consistent iff it satisfies the following conditions.
  \begin{enumerate}
  \item The epoch list and privilege are valid.
  \item The task graph is acyclic.
  \item Valid instances are accessed by operations in their current epochs:
    $\forall r^i \in V(r). E(r^i).1 \ne \varnothing$.
  \item Operations in the task graph are connected if they are dependent, i.e.,
    \begin{equation*}
      \begin{array}{l}
        \forall \mi{op_1}, \mi{op_2} \in O. \mi{op}_1 \dep \mi{op}_2 \implies
        \\
        \quad
        \mangle{\mi{op}_1, \mi{op}_2} \in D^{+} \lor \mangle{\mi{op}_2, \mi{op}_1} \in D^{+}
      \end{array}    
    \end{equation*}
  \item The task graph has an edge from every operation in the previous epoch to every operation in the current epoch.
    \begin{equation*}
      \forall r^i. \forall \mi{op_1} \in E(r^i).1. \mi{op_2} \in E(r^i).2.
      \mangle{\mi{op_2}, \mi{op_1}} \in D
    \end{equation*}
  \zap{
  \item The operations in the current epoch of instance $r^i$ have no successor in the $r^i$-connected subgraph $\mangle{O_{r^i}, D_{r^i}}$:
    \begin{equation*}
      \forall \mi{op}_1 \in E(r^i).1. \not \exists \mi{op}_2. \mangle{\mi{op}_1, \mi{op}_2} \in D_{r^i}.
    \end{equation*}
  }
  \end{enumerate}
\end{df}
\noindent
The relation $\tdep$ preserves the consistency of its state.
%
\begin{lem}
  If dependence analysis state $(E, P, V, G)$ is consistent and $(E, P, V, G, p) \tdep (E', P', V', G', p')$, then state $(E', P', V', G')$ is also consistent.
\end{lem}
%
\begin{df}
  The dependence analysis of program $p$ is any task graph $G$ satisfying for some $E$, $P$, and $V$:
  \begin{equation*}
    (\bullet, \bullet, \bullet, \emptyg, p) \tdep^{*} (E, P, V, G, \epsilon)
  \end{equation*}
\end{df}
\noindent
The dependence analysis is not deterministic because of the non-deterministic choice of the source instance of the copy in the rule $\text{(DR2)}$.

\begin{ex}
  Consider a program $p = t_1;t_2;t_3$ where
  \begin{equation*}
    \begin{array}{l}
      t_1 = (1, \mset{(r^1, \ms{wr}), (s^1, \ms{wr})}),
      \quad
      t_2 = (2, \mset{(r^1, \ms{rd})}),\text{ and}
      \\
      t_3 = (3, \mset{(r^1, \ms{wr}), (s^1, \ms{rd})}).
    \end{array}
  \end{equation*}
  %
  Then, the dependence analysis of $p$ is as follows.
  % 
  \begin{center}
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
      \node at (0, 0) (t1) {$t_1$};
      \node at (1.5, 0.5) (t2) {$t_2$};
      \node at (3, 0) (t3) {$t_3$};
      \draw (t1) -- (t2);
      \draw (t1) -- (t3);
      \draw (t2) -- (t3);
    \end{tikzpicture}
  \end{center}
  \label{ex:dep}
\end{ex}

The most important property of the dependence analysis is {\it soundness}; i.e., it generates a task graph that is valid for the program.
%
\begin{thm}
  The dependence analysis of $p$ is valid for $p$.
\end{thm}
%
The proof of this theorem is derived by the property of $\tdep$ preserving the consistency of dependence analysis state.
%
\zap{
%
Another property of the system is that it generates the most relaxed task graph.
%
This means that the dependence analysis is {\it complete} in that it does not miss any independent tasks;
%
i.e., there is no path between two independent tasks in the generated graph.
%
Note that being most relaxed does not imply that the graph has the minimum number of edges; unneeded transitive edges can still be introduced.
%
\begin{thm}
  For any program $p$, the dependence analysis of $p$ is a most relaxed task graph that is valid for $p$.
\end{thm}
}
\zap{
\noindent
However, in a more general system where regions can overlap and instances can belong to multiple regions, this completeness is difficult, or inefficient, to guarantee.
}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "tracing"
%%% End:
