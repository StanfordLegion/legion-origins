\section{Evaluation}
\label{sec:results}

\begin{figure}
  \centering
  \begin{tabular}{lccr}
    & & {\bf Dependent} & \\
    & {\bf Original} & {\bf Partitioning} & \\
    {\bf Application} & {\bf LOC} & {\bf LOC} & {\bf Reduction} \\
    \hline 
    PENNANT & 163 & 6 & 96\% \\
    Circuit & 159 & 8 & 95\% \\
    MiniAero & 51 & 7 & 86\% \\
  \end{tabular}
%%  \vspace*{-3mm}
  \caption{Reduction in code required to compute partitions\label{fig:loc}}
%%  \vspace*{-6mm}
\end{figure}

In addition to the qualitative benefits of catching many partitioning-related problems at compile time,
our dependent partitioning framework provides quantitative improvements to both programmer productivity and
application performance.  To assess these benefits, we look at three applications that have been written
and tuned for Regent.  Two of these applications are the circuit simulation and PENNANT, which were discussed
briefly in Sections~\ref{sec:intro} and~\ref{sec:dpl}.  The third application is MiniAero,
part of the Mantevo\cite{Mantevo} project, which performs simulation of fluid dynamics in an
unstructured 3-D mesh.  Although all three applications perform computations on an unstructured graph or mesh,
there are differences in how ``unstructured'' they are.  Both PENNANT and MiniAero use a spatial decomposition
to derive their independent partition, resulting in a nearest-neighbor communication pattern between the
partitions.  In contrast, the circuit example uses a randomized graph that
%has locality that can be captured within a partition, but
has an all-pairs communication pattern between the partitions.

We rewrote the partitioning code of each application to use the new dependent partitioning operations
in Regent that we described in Section~\ref{sec:regent}.  The effort took only minutes, and involved deleting
nearly all of the code that was generating colorings for the old {\tt partition} operation, and instead using
images, preimages and set operations to achieve the same effect.  This time also includes the time that was
necessary to run the static analysis described in Section~\ref{sec:analysis} and find two bugs in the newly
written code, including the need to ``filter'' the PENNANT mesh as described in Section~\ref{subsec:limits}.
Since there were no changes to the actual
partitions being computed, no other code was changed in any of the applications.  Figure~\ref{fig:loc} summarizes
the dramatic improvements in the number of partitioning-related lines of code for each application.
These results do not include the application-specific code that computes
the assignments for the independent partition, as our framework allows that code to be used as is.

\begin{figure}
\includegraphics[width=\linewidth]{figs/single_node.pdf}
\vspace*{-6mm}
\caption{Partitioning time improvement on a single node\label{fig:single_node}}
%%\vspace*{-4mm}
\end{figure}

\begin{figure*}[t]
  \centering
  \subfloat[PENNANT]{
    \label{fig:pennant_scaling}
    \includegraphics[width=0.32\linewidth]{figs/pennant_scaling.pdf}
  }
  \subfloat[Circuit]{
    \label{fig:circuit_scaling}
    \includegraphics[width=0.32\linewidth]{figs/circuit_scaling.pdf}
  }
  \subfloat[MiniAero]{
    \label{fig:miniaero_scaling}
    \includegraphics[width=0.32\linewidth]{figs/miniaero_scaling.pdf}
  }
  \vspace*{-3mm}
  \caption{Strong scaling of partitioning work up to 64 nodes.\label{fig:scaling}}
  %%\vspace*{-4mm}
\end{figure*}

\begin{figure*}
  \vspace*{-5mm}
  \centering
  \subfloat[PENNANT]{
    \label{fig:pennant_isectopt}
    \includegraphics[width=0.32\linewidth]{figs/pennant_isectopt.pdf}
  }
  \subfloat[Circuit]{
    \label{fig:circuit_isectopt}
    \includegraphics[width=0.32\linewidth]{figs/circuit_isectopt.pdf}
  }
  \subfloat[MiniAero]{
    \label{fig:miniaero_isectopt}
    \includegraphics[width=0.32\linewidth]{figs/miniaero_isectopt.pdf}
  }
  \vspace*{-3mm}
  \caption{Impact of intersection optimization.\label{fig:isectopt}}
  %%\vspace*{-5mm}
\end{figure*}

The next benefit that can be seen is in the performance improvements in the partitioning computation when
run on a single compute node.  Figure~\ref{fig:single_node} shows these benefits for each of the applications.
These three applications perform partitioning during initialization and then simulate for anything from seconds
to hours, depending on the user's needs.
To eliminate that variability,
we report partitioning speedups considering only the time required for computing the partitioning and the
subsequent verification in the original Regent code.  Overall application speedup will vary between this
upper bound and negligible, depending on the length of the simulation.
%only the time required for computing
%the partitioning and subsequent verification is shown, and the times are normalized to the time taken for
%performing the partitioning in the original Regent code.
(None of these perform any re-partitioning (e.g. for load balancing)
during the computation, due in part to the cost of such repartitioning with existing programming models.  It is
our hope that the performance and productivity improvements of dependent partitioning make this a much more
attractive option in the future.)

Each application was run with three input sizes, each roughly 10x larger than the next.
For each case, the blue bar on the left shows the time
taken by the old partitioning method in Regent, and it is split into the time spent in the application to compute the coloring
(the darker blue) versus the time spent in the runtime to convert the coloring into its internal representation
(the lighter, but still solid, blue).  Across all three applications, the majority of the partitioning-related
effort falls to the application.

The middle grey bar in each case shows the time required to perform the equivalent dependent partitioning operations
on a single thread.  PENNANT partitioning times are improved by 76-100\%, Circuit by 62-103\%.
MiniAero benefits the most from a data structure that efficiently stores clusters of indices, improving by
11.2-12.7X.  The orange bar on the right in each case shows the further improvement in partitioning time when
4 runtime threads are used to perform the dependent partitioning operations.  PENNANT sees an additional 45-74\%
improvement, Circuit gets 60-139\% better.  MiniAero improves by 30-35\%, resulting in an overall 15.1-16.6X
speedup compared to the original partitioning.

The hashed bars stacked on top of each case show the additional runtime cost of verifying the consistency of the computed
partitions.  This cost is comparable to the cost of the partitioning itself, and in current Regent programs, the
user is tempted to disable the checks.  With dependent partitioning, these consistency checks can be described in terms
of the same set of dependent partitioning
operations, and if they must be run, they see similar performance benefits.  For all three of these applications,
the static analysis described in Section~\ref{sec:analysis} validated the assertions using Z3, and the runtime cost
is only shown for comparison purposes.
%In practice, however, they can be eliminated
%entirely and are only shown in this graph for comparison purposes.  For all three of these
%applications, the static analysis described in Section~\ref{sec:analysis} was able to validate the consistency
%assertions for all possible inputs and the runtime cost for consistency checks is nonexistent.  (These proofs were handled
%easily by Z3's existing heuristics.)

%Section~\ref{sec:legion} describes how the Legion runtime implementation is able to operate on program data that
%has been distributed across a cluster, and 
At least as important as the speedups achieved by our dependent partitioning framework are the improvements to
scalability, which significantly improve Regent's ability to partition large problem sizes.
%We next examine the scalability of the dependent partitioning operations
%on larger problem sizes, some of which are too large to even fit on a single compute node.
For these experiments, the number
of pieces into which the data was partitioned was held constant to keep the amount of work stable as the node
count is increased from a single node up to 64 nodes. Our experiments were performed
on a large Infiniband cluster with each node 
containing two six-core Intel Westmere CPUs and 32GB of system memory.
Figure~\ref{fig:scaling} shows the results of our experiments.
The PENNANT and MiniAero applications enjoy excellent strong scaling behavior, with PENNANT obtaining
up to a 29x improvement on 64 nodes.  In contrast, the circuit example receives some initial benefit from going up to
12 or 16 nodes, but beyond that, the communication required to compute a partitioning that involves all-pairs communication
becomes the bottleneck.
%Because the dependent partitioning operations are able to operate on the data wherever the
%application has placed it, a possible optimization in cases like this would be for the application to perform the
%partitioning computation on only a subset of the nodes that it is using.

Our final experiment explores the benefit of the intersection optimization discussed in Section~\ref{sec:legion}.
For each application, a single problem size was selected and scaling experiments were performed, first with the optimization
enabled, and then again with the optimization disabled.  For this case, the number of partitions into which the data is being
divided is chosen to be 8 times the node count, anticipating the need to expose some parallelism within a node as well as between
the nodes.  The total cost of partitioning therefore increases with increasing node count.  Despite this,
the results (Figure~\ref{fig:isectopt}) show the nearest-neighbor structure of the PENNANT and MiniAero meshes allows improvements
in the time taken to compute a partitioning of their data, as long as the intersection optimization is enabled.  Without the
optimization, not only does the total work increase for all node counts, it now grows quadratically for all cases, causing erratic but
generally increasing partitioning times for all applications as the node count increases.
