\section{Introduction}
\label{sec:intro}

%To achieve high performance on modern architectures, an application
%must identify sufficient parallel work to keep multiple processors busy and 
%minimize the cost of data movement by judiciously placing
%application data within complex and distributed memory
%hierarchies.
%The partitioning of an application's data is essential to both of these
%efforts, and presents a challenge in the design of high-performance
%programming models.  On the one hand, it is desirable to have an expressive
%partitioning system that allows applications to describe precisely what data
%is needed where.  On the other, it is desirable to have partitioning constructs
%for which analysis (whether at compile- or run-time) is tractable.
%A motivating example for the usefulness of analysis is given below.  Even apparently
%simple partitioning systems may have exponential or even undecidable analysis problems,
%so achieving tractability necessarily requires eliminating a great deal of expressivity.

The partitioning of the data used by a parallel application is critical for both performance
and scalability.  Independent subsets of data can be processed concurrently and the right
placement of data within the memory hierarchy minimizes data movement.
Every general-purpose parallel programming model must confront the tension
%The choice of data partitioning scheme has profound implications for the performance of
%a parallel application, dictating how much parallelism is exposed as well as constraining
%how the application data can be distributed across a complicated memory hierarchy.
%A parallel programming model must provide abstractions to support partitioning, and tension
between a programmer's desire for expressivity (i.e. the ability to describe exactly the best
partitioning for a given application) and the compiler and runtime's need for tractability (i.e. the ability to
perform analysis and optimizations).
%% SJT: I want to fit this paragraph in here somewhere to make it clear early on why
%% we're not the same as graph analysis frameworks
\footnote{Domain-specific programming models are often able to
compute adequate partitions themselves (e.g. for computations on graphs\cite{Malewicz10,Salihoglu13}),
but acceptable solutions for arbitrary data structures and arbitrary computations over them
are elusive.}

As an example, consider a distributed computation on a graph-based data structure, 
such as the simulation of
an electric circuit.  On today's supercomputers, such computations are generally limited
by data movement over the network between the systems in the cluster.  The resulting
importance of the distribution of data
(the nodes and edges of the circuit's graph) to different processes ({\em ranks})
is such that it is worth the expense of computing a near-optimal partitioning of the data
with a graph partitioning tool such as PARMETIS\cite{Karypis99} before running a long
simulation.
(The hashing or greedy clustering approaches used by graph processing frameworks such as
Pregel\cite{Malewicz10} and PowerGraph\cite{Gonzalez12} focus instead on computing ``good enough''
partitions quickly.)

In addition to an optimal assignment of graph nodes to ``owner'' ranks, data movement is
further minimized by computing a partition of the circuit graph's edges, as well as computing
whether nodes are ``private'' (i.e. only accessed by the owner rank) or ``shared'', and which nodes
are ``ghost'' nodes for each rank (i.e. accessed by that rank but owned by another).
Figure~\ref{fig:graph_partition} summarizes the various partitions used for this computation.
These additional partitions may not be chosen
freely, because the application code will in general be written with assumptions about how
the rest of the data is organized relative to the original partition of the nodes.
For example, a step
in the application that iterates over the edges owned by a rank might require that all the nodes
on which these edges are incident be in the private, shared, or ghost partitions for that rank.

\begin{figure*}
  \centering
  \includegraphics[width=0.70\textwidth]{figs/circuit_deppart.pdf}
%%  \vspace*{-4mm}
  \caption{Partitioning of a graph-based computation.  The {\em independent} partition is in the center,
    and four {\em dependent} partitions are derived from it. \label{fig:graph_partition}}
%%  \vspace*{-6mm}
\end{figure*}

Existing programming models generally provide abstractions for partitioning that are at 
one of the two extremes on the tractability vs. expressivity spectrum, both of which have
significant drawbacks.  One programming model that favors tractability is Chapel\cite{Chamberlain07},
which provides a small set of primitive {\em domain maps} such as
{\em blocked}, {\em cyclic}, or {\em replicated} distributions.  These domain maps concisely describe
common data distribution patterns for structured data (e.g. computations on dense arrays or grids).
The primitives are easy for the
programmer to specify, and the Chapel compiler and runtime are able to efficiently analyze
how two or more of these domain maps interact.  However, the expressivity of such a system
is limited.  For computations on irregular data structures such as the circuit graph above, the
nodes and edges must be manually ordered into arrays (which often requires the duplication of
ghost nodes) so that they may then be distributed using
the available domain map primitives.
The programmer is solely responsible for maintaining and testing this mapping --- neither
the Chapel compiler nor the runtime can help in guaranteeing correctness.

In contrast, the Legion\cite{Legion12} programming model
allows a {\em logical region} to be partitioned into any computable subsets,
thereby maximizing the programmer's ability to express partitioning schemes. 
This generality is achieved by allowing arbitrary code (including external libraries) to be used
for partitioning, with only the results of the execution of the code being provided to
the Legion runtime as a {\em coloring}.  The Legion runtime is able to perform some consistency checks on
the resulting partitions, but only at runtime --- the use of arbitrary code to compute
partitions rules out most attempts at static analysis.  Further, the use of colorings for even
simple partitions prevents the Legion runtime from optimizing the computation or distributing it across
multiple ranks, limiting the size of problem that can be tackled.

Our {\em dependent partitioning} framework
is a new, middle ground between these two extremes.  We split
the partitions used by an application into two types.  {\em Independent partitions} are those that
are computed from ``scratch'' (e.g. the central node ownership map in Figure~\ref{fig:graph_partition}) and
preserve the expressivity benefit of a Legion-like approach, using a {\em filtering} operation that is similar
to Legion's colorings,
but can work on distributed data sets, allowing the partitioning performance to scale with the available
computing resources (yielding up to a 29X speedup in our experiments
for partitioning operations on 64 nodes compared to a single node) and enabling the
partitioning of data sets that cannot be fit onto a single node.

{\em Dependent partitions} are the second type, and are those derived from
independent partitions and/or other dependent partitions (e.g. the four smaller partitions in
Figure~\ref{fig:graph_partition}).  The computation of these is based on a small set
of {\em dependent partitioning operations} that are similar in style to Chapel's domain maps.  They are
easy to use, resulting in dramatic (86-96\%) reductions in the amount of application code needed to
compute dependent partitions and can be optimized by the runtime, resulting in 2.6-12.7X speedups of
these partitioning operations on a single thread.  Dependent partitioning operations
(described in more detail in Section~\ref{sec:dpl}) are based on
set operations and reachability via pointers within the application data, and are carefully chosen to
provide the tools needed by these applications while preserving the ability to perform static analysis,
allowing most consistency checks between the various partitions to be discharged during compilation.

\input{dpl_grammar}

We have designed the dependent partitioning framework to be general enough to be implemented in
any programming model that includes the concepts of stable ``names'' for objects (e.g. pointers), sets and
subsets of those names, and fields defined for each object in a set.
We evaluate our framework with an implementation in the Regent language\cite{Regent15} and the
Legion runtime\cite{Legion12}, and borrow their terminology in many cases, but 
a natural mapping exists as well to Chapel {\em domains} and {\em arrays} defined over those domains.
%includes the concepts of stable ``names'' (e.g. pointers or array indices) for objects, sets and subsets of those
%names (e.g. {\em logical regions} in Legion or {\em domains} in Chapel), and fields defined for each object
%in a set (e.g. Legion {\em fields} or Chapel {\em arrays}).

The rest of this paper is organized as follows.
The majority of the paper focuses on developing concepts and algorithms for a data partitioning sublanguage.
We begin by describing an abstract Dependent Partitioning Language (DPL) in Section~\ref{sec:dpl} and
give several examples to demonstrate its expressivity.  Section~\ref{sec:analysis} covers the static analysis 
required to discharge most consistency checks at compile time and discusses when and why completeness is sacrificed.
The final parts of the paper describe the integration of DPL into a full programming language for parallel computation.
We describe the changes made to the Regent compiler (Section~\ref{sec:regent}) and the Legion
runtime (Section~\ref{sec:legion}) to implement the DPL framework.  Section~\ref{sec:results} provides our experimental
results 
%showing an 86-96\% reduction in the lines of application code required for computing partitions, a
%2.6-12.7X performance improvement in computing partitions on a single node and up to a further 29x improvement when
%distributing the computation across 64 nodes.  Finally, 
and Section~\ref{sec:related} places our effort in the context of
related work.


%% computation on an element that requires accessing related elements (in the same or another data structure)
%% will require that those related elements be in accessible (and ideally nearby) memory locations.
%% operations 
%% any computation that involves more than one data
%% structure

%% ...

%% talk about common pattern of one {\em independent partition} and multiple
%% {\em dependent partitions} - this where the whole idea of "consistency checks"
%% comes from - argue this occurs for both unstructured and structured cases
%% REDO GRAPH EXAMPLE

%% talk about how (expressive) partitioning info itself can be large and must
%% be distributed - desirable to fit within overall data model to control
%% how dependent partitioning operations are distributed

%% mention challenge of mutability of topology, benefits of using local
%% immutability

%% Our contributions are:

%% \begin{itemize}

%% \item A language-independent framework for dependent partitioning that allows
%% maximum expressibility of independent partitions as well as decidable analysis
%% of any dependent partitions.
%% \item A simple Data Partitioning Language (DPL) and type system, demonstrating
%% the soundness of the dependent partitioning framework.
%% \item An implementation in Regent, resulting in XX\% reductions in the code
%% needed to describe dependent partitions, the ability to parallelize the 
%% partitioning operation across multiple cores or nodes, and the elimination of
%% the expensive runtime checks currently needed to verify consistency.

%% \end{itemize}
