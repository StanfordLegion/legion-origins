\section{Dependent Partitioning Language}
\label{sec:dpl}

Figure~\ref{fig:grammar} defines the syntax of an abstract Dependent Partitioning Language (DPL) that we
use to describe our framework.
DPL is a sublanguage that includes only the features needed for creating,
manipulating and automatically reasoning about data partitions.   Any
full programming language incorporating DPL inherits DPL's capabilities
but would also need additional standard constructs (e.g., while loops,
conditionals, etc.) to carry out computations on the data itself.
%Rather than being a full programming language, DPL is a sublanguage for data
%partitioning
%that abstracts away the ``real'' computation of an application but contains
%all the features needed for automated reasoning about properties of data partitions.  Thus, any
%programming language that incorporates DPL also inherits the ability to reason about the correctness of
%partitioning.
%DPL is not suitable for writing full applications because it abstracts out all
%the ``real'' computation.  Instead, applications written in any language that has been augmented with our
%dependent partitioning framework can be reduced to a DPL program and all reasoning about the correctness of
%the partitioning in the original program can be performed on the DPL equivalent.

DPL is an imperative language in which {\em index spaces} (i.e. sets of indices) are computed and
tested against constraints.
Program data in DPL is stored in {\em fields}, which associate a value of a specified
{\em range type} with each index in the {\em domain} index space.  The range type may be either a base type or
another index space --- in the latter case it may be {\em null-extended} to allow null pointers (which are not a
part of any index space) to be stored.  The value associated with an index in a field may change
during a program's execution, but at any given point in time a field represents a
mathematical function from its index space to the values associated with each index.  Dually, we can view
an index space as a collection of objects of a given type, fields as member variables of the type, and an
index $i$ as the identity of an object
whose field {\em i}$\rightarrow${\em f} is stored in $f[i]$.
%This duality between a field lookup of an object and a function application is used
%heavily in the discussion to follow.

The actual contents of any field are unimportant in DPL, as we wish the consistency checks to be valid for all possible
inputs.
To simplify the presentation, we omit constructs for updating fields and instead focus on scopes in which
%As such, there are no constructs in the language to describe how fields are changed.  Instead, DPL
% focuses on scopes in which
one or more fields are {\em immutable}, in which case multiple computations based on those fields' contents
are guaranteed to have used the same values.  Outside any of its immutable scopes, a field is
conservatively assumed to change arbitrarily between every reference.

An implementation of the circuit partitioning example from Section~\ref{sec:intro} is given in Figure~\ref{fig:circuit_dpl}.
%Figure~\ref{fig:circuit_dpl} shows the circuit partitioning example from Section~\ref{sec:intro}.
The code defines an assignment of circuit nodes to subcircuits and then uses the graph topology to define
the necessary subsets of nodes or wires (i.e. edges) and verify their internal consistency.
%Although the key parts are
%the partitioning operations (lines 15-21) and consistency checks (lines 23-25), we must first describe
%the basic building blocks of a DPL program.
The initial {\tt nodes} and {\tt wires} index spaces are declared (lines 1-2) using the
opaque {\tt ptr} base type.
%, but index spaces may be declared that use machine integers of various sizes
%or tuples thereof (e.g. {\tt int64} or {\tt <int32, int32>}).
Index space declarations are lexically
scoped and once declared, an index space is immutable.
%Application data is captured in DPL as {\em fields}.  A field stores a value of a particular {\em range} type
%for each index in the
%specified {\em domain} index space.
%The domain of this function is always an index space, while the range may be either another
%index space\ednote{describe null-extension here?} or a base type (base types include machine integers as well
%as floating-point and booleans) or tuple of types.
Lines 3-5 declare the fields we need for this example:
an integer {\tt subcircuit\_id} for each node and two node pointers 
({\tt in\_node} and {\tt out\_node}) for each wire. Note that fields are
given function types {\em indexspace $\rightarrow$ range}
%for these fields reflect their functional duality referenced earlier
(the + qualifier permits null values).

DPL also includes scalar values, which capture runtime constants (line 7).  A limited form of scalar
variable is available for iteration over the indices in an index space (line 14), but a loop variable
may not be modified in the loop body.
%All application-mutable data is captured in fields.

\begin{figure}[h]
%%\vspace*{-3mm}
\begin{lstlisting}[language=DPL]
idx nodes = ispace(ptr);
idx wires = ispace(ptr);
field subcircuit_id : nodes -> int32;
field in_node : wires -> nodes+;
field out_node : wires -> nodes+;

val N : int;
idx partitions = ispace(int, 0, N);

immutable subcircuit_id, in_node, out_node {
  for p in partitions {
    idx owned_nodes = nodes{ n | n->subcircuit_id = p };
    idx owned_wires = owned_nodes <- in_node;
    idx cross_wires = wires{ w | w->in_node->subcircuit_id !=
                                                   w->out_node->subcircuit_id };
    idx all_shared = cross_wires -> out_node;
    idx my_private = owned_nodes - all_shared;
    idx my_shared = owned_nodes & all_shared;
    idx my_ghost = (owned_wires -> out_node) - owned_nodes;

    assert (owned_wires -> in_node) <= (my_private | my_shared);
    assert (owned_wires -> out_node) <= (my_private | my_shared | my_ghost);
    assert my_private * my_shared;
  }
}
\end{lstlisting}
%%\vspace*{-5mm}
\caption{Circuit partitioning in DPL \label{fig:circuit_dpl}}
%%\vspace*{-5mm}
\end{figure}

%There is no mechanism in DPL to describe the concrete value of a field, as we want analyses
%to be valid for all possible contents, but it is important to be able to describe when field values
%may change.  The {\tt update} keyword used on lines 9-11 describes a (potential) change to some or all of a field's
%contents.  An index space is specified, and the field's old function value is replaced by a new
%function that may take arbitrary values within the specified domain, but must agree with
%the old function outside of that domain.  This allows DPL to capture applications that perform
%data-dependent partitioning, either at initial startup or in the middle of execution.

%In addition to describing when field contents are changed, it is necessary to capture when they
%may not change.  The {\tt immutable} keyword defines
%a lexical scope in which the contents of one or more fields may not change.  As with {\tt update},
%the domain of the field that is made immutable may be restricted to a subset of the field's full
%domain.  Although it is possible to perform partitioning operations using mutable fields, the
%consistency guarantees are quite weak if the field might be changed immediately after.  In this case,
%our partitioning operations contain multiple uses of our three fields and the {\tt immutable} block
%that starts on line 13 is what guarantees that these multiple uses see the same field values.

%In our example here, one could imagine inferring the {\tt immutable} block based on the lack of
%{\tt updates} after line 11.  In addition to the simplicity of making it explicit, there are more
%complicated cases in which some part of a field is updated while another is held immutable,
%or where updates are potentially occurring in other threads of execution.

Line 9 declares the fields locally immutable so that we can compute index spaces from them (lines 11-18) and
check their consistency (lines 20-22).
Line 11 uses the (opaque) contents of the {\tt subcircuit\_id}
field to {\em filter} the original {\tt nodes} index space down to the subset of nodes whose subcircuit
ID match the loop variable {\tt i}.  As mentioned above, $\texttt{n}\rightarrow\texttt{subcircuit\_id}$ can be
thought of as a field lookup of node {\tt n}, or dually, as a function application
$\texttt{subcircuit\_id}(\texttt{n})$.  Filter operations use a syntax similar to set comprehensions,
with the domain of the variable being limited to the index space to which the filter operation is being
applied (i.e. {\tt nodes} in this case).  A filter operation may be classified as {\em simple} or
{\em complex} based on the predicate used.  A simple filter's predicate may either test the %comprehension
variable directly against a constant or use the variable to perform a field lookup and test that value
against a constant.
%The test
%used in line 11 is for equality, but inequality as well as less-than/greater-than may be used on integer
%or floating-point values.
%\footnote{Comparisons of machine floating-point values are easily described in
%terms of comparisons on their bit strings interpreted as integers.}

Filtering is used to describe the independent partitions used in an application.  The contents of the
{\tt subcircuit\_id} field may be read from an input file or they may be the result of an
online graph clustering algorithm.  DPL places no constraints on the form or result of that computation --- it
simply captures how index spaces are derived from it.

Line 12 shows the use of arbitrary application data to derive a dependent partition.
%Line 12 gives an example of the other way in which the results of an arbitrary computation can be used: to
%compute a dependent partition.  In particular, we want every wire to belong to the same subcircuit as its {\tt in\_node}.
We want every wire to belong to the same subcircuit as its {\tt in\_node}, and
the expression {\tt owned\_nodes}$\leftarrow${\tt in\_node} computes the set of edges $e$ where
$\texttt{in\_node}(e) \in \texttt{owned\_nodes}$.  Put another way, it is the {\em preimage} of {\tt owned\_node}
under the function {\tt in\_node}.
We use preimage (and image) in the standard mathematical sense: for any function $f : X \rightarrow Y$,
we define the image $f^\rightarrow : 2^X \rightarrow 2^Y$ such that $f^\rightarrow(S) = \left\{ f(x)\ \middle|\ x \in S \right\}$ and 
the preimage $f^\leftarrow : 2^Y \rightarrow 2^X$ such that $f^\leftarrow(S) = \left\{ x\ \middle|\ f(x) \in S \right\}$.
Although it remains uninterpreted, the knowledge that the current value of {\tt in\_node}
is a mathematical function allows DPL to learn constraints on how {\tt owned\_nodes} and {\tt owned\_wires}
may relate.

Line 13 uses another filter operation to compute the set of wires that cross between subcircuits, but
uses a {\em complex filter}.  The predicate for a complex filter may include
multiple field lookups on either side of the (in)equality.  Although these
complex predicates are critical for expressing partitions in many cases, they can be problematic for 
static analysis, as we discuss in Section~\ref{sec:analysis}.

The counterpart to line 11's preimage operation is the {\em image} operation on line 15 in which we again
treat a field ({\tt out\_node} this time) as a function and compute the image of the {\tt cross\_wires}
subset of the {\tt all\_wires} domain to determine the set of all nodes that are shared (i.e. accessed
by another subcircuit).

The final way in which index spaces can be derived from others is by standard set union ({\tt |}), 
intersection ({\tt \&}), and difference ({\tt -}) operations.  Lines 16 and 17 split
the {\tt owned\_nodes} into the {\tt my\_private} and {\tt my\_shared} subsets based on whether
any other subcircuit will access them.
The ghost nodes for a given subcircuit can be determined by computing the image of just its
{\tt owned\_wires} through the {\tt out\_node} field and subtracting out any nodes that are owned.
There is no need to do the same for the {\tt in\_node} references, but we will test that assertion
shortly.

The consistency requirements between different partitions are expressed using the {\tt assert} keyword,
which can be used to confirm the containment of one index space in another ({\tt <=}) or the disjointness
of two index spaces ({\tt *}).  Requirements are often expressed
by computing additional dependent partitions and comparing them instead.  Lines 20 and 21 use the image
operation again to compute the nodes that can be directly reached from a wire in {\tt owned\_wires}
and demand that they fall into that subcircuit's private, shared, or ghost nodes.  (The omission of 
{\tt my\_ghost} on line 20 allows us to verify our claim above that the image through {\tt in\_node} was
unnecessary.)
This example's assertions are easily verified by hand, but an automated approach is obviously
desirable and is discussed in Section~\ref{sec:analysis}.

%For this example, it is easy to verify the assertions by hand, but a more automated approach is obviously
%desirable for sophisticated applications and is discussed in Section~\ref{sec:analysis}.

\subsection{Integer Arithmetic}

The circuit example uses unstructured data, but it is equally important to capture
structured (and semi-structured) cases in which partitions are defined in whole or in part by operations
on the indices themselves.  An example of this is red-black Gauss-Seidel iteration, in which the convergence
of an iterative algorithm can be improved by separating elements into two sets (red and black) and alternately
updating one based on the other.  Figure~\ref{fig:redblack_dpl} shows a DPL program that implements this
partitioning.

The operations performed on indices are generally pure functions involving fairly simple arithmetic
but commonly require the ability to force computed values into an interval, either by clamping the out-of-range
values or by wrapping them around to the other end of the interval.  Adding a whole sub-language for these
index-based
computations would complicate DPL considerably.  Instead, DPL treats them very similarly to fields (i.e.
as uninterpreted functions), but allows {\em properties} to be provided that can partially (or fully)
specify the behavior of the function.  The actual function is used at runtime to compute images and preimages,
but DPL's static analysis is based entirely on the properties, and any verification results from that
analysis will hold for any variant of the function that still satisfies the given properties.
%(The static analysis required to soundly extract these properties
%from the actual function definition is left to the implementer of this framework in a given target
%language.)

The restrictions on what a property may describe are similar to those of filter predicates.  They may
refer to the inputs and outputs of a single function and any scalar values or loop variables in scope.
They may perform comparisons of integer and floating-point values.  They may also include linear integer
arithmetic: addition and subtraction, as well as multiplication, division and remainder by integer
constants.  Examples can be seen on lines 4, 7, 11, and 14 of Figure~\ref{fig:redblack_dpl}.
%This fragment is
%sometimes referred to as Presburger arithmetic with integer divisibility (PAID).\cite{Brillout11}

Once declared, a function may be used in place of a field in any operation.
%filter, image, or preimage operations.
A simple filter may contain a single function evaluation instead of a field lookup, but any
combination of the two results in a complex filter operation.

\begin{figure}
\begin{lstlisting}[language=DPL]
val N : int;
idx elems = ispace(int32, 0, N);
function left : int32 -> int32; -- subtract 1 and clamp to 0
property (left(x) >= 0) && (left(x) >= (x-1)) && (left(x) <= x);

function right : int32 -> int32; -- add 1 and clamp to N-1
property (right(x) < N) && (right(x) <= (x+1)) && (right(x) >= x);

val B : int;
function blockid : elems -> int;
property blockid(x) = x / (2*B);

function isred : elems -> int;
property isred(x) = (((x / B) % 2) = 0);

for i in ispace(int, 0, N/(2*B)) {
  idx red_update_i = elems{ x | x->blockid = i && x->isred };
  idx red_access_i = red_update_i | red_update_i->left | red_update_i->right;

  for j in ispace(int, 0, N/(2*B)) {
    idx red_update_j = elems{ x | x->blockid = j && x->isred };
    idx red_access_j = red_update_j | red_update_j->left | red_update_j->right;

    assert (i != j) && (B > 0) => (red_access_i * red_update_j);
  }
}
\end{lstlisting}
%%\vspace{-4mm}
\caption{Red-black Gauss-Seidel partitioning in DPL \label{fig:redblack_dpl}}
%%\vspace{-6mm}
\end{figure}

Once these functions and their properties are defined, the set of elements that will be updated as part
of a ``red'' iteration are computed on lines 17 and 21, and in turn the elements they will access are
computed on lines 18 and 22.  Line 24 asserts that these red blocks can be operated on in parallel and
uses the ability to place conditions on when the assertion must hold.

\subsection{Limits on Expressiveness}
\label{subsec:limits}

When designing a language with static analysis in mind,
% deciding what to include is often less important than
deciding what to exclude is important.  We discuss a few conscious omissions: the immutability of 
index spaces, the limitation to linear integer arithmetic, and the question of transitive reachability.

Although DPL must necessarily include loops to describe interesting partitions and applications with
iterative computations, the immutability and lexical scoping of index spaces ensure that the fragment
of a DPL program that feeds into a given partitioning operation or consistency check is loop-free.
Our analysis in the next section depends on this loop-freedom.
A given iteration of a loop may not refer to index spaces computed in the previous iteration.
Information may still flow from previous iterations (e.g. for incremental repartitioning), 
but it must flow through fields.
%Fields may incorporate the results of previous iterations of loops, but the only index spaces available are
%those from the current iteration of the loops in which the operation lies, and a total ordering exists
%on the definitions of the index spaces that is critical to the efficient implentation of the analysis in
%the next section.

Second, the inability to express properties of functions that perform general integer multiplication or
any kind of arithmetic on floating-point values is again necessary for tractable
analysis.  We also argue that such operations rarely appear in partitioning-related computations.
In particular, the linear integer arithmetic contained in DPL is sufficient to precisely describe
the standard domain maps in Chapel.\ednote{list others like UPC here?}

Finally, image and preimage operations compute reachability, but only allow a fixed number of steps that can be
statically determined.
Although transitive reachability (i.e. a fixed-point operator) is a very desirable property to describe,
static analysis of it is
intractable in most cases.  Worse, the time complexity of a generic dynamic analysis used as a fallback
is quadratic in the size of the index space.
%index space of size $N$ is $O(N^2)$ in the worst case.
By excluding transitive reachability from DPL, the programmer is left with two choices:
\begin{enumerate}
\item Performing the fixed-point operation manually and supplying the result in a field, or
\item Finding an equivalent way to express the desired constraint using the available
partitioning operations. % without using fixed points.
\end{enumerate}

\begin{figure}
\centering
\scalebox{0.75}{
\begin{tikzpicture}
\coordinate (x1) at (3,1.3);
\coordinate (x2) at (0,2.8);
\coordinate (x3) at (-2.3,1.3);
\coordinate (x4) at (-1.7,-2.6);
\coordinate (x5) at (1,-2.0);
\coordinate (s1) at ($ 0.3333*(x1) + 0.3333*(x2) $);
\coordinate (s2) at ($ 0.3333*(x2) + 0.3333*(x3) $);
\coordinate (s3) at ($ 0.3333*(x3) + 0.3333*(x4) $);
\coordinate (s4) at ($ 0.3333*(x4) + 0.3333*(x5) $);
\coordinate (s5) at ($ 0.3333*(x5) + 0.3333*(x1) $);
\draw[very thick,black!30] (0,0) -- ($ (x1) $);
\draw[very thick,black!30] (0,0) -- ($ (x2) $);
\draw[very thick,black!30] (0,0) -- ($ (x3) $);
\draw[very thick,black!30] (0,0) -- ($ (x4) $);
\draw[very thick,black!30] (0,0) -- ($ (x5) $);
\draw[thick,dashed] ($ (x1) $) -- ($ (x1) + (0.3,0.3) $);
\draw[thick,dashed] ($ (x2) $) -- ($ (x2) + (0.05,0.3) $);
\draw[thick,dashed] ($ (x3) $) -- ($ (x3) + (-0.3,0.3) $);
\draw[thick,dashed] ($ (x4) $) -- ($ (x4) + (-0.3,-0.15) $);
\draw[thick,dashed] ($ (x5) $) -- ($ (x5) + (0.05,-0.3) $);
\draw[very thick] ($ (x1) $) -- ($ (x2) $) -- ($ (x3) $) -- ($ (x4) $) -- ($ (x5) $) -- ($ (x1) $);
\node[label=45:{\small $z_0$},shape=circle,fill=black] (z0) at (0,0) {};
\node[label=30:{\small $s_1$},shape=circle,fill=black] (c1) at ($ (s1) $) {};
\node[label=80:{\small $s_2$},shape=circle,fill=black] (c2) at ($ (s2) $) {};
\node[label=130:{\small $s_3$},shape=circle,fill=black] (c3) at ($ (s3) $) {};
\node[label=300:{\small $s_4$},shape=circle,fill=black] (c4) at ($ (s4) $) {};
\node[label=30:{\small $s_5$},shape=circle,fill=black] (c5) at ($ (s5) $) {};
%\node[label={\small $s_1$},shape=circle,fill=black] (c2) at ($ (s1) $) {};
\draw[thick,blue,->] (c1) to [bend right=20] (z0);
\draw[thick,blue,->] (c2) to [bend right=20] (z0);
\draw[thick,blue,->] (c3) to [bend right=20] (z0);
\draw[thick,blue,->] (c4) to [bend right=20] (z0);
\draw[thick,blue,->] (c5) to [bend right=20] (z0);
\draw[thick,red,->] (c1) to [bend right=20] (c2);
\draw[thick,red,->] (c2) to [bend right=20] (c3);
\draw[thick,red,->] (c3) to [bend right=20] (c4);
\draw[thick,red,->] (c4) to [bend right=20] (c5);
\draw[thick,red,->] (c5) to [bend right=20] (c1);
%\draw[fill=black] (0,0) circle [radius=0.1];
\draw[fill=black] ($ (s1) $) circle [radius=0.1];
%\draw[fill=black] ($ (x1) $) circle [radius=0.1];
\end{tikzpicture}
}
%%\vspace*{-3mm}
\caption{PENNANT mesh structure \label{fig:pennant_mesh}}
%%\vspace*{-6mm}
\end{figure}

An example of the latter situation can be found in the PENNANT\cite{PENNANT} application, which simulates
hydrodynamics in an irregular mesh and is used in our evaluation in Section~\ref{sec:results}.  A {\em zone} in a PENNANT mesh
is an arbitrary polygon which is divided into triangular {\em sides} (see Figure~\ref{fig:pennant_mesh}).
Each side has a pointer to the
zone of which it is a part and also to the next side.  In a distributed computation, the zones are 
partitioned based on their {\tt submesh\_id} and then the corresponding partition of the sides is a 
preimage through the {\tt mapsz} (``map sides to zones'') field.  This trivially satisfies the first
consistency requirement on line 15 of Figure~\ref{fig:pennant_dpl}.

The application also depends
on being able to walk the sides ``around'' a zone (line 16), which appears to necessitate a fixed-point
operation.  In this case (and in others like it), the key observation is that while walking all the sides
reachable from a given side requires a fixed point, determining whether two adjacent sides are {\em not} in
the same zone does not.  The
combination of a complex filter (line 7) to
identify malformed sides, an image to compute the corresponding malformed zones (line 8), and then a preimage to
share that information with the other sides of the same zones (line 9) provides the equivalent of the
fixed-point operator, but is still statically analyzable and has only linear runtime cost.

\begin{figure}
\begin{lstlisting}[language=DPL]
idx zones = ispace(ptr);
idx sides = ispace(ptr);
field submesh_id : zones -> int;
field mapsz : sides -> zones+;
field mapss3 : sides -> sides+;

idx bad_sides = sides{ s | s->mapsz != s->mapss3->mapsz };
idx bad_zones = bad_sides -> mapsz;
idx any_bad_sides = bad_zones <- mapsz;

for i in ispace(0, N) {
  idx my_zones = zones{ z | z->submesh_id = i };
  idx my_sides = (my_zones <- mapsz) - any_bad_sides;

  assert my_sides->mapsz <= my_zones;
  assert my_sides->mapss3 <= my_sides;
}
\end{lstlisting}
%%\vspace*{-4mm}
\caption{PENNANT partitioning in DPL \label{fig:pennant_dpl}}
%%\vspace*{-6mm}
\end{figure}

Given these exclusions in the pursuit of static analyzability, the reader may question the inclusion of
the complex filter operations.
%, which are similar to these excluded features in their ability to cause problems for static analysis.
Complex filter operations are distinguished from the omitted features
above in three critical ways:
\begin{enumerate}
\item Pragmatically, their exclusion would result in DPL being too weak to describe both the circuit
and PENNANT examples, and likely many other common patterns.
\item Several heuristics (described in Section~\ref{subsec:complex}) are available to eliminate such
expressions in many cases.
%Heuristics based on the structure of the filter expression allows SMT solvers to
%eliminate such expressions in many cases.
\item When the static analysis is unable to yield a result, the corresponding dynamic analysis has
both time and space complexity that is linear in the index space sizes --- no worse than any of the
other partitioning operations.  (This is due to the restriction that even complex filter operations must
be quantifier-free.)
\end{enumerate}

