#!/usr/bin/env perl -w

@lines = <STDIN>;

while(@lines) {
    my $l = shift @lines;
    if($l =~ /^[^\%]*\\begin\{lstlisting\}/) {
	print $l;
	my $x = '';
	while($lines[0] !~ /^[^\%]*\\end\{lstlisting\}/) {
	    $x .= shift @lines;
	}
	while(1) {
	    $x =~ s/(\\DIFaddbeginFL.+?)\\DIFaddFL\{(.+?)\s*\}(.*?\\DIFaddendFL)/$1|+$2+|$3/ && next;
	    $x =~ s/(\\DIFdelbeginFL.+?)\\DIFdelFL\{(.+?)\s*\}(.*?\\DIFdelendFL)/$1|-$2-|$3/ && next;
	    $x =~ s/\\DIFaddbeginFL.(.+?)\\DIFaddendFL/$1/ && next;
	    $x =~ s/\\DIFdelbeginFL.(.+?)\\DIFdelendFL/$1/ && next;
	    last;
	}
	print $x;
    } else {
	print $l;
    }
}
