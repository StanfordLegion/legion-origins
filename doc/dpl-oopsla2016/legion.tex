\section{Dependent Partitioning in Legion}
\label{sec:legion}

The output of the Regent compiler is C++ code that uses the Legion runtime API.  Applications can
also be written directly for the Legion runtime API (although without the benefits of the Regent
compiler's type checking and static analysis), therefore it is important that the new dependent
partitioning operations are supported by the Legion runtime.  The Legion API was augmented
to include new entry points for each of the operations added to Regent.

%% The Legion runtime is divided into two layers: a higher-level runtime that implements the Legion
%% programming model, and Realm\cite{Realm14}, a
%% lower-level runtime that provides a portable layer for the distributed execution of a directed
%% acyclic {\em operation graph}, where the nodes are operations (computations on regions, copies, or synchronization)
%% and edges represent a partial ordering of execution.
%% The high-level runtime is responsible for translating logical regions and semantically-sequential
%% application code into an operation graph.
%% %parallel tasks using concrete physical instances of those regions, and requesting
%% %any copy operations needed to keep those instances consistent.
%% Although we could have implemented the new dependent partitioning operations as tasks, we wanted
%% to take advantage of the optimization opportunities that arise when multiple images (or preimages) are performed
%% using the same (but likely distributed) field data, and this was more easily achieved by adding new kinds of
%% Realm operations and allowing the Legion runtime to insert them into the appropriate places in the overall
%% operation graph.

Although the effort involved in achieving an efficient and scalable implementation required solving many
issues, we focus on two.  First, we discuss the internal representation of an 
index space in Legion, and second, we cover a key optimization that improves the scalability of image and
preimage operations.

\subsection{Index Space Data Structure}

Since index spaces are the key data type used in partitioning operations, it is important that they be efficient
in both memory usage and query complexity.  The construction cost for an index space is a secondary consideration.
As our framework allows index spaces to be computed from arbitrary data, we must use a data structure that
can describe any set of $N$ indices, which will necessarily require $O(N)$ storage cost and $O(log N)$ query
time. However, we also provide a mechanism for compressing commonly occurring patterns to
improve memory usage and query time.
%However, we wish to take advantage of commonly occurring patterns in index spaces and chose a data
%structure able to compress these patterns and improve memory usage or query time in those cases.

%% For example, a {\em dense} index space is one in which the
%% set of indices can be described by a single interval of the underlying type, yielding constant storage and query time.
%% A {\em mostly dense} index space
%% has an interval that covers the set of indices as well as some indices not in the set, and can use a bit mask
%% to determine membership.  The storage cost becomes linear (although with a small constant), while query time
%% remains constant.  Finally, even when the indices in an index space can't be tightly bounded by a single interval,
%% they will often contain clusters of indices that are either dense or mostly dense.

The index space data structure contains a {\tt bounds} interval that 
gives an upper bound on elements of the index space, a {\tt dense} flag that indicates if 
every index in the {\tt bounds}
interval is included, and a {\tt cluster\_list} stores the intervals containing elements when the {\tt dense} flag is disabled.  Each
list entry includes an optional bitmask to capture non-dense clusters of indices.  The intervals are sorted to
maintain a logarithmic worst-case complexity for queries.

%% The reader will note that this structure is not well suited to alterations in an index space.  Insertion into a sorted
%% array is expensive, and identifying the clusters in an index space incrementally would be very challenging.  For these
%% reasons, Realm uses an alternative data structure during index space computations that is optimized for insertion and
%% delation and then performs a post-processing
%% step once a computation is complete in order to ``bake'' it into the query-friendly form above.

The Legion runtime operates in a distributed memory environment, and index spaces often need to be copied
between nodes to perform partitioning operations.  In some cases, imprecision in the index space
membership is acceptable if it reduces the network traffic.
Upon request, a precise index space is
turned into an {\em approximate index space} that has a configurable maximum size by first omitting all of the
bitmasks for non-dense clusters and then iteratively merging pairs of clusters into single intervals that cover
both.
%mostly dense clusters and then iteratively merging clusters until no more than $K$ remain.  ($K$ is a configurable parameter,
%which defaults to 62, bounding the maximum size of an approximate index space to 256 bytes.)
%The merging of clusters
%is done by selecting two intervals with the fewest indices between them and replacing them with a single interval 
%that covers both of the original ones.
This algorithm guarantees that an approximate index space is an upper bound for
the original index space --- i.e. every index present in the original index space is also present in the approximation.
Approximate index spaces are a crucial component of the implementation of 
several dependent partitioning operations that we discuss below.
%These approximations will be used as part of the scalability optimization below.\ednote{seems like I should have an experiment
%to show the effects of K}

\subsection{Scalability of Image and Preimage Operations}

The Legion runtime allows data for a field to be distributed across multiple {\em region instances},
and the implementation of dependent partitioning operations that use field data.  The internal
interface for computing images is shown here:

%The Realm interface for these dependent partitioning operations differs from the Regent (and Legion) APIs in
%one important way, as it must be told which instance(s) contain which subset(s) of the field data to be used
%for the operation.  A simplified version of the Realm API for computing an image operation is shown here.
%(The actual version is templated to allow for other base index types and to understand instances that contain
%data for multiple fields.)

\begin{lstlisting}[language=C++,aboveskip=2mm]
  Event compute_images(IndexSpace parent,
                       const vector<IndexSpace>& sources,
                       const vector<RegionInstance>& instances,
                       const vector<IndexSpace>& instance_domains,
                       vector<IndexSpace>& images,
                       Event wait_for);
\end{lstlisting}

The {\tt parent} parameter is the index space for the Regent {\tt parent\_region},
%(this Realm interface is actually closer to the DPL version in this respect)
while {\tt sources} contains the index
spaces for each region in the Regent {\tt source\_partition}.  The index spaces that Legion will use to construct
the output partition are filled into the {\tt images} vector - this will have the same length as the {\tt sources}
input vector.  The {\tt wait\_for} precondition and output events
allow the Legion runtime to describe the necessary scheduling dependencies for this operation.
%(i.e. its location in the overall operation graph).
Finally, the {\tt instances} vector provides the list of instances
containing the field data.  Each instance can be thought of as a partial function $f_j$, with the corresponding
{\tt instance\_domain} element giving the subdomain $D_j$ over which each partial function is defined.
%Using $f_j$ for
%each partial function and $D_j$ as its domain, we can write:
%$$f(x) \equiv \begin{cases}
%  f_1(x) & \text{, if } x \in D_1, \\
%  f_2(x) & \text{, if } x \in D_2, \\
%  \ldots
%\end{cases}$$
Using these partial functions, we can write 
the image operation as a union of the image of each partial function (using $P$ for the parent index space,
$S_i$ for a given source index space and $I_i$ for the corresponding image output:

\vspace*{1mm}
{\centering{\small$\displaystyle{I_i = \bigcup_{j} P \cap f_j^\rightarrow(S_i)}$}

}

\vspace*{2mm}
Note that every $S_i$ interacts with every $f_j$ in this operation.  Although the sizes of the {\tt sources}
and {\tt instances} vectors need not match, they will often both be $O(N)$ in practice (where $N$ is the number of
ranks or cores in the machine).  In the worst case (e.g. a completely random graph), this requires $O(N^2)$ work.
However, in most common cases, there will be at least some structure to the data and $f_j^\rightarrow(S_i)$ will be
a nonempty set for at most $M \ll N^2$ pairs.  By using an {\em output-sensitive algorithm}, we can greatly
improve the performance of these common cases.  We first rewrite the above formula to make the known domain of each
partial function explicit and then take advantage of the fact that the image of an empty set is an empty set:

\vspace*{1mm}
{\centering{\small$\displaystyle{I_i = \bigcup_{j,\ D_j \cap S_i \not= \emptyset} P \cap f_j^\rightarrow(D_j \cap S_i)}$}

  }

\vspace*{2mm}
We have reduced the work to require only $O(M)$ image calculations, but we still have $O(N^2)$ intersection tests to
perform.  However, if we can perform these tests in a centralized location, we can use an {\em interval tree} to reduce
the cost to $O(N log N + M)$.  Copying all the $S_i$ and $D_j$ to a single node can be prohibitive if the index spaces
are sparse, so the final improvement is to construct the interval tree and perform the intersection tests using the
approximate index spaces $\widetilde{S_i}$ and $\widetilde{D_j}$.  This approximation is sound as the intersection of
two upper bounds can be empty only if the actual sets are empty.  The precise intersection is still needed within the image,
but is only performed $O(M)$ times, and the complexity for the whole operation is $O(N log N + M)$ as well.

A similar optimization is used for preimages, with the complication that we cannot directly intersect the instance domains
$D_j$ with the targets $T_i$ in:

\vspace*{1mm}
{\centering{\small$\displaystyle{I_i = \bigcup_{j} P \cap f_j^\leftarrow(T_i)}$}

  }

\vspace*{2mm}
Instead, we compute $R_j = f^\rightarrow(P)$ and use the identity:

\vspace*{1mm}
{\centering{\small$\displaystyle{A \cap f^\leftarrow(B) = A \cap f^\leftarrow(f^\rightarrow(A) \cap B)}$}

  }

\vspace*{2mm}
\noindent to yield:

\vspace*{1mm}
{\centering{\small$\displaystyle{I_i = \bigcup_{j} P \cap f_j^\leftarrow(R_j \cap T_i)}$}

}

\vspace*{2mm}
\noindent and again perform approximate intersection tests.  Noting that the intermediate $R_j$'s will be discarded after this operation,
we can save more time and memory by only computing an
{\em approximate image} $\widetilde{R_j} = \widetilde{f_j^\rightarrow}(P)$ in which bitmasks are never generated and intervals
are merged during the computation.  Our final form again requires $O(N log N + M)$ work to compute $N$ preimages:

\vspace*{1mm}
{\centering{\small$\displaystyle{I_i = \bigcup_{j,\ \widetilde{R_j} \cap \widetilde{T_i} \not= \emptyset} P \cap f_j^\leftarrow(\widetilde{R_j} \cap T_i)}$}

  }

