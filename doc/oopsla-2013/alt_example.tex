
\section{Circuit Example}
\label{sec:example}

\lstset{
  captionpos=b,
  language=Haskell,
  basicstyle=\footnotesize,
  columns=fullflexible,
  stepnumber=1,
  escapechar=\#,
  keepspaces=true,
  literate={<}{{$\langle$}}1 {>}{{$\rangle$}}1,
  morekeywords={function,rr,int,float,bool,isnull,partition,as,downregion,upregion,reads,writes,rdwrs,reduces,read,write,reduce,using,unpack,pack,coloring,color,newcolor,atomic,simultaneous},
  deletekeywords={head,min,max}
}
% Pulled line number info out so no line numbers appear on code snippets
%  numbers=left,
%  numberstyle=\tiny,

We begin by introducing an example program motivating the novel
features of Legion.  We use the spartan syntax of Core Legion, defined
formally in Section~\ref{sec:legioncore}, which is designed to
illustrate concepts concisely (see \cite{Legion12} for examples
written in a syntax friendlier to programmers).  For brevity we
present only code fragments; the full example is in
Appendix~\ref{sec:examplefull}.

Consider a simulation of an electrical circuit.
The simulation takes as input an arbitrary graph of circuit elements (wires and nodes where the wires connect)
that have been dynamically allocated in two logical regions, {\tt all\_nodes} 
and {\tt all\_wires}.  The types of nodes and wires are: 
% THE BLANK LINE AT THE START OF THE LISTING SEEMS NECESSARY
%[label={lst:tuples},caption={Tuples and Pointers Example}]
\begin{lstlisting} 
--                        voltage, current, charge, capacitance, piece ID
type CircuitNode        = <float, float, float, float, int>
--                      owned node, owned or ghost node, resistance, current
type CircuitWire<rn,rg>  = <CircuitNode@rn, CircuitNode@(rn,rg), float, float>
\end{lstlisting}

A {\tt CircuitNode} is just a tuple of electrical properties and a
piece ID used for partitioning (explained further below).  The first two fields of a {\tt CircuitWire} are
pointers specifying the wire's {\tt CircuitNode} end points.  Because our system is
region-based, pointer types must state to what region(s) they can point.
The syntax {\tt CircuitNode@rn} indicates the first component of the
tuple is a pointer into the region {\tt rn} and the region list syntax
{\tt CircuitNode@(rn,rg)} indicates the second component is a
pointer to a {\tt CircuitNode} in either region {\tt rn} or {\tt rg}.
These region annotations on the two pointers capture the invariant that  
a wire either connects two nodes in the same region {\tt rn}, or one of the endpoints
is in a ghost region {\tt rg} of nodes bordering {\tt rn}.
The {\tt CircuitWire} type is parametrized (templated) by the region
names {\tt rn} and {\tt rg}; different instances of the type are
formed by substituting region names for the parameters {\tt rn} and {\tt rg}.
All pointer types carry a set of possible target regions and the
correctness of region pointers is statically checked.

The most important decision in any Legion program is how to partition
program data.  An ideal partitioning depends on many factors,
including the shape of data structures, the input, and the desired number of
partitions (which usually varies with the target machine).   In Legion,
partitioning takes place in two steps.  First,  the programmer assigns
a {\em color} to each element of the region to be partitioned. The number
of colors and how they are assigned to elements can be the result of whatever
computation the programmer wishes.  Second, Legion creates new subregions, one
for each color, with each region element assigned to the subregion of the appropriate color.
Thus, the programmer expresses the desired partition of a region, and Legion provides
the mechanism to carry out the programmer's directions.
In this example we invoke METIS\cite{Metis98}, a 
well-known dynamic graph partitioning library.
METIS requires a way to iterate over the graph elements, so it
takes as arguments two lists of the nodes and wires in
the graph.  It records how the graph is to be partitioned by annotating each
{\tt CircuitNode} with a piece ID.
%[label={lst:metis},caption={Partition Computation}]
\begin{lstlisting} 
type NodeList<rl,rn>       = < CircuitNode@rn, NodeList<rl,rn>@rl >
type WireList<rl,rw,rn,rg>= < CircuitWire<rn,rg>@rw, WireList<rl,rw,rn,rg>@rl >
function extern_metis[rl,rn,rw](node_list : NodeList<rl,rn>@rl,
          wires_list : WireList<rl,rw,rn,rn>@rl), reads(rl,rn,rw), writes(rn) : bool
\end{lstlisting}

We require two new types.  A {\tt NodeList} is parametrized on a
region {\tt rl} that holds the spine of the list and a region {\tt rn}
that holds the {\tt CircuitNode} elements of the list.  A {\tt
WireList} is similar, except that two regions {\tt rn} and {\tt rg}
are required for the {\tt CircuitWire} type.

The function {\tt extern\_metis} is also parametrized on the regions it 
accesses: {\tt rn} is a node region, {\tt rw} is a wire region,
and {\tt rl} is a region holding both lists of pointers.\footnote{A detail: Since the graph
is not yet partitioned, both {\tt CircuitNode} arguments to {\tt Wirelist} are {\tt rn}.  The formal parameter {\tt rn}
will hold all the nodes of the graph, thus every wire is guaranteed to have both endpoints in {\tt rn}.}
The function also specifies the privileges it requires for each region.
All three regions are read by {\tt extern\_metis}, but only the {\tt rn}
region is written (since it writes the piece ID). 

%A function can only 
%have privileges that its caller also possesses; this property is verified statically. 
% by the Legion type system.

\begin{figure}[t]
\centering
\subfigure[Node region tree.]{
\label{sfig:part_fig:tree}
\includegraphics[scale=0.50]{figs/OwnedTree}
}
\subfigure[Circuit piece coloring.]{
\label{sfig:part_fig:pieces}
\includegraphics[scale=0.67]{figs/Owned_Nodes}
}
\subfigure[Ghost {\tt rg0} coloring.]{
\label{sfig:part_fig:ghost_zero}
\includegraphics[scale=0.67]{figs/Ghost_RN0}
}
\subfigure[Ghost {\tt rg1} coloring.]{
\label{sfig:part_fig:ghost_one}
\includegraphics[scale=0.67]{figs/Ghost_RN1}
}
\vspace{-2mm} 
\caption{Partitions of $r{\_}all{\_}nodes$. \label{fig:part_fig}}
\vspace{-4mm}
%  \caption[My caption
\end{figure}

METIS does not actually partition the data.  It
decides and records, at runtime, how many graph pieces there will be
and which nodes and wires will go in each piece.  Partitioning itself is a
language primitive in Legion.

Before describing how partitioning works, we need to explain more
about the goals of the circuit simulation.  To efficiently support the
simulation's access patterns, the region {\tt all\_nodes} holding all
the nodes of the graph is partitioned in two different ways.  The
desired {\em region tree} is shown in Figure~\ref{sfig:part_fig:tree}.
First, there are subregions of {\tt all\_nodes} that describe
the set of nodes ``owned'' by each piece, called {\tt rn0}, {\tt rn1},
$\ldots$. Since each node is in one piece, this partition is {\em
disjoint} (indicated by a $*$ on the left subtree).
Figure~\ref{sfig:part_fig:pieces} shows one possible node partition;
different colors identify different piece IDs.  Second, each piece of
the circuit needs access to the ghost nodes on its border, The
ghost nodes for two circuit pieces are shown in
Figures~\ref{sfig:part_fig:ghost_zero}
and~\ref{sfig:part_fig:ghost_one}; note two nodes are in both sets.
Because a node may neighbor more than one other circuit piece, this
second partition of {\tt all\_nodes} is {\em aliased}.  Thus, there
are two sources of aliasing in the region tree: the two distinct
partitions divide the {\tt all\_nodes} region in different ways, and
the ghost node subregions are not disjoint.  

Note there are two alternative approaches to using multiple partitions,
both of which avoid introducing aliasing:
first, we could create a single partition with $2^n$ subregions, one for each
possible case of sharing; second, computations on each piece could use the
{\tt all\_nodes} region to access their ghost nodes.  Neither option
is attractive: the former significantly complicates programming  and the latter
greatly overestimates the required ghost nodes,
increasing runtime data movement as well as limiting parallelism.

Programs express partitions using {\em colorings}, a built-in Legion type.
Colorings are maps from region elements to integers (the ``colors'') identifying which subregion an element
should be in.
The function {\tt create\_piece\_coloring}
constructs a coloring from the piece IDs of a list of nodes.
The coloring of wires is similar, with each wire
using the color of its first endpoint.  
For aliased partitions such as the ghost nodes, a {\em multicoloring}
is used, which assigns each region element one or more colors.  
\footnote{For simplicity, multicolorings are
implemented in the simplified language used in this paper as separate colorings for each color (see Appendix~\ref{sec:examplefull}).}
%[label={lst:coloring},caption={Coloring Construction}]
\begin{lstlisting}  
function create_piece_coloring[rl,rn](node_list: NodeList<rl,rn>@rl),
            reads(rl,rn) : coloring(rn) = 
    if isnull(node_list) then
        newcolor(rn) 
    else                                 -- tuple fields accessed by .(field number)
        let list_elem : NodeList<rl,rn> = read(node_list) in
        let part_coloring : coloring(rn) = create_piece_coloring(list_elem.2) in
        let node : CircuitNode = read(list_elem.1) in
            color(part_coloring, list_elem.1, node.5)
\end{lstlisting}
A fresh coloring for a region is created by the Legion primitive {\tt newcolor},
and colorings are extended by the primitive {\tt color}.  On the last line the call to {\tt color}
extends the current coloring {\tt part\_coloring} by associating the node {\tt list\_elem.1} with the
piece ID {\tt node.5}.  The expression {\tt read(X)} is an explicit dereference of the pointer expression
{\tt X}---again, the language used here is primarily 
for the later formal development and is not the syntax we would expect programmers to use.
Note that colorings are computed dynamically and can be arbitrary.


Colorings are the input to the {\tt partition} operation:  
%[label={lst:partition},caption={Partition Operation Example}]
\begin{lstlisting} 
let owned_map : coloring(all_nodes) = 
                    create_piece_coloring[rl,all_nodes](node_list) in
partition all_nodes using owned_map as rn0,rn1... in ...
\end{lstlisting}
The partition statement partitions the graph into a number of different pieces; each piece ``owns'' a subset of the nodes.
Since the coloring assigns each region element at most one color,
this partition operation results in  disjoint subregions, each a subset of the parent region.
At runtime, the partition operation extends the region tree (recall Figure~\ref{sfig:part_fig:tree}) maintained
by the Legion runtime; this data structure, which includes all the allocated dynamic regions and their parent-child relationships, is used to decide whether computations can run in parallel based on what regions they access and with what privileges.
At static type checking time, a partition operation introduces constraints into the static type environment describing
both the disjointness of subregions (e.g., {\tt rn0 $*$ rn1})
and the subregion relationships
(e.g., {\tt rn0 $\leq$ all\_nodes}).  A partition created from a multicoloring
introduces only the subregion relationships, as the partition may be aliased.
The partitioning of wires is identical to the partitioning of nodes.
The partitioning of the ghost nodes is performed as a separate partitioning operation
for each color (see Appendix~\ref{sec:examplefull}).

For simplicity this example
has only one level of partitioning (although in two different ways).
All the semantic issues that concern the results of this paper can be illustrated
with one level of partitioning.  In general, however, the region tree has any number of levels, as subregions are themselves partitioned,
perhaps also in multiple ways. Typically, the number of levels and size of partitions
depends on both the data and the memory hierarchy of the target machine, as the Legion runtime places
regions in levels of memory where they fit \cite{Legion12}.




%% In addition, to partitioning the graph to describe {\em owned} nodes
%% for each piece, the circuit example also
%% needs to be able to name the {\em ghost nodes} for each piece.  Ghost node
%% regions are used for accessing the nodes of wires that span
%% two different pieces. (This is also the reason {\tt CircuitWire} is parametrized on
%% two regions).  Figure~\ref{sfig:part_fig:ghost_zero} and 
%% Figure~\ref{sfig:part_fig:ghost_one} show colorings for the ghost regions
%% corresponding to {\tt rn0} and {\tt rn1} respectively.  Note that two 
%% nodes are colored in both colorings.  Since nodes can appear in multiple
%% subregions, we use a {\em multicoloring} (implemented in core Legion
%% by multiple single colorings) to describe {\em aliased} partitions.  Aliased
%% partitions still introduce subregion constraints into the static checking
%% environment, but not disjointness constraints.  Figure~\ref{sfig:part_fig:tree}
%% shows the resulting logical region tree for node regions.

Logical regions are first class values and can be written to and read from the heap.  
When a region is written to the heap, it is necessary to remember its subregion and
disjointness relationships to other regions, so that this information can be recovered
when the region is subsequently read from the heap.  A {\em region relationship}
is a bounded existential type that allows programmers to {\em pack} a group of regions and pointers together 
with their subregion and disjointness constraints.
The example region relationship below for a {\tt CircuitPiece} 
involves its region of wires {\tt rpw}, region of 
nodes {\tt rpn}, region of ghost nodes {\tt rg}, and important constraints.  Note that the
names of {\tt rpw}, {\tt rpn}, and {\tt rg} are bound in the region relationship,
and the knowledge that they are subregions of free region names {\tt rw} and {\tt rn} is captured in the constraints.

%-- Keyword for region relationship is rr
%[label={lst:rr},caption={Region Relationship Example}]
\begin{lstlisting}  
-- Keyword for region relationship is rr
type CircuitPiece<rl,rw,rn> = rr[rpw,rpn,rg]    
                            < WireList<rl,rpw,rpn,rg>@rl, NodeList<rl,rpn>@rl >         
                            where rpn #$\le$# rn and rg #$\le$# rn and rpw #$\le$# rw and
                                  rn * rw and rl * rn and rl * rw
\end{lstlisting}

The Legion type system verifies properties of region relationships when packing.  When
a region relationship is unpacked later, fresh names are given to the regions that were
bound by the region relationship and the properties that are known to hold for these
regions are re-introduced in the static type environment.
In contrast, region access privileges cannot be captured in a region
relationship---privileges belong to functions. This is a key requirement for soundness of the Legion type
system that we return to in Section~\ref{sec:legioncore}.  When a function unpacks
a region $r$ from a region relationship, to access  $r$ it must already hold all the needed privileges on some
superset of $r$ (i.e., $r$'s parent or other ancestor region).

\lstset{
  captionpos=b,
  language=Haskell,
  basicstyle=\scriptsize,
  numbers=left,
  numberstyle=\tiny,
  columns=fullflexible,
  stepnumber=1,
  escapechar=\#,
  keepspaces=true,
  belowskip=-10pt,
  literate={<}{{$\langle$}}1 {>}{{$\rangle$}}1,
  morekeywords={function,rr,int,float,bool,isnull,partition,as,downregion,upregion,reads,writes,rdwrs,reduces,read,write,reduce,using,unpack,pack,coloring,color,newcolor,atomic,simultaneous},
  deletekeywords={head,min,max}
}
\begin{lstlisting}[float={t},label={lst:loop},caption={Main Simulation Loop}]
-- Leaf Task Declarations (implementations in appendix)
function calc_new_currents[rl,rw,rn,rg] ( ptr_list : WireList<rl,rw,rn,rg>@rl ), 
      reads(rl,rw,rn,rg), writes(rw) : bool
function distribute_charge[rl,rw,rn,rg] ( ptr_list : WireList<rl,rw,rn,rg>@rl ), 
      reads(rl,rw,rn), reduces(reduce_charge,rn,rg), atomic(rn,rg) : bool 
function update_voltage[rl,rn] ( ptr_list : NodeList<rl,rn>@rl ), 
      reads(rl,rn), writes(rn) : bool

-- Reduction function for distribute charge
function reduce_charge ( node : CircuitNode, current : float ) : CircuitNode
    let new_charge : float = node.3 + current in
        < node.1,new_charge,node.3,node.4>

-- Time Step Loop
function execute_time_steps[rl,rw,rn] ( p0 : CircuitPiece<rl,rw,rn>, 
      p1 : CircuitPiece<rl,rw,rn>, steps : int ) , reads(rn,rw,rl), writes(rn,rw) : bool = 
  if steps #$<$# 1 then true else
  unpack p0 as piece0 : CircuitPiece<rl,rw,rn>[rw0,rn0,rg0] in 
  unpack p1 as piece1 : CircuitPiece<rl,rw,rn>[rw1,rn1,rg1] in
  let _ : bool = calc_new_currents[rl,rw0,rn0,rg0](piece0.1) in
  let _ : bool = calc_new_currents[rl,rw1,rn1,rg1](piece1.1) in
  let _ : bool = distribute_charge[rl,rw0,rn0,rg0](piece0.1) in
  let _ : bool = distribute_charge[rl,rw1,rn1,rg1](piece1.1) in
  let _ : bool = update_voltage[rl,rn0](piece0.2) in
  let _ : bool = update_voltage[rl,rn1](piece1.2) in
      execute_time_steps[rl,rw,rn](p0,p1,steps-1)
\end{lstlisting}

The main simulation loop, shown in Listing~\ref{lst:loop}, runs for
 many time steps, each of which performs three computations: calculate
 new currents, distribute charges, and update voltages on the circuit.
For simplicity, this example is written for a graph that is partitioned into
only two pieces.
For each time step, the loop (tail recursive function {\tt
 execute\_time\_steps}, lines 15-26) unpacks the two previously packed
 circuit pieces, giving new names to the subregions introduced by each
 region relationship.  The {\tt execute\_time\_steps} function will
 have read/write privileges for the newly named regions, such as {\tt
 rn0}, because it has read/write privileges for {\tt rn} and the {\tt
 CircuitPiece} region relationship ensures that {\tt rn0 $\leq$ rn}.

Because data is partitioned dynamically in arbitrary ways and because
these partitions may not be disjoint, parallelism is necessarily also detected
dynamically in Legion.  Functions that the Legion runtime decides to execute
in parallel are called {\em tasks}.  All of the features we have discussed---the
dynamic partitioning of data and the runtime region tree, the static region disjointness
and subregion relationships, and the region privileges held by functions---as well
as data coherence, which we have not yet discussed, come together in the
decision of what functions can be launched as parallel tasks.  Function calls
are considered in program execution order. If a function's region accesses will not conflict
with a previously issued task, the function can also be executed as a parallel task.
One of our main results is a sufficient condition for deciding that two functions do not {\em interfere}
on their region arguments and can be executed in parallel as tasks.
Subtasks may also be launched within tasks, giving nested parallelism.
A second result allows even the scheduling decisions to be done in parallel, 
so that scheduling does not become a serial bottleneck.
Both results are in Section~\ref{subsec:coherenceparallel}.

The {\tt execute\_time\_steps} function illustrates the importance of
having different partitions provide multiple views onto the same
logical region.  The 
{\tt calc\_new\_currents} function uses the owned
and ghost regions of a piece, which are from different partitions;
no single partition of the nodes describes this access pattern. In
{\tt calc\_new\_currents} these regions only need read privileges,
while the only writes are performed to the wires subregion belonging
to that piece.  Thus, both instances of {\tt calc\_new\_currents} 
can be run as parallel tasks.  Similarly, the {\tt update\_voltage}
function (lines 6-7) modifies only the disjoint owned regions, while
only reading from regions shared with the other instance; the two
instances of {\tt update\_voltage} can also run in parallel.



The most interesting function is {\tt distribute\_charge} (lines
2-5), which uses a {\em reduction} privilege for regions {\tt rn} and
{\tt rg}.  A reduction names the reduction operator (which is assumed
to be associative and commutative) as the first component of the
privilege.  Programmers can write their own reduction operators,
such the function {\tt reduce\_charge} in Listing~\ref{lst:loop}.  
Reductions allow updates to the named regions that
are performed with the named reduction operator to be reordered.
For example, reductions can be performed locally by a task and only
the final results folded in to the destination region.  However, by
default, functions with no coherence annotation have {\em exclusive} coherence
for their region arguments: reads and writes have the results
expected as if the original sequential execution order of the program
was preserved, unaffected by any concurrently executing tasks.  Thus,
to fully exploit reductions it is important to use a relaxed coherence mode,
in this case {\tt atomic} coherence, which permits other tasks performing
the same reduction operation on the named regions to execute in parallel.
The most relaxed coherence
mode is {\tt simult}; simultaneous coherence allows concurrent access 
to the region by all functions that are using the region
in a simultaneous mode.  The interaction between tasks
using the same region with different coherence
modes is formalized in Section~\ref{sec:coherence}.
While associative and commutative reductions will always produce the same
result regardless of execution order, in general relaxed coherence modes introduce non-determinism into Legion
programs.  This non-determinism is completely under programmer control,
 at the per-region (or subregion) granularity.


%The {\tt execute\_time\_steps} function also demonstrates the need to 
%dynamically discover parallelism through region disjointness.  
%To run the two instances of 
%{\tt calc\_new\_currents} in parallel requires knowing that {\tt rw0}
%and {\tt rw1} are disjoint (lines 17-18).  There is a similar requirement for parallel
%execution of the two {\tt update\_voltage} calls with {\tt rn0} and {\tt rn1} (lines 21-22).  In
%both cases, this knowledge was statically available when the subregions were created 
%and could have been captured in a region relationship at the cost
%of much more complicated code.  Instead, we are able to use a simpler region relationship
%and rely on an inexpensive dynamic disjointness
%check by the Legion runtime to (re)discover the parallelism.

%In addition to privileges, functions can also specify {\em coherence}
%on regions.  By default, coherence on all regions is {\em exclusive}, meaning
%the function must appear to execute in program order relative to other function
%calls that access non-disjoint data.  Programmers can allow other
%execution orderings using relaxed coherence.  Line 5 of Listing~\ref{lst:loop} shows
%an example of {\tt atomic}, a relaxed coherence mode requiring
%that operations to {\tt rn} and {\tt rg} appear atomic relative
%to other functions using regions which may alias.  

%The most important decision in any Legion program is the choice of
%how to partition program data into logical regions.  Logical regions
%are the granularity at which the Legion runtime will reason
%about the independence of tasks, and therefore how much parallelism
%is available.  Logical regions also define the granularity at which
%the Legion runtime will move data.  Coarser regions will lead
%to more efficient bulk copies of data throgh the memory hierarchy due
%to better locality.  There is therefore a natural tension: partitioning 
%should be fine-grained enough to express desired parallelism, but not 
%so fine-grained as to result in inefficient data movement.

%The first step in the circuit example is to partition the arbitrary graph
%of circuit elements into pieces that can be handled independently.
%In the case of this example all nodes in the graph have initially
%been allocated in the region {\tt all\_nodes} (Figure~\label{sfig:part_fig:tree}).
%The decision of how to partition the graph is the responsibility
%of the programmer and can be done by an external library (e.g. METIS
%in this case).  Once the decision of how to partition is made, the
%result must be communicated to the Legion runtime to 
%generate the corresponding logical regions.  Partitioning decisions
%are described via {\em colorings}.  Colorings are parametrized by
%the region they are partitioning and capture a mapping from pointers
%into the region to specific colors (e.g. integers).  Figure~\ref{sfig:part_fig:pvs}
%illustrates the coloring used for generating the {\tt p\_nodes\_pvs}
%partition.



