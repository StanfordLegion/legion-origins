
\section{Core Legion Circuit Code}
\label{sec:examplefull}

%% SJT - no actual text here for now
\texcomment{
Listing~\ref{lst:circuit_ex_full} shows code for the full circuit simulation written 
in Core Legion.  For clarity, we briefly describe how this program works,
though some of the explanation overlaps with the discussion in Section~\ref{sec:example}.

The entry function is {\tt simulate\_circuit} (line 16), which is
parametrized on regions {\tt rl}, {\tt rw}, and {\tt rn}.
The wires and nodes are contained in regions {\tt rw} and {\tt rn},
respectively.  The {\tt rl} region holds lists of pointers to the nodes and wires.
When {\tt simulate\_circuit} is invoked it must have both read and
write privileges for the regions {\tt rl}, {\tt rw}, and {\tt rn}
(line 18); the type system verifies (at compile-time) that
{\tt simulate\_circuit} only accesses these regions and their
subregions.

Partitioning of the graph is done using the three colorings built in the
{\tt color\_circuit} function (line 19-20).  The first
maps each node to the graph piece that owns it.
The third maps each wire to a graph piece that
owns one of its nodes.  The second coloring captures, for each piece $p$,
the nodes in other pieces on the boundary of $p$ (the ghost nodes).  
%We describe how ghost node regions are used in
%conjunction with the owned nodes shortly.  
Note that 
ghost nodes may be on the boundary of multiple pieces and therefore colored
multiple ways.  The second coloring is a {\em multicoloring} (because
it may map the same element to multiple colors) which is implemented in Core
Legion as a tuple of colorings (line 13), one for each color.
We omit the full implementation
of the {\tt color\_circuit} function for brevity, but show the sub-function
{\tt create\_piece\_coloring} which computes the coloring for the owned
nodes of each piece of the graph.

After creating the colorings, the application partitions the node and wire
regions into subregions according to the colorings (lines 22-27).  For
simplicity, our example only partitions the circuit into two pieces.
Line 22 uses coloring {\tt pc.1} to partition {\tt rn} into {\tt rn0}, and {\tt rn1}.
Subregion {\tt rn0} will contain all locations with a color of 1 in {\tt pc.1},
while {\tt rn1} will contain all locations with a color of 2.  This
partitioning uses a coloring and results in disjoint subregions.
Constraints are introduced into the static environment describing both the
disjointness of the subregions (i.e. {\tt rn0 $*$ rn1}) and the inclusion of
each subregion in the original region (e.g. {\tt rn0 $\leq$ rn}).  Lines
24-25 show the partitioning based on a multicoloring done in Core Legion as
multiple partition operations.  Inclusion constraints are still introduced
into the static environment but due to potential aliasing, no disjointness 
constraints are introduced.
}


% This is a description of how the listings should be formatted.
% It can go anywhere before the listings.
\lstset{
  captionpos=b,
  language=Haskell,
  basicstyle=\scriptsize,
  numbers=left,
  numberstyle=\tiny,
  columns=fullflexible,
  stepnumber=1,
  escapechar=\#,
  keepspaces=true,
  literate={<}{{$\langle$}}1 {>}{{$\rangle$}}1,
  morekeywords={function,rr,int,float,bool,isnull,partition,as,downregion,upregion,reads,writes,rdwrs,reduces,read,write,reduce,using,unpack,pack,coloring,color,newcolor,atomic,simultaneous},
  deletekeywords={float,head,min,max},
  showlines=true
}

\lstinputlisting[float={h},firstline=1,lastline=68,firstnumber=1,caption={Top-Level Application Code}]{circuit_example.shll}
\newpage
\vspace*{0.11in}
\lstinputlisting[float={h},firstline=69,lastline=108,firstnumber=69,caption={Leaf Computation Tasks}]{circuit_example.shll}
\newpage
\lstinputlisting[float={h!},firstline=109,firstnumber=109,lastline=160,caption={Coloring Functions}]{circuit_example.shll}
\newpage
\lstinputlisting[float={h!},firstline=161,firstnumber=161,caption={List-Building Helper Functions}]{circuit_example.shll}

\texcomment{
The circuit simulation uses an {\em allocate-then-partition} style of
computation, where a large data structure is first allocated and then
partitioned.  Legion also supports a {\em partition-then-allocate} style,
where empty regions are first partitioned and later populated with
data.  Both are useful; e.g., Sequoia \cite{Fatahalian06} and DPJ
\cite{Bocchino09} support static forms of allocate-then-partition and
partition-then-allocate, respectively.  Legion supports both
approaches dynamically.

After partitioning the circuit into pieces, the application creates
instances of {\tt CircuitPiece} (defined on lines 8-11) for each piece
(lines 30-35).  Recall that {\tt CircuitPiece} is a region relationship.
Note that on line 42
function {\tt execute\_time\_steps} has read/write privileges for
region {\tt rn0} because it has read/write privileges for {\tt rn} and
the {\tt CircuitPiece} region relationship constraints ensure $\tt rn0
\leq rn$ (line 10).


The {\tt execute\_time\_steps} function (lines 39-50)  
also illustrates the importance of using different partitions to give
multiple views onto the same logical region.  Both {\tt
  calc\_new\_currents} and {\tt distribute\_charge} 
use the owned and ghost regions of a piece, which are from different partitions. In
{\tt calc\_new\_currents} these regions only need read
privileges, allowing both instances of {\tt calc\_new\_currents} to be
run in parallel.  In {\tt distribute\_charge} the
privilege is for a reduction which can also be done in parallel
because of the atomic and commutative nature of reductions.  Finally,
the {\tt update\_voltage} function modifies only the owned regions, permitting
each piece to be updated in parallel.  

%This function also demonstrates the benefit of being able to dynamically
%(re-)discover disjointness.  Safely running both instances of 
%{\tt calc\_new\_currents} in parallel depends on knowing that {\tt rw0} and
%{\tt rw1} in {\tt execute\_time\_steps} are disjoint.  Similarly, parallel
%execution of the instances of {\tt update\_voltage} requires knowing that
%{\tt rn0} is disjoint from {\tt rn1}.  In both cases, this knowledge was
%statically available in {\tt simulate\_circuit} and could have been captured
%in a region relationship, but it would have resulted in much more complicated
%code.
%Instead, an inexpensive dynamic disjointness check is performed by the Legion
%runtime, and the safety of parallel execution follows from the soundness of
%the type system.
%The {\tt execute\_time\_steps} function also demonstrates the benefit of dynamically
%discovering disjointness.  To run the two instances of 
%{\tt calc\_new\_currents} in parallel requires knowing that {\tt rw0}
%and {\tt rw1} are disjoint.  There is a similar requirement for parallel
%execution of the two {\tt update\_voltage} calls with {\tt rn0} and {\tt rn1}.  In
%both cases, this knowledge was statically available in {\tt simulate\_circuit}
%and could have been captured in a region relationship at the cost
%of much more complicated code.  Instead, an inexpensive dynamic disjointness
%check by the Legion runtime will discover the parallelism.

Listing~\ref{lst:circuit_leaf_full} shows the leaf functions for each phase.
Each function iterates over the list of wires or
nodes for its piece.  Each function specifies the region privileges it
requires (lines 2,14,23).  
%These privileges
%are statically checked to match the operations performed inside of
%each function (e.g. read and write).  
In the case of {\em reduce} the
privilege must also specify which reduction function is used
(line 14).
}


