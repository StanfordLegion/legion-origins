
\section{Circuit Example}
\label{sec:example}

\lstset{
  captionpos=b,
  language=Haskell,
  basicstyle=\footnotesize,
  columns=fullflexible,
  stepnumber=1,
  escapechar=\#,
  keepspaces=true,
  literate={<}{{$\langle$}}1 {>}{{$\rangle$}}1,
  morekeywords={function,rr,int,float,bool,isnull,partition,as,downregion,upregion,reads,writes,rdwrs,reduces,read,write,reduce,using,unpack,pack,coloring,color,newcolor,atomic,simultaneous},
  deletekeywords={head,min,max},
  xleftmargin=10pt,
}
% Pulled line number info out so no line numbers appear on code snippets
%  numbers=left,
%  numberstyle=\tiny,

We begin by introducing a circuit simulation written in the
Legion programming model that serves as a running example 
throughout the remainder of the paper.  In this section we
describe how the requirements of the simulation motivate the novel
features of Legion.  Section~\ref{sec:legioncore} 
introduces the Core Legion language by showing
examples of code from the circuit simulation.

%Section~\ref{subsec:circuit_partition} introduces
%the Legion partioning process by describing how data is partitioned
%in a circuit simulation.  In Section~\ref{subsec:circuit_syntax} we 
%present code snippets from the circuit simulation to introduce the language
%constructs for Core Legion (see Section~\ref{sec:legioncore}).
%Core Legion language (formally defined in Section~\ref{sec:legioncore}). 
%For brevity we present only code fragments; the full example is in
%Appendix~\ref{sec:examplefull}.
%(see \cite{Legion12} for examples
%written in a syntax friendlier to programmers).  

The circuit simulation takes as input an arbitrary graph of circuit elements 
(wires and nodes where the wires connect) represented by the two logical
regions {\tt all\_nodes} and {\tt all\_wires}.  The simulation iterates for many 
time steps, performing three computations during each time step:
{\tt calc\_new\_currents}, {\tt distribute\_charge}, and finally {\tt update\_voltage}.
For these computations to be run in parallel, the regions representing the
graph must be partitioned into pieces that match the simulation's data access patterns.  The choice
of partitioning will ultimately dictate performance and is therefore the most important 
decision in any Legion program.

An ideal partitioning depends on many factors, including the shape of
data structures, the input, and the desired number of partitions
(which usually varies with the target machine).  Due to the multitude
of factors that can influence partitioning, a critical design decision
made in Legion is to provide a programmable interface whereby the
application can compute a partitioning dynamically and communicate
that partitioning to the Legion runtime system.  This design absolves
the Legion implementation of the responsibility for computing an ideal
partition for all regions across all applications on any potential
architecture.  Instead, our approach provides the application with
direct control over all partitioning decisions that ultimately impact
performance.

In Legion, partitioning takes place in two steps.  First, the programmer assigns
a {\em color} to each element of the region to be partitioned. The number
of colors and how they are assigned to elements can be the result of an arbitrary
computation, giving the programmer complete control over the coloring.
Second, Legion creates new subregions, one
for each color, with each region element assigned to the subregion of the appropriate color.
Thus, the programmer expresses the desired partitioning of a region, and Legion provides
the mechanism to carry out the programmer's directions.

\begin{figure}[t]
\centering
\subfigure[Node region tree.]{
\label{sfig:part_fig:tree}
\includegraphics[scale=0.50]{figs/OwnedTree}
}
\subfigure[Circuit piece coloring.]{
\label{sfig:part_fig:pieces}
\includegraphics[scale=0.65]{figs/Owned_Nodes}
}
\subfigure[Ghost {\tt rg0} coloring.]{
\label{sfig:part_fig:ghost_zero}
\includegraphics[scale=0.65]{figs/Ghost_RN0}
}
\subfigure[Ghost {\tt rg1} coloring.]{
\label{sfig:part_fig:ghost_one}
\includegraphics[scale=0.65]{figs/Ghost_RN1}
}
\vspace{-2mm} 
\caption{Partitions of the $all{\_}nodes$ region. \label{fig:part_fig}}
\vspace{-2mm}
%  \caption[My caption
\end{figure}

\newcommand{\oton}[1]{{#1}_1,\ldots,{#1}_n}
\newcommand{\otok}[2]{{#2}_1,\ldots,{#2}_{#1}}

\begin{figure*}[t]
\centering
{\small
\begin{tabular}{ccl@{\hspace{-0.1in}}r|ccl@{\hspace{-0.1in}}r}

$T$ & ::= &  & types & $bv$ & ::= & false $\;\;\;\mid\;\;\;$ true & \\
  &$\mid$& bool $\;\;\;\mid\;\;\;$ int & base types & & & & \\
  &$\mid$& $\langle T_1, \ldots, T_n \rangle$ & tuple & $iv$ & ::= & 0 $\;\;\;\mid\;\;\;$ 1 $\ldots$ & \\
  &$\mid$& $T@(\oton{r})$ & pointer & & & & \\
  &$\mid$& $\text{coloring}(r)$ & region coloring & $e$ & ::= & & expressions \\
  &$\mid$& $\exists \oton{r}. T\text{ where }\Omega$ & region relationship &   &$\mid$& $bv$ $\;\;\;\mid\;\;\;$ $iv$ & constants \\
  &$\mid$& $\forall \oton{r}. (\oton{T}), \Phi, Q \rightarrow T_r$ & functions &   &$\mid$& $\langle e_1, \ldots, e_n \rangle$ $\;\;\;\mid\;\;\;$ $e$.1 $\;\;\;\mid\;\;\;$ $e$.2 $\;\;\;\mid\;\;\;$ \ldots & tuple \\
& & & &   &$\mid$& $id$ &  \\
$\Omega$ & ::= & $\{ \oton{\omega} \}$ & region constraints &   &$\mid$& $\text{new}\ T@r$ $\;\;\;\mid\;\;\;$ $\text{null }T@r$ $\;\;\;\mid\;\;\;$ $\text{isnull}(e)$ & \\
$\omega$ & ::= & $r_1 \leq r_2$ & subregion   &   
  &$\mid$& $\text{upregion}(e, r_1,\ldots,r_n)$ & \\
  &$\mid$& $r_1 * r_2$ & disjointness &   
  &$\mid$ & $\text{downregion}(e, r_1,\ldots,r_n)$ & \\  
& & &  & 
  &$\mid$& $\text{read}(e_1)$ $\;\mid\;$ $\text{write}(e_1, e_2)$ & memory access \\
$\Phi$ & ::= & $\{ \oton{\phi} \}$ & privileges & 
  & $\mid$ & $\text{reduce}(id, e_1, e_2)$ & \\
$\phi$ & ::= & reads$(r)$ $\;\mid\;$ writes$(r)$ $\;\mid\;$ $\text{reduces}_{id}(r)$ &  &  
  &$\mid$& $\text{newcolor}\ r$ $\;\mid\;$ $\text{color}(e_1, e_2, e_3)$ & coloring \\
& & & &  
  &   $\mid$& $e_1 + e_2$ & integer ops \\
$Q$ & ::= & $\{ \oton{q} \}$ & coherence modes  &   
       & $\mid$& $e_1 < e_2$ & comparisons \\
$q$ & ::= & atomic$(r)$ $\;\;\;\mid\;\;\;$ simult$(r)$ & &   
  &$\mid$& $\text{let}\ id : T = e_1 \text{in}\ e_2$ &  \\
& & & &   
       & $\mid$& $\text{if}\ e_1\ \text{then}\ e_2\ \text{else}\ e_3$ &  \\
$v$ & ::= & & values &   
       & $\mid$& $id[r_1, \ldots, r_n](e_1,\ldots,e_n)$ & function calls \\
  &$\mid$& $bv$ $\;\;\;\mid\;\;\;$ $iv$ & base values &   
       & $\mid$ & $\text{partition}\ r_p\text{ using }e_1\text{ as }\oton{r}\text{ in }\ e_2$ &  \\
  &$\mid$& $\langle v_1, v_2 \rangle$ & tuple & 
       & $\mid$& $\text{pack}\ e_1\ \text{as}\ T[r_1,\ldots,r_n]$ &  \\
  &$\mid$& null $\;\;\;\mid\;\;\;$ $l$ & memory location & 
       & $\mid$& $\text{unpack}\ e_1\ \text{as}\ id : T[r_1,\ldots,r_n]\ \text{in}\ e_2$ &  \\
  &$\mid$& $\{ (l, iv), \ldots \}$ & coloring & 
    & & & \\
  &$\mid$& $\langle \langle \oton{\rho}, v\rangle \rangle$ & reg. relation instance & 
   & & & \\
%$bv$ & ::= & false $\;\;\;\mid\;\;\;$ true \\
%\\
%$iv$ & ::= & 0 $\;\;\;\mid\;\;\;$ 1 $\ldots$ \\
%\\
%$e$ & ::= & & expressions \\
%  &$\mid$& $bv$ $\;\;\;\mid\;\;\;$ $iv$ & constants \\
%  &$\mid$& $\langle e_1, e_2 \rangle$ $\;\;\;\mid\;\;\;$ $e$.1 $\;\;\;\mid\;\;\;$ $e$.2 & tuple \\
%  &$\mid$& $id$ &  \\
%  &$\mid$& $\text{new}\ T@r$ $\;\;\;\mid\;\;\;$ $\text{null }T@r$ $\;\;\;\mid\;\;\;$ $\text{isnull}(e)$ & \\
%  &$\mid$& $\text{upregion}(e, r_1,\ldots,r_n)$ $\;\;\;\mid\;\;\;$ $\text{downregion}(e, r_1,\ldots,r_n)$ & \\
%  &$\mid$& $\text{read}(e_1)$ $\;\;\;\mid\;\;\;$ $\text{write}(e_1, e_2)$ $\;\;\;\mid\;\;\;$ $\text{reduce}(id, e_1, e_2)$ & memory access \\
%  &$\mid$& $\text{newcolor}\ r$ $\;\;\;\mid\;\;\;$ $\text{color}(e_1, e_2, e_3)$ & coloring operations \\
%  &$\mid$& $e_1 + e_2$ & integer operations \\
%  &$\mid$& $e_1 < e_2$ & comparison operations \\
%  &$\mid$& $\text{let}\ id : T = e_1 \text{in}\ e_2$ &  \\
%  &$\mid$& $\text{if}\ e_1\ \text{then}\ e_2\ \text{else}\ e_3$ &  \\
%  &$\mid$& $id[r_1, \ldots, r_n](e_1,\ldots,e_n)$ & function calls \\
%  &$\mid$& $\text{partition}\ r_p\text{ using }e_1\text{ as }\oton{r}\text{ in }\ e_2$ &  \\
%  &$\mid$& $\text{pack}\ e_1\ \text{as}\ T[r_1,\ldots,r_n]$ &  \\
%  &$\mid$& $\text{unpack}\ e_1\ \text{as}\ id : T[r_1,\ldots,r_n]\ \text{in}\ e_2$ &  \\
\end{tabular}
}
\caption{Core Legion}
\label{fig:langdef}
\vspace{-3mm}
\end{figure*}

To efficiently support the circuit simulation's access patterns, 
the region {\tt all\_nodes} holding all
the nodes of the graph is partitioned in two different ways.  The
desired {\em region tree} is shown in Figure~\ref{sfig:part_fig:tree}.
First, there are subregions of {\tt all\_nodes} that describe
the set of nodes ``owned'' by each piece, called {\tt rn0}, {\tt rn1},
$\ldots$. Since each node is in one piece, this partition is {\em
disjoint}, which is indicated by a $*$ on the left subtree.
Figure~\ref{sfig:part_fig:pieces} shows one possible partitioning
along with the necessary coloring to generate the disjoint partition
in Figure~\ref{sfig:part_fig:tree}.
%different colors identify different piece IDs.  
Second, each piece of
the circuit needs access to the ghost nodes on its border. The
ghost nodes for two circuit pieces are shown in
Figures~\ref{sfig:part_fig:ghost_zero}
and~\ref{sfig:part_fig:ghost_one}; note that two nodes are in both sets.
Because a node may neighbor more than one other circuit piece, this
second partition of {\tt all\_nodes} is {\em aliased}.  Thus, there
are two sources of aliasing in the region tree: the two distinct
partitions divide the {\tt all\_nodes} region in different ways, and
the ghost node subregions are not disjoint.  

There are two alternative approaches to using multiple partitions
for the circuit simulation, both of which avoid introducing aliasing.
We could create a single partition with $2^n$ subregions, one for each
possible case of sharing, or computations on each piece could use the
{\tt all\_nodes} region to access their ghost nodes.  Neither option
is attractive: the former significantly complicates programming  and the latter
greatly overestimates the required ghost nodes,
increasing runtime data movement as well as limiting parallelism.

For simplicity this example
has only one level of partitioning (although in two different ways).
All the semantic issues that concern the results of this paper can be illustrated
with one level of partitioning.  In general, however, the region tree can have  many levels, 
as subregions are themselves partitioned,
perhaps also in multiple ways. Typically, the number of levels and size of partitions
depends on both the data and the memory hierarchy of the target machine, allowing regions 
to be placed in levels of memory where they fit \cite{Legion12}.

Because data is partitioned dynamically in arbitrary ways and because
these partitions may not be disjoint, parallelism is necessarily detected
dynamically in Legion.  Functions that the Legion runtime considers for parallel execution
are called {\em tasks}.  Tasks are required to specify the regions
that they access as well as the task's privileges and coherence modes
on each region; the type system introduced in Section~\ref{subsec:coretypes}
verifies that Legion tasks abide by their declared region access privileges.
  The partitioning of the data, task region privileges, and task coherence modes
all contribute to determining which tasks can be executed in parallel.


The Legion task scheduler considers task calls in sequential program
execution order.  If a task's region accesses do not conflict with a
previously issued task, the task can be launched as a parallel task,
otherwise it is serialized with all of its conflicting tasks.  One of
our main results is a sufficient condition for deciding that two tasks
do not {\em interfere} on their region arguments and can be executed
in parallel (Section~\ref{sec:coherence}).  Subtasks may also be
launched within tasks, giving nested parallelism.  A second result
allows even the scheduling decisions to be made in parallel, so that
scheduling does not become a serial bottleneck
(Section~\ref{sec:scheduling}).

%Circuit issues three tasks for each piece of the 
%circuit simulation during every time step to perform the 
%{\tt calc\_new\_currents}, {\tt distribute\_charge}, and {\tt update\_voltage}
%computations, each accessing different sets of regions with varying privileges
%and coherence modes. 





%All of the features we have discussed---the
%dynamic partitioning of data and the runtime region tree, the static region disjointness
%and subregion relationships, and the region privileges held by functions---as well
%as data coherence, which we have not yet discussed, come together in the
%decision of what functions can be launched as parallel tasks.  Function calls
%are considered in program execution order. If a function's region accesses will not conflict
%with a previously issued task, the function can also be executed as a parallel task.
%One of our main results is a sufficient condition for deciding that two functions do not {\em interfere}
%on their region arguments and can be executed in parallel as tasks.
%Both results are in Section~\ref{subsec:coherenceparallel}.

%% In addition, to partitioning the graph to describe {\em owned} nodes
%% for each piece, the circuit example also
%% needs to be able to name the {\em ghost nodes} for each piece.  Ghost node
%% regions are used for accessing the nodes of wires that span
%% two different pieces. (This is also the reason {\tt CircuitWire} is parametrized on
%% two regions).  Figure~\ref{sfig:part_fig:ghost_zero} and 
%% Figure~\ref{sfig:part_fig:ghost_one} show colorings for the ghost regions
%% corresponding to {\tt rn0} and {\tt rn1} respectively.  Note that two 
%% nodes are colored in both colorings.  Since nodes can appear in multiple
%% subregions, we use a {\em multicoloring} (implemented in core Legion
%% by multiple single colorings) to describe {\em aliased} partitions.  Aliased
%% partitions still introduce subregion constraints into the static checking
%% environment, but not disjointness constraints.  Figure~\ref{sfig:part_fig:tree}
%% shows the resulting logical region tree for node regions.




%The {\tt execute\_time\_steps} function also demonstrates the need to 
%dynamically discover parallelism through region disjointness.  
%To run the two instances of 
%{\tt calc\_new\_currents} in parallel requires knowing that {\tt rw0}
%and {\tt rw1} are disjoint (lines 17-18).  There is a similar requirement for parallel
%execution of the two {\tt update\_voltage} calls with {\tt rn0} and {\tt rn1} (lines 21-22).  In
%both cases, this knowledge was statically available when the subregions were created 
%and could have been captured in a region relationship at the cost
%of much more complicated code.  Instead, we are able to use a simpler region relationship
%and rely on an inexpensive dynamic disjointness
%check by the Legion runtime to (re)discover the parallelism.

%In addition to privileges, functions can also specify {\em coherence}
%on regions.  By default, coherence on all regions is {\em exclusive}, meaning
%the function must appear to execute in program order relative to other function
%calls that access non-disjoint data.  Programmers can allow other
%execution orderings using relaxed coherence.  Line 5 of Listing~\ref{lst:loop} shows
%an example of {\tt atomic}, a relaxed coherence mode requiring
%that operations to {\tt rn} and {\tt rg} appear atomic relative
%to other functions using regions which may alias.  

%The most important decision in any Legion program is the choice of
%how to partition program data into logical regions.  Logical regions
%are the granularity at which the Legion runtime will reason
%about the independence of tasks, and therefore how much parallelism
%is available.  Logical regions also define the granularity at which
%the Legion runtime will move data.  Coarser regions will lead
%to more efficient bulk copies of data throgh the memory hierarchy due
%to better locality.  There is therefore a natural tension: partitioning 
%should be fine-grained enough to express desired parallelism, but not 
%so fine-grained as to result in inefficient data movement.

%The first step in the circuit example is to partition the arbitrary graph
%of circuit elements into pieces that can be handled independently.
%In the case of this example all nodes in the graph have initially
%been allocated in the region {\tt all\_nodes} (Figure~\label{sfig:part_fig:tree}).
%The decision of how to partition the graph is the responsibility
%of the programmer and can be done by an external library (e.g. METIS
%in this case).  Once the decision of how to partition is made, the
%result must be communicated to the Legion runtime to 
%generate the corresponding logical regions.  Partitioning decisions
%are described via {\em colorings}.  Colorings are parametrized by
%the region they are partitioning and capture a mapping from pointers
%into the region to specific colors (e.g. integers).  Figure~\ref{sfig:part_fig:pvs}
%illustrates the coloring used for generating the {\tt p\_nodes\_pvs}
%partition.



