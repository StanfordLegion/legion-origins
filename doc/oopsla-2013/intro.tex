

\section{Introduction}
\label{sec:intro}

In the last decade machine architecture, particularly at the high
performance end of the spectrum, has undergone a revolution.  The
latest supercomputers are now composed of heterogeneous processors and
deep memory hierarchies.  Current programming systems for these
machines have elaborate features for describing parallelism, but few
abstractions for describing the organization of data. However, having
the data organized correctly within the machine is becoming ever more
important.  Current supercomputers have at least six levels of memory,
most of which are explicitly managed by software; even current
commodity desktop and mobile computers have at least five
levels.\footnote{A typical organization is (1) distributed memory
across a physical network of nodes; (2) shared RAM on chip; (3) one to
three levels of cache for each CPU, some shared, some not; (4) GPU
global memory; (5) GPU shared memory; (6) GPU registers.  Only
the CPU caches are managed by hardware and only the global network is
not present in commodity consumer machines.}  As machines of all
scales increase the number of processing cores and quantity of
available memory, the latency
between system components inevitably increases.  For many applications the
placement and movement of data is already the dominant performance 
consideration, particularly in high-end machines, and this problem
will only grow more acute as overall transistor counts and latencies in
future machines increase while the total power budget remains relatively
constant.  

To program parallel machines with distributed memory (hierarchically
organized or not), data must be partitioned into subsets that are
placed in the individual memories.  For example, in graph computations it 
is common to subdivide the graph into subgraphs sized to fit in fast
memory close to a processor.  Note that the term {\em partition} does
not imply the subdivisions of the data are always disjoint---it is desirable
to also allow subdivisions that overlap or {\em alias}.
Continuing with the example, many graph computations require
knowledge of the nodes bordering each subgraph.  Some of these {\em
ghost nodes} for a particular subgraph may also border other
subgraphs.  In general, the ghost nodes for different subgraphs often
alias.

In machines with more than two levels of explicitly managed memory,
data partitioning involves a hierarchy where the initial partitions of
the data are themselves further partitioned.  Often divide-and-conquer
strategies repeatedly subdivide the data so that the finest
granularity fits in the smallest, fastest memory closest to a processor where
a specific computation can access it, which results  in
complex communication patterns as coarser and finer sets of data are shuffled
up and down the memory hierarchy \cite{Fatahalian06}.  Thus, 
the placement and movement of data, and subsets of data, is
a first-order programming concern.  We adopt a region-based approach
that makes these groupings of data explicit in the program:
a {\em logical region} names a set of data, a {\em subregion} of
a logical region names a subset of a logical region's data, and
a {\em partitioning} of a logical region $r$ names a number of (possibly
overlapping) subregions of $r$.  We use the term {\em logical region}
(which we sometimes abbreviate to {\em region}) to emphasize
that our language-level regions do not imply a physical layout or
placement of the logical region's data in the memory hierarchy.  Logical regions are just sets of
elements and a subregion is literally a subset of its parent
region.\footnote{A separate system of {\em physical regions} hold concrete copies of the data of logical regions at run-time.  Physical regions have a specific data layout and live in a specific memory.
The Legion run-time system may maintain multiple physical copies of a single logical region for performance reasons; for example, read-only may be replicated in multiple physical regions to put it closer to the computations that use it.}

By making the groupings of data into regions explicit, 
it becomes possible for the programmer to express properties of the different regions in a program
and for the language system to leverage this information for both performance and correctness in ways
that would be difficult to infer without the programmer's guidance.  In addition
to partitioning regions into subregions, we focus on three properties that Legion programmers can
express about regions:

\begin{itemize}

\item  {\em Privileges}. Computations have privileges specifying
how they can use regions: {\em read-only}, {\em read-write}, and {\em reduce}.
More computations can execute in parallel using privileges than without.  For example,
regions that alias can still be accessed simultaneously by multiple parallel computations
provided that the computations all access the regions with read-only privileges,
 or all access the regions
to perform reductions using the same associative and commutative reduction operator.

\item {\em Coherence}. Computations are written in a sequential program order.  By default
all computations access regions with {\em exclusive} coherence, which ensures
the computations appear to execute in the sequential order, permitting parallelism only
when computations access disjoint regions or have non-interfering privileges.
However, computations can also request relaxed coherence modes {\em atomic}
and {\em simultaneous} on regions.  Relaxed coherence modes allow reordering and parallel
execution of computations that otherwise would execute sequentially due to accessing
aliased sets of regions.  For example, two computations each requesting atomic 
coherence on the same region may be re-ordered with respect to the sequential execution order so long as
their accesses are serializable.  Simultaneous coherence 
imposes no restrictions on other computations' access to a region; one
instance where simultaneous access is useful is when a programmer
has implemented his own, higher-level synchronization mechanism.
%\item {\em Coherence}.  Computations can request {\em exclusive}
%access to regions, or permit relaxed data coherence modes {\em atomic}
%and {\em simultaneous}.  Relaxed coherence modes allow reordering and
%parallel execution of computations that otherwise would be serialized
%due to conflicting accesses to the region.  For example, if a
%computation only requests atomic access to a region, other
%computations can access the same region as long as the current
%computation appears to execute atomically.  Simultaneous access
%imposes no restrictions on other computations access to a region; one
%instance where simultaneous access is useful is when the programmer
%has implemented their own, higher-level synchronization mechanism.

\item {\em Aliasing}.  As outlined above, regions can be partitioned into subregions
  that may be disjoint or may overlap.  Detecting region aliasing is  necessary to identify
computations that can run in parallel.  A central insight of our approach is that detecting 
region aliasing is both easy and inexpensive when done dynamically at the granularity of logical 
regions instead of individual memory locations.  
%In contrast, other dynamic approaches, such as transactional memory, suffer from
%mhigh overhead because the tests are done at the granularity of individual memory words.
\end{itemize}


%Knowledge of how data is partitioned allows programmers and
%programming systems to reason about {\em independence} and {\em
%locality}.  When two computations work on two different, disjoint
%subsets of the data they are {\em independent}.  Identifying
%independent computations is the basis of parallel computation, since
%independent computations do not need to communicate and can execute in
%any order.  A separate property is {\em locality}: the data used by a
%particular computation should be placed in a memory close to the
%processor where the computation will be carried out.  Because closer
%memories require less time and power to access, a computation with
%good locality will be much faster and more power efficient than a
%computation with poor locality.  Note that a program execution can
%have many independent computations but still have very poor
%performance due to lack of locality---both properties are important. 


%The current state of practice requires the programmer to orchestrate
%all explicitly managed levels of the memory hierarchy themselves,
%typically programming in a melange of MPI, OpenMP, CPU-to-GPU APIs,
%and CUDA or OpenCL.  Besides requiring the programmer to master a
%number of complex technologies each of which does essentially the same
%thing (notate how and when to move data from one place to another in
%the machine), programs written in this way bake in too much knowledge
%of a particular machine, making them very difficult to port to even
%slightly different machines.  In the absence of any language-level
%semantics, this approach also introduces concurrency correctness
%issues that the system cannot reason about.

Previous work on hierarchically partitioned data has focused on
fully static approaches with no runtime overhead.  A key feature
of these systems is that they disallow all aliasing to make their 
static analyses tractable.    Two recent examples, Sequoia
\cite{Fatahalian06} and Deterministic Parallel Java (DPJ)
\cite{Bocchino09}, each provide a mechanism to statically partition
the heap into a tree of collections of data.  The two designs are
different in many aspects, but agree that there is a single
tree-shaped partitioning of data that must be checked statically (see
Section~\ref{sec:related}).  Both approaches also include a system
of privileges, but have either no or limited coherence systems.

%A second class of work permits data
%aliasing, allowing greater expressivity, and uses fully dynamic
%analyses of program execution to detect aliasing and guarantee correct
%results.  This approach incurs significant runtime overhead and
%requires centralized control logic that is difficult to scale to
%distributed memory machines; examples include transactional memory
%\cite{Harris05} and thread-level speculation \cite{Steffan00}.

Our own experience writing high-performance applications in Sequoia
\cite{Fatahalian06} as well as in the current industry standard mix of
MPI, shared-memory threads, and CUDA has taught us that a fully static
system is insufficient.  In many cases, the best way to partition data
is a function of the data itself---the partitions must be
dynamically computed and cannot be statically described.  Furthermore,
applications often need multiple, simultaneous partitions of the same
data---a single partitioning is not enough.  Because data partitioning
is at the center of what these applications do, shifting from fully
static partitions to partitions computed at run-time affects all
aspects of the programming model, and in particular the interactions
between aliasing, privileges, and coherence.  The challenge is to
design a system that is both semantically sound and flexible in
handling partitions, privileges and coherence with minimal runtime
overhead.


%Consequently, the burden of managing
%the correctness of parallel computations and movement of data
%through the memory hierarchy is placed on the programmer.
%However,
%these systems relegate to the programmer the responsibility for:
%\begin{itemize}
%\item the correctness of their parallel declarations
%\item the movement and placement of data in the memory hierarchy
%\end{itemize}
%Coupled with the complexity of these machines, this places a heavy
%burden on the programmer.  The reason that the programmer must bear
%this load is because current programming systems have few or no
%insights into properties of program data (e.g. locality and independence).  
%To enable more efficient programming of this class of machines we 
%need programming systems capable of reasoning both statically and dynamically
%about the structure of program data.

In this paper, we present static and dynamic semantics for Legion  \cite{Legion12},
a parallel programming model that supports multiple, dynamic data partitions and is
able to efficiently reason about aliasing, privileges, and coherence.  Specifically:

\begin{itemize}
\item Legion's logical regions are first-class values 
and may be dynamically allocated and stored in data structures.

\item  Logical regions can be dynamically partitioned into {\em subregions};
partitions are fully programmable. 

\item  A logical region may be dynamically partitioned in multiple different ways;
subregions from multiple partitions may include the same data.

%\item Each logical region has associated {\em privileges} and {\em coherence} modes, giving
%the programmer fine-grain control over how the data is used on a per-region basis.
\item For each computation, {\em privileges} and {\em coherence} modes are specified on
a per-region basis, giving the programmer fine-grained control over how data is accessed.
\end{itemize}

%Both dynamically computed partitions
%and simultaneous partitions of the same data introduce the possibility
%of {\em aliasing}---distinct regions may have overlapping data.

%While data in Legion is organized in a hierarchy of logical regions,
%computation is organized in a hierarchy of tasks.  Logical regions and
%tasks interact through a system of {\em privileges} that specify which
%operations ({\em read}, {\em write}, or {\em reduce}) a task (or any
%of its subtasks) may perform on each logical region argument.
%Privileges help to identify more parallelism than can be discovered
%from aliasing alone.  For example, if two tasks access overlapping
%(aliased) regions, but the tasks only read those regions, the tasks
%may still execute in parallel.

%A closely related problem is supporting distributed scheduling of
%parallel work.  In large, hiearchical machines a centralized scheduler
%that makes all scheduling decisions is itself a serial bottleneck.
%Because computation in Legion is hierarchical, it it is natural to
%send two sibling tasks to different schedulers on different processors and
%let scheduling proceed independently (without communication) for their
%descendants.  However, this approach is correct only
%if all the subtasks of one sibling are independent of all the subtasks of the other.
%Legion's design for analyzing aliasing and permissions guarantees this property.


%Tasks can only access logical regions for which they 
%possess privileges.  
%Tasks acquire privileges from their calling task
%or by creating new logical regions.  A key restriction is that 
%a sub-task can only be passed a subset of the privileges of its parent task.  

%Through multiple partitions of a logical region, a given datum may
%belong to multiple different regions.  

%To support additional parallelism when tasks access aliased regions,
%Legion provides {\em coherence} modes at the granularity of regions, 
%which allow relaxed constraints on task execution order.  The programmer may
%specify that tasks accessing aliased regions run in
%program order (the default), {\em atomically} with respect to each
%other, or even {\em simultaneously} if the application has alternative
%methods for managing non-deterministic
%behavior in that region.

%To guarantee the safety and correctness of programs, Legion relies on
%a combination of static and dynamic analysis based on the privilege
%and coherence systems.  
%To efficiently guarantee the safety and
%correctness of programs using first-class regions,
%privilege and coherence properties in the presence of arbitrary region
%aliasing, Legion relies on a combination of static and
%dynamic checks.  

%The critical feature that makes the combination of static
%and dynamic checks in Legion both sound and efficient is
%the dynamic partitioning of logical regions.  Detecting region aliasing
%is both easy and inexpensive if done dynamically at the granularity of logical
%regions that are known to the runtime system.  
%In contrast, transactional memory systems must do
%expensive dynamic checks for conflicts at the granularity of
%individual memory words and, as mentioned previously, at the other
%extreme languages that attempt to do all partitioning statically face
%a very difficult static alias analysis problem unless substantial
%restrictions are imposed on the programming model.

We make the following specific contributions:
\begin{itemize}
\item We present a type system for {\em Core Legion} programs that statically
  verifies the safety of individual pointers and region
  privileges at call boundaries (Section~\ref{subsec:coretypes}).
%  A subtle issue arises in checking privileges when regions are
%  stored in and later retrieved from heap data structures.  The type
%  system provides enough static information that the Legion runtime
%  can leverage its dynamic information about region aliasing to
%  perform dynamic privilege checks guaranteeing safety.

\item We present a novel parallel operational semantics for Core Legion.
 This semantics is compositional,
  hierarchical, and asynchronous, reflecting the way such programs
  actually execute on the hardware (Section~\ref{subsec:opsemantics}).  
%The semantics also
%  models conflicting memory operations between tasks in cases of relaxed
%  coherence, making explicit the boundary between deterministic and
%  possible non-deterministic behavior 

\item We prove the soundness of Legion's static type and privilege system (Section~\ref{sec:soundness}).  In particular, we show that Legion's very liberal dynamic manipulations of regions can
be handled with a combination of static and inexpensive dynamic checks.


%\item We use soundness of the type system to prove the soundness of
%  Legion's region coherence modes (Section~\ref{sec:coherence}).


\item Using the soundness of the type system,  we show that if expressions $e_1$ and $e_2$ 
are {\em non-interfering} (can be executed in parallel), then subexpressions
$e_1'$ of $e_1$ and $e_2'$ of $e_2$ are also non-interfering (Section~\ref{sec:scheduling}).  
This result is the basis
for Legion's hierarchical, distributed scheduler, which is crucial for high performance
on the target class of machines.  We note that no other parallel language or runtime system
currently supports distributed scheduling.
%  any form of centralized scheduling would be a bottleneck because of the
%large communication latencies in the target class of machines.
%; on the target class of machines, any centralized
%scheduler would be a serious bottleneck.

\item We give experimental evidence that supports the Legion design choices.  On three
  real-world applications, we show that dynamic region pointer checks
  would be expensive, justifying checking this aspect of the type
  system statically.  We also show that the cost of region aliasing
  checks is low, showing that an expressive and dynamic
  language with aliasing is compatible with both high
  performance and safety (Section~\ref{sec:evaluation}).

\end{itemize}



%The crucial aspect of this result is that the tasks $t_1$ and $t_2$ only 
%need be non-interfering instead of non-aliased.  Thus, through a combination
%of static and dynamic analysis, our privilge type system and coherence system
%enable us to implement a hierarchical, distributed scheduling algorithm even
%in the presence of aliasing introduced by the need for dynamic descriptions of
%data via first-class logical regions.  
%All of this is possible only because
%our programming system supports an abstraction for understanding the structure
%of program data.



%The important difference between Legion and the more static approaches
%is that Legion allows region aliasing, but does so in a structured way that minimizes
%runtime overheads.  
%The key observation is that static alias analysis is hard, but dynamic 
%alias analysis is very easy and relatively cheap when done at the granularity 
%of regions instead of individual heap locations.  
%Thus, Legion falls between fully static systems, such as
%Sequoia and DPJ, and fully dynamic approaches such as transactional
%memory.  
%Legion still has a significant static component; in
%particular, the required privileges for function calls
%and region
%pointer dereferences are checked statically.  The dynamic alias checks
%for independence are done at the granularity of regions and one check 
%on a region
%often serves to prove the independence of many subsequent uses of that region.
%In contrast, because transactional memory has no mechanism
%for grouping heap data into larger collections, it must test the
%overlap between two sets of data on each individual memory location, which is
%significantly more expensive.



%which makes it difficult 
%to statically determine which computations can be run in parallel.  
%One approach is to outlaw aliasing which allows for a static analysis
%to discover all available parallelism and schedule it entirely at
%compile-time, avoiding any runtime overhead.  Languages such as
%Sequoia and DPJ take this approach and allow only a single static partition of any region 
%to eliminate region aliasing, at some cost in expressiveness.



%While many programming systems for
%these machines have intricate features for describing parallelism,
%few have any abstractions for capturing properties of a program's data (e.g.
%independence and locality).  Understanding the structure of a program's data
%is critical for allowing programming systems to validate/discover
%parallelism or support placement of data in the memory hierarchy.  
%The few programming systems that do support data abstraction 
%features\cite{Fatahalian06,Bocchino09} permit only
%statically analyzable abstractions which restrict expressivity.  We
%present a type system for Legion\cite{Legion12}, a programming model
%that permits both static and dynamic descriptions of the structure of 
%program data.  Despite the dual nature of Legion, our type system allows us 
%to statically prove several useful properties about the structure and usage 
%of data in Legion programs.  We show how to leverage these properties 
%to enable a scalable, hierarchical scheduling algorithm based on a 
%dynamic analysis of program data.

%complex hierarchies of many different
%kinds of computing technologies: networks of nodes at the top level,
%multiple chips per node, multiple cores within a chip, and, 
%recently, multiple accelerators (usually GPUs) per node, which 
%can themselves be further decomposed.  We present the operational and static
%semantics of Legion\cite{Legion12}, a programming model targeted at providing an
%appropriate level of abstraction for programming such machines, one
%that is both sufficiently high-level to be portable while still
%exposing aspects that are crucial to performance. 

%The primary abstraction for capturing the structure of data in Legion is {\em logical regions}. Logical
%regions express {\em locality} (data that will be used together, and therefore should be colocated) 
%and {\em independence} (disjoint data that may be operated on in parallel).
%Legion programs organize data into a forest of logical regions.  Logical regions can be
%recursively partitioned into sub-regions.  

%In Legion data is organized in a hierarchy of {\em regions}
%and subregions while computation is organized in a hierarchy of {\em
%tasks} and subtasks operating on regions.  Regions and tasks interact
%through a static system of {\em region privileges} specifying 
%operations a task may perform on a region argument (read,
%write, or reduce) and  {\em region coherence} annotations that
%express what other tasks may do concurrently with
%the region (exclusive, atomic, or simultaneous).  We prove the
%soundness of the privileges and coherence system and use these two
%results to prove a third result: if two {\em siblings} (tasks that
%have the same immediate parent task) $t_1$ and $t_2$ are 
%{\em non-interfering} (have no ordering requirements for correctness), 
%then the any descendant of $t_1$ in the task hierarchy is non-interfering
%with any descendant of $t_2$.  This theorem is the basis for correct distributed
%task scheduling in a Legion implementation, which is crucial for
%performance; a centralized scheduler would be a bottleneck
%because of the large communication latencies in the target
%class of machines.


% Putting this here so that we can get the code in a good place
% Moving this to the circuit example section since it fits better there
% and some of this is redundant
%Legion's execution model is that by default, a program's semantics is
%its meaning as a sequential program, which we refer to as the {\em
%program order} execution.  While the
%implementation is not our focus here, if two functions use
%disjoint data (because all of the regions they use are disjoint), then
%Legion may execute them simultaneously.  Also like DPJ,
%Legion has a system of {\em privileges} ({\em read}, {\em write}, and
%{\em reduce}) on regions and an orthogonal system of region {\em
%coherence} modes ({\em excl}, {\em atomic}, and {\em simultaneous})
%that increase the number of situations in which functions can be
%executed in parallel.

%The rest of the paper is organized as follows.
%We begin in Section~\ref{sec:example} with an example program that illustrates
%a typical style of programming in Legion and motivates the need for multiple, dynamically computed partitions of
%a region that introduce aliased data.  We define Core Legion, a small language suitable 
%for our formal development, in Section~\ref{sec:legioncore}.
%Finally, Section~\ref{sec:related} discusses the most closely related work and
%Section~\ref{sect:conclusion} concludes.


  










