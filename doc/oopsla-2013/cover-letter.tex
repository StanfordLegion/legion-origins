\documentclass[11pt]{letter}
\usepackage[margin=1in]{geometry}

\begin{document}

We thank the reviewers for their very detailed comments 
regarding the paper.  We made several alterations to the paper to address
the reviewers' suggestions as well as to conform to the page limit.  In
this cover letter we first address the mandatory updates that were 
requested by the reviewers.  At the end we address comments and
questions specific to individual reviewers.

With the additional page allowance for this draft of the paper we have
added a substantial overview and worked example of the
operational semantics, which we believe will help readers understand 
the semantics and why our general approach is sound.
We also opted to move some interesting details from the proofs in the appendix
of our original draft into the body of the paper.  However, not all
the proofs fit within the new page limit.  Consequently, we have also
prepared an extended technical report version of our paper that
contains all the proofs in detail.  For completeness, we are including
this extended version of the paper with this submission so reviewers
can see that it aligns with the original draft of the paper.  If the
paper is accepted we will make the extended version of the paper
publicly available.

We reduced the redundancy between the introductory and technical sections
by refactoring the presentation of the circuit example in Section 2 (Circuit Example) 
so that it now spans both Sections 2 and 3 (Core Legion).  Section 2 is 
much shorter now and gives a high-level overview of the circuit simulation 
which should aid the reader in building intuition about Legion.  The code 
snippets that originally appeared in Section 2 are now in Section 3 and 
have been combined with the descriptions of the Core Legion language 
constructs.  This allows readers to understand the description of a 
language feature with a concrete example.  It also removes the redundancy 
in the explanation of language constructs that existed between Sections 2 
and 3.  Overall we feel this has made the paper easier to read.

We have clarified the description of failed dynamic checks.  The fifth
paragraph in Section 5 (Operational Semantics) now describes the semantics
of a failed dynamic check on a downcast operation:  
\begin{quote}
``Similarly, downregion checks whether a location is within the set assigned to the logical region.  
If this dynamic check fails, null is returned.  The application can use the 
isnull expression to test for this case and handle it appropriately."
\end{quote}
An actual dereference of a null pointer will result in a fault that will abort program
execution.  Failed dynamic independence tests in the runtime scheduler will
never cause the program to abort, but will result in serialized (i.e. conservative) execution.

As mentioned above, we have added an overview of the operational
semantics.  The first part of the overview (the new start to Section
5) explains our decision to use less conventional big-step operational
semantics.  We then explain the more novel aspects of the semantics,
focusing on memory traces (Section 5.1) and clobber sets
(Section 5.2) and then giving an extended example using a small program
that illustrates the most interesting features of the semantics.  We
would be happy to make further modifications if the referees still
have questions about the semantics.

Regarding concerns about the correctness of allocation, we have added the missing
constraint that allocation operations return fresh locations.  While not
strictly necessary for soundness of the type system, we agree that its omission
was confusing to the reader.

Below we address specific questions and comments from individual reviewers.

Reviewer 1 questioned why the two calls to {\tt calc\_new\_currents} are safe to
execute in parallel.  The reviewer is correct in recognizing that {\tt rn0} and
{\tt rg0} will overlap with {\tt rn1} and {\tt rg1} and may even end up using the same 
physical region(s).  The reason the tasks can run in parallel is that read-only
privileges are requested on these regions.  Since there are no requested
write privileges on the node regions, the two instances of {\tt calc\_new\_currents}
do not interfere on the node regions despite the presence of region aliasing.  
Note that the two calls to {\tt calc\_new\_currents}
only request write privileges on {\tt rw0} for the first instance of the task 
and {\tt rw1} for the second instance.  Since we can prove these regions are 
disjoint the tasks are non-interfering and therefore safe to run in parallel.

Reviewer 1 also wondered what would happen if the programmer mistakenly unpacked
{\tt p0} as {\tt p1}.  The reviewer correctly identifies that the type system will not 
reject this program.  However, the program will run safely in this case.  The
runtime system will dynamically detect that the two instances of each task actually use
the same sets of regions and will serialize the tasks in program order.  The
execution of the program is therefore well defined, even though there is a bug
that will likely lead to an incorrect simulation.

Reviewer 2 commented that Legion does not provide a full memory model by defining
both memory coherence and memory consistency rules.  In the case of {\em exclusive} and
{\em atomic} coherence modes, the Legion programming model provides consistency at the
granularity of regions - synchronization between tasks at a finer granularity than that
this is unlikely to be performant in a distributed memory system.
Per-location memory consistency rules are relevant for cases in which multiple tasks
use {\em simultaneous} coherence.  The Core Legion operational semantics as defined
in this paper require sequential consistency by insisting on a total
ordering of all reads and writes performed by all tasks, but a relaxation of the
memory trace interleaving rules (i.e. the {\em valid\_interleave} predicate) to better
match the consistency models of underlying hardware (e.g.
total-store ordering on x86, non-coherent caches on GPUs, etc.) would be a straightforward
extension of these semantics.

\end{document}
