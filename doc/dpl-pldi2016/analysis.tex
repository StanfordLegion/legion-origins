\section{Static Analysis}
\label{sec:analysis}

This section describes the static analysis used to discharge most DPL assertions at compile time.
We first describe a sound and complete version of the analysis that can be used in the absence
of complex filter operations.
We then cover the problems caused by the introduction of complex filter operations, and present
a modified version of the algorithm that must necessarily be incomplete, but for which an efficient dynamic
check can be defined to handle cases where the static analysis fails to yield a yes/no answer.

At a high level, the analysis proceeds as follows:
\begin{enumerate}
\item Extract the loop-free subset of a DPL program that culminates in the assertion(s) to be checked.
%\item Perform a symbolic execution of the subset, replacing references to fields with concrete functions.
\item Translate the DPL statements into a formula in a fragment of first-order logic that may contain quantifiers, linear arithmetic,
and uninterpreted functions, but is restricted enough to remain decidable.
\item Translate the formula into an equisatisfiable one that is quantifier-free.
\item Use a decision procedure for the theory of Presburger arithmetic and uninterpreted functions
to decide the satisfiability of this final formula.
\end{enumerate}

The first two steps are straightforward.
%As mentioned earlier, the immutability and lexical scoping of index spaces means that the subset
%of a DPL program needed for a given assertion amounts to the text of the program up to
%and including the assertion statement(s).
The immutability and lexical scoping of index spaces allows a syntax-directed translation
of the DPL program to generate the necessary formulas in linear time.  The translation maintains the following state:
\begin{itemize}
\item The names of index spaces in scope, with unary predicates $P_I(x)$ that capture membership in each index space.
  The forms this predicate can take are described below.
  %At the end of a for loop, all index spaces defined in that loop are deleted.
\item The for loop variables in scope, with any known bounds.
\item The mapping $\mathbf{F}$ of DPL functions and currently immutable fields to function symbols --- a field is assigned a fresh function symbol for the duration of an {\tt immutable} block.
  %at the start of an {\tt immutable} block and the association is removed at the end of the block.
  (When it does not appear in the immutable set, a field is given a fresh function symbol for each occurrence.)
%  When an index space expression uses a field
%  that is not in the immutable set, a fresh function symbol is used for each occurrance.)
\item The properties associated with each DPL function.  These take the form of binary predicates $P_f(x,f(x))$.  For convenience,
  a field (or a function with no properties) has a predicate that is simply $\top$ ({\em true}).
\end{itemize}

\begin{figure}
  \scriptsize
  \begin{tabular}{l@{ }l}
    {\bf idx} A = {\bf ispace}({\bf int32}); & -{}- $P_A(x) \equiv \textit{int32}(x)$ \\
    {\bf field} f : A$\rightarrow$A; \\
    {\bf function} g : {\bf int32}$\rightarrow${\bf int32}; & -{}- $\textbf{F}[g] = \beta$ \\
    {\bf property} g(x) = x + 1; & -{}- $P_g(x,y) \equiv y = x + 1$ \\
    \\
    {\bf immutable} f \{ & -{}- $\textbf{F}[f] = \alpha$ \\
    \quad{\bf idx} Y = A$\rightarrow$f; & -{}- $P_Y(x) \equiv \exists y, x = \alpha(y) \wedge P_A(y)$ \\
    \quad{\bf idx} W = A - Y; & -{}- $P_W \equiv P_A(x) \wedge \neg P_Y(x)$ \\
    \quad{\bf idx} Z = W$\leftarrow$g; & -{}- $P_Z(x) \equiv \exists y, \beta(x) = y \wedge P_g(x,y) \wedge P_W(y)$ \\
    \\
    \quad{\bf assert} Y $*$ Z; & -{}- $\text{SAT}[\ P_Y(x) \wedge P_Z(x)\ ]$ \\
    \}
  \end{tabular}
%% \begin{lstlisting}[language=DPL]
%% idx A = ispace(ptr);        -- P_A(x) = ptr(x)

%% field f : A->A;
%% func g : A->A;              -- F[g] = \beta
%% property g(x) = x + 1;      -- P_g(x,y) = y = x + 1

%% immutable f {               -- F[f] = \alpha
%%   idx Y = A->f;             -- P_y(x) = \exists y, x = \alpha(y) ^ P_A(y)
%%   idx W = A - Y;            -- P_W(x) = P_A(x) ^ \neg P_Y(x)
%%   idx Z = W<-g;             -- P_Z(x) = \exists y, \beta(x) = y \wedge y = x + 1 \wedge P_W(y)

%%   assert Y * Z;             -- SAT[ P_Y(x) ^ P_Z(x) ]
%% }
  %% \end{lstlisting}
\vspace*{-3mm}
\caption{Example of syntax-directed translation of DPL\label{fig:analysis_ex}}
\vspace*{-6mm}
\end{figure}

When a line of the program contains index space expressions, they are parsed and a fresh predicate is created for each
subexpression.  A new index space created using the {\tt ispace} keyword places constraints based on the base type and any
supplied bounds:

\noindent{\small$\begin{array}{@{}l@{\ }c@{\ }l}
C = \texttt{ispace}(T)        & \Longrightarrow & P_C(x) \equiv P_T(x) \\
C = \texttt{ispace}(T, v_1, v_2)        & \Longrightarrow & P_C(x) \equiv P_T(x) \wedge x \geq v_1 \wedge x \le v_2
\end{array}$}


\noindent Set operations result in standard logical connectives:

\noindent{\small$\begin{array}{@{}l@{\ }c@{\ }l}
C = A\ \&\ B;               & \Longrightarrow & P_C(x) \equiv P_A(x) \wedge P_B(x) \\
C = A\ |\ B;               & \Longrightarrow & P_C(x) \equiv P_A(x) \vee P_B(x) \\
C = A - B;                & \Longrightarrow & P_C(x) \equiv P_A(x) \wedge \neg P_B(x)
\end{array}$}

\vspace*{1mm}
\noindent Image and preimage operations yield existential quantifiers:

\noindent{\small$\begin{array}{@{}l@{\ }c@{\ }l}
C = A \rightarrow f & \Longrightarrow & P_C(x) \equiv \exists y, x = \mathbf{F}[f](y) \wedge P_f(y,x) \wedge P_A(y) \\
C = A \leftarrow f & \Longrightarrow & P_C(x) \equiv \exists y, \mathbf{F}[f](x) = y \wedge P_f(x,y) \wedge P_A(y)
\end{array}$}

\vspace*{1mm}
\noindent Finally, the translation of a simple filter depends on whether the index is used directly or if a field lookup is performed
(in which case it is very similar to a preimage):

\noindent{\small$\begin{array}{@{}l@{\ }c@{\ }l}
C = A\{ x\ |\ \phi(x) \} & \Longrightarrow & P_C(x) \equiv P_A(x) \wedge \phi(x) \\
C = A\{ x\ |\ \phi(x, f(x)) \} & \Longrightarrow & P_C(x) \equiv P_A(x) \wedge \\ && \exists y, \mathbf{F}[f](x) = y \wedge P_f(x,y) \wedge \phi(x,y)
\end{array}$}

\vspace*{1mm}
Whereas index space expressions generate unary predicates, DPL {\tt assert} statements use those predicates to yield
statements whose validity can be tested.  As is standard, these statements are negated, yielding an existential statement
suitable for a satisfiability query:

\noindent{\small$\begin{array}{@{}l@{\ }c@{\ }l}
\texttt{assert } A \leq B & \Longrightarrow & \exists x, P_A(x) \wedge \neg P_B(x) \\
\texttt{assert } A * B & \Longrightarrow & \exists x, P_A(x) \wedge P_B(x)
\end{array}$}

\vspace*{1mm}
Figure~\ref{fig:analysis_ex} gives an example DPL program and its translation into logic.  The first step
of the analysis is
to expand all of the predicates, yielding the following SAT problem.  The assertion is valid if the formula is unsat.

\noindent{\small
  $\begin{array}{@{}l@{}l}
    \text{SAT}[ (\exists y, x = \alpha(y)) \wedge (\exists z,& \beta(x) = z \wedge z = x + 1\ \wedge \\
      & \neg (\exists w, z = \alpha(w))) ]
    \end{array}$}

The base type predicates $\textit{int32}(\cdot)$ have been eliminated and all variables have been given unique names for clarity,
but the predicate expansion has duplicated the subformula for $P_Y$.  This is necessary for our decision
procedure below, but we note that it can result in a formula that is exponential in the size of the original program in
the worst case.

\begin{figure}
  \scriptsize
  $\begin{array}{l}
    \textbf{function } \text{DPLSAT}(\phi) \\
    \quad\rho := \top \\
    \quad\textbf{while }\neg \textit{quantifier-free}(\phi) \\
    \quad\quad\textbf{select }\text{top-level quantifier }\exists y, \psi(\bar x, y) \text{ in } \phi \\
    \quad\quad\textbf{if }\textit{coinflip}() = \textit{true} \\
    \quad\quad\quad\phi := \phi[\exists y, \psi(\bar x, y)/\top] \wedge psi(\bar x, y') \\
    \quad\quad\textbf{else} \\
    \quad\quad\quad\phi := \phi[\exists y, \psi(\bar x, y)/\bot] \\
    \quad\quad\quad\rho := \rho \wedge \neg \psi(\bar x, q)\quad\quad\quad\textit{-{}- q does not appear in $\psi$} \\
    \quad\textbf{if }\rho \equiv \top \\
    \quad\quad\textbf{return }\text{PAUFSAT}(\phi) \\
    \quad\textbf{else} \\
    \quad\quad\textbf{for }v\textbf{ in }\textit{free-vars}(\phi) \\
    \quad\quad\quad\phi := \phi \wedge \rho(\bar x, v) \\
    \quad\quad\textbf{return }\text{DPLSAT}(\phi)
  \end{array}$
%% \begin{lstlisting}
%% function DPLSAT(phi)
%%   rho = top
%%   while !quantifier-free(phi)
%%     select top-level quantifier exists y, psi(x,y) in phi
%%     if coinflip == true then
%%       phi = phi[exists y, psi(x,y)/top] ^ psi(x,y')]
%%     else
%%       phi = phi[exists y, psi(x,y)/bot]
%%       rho = rho ^ neg psi(x,q)  -- q does not in appear in psi
%%   if rho == top
%%     return PAUFSAT(phi)
%%   else
%%     for v in free-vars(phi)
%%       phi = phi ^ rho(x,v)
%%     return DPLSAT(phi)
%% \end{lstlisting}
\vspace*{-3mm}
\caption{DPLSAT Pseudocode\label{fig:dplsat}}
\vspace*{-7mm}
\end{figure}

The final two steps of our strategy are also combined in the form of a {\bf DPLSAT} procedure that decides the satisfiability
of our statements, using a known decision procedure for the theory of quantifier-free Presburger arithmetic and uninterpreted functions
({\bf PAUFSAT}) as a subroutine\cite{Shostak1979}.  Figure~\ref{fig:dplsat} gives pseudocode for a non-deterministic form of the {\bf DPLSAT}
algorithm, and we describe the intuition here.

The {\bf DPLSAT} algorithm uses case analysis to eliminate quantifiers, exploring the subset of possible models in which a
given quantified expression holds and those for which it does not hold.  This is captured by the following transformation,
which is generally applicable.  A top-level existential quantifier (i.e. one not contained in another quantifier) is
removed from the main formula and either added as a condition on a new free variable in the ``assumed true'' case or in
its negated form (i.e. a universally quantified axiom) in the ``assumed false'' case.

\vspace*{1mm}
$\begin{array}{l@{}l}
\phi(\bar x, \exists y \psi(\bar x, y)) \Leftrightarrow^{SAT} ( & \psi(\bar x, \top) \wedge \psi(y^+))\ \vee \\
& (\forall y^-, \neg \psi(\bar x, y^-) ) \wedge (\phi(\bar x, \bot))
\end{array}$

Each case is further subdivided until $\phi$ becomes quantifier-free.  If more than one quantifier was
``assumed false'', their bodies are conjoined, resulting in a statement of the form:

$(\forall q, \rho(\bar x, \bar y, q)) \wedge \phi_{QF}(\bar x, \bar y)$

The universal quantifier can now be instantiated for all free variables in $\phi_{QF}$.
%, but
%the formula $\rho$ may still contain quantifiers, presenting a problem for forward progress in the general case.
%When these newly introduced quantifiers are eliminated, new free variables are created, for which $\rho$ must be
%instantiated again.
%However,
This preserves equisatisfiability because the statements generated for DPL assertions fit within a very restricted fragment
of first-order logic, allowing the elimination of the universally quantified axiom from the statement once it has been
instantiated for all free variables that exist at the time.  The body of every existential quantifier in
a DPL-generated formula is in $\text{FO}^2$ (i.e. first-order logic with at most 2 free variables in any subformula), which means
that the body of a nested quantifier can only refer to the most immediately enclosing quantified variable.  Further,
each body contains exactly one term that uses a function, and it is always of the form $f(x) = y$, i.e. a functional dependence
between the quantified variable and the enclosing one.  As a result, when these bodies are negated, the
resulting $\rho$ includes a disjunction of disequalities.  In the instantiation of $\rho$ for one of the free variables
in $\phi_{QF}$,
these disequalities may be incompatible with functional dependences already present in $\phi_{QF}$, making the
rest of $\rho$ relevant for satisfiability.
However, any new quantifier created through this instantiation will create a new free variable that has
a functional dependence with exactly one existing variable.  The instantiation of the universal quantifier for
this new variable always results in a disequality that can be trivially satisfied.
%and these
%subsequent instantiations of the ``old'' $\rho$ have no impact on the satisfiability of the statement.
With this constraint, the {\bf DPLSAT} algorithm is guaranteed to terminate and we have the following bounds on its complexity.

\begin{theorem}
  {\bf DPLSAT} runs in NEXPTIME (and requires EXPSPACE).
\end{theorem}

Briefly, each recursive call to {\bf DPLSAT} may quadratically
increase the number of quantifiers in $\phi$ in the worst case, but it reduces the maximum nesting depth of these quantifiers
by at least one level.  The loop in each call to {\bf DPLSAT} is linear in the number of quantifiers occurring in $\phi$.
In practice, these worst-case bounds can be dramatically improved by performing simplifications on $\phi$ and $\rho$ within
the %quantifier-elimination
loop, pruning that part of the search if either becomes unsatisfiable.

Returning to our example, we have 3 quantifiers, and a maximum nesting level of 2.  There are 6 ways in which the
nondeterminstic choices in the quantifier-elimination loop can complete (if $z$ is assumed unsatisfiable, $w$ disappears from
$\phi$ as well).  Only one results in a new $\phi$ that isn't trivially unsatisfiable - i.e. the case in which
$y$ and $z$ are hypothesized to be satisfiable, but $w$ is not.  This case yields:

\noindent{\small$\begin{array}{l}
  \phi = \top \wedge \top \wedge \top \wedge f(y^+) = x \wedge (g(z^+) = x \wedge z^+ = x + 1) \\
  \rho = f(q) \not= z^+
\end{array}$}

\noindent The simplification of $\phi$ is not able to eliminate any variables, so $\rho$ is instantiated 3 times, yielding:

\noindent{\small$\begin{array}{l}
    f(y^+) = x \wedge g(z^+) = x \wedge z^+ = x + 1\ \wedge \\
    \quad\quad\quad f(x) \not= z^+ \wedge f(y^+) \not= z^+ \wedge f(z^+) \not= z^+
    \end{array}$}

\noindent which can be easily satisfied (e.g. $x = 0, y^+ = 0, z^+ = 1, f(\cdot) = 2$).  This satisfying assignment yields a model
for the original formula and we have shown the assertion in the DPL code in Figure~\ref{fig:analysis_ex}
to be incorrect under some possible execution of the program.  Had the code made $g$ an identity function instead of a
successor function, the instantiated
formula would contain a contradiction (i.e. $f(y^+) = x \wedge z^+ = x \wedge f(y^+) \not= z^+$), rendering it unsatisfiable and
the original assertion would be discharged.

This divide-and-conquer approach to the problem is similar to the techniques used in SMT solvers such as Z3\cite{Z3}
and CVC4\cite{CVC4}.  The main difference is while SMT solvers rely on heuristics to guess which cases to try, we are
able to precisely enumerate the cases that matter.  Interestingly, our bounds on the complexity of {\bf DPLSAT} suggest that a
heuristic approach in which universal quantifiers are only instantiated on demand will also have a bounded execution time.
Anecdotally, this appears to be the case - the Z3 SMT solver took under a second to prove the validity of each
assertion in our test cases.

\subsection{Complex Filter Operations}
\label{subsec:complex}

Our algorithm above relies on efficient queries for quantifier-free formulas,
%individual leaf node satisfiability queries
%can be efficiently performed.
which are available only in the absence of complex filter operations.
In complex filter operations, the composition of multiple functions, or a
function with arithmetic, effectively creates additional free variables,
extending the formula to $\text{FO}^3$ and beyond, fragments that are known to be undecidable\cite{logicbook}.
%beyond $\text{FO}^2$ and rendering our elimination of the universally quantified $\rho$ after instantiation
%unsound.

As discussed in Section~\ref{subsec:limits}, complex filter operations are necessary to express many common application
uses cases, so we extend our analysis attempt to discharge these assertions either entirely at compile time or with a
dynamic test that is more efficient than an explicit test of the assertion condition.  We use three heuristics.

For the first heuristic, we observe that any complex filter
can be replaced by a simple filter using a fresh boolean function and the resulting formula is weaker
than the original.  Therefore, we can start by replacing all complex filters operations in this manner and test for
satisfiability.  If the modified formula proves to be unsatisfiable, the original formula must be as well and we
have validated the assertion.  If a satisfying assignment is found, we test it against the complex filter predicates that
were removed.  If it is compatible with these predicates, we have a true positive and the original assertion is invalid.
However, if at least one of the complex predicates is not satisfied, the assignment represents a false positive and we
continue with the next heuristic.

The second heuristic also attempts to avoid runtime checks entirely, and takes the pragmatic approach of adding
back in the problematic predicate(s) and hoping that a modern SMT solver can handle it.  As termination is no longer guaranteed,
a timeout must be set on the query, but this works well in practice.  Z3,
with its inbuilt heuristics,
%likely with the help of additional heuristics beyond those related to quantifier instantiation,
yielded unsat results for all of our test cases in just a few seconds.

The final heuristic attempts to simplify the runtime check rather than eliminating it entirely, and targets
the case where these complex filter operations are used to ``sanitize'' input data.  An example of this was seen with PENNANT
in Section~\ref{subsec:limits}.  While this filtering is critical for the correctness of the program,
it is expected that application mesh data structures will be well-formed in practice.
%When a complex filter predicate is not addressed by either of the first two heuristics, this final attempt
This heuristic tries replacing a complex filter with trivial predicates that either include
all or none of the elements of the input index space and retesting the assertion.  If either is successful, an additional runtime
check is added that tests this intermediate result for equality or emptiness (depending on which predicate succeeded).  These
runtime checks are trivial, and if they succeed, the more expensive check can be skipped.
