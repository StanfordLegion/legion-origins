\section{Dependent Partitioning in Regent}
\label{sec:regent}

Our dependent partitioning framework is intended to be
applicable to a variety of programming models.  In this section, we discuss
its implementation in Regent\cite{Regent15}, a language designed to target
the Legion runtime. The Regent data model and type system make it easy
to embed DPL partitioning operations, and the compiler can usually
insert the necessary consistency checks automatically.
%Regent's data model and type
%system\cite{LegionTypes13} enable the
%dependent partitioning operations to be easily defined and the consistency
%checks can generally be introduced automatically by the compiler.

%In Regent, a (logical) region is conceptually an array of structures.  A
%{\em field space} defines which fields exist for each element of the
%region, which is indexed either by opaque pointers or normal array indices,
%depending on how the region is declared.
%A Regent program is divided into
%tasks that modify regions in accordance with their requested privileges,
%allowing the compiler and runtime to operate on a program with sequential
%semantics and extract the implicit parallelism in it based on non-interference
%of the tasks.

Data in a Regent program is stored in {\em regions}, each of which has one or
more fields.
Regent includes a first-class object called a {\em partition} which is
equivalent to an array of subregions.
%The values used to look up a particular subregion are called {\em colors}.
The dependent partitioning operations in our framework must be extended to
work on these arrays.
%The implementation of the dependent
%partitioning operations must take this into account,
%but this improves over a direct transliteration because it captures more
%data reuse.

Partitions are created in Regent by supplying a region to the
{\tt partition} operation along with a {\em coloring} object.  A coloring
is an abstract data type that allows the application to describe any
association of colors (generally small integers) to elements of a region.
The operation creates an array of subregions where each subregion
contains all the elements of the parent region of a particular color.

%For example, the coloring required for the independent {\tt owned\_nodes}
%coloring in our original example could be computed in Regent as follows:
%
%\begin{lstlisting}[language=Regent]
%var c = c.legion_coloring_create()
%for r in r_nodes do
%  c.legion_coloring_add_point(c, n.subckt_id, n)
%end
%\end{lstlisting}

A coloring may assign any number of colors (or none) to each element in a
region and must allow random access, as colorings are often computed by
following the topology of the application's data structures.  Regent uses
Legion's standard map-of-sets-of-indices representation.
%which, while very general and simple to use, is suboptimal in nearly all
%cases.
Ideally, other representations would be available that optimize for 
%implementation of the coloring object would also understand
various common
sparsity patterns, but this would complicate the interface significantly.
%We feel that a better answer is to eliminate the coloring object entirely
%and allow the lower-level implementation in Legion to choose data structures
%that are suited for the observed sparsity patterns in a given run of the 
%program.
Our dependent partitioning framework lets us eliminate the coloring object
entirely, instead using a field to represent the coloring.
%permits us to do just that
%by allowing the coloring to itself be represented as a field.
The result is the deletion of Regent's existing {\tt partition}
operation and coloring-related interfaces, and the following new dependent
partitioning operations:

{\centering{
\begin{tabular}{l}
{\bf partition}({\em parent\_region}, {\em field}, $N$) \\
{\bf image}({\em parent\_region}, {\em source\_partition}, {\em field}) \\
{\bf preimage}({\em parent\_region}, {\em target\_partition}, {\em field}) \\
{\bf union}({\em region\_or\_partition}, $\ldots$) \\
{\bf intersection}({\em region\_or\_partition}, $\ldots$) \\
{\bf difference}({\em region\_or\_partition}, {\em region\_or\_partition}) \\
{\bf block\_split}({\em parent\_region}, $N$)
\end{tabular}

}}

\vspace*{1mm}
The new {\tt partition} operation takes a parent region, a field, and the 
number of subregions to create.  For each $i$ in $[0,N)$, a simple filter
defines the subregion:
  
${\textit subregion}_i = {\textit parent}\{ x\ |\ x\rightarrow{}field = i \}$

\vspace*{1mm}
The {\tt image} and {\tt preimage} operations accept an existing partition
(i.e. an array of subregions) and map an image (or preimage) over the elements,
returning a new array of image (or preimage) subregions:

${\textit image}_i = {\textit parent}\ \&\ ({\textit source}_i\rightarrow{}field)$

\vspace*{1mm}
\noindent The additional intersection with a specified {\tt parent\_region} is needed
for Regent's type system and can be efficiently fused with the image/preimage
computation in the runtime.

The set operations accept either regions or partitions as arguments.  If
any of the arguments are partitions,
%they must all have the same number of subregions, and
the set operation is mapped over the elements of the arrays,
yielding a new partition.
%(Any arguments that
%are regions in this case are effectively replicated for each element of the
%map.)
The {\tt union} and {\tt intersection} operations may also be called
with a single partition as an argument, in which case the elements of that
partition are reduced to a single region that is the union (or intersection)
of all the subregions of the original partition.

Finally, the {\tt block\_split} operation allows a region to be partitioned
into $N$ (roughly) equal subregions using arithmetic on 
the indices themselves.  This operation can be performed before
storage is allocated for the region's fields.

\begin{figure}
\begin{lstlisting}[language=Regent]
  var p_nodes_eq = block_split(all_nodes, N)
  var p_wires_eq = block_split(all_wires, W)

  for i in 0, N do load_circuit(p_nodes_eq[i], p_wires_eq[i]) end
  
  var p_nodes = partition(all_nodes, subckt, N)
  var p_wires = preimage(all_wires, p_nodes, in_node)
  var p_extern = difference(image(p_nodes, p_wires, out_node), p_nodes)
  var all_shared = union(p_extern)
  var all_private = difference(all_nodes, all_shared)
  var p_pvt = intersection(p_nodes, all_private)
  var p_shr = intersection(p_nodes, all_shared)
  var p_ghost = intersection(p_extern, all_shared)

  fspace Subcircuit(rn : region(Node), rw : region(Wire(rn, rn, rn))) {
    rp : region(Node),
    rs : region(Node),
    rg : region(Node),
    wp : region(Wire(rp, rs, rg)),
    ...
  } where rp * rs * rg, rp <= rn, rs <= rn, rg <= rn, wp <= rw

  for i = 0, N do
    subckt = Subcircuit(all_nodes, all_wires) {
      rp = p_pvt[i],
      rs = p_shr[i],
      rg = p_ghost[i],
      wp = p_wires[i],
      ...
    }
\end{lstlisting}
\vspace*{-4mm}
\caption{Regent circuit simulation with new partitioning operations \label{fig:circuit_rg}}
\vspace*{-6mm}
\end{figure}

Figure~\ref{fig:circuit_rg} shows the new partitioning code for the circuit
simulation in Regent.  When a Regent program is modified to use these new
dependent partitioning
operations, there are four benefits that are realized:
\begin{enumerate}
\item The number of partitioning operations remains roughly the same, but
change from a sequence of nondescript {\tt partition} calls to
a self-documenting description of the partitioning computation.
\item All the code related to creating and populating the coloring objects 
can be deleted.
\item Significant improvements in partitioning time, due to the runtime's
ability to choose optimized data structures (discussed in the next section)
and (in nearly all cases) the elimination of dynamic consistency checks.
\item The program's input file loading or generation code can
be distributed as well, allowing the application to
work on data structures that are too large to fit on a single node.
\end{enumerate}

Consistency checks are generated by Regent when pointers or regions are ``downcast'' to
a more restrictive type.  An example of this is in lines 24-29 of
Figure~\ref{fig:circuit_rg}.  To discharge these checks during compilation,
the Regent compiler must 
perform the static analysis described in Section~\ref{sec:analysis}.  Rather than
implementing the analysis a second time, the Regent compiler translates the
relevant parts of the program into DPL and performs the analysis on the DPL version.

Regent allows a programmer to assert that they expect iterations of a loop to
be able to run in parallel.  This requires static analysis to verify there are no
loop-carried dependencies, which often demands that the regions accessed in each
iteration are non-overlapping.  The current Regent compiler can perform the needed
analysis only in simple cases.
%do so only when the
%regions involved are derived from a {\tt partition} operation with a disjoint coloring
%(i.e. no element is assigned more than one color).
With the use of the dependent partitioning framework, many cases that could
not be checked before (such as the red-black Gauss-Seidel example in Section~\ref{sec:dpl})
are handled.
