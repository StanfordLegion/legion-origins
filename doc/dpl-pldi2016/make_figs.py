#!/usr/bin/python

import subprocess
import sys, os, shutil
import string, re
import getopt
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['font.family'] = 'serif'

tableau1 = (0.968,0.714,0.824)
tableau2 = (0.882,0.478,0.470)
tableau3 = (0.565,0.663,0.792)
tableau4 = (0.635,0.635,0.635)
tableau5 = (0.678,0.545,0.788)
tableau6 = (1.000,0.620,0.290)
tableau7 = (0.859,0.859,0.553)
tableau8 = (0.769,0.612,0.580)
tableau9 = (0.478,0.757,0.424)
tableau10= (0.427,0.800,0.855)
tableau11= (0.929,0.592,0.792)
tableau12= (0.929,0.400,0.364)
tableau13= (0.447,0.620,0.808)
tableau14= (0.780,0.780,0.780)
tableau15= (0.773,0.690,0.835)
tableau16= (0.882,0.616,0.353)
tableau17= (0.804,0.800,0.365)
tableau18= (0.659,0.471,0.431)
tableau18= (0.404,0.749,0.361)
tableau19= (0.137,0.122,0.125)

# single-node runs
sng_pennant = (
    # pennant.tests/sedovbig1x30/sedovbig.pnt -npieces 16
    ('540', 98120, 20475, 191469, 67251, 40753, 40161, 11064),
    # pennant.tests/sedovbig3x30/sedovbig.pnt -npieces 16
    ('1620', 981627, 264417, 1386518, 621426, 397287, 427591, 129919),
    # pennant.tests/sedovbig1x30/sedovbig.pnt -npieces 16
    ('4320', 7572278, 1799914, 9759816, 4708321, 2872307, 2700757, 812254),
    )

sng_circuit = (
    # -p 16 -npp 4166 -wpp 6250
    ('100K', 54122, 6359, 45856, 37406, 13082, 23342, 10784),
    # -p 16 -npp 41666 -wpp 62500
    ('1M', 752820, 96268, 195822, 416642, 159027, 212407, 69817),
    # -p 16 -npp 416666 -wpp 625000
    ('10M', 10502980, 895197, 1584112, 6425585, 1776025, 2686637, 881674),
    )

sng_miniaero =  (
    # -mesh 50x50x50 -blocks 16
    ('50^3', 249062, 13394, 54282, 23407, 27907, 17366, 8834),
    # -mesh 100x100x100 -blocks 16
    ('100^3', 1883663, 81169, 393404, 161044, 214382, 121181, 59896),
    # -mesh 200x200x200 -blocks 16
    ('200^3', 13926887, 598578, 2816817, 1143857, 1200989, 874386, 486871),
    )

def summarize(*vals):
    a = np.array(list(vals))
    return { 'avg': np.mean(a),
             'std': np.std(a),
             'min': min(*vals),
             'max': max(*vals),
             }

def collect_exp_data(pattern, paths, filter = None):
    times = {}
    for p in paths:
        for dirpath, dirnames, filenames in os.walk(p):
            for f in filenames:
                m = re.match(pattern, f)
                if m is None:
                    continue
                key = int(m.group(1))
                t = None
                with open(os.path.join(dirpath, f), 'r') as ff:
                    for l in ff:
                        m = re.search(r'dependent partitioning work (\d+(\.\d+)?)', l)
                        if m is not None:
                            times[key] = times.get(key,[]) + [ float(m.group(1)) ]
    return dict((k, summarize(*v)) for k, v in times.iteritems())
            

def darken(c, factor):
    return tuple(x*factor for x in c)

def single_node_plot(outfile = None, show = False, normalize = True):
    cases = []
    x = 1
    for ss in (('PENNANT', sng_pennant),
               ('Circuit', sng_circuit),
               ('MiniAero', sng_miniaero)) :
        for i, c in enumerate(ss[1]):
            cc = dict(x = x,
                      label = '$' + c[0] + '$',
                      y_app = c[1]*1e-6,
                      y_old = (c[1]+c[2])*1e-6,
                      y_ock = (c[1]+c[2]+c[3])*1e-6,
                      y_new1 = c[4]*1e-6,
                      y_nck1 = (c[4]+c[5])*1e-6,
                      y_new4 = c[6]*1e-6,
                      y_nck4 = (c[6]+c[7])*1e-6,
                  )
            if i == (len(ss[1]) >> 1):
                cc['label'] += '\n' + ss[0]
            cases.append(cc)
            x += 1
        x += 0.5

    if normalize:
        for c in cases:
            for k in c:
                if k.startswith('y_'):
                    c[k] /= c['y_old']

    fig = plt.figure(figsize = (7,5))

    xr = (cases[0]['x'] - 1, cases[-1]['x'] + 1)
    plt.plot(xr,
             (1, 1),
             color='black')

    plt.xlim(xr)

    w = 0.25

    
    nck4 = plt.bar(list(c['x']+0.5*w for c in cases),
                   list(c['y_nck4'] for c in cases),
                   width=w,
                   edgecolor=tableau6,
                   color='none',
                   hatch='////',
                   label='Verification')

    new4 = plt.bar(list(c['x']+0.5*w for c in cases),
                   list(c['y_new4'] for c in cases),
                   width=w,
                   color=tableau6,
                   edgecolor=darken(tableau6, 0.5),
                   label='Computation')

    nck1 = plt.bar(list(c['x']-0.5*w for c in cases),
                   list(c['y_nck1'] for c in cases),
                   width=w,
                   edgecolor=tableau4,
                   color='none',
                   hatch='////',
                   label='Verification')

    new1 = plt.bar(list(c['x']-0.5*w for c in cases),
                   list(c['y_new1'] for c in cases),
                   width=w,
                   color=tableau4,
                   edgecolor=darken(tableau4, 0.5),
                   label='Computation')

    ock = plt.bar(list(c['x']-1.5*w for c in cases),
                  list(c['y_ock'] for c in cases),
                  width=w,
                  edgecolor=tableau10,
                  color='none',
                  hatch='////',
                  label='Verifcation (Old)')

    old = plt.bar(list(c['x']-1.5*w for c in cases),
                  list(c['y_old'] for c in cases),
                  width=w,
                  color=tableau10,
                  edgecolor=darken(tableau10, 0.5),
                  label='Runtime Time')

    app = plt.bar(list(c['x']-1.5*w for c in cases),
                  list(c['y_app'] for c in cases),
                  width=w,
                  color=darken(tableau10, 0.85),
                  edgecolor=darken(tableau10, 0.5),
                  label='App Time')

    dummy = plt.bar([0.5], [0], width=0.1, color='white', edgecolor='none')

    ax = fig.get_axes()[0]
    #plt.xticks(list((c['x'], c['label']) for c in cases))
    ax.tick_params(axis='x', direction='out')
    ax.set_xticks(list(c['x'] for c in cases))
    ax.set_xticklabels(list(c['label'] for c in cases))
    ax.xaxis.tick_bottom()

    plt.ylabel('Execution Time (Normalized)')

    plt.legend([ ock, old, app,
                 dummy, nck1, new1,
                 dummy, nck4, new4 ],
               [ 'Verification (old)',
                 'Runtime Cost (old)',
                 'App Cost (old)',
                 '1 thread',
                 'Verification',
                 'Runtime',
                 '4 threads',
                 'Verification',
                 'Runtime' ],
               fontsize=11,
               ncol=3,
               loc='upper right')
    
    if show:
        plt.show()
        
    if outfile:
        fig.savefig(outfile, format='pdf', bbox_inches='tight', pad_inches=0.1)


def do_plot(series,
            xrange=None, yrange=None,
            xticks=None, yticks=None,
            xlabel=None, ylabel='Time (seconds)',
            legend=True,
            outfile = None, show = False):
    fig = plt.figure(figsize = (5.5,3.8))

    if xrange:
        plt.xlim(*xrange)

    if yrange:
        plt.ylim(*yrange)

    if xlabel:
        plt.xlabel(xlabel)

    if ylabel:
        plt.ylabel(ylabel)

    if xticks:
        plt.xticks(xticks)    

    for name, data in series:
        xs = sorted(data)
        ys = list(data[k]['avg'] for k in xs)
        print xs, ys
        plt.plot(xs, ys, marker='o', markersize=10, label=name)

    if legend:
        plt.legend(loc='upper right')

    if show:
        plt.show()

    if outfile:
        fig.savefig(outfile, format='pdf', bbox_inches='tight', pad_inches=0.1)

without_cema=[679,179,725,225]
with_cema=[730,180,842,244]

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        plt.text(rect.get_x()+rect.get_width()/2., height+0.1, '%.2f'%height,
                ha='center', va='bottom')

def make_plot(show = True, save = True):
    N = 4
    ind = np.arange(N)
    width = 0.35
    offset = 0.175
    fig, ax = plt.subplots()

    without_perstep = map(lambda t: float(t)/float(100), without_cema)
    with_perstep    = map(lambda t: float(t)/float(100), with_cema)
    #without_throughput = map(lambda t: float(48*48*48*100)/float(t), without_cema)
    #with_throughput = map(lambda t: float(48*48*48*100)/float(t), with_cema)

    rects1 = ax.bar(offset+ind, without_perstep, width, color=tableau2)
    rects2 = ax.bar(offset+ind+width, with_perstep, width, color=tableau3)
    ax.set_ylabel('Time per Time Step (s)', fontsize=16)
    ax.set_ylim((0,10))
    #rects1 = ax.bar(offset+ind, without_throughput, width, color=tableau2)
    #rects2 = ax.bar(offset+ind+width, with_throughput, width, color=tableau3)
    #ax.set_ylabel('Throughput Per Node (Points/s)', fontsize=16)
    plt.yticks(fontsize=16)
    ax.set_xticks(offset+ind+width)
    ax.set_xticklabels(('MPI Fortran\nPiz Daint', 'Legion\nPiz Daint',
                        'MPI Fortran\nTitan', 'Legion\nTitan'), fontsize=16)

    ax.legend( (rects1[0], rects2[0]), ('Without CEMA', 'With CEMA') , fontsize=16, loc=2)
    autolabel(rects1)
    autolabel(rects2)

    if show:
        plt.show()

    if save:
        print "Saving figure to cema_overhead.pdf"
        fig.savefig('cema_overhead.pdf',format='pdf',bbox_inches='tight',pad_inches=0.1)


#if __name__ == "__main__":
#    make_plot(("-s" not in sys.argv), ("-w" in sys.argv))

opts, args = getopt.getopt(sys.argv[1:], 'o:')
opts = dict(opts)

outdir = opts.get('-o')

# pennant scaling
if True:
    s = []
    for size in (30000, 10800, 4320):
        d = collect_exp_data(r'pennant_{:d}_(\d+)_4.txt'.format(size), args)
        #for k in sorted(d):
            #print '{:2d}: {:6.2f} {:6.2f} {:+4.1f} {:+4.1f}'.format(k, d[k]['avg'], d[k]['std'],
            #                                                        (d[k]['min'] - d[k]['avg']) / d[k]['std'],
            #                                                        (d[k]['max'] - d[k]['avg']) / d[k]['std'],
            #                                                    )
        s.append(('$\\textit{{nz}}={:d}$'.format(size), d))
    do_plot(s, xrange=[0,65], xticks=[1,4,8,12,16,24,32,40,48,56,64], xlabel = 'Node count',
            outfile = os.path.join(outdir, 'pennant_scaling.pdf') if outdir else None)
                                          
# miniaero scaling
if True:
    s = []
    for size in (1100, 600, 300):
        d = collect_exp_data(r'miniaero_{:d}_(\d+)_4.txt'.format(size), args)
        #for k in sorted(d):
            #print '{:2d}: {:6.2f} {:6.2f} {:+4.1f} {:+4.1f}'.format(k, d[k]['avg'], d[k]['std'],
            #                                                        (d[k]['min'] - d[k]['avg']) / d[k]['std'],
            #                                                        (d[k]['max'] - d[k]['avg']) / d[k]['std'],
            #                                                    )
        s.append(('$\\textit{{mesh}}={:d}^3$'.format(size), d))
    do_plot(s, xrange=[0,65], xticks=[1,4,8,12,16,24,32,40,48,56,64], xlabel = 'Node count',
            outfile = os.path.join(outdir, 'miniaero_scaling.pdf') if outdir else None)
                                          
# circuit scaling
if True:
    s = []
    for size in (18, 3):
        d = collect_exp_data(r'circuit_{:d}M_(\d+)_1.txt'.format(size), args)
        #for k in sorted(d):
            #print '{:2d}: {:6.2f} {:6.2f} {:+4.1f} {:+4.1f}'.format(k, d[k]['avg'], d[k]['std'],
            #                                                        (d[k]['min'] - d[k]['avg']) / d[k]['std'],
            #                                                        (d[k]['max'] - d[k]['avg']) / d[k]['std'],
            #                                                    )
        s.append(('$\\textit{{size}}={:d}M$'.format(size), d))
    do_plot(s, xrange=[0,65], xticks=[1,4,8,12,16,24,32,40,48,56,64], xlabel = 'Node count',
            outfile = os.path.join(outdir, 'circuit_scaling.pdf') if outdir else None)

# pennant isectopt
if True:
    s = []
    for opt in (0,1):
        d = collect_exp_data(r'isect{:d}_pennant_(\d+).txt'.format(opt), args)
        s.append(('normal' if opt else 'no opt', d))
    do_plot(s, xrange=[0,65], xticks=[1,4,8,12,16,24,32,40,48,56,64], xlabel = 'Node count',
            legend=False,
            outfile = os.path.join(outdir, 'pennant_isectopt.pdf') if outdir else None)
    

# miniaero isectopt
if True:
    s = []
    for opt in (0,1):
        d = collect_exp_data(r'isect{:d}_miniaero_(\d+).txt'.format(opt), args)
        s.append(('normal' if opt else 'no opt', d))
    do_plot(s, xrange=[0,65], xticks=[1,4,8,12,16,24,32,40,48,56,64], xlabel = 'Node count',
            outfile = os.path.join(outdir, 'miniaero_isectopt.pdf') if outdir else None)
    

# circuit isectopt
if True:
    s = []
    for opt in (0,1):
        d = collect_exp_data(r'isect{:d}_circuit_(\d+).txt'.format(opt), args)
        s.append(('normal' if opt else 'no opt', d))
    do_plot(s, xrange=[0,65], xticks=[1,4,8,12,16,24,32,40,48,56,64], xlabel = 'Node count',
            legend=False,
            outfile = os.path.join(outdir, 'circuit_isectopt.pdf') if outdir else None)
    
if True:
    single_node_plot(outfile = os.path.join(outdir, 'single_node.pdf'))
