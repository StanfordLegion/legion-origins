\section{Introduction}
\label{sec:intro}

%To achieve high performance on modern architectures, an application
%must identify sufficient parallel work to keep multiple processors busy and 
%minimize the cost of data movement by judiciously placing
%application data within complex and distributed memory
%hierarchies.
%The partitioning of an application's data is essential to both of these
%efforts, and presents a challenge in the design of high-performance
%programming models.  On the one hand, it is desirable to have an expressive
%partitioning system that allows applications to describe precisely what data
%is needed where.  On the other, it is desirable to have partitioning constructs
%for which analysis (whether at compile- or run-time) is tractable.
%A motivating example for the usefulness of analysis is given below.  Even apparently
%simple partitioning systems may have exponential or even undecidable analysis problems,
%so achieving tractability necessarily requires eliminating a great deal of expressivity.

The choice of data partitioning scheme has profound implications for the performance of
a parallel application, dictating how much parallelism is exposed as well as constraining
how the application data can be distributed across a complicated memory hierarchy.
A parallel programming model must provide abstractions to support partitioning, and tension
between a programmer's desire for expressivity (i.e. the ability to describe exactly the partitioning
desired) and the compiler and runtime's need for tractability (i.e. the ability to
perform analysis and optimizations) is unavoidable.

%For many applications, the choice of data partitioning scheme will have massive 
%ramifications on the performance of the resulting code. Despite this, few programming
%systems provide abstractions for understanding the partitioning of program data. 
%In many cases this severely limits opportunities for program analysis, transformation,
%and optimization. 

%More recently, several systems have introduced partitioning abstractions to 
%capture important partitioning information. Within this design space there 
%is a natural tension between supporting expressive programming abstractions and 
%enabling non-trivial program analysis that does not require exponential or 
%undecidable algorithms. Two recent efforts demonstrate this tension.

Existing efforts have generally chosen extreme points of the design space.
For example, the Chapel\cite{Chamberlain07} programming language provides a small set
of primitive {\em domain maps} such as
{\em blocked}, {\em cyclic}, or {\em replicated} distributions.  These domain maps concisely describe
common data distribution patterns.
%(e.g. {\em blocked} divides the data in coarse blocks and assigns blocks
%to locales, while {\em cyclic} finely interleaves elements of a domain across the locales).
These primitives are easy for the
programmer to specify, and the Chapel compiler and runtime are able to efficiently analyze
how two or more of these domain maps interact.  However, the expressivity of such a system
is limited.  When performing computation on complex data structures such as graphs, the individual
elements must be manually ordered into arrays so that they may then be distributed using
the available domain map primitives.
%When the desired ordering is dependent on the actual data,
%a computation must be performed to determine the ordering (e.g. a call to PARMETIS), and this computation is ideally parallelized
%as well, requiring a distribution of the data in order to compute a distribution of the data.
% and cases where an element appears in multiple
%partitions are generally transformed into explicit ghost nodes with copies inserted manually when
%updates are performed.
The programmer is solely responsible for maintaining this mapping --- neither
the Chapel compiler nor the runtime can help in guaranteeing correctness.
%assist with the computation or do any correctness 
%checking.

In contrast, the Legion\cite{Legion12} programming model
allows a {\em logical region} to be partitioned into any computable subsets,
thereby maximizing the programmer's ability to express partitioning schemes. 
This generality is achieved by allowing arbitrary code to be used
for partitioning.  Unsurprisingly, this limits the Legion system's ability
to reason about partitions and their relationships and to perform optimizations.

\begin{figure*}
  \centering
  \includegraphics[width=0.70\textwidth]{figs/circuit_deppart.pdf}
  \vspace*{-4mm}
\caption{Dependent partitioning in a graph-based computation \label{fig:graph_partition}}
  \vspace*{-6mm}
\end{figure*}

%To motivate the need to reason about the relationships between
%partitions, we now introduce a driving example.
As an example, consider a distributed computation on a graph-based data structure, 
such as the simulation of
an electric circuit,
that divides the nodes of the graph between different processes ({\em ranks}).
A good division of the nodes is non-trivial, and applications often 
use a third-party library such
as PARMETIS\cite{Karypis99} to compute it.
This initial partition is further refined into additional
partitions that classify nodes accessed by individual ranks
%The affinity and parallelism of the
%distributed computation is described by further classifying nodes accessed by a given rank
as ``private'' (i.e. only used by this rank), ``shared'' (i.e. owned
by this rank but accessed by others), or ``ghost'' (i.e. accessed by this rank but owned by
another rank).  Figure~\ref{fig:graph_partition} summarizes this pattern.

The application also requires a corresponding partition of the edges based
on the partitioning of the nodes, and commonly this partitioning must for correctness maintain a
number of consistency 
criteria with respect to the partitions of the nodes.
For example, a typical constraint is that an edge accessed by a given rank should be incident only on nodes
(private, shared, or ghost) associated with that rank.

Consistency checks such as these arise in nearly all distributed applications.  Any
pointer dereference or array index computation to find another element requires 
consistency between different partitions, often expressed as dependences in
the derivation of one partition from another.
%dependency in the data structure(s) and partition(s) thereof.  
%Care must be taken to ensure that these related elements
%are in accessible (and ideally nearby) memory locations.  Our observation is that while these
While these dependences may be complex, we make the observation that,
with the proper building block abstractions, we can precisely capture the
dependences and leverage them for doing program analysis to check correctness
invariants and improve performance.
%dependencies can be quite involved, they can be constructed from building blocks based on these
%dereference and/or index computation operations.
%two simple ways of computing the address of one element from another.

%In Legion,
%each of the partitions in Figure~\ref{fig:graph_partition} is described by a {\em coloring} object that is computed by
%the application code, which is opaque to the runtime.  Unlike all other application
%data in a Legion program, these coloring objects cannot be distributed, adding additional overhead when
%the colors have been computed by other Legion tasks.  The consistency
%of the resulting
%data partitioning  
% must be verified at runtime.  The computation of the colorings and
%the verification of the consistency of the partitions both add significant runtime overhead.\footnote{The verification
%is often disabled in production runs, but the coloring computation is unavoidable.}



To capture relationships between multiple partitions, we present a framework for {\em dependent partitioning} 
that concisely describes how to functionally derive partitions 
of data from previous partitions
%that provides a bridge between these two approaches,
%allowing concise descriptions of partitions of both structured and unstructured application data
%based on arbitrary application-supplied data
while retaining the ability to reason about computed partitions during compilation and at runtime.
The core idea is to divide partitioning into two steps:
\begin{itemize}
\item First, an application computes an {\em independent partition} of a data structure.  This partition can be arbitrary --- i.e. there are no expressivity restrictions.  In the example in
Figure~\ref{fig:graph_partition}, the {\tt nodes} partition (in the center) is an independent partition.
\item Second, the application uses a small set of building block functions to compute {\em dependent partitions} from the independent partition or other dependent partitions.
%that can be computed from the independent partition (and other dependent partitions).  
These building block functions
and their compositions are statically analyzable, making it possible to automatically check the consistency
of a dependent partition with the partition(s) from which was is derived.  The other four partitions in
Figure~\ref{fig:graph_partition} are all dependent on the original {\tt nodes} partition.
\end{itemize}
% allow maximal expressivity for an {\em independent partition} of a data structure,
%but provide a set of {\em dependent partitioning operations} that define {\em dependent partitions}, either
%of the same data structure or another.  These dependent partitioning operations can be analyzed by a compiler to
%statically verify the necessary consistency and efficiently implemented by a runtime to reduce the overhead of
%partition computation.


%\noindent Moreover, the inputs and outputs of these operations can be described
%without the need for the extra mechanism of coloring objects, allowing partitionings to be expressed
%using the same facilities for parallelism and distribution as all other computations.


%model's natural data model,
%allowing the operations to be performed in a distributed and scalable way while minimizing data
%movement.

\input{dpl_grammar}

We evaluate our framework with an implementation in the Regent language\cite{Regent15} and the
Legion runtime\cite{Legion12}, and borrow their terminology in many cases, but 
%We evaluate our system by embedding it in the Regent language and the Legion runtime.
%While our implementation is conducted using Regent and Legion and we
%borrow their terminology,
the dependent partitioning framework is general enough that it can be implemented in any programming model that
includes the concepts of stable ``names'' for objects (e.g. pointers), sets and subsets of those
names, and fields defined for each object
in a set.  For example, a natural mapping exists to Chapel {\em domains} and {\em arrays} defined over those domains.
%includes the concepts of stable ``names'' (e.g. pointers or array indices) for objects, sets and subsets of those
%names (e.g. {\em logical regions} in Legion or {\em domains} in Chapel), and fields defined for each object
%in a set (e.g. Legion {\em fields} or Chapel {\em arrays}).

The rest of this paper is organized as follows.
We describe an abstract Dependent Partitioning Language (DPL) in Section~\ref{sec:dpl} and
give several examples to demonstrate its expressivity.  Section~\ref{sec:analysis} covers the static analysis 
required to discharge most consistency checks at compile time and discusses when and why completeness is sacrificed.
We then describe the changes made to the Regent compiler (Section~\ref{sec:regent}) and the Legion
runtime (Section~\ref{sec:legion}) to implement the framework.  Section~\ref{sec:results} provides our experimental
results 
%showing an 86-96\% reduction in the lines of application code required for computing partitions, a
%2.6-16.6x performance improvement in computing partitions on a single node and up to a further 29x improvement when
%distributing the computation across 64 nodes.  Finally, 
and Section~\ref{sec:related} places our effort in the context of
related work.


%% computation on an element that requires accessing related elements (in the same or another data structure)
%% will require that those related elements be in accessible (and ideally nearby) memory locations.
%% operations 
%% any computation that involves more than one data
%% structure

%% ...

%% talk about common pattern of one {\em independent partition} and multiple
%% {\em dependent partitions} - this where the whole idea of "consistency checks"
%% comes from - argue this occurs for both unstructured and structured cases
%% REDO GRAPH EXAMPLE

%% talk about how (expressive) partitioning info itself can be large and must
%% be distributed - desirable to fit within overall data model to control
%% how dependent partitioning operations are distributed

%% mention challenge of mutability of topology, benefits of using local
%% immutability

%% Our contributions are:

%% \begin{itemize}

%% \item A language-independent framework for dependent partitioning that allows
%% maximum expressibility of independent partitions as well as decidable analysis
%% of any dependent partitions.
%% \item A simple Data Partitioning Language (DPL) and type system, demonstrating
%% the soundness of the dependent partitioning framework.
%% \item An implementation in Regent, resulting in XX\% reductions in the code
%% needed to describe dependent partitions, the ability to parallelize the 
%% partitioning operation across multiple cores or nodes, and the elimination of
%% the expensive runtime checks currently needed to verify consistency.

%% \end{itemize}
