\documentclass[10pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{amssymb}

\begin{document}

\lstset{
  literate={<}{{$\langle$}}1 {>}{{$\rangle$}}1,
}

\section{Logical Regions}
\label{sec:regions}

Logical regions are the ``address spaces'' within which Legion data resides.  A logical region
may be a {\em top-level region}, or it may
be a {\em subregion} of another logical region.  Top-level regions may be dynamically created
within a Legion application:
\begin{lstlisting}
region<IS> r = newregion<IS>();
\end{lstlisting}

As shown above, a logical region is associated with an {\em index space} which defines the
underlying type used to name locations within
the region.  An index space may either be the space of opaque pointers (\verb+ptr+) or it may
be any enumerable type (e.g. \verb+int+ or
\verb+<int, <int, int> >+).

Subregions are ``views'' into (rather than copies of) subsets of a logical region, and may be
created by partitioning or other operations
described in Section~\ref{sec:regionops}.  A top-level region and all of its subregions make up
a {\em logical region tree}.  Information
about these trees is maintained by both the type system and the runtime, and includes:
\begin{itemize}
\item subregion properties - written $a \leq b$, they indicate that region $a$ is a subset of
region $b$.
\item disjointness properties - written $a * b$, disjointness captures the property that there
is no overlap between regions $a$ and $b$.
\end{itemize}

\begin{wrapfigure}{r}{3in}
\vspace{-10pt}
\centering\includegraphics[scale=0.3]{lrg_example-crop.pdf}
\caption{Logical Region Graph Example \label{fig:lrg_example}}
%vspace{-30pt}
\end{wrapfigure}

\begin{wrapfigure}{r}{3in}
\centering\includegraphics[scale=0.3]{lrg_partitions-crop.pdf}
\caption{Logical Region Graph Partitions \label{fig:lrg_partitions}}
\end{wrapfigure}

A number of identities hold for these properties:
\begin{itemize}
\item subregion reflexivity - $a \leq a$
\item subregion transitivity - $a \leq b \wedge b \leq c \Rightarrow a \leq c$
\item disjointness of subregions - $a \leq b \wedge a * c \Rightarrow b * c$
\end{itemize}

These identities also serve as a set of inference rules that can be applied to a set of logical
region tree properties to generate all other
properties that are logically entailed by that set.  (This will be a finite set.)  Conversely,
given a set of properties, these rules can be
used to determine the minimal subset of properties that logically entail the original set.  (I
believe this subset is uniquely determined.)

These properties can be visualized as a {\em logical region graph} in which subregion
properties are directed solid edges from ``parent'' region to
``child'' regions and disjointness properties are undirected dotted edges between disjoint
regions.  For readability, typically only properties that
are not entailed by other properties are shown.  Figure~\ref{fig:lrg_example} shows an example
logical region graph.  As a further notational
convenience, the common patterns of {\em disjoint partitions} (i.e. a set of subregions of a
given region for which all subregions are pairwise
disjoint) and {\em aliased partitions} (i.e. a set of subregions of a given region for which
pairwise disjointness is not certain) can be expressed
as shown in Figure~\ref{fig:lrg_partitions}.  (Note the $*$ on the disjoint partition, a
notation borrowed from separation logic.)

\section{Pointers}
\label{sec:pointers}

Individual locations within a region are stored in pointers.  The type of a pointer includes
both the underlying index space that the pointer's value
inhabits as well as the region into which the pointer is known to point.  New pointers into a
region can be dynamically allocated and released (freed):
\begin{lstlisting}
region<IS> r = ...;
IS@r p = alloc(r);
release(p);
\end{lstlisting}

For regions using an enumerable index space (i.e. not the opaque \verb+ptr+ type), specific
ranges of pointers may be reserved.  These pointers will
not be returned by any subsequent allocate request, and can be released in the same way.
\begin{lstlisting}
region<IS> r = ...;
IS range_min = ...;
IS range_max = ...;
reserve(r, range_min, range_max);
\end{lstlisting}
A range on a tuple type is defined as the cartesian product on the range of each field.  For
example, a range on a two-dimensional (\verb+<int, int>+)
index space defines a rectangle by its lower-left and upper-right coordinates.

\section{Fields}
\label{sec:fields}

On their own, logical regions and the pointers into them have no associated storage.
Persistent data in Legion is stored in {\em fields}.  A field
is associated with a particular logical region and provides a storage location for every
pointer within that region.  All locations in a field store
values of a single type (referred to as the {\em range type} for reasons which will be clear
below), which is specified at field creation time.
Fields may be dynamically created and destroyed, and may be stored in heap data
structures (which poses some challenges for the type system).  Fields are (generally) mutable,
but at any given point in a program's execution, it
defines a function from pointers in the logical region to values in the range type.  We refer
to these (immutable) mathematical functions as {\em snapshots} of a
field.  Although application code cannot name snapshots directly, they will be useful in
describing the programming model.  To avoid confusion with
the types of Legion functions, we use a squiggly arrow:
\begin{lstlisting}[mathescape]
region<IS> r = ...;
field<IS@r,T> f = newfield<T>(r);
$\vdash \textit{snapshot}(f) : IS@r \leadsto T$
\end{lstlisting}

Field variables in a Legion application are actually {\em field references}, so we'll use
double-arrows for that:
\begin{lstlisting}[mathescape]
field<IS@r, T> f = ...;
$\vdash f : IS@r \Rightarrow T$
\end{lstlisting}

\section{Privileges}
\label{sec:privileges}

In order to perform allocation operations on a logical region or reads or writes to a field,
the application must have the necessary privileges.  The 
kinds of privileges an application may have are:
\begin{itemize}
\item $\textit{alloc}(r)$ - grants the code the ability to allocate new location in region $r$
\item $\textit{read}(r,f)$ - grants the code the ability to read the value stored in field $f$
for any pointer in logical region $r$ (which need not be the
whole region for which field $f$ contains data)
\item $\textit{write}(r,f)$ - grants the code the ability to update the value stored in field
$f$ for any pointer in logical region $r$
\item $\textit{reduce}(r,f,o)$ - grants the code the ability to update the value stored in
field $f$ using applications of a reduction operation $o$ for any pointer in the logical
region $r$
\end{itemize}

\section{Tasks}
\label{sec:tasks}

A Legion application consists of one or more {\em tasks}.  A task is similar to a function in
C in that it accepts arguments, (optionally) produces a
return value, and has local variables that it may manipulate freely.  However, all access to
persistent data is done through fields, and a task must
declare which fields it will access (and in what manner) in the form of {\em privileges}.  A
task may invoke other tasks as {\em subtasks}, provided 
it has all the privileges needed by the subtask.

A task has the following properties:
\begin{itemize}
\item {\em region parameters} - a list of zero or more regions that can be provided to the
task by the caller
\item {\em field parameters} - a list of zero or more field references provided by the caller
 - any regions needed by the reference types must be in the
region argument list
\item {\em input constraints} - a set of constraints that must hold on the regions and/or
fields provided by the caller
\item {\em privileges} - a set of privileges required by the task - all regions and fields
named in the privileges must be arguments to the task
\item {\em coherence modes} - a set of relaxed coherence modes requested by the task
\item {\em formal parameters} - a list of zero or more parameter names with the required types
\item {\em return type} - the type of the task's return value, if any
\item {\em predicate} - a boolean expression that determines whether the task should be run at
all
\item {\em body} - the code for the body of the task
\end{itemize}

\section{Futures}
\label{sec:futures}

When a subtask is spawned by a parent task's application code, the call returns immediately to
the parent, providing a {\em future} that will
eventually contain the value returned by the subtask.  Although the parent task may examine the
future itself (waiting for the subtask to complete
if necessary), the preferred usage of futures is to pass them directly to subsequent subtask
invocations.  This has two important advantages:
\begin{enumerate}
\item It avoids unnecessary synchronization in the parent task.  Although the second subtask
must wait for the first subtask to complete before it
has the argument values it needs, the parent task may continue, spawning further subtasks that
maybe able to run in parallel with the first two.
\item It avoid unnecessary contamination of the parent task.  As we'll see below, if a fault
occurs in the execution of a task, any computation that
made use of the original (erroneous) result must be undone and recomputed.  The effects of
subtasks can often be contained to a subset of the 
fields that are accessible to the parent task, limiting what must be recomputed, but if the
computation in the parent task is tainted, the
runtime has no way to tell what might have been corrupted, so the whole parent task will have
to be restarted from scratch.
\end{enumerate}

\section{Execution Contexts}
\label{sec:contexts}

For each execution of a task (i.e. each task instance), a execution context is created.  The
execution context is used to make scheduling decisions and track progress of the 
subtasks spawned by the evaluation of the task's body.  Each of those subtasks will have its
own execution context for its subtasks, permitting 
hierarchical scheduling (and resiliency).

The information in an execution context can be captured in a {\em task dataflow graph} (or
TDG).  For the impatient, an example of a task dataflow graph is shown in
Figure~\ref{fig:tdg_example}.
A task dataflow graph has several kinds of nodes, each of which will be described in more
detail below:
\begin{itemize}
\item Large rectangles are {\em field snapshots}, which represent the logical state of a field
(for some index subspace) after some set of subtasks have manipulated it.  A snapshot node may
be associated with any
number of physical instances that are known to hold (or will hold) values matching the
snapshot.
\item Diamonds are {\em operations}, which include subtask invocations, explicit
field-to-field copies, and inline mappings.
\item Small rectangles are futures.
\end{itemize}

The shape of the TDG is ``semi-static''.  It is defined implicitly by the runtime operations
performed in the execution of the task's body.  In simple tasks, it may be possible to construct
the TDG at compile time, but any data dependence (either on task inputs or on results of
subtasks) will generally require the dynamic generation of the TDG.  However, once a node or
edge has been added to the TDG, it cannot be changed.  

\subsection{Context Initialization}

When the execution of a task begins, the TDG is initialized with a field snapshot for each 
field for which the task has privileges.  

\subsection{Task Dataflow Graph Construction}

Each runtime operation requested by a task results in nodes being added to the TDG.  Operations
are always added to the TDG in the order they were requested.  The first step is to add an
operation node to the TDG for the requested operation.  This is usually followed by the
addition of more nodes and edges based on how this operation interacts with previous (and
future) operations.  In most cases, the additions to the TDG for a given operation are
deterministic, but there are a few choices that come up.  There are
many different kinds of operations, but the properties used to stitch them into the TDG are 
common to all of them:
\begin{itemize}
\item {\em Observed fields} - an operation that has only read privileges for a field is usually
considered an {\em observer} of the field.  The observation of a field by an operation requires
an {\em observation edge} from a valid snapshot of the field to the operation node.  In order to
be a valid snapshot, the snapshot must:
\begin{enumerate}
\item be for the requested field
\item be for the requested index space or a index space known to contain it
\item have no outgoing edges other than observation edges
\end{enumerate}
It can be shown that there will be either zero or one valid snapshots for any observed field.
The four possible cases are:
\begin{enumerate}
\item A valid snapshot for the exact index space exists.  In this case, this snapshot is used.
\item A valid snapshot for an enclosing index space exists.  (This case is mutually exclusive
with the previous one - at most one valid snapshot will exist.)  A choice is available here.
Either
the snapshot can be observed directly, or it may be {\em opened} by creating a new snapshot
node for the requested index space, creating an {\em opening edge} from the original snapshot
to the new snapshot and an observation edge from the new snapshot to the operation.
\item A snapshot exists for an enclosing index space that would be valid, but for one or more
outgoing opening edges for index spaces that are known to be disjoint from the requested
index space.  In this case, an additional opening edge can be added to that snapshot to a 
new snapshot node for the requested region.
\item Finally, a valid snapshot for the requested index space (or an enclosing one) can be 
always be regenerated by {\em closing} up one or more previously-opened snapshots.  To close up
a snapshot that has one or more opening edges, a valid snapshot must be available corresponding
to each opening edge.  (This may not be exactly the same snapshot, but must be for the exact
index space.)  As openings form a tree, there must always be at least one leaf opening that
satisfies this requirement.  To close up the snapshot, a new snapshot is created for the 
original index space with a {\em versioning edge} from the originally opened snapshot to this
new snapshot.  Additionally, {\em closing edges} are created from each of the subspace's
snapshots to the new snapshot.  Once enough snapshots have been closed up, one of the first two
cases will hold, and the observation edge can be added.
\end{enumerate}

\item {\em Contributed fields} - an operation may be considered a {\em contributor} to any
field for which it has privileges.  If the privileges allow writing or reductions, it {\bf must}
be a contributor.  For each field an operation contributes to, a {\em contribution edge} is
added from the operation to a valid snapshot.  To be a valid snapshot for contribution, the
a snapshot must:
\begin{enumerate}
\item be for the requested field
\item be for the requested index space or a index space known to contain it
\item have no outgoing edges at all
\item either have no incoming contribution edges (if the new operation uses exclusive coherence)
or only have contribution edges from other operations that also use relaxed (i.e. atomic or
simultaneous) coherence
\end{enumerate}
As with observation edges, snapshots may be opened or closed as necessary to obtain a snapshot
with the desired index space.  If a snapshot is valid but for existing (and incompatible)
contribution edges, a new snapshot for that index space is created, a {\em versioning edge}
is added between the old and new snapshots, and the contribution edge is connected to the 
new snapshot.

\item {\em Futures accessed} - an operation may accept futures as inputs (either for task
arguments or predication).  For each future accessed, an {\em access edge} is added from the
future to the operation.

\item {\em Result future} - every operation generates a result when it completes (even if the
type of the result is {\tt void}).  A new future node is created for this result and a
{\em result edge} is added from the operation to the future.  (In cases where a given future
is never accessed, we'll generally leave it out of the figures for tidiness, but it always
exists.)

%\item {\em Escapes} - when the result of a subtask is used by the parent task (either as a
%result of an inline mapping or the access of a subtask's future), a data dependence between
%the subtask and the parent task is created that the Legion runtime cannot track precisely.
%To record this, an {\em escape edge} is created from the operation.  In the diagrams, this is
%shown as a dangling edge, but you can think of it as going to a special ``parent task'' node
%if you prefer.  As a further simplification to the diagrams, the operation for the accessing
%of a future is often omitted, with the escape edge drawn directly from the future itself.

\end{itemize}

\noindent\begin{tabular}{lc}
\begin{minipage}{0.45\textwidth}
Example 1: Single subtask modifiying a single field
\begin{verbatim}
void parent(...): RWE(r1.x) {
  child1(...); // RWE(r1.x)
}
\end{verbatim}
\end{minipage}
&
\begin{minipage}{0.45\textwidth}
\includegraphics[scale=0.75]{tdg_ex1.pdf}
\end{minipage}
\end{tabular}

\noindent\begin{tabular}{lc}
\begin{minipage}{0.45\textwidth}
Example 2: Two tasks, two fields
\begin{verbatim}
void parent(...): RWE(r1.x, r1.y) {
  child1(...); // ROE(r1.x), RWE(r1.y)
  child2(...); // RWE(r1.x)
}
\end{verbatim}
\end{minipage}
&
\begin{minipage}{0.45\textwidth}
\includegraphics[scale=0.75]{tdg_ex2.pdf}
\end{minipage}
\end{tabular}

\noindent\begin{tabular}{lc}
\begin{minipage}{0.45\textwidth}
Example 3: Disjoint Subregions
\begin{verbatim}
void parent(...): RWE(r1.x) {
  child1(...); // RWE(r2.x)
  child2(...); // RWE(r3.x)
}
\end{verbatim}
\end{minipage}
&
\begin{minipage}{0.45\textwidth}
\includegraphics[scale=0.75]{tdg_ex3.pdf}
\end{minipage}
\end{tabular}

\noindent\begin{tabular}{lc}
\begin{minipage}{0.45\textwidth}
Example 4a: Aliased Subregions
\begin{verbatim}
void parent(...): RWE(r1.x) {
  child1(...); // RWE(r2.x)
  child2(...); // RWE(r4.x)
}
\end{verbatim}
\end{minipage}
&
\begin{minipage}{0.45\textwidth}
\includegraphics[scale=0.75]{tdg_ex4a.pdf}
\end{minipage}
\end{tabular}

\noindent\begin{tabular}{lc}
\begin{minipage}{0.45\textwidth}
Example 4b: Aliased Subregions (Simultaneous)
\begin{verbatim}
void parent(...): RWE(r1.x) {
  child1(...); // RWS(r2.x)
  child2(...); // RWS(r4.x)
}
\end{verbatim}
\end{minipage}
&
\begin{minipage}{0.45\textwidth}
\includegraphics[scale=0.75]{tdg_ex4b.pdf}
\end{minipage}
\end{tabular}

\noindent\begin{tabular}{lc}
\begin{minipage}{0.45\textwidth}
Example 5: Futures
\begin{verbatim}
void parent(...): RWE(r1.x) {
  future<bool> f1 = child1(...); // ROE(r1.x)
  future<float> f2 = child2(...); // RWE(r1.x)
  f1? child3(f2, ...); // RWE(r1.x)
  child4(...); // RWE(r1.x)
}
\end{verbatim}
\end{minipage}
&
\begin{minipage}{0.45\textwidth}
\includegraphics[scale=0.75]{tdg_ex5.pdf}
\end{minipage}
\end{tabular}

\noindent\begin{tabular}{lc}
\begin{minipage}{0.45\textwidth}
Example 6: Inline Mappings
\begin{verbatim}
void parent(...): RWE(r1.x) {
  child1(...); // RWE(r1.x)
  map_region(r1.x);
  ...;
  unmap_region(r1.x);
  child2(...); // RWE(r1.x);
}
\end{verbatim}
\end{minipage}
&
\begin{minipage}{0.45\textwidth}
\includegraphics[scale=0.75]{tdg_ex6.pdf}
\end{minipage}
\end{tabular}

The description above used many different kinds of edges, but the observant reader will note
that they all use solid edges in the diagrams.  The even more observant reader will note that
the type of an edge is fully determined by what kinds of nodes it joins:
\begin{itemize}
\item Edges from snapshots to operations are always {\em observation edges}.
\item Edges from operations to snapshots are always {\em contribution edges}.
\item Edges from snapshots to snapshots with the same index space are {\em versioning edges}.
\item Edges from snapshots to snapshots with index subspaces are {\em opening edges}.
\item Edges from snapshots to snapshots with parent index spaces are {\em closing edges}.
\item Edges from futures to operations are {\em access edges}.
\item Edges from operations to futures are {\em result edges}.
%\item Dangling edges from any node are {\em escape edges}.
\end{itemize}

\subsection{Context Finalization}

Once the body of a task has been fully executed and all requested operations have been added
to the TDG, the final step is to identify (or possibly create) a snapshot for each field that
the task is contributing to in its parent task's context.  The requirements for a valid output
snapshot are the same as for observation edges.  Output snapshots are shown in the diagrams as
rectangles with heavy borders.

\section{Event-Driven Execution}

The deferred execution of the operations requested by a task is managed by the Legion runtime.
The runtime tracks this execution by annotating the nodes of the TDG with the current state.
(Whereas the shape of the TDG was ``semi-static'', the state of the TDG is very dynamic.)

Different nodes have different kinds of state.  Field snapshots have:
\begin{itemize}
\item {\em status} - one of: {\tt INVALID}, {\tt READY}, {\tt SPECULATIVE}, {\tt COMPLETE}, {\tt COMMITTED}
\item {\em instances} - a list of instances known to contain (or eventually contain) data
matching this snapshot
\end{itemize}
\noindent The initial snapshots created in a TDG are in the {\tt COMMITTED} state and will
generlly have at least one instance provided by the parent task's context.  All other snapshots
are initially given the {\tt INVALID} status and have an empty instance list.

Futures have:
\begin{itemize}
\item {\em status} - one of: {\tt INVALID}, {\tt SPECULATIVE}, {\tt COMPLETE}, {\tt COMMITTED}
\end{itemize}
\noindent All futures are initially given the {\tt INVALID} status.

Operations have:
\begin{itemize}
\item {\em status} - one of: {\tt INIT}, {\tt READY}, {\tt MAPPED}, {\tt RUNNING}, {\tt SPECULATIVE},
{\tt COMPLETE}, {\tt COMMITTED}
\item {\em speculation} - whether or not the predicate for the operation has been speculated.
May be: {\tt True} (i.e. predict taken), {\tt False} (i.e. predict not taken), or {\tt None}
(i.e. no prediction made explicitly for this task)
\item {\em instances} - for each field, exactly which instance is being used by the task
\end{itemize}
\noindent All operations are initially given the {\tt INIT} status, a speculation value of
{\tt None} and an empty list of instances.

The execution model used by the runtime is characterized by:
\begin{itemize}
\item {\em actions} - a transformation of the state of the TDG, often accompanied by requests
for operations to the low-level runtime.  Each kind of action has preconditions that must be
met for the action to be allowed.  In many cases, multiple actions will be allowed, and the
runtime may choose any one of them.  Although this can result in nondeterministic scheduling,
the Legion programming model guarantees deterministic results when only exclusive coherence
is used and limits the nondeterminism in relaxed coherence cases to just the transitive fanout
of those operations.
\item {\em events} - notifications from the low-level runtime that requested operations have
completed successfully, or when faults have occurred.  Events do not themselves cause new
requests to the low-level runtime - they simply modify the state of the TDG, often enabling
new actions to be performed.
\end{itemize}

\subsection{Resetting Nodes}

The runtime operates in an event-driven loop, processing until the TDG is known to be
complete (i.e. the task body has
finished executing) and all snapshots designated as task outputs have a status of either
{\tt COMPLETE} or {\tt COMMITTED}.

In general the progress will be monotonic, but in the presence of mispredicted speculation or 
faults, nodes in the TDG may need to be {\em reset}.  When a node is reset, its state is 
reset to its initial values.  Additionally, any node connected to the node by an outgoing edge or
an incoming contribution edge must also be reset.  (This applies transitively.)  When a snapshot
is reset, the existing instances are destroyed, which may involve removing them from the
instance list of other snapshots in the TDG as well.

Some operations
may be marked as being {\em non-idempotent} - these are operations that have side-effects that
cannot be tracked by the Legion runtime (e.g. inline mappings by a parent task, tasks that
write to disk).  If a non-idempotent operation that is at least {\tt RUNNING} needs to be reset,
special action needs to be taken.  If the operation provides a {\em recovery handler}, it is run
and if successful, the node is reset as normal.  However, if the recovery handler fails or if
no handler is provided, the enclosing task is considered to have failed an an {\em execution
fault} is reported to the task's parent context.

When faults occur, it is also possible for the process to get stuck if some data is lost that
cannot be regenerated.  This case also results in an execution fault being reported to the
parent task's context.  In the absence of faults, eventual successful completion is
guaranteed as long as there's only a finite number of (incorrect) speculation attempts.

\subsection{Basic Execution}

We first describe the actions and events that are used for execution of a single task.

Actions:
\begin{itemize}
\item {\tt VERSION} - A snapshot which is {\tt INVALID} but has an incoming versioning edge
from a snapshot that is at least {\tt SPECULATIVE} (and therefore has at least one instance) may
be changed to {\tt READY} and given at least one instance by either creating a new instance
and requesting a copy from one of the input snapshot's instances or by stealing one or more
of the input snapshot's instances.  The stealing of an instance is only permitted if every
observing task of the input snapshot is either at least {\tt RUNNING} or is {\tt MAPPED} and
has selected an instance other than the one(s) being stolen.

\item {\tt OP READY} - Any operation that is {\tt INIT}, for which all observed fields are at
least {\tt SPECULATIVE}, all accessed futures are at least {\tt SPECULATIVE} and all contributed
fields are {\tt READY}, may be changed to a status of {\tt READY} with a speculation of
{\tt None}.

\item {\tt MAP} - An operation in the {\tt READY} state may be mapped.  An instance must be
selected for each field snapshot connected to the operation.  The instance may either be an
existing instance associated with the snapshot or a new one, for which a copy must be 
requested.  If a new instance is created, it is also associated with the snapshot.  For
contributed fields, the new instance replaces the old one.  The operation's state is updated
to include the instances chosen and the status is set to {\tt MAPPED}.

\item {\tt EXECUTE} - An operation in the {\tt MAPPED} state can be queued up for execution.
A request to the low-level runtime is made and the status of the operation is changed to
{\tt RUNNING}.  In general, non-idempotent operations should also wait until all inputs are
at least {\tt COMPLETE} in order to prevent mispredicted speculation from causing execution
faults.

\item {\tt COMPLETE} - Any node that is {\tt SPECULATIVE} for which all nodes with incoming
edges are {\tt COMPLETE} may be changed to {\tt COMPLETE}.  The one exception is if the node
is an operation with a speculation state other than {\tt None} (this is covered by the 
{\tt RESOLVE} action instead).
\end{itemize}

Events:
\begin{itemize}
\item {\tt OP COMPLETE} - A notification that a given operation has completed (apparently)
successfully.  If the speculation state of the operation is either {\tt True} or {\tt False} the
status is changed to {\tt SPECULATIVE}.
If the speculation state of the operation is {\tt None}, then the status is changed to
{\tt COMPLETE}.  For each snapshot to which the operation contributes, if all the contributing
operations are {\tt COMPLETE}, the snapshot is also marked {\tt COMPLETE}.  If not, but all 
contributing operations are at least {\tt SPECULATIVE}, then the snapshot is also changed to
{\tt SPECULATIVE}.
\end{itemize}

Example 1: Single subtask modifiying a single field
\begin{verbatim}
void parent(...): RWE(r1.x) {
  child1(...); // RWE(r1.x)
}
\end{verbatim}

\subsection{Opening and Closing Actions}

The next set of actions are related to the opening and closing edges in a TDG.  No new events
are needed for this:

\begin{itemize}
\item {\tt OPEN} - A snapshot with outgoing opening edges that is at least {\tt SPECULATIVE} and
for which all snapshots connected to the outgoing opening and versioning edges are {\tt INVALID}
may perform an opening action.  The snapshots connected by opening edges are changed to have
the same status as the originating snapshot, while the snapshot connected by a versioning edge
is changed to {\tt READY}.  Each recipient snapshot may copy and/or steal instances as with
the {\tt VERSION} action.

\item {\tt CLOSE} - A snapshot that is {\tt READY} and has incoming closing edges may perform
a closing up operation if all of the snapshots connected by those closing edges are at least
{\tt SPECUALATIVE}.  For each instance in the target snapshot, copies must be requested from an
instance in each of the source snapshots unless the source snapshot also contains the
destination instance.  When multiple instances exist in the target snapshot, some (but not all)
of them may be discarded rather than completed.  The resulting status of the target snapshot
is the minumum of all the source snapshots (the snapshot from which the opening was originally
done need not be consulted, as it will be at least as far along as any of the source snapshots).
\end{itemize}

Example 2: Disjoint Subregions
\begin{verbatim}
void parent(...): RWE(r1.x) {
  child1(...); // RWE(r2.x)
  child2(...); // RWE(r3.x)
}
\end{verbatim}

\subsection{Speculation}

When the predicate for a task is not known, the runtime (usually under the guidance of the mapper)
may {\em speculate} on the not-yet-known value of the predicate.  Additional actions are added to
cover both the act of speculation and the resolution of the speculation (which may result in some
work being thrown away or redone):

\begin{itemize}
\item {\tt SPECULATE TRUE} - Any operation satisfying the requirements of {\tt OP READY}, except for one or more futures used as part of the predicate being only {\tt INVALID} may be speculated ``true''.  The status of the task is changed to {\tt READY} and the speculation is set to {\tt True}.

\item {\tt SPECULATE FALSE} - Any operation with at least one {\tt INVALID} future contributing to the predicate may be speculated ``false''.  The status of the task is changed to {\tt SPECULATIVE} and the speculation value is set to {\tt False}.

\item {\tt RESOLVE} - An operation with a speculation state of either {\tt True} or {\tt False}
and for which all accessed futures are at least {\tt COMPLETE} can have its speculation
resolved.  If the actual evaluation of the predicate matches the speculation state of the
operation, the speculation state is changed to {\tt None}.  If the operation is
{\tt SPECULATIVE} and all of its observed fields are at least {\tt COMPLETE}, the operation's
status is changed to {\tt COMPLETE}.

However, if the predicate and the speculation state do not match, a misprediction has occurred.
The mispredicted task is reset (along with everything in the transitive fanout).

[Note: The requirement that all accessed futures are {\tt COMPLETE} is conservative.  If
speculations are generally assumed to be correct, relaxing the requirement to just at least
{\tt SPECULATIVE} status for the accessed futures might be appropriate.]

[TODO: Figure out the rules for when tasks being speculatively executed may ``steal'' instances
from previous snapshots.  Goal is to guarantee that bad speculation decisions never get the
execution stuck.]
\end{itemize}

\subsection{Resiliency}

Support for resiliency comes in the form of a number of events that may be reported by the low-level
runtime.  These events provide notification that tasks and/or data have been lost, and make
(generally undesirable) modifications to the state of the TDG from which the runtime must attempt
to recover.

The events are:
\begin{itemize}
\item {\tt EXECUTION FAULT} - A notification that an operation had started successfully, but
encountered some sort of unrecoverable fault during its execution.  The operation is reset
(which may impact other operations contributing to the same snapshots).

\item {\tt PRESTART FAULT} - A notification that an operation has not started, but can no
longer be
run in the specified location (e.g. due to a processor or node failure).  The operation's
status is changed to {\tt READY} so that it can be remapped.  [TODO: Figure out how this 
interacts with other operations contributing to the same snapshots?]

\item {\tt STORAGE FAULT} - A notification that the contents of an instance have been corrupted.
(This will be due to things like disk faults and uncorrectable ECC errors in DRAM.)
The instance is removed from all snapshots, and any snapshot that is left with zero instances
is changed to {\tt INVALID}.  Additionally, any operation that is {\tt RUNNING} and has the
corrupted instance in its instance list must be reset.

\item {\tt DATA FAULT} - A notification that the contents of an instance appear to have been 
corrupted at some time in the past.  (This fault will generally be reported by an operation that
reads the data and thinks it is fishy.)  Like a storage fault, the instance is removed from all
snapshots and any {\tt RUNNING} task using the instance is reset.  However, because the time at
which
the corruption occurred is not known (it could have even been a silent fault in the operation
that originally wrote the data), ALL operations that are using that instance are reset.  (If 
any of those operations were non-idempotent or had a status of {\tt COMMIT}, this will likely
cause the enclosing task to fault.)
\end{itemize}

Although not specific to resiliency, there is one additional action that is only important when
faults are possible:
\begin{itemize}
\item {\tt COMMIT} - Any node that is {\tt COMPLETE} may be changed to {\tt COMMITTED}.
Unlike the previous action, which is safe to perform eagerly, the committing of a task or of
a snapshot limits the ability to recover from faults, so should generally be initiated
(perhaps indirectly) by the application (i.e. the mapper) in cases where either the chance
of a fault is deemed low enough or the cost of maintaining the intermediate state is too high.
\end{itemize}

\end{document}
