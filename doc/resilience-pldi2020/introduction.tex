%sri

\section{Introduction}
A failure during a large-scale execution of any application on an extreme-scale
system leads to loss of time and money, and can cause nightmares for any
application developer. While failures can be the result of
application bugs, soft
failures due to ECC-related errors are becoming more prevalent.  For example,
Geist states that such failures are a common occurrence on Oak Ridge National
Laboratory's leadership class machines~\cite{errors_ecc}, and Schroeder and
Gibson state that a large number of CPU and memory failures were from parity
errors after tracking a five-year log of hardware replacements for a $765$ node
high-performance computing cluster~\cite{schroeder_gibson}. Echoing the
sentiment of Snir et al.~\cite{snir}, we believe that it is critical to
overcome the effect of such failures through a productive and performant
resilience framework. Doing so will become increasingly important as
soft failures become even more prevalent in upcoming exa-scale systems.

\begin{figure}
\begin{minipage}{.5\textwidth}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.80\textwidth]{images/soleil.png}
\caption{The above figure shows a portion of the application task graph for
Soleil-X, which is a particle solver written in the Legion parallel programming
model. The red arrow highlights a task, \texttt{particles\_pushall} that failed
during the execution.}
\label{fig:soleil}
\end{minipage}
\begin{minipage}{.5\textwidth}
\centering
\includegraphics[width=.80\textwidth]{images/coneofrestarts.png}
\caption{Illustration of tasks that are needed and affected due to the failure
of the task marked by `X'. Delegating the responsibility of identifying these
tasks and setting them up for re-execution on an application programmer is not
productive.}
\label{fig:coneofrestarts}
\end{minipage}
\end{figure}


A well-known solution to address soft failures is
global checkpoint-restart~\cite{checkpointrestart}. In this solution,
periodic global
checkpoints of the application's control and data are preserved and used for
recovery in the event of a failure. However, this solution will not be able to
handle the higher rate of soft failures expected of future machines nor be
performant enough to manage the data volumes generated by the working sets of
future applications. This is because as failure rates and data volumes grow,
increasingly large fractions of the overall execution time will be spent in
expensive, global interactions with the I/O subsystem needed to generate the
checkpoints. More performant solutions use fine-grained recovery mechanisms
that restart only the necessary portions of the original computation,
minimizing the data that must be regenerated. An example parallel framework
that argues in favor of fine-grained recovery mechanism is the Local Failure Local
Recovery framework (LFLR) developed by Teranishi et al.~\cite{lflr}, where the
LFLR model allows programmers to restrict the recovery footprint to the
processors affected by the failure. However, the LFLR model utilizes a
prototype implementation of MPI-ULFM\footnote{MPI's User Level Fault Mitigation layer
that is part of the future MPI 4.0 standard and is not supported by many MPI vendors
support it yet} and hence, the productivity and performance of their framework
was limited. 

%Several
%application-focused papers in the literature, e.g., \textbf{????},
%have demonstrated the need for local recovery in a distributed setting.
%Consider the illustration of a task graph in
%Figure~\cref{fig:restartandrollback}.  The nodes in the task graph represent
%tasks while the edges represent dependences between tasks.  To address the
%failure of the task marked with `X', 
%Thus we need a solution for fine-grained resilience that is
%configurable. 


Two critical aspects of a fine-grained resilience framework are the answers to
who sets the resilience policy and who implements the recovery mechanism.  
A resilience policy determines what data should be protected while the
resilience mechanism determines the impact of a failure in terms of the tasks
that need to be restarted and rescheduled, and re-executes these tasks while guaranteeing sequential consistency. 

\paragraph{Our position on Resilience Policy}
In general, it is difficult (if not impossible) for automatically determining
the application data that must be protected for resilience because of the
diverse natures of different applications. Utilizing heuristics to determine
critical application data makes a resilience framework cater to only specific
application domains.  For example, Sultana et al.~\cite{sultana} identified the
minimal application data needed for recovery of bulk-synchronous MPI
applications and implemented recovery using these states that were captured at
``MPI Stages". Because of the amount of data maintained, their recovery
solution is not applicable to other classes of applications.  

Similarly, if a resilience framework does not involve the programmer in the
runtime-decision loop to control task and data placements during recovery,
e.g., PaRSEC~\cite{parsec}, then such a framework is not productive as well.
Consider a portion of the task graph of a multi-physics particle solver,
Soleil-X, shown in ~\cref{fig:soleil}. Here, the colored boxes indicate
tasks in the application, while edges indicate dependences. If the task
\texttt{particles\_pushall}, marked in blue, fails, the programmer might want
to choose a different data placement for the inputs to the restarted task with
the concern that the previously chosen memory banks might be more prone to
ECC-related errors. Hence, we opine that the surgical control to restart a task
by using the same memory location as done during the failed execution or
choosing different data placements should be at the discretion of the
programmer.

\paragraph{Our position on Resilience Mechanism} 
The resilience mechanism involves determining the set of
successfully-executed and to-be-executed tasks affected by the
failure of a task, and execute the tasks in these sets while maintaining
sequential consistency.  One solution to this mechanism is developed by the X10
parallel programming model; X10 provides a well-defined \texttt{finish}
construct that captures the distributed completion of asynchronous tasks
launched within a block of code and defines the semantics when failures occur
in these tasks~\cite{X10}.  However, the tasks and data that need to be
launched and re-initialized, respectively, in response to the failure is
completely left to the programmer.  This solution is simply not
productive. 

Consider the illustration of a task graph in
~\cref{fig:coneofrestarts}. The nodes in the graph indicate tasks while edges
indicate dependences. If the task marked by `X' fails, then the two sets of
tasks, i.e., identified by the two cones originating from the failed task, need
to be identified. One set of previously-successfully-executed tasks
needed to regenerate the input to the failed task and one set of to-be-executed
tasks that might need to change their data placements in response to the failed
task's data placements during re-execution.  It is difficult, if not
impossible, for a programmer to manually orchestrate the re-execution of these
tasks. This mechanism is best done automatically by the parallel runtime that
constructed the task graph.      

%Such a solution is not productive as it leaves too much of the implementation
%(i.e., mechanism) to the user. For example, consider a portion of the task
%graph of a multi-physics particle solver, Soleil-X, shown in
%Figure~\ref{fig:soleil}. Here, the colored boxes indicate tasks in the
%application, while edges indicate dependences. If the task
%\texttt{particles\_pushall}, marked in blue, fails, it is highly unproductive
%for a programmer to   This recovery mechanism is best done
%automatically by a parallel runtime that constructured the task graph. The
%adjoining figure illustrates the possible due to the failure of
%the task.  

In toto, our position in this paper is that a productive resilience framework allows the
application programmer to specify the granularity, i.e., what data should be
protected (i.e., policy) while leaving the implementation of that protection to
the underlying programming system (i.e., mechanism). 

The contributions of this paper are as follows:
\begin{itemize}
\item A resilience framework that supports a diverse range of policies and a
performant implementation that realizes these policies. 
\item A well-defined semantics for recovery from failures during a parallel
execution.
\item A well-defined mechanism with programmer involvement during the recovery
phase, providing programmer's with the same freedom of data and compute
placements as during normal executions.
\item A resilience mechanism that can restart failures on any hybrid compute
architecture, e.g., CPUs and GPUs.
\item A debugging tool, Legin Spy++, to address a critical need of debugging
large parallel applications with failures. Legion Spy++ provides a visual
description of the point of failure, the parallel-runtime-identified task
sub-graph to address the failure, and the chosen data placements. 
%A debugging tool, namely, Legion Spy++, that provides a visual description of
%the failed task and the correcting task subgraph introduced to address the
%failure, along with the chosen data placements. 
\end{itemize}

The rest of the paper is organized as follows:

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "resilience"
%%% End:
