% formatting shortcuts
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mi}[1]{\mathit{#1}}
\newcommand{\ms}[1]{\mathsf{#1}}
\newcommand{\msc}[1]{\mathsc{#1}}
\newcommand{\mtt}[1]{\mathtt{#1}}
\newcommand{\ttt}[1]{\texttt{#1}}
\newcommand{\tsc}[1]{\textsc{#1}}

% parenthesis
\newcommand{\ttparen}[1]{\ttt{(}#1\ttt{)}}
\newcommand{\ttbracket}[1]{\ttt{[}#1\ttt{]}}
\newcommand{\ttbrace}[1]{\ttt{\{}#1\ttt{\}}}
\newcommand{\mset}[1]{\{#1\}}
\newcommand{\mangle}[1]{\langle{}#1\rangle{}}
\newcommand{\bigmangle}[1]{\big\langle{}#1\big\rangle{}}
\newcommand{\mparen}[1]{(#1)}
\newcommand{\mbbr}[1]{[#1]}

% misc.
\newcommand{\augsep}{\parallel}
\newcommand{\augment}[1]{\colorbox{lightgray}{$\!#1\!$}}
\newcommand{\emptyg}{G_{\varnothing}}
\newcommand{\emptyE}{E_{\varnothing}}
\newcommand{\emptyP}{P_{\varnothing}}
\newcommand{\none}{\bot}

\newcommand{\nodes}{T}
\newcommand{\edges}{D}
\newcommand{\graph}{G}
\newcommand{\retire}{R}
\newcommand{\commit}{C}
\newcommand{\memory}{M}
\newcommand{\snap}{S}
\newcommand{\mkgraph}[2]{\mangle{#1, #2}}
\newcommand{\dep}{\Leftrightarrow}

\newcommand{\readset}{\ms{rd}}
\newcommand{\writeset}{\ms{wr}}

\newcommand{\sem}[1]{[\![#1]\!]}
\newcommand{\proj}[2]{#1\downarrow_{#2}}
%\newcommand{\inv}[1]{\overleftarrow{#1}}
\newcommand{\inv}[1]{#1^{-1}}
\newcommand{\texec}{\stackrel{\ms{exec}}{-->}}
\newcommand{\trexec}{\stackrel{\ms{rexec}}{-->}}
\newcommand{\tpexec}{\stackrel{\ms{rexec}}{-->}}

\newtheorem{thm}{Theorem}
\newtheorem{assume}{Assumption}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{df}{Definition}
\newtheorem{ex}{Example}

\section{Resilience Framework}
\label{sec:formalism}

% A summary of framework
%  goal: the program produces the same result as if there were no failures
%  method: recover a failure by locally re-executing tasks that produce the input of the failed portion
%  prerequisites:
%    a program is represented as a task graph that encodes data dependencies
%    tasks in a program update the memory that is divided into regions 
%    tasks have well-defined semantics whose input and output regions are known
%  bonus point: recovery is an idempotent process, allowing failures in the recover task graph to be handled

% motivate snapshot points
%  the amount of necessary rollback can be great, especially with in-place udpates in tasks that are common
%  users can specify a policy to insert snaphost points each of which makes a redundant copy of a region in a separate storage
%  a policy describes a set of tasks whose output should be persisted
%  snapshot points can reduce the amount of rollback by preserving old versions of regions
%  they give control to users on how resilient system behaves

% definition of the baseline
%  a system that executes task graphs in the absence of failures
%  to which the resilient system is equivalent

% definition of our resilient sytem
%  failures are modeled as a non-deterministic choice after task execution
%  task commits in two steps
%  extension to program state: snapshot stores, retiring set, commit set
%  rollback
%  renaming


The goal of our resilience framework is to provide {\em local} recovery from soft failures.
%
Our framework recovers from a failure during execution by identifying and re-executing only the parts that are necessary to repair the failed part.
%
To facilitate this local recovery, we decompose a parallel program into {\em tasks}, a unit of computation with well-defined input and output parameters, and represent it using a {\em task graph}, a DAG of tasks whose edges represent data dependencies between tasks.
%
The framework then can recover from a failure by re-executing the whole failed task.
%
When tasks perform {\em in-place} updates, i.e., have parameters serve as both inputs and outputs, values of those parameters are corrupted during the failed execution and thus not usable for an immediate re-execution.
%
In such cases, the framework reconstructs the values of the parameters prior to the failed execution by re-running the tasks that produced them.
%
To perform this reconstruction, the framework exploits producer-consumer relationships between tasks recorded in the task graph.

When an input value of a failed task was constructed by a long chain of tasks on producer-consumer relationships, the reconstruction could require many tasks to be re-executed even with our local recovery scheme.
%
In an extreme case where a program has an initialization task followed by accumulator tasks that all accumulate their values to the same memory region, a failure in any accumulator task would make the execution roll back to the initialization task at the beginning.
%
Our framework uses {\em snapshots} to ``cut'' a long chain of data dependencies, thereby reducing the amount of re-execution.
%
A snapshot is the value of an output parameter of a task copied to a persistent storage.
%
When the framework traverses backward in a task graph to reconstruct an input value of a failed task, it can use snapshots to replace the execution of their producer tasks.
%
Since a fully automatic planning of snapshots agnostic to applications is, though desirable, often ineffective, our framework takes a {\em resilience policy} as an input to configure the resilience mechanism.
%
A policy prescribes a set of tasks for which snapshots must be taken, and the framework augments the task graph with {\em snapshot points}, operations that make snapshots for variables during execution, according to that policy.

The rest of this section formalizes our resilience framework and shows that it provides tolerance to soft failures by proving its equivalence to a model of the failure-free task graph execution.

\subsection{Tasks and task graphs}

A task is a unit of computation that executes atomically once scheduled on a processor.
%
The computation of a task is described by a semantics function on memory states;
%
a memory state is a map from variables to values.
%
Each task is distinguished by a unique identifier.
%
\begin{equation*}
  \begin{array}{r@{\ }r@{\ }c@{\ }l}
    t \in & \mi{Task} & = & \mi{Func} \times \mi{Id}
    \\
    f, g, h \in & \mi{Function} & = & 2^{\mi{Memory} -> \mi{Memory}}
    \\
    m \in & \mi{Memory} & = & \mi{Variable} -> \mi{Value}
    \\
    x, y, z, w \in & \mi{Variable}
  \end{array}
\end{equation*}
%
For a task $t$, we write $\sem{t}$ for $t$'s semantics function and $\mi{id}(t)$ for $t$'s identifier.
%
For a semantics function $f$, we write $\readset(f)$ (resp. $\writeset(f)$) for the set of input variables of $f$ (resp. the set of output variables of $f$), and we use $\ms{var}(f)$ for the set of all variables used by $f$, i.e., $\ms{var}(f) \triangleq \readset(f) \cup \writeset(f)$.
%
The same variable can be used as both an input and an output for a task, and constant functions have no input variables.
%
We assume that a semantics function produces values of its output variables using only values of its input variables and updates only its output variables.
%
\begin{assume}
  \label{assumption:read-determinism}
  \begin{equation*}
    \begin{array}{l}
      \forall \memory, \memory' \in \mi{Memory}.
      \\
      \quad (\forall x \in \readset(f). \memory(x) = \memory'(x)) \implies f(\memory) = f(\memory')
    \end{array}
  \end{equation*}
\end{assume}
%
\begin{assume}
  \begin{equation*}
    \forall x. \memory(x) \ne f(\memory)(x) \implies x \in \writeset(f)
  \end{equation*}
\end{assume}

Tasks have a dependence when they access the same variable and at least one of them can update the variable.
%
We write $t \dep t'$ when tasks $t$ and $t'$ are dependent and $\ms{conf}(t, t')$ for the set of variables shared by $t$ and $t'$ and updated by one of them.
%
\begin{equation*}
  \begin{array}{@{}r@{\ }c@{\ }l@{}}
    \ms{conf}(t, t') & \triangleq & \ms{var}(\sem{t}) \cap \ms{var}(\sem{t'}) -
                                    \readset(\sem{t}) \cap \readset(\sem{t'})
    \\
    t \dep t' & \triangleq & \ms{conf}(t, t') \ne \varnothing
  \end{array}
\end{equation*}

We use task graphs to represent parallel programs.
%
A task graph is a DAG of tasks whose edges represent data dependencies.
%
Any parallel execution of tasks must respect all data dependencies encoded in this task graph.
%
\begin{equation*}
  \graph = \mkgraph{\nodes}{\edges} \in \mi{TaskGraph} =
  2^{\mi{Task}} \times 2^{\mi{Task}\times\mi{Task}}
\end{equation*}
%
We assume that every task graph $\graph$ is cycle-free:
%
\begin{assume}
  Every task graph $\mkgraph{\nodes}{\edges}$ is a DAG:
  \begin{equation*}
    \forall t \in \nodes. (t, t) \not \in \edges \land
    \forall t, t' \in \nodes. (t, t') \in \edges^{\star} \implies (t', t) \not \in \edges^{\star}
  \end{equation*}
\end{assume}
%
A task graph $\graph$ is well-formed when the following conditions hold:
\begin{enumerate}
\item Every edge in $\graph$ represents a data dependence between tasks.
\item Every two dependent tasks are connected by a path.
\end{enumerate}
% 
\begin{df}
  \begin{equation*}
    \begin{array}{l}
      \mkgraph{\nodes}{\edges}\text{ is well-formed}\ \triangleq
      \\
      \quad
      \forall t, t' \in \nodes. (t, t') \in \edges \implies t \dep t'\ \land
      \\
      \quad
      \forall t, t' \in \nodes. t \dep t' \implies (t, t') \in \edges^{\star}
    \end{array}
  \end{equation*}
\end{df}
\noindent
%
By construction, any parallel execution respecting a well-formed task graph is data-race free.
%
We consider only well-formed task graphs in our discussion.
% %
% Finally, we use the following notations for task graphs in the rest of this section:
% %
% \begin{equation*}
%   t \in \mkgraph{\nodes}{\edges} \triangleq t \in \nodes \quad\quad
%   (t, t') \in \mkgraph{\nodes}{\edges} \triangleq (t, t') \in \edges
% \end{equation*}

\subsection{Failure-free execution of task graphs}

{
\floatstyle{boxed}
\restylefloat{figure}

\begin{figure}[t!]
  \centering
  \small
  \begin{equation*}
    \begin{array}{l}
      \framebox{$
      (\graph, \memory) \texec (\graph, \memory)
      $}
      \\
      \\
      \begin{array}{c}
        \inference
        {
        t \in \nodes \quad \not \exists t'. (t', t) \in \edges
        \\
        \nodes' = \nodes - \mset{t} \quad \edges' = \proj{\edges}{\nodes'}
        \quad
        \memory' = \sem{t}(\memory)
        }
        {(\mkgraph{\nodes}{\edges}, \memory) \texec (\mkgraph{\nodes'}{\edges'}, \memory')}
      \end{array}
    \end{array}
  \end{equation*}
  \caption{Normal graph execution}
  \label{fig:normal-exec}
\end{figure}
}

We define a model of the failure-free task graph execution as the baseline;
%
the resilient execution described in the next section must be equivalent to this model.

In the absence of failures, we can execute a task graph by continuously exhausting ``ready'' tasks, i.e., by repeatedly finding and executing a task with no predecessors in the graph and removing it, which in turn makes other tasks ready.
%
The transition relation in Figure~\ref{fig:normal-exec} shows a single step of this task graph execution;
%
a transition $(\graph, \memory) \texec (\graph', \memory')$ finds a ready task $t$ in the task graph $\graph$; updates the memory state $\memory$ using $t$'s semantics function, yielding the next memory state $\memory'$; and removes $t$ from $\graph$, yielding the task graph $\graph'$ having one fewer task.
%
In the rule, we write $\proj{A}{B}$ for a subset of a relation $A$ projected onto a set $B$:
%
\begin{equation*}
  \proj{A}{B} \triangleq \mset{(x, y) \in A \mid x, y \in B}
\end{equation*}
%
The execution ends when there is no task left in the task graph.

\subsection{Resilient execution of task graphs}

The first step in the resilient execution of a task graph is to augment the graph with {\em snapshot points}.
%
A snapshot point is an operation in the task graph, which makes a snapshot for an output variable of a task.
%
Each snapshot point $s_x$ consists of a unique identifier $s$ and a variable $x$ for which it makes a snapshot:
%
\begin{equation*}
  s_x \in \mi{SnapshotPoint} = \mi{Id} \times \mi{Variable}
\end{equation*}
%
Snapshot points for a task graph are specified by a policy, which is a set of pairs of tasks and variables.
%
\begin{equation*}
  \pi \in \mi{Policy} = 2^{\mi{Task}\times\mi{Variable}}
\end{equation*}
%
The augmentation inserts a snapshot point $s_x$ to for every pair of a task $t$ and a variable $x$ in the policy to the task graph and makes $s_x$ run before all $t$'s subsequent tasks using $x$;
%
we write $\pi(\graph)$ for a task graph $\graph$ augmented with snapshot points prescribed by a policy $\pi$, defined as follows:
%
\begin{equation*}
  \begin{array}{r@{\ }c@{\ }l}
    \multicolumn{3}{l}{
    \pi(\mkgraph{\nodes}{\edges}) \triangleq \mkgraph{\nodes'}{\edges'}\text{ where }
    }
    \\
    \quad \nodes' & = & \nodes \cup \mset{s_x \mid (t, s_x) \in P}
    \\
    \quad \edges' & = & \edges \cup P\ \cup
    \\
    & & \mset{(s_x, t') \mid (t, s_x) \in P \land (t, t') \in \edges \land x \in \ms{var}(\sem{t'})}
    \\
    \quad P & = & \{(t, s_x) \mid t \in \nodes \land (t, x) \in \pi \land x \in \writeset(\sem{t})\ \land
    \\
    & & \qquad \quad\ \ \, s\text{ is fresh}\}
  \end{array}
\end{equation*}
%
Figure~\ref{fig:ex-augment} shows an example of an augmented task graph.

\begin{figure}[t!]
  \centering
  \small
  \begin{tabular}{@{}c@{}c@{}}
    \begin{tabular}{@{}c@{}}
      \begin{minipage}{0.45\linewidth}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
          \tikzstyle{every state}=
          [rounded corners,rectangle,fill=none,draw=black,text=black,inner sep=1.5,minimum height=6mm]

          \node[state,label=left:$t_1$] (T1) {$x=f(y)$};

          \node[above=0.3 of T1] (dummy1) {};
        
          \node[below=0.5 of T1] (center) {};
          
          \node[state,left=0.2 of center] (T2) {$z=g(x)$};
          \node[state,right=0.2 of center] (T3) {$y=h(x, y)$};
          
          \draw (dummy1) -- (T1);
          \draw (T1) -- (T2);
          \draw (T1) -- (T3);
        \end{tikzpicture}      
        \subcaption{$\graph$}
      \end{minipage}
    \end{tabular}
    &
    \begin{tabular}{@{}c@{}}
      \begin{minipage}{0.45\linewidth}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
          \tikzstyle{every state}=
          [rounded corners,rectangle,fill=none,draw=black,text=black,inner sep=1.5,minimum height=6mm]

          \node[state,label=left:$t_1$] (T1) {$x=f(y)$};

          \node[state,below=0.3 of T1] (S1) {$s_x$};

          \node[above=0.3 of T1] (dummy1) {};
        
          \node[below=0.5 of S1] (center) {};
          
          \node[state,left=0.2 of center] (T2) {$z=g(x)$};
          \node[state,right=0.2 of center] (T3) {$y=h(x, y)$};
          
          \draw (dummy1) -- (T1);
          \draw (T1) -- (S1);
          \path (T1) edge [bend right=15] (T2) {};
          \path (T1) edge [bend left=15] (T3) {};
          \draw (S1) -- (T2);
          \draw (S1) -- (T3);
        \end{tikzpicture}      
        \subcaption{$\pi(\graph)\ \ (\pi = \mset{(t_1, x)})$}
      \end{minipage}
    \end{tabular}
  \end{tabular}
  \caption{Example of an augmented task graph}
  \label{fig:ex-augment}
\end{figure}

{
\floatstyle{boxed}
\restylefloat{figure}

\begin{figure}[t!]
  \centering
  \small
  \begin{equation*}
    \begin{array}{l}
      \framebox{$
      (\graph, \memory, \retire, \commit, \snap) \trexec (\graph, \memory, \retire, \commit, \snap)
      $}
      \\
      \\
      \begin{array}{c}
        \inference[\tsc{Trun}]
        {
        t \in \nodes \quad \not \exists t'. (t', t) \in \edges \land t' \not \in \commit
        \\
        \memory' = \sem{t}(\memory') \quad \retire' = \retire \cup \mset{t}
        }
        {
        (\mkgraph{\nodes}{\edges}, \memory, \retire, \commit, \snap) \trexec 
        (\mkgraph{\nodes}{\edges}, \memory', \retire', \commit, \snap)
        }
        \\
        \\
        \inference[\tsc{Tsucc}]
        {
        t \in R
        \quad
        t \rightsquigarrow \ms{success}
        \quad
        \retire' = \retire - \mset{t}
        \quad
        \commit' = \commit \cup \mset{t}
        }
        {
        (\graph, \memory, \retire, \commit, \snap) \trexec
        (\graph, \memory, \retire', \commit', \snap)
        }
        \\
        \\
        \inference[\tsc{Tsnap}]
        {
        s_x \in \nodes \quad \not \exists t'. (t', s_x) \in \edges \land t' \not \in \commit
        \\
        \snap' = \snap[(s, x) \mapsto \memory(x)]
        }
        {
        (\mkgraph{\nodes}{\edges}, \memory, \retire, \commit, \snap) \tpexec
        (\mkgraph{\nodes}{\edges}, \memory, \retire, \commit, \snap')
        }
        \\
        \\
        \inference[\tsc{Tfail}]
        {
        t \in R
        \quad
        t \rightsquigarrow \ms{failure}
        \quad
        \graph_r = \ms{recover}(\graph, t)
        \\
        t' = \ms{sink}(\graph_r)
        \quad
        \graph' = \graph \oplus_{t,t'} \graph_r
        \\
        \retire' = \retire - \mset{t}
        \quad
        \commit' = \commit \cup \mset{t}
        }
        {
        (\graph, \memory, \retire, \commit, \snap) \trexec
        (\graph', \memory, \retire', \commit', \snap)
        }
        \\
        \\
        \inference[\tsc{Trcvr}]
        {
        \inv{s}_{x->y} \in \nodes
        \quad
        \memory' = \memory[y \mapsto \snap(s, x)]
        }
        {
        (\graph, \memory, \retire, \commit, \snap) \tpexec
        (\graph, \memory', \retire, \commit, \snap)
        }
      \end{array}
    \end{array}
  \end{equation*}
  \caption{Resilient graph execution}
  \label{fig:resilient-exec}
\end{figure}
}

Figure~\ref{fig:resilient-exec} shows our system for the resilient task graph execution.
%
In addition to an augmented task graph and a memory state, the system also maintains a set of retiring tasks, a set of committed tasks, and a snapshot storage.
%
Since a soft failure in a task can be recognized during or even after the execution of the task, the system must {\em commit} the task only after it confirms that the task executed successfully.
%
This commit protocol is expressed by the rules \tsc{Trun} and \tsc{Tsucc} in Figure~\ref{fig:resilient-exec} as follows:
%
\begin{enumerate}
\item In the rule \tsc{Trun}, a task $t$ is added to the set $\retire$ of retiring tasks once it is executed (i.e., its semantics function $\sem{t}$ is applied to the memory state $\memory$).

\item In the rule \tsc{Tsucc}, if a retiring task $t$ in $R$ turns out to be successful, the task is added to the set $C$ of committed tasks.
  %
  When the rule \tsc{Trun} determines the readiness of a task $t$ by checking the predecessors, committed predecessors of the task are ignored, as they have been completed successfully.
\end{enumerate}

When the system finds a snapshot point $s_x$ ready in the task graph (rule \tsc{Tsnap}), it makes a snapshot for the variable $x$ in the snapshot storage $\snap$.
%
A snapshot storage is a map from pairs of identifiers and variables to values;
%
as the tasks updating the same variable can potentially produce different values, their snapshots are distinguished by snapshot points' identity.
%
\begin{equation*}
  \snap \in \mi{SnapStorage} = \mi{Id} \times \mi{Variable} -> \mi{Value}
\end{equation*}
%
Unlike tasks, which mutate memory states, snapshot points are idempotent and thus the system can repeatedly execute them until they succeed;
%
we simply assume that snapshot points do not fail.

{
\begin{figure*}[t!]
  \centering
  \begin{tabular}{@{}c@{}c@{}}
    \begin{tabular}{@{}c@{}}
      \begin{minipage}{0.6\linewidth}
        \centering
        \small
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
          \tikzstyle{every state}=
          [rounded corners,rectangle,fill=none,draw=black,text=black,inner sep=1.5,minimum height=6mm]

          \node[state] (S1) {$s_x$};

          \node[state,left=0.4 of S1,label=above:$t_1$]   (T1) {$x=f_1(y)$};

          \node[state,label=above:$t_2$,right=0.4 of S1] (T2) {$x,y=f_2(x)$};

          \node[state,label=below:$t_3$,below=0.5 of T2] (T3) {$y=f_3(y)$};

          \node[state,right=0.7 of T2] (S2) {$s'_x$};

          \node[state,fill=lightgray,below=0.5 of S2,label=right:$t_{\lightning}$] (T4) {$z = f_4(x, y)$};
          
          \node[state,left=0.5 of T3] (T5) {$w = f_5(y)$};

          \node[rectangle,left=0.4 of T1] (dummy1) {$\cdots$};

          \node[rectangle,below=0.4 of T4,inner sep=0] (dummy2) {$\vdots$};

          \node[rectangle,below=0.4 of T5,inner sep=0] (dummy3) {$\vdots$};

          \node[rectangle,right=0.4 of S2] (dummy4) {$\cdots$};

          \draw (T1) -- (S1);

          \draw (S1) -- (T2);

          \draw (T2) -- (S2);

          \draw (T2) -- (T3);

          \draw (S2) -- (T4);

          \draw (T3) -- (T4);

          \draw (T3) -- (T5);

          \draw (dummy1) -- (T1);

          \draw (T4) -- (dummy2);

          \draw (T5) -- (dummy3);

          \draw (S2) -- (dummy4);

        \end{tikzpicture}      
        \subcaption{Task  $t_{\lightning}$ in task graph $\graph$ fails}
        \label{subfig:ex-failure}
      \end{minipage}
    \end{tabular}
    &
    \begin{tabular}{@{}c@{}}
      \begin{minipage}{0.4\linewidth}
        \centering
        \small
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
          \tikzstyle{every state}=
          [rounded corners,rectangle,fill=none,draw=black,text=black,inner sep=1.5,minimum height=6mm]

          \node[state] (S1) {$s_x$};

          \node[state,label=above:$t_2$,right=0.4 of S1] (T2) {$x,y=f_2(x)$};

          \node[state,label=below:$t_3$,below=0.5 of T2] (T3) {$y=f_3(y)$};

          \node[state,right=0.7 of T2] (S2) {$s'_x$};

          \node[state,fill=lightgray,below=0.5 of S2,label=right:$t_{\lightning}$] (T4) {$z = f_4(x, y)$};

          \draw (S1) -- (T2);

          \draw (T2) -- (S2);

          \draw (T2) -- (T3);

          \draw (S2) -- (T4);

          \draw (T3) -- (T4);

        \end{tikzpicture}      
        \subcaption{$\ms{rollback}(\graph, t_{\lightning})$}
        \label{subfig:ex-rollback}
      \end{minipage}
    \end{tabular}
    \\
    \begin{tabular}{@{}c@{}}
      \begin{minipage}{0.6\linewidth}
        \centering
        \small
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
          \tikzstyle{every state}=
          [rounded corners,rectangle,fill=none,draw=black,text=black,inner sep=1.5,minimum height=6mm]

          \node[state] (S1) {$s_x$};

          \node[state,below=0.5 of S1] (R1) {$\inv{s}_{x \to x}$};

          \node[state,label=above:$t_2$,right=0.4 of S1] (T2) {$x,y=f_2(x)$};

          \node[state,label=below:$t_3$,below=0.5 of T2] (T3) {$y=f_3(y)$};

          \node[state,right=0.7 of T2] (S2) {$s'_x$};

          \node[state,fill=lightgray,below=0.5 of S2,label=right:$t_{\lightning}$] (T4) {$z = f_4(x, y)$};

          \draw (S1) -- (T2);

          \draw (R1) -- (S1);

          \draw (T2) -- (S2);

          \draw (T2) -- (T3);

          \draw (S2) -- (T4);

          \draw (T3) -- (T4);

        \end{tikzpicture}      
        \subcaption{$\ms{invert}(\ms{rollback}(\graph, t_{\lightning}))$}
        \label{subfig:ex-invert}
      \end{minipage}
    \end{tabular}
    &
    \begin{tabular}{@{}c@{}}
      \begin{minipage}{0.4\linewidth}
        \centering
        \small
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
          \tikzstyle{every state}=
          [rounded corners,rectangle,fill=none,draw=black,text=black,inner sep=1.5,minimum height=6mm]

          \node[state] (S1) {$s_{x'}$};

          \node[state,below=0.5 of S1] (R1) {$\inv{s}_{x \to x'}$};

          \node[state,label=above:$t'_2$,right=0.4 of S1] (T2) {$x',y'=f_2(x')$};

          \node[state,label=below:$t'_3$,below=0.5 of T2] (T3) {$y'=f_3(y')$};

          \node[state,right=0.7 of T2] (S2) {$s'_{x'}$};

          \node[state,fill=lightgray,below=0.5 of S2,label=right:$t'_{\lightning}$] (T4) {$z = f_4(x', y')$};

          \draw (S1) -- (T2);

          \draw (R1) -- (S1);

          \draw (T2) -- (S2);

          \draw (T2) -- (T3);

          \draw (S2) -- (T4);

          \draw (T3) -- (T4);

        \end{tikzpicture}      
        \subcaption{$\ms{alpha}(\ms{invert}(\ms{rollback}(\graph, t_{\lightning})))$}
        \label{subfig:ex-alpha}
      \end{minipage}
    \end{tabular}
  \end{tabular}
  \caption{Example of a recovery}
  \label{fig:ex-recovery}
\end{figure*}
}

If a retiring task turns out to be failed (rule \tsc{Tfail}), the system must recover from the failure.
%
For the recovery, the system identifies the subgraph that must be re-executed and inserts it to the task graph;
%
by merging the subgraph for recovery with the task graph being executed, any failures during recovery are handled in the same way as the failures in the normal execution.
%
The function $\ms{recover}(\graph, t)$ finds a {\em recovery subgraph} in $\graph$ necessary to repair a failed task $t$.
%
\begin{df}
  \begin{equation*}
    \ms{recover}(\graph, t) \triangleq \ms{alpha}(\ms{invert}(\ms{rollback}(\graph, t)))
  \end{equation*}
\end{df}
\noindent
The recovery subgraph is constructed in three steps:
% 
First, a subgraph $\graph'$ of the task graph to reconstruct the input values of $t$ is identified (the function $\ms{rollback}$);
%
Second, for each {\em source} snapshot point $s_x$, i.e., a snapshot point having no predecessors in $\graph'$, we introduce a recovery point that performs the inverse of $s_x$, i.e., copies the snapshot of $x$ from the snapshot storage to the memory state (the function $\ms{invert}$);
%
Third, the tasks in $\graph'$ are alpha-converted so that the recovery does not interfere with the rest of the execution (the function $\ms{alpha}$).
%
In the following, we explain each of the steps in detail using the example in Figure~\ref{fig:ex-recovery}.

In the rollback step, we gather tasks that produce inputs for the failed task $t$ by traversing backward edges to $t$.
%
Since those producers may also depend on other tasks for their inputs, we must recursively traverse the task graph until we reach tasks using constant functions;
%
for the purpose of recovery, a snapshot point $s_x$ is treated as a constant function, i.e., $\readset(s_x) = \varnothing$ and $\writeset(s_x) = \mset{x}$.
%
\begin{df}
  \begin{equation*}
    \begin{array}{l}
      \ms{rollback}(\mkgraph{\nodes}{\edges}, t) \triangleq
      \mkgraph{\nodes'}{\proj{\edges}{\nodes'}} \text{ where }
      \\[0.4em]
      \quad \nodes' = \ms{backtrack}(\mkgraph{\nodes}{\edges}, t, \readset(t))
      \\[0.4em]
      \quad \ms{backtrack}(\mkgraph{\nodes}{\edges}, t, V)\ \triangleq
      \\[0.4em]
      \quad\quad
      \displaystyle
      \bigcup_{(t', t) \in \edges \land \writeset(t')\, \cap\, V \ne \varnothing }
      \ms{backtrack}(\mkgraph{\nodes}{\edges}, t', \readset(t')) \cup \mset{t}
    \end{array}
  \end{equation*}
\end{df}
\noindent
Figure~\ref{subfig:ex-rollback} shows a subgraph of the graph in Figure~\ref{subfig:ex-failure} to reconstruct inputs for the failed task $t_{\lightning}$.
%
The rollback eventually stops once it reaches the snapshot point $s_x$ that has no input variables.
%
Note that this rollback does not work when the task graph has edges for transitive dependencies;
%
for example, if the task graph $\graph$ had an edge between $t_1$ and $t_{\lightning}$, which denotes a transitive dependence, the rollback would not stop at $s_x$ but traverse further from $t_1$ backward.
%
For brevity, we assume that task graphs have no edges for transitive dependencies, and in the implementation, we  enforce that the rollback visits tasks in reverse topological order.
%
\begin{assume}
  \begin{equation*}
    \forall t_1, t_2. (t_1, t_2) \in \edges \implies \not \exists t_3. 
    (t_1, t_3) \in \edges^{\star} \land (t_3, t_2) \in \edges^{\star}
  \end{equation*}
\end{assume}

In the next step, we insert recovery points to the recovery graph to restore snapshots in the memory state.
%
Recovery points are necessary for only the source snapshot points, because snapshots for the other snapshot points are computed again when thier producers are re-executed.
%
For example, in Figure~\ref{subfig:ex-rollback}, the snapshot point $s_x'$  does not need a recovery point as its pproducer $t_2$ is already included in the recovery graph, whereas the source snapshot point $s_x$ introduces a recovery point $\inv{s}_{x \to x}$ in Figure~\ref{subfig:ex-invert}.
%
The function $\ms{invert}$ is defined as follows:
%
\begin{df}
  \begin{equation*}
    \begin{array}{l}
      \ms{invert}(\mkgraph{\nodes}{\edges}) = \mkgraph{\nodes'}{\edges'}\text{ where }
      \\
      \quad R = \mset{\inv{s}_{x \to x} \mid s_x \in \nodes \land \not \exists t. (t, s_x) \in \edges}
      \\
      \quad \nodes' = \nodes \cup R
      \quad \edges' = \edges \cup \mset{(\inv{s}_{x \to x}, s_x) \mid \inv{s}_{x \to x} \in R}
    \end{array}
  \end{equation*}
\end{df}
\noindent 
The rule \tsc{Trcvr} in Figure~\ref{fig:resilient-exec} describes the semantics of recovery points;
%
a recovery point $\inv{s}_{x \to y}$ retrieves the snapshot made by the snapshot point $s_x$ and assigns it to the variable $y$.
%
Note that recovery points do not necessarily store snapshots in their original locations, because of the renaming explained in the next paragraph.

Tasks in a recovery graph must be alpha-converted so that they do not interfere with the normal execution.
%
The alpha conversion is done with a bijection $\sigma$ that maps variables in the original task graph to a set of fresh variables.
%
This bijection, however, maps the output variables of the failed task to themselves because we want the re-execution of the task to produce output values in their original locations as if it normally executed.
%
The conversion $\sigma(t)$ of a task $t$ with a bijection $\sigma$ is defined as follows:
%
\begin{df}
  \begin{equation*}
    \begin{array}{@{\ }l@{\ }}
      \sigma(t) \triangleq t' \text{ such that }
      \\
      \ \forall M, M'. M(x) = M'(\sigma(x))\!\implies\!\sem{t}(M)(x) = \sem{t'}(M')(\sigma(x))
      \\
      \sigma(s_x) \triangleq s_{\sigma(x)}
      \quad
      \sigma(\inv{s}_{x \to x}) \triangleq \inv{s}_{x \to \sigma(x)}
    \end{array}
  \end{equation*}
\end{df}
\noindent
Note that the conversion of a recovery point only changes the variable to which the point assigns the snapshot to make it copy from the right location in the snapshot storage.

The final step for the recovery in the rule \tsc{Tfail} is to insert the recovery graph $\graph_r$ to the task graph $\graph$ by connecting the {\em sink}, i.e., a node with no successors, in $\graph_r$ to the successors of the failed task in $\graph$;
%
any recovery graph constructed by the function $\ms{recover}$ has only a single sink, which correponds to the failure task after renaming.
%
The function $\ms{sink}$ and the opeerator $\oplus$ are defined below:
%
\begin{df}
  $\ms{sink}(\mkgraph{\nodes}{\edges}) = t$ such that $\not \exists t'. (t, t') \in \edges$
\end{df}
%
\begin{lem}
  $\ms{sink}(\ms{rollback}(\graph, t))$ is well-defined.
\end{lem}
%
\begin{df}
  \begin{equation*}
    \begin{array}{l}
      \mkgraph{\nodes}{\edges} \oplus_{t, t'} \mkgraph{\nodes'}{\edges'} \triangleq
      \mkgraph{\nodes \cup \nodes'}{\edges \cup \edges' \cup \edges_{t \to t'}} \text{ where }
      \\
      
\quad \edges_{t \to t'} = \mset{(t', t'') \mid (t, t'') \in \edges}
    \end{array}
  \end{equation*}
\end{df}

A resilient execution of a task graph leads to a memory state that is equivalent to that produced by a failure-free execution of the graph.
%
\begin{thm}
  For a task graph $\graph = \mkgraph{\nodes}{\edges}$ and a resilient policy $\pi$, the following holds:
  \begin{equation*}
    \begin{array}{l}
      (\graph, \varnothing) \texec^{\star} (\emptyg, M)\ \land
      \\
      (\pi(\graph), \varnothing, \varnothing, \varnothing, \varnothing) \trexec^{\star}
      (\pi(\graph), M', \varnothing, \nodes', S) \land \nodes \subseteq \nodes'
      \\
      \implies \forall x \in \mi{dom}(M). M(x) = M'(x)
    \end{array}
  \end{equation*}
\end{thm}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "resilience"
%%% End:
