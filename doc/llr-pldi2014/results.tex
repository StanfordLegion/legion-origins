
\section{Application Evaluation}
\label{sec:results}


In \cite{Legion12}, several applications were ported to the Legion
programming model and shown to outperform independently tuned MPI applications
by up to 5.4X.  To quantify the performance of our \Realm implementation, we use the
same applications and profile several aspects of performance.  We first look at the use 
of \Realm events by the applications and the Legion runtime in Section~\ref{subsec:eventlife}.  
Section~\ref{subsec:resperf} covers the use of reservations.  Finally, in 
Section~\ref{subsec:bulkcomp} we attempt to estimate the performance benefit of the 
deferred execution model by modifying the Legion runtime to explicitly wait for the 
completion of subtasks (effectively returning to a bulk-synchronous execution model) 
and measuring the resulting slowdown.
%We quantify the performance 
%of our \Realm implementation on three real 
%applications written in the Legion programming model.  

All three applications are multi-phase and
require parallel computation, data exchange, 
and synchronization between phases.  {\em Circuit}
is the circuit simulation described in 
Section~\ref{sec:circuit_ex}.
%We first look at the performance of
%\Realm events and illustrate the need for generational
%events in Section~\ref{subsec:eventlife}.  In 
%Section~\ref{subsec:resperf}, we demonstrate that \reservations
%are not a bottleneck for any of our
%applications.  Finally, in Section~\ref{subsec:bulkcomp}
%we quantify the performance advantages conferred by
%a deferred execution model compared to a
%bulk-synchronous execution model.

%Both the applications level code as well as
%the Legion runtime code should be thought of as the 
%client for our \Realm implementation as even runtime
%level tasks can be deferred in Legion.  


{\em Fluid} is an incompressible fluid flow simulation from the PARSEC
benchmark suite\cite{bienia11benchmarking},  
%The reference
%implementation only runs on shared memory machines, but when rewritten
%in Legion, Fluid runs on distributed machines as well.  
which models particles flowing through cells.  Each time
step involves multiple phases updating different properties of the
particles using properties of neighboring particles. % in the same cell.  
%The
%space of cells is partitioned and neighboring cells in different
%partitions must exchange data between phases.  Legion ensures that
%these exchanges are done point-to-point by chaining copies and tasks
%using events rather than employing a global bulk-synchronous approach.

{\em AMR} is an adaptive mesh refinement benchmark based on the third heat
equation example from the Berkeley Labs BoxLib project\cite{BoxLib}.  AMR
simulates the two dimensional heat diffusion equation using three different levels
of refinement.  
%Each level is partitioned and distributed across the machine.  
%nTime steps require both intra- and inter-level communication and 
%synchronization.  Legion again expresses dependences between tasks from 
%the same and different levels through events.

\subsection{Event Lifetimes}
\label{subsec:eventlife}
%We instrumented the heterogeneous implementation of \Realm to 
%capture event usage information.  
Figure~\ref{fig:eventlife} shows
a timeline of the execution of the Fluid application on 16 nodes using 128 cores.
The dynamic events line measures the total number of event creations.
A large number of events are created---over 260,000 in less than 15
seconds of execution---and allocating separate storage for every event would clearly
be difficult for long-running applications.  

An event is {\em live} until its last operation 
(e.g., wait, trigger) is performed.  After
an event's last operation a reference counting implementation would
recover the event's associated storage.  The live events line
in Figure~\ref{fig:eventlife} is therefore the number of needed events in a reference counting
scheme.  In this example,  reference counting reduces
the storage needed for dynamic events by over 10X, but at the cost of
computation and communication for the reference counting.  

As discussed in Section~\ref{subsec:eventimpl}, our implementation
requires storage that grows with the maximum number of untriggered events, 
a number that is 10X smaller than even the maximal live event count.  The 
actual storage requirements of our \Realm implementation are shown by 
the generational events line, which shows the total number of generational 
events (recall Section~\ref{subsec:eventimpl}) in the system.
The maximum number of generational events needed is slightly larger than 
the peak number of untriggered events because
nodes create events locally if they have no free generational events, 
even if there are unused generational events on remote nodes.
Overall, our implementation uses 5X less storage
than a reference counting implementation and avoids any overhead related to 
reference counting.  
%These savings would likely be even more dramatic for longer 
%runs of the application, as the number of live events is steadily growing as the
%application runs, while the peak number of generational events needed appears to 
%occur during the start-up of the application.

\subsection{\Reservation Performance}
\label{subsec:resperf}
%We also instrumented our heterogeneous implementation of \Realm to profile
%the usage of \reservations in real applications.  
The Circuit and AMR 
applications both made use of \reservations, creating 3336 and 1393 
\reservations respectively.  Of all created \reservations in both applications, 
14\% were migrated at least once.  
The grant rates for both applications are orders of magnitude smaller
than the maximum \reservation grant rates achieved by our \reservation micro-benchmarks
in Section~\ref{subsec:acquiremicro}.  Thus, for these benchmarks \reservations
were needed to express non-blocking synchronization 
and were far from being  a performance limiter.

\begin{figure*}[!ht]
\centering
\subfigure[Fluid Application]{
\label{fig:fluidbulk}
\includegraphics[scale=0.35]{figs/fluid_bulk_sync.pdf}
%\vspace*{3mm}
}
\subfigure[Circuit Application]{
\label{fig:cktbulk}
\includegraphics[scale=0.35]{figs/circuit_bulk_sync.pdf}
%\vspace*{-6mm}
}
\subfigure[AMR Application]{
\label{fig:amrbulk}
\includegraphics[scale=0.35]{figs/amr_bulk_sync.pdf}
}
%\vspace{-2mm}
\caption{Bulk-Synchronous Performance.\label{fig:bulksync}}
\vspace{-2mm}
\end{figure*}

\subsection{Bulk-Synchronous Comparison}
\label{subsec:bulkcomp}
%To quantify the performance advantages of \Realm's deferred execution
%model, we compare the performance of a bulk-synchronous implementation
%of Legion to a deferred execution model of Legion using \Realm.  
%While
%a deferred execution implementation of Legion based on \Realm has already 
%been shown to outperform independently tuned MPI applications\cite{Legion12},
%those comparisons do not control for advantages derived from the Legion
%programming system.  By differentiating only between we bulk-sychronous
%and deferred implementations of Legion, we will be able to accurately 
%ascertain the benefits of a deferred execution model.

To isolate the contribution of deferred execution,
Figure~\ref{fig:bulksync} compares the performance of a bulk-synchronous
implementation of Legion versus the existing implementation using \Realm
for the Fluid, Circuit, and AMR applications.  Each plot contains performance
curves for both implementations on two different problem sizes.
Circuit is compute-bound and the bulk-synchronous implementation
performs reasonably well.  By 16 nodes, however, the overhead grows to 19\%
and 22\% on the small and large inputs respectively.  Fluid
has a more evenly balanced computation-to-communication ratio and
suffers more by switching to
a bulk-synchronous model.  At 16 nodes, performance is 135\% and 52\% worse
than the deferred execution implementations on the small and large problem sizes
respectively.  Finally, AMR is memory bound,
and on 16 nodes the bulk-synchronous version is 82\% and 102\% slower 
on the small and large problem sizes.
These results show that deferred execution as supported by \Realm
provides significant latency hiding capability, and  the benefit 
grows with the number of nodes.  
%In all cases,
%the overhead of the bulk-synchronous implementation grew with node
%count compared to the \Realm implementation.  
%As we continue to scale
%applications to larger and larger machines, the ability of \Realm
%to support deferred execution will be essential to hiding the large
%latencies inherent in such architectures.  



