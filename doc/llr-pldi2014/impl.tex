
\section{\Realm Implementation}
\label{sec:impl}
%There are currently two implementations of \Realm:
%one for shared memory machines and
%one for  clusters of both CPUs
%and GPUs.  The shared-memory implementation uses 
%POSIX threads\cite{PTHREADS} (Pthreads)
%and is primarily for debugging.  
Our implementation of \Realm for heterogeneous clusters of both CPUs and GPUs
is built on Pthreads, the CUDA runtime library for GPUs\cite{CUDA} 
and the GASNet cluster API\cite{GASNET07}.  
The cluster is modeled as having two kinds of processors (a {\tt CPU} processor for each
CPU core and a {\tt GPU} processor for each GPU)
and the four kinds of memory discussed in Section~\ref{sec:circuit_ex}.  
%The is a different {\tt CPU} processor or each CPU core and a {\tt GPU} processor for
%each GPU, matching the scheduling granularity available in the Pthreads and CUDA APIs,
%respectively.  

%The first kind of memory is the 
%system memory that is accessible by every CPU core on 
%a given node.  The second is the framebuffer memory on 
%each GPU (and accessible only by that GPU).  The third 
%kind of memory is system memory on a node that has been 
%made accessible to the GPU(s) on that node as well as the 
%CPU cores, commonly referred to as {\em zero-copy memory}.
%The final kind of memory is the portion of system memory on 
%each node that has been registered with the GASNet runtime 
%to allow {\em remote direct memory access} (RDMA) by other 
%nodes in the cluster.  We refer to this memory 
%as {\em GASNet memory}.

Runtime features requiring communication
rely on GASNet's {\em active messages}, which consist
of a command and payload sent by one node to 
another without coordination.  Upon arrival
at a destination node, a handler routine is invoked to 
process the message\cite{vonEicken92}.

We describe the implementation of \Realm's novel features, specifically
events, \reservations, and reduction instances.

\subsection{Events}
\label{subsec:eventimpl}
Events are created on demand and are {\em owned} by the creating node.
The space of event handles is divided statically across the nodes,
allowing each node to assign handles to new events without
conflicting with another node's assignments and without inter-node
communication.  The static division of event handles also permits any
node to determine an event's owning node without communication.

When a new event $e$ is created, the owning node $o$ allocates a 
data structure to track $e$'s state ({\em triggered} 
or {\em untriggered}) and $e$'s local list of {\em waiters}:
dependent operations (e.g., copy operations and task launches) 
on node $o$.  The first operation 
dependent on $e$ from a remote node $n$ allocates the same 
data structure on $n$.  An {\em event subscription} 
active message is then sent to node $o$ indicating 
node $n$ should be informed when $e$ triggers.  
Any additional dependent operations on node $n$
are added to $n$'s local list of $e$'s waiters without 
further communication.  When $e$ triggers, the owner node $o$
notifies all local waiters and sends an {\em event trigger} 
message to each subscribing node.  If the owner node $o$
receives a subscription message after $e$ triggers, $o$
immediately responds with a trigger message.

The triggering of an event may occur on any node.  
If it occurs on a node $n$ other than the owner $o$, a 
trigger message is sent from $n$ to $o$, which forwards that message 
to all other subscribed nodes.  The triggering node  $n$
notifies its waiters and no message is sent from 
$o$ back to $n$.  While a remote 
event trigger results in the latency of a triggering 
operation being at least two active message flight times, it 
bounds the number of active messages required per event to 
$2N-2$ where $N$ is the number of nodes monitoring the event
(which is generally a small fraction of the total number of machine nodes).  
An alternative is to share the subscriber list 
so that the triggering node can notify all interested nodes 
directly.  However, such an algorithm is more complicated 
(due to race conditions) and requires $O(N^2)$ active messages.  
Any algorithm super-linear in the node count will not 
scale well, and as we show in Section~\ref{subsec:eventmicro},
the latency of a single event trigger active message is very small. 

The data structure used to track an event $e$ cannot be freed until
all operations on $e$ have been performed.  Creation and triggering
can each happen only once, but any number of operations may depend on
$e$.  Furthermore, some operations may not even be requested until
long after $e$ has triggered.  Other systems
address this issue by reference counting event
handles\cite{Khronos:OpenCL}, but reference counting adds client and
runtime overhead even on a single node, let alone a distributed
reference-counting implementation.

Instead of freeing event data structures, our 
implementation aggressively recycles them.  Compared to 
reference counting our implementation requires fewer 
total event data structures and has no client/runtime overhead.  
The key observation is that one {\em generational event} data 
structure can represent one untriggered 
event and a large number (e.g., $2^{32}-1$) of already-triggered 
events.  We extend each event handle to 
include a generation number and the identifier for its 
generational event.  Each generational event records how many 
generations have already triggered.
A generational event can be 
reused for a new generation as soon as the current generation 
triggers.
(Any new operation 
dependent on a previous generation can immediately be executed.)
To create a new event, a node finds a generational 
event in the triggered state, increases the generation by one, 
and sets the generational event's state to untriggered.  As before, 
this can be done with no inter-node communication.

Nodes also maintain generational event data structures for
remote events they have observed.  These data structures 
record the most recent generation known to have triggered as well
as the generation of the most recent subscription message sent 
(if any).  Remote generational events enable an interesting optimization.  If
a remote generational event receives a request to wait 
on a later generation than its current generation, it 
can infer that all generations up to the requested generation have 
triggered, because a new generation of the event was 
already created by the event's owner.  All local waiters for earlier
generations can 
be notified even before receiving the event trigger message 
for the current generation.

\subsection{\Reservations}
\label{subsec:reservationimpl}
Like events, \reservations are created on demand, using a space 
of handles divided across the nodes.  
\Reservation creation requires no communication
and the handle is sufficient to determine 
the creating node.  However, whereas event ownership 
is static, \reservation ownership may migrate; the creating node 
is the initial owner, but ownership can be transferred to other nodes.
Since any node may at some point own a \reservation $r$, 
all nodes use the same data structure to track the state of $r$:

\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
\item {\em owner node} - the most recently known owner of $r$.  
If the current node is the owner, this
information is correct.  If not, this information may 
be stale, but by induction the recorded owner 
either is the owner or knows the next node to query about ownership.

\item {\em \reservation status} - records whether $r$ is 
currently held; valid only on the current owner.

\item {\em local waiters} - a list of pending local acquire 
requests.  This data is always valid on all nodes.

\item {\em remote waiters} - a bitmask of which other nodes have 
pending acquire requests;  valid only on the current owner.

\item {\em local payload pointer and size} - a local copy of $r$'s payload
\end{itemize}

Each time an acquire request is made, a new event is created 
to track when the grant occurs. 
% The current node then
%examines its copy of the \reservation data structure to determine 
%if it is the owner.  
If the current node is the owner and the 
\reservation isn't held, the acquire request is granted immediately 
and the event is triggered.  If the \reservation is held, the 
event is added to the list of local waiters.  Note that the 
event associated with the acquire request is the only data that 
must be stored.  If the current node isn't the owner, a 
{\em \reservation acquire} active message is sent to the most 
recently known owner.  If the receiver of an acquire request  
message is no longer the owner, it forwards the message on to 
the node it has recorded as the owner.  If the current owner's 
status shows the \reservation is currently held, the bit for 
the requesting node is set in the remote waiters bitmask.  If 
the \reservation is not held, the ownership of the \reservation 
is given to the requesting node via a {\em \reservation transfer} 
active message, which includes the bitmask of remote waiters and 
an up-to-date copy of the \reservation's payload.  
%The inclusion 
%of the payload in the active message is the reason for the 
%4KB size limit specified on payloads in Section~\ref{subsec:reservations}.

Similarly, a release request is first checked against the local 
node's \reservation state.  If the local node is not the owner, 
a {\em release} active message is sent to the most recently 
known owner, which is forwarded if necessary.  Once the release 
request is on the \reservation's current owning node, the local 
waiter list is examined.  If the list is non-empty, the \reservation 
remains in the acquired state and the first acquire grant
event is pulled off the local waiter list and triggered.  If 
the local waiter list is empty, the acquire state is 
changed to not-acquired, and the bitmask of remote waiters 
is examined.  If there are remote waiters, one is
chosen and the corresponding node becomes the new owner via a 
\reservation transfer.

The unfairness inherent in favoring local waiters over remote waiters
is intentional.  When contention on a \reservation is high (the only
time fairness is relevant), the latency of transferring a \reservation
between nodes can be the limiter on throughput.  By minimizing the
number of \reservation transfers, throughput on the \reservation is
maximized (see Section~\ref{subsec:acquiremicro}).

\subsection{Reduction Instances}
\label{subsec:reducimpl}

\Realm supports {\em reduction-only} physical region instances that differ in two 
important ways from normal instances.  First, the per-element storage in reduction-only
instances is sized to hold the ``right-hand side'' of the reduction
operation (e.g., the $v$ in $struct.field\ \text{+=}\ v$).
Second, individual reductions are applied (atomically) to the local
instance, which is then sent as a batched reduction to
the target instance.  When multiple reductions are made to the same element,
they are {\em folded} locally, further reducing 
communication. The fold operation
is not always identical to the reduction operation.  For example,
if the reduction is exponentiation, the corresponding fold
is multiplication:
$$(r[i]\ \text{**=}\ a)\ \text{**=}\ b \quad \Leftrightarrow 
\quad r[i]\ \text{**=}\ (a * b)$$
The client registers reduction and fold operations at system
startup (these operations are omitted from Figure~\ref{fig:runtimeapi}).
%As mentioned in Section~\ref{subsec:phyreg}, 
Reduction instances can be folded into other reduction instances to
build hierarchical bulk reductions matching the memory 
hierarchy.  For example, in the circuit simulation, when there are multiple
GPUs per node each GPU reduces into a local reduction instance in zero-copy memory,
which are then folded together into a reduction instance for each node, and finally 
reduced across all nodes into GASNet memory.

%
% Alex: Need to make it clear back in section on physical regions that they are close to arrays.
%
\Realm supports two classes of reduction-only instances.  
A {\em reduction fold instance} is similar
to a normal physical region in that it is implemented as an array 
indexed by the same element indices.
The difference is that each instance element is a value of the reduction's
right-hand-side type, which is often smaller than the 
region's element type (the left-hand-side type).  
A reduction operation simply folds the supplied right-hand-side value into 
the corresponding array location.  
%Any number of reductions can be folded into each location.  
When the reduction fold instance $p$ is reduced
to a normal instance $r$, first $p$
is copied to $r$'s location, where  
\Realm automatically applies 
$p$ to $r$ by invoking the 
reduction function once per location via a cache-friendly 
linear sweep over the memory.

The second kind of reduction instance is a {\em reduction list instance},
where the instance is implemented as a list of reductions.
%This representation can be better if updates are sparse (see
%Section~\ref{subsec:reducmicro}).  
A reduction list instance logs
every reduction operation (the pointer location and
right-hand-side value).  When the reduction list instance $p$ is reduced 
to a normal physical region $r$, $p$ is transferred and replayed
at $r$'s location.  In cases where the list of reductions
is smaller than the number of elements in $r$
the reduction in data transferred can yield better performance
(see Section~\ref{subsec:reducmicro}).
