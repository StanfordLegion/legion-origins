\section{Deferred Execution}
\label{sec:motivation}

In the bulk-synchronous model, programs are divided into phases of computation, communication,
and synchronization.  A common bulk-synchronous idiom is a loop that alternates phases:
\newpage
\begin{samepage}
\begin{verbatim}
while (...) {
   compute(...);     // perform task-local computation
   barrier;
   communicate(...);  // exchange data between threads
   barrier;
}
\end{verbatim}
\end{samepage}
By design, computation and communication cannot happen at the same
time in a bulk synchronous execution, idling significant machine
resources.  To help avoid this problem, programming systems that support bulk
synchronous programming, such as MPI and the PGAS family of languages,
have added asynchronous operations that overlap communication and
computation.  Consider the following MPI-like code.
\begin{verbatim}
receive(x, ...);  // writes remote data into *x
Y;                // see discussion
sync;
f(x);
\end{verbatim}
Here {\tt x} is a pointer to local storage that is the target of a
{\tt receive} operation that copies data from a remote memory.  The
{\tt receive} executes asynchronously---execution continues with {\tt
  Y} while the {\tt receive} is also executing---but the only way to
safely use the contents of {\tt x} is to execute a
synchronization operation ({\tt sync} in the above code) that blocks
until the {\tt receive} completes.  

The problem for the programmer is to find some useful computation {\tt Y}
to overlap with the {\tt receive}.  There are several constraints on the choice of {\tt Y}.
The execution time of {\tt Y}  must not be too short (or the latency of the {\tt receive} will not be hidden)
and it must not be too long (or the continuation {\tt f(x)} will
be unnecessarily delayed).  Particularly hard to reason about is the case where
{\tt Y}'s execution time is variable, for example, if {\tt Y} uses blocking operations to
gain exclusive access to a resource:
\begin{verbatim}
lock(y);
... Y's computation ...
unlock(y);
\end{verbatim}
In general, any variance in the  execution time of {\tt Y} or of the {\tt receive} makes
proper overlapping of the communication and computation very challenging.
Finally, since {\tt Y} can't use {\tt x}, {\tt Y} must
be an unrelated computation, often resulting in particularly non-modular and hard to maintain
code. 

What makes this programming problem especially hard is the presence of
the blocking constructs, which force the programmer to (statically)
find additional computations to perform.  An alternative is {\em deferred
  execution}, which simply means that every long latency operation
returns immediately, while the actual execution is deferred by the
runtime system until some later point in time.  In the example above,
{\tt receive(x, ...)} is already an asynchronous operation.  The
question is how to substitute an asynchronous operation for the
blocking synchronization.  The key observation is that {\tt f(x)} can
also be issued asynchronously if we can express that the
operation depends on {\tt receive(x, ...)}.  That is, to
provide deferred execution for all long latency operations, these
operations must be composable.

In \Realm, events are used to compose asynchronous operations. \Realm events
have two states, {\em untriggered} and {\em triggered}.
Every asynchronous operation takes an event as input that must trigger before
the operation can begin and returns a new event that will trigger when the operation
completes:
\begin{samepage}
\begin{verbatim}
e1 = receive(noevent, x, ...);
e2 = execute(e1, f(x));
\end{verbatim}
\end{samepage}
Here {\tt receive} now takes an event {\em precondition} as its first argument and
returns event {\tt e1} which will trigger when the operation completes ({\tt
  noevent} is an event constant that is guaranteed to be in the
triggered state).  The asynchronous operation {\tt execute(e1, f(x))} 
is held by the runtime system until {\tt e1} triggers, at which point {\tt f(x)} is 
(asynchronously) executed.
Since both operations are deferred, the main thread continues
executing, possibly issuing more deferred operations.  Note that
deferred execution shifts the responsibility for hiding
latency from the programmer to the runtime
system; it is now up to the runtime system to find ways to overlap
communication and computation, but it has the information to do so in the
form of knowledge of which event preconditions of deferred operations
have triggered.

The \Realm interface provides composable asynchronous operations for the important
long latency operations on modern distributed memory supercomputers, including moving
data from one place to another and launching computation, as in the example above.
In this respect, \Realm is similar to implementations of dataflow programming languages.
The closest previous system to \Realm is TAM, the Threaded Abstract Machine, which was
originally designed to implement the dataflow language Id \cite{CullerGSvE93}.  
Like \Realm, TAM is designed as a low-level runtime system to support higher-level
clients, such as dataflow languages.
TAM is also designed to avoid blocking constructs and has
asynchronous communication and computation that can be composed into chains (and more
generally acyclic graphs) of dependent operations.  Beyond these similarities, however,
there are several basic differences between \Realm and TAM, as well as other dataflow implementations:

\begin{itemize}
\item TAM clients can express dependencies between operations, but
  there is no mechanism for clients to express weaker ordering
  constraints on sets of operations, such as atomicity.  \Realm
  provides {\em \reservations}, a novel deferred execution
  synchronization primitive; as we shall see in Section~\ref{sec:circuit_ex}, \reservations are heavily used in idiomatic \Realm applications.

\item The movement of data has become even more expensive relative to
  computation since TAM was designed.  TAM provides only
  asynchronous remote loads and stores.  \Realm focuses on support for 
  bulk data movement, including
  {\em physical regions} in specific memories that know the structure of the data they
  contain and can support reductions between physical regions.  Using these primitives,
  \Realm clients can express highly optimized
  data movement patterns, such as deferred combining trees of
  reductions that match the structure of the memory hierarchy, that are not
  expressible in TAM.
  
\item Finally, dataflow implementations were
  designed for machines much smaller than those being
  programmed today.  To scale well, \Realm must have extremely
  lightweight implementations of its primitives, especially with
  respect to distributed memory management of \Realm data structures.
  For example, \Realm event handles are 64-bit words that are passed
  by value and valid anywhere in the system, with no central
  management.  Because events are lightweight and have a very limited
  interface, they can be used ubiquitously in \Realm and heavily
  optimized (see Section~\ref{sec:impl}).  TAM does not have a separate
  event primitive, instead bundling the logic for events with client code in heavier weight
  constructs that would likely incur much higher overheads on today's
  machines. 
\end{itemize}

