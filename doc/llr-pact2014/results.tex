
\section{Application Evaluation}
\label{sec:results}

To quantify the performance of our \Realm implementation 
real-world applications we target an existing high-level
runtime system\cite{Legion12} to the \Realm interface. We use the
same three applications as \cite{Legion12} and profile several 
aspects of performance. We first look at the use of \Realm events 
by the applications in Section~\ref{subsec:eventlife}.
Section~\ref{subsec:resperf} 
covers the use of reservations.  Finally, in Section~\ref{subsec:bulkcomp} 
we estimate the performance benefits conferred by \Realm
relative to implicit representation systems.

All three applications we investigate are multi-phase and
require parallel computation, data exchange, 
and synchronization between phases.  {\em Circuit}
is an electrical circuit simulation.  The circuit is represented
as an irregular graph where the edges are wires and nodes are
where wires meet.  The graph is dynamically partitioned into pieces that are
distributed around the machine and the simulation is run for many time steps,
each of which involves three distinct phases with a variety of access
patterns. Figure~\ref{fig:circuit_map} (reproduced from \cite{Legion12}) 
shows the placement of physical
regions for storing node and wire data in different machine memories for
one time step.  Wire and private nodes for each piece can remain in device memory for
each GPU.  Physical regions for shared and ghost node data are placed
in zero-copy memory to facilitate direct data movement to/from
GASNet memory.  Reduction instances in zero-copy memory buffer reductions
from the {\tt Distribute Charge} task running on the GPU before they are 
folded back to GASNet memory using a bulk reduction. The \Realm event
graph in Figure~\ref{fig:circuit_events} directly corresponds to
the operations shown in Figure~\ref{fig:circuit_map}.

%We first look at the performance of
%\Realm events and illustrate the need for generational
%events in Section~\ref{subsec:eventlife}.  In 
%Section~\ref{subsec:resperf}, we demonstrate that \reservations
%are not a bottleneck for any of our
%applications.  Finally, in Section~\ref{subsec:bulkcomp}
%we quantify the performance advantages conferred by
%a deferred execution model compared to a
%bulk-synchronous execution model.

%Both the applications level code as well as
%the Legion runtime code should be thought of as the 
%client for our \Realm implementation as even runtime
%level tasks can be deferred in Legion.  

{\em Fluid} is a distributed memory port of the PARSEC 
fluidanimate benchmark\cite{bienia11benchmarking},  
%The reference
%implementation only runs on shared memory machines, but when rewritten
%in Legion, Fluid runs on distributed machines as well.  
which models fluid flow as particles moving through a volume divided into cells.  
Each time step involves multiple phases, each updating different properties of the
particles based on neighboring particles. % in the same cell.  
The space of cells is partitioned and neighboring cells in 
different partitions must exchange data between phases. 
These exchanges are done point-to-point by chaining copies and tasks
using events rather than employing a global bulk-synchronous approach
to exchange neighboring particle information.

{\em AMR} is an adaptive mesh refinement benchmark based on the third heat
equation example from the Berkeley Labs BoxLib project\cite{BoxLib}.  AMR
simulates the two dimensional heat diffusion equation using three different levels
of refinement.  Each level is partitioned and distributed across the machine.  
Time steps require both intra- and inter-level communication and 
synchronization.  Dependences between tasks from 
the same and different levels are again expressed through events.  

\begin{figure}
\begin{center}
\includegraphics[scale=0.42]{figs/RealmMem.pdf}
\end{center}
\vspace{-3mm}
\caption{Data Placement and Movement for Circuit.\label{fig:circuit_map}}
\end{figure}

\subsection{Event Lifetimes}
\label{subsec:eventlife}
%We instrumented the heterogeneous implementation of \Realm to 
%capture event usage information.  
To illustrate the need for generational events data structures, 
we instrumented \Realm to capture information about the lifetime
of events. The usage of events by all three applications was similar, 
so we present representative results from just one.  Figure~\ref{fig:eventlife} 
shows a timeline of the execution of the Fluid application on 16 nodes using
128 cores.  The dynamic events line measures the total number of event
creations.  A large number of events are created---over 260,000 in
less than 20 seconds of execution---and allocating separate storage
for every event would clearly be difficult for long-running
applications.

An event is {\em live} until its last operation 
(e.g., query, trigger) is performed.  After
an event's last operation a reference counting implementation would
recover the event's associated storage.  The live events line
in Figure~\ref{fig:eventlife} is therefore the number of needed 
events in a reference counting scheme.  In this example, reference 
counting reduces the storage needed for dynamic events by over 10X, 
but with the additional overhead associated with reference counting. 
This line also gives a lower bound for the number of events when the 
application performs explicit creation and destruction of events.

As discussed in Section~\ref{sec:generation}, our implementation
requires storage that grows with the maximum number of untriggered events, 
a number that is 10X smaller than even the maximal live event count.  The 
actual storage requirements of our \Realm implementation are shown by 
the generational events line, which shows the total number of generational 
events in the system.
The maximum number of generational events needed is slightly larger than 
the peak number of untriggered events because
nodes must create a new event locally if they have no available (i.e. triggered)
 generational events, 
even if there are available generational events on remote nodes.
Overall, our implementation uses 5X less storage
than a reference counting implementation and avoids any related overhead.
These savings would likely be even more dramatic for longer 
runs of the application, as the number of live events is steadily growing as the
application runs, while the peak number of generational events needed appears to 
occur during the start-up of the application. Overall this demonstrates
the ability of generational events to represent large numbers
of live events with minimal storage overhead.

\subsection{\Reservation Performance}
\label{subsec:resperf}
%We also instrumented our heterogeneous implementation of \Realm to profile
%the usage of \reservations in real applications.  
The Circuit and AMR 
applications both made use of \reservations, creating 3336 and 1393 
\reservations respectively.  Of all created \reservations in both applications, 
14\% were migrated at least once between nodes.  
The grant rates for both applications are orders of magnitude smaller
than the maximum \reservation grant rates achieved by our \reservation microbenchmarks
in Section~\ref{subsec:acquiremicro}.  Thus, for these benchmarks \reservations
were needed to express non-blocking synchronization 
and were far from being a performance limiter.

\begin{figure*}[!ht]
\centering
\subfloat[AMR Application: 16384 Cells]{
\label{fig:amrbulk}
\includegraphics[scale=0.37]{figs/amr_comparison_16384.pdf}
%\vspace*{-6mm}
}
\subfloat[Circuit Application]{
\label{fig:cktbulk}
\includegraphics[scale=0.37]{figs/circuit_bulk_sync.pdf}
%\vspace*{-6mm}
}
\subfloat[Fluid Application]{
\label{fig:fluidbulk}
\includegraphics[scale=0.37]{figs/fluid_bulk_sync.pdf}
%\vspace*{3mm}
}
%\vspace{-2mm}
\caption{Comparisons with a Generic Implicit System.\label{fig:bulksync}}
\vspace{-2mm}
\end{figure*}



\subsection{Comparison with Implicit Representations}
\label{subsec:bulkcomp}
%To quantify the performance advantages of \Realm's deferred execution
%model, we compare the performance of a bulk-synchronous implementation
%of Legion to a deferred execution model of Legion using \Realm.  
%While
%a deferred execution implementation of Legion based on \Realm has already 
%been shown to outperform independently tuned MPI applications\cite{Legion12},
%those comparisons do not control for advantages derived from the Legion
%programming system.  By differentiating only between we bulk-sychronous
%and deferred implementations of Legion, we will be able to accurately 
%ascertain the benefits of a deferred execution model.

We now attempt to estimate the performance gains attributable to the
latency hiding provided by deferred execution.  To compare with a standard
implicit implementation, we modified each \Realm application to wait for
events in the application code immediately before the dependent operation
rather than supplying them as preconditions.  While this methodology has the
disadvantage that our approximation of an implicit runtime may not be as fast as a
purpose-built one, it has the great advantage of controlling for the
myriad possible performance effects in comparing two completely
different implementations: any performance differences will be due
exactly to the more relaxed execution ordering enabled by deferred execution.

%We first note that the dynamic nature of the AMR application 
%(input-dependent refinements) prevents a direct comparison to any
%static explicit systems as data dependence must be computed dynamically. 
%We therefore compare against an implicit bulk-synchronous implementation
%for each of the applications, and for AMR against an existing 
%independently written and optimized MPI application\cite{BoxLib}.

%We would like to isolate the to isolate the contribution of using a
%runtime system based on dynamic, explicit representation over an
%implicit runtime, but it is very difficult to do an apples-to-apples
%comparison because many factors affect the performance of runtime
%systems.  To conduct a controlled experiment, we measure the
%performance of the three applications using deferred execution and
%also with {\em implicit \Realm}, a version of \Realm  that simulates standard asynchronous
%constructs of implicit systems, such as MPI's {\em send}/{\em wait}.
%The only change in the implicit version of \Realm is that any operation $x$ that
%depends on a previously issued operation $y$ must wait on the result
%of $y$ at the time $x$ is issued, thereby blocking the issuing of any
%further operations until $y$ completes; this is the standard behavior
%of asynchronous operations in implicit systems.  Note that in
%(unmodified) \Realm, $x$'s dependence on $y$ is simply added to the
%dynamically generated graph and the main thread continues issuing
%operations.

Figure~\ref{fig:amrbulk} shows three curves for the AMR application:
the original \Realm version, the implicit version, and an independently written and
optimized MPI implementation \cite{BoxLib}.  Observe that the implicit
version is competitive with the independent MPI code, which is some
evidence that using the implicit version as a reference point is
reasonable.  Both the \Realm and implicit versions start out ahead of
MPI due to better mapping decisions provided by the higher-level
runtime\cite{Legion12}.  The \Realm implementation of AMR uses a simple all-to-all
pattern for its communication.  The additional latency inherent in this pattern is
hidden well by the deferred execution model, but is a bottleneck for the 
implicit version, resulting in up to 102\% slowdown at 16 nodes. The MPI
version continues to scale by using much more complicated asynchronous
communication patterns, but the
need for blocking synchronization primitives still results in exposed
communication latency, causing a 66\% slowdown relative to \Realm on
16 nodes. 

It is worth emphasizing that in principle the MPI code can be just as
fast as (or faster than) \Realm---there is nothing in \Realm that a
programmer cannot emulate with sufficient effort using the primitives
available in any implicit runtime system.  However, as discussed in
Section~\ref{sec:related}, this programming work is substantial,
difficult to maintain, and often machine specific; it is our
experience that few programmers undertake it.

Figures~\ref{fig:cktbulk} and \ref{fig:fluidbulk} show performance
results for the Circuit and Fluid applications respectively; for these
applications we do not have independently optimized distributed memory 
implementations and so we compare only the \Realm and implicit implementations.
Each plot contains performance curves for both implementations on 
two different problem sizes. Circuit is compute-bound and the 
implicit implementation performs reasonably well.  
By 16 nodes, however, the overhead grows to 19\%
and 22\% on the small and large inputs respectively.  Fluid
has a more evenly balanced computation-to-communication ratio and
suffers more by switching to the implicit model.  
At 16 nodes, performance is 135\% and 52\% worse than the 
deferred execution implementations on the small and 
large problem sizes respectively. Ultimately the input 
problem size for the fluid application is too small to 
strong scale beyond 16 nodes as the computation-to-communication
ratio approaches unity.

%Overall these results demonstrate that \Realm's deferred execution
%model, made possible by generational events, provides superior
%latency hiding and performance on modern architectures. As
%latencies continue to grow and the temporal performance of machine
%components becomes more volatile\cite{TurboBoost}, we expect runtimes 
%that support deferred execution models such as \Realm will be 
%important for achieving high performance.

%In all cases,
%the overhead of the bulk-synchronous implementation grew with node
%count compared to the \Realm implementation.  
%As we continue to scale
%applications to larger and larger machines, the ability of \Realm
%to support deferred execution will be essential to hiding the large
%latencies inherent in such architectures.  



