\section{Background and Related Work}
\label{sec:related}
In this section we discuss both related work and the distinctions 
between implicit/explicit and static/dynamic runtime systems in 
more detail.  The presentation of \Realm begins in 
Section~\ref{sec:deferred}.  A categorization of a number of 
(but by no means all) classic and recent parallel runtimes is 
given in Table~\ref{tab:categories}.

The most widely used high-performance parallel runtime 
is MPI \cite{MPI}, which is an implicit representation system.
MPI implements a bulk-synchronous model in which programs are divided into phases of
computation, communication, and synchronization \cite{BulkSynch94}.  A common
bulk-synchronous idiom is a loop that alternates phases:

\begin{samepage}
\begin{verbatim}
while (...) {
   compute(...); // local computation    
   barrier;
   communicate(...); 
   barrier;
}
\end{verbatim}
\end{samepage}
By design, computation and communication cannot happen at the same
time in a bulk synchronous execution, idling significant machine
resources.  Thus, MPI has evolved to include 
asynchronous operations for overlapping communication and
computation.  Consider the following MPI-like code:
\begin{verbatim}
receive(x, ...);  
Y;   // see discussion
sync;
f(x);
\end{verbatim}
Here {\tt x} is a local buffer that is the target of a
{\tt receive} operation copying data from remote memory.  The
{\tt receive} executes asynchronously---the main thread continues execution 
with {\tt   Y} while the {\tt receive} is also executing---but the only way to
safely use the contents of {\tt x} is to perform a {\tt sync} operation
that blocks until the {\tt receive} completes.  

It is the responsibility of the programmer to find useful computation {\tt Y}
to overlap with the {\tt receive}.  There are several constraints on the 
choice of {\tt Y}. The execution time of {\tt Y} must not be too short 
(or the latency of the {\tt receive} will not be hidden)
and it must not be too long (or the continuation {\tt f(x)} will
be unnecessarily delayed).  Since {\tt Y} can't use {\tt x}, {\tt Y} must
be an unrelated computation, which can result in non-modular 
code that is difficult to maintain.  

\begin{table}
\begin{tabular}{|c|c|}
\hline
\multicolumn{2}{|c|}{\em Implicit Representation Systems} \\
\hline
\multicolumn{2}{|c|}{MPI \cite{MPI} \ \ \ GASNet \cite{GASNET07}\ \ \ Co-Array Fortran \cite{COARRAY_FORTRAN}} \\
\multicolumn{2}{|c|}{UPC \cite{UPC99} \ \ \ Titanium \cite{TIT98} \ \ \ Chapel \cite{Chamberlain:Chapel}} \\
\multicolumn{2}{|c|}{X10 \cite{X10} \ \ \ HJ \cite{HJ11}} \\
\multicolumn{2}{|c|}{Cilk \cite{Cilk95} \ \ \ Charm++ \cite{Kale1993charm++}}\\
\multicolumn{2}{|c|}{\ \ }\\
\hline
\multicolumn{2}{|c|}{\em Explicit Representation Systems} \\
\hline
{\em Static} & {\em Dynamic} \\
\hline 
TAM \cite{TAM93}  \ \  \ Id \cite {ID87}  &  \\
Sequoia \cite{Fatahalian06,Houston08} \ \ \ DPJ \cite {Bocchino09} & StarPU \cite{StarPU11} \ \ \ TAM \cite{TAM93} \\
Tarragon \cite{Tarragon06} \ \ \ CnC \cite{knobe2009ease,budimlic2010concurrent} &  Realm \\
Lucid \cite{Lucid95}  \ \ \ CGD \cite{CGD09} & \\
\hline
\end{tabular}
\caption{Categorization of parallel runtimes.}
\label{tab:categories}
\vspace{-4mm}
\end{table}

Thus, the programmer is responsible not only for adding sufficient
synchronization but also for static scheduling (e.g., overlapping {\tt
  Y} with the {\tt receive}).  Other implicit runtimes have the same
issue, as only the programmer has the knowledge of what can be
parallelized.  The PGAS languages (UPC \cite{UPC99}, Titanium
\cite{TIT98} and Co-Array Fortran \cite{COARRAY_FORTRAN})
are, for the purposes of this discussion, very similar to MPI.
Other programming models that differ significantly from the bulk synchronous model
still require user-placed and potentially blocking synchronization to
join asynchronous computations (e.g., Cilk's {\em spawn}/{\em
  synch} \cite{Cilk95}, X10's \cite{X10} and Habanero Java's \cite{HJ11} {\em
  asynch}/{\em finish} and Chapel's rich collection of synchronization constructs
\cite{Chamberlain:Chapel}).  The actor model provided by Charm++ \cite{Kale1993charm++} is a form of
implicit representation, as the runtime has no knowledge of what messages will be sent by a message
handler until it is actually executed.  Charm++ also provides futures that allow asynchronous
computations to be called;
a thread using a future executes an implicit synchronization operation if the value is not yet available.

Static explicit systems vary widely in how they represent the graph of
dependent operations.  Some, including classic dataflow systems such
as Id \cite{ID87}, provide languages that are very close to the underlying static graphs.  
There are also recent examples,
particularly for coarse-grain static dataflow \cite{Lucid95,CGD09}.
Tarragon \cite{Tarragon06} is a good example of the wide range of possible
semantics for static systems, having a more actor-like instead of purely dataflow
semantics for its graphs.  Another example is Concurrent Collections (CnC) 
\cite{budimlic2010concurrent}, which incorporates both control and data dependence edges in the
graph. In other static explicit systems the static graph is 
is constructed as a compiler's intermediate representation;
examples are  Sequoia \cite{Fatahalian06} and Deterministic Parallel Java (DPJ) \cite{Bocchino09}.

A significant advantage of static dependence graphs is that they can be
scheduled at compile time, resulting in very low runtime overheads, which
in turn enables exploitation of finer-grain parallelism. Furthermore,  
because the dependence graph is explicit, the user is relieved of
specifying the overlap of operations, allowing the
implementation more scope for optimizations and different 
strategies for different platforms.  There are two disadvantages to static
explicit systems.  The first is
that dynamic decision making, while not impossible, must be encoded as a 
choice among a static set of possibilities, leading to cumbersome 
implementations that would be simple in a dynamic 
setting.  The other issue is that  partial orders specified by graphs
cannot express some useful weaker dependence patterns, 
and in fact some explicit systems have added constructs that capture 
weaker ordering constraints at the cost of more complex semantics and 
implementations \cite{Arvind89}.

In contrast, there are few previous dynamic explicit
systems for distributed memory machines.  
TAM is a low-level runtime for dataflow languages
that targets conventional hardware.  TAM is a hybrid static/dynamic
system and is listed in Table~\ref{tab:categories} in both categories.
At a fine grain (roughly, within a function body) TAM is quite static,
which is necessary to exploit very fine grain parallelism.  At coarser
granularity TAM is dynamic, with the graph of dependences between
{\em frames} evolving at runtime.  TAM, which was designed for much smaller
machines than today's heterogeneous supercomputers, leaves the runtime
management of the dynamic parts of the graph to the client.
TAM also has no facilities for relaxed execution 
orderings nor for bulk reductions.  Parallex \cite{gao2007parallex,kaiser2009parallex}
is a more recent system with similarities to TAM, in that it
is thread-based and latency-hiding is achieved through cooperative multithreading.

StarPU \cite{StarPU11} is closer to \Realm, providing a similar 
interface to build a dynamically generated 
control-dependence graph.  Like \Realm, it provides separate 
data movement primitives that appear as operations in the graph. 
StarPU has no primitive equivalent to \Realm's reservations for expressing 
synchronization in a deferred execution environment, and does 
not support bulk reductions, two of the novel features
of \Realm.  Furthermore, StarPU's {\em tags}, which play the 
role of \Realm's events, are managed by the client, not by StarPU, 
which means they cannot be optimized for time or space consumption.  
%The user must be careful not to deallocate
%tags before they are done being used to record dependences, which 
%requires explicit memory management (e.g., user-level 
%reference counting of the tags) to handle correctly in general.  
It is important to note that the goals of StarPU and \Realm are 
also different: \Realm is intended to be a low-level runtime for 
higher-level languages and runtimes that will run efficiently on 
very large heterogeneous machines; StarPU is designed with a pragmatic C-level interface
to be used directly by programmers.

Ompss also provides a dynamically constructed, explicit graph of
operations\cite{duran2011ompss}. Ompss' graphs are heavier weight 
than \Realm's, including data as well as control dependences. This
prevents Ompss from replicating the important performance optimizations
performed by \Realm for distributed memory machines. \Realm also differs 
in providing reservations as well as physical regions and associated
operations, such as bulk reductions.

%CUDA \cite{CUDA} and OpenCL \cite{Khronos:OpenCL} support
%the composition of asynchronous kernel launches and asynchronous copies
%between a host node and a single GPU.  However, the only synchronization
%options available in both interfaces are blocking operations on either
%a stream or work queue.  OpenCL has events similar to \Realm for
%expressing ordering, but has no synchronization primitive comparable to 
%\reservations for expressing more relaxed properties.
%OpenCL events are valid only within a single GPU context and cannot be used
%globally.

%Cilk demonstrated the power of
%asynchronous function calls\cite{CILK95}.  The Cilk {\em work-first}
%principle provides an argument for 
%interfaces such as \Realm that optimize for throughput at the potential
%expense of adding additional latency to the critical 
%dependence path\cite{Frigo98}.

%The Threaded Abstract Machine (TAM) is a programming model designed to
%make it easy to port dataflow programs onto a parallel machine\cite{CullerGSvE93}.  
%Conceptually \Realm is similar to the dataflow languages TAM supports.
%However, the implementation of TAM is designed for a class of much smaller machines
%and therefore is based on heavier communication and threading mechanisms.
%While TAM supports composable asynchronous computation and communication, its
%synchronization mechanisms still require blocking operations. TinyOS\cite{PowerLock}
%provides {\em power locks} which provide non-blocking requests and callbacks on lock
%acquisition, but only to mediate access to hardware resources in a single-threaded
%environment.

%I-structures from Id \cite{Arvind89}
%have similar semantics to \Realm events, but 
%are limited to shared address spaces.
%The implementation of many dataflow languages
%have features related to \Realm, but are not implemented
%for distributed memory machines.  



%Sequoia's runtime interface for arrays\cite{Houston08} in that 
%both systems are aware of the structure of the data.  Sequoia supports
%asynchronous tasks and copies, but does not permit composition of
%asynchronous operations.

%The implementation of \Realm shares some similarities with large
%distributed systems.  

Many distributed systems use a publish/subscribe
abstraction for supporting communication that has similarities to \Realm's events 
and event waiters\cite{Aguilera99,Carzaniga01}.  Work has
also been done on using object-oriented languages to build event-based
distributed systems\cite{Eugster01,Harrison97,Chang91}.  
%In these cases
%callbacks are registered to run when event operations are triggered
%remotely.  
Events in these systems are much heavier weight
and often carry large data payloads.  Many systems
focus on resiliency instead of performance\cite{Ostrowski09}.
