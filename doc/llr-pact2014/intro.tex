
\section{Introduction}
\label{sec:intro}

All parallel programs can be thought of as graphs with nodes
representing operations to be performed and edges representing
ordering constraints, such as control or data dependences.  To
simplify the task of mapping parallel programs to
distributed memory machines, runtime systems take several distinct approaches 
to expressing and manipulating these graphs.

In {\em implicit representation} systems, the runtime is unaware of the
dependences between operations and the burden of orchestrating and
optimizing the program for each target architecture falls primarily
on the programmer; MPI \cite{MPI} and X10 \cite{X10} are examples 
of systems in which the programmer encodes the partial order on the
execution of operations using primitives for launching parallel work 
and performing synchronization. In {\em explicit representation} systems, 
the runtime has direct access to the graph of operations and takes 
responsibility for all synchronization and scheduling. 
Explicit representation systems can yield both higher
performance (because an automated system can exploit opportunities for
overlapping operations that are too difficult for a programmer to
express) and more portable performance (because choices for a
particular machine are not baked in to the program). Recent examples 
of explicit representation systems include Sequoia \cite{Fatahalian06},
Tarragon \cite{Tarragon06}, and Deterministic Parallel Java (DPJ)
\cite{Bocchino09}.

Most explicit systems for distributed memory machines are {\em static},
meaning the dependence graph of operations is available at compile time. In contrast,
{\em dynamic} explicit systems must manage the graph as it is
generated on-line by the client, and any runtime system overheads will
limit performance. The additional cost of communication in distributed
memory machines makes dynamically handling graph construction and
execution challenging, traditionally relegating the scope of dynamic
explicit systems to shared memory machines.  However, dynamic explicit
systems are essential when dealing with applications with
data-dependent parallelism or reacting to large
variations in hardware performance.  As applications become more
irregular and the performance of distributed machines exhibits more variation due to
heterogeneity and power saving techniques\cite{TurboBoost}, dynamic explicit systems
will become important for achieving high performance.

In this paper we present \Realm, a dynamic, explicit representation
runtime system for heterogeneous, distributed memory machines. 
\Realm uses a light-weight {\em event} system to dynamically represent
the program graph. Client applications use \Realm events to encode 
control dependences between computation, data movement, and synchronization
operations. Using {\em generational events}, a novel implementation technique,
\Realm compresses the representation of
many events into a single generational event and eliminates the need for
\Realm (or the programmer) to manage the lifetime of every event.
This allows events to be passed by value and stored in arbitrary data structures
without reference-counting overhead or explicit deallocation performed by
program code.  The reduction in storage costs and overhead allows \Realm
to provide the benefits of a dynamically-generated explicit representation
even for programs that are distributed across many nodes.

%that achieves 
%further allow events to be passed by value without the need for
%\Realm to track where they propagate.  The result is a very large
%reduction in the overhead of storing and using events compared to
%traditional event-based systems, thereby allowing \Realm to support
%a dynamically-generated explicit representation of the program 
%on distributed memory architectures.

We begin in Section~\ref{sec:related} with more discussion of
related work in parallel runtime systems. Section~\ref{sec:deferred}
covers how light-weight \Realm events enable a {\em deferred execution model}, how
deferred execution differs from standard asynchronous models, and
motivates the need for the other novel features of \Realm including
{\em reservations} and {\em physical regions}. Section~\ref{sec:interface} gives 
an overview of the \Realm interface. Each subsequent section highlights
one of our primary contributions:

\begin{itemize}

\item Section~\ref{sec:events} describes the use of \Realm events and
  how events are implemented with generational events.  
  The low cost of managing events in \Realm
  is central to the overall design, as \Realm clients allocate
  events at rates in the tens of thousands per second per node.

\item Section~\ref{sec:reservations} introduces {\em reservations} for
  performing synchronization in a deferred execution model, 
  thereby enabling relaxed execution orderings not expressible in 
  many explicit systems.  We show how reservations are typically 
  used by \Realm clients and present an efficient implementation.

\item Section~\ref{sec:physreg} covers \Realm's {\em physical region 
  system} and how it supports data movement in a deferred execution
  model. We also cover \Realm's novel support for bulk reductions.

\item Section~\ref{sec:micro} evaluates \Realm on a collection of
  microbenchmarks that stress-test our implementations of events,
  reservations, and data movement operations.  We show that the performance
  of \Realm's primitives approach the limits of   the underlying hardware.

\item Section~\ref{sec:results} details the performance of three real-world
%  applications written in both a bulk-synchronous model without events,
%  and in \Realm's deferred execution model. In one case 
  applications written in both an implicit representation model
  and in \Realm's explicit deferred execution model. In one case 
  we also compare against an independently written and optimized MPI
  code. We find that applications using \Realm range from 22-135\% 
  faster than equivalent implicit versions.

\end{itemize}

