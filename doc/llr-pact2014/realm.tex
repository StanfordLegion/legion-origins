
\section{\REALM Interface}
\label{sec:interface}
%\Realm clients are high-level
%language compilers and runtime systems, such as 
%Legion\cite{Legion12}.  These clients require
%control over the underlying hardware and transparent 
%performance.  To support these 
%demands we have designed \Realm to be as low-level 
%as possible.  
%In many cases we have chosen to trade-off
%ease of programmability for performance.  
%We eschewed any 
%features that would automatically be performed without 
%direction from the client. 

%\Realm is a low-level runtime system, intended to serve as the target
%of higher-level compilers and libraries.  Thus, \Realm provides 
%provides a minimal set of low-level 
%high-performance primitives for supporting
%deferred execution in a distributed environment.

\Realm is a low-level runtime, providing a small set of primitives
for performing computations on heterogeneous, distributed memory machines.
The focus is on providing the necessary mechanisms, while leaving the policy
decisions (e.g. which processor to run a task on, what copies to perform)
under the complete control of the client.  While that client may be a
programmer coding directly to the \Realm interface, the expected usage
model for \Realm is as a target for higher-level languages and runtimes.

The \Realm interface is shown in Figure~\ref{fig:runtimeapi}.
%with functionality organized into objects.
%We describe these objects in five parts: {\em events}
%for composing operations (Section~\ref{subsec:events}),
%{\em processors} for parallel computation
%(\ref{subsec:procs}), {\em \reservations} for 
%synchronization(\ref{subsec:reservations}), 
%{\em physical regions} for data layout and movement
%(\ref{subsec:phyreg}), and a {\em machine} object
%for introspection of the underlying hardware
%(\ref{subsec:machmodel}).  
Except for the static singleton machine object,
object instances are light-weight {\em handles}
that uniquely name the underlying object.
Every handle is valid everywhere in the system, allowing
handles to be freely copied, passed as arguments, and stored
in the heap.  For performance, \Realm does not track where handles
propagate. In the case of events, this creates
an interesting problem of knowing when it is safe
to reclaim resources associated with events
(see Section~\ref{sec:events}).

\lstset{
  captionpos=b,
  language=C++,
  basicstyle=\scriptsize,
  numbers=left,
  numberstyle=\tiny,
  columns=fullflexible,
  stepnumber=1,
  escapechar=\#,
  keepspaces=true,
  belowskip=-10pt,
  literate={<}{{$\langle$}}1 {>}{{$\rangle$}}1,
  %morekeywords={region,coloring,partition,spawn,disjoint,aliased},
  %deletekeywords=float,
}

\begin{figure}
\begin{lrbox}{\mylistingbox}
\begin{lstlisting}
class Event {
  const unsigned id, gen;
  static const Event NO_EVENT;

  bool has_triggered() const;
  void wait() const;
  static Event merge_events(const set<Event> &to_merge);
};

class UserEvent : public Event {
  static UserEvent create_user_event();
  void trigger(Event wait_on = NO_EVENT) const;
};
\end{lstlisting}
\end{lrbox}
\hspace{0.2cm} \subfloat{\usebox{\mylistingbox}} \\
\vspace{0.2cm}

\begin{lrbox}{\mylistingbox}
\begin{lstlisting}[firstnumber=last]
class Processor {
  const unsigned id;
  typedef unsigned TaskFuncID;
  typedef void (*TaskFuncPtr)(void *args,size_t arglen,Processor p);
  typedef map<TaskFuncID, TaskFuncPtr> TaskIDTable;

  enum Kind { CPU_PROC,GPU_PROC /* ... */ };
  Kind kind() const;

  Event spawn(TaskFuncID func_id,const void *args,size_t arglen,
              Event wait_on) const;
};
\end{lstlisting}
\end{lrbox}
\hspace{0.2cm} \subfloat{\usebox{\mylistingbox}} \\
\vspace{0.2cm}

\begin{lrbox}{\mylistingbox}
\begin{lstlisting}[firstnumber=last]
class Reservation {
  const unsigned id;
  Event acquire(Event wait_on = NO_EVENT) const;
  void release(Event wait_on = NO_EVENT) const;

  static Reservation create_reservation(size_t payload_size = 0);
  void *payload_ptr();
  void destroy_lock();
};
\end{lstlisting}
\end{lrbox}
\hspace{0.2cm} \subfloat{\usebox{\mylistingbox}} \\
\vspace{0.2cm}

\begin{lrbox}{\mylistingbox}
\begin{lstlisting}[firstnumber=last]
class Memory {
  const unsigned id;
  size_t size() const;
};

class PhysicalRegion {
  const unsigned id;
  static const PhysicalRegion NO_REGION;

  static PhysicalRegion create_region(size_t num_elmts, size_t elmt_size);
  void destroy_region() const;

  ptr_t alloc();
  void free(ptr_t p);

  RegionInstance create_instance(Memory memory) const;
  RegionInstance create_instance(Memory memory,
                                 ReductionOpID redopid) const;
  void destroy_instance(RegionInstance instance,
                        Event wait_on = NO_EVENT) const;
};

class RegionInstance {
  const unsigned id;

  void *element_data_ptr(ptr_t p);
  Event copy_to(RegionInstance target, Event wait_on = NO_EVENT);
  Event reduce_to(RegionInstance target, ReductionOpID redopid,
                  Event wait_on = NO_EVENT);
};
\end{lstlisting}
\end{lrbox}
\hspace{0.2cm} \subfloat{\usebox{\mylistingbox}} \\
\vspace{0.2cm}

\begin{lrbox}{\mylistingbox}
\begin{lstlisting}[firstnumber=last]
class Machine {
  Machine(int *argc, char ***argv,
          const Processor::TaskIDTable &task_table);

  void run(Processor::TaskFuncID task_id,
           const void *args, size_t arglen);

  static Machine* get_machine(void);
  const set<Memory>& get_all_memories(void) const;
  const set<Processor>& get_all_processors(void) const;

  int get_proc_mem_affinity(vector<ProcMemAffinity> &result, ...);
  int get_mem_mem_affinity(vector<MemMemAffinity> &result, ...);
};
\end{lstlisting}
\end{lrbox}
\hspace{0.2cm} \subfloat{\usebox{\mylistingbox}} \\

\caption{Runtime Interface.\label{fig:runtimeapi}}
\vspace{-4mm}
\end{figure}

In the rest of this section we explain \Realm processor and machine objects.
In subsequent sections we present \Realm's events, reservations,
and physical regions.

\subsection{Processors}
\label{subsec:procs}
Lines 14-25 of Figure~\ref{fig:runtimeapi} show
the interface for {\tt Processor} objects.
Processors name every computational
unit within the machine.  
%We describe how to discover
%processor handles in Section~\ref{subsec:machmodel}.
Processors have a kind, currently either CPU
or GPU (line 20).  Exposing heterogeneous processor types through
a common interface allows the client to alter the task mapping without
having multiple code paths for launching tasks and keeps the
\Realm interface open to extension for new processor kinds (e.g. FPGAs).

The {\tt spawn} method (line 23) launches a new {\em task} on a
processor, adding a new node to \Realm's dynamic control dependence graph.
The spawn operation is 
invoked on a processor handle, enabling a task on one processor to launch
another task on any other processor in the system.
Spawn takes an optional event that must trigger 
before the task begins execution and returns a (fresh) event that 
triggers when the task completes.
For example,
Figure~\ref{fig:circuit_events} shows a portion of a \Realm event
graph generated from a real client application \cite{Legion12}.  
Tasks are rectangles in Figure~\ref{fig:circuit_events}.
The right-hand side of the graph shows the actual application-level 
tasks, while the left-hand side shows {\em mapping} tasks launched 
by the higher-level runtime to dynamically compute the placement of the
application tasks and data.
In Figure~\ref{fig:circuit_events}, the application-level
tasks are launched on the GPU from the 
mapping tasks running on a CPU.  Dashed lines 
indicate that one task is launched by another task.

\subsection{Machine}
\label{subsec:machmodel}
\Realm provides a singleton {\tt Machine} object (lines 65-78 of
Figure~\ref{fig:runtimeapi}) that handles initialization and run-time
introspection of the hardware.  The {\tt Machine} object is 
created at program start,
providing a task table mapping task IDs to function pointers, and
then invokes the {\tt run} method (line 69).  Any task can call
the {\tt get\_machine} method
(line 72) and use it to obtain lists of all {\tt Memory} (line 73) and
{\tt Processor} (line 74) handles.  Using the affinity methods (lines
76-77), the client can also determine which memories are accessible by
each processor and with what performance, as well as which pairs of
memories can support copy operations.

\subsection{Implementation}

Our implementation of \Realm for heterogeneous clusters of both CPUs and GPUs
is built on Pthreads, CUDA for GPUs\cite{CUDA}, 
and the GASNet cluster API\cite{GASNET07} for portability across
interconnect fabrics.  
The cluster is modeled as having two kinds of 
processors (a {\tt CPU} processor for each
CPU core and a {\tt GPU} processor for each GPU, matching the scheduling granularity of
Pthreads and CUDA respectively)
and four kinds of memory (distributed GASNet memory accessible by all nodes via RDMA operations, 
system memory on each node, GPU device memory, and zero-copy memory, 
which is a segment of system memory that has been mapped into both the CPU
and GPU's address spaces).  Internally, \Realm maintains a queue for each processor
of tasks that are ready to execute (i.e., those for which the precondition
event has triggered); when the processor becomes idle \Realm executes the next
task in the queue.

\Realm features requiring communication
rely on GASNet's {\em active messages}, which consist
of a command and payload sent by one node to 
another.  Upon arrival
at a destination node, a handler routine is invoked to 
process the message\cite{vonEicken92}.

