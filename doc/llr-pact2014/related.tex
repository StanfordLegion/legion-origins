

\section{Related Work}
\label{sec:related}
  

There are two primary designs
for systems with asynchronous operations in the literature.  In {\em fork-join} systems
a thread of control can launch an asynchronous operation $o$ but still
must explicitly synchronize before $o$'s result can be used, blocking if $o$
has not yet completed.  Classic fork/join primitives in operating systems \cite{},
MPI's non-blocking {\em send} and {\em receive} \cite{}, Cilk's {\em spawn} and {\em synch} \cite{} and X10's {\em asynch} and {\em finish} all fall into this category.  
While providing asynchrony, these designs still can expose latency by the need to 
block for the result (see Section~\ref{sec:motivation}).

The other common design for asynchronous operations is to represent
computations using dataflow graphs.  Nodes in this graph are tasks and
directed edges represent a partial order on task execution as well as,
usually, a channel connecting the outputs of one task to the inputs of
another.  Dataflow graphs can hide more latency than fork/join
systems, because the system need never block if there is work do---any
task with no unfinished predecessors can execute.  Beyond this
high-level description, existing dataflow-like systems have
considerable variation.  The dataflow graph may be static and
therefore available for compile-time optimizations \cite{Tarragon06}, it
may be fully dynamic, allowing data-dependent generation of
parallelism \cite{StarPU11}, or it may be a hybrid \cite{TAM93}.  The
tasks may be purely functional \cite{budimlic2010concurrent} or have access to persistent
state \cite{Tarragon06}.  Finally, edges variously
represent data dependences and the transfer of data from a
producer task to a consumer task \cite{TAM93}, pure control dependences as in
Realm, either data dependence or control dependence as in CnC \cite{budimlic2010concurrent},
or the semantics can be user-defined as in Tarragon's firing rules \cite{Tarragon06}.
 






Bulk-synchronous models have
gradually added asynchronous operations.  For example, MPI has
non-blocking {\em send} and {\em receive} for overlapping
communication with computation \cite{MPI}.  Unfortunately, the
application must still eventually wait on the asynchronous
operation---there is no way to have a computation that will use the
results of the receive (or modify the inputs of a send) automatically
begin once the transfer
completes.
Although the
programmer chooses where to place these synchronization points,
correct placement is difficult in practice (see Section~\ref{sec:motivation})
and consequently programs written in
this style tend to have neither predictable nor portable performance.

An alternative to a bulk-synchronous model with asynchronous
extensions is {\em deferred execution}.  In deferred

MPI is the current industry standard for programming 
super\-computers\cite{MPI}.  While MPI supports asynchronous
communication there is no mechanism for fully
deferred execution.  GASNet is another interface for programming
large clusters based on active messages\cite{GASNET07}.  GASNet
is part of the heterogeneous implementation of \Realm, but like
MPI does not support composition of asynchronous active messages with
other constructs.  Co-array Fortran, UPC, and Titanium
are array-based languages that implement bulk-synchronous
programming models similar to MPI that allow asynchronous
data exchange\cite{COARRAY_FORTRAN,UPC99,JV:Yel98}.  Like
MPI however, none of these languages allow for a general deferred
execution model with non-blocking tasks, communication, and synchronization.

%Both POSIX threads and OpenMP\cite{OPENMP98} are used for intra-node parallel
%programming on large machines, but neither support asynchronous
%operations.  CUDA\cite{CUDA} and OpenCL\cite{Khronos:OpenCL} support
CUDA and OpenCL support
the composition of asynchronous kernel launches and asynchronous copies
between a host node and a single GPU.  However, the only synchronization
options available in both interfaces are blocking operations on either
a stream or work queue.  
%OpenCL has events similar to \Realm for
%expressing ordering, but has no synchronization primitive comparable to \reservations
%for expressing more relaxed properties such as atomicity.
%OpenCL events are valid only within a single GPU context and cannot be used
%globally.

%Cilk demonstrated the power of
%asynchronous function calls\cite{CILK95}.  The Cilk {\em work-first}
%principle provides an argument for 
%interfaces such as \Realm that optimize for throughput at the potential
%expense of adding additional latency to the critical 
%dependence path\cite{Frigo98}.

%The Threaded Abstract Machine (TAM) is a programming model designed to
%make it easy to port dataflow programs onto a parallel machine\cite{CullerGSvE93}.  
%Conceptually \Realm is similar to the dataflow languages TAM supports.
%However, the implementation of TAM is designed for a class of much smaller machines
%and therefore is based on heavier communication and threading mechanisms.
%While TAM supports composable asynchronous computation and communication, its
%synchronization mechanisms still require blocking operations. TinyOS\cite{PowerLock}
%provides {\em power locks} which provide non-blocking requests and callbacks on lock
%acquisition, but only to mediate access to hardware resources in a single-threaded
%environment.

The design of coarse-grained dataflow languages such as Lucid \cite{Lucid95}
and CGD \cite{CGD09} are orthogonal to the design of \Realm.  We view
\Realm as a potential implementation target for many
higher-level programming systems, including coarse-grained
dataflow programming models.  See Section~\ref{sec:motivation} for a comparison
of \Realm and dataflow implementation techniques.
%I-structures from Id \cite{Arvind89}
%have similar semantics to \Realm events, but 
%are limited to shared address spaces.
%The implementation of many dataflow languages
%have features related to \Realm, but are not implemented
%for distributed memory machines.  


Chapel\cite{Chamberlain:Chapel} and X10\cite{X1005} are high-level parallel
programming languages that support asynchronous operations.  The constructs
introduced in these languages are higher-level and have complex 
semantics.  \Realm is lower level and a platform for implementing
higher level languages efficiently, similar to Legion\cite{Legion12}.

%Physical regions in \Realm are related to
%Sequoia's runtime interface for arrays\cite{Houston08} in that 
%both systems are aware of the structure of the data.  Sequoia supports
%asynchronous tasks and copies, but does not permit composition of
%asynchronous operations.

%The implementation of \Realm shares some similarities with large
%distributed systems.  
Many distributed systems implement a publish/subscribe
abstraction for supporting communication that has similarities to our
events and event waiters\cite{Aguilera99,Carzaniga01}.  Work has
also been done on using object-oriented languages to build event-based
distributed systems\cite{Eugster01,Harrison97,Chang91}.  
%In these cases
%callbacks are registered to run when event operations are triggered
%remotely.  
Events in these systems are much heavier weight
and often carry large data payloads.  Many distributed event systems
focus on resiliency instead of performance\cite{Ostrowski09}.
