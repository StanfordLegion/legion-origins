
\section{Microbenchmarks}
\label{sec:micro}

We evaluate our
\Realm implementation
using microbenchmarks that test whether
performance approaches the capacity of the
underlying hardware.  All experiments were run on 
the Keeneland supercomputer\cite{Keeneland}.  Each 
Keeneland KIDS node is composed of two Xeon 5660 CPUs, 
three Tesla M2090 GPUs, and 24 GB of DRAM.  Nodes
are connected by an Infiniband QDR interconnect.

\begin{figure}
\begin{center}
{ \small
\begin{tabular}{m{2cm}|c|c|c|c|c}
Nodes & 1 & 2 & 4 & 8 & 16 \\ \hline
Mean Trigger Time ($\mu$s) & 0.329 & 3.259 & 3.799 & 3.862 & 4.013 \\
\end{tabular}
}
\end{center}
\vspace{-3mm}
\caption{Event Latency Results.\label{tab:eventlat}}
%\vspace{-4mm}
\end{figure}

\hyphenation{GASNet}

\subsection{Event Latency and Trigger Rates}
\label{subsec:eventmicro}

\begin{figure*}[!ht]
\centering
\subfloat[Event Trigger Rates]{
\label{fig:eventthroo}
\includegraphics[scale=0.35]{figs/event_throughput.pdf}
}
\subfloat[Reservations for Fixed 1024 Chains]{
\label{fig:fixedlock}
\includegraphics[scale=0.35]{figs/fixed_lock_chains.pdf}
}
\subfloat[Reservations for Fixed 8 Nodes]{
\label{fig:fixednode}
\includegraphics[scale=0.35]{figs/fixed_node_lock.pdf}
}
\caption{Microbenchmark Results.}
\vspace{-2mm}
\end{figure*}

We use two microbenchmarks to evaluate event performance.  
The first tests event triggering latency,
both within and between nodes.  Processors are organized
in a ring and each processor creates a user event dependent
on the previous processor's event.  The first event in the chain of
dependent events is triggered and the time until the triggering of the chain's
last event is measured; dividing the total time by the number
of events in the chain yields the mean trigger time.  In the single-node 
case, all events are local to that node, so no active messages are required.  
For all other cases, the ring uses a single processor per node so that 
every trigger requires the transmission (and reception) of an event trigger 
active message.

Table~\ref{tab:eventlat} shows the mean trigger times.
The cost of manipulating the data structures and running dependent
operations is shown by the single-node case, which had an average
latency of only 329 nanoseconds.  The addition of nearly 3 microseconds when
going from one node to two is attributable to the latency of a GASNet
active message; others have measured similar latencies\cite{GASNET06}.
The gradual increase in latency with increasing node count is likely 
related to the point-to-point nature of Infiniband communication,
which requires GASNet to poll a separate connection for every other node.

Our second microbenchmark measures the maximum rate at 
which events can be triggered by our implementation.  Instead of a 
single chain, a parameterized number (the {\em fan-in/out factor}) of parallel chains are created.  
The event at step $i+1$ of a chain depends on the $i$th event of every other chain.
The events within each step of the chains are distributed across the nodes.  Recall from 
Section~\ref{sec:events} that the aggregation of event subscriptions
limits the number of event trigger active messages to one per node 
(per event trigger) even when the fan-in/out factor exceeds the node count.


%% \begin{figure}
%% \begin{center}
%% \includegraphics[scale=0.35]{figs/event_throughput.pdf}
%% \end{center}
%% \vspace{-3mm}
%% \caption{Event Trigger Rate Microbenchmark.\label{fig:eventthroo}}
%% %\vspace{-4mm}
%% \end{figure}

Figure~\ref{fig:eventthroo} shows the event trigger rates 
for a variety of node counts and fan-in/out factors.
For small fan-in/out factors, the total rate falls off initially going 
to two nodes as active messages become necessary, but increases 
slightly again at larger node counts.  Higher fan-in/out factors require 
more messages and have lower throughput that also increases with node 
count.  Although the number of events waiting on each node decreases with 
increasing node count, the minimal scaling indicates the 
bottleneck is in the processing of the active message each node 
must receive rather than the local redistribution of the triggering
notification.

The compute-bound nature of the benchmark shows that active 
messages do not tax the network and leave bandwidth 
for application data movement.  The event trigger rates 
in this microbenchmark are one to two orders
of magnitude larger than the trigger rates required by the 
real applications described in Section~\ref{sec:results}.

\subsection{\Reservation Acquire Rates}
\label{subsec:acquiremicro}

The \reservation microbenchmark measures the rate at
which \reservation acquire requests can be granted.
A parameterized number of \reservations are created per
node and their handles are made available to every node.  Each node
then creates a parameterized number of {\em chains} of acquire/release
request pairs, where each request attempts to acquire a random \reservation and
is made dependent on the previous acquisition in the chain.
Thus the total number of chains across all
nodes gives the total number of acquire requests that can exist in the
system at any given time.  All chains are started at the same time 
%(via a dependence on a single user event), 
and the time to process all
chains is divided into the total number of acquire requests to yield an
average \reservation grant rate.

Figure~\ref{fig:fixedlock} shows the \reservation grant rate for a variety 
of node counts and \reservations per node.  The number of chains per node 
is varied so that the total number of chains in the system is 1024 in all 
cases.  For the single-node cases, the insensitivity to the number of 
\reservations indicates that the bottleneck is in the computational 
ability of the node to process the requests.  For larger numbers of nodes, 
especially for the larger numbers of \reservations per node (where contention
for any given \reservation is low), the speedup with increasing node count 
suggests the limiting factor is the rate at which \reservation-related 
active messages can be sent (nearly every request will require a
transfer of ownership).  In nearly all cases,
the performance actually increases with decreasing number of \reservations.  
Although contention increases, favoring local \reservation 
requestors makes contention an advantage, reducing the number of 
\reservation-related active messages that must be sent per 
\reservation grant.

%% \begin{figure}
%% \begin{center}
%% \includegraphics[scale=0.35]{figs/fixed_lock_chains.pdf}
%% \end{center}
%% \vspace{-3mm}
%% \caption{\Reservation Benchmark for Fixed Total Chains.\label{fig:fixedlock}}
%% %\vspace{-4mm}
%% \end{figure}

The benefit of \reservation unfairness is more clearly shown in Figure~\ref{fig:fixednode}.
Here the node count is fixed at 8 and \reservation grant rates are shown for a variety of total 
\reservation counts and number of chains per node. At 32 chains 
per node (256 chains total) contention is low and the grant 
rate is high.  As the number of chains per node increases there is more 
contention for \reservations and the grant rate drops.  For smaller 
\reservation counts, further increases in the number of chains results in 
improved grant rates.  On each line the increase occurs
when the chains per node exceeds the total number of \reservations, which 
is where the expected number of requests per 
\reservation per node exceeds one.  As soon as there are multiple 
requestors for the same \reservation on a node, the unfairness 
of \reservations reduces the number of \reservation ownership 
transfers, yielding better performance.

%% \begin{figure}
%% \begin{center}
%% \includegraphics[scale=0.35]{figs/fixed_node_lock.pdf}
%% \end{center}
%% \vspace{-3mm}
%% \caption{\Reservation Benchmark for Fixed Node Count.\label{fig:fixednode}}
%% %\vspace{-4mm}
%% \end{figure}

\subsection{Reduction Throughput}
\label{subsec:reducmicro}
To evaluate reduction instances we use
a histogram microbenchmark where all nodes perform 
an addition reduction to a physical region in the globally 
visible GASNet memory.
Using the reduction interface (Section~\ref{sec:physreg})
reductions are performed in five ways:

\begin{figure*}[!ht]
\centering
\subfloat[Dense Reduction Benchmark]{
\label{fig:reducdense}
\includegraphics[scale=0.35]{figs/reduce_dense.pdf}
}
\subfloat[Sparse Reduction Benchmark]{
\label{fig:reducsparse}
\includegraphics[scale=0.35]{figs/reduce_sparse.pdf}
}
\subfloat[Event Lifetimes - Fluid Application]{
\label{fig:eventlife}
\includegraphics[scale=0.35]{figs/event_lifetimes.pdf}
}
\caption{Performance of Reductions and Events.}
\vspace{-1mm}
\end{figure*}

\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
\item Single Reads/Writes - Each node acquires a \reservation on the 
global physical region and for each reduction
reads a value from the region, performs the addition, 
and writes the result back.  The \reservation is released 
after all reductions are performed.

\item Single Reductions - Each reduction is individually sent as 
an active message to the node whose system memory holds the target 
of the reduction.

\item Localized Instance - Each node takes a \reservation on the 
global physical region, copies it to a new instance in its own system memory, performs all reductions to the 
local region, copies the region back, and releases the \reservation.

\item Fold Instance - Each node creates a local fold reduction instance.
All reductions are folded into the instance, which is then copied and 
applied to the global region.

\item List Instance - Each node creates a local list reduction instance.
Reductions are added to the list reduction instance, which is 
then copied and applied to the global region.
\end{itemize}

We ran two experiments, one with dense reductions and one
with sparse reductions.  In both cases a 
large random source of data is divided into chunks, which are given to
separate reduction tasks.  Eight reduction tasks are created for 
each node, one per processor.  The dense case uses a histogram with 
256K buckets and each reduction task performs 4M reductions (Figure~\ref{fig:reducdense}).  
The sparse case uses a histogram with 4M buckets, but only 64K 
reductions are performed by each task (Figure~\ref{fig:reducsparse}).

In the dense experiment, the reduction fold instances perform 
best and scale well with the number of nodes, achieving over a billion 
reductions per second in the 16 node case.  List instances also scale well, 
but perform about an order of magnitude worse than fold instances  in 
the dense case.  The use of a separate active message for each reduction 
operation is another two orders of magnitude worse---data must be transferred in 
larger blocks to be efficient.  The localized instance approach works well for 
a single node, but its serial nature doesn't benefit from 
increasing node counts.  Finally, the only time the latency of 
performing individual RDMA reads and writes isn't disastrous is
on a single node, where the reads and writes are all local.

The sparse case is similar, except 
the reduction fold case suffers from the overhead of transferring a value 
for every bucket even though most buckets are unmodified.
The reduction list case continues to scale well, surpassing the 
reduction fold performance at larger node counts and showing that
list instances are better suited for scaling sparse reduction computations.


