
\section{Deferred Execution}
\label{sec:deferred}

On distributed memory architectures communication costs
can easily dominate performance. Thus,
all distributed memory runtime systems must provide mechanisms for
hiding long latency communication.  Dynamic explicit runtimes
have a unique additional problem: the operations to construct the dynamic 
graph are themselves potentially long latency. However, by providing
a well-designed client interface, \Realm is able to use the same
mechanism to hide both forms of latency.
%This problem does not exist in implicit
%systems (because there is no explicit graph) or in static explicit systems (because
%the analogous work on the graph is done statically).  However, it turns out that 
%given the right interface to the client, the dynamic construction of the graph
%can itself be made asynchronous and overlapped with other useful work.

In \Realm, all operations execute asynchronously.
When invoked, every \Realm operation returns immediately with an 
{\em event} that {\em triggers} when the operation completes. 
Furthermore, every \Realm operation takes an event as a 
precondition, and the operation is guaranteed not to begin 
until the precondition event has triggered. Consider
an application that needs to run operations
$A$ and $B$ on different nodes, where $A$ produces data $a$
that must be copied to the input $b$ of $B$, and also operations
$C$ and $D$, where $D$ depends on $C$.
The \Realm client would issue the following calls:
\begin{lstlisting}
Event e1 = p1.spawn(A,...,NO_EVENT);
Event e2 = a.copy_to(b,e1);
Event e3 = p2.spawn(B,...,e2);
Event e4 = p1.spawn(C,...,NO_EVENT);
Event e5 = p1.spawn(D,...,e4);
\end{lstlisting}

Here {\tt p.spawn(X,\ldots)} means operation {\tt X} is to be run on processor {\tt p}.
The use of events {\tt e1}, {\tt e2}, and {\tt e4} as preconditions
tells \Realm how to construct the dynamic dependence graph: \\[1mm]

\simplegraph
 
This example illustrates two important points.  First, events 
in \Realm represent control dependences only---events carry 
no data and copies are themselves operations in the graph.
The simplicity of events enables the representation 
and optimizations discussed in Section~\ref{sec:events}.

Second, from the client's point of view these
five statements execute without delay---all the operations are
launched asynchronously and the client is never required to
block on any \Realm operation.  In general, the use of events as dependences
between operations allows clients to launch
arbitrarily deep acyclic graphs of dependent operations without
the need to block on intermediate results. To the best of our
knowledge, this execution model has no name in the literature;
we refer to it as {\em deferred execution}.

For contrast, consider how this example would be executed by an
implicit runtime.  First $A$ would be spawned asynchronously.
Next, the asynchronous copy from $a$ to $b$ would
block pending the availability of $a$.  Up to this point,
the two models are the same: $A$ is
running and the copy is waiting on the completion of $A$.  However, in
standard asynchronous execution the client makes no further progress because
it has the responsibility of waiting until it is
safe to issue the copy. In deferred execution, this responsibility is delegated
to the runtime, and the client can immediately continue 
with the spawn of $B$, even if the data in $a$ is not yet ready.
Deferred execution allows the client to continue to build the explicit
graph, enabling the runtime to discover and construct the graph for the independent chain of
operations $C$ and $D$ while $A$ is executing.  Furthermore, once processor $p1$ is available
after executing $A$, the two chains of operations can execute in parallel.
A similar effect can be achieved in implicit systems by 
hoisting $C$ and $D$ above $B$, but this solution places the burden for
scheduling on the programmer (recall Section~\ref{sec:related}).

%Deferred execution has two attractive features. First, the
%mechanism for hiding latency is decoupled from the implementation.
%\Realm clients can issue operations in any order
%allowing code to be modular for maintainability.
%The \Realm implementation is then free to re-order operations as
%long as it obeys event dependences. The \Realm implementation
%can run operations as soon as their precondition events have
%triggered, maximizing throughput and providing automatic
%overlap of work with long latency operations. Second, \Realm
%clients are, by construction, performance portable.  The \Realm
%implementation will automatically adapt to different latencies
%on new architectures without needing to modify client code.

A key to enabling deferred execution in \Realm is making 
events inexpensive.  With the client able to issue operations far
ahead of the actual execution, a large number of events are needed to
track the dependences between operations.
%\Realm allows clients to issue operations
%far ahead of actual execution to hide the latency of building and managing
%the graph. 
As we show 
in Section~\ref{sec:results}, \Realm clients can generate 
tens of thousands of unique events per second per node during execution. 
In Section~\ref{sec:events}, we describe the implementation of
generational events that are crucial to 
making events cheap and deferred execution practical.

%The first design decision in \Realm is that edges in the dependence graph 
%represent only control dependences---an 
%edge $(A,B)$ means only that
%operation $A$ must complete before operation $B$ can begin.  In
%particular, edges in the \Realm dependence graph carry no data. \Realm
%provides separate operations to copy data from one place to another,
%and copies are themselves operations in the dependence graph.  As an
%example, assume that a \Realm client needs to issue an operation $A$
%that produces data in a location $a$ that must be copied to another
%location $b$ where it will serve as the input to another operation
%$B$. The \Realm client issues three operations $A$, $copy\langle a
%\rightarrow b \rangle$ and $B$ with the dependences (edges)
%$(A,copy\langle a \rightarrow b \rangle)$ and $(copy\langle a
%\rightarrow b \rangle,B)$: \\[1mm]
%
%\simplegraph
%
%The edges express that the copy cannot
%start until $A$ has completed and produced its output in $a$, and $B$
%cannot start until the copy has finished moving the data to $b$. 
%
%The separation of control from data movement in \Realm allows a very
%efficient distributed representation of the dependence graph, which is 
%central to achieving low overheads on large machines. Graph edges
%are implemented by \Realm\ {\em events}.  Events record only a single bit, whether the
%event is {\em untriggered} or {\em triggered}.  A graph edge's corresponding
%event is triggered when its source operation has completed.  Using
%a novel implementation technique of {\em generational events}, \Realm 
%compresses the representation of many events into a single generational event and
%passes events by value without the need to track where events propagate and are
%used across the machine.  The result is a very large reduction
%in the overheads of storing and using events in comparison to traditional event-based
%systems.  
%
%The fact that events are extremely cheap makes it possible to use 
%them ubiquitously as \Realm's main building block.  When invoked, every \Realm operation 
%returns immediately with an event that triggers
%when the operation completes.  Furthermore, every operation in \Realm takes
%an event as a precondition, and the operation does not begin execution until the precondition
%event has triggered.  To generate the example dependence graph given above, a client
%would issue the following operations:   
%
% FIXME:  Do we want to mention deferred execution at all? Yes! :)
%

\Realm's novel operations are integrated into the deferred execution model as well.
For example, a reservation request immediately returns an event
that triggers when the reservation is granted.  In this way,
reservations are a deferred execution version of locks that do not
block and allow another operation's execution to be
dependent on the reservation's acquisition.
Similarly, \Realm's data movement operations are deferred.
\Realm's support for novel bulk reductions allows clients to construct
sophisticated asynchronous reduction trees that match the 
shape of the machine's memory hierarchy. We illustrate a real-world 
application that leverages this feature in Section~\ref{sec:results}.

