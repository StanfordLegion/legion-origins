
%\section{\Realm Implementation}
%\label{sec:impl}
%There are currently two implementations of \Realm:
%one for shared memory machines and
%one for  clusters of both CPUs
%and GPUs.  The shared-memory implementation uses 
%POSIX threads\cite{PTHREADS} (Pthreads)
%and is primarily for debugging.  

%The is a different {\tt CPU} processor or each CPU core and a {\tt GPU} processor for
%each GPU, matching the scheduling granularity available in the Pthreads and CUDA APIs,
%respectively.  

%The first kind of memory is the 
%system memory that is accessible by every CPU core on 
%a given node.  The second is the framebuffer memory on 
%each GPU (and accessible only by that GPU).  The third 
%kind of memory is system memory on a node that has been 
%made accessible to the GPU(s) on that node as well as the 
%CPU cores, commonly referred to as {\em zero-copy memory}.
%The final kind of memory is the portion of system memory on 
%each node that has been registered with the GASNet runtime 
%to allow {\em remote direct memory access} (RDMA) by other 
%nodes in the cluster.  We refer to this memory 
%as {\em GASNet memory}.


%We describe the implementation of \Realm's novel features, specifically
%events, \reservations, and reduction instances.

\section{Events}
\label{sec:events}

\circuiteventgraph

Events describe
dependences between operations in \Realm.  
In Figure~\ref{fig:circuit_events}, events are 
solid black arrows between operations, indicating
one operation must be performed before another.  
Events express control dependences only---events
order operations but do not imply anything about 
data dependences and do not involve data movement.

%The right side of the figure
%shows application tasks while the left side shows ``mapping'' tasks 
%that dynamically compute where application tasks should run. 

Lines 1-13 of Figure~\ref{fig:runtimeapi} show 
the event interface.  An instance of the
{\tt Event} type names a unique event in the system.
{\tt NO\_EVENT} (line 3) is a special instance 
that by definition has always triggered.
The event interface supports testing whether an
event has triggered (line 5) and waiting on an event
to trigger (line 6), but the preferred use of events
is passing them as preconditions to other operations.
If an operation has more than one precondition, those
events are merged into an aggregate event with the
{\tt merge\_events} call (line 7).  The aggregate event
does not trigger until all of the constituent events have
triggered.
In most cases, events are created as the result of
other \Realm calls, and the implementation is responsible
for triggering these events.  However, clients can also create
a {\tt UserEvent} (line 10) that is triggered explicitly
by the client.

\subsection{Event Implementation}
\label{sec:eventimpl}

Events are created on demand and are {\em owned} by the creating node.
The {\tt Event} type is a light-weight {\em handle}. The space of event 
handles is divided statically across the nodes by
including the owner node's ID in the upper bits of the event handle.
This allows each node to assign handles to new events without
conflicting with another node's assignments and without inter-node
communication.  The inclusion of the node ID in event handles also permits any
node to determine an event's owning node without communication.

When a new event $e$ is created, the owning node $o$ allocates a 
data structure to track $e$'s state ({\em triggered} 
or {\em untriggered}) and $e$'s local list of {\em waiters}:
dependent operations (e.g., copy operations and task launches) 
on node $o$.  The first reference to $e$ by a remote node $n$ allocates the same
data structure on $n$.  An {\em event subscription} 
active message is then sent to node $o$ indicating 
node $n$ should be informed when $e$ triggers.  
Any additional dependent operations on node $n$
are added to $n$'s local list of $e$'s waiters without 
further communication.  When $e$ triggers, the owner node $o$
notifies all local waiters and sends an {\em event trigger} 
message to each subscribed node.  If the owner node $o$
receives a subscription message after $e$ triggers, $o$
immediately responds with a trigger message.

The triggering of an event may occur on any node.  
When it occurs on a node $t$ other than the owner $o$, a 
trigger message is sent from $t$ to $o$, which forwards that message 
to all other subscribed nodes.  The triggering node $t$
notifies its local waiters immediately; no message is sent from 
$o$ back to $t$.  While a remote 
event trigger results in the latency of a triggering 
operation being at least two active message flight times, it 
bounds the number of active messages required per event trigger to 
$2N-2$ where $N$ is the number of nodes monitoring the event
(which is generally a small fraction of the total number of machine nodes).  
An alternative is to share the subscriber list 
so that the triggering node can notify all interested nodes 
directly.  However, such an algorithm is both more complicated 
(due to race conditions) and requires $O(N^2)$ active messages.  
Any algorithm super-linear in the number of nodes in the system will 
not scale well, and as we show in Section~\ref{subsec:eventmicro},
the latency of a single event trigger active message is very small. 

\begin{figure}
\resizebox{0.5\textwidth}{!}{
\begin{tikzpicture}[every node/.style={scale=0.5,inner sep=0.02in}]

\node at (0.25,0) {Event $x$};
\draw[thin,gray!30] (0.75,0) -- ++(4.25,0);
\draw[thin,fill=white] (1,-0.1) rectangle ++(0.5,0.2);
\draw[thin,fill=gray!50] (1.5,-0.1) rectangle ++(3.3,0.2);
\node[thin] at (1,0.5) {C}; \draw[thin,->] (1.0,0.35) -> ++(0,-0.2);
\node[thin] at (1.5,0.5) {T}; \draw[thin,->] (1.5,0.35) -> ++(0,-0.2);
\node[thin] at (1.25,0.48) {Q}; \draw[thin,dotted,->] (1.25,0.35) -> ++(0,-0.2);
\node[thin] at (1.75,0.48) {Q}; \draw[thin,->] (1.75,0.35) ->  ++(0,-0.2);
\node[thin] at (4.8,0.48) {Q}; \draw[thin,->] (4.8,0.35) ->  ++(0,-0.2);

\node at (0.25,-1) {Event $y$};
\draw[thin,gray!30] (0.75,-1) -- ++(4.25,0);
\draw[thin,fill=white] (2.0,-1.1) rectangle ++(0.5,0.2);
%\draw[thin,fill=gray!50] (2,-0.1) rectangle ++(2.5,0.2);
\node[thin] at (2.0,-0.5) {C}; \draw[thin,->] (2.0,-0.65) -> ++(0,-0.2);
\node[thin] at (2.5,-0.5) {T}; \draw[thin,->] (2.5,-0.65) -> ++(0,-0.2);
%\node[thin] at (1.7,0.48) {Q}; \draw[thin,dotted,->] (1.7,0.35) -> ++(0,-0.2);
%\node[thin] at (4.5,0.48) {Q}; \draw[thin,->] (4.5,0.35) -> ++(0,-0.2);

\node at (0.25,-2) {Event $z$};
\draw[thin,gray!30] (0.75,-2) -- ++(4.25,0);
\draw[thin,fill=white] (3,-2.1) rectangle ++(0.75,0.2);
\draw[thin,fill=gray!50] (3.75,-2.1) rectangle ++(0.75,0.2);
\node[thin] at (3,-1.5) {C}; \draw[thin,->] (3,-1.65) -> ++(0,-0.2);
\node[thin] at (3.75,-1.5) {T}; \draw[thin,->] (3.75,-1.65) -> ++(0,-0.2);
\node[thin] at (3.5,-1.52) {Q}; \draw[thin,dotted,->] (3.5,-1.65) -> ++(0,-0.2);
\node[thin] at (4.5,-1.52) {Q}; \draw[thin,->] (4.5,-1.65) -> ++(0,-0.2);

\draw[thin] (-0.2,-2.5) -- ++(5.4,0);

\node at (0.25,-3.5) {\begin{tabular}{c}Gen.\\Event $w$\end{tabular}};
\fill[gray!50] (0.75,-3.6) rectangle ++(4.25,0.2);
\draw[thin] (0.75,-3.4) -- ++(4.25,0);
\draw[thin] (0.75,-3.6) -- ++(4.25,0);
\draw[thin,fill=white] (1,-3.6) rectangle ++(0.5,0.2);
\draw[thin,fill=white] (2.0,-3.6) rectangle ++(0.5,0.2);
\draw[thin,fill=white] (3,-3.6) rectangle ++(0.75,0.2);

\draw[thin] (1,-3.6) -- ++(0,-0.2);
\draw[thin] (2,-3.6) -- ++(0,-0.2);
\draw[thin] (3,-3.6) -- ++(0,-0.2);
\node at (0.75,-3.8) {0};
\node at (1.5,-3.8) {1};
\node at (2.5,-3.8) {2};
\node at (3.5,-3.8) {3};

\node[thin] at (1,-3.0) {C${}_1$}; \draw[thin,->] (1.0,-3.15) -> ++(0,-0.2);
\node[thin] at (1.5,-3.0) {T${}_1$}; \draw[thin,->] (1.5,-3.15) -> ++(0,-0.2);
\node[thin] at (1.25,-3.02) {Q${}_1$}; \draw[thin,dotted,->] (1.25,-3.15) -> ++(0,-0.2);
\node[thin] at (1.75,-3.02) {Q${}_1$}; \draw[thin,->] (1.75,-3.15) ->  ++(0,-0.2);
\node[thin] at (4.8,-3.02) {Q${}_1$}; \draw[thin,->] (4.8,-3.15) ->  ++(0,-0.2);

\node[thin] at (2.0,-3.0) {C${}_2$}; \draw[thin,->] (2.0,-3.15) -> ++(0,-0.2);
\node[thin] at (2.5,-3.0) {T${}_2$}; \draw[thin,->] (2.5,-3.15) -> ++(0,-0.2);

\node[thin] at (3,-3.0) {C${}_3$}; \draw[thin,->] (3,-3.15) -> ++(0,-0.2);
\node[thin] at (3.75,-3.0) {T${}_3$}; \draw[thin,->] (3.75,-3.15) -> ++(0,-0.2);
\node[thin] at (3.5,-3.02) {Q${}_3$}; \draw[thin,dotted,->] (3.5,-3.15) -> ++(0,-0.2);
\node[thin] at (4.5,-3.02) {Q${}_3$}; \draw[thin,->] (4.5,-3.15) -> ++(0,-0.2);
\end{tikzpicture}
}
\caption{Generational Event Timelines\label{fig:gen_events}}
\vspace{-2mm}
\end{figure}

\subsection{Generational Events}
\label{sec:generation}

There are several constraints on the lifetime of the data structure used to
represent an event $e$.  Creation and triggering of $e$
can each happen only once, but any number of operations 
may depend on $e$.  Furthermore, some operations depending 
on $e$ may not even be requested until long after $e$ has 
triggered. Therefore, the data structure used to represent $e$ cannot
be freed until all these operations depending on $e$ have
been registered.  Some systems ask the programmer to explicitly
create and destroy events\cite{StarPU11}, but this is problematic 
when most events are created by \Realm rather than the programmer.  
Other systems address this issue by reference counting event
events\cite{Khronos:OpenCL}, but reference counting adds client and
runtime overhead even on a single node, and incurs even greater cost 
in a distributed memory implementation.

Instead of freeing event data structures, our 
implementation aggressively recycles them.  Compared to 
reference counting, our implementation requires fewer 
total event data structures and has no client or runtime overhead.  
The key observation is that one {\em generational event} data 
structure can represent one untriggered 
event and a large number (e.g., $2^{32}-1$) of already-triggered 
events.  We extend each event handle to 
include a generation number and the identifier for its 
generational event.  Each generational event records how many 
generations have already triggered.
A generational event can be 
reused for a new generation as soon as the current generation 
triggers.
(Any new operation 
dependent on a previous generation can immediately be executed.)
To create a new event, a node finds a generational 
event in the triggered state (or creates one if all existing
generational events owned by the node are in the untriggered state),
increases the generation by one, 
and sets the generational event's state to untriggered.  As before, 
this can be done with no inter-node communication.

An example of how multiple events can be represented by a single generational
event is shown in Figure~\ref{fig:gen_events}.  Timelines for events $x$,
$y$, and $z$ indicate where creation (C), triggering (T) and queries (Q)
occur.  Queries that succeed (i.e. the event has triggered) are 
shown with solid arrows, while those that fail are dotted.  The lifetime of
an event extends from its creation until the last operation (trigger or query) performed on it.
Although the lifetime of event $x$ overlaps with those of $y$ and $z$, the
untriggered intervals are non-overlapping, and all three can be mapped on
to generational event $w$, with event $x$ being assigned generation $1$, $y$
being assigned $2$, and $z$ being assigned $3$.  A query on the generational event
succeeds if the generational event is either in the triggered state or has a current generation
larger than the one associated with the query.

Nodes maintain generational event data structures for
both events they own as well as remote events that they
have observed. Remote generational event data structures 
record the most recent generation known to have triggered as well
as the generation of the most recent subscription message sent 
(if any).  Remote generational events enable an interesting optimization.  If
a remote generational event receives a query 
on a later generation than its current generation, it 
can infer that all generations up to the requested generation have 
triggered, because the new generation(s) of the event were able to be  
created by the event's owner.  All local waiters for earlier
generations can 
be notified even before receiving the event trigger message 
for the current generation.

In Section~\ref{sec:micro} we show that the latency of event triggering is very low, even
in the case of dense, distributed graphs of dependent operations.  In Section~\ref{sec:results}
we show that our generational event implementation results in a large reduction
in space requirements to record events for real applications.


\section{\Reservations}
\label{sec:reservations}
%Recall from Section~\ref{sec:circuit_ex}
%that each circuit task launch requires a Legion
%runtime task to compute its data dependences.  

Recall that the tasks on the right of  Figure~\ref{fig:circuit_events}
are the actual application-level tasks and that the mapping tasks on the left
dynamically compute where the application-level tasks should run.
The higher-level runtime's mapping tasks may be run in any order, but each
requires exclusive access to a shared data structure.
In most systems, locks are used
for atomic data access.  However, standard locking
primitives do not integrate well with deferred execution,
as the requestor must wait (or constantly poll) for the lock
to be granted.
{\em \Reservations} are a new synchronization mechanism
that serve the purpose of locks in \Realm's dynamically generated 
control dependence graph of deferred operations.
%However, standard locking primitives
%do not integrate well with deferred execution, where we want to launch operations
%asynchronously with actual execution contingent on dependences on other operations.
%{\em \Reservations} are a new synchronization mechanism that satisfy these requirements.

\Reservations (lines 26-34 of Figure~\ref{fig:runtimeapi}) are 
requested using the {\tt acquire} method.  Rather than waiting when
the reservation is held (or returning a ``retry'' response), the request
 returns immediately 
with an event that will trigger when the \reservation is eventually granted.  
\Reservations are released with the {\tt release} method. As with 
other \Realm operations, the {\tt acquire} and {\tt release} methods
accept an event parameter as a precondition (lines 28-29). A chain
of event dependences should always exist between paired acquire
and release invocations.

Another important difference between \reservations and
locks is that the processor requesting the reservation
need not be the one that uses it.  A common \Realm idiom
is to acquire a \reservation on behalf of a task
being launched.  For example, in Figure~\ref{fig:circuit_events},
acquire requests ({\tt acq} diamonds) are made for the \reservation 
protecting the higher-level runtime's meta-data.  The event returned by
these requests then becomes the precondition for launching the 
mapping tasks, which actually use the meta-data.
The \reservation releases ({\tt rel} diamonds) are
made before the mapping tasks even run, but are conditioned on
the completion of the mapping tasks.  Dotted arrows between acquire
and release nodes indicate one possible execution order of \reservation
grants and requests.  Note that completion events
for mapping calls are made preconditions for later \reservation
requests.  This prevents acquire requests from later mapping
tasks from being processed before other preconditions
have been satisfied, which could lead to deadlock.  Preconditions on
reservation acquires also allow the acquisition of multiple reservations
to follow a specific order, analogous to the standard technique for 
avoiding deadlock when requesting multiple locks simultaneously.

Often \reservations are used to guard small allocations
of data.  To improve support for this idiom in a distributed environment,
we allow a small (less than 4KB) {\em payload} of data to be
associated with a \reservation.  The payload is
guaranteed to be coherent while the \reservation is held.  The
payload size is specified when the \reservation is created
(line 31) and a pointer to the local copy of the payload is
obtained from the {\tt payload\_ptr} method (line 32).

Events and \reservations give \Realm expressiveness equivalent
to the interfaces of implicit representation systems for ordering
and serialization.  \Realm also makes these operations deferred and 
composable, neither of which is possible in other runtime interfaces.

\subsection{Reservation Implementation}
\label{sec:resimpl}

Like events, \reservations are created on demand, using a space 
of handles statically divided across the nodes.  
\Reservation creation requires no communication
and the handle is sufficient to determine 
the creating node.  However, whereas event ownership 
is static, \reservation ownership may migrate; the creating node 
is the initial owner, but ownership can be transferred to other nodes.
Since any node may at some point own a \reservation $r$, 
all nodes use the same data structure with the following fields 
to track the state of $r$:

\begin{itemize} \itemsep1pt \parskip0pt \parsep0pt
\item {\em owner node} - the most recently known owner of $r$.  
If the current node is the owner, this
information is correct.  If not, this information may 
%be stale, but by induction the recorded owner 
%either is the owner or knows the next node to query about ownership.
be stale, but the recorded node will have more recent information
about the true owner and will forward the request.

\item {\em \reservation status} - records whether $r$ is 
currently held; valid only on the current owner.

\item {\em local waiters} - a list of pending local acquire 
requests.  This data is always valid on all nodes.

\item {\em remote waiters} - a set of other nodes known to have 
pending acquire requests;  valid only on the current owner.

\item {\em local payload pointer and size} - a copy of $r$'s payload
\end{itemize}

Each time an acquire request is made, a new event is created 
to track when the grant occurs. 
 The current node then
examines its copy of the \reservation data structure to determine 
if it is the owner.  
If the current node is the owner and the 
\reservation isn't held, the acquire request is granted immediately 
and the event is triggered.  If the \reservation is held, the 
event is added to the list of local waiters.  Note that the 
event associated with the acquire request is the only data that 
must be stored.  If the current node isn't the owner, a 
{\em \reservation acquire} active message is sent to the most 
recently known owner.  If the receiver of an acquire request  
message is no longer the owner, it forwards the message on to 
the node it has recorded as the owner.  If the current owner's 
status shows the \reservation is currently held, the requesting node's 
ID is added to the remote waiters set.  If 
the \reservation is not held, the ownership of the \reservation 
is given to the requesting node via a {\em \reservation transfer} 
active message, which includes the set of remaining remote waiters and 
an up-to-date copy of the \reservation's payload.  
%The inclusion 
%of the payload in the active message is the reason for the 
%4KB size limit specified on payloads in Section~\ref{subsec:reservations}.

Similarly, a release request (once its preconditions have been 
satisfied) is first checked against the local 
node's \reservation state.  If the local node is not the owner, 
a {\em release} active message is sent to the most recently 
known owner, which is forwarded if necessary.  Once the release 
request is on the \reservation's current owning node, the local 
waiter list is examined.  If the list is non-empty, the \reservation 
remains in the acquired state and the first acquire grant
event is pulled off the local waiter list and triggered.  If 
the local waiter list is empty, the acquire state is 
changed to not-acquired, and the set of remote waiters 
is examined.  If there are remote waiters, one is
chosen and the corresponding node becomes the new owner via a 
\reservation transfer.

The unfairness inherent in favoring local waiters over remote waiters
is intentional.  When contention on a \reservation is high (the only
time fairness is relevant), the latency of transferring a \reservation
between nodes can be the limiter on throughput. Minimizing the
number of \reservation transfers maximizes \reservation throughput
(see Section~\ref{subsec:acquiremicro}).

\section{Physical Regions}
\label{sec:physreg}

Traditionally, most interfaces for data movement in distributed
memory architectures only support copies of untyped buffers
(e.g. MPI send operations). However, it is often useful to 
associate operations on data, such as reductions, in conjunction with 
data movement.  Performing bulk data movement and reductions simultaneously
can significantly reduce their cost compared to performing the operations 
separately. To support bulk reductions, where each element of a collection 
is the result of a reduction, \Realm needs to know the type (or at least the
size) of the elements in the collections involved.  \Realm relies on a 
system of typed {\em physical regions} to manage the layout and movement 
of data in a deferred execution model.

%To defer data movement operations, \Realm requires 
%the type of data stored in buffers.  For example, in Section~\ref{sec:circuit_ex}, 
%reductions were first buffered in a local memory (e.g. on a GPU)
%and then further reduced into a different remote memory
%(e.g. GASNet memory).  
%As discussed in Section~\ref{sec:circuit_ex}, reductions in \Realm
%can be buffered in a local memory (e.g., on a GPU) and then
%further reduced into a different remote memory (e.g., GASNet memory).  
%More generally, a common deferred execution idiom is to build a combining
%tree of reductions where the shape of the tree matches the memory
%hierarchy and execution of each tree node is deferred until
%the reductions of its subtrees complete.  

A physical region defines an addressing scheme for a collection of
elements of a common type.  To a first approximation, physical regions
of elements of type $T$ are arrays of $T$ with
additional metadata to support efficient copies, reductions, and
allocation/deallocation of elements within the region.
\Realm supports creating multiple {\em instances} of a physical region
in different memories for replication or data migration.  Because all instances of a physical region
$r$ use the same addressing scheme, \Realm has sufficient information
to perform deferred copies between instances of $r$.

Lines 35-64 of Figure~\ref{fig:runtimeapi} show a subset of the
interface for physical regions (we omit portions of the interface due to space constraints).
%\footnote{Due to space constraints
%we omit the full interface which contains support for the subsetting
%of physical regions and registering of client-defined reduction operations
%with apply and fold functions.}
Each physical memory is named by a {\tt Memory} object (line 35).
Physical region objects are constructed by defining the maximum number
and size of elements (line 44).  Physical region instances 
are created in a specific {\tt Memory} (line 50).
To maintain performance transparency, there is no virtualization of
memory---each {\tt Memory} is sized based on a physical capacity and a
new instance can be allocated only if sufficient space remains.
Instances must be explicitly destroyed, which can be contingent on an event (lines 53-54).

%
% ALEX: Do we need to explain this at all?
%
Elements can be dynamically allocated or freed within a physical
region (lines 47-48).  Elements are accessed by \Realm pointers of
type {\tt ptr\_t}.  By definition, a \Realm pointer into physical
region $r$ is valid for every instance of $r$ regardless of
its memory location (line 60).  This allows \Realm pointers to be stored
in data structures and reused later, even if instances have been moved around.
In common cases, pointer indexing
reduces to inexpensive array address calculations which are compiled
to individual loads and stores.

\Realm supports copy operations between instances of
the same physical region (line 61).  Copy operations in 
Figure~\ref{fig:circuit_events} are rhomboids marked {\tt copy}.
Like all other \Realm operations, copy 
requests accept an event precondition and return an event that 
triggers upon completion of the copy.  \Realm 
does not guarantee the coherence of data between different
instances; coherence must be explicitly managed by the client 
via copy operations.

\subsection{Reduction Instances}
\label{subsec:reducimpl}

If a task only performs reductions on an instance,
a special reduction-only instance may be created (lines 51-52).  
%In Section~\ref{sec:circuit_ex},
In Figure~\ref{fig:circuit_events}, each task $\tt T_i$  is mapped 
to use reduction-only instance $t_i$ to accumulate reductions
in the GPU zero-copy memory.  Using the {\tt reduce\_to} method
(lines 62-63), these reduction buffers are eventually applied to
a normal instance residing in GASNet memory.  We detail this
pattern in a real-world application in Section~\ref{sec:results}.
Bulk reduction operations are rhomboids marked {\tt reduce} in 
Figure~\ref{fig:circuit_events}.
%The use of reduction-only instances can result in
%significantly better performance (see Section~\ref{subsec:reducmicro}).

%we show that reduction-only instances reduce both overhead and total
%inter-node traffic while still permitting parallelism.  Furthermore, 
%reduction-only instances are often smaller than normal instances because 
%they only need to store a single field of a larger structure.  Smaller 
%reduction instances improve cache performance and help address capacity 
%issues for smaller memories (e.g., GPU device memory).

Reduction-only instances differ in two important ways from normal instances.
First, the per-element storage in reduction-only
instances is sized to hold the ``right-hand side'' of the reduction
operation (e.g., the $v$ in $struct.field\ \text{+=}\ v$).
Second, individual reductions are accumulated (atomically) into the local
reduction instance, which can then be sent as a batched reduction to
a remote target instance.  When multiple reductions are made to the same element,
they are {\em folded} locally, further reducing 
communication. The fold operation
is not always identical to the reduction operation.  For example,
if the reduction is exponentiation, the corresponding fold
is multiplication:
$$(r[i]\ \text{**=}\ a)\ \text{**=}\ b \quad \Leftrightarrow 
\quad r[i]\ \text{**=}\ (a * b)$$
The client registers reduction and fold operations at system
startup (omitted from Figure~\ref{fig:runtimeapi} due to space constraints).
%As mentioned in Section~\ref{subsec:phyreg}, 
Reduction instances can also be folded into other reduction instances to
build hierarchical bulk reductions matching the memory 
hierarchy.  

%For example, in the circuit simulation, when there are multiple
%GPUs per node each GPU reduces into a local reduction instance in zero-copy memory,
%which are then folded together into a reduction instance for each node, and finally 
%reduced across all nodes into GASNet memory.

%
% Alex: Need to make it clear back in section on physical 
%       regions that they are close to arrays.
% Mike: We do this above where you ask if we need to describe
%       how data in physical regions are accessed
%
\Realm supports two classes of reduction-only instances.  
A {\em reduction fold instance} is similar
to a normal physical region in that it is implemented as an array 
indexed by the same element indices.
The difference is that each instance element is a value of the reduction's
right-hand-side type, which is often smaller than the 
region's element type (the left-hand-side type).  
A reduction operation simply folds the supplied right-hand-side value into 
the corresponding array location.  
%Any number of reductions can be folded into each location.  
When the reduction fold instance $p$ is reduced
to a normal instance $r$, first $p$
is copied to $r$'s location, where  
\Realm automatically applies 
$p$ to $r$ by invoking the 
reduction function once per location via a cache-friendly 
linear sweep over the memory.

The second kind of reduction instance is a {\em reduction list instance},
where the instance is implemented as a list of reductions.
%This representation can be better if updates are sparse (see
%Section~\ref{subsec:reducmicro}).  
A reduction list instance logs
every reduction operation (the pointer location and
right-hand-side value).  When the reduction list instance $p$ is reduced 
to a normal physical region $r$, $p$ is transferred and replayed
at $r$'s location.  In cases where the list of reductions
is smaller than the number of elements in $r$
the reduction in data transferred can yield better performance
(see Section~\ref{subsec:reducmicro}).
