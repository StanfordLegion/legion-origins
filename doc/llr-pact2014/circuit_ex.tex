
\lstset{
  captionpos=b,
  language=C++,
  basicstyle=\scriptsize,
  numbers=left,
  numberstyle=\tiny,
  columns=fullflexible,
  stepnumber=1,
  escapechar=\#,
  keepspaces=true,
  belowskip=-10pt,
  literate={<}{{$\langle$}}1 {>}{{$\rangle$}}1,
  morekeywords={region,coloring,partition,spawn,disjoint,aliased,task},
  deletekeywords={float},
  xleftmargin=15pt,
  %morekeywords={region,coloring,partition,spawn,disjoint,aliased},
  %deletekeywords=float,
}

\section{Motivating Example}
\label{sec:circuit_ex}

%\Realm is a run-time system for deferred execution on distributed memory parallel machines. 
%\Realm provides a few primitives with maximum performance at a low level, 
%just above the hardware.
To help motivate \Realm's design, 
we describe an optimized implementation of 
a real-world application that benefits from deferred execution.  This application is written using
Legion \cite{Legion12}, and both the application and the Legion
implementation itself are \Realm clients.  This section gives a high-level overview of both the application's
structure and Legion, focusing on aspects that are important for \Realm.  The
discussion of \Realm itself begins in Section~\ref{sec:interface}.

Consider simulating
an integrated circuit.  The input is a graph 
of wires (edges) and nodes where  
wires connect.  
%The circuit simulation iterates for many time steps.  
Each time step performs three computations: {\tt
calc\_new\_currents}, {\tt distribute\_charge}, and {\tt
update\_voltages}.  High-level Legion code
for the main loop is shown on lines 24-27 of
Listing~\ref{lst:circuit_code}.  
%After explaining the code and the
%hardware target, we illustrate how \Realm's low-level
%interface enables deferred execution of this program.
%discuss some issues in using \Realm's low-level
%interface for deferred execution to implement this program.

\begin{lstlisting}[float={t},label={lst:circuit_code},caption={Legion code for a circuit simulation.}]
struct Circuit { region   rn; /* contains all nodes for the circuit */
                 region   rw; /* contains all circuit wires */ };
struct CircuitPiece {
  region  rn_owned, rn_ghost; /* owned, ghost node regions */
  region  rw_owned;           /* private wires region */ };

                           // RO = Read-Only, RW = Read-Write
task void calc_new_currents(CircuitPiece piece):
        RW(piece.rw_owned), RO(piece.rn_owned, piece.rn_ghost);
                          // Rd = Reduction
task void distribute_charge(CircuitPiece piece, float dt):
        RO(piece.rw_owned), Rd(piece.rn_owned, piece.rn_ghost);
task void update_voltages(CircuitPiece piece): RW(piece.rn_owned);

task void simulate_circuit(Circuit c, float dt) : RW(c.rn, c.rw)
{
  // Partitioning Details Not Shown
  //  ...
  // Construct Circuit Pieces
  CircuitPiece pieces[NUM_PIECES];
  for(i = 0; i #$<$# NUM_PIECES; i++) 
    pieces[i] = { rn_owned: p_owned_nodes[i], 
                  rn_ghost: p_ghost_nodes[i], rw_owned: p_wires[i] };
  for (t = 0; t #$<$# TIME_STEPS; t++) {
    spawn (i = 0; i #$<$# NUM_PIECES; i++) calc_new_currents(pieces[i]);
    spawn (i = 0; i #$<$# NUM_PIECES; i++) distribute_charge(pieces[i], dt);
    spawn (i = 0; i #$<$# NUM_PIECES; i++) update_voltages(pieces[i]);
  }
}
\end{lstlisting}


Figure~\ref{fig:circuit_part} shows the partitioning of tasks and data
for parallel execution.  Tasks and the subtasks they launch are represented by
the nested rectangles in the figure's center.  The data for the
nodes and wires in the graph are placed in collections called
{\em regions}, which are delineated by rectangles with rounded corners.
Each of {\tt calc\_new\_currents}, {\tt
distribute\_charge} and {\tt update\_voltages} is partitioned into
sub-tasks that operate on separate {\em pieces} of the circuit.
Each piece is a subgraph, a subset of
wires and nodes {\em owned} by that piece, indicated by ovals in the
node and wire regions.  
The tasks also need access to the nodes bordering a piece, so each piece
has a {\em ghost node} region, which overlap with the owned nodes
of other pieces (indicated by overlapping ovals).

Figure~\ref{fig:circuit_part} also shows the access {\em privileges} of
each task for each region (e.g. read-only,
read-write, reduction).  Each {\tt calc\_new\_currents} task reads 
its owned and ghost node regions and writes its owned wires region
(lines 9-10).  The {\tt distribute\_charge} tasks read their 
owned wires region and perform reductions to their owned 
and ghost node regions (lines 11-12); the reduction 
is a summation, indicated by {\tt +=}.  Finally, 
each {\tt update\_voltages} task reads and writes its owned 
node region (line 13).

\circuitlogicalfig
\circuitphysicalfig

Figure~\ref{fig:circuit_map} shows the physical mapping of the 
simulation onto the Keeneland supercomputer\cite{Keeneland}.  Each
node has 12 CPU cores and 3 GPUs.  Data can be placed in one of four
types of memory:
\begin{itemize} \itemsep1pt \parskip1pt \parsep1pt
\item System Memory - DRAM visible to all CPUs on a node
\item Framebuffer Memory - GPU device memory
\item Zero-Copy Memory - host pinned memory mapped into a GPU's address space
                         and visible to all CPUs and one GPU
\item GASNet Memory\footnote{We refer to it as GASNet memory since the underlying 
implementation of \Realm relies on GASNet's architecture independent RDMA interface.} 
- memory registered on all nodes visible to all CPUs via one-sided 
Infiniband RDMA operations.
\end{itemize}

We discuss three features of Figure~\ref{fig:circuit_map} 
relevant to the design of \Realm.  First, 
Listing~\ref{lst:circuit_code} has the usual sequential semantics, but
the Legion high-level runtime uses the
region and privilege information to discover task parallelism and {\em
map} (assign) each task and its region arguments to the hardware
\cite{Legion12}.  Legion then issues the necessary low-level \Realm
deferred operations (data movement and task launches) to execute the
circuit simulation.  Legion executes mapping tasks which pick the processors
where the tasks execute and the memories where regions are placed.
To keep mapping tasks off the critical path of
execution, these tasks are also deferred.  From \Realm's perspective,
both Legion runtime tasks and the simulation's tasks are client code.
Deferred execution allows the mapping tasks to run and issue commands
for executing the circuit simulation many time-steps in advance of
where application execution is, automatically hiding Legion runtime
task overheads.

Second, throughout the simulation, a %(sometimes stale)
copy of all the ghost nodes resides in GASNet memory.  
Since GASNet memory is not visible to the GPUs, 
local copies of the ghost nodes must be moved 
to either the GPU framebuffer or zero-copy memory and later
scattered back to GASNet memory once the GPU tasks complete.
\Realm achieves this by using events to chain together combinations of 
dependent task and copy operations.

Third, if the {\tt distribute\_charge} tasks are run on the GPU, the
reductions cannot be applied immediately to the overlapping ghost
regions residing in GASNet memory.  \Realm's regions support
the buffering of reduction operations in a GPU-visible
memory and later applying bulk reduction operations from the local-buffer
to the ghost nodes residing in GASNet memory.



