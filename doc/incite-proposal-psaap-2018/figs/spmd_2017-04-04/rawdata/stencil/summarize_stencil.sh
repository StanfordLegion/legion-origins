#!/bin/bash

echo "X Time Perf" > DATA_BASELINE
CNT=2
for i in 2 8 32 128 512
do
  AVG_TIME=`grep time baseline/out_"$i"x* | awk '{print $7 * 50}'`
  PERF=`ruby -e "puts ((40000 ** 2)/$AVG_TIME/1000000.0)"`
  echo "$CNT $AVG_TIME $PERF" >> DATA_BASELINE
  CNT=$((CNT + 2))
done

echo "X Time Perf" > DATA_REGENT
CNT=1
for i in 1 2 4 8 16 32 64 128 256 512
do
  AVG_TIME=`../summarize.py spmd8/out_"$i"x* | awk '{print $4}' | avg.rb`
  PERF=`ruby -e "puts ((40000 ** 2)/$AVG_TIME/1000000.0)"`
  echo "$CNT $AVG_TIME $PERF" >> DATA_REGENT
  CNT=$((CNT + 1))
done

echo "X Time Perf" > DATA_REGENT_NO_SPMD
CNT=1
for i in 1 2 4 8 16 32 64
do
  AVG_TIME=`../summarize.py none/out_"$i"x* | awk '{print $4}'`
  PERF=`ruby -e "puts ((40000 ** 2)/$AVG_TIME/1000000.0)"`
  echo "$CNT $AVG_TIME $PERF" >> DATA_REGENT_NO_SPMD
  CNT=$((CNT + 1))
done
