import matplotlib
matplotlib.use('PDF')
import matplotlib.pyplot as plt
from matplotlib.mlab import csv2rec
import numpy as np

tableau1 = (0.968,0.714,0.824)
tableau2 = (0.882,0.478,0.470)
tableau3 = (0.565,0.663,0.792)
tableau4 = (0.635,0.635,0.635)
tableau5 = (0.678,0.545,0.788)
tableau6 = (1.000,0.620,0.290)
tableau7 = (0.859,0.859,0.553)
tableau8 = (0.769,0.612,0.580)
tableau9 = (0.478,0.757,0.424)
tableau10= (0.427,0.800,0.855)
tableau11= (0.929,0.592,0.792)
tableau12= (0.929,0.400,0.364)
tableau13= (0.447,0.620,0.808)
tableau14= (0.780,0.780,0.780)
tableau15= (0.773,0.690,0.835)
tableau16= (0.882,0.616,0.353)
tableau17= (0.804,0.800,0.365)
tableau18= (0.659,0.471,0.431)
tableau18= (0.404,0.749,0.361)
tableau19= (0.137,0.122,0.125)

# matplotlib.rcParams["font.family"] = "STIXGeneral"
# matplotlib.rcParams["mathtext.fontset"] = "stix"
matplotlib.rcParams["mathtext.fontset"] = "stixsans"

matplotlib.rc('xtick', labelsize=16)
matplotlib.rc('ytick', labelsize=16)

fig = plt.figure(figsize=(8,6))
ax = fig.add_subplot(111)
plt.subplots_adjust(bottom=0.15)

ax.spines['top'].set_linewidth(1.5)
ax.spines['bottom'].set_linewidth(1.5)
ax.spines['left'].set_linewidth(1.5)
ax.spines['right'].set_linewidth(1.5)
ax.tick_params(axis='x', width=1)
ax.tick_params(axis='y', width=1)

data = csv2rec("data.csv")
plt.semilogx(basex=2)
plt.plot(data.nodes, data.regent/1e3, '-', color=tableau12, marker='o', linewidth=1.5, markersize=15, markeredgewidth=0, label="Regent (CPU)")
plt.plot(data.nodes, data.mpi/1e3, '-', color=tableau13, marker='s', linewidth=1.5, markersize=15, markeredgewidth=0, label="MPI")
plt.xticks(data.nodes, data.nodes, rotation=30)
plt.xlim(1/1.2, 1024*1.2)
plt.ylim(0, 1.6)

plt.xlabel('Nodes', fontsize=18)
plt.ylabel('Throughput Per Node ($10^6$ Cells/s)', fontsize=18)
# plt.title(r'Weak Scaling ($512^3$ Cells)', fontsize=20)
plt.legend(loc='lower left', ncol=1, fontsize=16)
# plt.grid(True)
plt.savefig("miniaero.pdf")
plt.show()
