\documentclass[11pt]{article}

\usepackage[a4paper,includeheadfoot,top=1cm,bottom=2cm,left=2cm,right=2cm]{geometry}
\usepackage{here}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{gnuplottex}
\usepackage[colorlinks=true,urlcolor=blue]{hyperref}
\usepackage{authblk}
\usepackage{caption}

\title{This is the title of the Proposal}
\author[1]{Principal Investigator}
\author[2]{First Collaborator}
\author[1,2]{Last Collaborator}
\affil[1]{Affiliation of the PI}
\affil[2]{Other affiliation}

\date{}             

% define section and subsection numbering: hidden
\renewcommand\thesection{}
\renewcommand\thesubsection{}
% define secondary label of items in lists
\renewcommand{\labelitemii}{$\star$}

\begin{document}
\maketitle

\begin{abstract}
This is the abstract of the proposal: in this document we provide a template for 
CSCS Production Project Submission with guidelines, focusing in particular on 
sections \textbf{Representative benchmarks and Scaling}, \textbf{Performance Analysis} 
and \textbf{Resource Justification}.
\end{abstract}

\section{Background and Significance}
The project proposal should be no longer than \textbf{10 A4 pages} including graphs and references, 
and must contain the following information:
\begin{itemize}
 \item Abstract
 \item Background and significance
 \item Scientific goals and objectives
 \item Research methods, algorithms and code parallelization (including memory requirements) 
 \item Representative benchmarks and scaling
 \item Performance analysis
 \item Resource justification (annual node hours and disk space)
 \begin{itemize}
  \item Visualization, pre- and post-processing needs
  \item Development and debugging requirements
 \end{itemize}
 \item Project plan: tasks and milestones
 \item Previous results 
\end{itemize}
Please follow the structure used in this template, which reflects the requirements reported on 
\href{http://www.cscs.ch/user_lab/allocation_schemes/submission100/index.html}{Production Projects Submission}. 

\section{Scientific Goals and Objectives}
\ldots 

\section{Research Methods, Algorithms and Code Parallelization}
Please insert in this section a description of the methods and algorithms of the code adopted for 
your computational study. You should include a brief list of the main scientific libraries employed 
and a description of the parallelization approach, with specific memory requirements as well.

In general community codes publish this information on their web sites. For instance, 
the \href{www.cp2k.org}{CP2K home page} reports that \emph{CP2K is written in Fortran 2003 
and can be run efficiently in parallel using a combination of multi-threading, MPI, and CUDA}.

If you don't use a community code, please report if the code employs MPI distributed parallelism or 
hybrid MPI/OpenMP, which type of MPI communication has been implemented and if it makes use of 
shared memory parallelism, GPU accelerators or OpenACC/CUDA, with specific memory requirements as well.

\section{Representative Benchmarks and Scaling}
Please report in this section the results of the mandatory strong scaling tests performed with the 
selected code: you should report scaling data and plot for every representative system of your project. 
The goal is to choose the most efficient job sizes to run the performance analysis on your representative 
systems. 

You should select meaningful job sizes to simulate the representative systems, compatible with 
reasonably short runtimes: the lowest number of nodes is determined in general by memory and wall time 
constraints, while the highest node counts should let you identify the job size at which you 
reach $\sim 50\%$ of the parallel efficiency with respect to ideal scaling. 
If possible, please provide weak scaling tests as well, in addition to the required strong scaling data. 

Table~\ref{table:scaling} reports the wall time in seconds and the corresponding speed-up for a single 
representative system. Figure~\ref{fig:scaling} shows the scaling plot: we started the scaling test on 
2 nodes, taking this runtime as a reference to compute the speed-up of larger job sizes. 
We then proceed doubling the number of nodes and checking the corresponding speed-up, until we are 
sure to have reached the $\sim 50\%$ limit in parallel efficiency (16 nodes in the small example below).

\begin{table}[H]
 \begin{minipage}{0.35\linewidth}
 \centering
  \begin{tabular}{@{}*3{r}@{}}
   \hline \\
   Nodes & Wall time (s) & Speed-up \\
   \\ \hline \hline \\
    2 & 1022 & 1.00 \\ 
    4 &  476 & 2.15 \\
    8 &  339 & 3.01 \\
   16 &  210 & 4.87 \\
   32 &  206 & 4.96 \\
   \\ \hline
  \end{tabular}
  \caption{Wall time and speed-up}
  \label{table:scaling}
 \end{minipage}
 \hfill
 \begin{minipage}{0.65\linewidth}
  \centering
  \begin{gnuplot}
   % gnuplot script file to create a scalability plot
   %set terminal latex
   set xlabel "Number of Nodes"
   set xrange [0:34]
   set xtics 0,4,32
   set ylabel "Speed-up" offset 17,.5
   set ytics border offset 5.5,0 2,2,14
   #unset ytics 
   #set y2label "Speed-up" offset -15
   #set y2tics border mirror offset -5.5,0 2,2,14
   # plot legend
   set key center top
   set size 0.85,0.85
   plot "-" w linespoints linewidth 2 title "Representative benchmark", x/2 w lines lt 3 title "Ideal Speed-up"
    2 1.00 
    4 2.15
    8 3.01
   16 4.87 
   32 4.96
  \end{gnuplot}
  \captionof{figure}{Strong scaling vs. ideal speed-up}
  \label{fig:scaling}
 \end{minipage}
\end{table}

\section{Performance Analysis}
Please report here a summary of the performance analysis conducted on each representative system 
at the optimal job sizes selected in the previous section, which reached $\sim 50\%$ parallel efficiency: 
you should run using the executable instrumented with \emph{Cray Performance and Analysis Tools (CrayPAT)}.
 
The performance analysis job will create a report text file with extension \verb!.rpt! 
and a larger apprentice binary file with extension \verb!.ap2!. 
Please make these two files available for inspection, either by enclosing them at submission time or 
indicating where they can be accessed for reading under your \verb!$HOME! (not \verb!$SCRATCH!).  

The summary data can be extracted using the following commands on the report text file:
\begin{verbatim}
 grep -A 7 CrayPat/X <report>.rpt
 grep \|USER <report>.rpt
 grep \|MPI <report>.rpt
 grep \|Total <report>.rpt
\end{verbatim}

The summary should look like the example below:
\begin{verbatim}
 CrayPat/X:  Version 6.4.5 Revision 87dd5b8  01/23/17 15:37:24
 Experiment:                   lite  lite/gpu     
 Number of PEs (MPI ranks):      16
 Numbers of PEs per Node:         1  PE on each of  16  Nodes
 Numbers of Threads per PE:   1,114
 Number of Cores per Socket:     12
 Execution start time:  Tue Mar 28 15:15:55 2017
 System name and speed:  nid02294  2601 MHz (approx)

 |  59.2% | 1,236.266484 | 110.728787 |  8.8% |          1.0 |USER

 |  31.8% |   664.415775 |         -- |    -- |     35,648.0 |MPI_SYNC
 |   2.8% |    58.511390 |         -- |    -- | 14,458,788.1 |MPI

 100.0% | 2,086.808412 |         -- |    -- | 18,723,148.8 |Total
 100.0% | 1.89 |  105,946 |   287.62 | 75,246 |Total
 56.092035 | 3,764.356845 |  67.110363 | 62,097,047.0 |    63.57 |Total
 0.151159 | 2.949494 |  19.512511 | 74,334.0 |    41.61 |Total
\end{verbatim}

The first command extracts general information on the job, then we extract the statistics of USER and MPI functions; 
the last command reports the Total of each Table (functions, accelerator, read and write statistics).

\section{Resource Justification}
The request of the annual amount of node hours should be clearly linked with the node hours used 
by the representative benchmarks: the number of node hours consumed by a simulation is computed 
multiplying the number of nodes by the wall time expressed in hours. 
CrayPAT adds an overhead to the wall time, therefore you cannot use that timing to justify your request.

In the small example used throughout this template, the optimal job size of the representative 
benchmark is 16 nodes and the corresponding wall time reported in Table~\ref{table:scaling} is 
210 s, which is equivalent to $\sim 0.933$ node hours, as a result of the following product: 
$$
0.933 \mbox{ node hours } = 16 \mbox { nodes } \times \frac{210 s}{3600 \frac{s}{hour}}
$$
The benchmark is short and represents in general a small number of iterations (cycles, timesteps or 
an equivalent measure), while in a real production simulation we will need to extend it.

Therefore we will estimate how many iterations should be necessary to complete a simulation 
in production. Furthermore, the project plan might contain multiple tasks, each of them requiring 
several sets of simulations to complete: the annual resource request will sum up the corresponding 
node hours obtained multiplying all the factors reported in Table~\ref{table:resource_request}. 
\begin{table}[H]
 \begin{center}
  \begin{tabular}{@{}*3{r}@{}}
   \hline \hline
   & First task & Second task \\ 
   \hline \hline
   Simulations per task & 2 & 4 \\
   Iterations per simulation & 5000 & 10000 \\
   node hours per iteration & 0.933 & 0.933 \\
   Total node hours & 9333 & 37333 \\
   \hline \hline
  \end{tabular}
 \end{center}
 \caption{Justification of the resource request}
 \label{table:resource_request}
\end{table}
The resource request of this small example will sum up to a total of 46666 annual node hours, 
summing the node hours estimated to complete the first and the second task of the project 
(Table~\ref{table:resource_request}), in agreement with the \textbf{Project Plan}.

You should present in this section your request for long term storage as well, explaining your needs 
based on the I/O pattern of the representative benchmarks reported by the performance analysis. 
 
\subsection{Visualization, pre- and post-processing}
Please insert in this subsection the optional requirements for visualization, pre- and post-processing.

\subsection{Development and debugging}
Please insert in this subsection the optional requirements for development and debugging. 

\section{Project Plan: Tasks and Milestones}
Please report tasks and milestones of your project. When describing your project development, 
aside from laying out the tasks that take you from beginning to end, please mark key dates as well.  
An easy way to do this graphically is through the use of a Gantt chart: milestones charts  
are also useful to determine more accurately whether or not a project is on schedule. 

\section{Results from Previous Allocations}
Please list here your past requests, granted projects and used allocations (if applicable). 
You should also include a list of research publications that resulted from past allocations.

\section*{References}
\ldots
\bibliographystyle{abbrv}
\bibliography{main}

\end{document}
