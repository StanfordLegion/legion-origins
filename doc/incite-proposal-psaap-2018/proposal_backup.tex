\documentclass[11pt]{article}

\usepackage[a4paper,includeheadfoot,top=1cm,bottom=2cm,left=2cm,right=2cm]{geometry}
\usepackage{microtype}
\usepackage{amsmath}
\usepackage{here}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage[colorlinks=true,urlcolor=blue]{hyperref}
\usepackage{authblk}
\usepackage{caption}
\usepackage{subfig}

\title{Scaling the Legion Programming System}
\author[1]{Alex Aiken}
%% \author[2]{First Collaborator}
%% \author[1,2]{Last Collaborator}
\affil[1]{Stanford University}
%% \affil[2]{Other affiliation}

\date{}

% define section and subsection numbering: hidden
\renewcommand\thesection{}
\renewcommand\thesubsection{}
% define secondary label of items in lists
\renewcommand{\labelitemii}{$\star$}

\begin{document}
\maketitle

\begin{Summary}

Legion is an exa-scale ready task-based parallel programming system for
high-performance computing on supercomputers. With a goal focused on reducing
programmer effort, Legion exposes a programming model that helps application
developers achieve effective and efficient use of heterogenous extreme-scale
systems. In the past few years, Legion has gained significant traction in the
computing community, and it has been embraced by a broad spectrum of researchers
in national laboratories, academia and industry. This adoption has been
successful in part because of collaborations with domain experts to develop
large-scale cutting-edge technologies such as Soleil-X. In this proposal, we are 
requesting access to the \textit{SUMMIT} supercomputer to:

\begin{itemize}
\item to further the co-design efforts with Soleil-X, 
\item to evaluate and ensure the readines of Legion for Soleil-X, and
\item to develop, test and deploy Power9-Volta specific optimization in the
Legion programming system.
\end{itemize}
\end{abstract}

\section{Background and Significance}

Modern supercomputers are increasingly hierarchical and heterogeneous.
In most current programming models, programmers are
responsible for data movement between and within nodes of the machine,
interfacing with at least one of a variety of on-node programming models,
and directly managing and programming accelerators (typically GPUs). As
machines become increasingly complex, the variation in communication
latencies also increases, making it ever more important to overlap
communication with computation, compounding the challenges that come
from combining traditional intra- and inter-node programming
models. In addition, the upcoming generation of machines is expected
to demonstrate increased diversity, in that different machines employ
different combinations of heterogeneous processors and
non-uniformity in memories. Writing portable and performant codes under these conditions
with traditional programming models requires a heroic amount of
effort, and in practice the prohibitive costs lead to sacrifices in
one or more of the performance, portability, or maintainability of the
code.

Legion~\cite{Legion12} is a general purpose, task-based programming
model for contemporary and future heterogeneous supercomputers that
has already been demonstrated to provide very significant speedups at scale (e.g., more than 3X in a production application \cite{LegionFields14}), especially for complex codes, with more portable
and maintainable code.  In contrast to conventional
programming models, Legion provides sequential semantics; that is, a
program can be understood to behave as if it executes sequentially. In
this manner Legion avoids by construction the various and well-known
pitfalls of parallel programming: data races, deadlocks,
etc. Internally, the Legion implementation is responsible for
scheduling the execution of the application asynchronously and
efficiently, and for managing all data movement, both intra- and
inter-node. These details are
hidden from the programmer, allowing the application
developer to focus on the algorithm to be implemented
and not the details of asynchronous, distributed execution on the
machine.

Such results are possible because Legion supports a rich data model
that enables the Legion implementation to perform a number of
aggressive optimizations. Programmers
must say more in Legion about how their program uses its data,
but in return programmers get improved portability and performance.
Legion's optimizations can be performed either at runtime by the current
dynamic runtime implementation in C++, or at compile time by the compiler 
for the Regent
programming language~\cite{Regent15}. The latter approach also
provides the option to use automatic generation of high-performance
kernels for a variety of kinds of processors including auto-vectorized code
for CPUs, Intel Xeon Phi, and NVIDIA GPUs.

This proposal focuses on accelerating the development of
Legion and improving its readiness for use by production users of
high-performance scientific codes. In the development of any large
software project, there are a number of tasks required to achieve a
mature system, ranging from novel research to software
engineering. Many of these activities must be tested at scale and
thus require allocation of time on a large-scale
heterogeneous supercomputer to make progress.  A particular challenge
for the Legion project is to support multiple, different machines; because
Piz Daint is currently the best machine of its kind, we are very interested
in having Piz Daint be one of those machines.

\section{Scientific Goals and Objectives}

Note that this proposal is not for a specific code or class of
algorithms. Instead Legion represents an entirely new class of
programming systems for high-performance scientific computing, and as
such, aims to be useful for a wide variety of high-performance
scientific applications.

Our general goals, which are outlined in more detail
below, are broadly focused around improving the quality of the system,
and ensuring that a wide variety of applications are able to achieve
high-performance and portability with reduced programmer effort.

Specific scientific goals include charaterizing the scalability and performance of Legion on a wide variety of applications.  Some of the specific measurements we are interested in and typically report in publications are:
\begin{itemize}
\item Parallel efficiency over a wide range of scales for a broad collection of Legion programs.
\item  The impact of novel features of Legion, and improvements to those features, on programmability and performance.
\item The effectiveness of Legion at latency hiding---the abiity to extract parallelism to keep processing and communiation units busy.
\item Comparisons between Legion implementations and other implementations of high performance codes.
\end{itemize}

\section{Research Methods}

The Legion programming model is a significant departure from the state of
practice with MPI and MPI+X and thus it may be helpful to give an overview
of the architecture of the Legion programming system. The system
includes the following components:

\begin{itemize}
\item Legion itself refers to the C++ runtime system, the centerpiece
  of our programming system.
\item Regent is a programming language that implements the Legion
  programming model and provides an aggressive optimizing compiler
  that enables efficient kernel generation and improved scalability.
  The Regent compiler generates code for the Legion C++ runtime.
\item Realm \cite{Realm14} is a low-level portability layer used in the Legion runtime.
\item Legion Prof is a performance profiler for Legion applications.
\item Legion Spy provides visualizations of the potential parallel
  execution of Legion applications.
\end{itemize}

Generally speaking, applications of the Legion programming system are
written in either C++ (using the runtime API) or in the Regent
programming language. Legion programs consist of {\em tasks}, which are the
fundamental units of parallel execution. A task might run on a
CPU core, or on a set of CPU cores (via OpenMP), or on a GPU. In C++,
the code for these tasks must be provided by the user. For example, 
to target NVIDIA GPUs, the user must supply a hand-written CUDA
kernel. Legion itself is agnostic to the manner in which tasks are
written. The user is free to use portability layers such as Trilinos
Kokkos~\cite{Kokkos}, for example, to generate GPU kernels if they
wish. Users of the Regent programming language can supply hand-written
kernels as in Legion, or can opt to use Regent's built-in support for
generating high-performance kernels for CPUs or GPUs.

Tasks take collection arguments called {\em regions}.  By analyzing 
task arugments, Legion dynamically constructs a directed acyclic
graph (DAG) of task dependencies. These dependencies ensure that tasks
execute in a manner consistent with the original sequential ordering
of the program. Tasks execute asynchronously as soon as all
dependencies are met. In general Legion does not possess the entire
task graph at once; tasks may be generated dynamically (to enable
input-dependent behavior) and the Legion runtime schedules tasks as
they are issued by the application.

The dependence graph of tasks also determines when communication
occurs in a distributed execution of the application. If a task $A$ reads
a region written by task $B$, then if $A$ and $B$ are scheduled for execution
on different nodes,  communication is required to move the results produced by $B$
to where $A$ can read them. The Legion
programming model captures sufficient information for the system to
completely characterize task dependencies and infer needed communication.
Legion schedules such communication asynchronously, and without blocking any
independent tasks that might be executing concurrently.

The internal analysis to compute the task dependence graph is
itself asynchronous and is performed concurrently with the execution
of the application. As long as the overall throughput of the runtime
analysis is sufficient, the visible overhead (i.e.\ the increase in
overall wall-clock time) of the system is quite small. In many cases,
and most especially in complex applications with large amounts of task
parallelism, Legion can easily compensate for any overhead through its
improved scheduling and often significantly improves upon the
state of the art.

Finally, note that in Legion all significant performance decisions can
be controlled by the application via Legion's \emph{mapping} interface. For
example, the application can choose to send some tasks to CPUs and others to
GPUs, and can control (if desired) the exact assignment of tasks to
processors. This feature is optional and Legion will select (via
heuristics) a best-effort mapping if none is supplied. However, many
users welcome this flexibility as it ensures they will never find
themselves in a position where they know how they want Legion to
execute their program, but Legion can't do it. Legion will always
provide full control over any scheduling decisions that are relevant
for performance.

\section{Development Outline}

In this section we outline our development plans for Legion.

\subsection{Current Status}

Legion has been demonstrated on a variety of full-scale and mini-app codes
which use the capabilities of the system in different ways. In this section we
summarize the salient performance and scalability results from our
experiments.

\input{e1_s3d}

S3D is a full-scale code for the direct numerical simulation (DNS) of
turbulent combustion with complex, multicomponent chemistry. S3D is a
mature code whose history spans over 30 years of development. In
collaboration with Jacqueline H. Chen and the S3D team at Sandia
National Laboratories, we developed a Legion implementation of
S3D. The original code was written in Fortran with MPI. (A version
written in OpenACC is also available, but has limited support for the
chemical mechanisms of interest in this study, and thus was not
considered in our evaluation.) Figure~\ref{fig:s3d-weak} shows a
comparison between Legion and the MPI implementation. Legion enabled
the first production simulation of a primary reference fuel (PRF)
capable of resolving micro-meter-scale turbulent flame
structures. Legion was 3.95X faster on a single node than MPI and
provides equivalent parallel efficiency through 4096 nodes. Note that
to provide equivalent parallel efficiency, Legion also reduces
the overhead of the network by 4X compared to MPI, due to the increase
in per-node throughput.

Figure~\ref{fig:s3d-strong} shows wall-clock time and throughput per
node while strong scaling the Legion S3D implementation at the
following node counts: 1296, 1728, 2596, and 3888. (The S3D code
requires that the grid size divide evenly among the nodes, limiting
the number of allowable node counts.) From 1728 to 3888 nodes the
parallel efficiency of the Legion implementation dropped only 11\%
while providing an increase in simulation speed of 2X, making it
profitable to scale the simulation in this way.

These S3D experiments were conducted on Piz Daint (under project g84)
prior to the November 2016 modifications. All of the subsequent
experiments were performed as a part of our small development project
(d51) following the November 2016 upgrade to the machine.

\input{e2_soleil}

Soleil-X is a multi-physics application that simulates turbulent fluid
dynamics in the presence of macroscopic particles heated by a
radiation source. The code uses a structured grid for the fluid
simulation with unstructured bags of particles for each subgrid, and
uses an optically-thin radiation source. Soleil-X is written in a DSL
which compiles to Regent and uses Regent's support for automatic
generation of GPU kernels to leverage the GPUs on Piz
Daint. Figure~\ref{fig:soleil-weak} shows the weak scaling performance
of the fluid component only. Note that the scale is log-log 
to make it possible to meaningfully compare CPU and GPU results on the
same chart. Single-node GPU performance is 19.5X better than the CPU
(even using all available cores on a node). Efficiency of the GPU code
drops to 50\% at 64 nodes, demonstrating an opportunity for further
improvement in the system. While all data movement in Legion is
asynchronous, Legion does not currently use GPUDirect for GPU to GPU
data movement which results in some loss of efficiency at larger
scales.

Figure~\ref{fig:soleil-strong} shows CPU-only strong scaling of the
fluid simulation with and without particles. (The Regent GPU code
generator requires some extensions for this code to run on GPUs.)
Legion currently achieves 53\% efficiency at 128 nodes on the fluid-only
simulation, and 47\% when particles are enabled.

\input{e3_miniapps}
\input{e4_solver}

We have written Legion implementations of a number of mini-apps which
demonstrate various styles of computation not covered by our
full-scale applications: a circuit simulation on an unstructured graph
(Figure~\ref{fig:circuit-weak}), an aerodynamics simulation on a 3D
unstructured mesh (Figure~\ref{fig:miniaero-weak}), a Lagrangian
hydrodynamics simulation on a 2D unstructured mesh
(Figure~\ref{fig:pennant-weak}), and a stencil on a 2D structured grid
(Figure~\ref{fig:stencil-weak}). These applications are written in
Regent, which has allowed us to quickly validate and test our
implementations. Three of the four require extensions to the Regent
GPU code generator and thus were tested on CPUs; this work is
ongoing. The efficiency of Circuit
(which we tested on GPUs) drops below 50\% at 512 nodes, again
demonstrating the need for further work on Legion's DMA
subsystem. These tests can only be conducted at scale as the
efficiency loss only becomes visible at large node counts.

Finally, we are exploring the use of Legion for a fast direct solver
for hierarchically off-diagonal low-rank (HODLR)
matrices. Figure~\ref{fig:solver} shows weak scaling results using
CPUs. The results, although preliminary, are encouraging. Over the
last six months we have achieved a substantial increase in the
efficiency of the implementation. (In the figure, the old results are
in green, the new results in yellow, and the theoretical maximum in
red.) A notable difference between this and the other applications
describe above is
that the solver employs a tree-shaped form of parallelism, where work
is performed first at the leaves of the tree and then results combined
at each inner node going up the tree.

\subsection{Algorithms and Implementation}

This proposal is not for a specific application or class of
algorithms. Instead, we are interested in ensuring the readiness of
Legion for a wide variety of applications. As demonstrated above, we
have performed initial evaluations on a broad set of applications. We
are interested in extending this set of applications further, and on
working to improve the Legion implementation where there are currently
shortcomings impacting the of performance on existing applications
(such as in the efficiency of GPU to GPU data transfers).

\subsection{Programming and Parallelization}

A user of Legion is responsible for decomposing their algorithms into
tasks (functions eligible for parallel execution) and their data
structures into regions (collections that can be partitioned and
distributed around the machine). Legion achieves a parallel execution
of the program by analyzing tasks for dependencies; the dependencies
ensure that tasks execute in a manner consistent with the original
program order.

A challenge in the implementation of a Legion-like system is that the
analysis of $N$ tasks requires $\mathcal{O}(N)$ time. In
a straightforward implementation, this analysis is performed on a
single node and thus becomes a bottleneck as $N$ grows with the number
of nodes while time per task typically stays constant (under weak
scaling) or decreases (under strong scaling). A critical innovation in
our design is an optimization called \emph{control replication} which
permits this analysis to be performed in a distributed manner,
reducing the analysis cost to $\mathcal{O}(1)$ (assuming that the
number of tasks is proportional to the number of nodes used in
execution of the program). We have an initial prototype implementation
of control replication which is available in the Regent compiler, and
which was used in the Regent-based experiments
(Figures~\ref{fig:soleil} and \ref{fig:miniapps}) and has been
demonstrated to be effective. A major goal for this proposal is using
Piz Daint in the development and scaling of this optimization
in the dynamic C++ runtime itself.

\subsection{Project Plan: Tasks and Milestones}

In this proposal we are interested in improving Legion's readiness for
potential use by production codes. This effort consists of two major
components.

First, it is critical in the development of any significant
programming system to test frequently and at an appropriate scale. As
is well-known in the discipline of software engineering, the cost of
fixing a bug is proportional to the duration of
time that bug survives in the software. Bugs are significantly less
expensive to find during the initial implementation effort than
afterwards.  The Legion project currently uses a continuous integration
framework that runs our test suite on every single commit to the
Legion repository. However, as suggested above, certain classes of
(correctness or performance) bugs only reproduce at large node
counts. Our continuous integration framework only has access to a
small number of nodes. We propose to regularly run a set of tests at
scale on Piz Daint. We will design the tests to minimize resource
usage and avoid impacting any other users of Piz Daint. (The proposed
resource requirements are described in the section on Resource
Justification, below.)

Second, we have identified a number of features of the Legion
programming system which are of strategic importance to existing and
potential production users of Legion. These consist of:

\begin{enumerate}
\item Dynamic control replication
\item Projection functors
\item DMA improvements
\item Resilience
\end{enumerate}

Each item is described below:

\emph{Dynamic control replication}: We have demonstrated the
effectiveness of a static, compile-time implementation of control
replication as an optimization in the Regent compiler. We are
interested in adding this optimization to the Legion runtime itself,
for two reasons: First, not all Legion codes are written in Regent and
we would like C++ Legion programs to be able to take advantage of
control replication. Second, a dynamic runtime system simply has more
information available to it (e.g.\ when program behavior is
input-dependent) and can thus be expected to optimize programs that
are too dynamic for the use of static control replication. We believe the
availability of dynamic control replication will have a substantial
impact on the Legion ecosystem.

\emph{Projection functors}: This is an independent, but related item
to control replication. Control replication is most effective when the
compiler or runtime can analyze in constant time the communication
patterns of the application. To make this analysis tractable
(i.e.\ without resorting to enumerating all $N$ tasks to be executed),
it is necessary to use a more sophisticated analysis of the tasks
launched, and in particular, the subsets of data elements passed to
tasks. Projection functors will enable these sets of data elements to
be named in a way that is amenable to analysis both at runtime and
compile-time, and when used will improve the efficiency of both
control replicated and non-control replicated Legion code.

\emph{DMA improvements}: While the Legion DMA (Direct Memory Access)
system is mature, the feature to support direct GPU to GPU data
transfers is relatively new and, as noted above, causes a loss of
efficiency in GPU codes at high node counts. We will perform further
tuning of the system to improve the throughput and latency of these
communication patterns in Legion.

\emph{Resilience}: The Legion programming model is ideally situated to
provide resilience with minimal impact on application codes. If a task
fails (either due to a temporary or permanent failure), Legion
understands the complete dependence graph of the program, including
all data movement into and out of the task, and can reissue that task
on a different processor and reroute data movement as necessary so
that the computation is able to proceed undisturbed, even in the
absence of explicit application-level checkpointing.

\section{Resource Justification}

In this proposal we request a resource allocation of 320k node-hours
and 2 TB of storage to support the activities above. The
allocation is further subdivided between the following three
components:

\begin{enumerate}
\item Development: 130k node-hours
\item Testing: 110k node-hours
\item Experiments: 80k node-hours
\end{enumerate}

\emph{Development}: The initial development of each feature described
above will require debugging the feature at scale. This process is
iterative, and requires in general testing at 1 node, then 2, 4,
etc. up to 4096 nodes to ensure that the feature works up to the full
scale of the machine. At each node count it is expected that some
effort will be involved, requiring multiple runs. Even though each run
is relatively short (less than an hour), in our experience the
aggregate time at each node count is typically about 4 hours of total
machine time. Thus the expected resource requirement for the four
features suggested is: $(1 + 2 + 4 + \ldots + 4096)
\operatorname{nodes} \times 4 \operatorname{hours} \times 4
\operatorname{features} = 130\mathrm{k}$ node-hours for the proposed
features.  These debugging runs are also sufficient to gather the data
necessary for research papers on Legion and Legion applications.

\emph{Testing}: As described above, it is important to regularly test
Legion at scale to ensure protection against bugs that only reproduce
at large node counts. We believe that running tests once a week should
be sufficient for this purpose. The test suite is expected to run for
2 hours at 1024 nodes. Thus the expected annual resource requirement
is: $1024 \operatorname{nodes} \times 2 \operatorname{hours} \times 52 \operatorname{runs/year} = 106\mathrm{k}$
which we round to 110k node-hours.

\emph{Experiments}: To ensure the readiness of Legion for
production applications it is necessary to conduct performance
experiments with a representative set of applications. These
experiments do not collect any actual production data and thus need
only run as long as necessary to confirm the application's steady-state 
performance. In practice these performance experiments also involve a
certain amount of debugging to resolve lingering issues. In our
experience, full-scale performance experiments typically require about
20k node-hours per application tested. We propose testing with the
following applications, for a total of 80k node-hours.

\begin{itemize}
\item S3D: We expect that when dynamic control replication is ready,
  we will want to rewrite the S3D Legion implementation to use it, and
  verify its performance.
\item Soleil-X: We are interested in adding a discrete ordinates
  method (DOM) radiation solver to the code. Separately, we are
  considering the addition of particle-particle collisions. These
  features would be evaluated separately for scalability (thus
  requiring two performance experiments).
\item Fast solvers: We are interested in the further tuning of the
  fast solver for HODLR matrices described above, as well as possible
  integrations into larger applications.
\end{itemize}

Regarding disk usage: Legion itself does not require substantial use
of disk, and most of the applications above either do not require or
can be configured to run in a mode that does not require substantial
disk usage. The only exception is that the log files generated for use
with the Legion Prof application profiler can become on the order of
GBs as they must track start and end times for every executed task. In
some cases it can be useful to keep the log files for some time while
performing analysis, and thus it may not be appropriate to use scratch
space to store them in all cases. We believe 2 TB of storage should be
sufficient for this purpose.

\section{Infrastructural Requirements}

We do not have any specific infrastructural needs.

Legion is very flexible system and is designed to adapt well to a
variety of heterogeneous hardware. In our experience so far on Piz
Daint the programming system has worked quite well with the available
hardware and software stack.

\section{Duration of the Project}

% FIXME (Elliott): Should we make the duration 3 years? I think
% resilience may require more time to fully tune

This proposal is for a one year project. 

\bibliographystyle{abbrv}
\bibliography{bibliography}

\end{document}
