
#ifndef __LEGION_MEMTRACE__
#define __LEGION_MEMTRACE__

void start_memory_trace(void);
void stop_memory_trace(void);
void dump_memory_trace(void);

void start_mmap_trace(void);
void stop_mmap_trace(void);

#endif

