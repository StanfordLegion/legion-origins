
#include <map>
#include <dlfcn.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <execinfo.h>
#include <sys/mman.h>
#include "memtrace.h"

static void* (*myfn_malloc)(size_t size);
static void  (*myfn_free)(void *ptr);
static void* (*myfn_realloc)(void *ptr, size_t size);
static void* (*myfn_mmap)(void *addr, size_t length, int prot, int flags, int file, off_t off);
static void* (*myfn_mmap2)(void *addr, size_t length, int prot, int flags, int file, off_t off);
static void* (*myfn_mmap64)(void *addr, size_t length, int prot, int flags, int file, off64_t off);
static int   (*myfn_brk)(void *addr);
static void* (*myfn_sbrk)(intptr_t increment);

static volatile bool initialized = false;
static volatile bool tracing = true;
static volatile bool collecting = true;
static volatile bool mmap_tracing = false;
static pthread_mutex_t mem_lock = PTHREAD_MUTEX_INITIALIZER;

__thread volatile bool in_trace = false;

static void init(void)
{
  myfn_malloc   = (void* (*)(size_t))        dlsym(RTLD_NEXT, "malloc");
  myfn_free     = (void (*)(void*))          dlsym(RTLD_NEXT, "free");
  myfn_realloc  = (void* (*)(void*,size_t))  dlsym(RTLD_NEXT, "realloc");
  myfn_mmap     = (void* (*)(void*,size_t,int,int,int,off_t))dlsym(RTLD_NEXT, "mmap");
  myfn_mmap2    = (void* (*)(void*,size_t,int,int,int,off_t))dlsym(RTLD_NEXT, "mmap2");
  myfn_mmap64   = (void* (*)(void*,size_t,int,int,int,off64_t))dlsym(RTLD_NEXT, "mmap64");
  myfn_brk      = (int (*)(void*))           dlsym(RTLD_NEXT, "brk");
  myfn_sbrk     = (void* (*)(intptr_t))      dlsym(RTLD_NEXT, "sbrk");
  initialized = true;
}

struct Allocation {
public:
  Allocation(void) 
    : name(NULL), size(0) { }
  Allocation(char *n, size_t s)
    : name(n), size(s) { }
public:
  char *name;
  size_t size;
};
std::map<void*,Allocation> memory_allocations;

static void handle_alloc_ptr(Allocation &ref, size_t size)
{ 
  void *bt[256];
  int bt_size = backtrace(bt, 256);
  char **bt_syms = backtrace_symbols(bt, bt_size);
  size_t buffer_size = 1;
  for (int i = 0; i < bt_size; i++)
    buffer_size += (strlen(bt_syms[i]) + 1);
  char *buffer = (char*)malloc(buffer_size);
  int offset = 0;
  for (int i = 0; i < bt_size; i++)
    offset += sprintf(buffer+offset,"%s\n",bt_syms[i]);
  free(bt_syms);
  ref.name = buffer;
  ref.size = size;
}

static void handle_free_ptr(void *ptr)
{
  std::map<void*,Allocation>::iterator finder = memory_allocations.find(ptr);
  if (finder != memory_allocations.end()) {
    free(finder->second.name);
    memory_allocations.erase(finder);
  } 
}

static void report_mmap(const char *name, void *ptr)
{
  pthread_mutex_lock(&mem_lock);
  in_trace = true;
  void *bt[256];
  int bt_size = backtrace(bt, 256);
  char **bt_syms = backtrace_symbols(bt, bt_size);
  size_t buffer_size = 1;
  for (int i = 0; i < bt_size; i++)
    buffer_size += (strlen(bt_syms[i]) + 1);
  char *buffer = (char*)malloc(buffer_size);
  int offset = 0;
  for (int i = 0; i < bt_size; i++)
    offset += sprintf(buffer+offset,"%s\n",bt_syms[i]);
  free(bt_syms);
  fprintf(stdout,"NEW %s at %p\n%s\n\n", name, ptr, buffer);
  fflush(stdout);
  free(buffer);
  in_trace = false;
  pthread_mutex_unlock(&mem_lock);
}

extern "C" {

void* malloc(size_t size)
{
  if (!initialized) {
    pthread_mutex_lock(&mem_lock);
    if (!initialized) {
      init();
    }
    pthread_mutex_unlock(&mem_lock);
  }
  void *result = (*myfn_malloc)(size);
  // Can test without the lock because it is thread local
  if (!in_trace) {
    pthread_mutex_lock(&mem_lock);
    if (tracing) {
      in_trace = true;
      std::map<void*,Allocation>::iterator finder = memory_allocations.find(result);
      if (finder != memory_allocations.end()) {
        fprintf(stderr,"WARNING: allocation %p of size %ld was mysteriously freed!\n%s\n\n",
                finder->first, finder->second.size, finder->second.name);
        fflush(stderr);
        free(finder->second.name);
        memory_allocations.erase(finder);
      }
      Allocation &ref = finder->second;
      pthread_mutex_unlock(&mem_lock);
      handle_alloc_ptr(ref, size); 
      in_trace = false;
    } else {
      pthread_mutex_unlock(&mem_lock);
    }
  }
  return result;
}

void free(void *ptr)
{
  if (!initialized) {
    pthread_mutex_lock(&mem_lock);
    if (!initialized) {
      init();
    }
    pthread_mutex_unlock(&mem_lock);
  }
  if (!in_trace) {
    pthread_mutex_lock(&mem_lock);
    if (collecting) {
      in_trace = true;
      handle_free_ptr(ptr);
      in_trace = false;
    }
    pthread_mutex_unlock(&mem_lock);
  }
  (*myfn_free)(ptr);
}

void* realloc(void *ptr, size_t size)
{
  if (!initialized) {
    pthread_mutex_lock(&mem_lock);
    if (!initialized) {
      init();
    }
    pthread_mutex_unlock(&mem_lock);
  }
  if (!in_trace) {
    pthread_mutex_lock(&mem_lock);
    if (collecting) {
      in_trace = true;
      handle_free_ptr(ptr);
      in_trace = false;
    }
    pthread_mutex_unlock(&mem_lock);
  }
  void *result = (*myfn_realloc)(ptr, size);
  if (!in_trace) {
    pthread_mutex_lock(&mem_lock);
    if (tracing) {
      in_trace = true;
      std::map<void*,Allocation>::iterator finder = memory_allocations.find(result);
      if (finder != memory_allocations.end()) {
        fprintf(stderr,"WARNING: allocation %p of size %ld was mysteriously freed!\n%s\n\n",
                finder->first, finder->second.size, finder->second.name);
        fflush(stderr);
        free(finder->second.name);
        memory_allocations.erase(finder);
      }
      Allocation &ref = finder->second;
      pthread_mutex_unlock(&mem_lock);
      handle_alloc_ptr(ref, size); 
      in_trace = false;
    } else {
      pthread_mutex_unlock(&mem_lock);
    }
  }
  return result;
}

void* mmap(void *addr, size_t length, int prot, int flags, int fd, off_t off)
{
  if (!initialized) {
    pthread_mutex_lock(&mem_lock);
    if (!initialized) {
      init();
    }
    pthread_mutex_unlock(&mem_lock);
  }
  void *result = (*myfn_mmap)(addr, length, prot, flags, fd, off);
  if (mmap_tracing)
    report_mmap("mmap", result);
  return result;
}

void* mmap2(void *addr, size_t length, int prot, int flags, int fd, off_t off)
{
  if (!initialized) {
    pthread_mutex_lock(&mem_lock);
    if (!initialized) {
      init();
    }
    pthread_mutex_unlock(&mem_lock);
  }
  void *result = (*myfn_mmap2)(addr, length, prot, flags, fd, off);
  if (mmap_tracing)
    report_mmap("mmap2", result);
  return result;
}

void* mmap64(void *addr, size_t length, int prot, int flags, int fd, off64_t off)
{
  if (!initialized) {
    pthread_mutex_lock(&mem_lock);
    if (!initialized) {
      init();
    }
    pthread_mutex_unlock(&mem_lock);
  }
  void *result = (*myfn_mmap64)(addr, length, prot, flags, fd, off);
  if (mmap_tracing)
    report_mmap("mmap64", result);
  return result;
}

int brk(void *addr)
{
  if (!initialized) {
    pthread_mutex_lock(&mem_lock);
    if (!initialized) {
      init();
    }
    pthread_mutex_unlock(&mem_lock);
  }
  int result = (*myfn_brk)(addr);
  if (mmap_tracing)
    report_mmap("brk", addr);
  return result;
}

void* sbrk(intptr_t increment)
{
  if (!initialized) {
    pthread_mutex_lock(&mem_lock);
    if (!initialized) {
      init();
    }
    pthread_mutex_unlock(&mem_lock);
  }
  void *result = (*myfn_sbrk)(increment);
  if (mmap_tracing)
    report_mmap("sbrk", result);
  return result;
}

}

void start_memory_trace(void)
{
  fprintf(stdout,"------------------ STARTING MEMORY TRACE -----------------\n");
  fflush(stdout);
  pthread_mutex_lock(&mem_lock);
  tracing = true;
  collecting = true;
  pthread_mutex_unlock(&mem_lock);
}

void stop_memory_trace(void)
{
  pthread_mutex_lock(&mem_lock);
  tracing = false;
  pthread_mutex_unlock(&mem_lock);
  fprintf(stdout,"------------------ STOPPING MEMORY TRACE -----------------\n");
  fflush(stdout);
}

void dump_memory_trace(void)
{
  pthread_mutex_lock(&mem_lock);
  collecting = false;
  pthread_mutex_unlock(&mem_lock);
  in_trace = true;
  for (std::map<void*,Allocation>::const_iterator it = memory_allocations.begin();
        it != memory_allocations.end(); it++)
  {
    fprintf(stderr,"ERROR: allocation at %p of size %ld was not collected!\n%s\n\n",
            it->first, it->second.size, it->second.name);
    free(it->second.name);
  }
  fflush(stderr);
  in_trace = false;
}

void start_mmap_trace(void)
{
  fprintf(stdout,"------------------ STARTING MMAP TRACE -----------------\n");
  fflush(stdout);
  pthread_mutex_lock(&mem_lock);
  mmap_tracing = true;
  pthread_mutex_unlock(&mem_lock);
}

void stop_mmap_trace(void)
{
  pthread_mutex_lock(&mem_lock);
  mmap_tracing = false;
  pthread_mutex_unlock(&mem_lock);
  fprintf(stdout,"------------------ STOPPING MMAP TRACE -----------------\n");
  fflush(stdout);
}

