// realm's DynamicSet class tries to be a std::set of integer-like things that
//  switches between various implementations to optimize for both near-empty and
//  near-full cases

#ifndef REALM_DYNAMIC_SET_H
#define REALM_DYNAMIC_SET_H

#include <cassert>
#include <vector>
#include <set>

template <typename IT>
class DynamicSet {
 public:
  DynamicSet(void);
  DynamicSet(const DynamicSet<IT>& copy_from);

  ~DynamicSet(void);

  // assignment and exchange
  DynamicSet<IT>& operator=(const DynamicSet<IT>& copy_from);
  DynamicSet<IT>& swap(DynamicSet<IT>& swap_with);

  // queries
  bool empty(void) const;
  size_t size(void) const;
  bool contains(IT index) const;

  // iterators
  class iterator;
  class const_iterator;
  
  iterator begin(void);
  iterator end(void);

  const_iterator begin(void) const;
  const_iterator end(void) const;

  // modification - add/remove return whether index was present before operation
  void clear(void);
  bool add(IT index);
  bool remove(IT index);
  void add(const DynamicSet<IT>& other);
  void remove(const DynamicSet<IT>& other);
  void intersect(const DynamicSet<IT>& other);

 protected:
  // a set can be in a bunch of different modes
  enum DSetMode {
    DSET_EMPTY,
    DSET_IMMEDIATE,
    DSET_VECTOR,
    DSET_SET,
    DSET_BITMAP
  };

  typedef unsigned long long RawType;
  typedef unsigned long long BitMaskType;

  typedef std::vector<IT> IndexVector;
  typedef std::set<IT> IndexSet;
  typedef std::vector<BitMaskType> BitMaskVector;

  static const int RAWTYPE_BITS = sizeof(RawType) * 8;
  static const int BITMASK_BITS = sizeof(BitMaskType) * 8;
  static const int IMM_VAL_BITS = 15;
  static const RawType IMM_VAL_MASK = (((RawType)1) << IMM_VAL_BITS) - 1;
  static const int TAG_BITS = 3;
  static const RawType TAG_MASK = (((RawType)1) << TAG_BITS) - 1;
  static const int IMM_PER_RAWTYPE = (RAWTYPE_BITS - TAG_BITS) / IMM_VAL_BITS;
  // the number of immediate values we can store is limited by both the
  // actual capacity to store the values and by the what's left for the size field
  static const int IMM_SIZE_BITS = RAWTYPE_BITS - IMM_PER_RAWTYPE * IMM_VAL_BITS;
  static const RawType IMM_SIZE_MASK = (((RawType)1) << IMM_SIZE_BITS) - 1;
  static const int MAX_IMM_SIZE = ((IMM_SIZE_MASK+1) < (2*IMM_PER_RAWTYPE)) ? (IMM_SIZE_MASK+1) : (2*IMM_PER_RAWTYPE);
  // what's the most we'll put in a vector that requires manually moving elements?
  static const size_t START_VECTOR_SIZE = 16;
  static const size_t MAX_VECTOR_SIZE = 16;

  DSetMode mode(void) const;
  IndexVector *idxvec_ptr(void) const;
  IndexSet *idxset_ptr(void) const;
  BitMaskVector *bitvec_ptr(void) const;

  void replace_ptr(void *newptr);

  RawType get_imm(int i) const;
  void set_imm(int i, RawType newval);

  void clone_stl_container(void);
  
  RawType tag_and_ptr;
  RawType size_and_imm;
#ifdef DEBUG_DYNAMIC_SET
  IndexSet reference_set;
#endif
};

#include "dynamic_set.inl"

#endif
