// realm's DynamicSet class tries to be a std::set of integer-like things that
//  switches between various implementations to optimize for both near-empty and
//  near-full cases

// nop, but helps IDEs
#include "dynamic_set.h"

template <typename IT>
DynamicSet<IT>::DynamicSet(void)
  : tag_and_ptr(DSET_EMPTY), size_and_imm(0)
{}

template <typename IT>
DynamicSet<IT>::DynamicSet(const DynamicSet<IT>& copy_from)
  : tag_and_ptr(copy_from.tag_and_ptr), size_and_imm(copy_from.size_and_imm)
#ifdef DEBUG_DYNAMIC_SET
  , reference_set(copy_from.reference_set)
#endif
{
  // if the representation uses a vector or set, we have to actually copy it
  clone_stl_container();
}

template <typename IT>
typename DynamicSet<IT>::DSetMode DynamicSet<IT>::mode(void) const
{
  return (DSetMode)(tag_and_ptr & TAG_MASK);
}

template <typename IT>
typename DynamicSet<IT>::IndexVector *DynamicSet<IT>::idxvec_ptr(void) const
{
#ifdef DEBUG_DYNAMIC_SET
  assert(mode() == DSET_VECTOR);
#endif
  return reinterpret_cast<DynamicSet<IT>::IndexVector *>(tag_and_ptr & ~TAG_MASK);
}

template <typename IT>
typename DynamicSet<IT>::IndexSet *DynamicSet<IT>::idxset_ptr(void) const
{
#ifdef DEBUG_DYNAMIC_SET
  assert(mode() == DSET_SET);
#endif
  return reinterpret_cast<DynamicSet<IT>::IndexSet *>(tag_and_ptr & ~TAG_MASK);
}

template <typename IT>
typename DynamicSet<IT>::BitMaskVector *DynamicSet<IT>::bitvec_ptr(void) const
{
#ifdef DEBUG_DYNAMIC_SET
  assert(mode() == DSET_BITMAP);
#endif
  return reinterpret_cast<DynamicSet<IT>::BitMaskVector *>(tag_and_ptr & ~TAG_MASK);
}

template <typename IT>
void DynamicSet<IT>::replace_ptr(void *newptr)
{
  RawType r = reinterpret_cast<RawType>(newptr);
  // this better not have any bits set in the tag area
  assert((r & TAG_MASK) == 0);
  tag_and_ptr = r | (tag_and_ptr & TAG_MASK);
}

template <typename IT>
void DynamicSet<IT>::clone_stl_container(void)
{
  switch(mode()) {
  case DSET_VECTOR:
    {
      replace_ptr(new IndexVector(*idxvec_ptr()));
      break;
    }

  case DSET_SET:
    {
      replace_ptr(new IndexSet(*idxset_ptr()));
      break;
    }

  case DSET_BITMAP:
    {
      replace_ptr(new BitMaskVector(*bitvec_ptr()));
      break;
    }

  default: break; // nothing to do here
  }
}

template <typename IT>
void DynamicSet<IT>::clear(void)
{
  // can free any container we own
  switch(mode()) {
  case DSET_VECTOR: { delete idxvec_ptr(); break; }
  case DSET_SET:    { delete idxset_ptr(); break; }
  case DSET_BITMAP: { delete bitvec_ptr(); break; }
  default: break;
  }

  tag_and_ptr = DSET_EMPTY;
  size_and_imm = 0;

#ifdef DEBUG_DYNAMIC_SET
  reference_set.clear();
#endif
}

template <typename IT>
DynamicSet<IT>::~DynamicSet(void)
{
  clear();
}

// assignment and exchange
template <typename IT>
DynamicSet<IT>& DynamicSet<IT>::operator=(const DynamicSet<IT>& copy_from)
{
  // delete our old data
  clear();
  // shallow copy, and then duplicate STL as needed
  tag_and_ptr = copy_from.tag_and_ptr;
  size_and_imm = copy_from.size_and_imm;
  clone_stl_container();
#ifdef DEBUG_DYNAMIC_SET
  reference_set = copy_from.reference_set;
#endif
  return *this;
}

template <typename IT>
DynamicSet<IT>& DynamicSet<IT>::swap(DynamicSet<IT>& swap_with)
{
  std::swap(tag_and_ptr, swap_with.tag_and_ptr);
  std::swap(size_and_imm, swap_with.size_and_imm);
#ifdef DEBUG_DYNAMIC_SET
  reference_set.swap(swap_with.reference_set);
#endif
  return *this;
}

// queries
template <typename IT>
bool DynamicSet<IT>::empty(void) const
{
  bool result = (mode() == DSET_EMPTY);
#ifdef DEBUG_DYNAMIC_SET
  assert(result == reference_set.empty());
#endif
  return result;
}

template <typename IT>
size_t DynamicSet<IT>::size(void) const
{
  size_t result;
  // immediate mode is a special case
  if(mode() == DSET_IMMEDIATE)
    result = ((size_and_imm & IMM_SIZE_MASK) + 1);  // always nonzero
  else
    result = size_and_imm;  // guaranteed to be 0 for DSET_EMPTY
#ifdef DEBUG_DYNAMIC_SET
  assert(result == reference_set.size());
#endif
  return result;
}

template <typename IT>
typename DynamicSet<IT>::RawType DynamicSet<IT>::get_imm(int i) const
{
#ifdef DEBUG_DYNAMIC_SET
  assert(i <= (2 * IMM_PER_RAWTYPE));
#endif
  RawType v = tag_and_ptr;
  if(i >= IMM_PER_RAWTYPE) {
    v = size_and_imm;
    i -= IMM_PER_RAWTYPE;
  }
  return (v >> (IMM_SIZE_BITS + i * IMM_VAL_BITS)) & IMM_VAL_MASK;
}

template <typename IT>
void DynamicSet<IT>::set_imm(int i, RawType newval)
{
#ifdef DEBUG_DYNAMIC_SET
  assert(i <= (2 * IMM_PER_RAWTYPE));
  assert(newval <= IMM_VAL_MASK);
#endif
  if(i < IMM_PER_RAWTYPE) {
    int shft = IMM_SIZE_BITS + i * IMM_VAL_BITS;
    tag_and_ptr = (tag_and_ptr & ~(IMM_VAL_MASK << shft)) | (newval << shft);
  } else {
    int shft = IMM_SIZE_BITS + (i - IMM_PER_RAWTYPE) * IMM_VAL_BITS;
    size_and_imm = (size_and_imm & ~(IMM_VAL_MASK << shft)) | (newval << shft);
  }
}

template <typename IT>
bool DynamicSet<IT>::contains(IT index) const
{
  // implementation depends on current mode
  bool result;
  switch(mode()) {
  case DSET_EMPTY:
    {
      result = false;
      break;
    }
    
  case DSET_IMMEDIATE:
    {
      int s = (size_and_imm & IMM_SIZE_MASK) + 1; // offset of 1 in this mode
      RawType idx(index);
      result = false;
      for(int i = 0; i < s; i++)
	if(get_imm(i) == idx) {
	  result = true;
	  break;
	}
      break;
    }

  case DSET_VECTOR:
    {
      // binary search for the element
      int lo = 0;
      int hi = size_and_imm - 1;
      const IndexVector& v(*idxvec_ptr());
#ifdef DEBUG_DYNAMIC_SET
      assert(size_and_imm == v.size());
#endif
      result = false;
      while(lo <= hi) {
	int mid = (lo + hi) >> 1;
	if(v[mid] > index) hi = mid - 1; else
	  if(v[mid] < index) lo = mid + 1; else {
	    result = true;
	    break;
	  }
      }
      break;
    }

  case DSET_SET:
    {
      result = idxset_ptr()->count(index) > 0;
      break;
    }

  case DSET_BITMAP:
    {
      RawType idx(index);  // cast to an integer
      RawType elem = idx / BITMASK_BITS;
      const BitMaskVector *v = bitvec_ptr();
      if(elem < v->size()) {
	RawType bpos = idx % BITMASK_BITS;
	result = ((*v)[elem] >> bpos) & 1;
      } else
	result = false;
      break;
    }

  default: assert(0);
  }
#ifdef DEBUG_DYNAMIC_SET
  assert(result == (reference_set.count(index) > 0));
#endif
  return result;
}

template <typename IT>
bool DynamicSet<IT>::add(IT index)
{
  // implementation depends on current mode
  bool result = false;
  switch(mode()) {
  case DSET_EMPTY:
    {
      RawType idx(index);
      assert(idx <= IMM_VAL_MASK); // make sure it fits
      // transition to immediate mode with one entry
#ifdef DEBUG_DYNAMIC_SET
      printf("switch EMPTY -> IMM\n");
#endif
      tag_and_ptr = (idx << IMM_SIZE_BITS) | DSET_IMMEDIATE;
      // no need to update size because a literal 0 means "1" for immediate
      result = false;
      break;
    }

  case DSET_IMMEDIATE:
    {
      int s = (size_and_imm & IMM_SIZE_MASK) + 1;  // offset of 1 in this mode
      RawType idx(index);
      result = false;
      int i;
      for(i = 0; i < s; i++) {
	RawType v = get_imm(i);
	if(v == idx) {
	  result = true;  // matched
	  break;
	}
	if(v > idx) {
	  // no match, but this is the insertion point
	  break;
	}
      }
      if(!result) {
	if(s < MAX_IMM_SIZE) {
	  // add to the immediate values
	  // shift values the brute-force way
	  for(int j = s; j >= i; j--)
	    set_imm(j, get_imm(j-1));
	  set_imm(i, idx);
	  size_and_imm++;  // won't overflow because of the check above
	} else {
#ifdef DEBUG_DYNAMIC_SET
	  printf("switch IMM -> VECTOR\n");
#endif
	  // have to convert to a vector
	  IndexVector *v = new IndexVector;
	  v->reserve(START_VECTOR_SIZE);
	  v->resize(s+1);
	  for(int j = 0; j < i; j++)
	    (*v)[j] = get_imm(j);
	  (*v)[i] = idx;
	  for(int j = i; j < s; j++)
	    (*v)[j+1] = get_imm(j);
	  tag_and_ptr = DSET_VECTOR;
	  replace_ptr(v);
	  size_and_imm = s + 1; // no offset now
	}
      }
      break;
    }

  case DSET_VECTOR:
    {
      IndexVector& v = *idxvec_ptr();
      // binary search to see if entry is present or to find insertion location
      int lo = 0;
      int hi = v.size() - 1;
      result = false;
      while(lo <= hi) {
	int mid = (lo + hi) >> 1;
	if(v[mid] > index) hi = mid - 1; else
	  if(v[mid] < index) lo = mid + 1; else {
	    result = true;
	    break;
	  }
      }
      if(!result) {
	// need to add the item - check to see if we should transition to a bitmap though
	int s = v.size();
	if(size_and_imm >= MAX_VECTOR_SIZE) {
	  RawType maxidx((index > v[s-1]) ? index : v[s-1]);
	  RawType maxelem = maxidx / BITMASK_BITS;
#ifdef DEBUG_DYNAMIC_SET
	  printf("switch VECTOR -> BITMASK\n");
#endif
	  BitMaskVector *v2 = new BitMaskVector(maxelem+1, 0);
	  // add in all the existing elements
	  for(int i = 0; i < s; i++) {
	    RawType idx(v[i]);
	    (*v2)[idx / BITMASK_BITS] |= ((RawType)1) << (idx % BITMASK_BITS);
	  }
	  // and add the new element
	  RawType idx(index);
	  (*v2)[idx / BITMASK_BITS] |= ((RawType)1) << (idx % BITMASK_BITS);
	  // now we can delete the old vector and switch to the new one
	  delete &v;
	  tag_and_ptr = DSET_BITMAP;
	  replace_ptr(v2);
	} else {
	  // insertion point is 'lo'
	  v.resize(s + 1);
	  while(s > lo) {
	    v[s] = v[s-1];
	    s--;
	  }
	  v[lo] = index;
	}
	// either way, the size is one larger
	size_and_imm++;
      }
      break;
    }

  case DSET_SET:
    {
      IndexSet& s = *idxset_ptr();
      if(s.insert(index).second) {
	// element was added
	size_and_imm++;
	result = false;
      } else
	result = true;
      // TODO: consider switch to bitmask?
      break;
    }

  case DSET_BITMAP:
    {
      BitMaskVector& v = *bitvec_ptr();
      RawType idx(index); // convert to integer we can bitslice
      RawType elem = idx / BITMASK_BITS;
      RawType bpos = idx % BITMASK_BITS;
      if(elem < v.size()) {
	// bits for this range already exist - check to see if index is present
	if((v[elem] >> bpos) & 1) {
	  result = true;
	} else {
	  v[elem] |= ((BitMaskType)1) << bpos;
	  result = false;
	  size_and_imm++;
	}
      } else {
	// TODO: consider switch to set?
	size_t old_size = v.size();
	v.resize(elem + 1);
	for(size_t i = old_size; i < elem; i++)
	  v[i] = 0;
	v[elem] = ((BitMaskType)1) << bpos;
	result = false;
	size_and_imm++;
      }
      break;
    }
  }
#ifdef DEBUG_DYNAMIC_SET
  assert(result == (reference_set.count(index) > 0));
  reference_set.insert(index);
#endif
  return result;
}

template <typename IT>
bool DynamicSet<IT>::remove(IT index)
{
  // implementation depends on current mode
  bool result = false;
  switch(mode()) {
  case DSET_EMPTY:
    {
      result = false;
      break;
    }

  case DSET_IMMEDIATE:
    {
      int s = (size_and_imm & IMM_SIZE_MASK) + 1;  // offset of 1 in this mode
      RawType idx(index);
      result = false;
      for(int i = 0; i < s; i++)
	if(idx == get_imm(i)) {
	  result = true;  // matched
	  // shift immediate values down
	  for(int j = i+1; j < s; j++)
	    set_imm(j-1, get_imm(j));
	  if(s > 1) {
	    // after removal, we're still non-empty
	    size_and_imm--;
	  } else {
	    // now we're empty
#ifdef DEBUG_DYNAMIC_SET
	    printf("switch IMM -> EMPTY\n");
#endif
	    tag_and_ptr = DSET_EMPTY;
	    size_and_imm = 0;
	  }
	  break;
	}
      break;
    }

  case DSET_VECTOR:
    {
      IndexVector& v = *idxvec_ptr();
      // binary search to see if entry is present or to find insertion location
      int lo = 0;
      int hi = v.size() - 1;
      result = false;
      while(lo <= hi) {
	int mid = (lo + hi) >> 1;
	if(v[mid] > index) hi = mid - 1; else
	  if(v[mid] < index) lo = mid + 1; else {
	    result = true;
	    if(v.size() > 1) {
	      // shift entries down and resize array
	      for(int i = mid+1; i < v.size(); i++)
		v[i - 1] = v[i];
	      v.resize(v.size() - 1);
	      size_and_imm--;
	    } else {
	      // now we're empty - delete the array
#ifdef DEBUG_DYNAMIC_SET
	      printf("switch VECTOR -> EMPTY\n");
#endif
	      delete &v;
	      tag_and_ptr = DSET_EMPTY;
	      size_and_imm = 0;
	    }
	    break;
	  }
      }
      break;
    }

  case DSET_SET:
    {
      IndexSet& s = *idxset_ptr();
      result = false;
      if(s.erase(index)) {
	result = true;
	if(s.empty()) {
#ifdef DEBUG_DYNAMIC_SET
	  printf("switch SET -> EMPTY\n");
#endif
	  delete &s;
	  tag_and_ptr = DSET_EMPTY;
	  size_and_imm = 0;
	} else {
	  size_and_imm--;
	  // TODO: consider switch to bitmask?
	}
      }
      break;
    }

  case DSET_BITMAP:
    {
      BitMaskVector& v = *bitvec_ptr();
      RawType idx(index); // convert to integer we can bitslice
      RawType elem = idx / BITMASK_BITS;
      RawType bpos = idx % BITMASK_BITS;
      result = false;
      if(elem < v.size()) {
	// bits for this range already exist - check to see if index is present
	if((v[elem] >> bpos) & 1) {
	  result = true;
	  v[elem] &= ~(((BitMaskType)1) << bpos);
	  if(size_and_imm > 1) {
	    size_and_imm--;
	  } else {
	    // we're empty
#ifdef DEBUG_DYNAMIC_SET
	    printf("switch BITMASK -> EMPTY\n");
#endif
	    delete &v;
	    tag_and_ptr = DSET_EMPTY;
	    size_and_imm = 0;
	  }
	}
      }
      break;
    }
  }
#ifdef DEBUG_DYNAMIC_SET
  assert(result == (reference_set.count(index) > 0));
  reference_set.erase(index);
#endif
  return result;
}
