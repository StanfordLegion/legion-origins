#include <stdio.h>
#include <unistd.h>

//define DEBUG_DYNAMIC_SET
#include "../dynamic_set.h"

template <typename IT>
void do_test(int iterations, int avg_elems_per_iter, int max_index)
{
  DynamicSet<IT> dset;
  std::set<int> exp;

  for(int i = 0; i < iterations; i++) {
    int n;
    n = rand() % (2 * avg_elems_per_iter);
    for(int j = 0; j < n; j++) {
      int k = rand() % max_index;
      bool f2 = !(exp.insert(k).second);
      printf("add %d (%d) -> ", k, f2);
      fflush(stdout);
      bool f1 = dset.add(k);
      printf("%d\n", f1);
      assert(f1 == f2);
    }
    n = rand() % (2 * avg_elems_per_iter);
    for(int j = 0; j < n; j++) {
      int k = rand() % max_index;
      bool f2 = exp.count(k);
      printf("contains %d (%d) -> ", k, f2);
      fflush(stdout);
      bool f1 = dset.contains(k);
      printf("%d\n", f1);     
      assert(f1 == f2);
    }
    n = rand() % (2 * avg_elems_per_iter);
    for(int j = 0; j < n; j++) {
      int k = rand() % max_index;
      bool f2 = exp.erase(k);
      printf("remove %d (%d) -> ", k, f2);
      fflush(stdout);
      bool f1 = dset.remove(k);
      printf("%d\n", f1);     
      assert(f1 == f2);
    }
    n = rand() % (2 * avg_elems_per_iter);
    for(int j = 0; j < n; j++) {
      int k = rand() % max_index;
      bool f2 = exp.count(k);
      printf("contains %d (%d) -> ", k, f2);
      fflush(stdout);
      bool f1 = dset.contains(k);
      printf("%d\n", f1);     
      assert(f1 == f2);
    }
    if(!(rand() % 10)) {
      printf("clear\n");
      dset.clear();
      exp.clear();
    }
  }
}

int main(int argc, const char *argv[])
{
  int iters = (argc > 1) ? atoi(argv[1]) : 100;
  int epi = (argc > 2) ? atoi(argv[2]) : 100;
  int maxidx = (argc > 3) ? atoi(argv[3]) : 128;
  int seed = (argc > 4) ? atoi(argv[4]) : getpid();

  srand(seed);
  do_test<unsigned short>(iters, epi, maxidx);
}
