#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <map>
#include <set>
#include <vector>
#include <sys/time.h>
#include <sys/resource.h>

#define MAX_PREDS 32

class Event {
 public:
  static Event *get_event(unsigned long long ev_id, unsigned gen);
  static void link_events(Event *p, Event *s);

  static int count_stalled(void);
  static void find_loops(void);

  void set_desc(const char *s);

  unsigned long long id;
  union {
    Event 	    *single;
    Event           **few;
    std::set<Event *> *multi;
  } preds;
  unsigned gen;
  short int pred_count;
  bool visited;
  //char *desc;

  static std::map<unsigned/*node*/,std::vector<Event*> > events;
  static std::map<unsigned/*node*/,std::map<std::pair<unsigned long long,unsigned>,Event*> > barriers;

 protected:
  Event(unsigned long long _id, unsigned _gen);
};

std::map<unsigned,std::vector<Event*> > Event::events;
std::map<unsigned,std::map<std::pair<unsigned long long,unsigned>,Event*> > Event::barriers;

Event::Event(unsigned long long _id, unsigned _gen)
  : id(_id), gen(_gen), pred_count(0), visited(false) { preds.single = NULL; }

/*static*/ Event *Event::get_event(unsigned long long ev_id, unsigned gen)
{
  unsigned kind = (ev_id >> 60);
  assert((kind == 2) || (kind == 3));
  unsigned node = (ev_id >> 44) & 0xFFFF;
  assert(node < 8192);
  if (kind == 2) {
    std::vector<Event*> &node_events = events[node];
    int index = 2*(ev_id & 0xFFFFFFFFFFF);
    if (index >= node_events.size())
      node_events.resize(index+1024,NULL);
    Event *result = node_events[index];
    if (result == NULL) {
      result = new Event(ev_id,gen);
      node_events[index] = result;
    } else {
      // First check to see if they are the same
      if (result->gen != gen) {
        result = node_events[index+1];
        if (result == NULL) {
	  result = new Event(ev_id,gen);
	  node_events[index+1] = result;
        } else {
	  if (result->gen != gen)
	    printf("THREE GENS FOR %llx: %d %d %d\n", ev_id, node_events[index]->gen,
			node_events[index+1]->gen, gen);
	  assert(result->gen == gen);
        }
      }
    }
    return result;
  } else {
    std::map<std::pair<unsigned long long,unsigned>,Event*> &bars = barriers[node]; 
    std::pair<unsigned long long,unsigned> key(ev_id,gen);
    std::map<std::pair<unsigned long long,unsigned>,Event*>::const_iterator finder = 
	bars.find(key);
    Event *result;
    if (finder == bars.end()) {
      result = new Event(ev_id,gen);
      bars[key] = result;
    } else {
      result = finder->second;
    }
    return result;
  }
}

/*static*/ void Event::link_events(Event *p, Event *s)
{
  //printf("%x/%d -> %x/%d\n", p->id, p->gen, s->id, s->gen);
  //assert(s->preds.find(p) == s->preds.end());
  if (s->pred_count == 0) {
    s->preds.single = p;
    s->pred_count = 1;
  } else if (s->pred_count == 1) {
    Event **next = (Event**)malloc(2*sizeof(Event*));
    next[0] = s->preds.single;
    next[1] = p; 
    s->preds.few = next;
    s->pred_count = 2;
  } else if (s->pred_count < MAX_PREDS) {
    Event **next = (Event**)realloc(s->preds.few, (s->pred_count+1)*sizeof(Event*)); 
    next[s->pred_count] = p;
    s->preds.few = next;
    s->pred_count++;
  } else if (s->pred_count == MAX_PREDS) {
    std::set<Event*> *multi = new std::set<Event*>();
    for (short i = 0; i < s->pred_count; i++)
      multi->insert(s->preds.few[i]);
    free(s->preds.few);
    s->preds.multi = multi;
    s->pred_count++;
  } else {
    s->preds.multi->insert(p);
    s->pred_count++;
  }
}

void Event::set_desc(const char *s)
{
#if 0
  if(desc != 0) {
    assert(!strcmp(desc, s));
    return;
  }
  desc = strdup(s);
#endif
}

/*static*/ int Event::count_stalled(void)
{
  int count = 0;
  for (std::map<unsigned,std::vector<Event*> >::const_iterator it = events.begin(); 
	it != events.end(); it++) {
    for (unsigned idx = 0; idx < it->second.size(); idx++) {
      if ((it->second[idx] != NULL) && (it->second[idx]->preds.single == NULL))
	count++;
    }
  }
  for (std::map<unsigned,std::map<std::pair<unsigned long long,unsigned>,Event*> >::iterator
	node_it = barriers.begin(); node_it != barriers.end(); node_it++) {
    std::map<std::pair<unsigned long long,unsigned>,Event*> &bars = node_it->second;
    for (std::map<std::pair<unsigned long long,unsigned>,Event*>::const_iterator it =
  	  bars.begin(); it != bars.end(); it++)
    {
      if (it->second->preds.single == NULL)
        count++;
    }
  }
  
  return count;
}

static void walk_tree(Event *e, std::vector<Event *>& stack)
{
  if(e->visited) return;

  for(std::vector<Event *>::iterator it = stack.begin(); it != stack.end(); it++)
    if(*it == e) {
      // LOOP!
      printf("LOOP DETECTED!\n");
      std::vector<Event *>::reverse_iterator it = stack.rbegin();
      do {
        printf("  %llx/%d: (%3zd) %s", (*it)->id, (*it)->gen, (*it)->pred_count, "blah");
        if(*it == e) return;
        it++;
      } while(1);
      e->visited = true;
      return;
    }

  // if we're here, no loops, so put ourselves on the stack and keep searching
  stack.push_back(e);

  if (e->preds.single != NULL) {
    if (e->pred_count == 1) {
      walk_tree(e->preds.single, stack);
    } else if (e->pred_count <= MAX_PREDS) {
      for (short i = 0; i < e->pred_count; i++)
 	walk_tree(e->preds.few[i], stack); 
    } else {
      for(std::set<Event *>::iterator it = e->preds.multi->begin();
          it != e->preds.multi->end();
          it++)
        walk_tree(*it, stack);
    }
  }

  // done, now take ourselves off the stack and mark ourselves visited
  stack.pop_back();
  e->visited = true;
}

/*static*/ void Event::find_loops(void)
{
#if 0
  for(std::map<unsigned long long, std::map<unsigned, Event *> *>::iterator it = events.begin();
      it != events.end();
      it++) {
    for(std::map<unsigned, Event *>::iterator it2 = it->second->begin();
        it2 != it->second->end();
        it2++) {
      it2->second->visited = false;
    }
  }
#endif
  for (std::map<unsigned,std::vector<Event*> >::const_iterator it = events.begin(); 
	it != events.end(); it++) {
    for (unsigned idx = 0; idx < it->second.size(); idx++) {
      if (it->second[idx] != NULL) {
        std::vector<Event*> stack;
        walk_tree(it->second[idx], stack);
      }
    }
  }
  for (std::map<unsigned,std::map<std::pair<unsigned long long,unsigned>,Event*> >::iterator
	node_it = barriers.begin(); node_it != barriers.end(); node_it++) {
    std::map<std::pair<unsigned long long,unsigned>,Event*> &bars = node_it->second;
    for (std::map<std::pair<unsigned long long,unsigned>,Event*>::const_iterator it =
  	  bars.begin(); it != bars.end(); it++)
    {
      std::vector<Event*> stack;
      walk_tree(it->second, stack);
    }
  }
}

int read_events(FILE *f)
{
  char line[256];
  // skip to beginning of events section
  do {
    char *s = fgets(line, 255, f);
    if(!s) {
      printf("WARNING: Unexpected EOF\n");
      return 0;
    }
  } while(strcmp(line, "PRINTING ALL PENDING EVENTS:\n"));

  // now read events until we get to "DONE"
  char *s = fgets(line, 255, f);
  if(!s) {
    printf("WARNING: Unexpected EOF\n");
    return 0;
  }
  while(strcmp(line, "DONE\n")) {
    // Looking for something like this:
    // Event 20000003: gen=110 subscr=0 local=1 remote=1
    unsigned long long ev_id;
    unsigned gen, subscr, nlocal, nremote;
    int ret = sscanf(s, "Barrier %llx: gen=%d subscr=%d", 
                     &ev_id, &gen, &subscr);
    if (ret != 3) {
      ret = sscanf(s, "Event %llx: gen=%d subscr=%d local=%d remote=%d",
                   &ev_id, &gen, &subscr, &nlocal, &nremote);
      assert(ret == 5);
    }

    // now read lines that start with a space
    while(1) {
      s = fgets(line, 255, f);
      if(!s) {
        printf("WARNING: Unexpected EOF\n");
        return 0;
      }
      //printf("(%s)\n", s);
      if(s[0] != ' ') break;

      {
        unsigned long long ev2_id;
        unsigned wgen, pos, proc_id, gen2;
        int ret = sscanf(s, "  [%d] L:%*p %nutility thread for processor %x: after=%llx/%d",
                         &wgen, &pos, &proc_id, &ev2_id, &gen2);
        if(ret == 4) {
          Event *e1 = Event::get_event(ev_id, wgen);
          Event *e2 = Event::get_event(ev2_id, gen2);
          Event::link_events(e1, e2);
          e2->set_desc(s + pos);
          continue;
        }
      }

      {
        unsigned long long ev2_id;
        unsigned wgen, pos, gen2;
        int ret = sscanf(s, "  [%d] L:%*p %nevent merger: %llx/%d",
                         &wgen, &pos, &ev2_id, &gen2);
        if(ret == 3) {
          Event *e1 = Event::get_event(ev_id, wgen);
          Event *e2 = Event::get_event(ev2_id, gen2);
          Event::link_events(e1, e2);
          e2->set_desc(s + pos);
          continue;
        }
      }

      {
        unsigned long long ev2_id;
        unsigned wgen, pos, gen2;
        int ret = sscanf(s, "  [%d] L:%*p %ndeferred trigger: after=%llx/%d",
                         &wgen, &pos, &ev2_id, &gen2);
        if(ret == 3) {
          Event *e1 = Event::get_event(ev_id, wgen);
          Event *e2 = Event::get_event(ev2_id, gen2);
          Event::link_events(e1, e2);
          e2->set_desc(s + pos);
          continue;
        }
      }

      {
        unsigned long long ev2_id;
        unsigned wgen, pos, gen2;
        int ret = sscanf(s, "  [%d] L:%*p %nGPU Task: %*p after=%llx/%d",
                         &wgen, &pos, &ev2_id, &gen2);
        if(ret == 3) {
          Event *e1 = Event::get_event(ev_id, wgen);
          Event *e2 = Event::get_event(ev2_id, gen2);
          Event::link_events(e1, e2);
          e2->set_desc(s + pos);
          continue;
        }
      }

      {
        unsigned long long ev2_id;
        unsigned wgen, pos, gen2;
        int ret = sscanf(s, "  [%d] L:%*p %ndma request %*p: after %llx/%d",
                         &wgen, &pos, &ev2_id, &gen2);
        if(ret == 3) {
          Event *e1 = Event::get_event(ev_id, wgen);
          Event *e2 = Event::get_event(ev2_id, gen2);
          Event::link_events(e1, e2);
          e2->set_desc(s + pos);
          continue;
        }
      }

      {
        unsigned long long ev2_id;
        unsigned wgen, pos, gen2;
        int ret = sscanf(s, "  [%d] L:%*p %ndeferred task: func=%*d proc=%*x finish=%llx/%d",
                         &wgen, &pos, &ev2_id, &gen2);
        if(ret == 3) {
          Event *e1 = Event::get_event(ev_id, wgen);
          Event *e2 = Event::get_event(ev2_id, gen2);
          Event::link_events(e1, e2);
          e2->set_desc(s + pos);
          continue;
        }
      }

      {
        unsigned long long ev2_id;
        unsigned wgen, pos, gen2, ts;
        int delta;
        int ret = sscanf(s, "  [%d] L:%*p %ndeferred arrival: barrier=%llx/%d (%d), delta=%d",
                         &wgen, &pos, &ev2_id, &gen2, &ts, &delta);
        if(ret == 5) {
          Event *e1 = Event::get_event(ev_id, wgen);
          Event *e2 = Event::get_event(ev2_id, gen2);
          Event::link_events(e1, e2);
          e2->set_desc(s + pos);
          continue;
        }
      }

      {
        unsigned long long ev2_id;
        unsigned wgen, gen2;
        unsigned long long thr_id;
        int ret = sscanf(s, "  [%d] L:%*p thread %llx waiting on %llx/%d",
                         &wgen, &thr_id, &ev2_id, &gen2);
        if(ret == 4) {
          Event *e1 = Event::get_event(ev_id, wgen);
          continue;
        }
      }

      {
        unsigned wgen;
        char dummy;
        int ret = sscanf(s, "  [%d] L:%*p external waiter%c",
                         &wgen, &dummy);
        if(ret == 2) {
          Event *e1 = Event::get_event(ev_id, wgen);
          continue;
        }
      }

      {
        unsigned wgen, dummy;
        int ret = sscanf(s, "  [%d] R: %d",
                         &wgen, &dummy);
        if(ret == 2) {
          // do something with this?
          continue;
        }
      }

      // if we get here, we failed to parse the dependent event
      printf("(%s)\n", s);
      exit(1);
    }
  }
}

int main(int argc, const char *argv[])
{
  printf("sizeof(Event) = %ld\n", sizeof(Event));
  for(int i = 1; i < argc; i++) {
    printf("%s\n", argv[i]);
    FILE *f = fopen(argv[i], "r");
    assert(f);
    read_events(f);
    fclose(f);
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);
    printf("Usage: %ld\n", usage.ru_maxrss);
  }

  int stalled = Event::count_stalled();
  printf("%d stalled events\n", stalled);

  Event::find_loops();
}

