#!/usr/bin/python

import sys, re
import operator

predicated_pat  = re.compile("\s+/\*\w+\*/\s+[@!\w]* (?P<instr>[\w\.]+)")
instruction_pat = re.compile("\s+/\*\w+\*/\s+(?P<instr>[\w\.]+)")

def filter():
    if len(sys.argv) == 1:
        print "Need file name"
        return
    fhandle = open(sys.argv[1],"r")
    max_lines = 0 
    if len(sys.argv) > 2:
        max_lines = int(sys.argv[2]) 
    max_instructions = 0 
    if len(sys.argv) > 3:
        max_instructions = int(sys.argv[3])
    histogram = dict()
    num_lines = 0
    for line in fhandle:
        num_lines = num_lines + 1
        if num_lines == max_lines:
            break
        m = instruction_pat.match(line)
        if m <> None:
            instruction = m.group('instr')
            if instruction in histogram:
                histogram[instruction] = histogram[instruction]+1
            else:
                histogram[instruction] = 1
            continue
        m = predicated_pat.match(line)
        if m <> None:
            instruction = m.group('instr')
            if instruction in histogram:
                histogram[instruction] = histogram[instruction]+1
            else:
                histogram[instruction] = 1
            continue
    fhandle.close()
    num_instructions = 0
    for k,v in reversed(sorted(histogram.iteritems(), key=operator.itemgetter(1))):
        print str(k)+"  "+str(v)
        num_instructions = num_instructions + 1
        if num_instructions == max_instructions:
            break


if __name__ == "__main__":
    filter()

