#!/usr/bin/python

import os, shutil, stat, subprocess, sys

###
### License owned exclusively by Stanford
###

cpp_apache = [
    '/* Copyright 2015 Stanford University',
    ' *',
    ' * Licensed under the Apache License, Version 2.0 (the "License");',
    ' * you may not use this file except in compliance with the License.',
    ' * You may obtain a copy of the License at',
    ' *',
    ' *     http://www.apache.org/licenses/LICENSE-2.0',
    ' *',
    ' * Unless required by applicable law or agreed to in writing, software',
    ' * distributed under the License is distributed on an "AS IS" BASIS,',
    ' * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    ' * See the License for the specific language governing permissions and',
    ' * limitations under the License.',
    ' */']

make_apache = [
    '# Copyright 2015 Stanford University',
    '#',
    '# Licensed under the Apache License, Version 2.0 (the "License");',
    '# you may not use this file except in compliance with the License.',
    '# You may obtain a copy of the License at',
    '#',
    '#     http://www.apache.org/licenses/LICENSE-2.0',
    '#',
    '# Unless required by applicable law or agreed to in writing, software',
    '# distributed under the License is distributed on an "AS IS" BASIS,',
    '# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '# See the License for the specific language governing permissions and',
    '# limitations under the License.',
    '#']

py_apache = [
    '#!/usr/bin/env python',
    '',
    '# Copyright 2015 Stanford University',
    '#',
    '# Licensed under the Apache License, Version 2.0 (the "License");',
    '# you may not use this file except in compliance with the License.',
    '# You may obtain a copy of the License at',
    '#',
    '#     http://www.apache.org/licenses/LICENSE-2.0',
    '#',
    '# Unless required by applicable law or agreed to in writing, software',
    '# distributed under the License is distributed on an "AS IS" BASIS,',
    '# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '# See the License for the specific language governing permissions and',
    '# limitations under the License.',
    '#']

lua_apache = [
    '-- Copyright 2015 Stanford University',
    '--',
    '-- Licensed under the Apache License, Version 2.0 (the "License");',
    '-- you may not use this file except in compliance with the License.',
    '-- You may obtain a copy of the License at',
    '--',
    '--     http://www.apache.org/licenses/LICENSE-2.0',
    '--',
    '-- Unless required by applicable law or agreed to in writing, software',
    '-- distributed under the License is distributed on an "AS IS" BASIS,',
    '-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '-- See the License for the specific language governing permissions and',
    '-- limitations under the License.'
    ]

###
### License owned joinly by Stanford and LANL
###

cpp_apache_lanl = [
    '/* Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    ' *',
    ' * Licensed under the Apache License, Version 2.0 (the "License");',
    ' * you may not use this file except in compliance with the License.',
    ' * You may obtain a copy of the License at',
    ' *',
    ' *     http://www.apache.org/licenses/LICENSE-2.0',
    ' *',
    ' * Unless required by applicable law or agreed to in writing, software',
    ' * distributed under the License is distributed on an "AS IS" BASIS,',
    ' * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    ' * See the License for the specific language governing permissions and',
    ' * limitations under the License.',
    ' */']

make_apache_lanl = [
    '# Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    '#',
    '# Licensed under the Apache License, Version 2.0 (the "License");',
    '# you may not use this file except in compliance with the License.',
    '# You may obtain a copy of the License at',
    '#',
    '#     http://www.apache.org/licenses/LICENSE-2.0',
    '#',
    '# Unless required by applicable law or agreed to in writing, software',
    '# distributed under the License is distributed on an "AS IS" BASIS,',
    '# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '# See the License for the specific language governing permissions and',
    '# limitations under the License.',
    '#']

py_apache_lanl = [
    '#!/usr/bin/env python',
    '',
    '# Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    '#',
    '# Licensed under the Apache License, Version 2.0 (the "License");',
    '# you may not use this file except in compliance with the License.',
    '# You may obtain a copy of the License at',
    '#',
    '#     http://www.apache.org/licenses/LICENSE-2.0',
    '#',
    '# Unless required by applicable law or agreed to in writing, software',
    '# distributed under the License is distributed on an "AS IS" BASIS,',
    '# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '# See the License for the specific language governing permissions and',
    '# limitations under the License.',
    '#']

lisp_apache_lanl = [
    '; Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    ';',
    '; Licensed under the Apache License, Version 2.0 (the "License");',
    '; you may not use this file except in compliance with the License.',
    '; You may obtain a copy of the License at',
    ';',
    ';     http://www.apache.org/licenses/LICENSE-2.0',
    ';',
    '; Unless required by applicable law or agreed to in writing, software',
    '; distributed under the License is distributed on an "AS IS" BASIS,',
    '; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '; See the License for the specific language governing permissions and',
    '; limitations under the License.',
    ';']

legion_apache_lanl = [
    '/* Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    ' *',
    ' * Licensed under the Apache License, Version 2.0 (the "License");',
    ' * you may not use this file except in compliance with the License.',
    ' * You may obtain a copy of the License at',
    ' *',
    ' *     http://www.apache.org/licenses/LICENSE-2.0',
    ' *',
    ' * Unless required by applicable law or agreed to in writing, software',
    ' * distributed under the License is distributed on an "AS IS" BASIS,',
    ' * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    ' * See the License for the specific language governing permissions and',
    ' * limitations under the License.',
    ' */']

legion_reset_line_apache_lanl = [
    '/* Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    ' *',
    ' * Licensed under the Apache License, Version 2.0 (the "License");',
    ' * you may not use this file except in compliance with the License.',
    ' * You may obtain a copy of the License at',
    ' *',
    ' *     http://www.apache.org/licenses/LICENSE-2.0',
    ' *',
    ' * Unless required by applicable law or agreed to in writing, software',
    ' * distributed under the License is distributed on an "AS IS" BASIS,',
    ' * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    ' * See the License for the specific language governing permissions and',
    ' * limitations under the License.',
    ' */',
    '',
    '#line 0']

# These files have licenses added manually or have no license for some
# other reason.
none = []

class SourceFile:
    def __init__(self, src, dst = None, license = None):
        assert license is not None
        if dst is None:
            dst = src
        self.src_postfix = src
        self.dst_postfix = dst
        self.license = license

    def move_file(self, src_prefix, dst_prefix):
        src_path = os.path.join(src_prefix, self.src_postfix)
        dst_path = os.path.join(dst_prefix, self.dst_postfix)

        # Touch the file to ensure directories and permissions exist.
        if not os.path.isdir(os.path.dirname(dst_path)):
            raise Exception('Directory %s does not exist' % os.path.dirname(dst_path))

        # Copy the file.
        with open(dst_path, 'wb') as dst_file:
            with open(src_path, 'rb') as src_file:
                dst_file.write(self.modify_content(src_file.read()))

        # Copy the permissions of the original file.
        try:
            src_mode = os.stat(src_path).st_mode
            os.chmod(dst_path, src_mode)
        except:
            raise Exception('Chmod failed on %s' % dst_path)

    def modify_content(self, content):
        if len(self.license) == 0:
            return content
        return '\n'.join(self.license + [''] + content.split('\n'))

class PythonSourceFile(SourceFile):
    def modify_content(self, content):
        lines = content.split('\n')
        if lines[0].startswith('#!'):
            lines = lines[1:]
            if len(lines[0].strip()) == 0:
                lines = lines[1:]
        return '\n'.join(self.license + [''] + lines)

class SymlinkSourceFile:
    def __init__(self, src, dst = None):
        if dst is None:
            dst = src
        self.src_postfix = src
        self.dst_postfix = dst

    def move_file(self, src_prefix, dst_prefix):
        src_path = os.path.join(src_prefix, self.src_postfix)
        dst_path = os.path.join(dst_prefix, self.dst_postfix)

        # Touch the file to ensure directories and permissions exist.
        if not os.path.isdir(os.path.dirname(dst_path)):
            raise Exception('Directory %s does not exist' % os.path.dirname(dst_path))

        if not os.path.islink(src_path):
            raise Exception('Source %s is not symlink')

        if os.path.exists(dst_path):
            os.unlink(dst_path)
        os.symlink(os.readlink(src_path), dst_path)

class SourceDirectory:
    def __init__(self, src, dst = None, license_map = None):
        assert license_map is not None
        if dst is None:
            dst = src
        self.src_postfix = src
        self.dst_postfix = dst
        self.license_map = license_map

    def move_file(self, src_prefix, dst_prefix):
        src_path = os.path.join(src_prefix, self.src_postfix)
        dst_path = os.path.join(dst_prefix, self.dst_postfix)

        if not os.path.isdir(dst_path):
            raise Exception('Directory %s does not exist' % dst_path)

        def helper(_ignore, src_dirname, filenames):
            postfix = os.path.relpath(src_dirname, src_prefix)
            dst_dirname = os.path.join(dst_prefix, postfix)

            if not os.path.isdir(dst_dirname):
                os.mkdir(dst_dirname)

            for filename in filenames:
                ext = os.path.splitext(filename)[1]
                if ext in self.license_map:
                    src_file = SourceFile(
                        src = os.path.join(postfix, filename),
                        license = self.license_map[ext])
                    src_file.move_file(src_prefix, dst_prefix)
        os.path.walk(src_path, helper, None)

def get_files():
    files = [
        # Top-level files
        SourceFile(src = '.gitignore', license = none),
        SourceFile(src = '.travis.yml', license = none),
        # Runtime files
        SourceFile(src = 'runtime/accessor.h', license = cpp_apache),
        SourceFile(src = 'runtime/arrays.h', license = cpp_apache),
        SourceFile(src = 'runtime/activemsg.h', license = cpp_apache),
        SourceFile(src = 'runtime/common.h', license = cpp_apache),
        SourceFile(src = 'runtime/default_mapper.h', license = cpp_apache),
        SourceFile(src = 'runtime/field_tree.h', license = cpp_apache),
        SourceFile(src = 'runtime/garbage_collection.h', license = cpp_apache),
        SourceFile(src = 'runtime/interval_tree.h', license = cpp_apache),
        SourceFile(src = 'runtime/rectangle_set.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_allocation.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_c.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_config.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_c_util.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_logging.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_profiling.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_ops.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_spy.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_tasks.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_trace.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_types.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_utilities.h', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel.h', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_config.h', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_impl.h', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_dma.h', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_gpu.h', license = cpp_apache),
        SourceFile(src = 'runtime/region_tree.h', license = cpp_apache),
        SourceFile(src = 'runtime/runtime.h', license = cpp_apache),
        SourceFile(src = 'runtime/shim_mapper.h', license = cpp_apache),
        SourceFile(src = 'runtime/utilities.h', license = cpp_apache),
        SourceFile(src = 'runtime/activemsg.cc', license = cpp_apache),
        SourceFile(src = 'runtime/default_mapper.cc', license = cpp_apache),
        SourceFile(src = 'runtime/garbage_collection.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_c.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_logging.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_spy.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_ops.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_tasks.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_trace.cc', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel.cc', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_dma.cc', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_gpu.cc', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_disk.cc', license = cpp_apache),
        SourceFile(src = 'runtime/region_tree.cc', license = cpp_apache),
        SourceFile(src = 'runtime/runtime.cc', license = cpp_apache),
        SourceFile(src = 'runtime/runtime.mk', license = make_apache),
        SourceFile(src = 'runtime/shared_lowlevel.cc', license = cpp_apache),
        SourceFile(src = 'runtime/shim_mapper.cc', license = cpp_apache),
        SourceFile(src = 'runtime/alt_mappers.h', license = cpp_apache),
        SourceFile(src = 'runtime/alt_mappers.cc', license = cpp_apache),
        SourceFile(src = 'runtime/mapping_utilities.h', license = cpp_apache),
        SourceFile(src = 'runtime/mapping_utilities.cc', license = cpp_apache),
        SourceFile(src = 'runtime/realm/logging.h', license = cpp_apache),
        SourceFile(src = 'runtime/realm/logging.inl', license = cpp_apache),
        SourceFile(src = 'runtime/realm/logging.cc', license = cpp_apache),
        # Saxpy files
        SourceFile(src = 'apps/saxpy/.gitignore', license = none),
        SourceFile(src = 'apps/saxpy/saxpy_array.cc', license = cpp_apache),
        SourceFile(src = 'apps/saxpy/Makefile', license = make_apache),
        # Circuit files
        SourceFile(src = 'apps/circuit2/circuit.h',
                   dst = 'apps/circuit/circuit.h', license = cpp_apache),
        SourceFile(src = 'apps/circuit2/circuit_mapper.h',
                   dst = 'apps/circuit/circuit_mapper.h', license = cpp_apache),
        SourceFile(src = 'apps/circuit2/Makefile',
                   dst = 'apps/circuit/Makefile', license = make_apache),
        SourceFile(src = 'apps/circuit2/circuit.cc',
                   dst = 'apps/circuit/circuit.cc', license = cpp_apache),
        SourceFile(src = 'apps/circuit2/circuit_cpu.cc',
                   dst = 'apps/circuit/circuit_cpu.cc', license = cpp_apache),
        SourceFile(src = 'apps/circuit2/circuit_gpu.cu',
                   dst = 'apps/circuit/circuit_gpu.cu', license = cpp_apache),
        SourceFile(src = 'apps/circuit2/circuit_mapper.cc',
                   dst = 'apps/circuit/circuit_mapper.cc', license = cpp_apache),
        # Delaunay files
        SourceFile(src = 'apps/delaunay/delaunay.h', license = cpp_apache),
        SourceFile(src = 'apps/delaunay/delaunay.cc', license = cpp_apache),
        SourceFile(src = 'apps/delaunay/Makefile', license = make_apache),
        # Example files
        SourceFile(src = 'examples/00_hello_world/hello_world.cc', license = cpp_apache),
        SourceFile(src = 'examples/00_hello_world/Makefile', license = make_apache),
        SourceFile(src = 'examples/01_tasks_and_futures/tasks_and_futures.cc', license = cpp_apache),
        SourceFile(src = 'examples/01_tasks_and_futures/Makefile', license = make_apache),
        SourceFile(src = 'examples/02_index_tasks/index_tasks.cc', license = cpp_apache),
        SourceFile(src = 'examples/02_index_tasks/Makefile', license = make_apache),
        SourceFile(src = 'examples/03_global_vars/global_vars.cc', license = cpp_apache),
        SourceFile(src = 'examples/03_global_vars/Makefile', license = make_apache),
        SourceFile(src = 'examples/04_logical_regions/logical_regions.cc', license = cpp_apache),
        SourceFile(src = 'examples/04_logical_regions/Makefile', license = make_apache),
        SourceFile(src = 'examples/05_physical_regions_and_accessors/physical_regions.cc', license = cpp_apache),
        SourceFile(src = 'examples/05_physical_regions_and_accessors/Makefile', license = make_apache),
        SourceFile(src = 'examples/06_privileges/privileges.cc', license = cpp_apache),
        SourceFile(src = 'examples/06_privileges/Makefile', license = make_apache),
        SourceFile(src = 'examples/07_partitioning/partitioning.cc', license = cpp_apache),
        SourceFile(src = 'examples/07_partitioning/Makefile', license = make_apache),
        SourceFile(src = 'examples/08_multiple_partitions/multiple_partitions.cc', license = cpp_apache),
        SourceFile(src = 'examples/08_multiple_partitions/Makefile', license = make_apache),
        SourceFile(src = 'examples/09_custom_mappers/custom_mapper.cc', license = cpp_apache),
        SourceFile(src = 'examples/09_custom_mappers/Makefile', license = make_apache),
        SourceFile(src = 'examples/full_circuit/Makefile', license = make_apache),
        SourceFile(src = 'examples/full_circuit/circuit.h', license = cpp_apache),
        SourceFile(src = 'examples/full_circuit/circuit.cc', license = cpp_apache),
        SourceFile(src = 'examples/full_circuit/circuit_cpu.cc', license = cpp_apache),
        SourceFile(src = 'examples/full_circuit/circuit_gpu.cu', license = cpp_apache),
        SourceFile(src = 'examples/full_circuit/circuit_mapper.h', license = cpp_apache),
        SourceFile(src = 'examples/full_circuit/circuit_mapper.cc', license = cpp_apache),
        SourceFile(src = 'examples/full_ghost/Makefile', license = make_apache),
        SourceFile(src = 'examples/full_ghost/ghost.cc', license = cpp_apache),
        # Tool files
        PythonSourceFile(src = 'tools/legion_spy.py', license = py_apache),
        PythonSourceFile(src = 'tools/spy_parser.py', license = py_apache),
        PythonSourceFile(src = 'tools/spy_state.py', license = py_apache),
        PythonSourceFile(src = 'tools/spy_timeline.py', license = py_apache),
        PythonSourceFile(src = 'tools/spy_analysis.py', license = py_apache),
        PythonSourceFile(src = 'tools/legion_prof.py', license = py_apache),
        SourceFile(src = 'apps/Makefile.template', license = make_apache),
        # Lua Files
        SourceFile(src = 'bindings/lua/.gitignore', license = none),
        SourceFile(src = 'bindings/lua/array_test.lua', license = lua_apache),
        SourceFile(src = 'bindings/lua/binding.cc', license = cpp_apache),
        SourceFile(src = 'bindings/lua/binding_functions.h', license = cpp_apache),
        SourceFile(src = 'bindings/lua/binding.h', license = cpp_apache),
        SourceFile(src = 'bindings/lua/Jamroot.diff', license = make_apache),
        SourceFile(src = 'bindings/lua/legionlib.lua', license = lua_apache),
        SourceFile(src = 'bindings/lua/legionlib-terra.t', license = lua_apache),
        SourceFile(src = 'bindings/lua/legion.lua', license = lua_apache),
        SourceFile(src = 'bindings/lua/Makefile', license = make_apache),
        SourceFile(src = 'bindings/lua/README', license = none),
        SourceFile(src = 'bindings/lua/Set.lua', license = lua_apache),
        SourceFile(src = 'bindings/lua/examples/circuit/circuit_cpu.lua', license = lua_apache),
        SourceFile(src = 'bindings/lua/examples/circuit/circuit_defs.lua', license = lua_apache),
        SourceFile(src = 'bindings/lua/examples/circuit/circuit.lua', license = lua_apache),
        SourceFile(src = 'bindings/lua/examples/circuit/circuit_tasks.lua', license = lua_apache),
        SourceFile(src = 'bindings/lua/examples/circuit_terra_native/circuit_cpu.t', license = lua_apache),
        SourceFile(src = 'bindings/lua/examples/circuit_terra_native/circuit_defs.t', license = lua_apache),
        SourceFile(src = 'bindings/lua/examples/circuit_terra_native/circuit.t', license = lua_apache),
        SourceFile(src = 'bindings/lua/examples/circuit_terra_native/circuit_tasks.t', license = lua_apache),
        SourceFile(src = 'bindings/lua/examples/saxpy/saxpy.lua', license = lua_apache),
        SourceFile(src = 'bindings/lua/examples/saxpy/saxpy_task.lua', license = lua_apache),
        # Terra Bindings Files
        SourceFile(src = 'bindings/terra/.gitignore', license = none),
        SourceFile(src = 'bindings/terra/legionlib.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/legionlib-mapper.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/legionlib-terra.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/legionlib-util.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/legion_terra.cc', license = cpp_apache),
        SourceFile(src = 'bindings/terra/legion_terra.h', license = cpp_apache),
        SourceFile(src = 'bindings/terra/legion_terra_util.h', license = cpp_apache),
        SourceFile(src = 'bindings/terra/liszt_gpu_mapper.cc', license = cpp_apache),
        SourceFile(src = 'bindings/terra/liszt_gpu_mapper.h', license = cpp_apache),
        SourceFile(src = 'bindings/terra/lua_mapper_wrapper.cc', license = cpp_apache),
        SourceFile(src = 'bindings/terra/lua_mapper_wrapper.h', license = cpp_apache),
        SourceFile(src = 'bindings/terra/legion_terra_partitions.cc', license = cpp_apache),
        SourceFile(src = 'bindings/terra/legion_terra_partitions.h', license = cpp_apache),
        SourceFile(src = 'bindings/terra/Makefile', license = make_apache),
        SourceFile(src = 'bindings/terra/README', license = none),
        SourceFile(src = 'bindings/terra/set_env.sh', license = py_apache),
        SourceFile(src = 'bindings/terra/examples/lua/00_hello_world/hello_world.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/examples/lua/01_tasks_and_futures/fibonacci.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/examples/lua/02_index_tasks/index_tasks.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/examples/lua/04_logical_regions/logical_regions.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/examples/lua/05_physical_regions_and_accessors/physical_regions.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/examples/lua/06_privileges/privileges.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/examples/lua/07_partitioning/partitioning.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/examples/lua/08_multiple_partitions/multiple_partitions.t', license = lua_apache),
        SourceFile(src = 'bindings/terra/examples/terra/00_hello_world/hello_world.t', license = lua_apache),
        # Compiler files
        SourceFile(src = 'language/.gitignore', license = none),
        SourceFile(src = 'language/README.md', license = none),
        PythonSourceFile(src = 'language/install_old.py', license = py_apache),
        PythonSourceFile(src = 'language/install.py', license = py_apache),
        PythonSourceFile(src = 'language/legion_old.py', license = py_apache),
        PythonSourceFile(src = 'language/legion.py', license = py_apache),
        PythonSourceFile(src = 'language/test.py', license = py_apache),
        PythonSourceFile(src = 'language/travis.py', license = py_apache),
        SourceFile(src = 'language/src/legion.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/ast.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/builtins.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/codegen.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/config.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/cudahelper.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/log.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/optimize_config_options.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/optimize_divergence.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/optimize_futures.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/optimize_inlines.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/optimize_loops.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/parser.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/specialize.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/std.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/symbol_table.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/traverse_symbols.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/type_check.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/union_find.t', license = lua_apache),
        SourceFile(src = 'language/src/legion/vectorize_loops.t', license = lua_apache),
        SourceFile(src = 'language/examples/circuit.cc', license = cpp_apache),
        SourceFile(src = 'language/examples/circuit.h', license = cpp_apache),
        SourceFile(src = 'language/examples/circuit.lg', license = lua_apache),
        SourceFile(src = 'language/examples/list.lg', license = lua_apache),
        SourceFile(src = 'language/examples/manual_capi.lg', license = lua_apache),
        SourceFile(src = 'language/examples/manual_capi_1d.lg', license = lua_apache),
        SourceFile(src = 'language/examples/manual_capi_fields_after_region.lg', license = lua_apache),
        SymlinkSourceFile(src = 'language/examples/manual_lua_00_hello_world.lg'),
        SymlinkSourceFile(src = 'language/examples/manual_lua_01_fibonacci.lg'),
        SymlinkSourceFile(src = 'language/examples/manual_lua_02_index_tasks.lg'),
        SymlinkSourceFile(src = 'language/examples/manual_lua_04_logical_regions.lg'),
        SymlinkSourceFile(src = 'language/examples/manual_lua_05_physical_regions.lg'),
        SymlinkSourceFile(src = 'language/examples/manual_lua_06_privileges.lg'),
        SymlinkSourceFile(src = 'language/examples/manual_lua_07_partitioning.lg'),
        SymlinkSourceFile(src = 'language/examples/manual_lua_08_multiple_partitions.lg'),
        SymlinkSourceFile(src = 'language/examples/manual_terra_00_hello_world.lg'),
        SourceFile(src = 'language/examples/pennant.cc', license = cpp_apache),
        SourceFile(src = 'language/examples/pennant.h', license = cpp_apache),
        SourceFile(src = 'language/examples/pennant.lg', license = lua_apache),
        SourceFile(src = 'language/examples/pennant_stripmine.lg', license = lua_apache),
        SourceFile(src = 'language/examples/saxpy.lg', license = lua_apache),
        SourceFile(src = 'language/examples/sleep.lg', license = lua_apache),
        SourceFile(src = 'language/examples/tree.lg', license = lua_apache),
        SourceDirectory(src = 'language/examples/pennant.tests',
                        license_map = {'.pnt': none,
                                       '.std': none,
                                       '.std4': none}),
        SourceDirectory(src = 'language/tests/compile_fail',
                        license_map = {'.lg': none}),
        SourceDirectory(src = 'language/tests/run_pass',
                        license_map = {'.lg': lua_apache}),
        ]
    return files

def main():
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print 'Usage: %s [src_path] <target_path>' % sys.argv[0]
        sys.exit(1)

    src_dir = None
    dst_dir = None

    if len(sys.argv) == 2:
        if not 'LG_RT_DIR' in os.environ:
            print 'LG_RT_DIR environment variable is not set'
            sys.exit(2)
        src_dir = os.path.join(os.environ['LG_RT_DIR'], '..')
        dst_dir = sys.argv[1]
    else:
        assert len(sys.argv) == 3
        src_dir = sys.argv[1]
        dst_dir = sys.argv[2]

    files = get_files()

    for f in files:
        f.move_file(src_dir, dst_dir)
    print 'Success'

if __name__ == '__main__':
    main()
