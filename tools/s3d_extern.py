#!/usr/bin/python

import os, shutil, stat, subprocess, sys

###
### License owned exclusively by Stanford
###

cpp_apache = [
    '/* Copyright 2014 Stanford University',
    ' *',
    ' * Licensed under the Apache License, Version 2.0 (the "License");',
    ' * you may not use this file except in compliance with the License.',
    ' * You may obtain a copy of the License at',
    ' *',
    ' *     http://www.apache.org/licenses/LICENSE-2.0',
    ' *',
    ' * Unless required by applicable law or agreed to in writing, software',
    ' * distributed under the License is distributed on an "AS IS" BASIS,',
    ' * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    ' * See the License for the specific language governing permissions and',
    ' * limitations under the License.',
    ' */']

make_apache = [
    '# Copyright 2014 Stanford University',
    '#',
    '# Licensed under the Apache License, Version 2.0 (the "License");',
    '# you may not use this file except in compliance with the License.',
    '# You may obtain a copy of the License at',
    '#',
    '#     http://www.apache.org/licenses/LICENSE-2.0',
    '#',
    '# Unless required by applicable law or agreed to in writing, software',
    '# distributed under the License is distributed on an "AS IS" BASIS,',
    '# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '# See the License for the specific language governing permissions and',
    '# limitations under the License.',
    '#']

py_apache = [
    '#!/usr/bin/env python',
    '',
    '# Copyright 2014 Stanford University',
    '#',
    '# Licensed under the Apache License, Version 2.0 (the "License");',
    '# you may not use this file except in compliance with the License.',
    '# You may obtain a copy of the License at',
    '#',
    '#     http://www.apache.org/licenses/LICENSE-2.0',
    '#',
    '# Unless required by applicable law or agreed to in writing, software',
    '# distributed under the License is distributed on an "AS IS" BASIS,',
    '# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '# See the License for the specific language governing permissions and',
    '# limitations under the License.',
    '#']

lua_apache = [
    '-- Copyright 2014 Stanford University',
    '--',
    '-- Licensed under the Apache License, Version 2.0 (the "License");',
    '-- you may not use this file except in compliance with the License.',
    '-- You may obtain a copy of the License at',
    '--',
    '--     http://www.apache.org/licenses/LICENSE-2.0',
    '--',
    '-- Unless required by applicable law or agreed to in writing, software',
    '-- distributed under the License is distributed on an "AS IS" BASIS,',
    '-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '-- See the License for the specific language governing permissions and',
    '-- limitations under the License.'
    ]

###
### License owned joinly by Stanford and LANL
###

cpp_apache_lanl = [
    '/* Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    ' *',
    ' * Licensed under the Apache License, Version 2.0 (the "License");',
    ' * you may not use this file except in compliance with the License.',
    ' * You may obtain a copy of the License at',
    ' *',
    ' *     http://www.apache.org/licenses/LICENSE-2.0',
    ' *',
    ' * Unless required by applicable law or agreed to in writing, software',
    ' * distributed under the License is distributed on an "AS IS" BASIS,',
    ' * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    ' * See the License for the specific language governing permissions and',
    ' * limitations under the License.',
    ' */']

make_apache_lanl = [
    '# Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    '#',
    '# Licensed under the Apache License, Version 2.0 (the "License");',
    '# you may not use this file except in compliance with the License.',
    '# You may obtain a copy of the License at',
    '#',
    '#     http://www.apache.org/licenses/LICENSE-2.0',
    '#',
    '# Unless required by applicable law or agreed to in writing, software',
    '# distributed under the License is distributed on an "AS IS" BASIS,',
    '# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '# See the License for the specific language governing permissions and',
    '# limitations under the License.',
    '#']

py_apache_lanl = [
    '#!/usr/bin/env python',
    '',
    '# Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    '#',
    '# Licensed under the Apache License, Version 2.0 (the "License");',
    '# you may not use this file except in compliance with the License.',
    '# You may obtain a copy of the License at',
    '#',
    '#     http://www.apache.org/licenses/LICENSE-2.0',
    '#',
    '# Unless required by applicable law or agreed to in writing, software',
    '# distributed under the License is distributed on an "AS IS" BASIS,',
    '# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '# See the License for the specific language governing permissions and',
    '# limitations under the License.',
    '#']

lisp_apache_lanl = [
    '; Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    ';',
    '; Licensed under the Apache License, Version 2.0 (the "License");',
    '; you may not use this file except in compliance with the License.',
    '; You may obtain a copy of the License at',
    ';',
    ';     http://www.apache.org/licenses/LICENSE-2.0',
    ';',
    '; Unless required by applicable law or agreed to in writing, software',
    '; distributed under the License is distributed on an "AS IS" BASIS,',
    '; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    '; See the License for the specific language governing permissions and',
    '; limitations under the License.',
    ';']

legion_apache_lanl = [
    '/* Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    ' *',
    ' * Licensed under the Apache License, Version 2.0 (the "License");',
    ' * you may not use this file except in compliance with the License.',
    ' * You may obtain a copy of the License at',
    ' *',
    ' *     http://www.apache.org/licenses/LICENSE-2.0',
    ' *',
    ' * Unless required by applicable law or agreed to in writing, software',
    ' * distributed under the License is distributed on an "AS IS" BASIS,',
    ' * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    ' * See the License for the specific language governing permissions and',
    ' * limitations under the License.',
    ' */']

legion_reset_line_apache_lanl = [
    '/* Copyright 2014 Stanford University and Los Alamos National Security, LLC',
    ' *',
    ' * Licensed under the Apache License, Version 2.0 (the "License");',
    ' * you may not use this file except in compliance with the License.',
    ' * You may obtain a copy of the License at',
    ' *',
    ' *     http://www.apache.org/licenses/LICENSE-2.0',
    ' *',
    ' * Unless required by applicable law or agreed to in writing, software',
    ' * distributed under the License is distributed on an "AS IS" BASIS,',
    ' * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.',
    ' * See the License for the specific language governing permissions and',
    ' * limitations under the License.',
    ' */',
    '',
    '#line 0']

# These files have licenses added manually or have no license for some
# other reason.
none = []

class SourceFile:
    def __init__(self, src, dst = None, license = None):
        if dst is None:
            dst = src
        self.src_postfix = src
        self.dst_postfix = dst
        self.license = license

    def move_file(self, src_prefix, dst_prefix):
        src_path = os.path.join(src_prefix, self.src_postfix)
        dst_path = os.path.join(dst_prefix, self.dst_postfix)

        # Touch the file to ensure directories and permissions exist.
        if not os.path.isdir(os.path.dirname(dst_path)):
            raise Exception('Directory %s does not exist' % dst_path)

        # Copy the file.
        with open(dst_path, 'wb') as dst_file:
            with open(src_path, 'rb') as src_file:
                dst_file.write(self.modify_content(src_file.read()))

        # Copy the permissions of the original file.
        try:
            src_mode = os.stat(src_path).st_mode
            os.chmod(dst_path, src_mode)
        except:
            raise Exception('Chmod failed on %s' % dst_path)

    def modify_content(self, content):
        if self.license == None:
            return content
        if len(self.license) == 0:
            return content
        return '\n'.join(self.license + [''] + content.split('\n'))

class PythonSourceFile(SourceFile):
    def modify_content(self, content):
        lines = content.split('\n')
        if lines[0].startswith('#!'):
            lines = lines[1:]
            if len(lines[0].strip()) == 0:
                lines = lines[1:]
        return '\n'.join(self.license + [''] + lines)

class SourceDirectory:
    def __init__(self, src, dst = None, license_map = None):
        assert license_map is not None
        if dst is None:
            dst = src
        self.src_postfix = src
        self.dst_postfix = dst
        self.license_map = license_map

    def move_file(self, src_prefix, dst_prefix):
        src_path = os.path.join(src_prefix, self.src_postfix)
        dst_path = os.path.join(dst_prefix, self.dst_postfix)

        if not os.path.isdir(dst_path):
            raise Exception('Directory %s does not exist' % dst_path)

        def helper(_ignore, src_dirname, filenames):
            postfix = os.path.relpath(src_dirname, src_prefix)
            dst_dirname = os.path.join(dst_prefix, postfix)

            if not os.path.isdir(dst_dirname):
                os.mkdir(dst_dirname)

            for filename in filenames:
                ext = os.path.splitext(filename)[1]
                if ext in self.license_map:
                    src_file = SourceFile(
                        src = os.path.join(postfix, filename),
                        license = self.license_map[ext])
                    src_file.move_file(src_prefix, dst_prefix)
        os.path.walk(src_path, helper, None)

def get_files():
    files = [
        # Runtime files
        SourceFile(src = 'runtime/accessor.h', license = cpp_apache),
        SourceFile(src = 'runtime/arrays.h', license = cpp_apache),
        SourceFile(src = 'runtime/activemsg.h', license = cpp_apache),
        SourceFile(src = 'runtime/common.h', license = cpp_apache),
        SourceFile(src = 'runtime/default_mapper.h', license = cpp_apache),
        SourceFile(src = 'runtime/field_tree.h', license = cpp_apache),
        SourceFile(src = 'runtime/garbage_collection.h', license = cpp_apache),
        SourceFile(src = 'runtime/interval_tree.h', license = cpp_apache),
        SourceFile(src = 'runtime/rectangle_set.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_logging.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_profiling.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_spy.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_ops.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_tasks.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_trace.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_types.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_utilities.h', license = cpp_apache),
        SourceFile(src = 'runtime/legion_allocation.h', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel.h', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_impl.h', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_dma.h', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_gpu.h', license = cpp_apache),
        SourceFile(src = 'runtime/region_tree.h', license = cpp_apache),
        SourceFile(src = 'runtime/runtime.h', license = cpp_apache),
        SourceFile(src = 'runtime/shim_mapper.h', license = cpp_apache),
        SourceFile(src = 'runtime/utilities.h', license = cpp_apache),
        SourceFile(src = 'runtime/activemsg.cc', license = cpp_apache),
        SourceFile(src = 'runtime/default_mapper.cc', license = cpp_apache),
        SourceFile(src = 'runtime/garbage_collection.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_logging.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_spy.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_ops.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_tasks.cc', license = cpp_apache),
        SourceFile(src = 'runtime/legion_trace.cc', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel.cc', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_dma.cc', license = cpp_apache),
        SourceFile(src = 'runtime/lowlevel_gpu.cc', license = cpp_apache),
        SourceFile(src = 'runtime/region_tree.cc', license = cpp_apache),
        SourceFile(src = 'runtime/runtime.cc', license = cpp_apache),
        SourceFile(src = 'runtime/runtime.mk', license = make_apache),
        SourceFile(src = 'runtime/shared_lowlevel.cc', license = cpp_apache),
        SourceFile(src = 'runtime/shim_mapper.cc', license = cpp_apache),
        SourceFile(src = 'runtime/alt_mappers.h', license = cpp_apache),
        SourceFile(src = 'runtime/alt_mappers.cc', license = cpp_apache),
        SourceFile(src = 'runtime/mapping_utilities.h', license = cpp_apache),
        SourceFile(src = 'runtime/mapping_utilities.cc', license = cpp_apache),
        # Heptane
        SourceFile(src = 'apps/s3d/C7H16_kernels/rhsf_gpu.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/rhsf_gpu.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/chemistry/sse_getrates.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/chemistry/sse_getrates.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/chemistry/avx_getrates.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/chemistry/avx_getrates.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/chemistry/base_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/chemistry/fast_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/chemistry/base_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/chemistry/fast_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/sse_getcoeffs.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/sse_getcoeffs.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/avx_getcoeffs.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/avx_getcoeffs.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/conductivity/conduc_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/conductivity/conduc_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/diffusion/diff_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/diffusion/diff_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/thermal/therm_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/thermal/therm_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/viscosity/visc_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/C7H16_kernels/physics/viscosity/visc_kepler.cu', license = cpp_apache),
        # DME
        SourceFile(src = 'apps/s3d/DME_kernels/rhsf_gpu.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/rhsf_gpu.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/chemistry/sse_getrates.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/chemistry/sse_getrates.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/chemistry/avx_getrates.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/chemistry/avx_getrates.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/chemistry/base_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/chemistry/fast_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/chemistry/base_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/chemistry/fast_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/sse_getcoeffs.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/sse_getcoeffs.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/avx_getcoeffs.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/avx_getcoeffs.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/conductivity/conduc_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/conductivity/conduc_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/diffusion/diff_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/diffusion/diff_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/thermal/therm_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/thermal/therm_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/viscosity/visc_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/DME_kernels/physics/viscosity/visc_kepler.cu', license = cpp_apache),
        # PRF
        SourceFile(src = 'apps/s3d/PRF_kernels/rhsf_gpu.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/rhsf_gpu.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/chemistry/sse_getrates.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/chemistry/sse_getrates.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/chemistry/avx_getrates.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/chemistry/avx_getrates.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/chemistry/base_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/chemistry/base_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/sse_getcoeffs.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/sse_getcoeffs.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/avx_getcoeffs.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/avx_getcoeffs.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/conductivity/conduc_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/conductivity/conduc_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/diffusion/diff_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/diffusion/diff_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/thermal/therm_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/thermal/therm_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/viscosity/visc_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/PRF_kernels/physics/viscosity/visc_kepler.cu', license = cpp_apache),
        # H2
        SourceFile(src = 'apps/s3d/H2_kernels/rhsf_gpu.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/rhsf_gpu.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/chemistry/sse_getrates.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/chemistry/sse_getrates.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/chemistry/avx_getrates.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/chemistry/avx_getrates.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/chemistry/base_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/chemistry/base_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/sse_getcoeffs.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/sse_getcoeffs.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/avx_getcoeffs.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/avx_getcoeffs.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/conductivity/conduc_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/conductivity/conduc_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/diffusion/diff_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/diffusion/diff_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/thermal/therm_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/thermal/therm_kepler.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/viscosity/visc_fermi.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d/H2_kernels/physics/viscosity/visc_kepler.cu', license = cpp_apache),
        # Singe
        SourceFile(src = 'apps/s3d/singe/README.txt', license = make_apache),
        SourceFile(src = 'apps/s3d/singe/Makefile', license = make_apache),
        SourceFile(src = 'apps/s3d/singe/chemistry.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/codegen.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/codegen.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/devil.c', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/globals.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/main.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/parse.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/physics.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/singe.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/singe.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/singe.lex', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/singe.y', license = cpp_apache),
        SourceFile(src = 'apps/s3d/singe/inputs/DME/chem.asc', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/DME/chem.inp', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/DME/qsslist.txt', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/DME/qss_stif.txt', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/DME/stiflist.txt', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/DME/therm.dat', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/DME/tran.asc', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/DME/tran.dat', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/h2/chem.inp', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/h2/therm.dat', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/h2/tran.asc', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/nC7H16/chem.inp', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/nC7H16/qsslist.txt', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/nC7H16/qss_stif.txt', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/nC7H16/stiflist.txt', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/nC7H16/therm.dat', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/nC7H16/tran.dat', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/PRF/chem.asc', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/PRF/chem.inp', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/PRF/qsslist.txt', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/PRF/qss_stif.txt', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/PRF/stiflist.txt', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/PRF/therm.dat', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/PRF/tran.asc', license = None),
        SourceFile(src = 'apps/s3d/singe/inputs/PRF/tran.dat', license = None),
        # S3D RHSF files
        SourceFile(src = 'apps/s3d_rhsf/Makefile', license = make_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_chem.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_chem.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_flux.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_flux.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_physics.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_physics.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_pre.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_pre.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_stencil.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_stencil.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_sum.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_sum.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_temp.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_temp.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_volume.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/calc_volume.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/check_field.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/check_field.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/cpu_stencils.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/cpu_stencils.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/gpu_check.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/gpu_flux.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/gpu_stencils.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/gpu_stencils.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/gpu_sum.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/gpu_temp.cu', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rank_helper.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rhsf.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rhsf.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rhsf_fortran.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rhsf_test.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rhsf_impl.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rhsf_impl.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rhsf_init.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rhsf_init.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rhsf_mapper.h', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/rhsf_mapper.cc', license = cpp_apache),
        SourceFile(src = 'apps/s3d_rhsf/chemistry/dme/chem.asc', license = None),
        SourceFile(src = 'apps/s3d_rhsf/chemistry/H2/chem.asc', license = None),
        SourceFile(src = 'apps/s3d_rhsf/chemistry/heptane/chem.asc', license = None),
        SourceFile(src = 'apps/s3d_rhsf/chemistry/prf/chem.asc', license = None),
        SourceFile(src = 'apps/s3d_rhsf/codegen/cklib.lua', license = lua_apache),
        SourceFile(src = 'apps/s3d_rhsf/codegen/gen_calc_temp.lua', license = lua_apache),
        SourceFile(src = 'apps/s3d_rhsf/codegen/refvals.lua', license = lua_apache),
        # Scripts 
        SourceFile(src = 'apps/s3d_stuff/buildme', license = None),
        SourceFile(src = 'apps/s3d_stuff/run_dmedebug.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_dmehuge.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_dmelarge.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_dmemedium.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_dmesmall.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_dmetiny.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_heptdebug.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_hepthuge.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_heptlarge.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_heptmedium.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_heptsmall.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_hepttiny.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_prfhuge.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_prflarge.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_prfmedium.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_prfsmall.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_prftiny.pbs', license = make_apache),
        SourceFile(src = 'apps/s3d_stuff/run_s3d.py', license = py_apache),
        ]
    return files

def main():
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print 'Usage: %s [src_path] <target_path>' % sys.argv[0]
        sys.exit(1)

    src_dir = None
    dst_dir = None

    if len(sys.argv) == 2:
        if not 'LG_RT_DIR' in os.environ:
            print 'LG_RT_DIR environment variable is not set'
            sys.exit(2)
        src_dir = os.path.join(os.environ['LG_RT_DIR'], '..')
        dst_dir = sys.argv[1]
    else:
        assert len(sys.argv) == 3
        src_dir = sys.argv[1]
        dst_dir = sys.argv[2]

    files = get_files()

    for f in files:
        f.move_file(src_dir, dst_dir)
    print 'Success'

if __name__ == '__main__':
    main()
