#!/usr/bin/perl -w

use strict;

use FileHandle;
use Data::Dumper;

sub any_zeros {
    for (@_) {
	return 1 if(defined($_) && ($_ == 0));
    }
    return 0;
}

sub strip_defines {
    my($ifh, $ofh, $is_defined, $define_val) = @_;

    my $ifstack = [];
    my $printing = 1;

    while(defined(my $line = <$ifh>)) {
	$line =~ /^\s*\#if(n?)def\s+(\S+)/ && do {
	    my $negate = $1 ? 1 : 0;
	    my $defname = $2;

	    # is this a define we're stripping?
	    if(exists($is_defined->{$defname})) {
		my $s = $is_defined->{$defname} ^ $negate;
		push @$ifstack, $s;
		$printing = $printing && $s;
		#print $defname, " ", Dumper($ifstack);
		# never print
	    } else {
		# this is an ifdef with an unknown state - keep it on stack
		push @$ifstack, undef;
		print $ofh $line if($printing);
	    }
	    next;
	};

	$line =~ /^\s*\#if\b/ && do {
	    # no attempt to parse if expressions right now

	    push @$ifstack, undef;
	    print $ofh $line if($printing);

	    next;
	};

	$line =~ /^\s*\#else\b/ && do {
	    my $s = $ifstack->[-1];
	    if(defined($s)) {
		$ifstack->[-1] = 1 - $s;
		# recalculate printing state
		$printing = !any_zeros(@$ifstack);
	    } else {
		print $ofh $line if($printing);
	    }
	    next;
	};

	$line =~ /^\s*\#endif\b/ && do {
	    my $s = pop @$ifstack;
	    if(defined($s)) {
		# recalculate printing state
		$printing = !any_zeros(@$ifstack);
	    } else {
		print $ofh $line if($printing);
	    }
	    next;
	};

	# fall through - print if we're printing
	if($printing) {
	    print $ofh $line;
	} else {
	    # warn if we've stripped out a #define that might impact things
	    if($line =~ /^\s*\#define\b/) {
		print STDERR "WARNING: #define removed: ", $line;
	    }
	}
    }

    die "non-empty stack!" if(scalar(@$ifstack));
}

my $is_defined = {};
my $define_val = {};
my $num_files_done = 0;
my $keep_backups = 0;

foreach my $x (@ARGV) {
    $x =~ /^\+(.+)$/ && do {
	$is_defined->{$1} = 1;
	next;
    };

    $x =~ /^\-(.+)$/ && do {
	$is_defined->{$1} = 0;
	delete $define_val->{$1};
	next;
    };

    $x =~ /^(.+)=(.+)$/ && do {
	$is_defined->{$1} = 1;
	$define_val->{$1} = $2;
	next;
    };

    # assume it's a filename
    my $xbkp = $x . ".bkp";
    rename($x, $xbkp) || die "rename: $@";
    my $ifh = FileHandle->new($xbkp, "r") || die "read $xbkp: $@";
    my $ofh = FileHandle->new($x, "w") || die "write $x: $@";
    strip_defines($ifh, $ofh, $is_defined, $define_val);
    $ifh->close();
    $ofh->close();
    unlink($xbkp) unless($keep_backups);
    $num_files_done++;
}

# if no files were given, filter stdin to stdout
strip_defines(\*STDIN, \*STDOUT, $is_defined, $define_val)
    unless($num_files_done);
